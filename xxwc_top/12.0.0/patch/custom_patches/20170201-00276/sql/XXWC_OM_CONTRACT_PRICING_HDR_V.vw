/*************************************************************************
*   SCRIPT Name: create_header
*
*   PURPOSE:   To create view
*   HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.2     26/01/2018     Niraj K ranjan         TMS#20170201-00276   CSP Form Should be modified to accommodate Matrix modifiers--Matrix Maintenance
****************************************************************************/
CREATE OR REPLACE VIEW APPS.XXWC_OM_CONTRACT_PRICING_HDR_V
(
   AGREEMENT_ID
 , PRICE_TYPE
 , CUSTOMER_ID
 , CUSTOMER_SITE_ID
 , AGREEMENT_STATUS
 , REVISION_NUMBER
 , ORGANIZATION_ID
 , GROSS_MARGIN
 , INCOMPATABILITY_GROUP
 , CUSTOMER_INFO
 , SITE_INFO
 , BRANCH_INFO
 , PARTY_SITE_NUMBER
 , agreement_type --ver 1.2
)
AS
   SELECT a.agreement_id
        , a.price_type
        , a.customer_id
        , a.customer_site_id
        , a.agreement_status
        , a.revision_number
        , a.organization_id
        , a.gross_margin
        , a.incompatability_group
        , (SELECT account_number || ' - ' || party_name
             FROM hz_cust_accounts acc, hz_parties hzp
            WHERE     acc.party_id = hzp.party_id
                  AND acc.cust_account_id = a.customer_id
                  AND a.price_type <> 'NATIONAL'
           UNION
           SELECT description
             FROM fnd_lookup_values
            WHERE     lookup_type = 'NATIONAL_ACCOUNTS'
                  AND lookup_code = TO_CHAR (a.customer_id)
                  AND a.price_type = 'NATIONAL')
             customer_info
        , (SELECT location
             FROM apps.hz_cust_site_uses hsu
            WHERE hsu.site_Use_id = a.customer_site_id)
             site_info
        , (SELECT org_code_name
             FROM xxwc_inv_organization_v inv
            WHERE inv.organization_id = a.organization_id)
             branch_info
             --Shankar TMS 20130201-01407
        ,  ( SELECT z.party_site_number
                 FROM apps.hz_cust_site_uses x
                    , apps.hz_cust_acct_sites y
                    , hz_party_sites z
                WHERE x.cust_acct_site_id = y.cust_acct_site_id
                      AND y.party_site_id = z.party_site_id
                      AND x.site_use_id = a.customer_site_id
            )  PARTY_SITE_NUMBER 
        , a.agreement_type --ver 1.2		
     FROM apps.xxwc_om_contract_pricing_hdr a
/     
