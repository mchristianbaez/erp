/*************************************************************************
    *   SCRIPT Name: XXWC_OM_CONTRACT_PRICING_HDR_ALTER
    *
    *   PURPOSE:   Alter table XXWC_OM_CONTRACT_PRICING_HDR
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     01/16/2018     Niraj K ranjan   TMS#20170201-00276   CSP Form Should be modified to accommodate Matrix modifiers--Matrix Maintenance
*****************************************************************************/
UPDATE xxwc.xxwc_om_contract_pricing_hdr
SET agreement_type = 'CSP'
WHERE agreement_type IS NULL
AND nvl(agreement_type,'CSP') <> 'MTX';
COMMIT;