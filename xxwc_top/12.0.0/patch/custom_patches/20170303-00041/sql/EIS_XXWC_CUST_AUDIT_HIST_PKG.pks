CREATE OR REPLACE 
PACKAGE       XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG
AS
  --//============================================================================
  --// Object Name          :: EIS_XXWC_CUST_AUDIT_HIST_PKG
  --//
  --// Object Type          :: Package Specification
  --//
  --// Object Description   :: This Package will trigger in view and populate the values into View.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva          11/22/2017         TMS#20170303-00041  
  --//============================================================================
type T_VARCHAR_TYPE_VLDN_TBL is table of varchar2(200) index by varchar2(200);

G_Meaning_Vldn_Tbl   T_Varchar_Type_Vldn_Tbl;
g_meaning varchar2(240);

G_INSERT_VALUE VARCHAR2(240);
g_insert_value_vldn_tbl T_Varchar_Type_Vldn_Tbl;

FUNCTION GET_LOOKUP_VALUE(
    P_LOOKUP_CODE VARCHAR2,
    P_Lookup_Type   Varchar2)
  RETURN VARCHAR2;
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Customer Audit History Report - WC"
  --//
  --// Object Name          :: GET_LOOKUP_VALUE
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva           11/22/2017         TMS#20170303-00041 
  --//============================================================================ 
  
  FUNCTION get_cust_new_value(
      P_Table_Name   IN VARCHAR2,
      P_COLUMN_NAME  IN VARCHAR2,
      P_Rowkey       IN NUMBER,
      P_CUST_ACCT_ID IN NUMBER,
      p_order_column IN VARCHAR2)
    RETURN VARCHAR2;
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Customer Audit History Report - WC"
  --//
  --// Object Name          :: get_cust_new_value
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva           11/22/2017         TMS#20170303-00041 
  --//============================================================================ 
  
  FUNCTION get_cust_insert_value(
      P_Table_Name   IN VARCHAR2,
      P_COLUMN_NAME  IN VARCHAR2,
      P_CUST_ACCT_ID IN NUMBER,
       p_order_column IN VARCHAR2)
    RETURN VARCHAR2;  
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Customer Audit History Report - WC"
  --//
  --// Object Name          :: get_cust_insert_value
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva           11/22/2017         TMS#20170303-00041 
  --//============================================================================ 
  
END;
/
