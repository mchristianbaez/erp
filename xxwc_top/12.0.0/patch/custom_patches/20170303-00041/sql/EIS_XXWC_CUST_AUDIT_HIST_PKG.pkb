CREATE OR REPLACE 
PACKAGE BODY   XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG
AS
  --//============================================================================
  --// Object Name          :: EIS_XXWC_CUST_AUDIT_HIST_PKG
  --//
  --// Object Type          :: Package body
  --//
  --// Object Description   :: This Package will trigger in view and populate the values into View.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva          11/22/2017         TMS#20170303-00041  
  --//============================================================================
  
FUNCTION GET_LOOKUP_VALUE(
    P_LOOKUP_CODE VARCHAR2,
    P_LOOKUP_TYPE   VARCHAR2)
  RETURN VARCHAR2
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Customer Audit History Report - WC"
  --//
  --// Object Name          :: GET_LOOKUP_VALUE
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva           11/22/2017         TMS#20170303-00041 
  --//============================================================================ 
  
L_HASH_INDEX varchar2(100);
L_SQL varchar2(32000);
begin
g_meaning := NULL;

l_sql :='select arl.meaning From Apps.Ar_Lookups Arl
Where Arl.Lookup_Code = :1
And Arl.Lookup_Type = :2
';
    BEGIN
        l_hash_index:=P_LOOKUP_CODE||'-'||P_LOOKUP_TYPE;
        g_meaning := G_MEANING_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO g_meaning USING P_LOOKUP_CODE,P_LOOKUP_TYPE;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_meaning := null;
    When Others Then
    g_meaning := null;
    END;      
                      l_hash_index:=P_LOOKUP_CODE||'-'||P_LOOKUP_TYPE;
                       G_MEANING_VLDN_TBL(L_HASH_INDEX) := g_meaning;
    END;
     return  g_meaning;
     Exception When Others Then
      g_meaning:=null;
      RETURN  g_meaning;

end GET_LOOKUP_VALUE;

 FUNCTION get_cust_new_value(
      P_TABLE_NAME   IN VARCHAR2,
      P_COLUMN_NAME  IN VARCHAR2,
      P_ROWKEY       IN NUMBER,
      P_CUST_ACCT_ID IN NUMBER,
      P_ORDER_COLUMN in varchar2)
    RETURN VARCHAR2
  as
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Customer Audit History Report - WC"
  --//
  --// Object Name          :: get_cust_new_value
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva           11/22/2017         TMS#20170303-00041 
  --//============================================================================ 
  
    L_Var VARCHAR2(100);
    L_Sql VARCHAR2(10000);
  BEGIN
    l_sql:='select '||P_Column_Name||' from 
            (
              Select    
                  '||P_Column_Name||',  
                    Row_Key,   
                    Rank() Over (Partition By '||p_order_column||' Order By Row_Key) Rn
              From '|| P_Table_Name||' 
              Where '||p_order_column||'='||P_Cust_Acct_Id||' 
              )
            where rn =
                  (Select rn+1 from ( 
                      Select    
                          '||P_Column_Name||',  
                            Row_Key,   
                            Rank() Over (Partition By '||p_order_column||' Order By Row_Key) Rn
                    From '|| P_Table_Name||' 
                      Where '||p_order_column||'='||P_Cust_Acct_Id||' 
                    )
                    Where Row_Key ='|| P_Rowkey||' )';

    EXECUTE immediate l_sql INTO L_Var ;
    RETURN L_Var;
  EXCEPTION
  WHEN OTHERS THEN
 fnd_file.Put_Line(fnd_file.log,'sql error : '||Sqlerrm);

    RETURN NULL;
  END get_cust_new_value;


FUNCTION get_cust_insert_value(
      P_Table_Name   IN VARCHAR2,
      P_COLUMN_NAME  IN VARCHAR2,
      P_CUST_ACCT_ID IN NUMBER,
       P_ORDER_COLUMN in varchar2)
    RETURN VARCHAR2
  as
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Customer Audit History Report - WC"
  --//
  --// Object Name          :: get_cust_insert_value
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva           11/22/2017         TMS#20170303-00041 
  --//============================================================================   
  
L_HASH_INDEX VARCHAR2(200);
L_SQL varchar2(32000);
BEGIN
g_insert_value := NULL;

l_sql :='select '||P_Column_Name||' from 
            (
              Select    
                  '||P_Column_Name||',  
                    Row_Key,   
                    Rank() Over (Partition By '||p_order_column||' Order By Row_Key) Rn
              From '|| P_Table_Name||' 
              Where '||p_order_column||' ='||P_Cust_Acct_Id||' 
              )
            where rn = 1';
    BEGIN
        l_hash_index:= P_TABLE_NAME||'-'||P_COLUMN_NAME||'-'||P_CUST_ACCT_ID||'-'||P_ORDER_COLUMN;
        g_insert_value := g_insert_value_vldn_tbl(L_HASH_INDEX);
        
 exception
    WHEN NO_DATA_FOUND
    THEN
    BEGIN      
    EXECUTE IMMEDIATE L_SQL INTO G_INSERT_VALUE ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_insert_value := null;
    When Others Then
    g_insert_value := null;
    END;      
                      l_hash_index:= P_TABLE_NAME||'-'||P_COLUMN_NAME||'-'||P_CUST_ACCT_ID||'-'||P_ORDER_COLUMN;
                       g_insert_value_vldn_tbl(L_HASH_INDEX) := g_insert_value;
    END;
     return  g_insert_value;
     Exception When Others Then
      g_insert_value:=null;
      RETURN  g_insert_value;

end get_cust_insert_value;


END;
/
