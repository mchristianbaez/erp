CREATE OR REPLACE PACKAGE APPS.xxwc_om_quote_pkg
AS
   /*************************************************************************
   *   $Header xxwc_om_quote_pkg.pks $
   *   Module Name: xxwc OM Custom Quote package
   *
   *   PURPOSE:   Used in Quotes form
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        01/25/2013  Shankar Hariharan         Initial Version
   *   2.0        10/08/2013  Ram Talluri               TMS-20131003-00186 - Custom Quote to SO Imports - Price Adjustments Missing
   *   3.0        04/10/2014  Maharajan Shunmugam and Ram   TMS#20140407-00346 - Performance issue
   *   4.0        04/02/2018  Niraj K Ranjan            TMS#20180321-00087   WC Custom Quote Enhancements
   * ***************************************************************************/
   PROCEDURE calculate_tax (i_quote_number    IN     NUMBER
                          , i_call_mode       IN     VARCHAR
                          , o_return_status      OUT VARCHAR
                          , o_msg_data           OUT VARCHAR);
/*************************************************************************
   *   Function: xxwc_to_number (p_in_string VARCHAR2)
   *   PURPOSE:   Used to convert char value to to number.
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *  1.0        10/08/2013  Ram Talluri               TMS-20131003-00186 - Custom Quote to SO Imports - Price Adjustments Missing
   **********************************************************************/
   FUNCTION xxwc_to_number(p_in_string VARCHAR2)
    RETURN NUMBER;
/*************************************************************************
   *  Procedure: process_price_adjustments (i_quote_number    IN     NUMBER)
   *   PURPOSE:   Used to convert char value to to number.
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *  1.0        10/08/2013  Ram Talluri               TMS-20131003-00186 - Custom Quote to SO Imports - Price Adjustments Missing
   **********************************************************************/
   PROCEDURE process_price_adjustments (i_quote_number    IN     NUMBER
                                       , o_return_status      OUT VARCHAR
                                       , o_msg_data           OUT VARCHAR);
/*************************************************************************
   *  Procedure: update_calc_price_flag (i_quote_number    IN     NUMBER)
   *   PURPOSE:   Used to convert char value to to number.
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *  1.0        10/08/2013  Ram Talluri               TMS-20131003-00186 - Custom Quote to SO Imports - Price Adjustments Missing
   **********************************************************************/
   PROCEDURE update_calc_price_flag (i_quote_number    IN     NUMBER
                                       , o_return_status      OUT VARCHAR
                                       , o_msg_data           OUT VARCHAR);  
    --Added below record type by Maha and Ram for ver 3.0
                                       
    TYPE XXWC_OM_TAX_REC_TYPE IS RECORD (line_number 		NUMBER,
                                         attribute4 		VARCHAR2(240),                                         
                                         inventory_item_id 	NUMBER,
                                         tax_amount 		NUMBER,
                                         qty			NUMBER,
                                         attribute18		VARCHAR2(240));
    
                                       
    TYPE XXWC_OM_TAX_TBL_TYPE IS TABLE OF XXWC_OM_TAX_REC_TYPE
    INDEX BY BINARY_INTEGER;
    
    G_MISS_XXWC_OM_TAX_TBL XXWC_OM_TAX_TBL_TYPE;
    
/*************************************************************************
    *   PROCEDURE Name: alert_quote_expiration
    *
    *   PURPOSE:   send FYI notifications 1 week prior of quote expiration and reminder 1 day before.
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    4.0     04/02/2018     Niraj K Ranjan        TMS#20180321-00087   WC Custom Quote Enhancements
   *****************************************************************************/
   PROCEDURE alert_quote_expiration(errbuf              OUT VARCHAR2,
                                    retcode             OUT NUMBER);
                          
END xxwc_om_quote_pkg;
/
