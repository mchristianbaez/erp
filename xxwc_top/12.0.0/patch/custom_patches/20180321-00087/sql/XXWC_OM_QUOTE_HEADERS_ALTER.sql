/*************************************************************************
   *   $Header XXWC_OM_QUOTE_HEADERS_ALTER.sql $
   *   Module Name: table alter script
   *
   *   PURPOSE:   table alter script for table xxwc.XXWC_OM_QUOTE_HEADERS
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        04/02/2018  Niraj K Ranjan          TMS#20180321-00087   WC Custom Quote Enhancements
   * ***************************************************************************/
ALTER TABLE xxwc.XXWC_OM_QUOTE_HEADERS
ADD (expire_alert_sent VARCHAR2(1));