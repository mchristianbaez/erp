/*
 TMS: 20161221-00135
 Date: 12/21/2016
 Notes:  Prefix 2017 offer names with TERM- that were created in DRAFT status during mass copy on 20-DEC-16 
*/
SET SERVEROUTPUT ON SIZE 1000000
declare
    cursor offers (p_date in date) is
    select a.offer_id, a.org_id, a.offer_code, a.qp_list_header_id, a.object_version_number obj_ver_num, b.description offer_name
    from apps.ozf_offers a, apps.qp_list_headers_vl b
    where 1 =1
         and trunc(a.creation_date) =p_date
         and b.list_header_id =a.qp_list_header_id
         and b.attribute7 ='2017' 
         and a.status_code ='DRAFT'
         and b.list_header_id !=3720124
         ;

    x_offer_adjustment_id   NUMBER;
    x_offer_adjst_tier_id   NUMBER;
    x_offer_adj_line_id     NUMBER;
    x_object_version_number NUMBER;
  
    x_qp_list_header_id NUMBER;
    x_err_location      NUMBER;
  
    l_offer_adj_rec        ozf_offer_adjustment_pvt.offer_adj_rec_type;
    l_offadj_tier_rec_type ozf_offer_adj_tier_pvt.offadj_tier_rec_type;
  
    l_offadj_line_rec_type ozf_offer_adj_line_pvt.offadj_line_rec_type;
  
    l_modifier_list_rec ozf_offer_pub.modifier_list_rec_type;
    l_modifier_line_tbl ozf_offer_pub.modifier_line_tbl_type;
    l_na_qualifier_tbl  ozf_offer_pub.na_qualifier_tbl_type;
    l_prod_rec_tbl      ozf_offer_pub.prod_rec_tbl_type;
    l_offer_tier_tbl    ozf_offer_pub.offer_tier_tbl_type;
    l_excl_rec_tbl      ozf_offer_pub.excl_rec_tbl_type;
    l_discount_line_tbl ozf_offer_pub.discount_line_tbl_type;
    l_act_product_tbl   ozf_offer_pub.act_product_tbl_type;
    l_vo_pbh_tbl        ozf_offer_pub.vo_disc_tbl_type;
    l_vo_dis_tbl        ozf_offer_pub.vo_disc_tbl_type;
    l_vo_prod_tbl       ozf_offer_pub.vo_prod_tbl_type;
    l_qualifier_tbl     ozf_offer_pub.qualifiers_tbl_type;
    l_vo_mo_tbl         ozf_offer_pub.vo_mo_tbl_type;
    l_budget_tbl        ozf_offer_pub.budget_tbl_type;
  
    l_object_version_number NUMBER;
    l_obj_version           NUMBER;
    l_start_date            DATE;
    l_offer_id              NUMBER;
    l_description           qp_list_headers_vl.description%TYPE;
    l_offer_type            VARCHAR2(50);
    l_user_status_id        NUMBER;
   --
    x_errbuf  VARCHAR2(2000);
    x_retcode VARCHAR2(2000);
    --
    x_return_status VARCHAR2(1);
    x_msg_count     NUMBER;
    x_msg_data      VARCHAR2(4000);
    --
     v_db_name varchar2(240) :=Null;
      l_date date :=Null;
     --
    l_failed number :=0;
    l_success number :=0;
    --
begin
 --
 select upper(name) into v_db_name from v$database; 
 --
     if v_db_name ='EBSPRD' then 
         l_date :='20-DEC-16';
     else
          l_date :=trunc(sysdate);
     end if; 
     -- 
    mo_global.set_policy_context('S', 101);
    --     
 for rec in offers (p_date =>l_date) loop
  mo_global.set_policy_context('S', rec.org_id);
  savepoint here_we_start;
  
      l_modifier_list_rec.qp_list_header_id     :=rec.qp_list_header_id;
      l_modifier_list_rec.object_version_number :=rec.obj_ver_num;     
      l_modifier_list_rec.status_code           :='CANCELLED'; --'TERMINATED';
      l_modifier_list_rec.user_status_id        :=1603; --1608;
      l_modifier_list_rec.modifier_operation    :='UPDATE';
      l_modifier_list_rec.offer_operation       :='UPDATE';
      l_modifier_list_rec.offer_id              :=rec.offer_id;
    
      ozf_offer_pub.process_modifiers(p_init_msg_list     => fnd_api.g_false
                                     ,p_api_version       => 1.0
                                     ,p_commit            => fnd_api.g_false
                                     ,x_return_status     => x_return_status
                                     ,x_msg_count         => x_msg_count
                                     ,x_msg_data          => x_msg_data
                                     ,p_offer_type        => l_offer_type
                                     ,p_modifier_list_rec => l_modifier_list_rec
                                     ,p_modifier_line_tbl => l_modifier_line_tbl
                                     ,p_qualifier_tbl     => l_qualifier_tbl
                                     ,p_budget_tbl        => l_budget_tbl
                                     ,p_act_product_tbl   => l_act_product_tbl
                                     ,p_discount_tbl      => l_discount_line_tbl
                                     ,p_excl_tbl          => l_excl_rec_tbl
                                     ,p_offer_tier_tbl    => l_offer_tier_tbl
                                     ,p_prod_tbl          => l_prod_rec_tbl
                                     ,p_na_qualifier_tbl  => l_na_qualifier_tbl
                                     ,x_qp_list_header_id => x_qp_list_header_id
                                     ,x_error_location    => x_err_location);
    
      IF ((x_return_status = fnd_api.g_ret_sts_error) OR (x_return_status = fnd_api.g_ret_sts_unexp_error)) THEN        
        dbms_output.put_line('offer '||rec.offer_name||', status failed to update to TERMINATED, message :'||x_msg_data||', msg count '||x_msg_count);    
        rollback to here_we_start;
        l_failed :=l_failed+1;      
        FOR i IN 0 .. x_msg_count
        LOOP
           dbms_output.put_line(substr(fnd_msg_pub.get(p_msg_index => i
                                          ,p_encoded   => 'F')
                          ,1
                          ,254));
        END LOOP;        
      ELSE       
        --dbms_output.put_line('offer '||rec.offer_name||', status updated successfully to TERMINATED');
        l_success :=l_success +1;            
      END IF;          
 end loop;
 --
 dbms_output.put_line('Org US - Failed Count : '||l_failed);
 dbms_output.put_line('Org US - Success Count : '||l_success); 
 --dbms_output.put_line('Total offers processed : '||l_success + l_failed); 
 --
 commit;
 --
 dbms_output.put_line('Org US - Commit Complete...');
 --
-- Begin Canadian Org 
 --
    mo_global.set_policy_context('S', 102);
    --     
 for rec in offers (p_date =>l_date) loop
  mo_global.set_policy_context('S', rec.org_id);
  savepoint here_we_start;
  
      l_modifier_list_rec.qp_list_header_id     :=rec.qp_list_header_id;
      l_modifier_list_rec.object_version_number :=rec.obj_ver_num;     
      l_modifier_list_rec.status_code           :='CANCELLED'; --'TERMINATED';
      l_modifier_list_rec.user_status_id        :=1603; --1608;
      l_modifier_list_rec.modifier_operation    :='UPDATE';
      l_modifier_list_rec.offer_operation       :='UPDATE';
      l_modifier_list_rec.offer_id              :=rec.offer_id;
    
      ozf_offer_pub.process_modifiers(p_init_msg_list     => fnd_api.g_false
                                     ,p_api_version       => 1.0
                                     ,p_commit            => fnd_api.g_false
                                     ,x_return_status     => x_return_status
                                     ,x_msg_count         => x_msg_count
                                     ,x_msg_data          => x_msg_data
                                     ,p_offer_type        => l_offer_type
                                     ,p_modifier_list_rec => l_modifier_list_rec
                                     ,p_modifier_line_tbl => l_modifier_line_tbl
                                     ,p_qualifier_tbl     => l_qualifier_tbl
                                     ,p_budget_tbl        => l_budget_tbl
                                     ,p_act_product_tbl   => l_act_product_tbl
                                     ,p_discount_tbl      => l_discount_line_tbl
                                     ,p_excl_tbl          => l_excl_rec_tbl
                                     ,p_offer_tier_tbl    => l_offer_tier_tbl
                                     ,p_prod_tbl          => l_prod_rec_tbl
                                     ,p_na_qualifier_tbl  => l_na_qualifier_tbl
                                     ,x_qp_list_header_id => x_qp_list_header_id
                                     ,x_error_location    => x_err_location);
    
      IF ((x_return_status = fnd_api.g_ret_sts_error) OR (x_return_status = fnd_api.g_ret_sts_unexp_error)) THEN        
        dbms_output.put_line('offer '||rec.offer_name||', status failed to update to TERMINATED, message :'||x_msg_data||', msg count '||x_msg_count);    
        rollback to here_we_start;
        l_failed :=l_failed+1;      
        FOR i IN 0 .. x_msg_count
        LOOP
           dbms_output.put_line(substr(fnd_msg_pub.get(p_msg_index => i
                                          ,p_encoded   => 'F')
                          ,1
                          ,254));
        END LOOP;        
      ELSE       
        --dbms_output.put_line('offer '||rec.offer_name||', status updated successfully to TERMINATED');
        l_success :=l_success +1;            
      END IF;          
 end loop;
 --
 dbms_output.put_line('Org CAD - Failed Count : '||l_failed);
 dbms_output.put_line('Org CAD - Success Count : '||l_success); 
 --dbms_output.put_line('Total offers processed : '||l_success + l_failed); 
 --
 commit;
 --
 dbms_output.put_line('Org CAD - Commit Complete...');
 --
exception
 when others then
  rollback to here_we_start;       
END;
/