/*
 TMS: 20160404-00014
 Date: 04/04/2016
 Notes: OEOH to fix the workflow for pick ticket printing
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   	DBMS_OUTPUT.put_line ('TMS: 20160404-00014 , Script Before Delete');
	
	DELETE FROM XXWC.xxwc_oe_open_order_lines op
	WHERE EXISTS( SELECT 1
                    FROM apps.oe_order_headers_all oh,
                         apps.oe_order_lines_all ol,
                         apps.mtl_system_items msi,
                         apps.org_organization_definitions od, 
                         apps.oe_Transaction_types_tl ot
                   WHERE 1=1
                     AND oh.header_id          = ol.header_id
                     AND oh.header_id          = op.header_id
                     AND ol.line_id            = op.line_id
                     AND msi.inventory_item_id = op.inventory_item_id
                     AND msi.organization_id   = op.organization_id    
                     AND od.organization_id    = op.organization_id
                     AND oh.order_type_id      = ot.transaction_type_id
                     AND ol.source_type_code   = 'EXTERNAL'
                     AND ot.name               = 'STANDARD ORDER'
	            );
	
	DBMS_OUTPUT.put_line ('TMS: 20160404-00014, After Delete, records Deleted : '||sql%rowcount);
	
	COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160404-00014, Errors ='||SQLERRM);
END;
/