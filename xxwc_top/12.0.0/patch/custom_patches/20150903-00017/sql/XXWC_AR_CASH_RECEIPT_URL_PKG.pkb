CREATE OR REPLACE PACKAGE BODY XXWC_AR_CASH_RECEIPT_URL_PKG IS
   /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header XXWC_AR_CASH_RECEIPT_URL_PKG $
     Module Name: XXWC_AR_CASH_RECEIPT_URL_PKG.pkb

     PURPOSE:   Update Image url of AR Cash Receipt

     REVISIONS:
     Ver        Date         Author                     Description
     ---------  ----------   ---------------         -------------------------
     1.0        13-SEP-2016  Niraj K Ranjan          Initial Version - 20150903-00017   IT - data script for new URL on lockbox receipts
  **************************************************************************/
  -- Error DEBUG
  l_err_callfrom  VARCHAR2(75) DEFAULT 'XXWC_AR_CASH_RECEIPT_URL_PKG';
  g_dflt_email   fnd_user.email_address%TYPE    := 'WC-ITDEVALERTS-U1@HDSupply.com';
  g_retmsg   VARCHAR2(2000);
  
  /********************************************************************************
   ProcedureName : WRITE_OUTPUT
   Purpose       : write output in output file

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     13-SEP-2016   Niraj K Ranjan  Initial Version - 20150903-00017   IT - data script for new URL on lockbox receipts
   ********************************************************************************/
   PROCEDURE write_output(p_request_id     IN NUMBER
                         ,p_retcode        OUT VARCHAR2
                         ,p_retmsg         OUT VARCHAR2
                         )
   IS
      CURSOR cr_urls(p_req_id NUMBER)
	  IS
	     SELECT DCTM_OBJECT_ID,
		        APEX_OBJECT_ID,
		        APEX_ENCRYPTED_ID,
				STATUS,
				ERROR_MESSAGE
         FROM xxwc_ar_cash_receipts_url_stg
		 WHERE  request_id = p_req_id;
   BEGIN
      fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG , 'Start of Procedure : write_output'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

      fnd_file.put_line (fnd_file.output,
                           'DCTM_OBJECT_ID'
                        || '|'
						|| 'APEX_OBJECT_ID'
						|| '|'
                        || 'APEX_ENCRYPTED_ID'
                        || '|'
                        || 'STATUS'
						|| '|'
						|| 'ERROR_MESSAGE'
						);				
					
      FOR rec_url IN cr_urls(p_request_id)
      LOOP
            fnd_file.put_line (fnd_file.output,
                                rec_url.DCTM_OBJECT_ID
                              || '|'
                              || rec_url.APEX_OBJECT_ID
                              || '|'
                              || rec_url.APEX_ENCRYPTED_ID
							  || '|'
							  || rec_url.STATUS
							  || '|'
							  || rec_url.ERROR_MESSAGE
		    				  );
      END LOOP;
      fnd_file.put_line (fnd_file.LOG , 'End of Procedure : write_output'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
   EXCEPTION
      WHEN OTHERS THEN
         p_retcode := SQLCODE;
         p_retmsg  := SQLERRM;
   END write_output;
    
  /********************************************************************************
   ProcedureName : XXWC_UPDATE_URL
   Purpose       : CP to update image URL of AR Cash Receipt

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     13-SEP-2016   Niraj K Ranjan  Initial Version - 20150903-00017   IT - data script for new URL on lockbox receipts
   ********************************************************************************/ 
  PROCEDURE xxwc_update_url(p_errbuf         OUT VARCHAR2
                           ,p_retcode        OUT NUMBER
						   ,p_no_of_records  IN  NUMBER
						   ) 
  IS

     e_url_exception          EXCEPTION;
	 e_too_many_records       EXCEPTION;
     l_err_msg                VARCHAR2 (2000);
     l_err_code               VARCHAR2(200);
     l_sec                    VARCHAR2 (150);
	 l_sql_stat               VARCHAR2 (3000);
	 l_row_count              NUMBER;
	 l_cash_receipt_id        NUMBER;
	 l_request_id             NUMBER;
	 
	 CURSOR cr_fetch_url(p_rownum NUMBER)
     IS
       SELECT ROWID stg_rowid,
             apex_object_id,
             apex_encrypted_id,
             apex_url,
             dctm_object_id,
             dctm_url,
             request_id,
             error_message,
             last_update_date,
             last_updated_by,
             status
        FROM xxwc_ar_cash_receipts_url_stg xacru
       WHERE xacru.status = 'ENTERED'
	   AND EXISTS(SELECT 1
                   FROM xxwc.xxwc_ar_cash_rcpt_tbl_tmp xacrt
                   WHERE xacrt.old_url = xacru.dctm_url
				 )
	   AND ROWNUM <= 	NVL(p_rownum,ROWNUM);
	   
	 TYPE CR_FETCH_URL_TAB IS TABLE OF CR_FETCH_URL%ROWTYPE
       INDEX BY BINARY_INTEGER;
     OBJ_FETCH_URL_TAB     CR_FETCH_URL_TAB;
	 
	 TYPE TYPE_CASH_URL IS TABLE OF XXWC.XXWC_AR_CASH_RCPT_TBL_TMP.NEW_URL%TYPE
      INDEX BY BINARY_INTEGER;
     OBJ_CASH_URL          TYPE_CASH_URL;
	 
	 TYPE TYPE_CASH_RECEIPT_ID
      IS TABLE OF XXWC.XXWC_AR_CASH_RCPT_TBL_TMP.CASH_RECEIPT_ID%TYPE
      INDEX BY BINARY_INTEGER;
	 OBJ_CASH_RECEIPT_ID   TYPE_CASH_RECEIPT_ID;
	 
	CURSOR CUR_FND_DOCS
    IS
      SELECT CASH_ROWID, CASH_RECEIPT_ID, OLD_URL, NEW_URL
        FROM XXWC.XXWC_AR_CASH_RCPT_TBL_TMP
		WHERE NEW_URL IS NOT NULL;

    TYPE TYPE_FND_DOCS IS TABLE OF CUR_FND_DOCS%ROWTYPE
      INDEX BY BINARY_INTEGER;
    OBJ_FND_DOCS TYPE_FND_DOCS;
	 
	 ln_cash_counter     NUMBER;
     ln_aprx_counter     NUMBER;
     ln_user_id          FND_USER.USER_ID%TYPE := FND_GLOBAL.USER_ID;
	 
  BEGIN
     p_errbuf   := 'Success';
     p_retcode  := '0';
     l_err_code := NULL;
	 l_err_msg  := NULL;
	 l_request_id := FND_GLOBAL.CONC_REQUEST_ID;
     
     fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
     fnd_file.put_line (fnd_file.LOG , 'Start of Procedure : xxwc_update_url'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
     fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
	 fnd_file.put_line (fnd_file.LOG ,'**********************************************************************************');
	 fnd_file.put_line (fnd_file.LOG ,'Parameter1=>p_no_of_records:'||p_no_of_records);
	 --fnd_file.put_line (fnd_file.LOG ,'g_retmsg:'||g_retmsg);

	 l_sec := 'truncate xxwc_ar_cash_rcpt_tbl_tm';
	 EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_AR_CASH_RCPT_TBL_TMP';
	 l_sec := 'insert into xxwc_ar_cash_rcpt_tbl_tmp';
	 INSERT /* + APPEND */ INTO XXWC.XXWC_AR_CASH_RCPT_TBL_TMP (CASH_ROWID,CASH_RECEIPT_ID,OLD_URL) 
	 (SELECT /*+ parallel (ara 8) */ ara.ROWID,ara.cash_receipt_id,ara.attribute1 
	  FROM ar_cash_receipts_all ara WHERE ara.org_id=162 AND ara.attribute1 IS NOT NULL);
	 COMMIT;
	 fnd_file.put_line (fnd_file.LOG , 'Start of lOOP for cursor cr_fetch_url'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
	 l_sec := 'Open CR_FETCH_URL';
	 OPEN CR_FETCH_URL(p_no_of_records);
	 LOOP
	    EXIT WHEN CR_FETCH_URL%NOTFOUND;
		FETCH CR_FETCH_URL BULK COLLECT INTO OBJ_FETCH_URL_TAB LIMIT 10000;
		
		fnd_file.put_line (fnd_file.LOG , 'Start loop rec_apex_counter for '||OBJ_FETCH_URL_TAB.COUNT||' records '||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
        ln_cash_counter := 1;
		l_sec := 'Loop rec_apex_counter';
		FOR rec_apex_counter IN 1 .. OBJ_FETCH_URL_TAB.COUNT
        LOOP
           BEGIN
              l_cash_receipt_id := 0;
              l_sec := 'fetch cash_receipt_Id';
              SELECT cash_receipt_Id
                INTO l_cash_receipt_id
                FROM XXWC.XXWC_AR_CASH_RCPT_TBL_TMP XACRT
               WHERE XACRT.OLD_URL =OBJ_FETCH_URL_TAB (rec_apex_counter).dctm_url;
			 --fnd_file.put_line (fnd_file.LOG , 'cash_receipt_Id:'||l_cash_receipt_id);
           EXCEPTION
              WHEN NO_DATA_FOUND
              THEN
                 OBJ_FETCH_URL_TAB (rec_apex_counter).status := 'ERROR';
                 OBJ_FETCH_URL_TAB (rec_apex_counter).error_message := 'DCTM URL not found';
                 l_cash_receipt_id := 0;
				 p_errbuf := 'Warning';
                 p_retcode := '1';
              WHEN TOO_MANY_ROWS
              THEN
			     OBJ_FETCH_URL_TAB (rec_apex_counter).status := 'ERROR';
                 OBJ_FETCH_URL_TAB (rec_apex_counter).error_message := 'DCTM URL found for more than one receipt';
                 l_cash_receipt_id := 0;
				 p_errbuf := 'Warning';
                 p_retcode := '1';
           END;
		   l_sec := 'Populate collections';
		   IF l_cash_receipt_id > 0
           THEN
              OBJ_FETCH_URL_TAB (rec_apex_counter).status := 'SUCCESS';
              OBJ_FETCH_URL_TAB (rec_apex_counter).error_message := NULL;
			  
              OBJ_CASH_URL (ln_cash_counter)        := OBJ_FETCH_URL_TAB (rec_apex_counter).apex_url;
              OBJ_CASH_RECEIPT_ID (ln_cash_counter) := l_cash_receipt_id;
              
              ln_cash_counter := ln_cash_counter + 1;
           END IF; 
		END LOOP;
		fnd_file.put_line (fnd_file.LOG , 'End loop rec_apex_counter for '||OBJ_FETCH_URL_TAB.COUNT||' records '||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
		l_sec := 'Update apex url into xxwc_ar_cash_rcpt_tbl_tmp';
		IF OBJ_CASH_URL.COUNT > 0
        THEN
           FORALL rec_new_url IN 1 .. OBJ_CASH_URL.COUNT
            UPDATE XXWC.XXWC_AR_CASH_RCPT_TBL_TMP cash
               SET CASH.NEW_URL = OBJ_CASH_URL (rec_new_url)
             WHERE CASH_RECEIPT_ID = OBJ_CASH_RECEIPT_ID (rec_new_url);
        END IF;
		l_sec := 'Update table xxwc_ar_cash_receipts_url_stg';
		FORALL rec_update_stg IN 1 .. OBJ_FETCH_URL_TAB.COUNT
           UPDATE XXWC.XXWC_AR_CASH_RECEIPTS_URL_STG xacr
            SET xacr.STATUS        = OBJ_FETCH_URL_TAB (rec_update_stg).status,
                xacr.ERROR_MESSAGE = OBJ_FETCH_URL_TAB (rec_update_stg).error_message,
				request_id         = l_request_id,
			    last_update_date   = SYSDATE,
				last_updated_by    = ln_user_id
            WHERE xacr.ROWID       = OBJ_FETCH_URL_TAB (rec_update_stg).stg_rowid;
        OBJ_FETCH_URL_TAB.DELETE;
        OBJ_CASH_RECEIPT_ID.DELETE;
        OBJ_CASH_URL.DELETE;
		COMMIT;
	 END LOOP;
	 CLOSE CR_FETCH_URL;
     fnd_file.put_line (fnd_file.LOG , 'End of lOOP for cursor cr_fetch_url'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
	 l_sec := 'Merge into ar_cash_receipts_all';
	 l_sql_stat :=
         'MERGE INTO AR.AR_CASH_RECEIPTS_ALL ACRA '
      || 'USING XXWC.XXWC_AR_CASH_RCPT_TBL_TMP XACRT '
      || 'ON (ACRA.CASH_RECEIPT_ID = XACRT.CASH_RECEIPT_ID 
	          AND ACRA.ROWID = XACRT.CASH_ROWID 
			  AND ACRA.ORG_ID=162 
			  AND XACRT.NEW_URL IS NOT NULL) '
      || 'WHEN MATCHED THEN UPDATE SET ACRA.ATTRIBUTE1 = XACRT.NEW_URL';

     EXECUTE IMMEDIATE l_sql_stat;
     l_sec := 'open cursor cur_fnd_docs';
	 fnd_file.put_line (fnd_file.LOG , 'Start lOOP for cursor cur_fnd_docs '||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
	 OPEN CUR_FND_DOCS;
     LOOP
        EXIT WHEN CUR_FND_DOCS%NOTFOUND;
        FETCH CUR_FND_DOCS BULK COLLECT INTO OBJ_FND_DOCS LIMIT 10000;
		
		fnd_file.put_line (fnd_file.LOG , 'Update fnd_documents for record count '||OBJ_FND_DOCS.COUNT);
        FORALL rec_doc IN 1..OBJ_FND_DOCS.COUNT
            UPDATE fnd_documents fds
               SET url = OBJ_FND_DOCS (rec_doc).NEW_URL,
                   last_update_date = SYSDATE,
                   last_updated_by = ln_user_id
             WHERE  url = OBJ_FND_DOCS (rec_doc).OLD_URL
			   AND  OBJ_FND_DOCS (rec_doc).NEW_URL IS NOT NULL
               AND fds.datatype_id = 5
               AND EXISTS(SELECT 1
                          FROM fnd_attached_documents fad
                          WHERE fad.document_id = fds.document_id
                          AND fad.pk1_value = OBJ_FND_DOCS (rec_doc).cash_receipt_id
                          AND fad.entity_name = 'AR_CASH_RECEIPTS');    
     COMMIT;
     END LOOP;
     CLOSE CUR_FND_DOCS;
	 fnd_file.put_line (fnd_file.LOG , 'End lOOP for cursor cur_fnd_docs '||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
     
	 
	 --Call to generate text output
     l_sec := 'Call write_output';
     write_output( p_request_id    => l_request_id
                  ,p_retcode       => l_err_code
                  ,p_retmsg        => l_err_msg
                 );
     IF l_err_msg IS NOT NULL THEN
        fnd_file.put_line (fnd_file.LOG, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
	    fnd_file.put_line (fnd_file.output, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
        p_errbuf := 'Error';
        p_retcode := '2';
     END IF;

     fnd_file.put_line (fnd_file.LOG ,'**********************************************************************************');
     fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
     fnd_file.put_line (fnd_file.LOG , 'End of Procedure : xxwc_update_url'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
     fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');

  EXCEPTION
     WHEN OTHERS THEN
       p_errbuf := 'Error';
       p_retcode := '2';
	   l_err_code := SQLCODE;
       l_err_msg  := SUBSTR(SQLERRM,1,2000);
       fnd_file.put_line (fnd_file.LOG, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
       fnd_file.put_line (fnd_file.output, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
	   ROLLBACK;
       xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_AR_CASH_RECEIPT_URL_PKG.XXWC_UPDATE_URL'
           ,p_calling             => l_sec
           ,p_request_id          => fnd_global.conc_request_id
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error while updating url'
           ,p_distribution_list   => g_dflt_email
           ,p_module              => 'XXWC');
  END xxwc_update_url;
   
END XXWC_AR_CASH_RECEIPT_URL_PKG;
/
