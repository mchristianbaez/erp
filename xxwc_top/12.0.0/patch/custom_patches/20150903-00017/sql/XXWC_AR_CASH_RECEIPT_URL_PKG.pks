CREATE OR REPLACE PACKAGE XXWC_AR_CASH_RECEIPT_URL_PKG IS
   /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header XXWC_AR_CASH_RECEIPT_URL_PKG $
     Module Name: XXWC_AR_CASH_RECEIPT_URL_PKG.pkb

     PURPOSE:   Update Image url of AR Cash Receipt

     REVISIONS:
     Ver        Date         Author                     Description
     ---------  ----------   ---------------         -------------------------
     1.0        13-SEP-2016  Niraj K Ranjan          Initial Version - 20150903-00017   IT - data script for new URL on lockbox receipts
  **************************************************************************/

    
  /********************************************************************************
   ProcedureName : XXWC_UPDATE_URL
   Purpose       : CP to update image URL of AR Cash Receipt

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     13-SEP-2016   Niraj K Ranjan  Initial Version - 20150903-00017   IT - data script for new URL on lockbox receipts
   ********************************************************************************/ 
  PROCEDURE xxwc_update_url(p_errbuf         OUT VARCHAR2
                           ,p_retcode        OUT NUMBER
						   ,p_no_of_records  IN  NUMBER
						   );
   
END XXWC_AR_CASH_RECEIPT_URL_PKG;
/
