----------------------------------------------------------------------
-- SQL-Loader Control File
-- File Name: XXWC_AR_CASH_RECEIPTS_CTRL
-- ***************************************************************************************************
--
-- PURPOSE: To load image link data for Ar Cash Receipt's attached images
-- HISTORY
-- ===================================================================================================
-- ===================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- -------------------------------------------------------------
-- 1.0     12-Sep-2016   Niraj K Ranjan   Initial Version - TMS#20150903-00017   IT - data script for new URL on lockbox receipts
----------------------------------------------------------------------
OPTIONS 
   ( skip=1          
     --, rows=10000    
     --, errors=10     
     --, load=5        
     --, direct=false  
     --, parallel=false
   )
LOAD DATA
   INFILE        *           
   BADFILE       'XXWC_AR_CASH_RECEIPTS_BAD.log'      
   DISCARDFILE   'XXWC_AR_CASH_RECEIPTS_DISCARDED.log'
   APPEND                                   
   INTO TABLE     apps.xxwc_ar_cash_receipts_url_stg    
Fields Terminated    by ","
Optionally Enclosed  by '"'
Trailing Nullcols      
   ( APEX_OBJECT_ID   
    ,APEX_ENCRYPTED_ID
    ,APEX_URL         
    ,DCTM_OBJECT_ID   
    ,DCTM_URL         
    ,STATUS               CONSTANT "ENTERED"
    ,ERROR_MESSAGE    
    ,CREATION_DATE        SYSDATE --DATE "YYYY-MM-DD HH24:MI:SS" 
    ,CREATED_BY           "fnd_global.user_id"
    ,LAST_UPDATE_DATE     SYSDATE --DATE "YYYY-MM-DD HH24:MI:SS"
    ,LAST_UPDATED_BY      "fnd_global.user_id"
   )