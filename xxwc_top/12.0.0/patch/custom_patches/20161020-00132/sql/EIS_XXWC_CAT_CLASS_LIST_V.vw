---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_CAT_CLASS_LIST_V $
  REVISIONS   :
  VERSION 		DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0    	   21-Dec-2016        	Siva   		 TMS#20161020-00132   --Developed new report
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_CAT_CLASS_LIST_V;
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_CAT_CLASS_LIST_V (CATCLASS, DESCRIPTION, COGS_ACCOUNT, SALES_ACCOUNT, ON_THE_FLY_INCLUSION,
 TAXWARE_CODE, CATMGT_CATEGORY, CATMGT_CATEGORY_DESC, CATMGT_SUBCATEGORY, CATMGT_SUBCATEGORY_DESC, CATMGT_PROD_LINE,
 CATMGT_PROD_LINE_DESC, DISABLE_DATE, ENABLED_FLAG, CREATION_DATE, CAT_DIR, CAT_MGR)
AS
  SELECT mcv.concatenated_segments catclass,
    mct.description,
    mcv.attribute1 cogs_account,
    mcv.attribute2 sales_account,
    mcv.attribute3 on_the_fly_inclusion,
    mcv.attribute4 taxware_code,
    mcv.attribute5 catmgt_category,
    t_cat.description catmgt_category_desc,
    mcv.attribute6 catmgt_subcategory,
    t_sub_cat.description catmgt_subcategory_desc,
    mcv.attribute7 catmgt_prod_line,
    t_prod_line.description catmgt_prod_line_desc,
    mcv.disable_date,
    MCV.ENABLED_FLAG,
    TRUNC(mcv.creation_date) creation_date,
    (SELECT cat_dir.full_name
    FROM apps.per_all_people_f cat_dir
    WHERE cat_dir.person_id = b_sub_cat.attribute1
    AND TRUNC(sysdate) BETWEEN cat_dir.effective_start_date AND cat_dir.effective_end_date
    AND cat_dir.person_type_id = 6
    ) cat_dir,
    (SELECT cat_mgr.full_name
    FROM apps.per_all_people_f cat_mgr
    WHERE cat_mgr.person_id = b_sub_cat.attribute2
    AND TRUNC(sysdate) BETWEEN cat_mgr.effective_start_date AND cat_mgr.effective_end_date
    AND cat_mgr.person_type_id = 6
    ) cat_mgr
  FROM apps.mtl_categories_b_kfv mcv,
    apps.mtl_categories_tl mct,
    apps.fnd_flex_values b_cat,
    apps.fnd_flex_values_tl t_cat,
    apps.fnd_flex_value_sets v_cat,
    apps.fnd_flex_values b_sub_cat,
    apps.fnd_flex_values_tl t_sub_cat,
    apps.fnd_flex_value_sets v_sub_cat,
    apps.fnd_flex_values b_prod_line,
    apps.fnd_flex_values_tl t_prod_line,
    apps.fnd_flex_value_sets v_prod_line
  WHERE mcv.category_id = mct.category_id
  AND mct.language      = userenv('LANG')
  AND mcv.structure_id  = 101
    --mcv.structure_name = 'Item Categories'
  AND mcv.attribute5                   = b_cat.flex_value
  AND b_cat.flex_value_id              = t_cat.flex_value_id
  AND t_cat.language                   = userenv('LANG')
  AND b_cat.flex_value_set_id          = v_cat.flex_value_set_id
  AND v_cat.flex_value_set_name       IN ('XXWC_CATMGT_CATEGORIES')
  AND mcv.attribute6                   = b_sub_cat.flex_value
  AND b_sub_cat.flex_value_id          = t_sub_cat.flex_value_id
  AND t_sub_cat.language               = userenv('LANG')
  AND b_sub_cat.flex_value_set_id      = v_sub_cat.flex_value_set_id
  AND v_sub_cat.flex_value_set_name   IN ('XXWC_CATMGT_SUBCATEGORIES')
  AND mcv.attribute7                   = b_prod_line.flex_value
  AND b_prod_line.flex_value_id        = t_prod_line.flex_value_id
  AND t_prod_line.language             = userenv('LANG')
  AND b_prod_line.flex_value_set_id    = v_prod_line.flex_value_set_id
  AND v_prod_line.flex_value_set_name IN ('XXWC_CATMGT_PRODUCT_LINES')
/
