--Report Name            : CatClass Listing Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_CAT_CLASS_LIST_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_CAT_CLASS_LIST_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_CAT_CLASS_LIST_V',401,'','','','','SA059956','XXEIS','Eis Xxwc Cat Class List V','EXCCLV','','Y','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_CAT_CLASS_LIST_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_CAT_CLASS_LIST_V',401,FALSE);
--Inserting Object Columns for EIS_XXWC_CAT_CLASS_LIST_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CAT_CLASS_LIST_V','CAT_MGR',401,'Cat Mgr','CAT_MGR','','','','SA059956','VARCHAR2','','','Cat Mgr','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CAT_CLASS_LIST_V','CAT_DIR',401,'Cat Dir','CAT_DIR','','','','SA059956','VARCHAR2','','','Cat Dir','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CAT_CLASS_LIST_V','CREATION_DATE',401,'Creation Date','CREATION_DATE','','','','SA059956','DATE','','','Creation Date','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CAT_CLASS_LIST_V','ENABLED_FLAG',401,'Enabled Flag','ENABLED_FLAG','','','','SA059956','VARCHAR2','','','Enabled Flag','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CAT_CLASS_LIST_V','DISABLE_DATE',401,'Disable Date','DISABLE_DATE','','','','SA059956','DATE','','','Disable Date','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CAT_CLASS_LIST_V','CATMGT_PROD_LINE_DESC',401,'Catmgt Prod Line Desc','CATMGT_PROD_LINE_DESC','','','','SA059956','VARCHAR2','','','Catmgt Prod Line Desc','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CAT_CLASS_LIST_V','CATMGT_PROD_LINE',401,'Catmgt Prod Line','CATMGT_PROD_LINE','','','','SA059956','VARCHAR2','','','Catmgt Prod Line','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CAT_CLASS_LIST_V','CATMGT_SUBCATEGORY_DESC',401,'Catmgt Subcategory Desc','CATMGT_SUBCATEGORY_DESC','','','','SA059956','VARCHAR2','','','Catmgt Subcategory Desc','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CAT_CLASS_LIST_V','CATMGT_SUBCATEGORY',401,'Catmgt Subcategory','CATMGT_SUBCATEGORY','','','','SA059956','VARCHAR2','','','Catmgt Subcategory','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CAT_CLASS_LIST_V','CATMGT_CATEGORY_DESC',401,'Catmgt Category Desc','CATMGT_CATEGORY_DESC','','','','SA059956','VARCHAR2','','','Catmgt Category Desc','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CAT_CLASS_LIST_V','CATMGT_CATEGORY',401,'Catmgt Category','CATMGT_CATEGORY','','','','SA059956','VARCHAR2','','','Catmgt Category','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CAT_CLASS_LIST_V','TAXWARE_CODE',401,'Taxware Code','TAXWARE_CODE','','','','SA059956','VARCHAR2','','','Taxware Code','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CAT_CLASS_LIST_V','ON_THE_FLY_INCLUSION',401,'On The Fly Inclusion','ON_THE_FLY_INCLUSION','','','','SA059956','VARCHAR2','','','On The Fly Inclusion','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CAT_CLASS_LIST_V','SALES_ACCOUNT',401,'Sales Account','SALES_ACCOUNT','','','','SA059956','VARCHAR2','','','Sales Account','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CAT_CLASS_LIST_V','COGS_ACCOUNT',401,'Cogs Account','COGS_ACCOUNT','','','','SA059956','VARCHAR2','','','Cogs Account','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CAT_CLASS_LIST_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','SA059956','VARCHAR2','','','Description','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CAT_CLASS_LIST_V','CATCLASS',401,'Catclass','CATCLASS','','','','SA059956','VARCHAR2','','','Catclass','','','Y','US');
--Inserting Object Components for EIS_XXWC_CAT_CLASS_LIST_V
--Inserting Object Component Joins for EIS_XXWC_CAT_CLASS_LIST_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report LOV Data for CatClass Listing Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - CatClass Listing Report - WC
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT mcv.concatenated_segments catclass
FROM mtl_categories_b_kfv mcv
ORDER BY 1','','XXWC INV CatClass LOV','This LOV shows the category class list','SA059956',NULL,'N','','','N','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT mct.description
FROM mtl_categories_tl mct
WHERE mct.language = userenv(''LANG'')
ORDER BY 1','','XXWC INV Category DESCR LOV','','SA059956',NULL,'N','','','N','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT mcv.attribute1 cogs_account
FROM apps.mtl_categories_b_kfv mcv
ORDER BY 1','','XXWC INV CAT COGS ACCOUNT LOV','','SA059956',NULL,'N','','','N','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT mcv.attribute2 sales_account
FROM apps.mtl_categories_b_kfv mcv
ORDER BY 1','','XXWC INV CAT SALES ACCOUNT LOV','','SA059956',NULL,'N','','','N','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT cat_dir.full_name cat_director
FROM apps.mtl_categories_b_kfv mcv,
  apps.fnd_flex_values b_sub_cat,
  apps.per_all_people_f cat_dir
WHERE mcv.attribute6     = b_sub_cat.flex_value
AND b_sub_cat.attribute1 = cat_dir.person_id
AND TRUNC(sysdate) BETWEEN cat_dir.effective_start_date AND cat_dir.effective_end_date
AND cat_dir.person_type_id = 6
ORDER BY 1','','XXWC INV CAT Director LOV','','SA059956',NULL,'N','','','N','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT cat_mgr.full_name cat_manager
FROM apps.mtl_categories_b_kfv mcv,
  apps.fnd_flex_values b_sub_cat,
  apps.per_all_people_f cat_mgr
WHERE mcv.attribute6     = b_sub_cat.flex_value
AND b_sub_cat.attribute2 = cat_mgr.person_id
AND TRUNC(sysdate) BETWEEN cat_mgr.effective_start_date AND cat_mgr.effective_end_date
AND cat_mgr.person_type_id = 6
ORDER BY 1','','XXWC INV CAT Manager LOV','','SA059956',NULL,'N','','','N','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report Data for CatClass Listing Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Deleting Report data - CatClass Listing Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'CatClass Listing Report - WC' );
--Inserting Report - CatClass Listing Report - WC
xxeis.eis_rsc_ins.r( 401,'CatClass Listing Report - WC','','Category Class and associated category managers and category directors.','','','','SA059956','EIS_XXWC_CAT_CLASS_LIST_V','Y','','','SA059956','','N','White Cap Reports','','EXCEL,','N','','','','','','','APPS','US','','','','');
--Inserting Report Columns - CatClass Listing Report - WC
xxeis.eis_rsc_ins.rc( 'CatClass Listing Report - WC',401,'CATCLASS','Cat Class','Catclass','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CAT_CLASS_LIST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CatClass Listing Report - WC',401,'CATMGT_CATEGORY','CATMGT Category','Catmgt Category','','','default','','7','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CAT_CLASS_LIST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CatClass Listing Report - WC',401,'CATMGT_CATEGORY_DESC','CATMGT Category Desc','Catmgt Category Desc','','','default','','8','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CAT_CLASS_LIST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CatClass Listing Report - WC',401,'CATMGT_PROD_LINE','CATMGT Prod Line','Catmgt Prod Line','','','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CAT_CLASS_LIST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CatClass Listing Report - WC',401,'CATMGT_PROD_LINE_DESC','CATMGT Prod Line Desc','Catmgt Prod Line Desc','','','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CAT_CLASS_LIST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CatClass Listing Report - WC',401,'CATMGT_SUBCATEGORY','CATMGT SubCategory','Catmgt Subcategory','','','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CAT_CLASS_LIST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CatClass Listing Report - WC',401,'CATMGT_SUBCATEGORY_DESC','CATMGT Sub Category Desc','Catmgt Subcategory Desc','','','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CAT_CLASS_LIST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CatClass Listing Report - WC',401,'CAT_DIR','Cat Director','Cat Dir','','','default','','16','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CAT_CLASS_LIST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CatClass Listing Report - WC',401,'CAT_MGR','Cat Manager','Cat Mgr','','','default','','17','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CAT_CLASS_LIST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CatClass Listing Report - WC',401,'COGS_ACCOUNT','COGS Account','Cogs Account','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CAT_CLASS_LIST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CatClass Listing Report - WC',401,'CREATION_DATE','Creation Date','Creation Date','','','default','','15','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CAT_CLASS_LIST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CatClass Listing Report - WC',401,'DESCRIPTION','Description','Description','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CAT_CLASS_LIST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CatClass Listing Report - WC',401,'DISABLE_DATE','Disable Date','Disable Date','','','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CAT_CLASS_LIST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CatClass Listing Report - WC',401,'ENABLED_FLAG','Enabled Flag','Enabled Flag','','','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CAT_CLASS_LIST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CatClass Listing Report - WC',401,'ON_THE_FLY_INCLUSION','On The Fly Inclusion','On The Fly Inclusion','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CAT_CLASS_LIST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CatClass Listing Report - WC',401,'SALES_ACCOUNT','Sales Account','Sales Account','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CAT_CLASS_LIST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CatClass Listing Report - WC',401,'TAXWARE_CODE','Taxware Code','Taxware Code','','','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_CAT_CLASS_LIST_V','','','','US','');
--Inserting Report Parameters - CatClass Listing Report - WC
xxeis.eis_rsc_ins.rp( 'CatClass Listing Report - WC',401,'Cat Class','Cat Class','CATCLASS','IN','XXWC INV CatClass LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_CAT_CLASS_LIST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'CatClass Listing Report - WC',401,'Cat Director','Cat Director','CAT_DIR','IN','XXWC INV CAT Director LOV','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_CAT_CLASS_LIST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'CatClass Listing Report - WC',401,'Cat Manager','Cat Manager','CAT_MGR','IN','XXWC INV CAT Manager LOV','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_CAT_CLASS_LIST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'CatClass Listing Report - WC',401,'COGS Account','COGS Account','COGS_ACCOUNT','IN','XXWC INV CAT COGS ACCOUNT LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_CAT_CLASS_LIST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'CatClass Listing Report - WC',401,'Creation Date From','Creation Date From','CREATION_DATE','>=','','','DATE','N','Y','5','N','Y','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_CAT_CLASS_LIST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'CatClass Listing Report - WC',401,'Description','Description','DESCRIPTION','IN','XXWC INV Category DESCR LOV','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_CAT_CLASS_LIST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'CatClass Listing Report - WC',401,'Sales Account','Sales Account','SALES_ACCOUNT','IN','XXWC INV CAT SALES ACCOUNT LOV','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_CAT_CLASS_LIST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'CatClass Listing Report - WC',401,'Creation Date To','Creation Date To','CREATION_DATE','<=','','','DATE','N','Y','6','N','Y','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_CAT_CLASS_LIST_V','','','US','');
--Inserting Dependent Parameters - CatClass Listing Report - WC
--Inserting Report Conditions - CatClass Listing Report - WC
xxeis.eis_rsc_ins.rcnh( 'CatClass Listing Report - WC',401,'EXCCLV.CATCLASS IN Cat Class','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CATCLASS','','Cat Class','','','','','EIS_XXWC_CAT_CLASS_LIST_V','','','','','','IN','Y','Y','','','','','1',401,'CatClass Listing Report - WC','EXCCLV.CATCLASS IN Cat Class');
xxeis.eis_rsc_ins.rcnh( 'CatClass Listing Report - WC',401,'EXCCLV.DESCRIPTION IN Description','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DESCRIPTION','','Description','','','','','EIS_XXWC_CAT_CLASS_LIST_V','','','','','','IN','Y','Y','','','','','1',401,'CatClass Listing Report - WC','EXCCLV.DESCRIPTION IN Description');
xxeis.eis_rsc_ins.rcnh( 'CatClass Listing Report - WC',401,'EXCCLV.COGS_ACCOUNT IN COGS Account','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','COGS_ACCOUNT','','COGS Account','','','','','EIS_XXWC_CAT_CLASS_LIST_V','','','','','','IN','Y','Y','','','','','1',401,'CatClass Listing Report - WC','EXCCLV.COGS_ACCOUNT IN COGS Account');
xxeis.eis_rsc_ins.rcnh( 'CatClass Listing Report - WC',401,'EXCCLV.SALES_ACCOUNT IN Sales Account','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SALES_ACCOUNT','','Sales Account','','','','','EIS_XXWC_CAT_CLASS_LIST_V','','','','','','IN','Y','Y','','','','','1',401,'CatClass Listing Report - WC','EXCCLV.SALES_ACCOUNT IN Sales Account');
xxeis.eis_rsc_ins.rcnh( 'CatClass Listing Report - WC',401,'EXCCLV.CREATION_DATE >= Creation Date From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Creation Date From','','','','','EIS_XXWC_CAT_CLASS_LIST_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',401,'CatClass Listing Report - WC','EXCCLV.CREATION_DATE >= Creation Date From');
xxeis.eis_rsc_ins.rcnh( 'CatClass Listing Report - WC',401,'EXCCLV.CREATION_DATE <= Creation Date To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Creation Date To','','','','','EIS_XXWC_CAT_CLASS_LIST_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',401,'CatClass Listing Report - WC','EXCCLV.CREATION_DATE <= Creation Date To');
xxeis.eis_rsc_ins.rcnh( 'CatClass Listing Report - WC',401,'EXCCLV.CAT_DIR IN Cat Director','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CAT_DIR','','Cat Director','','','','','EIS_XXWC_CAT_CLASS_LIST_V','','','','','','IN','Y','Y','','','','','1',401,'CatClass Listing Report - WC','EXCCLV.CAT_DIR IN Cat Director');
xxeis.eis_rsc_ins.rcnh( 'CatClass Listing Report - WC',401,'EXCCLV.CAT_MGR IN Cat Manager','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CAT_MGR','','Cat Manager','','','','','EIS_XXWC_CAT_CLASS_LIST_V','','','','','','IN','Y','Y','','','','','1',401,'CatClass Listing Report - WC','EXCCLV.CAT_MGR IN Cat Manager');
--Inserting Report Sorts - CatClass Listing Report - WC
xxeis.eis_rsc_ins.rs( 'CatClass Listing Report - WC',401,'CATCLASS','ASC','SA059956','1','');
--Inserting Report Triggers - CatClass Listing Report - WC
--inserting report templates - CatClass Listing Report - WC
--Inserting Report Portals - CatClass Listing Report - WC
--inserting report dashboards - CatClass Listing Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'CatClass Listing Report - WC','401','EIS_XXWC_CAT_CLASS_LIST_V','EIS_XXWC_CAT_CLASS_LIST_V','N','');
--inserting report security - CatClass Listing Report - WC
xxeis.eis_rsc_ins.rsec( 'CatClass Listing Report - WC','401','','XXWC_DATA_MNGT_SC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'CatClass Listing Report - WC','401','','XXWC_INVENTORY_SUPER_USER',401,'SA059956','','','');
--Inserting Report Pivots - CatClass Listing Report - WC
--Inserting Report   Version details- CatClass Listing Report - WC
xxeis.eis_rsc_ins.rv( 'CatClass Listing Report - WC','','CatClass Listing Report - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
