CREATE OR REPLACE PACKAGE APPS.XXWC_ISR_ROUTINES_PKG
/*************************************************************************
  $Header XXWC_ISR_ROUTINES_PKG.pks $
  Module Name: XXWC_ISR_ROUTINES_PKG

  PURPOSE: ISR Logic as routines for real time use - not for Mass data processing
     only for limited no of records

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        24-Aug-2015  Manjula Chellappan    Initial Version TMS # 20150521-00013 - Data Management Audit Report - make some fields live
**************************************************************************/
AS
   FUNCTION get_vendor_number (p_inventory_item_id   IN NUMBER,
                               p_organization_id     IN NUMBER,
                               p_source_type         IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION get_vendor_name (p_inventory_item_id   IN NUMBER,
                             p_organization_id     IN NUMBER,
                             p_source_type         IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION get_bpa_number (p_inventory_item_id   IN NUMBER,
                            p_organization_id     IN NUMBER)
      RETURN VARCHAR2;

   FUNCTION get_bpa_cost (p_inventory_item_id   IN NUMBER,
                          p_organization_id     IN NUMBER)
      RETURN NUMBER;
END XXWC_ISR_ROUTINES_PKG;
/