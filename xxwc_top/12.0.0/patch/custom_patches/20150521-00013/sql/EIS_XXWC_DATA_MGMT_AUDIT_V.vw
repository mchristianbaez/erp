/*************************************************************************
$Header EIS_XXWC_DATA_MGMT_AUDIT_V.vw $
Module Name: EIS_XXWC_DATA_MGMT_AUDIT_V.vw
PURPOSE: Data Management Audit Report
REVISIONS:
Ver        Date         Author                Description
---------  -----------  ------------------    ----------------
1.0        20-May-2015  Mahender Reddy        Initial Version TMS# 20150310-00182
1.1        04-Aug-2015  Mahender Reddy        TMS# 20150702-00174
1.2        26-Aug-2015  Manjula Chellappan    TMS# 20150521-00013 Data Management Audit Report - make some fields live   	
**************************************************************************/
CREATE OR REPLACE FORCE VIEW "XXEIS"."EIS_XXWC_DATA_MGMT_AUDIT_V" ("ITEM", "SHORT_DESCRIPTION", "LONG_DESCRIPTION", "UOM", "WEIGHT", "WEIGHT_UOM_CODE", "SHELF_LIFE_DAYS", "AVP_CODE", "COUNTRY_OF_ORIGIN", "MASTER_VENDOR_NUMBER", "SOURCE", "SOURCING_VENDOR_NUMBER", "SOURCING_VENDOR_NAME", "ORG", "ITEM_STATUS", "USER_ITEM_TYPE", "ENGINEERING_ITEM_FLAG", "ORG_DESCRIPTION", "DISTRICT", "REGION", "MFG_PART_NUMBER", "FIXED_LOT_MULTIPLIER", "LIST_PRICE_PER_UNIT", "AVERAGE_COST", "BPA_COST", "BPA_NUMBER", "INVENTORY_CATEGORY", "INVENTORY_CATEGORY_DESCRIPTION", "WEBSITE_ITEM", "WC_WEB_HIERARCHY", "BRAND", "PRODUCT_LINE", "WEB_MFG_PART_NUMBER", "HAZMAT_FLAG", "BULKY_ITEM_FLAG", "CYBERSOURCE_TAX_CODE", "FULLSIZE_IMAGE_1", "THUMBNAIL_IMAGE_1", "WEB_SEQUENCE", "WEB_SHORT_DESCRIPTION", "WEB_KEYWORDS", "WEB_LONG_DESCRIPTION", "ITEM_CREATION_DATE", "COGS_ACCOUNT", "SALES_ACCOUNT", "EXPENSE_ACCOUNT", "INVENTORY_ITEM_ID", "ITEM_LEVEL", "FULFILLMENT", "RETAIL_SELLING_UOM", "CONFLICT_MINERALS", "XPAR", "CREATED_BY") AS 
  SELECT msi.segment1 item,
          msi.description short_description,
          msit.long_description long_description,
          mt.unit_of_measure uom,
          unit_weight weight,
          uomw.unit_of_measure_tl weight_uom_code,
          msi.shelf_life_days,
          msi.attribute22 avp_code,
          coo.meaning country_of_origin -- Added by Mahender for TMS#20150310-00182  on 05/06/2015
                                       /*(SELECT emsy.c_ext_attr2 || '-' || territory_short_name
                                            FROM fnd_territories_tl
                                           WHERE territory_code = emsy.c_ext_attr2)
                                           country_of_origin*/
                                       -- Commented by Mahender for TMS#20150310-00182  on 05/06/2015
          ,                            --  EMSY.C_EXT_ATTR2 COUNTRY_OF_ORIGIN,
          emsy.c_ext_attr1 master_vendor_number,
          expv.source,
--        expv.vendor_num sourcing_vendor_number, -- Commented for Ver 1.2
		  DECODE(expv.inventory_item_id, NULL , apps.xxwc_isr_routines_pkg.get_vendor_number (
             msi.inventory_item_id,
             msi.organization_id,
             msi.source_type) , expv.vendor_num) sourcing_vendor_number, -- Added for Ver 1.2 
--        expv.vendor_name sourcing_vendor_name -- Commented for Ver 1.2 --,expv.org                      -- Commented by Mahesh for TMS#20150310-00182  on 04/17/2015
		  DECODE(expv.inventory_item_id, NULL , apps.xxwc_isr_routines_pkg.get_vendor_name (
             msi.inventory_item_id,
             msi.organization_id,
             msi.source_type), expv.vendor_name ) sourcing_vendor_name -- Added for Ver 1.2 
                                               ,
          ood.organization_code org -- Added by Mahesh for TMS#20150310-00182  on 04/17/2015
                                   ,
          msi.inventory_item_status_code item_status,
          (SELECT meaning
             FROM fnd_lookup_values
            WHERE lookup_type = 'ITEM_TYPE' AND lookup_code = msi.item_type)
             user_item_type,
          DECODE (msi.eng_item_flag,  'N', 'No',  'Y', 'Yes')
             engineering_item_flag,
          ood.organization_name org_description,
          mp.attribute8 district,
          mp.attribute9 region,
          SUBSTR (
             apps.xxwc_mv_routines_pkg.get_item_cross_reference (
                msi.inventory_item_id,
                'VENDOR',
                1),
             1,
             255)
             mfg_part_number,             -- mmp.mfg_part_num Mfg_Part_Number,
          msi.fixed_lot_multiplier,
          msi.list_price_per_unit,
          NVL (
             apps.cst_cost_api.get_item_cost (1,
                                              msi.inventory_item_id,
                                              msi.organization_id),
             0)
             average_cost,
--        expv.bpa_cost, -- Commented for Ver 1.2
          DECODE(expv.inventory_item_id, NULL , apps.xxwc_isr_routines_pkg.get_bpa_cost (msi.inventory_item_id,
                                                   msi.organization_id), expv.bpa_cost )
             bpa_cost, -- Added for Ver 1.2
--        expv.bpa bpa_number, -- Commented for Ver 1.2 -- xxeis.eis_rs_xxwc_com_util_pkg.get_best_buy ( msi.inventory_item_id, msi.organization_id, pov.vendor_id) BPA_NUMBER,
          DECODE(expv.inventory_item_id, NULL ,apps.xxwc_isr_routines_pkg.get_bpa_number (msi.inventory_item_id,
                                                     msi.organization_id),expv.bpa )  
			 bpa_number, -- Added for Ver 1.2
          xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class (
             msi.inventory_item_id,
             msi.organization_id)
             inventory_category,
          xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class_desc (
             msi.inventory_item_id,
             msi.organization_id)
             inventory_category_description,
          (SELECT DECODE (mcv.segment2,
                          NULL, mcv.segment1,
                          mcv.segment1 || '.' || mcv.segment2)
             FROM mtl_item_categories mic,
                  mtl_categories_vl mcv,
                  mtl_category_sets mcs
            WHERE     mic.inventory_item_id = msi.inventory_item_id
                  AND mic.organization_id = msi.organization_id
                  AND mic.category_id = mcv.category_id
                  AND mic.category_set_id = mcs.category_set_id
                  AND mcs.structure_id = mcv.structure_id
                  AND mcs.category_set_name = 'Website Item')
             website_item,
          (SELECT DECODE (mcv.segment2,
                          NULL, mcv.segment1,
                          mcv.segment1 || '.' || mcv.segment2)
             FROM mtl_item_categories mic,
                  mtl_categories_vl mcv,
                  mtl_category_sets mcs
            WHERE     mic.inventory_item_id = msi.inventory_item_id
                  AND mic.organization_id = msi.organization_id
                  AND mic.category_id = mcv.category_id
                  AND mic.category_set_id = mcs.category_set_id
                  AND mcs.structure_id = mcv.structure_id
                  AND mcs.category_set_name = 'WC Web Hierarchy')
             wc_web_hierarchy,
          emsy2.c_ext_attr1 brand,
          emsy2.c_ext_attr7 product_line,
          emsy2.c_ext_attr6 web_mfg_part_number,
          emsy2.c_ext_attr5 hazmat_flag,
          emsy2.c_ext_attr3 bulky_item_flag,
          emsy2.c_ext_attr11 cybersource_tax_code,
          emsy2.c_ext_attr15 fullsize_image_1,
          emsy2.c_ext_attr20 thumbnail_image_1,
          emsy2.n_ext_attr1 web_sequence,
          emsy2.c_ext_attr14 web_short_description,
          emstl.tl_ext_attr3 web_keywords,
          emstl.tl_ext_attr2 web_long_description,
          TRUNC (msi.creation_date) item_creation_date,
          xxeis.eis_rs_xxwc_com_util_pkg.get_item_acc (
             msi.cost_of_sales_account)
             cogs_account,
          xxeis.eis_rs_xxwc_com_util_pkg.get_item_acc (msi.sales_account)
             sales_account,
          xxeis.eis_rs_xxwc_com_util_pkg.get_item_acc (msi.expense_account)
             expense_account,
          msi.inventory_item_id,
          emsy.c_ext_attr6 item_level -- Added by Mahender for TMS#20150310-00182  on 05/06/2015
                                     ,
          emsy.c_ext_attr7 fulfillment -- Added by Mahender for TMS#20150310-00182  on 05/06/2015
                                      ,
          emsy.c_ext_attr10 retail_selling_uom -- Added by Mahender for TMS#20150310-00182  on 05/06/2015
                                              ,
          emsy.c_ext_attr11 conflict_minerals -- Added by Mahender for TMS#20150310-00182  on 05/06/2015
                                             ,
          (SELECT d.segment1
             FROM apps.mtl_item_catalog_groups_b d
--          WHERE msi.item_catalog_group_id = d.item_catalog_group_id(+)) -- Commented for Ver 1.3 
			WHERE msi.item_catalog_group_id = d.item_catalog_group_id) -- Added for Ver 1.3
             xpar   -- Added by Mahender for TMS#20150310-00182  on 05/06/2015
                 ,
          fu.description created_by                           -- Added by Mahender for TMS#20150702-00174   on 07/28/2015
     ---------
     FROM mtl_system_items_b msi,
          mtl_system_items_tl msit,
          mtl_units_of_measure_vl uomw,
          org_organization_definitions ood,
          mtl_parameters mp,
          fnd_user fu                                         -- Added by Mahender for TMS#20150702-00174   on 07/28/2015
                     ,
          mtl_units_of_measure_vl mt,
          apps.ego_mtl_sy_items_ext_b emsy,
          apps.ego_mtl_sy_items_ext_b emsy2,
          apps.ego_mtl_sy_items_ext_tl emstl,
          xxeis.eis_xxwc_po_isr_tab expv,
          (SELECT lookup_code, meaning
             FROM apps.FND_LOOKUP_VALUES
            WHERE lookup_type = 'XXWC_TERRITORIES') coo -- Added by Mahender for TMS#20150310-00182  on 05/06/2015
    WHERE     msi.organization_id = ood.organization_id
          AND mt.uom_code = msi.primary_uom_code
          AND msi.created_by = fu.user_id                     -- Added by Mahender for TMS#20150702-00174   on 07/28/2015
          AND msi.inventory_item_id = msit.inventory_item_id
          AND msi.organization_id = msit.organization_id
          AND msit.language = USERENV ('LANG')
          AND uomw.uom_code(+) = msi.weight_uom_code
          AND emsy.inventory_item_id(+) = msi.inventory_item_id
          AND emsy.organization_id(+) = msi.organization_id
          AND emsy.attr_group_id(+) = 861
          AND emsy2.inventory_item_id(+) = msi.inventory_item_id
          AND emsy2.organization_id(+) = msi.organization_id
          AND emsy2.attr_group_id(+) = 479
          AND emstl.inventory_item_id(+) = msi.inventory_item_id
          AND emstl.organization_id(+) = msi.organization_id
          AND emstl.attr_group_id(+) = 479		  
          AND msi.inventory_item_id = expv.inventory_item_id(+) -- Added Outer Join by Mahesh for TMS#20150310-00182  on 04/17/2015
          AND msi.organization_id = expv.organization_id(+) -- Added Outer Join by Mahesh for TMS#20150310-00182  on 04/17/2015
          AND ood.organization_id = mp.organization_id
          AND emsy.c_ext_attr2 = coo.lookup_code(+) -- Added by Mahender for TMS#20150310-00182  on 05/06/2015
          AND (   TRUNC (msi.creation_date) =
                     DECODE (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from,
                             NULL, TRUNC (msi.creation_date))
               OR (    TRUNC (msi.creation_date) >=
                          (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)
                   AND TRUNC (msi.creation_date) <= TRUNC (SYSDATE)));
/