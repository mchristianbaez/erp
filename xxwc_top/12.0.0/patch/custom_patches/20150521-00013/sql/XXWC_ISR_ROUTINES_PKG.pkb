CREATE OR REPLACE PACKAGE BODY APPS.XXWC_ISR_ROUTINES_PKG
/*************************************************************************
  $Header XXWC_ISR_ROUTINES_PKG.pkb $
  Module Name: XXWC_ISR_ROUTINES_PKG

  PURPOSE: ISR Logic as routines for real time use - not for Mass data processing
     only for limited no of records

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        24-Aug-2015  Manjula Chellappan    Initial Version TMS # 20150521-00013 - Data Management Audit Report - make some fields live
**************************************************************************/
IS
   g_dflt_email   fnd_user.email_address%TYPE
                     := 'HDSOracleDevelopers@hdsupply.com';


   FUNCTION get_vendor_number (p_inventory_item_id   IN NUMBER,
                               p_organization_id     IN NUMBER,
                               p_source_type         IN VARCHAR2)
      RETURN VARCHAR2
   /*************************************************************************
     $Header XXWC_ISR_ROUTINES_PKG.pkb $
     Module Name: XXWC_ISR_ROUTINES_PKG.get_vendor_number

     PURPOSE: ISR Logic as routines for real time use - not for Mass data processing
        only for limited no of records

     REVISIONS:
     Ver        Date         Author                Description
     ---------  -----------  ------------------    ----------------
     1.0        24-Aug-2015  Manjula Chellappan    Initial Version TMS # 20150521-00013 - Data Management Audit Report - make some fields live
   **************************************************************************/
   IS
      l_vendor_number   VARCHAR2 (30);
      l_sec             VARCHAR2 (100);
   BEGIN
      FOR r_source
         IN (SELECT msa.organization_id,
                    msa.inventory_item_id,
                    msa.sourcing_rule_id,
                    msr.sourcing_rule_name sourcing_rule,
                    msro.sr_receipt_id,
                    msso.vendor_id,
                    aps.vendor_name vendor_name,
                    aps.attribute3 tier,
                    aps.attribute4 cat_sba_owner,
                    aps.segment1 vendor_num,
                    msso.vendor_site_id,
                    ass.vendor_site_code vendor_site
               FROM mrp_sr_assignments msa,
                    mrp_sr_receipt_org msro,
                    mrp_sr_source_org msso,
                    mrp_sourcing_rules msr,
                    ap_suppliers aps,
                    ap_supplier_sites_all ass
              WHERE     msa.sourcing_rule_id = msro.sourcing_rule_id(+)
                    AND msa.sourcing_rule_id = msr.sourcing_rule_id(+)
                    AND msro.sr_receipt_id = msso.sr_receipt_id(+)
                    AND msso.vendor_id = aps.vendor_id(+)
                    AND msso.vendor_site_id = ass.vendor_site_id(+)
                    AND msa.assignment_type = 6
                    AND msa.assignment_set_id = 1
                    AND NVL (msso.source_type, 3) = 3
                    AND msa.inventory_item_id = p_inventory_item_id
                    AND msa.organization_id = p_organization_id)
      LOOP
         l_vendor_number := r_source.vendor_num;

         IF    r_source.sourcing_rule = '88888'
            OR r_source.sourcing_rule = '99999'
         THEN
            l_sec := 'Step 1 : Get Master Vendor';

            BEGIN
               SELECT attribute_char_value mst_vendor_num
                 INTO l_vendor_number
                 FROM ego_all_attr_base_v a
                WHERE     attribute_name = 'XXWC_VENDOR_NUMBER_ATTR'
                      AND attribute_group_name = 'XXWC_INTERNAL_ATTR_AG'
                      AND inventory_item_id = p_inventory_item_id
                      AND organization_id = 222;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_vendor_number := NULL;
            END;
         ELSIF r_source.sourcing_rule IS NULL
         THEN
            IF p_source_type = 'I'
            THEN
               l_vendor_number := '88888';
            ELSIF p_source_type = 'S'
            THEN
               l_sec := 'Step 2 : Get Most common Vendor';

               BEGIN
                  SELECT vendor_num
                    INTO l_vendor_number
                    FROM (  SELECT msa.inventory_item_id,
                                   msa.sourcing_rule_id,
                                   aps.vendor_name,
                                   aps.segment1 vendor_num,
                                   COUNT (*) source_count,
                                   ROW_NUMBER ()
                                   OVER (PARTITION BY msa.inventory_item_id
                                         ORDER BY COUNT (*) DESC)
                                      rn
                              FROM mrp_sr_assignments msa,
                                   mrp_sr_receipt_org msro,
                                   mrp_sr_source_org msso,
                                   mrp_sourcing_rules msr,
                                   ap_suppliers aps,
                                   ap_supplier_sites_all ass
                             WHERE     msa.sourcing_rule_id =
                                          msro.sourcing_rule_id(+)
                                   AND msa.sourcing_rule_id =
                                          msr.sourcing_rule_id(+)
                                   AND msro.sr_receipt_id =
                                          msso.sr_receipt_id(+)
                                   AND msso.vendor_id = aps.vendor_id(+)
                                   AND msso.vendor_site_id =
                                          ass.vendor_site_id(+)
                                   AND msa.assignment_type = 6
                                   AND msa.assignment_set_id = 1
                                   AND NVL (msso.source_type, 3) = 3
                                   AND msa.inventory_item_id =
                                          p_inventory_item_id
                          GROUP BY msa.inventory_item_id,
                                   msa.sourcing_rule_id,
                                   aps.segment1,
                                   aps.vendor_name)
                   WHERE rn = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_vendor_number := NULL;
               END;

               IF l_vendor_number = '88888' OR l_vendor_number = '99999'
               THEN
                  l_sec := 'Step 3 : Get Master Vendor';

                  BEGIN
                     SELECT attribute_char_value mst_vendor_num
                       INTO l_vendor_number
                       FROM ego_all_attr_base_v a
                      WHERE     attribute_name = 'XXWC_VENDOR_NUMBER_ATTR'
                            AND attribute_group_name =
                                   'XXWC_INTERNAL_ATTR_AG'
                            AND inventory_item_id = p_inventory_item_id
                            AND organization_id = 222;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_vendor_number := NULL;
                  END;
               END IF;
            END IF;
         END IF;
      END LOOP;

      RETURN (NVL (l_vendor_number, '88888'));
   EXCEPTION
      WHEN OTHERS
      THEN       
         RETURN (NVL (l_vendor_number, '88888'));
   END get_vendor_number;

   FUNCTION get_vendor_name (p_inventory_item_id   IN NUMBER,
                             p_organization_id     IN NUMBER,
                             p_source_type         IN VARCHAR2)
      RETURN VARCHAR2
   /*************************************************************************
     $Header XXWC_ISR_ROUTINES_PKG.pkb $
     Module Name: XXWC_ISR_ROUTINES_PKG.get_vendor_name

     PURPOSE: ISR Logic as routines for real time use - not for Mass data processing
        only for limited no of records

     REVISIONS:
     Ver        Date         Author                Description
     ---------  -----------  ------------------    ----------------
     1.0        24-Aug-2015  Manjula Chellappan    Initial Version TMS # 20150521-00013 - Data Management Audit Report - make some fields live
   **************************************************************************/
   IS
      l_vendor_number   VARCHAR2 (30);
      l_vendor_name     VARCHAR2 (240);
      l_sec             VARCHAR2 (100);
   BEGIN
      FOR r_source
         IN (SELECT msa.organization_id,
                    msa.inventory_item_id,
                    msa.sourcing_rule_id,
                    msr.sourcing_rule_name sourcing_rule,
                    msro.sr_receipt_id,
                    msso.vendor_id,
                    aps.vendor_name vendor_name,
                    aps.attribute3 tier,
                    aps.attribute4 cat_sba_owner,
                    aps.segment1 vendor_num,
                    msso.vendor_site_id,
                    ass.vendor_site_code vendor_site
               FROM mrp_sr_assignments msa,
                    mrp_sr_receipt_org msro,
                    mrp_sr_source_org msso,
                    mrp_sourcing_rules msr,
                    ap_suppliers aps,
                    ap_supplier_sites_all ass
              WHERE     msa.sourcing_rule_id = msro.sourcing_rule_id(+)
                    AND msa.sourcing_rule_id = msr.sourcing_rule_id(+)
                    AND msro.sr_receipt_id = msso.sr_receipt_id(+)
                    AND msso.vendor_id = aps.vendor_id(+)
                    AND msso.vendor_site_id = ass.vendor_site_id(+)
                    AND msa.assignment_type = 6
                    AND msa.assignment_set_id = 1
                    AND NVL (msso.source_type, 3) = 3
                    AND msa.inventory_item_id = p_inventory_item_id
                    AND msa.organization_id = p_organization_id)
      LOOP
         l_vendor_name := r_source.vendor_name;

         IF    r_source.sourcing_rule = '88888'
            OR r_source.sourcing_rule = '99999'
         THEN
            l_sec := 'Step 1 : Get Master Vendor';

            BEGIN
               SELECT (SELECT vendor_name
                         FROM ap_suppliers
                        WHERE segment1 = a.attribute_char_value)
                         mst_vendor_name
                 INTO l_vendor_name
                 FROM ego_all_attr_base_v a
                WHERE     attribute_name = 'XXWC_VENDOR_NUMBER_ATTR'
                      AND attribute_group_name = 'XXWC_INTERNAL_ATTR_AG'
                      AND inventory_item_id = p_inventory_item_id
                      AND organization_id = 222;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_vendor_name := NULL;
            END;
         ELSIF r_source.sourcing_rule IS NULL
         THEN
            IF p_source_type = 'I'
            THEN
               l_sec := 'Step 2 : Get Vendor Name for 88888';


               BEGIN
                  SELECT vendor_name
                    INTO l_vendor_name
                    FROM ap_suppliers
                   WHERE segment1 = '88888';
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_vendor_name := NULL;
               END;
            ELSIF p_source_type = 'S'
            THEN
               l_sec := 'Step 3 : Get Most common Vendor';

               BEGIN
                  SELECT vendor_num, vendor_name
                    INTO l_vendor_number, l_vendor_name
                    FROM (  SELECT msa.inventory_item_id,
                                   msa.sourcing_rule_id,
                                   aps.vendor_name,
                                   aps.segment1 vendor_num,
                                   COUNT (*) source_count,
                                   ROW_NUMBER ()
                                   OVER (PARTITION BY msa.inventory_item_id
                                         ORDER BY COUNT (*) DESC)
                                      rn
                              FROM mrp_sr_assignments msa,
                                   mrp_sr_receipt_org msro,
                                   mrp_sr_source_org msso,
                                   mrp_sourcing_rules msr,
                                   ap_suppliers aps,
                                   ap_supplier_sites_all ass
                             WHERE     msa.sourcing_rule_id =
                                          msro.sourcing_rule_id(+)
                                   AND msa.sourcing_rule_id =
                                          msr.sourcing_rule_id(+)
                                   AND msro.sr_receipt_id =
                                          msso.sr_receipt_id(+)
                                   AND msso.vendor_id = aps.vendor_id(+)
                                   AND msso.vendor_site_id =
                                          ass.vendor_site_id(+)
                                   AND msa.assignment_type = 6
                                   AND msa.assignment_set_id = 1
                                   AND NVL (msso.source_type, 3) = 3
                                   AND msa.inventory_item_id =
                                          p_inventory_item_id
                          GROUP BY msa.inventory_item_id,
                                   msa.sourcing_rule_id,
                                   aps.segment1,
                                   aps.vendor_name)
                   WHERE rn = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_vendor_name := NULL;
               END;

               IF l_vendor_number = '88888' OR l_vendor_number = '99999'
               THEN
                  l_sec := 'Step 4 : Get Master Vendor';

                  BEGIN
                     SELECT (SELECT vendor_name
                               FROM ap_suppliers
                              WHERE segment1 = a.attribute_char_value)
                               mst_vendor_name
                       INTO l_vendor_name
                       FROM ego_all_attr_base_v a
                      WHERE     attribute_name = 'XXWC_VENDOR_NUMBER_ATTR'
                            AND attribute_group_name =
                                   'XXWC_INTERNAL_ATTR_AG'
                            AND inventory_item_id = p_inventory_item_id
                            AND organization_id = 222;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_vendor_name := NULL;
                  END;
               END IF;
            END IF;
         END IF;
      END LOOP;

      IF l_vendor_name IS NULL
      THEN
         l_sec := 'Step 4 : Get Vendor Name for 88888';

         BEGIN
            SELECT vendor_name
              INTO l_vendor_name
              FROM ap_suppliers
             WHERE segment1 = '88888';
         EXCEPTION
            WHEN OTHERS
            THEN
               l_vendor_name := NULL;
         END;
      END IF;

      RETURN (l_vendor_name);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            l_sec := 'Step 5 : Get Vendor Name for 88888';

            SELECT vendor_name
              INTO l_vendor_name
              FROM ap_suppliers
             WHERE segment1 = '88888';
         EXCEPTION
            WHEN OTHERS
            THEN
               l_vendor_name := NULL;
         END;

         RETURN (l_vendor_name);
   END get_vendor_name;


   FUNCTION get_bpa_number (p_inventory_item_id   IN NUMBER,
                            p_organization_id     IN NUMBER)
      RETURN VARCHAR2
   /*************************************************************************
     $Header XXWC_ISR_ROUTINES_PKG.pkb $
     Module Name: XXWC_ISR_ROUTINES_PKG.get_bpa_number

     PURPOSE: ISR Logic as routines for real time use - not for Mass data processing
        only for limited no of records

     REVISIONS:
     Ver        Date         Author                Description
     ---------  -----------  ------------------    ----------------
     1.0        24-Aug-2015  Manjula Chellappan    Initial Version TMS # 20150521-00013 - Data Management Audit Report - make some fields live
   **************************************************************************/

   IS
      l_bpa_number   VARCHAR2 (20);
      l_sec          VARCHAR2 (100);
   BEGIN
      l_sec := 'Step 1 : Get sourcing Details';

      FOR r_source
         IN (SELECT msa.organization_id,
                    msa.inventory_item_id,
                    msa.sourcing_rule_id,
                    msr.sourcing_rule_name sourcing_rule,
                    msro.sr_receipt_id,
                    msso.vendor_id,
                    aps.vendor_name vendor_name,
                    aps.attribute3 tier,
                    aps.attribute4 cat_sba_owner,
                    aps.segment1 vendor_num,
                    msso.vendor_site_id,
                    ass.vendor_site_code vendor_site
               FROM mrp_sr_assignments msa,
                    mrp_sr_receipt_org msro,
                    mrp_sr_source_org msso,
                    mrp_sourcing_rules msr,
                    ap_suppliers aps,
                    ap_supplier_sites_all ass
              WHERE     msa.sourcing_rule_id = msro.sourcing_rule_id(+)
                    AND msa.sourcing_rule_id = msr.sourcing_rule_id(+)
                    AND msro.sr_receipt_id = msso.sr_receipt_id(+)
                    AND msso.vendor_id = aps.vendor_id(+)
                    AND msso.vendor_site_id = ass.vendor_site_id(+)
                    AND msa.assignment_type = 6
                    AND msa.assignment_set_id = 1
                    AND NVL (msso.source_type, 3) = 3
                    AND msa.inventory_item_id = p_inventory_item_id
                    AND msa.organization_id = p_organization_id)
      LOOP
         l_sec := 'Step 2 : Get BPA Number';

         SELECT bpa
           INTO l_bpa_number
           FROM (SELECT pol.po_line_id po_line_id,
                        poh.po_header_id,
                        poh.segment1 bpa,
                        TRUNC (poh.creation_date) po_creation_date,
                        poh.vendor_id,
                        poh.vendor_site_id,
                        poh.global_agreement_flag,
                        NVL (pol.closed_code, 'X') l_closed_code,
                        NVL (poh.closed_code, 'X') h_closed_code,
                        pol.item_id inventory_item_id,
                        NVL (pol.cancel_flag, 'N') l_cancel_flag,
                        NVL (poh.cancel_flag, 'N') h_cancel_flag,
                        pol.unit_price,
                        NVL (pol.quantity, 0) quantity,
                        ROW_NUMBER ()
                        OVER (
                           PARTITION BY pol.item_id,
                                        NVL (pol.cancel_flag, 'N')
                           ORDER BY pol.po_line_id DESC)
                           AS rn
                   FROM po_headers_all poh, po_lines_all pol
                  WHERE     poh.type_lookup_code = 'BLANKET'
                        AND poh.po_header_id = pol.po_header_id
                        AND NVL (pol.cancel_flag, 'N') = 'N'
                        AND NVL (poh.cancel_flag, 'N') = 'N'
                        AND pol.item_id = p_inventory_item_id
                        AND NVL (pol.closed_code, 'X') <> 'CLOSED'
                        AND NVL (poh.closed_code, 'X') <> 'CLOSED'
                        AND NVL (poh.authorization_status, 'X') = 'APPROVED'
                        AND NVL (
                               TRUNC (poh.end_date),
                               NVL (TRUNC (pol.expiration_date),
                                    TRUNC (SYSDATE) + 1)) > TRUNC (SYSDATE)
                        AND poh.vendor_id = r_source.vendor_id
                        AND poh.vendor_site_id = r_source.vendor_site_id
                        AND (   poh.global_agreement_flag = 'Y'
                             OR EXISTS
                                   (SELECT 'x'
                                      FROM po_line_locations_all
                                     WHERE     po_line_id = pol.po_line_id
                                           AND ship_to_organization_id =
                                                  p_organization_id))) l
          WHERE     l.rn = 1
                AND 1 =
                       (SELECT COUNT (*)
                          FROM po_lines_all
                         WHERE     po_header_id = l.po_header_id
                               AND item_id = l.inventory_item_id
                               AND NVL (closed_code, 'X') = l.l_closed_code
                               AND NVL (cancel_flag, 'N') = l.l_cancel_flag);
      END LOOP;

      RETURN (l_bpa_number);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (l_bpa_number);
   END get_bpa_number;

   FUNCTION get_bpa_cost (p_inventory_item_id   IN NUMBER,
                          p_organization_id     IN NUMBER)
      RETURN NUMBER
   /*************************************************************************
     $Header XXWC_ISR_ROUTINES_PKG.pkb $
     Module Name: XXWC_ISR_ROUTINES_PKG.get_bpa_cost

     PURPOSE: ISR Logic as routines for real time use - not for Mass data processing
        only for limited no of records

     REVISIONS:
     Ver        Date         Author                Description
     ---------  -----------  ------------------    ----------------
     1.0        24-Aug-2015  Manjula Chellappan    Initial Version TMS # 20150521-00013 - Data Management Audit Report - make some fields live
   **************************************************************************/
   IS
      l_bpa_cost   NUMBER;
      l_sec        VARCHAR2 (100);
   BEGIN
      l_sec := 'Step 1 : Get sourcing Details';

      FOR r_source
         IN (SELECT msa.organization_id,
                    msa.inventory_item_id,
                    msa.sourcing_rule_id,
                    msr.sourcing_rule_name sourcing_rule,
                    msro.sr_receipt_id,
                    msso.vendor_id,
                    aps.vendor_name vendor_name,
                    aps.attribute3 tier,
                    aps.attribute4 cat_sba_owner,
                    aps.segment1 vendor_num,
                    msso.vendor_site_id,
                    ass.vendor_site_code vendor_site
               FROM mrp_sr_assignments msa,
                    mrp_sr_receipt_org msro,
                    mrp_sr_source_org msso,
                    mrp_sourcing_rules msr,
                    ap_suppliers aps,
                    ap_supplier_sites_all ass
              WHERE     msa.sourcing_rule_id = msro.sourcing_rule_id(+)
                    AND msa.sourcing_rule_id = msr.sourcing_rule_id(+)
                    AND msro.sr_receipt_id = msso.sr_receipt_id(+)
                    AND msso.vendor_id = aps.vendor_id(+)
                    AND msso.vendor_site_id = ass.vendor_site_id(+)
                    AND msa.assignment_type = 6
                    AND msa.assignment_set_id = 1
                    AND NVL (msso.source_type, 3) = 3
                    AND msa.inventory_item_id = p_inventory_item_id
                    AND msa.organization_id = p_organization_id)
      LOOP
         l_sec := 'Step 2 : Get BPA Details';

         FOR r_bpa
            IN (SELECT *
                  FROM (SELECT pol.po_line_id po_line_id,
                               poh.po_header_id,
                               poh.segment1 bpa,
                               TRUNC (poh.creation_date) po_creation_date,
                               poh.vendor_id,
                               poh.vendor_site_id,
                               poh.global_agreement_flag,
                               NVL (pol.closed_code, 'X') l_closed_code,
                               NVL (poh.closed_code, 'X') h_closed_code,
                               pol.item_id inventory_item_id,
                               NVL (pol.cancel_flag, 'N') l_cancel_flag,
                               NVL (poh.cancel_flag, 'N') h_cancel_flag,
                               pol.unit_price,
                               NVL (pol.quantity, 0) quantity,
                               ROW_NUMBER ()
                               OVER (
                                  PARTITION BY pol.item_id,
                                               NVL (pol.cancel_flag, 'N')
                                  ORDER BY pol.po_line_id DESC)
                                  AS rn
                          FROM po_headers_all poh, po_lines_all pol
                         WHERE     poh.type_lookup_code = 'BLANKET'
                               AND poh.po_header_id = pol.po_header_id
                               AND NVL (pol.cancel_flag, 'N') = 'N'
                               AND NVL (poh.cancel_flag, 'N') = 'N'
                               AND pol.item_id = p_inventory_item_id
                               AND NVL (pol.closed_code, 'X') <> 'CLOSED'
                               AND NVL (poh.closed_code, 'X') <> 'CLOSED'
                               AND NVL (poh.authorization_status, 'X') =
                                      'APPROVED'
                               AND NVL (
                                      TRUNC (poh.end_date),
                                      NVL (TRUNC (pol.expiration_date),
                                           TRUNC (SYSDATE) + 1)) >
                                      TRUNC (SYSDATE)
                               AND poh.vendor_id = r_source.vendor_id
                               AND poh.vendor_site_id =
                                      r_source.vendor_site_id
                               AND (   poh.global_agreement_flag = 'Y'
                                    OR EXISTS
                                          (SELECT 'x'
                                             FROM po_line_locations_all
                                            WHERE     po_line_id =
                                                         pol.po_line_id
                                                  AND ship_to_organization_id =
                                                         p_organization_id)))
                       l
                 WHERE     l.rn = 1
                       AND 1 =
                              (SELECT COUNT (*)
                                 FROM po_lines_all
                                WHERE     po_header_id = l.po_header_id
                                      AND item_id = l.inventory_item_id
                                      AND (    NVL (closed_code, 'X') =
                                                  l.l_closed_code
                                           AND NVL (cancel_flag, 'N') =
                                                  l.l_cancel_flag)))
         LOOP
            l_sec := 'Step 3 : Get Promo Price';

            BEGIN
               SELECT promo_price
                 INTO l_bpa_cost
                 FROM xxwc.xxwc_bpa_promo_tbl
                WHERE     po_header_id = r_bpa.po_header_id
                      AND inventory_item_id = r_bpa.inventory_item_id
                      AND TRUNC (SYSDATE) BETWEEN NVL (promo_start_date,
                                                       TRUNC (SYSDATE))
                                              AND NVL (promo_end_date,
                                                       TRUNC (SYSDATE));
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_bpa_cost := NULL;
            END;

            IF l_bpa_cost IS NULL
            THEN
               IF r_source.vendor_site = '0PURCHASING'
               THEN
                  l_sec := 'Step 4 : Get Zone specific Price';

                  BEGIN
                     SELECT a.price_zone_price
                       INTO l_bpa_cost
                       FROM xxwc.xxwc_bpa_price_zone_tbl a,
                            xxwc.xxwc_vendor_price_zone_tbl b
                      WHERE     a.price_zone <> 0
                            AND a.inventory_item_id = p_inventory_item_id
                            AND a.po_header_id = r_bpa.po_header_id
                            AND b.vendor_id = r_bpa.vendor_id
                            AND b.vendor_site_id = r_bpa.vendor_site_id
                            AND b.organization_id = p_organization_id
                            AND a.price_zone = b.price_zone;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_bpa_cost := NULL;
                  END;

                  IF l_bpa_cost IS NULL
                  THEN
                     l_sec := 'Step 5 : Get National Price';

                     BEGIN
                        SELECT a.price_zone_price
                          INTO l_bpa_cost
                          FROM xxwc.xxwc_bpa_price_zone_tbl a
                         WHERE     a.price_zone = 0
                               AND a.inventory_item_id = p_inventory_item_id
                               AND a.po_header_id = r_bpa.po_header_id;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_bpa_cost := NULL;
                     END;
                  END IF;
               END IF;
            END IF;

            l_sec := 'Step 6 : Price from BPA';

            IF l_bpa_cost IS NULL
            THEN
               l_bpa_cost := r_bpa.unit_price;
            END IF;
         END LOOP;
      END LOOP;

      RETURN (l_bpa_cost);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (l_bpa_cost);
   END get_bpa_cost;
END XXWC_ISR_ROUTINES_PKG;
/

GRANT ALL ON APPS.XXWC_ISR_ROUTINES_PKG TO XXEIS;
/