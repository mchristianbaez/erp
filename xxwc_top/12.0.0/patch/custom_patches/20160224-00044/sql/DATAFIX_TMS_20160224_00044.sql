/*
 TMS: 20160224-00044 - Data Fix for item stuck in Cycle Counts in org 158
 created: 2/24/2016
 Created by: P.Vamshidhar
 Last Updated: 02/24/2016
  */
SET SERVEROUTPUT ON SIZE 1000000


BEGIN
   DBMS_OUTPUT.put_line ('Before update');

update mtl_cycle_count_entries
 set export_flag = null
 where organization_id = 304
 and entry_status_code in (1,3)
 and export_flag is not null;

DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);

COMMIT;

   DBMS_OUTPUT.put_line ('After update');

EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
END;
/