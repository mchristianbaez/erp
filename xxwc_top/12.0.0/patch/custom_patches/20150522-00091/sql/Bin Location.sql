--Report Name            : Bin Location
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Bin Location
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_BIN_LOC_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_BIN_LOC_V',401,'','','','','MR020532','XXEIS','Eis Rs Xxwc Inv Bin Loc V','EXIBLV','','');
--Delete View Columns for EIS_XXWC_INV_BIN_LOC_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_BIN_LOC_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_BIN_LOC_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','YTD_SALES',401,'Ytd Sales','YTD_SALES','','~T~D~2','','MR020532','NUMBER','DERIVED COLUMN','DERIVED COLUMN','Ytd Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','MTD_SALES',401,'Mtd Sales','MTD_SALES','','~T~D~2','','MR020532','NUMBER','DERIVED COLUMN','DERIVED COLUMN','Mtd Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','ONHAND',401,'Onhand','ONHAND','','','','MR020532','NUMBER','MTL_ONHAND_QUANTITIES_DETAIL','TRANSACTION_QUANTITY','Onhand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','AVERAGECOST',401,'Averagecost','AVERAGECOST','','~T~D~2','','MR020532','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Averagecost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','MAX_MINMAX_QUANTITY',401,'Max Minmax Quantity','MAX_MINMAX_QUANTITY','','','','MR020532','NUMBER','MTL_SYSTEM_ITEMS_KFV','MAX_MINMAX_QUANTITY','Max Minmax Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','MIN_MINMAX_QUANTITY',401,'Min Minmax Quantity','MIN_MINMAX_QUANTITY','','','','MR020532','NUMBER','MTL_SYSTEM_ITEMS_KFV','MIN_MINMAX_QUANTITY','Min Minmax Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','UOM',401,'Uom','UOM','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_KFV','PRIMARY_UOM_CODE','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_KFV','DESCRIPTION','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','PART_NUMBER',401,'Part Number','PART_NUMBER','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_KFV','SEGMENT1','Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','ORGANIZATION',401,'Organization','ORGANIZATION','','','','MR020532','VARCHAR2','','','Organization','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','ALTERNATE_BIN_LOC',401,'Alternate Bin Loc','ALTERNATE_BIN_LOC','','','','MR020532','VARCHAR2','','','Alternate Bin Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','BIN_LOC',401,'Bin Loc','BIN_LOC','','','','MR020532','VARCHAR2','','','Bin Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','LOCATION',401,'Location','LOCATION','','','','MR020532','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','PRIMARY_BIN_LOC',401,'Primary Bin Loc','PRIMARY_BIN_LOC','','','','MR020532','VARCHAR2','','','Primary Bin Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','OPEN_DEMAND',401,'Open Demand','OPEN_DEMAND','','','','MR020532','CHAR','','','Open Demand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','OPEN_ORDER',401,'Open Order','OPEN_ORDER','','','','MR020532','VARCHAR2','','','Open Order','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','STK',401,'Stk','STK','','','','MR020532','CHAR','','','Stk','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','CATEGORY',401,'Category','CATEGORY','','','','MR020532','VARCHAR2','','','Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','CATEGORY_SET_NAME',401,'Category Set Name','CATEGORY_SET_NAME','','','','MR020532','VARCHAR2','','','Category Set Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','LAST_RECEIVED_DATE',401,'Last Received Date','LAST_RECEIVED_DATE','','','','MR020532','DATE','','','Last Received Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','NO_BIN',401,'No Bin','NO_BIN','','','','MR020532','CHAR','','','No Bin','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','WEIGHT',401,'Weight','WEIGHT','','','','MR020532','NUMBER','MTL_SYSTEM_ITEMS_KFV','UNIT_WEIGHT','Weight','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','VENDOR_NUMBER',401,'Vendor Number','VENDOR_NUMBER','','','','MR020532','VARCHAR2','DERIVED COLUMN','DERIVED COLUMN','Vendor Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','MR020532','VARCHAR2','DERIVED COLUMN','DERIVED COLUMN','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','SELLING_PRICE',401,'Selling Price','SELLING_PRICE','','','','MR020532','NUMBER','MTL_SYSTEM_ITEMS_KFV','LIST_PRICE_PER_UNIT','Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','CAT',401,'Cat','CAT','','','','MR020532','VARCHAR2','MTL_CATEGORIES','SEGMENT2','Cat','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','MR020532','NUMBER','MTL_SYSTEM_ITEMS_KFV','INVENTORY_ITEM_ID','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','INV_ORGANIZATION_ID',401,'Inv Organization Id','INV_ORGANIZATION_ID','','','','MR020532','NUMBER','MTL_SYSTEM_ITEMS_KFV','ORGANIZATION_ID','Inv Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','ORG_ORGANIZATION_ID',401,'Org Organization Id','ORG_ORGANIZATION_ID','','','','MR020532','NUMBER','ORG_ORGANIZATION_DEFINITIONS','ORGANIZATION_ID','Org Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','APPLICATION_ID',401,'Application Id','APPLICATION_ID','','','','MR020532','NUMBER','GL_PERIOD_STATUSES','APPLICATION_ID','Application Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','SET_OF_BOOKS_ID',401,'Set Of Books Id','SET_OF_BOOKS_ID','','','','MR020532','NUMBER','GL_PERIOD_STATUSES','SET_OF_BOOKS_ID','Set Of Books Id','','','');
--Inserting View Components for EIS_XXWC_INV_BIN_LOC_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_KFV','MSI','MSI','MR020532','MR020532','-1','Inventory Item Definitions','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_V','MTL_CATEGORIES',401,'MTL_CATEGORIES_B','MCV','MCV','MR020532','MR020532','-1','Categories','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_V','GL_PERIOD_STATUSES',401,'GL_PERIOD_STATUSES','GPS','GPS','MR020532','MR020532','-1','Calendar Period Statuses','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_V','GL_CODE_COMBINATIONS_KFV',401,'GL_CODE_COMBINATIONS','GCC','GCC','MR020532','MR020532','-1','GL Code Combinations','','','','');
--Inserting View Component Joins for EIS_XXWC_INV_BIN_LOC_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXIBLV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXIBLV.ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','MTL_CATEGORIES','MCV',401,'EXIBLV.CATEGORY_ID','=','MCV.CATEGORY_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','GL_PERIOD_STATUSES','GPS',401,'EXIBLV.APPLICATION_ID','=','GPS.APPLICATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','GL_PERIOD_STATUSES','GPS',401,'EXIBLV.SET_OF_BOOKS_ID','=','GPS.SET_OF_BOOKS_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','GL_PERIOD_STATUSES','GPS',401,'EXIBLV.PERIOD_NAME','=','GPS.PERIOD_NAME(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','GL_CODE_COMBINATIONS_KFV','GCC',401,'EXIBLV.CODE_COMBINATION_ID','=','GCC.CODE_COMBINATION_ID(+)','','','','','MR020532','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Bin Location
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Bin Location
xxeis.eis_rs_ins.lov( '','Select ''Yes''  yes_no
from dual
Union
Select ''No'' yes_no
from dual','','Yes_No_Lov','This lov provides two values yes,no.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT   category_set_name,
         description
    FROM mtl_category_sets
   WHERE mult_item_cat_assign_flag = ''N''
ORDER BY category_set_name','','EIS_INV_CATEGORY_SETS_LOV','Category Sets','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 401,'SELECT DISTINCT concatenated_segments Item_Category,
                description
           FROM mtl_categories_kfv
           order by concatenated_segments','','EIS_INV_ITEM_CATEGORIES_LOV','Item Categories','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 401,'SELECT DISTINCT concatenated_segments Item_Category,
                description
           FROM mtl_categories_kfv
           order by concatenated_segments','','EIS_INV_ITEM_CATEGORIES_LOV','Item Categories','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 401,'SELECT distinct substr(SEGMENT1,3) bin_location FROM MTL_ITEM_LOCATIONS_KFV','','INV Bin Location','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT distinct substr(SEGMENT1,3) bin_location FROM MTL_ITEM_LOCATIONS_KFV','','INV Bin Location','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'','On Hand,No on Hand,ALL,Negative on hand','ON_HAND_TYPE_CSV','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME 
FROM ORG_ORGANIZATION_DEFINITIONS OOD 
WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' FROM DUAL','','XXWC All ORG LIST','XXWC All ORG LIST','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Bin Location
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Bin Location
xxeis.eis_rs_utility.delete_report_rows( 'Bin Location' );
--Inserting Report - Bin Location
xxeis.eis_rs_ins.r( 401,'Bin Location','','Items without Primary Bin Locations Assigned','','','','MR020532','EIS_XXWC_INV_BIN_LOC_V','Y','','','MR020532','','N','White Cap Reports','','Pivot Excel,EXCEL,','');
--Inserting Report Columns - Bin Location
xxeis.eis_rs_ins.rc( 'Bin Location',401,'AVERAGECOST','Average Cost','Averagecost','','~T~D~2','default','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'DESCRIPTION','Description','Description','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'MAX_MINMAX_QUANTITY','Max','Max Minmax Quantity','','~~~','default','','13','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'MIN_MINMAX_QUANTITY','Min','Min Minmax Quantity','','~~~','default','','12','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'ONHAND','Qty Onhand','Onhand','','~~~','default','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'PART_NUMBER','Item Number','Part Number','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'UOM','UOM','Uom','','','default','','10','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'VENDOR NUMBER-NAME','Vendor Number-Name','Uom','VARCHAR2','','default','','4','Y','','','','','','','(EXIBLV.VENDOR_NUMBER||''-''||EXIBLV.VENDOR_NAME)','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'ORGANIZATION','Organization','Organization','','','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'ALTERNATE_BIN_LOC','Alternate Bin Loc','Alternate Bin Loc','','','default','','16','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'EXTENDED VALUE','Extended Value','Alternate Bin Loc','NUMBER','~T~D~2','default','','11','Y','','','','','','','(EXIBLV.ONHAND*EXIBLV.AVERAGECOST)','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'LOCATION','Location','Location','','','default','','17','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'PRIMARY_BIN_LOC','Primary Bin','Primary Bin Loc','','','default','','15','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'PRIMARY BIN','Primary Loc Exists','Primary Bin Loc','NUMBER','~~~','default','','18','Y','','','','','','','decode(EXIBLV.primary_bin_loc,null,''N'',''Y'')','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'OPEN_DEMAND','Open Demand','Open Demand','','','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'OPEN_ORDER','Open Order','Open Order','','','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'STK','Stk','Stk','','','default','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'CATEGORY','Category','Category','','','','','20','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'CATEGORY_SET_NAME','Category Set Name','Category Set Name','','','','','19','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'LAST_RECEIVED_DATE','Last Received Date','Last Received Date','','','','','21','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location',401,'NO_BIN','No Bin','No Bin','','','','','22','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
--Inserting Report Parameters - Bin Location
xxeis.eis_rs_ins.rp( 'Bin Location',401,'Organization','Organization','ORGANIZATION','IN','XXWC All ORG LIST','','VARCHAR2','Y','Y','1','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Bin Location',401,'Starting Bin Location','Starting Bin Location','BIN_LOC','>=','INV Bin Location','','VARCHAR2','N','Y','2','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Bin Location',401,'Ending Bin Location','Ending Bin Location','BIN_LOC','<=','INV Bin Location','','VARCHAR2','N','Y','3','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Bin Location',401,'Category Set','Category Set','CATEGORY_SET_NAME','IN','EIS_INV_CATEGORY_SETS_LOV','Sales Velocity','VARCHAR2','N','Y','4','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Bin Location',401,'Category From','Category From','CATEGORY','>=','EIS_INV_ITEM_CATEGORIES_LOV','','VARCHAR2','N','Y','5','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Bin Location',401,'Category To','Category To','CATEGORY','<=','EIS_INV_ITEM_CATEGORIES_LOV','','VARCHAR2','N','Y','6','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Bin Location',401,'No Bin Location','No Bin Location','NO_BIN','IN','Yes_No_Lov','','VARCHAR2','N','Y','7','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Bin Location',401,'Quantity On Hand','Quantity On Hand','','IN','ON_HAND_TYPE_CSV','','VARCHAR2','N','Y','8','','N','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - Bin Location
xxeis.eis_rs_ins.rcn( 'Bin Location',401,'ORGANIZATION','IN',':Organization','','','N','2','N','MR020532');
xxeis.eis_rs_ins.rcn( 'Bin Location',401,'CATEGORY_SET_NAME','IN',':Category Set','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Bin Location',401,'CATEGORY','>=',':Category From','','','Y','4','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Bin Location',401,'CATEGORY','<=',':Category To','','','Y','5','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Bin Location',401,'NO_BIN','IN',':No Bin Location','','','Y','6','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Bin Location',401,'','','','','and (decode(:Quantity On Hand ,''On Hand'',EXIBLV.ONHAND) > 0
 or decode(:Quantity On Hand ,''No on Hand'',EXIBLV.ONHAND) = 0
 or decode(:Quantity On Hand,''Negative on hand'',EXIBLV.ONHAND)<0
 or decode(:Quantity On Hand,''ALL'',1) =1 )
AND ( ''All'' IN (:Organization) OR (ORGANIZATION IN (:Organization)))','Y','0','','MR020532');
--Inserting Report Sorts - Bin Location
xxeis.eis_rs_ins.rs( 'Bin Location',401,'PART_NUMBER','ASC','MR020532','','');
--Inserting Report Triggers - Bin Location
xxeis.eis_rs_ins.rt( 'Bin Location',401,'begin
xxeis.eis_rs_xxwc_com_util_pkg.g_start_bin := :Starting Bin Location;
xxeis.eis_rs_xxwc_com_util_pkg.g_end_bin  := :Ending Bin Location;
end;','B','Y','MR020532');
--Inserting Report Templates - Bin Location
--Inserting Report Portals - Bin Location
--Inserting Report Dashboards - Bin Location
--Inserting Report Security - Bin Location
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50619',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50924',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','51052',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50879',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50851',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50852',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50821',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','20005','','50880',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','20634',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','','LB048272','',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','','10012196','',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','51029',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50882',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50883',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50981',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50855',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50884',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','20005','','50900',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50895',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50865',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50864',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50849',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','660','','50871',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50863',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50862',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','51064',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50846',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50845',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50844',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','707','','51104',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','50990',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','401','','51104',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','201','','50892',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','','MM050208','',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Bin Location','','SO004816','',401,'MR020532','','');
--Inserting Report Pivots - Bin Location
xxeis.eis_rs_ins.rpivot( 'Bin Location',401,'Pivot','1','0,0|1,0,1','1,1,0,0|PivotStyleDark1|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Bin Location',401,'Pivot','LOCATION','ROW_FIELD','','Location','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Bin Location',401,'Pivot','ONHAND','DATA_FIELD','SUM','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Bin Location',401,'Pivot','PART_NUMBER','ROW_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Bin Location',401,'Pivot','PRIMARY BIN','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Bin Location',401,'Pivot','STK','PAGE_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Bin Location',401,'Pivot','OPEN_DEMAND','PAGE_FIELD','','','3','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Bin Location',401,'Pivot','PRIMARY_BIN_LOC','DATA_FIELD','COUNT','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Bin Location',401,'Pivot','LAST_RECEIVED_DATE','ROW_FIELD','','','3','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
