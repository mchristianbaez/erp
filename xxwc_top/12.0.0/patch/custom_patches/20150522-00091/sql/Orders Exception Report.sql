--Report Name            : Orders Exception Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Orders Exception Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_ORDER_EXP_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_ORDER_EXP_V',660,'','','','','MR020532','XXEIS','Eis Xxwc Om Order Exp V','EXOOEV','','');
--Delete View Columns for EIS_XXWC_OM_ORDER_EXP_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_ORDER_EXP_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_ORDER_EXP_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','LOC',660,'Loc','LOC','','','','MR020532','VARCHAR2','','','Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','EXCEPTION_DESCRIPTION',660,'Exception Description','EXCEPTION_DESCRIPTION','','','','MR020532','VARCHAR2','','','Exception Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','EXT_ORDER_TOTAL',660,'Ext Order Total','EXT_ORDER_TOTAL','','','','MR020532','NUMBER','','','Ext Order Total','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','MR020532','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','MR020532','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','EXCEPTION_DATE',660,'Exception Date','EXCEPTION_DATE','','','','MR020532','DATE','','','Exception Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','ORDER_DATE',660,'Order Date','ORDER_DATE','','','','MR020532','DATE','','','Order Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','MR020532','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','EXCEPTION_RESAON',660,'Exception Resaon','EXCEPTION_RESAON','','','','MR020532','VARCHAR2','','','Exception Resaon','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','SALES_PERSON_NAME',660,'Sales Person Name','SALES_PERSON_NAME','','','','MR020532','VARCHAR2','','','Sales Person Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','CREATED_BY',660,'Created By','CREATED_BY','','','','MR020532','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','QTY',660,'Qty','QTY','','','','MR020532','NUMBER','','','Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','SHIP_METHOD',660,'Ship Method','SHIP_METHOD','','','','MR020532','VARCHAR2','','','Ship Method','','','');
--Inserting View Components for EIS_XXWC_OM_ORDER_EXP_V
--Inserting View Component Joins for EIS_XXWC_OM_ORDER_EXP_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Orders Exception Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Orders Exception Report
xxeis.eis_rs_ins.lov( 660,'select  RS.Name,SALESREP_ID  from  RA_SALESREPS RS
WHERE  RS.NAME is not null','','OM SALES REP','This gives the sales representative name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Orders Exception Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Orders Exception Report
xxeis.eis_rs_utility.delete_report_rows( 'Orders Exception Report' );
--Inserting Report - Orders Exception Report
xxeis.eis_rs_ins.r( 660,'Orders Exception Report','','White Cap Exception Report','','','','MR020532','EIS_XXWC_OM_ORDER_EXP_V','Y','','','MR020532','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','');
--Inserting Report Columns - Orders Exception Report
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'EXCEPTION_DATE','Exception Date','Exception Date','','','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'EXCEPTION_DESCRIPTION','Exception Description','Exception Description','','','default','','12','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'EXCEPTION_RESAON','Exception Reason','Exception Resaon','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'EXT_ORDER_TOTAL','Line Total','Ext Order Total','','~T~D~2','default','','11','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'ORDER_DATE','Order Date','Order Date','','','default','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'ORDER_TYPE','Order Type','Order Type','','','default','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'CREATED_BY','Created By','Created By','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'QTY','Qty','Qty','','~~~','default','','10','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'LOC','Loc','Loc','','','','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'SALES_PERSON_NAME','Sales Person Name','Sales Person Name','','','','','13','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'SHIP_METHOD','Ship Method','Ship Method','','','','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
--Inserting Report Parameters - Orders Exception Report
xxeis.eis_rs_ins.rp( 'Orders Exception Report',660,'Sales Person Name','Sales Person Name','SALES_PERSON_NAME','IN','OM SALES REP','','VARCHAR2','N','Y','2','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Orders Exception Report',660,'Organization','Organization','LOC','IN','OM Warehouse All','','VARCHAR2','Y','Y','1','','Y','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - Orders Exception Report
xxeis.eis_rs_ins.rcn( 'Orders Exception Report',660,'SALES_PERSON_NAME','IN',':Sales Person Name','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Orders Exception Report',660,'LOC','IN',':Organization','','','N','2','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Orders Exception Report',660,'','','','','AND ( ''All'' IN (:Organization) OR (Loc IN (:Organization)))','Y','0','','MR020532');
--Inserting Report Sorts - Orders Exception Report
xxeis.eis_rs_ins.rs( 'Orders Exception Report',660,'EXCEPTION_RESAON','ASC','MR020532','1','');
--Inserting Report Triggers - Orders Exception Report
--Inserting Report Templates - Orders Exception Report
--Inserting Report Portals - Orders Exception Report
--Inserting Report Dashboards - Orders Exception Report
--Inserting Report Security - Orders Exception Report
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','51044',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','51509',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','51045',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','50860',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','50886',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','50859',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','50858',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','50901',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','51025',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','50857',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','20005','','50900',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','','HY016540','',660,'MR020532','','');
--Inserting Report Pivots - Orders Exception Report
xxeis.eis_rs_ins.rpivot( 'Orders Exception Report',660,'Summary by reason','1','1,0|1,2,0','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Summary by reason
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','EXT_ORDER_TOTAL','DATA_FIELD','SUM','Ext Total','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','EXCEPTION_RESAON','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','ORDER_NUMBER','ROW_FIELD','','','3','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','CREATED_BY','ROW_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','QTY','ROW_FIELD','','','4','','');
--Inserting Report Summary Calculation Columns For Pivot- Summary by reason
END;
/
set scan on define on
