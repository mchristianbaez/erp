--Report Name            : Inventory - Onhand Quantity - WC
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating View Data for Inventory - Onhand Quantity - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_INV_ONHAND_QUANTITY_V
xxeis.eis_rs_ins.v( 'EIS_INV_ONHAND_QUANTITY_V',401,'On-Hand quantities of Inventory Items','','','','MR020532','XXEIS','Eis Inv Onhand Quantity V','EIOQV','','');
--Delete View Columns for EIS_INV_ONHAND_QUANTITY_V
xxeis.eis_rs_utility.delete_view_rows('EIS_INV_ONHAND_QUANTITY_V',401,FALSE);
--Inserting View Columns for EIS_INV_ONHAND_QUANTITY_V
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','STATUS_CODE',401,'Status Code','STATUS_CODE','','','','MR020532','VARCHAR2','','','Status Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','UNIT_NUMBER',401,'Unit Number','UNIT_NUMBER','','','','MR020532','VARCHAR2','','','Unit Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','SUBINVENTORY_TYPE',401,'Subinventory Type','SUBINVENTORY_TYPE','','','','MR020532','NUMBER','','','Subinventory Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','INV_ORG_NAME',401,'Inv Org Name','INV_ORG_NAME','','','','MR020532','VARCHAR2','','','Inv Org Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','ORGANIZATION_CODE',401,'Organization Code','ORGANIZATION_CODE','','','','MR020532','VARCHAR2','','','Organization Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MASTER_ORG_CODE',401,'Master Org Code','MASTER_ORG_CODE','','','','MR020532','VARCHAR2','','','Master Org Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','SUBINVENTORY_CODE',401,'Subinventory Code','SUBINVENTORY_CODE','','','','MR020532','VARCHAR2','','','Subinventory Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','LOCATOR',401,'Locator','LOCATOR','','','','MR020532','VARCHAR2','','','Locator','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','ITEM',401,'Item','ITEM','','','','MR020532','VARCHAR2','','','Item','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','REVISION',401,'Revision','REVISION','','','','MR020532','VARCHAR2','','','Revision','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','OWNING_ORGANIZATION',401,'Owning Organization','OWNING_ORGANIZATION','','','','MR020532','VARCHAR2','','','Owning Organization','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','PLANNING_ORGANIZATION',401,'Planning Organization','PLANNING_ORGANIZATION','','','','MR020532','VARCHAR2','','','Planning Organization','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','COST_GROUP',401,'Cost Group','COST_GROUP','','','','MR020532','VARCHAR2','','','Cost Group','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','UNIT_OF_MEASURE',401,'Unit Of Measure','UNIT_OF_MEASURE','','','','MR020532','VARCHAR2','','','Unit Of Measure','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','ON_HAND',401,'On Hand','ON_HAND','','','','MR020532','NUMBER','','','On Hand','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','UNPACKED',401,'Unpacked','UNPACKED','','','','MR020532','NUMBER','','','Unpacked','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','PACKED',401,'Packed','PACKED','','','','MR020532','NUMBER','','','Packed','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','LOT_NUMBER',401,'Lot Number','LOT_NUMBER','','','','MR020532','VARCHAR2','','','Lot Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','PROJECT_NAME',401,'Project Name','PROJECT_NAME','','','','MR020532','VARCHAR2','','','Project Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','LOT_EXPIRY_DATE',401,'Lot Expiry Date','LOT_EXPIRY_DATE','','','','MR020532','DATE','','','Lot Expiry Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MAX_MINMAX_QUANTITY',401,'Max Minmax Quantity','MAX_MINMAX_QUANTITY~MAX_MINMAX_QUANTITY','','','','MR020532','NUMBER','','','Max Minmax Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MIN_MINMAX_QUANTITY',401,'Min Minmax Quantity','MIN_MINMAX_QUANTITY~MIN_MINMAX_QUANTITY','','','','MR020532','NUMBER','','','Min Minmax Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','MR020532','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MIL_ORGANIZATION_ID',401,'Mil Organization Id','MIL_ORGANIZATION_ID~MIL_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Mil Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MMS_STATUS_ID',401,'Mms Status Id','MMS_STATUS_ID~MMS_STATUS_ID','','','','MR020532','NUMBER','','','Mms Status Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MP_ORGANIZATION_ID',401,'Mp Organization Id','MP_ORGANIZATION_ID~MP_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Mp Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV_INVENTORY_ITEM_ID',401,'Msiv Inventory Item Id','MSIV_INVENTORY_ITEM_ID~MSIV_INVENTORY_ITEM_ID','','','','MR020532','NUMBER','','','Msiv Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV_ORGANIZATION_ID',401,'Msiv Organization Id','MSIV_ORGANIZATION_ID~MSIV_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Msiv Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSI_ORGANIZATION_ID',401,'Msi Organization Id','MSI_ORGANIZATION_ID~MSI_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Msi Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MTV_TASK_ID',401,'Mtv Task Id','MTV_TASK_ID~MTV_TASK_ID','','','','MR020532','NUMBER','','','Mtv Task Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','OODP_ORGANIZATION_ID',401,'Oodp Organization Id','OODP_ORGANIZATION_ID~OODP_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Oodp Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','OODW_ORGANIZATION_ID',401,'Oodw Organization Id','OODW_ORGANIZATION_ID~OODW_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Oodw Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','OOD_ORGANIZATION_ID',401,'Ood Organization Id','OOD_ORGANIZATION_ID~OOD_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Ood Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MLN_INVENTORY_ITEM_ID',401,'Mln Inventory Item Id','MLN_INVENTORY_ITEM_ID~MLN_INVENTORY_ITEM_ID','','','','MR020532','NUMBER','','','Mln Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MLN_LOT_NUMBER',401,'Mln Lot Number','MLN_LOT_NUMBER~MLN_LOT_NUMBER','','','','MR020532','VARCHAR2','','','Mln Lot Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MLN_ORGANIZATION_ID',401,'Mln Organization Id','MLN_ORGANIZATION_ID~MLN_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Mln Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','ORGANIZATION_ID',401,'Organization Id','ORGANIZATION_ID','','','','MR020532','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','LOCATOR_ID',401,'Locator Id','LOCATOR_ID','','','','MR020532','NUMBER','','','Locator Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','MR020532','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','INVENTORY_LOCATION_ID',401,'Inventory Location Id','INVENTORY_LOCATION_ID~INVENTORY_LOCATION_ID','','','','MR020532','NUMBER','','','Inventory Location Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','LANGUAGE',401,'Language','LANGUAGE~LANGUAGE','','','','MR020532','VARCHAR2','','','Language','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','LIST_PRICE_PER_UNIT',401,'List Price Per Unit','LIST_PRICE_PER_UNIT~LIST_PRICE_PER_UNIT','','','','MR020532','NUMBER','','','List Price Per Unit','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','LPN',401,'Lpn','LPN~LPN','','','','MR020532','VARCHAR2','','','Lpn','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','LPN_ID',401,'Lpn Id','LPN_ID~LPN_ID','','','','MR020532','NUMBER','','','Lpn Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','ONHAND_QUANTITIES_ID',401,'Onhand Quantities Id','ONHAND_QUANTITIES_ID~ONHAND_QUANTITIES_ID','','','','MR020532','NUMBER','','','Onhand Quantities Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','DATE_RECEIVED',401,'Date Received','DATE_RECEIVED','','','','MR020532','DATE','','','Date Received','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','SECONDARY_INVENTORY_NAME',401,'Secondary Inventory Name','SECONDARY_INVENTORY_NAME','','','','MR020532','VARCHAR2','','','Secondary Inventory Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','COST_GROUP_ID',401,'Cost Group Id','COST_GROUP_ID','','~T~D~2','','MR020532','NUMBER','','','Cost Group Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','PROJECT_ID',401,'Project Id','PROJECT_ID','','','','MR020532','NUMBER','','','Project Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','TASK_ID',401,'Task Id','TASK_ID','','','','MR020532','NUMBER','','','Task Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','SUBINVENTORY_STATUS_ID',401,'Subinventory Status Id','SUBINVENTORY_STATUS_ID','','','','MR020532','NUMBER','','','Subinventory Status Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','LOCATOR_STATUS_ID',401,'Locator Status Id','LOCATOR_STATUS_ID','','','','MR020532','NUMBER','','','Locator Status Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','LOT_STATUS_ID',401,'Lot Status Id','LOT_STATUS_ID','','','','MR020532','NUMBER','','','Lot Status Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','PLANNING_TP_TYPE',401,'Planning Tp Type','PLANNING_TP_TYPE','','','','MR020532','NUMBER','','','Planning Tp Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','PLANNING_ORGANIZATION_ID',401,'Planning Organization Id','PLANNING_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Planning Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','OWNING_TP_TYPE',401,'Owning Tp Type','OWNING_TP_TYPE','','','','MR020532','NUMBER','','','Owning Tp Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','OWNING_ORGANIZATION_ID',401,'Owning Organization Id','OWNING_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Owning Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','CCGA_COST_GROUP_ID',401,'Ccga Cost Group Id','CCGA_COST_GROUP_ID~CCGA_COST_GROUP_ID','','~T~D~2','','MR020532','NUMBER','','','Ccga Cost Group Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','CCGA_ORGANIZATION_ID',401,'Ccga Organization Id','CCGA_ORGANIZATION_ID~CCGA_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Ccga Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','CCG_COST_GROUP_ID',401,'Ccg Cost Group Id','CCG_COST_GROUP_ID~CCG_COST_GROUP_ID','','~T~D~2','','MR020532','NUMBER','','','Ccg Cost Group Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','HAOU_ORGANIZATION_ID',401,'Haou Organization Id','HAOU_ORGANIZATION_ID~HAOU_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Haou Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MIL#101#STOCKLOCATIONS',401,'Key Flex Field Column - MIL#101#STOCKLOCATIONS','MIL#101#STOCKLOCATIONS','','','','MR020532','VARCHAR2','MTL_ITEM_LOCATIONS','SEGMENT1','Stock Locations#101','','1015063','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MIL#101#STOCKLOCATIONS_DESC',401,'Key Flex Field Column - MIL#101#STOCKLOCATIONS_DESC','MIL#101#STOCKLOCATIONS_DESC','','','','MR020532','VARCHAR2','MTL_ITEM_LOCATIONS','SEGMENT1','Stock Locations#101#DESC','','1015063','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSI#101#ITEM',401,'Key Flex Field Column - MSI#101#ITEM','MSI#101#ITEM','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','SEGMENT1','Item#101','','1015057','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSI#101#ITEM_DESC',401,'Key Flex Field Column - MSI#101#ITEM_DESC','MSI#101#ITEM_DESC','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','SEGMENT1','Item#101#DESC','','1015057','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MP#FACTORY_PLANNER_DATA_DIRE',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Data Directory','MP#Factory_Planner_Data_Dire','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE1','Mp#Factory Planner Data Directory','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MP#FRU',401,'Descriptive flexfield: Organization parameters Column Name: FRU','MP#FRU','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE10','Mp#Fru','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MP#LOCATION_NUMBER',401,'Descriptive flexfield: Organization parameters Column Name: Location Number','MP#Location_Number','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE11','Mp#Location Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MP#BRANCH_OPERATIONS_MANAGER',401,'Descriptive flexfield: Organization parameters Column Name: Branch Operations Manager','MP#Branch_Operations_Manager','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE13','Mp#Branch Operations Manager','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MP#DELIVER_CHARGE',401,'Descriptive flexfield: Organization parameters Column Name: Deliver Charge','MP#Deliver_Charge','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE14','Mp#Deliver Charge','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MP#FACTORY_PLANNER_EXECUTABL',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Executable Directory','MP#Factory_Planner_Executabl','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE2','Mp#Factory Planner Executable Directory','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MP#FACTORY_PLANNER_USER',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner User','MP#Factory_Planner_User','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE3','Mp#Factory Planner User','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MP#FACTORY_PLANNER_HOST',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Host','MP#Factory_Planner_Host','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE4','Mp#Factory Planner Host','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MP#FACTORY_PLANNER_PORT_NUMB',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Port Number','MP#Factory_Planner_Port_Numb','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE5','Mp#Factory Planner Port Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MP#PRICING_ZONE',401,'Descriptive flexfield: Organization parameters Column Name: Pricing Zone','MP#Pricing_Zone','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE6','Mp#Pricing Zone','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MP#ORG_TYPE',401,'Descriptive flexfield: Organization parameters Column Name: Org Type','MP#Org_Type','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE7','Mp#Org Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MP#DISTRICT',401,'Descriptive flexfield: Organization parameters Column Name: District','MP#District','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE8','Mp#District','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MP#REGION',401,'Descriptive flexfield: Organization parameters Column Name: Region','MP#Region','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE9','Mp#Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#HDS#LOB',401,'Descriptive flexfield: Items Column Name: LOB Context: HDS','MSIV#HDS#LOB','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msiv#Hds#Lob','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#HDS#DROP_SHIPMENT_ELIGA',401,'Descriptive flexfield: Items Column Name: Drop Shipment Eligable Context: HDS','MSIV#HDS#Drop_Shipment_Eliga','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msiv#Hds#Drop Shipment Eligable','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#HDS#INVOICE_UOM',401,'Descriptive flexfield: Items Column Name: Invoice UOM Context: HDS','MSIV#HDS#Invoice_UOM','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msiv#Hds#Invoice Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#HDS#PRODUCT_ID',401,'Descriptive flexfield: Items Column Name: Product ID Context: HDS','MSIV#HDS#Product_ID','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msiv#Hds#Product Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#HDS#VENDOR_PART_NUMBER',401,'Descriptive flexfield: Items Column Name: Vendor Part Number Context: HDS','MSIV#HDS#Vendor_Part_Number','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msiv#Hds#Vendor Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#HDS#UNSPSC_CODE',401,'Descriptive flexfield: Items Column Name: UNSPSC Code Context: HDS','MSIV#HDS#UNSPSC_Code','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msiv#Hds#Unspsc Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#HDS#UPC_PRIMARY',401,'Descriptive flexfield: Items Column Name: UPC Primary Context: HDS','MSIV#HDS#UPC_Primary','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msiv#Hds#Upc Primary','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#HDS#SKU_DESCRIPTION',401,'Descriptive flexfield: Items Column Name: SKU Description Context: HDS','MSIV#HDS#SKU_Description','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msiv#Hds#Sku Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#CA_PROP_65',401,'Descriptive flexfield: Items Column Name: CA Prop 65 Context: WC','MSIV#WC#CA_Prop_65','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msiv#Wc#Ca Prop 65','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#COUNTRY_OF_ORIGIN',401,'Descriptive flexfield: Items Column Name: Country of Origin Context: WC','MSIV#WC#Country_of_Origin','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msiv#Wc#Country Of Origin','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#ORM_D_FLAG',401,'Descriptive flexfield: Items Column Name: ORM-D Flag Context: WC','MSIV#WC#ORM_D_Flag','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE11','Msiv#Wc#Orm-D Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#STORE_VELOCITY',401,'Descriptive flexfield: Items Column Name: Store Velocity Context: WC','MSIV#WC#Store_Velocity','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE12','Msiv#Wc#Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#DC_VELOCITY',401,'Descriptive flexfield: Items Column Name: DC Velocity Context: WC','MSIV#WC#DC_Velocity','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE13','Msiv#Wc#Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#YEARLY_STORE_VELOCIT',401,'Descriptive flexfield: Items Column Name: Yearly Store Velocity Context: WC','MSIV#WC#Yearly_Store_Velocit','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE14','Msiv#Wc#Yearly Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#YEARLY_DC_VELOCITY',401,'Descriptive flexfield: Items Column Name: Yearly DC Velocity Context: WC','MSIV#WC#Yearly_DC_Velocity','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msiv#Wc#Yearly Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#PRISM_PART_NUMBER',401,'Descriptive flexfield: Items Column Name: PRISM Part Number Context: WC','MSIV#WC#PRISM_Part_Number','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE16','Msiv#Wc#Prism Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#HAZMAT_DESCRIPTION',401,'Descriptive flexfield: Items Column Name: Hazmat Description Context: WC','MSIV#WC#Hazmat_Description','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE17','Msiv#Wc#Hazmat Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#HAZMAT_CONTAINER',401,'Descriptive flexfield: Items Column Name: Hazmat Container Context: WC','MSIV#WC#Hazmat_Container','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE18','Msiv#Wc#Hazmat Container','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#GTP_INDICATOR',401,'Descriptive flexfield: Items Column Name: GTP Indicator Context: WC','MSIV#WC#GTP_Indicator','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE19','Msiv#Wc#Gtp Indicator','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#LAST_LEAD_TIME',401,'Descriptive flexfield: Items Column Name: Last Lead Time Context: WC','MSIV#WC#Last_Lead_Time','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msiv#Wc#Last Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#AMU',401,'Descriptive flexfield: Items Column Name: AMU Context: WC','MSIV#WC#AMU','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE20','Msiv#Wc#Amu','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#RESERVE_STOCK',401,'Descriptive flexfield: Items Column Name: Reserve Stock Context: WC','MSIV#WC#Reserve_Stock','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE21','Msiv#Wc#Reserve Stock','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#TAXWARE_CODE',401,'Descriptive flexfield: Items Column Name: Taxware Code Context: WC','MSIV#WC#Taxware_Code','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE22','Msiv#Wc#Taxware Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#AVERAGE_UNITS',401,'Descriptive flexfield: Items Column Name: Average Units Context: WC','MSIV#WC#Average_Units','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE25','Msiv#Wc#Average Units','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#PRODUCT_CODE',401,'Descriptive flexfield: Items Column Name: Product code Context: WC','MSIV#WC#Product_code','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE26','Msiv#Wc#Product Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#IMPORT_DUTY_',401,'Descriptive flexfield: Items Column Name: Import Duty % Context: WC','MSIV#WC#Import_Duty_','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE27','Msiv#Wc#Import Duty %','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#KEEP_ITEM_ACTIVE',401,'Descriptive flexfield: Items Column Name: Keep Item Active Context: WC','MSIV#WC#Keep_Item_Active','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE29','Msiv#Wc#Keep Item Active','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#PESTICIDE_FLAG',401,'Descriptive flexfield: Items Column Name: Pesticide Flag Context: WC','MSIV#WC#Pesticide_Flag','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msiv#Wc#Pesticide Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#CALC_LEAD_TIME',401,'Descriptive flexfield: Items Column Name: Calc Lead Time Context: WC','MSIV#WC#Calc_Lead_Time','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE30','Msiv#Wc#Calc Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#VOC_GL',401,'Descriptive flexfield: Items Column Name: VOC G/L Context: WC','MSIV#WC#VOC_GL','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msiv#Wc#Voc G/L','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#PESTICIDE_FLAG_STATE',401,'Descriptive flexfield: Items Column Name: Pesticide Flag State Context: WC','MSIV#WC#Pesticide_Flag_State','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msiv#Wc#Pesticide Flag State','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#VOC_CATEGORY',401,'Descriptive flexfield: Items Column Name: VOC Category Context: WC','MSIV#WC#VOC_Category','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msiv#Wc#Voc Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#VOC_SUB_CATEGORY',401,'Descriptive flexfield: Items Column Name: VOC Sub Category Context: WC','MSIV#WC#VOC_Sub_Category','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE7','Msiv#Wc#Voc Sub Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#MSDS_#',401,'Descriptive flexfield: Items Column Name: MSDS # Context: WC','MSIV#WC#MSDS_#','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE8','Msiv#Wc#Msds #','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','MSIV#WC#HAZMAT_PACKAGING_GRO',401,'Descriptive flexfield: Items Column Name: Hazmat Packaging Group Context: WC','MSIV#WC#Hazmat_Packaging_Gro','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE9','Msiv#Wc#Hazmat Packaging Group','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','SALES_VELOCITY',401,'Sales Velocity','SALES_VELOCITY','','','','MR020532','VARCHAR2','','','Sales Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_ONHAND_QUANTITY_V','OPEN_DEMAND',401,'Open Demand','OPEN_DEMAND','','','','MR020532','NUMBER','','','Open Demand','','','');
--Inserting View Components for EIS_INV_ONHAND_QUANTITY_V
xxeis.eis_rs_ins.vcomp( 'EIS_INV_ONHAND_QUANTITY_V','PA_TASKS',401,'PA_TASKS','MTV','','MR020532','MR020532','-1','','N','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_ONHAND_QUANTITY_V','MTL_PARAMETERS',401,'MTL_PARAMETERS','MP','MP','MR020532','MR020532','-1','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_ONHAND_QUANTITY_V','CST_COST_GROUPS',401,'CST_COST_GROUPS','CCG','CCG','MR020532','MR020532','-1','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_ONHAND_QUANTITY_V','MTL_MATERIAL_STATUSES_VL',401,'MTL_MATERIAL_STATUSES_B','MMS','MMS','MR020532','MR020532','-1','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_ONHAND_QUANTITY_V','MTL_ITEM_LOCATIONS_KFV',401,'MTL_ITEM_LOCATIONS','MIL','MIL','MR020532','MR020532','-1','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_ONHAND_QUANTITY_V','PJM_UNIT_NUMBERS',401,'PJM_UNIT_NUMBERS','PUN','PUN','MR020532','MR020532','-1','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_ONHAND_QUANTITY_V','MTL_SECONDARY_INVENTORIES',401,'MTL_SECONDARY_INVENTORIES','MSI','MSI','MR020532','MR020532','-1','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_ONHAND_QUANTITY_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','OOD','OOD','MR020532','MR020532','-1','master_organization_id','N','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_ONHAND_QUANTITY_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','OODW','OODW','MR020532','MR020532','-1','owning_organization_id','N','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_ONHAND_QUANTITY_V','MTL_LOT_NUMBERS',401,'MTL_LOT_NUMBERS','MLN','MLN','MR020532','MR020532','-1','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_ONHAND_QUANTITY_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','OODP','OOP','MR020532','MR020532','-1','planning_organization_id','N','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_ONHAND_QUANTITY_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSIV','MSIV','MR020532','MR020532','-1','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_ONHAND_QUANTITY_V','CST_COST_GROUP_ACCOUNTS',401,'CST_COST_GROUP_ACCOUNTS','CCGA','CCGA','MR020532','MR020532','-1','','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_ONHAND_QUANTITY_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','HAOU','HAOU','MR020532','MR020532','-1','','N','','','');
--Inserting View Component Joins for EIS_INV_ONHAND_QUANTITY_V
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','PA_TASKS','MTV',401,'EIOQV.MTV_TASK_ID','=','MTV.TASK_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','MTL_PARAMETERS','MP',401,'EIOQV.MP_ORGANIZATION_ID','=','MP.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','CST_COST_GROUPS','CCG',401,'EIOQV.CCG_COST_GROUP_ID','=','CCG.COST_GROUP_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','MTL_MATERIAL_STATUSES_VL','MMS',401,'EIOQV.MMS_STATUS_ID','=','MMS.STATUS_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','MTL_ITEM_LOCATIONS_KFV','MIL',401,'EIOQV.INVENTORY_LOCATION_ID','=','MIL.INVENTORY_LOCATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','MTL_ITEM_LOCATIONS_KFV','MIL',401,'EIOQV.MIL_ORGANIZATION_ID','=','MIL.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','PJM_UNIT_NUMBERS','PUN',401,'EIOQV.UNIT_NUMBER','=','PUN.UNIT_NUMBER(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','MTL_SECONDARY_INVENTORIES','MSI',401,'EIOQV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','MTL_SECONDARY_INVENTORIES','MSI',401,'EIOQV.SECONDARY_INVENTORY_NAME','=','MSI.SECONDARY_INVENTORY_NAME(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','HR_ORGANIZATION_UNITS','OOD',401,'EIOQV.OOD_ORGANIZATION_ID','=','OOD.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','HR_ORGANIZATION_UNITS','OODW',401,'EIOQV.OODW_ORGANIZATION_ID','=','OODW.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','MTL_LOT_NUMBERS','MLN',401,'EIOQV.MLN_INVENTORY_ITEM_ID','=','MLN.INVENTORY_ITEM_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','MTL_LOT_NUMBERS','MLN',401,'EIOQV.MLN_LOT_NUMBER','=','MLN.LOT_NUMBER(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','MTL_LOT_NUMBERS','MLN',401,'EIOQV.MLN_ORGANIZATION_ID','=','MLN.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','HR_ORGANIZATION_UNITS','OODP',401,'EIOQV.OODP_ORGANIZATION_ID','=','OODP.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','MTL_SYSTEM_ITEMS_KFV','MSIV',401,'EIOQV.MSIV_INVENTORY_ITEM_ID','=','MSIV.INVENTORY_ITEM_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','MTL_SYSTEM_ITEMS_KFV','MSIV',401,'EIOQV.MSIV_ORGANIZATION_ID','=','MSIV.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','CST_COST_GROUP_ACCOUNTS','CCGA',401,'EIOQV.CCGA_COST_GROUP_ID','=','CCGA.COST_GROUP_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','CST_COST_GROUP_ACCOUNTS','CCGA',401,'EIOQV.CCGA_ORGANIZATION_ID','=','CCGA.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_ONHAND_QUANTITY_V','HR_ORGANIZATION_UNITS','HAOU',401,'EIOQV.HAOU_ORGANIZATION_ID','=','HAOU.ORGANIZATION_ID(+)','','','','','MR020532','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Inventory - Onhand Quantity - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Inventory - Onhand Quantity - WC
xxeis.eis_rs_ins.lov( 401,'select   ccg.cost_group
from cst_cost_groups ccg, cst_cost_group_accounts ccga
where ccg.cost_group_id = ccga.cost_group_id','','EIS_INV_COST_GROUP_LOV','List all the values for the cost group
','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 401,'SELECT   organization_code,organization_name
    FROM org_organization_definitions
   where operating_unit = fnd_profile.value (''ORG_ID'')
union
SELECT ''All Organizations'' organization_code, ''All'' organization_name  FROM dual','','EIS_INV_INVENTORY_ORGS_LOV','List of All Inventory Orgs under a given operating unit.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT DISTINCT concatenated_segments item, description FROM mtl_system_items_kfv msi,
                org_organization_definitions ood
          WHERE msi.organization_id = ood.organization_id
            AND ood.operating_unit = fnd_profile.VALUE (''ORG_ID'')
       ORDER BY concatenated_segments','','EIS_INV_ITEM_LOV','List of all inventory items.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select SECONDARY_INVENTORY_NAME SUB_INVENTORY,HAOU.NAME ORGANIZATION_NAME
from MTL_SECONDARY_INVENTORIES MSI,
     HR_ALL_ORGANIZATION_UNITS HAOU
WHERE 1=1
AND MSI.ORGANIZATION_ID = HAOU.organization_id','','EIS_INV_SUBINVENTORY_LOV','List of All SubInventories.','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Inventory - Onhand Quantity - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Inventory - Onhand Quantity - WC
xxeis.eis_rs_utility.delete_report_rows( 'Inventory - Onhand Quantity - WC' );
--Inserting Report - Inventory - Onhand Quantity - WC
xxeis.eis_rs_ins.r( 401,'Inventory - Onhand Quantity - WC','','The Onhand Quantity Report displays the total quantity of an item in a subinventory.','','','','MR020532','EIS_INV_ONHAND_QUANTITY_V','Y','','','MR020532','','Y','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Inventory - Onhand Quantity - WC
xxeis.eis_rs_ins.rc( 'Inventory - Onhand Quantity - WC',401,'INV_ORG_NAME','Inv Org Name','Inv Org Name','','','default','','1','N','','','','','','','','MR020532','N','N','','EIS_INV_ONHAND_QUANTITY_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - Onhand Quantity - WC',401,'ITEM','Item','Item','','','default','','4','N','','','','','','','','MR020532','N','N','','EIS_INV_ONHAND_QUANTITY_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - Onhand Quantity - WC',401,'ON_HAND','On Hand','On Hand','','~~~','default','','8','N','','','','','','','','MR020532','N','N','','EIS_INV_ONHAND_QUANTITY_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - Onhand Quantity - WC',401,'ORGANIZATION_CODE','Organization Code','Organization Code','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_INV_ONHAND_QUANTITY_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - Onhand Quantity - WC',401,'SUBINVENTORY_CODE','Subinventory Code','Subinventory Code','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_INV_ONHAND_QUANTITY_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - Onhand Quantity - WC',401,'UNIT_OF_MEASURE','Unit Of Measure','Unit Of Measure','','','default','','11','N','','','','','','','','MR020532','N','N','','EIS_INV_ONHAND_QUANTITY_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - Onhand Quantity - WC',401,'MAX_MINMAX_QUANTITY','Max Qty','Max Minmax Quantity','','~~~','default','','7','N','','','','','','','','MR020532','N','N','','EIS_INV_ONHAND_QUANTITY_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - Onhand Quantity - WC',401,'MIN_MINMAX_QUANTITY','Min Qty','Min Minmax Quantity','','~~~','default','','6','N','','','','','','','','MR020532','N','N','','EIS_INV_ONHAND_QUANTITY_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - Onhand Quantity - WC',401,'STOCK-Y/N','Stock-Y/N','Min Minmax Quantity','CHAR','','','0','9','Y','','','','','','','case when EIOQV.SALES_VELOCITY IN (''N'',''Z'') Then ''N'' ELSE ''Y'' END','MR020532','N','N','','EIS_INV_ONHAND_QUANTITY_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - Onhand Quantity - WC',401,'DESCRIPTION','Description','Description','','','','','5','N','','','','','','','','MR020532','N','N','','EIS_INV_ONHAND_QUANTITY_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - Onhand Quantity - WC',401,'OPEN_DEMAND','Open Demand','Open Demand','','','','','10','N','','','','','','','','MR020532','N','N','','EIS_INV_ONHAND_QUANTITY_V','','');
--Inserting Report Parameters - Inventory - Onhand Quantity - WC
xxeis.eis_rs_ins.rp( 'Inventory - Onhand Quantity - WC',401,'Organization','Inventory Organization','ORGANIZATION_CODE','IN','EIS_INV_INVENTORY_ORGS_LOV','','VARCHAR2','Y','Y','1','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory - Onhand Quantity - WC',401,'Inventory Item','Inventory Item','ITEM','IN','EIS_INV_ITEM_LOV','','VARCHAR2','N','Y','2','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory - Onhand Quantity - WC',401,'Cost Group','Cost Group','COST_GROUP','IN','EIS_INV_COST_GROUP_LOV','','VARCHAR2','N','Y','4','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory - Onhand Quantity - WC',401,'Subinventory','Subinventory','SUBINVENTORY_CODE','IN','EIS_INV_SUBINVENTORY_LOV','','VARCHAR2','N','Y','3','','Y','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - Inventory - Onhand Quantity - WC
xxeis.eis_rs_ins.rcn( 'Inventory - Onhand Quantity - WC',401,'COST_GROUP','IN',':Cost Group','','','Y','4','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Inventory - Onhand Quantity - WC',401,'SUBINVENTORY_CODE','IN',':Subinventory','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Inventory - Onhand Quantity - WC',401,'ITEM','IN',':Inventory Item','','','Y','2','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Inventory - Onhand Quantity - WC',401,'','','','','and ( ''All Organizations'' in (:Organization) or (ORGANIZATION_CODE in (:Organization)))','Y','0','','MR020532');
--Inserting Report Sorts - Inventory - Onhand Quantity - WC
--Inserting Report Triggers - Inventory - Onhand Quantity - WC
--Inserting Report Templates - Inventory - Onhand Quantity - WC
xxeis.eis_rs_ins.R_Tem( 'Inventory - Onhand Quantity - WC','Inventory - Onhand Quantity_wc','Seeded template for Inventory - Onhand Quantity_wc','','','','','','','','','','','Inventory - Onhand Quantity_wc.rtf','MR020532');
--Inserting Report Portals - Inventory - Onhand Quantity - WC
--Inserting Report Dashboards - Inventory - Onhand Quantity - WC
xxeis.eis_rs_ins.r_dash( 'Inventory - Onhand Quantity - WC','Dynamic 701','Dynamic 701','vertical percent bar','large','Cost Group','Cost Group','Cost Group','Cost Group','Count','MR020532');
xxeis.eis_rs_ins.r_dash( 'Inventory - Onhand Quantity - WC','Dynamic 702','Dynamic 702','pie','large','Unit Number','Unit Number','Subinventory Code','Subinventory Code','Count','MR020532');
xxeis.eis_rs_ins.r_dash( 'Inventory - Onhand Quantity - WC','Dynamic 703','Dynamic 703','horizontal stacked bar','large','Unit Number','Unit Number','Subinventory Code','Subinventory Code','Count','MR020532');
xxeis.eis_rs_ins.r_dash( 'Inventory - Onhand Quantity - WC','Dynamic 704','Dynamic 704','vertical stacked bar','large','Unit Number','Unit Number','Subinventory Code','Subinventory Code','Count','MR020532');
xxeis.eis_rs_ins.r_dash( 'Inventory - Onhand Quantity - WC','Dynamic 705','Dynamic 705','vertical stacked bar','large','Subinventory Code','Subinventory Code','On Hand','On Hand','Count','MR020532');
xxeis.eis_rs_ins.r_dash( 'Inventory - Onhand Quantity - WC','Dynamic 706','Dynamic 706','vertical stacked bar','large','Subinventory Code','Subinventory Code','On Hand','On Hand','Sum','MR020532');
xxeis.eis_rs_ins.r_dash( 'Inventory - Onhand Quantity - WC','Dynamic 707','Dynamic 707','vertical stacked bar','large','Subinventory Code','Subinventory Code','Item','Item','Count','MR020532');
--Inserting Report Security - Inventory - Onhand Quantity - WC
xxeis.eis_rs_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','50865',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','50862',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','50864',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','50849',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory - Onhand Quantity - WC','660','','50871',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','50884',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','50855',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','50981',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','50882',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','50883',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','20634',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory - Onhand Quantity - WC','20005','','50900',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','50895',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','50619',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','50851',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory - Onhand Quantity - WC','707','','51104',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory - Onhand Quantity - WC','401','','50990',401,'MR020532','','');
--Inserting Report Pivots - Inventory - Onhand Quantity - WC
xxeis.eis_rs_ins.rpivot( 'Inventory - Onhand Quantity - WC',401,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Inventory - Onhand Quantity - WC',401,'Pivot','INV_ORG_NAME','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Inventory - Onhand Quantity - WC',401,'Pivot','ITEM','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Inventory - Onhand Quantity - WC',401,'Pivot','ON_HAND','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Inventory - Onhand Quantity - WC',401,'Pivot','ORGANIZATION_CODE','PAGE_FIELD','','','4','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Inventory - Onhand Quantity - WC',401,'Pivot','SUBINVENTORY_CODE','PAGE_FIELD','','','3','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
