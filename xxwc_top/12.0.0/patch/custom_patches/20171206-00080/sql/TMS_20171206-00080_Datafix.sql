/***********************************************************************************************************************************************
   NAME:       TMS_20171206-00080_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        02/13/2018  Rakesh Patel     TMS#20171206-00080
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Delete');
   
   DELETE FROM xxwc.xxwc_oe_open_order_lines a
   where 1=1
   and not exists (select 1 from oe_order_headers_all where header_id=a.header_id);

   DBMS_OUTPUT.put_line ('Records deleted -' || SQL%ROWCOUNT);
   COMMIT;
   
  EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to detele record ' || SQLERRM);
	  ROLLBACK;
END;
/