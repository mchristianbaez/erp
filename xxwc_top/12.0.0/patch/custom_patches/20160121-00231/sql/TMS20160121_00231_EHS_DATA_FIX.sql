/*******************************************************************************************************************************************************
  $Header TMS20160121-00231_EHS_DATA_FIX.sql $
  Module Name TMS#20150717-00041 - 2015 PLM - EHS Attribute Related changes - Org Attributes Moving to Master UDA

  PURPOSE Data fix to move Hazmat attributes from Inventory to PDH. 

  REVISIONS
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        19-Jan-2016  P.Vamshidhar          TMS#20160121-00231 - EHS Data Conversion DFF to UDA's

*******************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 100000

begin mo_global.set_policy_context('S',162); end;
/
begin
fnd_global.APPS_INITIALIZE (user_id        => 28474,
                            resp_id        => 51829,
                            resp_appl_id   => 431);

end;
/
/* Formatted on 1/28/2016 12:49:04 PM (QP5 v5.265.14096.38000) */
DECLARE
   CURSOR cur_haz_item
   IS
      SELECT inventory_item_id,
             organization_id,
             attribute1,                                           --CA Prop65
             SUBSTR (attribute3, 1, 1) attribute3,         --CA Pesticide Flag
             attribute4,                                             --VOC G/L
             attribute5,                                     --Pesticide State
             attribute6,                                        --VOC Category
             attribute7,                                    --VOC Sub Category
             attribute8,                                               --MSDS#
             attribute9,                                     --Packaging Group
             attribute11,                                          --ORMD Flag
             attribute17,                                 --Hazmat Description
             attribute18                                     --Contractor Type
        FROM APPS.MTL_SYSTEM_ITEMS_B
       WHERE     HAZARDOUS_MATERIAL_FLAG = 'Y'
             AND (   ATTRIBUTE1 IS NOT NULL
                  OR ATTRIBUTE3 IS NOT NULL
                  OR ATTRIBUTE4 IS NOT NULL
                  OR ATTRIBUTE5 IS NOT NULL
                  OR ATTRIBUTE6 IS NOT NULL
                  OR ATTRIBUTE7 IS NOT NULL
                  OR ATTRIBUTE8 IS NOT NULL
                  OR ATTRIBUTE9 IS NOT NULL
                  OR ATTRIBUTE11 IS NOT NULL
                  OR ATTRIBUTE17 IS NOT NULL
                  OR ATTRIBUTE18 IS NOT NULL)
             AND ORGANIZATION_ID = 222
             AND ATTRIBUTE_CATEGORY = 'WC';


   l_attr_group_name      EGO_ATTR_GROUPS_V.attr_group_name%TYPE;
   l_attr_group_id        EGO_ATTR_GROUPS_V.attr_group_id%TYPE;
   l_appl_id              EGO_ATTR_GROUPS_V.application_id%TYPE;

   TYPE item_uda_rec_type IS RECORD
   (
      attr_grp_name     VARCHAR2 (30),
      attr_name         VARCHAR2 (30),
      attr_value_str    VARCHAR2 (155),
      attr_value_num    NUMBER,
      attr_value_date   DATE
   );

   TYPE item_uda_tbl_type IS TABLE OF item_uda_rec_type
      INDEX BY PLS_INTEGER;

   l_attr_grp_tbl         item_uda_tbl_type;

   TYPE attr_group_tbl IS TABLE OF apps.EGO_ITM_USR_ATTR_INTRFC%ROWTYPE
      INDEX BY PLS_INTEGER;

   l_row_identifier       NUMBER;
   l_attr_row_index       NUMBER := 1;
   l_attr_data_index      NUMBER := 1;

   l_attr_row_table       EGO_USER_ATTR_ROW_TABLE;
   l_attr_data_table      EGO_USER_ATTR_DATA_TABLE;
   x_failed_row_id_list   VARCHAR2 (1000);
   x_return_status        VARCHAR2 (100);
   x_msg_count            NUMBER;
   x_msg_data             VARCHAR2 (10000);
   x_error_code           NUMBER;
   ln_success_count       NUMBER := 0;
   ln_failed_count        NUMBER := 0;
   ln_records_processed   NUMBER := 0;
   l_error_message_list   Error_handler.error_tbl_type;
   l_err_message          VARCHAR2 (2000);
   l_err_msg              VARCHAR2 (2000);
BEGIN
   fnd_msg_pub.delete_msg;
   l_attr_data_table := EGO_USER_ATTR_DATA_TABLE ();
   l_attr_row_table := EGO_USER_ATTR_ROW_TABLE ();


   SELECT UPPER (attr_group_name), attr_group_id, application_id
     INTO l_attr_group_name, l_attr_group_id, l_appl_id
     FROM EGO_ATTR_GROUPS_V
    WHERE attr_group_name = 'XXWC_EHS_AG';

   FOR rec_haz_item IN cur_haz_item
   LOOP
      fnd_msg_pub.delete_msg;
      ln_records_processed := NVL (ln_records_processed, 0) + 1;

      l_attr_row_table.EXTEND;
      l_attr_row_index := l_attr_row_table.COUNT;
      l_row_identifier := l_attr_row_index;
      l_attr_row_table (l_attr_row_index) :=
         ego_user_attr_row_obj (l_row_identifier,
                                l_attr_group_id,
                                l_appl_id,
                                'EGO_ITEMMGMT_GROUP',
                                l_attr_group_name,
                                'ITEM_LEVEL',
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                NULL,
                                'SYNC');
      -- CA Prop65
      l_attr_data_table.EXTEND;
      l_attr_data_index := l_attr_data_table.COUNT;
      l_attr_data_table (l_attr_data_index) :=
         ego_user_attr_data_obj (l_row_identifier,
                                 'XXWC_CA_PROP65_ATTR',
                                 rec_haz_item.attribute1,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 l_row_identifier);
      -- Pesticide Flag
      l_attr_data_table.EXTEND;
      l_attr_data_index := l_attr_data_table.COUNT;
      l_attr_data_table (l_attr_data_index) :=
         ego_user_attr_data_obj (l_row_identifier,
                                 'XXWC_PESTICIDE_FLG_ATTR',
                                 rec_haz_item.attribute3,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 l_row_identifier);

      -- Limited Quantity Flag
      l_attr_data_table.EXTEND;
      l_attr_data_index := l_attr_data_table.COUNT;
      l_attr_data_table (l_attr_data_index) :=
         ego_user_attr_data_obj (l_row_identifier,
                                 'XXWC_LTD_QTY_ATTR',
                                 rec_haz_item.attribute11,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 l_row_identifier);

      -- Pesticide Flag State
      l_attr_data_table.EXTEND;
      l_attr_data_index := l_attr_data_table.COUNT;
      l_attr_data_table (l_attr_data_index) :=
         ego_user_attr_data_obj (l_row_identifier,
                                 'XXWC_PESTICIDE_FLG_ST_ATTR',
                                 rec_haz_item.attribute5,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 l_row_identifier);

      -- VOC Content
      l_attr_data_table.EXTEND;
      l_attr_data_index := l_attr_data_table.COUNT;
      l_attr_data_table (l_attr_data_index) :=
         ego_user_attr_data_obj (l_row_identifier,
                                 'XXWC_VOC_CONTENT_ATTR',
                                 rec_haz_item.attribute4,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 l_row_identifier);

      --  VOC Category
      l_attr_data_table.EXTEND;
      l_attr_data_index := l_attr_data_table.COUNT;
      l_attr_data_table (l_attr_data_index) :=
         ego_user_attr_data_obj (l_row_identifier,
                                 'XXWC_VOC_CAT_ATTR',
                                 rec_haz_item.attribute6,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 l_row_identifier);
      -- VOC Sub-Category
      l_attr_data_table.EXTEND;
      l_attr_data_index := l_attr_data_table.COUNT;
      l_attr_data_table (l_attr_data_index) :=
         ego_user_attr_data_obj (l_row_identifier,
                                 'XXWC_VOC_SUB_CAT_ATTR',
                                 rec_haz_item.attribute7,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 l_row_identifier);

      -- MSDS #
      l_attr_data_table.EXTEND;
      l_attr_data_index := l_attr_data_table.COUNT;
      l_attr_data_table (l_attr_data_index) :=
         ego_user_attr_data_obj (l_row_identifier,
                                 'XXWC_MSDS_No_ATTR',
                                 rec_haz_item.attribute8,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 l_row_identifier);

      -- Packaging Group
      l_attr_data_table.EXTEND;
      l_attr_data_index := l_attr_data_table.COUNT;
      l_attr_data_table (l_attr_data_index) :=
         ego_user_attr_data_obj (l_row_identifier,
                                 'XXWC_PKG_GROUP_ATTR',
                                 rec_haz_item.attribute9,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 l_row_identifier);



      -- Hazmat Description
      l_attr_data_table.EXTEND;
      l_attr_data_index := l_attr_data_table.COUNT;
      l_attr_data_table (l_attr_data_index) :=
         ego_user_attr_data_obj (l_row_identifier,
                                 'XXWC_DOT_S_N_ATTR',
                                 rec_haz_item.attribute17,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 l_row_identifier);
      -- Contrainer Type
      l_attr_data_table.EXTEND;
      l_attr_data_index := l_attr_data_table.COUNT;
      l_attr_data_table (l_attr_data_index) :=
         ego_user_attr_data_obj (l_row_identifier,
                                 'XXWC_CONTAINER_TYPE_ATTR',
                                 rec_haz_item.attribute18,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 l_row_identifier);


      --print_uda_data(l_attr_row_table,l_attr_data_table);
      EGO_ITEM_PUB.PROCESS_USER_ATTRS_FOR_ITEM (
         P_API_VERSION             => '1.0',
         P_INVENTORY_ITEM_ID       => rec_haz_item.inventory_item_id,
         P_ORGANIZATION_ID         => rec_haz_item.organization_id,
         P_ATTRIBUTES_ROW_TABLE    => l_attr_row_table,
         P_ATTRIBUTES_DATA_TABLE   => l_attr_data_table,
         X_FAILED_ROW_ID_LIST      => x_failed_row_id_list,
         X_RETURN_STATUS           => x_return_status,
         X_ERRORCODE               => x_error_code,
         X_MSG_COUNT               => x_msg_count,
         X_MSG_DATA                => x_msg_data);

      --DBMS_OUTPUT.PUT_LINE ('x_return_status' || x_return_status);

      IF x_return_status = 'S'
      THEN
         ln_success_count := NVL (ln_success_count, 0) + 1;

         UPDATE MTL_SYSTEM_ITEMS_B
            SET ATTRIBUTE1 = NULL,
                ATTRIBUTE3 = NULL,
                ATTRIBUTE4 = NULL,
                ATTRIBUTE5 = NULL,
                ATTRIBUTE6 = NULL,
                ATTRIBUTE7 = NULL,
                ATTRIBUTE8 = NULL,
                ATTRIBUTE9 = NULL,
                ATTRIBUTE11 = NULL,
                ATTRIBUTE17 = NULL,
                ATTRIBUTE18 = NULL,
                LAST_UPDATE_DATE = SYSDATE,
                LAST_UPDATED_BY = 28474
          WHERE     inventory_item_id = rec_haz_item.inventory_item_id
                AND ATTRIBUTE_CATEGORY = 'WC'
                AND HAZARDOUS_MATERIAL_FLAG = 'Y'
                AND (   ATTRIBUTE1 IS NOT NULL
                     OR ATTRIBUTE3 IS NOT NULL
                     OR ATTRIBUTE4 IS NOT NULL
                     OR ATTRIBUTE5 IS NOT NULL
                     OR ATTRIBUTE6 IS NOT NULL
                     OR ATTRIBUTE7 IS NOT NULL
                     OR ATTRIBUTE8 IS NOT NULL
                     OR ATTRIBUTE9 IS NOT NULL
                     OR ATTRIBUTE11 IS NOT NULL
                     OR ATTRIBUTE17 IS NOT NULL
                     OR ATTRIBUTE18 IS NOT NULL);

         COMMIT;
      ELSE
         Error_Handler.Get_message_list (l_error_message_list);
         l_err_message := NULL;
         l_err_msg := NULL;

         FOR i IN 1 .. l_error_message_list.COUNT
         LOOP
            l_err_msg := NULL;
            l_err_msg := l_error_message_list (i).MESSAGE_TEXT;
            l_err_message := l_err_message || l_err_msg;
         END LOOP;

         ln_failed_count := NVL (ln_failed_count, 0) + 1;
         DBMS_OUTPUT.PUT_LINE (
               'Failed Reason : '
            || x_msg_data
            || ' Message count '
            || x_msg_count);
         DBMS_OUTPUT.PUT_LINE (l_err_message);
      END IF;

      l_attr_row_table.delete;
      l_attr_data_table.delete;
   END LOOP;

/*
   UPDATE MTL_SYSTEM_ITEMS_B
      SET ATTRIBUTE1 = NULL,
          ATTRIBUTE3 = NULL,
          ATTRIBUTE4 = NULL,
          ATTRIBUTE5 = NULL,
          ATTRIBUTE6 = NULL,
          ATTRIBUTE7 = NULL,
          ATTRIBUTE8 = NULL,
          ATTRIBUTE9 = NULL,
          ATTRIBUTE11 = NULL,
          ATTRIBUTE17 = NULL,
          ATTRIBUTE18 = NULL,
          LAST_UPDATE_DATE = SYSDATE,
          LAST_UPDATED_BY = 28474
    WHERE            -- inventory_item_id = rec_haz_item.inventory_item_id AND
         ATTRIBUTE_CATEGORY = 'WC'
          AND HAZARDOUS_MATERIAL_FLAG = 'Y'
          AND (   ATTRIBUTE1 IS NOT NULL
               OR ATTRIBUTE3 IS NOT NULL
               OR ATTRIBUTE4 IS NOT NULL
               OR ATTRIBUTE5 IS NOT NULL
               OR ATTRIBUTE6 IS NOT NULL
               OR ATTRIBUTE7 IS NOT NULL
               OR ATTRIBUTE8 IS NOT NULL
               OR ATTRIBUTE9 IS NOT NULL
               OR ATTRIBUTE11 IS NOT NULL
               OR ATTRIBUTE17 IS NOT NULL
               OR ATTRIBUTE18 IS NOT NULL);

   DBMS_OUTPUT.PUT_LINE ('Rows updated ' || SQL%ROWCOUNT);

   COMMIT;
*/

   DBMS_OUTPUT.PUT_LINE (
      'Successfully Processed Records    : ' || ln_success_count);
   DBMS_OUTPUT.PUT_LINE (
      'Failed Records                    : ' || ln_failed_count);
   DBMS_OUTPUT.PUT_LINE (
      'Total Number of Records Processed :' || ln_records_processed);
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error occured ' || SUBSTR (SQLERRM, 1, 400));
END;
/