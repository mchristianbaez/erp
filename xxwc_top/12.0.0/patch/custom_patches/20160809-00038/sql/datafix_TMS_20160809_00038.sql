/*************************************************************************
  $Header TMS_20160809-00038_AWAITING_INVOICE_TO_CLOSE.sql $
  Module Name: TMS_20160809-00038  Data Fix script for I675908

  PURPOSE: Data fix script for I675908--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        30-AUG-2016  Niraj K Ranjan         TMS#20160809-00038 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160809-00038    , Before Update');

update apps.oe_order_lines_all
set FLOW_STATUS_CODE='CANCELLED',CANCELLED_FLAG='Y'
where line_id =76504755;

   DBMS_OUTPUT.put_line (
         'TMS: 20160809-00038  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160809-00038    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160809-00038 , Errors : ' || SQLERRM);
END;
/
