/* Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        11-Jan-2015  Kishorebabu V    TMS#20151118-00135 @SF - Datafix script to change Branch Number
                                            Initial Version   */

SET SERVEROUTPUT ON SIZE 100000;

BEGIN
UPDATE xxwc.xxwc_om_contract_pricing_hdr
SET    organization_id = 259
WHERE  agreement_id    IN (22081,22082)
AND    organization_id = 257;
--2 row expected to be updated
COMMIT;
EXCEPTION
  WHEN others THEN
    dbms_output.put_line('Error occured while updating Organization Id: '||SQLERRM);
END;
/