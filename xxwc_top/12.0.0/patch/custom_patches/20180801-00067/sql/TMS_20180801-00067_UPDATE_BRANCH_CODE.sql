/*************************************************************************
  $Header TMS_20180801-00067_UPDATE_BRANCH_CODE.sql $
  Module Name: TMS_20180801-00067_UPDATE_BRANCH_CODE.sql

  PURPOSE:   Need to update the Branch code details for batch source_id 1012

  REVISIONS:
  Ver        Date        Author               Description
  ---------  ----------  ---------------      -------------------------
  1.0        08/01/2018  Pattabhi Avula       TMS#20180801-00067
  **************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
CURSOR cur IS
SELECT INTERFACE_LINE_ATTRIBUTE1 trx_number, 
       UPPER(attribute2) warehouse
  FROM xxwc.xxwc_ar_invoices_conv;

l_rec_cnt NUMBER := 0;

BEGIN
 DBMS_OUTPUT.put_line ('TMS: Datafix script    , Before Update');
  FOR rec IN cur LOOP

    UPDATE apps.ra_customer_trx_all      rcta
       SET interface_header_attribute7 = rec.warehouse
     WHERE trx_number                  = rec.trx_number
       AND batch_source_id             = 1012;

    l_rec_cnt := l_rec_cnt + 1;

    IF l_rec_cnt > 5000 THEN
      DBMS_OUTPUT.put_line ('Records updated - ' || SQL%ROWCOUNT);
      COMMIT;
      l_rec_cnt := 0;
    END IF;

  END LOOP;
  
  COMMIT;
EXCEPTION
 WHEN OTHERS THEN
 DBMS_OUTPUT.put_line ('TMS: 20180801-00067 , Errors : ' || SQLERRM);
END;
/