------------------------------------------------------------------------------------
/******************************************************************************
   NAME       :  XXEIS.EIS_XXWC_OM_ORDER_EXP_V
   PURPOSE    :  Replaced the old view definition with new view.This view is created based on 
				 custom table XXEIS.EIS_XXWC_ORDER_EXP_TAB and this custom table data
				 gets populated by XXEIS.EIS_XXWC_OM_ORDER_EXP_PKG package.
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
    1.0        NA    			NA		            Initial Version
	1.1  		11/05/2015	Mahender Reddy			TMS#20150929-00066
	1.2  		12/23/2015	Mahender Reddy			TMS#20151209-00102 
	1.3		   06-May-2016   Siva  			        TMS#20160503-00089,20160601-00140  Performance Tuning                                         
	1.4	  	   17-Feb-2017	 Siva					TMS#20161028-00098 	
******************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_OM_ORDER_EXP_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_OM_ORDER_EXP_V (PROCESS_ID, LOC, EXCEPTION_RESAON, ORDER_NUMBER, ORDER_DATE, EXCEPTION_DATE, CUSTOMER_NAME, ORDER_TYPE, EXT_ORDER_TOTAL, QTY, EXCEPTION_DESCRIPTION, SALES_PERSON_NAME, CREATED_BY, SHIP_METHOD, REQUEST_DATE)
AS
  SELECT process_id,
    LOC,
    EXCEPTION_RESAON,
    ORDER_NUMBER,
    ORDER_DATE,
    EXCEPTION_DATE,
    CUSTOMER_NAME,
    ORDER_TYPE,
    EXT_ORDER_TOTAL,
    QTY,
    EXCEPTION_DESCRIPTION,
    SALES_PERSON_NAME,
    CREATED_BY,
    SHIP_METHOD
	,REQUEST_DATE --added for version 1.4
  FROM XXEIS.EIS_XXWC_ORDER_EXP_TAB
/
