create or replace 
PACKAGE BODY      XXWC_EGO_PUB_WCS_PKG
/*****************************************************************************************************************************************************************************
File Name: XXWC_EGO_PUB_WCS_PKG
PROGRAM TYPE: PL/SQL Package spec and body
HISTORY
PURPOSE: Procedures and functions for creating the WCS extracts
==============================================================================================================================================================================
VERSION DATE          AUTHOR(S)         DESCRIPTION
------- -----------   --------------- ----------------------------------------------------------------------------------------------------------------------------------------
1.0     09/23/2012    Rajiv Rathod      Initial creation of the package
1.1     12/27/2012    Jyotsna Bidkar    Changes done to add Sales Catalog directory
1.2     01/22/2013    Jyotsna Bidkar    Changes to PUB_WCS_PROC for performance
           enhancement
1.2.1 09/12/2013 Rajasekar Gunasekaran Updated for the below requirements
                                          1. Add Keywords attr to 'wc_catalogentries.csv'
                                          2. Remove 'ListPrice' and 'Price' attributes from interface file 'wc_catalogentries.csv'
                                          3. Add Sequence attr to 'wc_catalogentries.csv'
                                          4. Change mapping of 'Name' attribute to Category Description in 'wc_cataloggroups.csv'

1.2.2 09/13/2013 Rajasekar Gunasekaran Updated for adding more debug statements
1.2.3 09/20/2013 Rajasekar Gunasekaran Updated the cat hierarchy proc to generate the full refresh instead of delta
1.2.4 11/06/2013 Rajiv Rathod          Updated by Rajiv to incorporate the force flush
1.2.5   11/26/2013  Praveen Pawar      Modified the cursor query to improve the performance
1.2.6   11/26/2013  Praveen Pawar      Modified the SQL to include items assigned with 'Website Item' catalog
                                       having category value as 'C'.
1.2.7   11/27/2013  Praveen Pawar      Comment out the code which used to write files to Sales Catalog Directory
1.2.8   12/31/2013  Praveen Pawar      Comment out the change to Publish WhiteCapPublic catalog category as per ver 1.2.6
                                       Publish WhiteCapPublic attribute insert and delete records as per changed requirement
1.2.9   02/07/2014  Praveen Pawar      TMS# 20131203-00097 Changed the value identifier column value in CatalogEntryAttributeDictionaryAttributeRelationship.csv file
1.3     03/26/2014  Praveen Pawar      TMS# 20140327-00097
                                       Modified procedure CREATE_ITEM_ATTR_DATA to
                                       print the Item ID and attribute information in ascending order and
                                       Delete flag in descending order in CatalogEntryAttributeDictionaryAttributeRelationship.csv file.
1.4     07/17/2014  Praveen Pawar      Remove the filter on Inventory Item Status as Active and allow program to Publish all SKUs.
                                       TMS ticket # 20140610-00051
1.4.1   10/09/2014  Rajiv Rathod       Change the delete flag based and made it independent of Item status
                                       TMS ticket # 20140610-00051
1.5     03/10/2015  Vamshidhar         TMS#20150309-00325 -  To improve performance rewrote query in PUB_WCS_PROC procedure and details are in procedure.
                                       To imporove performance used 'XXWC_QP_ECOMMERCE_ITEMS_MV' materilized view
                                       Changes implemented in PUB_WCS_PROC procedure.
1.6     05/06/2015  P.Vamshidhar       TMS#20150501-00013 - PDH to Ecomm Data Interface Improvements
                                       To generate separate files for delete and insert/update.
                                       Removed unwanted code,
 *********************************************************************************************************************************************************************/
AS
   L_FILE                       UTL_FILE.FILE_TYPE;
   --L_SALES_FILE                 UTL_FILE.FILE_TYPE;   -- added on 12/27/2012 by Jyotsna -- Commented out w.r.t. ver 1.2.7 on 11/27/2013
   L_FILE_NAME                  VARCHAR2 (200) := NULL;
   L_FILE_NAME_I                VARCHAR2 (200) := NULL; -- Added on 06-May-2015 by Vamshi in 1.6v - TMS#20150501-00013
   L_FILE_NAME_D                VARCHAR2 (200) := NULL; -- Added on 06-May-2015 by Vamshi in 1.6v - TMS#20150501-00013
   L_FILE_I                     UTL_FILE.FILE_TYPE; -- Added on 06-May-2015 by Vamshi in 1.6v - TMS#20150501-00013
   L_FILE_D                     UTL_FILE.FILE_TYPE; -- Added on 06-May-2015 by Vamshi in 1.6v - TMS#20150501-00013
   MAX_LINE_LENGTH              BINARY_INTEGER := 32767;
   L_STRING                     VARCHAR2 (3000) := NULL;
   L_DEL                        VARCHAR2 (1) := '|';
   C_LOCAL_DIRECTORY   CONSTANT VARCHAR2 (25) := 'XXWC_WCS_CATALOG_DIR';
   C_SALES_DIRECTORY   CONSTANT VARCHAR2 (25) := 'XXWC_WCS_SALESCATALOG_DIR'; -- added on 12/27/2012 by Jyotsna
   L_TIMESTAMP                  VARCHAR2 (100) := NULL;
   L_CONC_PRGM_ID               NUMBER;
   L_REQUEST_ID                 NUMBER;
   L_LAST_RUN_DATE              DATE;
   i                            NUMBER := 1;
   j                            NUMBER := 1;
   l_out                        VARCHAR2 (2000);
   l_allowed_values             VARCHAR2 (2000);

   l_master_org_id              NUMBER;
   l_buffer_count               NUMBER := 0; -- added by rajiv  in version 1.2.4 to fix the buffer issue on 6-NOV-2013

   -- Added w.r.t. ver 1.2.8
   /********************************************************************************
   PROGRAM TYPE: PROCEDURE
   NAME: sku_pub_hist_proc
   PURPOSE: Procedure to maintain Website Item Publish history
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     01/10/2014    Praveen Pawar   Initial creation of the procedure
    ********************************************************************************/
   PROCEDURE sku_pub_hist_proc (errbuf       OUT NOCOPY VARCHAR2,
                                retcode      OUT NOCOPY VARCHAR2)
   IS
      lv_error_msg   VARCHAR2 (4000) := NULL;
   BEGIN
      retcode := 0;

      BEGIN
         DELETE FROM XXWC.XXWC_EGO_SKU_PUBLISH_HISTORY XES
               WHERE XES.INVENTORY_ITEM_ID NOT IN (SELECT MIC.INVENTORY_ITEM_ID
                                                     FROM MTL_ITEM_CATEGORIES MIC,
                                                          MTL_CATEGORY_SETS MCS
                                                    WHERE     MCS.CATEGORY_SET_NAME =
                                                                 'Website Item'
                                                          AND MCS.CATEGORY_SET_ID =
                                                                 MIC.CATEGORY_SET_ID
                                                          AND MIC.ORGANIZATION_ID =
                                                                 TO_NUMBER (
                                                                    APPS.fnd_profile.VALUE (
                                                                       'XXWC_ITEM_MASTER_ORG')));

         fnd_file.put_line (
            fnd_file.LOG,
               ' Number of records deleted from XXWC_EGO_SKU_PUBLISH_HISTORY table: '
            || SQL%ROWCOUNT);
      EXCEPTION
         WHEN OTHERS
         THEN
            lv_error_msg := SQLERRM;
            fnd_file.put_line (
               fnd_file.LOG,
                  ' Error while performing merge operation. Error message: '
               || lv_error_msg);
            retcode := 2;
      END;

      BEGIN
         INSERT INTO XXWC.XXWC_EGO_SKU_PUBLISH_HISTORY (INVENTORY_ITEM_ID,
                                                        ORGANIZATION_ID,
                                                        CATEGORY_SET_ID,
                                                        CATEGORY_ID,
                                                        PUBLISH_DATE,
                                                        REQUEST_ID)
            (SELECT MIC.INVENTORY_ITEM_ID INVENTORY_ITEM_ID,
                    MIC.ORGANIZATION_ID ORGANIZATION_ID,
                    MIC.CATEGORY_SET_ID CATEGORY_SET_ID,
                    MIC.CATEGORY_ID CATEGORY_ID,
                    SYSDATE PUBLISH_DATE,
                    MIC.REQUEST_ID REQUEST_ID
               FROM MTL_ITEM_CATEGORIES MIC, MTL_CATEGORY_SETS MCS
              WHERE     MIC.CATEGORY_SET_ID = MCS.CATEGORY_SET_ID
                    AND MIC.ORGANIZATION_ID =
                           TO_NUMBER (
                              APPS.fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG'))
                    AND MCS.CATEGORY_SET_NAME = 'Website Item'
                    AND MIC.INVENTORY_ITEM_ID NOT IN (SELECT INVENTORY_ITEM_ID
                                                        FROM XXWC.XXWC_EGO_SKU_PUBLISH_HISTORY));

         fnd_file.put_line (
            fnd_file.LOG,
               ' Number of records inserted in XXWC_EGO_SKU_PUBLISH_HISTORY table:  '
            || SQL%ROWCOUNT);
      EXCEPTION
         WHEN OTHERS
         THEN
            lv_error_msg := SQLERRM;
            fnd_file.put_line (
               fnd_file.LOG,
                  ' Error while performing merge operation. Error message: '
               || lv_error_msg);
            retcode := 2;
      END;

      BEGIN
         MERGE INTO XXWC.XXWC_EGO_SKU_PUBLISH_HISTORY XES
              USING (SELECT MIC.INVENTORY_ITEM_ID,
                            MIC.ORGANIZATION_ID,
                            MIC.CATEGORY_SET_ID,
                            MIC.CATEGORY_ID
                       FROM MTL_ITEM_CATEGORIES MIC,
                            MTL_CATEGORY_SETS MCS,
                            XXWC_EGO_MOD_ITEMS_TAB XEM
                      WHERE     MIC.CATEGORY_SET_ID = MCS.CATEGORY_SET_ID
                            AND MIC.ORGANIZATION_ID =
                                   TO_NUMBER (
                                      APPS.fnd_profile.VALUE (
                                         'XXWC_ITEM_MASTER_ORG'))
                            AND MCS.CATEGORY_SET_NAME = 'Website Item'
                            AND XEM.INVENTORY_ITEM_ID = MIC.INVENTORY_ITEM_ID)
                    MIX
                 ON (    XES.INVENTORY_ITEM_ID = MIX.INVENTORY_ITEM_ID
                     AND XES.ORGANIZATION_ID = MIX.ORGANIZATION_ID
                     AND XES.CATEGORY_SET_ID = MIX.CATEGORY_SET_ID)
         WHEN MATCHED
         THEN
            UPDATE SET
               XES.CATEGORY_ID = MIX.CATEGORY_ID, PUBLISH_DATE = SYSDATE;

         fnd_file.put_line (
            fnd_file.LOG,
               ' Number of records updated in XXWC_EGO_SKU_PUBLISH_HISTORY table:   '
            || SQL%ROWCOUNT);
      EXCEPTION
         WHEN OTHERS
         THEN
            lv_error_msg := SQLERRM;
            fnd_file.put_line (
               fnd_file.LOG,
                  ' Error while performing merge operation. Error message: '
               || lv_error_msg);
            retcode := 2;
      END;

      COMMIT;
   END sku_pub_hist_proc;

   -- End of code modification w.r.t. ver 1.2.8 --

   /********************************************************************************
     PROGRAM TYPE: FUNCTION
  NAME: publish_ready
  PURPOSE: Function to determine the eligilibilty of publish
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/23/2012    Rajiv Rathod    Initial creation of the function
   ********************************************************************************/
   FUNCTION publish_ready (p_item_id IN NUMBER)
      RETURN NUMBER
   IS
      l_web_hier       VARCHAR2 (200) := NULL;
      l_website_item   VARCHAR2 (10) := NULL;
      l_price_item     NUMBER := NULL;
   BEGIN
      BEGIN
         SELECT mcb.segment1
           INTO l_web_hier
           FROM MTL_CATEGORY_SETS MCS,
                MTL_ITEM_CATEGORIES MIC,
                MTL_CATEGORIES_B MCB,
                MTL_SYSTEM_ITEMS_B msib
          WHERE     MCS.CATEGORY_SET_NAME = 'WC Web Hierarchy'
                AND MIC.CATEGORY_SET_ID = MCS.CATEGORY_SET_ID
                AND MIC.INVENTORY_ITEM_ID = msib.inventory_item_id
                AND MIC.ORGANIZATION_ID = MSIB.ORGANIZATION_ID
                AND mcb.CATEGORY_ID = MIC.CATEGORY_ID
                AND MSIB.ORGANIZATION_ID =
                       TO_NUMBER (fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG'))
                AND msib.inventory_item_id = p_item_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_web_hier := NULL;
      END;

      BEGIN
         SELECT mcb.segment1
           INTO l_website_item
           FROM MTL_CATEGORY_SETS MCS,
                MTL_ITEM_CATEGORIES MIC,
                MTL_CATEGORIES_B MCB,
                MTL_SYSTEM_ITEMS_B msib
          WHERE     MCS.CATEGORY_SET_NAME = 'Website Item'
                AND MIC.CATEGORY_SET_ID = MCS.CATEGORY_SET_ID
                AND MIC.INVENTORY_ITEM_ID = msib.inventory_item_id
                AND MIC.ORGANIZATION_ID = MSIB.ORGANIZATION_ID
                AND mcb.CATEGORY_ID = MIC.CATEGORY_ID
                AND MSIB.ORGANIZATION_ID = l_master_org_id
                AND msib.inventory_item_id = p_item_id
                AND mcb.segment1 = 'Y';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_website_item := NULL;
      END;

      BEGIN
         SELECT NVL (operand, 0)
           INTO l_price_item
           FROM qp_list_headers_all a, qp_list_lines_v b
          WHERE     NVL (b.END_DATE_ACTIVE, SYSDATE) >= SYSDATE
                AND a.active_flag = 'Y'
                AND a.list_header_id = b.list_header_id
                AND a.name LIKE 'CATEGORY%'
                AND b.product_attr_value = TO_CHAR (p_item_id)
                AND ROWNUM = 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_price_item := 0;
      END;

      IF (    (l_web_hier IS NOT NULL)
          AND (l_website_item = 'Y')
          AND (l_price_item > 0))
      THEN
         RETURN 1;
      ELSE
         RETURN 0;
      END IF;
   END publish_ready;

   /********************************************************************************
                                          PROGRAM TYPE: FUNCTION
   NAME: publish_ready_date
   PURPOSE: Function to determine the eligilibilty of publish based on date
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/23/2012    Rajiv Rathod    Initial creation of the function
    ********************************************************************************/

   FUNCTION publish_ready_date (p_item_id IN NUMBER, p_LAST_RUN_DATE DATE)
      RETURN NUMBER
   IS
      l_web_site_id   NUMBER := 0;
      l_web_hier_id   NUMBER := 0;
      l_return_cat    NUMBER := 0;
   BEGIN
      BEGIN
         SELECT category_set_id
           INTO l_web_site_id
           FROM MTL_CATEGORY_SETS
          WHERE CATEGORY_SET_NAME = 'Website Item';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_web_site_id := NULL;
      END;

      BEGIN
         SELECT category_set_id
           INTO l_web_hier_id
           FROM MTL_CATEGORY_SETS
          WHERE CATEGORY_SET_NAME = 'WC Web Hierarchy';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_web_hier_id := NULL;
      END;

      -- to find if teh item category has been added

      BEGIN
         SELECT COUNT (1)
           INTO l_return_cat
           FROM MTL_ITEM_CATEGORIES
          WHERE     inventory_item_id = p_item_id
                AND organization_id = l_master_org_id
                AND last_update_date >= p_LAST_RUN_DATE
                AND category_set_id IN (l_web_hier_id, l_web_site_id);
      EXCEPTION
         WHEN OTHERS
         THEN
            l_return_cat := 0;
      END;

      IF (l_return_cat > 0)
      THEN
         RETURN 1;
      ELSE
         RETURN 0;
      END IF;
   END publish_ready_date;

   /********************************************************************************
            PROGRAM TYPE: FUNCTION
      NAME: allowed_values
      PURPOSE: Function to provide the values in value set
      HISTORY
      ===============================================================================
      VERSION DATE          AUTHOR(S)       DESCRIPTION
      ------- -----------   --------------- -----------------------------------------
      1.0     09/23/2012    Rajiv Rathod    Initial creation of the function
       ********************************************************************************/
   FUNCTION allowed_values (p_value_set_id IN NUMBER)
      RETURN VARCHAR2
   IS
      CURSOR all_val_cur
      IS
         SELECT flex_value
           FROM fnd_flex_values
          WHERE flex_value_set_id = p_value_set_id;
   BEGIN
      i := 0;
      l_out := NULL;

      FOR all_val_rec IN all_val_cur
      LOOP
         l_out := l_out || L_DEL || all_val_rec.flex_value;
         i := i + 1;

         IF (i >= j)
         THEN
            j := i;
         END IF;
      END LOOP;

      IF (p_value_set_id IS NULL)
      THEN
         FOR k IN 1 .. j
         LOOP
            l_out := l_out || L_DEL;
         END LOOP;
      END IF;

      IF (p_value_set_id IS NOT NULL AND j > i)
      THEN
         i := i + 1;

         FOR k IN i .. j
         LOOP
            l_out := l_out || L_DEL;
         END LOOP;
      END IF;

      RETURN l_out;
   END;

   /********************************************************************************
                                                PROGRAM TYPE: FUNCTION
      NAME: get_flex_value
      PURPOSE: Function to value of value set based on code
      HISTORY
      ===============================================================================
      VERSION DATE          AUTHOR(S)       DESCRIPTION
      ------- -----------   --------------- -----------------------------------------
      1.0     09/23/2012    Rajiv Rathod    Initial creation of the function
       ********************************************************************************/
   FUNCTION get_flex_value (p_value_set_id      IN NUMBER,
                            p_validation_code   IN VARCHAR2,
                            p_value             IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_value_desc     VARCHAR2 (4000) := NULL;
      l_value_col      VARCHAR2 (4000) := NULL;
      l_id_col         VARCHAR2 (4000) := NULL;
      l_table_name     VARCHAR2 (4000) := NULL;
      l_where_clause   VARCHAR2 (4000) := NULL;
   BEGIN
      IF p_value_set_id IS NULL
      THEN
         l_value_desc := p_value;
      ELSE
         IF (p_validation_code IS NULL)
         THEN
            l_value_desc := p_value;
         ELSIF (p_validation_code = 'N')
         THEN
            l_value_desc := p_value;
         ELSIF (p_validation_code = 'I')
         THEN                                           --Independant Valueset
            BEGIN
               SELECT description
                 INTO l_value_desc
                 FROM fnd_flex_values_vl
                WHERE     flex_value_Set_id = p_value_set_id
                      AND flex_value = p_value;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_value_desc := p_value;
            END;
         ELSIF (p_validation_code = 'X')
         THEN                                          --Translatable Valueset
            BEGIN
               SELECT flex_value_meaning
                 INTO l_value_desc
                 FROM fnd_flex_values_vl
                WHERE     flex_value_Set_id = p_value_set_id
                      AND flex_value = p_value;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_value_desc := p_value;
            END;
         ELSIF (p_validation_code = 'F')
         THEN                                        --Tabledependant Valueset
            BEGIN
               SELECT value_column_name,
                      id_column_name,
                      application_table_name,
                      additional_where_clause
                 INTO l_value_col,
                      l_id_col,
                      l_table_name,
                      l_where_clause
                 FROM fnd_flex_validation_tables
                WHERE flex_value_set_id = p_value_set_id;

               EXECUTE IMMEDIATE
                     ' SELECT '
                  || l_value_col
                  || ' FROM '
                  || l_table_name
                  || ' WHERE '
                  || l_where_clause
                  || ' AND '
                  || l_id_col
                  || '= :1'
                  INTO l_value_desc
                  USING p_value;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_value_desc := p_value;
            END;
         END IF;
      END IF;

      RETURN l_value_desc;
   END;

   /********************************************************************************************************************
      PROGRAM TYPE: PROCEDURE
      NAME: CREATE_ITEM_DATA
      PURPOSE: Procedure to generate the Item data
      HISTORY
      ==================================================================================================================
      VERSION DATE          AUTHOR(S)       DESCRIPTION
      ------- -----------   --------------- ----------------------------------------------------------------------------
    1.0     09/23/2012    Rajiv Rathod    Initial creation of the procedure
    1.1     12/12/2012    Jyotsna Bidkar  Changes done to make the delete_flag as Y
                                          if the item is made inactive
                                          (as per mail from Jas)
    1.2     12/27/2012    Jyotsna Bidkar  Changes done to include the Manufacturer
                                          Name in the template
    1.4.1   10/09/2014  Rajiv Rathod      Change the delete flag based and made it independent of Item status
                                          TMS ticket # 20140610-00051

     1.6     05/06/2015  P.Vamshidhar     TMS#20150501-00013 - PDH to Ecomm Data Interface Improvements
                                          To generate separate files for delete and insert/update.
 **********************************************************************************************************************/
   PROCEDURE CREATE_ITEM_DATA (P_TIMESTAMP IN VARCHAR2, L_LAST_RUN_DATE DATE)
   AS
   BEGIN
      L_FILE_NAME_I := 'wc_catalogentries_i.csv'; -- Added this code by Vamshi in 1.6 Version @ TMS#20150501-00013
      L_FILE_NAME_D := 'wc_catalogentries_d.csv'; -- Added this code by Vamshi in 1.6 Version @ TMS#20150501-00013

      -- Commented below code by Vamshi in 1.6 version @ TMS#20150501-00013
      /*L_FILE :=
           UTL_FILE.FOPEN (C_LOCAL_DIRECTORY,
                           L_FILE_NAME,
                           'W',
                           MAX_LINE_LENGTH);*/

      -- Added this code by Vamshi in 1.6 Version @ TMS#20150501-00013 -- Start
      L_FILE_I :=
         UTL_FILE.FOPEN (C_LOCAL_DIRECTORY,
                         L_FILE_NAME_I,
                         'W',
                         MAX_LINE_LENGTH);

      L_FILE_D :=
         UTL_FILE.FOPEN (C_LOCAL_DIRECTORY,
                         L_FILE_NAME_D,
                         'W',
                         MAX_LINE_LENGTH);

      -- Added above code by Vamshi in 1.6 Version @ TMS#20150501-00013 -- End
      --UTL_FILE.PUT_LINE (L_FILE, CHR (15711167)); -- Commented by Vamshi in 1.6 V @ TMS#20150501-00013
      UTL_FILE.PUT_LINE (L_FILE_I, CHR (15711167)); -- Added by Vamshi in 1.6 V @ TMS#20150501-00013
      UTL_FILE.PUT_LINE (L_FILE_D, CHR (15711167)); -- Added by Vamshi in 1.6 V @ TMS#20150501-00013

      L_STRING := 'CatalogEntry';
      --UTL_FILE.PUT_LINE (L_FILE, L_STRING); -- Commented by Vamshi
      UTL_FILE.PUT_LINE (L_FILE_I, L_STRING); -- Added by Vamshi in 1.6V @ TMS#20150501-00013
      UTL_FILE.PUT_LINE (L_FILE_D, L_STRING); -- Added by Vamshi in 1.6V @ TMS#20150501-00013

      --UTL_FILE.PUT_LINE (L_SALES_FILE, L_STRING);  -- Commented out w.r.t. ver 1.2.7 on 11/27/2013

      L_STRING := --<Raj 12-Sep-2013> added one new field 'Keyword' next to 'Buyable' and removed 'ListPrice|Price|' next to CurrencyCode
         'PartNumber|Type|ManufacturerPartNumber|Manufacturer|ParentPartNumber|Sequence|ParentGroupIdentifier|ParentStoreIdentifier|CurrencyCode|Name|ShortDescription|LongDescription|Thumbnail|FullImage|QuantityMeasure|WeightMeasure|Weight|Buyable|Keyword|Delete'; -- Commented w.r.t. ver 1.2.6 on 12/04/2013 -- Uncommented w.r.t. ver 1.2.8 on 12/31/2013
      --'PartNumber|Type|WhiteCapPublic|ManufacturerPartNumber|Manufacturer|ParentPartNumber|Sequence|ParentGroupIdentifier|ParentStoreIdentifier|CurrencyCode|Name|ShortDescription|LongDescription|Thumbnail|FullImage|QuantityMeasure|WeightMeasure|Weight|Buyable|Keyword|Delete';   -- Added w.r.t ver 1.2.6 on 12/04/2013     -- Commented w.r.t. ver 1.2.8 on 12/31/2013

      -- UTL_FILE.PUT_LINE (L_FILE, L_STRING); -- Commented by Vamshi in 1.6V @ TMS#20150501-00013
      UTL_FILE.PUT_LINE (L_FILE_I, L_STRING); -- Added by Vamshi in 1.6 V @ TMS#20150501-00013
      UTL_FILE.PUT_LINE (L_FILE_D, L_STRING); -- Added by Vamshi in 1.6 V @ TMS#20150501-00013

      -- UTL_FILE.PUT_LINE (L_SALES_FILE, L_STRING);  -- Commented out w.r.t. ver 1.2.7 on 11/27/2013

      FOR CUR_DATA
         IN (SELECT msib.segment1 partNumber,
                    -- msib.item_type TYPE,
                    NULL TYPE,
                    --(DECODE(mcb.segment1, 'Y', 'Yes', 'C', 'No', NULL)) WhiteCapPublic, -- Added w.r.t. ver 1.2.6 on 12/04/2013 -- Commented w.r.t. ver 1.2.8 on 12/31/2013
                    (                          --SELECT   attribute_char_value
                     --  FROM   XXWC_EGO_ALL_ATTR_BASE_V
                     -- WHERE   ATTRIBUTE_NAME = 'XXWC_M_P_N_ATTR'
                     --         AND ROWNUM = 1
                     --         AND organization_id =l_master_org_id
                     --         AND inventory_item_id = msib.inventory_item_id
                     SELECT XXWC_M_P_N_ATTR
                       FROM XXWC_WHITE_C_M_AGV
                      WHERE     ROWNUM = 1
                            AND organization_id = l_master_org_id
                            AND inventory_item_id = msib.inventory_item_id)
                       ManufacturerPartNumber,
                    --NULL Manufacturer,
                    -- added on 12/27/2012 by Jyotsna to include the Manufacturer name
                    (                          --SELECT   attribute_char_value
                     --          FROM   XXWC_EGO_ALL_ATTR_BASE_V
                     --         WHERE   ATTRIBUTE_NAME = 'XXWC_BRAND_ATTR'
                     --                 AND ROWNUM = 1
                     --                 AND organization_id =l_master_org_id
                     --                 AND inventory_item_id = msib.inventory_item_id
                     SELECT XXWC_BRAND_ATTR
                       FROM XXWC_WHITE_C_M_AGV
                      WHERE     ROWNUM = 1
                            AND organization_id = l_master_org_id
                            AND inventory_item_id = msib.inventory_item_id)
                       Manufacturer,
                    NULL ParentPartNumber,
                    --<Raj 12-Sep-2013> Extracting the Web Sequence Value
                    (                          --SELECT attribute_number_value
                     --FROM  XXWC_EGO_ALL_ATTR_BASE_V
                     --WHERE inventory_item_id = msib.inventory_item_id
                     --AND organization_id = l_master_org_id
                     --AND attribute_group_name = 'XXWC_WHITE_CAP_MST_AG'
                     --AND attribute_name = 'XXWC_WEB_SEQUENCE_ATTR'
                     --AND ROWNUM = 1
                     SELECT XXWC_WEB_SEQUENCE_ATTR
                       FROM XXWC_WHITE_C_M_AGV
                      WHERE     inventory_item_id = msib.inventory_item_id
                            AND organization_id = l_master_org_id
                            AND ROWNUM = 1)
                       SEQUENCE,
                    --'1' SEQUENCE,
                    xemi.web_hierarchy ParentGroupIdentifier,
                    NULL ParentStoreIdentifier,
                    'USD' CurrencyCode,
                    --<Raj 12-Sep-2013> Commented the ListPrice and Price attributes
                    --NVL (xemi.price, '-1.00') ListPrice,
                    --xemi.price Price,
                    (                          --SELECT   attribute_char_value
                     --  FROM   XXWC_EGO_ALL_ATTR_BASE_V
                     -- WHERE   ATTRIBUTE_NAME = 'XXWC_W_S_D_ATTR'
                     --         AND ROWNUM = 1
                     --         AND organization_id =l_master_org_id
                     --         AND inventory_item_id = msib.INVENTORY_ITEM_ID
                     SELECT XXWC_W_S_D_ATTR
                       FROM XXWC_WHITE_C_M_AGV
                      WHERE     ROWNUM = 1
                            AND organization_id = l_master_org_id
                            AND inventory_item_id = msib.INVENTORY_ITEM_ID)
                       Name,
                    msib.description ShortDescription,
                    (                  --SELECT   ATTRIBUTE_TRANSLATABLE_VALUE
                     --  FROM   XXWC_EGO_ALL_ATTR_LANG_V
                     -- WHERE   ATTRIBUTE_NAME =
                     --            'XXWC_WEB_LONG_DESCRIPTION_ATTR'
                     --         AND ROWNUM = 1
                     --         AND organization_id =l_master_org_id
                     --         AND inventory_item_id = msib.INVENTORY_ITEM_ID
                     SELECT XXWC_WEB_LONG_DESCRIPTION_ATTR
                       FROM XXWC_WHITE_C_M_AGV
                      WHERE     ROWNUM = 1
                            AND organization_id = l_master_org_id
                            AND inventory_item_id = msib.INVENTORY_ITEM_ID)
                       LongDescription,
                       fnd_profile.VALUE ('XXWC_THUMBNAIL_PROD')
                    || (                       --SELECT   attribute_char_value
                        --  FROM   XXWC_EGO_ALL_ATTR_BASE_V
                        -- WHERE   ATTRIBUTE_NAME = 'XXWC_A_T_1_ATTR'
                        --         AND ROWNUM = 1
                        --         AND organization_id =l_master_org_id
                        --         AND inventory_item_id =
                        --               msib.INVENTORY_ITEM_ID
                        SELECT XXWC_A_T_1_ATTR
                          FROM XXWC_WHITE_C_M_AGV
                         WHERE     ROWNUM = 1
                               AND organization_id = l_master_org_id
                               AND inventory_item_id = msib.INVENTORY_ITEM_ID)
                       Thumbnail,
                       fnd_profile.VALUE ('XXWC_FULL_IMAGE_PROD')
                    || (                       --SELECT   attribute_char_value
                        --  FROM   XXWC_EGO_ALL_ATTR_BASE_V
                        -- WHERE   ATTRIBUTE_NAME = 'XXWC_A_F_1_ATTR'
                        --         AND ROWNUM = 1
                        --         AND organization_id =l_master_org_id
                        --         AND inventory_item_id =
                        --               msib.INVENTORY_ITEM_ID
                        SELECT XXWC_A_F_1_ATTR
                          FROM XXWC_WHITE_C_M_AGV
                         WHERE     ROWNUM = 1
                               AND organization_id = l_master_org_id
                               AND inventory_item_id = msib.INVENTORY_ITEM_ID)
                       FullImage,
                    'C62' QuantityMeasure,
                    'LBS' WeightMeasure,
                    unit_weight Weight,
                    '1' Buyable,
                    --<Raj 12-Sep-2013> Extracting the Keyword Value
                    (                    --SELECT attribute_translatable_value
                     --FROM  XXWC_EGO_ALL_ATTR_LANG_V
                     --WHERE inventory_item_id = msib.inventory_item_id
                     --AND organization_id = l_master_org_id
                     --AND attribute_group_name = 'XXWC_WHITE_CAP_MST_AG'
                     --AND attribute_name = 'XXWC_WEB_KEYWORDS_ATTR'
                     --AND ROWNUM = 1
                     SELECT XXWC_WEB_KEYWORDS_ATTR
                       FROM XXWC_WHITE_C_M_AGV
                      WHERE     inventory_item_id = msib.inventory_item_id
                            AND organization_id = l_master_org_id
                            AND ROWNUM = 1)
                       Keyword,
                    /*  DECODE (msib.inventory_item_status_code,
                              'Inactive', '1',
                              '0')
                         Delete_flag      -- added done on 12/12/2012 by Jyotsna */
                    -- commented by rajiv for version 1.4.1 to make delete flag independent of item status
                    --'0' Delete_flag
                    '0' Delete_flag -- Added by Rajiv for version 1.4.1 to make delete flag independent of item status
               FROM MTL_SYSTEM_ITEMS_B MSIB, XXWC.XXWC_EGO_MOD_ITEMS_TAB XEMI
              WHERE     msib.organization_id =
                           TO_NUMBER (
                              fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG'))
                    AND xemi.INVENTORY_ITEM_ID = msib.INVENTORY_ITEM_ID
             UNION
             SELECT msib.segment1 partNumber,
                    NULL TYPE,
                    (SELECT XXWC_M_P_N_ATTR
                       FROM XXWC_WHITE_C_M_AGV
                      WHERE     ROWNUM = 1
                            AND organization_id = l_master_org_id
                            AND inventory_item_id = msib.inventory_item_id)
                       ManufacturerPartNumber,
                    (SELECT XXWC_BRAND_ATTR
                       FROM XXWC_WHITE_C_M_AGV
                      WHERE     ROWNUM = 1
                            AND organization_id = l_master_org_id
                            AND inventory_item_id = msib.inventory_item_id)
                       Manufacturer,
                    NULL ParentPartNumber,
                    (SELECT XXWC_WEB_SEQUENCE_ATTR
                       FROM XXWC_WHITE_C_M_AGV
                      WHERE     inventory_item_id = msib.inventory_item_id
                            AND organization_id = l_master_org_id
                            AND ROWNUM = 1)
                       SEQUENCE,
                    mcba.segment1 ParentGroupIdentifier,
                    NULL ParentStoreIdentifier,
                    'USD' CurrencyCode,
                    (SELECT XXWC_W_S_D_ATTR
                       FROM XXWC_WHITE_C_M_AGV
                      WHERE     ROWNUM = 1
                            AND organization_id = l_master_org_id
                            AND inventory_item_id = msib.INVENTORY_ITEM_ID)
                       Name,
                    msib.description ShortDescription,
                    (SELECT XXWC_WEB_LONG_DESCRIPTION_ATTR
                       FROM XXWC_WHITE_C_M_AGV
                      WHERE     ROWNUM = 1
                            AND organization_id = l_master_org_id
                            AND inventory_item_id = msib.INVENTORY_ITEM_ID)
                       LongDescription,
                       fnd_profile.VALUE ('XXWC_THUMBNAIL_PROD')
                    || (SELECT XXWC_A_T_1_ATTR
                          FROM XXWC_WHITE_C_M_AGV
                         WHERE     ROWNUM = 1
                               AND organization_id = l_master_org_id
                               AND inventory_item_id = msib.INVENTORY_ITEM_ID)
                       Thumbnail,
                       fnd_profile.VALUE ('XXWC_FULL_IMAGE_PROD')
                    || (SELECT XXWC_A_F_1_ATTR
                          FROM XXWC_WHITE_C_M_AGV
                         WHERE     ROWNUM = 1
                               AND organization_id = l_master_org_id
                               AND inventory_item_id = msib.INVENTORY_ITEM_ID)
                       FullImage,
                    'C62' QuantityMeasure,
                    'LBS' WeightMeasure,
                    unit_weight Weight,
                    '1' Buyable,
                    (SELECT XXWC_WEB_KEYWORDS_ATTR
                       FROM XXWC_WHITE_C_M_AGV
                      WHERE     inventory_item_id = msib.inventory_item_id
                            AND organization_id = l_master_org_id
                            AND ROWNUM = 1)
                       Keyword,
                    '1' Delete_flag
               FROM APPS.MTL_SYSTEM_ITEMS_B MSIB,
                    XXWC.XXWC_EGO_CATALOG_CATEGORY_LOG XECC,
                    APPS.MTL_CATEGORY_SETS MCSA,
                    APPS.MTL_ITEM_CATEGORIES MICA,
                    APPS.MTL_CATEGORIES_B MCBA
              WHERE     msib.ORGANIZATION_ID =
                           TO_NUMBER (
                              fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG'))
                    AND xecc.INVENTORY_ITEM_ID = msib.INVENTORY_ITEM_ID
                    AND xecc.INVENTORY_ITEM_ID NOT IN (SELECT xemi.inventory_item_id
                                                         FROM XXWC.XXWC_EGO_MOD_ITEMS_TAB XEMI)
                    AND mcsa.CATEGORY_SET_NAME = 'WC Web Hierarchy'
                    AND mica.CATEGORY_SET_ID = mcsa.CATEGORY_SET_ID
                    AND mica.INVENTORY_ITEM_ID = msib.INVENTORY_ITEM_ID
                    AND mica.ORGANIZATION_ID = msib.ORGANIZATION_ID
                    AND mcba.CATEGORY_ID = mica.CATEGORY_ID
                    AND xecc.CREATION_DATE IN (SELECT MAX (
                                                         XECL.CREATION_DATE)
                                                 FROM XXWC.XXWC_EGO_CATALOG_CATEGORY_LOG XECL
                                                WHERE     XECL.INVENTORY_ITEM_ID =
                                                             XECC.INVENTORY_ITEM_ID
                                                      AND XECL.ORGANIZATION_ID =
                                                             XECC.ORGANIZATION_ID)
                    AND xecc.CREATION_DATE >= L_LAST_RUN_DATE -- End of code modification w.r.t. ver 1.2.8 --
                                                             )
      LOOP
         L_STRING :=
               'P_'
            || TRIM (CUR_DATA.PARTNUMBER)
            || L_DEL
            || 'Product'
            || L_DEL
            || CUR_DATA.ManufacturerPartNumber
            || L_DEL
            || CUR_DATA.Manufacturer
            || L_DEL
            || CUR_DATA.ParentPartNumber
            || L_DEL
            || NVL (CUR_DATA.SEQUENCE, '99999')
            || L_DEL
            || CUR_DATA.ParentGroupIdentifier
            || L_DEL
            || CUR_DATA.ParentStoreIdentifier
            || L_DEL
            || CUR_DATA.CurrencyCode
            || L_DEL
            || CUR_DATA.Name
            || L_DEL
            || CUR_DATA.ShortDescription
            || L_DEL
            || CUR_DATA.LongDescription
            || L_DEL
            || CUR_DATA.Thumbnail
            || L_DEL
            || CUR_DATA.FullImage
            || L_DEL
            || CUR_DATA.QuantityMeasure
            || L_DEL
            || CUR_DATA.WeightMeasure
            || L_DEL
            || CUR_DATA.Weight
            || L_DEL
            || CUR_DATA.Buyable
            || L_DEL
            || CUR_DATA.Keyword
            || L_DEL
            || CUR_DATA.DELETE_FLAG;


         --UTL_FILE.PUT_LINE (L_FILE, L_STRING);   --row insert for product type  Commented by Vamshi in 1.6 @ TMS#20150501-00013
         -- Added below code by Vamshi in 1.6 @ TMS#20150501-00013 -- Start
         IF CUR_DATA.DELETE_FLAG = '0'
         THEN
            UTL_FILE.PUT_LINE (L_FILE_I, L_STRING);
         ELSE
            UTL_FILE.PUT_LINE (L_FILE_D, L_STRING);
         END IF;

         -- Added below code by Vamshi in 1.6 @ TMS#20150501-00013 -- End

         L_STRING :=
               CUR_DATA.PARTNUMBER
            || L_DEL
            || 'Item'
            || L_DEL
            || CUR_DATA.ManufacturerPartNumber
            || L_DEL
            || CUR_DATA.Manufacturer
            || L_DEL
            || 'P_'
            || TRIM (CUR_DATA.PARTNUMBER)
            || L_DEL
            || NVL (CUR_DATA.SEQUENCE, '99999')
            || L_DEL
            || CUR_DATA.ParentGroupIdentifier
            || L_DEL
            || CUR_DATA.ParentStoreIdentifier
            || L_DEL
            || CUR_DATA.CurrencyCode
            || L_DEL
            || CUR_DATA.Name
            || L_DEL
            || CUR_DATA.ShortDescription
            || L_DEL
            || CUR_DATA.LongDescription
            || L_DEL
            || CUR_DATA.Thumbnail
            || L_DEL
            || CUR_DATA.FullImage
            || L_DEL
            || CUR_DATA.QuantityMeasure
            || L_DEL
            || CUR_DATA.WeightMeasure
            || L_DEL
            || CUR_DATA.Weight
            || L_DEL
            || CUR_DATA.Buyable
            || L_DEL
            || CUR_DATA.Keyword
            || L_DEL
            || CUR_DATA.DELETE_FLAG;

         -- Added below code by Vamshi in 1.6 @ TMS#20150501-00013 -- Start
         IF CUR_DATA.DELETE_FLAG = '0'
         THEN
            UTL_FILE.PUT_LINE (L_FILE_I, L_STRING);
         ELSE
            UTL_FILE.PUT_LINE (L_FILE_D, L_STRING);
         END IF;
      -- Added below code by Vamshi in 1.6 @ TMS#20150501-00013 -- End

      END LOOP;

      UTL_FILE.FCLOSE (L_FILE_I); -- Added by Vamshi in 1.6 V  @TMS#20150501-00013
      UTL_FILE.FCLOSE (L_FILE_D); -- Added by Vamshi in 1.6 V  @TMS#20150501-00013
   EXCEPTION
      WHEN UTL_FILE.INTERNAL_ERROR
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            ' Debug INTERNAL_ERROR : ' || L_STRING);

         --      Added below code by Vamshi in 1.6 V @TMS#20150501-00013 -- start
         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;

         raise_application_error (
            -20500,
               'Cannot open file :'
            || L_FILE_NAME_I
            || '/'
            || L_FILE_NAME_D
            || ', internal error; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      --      Added above code by Vamshi in 1.6 V @TMS#20150501-00013 -- End

      WHEN UTL_FILE.INVALID_OPERATION
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            ' Debug INVALID_OPERATION : ' || L_STRING);

         --      Added below code by Vamshi in 1.6 V @TMS#20150501-00013 -- Start
         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;

         raise_application_error (
            -20501,
               'Cannot open file :'
            || L_FILE_NAME_I
            || '/'
            || L_FILE_NAME_D
            || ', invalid operation; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      --      Added above code by Vamshi in 1.6 V @TMS#20150501-00013 -- End

      WHEN UTL_FILE.INVALID_PATH
      THEN
         --      Added below code by Vamshi in 1.6 V @TMS#20150501-00013 -- Start
         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;

         raise_application_error (
            -20502,
               'Cannot open file :'
            || L_FILE_NAME_I -- Changed file name from L_FILE_NAME to L_FILE_NAME_I by Vamshi in 1.6 V @TMS#20150501-00013
            || '/'
            || L_FILE_NAME_D -- Changed file name from L_FILE_NAME to L_FILE_NAME_D by Vamshi in 1.6 V @TMS#20150501-00013
            || ', invalid path; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      WHEN UTL_FILE.WRITE_ERROR
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            ' Debug WRITE_ERROR : ' || L_STRING);

         -- Added below code by Vamshi in 1.6 V @TMS#20150501-00013 -- Start

         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;

         raise_application_error (
            -20503,
               'Cannot write to file :'
            || L_FILE_NAME_I
            || '/'
            || L_FILE_NAME_D
            || ', write error; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      -- Added above code by Vamshi in 1.6 V @TMS#20150501-00013 -- End

      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, ' Debug OTHERS : ' || L_STRING);

         -- Added below code by Vamshi in 1.6 V @TMS#20150501-00013 -- Start

         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;

         fnd_file.put_line (
            fnd_file.LOG,
               ' Debug OTHERS : '
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
   -- Added above code by Vamshi in 1.6 V @TMS#20150501-00013 -- End

   END;

   /***********************************************************************************************************************
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  PROGRAM TYPE: PROCEDURE
      NAME: CREATE_ITEM_ATTR_DATA
      PURPOSE: Procedure to generate the Item attribute data
      HISTORY
      =====================================================================================================================
      VERSION DATE          AUTHOR(S)       DESCRIPTION
      ------- -----------   --------------- -------------------------------------------------------------------------------
      1.0     09/23/2012    Rajiv Rathod    Initial creation of the procedure
      1.1     12/13/2012    Jyotsna Bidkar  Changes made to exclude the item attribute
                                            relationships data if an item is made inactive
                                            (as per mail from Jas)

      1.6     05/06/2015    P.Vamshidhar    TMS#20150501-00013 - PDH to Ecomm Data Interface Improvements
                                            To generate separate files for delete and insert/update.


      ********************************************************************************************************************/

   PROCEDURE CREATE_ITEM_ATTR_DATA (P_TIMESTAMP       IN VARCHAR2,
                                    L_LAST_RUN_DATE      DATE)
   AS
   BEGIN
      --L_FILE_NAME := 'wc_catalogentryattributedictionaryattributerelationship.csv';
      L_FILE_NAME_I :=
         'wc_catalogentryattributedictionaryattributerelationship_i.csv'; -- Added by Vamshi in V1.6 @TMS#20150501-00013
      L_FILE_NAME_D :=
         'wc_catalogentryattributedictionaryattributerelationship_d.csv'; -- Added by Vamshi in V1.6 @TMS#20150501-00013

      /*  -- Commented code by Vamshi in V1.6 @TMS#20150501-00013
            L_FILE :=
               UTL_FILE.FOPEN_NCHAR (C_LOCAL_DIRECTORY,
                                     L_FILE_NAME,
                                     'W',
                                     MAX_LINE_LENGTH);
      */
      -- Added below code by Vamshi in V1.6 @TMS#20150501-00013 -- Start

      L_FILE_I :=
         UTL_FILE.FOPEN_NCHAR (C_LOCAL_DIRECTORY,
                               L_FILE_NAME_I,
                               'W',
                               MAX_LINE_LENGTH);

      L_FILE_D :=
         UTL_FILE.FOPEN_NCHAR (C_LOCAL_DIRECTORY,
                               L_FILE_NAME_D,
                               'W',
                               MAX_LINE_LENGTH);

      UTL_FILE.PUT_LINE_NCHAR (L_FILE_I, CHR (15711167));
      UTL_FILE.PUT_LINE_NCHAR (L_FILE_D, CHR (15711167));

      -- Added above code by Vamshi in V1.6 @TMS#20150501-00013 -- End

      -- UTL_FILE.PUT_LINE_NCHAR (L_FILE, CHR (15711167));    -- commented by Vamshi in V1.6 @TMS#20150501-00013

      l_buffer_count := 0; --added by rajiv in version 1.2.4 to fix the buffer issue on 6-NOV-2013

      L_STRING := 'CatalogEntryAttributeDictionaryAttributeRelationship';

      --UTL_FILE.PUT_LINE_NCHAR (L_FILE, L_STRING); -- Commented by Vamshi in V1.6 @TMS#20150501-00013

      UTL_FILE.PUT_LINE_NCHAR (L_FILE_I, L_STRING); -- Added by Vamshi in V1.6 @TMS#20150501-00013
      UTL_FILE.PUT_LINE_NCHAR (L_FILE_D, L_STRING); -- Added by Vamshi in V1.6 @TMS#20150501-00013

      -- UTL_FILE.PUT_LINE_NCHAR (L_SALES_FILE, L_STRING);   -- Commented out w.r.t. ver 1.2.7 on 11/27/2013

      L_STRING :=
         'PartNumber|AttributeIdentifier|ValueIdentifier|Value|Usage|Sequence|Delete';

      --UTL_FILE.PUT_LINE_NCHAR (L_FILE, L_STRING); -- Commented by Vamshi  in V1.6 @TMS#20150501-00013
      UTL_FILE.PUT_LINE_NCHAR (L_FILE_I, L_STRING); -- Added by Vamshi in V1.6 @TMS#20150501-00013
      UTL_FILE.PUT_LINE_NCHAR (L_FILE_D, L_STRING); -- Added by Vamshi in V1.6 @TMS#20150501-00013

      -- UTL_FILE.PUT_LINE_NCHAR (L_SALES_FILE, L_STRING); -- Commented out w.r.t. ver 1.2.7 on 11/27/2013


      FOR CUR_DATA
         IN (SELECT msib.segment1 PartNumber,
                    c.ATTRIBUTEIDENTIFIER,
                    C.ATTRIBUTEIDENTIFIER || '-' || C.EXTENSION_ID
                       VALUEIDENTIFIER,
                    REPLACE (
                       NVL2 (
                          ValueIdentifier,
                             ValueIdentifier
                          || DECODE (RTRIM (c.description, ' [WCS]'),
                                     NULL, NULL,
                                     ' ' || RTRIM (c.description, ' [WCS]')),
                          VALUEIDENTIFIER),
                       CHR (13))
                       VALUE,
                    c.Usage,
                    c.Sequence,
                    DECODE (ValueIdentifier, NULL, '1', '0') Delete_flag,
                    c.ItemId
               FROM (SELECT attribute_group_name || '-' || attribute_name
                               AttributeIdentifier,
                            XXWC_EGO_PUB_WCS_PKG.get_flex_value (
                               value_Set_id,
                               validation_code,
                               DECODE (data_type_code,
                                       'C', attribute_char_value,
                                       'N', attribute_number_value,
                                       'D', attribute_date_value))
                               ValueIdentifier,
                            'Descriptive' Usage,
                            DECODE (attr_display_name,
                                    'Brand', 1,
                                    'Product Line', 2,
                                    'Includes', 3,
                                    'Length', 51,
                                    'Width', 52,
                                    'Height', 53,
                                    'Special Features', 54,
                                    'Usage', 55,
                                    4)
                               Sequence,
                            value_Set_id,
                            validation_code,
                            a.description,
                            extension_id,
                            a.inventory_item_id ItemId
                       FROM XXWC_EGO_ALL_ATTR_BASE_V a
                     UNION ALL
                     SELECT a.attribute_group_name || '-' || a.attribute_name
                               AttributeIdentifier,
                            XXWC_EGO_PUB_WCS_PKG.get_flex_value (
                               value_Set_id,
                               validation_code,
                               DECODE (data_type_code,
                                       'A', ATTRIBUTE_TRANSLATABLE_VALUE,
                                       NULL))
                               ValueIdentifier,
                            'Descriptive' Usage,
                            DECODE (attr_display_name,
                                    'Brand', 1,
                                    'Product Line', 2,
                                    'Includes', 3,
                                    'Length', 51,
                                    'Width', 52,
                                    'Height', 53,
                                    'Special Features', 54,
                                    'Usage', 55,
                                    4)
                               Sequence,
                            value_Set_id,
                            validation_code,
                            a.description,
                            extension_id,
                            a.inventory_item_id ItemId
                       FROM XXWC_EGO_ALL_ATTR_LANG_V a) c,
                    mtl_system_items_b msib,
                    xxwc_ego_mod_items_tab xemi
              WHERE     msib.inventory_item_id = xemi.inventory_item_id
                    AND msib.inventory_item_id = c.itemid
                    AND msib.organization_id = l_master_org_id
             UNION
             SELECT msib.segment1 PartNumber,
                    EAGV.ATTR_GROUP_NAME || '-XXWC_WHITECAPPUBLIC_ATTR'
                       ATTRIBUTEIDENTIFIER,
                    --EAGV.ATTR_GROUP_NAME||'-XXWC_WHITECAPPUBLIC_ATTR-'||(DECODE(mcb.segment1, 'Y', 'Yes', 'C', 'No', NULL)) VALUEIDENTIFIER, -- Commented w.r.t. ver 1.2.9 --
                    (DECODE (mcb.segment1,  'Y', 'Yes',  'C', 'No',  NULL))
                       VALUEIDENTIFIER,              -- Added w.r.t. ver 1.2.9
                    (DECODE (mcb.segment1,  'Y', 'Yes',  'C', 'No',  NULL))
                       VALUE,
                    'Defining' Usage,
                    NULL Sequence,
                    '0' Delete_flag,
                    msib.inventory_item_id ItemId
               FROM MTL_SYSTEM_ITEMS_B MSIB,
                    XXWC_EGO_MOD_ITEMS_TAB XEMI,
                    MTL_CATEGORY_SETS MCS,
                    MTL_ITEM_CATEGORIES MIC,
                    MTL_CATEGORIES_B MCB,
                    EGO_ATTR_GROUPS_V EAGV
              WHERE     msib.inventory_item_id = xemi.inventory_item_id
                    AND msib.organization_id = l_master_org_id
                    --AND msib.inventory_item_status_code = 'Active'  -- Commented out w.r.t. ver 1.4 --
                    AND mcs.category_set_name = 'Website Item'
                    AND mic.category_set_id = mcs.category_set_id
                    AND mic.inventory_item_id = msib.inventory_item_id
                    AND mic.organization_id = msib.organization_id
                    AND mcb.category_id = mic.category_id
                    AND eagv.attr_group_name = 'XXWC_WHITE_CAP_MST_AG'
             UNION
             SELECT msib.segment1 PartNumber,
                    EAGV.ATTR_GROUP_NAME || '-XXWC_WHITECAPPUBLIC_ATTR'
                       ATTRIBUTEIDENTIFIER,
                    --EAGV.ATTR_GROUP_NAME||'-XXWC_WHITECAPPUBLIC_ATTR-'||(DECODE(mcb.segment1, 'Y', 'Yes', 'C', 'No', NULL)) VALUEIDENTIFIER, -- Commented w.r.t. ver 1.2.9 --
                    (DECODE (mcb.segment1,  'Y', 'Yes',  'C', 'No',  NULL))
                       VALUEIDENTIFIER,              -- Added w.r.t. ver 1.2.9
                    (DECODE (mcb.segment1,  'Y', 'Yes',  'C', 'No',  NULL))
                       VALUE,
                    'Defining' Usage,
                    NULL Sequence,
                    '1' Delete_flag,
                    msib.inventory_item_id ItemId
               FROM MTL_SYSTEM_ITEMS_B MSIB,
                    XXWC_EGO_MOD_ITEMS_TAB XEMI,
                    MTL_CATEGORY_SETS MCS,
                    MTL_CATEGORIES_B MCB,
                    MTL_ITEM_CATEGORIES MIC,
                    EGO_ATTR_GROUPS_V EAGV,
                    XXWC.XXWC_EGO_SKU_PUBLISH_HISTORY XES
              WHERE     msib.inventory_item_id = xemi.inventory_item_id
                    AND msib.organization_id = l_master_org_id
                    --AND msib.inventory_item_status_code = 'Active'  -- Commented out w.r.t. ver 1.4 --
                    AND mcs.category_set_name = 'Website Item'
                    AND mcb.category_id = xes.category_id
                    AND xes.inventory_item_id = msib.inventory_item_id
                    AND xes.organization_id = msib.organization_id
                    AND mic.inventory_item_id = msib.inventory_item_id
                    AND mic.organization_id = msib.organization_id
                    AND mic.category_set_id = mcs.category_set_id
                    AND mic.category_id <> xes.category_id
                    AND xes.category_set_id = mcs.category_set_id
                    AND eagv.attr_group_name = 'XXWC_WHITE_CAP_MST_AG'
                    AND mcb.segment1 IN ('Y', 'C') -- Added w.r.t. ver 1.2.8 --
             ORDER BY ITEMID, ATTRIBUTEIDENTIFIER, DELETE_FLAG DESC -- Added w.r.t ver 1.3 on 26-Mar-2014 --
                                                                   )
      LOOP
         L_STRING :=
               CUR_DATA.partNumber
            || L_DEL
            || CUR_DATA.AttributeIdentifier
            || L_DEL
            || CUR_DATA.ValueIdentifier
            || L_DEL
            || CUR_DATA.VALUE
            || L_DEL
            || CUR_DATA.Usage
            || L_DEL
            || CUR_DATA.Sequence
            || L_DEL
            || CUR_DATA.Delete_flag;

         --UTL_FILE.PUT_LINE_NCHAR (L_FILE, L_STRING); -- Commented by Vamshi in V1.6 @TMS#20150501-00013

         -- Added below code by Vamshi in V1.6 @TMS#20150501-00013

         IF CUR_DATA.Delete_flag = '0'
         THEN
            UTL_FILE.PUT_LINE_NCHAR (L_FILE_I, L_STRING);
         ELSE
            UTL_FILE.PUT_LINE_NCHAR (L_FILE_D, L_STRING);
         END IF;

         -- Added above code by Vamshi in V1.6 @TMS#20150501-00013

         /*Added By rajiv in version 1.2.4 to fix the buffer issue on 6-NOV-13 */
         IF (l_buffer_count > 1000)
         THEN
            l_buffer_count := 0;
            --UTL_FILE.FFLUSH (L_FILE);    -- Commented by Vamshi in V1.6 @TMS#20150501-00013
            UTL_FILE.FFLUSH (L_FILE_I); -- Added by Vamshi in V1.6 @TMS#20150501-00013
            UTL_FILE.FFLUSH (L_FILE_D); -- Added by Vamshi in V1.6 @TMS#20150501-00013
         -- UTL_FILE.FFLUSH(L_SALES_FILE);  -- Commented out w.r.t. ver 1.2.7 on 11/27/2013
         END IF;

         l_buffer_count := l_buffer_count + 1;
      END LOOP;

      --UTL_FILE.FCLOSE (L_FILE); -- Commented by Vamshi  in V1.6 @TMS#20150501-00013
      UTL_FILE.FCLOSE (L_FILE_I); -- Added by Vamshi in V1.6 @TMS#20150501-00013
      UTL_FILE.FCLOSE (L_FILE_D); -- Added by Vamshi in V1.6 @TMS#20150501-00013
   -- UTL_FILE.FCLOSE (L_SALES_FILE);  -- Commented out w.r.t. ver 1.2.7 on 11/27/2013
   EXCEPTION
      WHEN UTL_FILE.INTERNAL_ERROR
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            ' Debug INTERNAL_ERROR : ' || L_STRING);

         /* -- Commented below code by Vamshi in V1.6 @TMS#20150501-00013 start
                  IF UTL_FILE.is_open (L_FILE)
                  THEN
                     UTL_FILE.FCLOSE (L_FILE);
                  END IF;
         */
         -- Commented above code by Vamshi in V1.6 @TMS#20150501-00013 End
         -- Added below code by Vamshi in V1.6 @TMS#20150501-00013 start

         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;

         -- Added above code by Vamshi in V1.6 @TMS#20150501-00013 end

         raise_application_error (
            -20500,
               'Cannot open file :'
            || L_FILE_NAME_I -- Changed from L_FILE_NAME to L_FILE_NAME_I by Vamshi in V1.6 @TMS#20150501-00013
            || '/'              -- Added by Vamshi in V1.6 @TMS#20150501-00013
            || L_FILE_NAME_D    -- Added by Vamshi in V1.6 @TMS#20150501-00013
            || ', internal error; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      WHEN UTL_FILE.INVALID_OPERATION
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            ' Debug INVALID_OPERATION : ' || L_STRING);

         /*   -- Commented below code by Vamshi in V1.6 @TMS#20150501-00013
                 IF UTL_FILE.is_open (L_FILE)
                  THEN
                     UTL_FILE.FCLOSE (L_FILE);
                  END IF;
         */
         --  Added below code by Vamshi in V1.6 @TMS#20150501-00013 -- Start
         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;

         --  Added above code by Vamshi in V1.6 @TMS#20150501-00013 -- End.

         raise_application_error (
            -20501,
               'Cannot open file :'
            || L_FILE_NAME_I -- Changed from L_FILE_NAME to L_FILE_NAME_I by Vamshi in  V1.6 @TMS#20150501-00013
            || '/'             -- Added by Vamshi in  V1.6 @TMS#20150501-00013
            || L_FILE_NAME_D   -- Added by Vamshi in  V1.6 @TMS#20150501-00013
            || ', invalid operation; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      WHEN UTL_FILE.INVALID_PATH
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            ' Debug INVALID_PATH : ' || L_STRING);

         --    Commented below code by Vamshi in V1.6 @TMS#20150501-00013
         /*        IF UTL_FILE.is_open (L_FILE)
                  THEN
                     UTL_FILE.FCLOSE (L_FILE);
                  END IF;
         */
         -- Added below 2 IF condition by Vamshi in  V1.6 @TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;

         raise_application_error (
            -20502,
               'Cannot open file :'
            || L_FILE_NAME_I -- Changed by Vamshi  in  V1.6 @TMS#20150501-00013
            || '/'            -- Added by Vamshi  in  V1.6 @TMS#20150501-00013
            || L_FILE_NAME_D  -- Added by Vamshi  in  V1.6 @TMS#20150501-00013
            || ', invalid path; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      WHEN UTL_FILE.WRITE_ERROR
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            ' Debug WRITE_ERROR : ' || L_STRING);

         -- Commented below code by Vamshi in  V1.6 @TMS#20150501-00013
         /*IF UTL_FILE.is_open (L_FILE)
         THEN
            UTL_FILE.FCLOSE (L_FILE);
         END IF;*/
         -- Added below 2 IF Conditions by Vamshi in  V1.6 @TMS#20150501-00013

         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;

         raise_application_error (
            -20503,
               'Cannot write to file :'
            || L_FILE_NAME_I  -- Changed by Vamshi in V1.6 @TMS#20150501-00013
            || '/'              -- Added by Vamshi in V1.6 @TMS#20150501-00013
            || L_FILE_NAME_D    -- Added by Vamshi in V1.6 @TMS#20150501-00013
            || ', write error; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      WHEN OTHERS
      THEN
         fnd_file.put_line (
            fnd_file.LOG,
               ' Debug OTHERS : '
            || L_STRING
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());

         -- Added below 2 IF clauses by Vamshi in V1.6 @TMS#20150501-00013

         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;
   END;

   /********************************************************************************
                                        PROGRAM TYPE: PROCEDURE
       NAME: CREATE_ITEM_DICTNRY_DATA
       PURPOSE: Procedure to generate the attribute dictionary data
       HISTORY
       ===============================================================================
       VERSION DATE          AUTHOR(S)       DESCRIPTION
       ------- -----------   --------------- -----------------------------------------
       1.0     09/23/2012    Rajiv Rathod    Initial creation of the procedure
       1.6     05/06/2015    P.Vamshidhar    TMS#20150501-00013 - PDH to Ecomm Data Interface Improvements
                                             To generate separate files for delete and insert/update.
        ********************************************************************************/
   PROCEDURE CREATE_ITEM_DICTNRY_DATA (P_TIMESTAMP       IN VARCHAR2,
                                       L_LAST_RUN_DATE      DATE)
   AS
   BEGIN
      L_FILE_NAME_I := 'wc_attributedictionaryattributeandallowedvalues_i.csv'; -- Added i to variable by Vamshi @1.6 v TMS#20150501-00013
      L_FILE_NAME_D := 'wc_attributedictionaryattributeandallowedvalues_d.csv'; -- Added by Vamshi @1.6 v TMS#20150501-00013


      L_FILE_I := ---- Added i to variable by Vamshi @1.6 v TMS#20150501-00013
         UTL_FILE.FOPEN (C_LOCAL_DIRECTORY,
                         L_FILE_NAME_I,
                         'W',
                         MAX_LINE_LENGTH);

      -- Added below code by Vamshi @1.6 v TMS#20150501-00013
      L_FILE_D :=
         UTL_FILE.FOPEN (C_LOCAL_DIRECTORY,
                         L_FILE_NAME_D,
                         'W',
                         MAX_LINE_LENGTH);


      UTL_FILE.PUT_LINE (L_FILE_I, CHR (15711167)); -- Added i to variable by Vamshi @1.6 v TMS#20150501-00013
      UTL_FILE.PUT_LINE (L_FILE_D, CHR (15711167)); -- Added by Vamshi @1.6 v TMS#20150501-00013

      L_STRING := 'AttributeDictionaryAttributeAndAllowedValues';
      UTL_FILE.PUT_LINE (L_FILE_I, L_STRING); -- Added i to variable by Vamshi @1.6 v TMS#20150501-00013
      UTL_FILE.PUT_LINE (L_FILE_D, L_STRING); -- Added by Vamshi @1.6 v TMS#20150501-00013

      L_STRING :=
            'Identifier|Type|AttributeType|Sequence|Displayable|Searchable|Comparable|Name'
         || '|AllowedValue'
         || '|Delete';

      UTL_FILE.PUT_LINE (L_FILE_I, L_STRING); -- Added i to variable by Vamshi @1.6 v TMS#20150501-00013
      UTL_FILE.PUT_LINE (L_FILE_D, L_STRING); -- Added by Vamshi @1.6 v TMS#20150501-00013

      FOR CUR_DATA
         IN (SELECT ag.attr_group_name || '-' || agc.attr_name
                       AttributeIdentifier,
                    'STRING' TYPE,
                    -- NVL2 (agc.value_set_id, 'AllowedValues', 'AssignedValues')
                    'AssignedValues' AttributeType,
                    DECODE (
                       ag.attr_group_name || '-' || agc.attr_name,
                       'XXWC_WHITE_CAP_MST_AG-XXWC_BRAND_ATTR', '1',
                       'XXWC_WHITE_CAP_MST_AG-XXWC_PRODUCT_LINE_ATTR', '2',
                       DECODE (ag.attr_group_name,
                               'XXWC_WHITE_CAP_MST_AG', NULL,
                               'XXWC_FEATURES_BENEFITS_AG', NULL,
                               '3'))
                       Sequence,
                    -- DECODE (agc.enabled_meaning, 'Yes', 'TRUE', 'FALSE')   Displayable,
                    DECODE (
                       ag.attr_group_name || '-' || agc.attr_name,
                       'XXWC_WHITE_CAP_MST_AG-XXWC_BRAND_ATTR', 'TRUE',
                       'XXWC_WHITE_CAP_MST_AG-XXWC_PRODUCT_LINE_ATTR', 'TRUE',
                       DECODE (ag.attr_group_name,
                               'XXWC_WHITE_CAP_MST_AG', 'FALSE',
                               'XXWC_FEATURES_BENEFITS_AG', 'FALSE',
                               'TRUE'))
                       Displayable,
                    --  DECODE (agc.search_meaning, 'Yes', 'TRUE', 'FALSE')  Searchable,
                    'FALSE' Searchable,
                    DECODE (
                       ag.attr_group_name || '-' || agc.attr_name,
                       'XXWC_WHITE_CAP_MST_AG-XXWC_BRAND_ATTR', 'TRUE',
                       'XXWC_WHITE_CAP_MST_AG-XXWC_PRODUCT_LINE_ATTR', 'TRUE',
                       DECODE (ag.attr_group_name,
                               'XXWC_WHITE_CAP_MST_AG', 'FALSE',
                               'XXWC_FEATURES_BENEFITS_AG', 'FALSE',
                               'TRUE'))
                       Comparable,
                    agc.attr_display_name Name,
                    --allowed_values (agc.value_set_id) AllowedValue,
                    NULL AllowedValue,
                    '0' Delete_flag
               FROM XXWC_EGO_ATTRS_V AGC, XXWC_EGO_ATTR_GROUPS_V AG
              WHERE     AGC.APPLICATION_ID = AG.APPLICATION_ID
                    AND AGC.ATTR_GROUP_TYPE = AG.ATTR_GROUP_TYPE
                    AND AGC.ATTR_GROUP_NAME = AG.ATTR_GROUP_NAME
                    -- AND AGC.DATA_TYPE_CODE != 'A'
                    AND AG.attr_group_Type = 'EGO_ITEMMGMT_GROUP'
                    AND AG.ATTR_GROUP_NAME LIKE 'XXWC%'
                    AND AGC.description LIKE '%[WCS]%'
                    AND ag.attr_group_name || '-' || agc.attr_name NOT IN ('XXWC_WHITE_CAP_MST_AG-XXWC_W_S_D_ATTR',
                                                                           'XXWC_WHITE_CAP_MST_AG-XXWC_M_P_N_ATTR',
                                                                           'XXWC_WHITE_CAP_MST_AG-XXWC_A_F_1_ATTR',
                                                                           'XXWC_WHITE_CAP_MST_AG-XXWC_A_T_1_ATTR',
                                                                           'XXWC_WHITE_CAP_MST_AG-XXWC_WEB_LONG_DESCRIPTION_ATTR')
                    -- AND AG.ATTR_GROUP_NAME = 'XXWC_CHOP_SAWS_AG'
                    AND (   (AGC.last_update_date >=
                                NVL (L_LAST_RUN_DATE, AGC.last_update_date))
                         OR (AG.last_update_date >=
                                NVL (L_LAST_RUN_DATE, AG.last_update_date))))
      LOOP
         L_STRING :=
               CUR_DATA.AttributeIdentifier
            || L_DEL
            || CUR_DATA.TYPE
            || L_DEL
            || CUR_DATA.AttributeType
            || L_DEL
            || CUR_DATA.Sequence
            || L_DEL
            || CUR_DATA.Displayable
            || L_DEL
            || CUR_DATA.Searchable
            || L_DEL
            || CUR_DATA.Comparable
            || L_DEL
            || CUR_DATA.NAME
            || L_DEL
            || CUR_DATA.AllowedValue
            || L_DEL
            || CUR_DATA.Delete_flag;

         IF CUR_DATA.Delete_flag = '0'
         THEN                     -- Added by Vamshi @1.6 v TMS#20150501-00013
            UTL_FILE.PUT_LINE (L_FILE_I, L_STRING); -- Added by Vamshi @1.6 v TMS#20150501-00013
         ELSE                     -- Added by Vamshi @1.6 v TMS#20150501-00013
            UTL_FILE.PUT_LINE (L_FILE_D, L_STRING); -- Added by Vamshi @1.6 v TMS#20150501-00013
         END IF;
      END LOOP;

      UTL_FILE.FCLOSE (L_FILE_I); -- Added i to variable by Vamshi @1.6 v TMS#20150501-00013
      UTL_FILE.FCLOSE (L_FILE_D); -- Added by Vamshi @1.6 v TMS#20150501-00013
   EXCEPTION
      WHEN UTL_FILE.INTERNAL_ERROR
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            ' Debug INTERNAL_ERROR : ' || L_STRING);

         -- Below code modified by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         -- Added by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;

         raise_application_error (
            -20500,
               'Cannot open file :'
            || L_FILE_NAME_I    -- Changed by Vamshi @1.6 v TMS#20150501-00013
            || '/'
            || L_FILE_NAME_D      -- Added by Vamshi @1.6 v TMS#20150501-00013
            || ', internal error; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      WHEN UTL_FILE.INVALID_OPERATION
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            ' Debug INVALID_OPERATION : ' || L_STRING);

         -- Below code modified by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         -- Added by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;

         raise_application_error (
            -20501,
               'Cannot open file :'
            || L_FILE_NAME_I    -- Changed by Vamshi @1.6 v TMS#20150501-00013
            || '/'
            || L_FILE_NAME_D      -- Added by Vamshi @1.6 v TMS#20150501-00013
            || ', invalid operation; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      WHEN UTL_FILE.INVALID_PATH
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            ' Debug INVALID_PATH : ' || L_STRING);

         -- Below code modified by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         -- Added by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;

         raise_application_error (
            -20502,
               'Cannot open file :'
            || L_FILE_NAME_I    -- Changed by Vamshi @1.6 v TMS#20150501-00013
            || '/'
            || L_FILE_NAME_D      -- Added by Vamshi @1.6 v TMS#20150501-00013
            || ', invalid path; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      WHEN UTL_FILE.WRITE_ERROR
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            ' Debug WRITE_ERROR : ' || L_STRING);

         -- Below code modified by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         -- Added by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;

         raise_application_error (
            -20503,
               'Cannot write to file :'
            || L_FILE_NAME_I    -- Changed by Vamshi @1.6 v TMS#20150501-00013
            || '/'
            || L_FILE_NAME_D      -- Added by Vamshi @1.6 v TMS#20150501-00013
            || ', write error; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      WHEN OTHERS
      THEN
         fnd_file.put_line (
            fnd_file.LOG,
               ' Debug OTHERS : '
            || L_STRING
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());

         -- Below code modified by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         -- Added by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;
   END;

   /********************************************************************************
                                                                                                                                                                                                                                                PROGRAM TYPE: PROCEDURE
      NAME: CREATE_PROD_HIER_DATA
      PURPOSE: Procedure to generate the product hierarchy data
      HISTORY
      ===============================================================================
      VERSION DATE          AUTHOR(S)       DESCRIPTION
      ------- -----------   --------------- -----------------------------------------
      1.0     09/23/2012    Rajiv Rathod    Initial creation of the procedure
      1.1     12/14/2012    Jyotsna Bidkar  Changes done to Category Image Filepaths
                                          (as per mail from Jeff)
      1.2    12/27/2012    Jyotsna Bidkar  Changes done to send the file to
                                            Sales Catalog directory
      1.2.1  09/12/2013   Rajasekar Gunasekaran Updated the value of the Name field with Description
      1.2.2  09/20/2013   Rajasekar Gunasekaran Updated the proc to generate the full refresh instead of delta

      1.6     05/06/2015    P.Vamshidhar    TMS#20150501-00013 - PDH to Ecomm Data Interface Improvements
                                            To generate separate files for delete and insert/update.

      *********************************************************************************/
   PROCEDURE CREATE_PROD_HIER_DATA (P_TIMESTAMP       IN VARCHAR2,
                                    L_LAST_RUN_DATE      DATE,
                                    FROM_DATE            VARCHAR2)
   AS
      l_category_Set_id   NUMBER;
      l_structure_id      NUMBER;
   BEGIN
      L_FILE_NAME_I := 'wc_cataloggroups_i.csv'; --Added '_i' by Vamshi @1.6 v TMS#20150501-00013
      L_FILE_NAME_D := 'wc_cataloggroups_d.csv'; --Added by Vamshi @1.6 v TMS#20150501-00013

      L_FILE_I :=             --Added '_i' by Vamshi @1.6 v TMS#20150501-00013
         UTL_FILE.FOPEN (C_LOCAL_DIRECTORY,
                         L_FILE_NAME_I,
                         'W',
                         MAX_LINE_LENGTH);

      --Added below code by Vamshi @1.6 v TMS#20150501-00013

      L_FILE_D :=
         UTL_FILE.FOPEN (C_LOCAL_DIRECTORY,
                         L_FILE_NAME_D,
                         'W',
                         MAX_LINE_LENGTH);


      UTL_FILE.PUT_LINE (L_FILE_I, CHR (15711167)); --Added '_i' by Vamshi @1.6 v TMS#20150501-00013
      UTL_FILE.PUT_LINE (L_FILE_D, CHR (15711167)); --Added by Vamshi @1.6 v TMS#20150501-00013

      L_STRING := 'CatalogGroup';

      UTL_FILE.PUT_LINE (L_FILE_I, L_STRING); --Added '_i' by Vamshi @1.6 v TMS#20150501-00013
      UTL_FILE.PUT_LINE (L_FILE_D, L_STRING); --Added by Vamshi @1.6 v TMS#20150501-00013

      L_STRING :=
         'GroupIdentifier|TopGroup|ParentGroupIdentifier|Sequence|Name|ShortDescription|LongDescription|Thumbnail|FullImage|KeyWord|Delete';

      UTL_FILE.PUT_LINE (L_FILE_I, L_STRING); --Added '_i' by Vamshi @1.6 v TMS#20150501-00013
      UTL_FILE.PUT_LINE (L_FILE_D, L_STRING); --Added by Vamshi @1.6 v TMS#20150501-00013

      -- UTL_FILE.PUT_LINE (L_SALES_FILE, L_STRING);  -- Commented out w.r.t. ver 1.2.7 on 11/27/2013

      /* -- needs to be removed after testing  */


      SELECT category_Set_id, structure_id
        INTO l_category_Set_id, l_structure_id
        FROM mtl_category_sets
       WHERE category_Set_name = 'WC Web Hierarchy';

      L_STRING :=
            'WC Web Hierarchy|TRUE||1|WC Web Hierarchy|Alt Catalog for White Cap Web Hierarchy||'
         || fnd_profile.VALUE ('XXWC_CATEGORY_THUMBNAIL')
         || 'WC Web Hierarchy.jpg|'
         || fnd_profile.VALUE ('XXWC_CATEGORY_IMAGE_PATH')
         || 'WC Web Hierarchy.jpg||0';

      UTL_FILE.PUT_LINE (L_FILE_I, L_STRING); --Added 'i' by Vamshi @1.6 v TMS#20150501-00013
      UTL_FILE.PUT_LINE (L_FILE_D, L_STRING); --Added by Vamshi @1.6 v TMS#20150501-00013

      FOR CUR_DATA
         IN (    SELECT mck.concatenated_segments GroupIdentifier,
                        'FALSE' TopGroup,
                        NVL ( (SELECT concatenated_segments
                                 FROM MTL_CATEGORIES_B_KFV a
                                WHERE a.category_id = mcsvc.parent_category_id),
                             'WC Web Hierarchy')
                           ParentGroupIdentifier,
                        1 sequence,
                        mck.concatenated_segments Name,
                        mct.description ShortDescription,
                        NULL LongDescription,
                           fnd_profile.VALUE ('XXWC_CATEGORY_THUMBNAIL')
                        -- added on 12/14/2012 by Jyotsna
                        || 't_'
                        || TRANSLATE (
                              UPPER (TRIM (mck.concatenated_segments)),
                              '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ ~!@#$%^&*()_+}{":?><`-=]['''';/.,',
                              '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ')
                        --|| mck.concatenated_segments
                        || '.jpg'
                           Thumbnail,
                           fnd_profile.VALUE ('XXWC_CATEGORY_IMAGE_PATH')
                        -- added on 12/14/2012 by Jyotsna
                        || 'f_'
                        || TRANSLATE (
                              UPPER (TRIM (mck.concatenated_segments)),
                              '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ ~!@#$%^&*()_+}{":?><`-=]['''';/.,',
                              '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ')
                        --|| mck.concatenated_segments
                        || '.jpg'
                           FullImage,
                        NULL KeyWord,
                        0 Delete_flag
                   FROM MTL_CATEGORY_SET_VALID_CATS mcsvc,
                        MTL_CATEGORIES_B_KFV mck,
                        MTL_CATEGORIES_TL mct
                  WHERE     mcsvc.category_set_id = l_category_Set_id
                        AND mck.structure_id = l_structure_id
                        AND mcsvc.category_id = mck.category_id
                        AND mct.category_id = mck.category_id
             START WITH     mcsvc.parent_category_id IS NULL
                        AND mcsvc.CATEGORY_SET_ID = l_category_Set_id
             CONNECT BY     PRIOR mcsvc.CATEGORY_ID =
                               mcsvc.PARENT_CATEGORY_ID
                        AND mcsvc.CATEGORY_SET_ID = l_category_Set_id)
      LOOP
         L_STRING :=
               CUR_DATA.GroupIdentifier
            || L_DEL
            || CUR_DATA.TopGroup
            || L_DEL
            || CUR_DATA.ParentGroupIdentifier
            || L_DEL
            || CUR_DATA.Sequence
            || L_DEL
            || CUR_DATA.ShortDescription
            || L_DEL
            || CUR_DATA.ShortDescription
            || L_DEL
            || CUR_DATA.LongDescription
            || L_DEL
            || CUR_DATA.Thumbnail
            || L_DEL
            || CUR_DATA.FullImage
            || L_DEL
            || CUR_DATA.KeyWord
            || L_DEL
            || CUR_DATA.Delete_flag;

         IF CUR_DATA.Delete_flag = '0'
         THEN                     -- Added by Vamshi @1.6 v TMS#20150501-00013
            UTL_FILE.PUT_LINE (L_FILE_I, L_STRING); -- Added by Vamshi @1.6 v TMS#20150501-00013
         ELSE                     -- Added by Vamshi @1.6 v TMS#20150501-00013
            UTL_FILE.PUT_LINE (L_FILE_D, L_STRING); -- Added by Vamshi @1.6 v TMS#20150501-00013
         END IF;
      END LOOP;

      UTL_FILE.FCLOSE (L_FILE_I); --Added 'i' by Vamshi @1.6 v TMS#20150501-00013
      UTL_FILE.FCLOSE (L_FILE_D);  --Added by Vamshi @1.6 v TMS#20150501-00013
   EXCEPTION
      WHEN UTL_FILE.INTERNAL_ERROR
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            ' Debug INTERNAL_ERROR : ' || L_STRING);

         --Added 'i' by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         --Added by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;

         raise_application_error (
            -20500,
               'Cannot open file :'
            || L_FILE_NAME_I
            || '/'
            || L_FILE_NAME_D
            || ', internal error; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      WHEN UTL_FILE.INVALID_OPERATION
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            ' Debug INVALID_OPERATION : ' || L_STRING);

         --Added 'i' by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         --Added by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;

         raise_application_error (
            -20501,
               'Cannot open file :'
            || L_FILE_NAME_I
            || '/'
            || L_FILE_NAME_D
            || ', invalid operation; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      WHEN UTL_FILE.INVALID_PATH
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            ' Debug INVALID_PATH : ' || L_STRING);

         --Added 'i' by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         --Added by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;

         raise_application_error (
            -20502,
               'Cannot open file :'
            || L_FILE_NAME_I
            || '/'
            || L_FILE_NAME_D
            || ', invalid path; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      WHEN UTL_FILE.WRITE_ERROR
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            ' Debug WRITE_ERROR : ' || L_STRING);

         --Added 'i' by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         --Added by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;

         raise_application_error (
            -20503,
               'Cannot write to file :'
            || L_FILE_NAME_I
            || '/'
            || L_FILE_NAME_D
            || ', write error; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      WHEN OTHERS
      THEN
         fnd_file.put_line (
            fnd_file.LOG,
               ' Debug OTHERS : '
            || L_STRING
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());

         --Added 'i' by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_I)
         THEN
            UTL_FILE.FCLOSE (L_FILE_I);
         END IF;

         --Added by Vamshi @1.6 v TMS#20150501-00013
         IF UTL_FILE.is_open (L_FILE_D)
         THEN
            UTL_FILE.FCLOSE (L_FILE_D);
         END IF;
   END;

   /********************************************************************************
                                                                                                                                                                                                                                               PROGRAM TYPE: PROCEDURE
   NAME: CREATE_PROD_RECL_DATA
   PURPOSE: Procedure to generate the parent catalog group relationship data
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     11/29/2012    Rajiv Rathod    Initial creation of the procedure
   1.6     05/06/2015    P.Vamshidhar    TMS#20150501-00013 - PDH to Ecomm Data Interface Improvements
                                         To generate separate files for delete and insert/update.
    *********************************************************************************/

   PROCEDURE CREATE_PROD_RECL_DATA (P_TIMESTAMP       IN VARCHAR2,
                                    L_LAST_RUN_DATE      DATE)
   AS
   BEGIN
      L_FILE_NAME_I := 'wc_catalogentryparentcataloggroup_i.csv'; -- Added 'i' by Vamshi 1.6 V @TMS#20150501-00013
      L_FILE_NAME_D := 'wc_catalogentryparentcataloggroup_d.csv'; -- Added by Vamshi 1.6v @TMS#20150501-00013

      L_FILE_I :=
         UTL_FILE.FOPEN (C_LOCAL_DIRECTORY,
                         L_FILE_NAME_I, -- Added 'i' by Vamshi 1.6v @TMS#20150501-00013
                         'W',
                         MAX_LINE_LENGTH);

      -- Added below codwe by Vamshi in 1.6v @TMS#20150501-00013

      L_FILE_D :=
         UTL_FILE.FOPEN (C_LOCAL_DIRECTORY,
                         L_FILE_NAME_D,
                         'W',
                         MAX_LINE_LENGTH);

      UTL_FILE.PUT_LINE (L_FILE_I, CHR (15711167)); -- Added by Vamshi in 1.6v @TMS#20150501-00013
      UTL_FILE.PUT_LINE (L_FILE_D, CHR (15711167)); -- Added by Vamshi in 1.6v @TMS#20150501-00013

      L_STRING := 'CatalogEntryParentCatalogGroupRelationship';

      UTL_FILE.PUT_LINE (L_FILE_I, L_STRING); -- Added '_i' by Vamshi in 1.6V @TMS#20150501-00013
      UTL_FILE.PUT_LINE (L_FILE_D, L_STRING); -- Added by Vamshi  in 1.6V @TMS#20150501-00013

      L_STRING :=
         'PartNumber|ParentGroupIdentifier|ParentStoreIdentifier|Sequence|Delete';

      UTL_FILE.PUT_LINE (L_FILE_I, L_STRING); -- Added '_i' by Vamshi in 1.6V @TMS#20150501-00013
      UTL_FILE.PUT_LINE (L_FILE_D, L_STRING); -- Added by Vamshi  in 1.6V @TMS#20150501-00013

      FOR CUR_DATA2 IN (  SELECT *
                            FROM XXWC_CAT_CHANGE_TAB
                        ORDER BY SEQUENCE ASC)
      LOOP
         IF (CUR_DATA2.DMLTYPE = 'I')
         THEN
            -- FOR INSERT RECORDS
            FOR CUR_DATA1
               IN (  SELECT mcb.segment1 category,
                            'P_' || msib.segment1 item_number,
                            msib.segment1 original_item,    --Added by Vamshi in TMS#20150501-00013
                            0 delete_flag,
                            NULL ParentStoreIdentifier,
                            1 sequence
                       FROM XXWC.XXWC_CAT_CHANGE_TAB a,
                            mtl_system_items_b msib,
                            mtl_categories_b mcb,
                            MTL_CATEGORY_SETS MCSa,
                            MTL_ITEM_CATEGORIES MICa,
                            MTL_CATEGORIES_B MCBa
                      WHERE     a.organization_id = l_master_org_id
                            AND a.organization_id = msib.organization_id
                            AND a.inventory_item_id = msib.inventory_item_id
                            AND a.category_id_new = mcb.category_id
                            AND a.category_id_new IS NOT NULL
                            --AND MCSa.CATEGORY_SET_NAME = 'Website Item'   Commented by Vamshi in TMS#20150501-00013
                            AND MCSa.CATEGORY_SET_NAME = 'WC Web Hierarchy' --Added by Vamshi in TMS#20150501-00013
                            AND MICa.CATEGORY_SET_ID = MCSa.CATEGORY_SET_ID
                            AND mcba.CATEGORY_ID = MICa.CATEGORY_ID
                            AND MICa.ORGANIZATION_ID = l_master_org_id
                            AND MICa.INVENTORY_ITEM_ID = a.inventory_item_id
                            --AND mcba.segment1 = 'Y'  Commented by Vamshi in TMS#20150501-00013
                            AND A.SEQUENCE = CUR_DATA2.SEQUENCE
                   ORDER BY a.last_update_date ASC)
            LOOP
               L_STRING :=
                     CUR_DATA1.item_number
                  || L_DEL
                  || CUR_DATA1.category
                  || L_DEL
                  || CUR_DATA1.ParentStoreIdentifier
                  || L_DEL
                  || CUR_DATA1.Sequence
                  || L_DEL
                  || CUR_DATA1.delete_flag;

               IF CUR_DATA1.Delete_flag = '0'
               THEN               -- Added by Vamshi @1.6 v TMS#20150501-00013
                  UTL_FILE.PUT_LINE (L_FILE_I, L_STRING); -- Added by Vamshi @1.6 v TMS#20150501-00013
               ELSE               -- Added by Vamshi @1.6 v TMS#20150501-00013
                  UTL_FILE.PUT_LINE (L_FILE_D, L_STRING); -- Added by Vamshi @1.6 v TMS#20150501-00013
               END IF;

               -- Added below code by Vamshi TMS#20150501-00013 -- Start
               L_STRING :=
                     CUR_DATA1.original_item
                  || L_DEL
                  || CUR_DATA1.category
                  || L_DEL
                  || CUR_DATA1.ParentStoreIdentifier
                  || L_DEL
                  || CUR_DATA1.Sequence
                  || L_DEL
                  || CUR_DATA1.delete_flag;

               IF CUR_DATA1.Delete_flag = '0'
               THEN
                  UTL_FILE.PUT_LINE (L_FILE_I, L_STRING);
               ELSE
                  UTL_FILE.PUT_LINE (L_FILE_D, L_STRING);
               END IF;
               -- Added below code by Vamshi TMS#20150501-00013 -- End

            END LOOP;
         END IF;

         IF (CUR_DATA2.DMLTYPE = 'D')
         THEN
            --FOR DELETE RECORDS
            FOR CUR_DATA1
               IN (  SELECT mcb.segment1 category,
                            'P_' || msib.segment1 item_number,
                            msib.segment1 original_item,   --Added by Vamshi in TMS#20150501-00013
                            1 delete_flag,
                            NULL ParentStoreIdentifier,
                            1 sequence
                       FROM XXWC.XXWC_CAT_CHANGE_TAB a,
                            mtl_system_items_b msib,
                            mtl_categories_b mcb,
                            MTL_CATEGORY_SETS MCSa,
                            MTL_ITEM_CATEGORIES MICa,
                            MTL_CATEGORIES_B MCBa
                      WHERE     a.organization_id = l_master_org_id
                            AND a.organization_id = msib.organization_id
                            AND a.inventory_item_id = msib.inventory_item_id
                            AND a.category_id_OLD = mcb.category_id
                            AND a.category_id_new IS NULL
                            -- AND MCSa.CATEGORY_SET_NAME = 'Website Item'  Commented by Vamshi in TMS#20150501-00013
                            AND MCSa.CATEGORY_SET_NAME = 'WC Web Hierarchy' --Added by Vamshi in TMS#20150501-00013
                            AND MICa.CATEGORY_SET_ID = MCSa.CATEGORY_SET_ID
                            AND mcba.CATEGORY_ID = MICa.CATEGORY_ID
                            AND MICa.ORGANIZATION_ID = l_master_org_id
                            AND MICa.INVENTORY_ITEM_ID = a.inventory_item_id
                            --AND mcba.segment1 = 'Y' Commented by Vamshi in TMS#20150501-00013
                            AND A.SEQUENCE = CUR_DATA2.SEQUENCE
                   ORDER BY a.last_update_date ASC)
            LOOP
               L_STRING :=
                     CUR_DATA1.item_number
                  || L_DEL
                  || CUR_DATA1.category
                  || L_DEL
                  || CUR_DATA1.ParentStoreIdentifier
                  || L_DEL
                  || CUR_DATA1.Sequence
                  || L_DEL
                  || CUR_DATA1.delete_flag;

               IF CUR_DATA1.Delete_flag = '0'
               THEN               -- Added by Vamshi @1.6 v TMS#20150501-00013
                  UTL_FILE.PUT_LINE (L_FILE_I, L_STRING); -- Added by Vamshi @1.6 v TMS#20150501-00013
               ELSE               -- Added by Vamshi @1.6 v TMS#20150501-00013
                  UTL_FILE.PUT_LINE (L_FILE_D, L_STRING); -- Added by Vamshi @1.6 v TMS#20150501-00013
               END IF;

               -- Added below code by Vamshi TMS#20150501-00013 -- Start
               L_STRING :=
                     CUR_DATA1.original_item
                  || L_DEL
                  || CUR_DATA1.category
                  || L_DEL
                  || CUR_DATA1.ParentStoreIdentifier
                  || L_DEL
                  || CUR_DATA1.Sequence
                  || L_DEL
                  || CUR_DATA1.delete_flag;

               IF CUR_DATA1.Delete_flag = '0'
               THEN
                  UTL_FILE.PUT_LINE (L_FILE_I, L_STRING);
               ELSE
                  UTL_FILE.PUT_LINE (L_FILE_D, L_STRING);
               END IF;

               -- Added below code by Vamshi TMS#20150501-00013 -- End

            END LOOP;
         END IF;

         IF (CUR_DATA2.DMLTYPE = 'U')
         THEN
            FOR CUR_DATA1
               -- IN CASE OF UPDATE FIRST DELETE RECORD
            IN (  SELECT mcb.segment1 category,
                         'P_' || msib.segment1 item_number,
                         msib.segment1 original_item,   --Added by Vamshi in TMS#20150501-00013
                         1 delete_flag,
                         NULL ParentStoreIdentifier,
                         1 sequence
                    FROM XXWC.XXWC_CAT_CHANGE_TAB a,
                         mtl_system_items_b msib,
                         mtl_categories_b mcb,
                         MTL_CATEGORY_SETS MCSa,
                         MTL_ITEM_CATEGORIES MICa,
                         MTL_CATEGORIES_B MCBa
                   WHERE     a.organization_id = l_master_org_id
                         AND a.organization_id = msib.organization_id
                         AND a.inventory_item_id = msib.inventory_item_id
                         AND a.category_id_OLD = mcb.category_id
                         --  AND a.category_id_new IS  NULL
                         -- AND MCSa.CATEGORY_SET_NAME = 'Website Item'  Commented by Vamshi in TMS#20150501-00013
                         AND MCSa.CATEGORY_SET_NAME = 'WC Web Hierarchy' --Added by Vamshi in TMS#20150501-00013
                         AND MICa.CATEGORY_SET_ID = MCSa.CATEGORY_SET_ID
                         AND mcba.CATEGORY_ID = MICa.CATEGORY_ID
                         AND MICa.ORGANIZATION_ID = l_master_org_id
                         AND MICa.INVENTORY_ITEM_ID = a.inventory_item_id
                         -- AND mcba.segment1 = 'Y' Commented by Vamshi in TMS#20150501-00013
                         AND A.SEQUENCE = CUR_DATA2.SEQUENCE
                ORDER BY a.last_update_date ASC)
            LOOP
               L_STRING :=
                     CUR_DATA1.item_number
                  || L_DEL
                  || CUR_DATA1.category
                  || L_DEL
                  || CUR_DATA1.ParentStoreIdentifier
                  || L_DEL
                  || CUR_DATA1.Sequence
                  || L_DEL
                  || CUR_DATA1.delete_flag;

               IF CUR_DATA1.Delete_flag = '0'
               THEN               -- Added by Vamshi @1.6 v TMS#20150501-00013
                  UTL_FILE.PUT_LINE (L_FILE_I, L_STRING); -- Added by Vamshi @1.6 v TMS#20150501-00013
               ELSE               -- Added by Vamshi @1.6 v TMS#20150501-00013
                  UTL_FILE.PUT_LINE (L_FILE_D, L_STRING); -- Added by Vamshi @1.6 v TMS#20150501-00013
               END IF;

               -- Added below code by Vamshi TMS#20150501-00013 -- Start

               L_STRING :=
                     CUR_DATA1.original_item
                  || L_DEL
                  || CUR_DATA1.category
                  || L_DEL
                  || CUR_DATA1.ParentStoreIdentifier
                  || L_DEL
                  || CUR_DATA1.Sequence
                  || L_DEL
                  || CUR_DATA1.delete_flag;

               IF CUR_DATA1.Delete_flag = '0'
               THEN
                  UTL_FILE.PUT_LINE (L_FILE_I, L_STRING);
               ELSE
                  UTL_FILE.PUT_LINE (L_FILE_D, L_STRING);
               END IF;
               -- Added below code by Vamshi TMS#20150501-00013 -- End

            END LOOP;

            -- IN CASE OF UPDATE SECOND CREATE NEW RECORD
            FOR CUR_DATA1
               IN (  SELECT mcb.segment1 category,
                            'P_' || msib.segment1 item_number,
                            msib.segment1 original_item,   --Added by Vamshi in TMS#20150501-00013
                            0 delete_flag,
                            NULL ParentStoreIdentifier,
                            1 sequence
                       FROM XXWC.XXWC_CAT_CHANGE_TAB a,
                            mtl_system_items_b msib,
                            mtl_categories_b mcb,
                            MTL_CATEGORY_SETS MCSa,
                            MTL_ITEM_CATEGORIES MICa,
                            MTL_CATEGORIES_B MCBa
                      WHERE     a.organization_id = l_master_org_id
                            AND a.organization_id = msib.organization_id
                            AND a.inventory_item_id = msib.inventory_item_id
                            AND a.category_id_NEW = mcb.category_id
                            --  AND a.category_id_new IS  NULL
                            -- AND MCSa.CATEGORY_SET_NAME = 'Website Item' Commented by Vamshi in TMS#20150501-00013
                            AND MCSa.CATEGORY_SET_NAME = 'WC Web Hierarchy' --Added by Vamshi in TMS#20150501-00013
                            AND MICa.CATEGORY_SET_ID = MCSa.CATEGORY_SET_ID
                            AND mcba.CATEGORY_ID = MICa.CATEGORY_ID
                            AND MICa.ORGANIZATION_ID = l_master_org_id
                            AND MICa.INVENTORY_ITEM_ID = a.inventory_item_id
                            --AND mcba.segment1 = 'Y'  Commented by Vamshi in TMS#20150501-00013
                            AND A.SEQUENCE = CUR_DATA2.SEQUENCE
                   ORDER BY a.last_update_date ASC)
            LOOP
               L_STRING :=
                     CUR_DATA1.item_number
                  || L_DEL
                  || CUR_DATA1.category
                  || L_DEL
                  || CUR_DATA1.ParentStoreIdentifier
                  || L_DEL
                  || CUR_DATA1.Sequence
                  || L_DEL
                  || CUR_DATA1.delete_flag;

               IF CUR_DATA1.Delete_flag = '0'
               THEN               -- Added by Vamshi @1.6 v TMS#20150501-00013
                  UTL_FILE.PUT_LINE (L_FILE_I, L_STRING); -- Added by Vamshi @1.6 v TMS#20150501-00013
               ELSE               -- Added by Vamshi @1.6 v TMS#20150501-00013
                  UTL_FILE.PUT_LINE (L_FILE_D, L_STRING); -- Added by Vamshi @1.6 v TMS#20150501-00013
               END IF;

               -- Added below code by Vamshi TMS#20150501-00013 -- Start
               L_STRING :=
                     CUR_DATA1.original_item
                  || L_DEL
                  || CUR_DATA1.category
                  || L_DEL
                  || CUR_DATA1.ParentStoreIdentifier
                  || L_DEL
                  || CUR_DATA1.Sequence
                  || L_DEL
                  || CUR_DATA1.delete_flag;

               IF CUR_DATA1.Delete_flag = '0'
               THEN
                  UTL_FILE.PUT_LINE (L_FILE_I, L_STRING);
               ELSE
                  UTL_FILE.PUT_LINE (L_FILE_D, L_STRING);
               END IF;
               -- Added below code by Vamshi TMS#20150501-00013 -- End

            END LOOP;
         END IF;
      END LOOP;

      UTL_FILE.FCLOSE (L_FILE_I); -- Added '_i' by vamshi in 1.6V @TMS#20150501-00013
      UTL_FILE.FCLOSE (L_FILE_D); -- Added by Vamshi @1.6 v @TMS#20150501-00013

      EXECUTE IMMEDIATE 'truncate table XXWC.XXWC_CAT_CHANGE_TAB';
   EXCEPTION
      WHEN UTL_FILE.INTERNAL_ERROR
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            ' Debug INTERNAL_ERROR : ' || L_STRING);

         IF UTL_FILE.is_open (L_FILE_I) -- Added '_i' by Vamshi in 1.6V @ @TMS#20150501-00013
         THEN
            UTL_FILE.FCLOSE (L_FILE_I); -- Added '_i' by Vamshi in 1.6V @ @TMS#20150501-00013
         END IF;

         IF UTL_FILE.is_open (L_FILE_D) -- Added by Vamshi in 1.6V @ @TMS#20150501-00013
         THEN
            UTL_FILE.FCLOSE (L_FILE_D); -- Added by Vamshi in 1.6V @ @TMS#20150501-00013
         END IF;

         raise_application_error (
            -20500,
               'Cannot open file :'
            || L_FILE_NAME_I
            || '/'
            || L_FILE_NAME_D
            || ', internal error; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      WHEN UTL_FILE.INVALID_OPERATION
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            ' Debug INVALID_OPERATION : ' || L_STRING);

         IF UTL_FILE.is_open (L_FILE_I) -- Added '_i' by Vamshi in 1.6V @ @TMS#20150501-00013
         THEN
            UTL_FILE.FCLOSE (L_FILE_I); -- Added '_i' by Vamshi in 1.6V @ @TMS#20150501-00013
         END IF;

         IF UTL_FILE.is_open (L_FILE_D) -- Added by Vamshi in 1.6V @ @TMS#20150501-00013
         THEN
            UTL_FILE.FCLOSE (L_FILE_D); -- Added by Vamshi in 1.6V @ @TMS#20150501-00013
         END IF;

         raise_application_error (
            -20501,
               'Cannot open file :'
            || L_FILE_NAME_I
            || '/'
            || L_FILE_NAME_D
            || ', invalid operation; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      WHEN UTL_FILE.INVALID_PATH
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            ' Debug INVALID_PATH : ' || L_STRING);

         IF UTL_FILE.is_open (L_FILE_I) -- Added '_i' by Vamshi in 1.6V @ @TMS#20150501-00013
         THEN
            UTL_FILE.FCLOSE (L_FILE_I); -- Added '_i' by Vamshi in 1.6V @ @TMS#20150501-00013
         END IF;

         IF UTL_FILE.is_open (L_FILE_D) -- Added by Vamshi in 1.6V @ @TMS#20150501-00013
         THEN
            UTL_FILE.FCLOSE (L_FILE_D); -- Added by Vamshi in 1.6V @ @TMS#20150501-00013
         END IF;

         raise_application_error (
            -20502,
               'Cannot open file :'
            || L_FILE_NAME_I
            || '/'
            || L_FILE_NAME_D
            || ', invalid path; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      WHEN UTL_FILE.WRITE_ERROR
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            ' Debug WRITE_ERROR : ' || L_STRING);

         IF UTL_FILE.is_open (L_FILE_I) -- Added '_i' by Vamshi in 1.6V @ @TMS#20150501-00013
         THEN
            UTL_FILE.FCLOSE (L_FILE_I); -- Added '_i' by Vamshi in 1.6V @ @TMS#20150501-00013
         END IF;

         IF UTL_FILE.is_open (L_FILE_D) -- Added by Vamshi in 1.6V @ @TMS#20150501-00013
         THEN
            UTL_FILE.FCLOSE (L_FILE_D); -- Added by Vamshi in 1.6V @ @TMS#20150501-00013
         END IF;

         raise_application_error (
            -20503,
               'Cannot write to file :'
            || L_FILE_NAME_I
            || '/'
            || L_FILE_NAME_D
            || ', write error; code:'
            || SQLCODE
            || ',message:'
            || SQLERRM
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());
      WHEN OTHERS
      THEN
         fnd_file.put_line (
            fnd_file.LOG,
               ' Debug OTHERS : '
            || L_STRING
            || ' Details: '
            || DBMS_UTILITY.format_error_backtrace ());

         IF UTL_FILE.is_open (L_FILE_I) -- Added '_i' by Vamshi in 1.6V @ @TMS#20150501-00013
         THEN
            UTL_FILE.FCLOSE (L_FILE_I); -- Added '_i' by Vamshi in 1.6V @ @TMS#20150501-00013
         END IF;

         IF UTL_FILE.is_open (L_FILE_D) -- Added by Vamshi in 1.6V @ @TMS#20150501-00013
         THEN
            UTL_FILE.FCLOSE (L_FILE_D); -- Added by Vamshi in 1.6V @ @TMS#20150501-00013
         END IF;
   END;

   /********************************************************************************
                                                                                                                                                                                                                              PROGRAM TYPE: PROCEDURE
      NAME: PUB_WCS_PROC
      PURPOSE: Main procedure to generate the extracts
      HISTORY
      ===============================================================================
      VERSION DATE          AUTHOR(S)       DESCRIPTION
      ------- -----------   --------------- -----------------------------------------
      1.0     09/23/2012    Rajiv Rathod    Initial creation of the procedure
      1.5     03/10/2015    Vamshidhar      TMS#20150309-00325 -  To improve performance rewrote one query and details are below.
                                            To imporove performance used 'XXWC_QP_ECOMMERCE_ITEMS_MV' materilized view
       ********************************************************************************/
   PROCEDURE PUB_WCS_PROC (ERRBUF                        OUT NOCOPY VARCHAR2,
                           RETCODE                       OUT NOCOPY VARCHAR2,
                           ITEM_DATA                  IN            VARCHAR2,
                           ITEM_ATTRIBUTE             IN            VARCHAR2,
                           ATTR_DICTIONARY            IN            VARCHAR2,
                           PRODUCT_HIERARCHY          IN            VARCHAR2,
                           PRODUCT_RECLASSIFICATION   IN            VARCHAR2,
                           FROM_DATE                  IN            VARCHAR2)
   AS
   BEGIN
      fnd_file.put_line (
         fnd_file.LOG,
         'Starting the Publish program FROM_DATE: ' || FROM_DATE);
      L_LAST_RUN_DATE := TO_DATE (FROM_DATE, 'yyyy/mm/dd hh24:mi:ss');

      IF (L_LAST_RUN_DATE IS NOT NULL)
      THEN
         L_LAST_RUN_DATE :=
            NVL (TO_DATE (FROM_DATE, 'yyyy/mm/dd hh24:mi:ss'), SYSDATE - 50);
      END IF;

      fnd_file.put_line (fnd_file.LOG,
                         'L_LAST_RUN_DATE: ' || L_LAST_RUN_DATE);

      SELECT TO_CHAR (CURRENT_TIMESTAMP, 'DD-MON-RRRR HH24:MI:SS')
        INTO L_TIMESTAMP
        FROM DUAL;

      SELECT TO_NUMBER (fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG'))
        INTO l_master_org_id
        FROM DUAL;

      IF (L_LAST_RUN_DATE IS NULL)
      THEN
         BEGIN
            SELECT concurrent_program_id
              INTO L_CONC_PRGM_ID
              FROM fnd_concurrent_programs_VL
             WHERE USER_CONCURRENT_PROGRAM_NAME = 'XXWC Publish data to WCS';

            SELECT MAX (REQUEST_ID)
              INTO L_REQUEST_ID
              FROM FND_CONCURRENT_REQUESTS
             WHERE     CONCURRENT_PROGRAM_ID = L_CONC_PRGM_ID
                   AND PHASE_CODE = 'C'
                   AND STATUS_CODE = 'C';
         EXCEPTION
            WHEN OTHERS
            THEN
               L_CONC_PRGM_ID := NULL;
         END;

         IF L_REQUEST_ID IS NOT NULL
         THEN
            SELECT LAST_UPDATE_DATE
              INTO L_LAST_RUN_DATE
              FROM FND_CONCURRENT_REQUESTS
             WHERE REQUEST_ID = L_REQUEST_ID;
         ELSE
            L_LAST_RUN_DATE :=
               NVL (TO_DATE (FROM_DATE, 'yyyy/mm/dd hh24:mi:ss'),
                    SYSDATE - 50);
         --L_LAST_RUN_DATE:= null;
         END IF;
      END IF;

      -- Added MV refresh TMS#20150309-00325 -- 1.5 v
      fnd_file.put_line (
         fnd_file.LOG,
            ' XXWC_QP_ECOMMERCE_ITEMS_MV MV refresh: started - '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

      DBMS_MVIEW.refresh (list => 'XXWC_QP_ECOMMERCE_ITEMS_MV', method => 'C'); -- C=complete  F=fast  ?=force  A=always

      fnd_file.put_line (
         fnd_file.LOG,
            ' XXWC_QP_ECOMMERCE_ITEMS_MV MV refresh: Completed -  '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));


      -- Insert in to custom table based on last update date
      EXECUTE IMMEDIATE 'truncate table xxwc.XXWC_EGO_MOD_ITEMS_TAB';

      /* Commented below query in TMS#20150309-00325 and wrote new query to improve performance.*/

      -- updated for query optimization on 01/18/2013
      --      INSERT /*+ append */
      --            INTO  XXWC.XXWC_EGO_MOD_ITEMS_TAB
      --         WITH QTL
      --              AS (  SELECT MAX (X.LAST_UPDATE_DATE) LAST_UPDATE_DATE_BASE,
      --                           MAX (y.last_update_date) LAST_UPDATE_DATE_LANG,
      --                           y.inventory_item_id,
      --                           Y.ORGANIZATION_ID
      --                      FROM apps.EGO_MTL_SY_ITEMS_EXT_B x,
      --                           apps.EGO_MTL_SY_ITEMS_EXT_TL y
      --                     WHERE     x.inventory_item_id = y.inventory_item_id
      --                           AND x.organization_id = y.organization_id
      --                           AND Y.ORGANIZATION_ID = l_master_org_id
      --                  GROUP BY Y.INVENTORY_ITEM_ID, Y.ORGANIZATION_ID)
      --         SELECT /*+ parallel(msib,16) */
      --               msib.segment1 item_number,
      --                msib.inventory_item_id,
      --                mcba.segment1 web_hierarchy,
      --                mica.last_update_date,
      --                mcb.segment1 web_flag,
      --                mic.last_update_date,
      --                --commented below code by Rajiv in version 1.2.4 to fix the temporary non impacting performance issue
      --                /*        (SELECT   TO_NUMBER (operand) operand
      --                               FROM   apps.qp_list_headers_all a, apps.qp_list_lines_v b
      --                               WHERE       NVL (b.END_DATE_ACTIVE, SYSDATE) >= SYSDATE
      --                                        AND a.active_flag = 'Y'
      --                                        AND a.list_header_id = b.list_header_id
      --                                        AND a.name LIKE 'CATEGORY%'
      --                                        AND b.product_attr_value = TO_CHAR (msib.inventory_item_id)
      --                                        AND ROWNUM = 1) price,
      --                              (select   B.LAST_UPDATE_DATE
      --                                 FROM   apps.qp_list_headers_all a, apps.qp_list_lines_v b
      --                                WHERE       NVL (b.END_DATE_ACTIVE, SYSDATE) >= SYSDATE
      --                                        AND a.active_flag = 'Y'
      --                                        AND a.list_header_id = b.list_header_id
      --                                        AND a.name LIKE 'CATEGORY%'
      --                                        AND b.product_attr_value = TO_CHAR (msib.inventory_item_id)
      --                                        AND ROWNUM = 1)
      --                                 last_update_date_p, */
      --                0 price, -- added by rajiv in version 1.2.4 to fix the performance issue on 6-NOV-13
      --                NULL last_update_date_p, -- added by rajiv in version 1.2.4 to fix the performance issue on 6-NOV-13
      --                MSIB.LAST_UPDATE_DATE LAST_UPDATE_DATE_MSIB,
      --                -- null,
      --                QTL.LAST_UPDATE_DATE_BASE,
      --                --null,
      --                qtl.LAST_UPDATE_DATE_LANG,
      --                NULL REQUEST_ID
      --           FROM APPS.MTL_CATEGORY_SETS MCS,
      --                APPS.MTL_ITEM_CATEGORIES MIC,
      --                APPS.MTL_CATEGORIES_B MCB,
      --                APPS.MTL_SYSTEM_ITEMS_B MSIB,
      --                APPS.MTL_CATEGORY_SETS MCSA,
      --                APPS.MTL_ITEM_CATEGORIES MICA,
      --                APPS.MTL_CATEGORIES_B MCBA,
      --                QTL
      --          WHERE     MCS.CATEGORY_SET_NAME = 'Website Item'
      --                AND MIC.CATEGORY_SET_ID = MCS.CATEGORY_SET_ID
      --                AND MIC.INVENTORY_ITEM_ID = msib.inventory_item_id
      --                AND MIC.ORGANIZATION_ID = MSIB.ORGANIZATION_ID
      --                AND QTL.INVENTORY_ITEM_ID = MSIB.INVENTORY_ITEM_ID
      --                AND qtl.ORGANIZATION_ID = MSIB.ORGANIZATION_ID
      --                AND mcb.CATEGORY_ID = MIC.CATEGORY_ID
      --                AND MSIB.ORGANIZATION_ID = l_master_org_id
      --                AND mcb.segment1 = 'Y'
      --                AND MCSa.CATEGORY_SET_NAME = 'WC Web Hierarchy'
      --                AND MICa.CATEGORY_SET_ID = MCSa.CATEGORY_SET_ID
      --                AND MICa.INVENTORY_ITEM_ID = msib.inventory_item_id
      --                AND MICA.ORGANIZATION_ID = MSIB.ORGANIZATION_ID
      --                AND MCBA.CATEGORY_ID = MICA.CATEGORY_ID;
      --
      --      COMMIT;


      -- Added w.r.t. ver 1.2.6 on 11/26/2013 to select all --
      -- the items having Website Item catalog value as 'C' --*/
      --      INSERT /*+ append */
      --            INTO  XXWC.XXWC_EGO_MOD_ITEMS_TAB
      --         WITH QTL
      --              AS (  SELECT MAX (X.LAST_UPDATE_DATE) LAST_UPDATE_DATE_BASE,
      --                           MAX (y.last_update_date) LAST_UPDATE_DATE_LANG,
      --                           y.inventory_item_id,
      --                           Y.ORGANIZATION_ID
      --                      FROM apps.EGO_MTL_SY_ITEMS_EXT_B x,
      --                           apps.EGO_MTL_SY_ITEMS_EXT_TL y
      --                     WHERE     x.inventory_item_id = y.inventory_item_id
      --                           AND x.organization_id = y.organization_id
      --                           AND Y.ORGANIZATION_ID = l_master_org_id
      --                  GROUP BY Y.INVENTORY_ITEM_ID, Y.ORGANIZATION_ID)
      --         SELECT /*+ parallel(msib,16) */
      --               msib.segment1 item_number,
      --                msib.inventory_item_id,
      --                mcba.segment1 web_hierarchy,
      --                mica.last_update_date,
      --                mcb.segment1 web_flag,
      --                mic.last_update_date,
      --                0 price,
      --                NULL last_update_date_p,
      --                MSIB.LAST_UPDATE_DATE LAST_UPDATE_DATE_MSIB,
      --                QTL.LAST_UPDATE_DATE_BASE,
      --                qtl.LAST_UPDATE_DATE_LANG,
      --                NULL REQUEST_ID
      --           FROM APPS.MTL_CATEGORY_SETS MCS,
      --                APPS.MTL_ITEM_CATEGORIES MIC,
      --                APPS.MTL_CATEGORIES_B MCB,
      --                APPS.MTL_SYSTEM_ITEMS_B MSIB,
      --                APPS.MTL_CATEGORY_SETS MCSA,
      --                APPS.MTL_ITEM_CATEGORIES MICA,
      --                APPS.MTL_CATEGORIES_B MCBA,
      --                QTL
      --          WHERE     MCS.CATEGORY_SET_NAME = 'Website Item'
      --                AND MIC.CATEGORY_SET_ID = MCS.CATEGORY_SET_ID
      --                AND MIC.INVENTORY_ITEM_ID = msib.inventory_item_id
      --                AND MIC.ORGANIZATION_ID = MSIB.ORGANIZATION_ID
      --                AND QTL.INVENTORY_ITEM_ID = MSIB.INVENTORY_ITEM_ID
      --                AND qtl.ORGANIZATION_ID = MSIB.ORGANIZATION_ID
      --                AND mcb.CATEGORY_ID = MIC.CATEGORY_ID
      --                AND MSIB.ORGANIZATION_ID = l_master_org_id
      --                AND mcb.segment1 = 'C'
      --                AND MCSa.CATEGORY_SET_NAME = 'WC Web Hierarchy'
      --                AND MICa.CATEGORY_SET_ID = MCSa.CATEGORY_SET_ID
      --                AND MICa.INVENTORY_ITEM_ID = msib.inventory_item_id
      --                AND MICA.ORGANIZATION_ID = MSIB.ORGANIZATION_ID
      --                AND MCBA.CATEGORY_ID = MICA.CATEGORY_ID;
      --
      -- Added w.r.t. ver 1.2.6 on 11/26/2013 to select all --
      -- the items having Website Item catalog value as 'C' --
      -- End of code modification w.r.t. ver 1.2.6 --
      -- Added below code to replace above code through TMS#20150309-00325
      -- Start

      INSERT /*+ append */
            INTO  XXWC.XXWC_EGO_MOD_ITEMS_TAB
         WITH QTL
              AS (  SELECT MAX (X.LAST_UPDATE_DATE) LAST_UPDATE_DATE_BASE,
                           MAX (y.last_update_date) LAST_UPDATE_DATE_LANG,
                           y.inventory_item_id,
                           Y.ORGANIZATION_ID
                      FROM apps.EGO_MTL_SY_ITEMS_EXT_B x,
                           apps.EGO_MTL_SY_ITEMS_EXT_TL y
                     WHERE     x.inventory_item_id = y.inventory_item_id
                           AND x.organization_id = y.organization_id
                           AND Y.ORGANIZATION_ID = l_master_org_id
                  GROUP BY Y.INVENTORY_ITEM_ID, Y.ORGANIZATION_ID)
         SELECT XQEM.item_number,
                XQEM.inventory_item_id,
                MCBA.SEGMENT1 web_hierarchy,
                mica.last_update_date,
                XQEM.web_flag,
                XQEM.last_update_date,
                0 price,
                NULL LAST_UPDATE_DATE_P,
                xqem.LAST_UPDATE_DATE_MSIB,
                QTL.LAST_UPDATE_DATE_BASE,
                qtl.LAST_UPDATE_DATE_LANG,
                NULL REQUEST_ID
           FROM APPS.XXWC_QP_ECOMMERCE_ITEMS_MV XQEM,
                APPS.MTL_ITEM_CATEGORIES MICA,
                APPS.MTL_CATEGORY_SETS MCSA,
                APPS.MTL_CATEGORIES_B MCBA,
                QTL
          WHERE     XQEM.INVENTORY_ITEM_ID = MICA.INVENTORY_ITEM_ID
                AND XQEM.ORGANIZATION_ID = MICA.ORGANIZATION_ID
                AND MCSA.CATEGORY_SET_ID = MICA.CATEGORY_SET_ID
                AND MCSA.CATEGORY_SET_NAME = 'WC Web Hierarchy'
                AND MICA.CATEGORY_ID = MCBA.CATEGORY_ID
                AND QTL.INVENTORY_ITEM_ID = XQEM.INVENTORY_ITEM_ID
                AND QTL.ORGANIZATION_ID = XQEM.ORGANIZATION_ID;

      -- Added below code to replace above code through TMS#20150309-00325 -  End

      COMMIT;



      DELETE FROM xxwc.XXWC_EGO_MOD_ITEMS_TAB a
            WHERE a.inventory_item_id NOT IN (SELECT b.inventory_item_id
                                                FROM xxwc.XXWC_EGO_MOD_ITEMS_TAB b
                                               WHERE (   (LAST_UPDATE_DATE_WH >=
                                                             L_LAST_RUN_DATE)
                                                      OR (LAST_UPDATE_DATE_WF >=
                                                             L_LAST_RUN_DATE)
                                                      OR (LAST_UPDATE_DATE_MSIB >=
                                                             L_LAST_RUN_DATE)
                                                      OR (LAST_UPDATE_DATE_BASE >=
                                                             L_LAST_RUN_DATE)
                                                      OR (LAST_UPDATE_DATE_LANG >=
                                                             L_LAST_RUN_DATE) --           OR (LAST_UPDATE_DATE_P >=L_LAST_RUN_DATE) -- commented by rajiv in version 1.2.4 to fix the performance issue
                                                                             ));

      COMMIT;

      DELETE FROM xxwc.XXWC_EGO_MOD_ITEMS_TAB a
            WHERE a.inventory_item_id IN (SELECT msib.inventory_item_id
                                            FROM mtl_system_items_b msib
                                           WHERE     a.inventory_item_id =
                                                        msib.inventory_item_id
                                                 AND msib.organization_id =
                                                        l_master_org_id
                                                 AND NVL (unit_weight, 0) <=
                                                        0);

      COMMIT;


      IF PRODUCT_HIERARCHY = 'Y'
      THEN
         fnd_file.put_line (
            fnd_file.LOG,
               'Creating Product Hierarchy files FROM DATE: '
            || L_LAST_RUN_DATE
            || '-'
            || FROM_DATE);
         CREATE_PROD_HIER_DATA (L_TIMESTAMP, L_LAST_RUN_DATE, FROM_DATE);

         DELETE FROM XXWC.XXWC_EGO_EVENT_LOG_TAB;

         COMMIT;
      END IF;

      IF ATTR_DICTIONARY = 'Y'
      THEN
         fnd_file.put_line (
            fnd_file.LOG,
               ' Creating Attribute Dictionary files. L_LAST_RUN_DATE : '
            || L_LAST_RUN_DATE);
         CREATE_ITEM_DICTNRY_DATA (L_TIMESTAMP, L_LAST_RUN_DATE);
      END IF;

      IF ITEM_DATA = 'Y'
      THEN
         fnd_file.put_line (
            fnd_file.LOG,
            'Creating Item Files. L_LAST_RUN_DATE : ' || L_LAST_RUN_DATE);
         CREATE_ITEM_DATA (L_TIMESTAMP, L_LAST_RUN_DATE);
      END IF;

      IF ITEM_ATTRIBUTE = 'Y'
      THEN
         fnd_file.put_line (
            fnd_file.LOG,
               ' Creating Item Attributes Files. L_LAST_RUN_DATE : '
            || L_LAST_RUN_DATE);
         CREATE_ITEM_ATTR_DATA (L_TIMESTAMP, L_LAST_RUN_DATE);
      END IF;


      IF PRODUCT_RECLASSIFICATION = 'Y'
      THEN
         fnd_file.put_line (
            fnd_file.LOG,
               'Creating Product Re-classification files. L_LAST_RUN_DATE: '
            || L_LAST_RUN_DATE);
         CREATE_PROD_RECL_DATA (L_TIMESTAMP, L_LAST_RUN_DATE);
      END IF;



      COMMIT;
      errbuf := '';
      RETCODE := '0';
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf :=
            SQLERRM || ' Details: ' || DBMS_UTILITY.format_error_backtrace ();
         RETCODE := '2';
   --dbms_output.put_line( SQLCODE || sqlerrm);

   END PUB_WCS_PROC;

   PROCEDURE insert_event_log (p_event_name       VARCHAR2 DEFAULT NULL,
                               p_event_key        VARCHAR2 DEFAULT NULL,
                               p_param_name1      VARCHAR2 DEFAULT NULL,
                               p_param_value1     VARCHAR2 DEFAULT NULL,
                               p_param_name2      VARCHAR2 DEFAULT NULL,
                               p_param_value2     VARCHAR2 DEFAULT NULL,
                               p_param_name3      VARCHAR2 DEFAULT NULL,
                               p_param_value3     VARCHAR2 DEFAULT NULL,
                               p_param_name4      VARCHAR2 DEFAULT NULL,
                               p_param_value4     VARCHAR2 DEFAULT NULL,
                               p_param_name5      VARCHAR2 DEFAULT NULL,
                               p_param_value5     VARCHAR2 DEFAULT NULL,
                               p_param_name6      VARCHAR2 DEFAULT NULL,
                               p_param_value6     VARCHAR2 DEFAULT NULL,
                               p_param_name7      VARCHAR2 DEFAULT NULL,
                               p_param_value7     VARCHAR2 DEFAULT NULL,
                               p_param_name8      VARCHAR2 DEFAULT NULL,
                               p_param_value8     VARCHAR2 DEFAULT NULL,
                               p_param_name9      VARCHAR2 DEFAULT NULL,
                               p_param_value9     VARCHAR2 DEFAULT NULL,
                               p_param_name10     VARCHAR2 DEFAULT NULL,
                               p_param_value10    VARCHAR2 DEFAULT NULL,
                               p_creation_date    DATE DEFAULT SYSDATE)
   IS
   BEGIN
      INSERT INTO XXWC.XXWC_EGO_EVENT_LOG_TAB (seq,
                                               creation_date,
                                               --CREATED_BY,
                                               event_name,
                                               event_key,
                                               param_name1,
                                               param_value1,
                                               param_name2,
                                               param_value2,
                                               param_name3,
                                               param_value3,
                                               param_name4,
                                               param_value4,
                                               param_name5,
                                               param_value5,
                                               param_name6,
                                               param_value6,
                                               param_name7,
                                               param_value7,
                                               param_name8,
                                               param_value8,
                                               param_name9,
                                               param_value9,
                                               param_name10,
                                               param_value10)
           VALUES (XXWC_EGO_EVENT_LOG_TAB_S.NEXTVAL,
                   p_CREATION_DATE,
                   --  p_CREATED_BY,
                   p_event_name,
                   p_event_key,
                   p_param_name1,
                   p_param_value1,
                   p_param_name2,
                   p_param_value2,
                   p_param_name3,
                   p_param_value3,
                   p_param_name4,
                   p_param_value4,
                   p_param_name5,
                   p_param_value5,
                   p_param_name6,
                   p_param_value6,
                   p_param_name7,
                   p_param_value7,
                   p_param_name8,
                   p_param_value8,
                   p_param_name9,
                   p_param_value9,
                   p_param_name10,
                   p_param_value10);

      COMMIT;
   END insert_event_log;

   /********************************************************************************
                                                                                                                                                                                                                     PROGRAM TYPE: FUNCTION
      NAME: PUB_WCS_PROC
      PURPOSE: Function to capture business events for WCS integration
      HISTORY
      ===============================================================================
      VERSION DATE          AUTHOR(S)       DESCRIPTION
      ------- -----------   --------------- -----------------------------------------
      1.0     09/23/2012    Rajiv Rathod    Initial creation of the function
       ********************************************************************************/
   FUNCTION process_pre_event (
      p_subscription_guid   IN            RAW,
      p_event               IN OUT NOCOPY WF_EVENT_T)
      RETURN VARCHAR2
   IS
      l_param_list           WF_PARAMETER_LIST_T;
      l_event_name           VARCHAR2 (100);
      l_event_key            VARCHAR2 (100);
      l_dml_type             VARCHAR2 (100);
      l_category_name        VARCHAR2 (100);
      l_category_id          VARCHAR2 (100);
      l_category_set_id      VARCHAR2 (100);
      l_parent_category_id   VARCHAR2 (100);
      L_PARAM_NAME           VARCHAR2 (100);
      L_PARAM_VALUE          VARCHAR2 (100);
      l_param_name1          VARCHAR2 (100);
      l_param_name2          VARCHAR2 (100);
      l_param_name3          VARCHAR2 (100);
      l_param_name4          VARCHAR2 (100);
      l_param_name5          VARCHAR2 (100);
      l_flag                 VARCHAR2 (10);
      l_str_id               NUMBER;
      l_cat_set_id           NUMBER;
   --PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      l_param_list := P_EVENT.GETPARAMETERLIST ();
      l_event_name := P_EVENT.GETEVENTNAME ();
      l_event_key :=
         SUBSTR (P_EVENT.GETEVENTKEY (),
                 INSTR (P_EVENT.GETEVENTKEY (), '-') + 1);



      IF l_param_list IS NOT NULL
      THEN
         FOR I IN l_param_list.FIRST .. l_param_list.LAST
         LOOP
            L_PARAM_NAME := l_param_list (I).GETNAME;
            L_PARAM_VALUE := l_param_list (I).GETVALUE;

            IF (l_param_name = 'DML_TYPE')
            THEN
               l_dml_type := l_param_value;
               l_param_name1 := l_param_name;
            ELSIF (l_param_name = 'CATEGORY_SET_ID')
            THEN
               l_category_set_id := l_param_value;
               l_param_name2 := l_param_name;
            ELSIF (l_param_name = 'CATEGORY_NAME')
            THEN
               l_category_name := l_param_value;
               l_param_name3 := l_param_name;
            ELSIF (l_param_name = 'CATEGORY_ID')
            THEN
               l_category_id := l_param_value;
               l_param_name4 := l_param_name;
            ELSIF (l_param_name = 'PARENT_CATEGORY_ID')
            THEN
               l_parent_category_id := l_param_value;
               l_param_name5 := l_param_name;
            END IF;
         END LOOP;

         ---------
         --Check if the modified category belongs to "WC Web Hierarchy"
         ---------
         /*    SELECT decode(count(1),0,'N','Y') INTO l_flag
                                                                                                                FROM    mtl_category_sets_tl mcstl,
                     MTL_CATEGORY_SET_VALID_CATS mcsvc
             WHERE  mcstl.category_set_name = 'WC Web Hierarchy'
             AND    mcsvc.category_set_id = mcstl.category_set_id
             AND    mcsvc.category_id=l_category_id; */

         SELECT category_set_id
           INTO l_cat_set_id
           FROM mtl_category_sets_tl mcstl
          WHERE mcstl.category_set_name = 'WC Web Hierarchy';

         SELECT structure_id
           INTO l_str_id
           FROM mtl_category_sets_b
          WHERE category_set_id = l_cat_set_id;

         SELECT DECODE (COUNT (1), 0, 'N', 'Y')
           INTO l_flag
           FROM MTL_CATEGORIES_B
          WHERE structure_id = l_str_id AND category_id = l_category_id;

         ---------
         --call insert event log to insert values into custom table
         ---------

         IF l_flag = 'Y'
         THEN
            insert_event_log (l_event_name,
                              l_event_key,
                              l_param_name1,
                              l_dml_type,
                              l_param_name2,
                              l_category_set_id,
                              l_param_name3,
                              l_category_name,
                              l_param_name4,
                              l_category_id,
                              l_param_name5,
                              l_parent_category_id);
         END IF;
      END IF;

      RETURN 'SUCCESS';
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'FAILURE';
   END process_pre_event;

   /*************************************************************************
   *   Procedure : submit_job
   *
   *   PURPOSE:   This procedure is called by UC4 to initiate Publish CP: XXWC Publish data to WCS
   *   Parameter:
   *          IN
   *              p_user_name              -- User name to initilize the request
   *               p_responsibility_name    -- Responsibility name to initilize the request
   *           OUT:
   *              errbuf         -- contains the error details
   *              retcode        -- 0 = Success, 1 = Warning, 2 = ERROR

    HISTORY
   ===============================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     05/29/2013    Rajasekar G    Initial creation of the procedure
   **********************************************************************************/
   PROCEDURE submit_job (errbuf           OUT NOCOPY VARCHAR2,
                         retcode          OUT NOCOPY VARCHAR2,
                         p_user_name   IN            VARCHAR2)
   IS
      l_req_id                NUMBER;
      l_phase                 VARCHAR2 (10);
      l_status                VARCHAR2 (10);
      l_dev_phase             VARCHAR2 (10);
      l_dev_status            VARCHAR2 (10);
      l_message               VARCHAR2 (1000);
      l_err_msg               VARCHAR2 (1000);

      l_user_id               NUMBER;
      l_responsibility_id     NUMBER;
      l_resp_application_id   NUMBER;
      l_resp_name    CONSTANT VARCHAR (40)
                                 := 'HDS Product Data Hub Admin - WC' ;
      l_request_status        BOOLEAN;
   BEGIN
      -- Deriving Ids from initalization variables
      -- to get the user id
      BEGIN
         l_user_id := Apps.FND_LOAD_UTIL.OWNER_ID (p_user_name);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
               'UserName ' || p_user_name || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving user_id for UserName - ' || p_user_name;
            RAISE PROGRAM_ERROR;
      END;

      -- to get the resp id and application id
      BEGIN
         SELECT responsibility_id, application_id
           INTO l_responsibility_id, l_resp_application_id
           FROM fnd_responsibility_vl
          WHERE     responsibility_name = l_resp_name
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
               'Responsibility ' || l_resp_name || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving Responsibility_id for ' || l_resp_name;
            RAISE PROGRAM_ERROR;
      END;

      -- Environment Initialization
      apps.fnd_global.apps_initialize (l_user_id,
                                       l_responsibility_id,
                                       l_resp_application_id);

      DBMS_OUTPUT.put_line (
         'Submitting the Concurrent Program : XXWC Publish data to WCS');
      -- Calling the CP API to submit the CP :XXWC Publish data to WCS
      l_req_id :=
         apps.fnd_request.submit_request (application   => 'XXWC',
                                          program       => 'XXWCEGOPUBWCS',
                                          argument1     => 'Y',
                                          argument2     => 'Y',
                                          argument3     => 'Y',
                                          argument4     => 'Y',
                                          argument5     => 'Y',
                                         -- argument6     => NULL); -- commented by Pattabhi for TMS#20171114-00008
										  argument6     => SYSDATE-6); -- commented by Pattabhi for TMS#20171114-00008
      COMMIT;
      DBMS_OUTPUT.put_line (
            'Concurrent Program Request Submitted Successfully. Request ID: '
         || l_req_id);

      IF (l_req_id > 0)
      THEN
         l_request_status :=
            apps.fnd_concurrent.wait_for_request (
               request_id   => l_req_id,
               -- interval     => 60,
               -- max_wait     => 90,
               phase        => l_phase,
               status       => l_status,
               dev_phase    => l_dev_phase,
               dev_status   => l_dev_status,
               MESSAGE      => l_message);

         IF UPPER (l_status) = 'WARNING' OR UPPER (l_dev_status) = 'WARNING'
         THEN
            l_err_msg := 'EBS Conc Prog Completed with warning ' || l_message;
            DBMS_OUTPUT.put_line (l_err_msg);
            retcode := 1;
         --RAISE PROGRAM_ERROR;
         ELSIF UPPER (l_status) = 'ERROR' OR UPPER (l_dev_status) = 'ERROR'
         THEN
            l_err_msg := 'EBS Conc Prog Completed with Error ' || l_message;
            DBMS_OUTPUT.put_line (l_err_msg);
            retcode := 2;
            RAISE PROGRAM_ERROR;
         ELSE                                        -- else of l_status check
            retcode := 0;
            DBMS_OUTPUT.put_line ('EBS Conc Program Successfully Completed');
         END IF;                                      -- end of l_status check
      ELSE                                        -- else of IF (l_req_id > 0)
         l_err_msg := 'EBS Conc Program not initated';
         DBMS_OUTPUT.put_line (l_err_msg);
         RAISE PROGRAM_ERROR;
      END IF;

      retcode := 0;
      errbuf := 'Program Successfully Completed';
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         retcode := 2;
         errbuf :=
            SUBSTR (
                  l_err_msg
               || SQLCODE
               || ' ERROR : '
               || SQLERRM
               || ' Details: '
               || DBMS_UTILITY.format_error_backtrace (),
               1,
               2000);
         DBMS_OUTPUT.put_line (errbuf);
      WHEN OTHERS
      THEN
         retcode := 2;
         errbuf :=
            SUBSTR (
                  l_err_msg
               || SQLCODE
               || ' ERROR : '
               || SQLERRM
               || ' Details: '
               || DBMS_UTILITY.format_error_backtrace (),
               1,
               2000);
         DBMS_OUTPUT.put_line (errbuf);
   END submit_job;
END XXWC_EGO_PUB_WCS_PKG;
/