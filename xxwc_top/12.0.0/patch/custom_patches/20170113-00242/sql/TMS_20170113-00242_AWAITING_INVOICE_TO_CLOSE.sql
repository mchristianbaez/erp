/*************************************************************************
  $Header TMS_20170113-00242_AWAITING_INVOICE_TO_CLOSE.sql $
  Module Name: TMS_20170113-00242  Data Fix script 

  PURPOSE: Datafix script
  
  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        05-APR-2017  Pattabhi Avula         TMS#20170113-00242 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170113-00242    , Before Update');

UPDATE apps.oe_order_lines_all
SET    FLOW_STATUS_CODE='CANCELLED',
       CANCELLED_FLAG='Y'
WHERE  line_id = 49303223
AND    header_id= 29957875;

   DBMS_OUTPUT.put_line (
         'TMS: 20170113-00242  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170113-00242    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170113-00242 , Errors : ' || SQLERRM);
END;
/