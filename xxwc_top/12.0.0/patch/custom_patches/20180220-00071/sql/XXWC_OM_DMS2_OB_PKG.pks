CREATE OR REPLACE PACKAGE APPS.XXWC_OM_DMS2_OB_PKG IS
 /*************************************************************************
     $Header XXWC_OM_DMS2_OB_PKG $
     Module Name: XXWC_OM_DMS2_OB_PKG.pks

     PURPOSE:   DESCARTES Project	
					   
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        09/05/2017  Rakesh Patel            TMS#20170811-00005-DMS Phase-2.0 Outbound Extract(Standard and Internal orders)
     3.0        2/13/2018   Rakesh Patel            TMS# 20180213-00090-DMS 2.0 Pilot issues Alternate Delivery Address and Return Order CreditBill Only Lines
     4.0        02/20/2018  Rakesh Patel            TMS#20180220-00071-DMS 2.0 Pilot enhancements based on branch feedback
**************************************************************************/
   
   TYPE t_request IS RECORD (
      method        VARCHAR2(256),
      namespace     VARCHAR2(256),
      body          CLOB,
      envelope_tag  VARCHAR2(30)
   );

   TYPE t_response IS RECORD 
   (
    doc           XMLTYPE,
    envelope_tag  VARCHAR2(30)
   );

   TYPE xxwc_log_rec IS RECORD (
      SERVICE_NAME            VARCHAR2(150)
     ,ORDER_NUMBER            NUMBER
     ,HEADER_ID               NUMBER
     ,LINE_ID                 NUMBER
     ,DELIVERY_ID             NUMBER
     ,ORDER_TYPE              VARCHAR2(150)  
     ,PO_NUMBER               NUMBER
     ,PO_HEADER_ID            NUMBER
     ,SCHEDULE_DELIVERY_DATE  DATE     
     ,RPT_CON_REQUEST_ID      NUMBER        
     ,WS_RESPONSECODE         VARCHAR2(500)
     ,WS_RESPONSEDESCRIPTION  VARCHAR2(4000)
     ,HTTP_STATUS_CODE        VARCHAR2(500)
     ,HTTP_REASON_PHRASE      VARCHAR2(4000)
     ,REQUEST_PAYLOAD         CLOB
     ,RESPONSE_PAYLOAD        CLOB 
   );
   
   PROCEDURE DEBUG_LOG (p_xxwc_log_rec IN OUT xxwc_log_rec );
   
   FUNCTION new_request(p_method        IN  VARCHAR2,
                     p_namespace     IN  VARCHAR2,
                     p_envelope_tag  IN  VARCHAR2 DEFAULT 'soapenv')
   RETURN t_request;

   PROCEDURE add_parameter(p_request  IN OUT NOCOPY  t_request,
                        p_name     IN             VARCHAR2,
                        p_value    IN             VARCHAR2,
                        p_type     IN             VARCHAR2 := NULL);

   PROCEDURE add_complex_parameter(p_request  IN OUT NOCOPY  t_request,
                                p_xml      IN             VARCHAR2);

   PROCEDURE invoke( p_request  IN OUT NOCOPY  t_request
                   ,p_url      IN             VARCHAR2
                   ,p_action   IN             VARCHAR2
                   ,p_xxwc_log_rec      IN OUT xxwc_log_rec);
   
   FUNCTION get_return_value(p_response   IN OUT NOCOPY  t_response,
                             p_name       IN             VARCHAR2,
                             p_namespace  IN             VARCHAR2)
   RETURN VARCHAR2;
  
   PROCEDURE show_envelope(p_env     IN  VARCHAR2,
                           p_heading IN  VARCHAR2 DEFAULT NULL);  

   PROCEDURE ShipmentSvc_clear_delivery ( p_order_number       IN NUMBER
                                       , p_sch_delivery_date IN DATE 
                                       , p_delivery_id       IN NUMBER
                                       , p_xxwc_log_rec      IN OUT xxwc_log_rec); 
                                      
   PROCEDURE ShipmentSvc_std_int_ord ( p_order_number       IN NUMBER
                                      , p_sch_delivery_date IN DATE 
                                      , p_delivery_id       IN NUMBER 
                                      , p_request_id        IN NUMBER
                                      , p_priority          IN VARCHAR2 --TMS# 20180213-00090
                                      , p_xxwc_log_rec      IN OUT xxwc_log_rec);  

   PROCEDURE ShipmentSvc_rental_ord ( p_order_number      IN NUMBER
                                     ,p_shipment_type     IN VARCHAR2
                                     ,p_line_type_id      IN NUMBER
                                     ,p_flow_status_code  IN VARCHAR2 
                                     ,p_sch_delivery_date IN DATE 
                                     ,p_rental_return     IN NUMBER DEFAULT NULL
                                     ,p_request_id        IN NUMBER
                                     ,p_xxwc_log_rec      IN OUT xxwc_log_rec );  
                                      
   PROCEDURE ShipmentSvc_return_ord ( p_order_number      IN NUMBER
                                     ,p_shipment_type     IN VARCHAR2
                                     ,p_sch_delivery_date IN DATE 
                                     ,p_request_id        IN NUMBER
                                     ,p_xxwc_log_rec      IN OUT xxwc_log_rec ); 
                                       
   PROCEDURE Send_to_DMS ( p_conc_program_name IN VARCHAR2
                          ,p_order_header_id   IN NUMBER
                          ,p_delivery_id       IN NUMBER
                          ,p_sch_delivery_date IN DATE
                          ,p_request_id        IN NUMBER
                          ,p_priority          IN VARCHAR2 --TMS# 20180213-00090
                         );
                         
   PROCEDURE Send_to_DMS_Clear_Delivery ( p_order_header_id   IN NUMBER
                                         ,p_delivery_id       IN NUMBER
                                         ,p_sch_delivery_date IN DATE DEFAULT SYSDATE
                                        );
                         
   PROCEDURE Send_to_DMS_WCD ( p_order_header_id   IN NUMBER
                              ,p_delivery_id       IN NUMBER
                              ,p_sch_delivery_date IN DATE DEFAULT SYSDATE
                              ,p_priority          IN VARCHAR2 --TMS# 20180213-00090
                              );
   
   PROCEDURE ShipmentSvc_WCD(  p_order_number       IN NUMBER
                             , p_sch_delivery_date IN DATE 
                             , p_delivery_id       IN NUMBER 
                             , p_priority          IN VARCHAR2 --TMS# 20180213-00090
                             , p_xxwc_log_rec      IN OUT xxwc_log_rec);  
   PROCEDURE debug_on;
   PROCEDURE debug_off;
   
   PROCEDURE main(errbuf             OUT VARCHAR2
                ,retcode             OUT NUMBER
                ,p_header_id         IN NUMBER
                ,p_sch_delivery_date IN VARCHAR2
                ,p_delivery_id       IN NUMBER
                ,p_shipment_type     IN VARCHAR2
                ,p_interface_type    IN VARCHAR2
                ,p_debug_flag        IN VARCHAR2
                ,p_request_id        IN NUMBER
                ,p_priority          IN VARCHAR2 --TMS# 20180213-00090
                );
   
   FUNCTION replace_special_characters( p_string in VARCHAR2 ) RETURN VARCHAR2;    
   
   FUNCTION IS_DMS_ELIGIBLE ( p_con_program_name     IN VARCHAR2
                            , p_default_shipping_org IN NUMBER 
                            , p_order_type_id        IN NUMBER
                            , p_header_id            IN NUMBER DEFAULT NULL----Rev#4.0
                            ) RETURN VARCHAR2;  
   
   FUNCTION IS_DMS_SHIP_METHOD ( p_ship_method IN VARCHAR2 ) RETURN VARCHAR2;
   
   FUNCTION GET_DMS_ORG ( p_branch_code IN VARCHAR2 ) RETURN VARCHAR2;   
   
   PROCEDURE raise_so_cancel_line ( p_header_id IN NUMBER
                                   ,p_line_id  IN NUMBER
                                   ,p_error_msg  OUT VARCHAR2);
   
   PROCEDURE raise_so_cancel_order ( p_header_id IN NUMBER
                                    ,p_error_msg  OUT VARCHAR2);
                                                                   
   FUNCTION Delete_SO_hdr_Line (p_subscription_guid   IN     RAW,
                                p_event               IN OUT wf_event_t)
      RETURN VARCHAR2;
                            
   PROCEDURE ShipmentSvc_delete_hdr_line(p_header_id            IN NUMBER,
                                     p_line_id              IN NUMBER,
                                     p_delivery_id         IN NUMBER,
                                     p_sch_delivery_date    IN DATE,
                                     p_hdr_flow_status_code IN VARCHAR2,
                                     p_ln_flow_status_code  IN VARCHAR2,
                                     p_xxwc_log_rec         IN OUT xxwc_log_rec ); 

   /**PROCEDURE Delete_SO_Lines (
                             p_application_id                 IN         NUMBER,
                             p_entity_short_name              IN         VARCHAR2,
                             p_validation_entity_short_name   IN         VARCHAR2,
                             p_validation_tmplt_short_name    IN         VARCHAR2,
                             p_record_set_short_name          IN         VARCHAR2,
                             p_scope                          IN         VARCHAR2,
                             x_result                         OUT NOCOPY NUMBER);**/
                             
  --This procedure called from processing constraints when shipping method is changed at header level
  PROCEDURE CANCEL_SO_OR_LINE(
                             p_application_id                 IN         NUMBER,
                             p_entity_short_name              IN         VARCHAR2,
                             p_validation_entity_short_name   IN         VARCHAR2,
                             p_validation_tmplt_short_name    IN         VARCHAR2,
                             p_record_set_short_name          IN         VARCHAR2,
                             p_scope                          IN         VARCHAR2,
                             x_result                         OUT NOCOPY NUMBER);   
                             
   PROCEDURE Raise_PO_Send_To_DMS2 (p_po_header_id  IN NUMBER,
                                    p_error_msg  OUT VARCHAR2);   

      
   FUNCTION PO_Send_To_DMS2 ( p_subscription_guid  IN  RAW
                            , p_event              IN  OUT wf_event_t)
   RETURN VARCHAR2;
   
   PROCEDURE ShipmentSvc_po(p_header_id         IN NUMBER,
                            p_xxwc_log_rec IN OUT xxwc_log_rec,
                            p_promised_date     IN DATE, --Rev#4.0
                            p_operationCode     IN VARCHAR2);
                            
  --Rev4.0 <Start
  PROCEDURE Send_to_DMS_INT_PICKUP( p_order_header_id   IN NUMBER
                                   ,p_sch_delivery_date IN DATE
                                   ,p_request_id        IN NUMBER
                                   );  
  
  PROCEDURE ShipmentSvc_int_ord_pickup( p_order_number      IN NUMBER
                                      , p_shipment_type     IN VARCHAR2
                                      , p_sch_delivery_date IN DATE 
                                      , p_request_id        IN NUMBER
                                      , p_xxwc_log_rec      IN OUT xxwc_log_rec);
  --Rev4.0 >End                                    
END XXWC_OM_DMS2_OB_PKG;
/