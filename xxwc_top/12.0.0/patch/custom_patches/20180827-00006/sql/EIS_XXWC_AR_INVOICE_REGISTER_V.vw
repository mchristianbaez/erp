---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_AR_INVOICE_REGISTER_V $
  Module Name : Receivables
  PURPOSE	  : Invoice Register - Write Offs
  REVISIONS   :
  VERSION 		DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.1     	   27-Sep-2016        	Siva   		 TMS#20160826-00345   --Changed the position of all tables and conditions in view
															and added hints for fixing the for fixing the Performance issue
  1.2		   27-Aug-2018			Siva		 TMS#20180827-00006 														
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_AR_INVOICE_REGISTER_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_AR_INVOICE_REGISTER_V (ADJUSTMENT_POSTABLE, TRX_TYPE, TRX_TYPE_CLASS, TRX_CURRENCY_CODE, RECEIPT_DATE, FISCAL_YEAR, FISCAL_MONTH, ORDER_NUMBER, INVOICE_NUMBER, INVOICE_DATE, CUSTOMER_NUMBER, CUSTOMER_NAME, INVOICE_AMOUNT, DISCOUNT_AMT, SHIP_TO_CITY, SHIP_TO_STATE, SHIP_TO_POSTAL_CODE, AMOUNT_APPLIED, SHIP_VIA, ACCOUNT, GL_ACCOUNT, BRANCH_DESCR, LOCATION, POSTAL_CODE, STATE, CITY, ACTIVITY_NAME, TRX_COMMENTS, TRX_DUE_DATE, TRX_GL_DATE, ADJUSTMENT_ID, ADJUSTMENT_NUM, ADJUSTMENT_DATE, ADJUSTMENT_GL_DATE, ADJ_TYPE, ADJ_TYPE_MEANING, ADJ_TYPE_CODE, WRITEOFF_AMOUNT, ADJUSTMENT_ACCTD_AMOUNT, PARTY_NAME, D_OR_I, COMPANY_NAME, COA_ID, FUNCTIONAL_CURRENCY, FUNCTIONAL_CURRENCY_PRECISION, PARTY_NUMBER, PARTY_TYPE, PARTY_ORIG_SYS_REF, CUSTOMER_KEY, DUNS_NUMBER, PERSON_FIRST_NAME, PERSON_MIDDLE_NAME, PERSON_LAST_NAME, PERSON_NAME_SUFFIX, PERSON_TITLE, COUNTRY, PROVINCE, CUST_ACCT_ORIG_SYS_REF, CUST_ACCT_STATUS, CUSTOMER_TYPE, CUST_ACCT_SITES_ORIG_SYS_REF,
  STATUS, CUSTOMER_CATEGORY_CODE, LANGUAGE, KEY_ACCOUNT_FLAG, TP_HEADER_ID, SITE_USE_CODE, PRIMARY_FLAG, CUST_SITE_USES_STATUS, SITE_USE_ORIG_SYS_REF, TAX_REFERENCE, TAX_CODE, OPERATING_UNIT, CUST_ACCOUNT_ID, PARTY_ID, SITE_USE_ID, CUST_ACCT_SITE_ID, CUST_TRX_TYPE_ID, PAYMENT_SCHEDULE_ID, RECEIVABLES_TRX_ID, CUSTOMER_TRX_ID, ORG_ID, CURRENCY_CODE, ORGANIZATION_ID, LEDGER_ID, HEADER_ID, CODE_COMBINATION_ID, GL#50328#ACCOUNT, GL#50328#ACCOUNT#DESCR, GL#50328#COST_CENTER, GL#50328#COST_CENTER#DESCR, GL#50328#FURTURE_USE, GL#50328#FURTURE_USE#DESCR, GL#50328#FUTURE_USE_2, GL#50328#FUTURE_USE_2#DESCR, GL#50328#LOCATION, GL#50328#LOCATION#DESCR, GL#50328#PRODUCT, GL#50328#PRODUCT#DESCR, GL#50328#PROJECT_CODE, GL#50328#PROJECT_CODE#DESCR)
AS
  SELECT  /*+ INDEX(adj XXWC_AR_ADJUSTMENTS_N18) INDEX(rec AR_RECEIVABLES_TRX_U1) INDEX(gle GL_LEDGERS_U2) INDEX(rctt RA_CUST_TRX_TYPES_U1) INDEX(gl GL_CODE_COMBINATIONS_U1) INDEX(fv FND_FLEX_VALUES_N1) INDEX(oh XXWC_OBIEE_OE_ORDER_HDR_AL2)*/   --added for version 1.1
    DECODE (adj.postable, 'Y', 'Yes', 'N', 'No') adjustment_postable,
    rctt.name trx_type,
    rctt.type trx_type_class,
    trx.invoice_currency_code trx_currency_code,
    (SELECT MAX(receipt_date)
    FROM ar_cash_receipts cr,
      ar_payment_schedules ps
    WHERE ps.cash_receipt_id = cr.cash_receipt_id
    AND ps.trx_number        = trx.trx_number
    AND cr.cash_receipt_id  IS NOT NULL
    )receipt_date,
    gps.period_year fiscal_year,  --added for version 1.1
    gps.period_name fiscal_month, --added for version 1.1
    --  XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.XXWC_GET_FISCAL_YEAR(trx.ORG_ID,adj.gl_date) FISCAL_YEAR, --commented for version 1.1
    --  XXEIS.eis_rs_xxwc_com_util_pkg.XXWC_GET_FISCAL_MONTH(trx.ORG_ID,adj.gl_date) FISCAL_MONTH, --commented for version 1.1
    DECODE(trx.interface_header_context,'ORDER ENTRY',trx.interface_header_attribute1,NVL(trx.ct_reference,xxeis.eis_rs_xxwc_com_util_pkg.get_ordnum(cust.account_number, trx.trx_number))) order_number,
    trx.trx_number invoice_number,
    trx.trx_date invoice_date,
    cust.account_number customer_number,
    NVL(cust.account_name,party.party_name)customer_name,
    (SELECT SUM(extended_amount)
    FROM ra_customer_trx_lines rct1
    WHERE rct1.customer_trx_id=trx.customer_trx_id
    )invoice_amount,
    NVL(pay.discount_original,0) discount_amt,
    locs.city ship_to_city,
    locs.state ship_to_state,
    locs.postal_code ship_to_postal_code,
    (SELECT SUM(ra.amount_applied)
    FROM ar_receivable_applications ra
    WHERE application_type         ='CASH'
    AND ra.status                  ='APP'
    AND ra.applied_customer_trx_id = trx.customer_trx_id
    ) amount_applied,
    --DECODE(interface_header_context,'CONVERSION',xxeis.eis_rs_xxwc_com_util_pkg.get_shipvia(cust.account_number, trx.trx_number),'PRISM INVOICES',interface_header_attribute12,flv_ship_mtd.meaning) ship_via,  --commented for version 1.2
	DECODE(interface_header_context,'CONVERSION',xxeis.eis_rs_xxwc_com_util_pkg.get_shipvia(cust.account_number, trx.trx_number),'PRISM INVOICES',
    (SELECT FLVL.MEANING
      FROM APPS.FND_LOOKUP_VALUES_VL FLVL
    WHERE FLVL.LOOKUP_CODE = TRX.INTERFACE_HEADER_ATTRIBUTE12
    AND FLVL.LOOKUP_TYPE   ='SHIP_METHOD'
    AND ROWNUM             =1
    ),flv_ship_mtd.meaning) ship_via, --Added for version 1.2
    gl.concatenated_segments account,
    gl.segment4 gl_account,
    fvt.description branch_descr, --Changed for version 1.1
    DECODE(trx.interface_header_context,'ORDER ENTRY',ood.organization_code,'PRISM INVOICES',trx.interface_header_attribute7,'CONVERSION',xxeis.eis_rs_xxwc_com_util_pkg.get_warehse(cust.account_number, trx.trx_number)) location,
    haou.postal_code,
    haou.region_2 state,
    haou.town_or_city city,
    rec.name activity_name,
    trx.comments trx_comments,
    pay.due_date trx_due_date,
    pay.gl_date trx_gl_date,
    adj.adjustment_id adjustment_id,
    adj.adjustment_number adjustment_num,
    adj.apply_date adjustment_date,
    adj.gl_date adjustment_gl_date,
    DECODE (adj.adjustment_type, 'C', look.meaning, DECODE (rec.type, 'FINCHRG', 'Finance Charges', 'Adjustment' ) ) adj_type,
    ladjtype.meaning adj_type_meaning,
    adj.type adj_type_code,
    ROUND (adj.amount, 2) writeoff_amount,
    adj.acctd_amount adjustment_acctd_amount,
    party.party_name,
    DECODE (adj.adjustment_type, 'C', DECODE (rctt.type, 'GUAR', 'I', 'D'), '' ) d_or_i,
    gle.name company_name,
    gle.chart_of_accounts_id coa_id,
    gle.currency_code functional_currency,
    cur.precision functional_currency_precision,
    party.party_number,
    party.party_type,
    party.orig_system_reference party_orig_sys_ref,
    party.customer_key,
    party.duns_number,
    party.person_first_name,
    party.person_middle_name,
    party.person_last_name,
    party.person_name_suffix,
    party.person_title,
    party.country,
    -- party.city,
    --party.postal_code,
    --party.state,
    party.province,
    cust.orig_system_reference cust_acct_orig_sys_ref,
    DECODE (cust.status, 'A', 'Active', 'I', 'Inactive', cust.status) cust_acct_status,
    cust.customer_type,
    acct_site.orig_system_reference cust_acct_sites_orig_sys_ref,
    DECODE (acct_site.status, 'A', 'Active', 'I', 'Inactive', acct_site.status ) status,
    acct_site.customer_category_code,
    acct_site.language,
    acct_site.key_account_flag,
    acct_site.tp_header_id,
    site_uses.site_use_code,
    site_uses.primary_flag,
    DECODE (site_uses.status, 'A', 'Active', 'I', 'Inactive', site_uses.status ) cust_site_uses_status,
    site_uses.orig_system_reference site_use_orig_sys_ref,
    site_uses.tax_reference,
    site_uses.tax_code,
    hou.name operating_unit
    --primary keys
    ,
    cust.cust_account_id,
    party.party_id,
    site_uses.site_use_id,
    acct_site.cust_acct_site_id,
    rctt.cust_trx_type_id,
    pay.payment_schedule_id,
    rec.receivables_trx_id,
    trx.customer_trx_id,
    --  param.org_id, --commented for version 1.1
    trx.org_id, --added for version 1.1
    cur.currency_code,
    hou.organization_id,
    gle.ledger_id,
    oh.header_id,
    gl.code_combination_id
    --primary Keys
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    ,
    gl.segment4 gl#50328#account ,
    xxeis.eis_rs_fin_utility.decode_vset(gl.segment4,'XXCUS_GL_ACCOUNT') gl#50328#account#descr ,
    gl.segment3 gl#50328#cost_center ,
    xxeis.eis_rs_fin_utility.decode_vset(gl.segment3,'XXCUS_GL_COSTCENTER') gl#50328#cost_center#descr ,
    gl.segment6 gl#50328#furture_use ,
    xxeis.eis_rs_fin_utility.decode_vset(gl.segment6,'XXCUS_GL_FUTURE_USE1') gl#50328#furture_use#descr ,
    gl.segment7 gl#50328#future_use_2 ,
    xxeis.eis_rs_fin_utility.decode_vset(gl.segment7,'XXCUS_GL_FUTURE_USE_2') gl#50328#future_use_2#descr ,
    gl.segment2 gl#50328#location ,
    xxeis.eis_rs_fin_utility.decode_vset(gl.segment2,'XXCUS_GL_LOCATION') gl#50328#location#descr ,
    gl.segment1 gl#50328#product ,
    xxeis.eis_rs_fin_utility.decode_vset(gl.segment1,'XXCUS_GL_PRODUCT') gl#50328#product#descr ,
    gl.segment5 gl#50328#project_code ,
    xxeis.eis_rs_fin_utility.decode_vset(gl.segment5,'XXCUS_GL_PROJECT') gl#50328#project_code#descr
    --gl#accountff#end
  FROM ra_customer_trx trx,
    ar_payment_schedules pay,
    ar_adjustments adj,
    ar_receivables_trx rec,
    ar_lookups ladjtype,
    ra_cust_trx_types rctt,
    ar_lookups look,
    hz_cust_accounts cust,
    hz_parties party,
    hz_cust_acct_sites acct_site,
    hz_cust_site_uses site_uses,
    hz_party_sites party_sites ,
    hz_locations locs,
    hr_operating_units hou,
    gl_ledgers gle,
    gl_period_statuses gps, --added for version 1.1
    --  ar_system_parameters param, --commented for version 1.1
    fnd_currencies cur,
    oe_order_headers oh,
    mtl_parameters ood, --added for version 1.1
    --  ORG_ORGANIZATION_DEFINITIONS OOD,  --commented for version 1.1
    fnd_lookup_values_vl flv_ship_mtd,
    hr_organization_units_v haou ,
    gl_code_combinations_kfv gl, --gl Sometimes gl account will not be complete.. but still we need to show the adjustment
    apps.fnd_flex_values_tl fvt,  --added for version 1.1
    apps.FND_FLEX_VALUES fv  --added for version 1.1
--    fnd_flex_values_vl fv  --commented for version 1.1
    /*
    XXWC.XXWC_AREXTI_GETPAID_DUMP_TBL conv*/
  WHERE trx.complete_flag         = 'Y'
  AND trx.customer_trx_id         = pay.customer_trx_id + 0
  AND pay.payment_schedule_id     = adj.payment_schedule_id
  AND NVL (adj.status, 'A')       = 'A'
  AND adj.postable                = 'Y'
  AND adj.adjustment_id           > 0
  AND adj.receivables_trx_id     IS NOT NULL
  AND adj.receivables_trx_id      = rec.receivables_trx_id
  AND adj.type                    = ladjtype.lookup_code
  AND ladjtype.lookup_type        = 'ADJUSTMENT_TYPE'
  AND trx.cust_trx_type_id        = rctt.cust_trx_type_id
  AND trx.org_id                  = rctt.org_id -- added for version 1.1
  AND rctt.type                  IN ('INV', 'DEP', 'GUAR', 'CM', 'DM', 'CB')
  AND rctt.type                   = look.lookup_code
  AND look.lookup_type            = 'INV/CM'
  AND trx.bill_to_customer_id + 0 = cust.cust_account_id
  AND cust.party_id               = party.party_id
  AND cust.cust_account_id        = acct_site.cust_account_id
  AND trx.ship_to_site_use_id     = site_uses.site_use_id
  AND acct_site.cust_acct_site_id = site_uses.cust_acct_site_id
  AND acct_site.party_site_id     = party_sites.party_site_id(+)
  AND party_sites.location_id     = locs.location_id(+)
  AND rctt.org_id                 = hou.organization_id
  AND trx.set_of_books_id         = gle.ledger_id
  AND gle.ledger_id               = gps.set_of_books_id           --added for version 1.1
  AND gle.accounted_period_type   = gps.period_type               --added for version 1.1
  AND TRUNC (adj.gl_date) BETWEEN gps.start_date AND gps.end_date --added for version 1.1
  AND gps.application_id = 101                                    --added for version 1.1
    --and gle.ledger_id               = param.set_of_books_id  --commented for version 1.1
  AND gle.currency_code                                                                              = cur.currency_code
  AND DECODE(trx.interface_header_context,'ORDER ENTRY',to_number(interface_header_attribute1),NULL) = oh.order_number(+)
  AND oh.ship_from_org_id                                                                            = ood.organization_id(+)
  AND oh.shipping_method_code                                                                        = flv_ship_mtd.lookup_code(+)
  AND flv_ship_mtd.lookup_type(+)                                                                    ='SHIP_METHOD'
  AND ood.organization_id                                                                            = haou.organization_id(+)
    --AND trx.trx_number              = conv.invno(+)
    --AND conv.custno                 = cust.account_number(+)
    --AND decode(interface_header_context,'CONVERSION',cust.account_number,conv.custno) = conv.custno
    --AND cust.account_number(+)      = conv.invno
    --AND trx.customer_trx_id         = 489259
  AND adj.code_combination_id = gl.code_combination_id(+)
  and gl.segment2             = fv.flex_value(+)     
  and fv.flex_value_id        = fvt.flex_value_id(+)   --added for version 1.1
  AND fvt.LANGUAGE(+)         = userenv('LANG')  --added for version 1.1
  AND fv.flex_value_set_id   IN
    (SELECT fs.flex_value_set_id
    FROM fnd_flex_value_sets fs
    WHERE fs.flex_value_set_name ='XXCUS_GL_LOCATION'
    )  
/