 /********************************************************************************
  FILE NAME: XXWC_AP_INVOICE_INT.sql

  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    --------------------------------------
  1.0     07/09/2018   P.vamshidhar     TMS#20180319-00237   -- AH HARRIS AP Invoices Conversion
  ********************************************************************************/
DROP TABLE XXWC.XXWC_AP_INVOICE_INT CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWC_AP_INVOICE_INT
(
  INVOICE_ID                VARCHAR2(30 BYTE),
  INVOICE_NUM               VARCHAR2(50 BYTE),
  INVOICE_TYPE_LOOKUP_CODE  VARCHAR2(50 BYTE),
  INVOICE_DATE              DATE,
  VENDOR_ID                 VARCHAR2(30 BYTE),
  VENDOR_NUM                VARCHAR2(40 BYTE),
  VENDOR_NAME               VARCHAR2(240 BYTE),
  VENDOR_SITE_ID            VARCHAR2(30 BYTE),
  VENDOR_SITE_CODE          VARCHAR2(100 BYTE),
  INVOICE_AMOUNT            VARCHAR2(30 BYTE),
  INVOICE_CURRENCY_CODE     VARCHAR2(15 BYTE),
  TERMS_ID                  VARCHAR2(30 BYTE),
  TERMS_NAME                VARCHAR2(100 BYTE),
  DESCRIPTION               VARCHAR2(240 BYTE),
  AWT_GROUP_ID              VARCHAR2(30 BYTE),
  AWT_GROUP_NAME            VARCHAR2(25 BYTE),
  ATTRIBUTE_CATEGORY        VARCHAR2(140 BYTE),
  ATTRIBUTE1                VARCHAR2(140 BYTE),
  STATUS                    VARCHAR2(15 BYTE),
  REJECT_CODE               VARCHAR2(500 BYTE),
  PAYMENT_METHOD_CODE       VARCHAR2(200 BYTE),
  ORG_ID                    NUMBER              DEFAULT NVL(TO_NUMBER(DECODE(SUBSTRB(USERENV('CLIENT_INFO'),1,1), ' ', NULL,SUBSTRB(USERENV('CLIENT_INFO'),1,10))),162)
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE OR REPLACE SYNONYM APPS.XXWC_AP_INVOICE_INT FOR XXWC.XXWC_AP_INVOICE_INT;


GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AP_INVOICE_INT TO EA_APEX;
