 /********************************************************************************
  FILE NAME: XXWC.XXWC_AP_PAID_INVOICE_STG.sql

  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    --------------------------------------
  1.0     05/016/2018   Pattabhi Avula     TMS#20180308-00301   -- Initial Version
  ********************************************************************************/
DROP TABLE XXWC.XXWC_AP_PAID_INVOICE_STG CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWC_AP_PAID_INVOICE_STG
(
  INVOICE_NUM       VARCHAR2(30 BYTE),
  VENDOR_NUM        VARCHAR2(30 BYTE),
  INVOICE_AMOUNT    NUMBER(38),
  INVOICE_DATE      DATE,
  VENDOR_SITE_CODE  VARCHAR2(100 BYTE),
  STATUS            VARCHAR2(30 BYTE),
  REJECT_CODE       VARCHAR2(500 BYTE),
  ORG_ID            VARCHAR2(10 BYTE)           DEFAULT NVL(TO_NUMBER(DECODE(SUBSTRB(USERENV('CLIENT_INFO'),1,1), ' ', NULL,SUBSTRB(USERENV('CLIENT_INFO'),1,10))),162)
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX XXWC.XXWC_AP_PAID_INV_STG_N1 ON XXWC.XXWC_AP_PAID_INVOICE_STG
(INVOICE_NUM, VENDOR_NUM)
LOGGING
TABLESPACE APPS_TS_TX_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE SYNONYM APPS.XXWC_AP_PAID_INVOICE_STG FOR XXWC.XXWC_AP_PAID_INVOICE_STG;


CREATE OR REPLACE PUBLIC SYNONYM XXWC_AP_PAID_INVOICE_STG FOR XXWC.XXWC_AP_PAID_INVOICE_STG;


GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AP_PAID_INVOICE_STG TO EA_APEX;
/