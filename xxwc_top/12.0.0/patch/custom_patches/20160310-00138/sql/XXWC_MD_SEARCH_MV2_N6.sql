  /*******************************************************************************
  Index: APPS.XXWC_MD_SEARCH_MV2_N6 
  Description: This index is used on XXWC_MD_SEARCH_PRODUCTS_MV2 Materialized View 
  and is used on AIS form
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     09-Mar-2016        Pahwa Nancy   TMS# 20160310-00138  Performance Tuning
  ********************************************************************************/
 create index APPS.XXWC_MD_SEARCH_MV2_N6 on XXWC_MD_SEARCH_PRODUCTS_MV2 (inventory_item_id);