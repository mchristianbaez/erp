CREATE MATERIALIZED VIEW APPS.XXWC_MD_SEARCH_PRODUCTS_MV2
REFRESH COMPLETE ON DEMAND
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header APPS.XXWC_MD_SEARCH_PRODUCTS_MV2 $
  Module Name: APPS.XXWC_MD_SEARCH_PRODUCTS_MV2 

  PURPOSE:Mv is created from the view so that context indexes can be placed on the mv

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     9-MAR-2016   Pahwa, Nancy                Initially Created 
TMS# 20160310-00138 
**************************************************************************/
AS 
select inventory_item_id,
organization_id,
partnumber,
type,
manufacturerpartnumber,
manufacturer,
sequence,
currencycode,
name,
shortdescription,
longdescription,
thumbnail,
fullimage,
quantitymeasure,
weightmeasure,
weight,
buyable,
keyword,
creation_date,
item_type,
cross_reference,
dummy,
primary_uom_code
 from APPS.XXWC_MD_SEARCH_PRODUCTS_VW2;
/