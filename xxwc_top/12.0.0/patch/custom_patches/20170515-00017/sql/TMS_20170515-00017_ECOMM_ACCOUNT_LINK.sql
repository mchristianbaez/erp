   /********************************************************************************************************************************
      $Header TMS_20170515-00017_ECOMM_ACCOUNT_LINK.SQL$    
      Module Name: EComm Account Linking issue - Data fix 

      PURPOSE:   EComm Account Linking issue - Data fix 

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------------------------------------------------------------
      1.0        15-May-2017 Pattabhi Avula          Initial Version - TMS#20170515-00017   Data fix script for Ecomm issue
   *********************************************************************************************************************************/

BEGIN
Dbms_output.put_line('Increasing the email address length');
execute immediate 'ALTER TABLE xxwc.xxwc_ecomm_email_stg_tbl modify CUSTOMER_EMAIL VARCHAR2(80)'; 	
END;
/