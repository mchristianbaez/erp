/*
 TMS: 20160216-00256
 Date: 02/16/2016
 Notes:  Backup oracle owned tables pn_locations_all and pn_lease_details_all tables before the mass update of documentum links with the new apex links.
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 b_go boolean :=Null;
 n_loc number :=Null;
 --
BEGIN --Main Processing...
   --
   b_go :=TRUE;
   --
   n_loc :=101;
   --
  if (b_go) then 
   --
   n_loc :=102;
   --
   begin
    --
     dbms_output.put_line('Before create of table xxcus.xxcus_tmp_20160216_00256_loc');
     dbms_output.put_line(' ');
    execute immediate 'create table xxcus.xxcus_tmp_20160216_00256_loc as select * from apps.pn_locations_all';
    dbms_output.put_line('After create of table xxcus.xxcus_tmp_20160216_00256_loc'); 
     dbms_output.put_line(' ');    
    --
    n_loc :=104;
    --
   exception
    when others then
     --
     n_loc :=105;
     --
     dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
     --   
   end;
   --
   begin 
    --
    n_loc :=106;
    --
    dbms_output.put_line('Before create of table xxcus.xxcus_tmp_20160216_00256_ldet');
     dbms_output.put_line(' ');    
    execute immediate 'create table xxcus.xxcus_tmp_20160216_00256_ldet as select * from apps.pn_lease_details_all';
    dbms_output.put_line('After create of table xxcus.xxcus_tmp_20160216_00256_ldet');
     dbms_output.put_line(' ');    
    --           
   exception
    when others then
     dbms_output.put_line(('****** @@@@@@ , '||sqlerrm));                  
   end;
   --
  else
   --
   n_loc :=103;
   --
   dbms_output.put_line('@b_go =FALSE, n_loc ='||n_loc);
   --
  end if;  --  if (b_go) then 
 --
 commit;
 --
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line (
         'TMS: 20150804-00228, Errors =' || SQLERRM);
END;
/