/***********************************************************************************************************************************************
   NAME:       TMS_20180207-00091_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        02/17/2018  Rakesh Patel     TMS#20180207-00091
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before LOOP');
   
   UPDATE xxwc.XXWC_OM_DMS2_SIGN_CAPTURE_TBL
      SET SHIP_CONFIRM_STATUS = NULL 
	      ,LAST_UPDATED_BY     = -1
	      ,LAST_UPDATE_DATE    = SYSDATE
	 WHERE ORDER_NUMBER IN ( '27407776' , '27407190', '27395266', '27390752', '27362640', '27387435', '27395718', '27395201');
   
   DBMS_OUTPUT.put_line ('Records Updated -' || SQL%ROWCOUNT);
   COMMIT;

   UPDATE xxwc.XXWC_OM_DMS2_SIGN_CAPTURE_TBL
      SET CONCURRENT_REQUEST_ID = 201009231
	      ,SHIP_CONFIRM_STATUS = NULL 
	      ,LAST_UPDATED_BY     = -1
	      ,LAST_UPDATE_DATE    = SYSDATE
	 WHERE ORDER_NUMBER = '27395971';

   UPDATE xxwc.XXWC_OM_DMS2_SHIP_CONFIRM_TBL
      SET CONCURRENT_REQUEST_ID = 201009231
	      ,LAST_UPDATED_BY     = -1
	      ,LAST_UPDATE_DATE    = SYSDATE
	 WHERE ORDER_NUMBER = '27395971';	 

   DBMS_OUTPUT.put_line ('Records Updated -' || SQL%ROWCOUNT);
   COMMIT;
   
   UPDATE xxwc.XXWC_OM_DMS2_SIGN_CAPTURE_TBL
      SET CONCURRENT_REQUEST_ID = 201006370
	      ,SHIP_CONFIRM_STATUS = NULL 
	      ,LAST_UPDATED_BY     = -1
	      ,LAST_UPDATE_DATE    = SYSDATE
	 WHERE ORDER_NUMBER = '27378465';

   UPDATE xxwc.XXWC_OM_DMS2_SHIP_CONFIRM_TBL
      SET CONCURRENT_REQUEST_ID = 201006370
	      ,LAST_UPDATED_BY     = -1
	      ,LAST_UPDATE_DATE    = SYSDATE
	 WHERE ORDER_NUMBER = '27378465';	 

   DBMS_OUTPUT.put_line ('Records Updated -' || SQL%ROWCOUNT);
   COMMIT;
   
   UPDATE xxwc.XXWC_OM_DMS2_SIGN_CAPTURE_TBL
      SET CONCURRENT_REQUEST_ID = 201005536
	      ,SHIP_CONFIRM_STATUS = NULL 
	      ,LAST_UPDATED_BY     = -1
	      ,LAST_UPDATE_DATE    = SYSDATE
	 WHERE ORDER_NUMBER = '27389749';

   UPDATE xxwc.XXWC_OM_DMS2_SHIP_CONFIRM_TBL
      SET CONCURRENT_REQUEST_ID = 201005536
	      ,LAST_UPDATED_BY     = -1
	      ,LAST_UPDATE_DATE    = SYSDATE
	 WHERE ORDER_NUMBER = '27389749';	 

   DBMS_OUTPUT.put_line ('Records Updated -' || SQL%ROWCOUNT);
   COMMIT;
   
   UPDATE xxwc.XXWC_OM_DMS2_SIGN_CAPTURE_TBL
      SET CONCURRENT_REQUEST_ID = 201009024
	      ,SHIP_CONFIRM_STATUS = NULL 
	      ,LAST_UPDATED_BY     = -1
	      ,LAST_UPDATE_DATE    = SYSDATE
	 WHERE ORDER_NUMBER = '27396209';

   UPDATE xxwc.XXWC_OM_DMS2_SHIP_CONFIRM_TBL
      SET CONCURRENT_REQUEST_ID = 201009024
	      ,LAST_UPDATED_BY     = -1
	      ,LAST_UPDATE_DATE    = SYSDATE
	 WHERE ORDER_NUMBER = '27396209';	 

   DBMS_OUTPUT.put_line ('Records Updated -' || SQL%ROWCOUNT);
   COMMIT;
   
      
   UPDATE xxwc.XXWC_OM_DMS2_SIGN_CAPTURE_TBL
      SET CONCURRENT_REQUEST_ID = 201004979
	      ,SHIP_CONFIRM_STATUS = NULL 
	      ,LAST_UPDATED_BY     = -1
	      ,LAST_UPDATE_DATE    = SYSDATE
	 WHERE ORDER_NUMBER = '27380152';

   UPDATE xxwc.XXWC_OM_DMS2_SHIP_CONFIRM_TBL
      SET CONCURRENT_REQUEST_ID = 201004979
	      ,LAST_UPDATED_BY     = -1
	      ,LAST_UPDATE_DATE    = SYSDATE
	 WHERE ORDER_NUMBER = '27380152';	 

   DBMS_OUTPUT.put_line ('Records Updated -' || SQL%ROWCOUNT);
   COMMIT;
   
      
   UPDATE xxwc.XXWC_OM_DMS2_SIGN_CAPTURE_TBL
      SET CONCURRENT_REQUEST_ID = 201004420
	      ,SHIP_CONFIRM_STATUS = NULL 
	      ,LAST_UPDATED_BY     = -1
	      ,LAST_UPDATE_DATE    = SYSDATE
	 WHERE ORDER_NUMBER = '27385208';

   UPDATE xxwc.XXWC_OM_DMS2_SHIP_CONFIRM_TBL
      SET CONCURRENT_REQUEST_ID = 201004420
	      ,LAST_UPDATED_BY     = -1
	      ,LAST_UPDATE_DATE    = SYSDATE
	 WHERE ORDER_NUMBER = '27385208';	 

   DBMS_OUTPUT.put_line ('Records Updated -' || SQL%ROWCOUNT);
   COMMIT;
   
  EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
	  ROLLBACK;
END;
/