set serveroutput on;
 /*************************************************************************
      PURPOSE:  Close out orders

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        09-MAY-2018  Sundaramoorthy     Initial Version TMS #20180508-00103 (Order: 28400200 Header_id: 72553498)
	                                             Initial Version TMS #20180308-00194 (Order: 27669085 Header_id: 70015330)
												 Initial Version TMS #20180406-00162 (Order: 27917281 Header_id: 70885041)
                                                 Initial Version TMS #20180427-00352 (Order: 27719140 Header_id: 70188610)
												 Initial Version TMS #20180507-00039 (Order: 28212564 Header_id: 71913622)
												 Initial Version TMS #20180508-00198 (Order: 27947633 Header_id: 70993078)
												 
************************************************************************************/
BEGIN
 dbms_output.put_line ('Start Update 1st Script ');

 UPDATE oe_order_headers_all
  SET  flow_status_code ='CANCELLED'
     , open_flag ='N'
   WHERE header_id in (72553498, 70015330, 70885041, 70188610, 71913622, 70993078);  
  
  dbms_output.put_line ('Update count '||SQL%ROWCOUNT);
  COMMIT;
  
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
	dbms_output.put_line ('Inside Exception '|| SUBSTR(SQLERRM,1,2000));	
END;
/	