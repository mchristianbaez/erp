/*
 TMS: 20160126-00032    
 Date: 01/27/2016
 Notes: @SC Data Fix for Items Stuck in PI in Org 297
*/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;


UPDATE mtl_cycle_count_entries 
SET export_flag = NULL 
WHERE organization_id = 341 
AND entry_status_code IN (1,3) 
AND export_flag IS NOT NULL;
		  
--10 row expected to be updated

COMMIT;

/