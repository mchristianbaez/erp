CREATE OR REPLACE PACKAGE BODY APPS.XXWC_BPA_NEW_PRD_PKG
AS
   /**************************************************************************
      $Header XXWC_BPA_NEW_PRD_PKG $
      Module Name: XXWC_BPA_NEW_PRD_PKG.pks

      PURPOSE:   This package is called by the concurrent programs
                 XXWC BPA New Product Build Program
      REVISIONS:
      Ver        Date        Author             Description
      ---------  ----------  ---------------   -------------------------
       1.0       09/29/2016  Neha Saini         Initial Build - Task ID: 20160921-00268
    /*************************************************************************/


   /*************************************************************************
     Procedure : Write_Log

     PURPOSE:   This procedure logs debug message in Concurrent Log file
     Parameter:
            IN
                p_debug_msg      -- Debug Message
    REVISIONS:
       Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       09/29/2016  Neha Saini         Initial Build - Task ID: 20160921-00268
   ************************************************************************/
   PROCEDURE write_log (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
   END write_log;

   /*************************************************************************
      Procedure : Write_Error

     PURPOSE:   This procedure logs error message
     Parameter:
            IN
                p_debug_msg      -- Debug Message
    REVISIONS:
       Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
     1.0       09/29/2016  Neha Saini         Initial Build - Task ID: 20160921-00268
   ************************************************************************/

   --add message to concurrent output file
   PROCEDURE write_error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_BPA_NEW_PRD_PKG';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running XXWC_BPA_NEW_PRD_PKG with PROGRAM ERROR',
         p_distribution_list   => l_distro_list,
         p_module              => 'PO');
   END write_error;


   /*************************************************************************
     Procedure : process BPA
     PARAMETERS:  IN - None
                  OUT - errbuf,retcode
     PURPOSE:  This procedure creates file for the concurrent program   XXWC BPA New Product Build Program
     REVISIONS:
       Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       09/29/2016  Neha Saini         Initial Build - Task ID: 20160921-00268
   ************************************************************************/
   PROCEDURE process_bpa (errbuf          OUT VARCHAR2,
                          retcode         OUT VARCHAR2,
                          p_item_number       VARCHAR2)
   IS
      g_vendor_exception     EXCEPTION;
      g_prog_exception       EXCEPTION;
      l_err_msg              VARCHAR2 (32767);
      l_list_price           mtl_system_items_b.LIST_PRICE_PER_UNIT%TYPE;
      l_bpa_price            NUMBER := 0;
      l_org_id               NUMBER := 222;
      l_inventory_item_id    mtl_system_items_b.inventory_item_id%TYPE;
      l_description          mtl_system_items_b.description%TYPE;
      l_uom_code             mtl_system_items_b.primary_unit_of_measure%TYPE;
      v_vendor_id            po_vendors.vendor_id%TYPE;
      l_return_status        VARCHAR2 (32767);
      l_expiration_date      po_lines_all.expiration_date%TYPE;
      l_line_num             po_lines_all.line_num%TYPE;
      v_po_number            po_headers_all.segment1%TYPE;
      v_po_header_id         po_headers_all.po_header_id%TYPE;
      v_type_lkup_code       po_headers_all.type_lookup_code%TYPE;
      v_vendor_site_id       po_headers_all.vendor_site_id%TYPE;
      v_agent_id             po_headers_all.agent_id%TYPE;
      v_shipto_location_id   po_headers_all.ship_to_location_id%TYPE;
      v_resp_id              apps.fnd_responsibilIty_vl.responsibility_id%TYPE;
      v_app_id               apps.fnd_application_vl.application_id%TYPE;
      lvc_vendor_number      VARCHAR2 (100);
      v_header_id            NUMBER;
      ln_count               NUMBER;
   BEGIN
      write_log (' start the procedure process_bpa');
      retcode := '0';
      l_err_msg := NULL;

      --Initialize apps
      fnd_global.apps_initialize (user_id        => FND_global.user_id,
                                  resp_id        => fnd_global.RESP_ID,
                                  resp_appl_id   => fnd_global.RESP_APPL_ID);
      mo_global.init ('PO');
      mo_global.set_policy_context ('S', 162);

      write_log (' apps initialization completed ');

      BEGIN
         --getting list price /Inventory Id details
         l_err_msg := 'getting  list price and Inventory Item Id';
         write_log (l_err_msg);

         BEGIN
            SELECT LIST_PRICE_PER_UNIT, inventory_item_id
              INTO l_list_price, l_inventory_item_id
              FROM mtl_system_items_b
             WHERE segment1 LIKE p_item_number AND organization_id = l_org_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_err_msg :=
                  'Error Occurred - List Price ' || SUBSTR (SQLERRM, 1, 250);
               write_log (l_err_msg);
               RAISE g_prog_exception;
         END;

         --get vendor details
         l_err_msg := 'getting  vendor details';
         write_log (l_err_msg);

         BEGIN
            write_log (
                  'l_org_id '
               || l_org_id
               || 'l_inventory_item_id '
               || l_inventory_item_id);

            BEGIN
               SELECT ATTRIBUTE1
                 INTO lvc_vendor_number
                 FROM APPS.MTL_CROSS_REFERENCES
                WHERE     CROSS_REFERENCE_TYPE = 'VENDOR'
                      AND INVENTORY_ITEM_ID = l_inventory_item_id
                      AND ATTRIBUTE1 IS NOT NULL
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  lvc_vendor_number := NULL;
                  l_err_msg :=
                     'Invalid Vendor Number at Cross Reference Level';
                  --RAISE g_vendor_exception; --Commented by Ashwin.S on 11-Dec-2017 for #TMS-20171121-00061
				  GOTO LAST_STEP;             --Added by Ashwin.S on 11-Dec-2017 for #TMS-20171121-00061
               WHEN OTHERS
               THEN
                  lvc_vendor_number := NULL;
                  l_err_msg := SUBSTR (SQLERRM, 1, 250);
                  RAISE g_prog_exception;
            END;

            IF lvc_vendor_number IS NOT NULL
            THEN
               SELECT vendor_id
                 INTO v_vendor_id
                 FROM po_vendors
                WHERE segment1 = lvc_vendor_number;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               write_log ('Vendor ' || SUBSTR (SQLERRM, 1, 250));
               RAISE g_prog_exception;
         END;

         --Vendor site id:
         l_err_msg := 'getting  vendor site details';
         write_log (l_err_msg);

         BEGIN
            SELECT vendor_site_id
              INTO v_vendor_site_id
              FROM ap_supplier_sites_all assa
             WHERE     assa.vendor_id = v_vendor_id
                   AND assa.org_id = 162
                   AND NVL (assa.purchasing_site_flag, 'N') = 'Y'
                   AND assa.vendor_site_code IN ('PURCHASING', '0PURCHASING')
                   AND SYSDATE < NVL (assa.inactive_date, SYSDATE + 1)
                   AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               write_log ('Vendor Site ' || SUBSTR (SQLERRM, 1, 250));
               RAISE g_prog_exception;
         END;


         BEGIN
            --get agent id/ship to location id and type lookup code
            l_err_msg := 'getting  agent or ship to location details';
            write_log (l_err_msg);

            SELECT segment1,
                   po_header_id,
                   type_lookup_code,
                   vendor_site_id,
                   agent_id,
                   ship_to_location_id
              INTO v_po_number,
                   v_po_header_id,
                   v_type_lkup_code,
                   v_vendor_site_id,
                   v_agent_id,
                   v_shipto_location_id
              FROM (  SELECT segment1,
                             po_header_id,
                             type_lookup_code,
                             vendor_site_id,
                             agent_id,
                             ship_to_location_id
                        FROM po_headers_all
                       WHERE     global_agreement_flag = 'Y'
                             AND NVL (cancel_flag, 'N') = 'N'
                             AND type_lookup_code = 'BLANKET'
                             AND vendor_site_id = v_vendor_site_id
                             AND vendor_id = v_vendor_id
                             AND org_id = 162
                             AND SYSDATE BETWEEN NVL (start_date, SYSDATE - 1)
                                             AND NVL (end_date, SYSDATE + 1)
                    ORDER BY CREATION_DATE DESC)
             WHERE ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               write_log ('PO Info ' || SUBSTR (SQLERRM, 1, 250));
			   GOTO LAST_STEP;            --Added by Ashwin.S on 11-Dec-2017 for #TMS-20171121-00061
               --RAISE g_prog_exception;  --Commented by Ashwin.S on 11-Dec-2017 for #TMS-20171121-00061
         END;

         write_log ('All variables value collected ');
         write_log ('po number ' || v_po_number);
         write_log ('type lookup code ' || v_type_lkup_code);
         write_log ('agent id ' || v_agent_id);
         write_log (' v_shipto_location_id ' || v_shipto_location_id);
         write_log (
               ' l_list_price  '
            || l_list_price
            || ' l_inventory_item_id '
            || l_inventory_item_id);
         write_log (' v_vendor_id ' || v_vendor_id);
         write_log (' v_vendor_site_id ' || v_vendor_site_id);
      END;

      IF v_po_number IS NOT NULL
      THEN
         -- Insert Header Record
         BEGIN
            write_log ('now inserting into header interface');

            SELECT po.po_headers_interface_s.NEXTVAL   -- get unique header_id
              INTO v_header_id
              FROM DUAL;



            INSERT INTO po.po_headers_interface (interface_header_id,
                                                 batch_id,
                                                 po_header_id,
                                                 process_code,
                                                 action,
                                                 org_id,
                                                 document_type_code,
                                                 document_num,
                                                 vendor_id,
                                                 vendor_site_id,
                                                 vendor_doc_num,
                                                 agent_id,
                                                 ship_to_location_id,
                                                 effective_date,
                                                 global_agreement_flag)
               SELECT v_header_id,
                      v_header_id,
                      v_po_header_id,
                      'PENDING',
                      'UPDATE',
                      162,
                      v_type_lkup_code,                     --need to find out
                      p_item_number,
                      v_vendor_id,
                      v_vendor_site_id,
                      NULL,
                      v_agent_id,                          -- need to find out
                      v_shipto_location_id,                -- need to find out
                      SYSDATE,
                      'Y'
                 FROM DUAL;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_err_msg :=
                     'Error while inserting into po header interface '
                  || SQLERRM;
               write_log (l_err_msg);
         END;

         write_log ('now inserting into header interface done ');

         -- item information for inserting into interface lines

         BEGIN
            write_log ('getting inv item id,desc,uom code');

            SELECT description, primary_unit_of_measure
              INTO l_description, l_uom_code
              FROM mtl_system_items_b
             WHERE     inventory_item_id = l_inventory_item_id
                   AND organization_id = l_org_id;

            write_log ('l_inventory_item_id ' || l_inventory_item_id);
            write_log (' l_description ' || l_description);
            write_log (' l_uom_code ' || l_uom_code);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_inventory_item_id := NULL;
               l_description := NULL;
               l_uom_code := NULL;
               l_err_msg :=
                     'Error while getting po lines info like inv item id,uom code and desc '
                  || SUBSTR (SQLERRM, 1, 250);
               write_log (l_err_msg);
         END;


         BEGIN
            write_log ('getting expiration date ');

            SELECT expiration_date
              INTO l_expiration_date
              FROM po_lines_all
             WHERE po_header_id = v_po_header_id AND ROWNUM = 1;

            write_log (
                  'l_expiration_date '
               || TO_CHAR (l_expiration_date, 'DD-MON-YYYY'));
         EXCEPTION
            WHEN OTHERS
            THEN
               l_expiration_date := NULL;

               l_err_msg :=
                     'Error while getting expiration date '
                  || SUBSTR (SQLERRM, 1, 250);
               write_log (l_err_msg);
         END;

         BEGIN
            write_log ('getting max po line num ');

            SELECT MAX (line_num) + 1
              INTO l_line_num
              FROM po_lines_all
             WHERE po_header_id = v_po_header_id;

            write_log (l_line_num);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_line_num := 1;
               l_err_msg := 'Error while getting line num ';
               write_log (l_err_msg);
         END;

         -- Adding po Lines interface
         BEGIN
            write_log ('now inserting into line interface ');

            INSERT INTO po.po_lines_interface (interface_line_id,
                                               interface_header_id,
                                               process_code,
                                               action,
                                               line_num,
                                               shipment_num,
                                               line_type,
                                               item,
                                               item_id,
                                               item_description,
                                               unit_of_measure,
                                               quantity,
                                               unit_price,
                                               promised_date,
                                               need_by_date,
                                               vendor_product_num,
                                               consigned_flag,
                                               NEGOTIATED_BY_PREPARER_FLAG,
                                               ALLOW_PRICE_OVERRIDE_FLAG,
                                               NOT_TO_EXCEED_PRICE,
                                               effective_date,
                                               expiration_date)
                 VALUES (po.po_lines_interface_s.NEXTVAL,
                         v_header_id,                           --PO_HEADER_ID
                         'PENDING',
                         'UPDATE',
                         l_line_num,
                         NULL,
                         'Goods',
                         p_item_number,
                         l_INVENTORY_ITEM_ID,
                         l_DESCRIPTION,
                         l_uom_code,                      --mtl_system items b
                         NULL,
                         l_bpa_price,
                         NULL,
                         NULL,
                         NULL,
                         NULL,
                         'N',
                         'Y',
                         NULL,
                         SYSDATE,
                         l_expiration_date);

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_err_msg :=
                  'Error while inserting into po lines interface ' || SQLERRM;
               write_log (l_err_msg);
         -- RAISE g_prog_exception;
         END;

         write_log ('now inserting into line interface done ');

         --Start Aproval Process

         BEGIN
            write_log ('now calling  "PO_PDOI_GRP.start_process "');

            PO_PDOI_GRP.start_process (
               p_api_version                  => 1.0,
               p_init_msg_list                => 'T',
               p_validation_level             => 100,
               p_commit                       => 'T',
               x_return_status                => l_return_status,
               p_gather_intf_tbl_stat         => 'F',
               p_calling_module               => 'UNKNOWN', --PO_PDOI_CONSTANTS.g_CALL_MOD_CONCURRENT_PRGM,
               p_selected_batch_id            => v_header_id,    --poheader id
               p_batch_size                   => 5000, --PO_PDOI_CONSTANTS.g_DEF_BATCH_SIZE,
               p_buyer_id                     => NULL,
               p_document_type                => 'BLANKET',
               p_document_subtype             => NULL,
               p_create_items                 => 'N',
               p_create_sourcing_rules_flag   => 'N',
               p_rel_gen_method               => NULL,
               p_sourcing_level               => NULL,
               p_sourcing_inv_org_id          => NULL,
               p_approved_status              => 'APPROVED',
               p_process_code                 => 'PENDING', --PO_PDOI_CONSTANTS.g_process_code_PENDING,
               p_interface_header_id          => NULL,
               p_org_id                       => 162,
               p_ga_flag                      => NULL);

            BEGIN
               SELECT COUNT (1)
                 INTO ln_count
                 FROM XXWC.XXWC_BPA_PRICE_ZONE_TBL
                WHERE     inventory_item_id = l_inventory_item_id
                      AND po_header_id = v_po_header_id;

               IF NVL (ln_count, 0) = 0
               THEN
                  INSERT
                    INTO APPS.XXWC_BPA_PRICE_ZONE_TBL (creation_date,
                                                       created_by,
                                                       last_update_date,
                                                       last_updated_by,
                                                       po_header_id,
                                                       inventory_item_id,
                                                       price_zone,
                                                       price_zone_price)
                  VALUES (SYSDATE,
                          fnd_global.user_id,
                          SYSDATE,
                          fnd_global.user_id,
                          v_po_header_id,
                          l_inventory_item_id,
                          0,
                          l_list_price);
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  NULL;
            END;

            COMMIT;

            write_log (
                  'now calling  "PO_PDOI_GRP.start_process " done l_return_status '
               || l_return_status);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_err_msg :=
                     'Error while calling PO_PDOI_GRP.start_process l_return_status '
                  || l_return_status
                  || ' Error '
                  || SQLERRM;
               write_log (l_err_msg);
               RAISE g_prog_exception;
         END;
      ELSE
         write_log ('PO Number is blank ');
         retcode := 1;
      END IF;

	  <<LAST_STEP>>           --Added by Ashwin.S on 11-Dec-2017 for #TMS-20171121-00061
	  write_log (l_err_msg);  --Added by Ashwin.S on 11-Dec-2017 for #TMS-20171121-00061
      write_log (
         'XXWC BPA New Product Build Program - Program Successfully completed');
   EXCEPTION
      WHEN g_vendor_exception
      THEN
         retcode := '1';
         --write_error (l_err_msg); --Commented by Ashwin.S on 11-Dec-2017 for #TMS-20171121-00061
         write_log (l_err_msg);
      WHEN g_prog_exception
      THEN
         errbuf := l_err_msg || ' : ' || SQLERRM;
         retcode := '1';
         write_error (l_err_msg);
         write_log (l_err_msg);
      WHEN OTHERS
      THEN
         l_err_msg := 'Error Msg :' || SQLERRM;
         errbuf := l_err_msg;
         retcode := '2';
         write_error (l_err_msg);
         write_log (l_err_msg);
   END process_bpa;

   PROCEDURE submit_bpa (errbuf            OUT VARCHAR2,
                         retcode           OUT VARCHAR2,
                         p_request_id   IN     NUMBER)
   IS
      CURSOR c1 (p_request_id NUMBER)
      IS
         SELECT segment1
           FROM apps.MTL_SYSTEM_ITEMS_INTERFACE
          WHERE request_id = p_request_id;

      lv_request_id         NUMBER;
      lc_phase              VARCHAR2 (50);
      lc_status             VARCHAR2 (50);
      lc_dev_phase          VARCHAR2 (50);
      lc_dev_status         VARCHAR2 (50);
      lc_message            VARCHAR2 (50);
      l_req_return_status   BOOLEAN;
      LN_COUNT              NUMBER;
      lvc_dev_phase         VARCHAR2 (100);
   BEGIN
      SELECT PHASE_CODE
        INTO lvc_dev_phase
        FROM APPS.FND_CONCURRENT_REQUESTS
       WHERE REQUEST_ID = P_REQUEST_ID;

      FND_FILE.PUT_LINE (FND_FILE.LOG, 'lvc_dev_phase ' || lvc_dev_phase);

      IF lvc_dev_phase = 'R'
      THEN
         LOOP
            l_req_return_status :=
               fnd_concurrent.wait_for_request (request_id   => p_request_id,
                                                INTERVAL     => 5 --interval Number of seconds to wait between checks
                                                                 ,
                                                max_wait     => 60 --Maximum number of seconds to wait for the request completion
                                                                  -- out arguments
                                                ,
                                                phase        => lc_phase,
                                                STATUS       => lc_status,
                                                dev_phase    => lc_dev_phase,
                                                dev_status   => lc_dev_status,
                                                MESSAGE      => lc_message);

            EXIT WHEN    UPPER (lc_phase) = 'COMPLETED'
                      OR UPPER (lc_status) IN ('CANCELLED',
                                               'ERROR',
                                               'TERMINATED');
         END LOOP;
      END IF;

      IF lc_phase = 'COMPLETED' OR LVC_DEV_PHASE = 'C'
      THEN
         SELECT COUNT (1)
           INTO LN_COUNT
           FROM apps.MTL_SYSTEM_ITEMS_INTERFACE
          WHERE request_id = p_request_id;

         FND_FILE.PUT_LINE (FND_FILE.LOG, 'ln_count ' || ln_count);

         FOR c2 IN c1 (p_request_id)
         LOOP
            lv_request_id :=
               fnd_request.submit_request (
                  application   => 'XXWC',
                  program       => 'XXWC_BPA_NEW_PRD_PKG',
                  description   => NULL,
                  start_time    => TO_CHAR (SYSDATE + 5 / 1440,
                                            'DD-MON-YYYY HH24:MI:SS'),
                  sub_request   => FALSE,
                  argument1     => C2.SEGMENT1);
            fnd_file.put_line (
               fnd_File.LOG,
               ' Item ' || C2.SEGMENT1 || ' Request id' || lv_request_id);
            COMMIT;
         END LOOP;
      END IF;
   END;
END XXWC_BPA_NEW_PRD_PKG;
/