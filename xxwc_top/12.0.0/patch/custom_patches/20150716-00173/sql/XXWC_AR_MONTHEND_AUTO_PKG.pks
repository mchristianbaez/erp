CREATE OR REPLACE PACKAGE APPS.XXWC_AR_MONTHEND_AUTO_PKG
IS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_AR_MONTHEND_AUTO_PKG.pks $
   *   Module Name: XXWC_AR_MONTHEND_AUTO_PKG.pks
   *
   *   PURPOSE:    This package is used to Automatate AR month end process.
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        05/03/2016  Neha Saini               Initial Version
   *   1.1       9/26/2016    Neha Saini             TMS# 20150716-00173 Added new procedure and changed MV name for snapshot
   * ***************************************************************************/


   --Global variable to store the org id
   g_org_id            NUMBER := fnd_profile.VALUE ('ORG_ID');
   g_conc_request_id   NUMBER := FND_GLOBAL.CONC_REQUEST_ID;
   g_user_id           NUMBER := FND_GLOBAL.USER_ID;


   /**************************************************************************
   *   procedure Name: check_unprocessed_lines
   *
   *   PURPOSE:  This procedure is used to check if there any unprocessed lines in interface table
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        05/03/2016  Neha Saini             Initial Version TMS# 20150716-00173 AR month end process
   * ***************************************************************************/
   PROCEDURE check_unprocessed_lines ( p_retVal OUT VARCHAR2);

   /**************************************************************************
   *   procedure Name: get_current_period
   *
   *   PURPOSE:   This procedure is used to get current period
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        05/03/2016  Neha Saini             Initial Version TMS# 20150716-00173 AR month end process
   * ***************************************************************************/
   PROCEDURE get_current_period (p_current_period_name   OUT VARCHAR2,
                                 p_future_period_name    OUT VARCHAR2,
                                 p_start_date            OUT DATE,
                                 p_end_date              OUT DATE,
                                 p_oem_sunday            OUT DATE,
                                 p_oem_sunday_check      OUT VARCHAR2,
                                 p_error_message         OUT VARCHAR2);

   /*************************************************************************
   *   Procedure : open_next_period
   *
   *   PURPOSE:   This procedure is to open new period.
   *   Parameter:
   *          IN  NONE
   *          OUT   ERRBUF,RETCODE
   * ************************************************************************/

   PROCEDURE open_next_period (ERRBUF              OUT VARCHAR2,
                               RETCODE             OUT NUMBER);

   /*************************************************************************
  *   Procedure : pending_close_period
  *
  *   PURPOSE:   This procedure is to pending close the current period.
  *   Parameter:
  *          IN  NONE
  *          OUT   ERRBUF,RETCODE
  * ************************************************************************/

   PROCEDURE pending_close_period (ERRBUF              OUT VARCHAR2,
                                   RETCODE             OUT NUMBER);

   /*************************************************************************
   *   Procedure : get_snap_shot_mv
   *
   *   PURPOSE:   This procedure is to take snapshot for AR CUSTOMER BAL MV.
   *   Parameter:
   *          IN  NONE
   *          OUT   ERRBUF,RETCODE
   * ************************************************************************/

   PROCEDURE get_snap_shot_mv(ERRBUF              OUT VARCHAR2,
                              RETCODE             OUT NUMBER);
   /*************************************************************************
   *   Procedure : unposted_items_report
   *
   *   PURPOSE:   This procedure is to submit unposted items report.
   *   Parameter:
   *          IN  None
   *          OUT   None
   * ************************************************************************/ 
   --ver1.1 starts
   PROCEDURE unposted_items_report;
     /*************************************************************************
   *   Procedure : get_amount_due
   *
   *   PURPOSE:   This procedure is to get amount due remaining comparison
   *   Parameter:
   *          IN  NONE
   *          OUT   ERRBUF,RETCODE
   * ************************************************************************/

   PROCEDURE get_amount_due(ERRBUF              OUT VARCHAR2,
                              RETCODE             OUT NUMBER); 
  --ver1.1 ends
END XXWC_AR_MONTHEND_AUTO_PKG;
/