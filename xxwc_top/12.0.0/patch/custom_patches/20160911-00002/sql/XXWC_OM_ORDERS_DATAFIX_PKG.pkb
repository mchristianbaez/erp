CREATE OR REPLACE PACKAGE BODY xxwc_om_orders_datafix_pkg
AS
   /********************************************************************************************************************************
      $Header XXWC_OM_ORDERS_DATAFIX_PKG.PKG $    
      Module Name: XXWC_OM_ORDERS_DATAFIX_PKG.PKG

      PURPOSE:   This package is used for data fix of order management

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------------------------------------------------------------
      1.0        22-Sep-2016 Niraj K Ranjan          Initial Version - TMS#20160911-00002   Data fix scripts into Concurrent program
   *********************************************************************************************************************************/

   --Email Defaults
   g_dflt_email   fnd_user.email_address%TYPE
                     := 'WC-ITDEVALERTS-U1@HDSupply.com';
   g_pkg_name     VARCHAR2 (50) := 'XXWC_OM_ORDERS_DATAFIX_PKG';

   /*************************************************************************
      PROCEDURE Name: Update 

      PURPOSE:   Update line attributes to process stucked order line

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        18-May-2016    Niraj             Initial Version
   ****************************************************************************/
   PROCEDURE update_record_status ( p_order_header_id  IN NUMBER
                                   ,p_order_line_id    IN NUMBER
                                   ,p_request_id       IN NUMBER
								   ,p_data_fix_script  IN VARCHAR2
                                   ,p_status           IN VARCHAR2
								   ,p_errmsg           IN VARCHAR2
                                  )
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
	  l_errmsg VARCHAR2(2000);
   BEGIN
      fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG , 'Start of Procedure : update_record_status'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      UPDATE xxwc_om_orders_datafix_log_tbl
	  SET  STATUS           = p_status,
	       ERROR_MESSAGE    = p_errmsg,
		   LAST_UPDATE_DATE = SYSDATE,
		   LAST_UPDATED_BY  = FND_GLOBAL.USER_ID
	  WHERE ORDER_HEADER_ID = p_order_header_id
	  AND   ORDER_LINE_ID   = p_order_line_id
	  AND   REQUEST_ID      = p_request_id 
	  AND   data_fix_script = p_data_fix_script;
      COMMIT;
	  fnd_file.put_line (fnd_file.LOG , 'End of Procedure : update_record_status'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
   EXCEPTION
      WHEN OTHERS THEN
	     ROLLBACK;
	     l_errmsg := SUBSTR(SQLERRM,1,2000);
	     fnd_file.put_line (fnd_file.LOG , 'When Others Error for header_id:'||p_order_header_id||' Line_id:'||p_order_line_id||'=>'||l_errmsg);
   END update_record_status;
    /*************************************************************************
      PROCEDURE Name: Correct_awaiting_Order

      PURPOSE:   Update line attributes to process stucked order line

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        18-May-2016    Niraj             Initial Version
   ****************************************************************************/
   PROCEDURE insert_record_log ( p_order_header_id IN  NUMBER
                                ,p_order_line_id   IN  NUMBER
                                ,p_request_id      IN  NUMBER
                                ,p_script_name     IN  VARCHAR2
                               )
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
	  l_errmsg VARCHAR2(2000);
   BEGIN
      fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG , 'Start of Procedure : insert_record_log'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      INSERT INTO xxwc_om_orders_datafix_log_tbl ( ORDER_HEADER_ID  
                                         ,ORDER_LINE_ID    
                                         ,REQUEST_ID       
                                         ,DATA_FIX_SCRIPT  
			                             ,STATUS           
			                             ,ERROR_MESSAGE    
			                             ,CREATION_DATE    
			                             ,CREATED_BY       
			                             ,LAST_UPDATE_DATE 
			                             ,LAST_UPDATED_BY  
			                            )
                                 VALUES (p_order_header_id,
                                         p_order_line_id,
                                         p_request_id,
									     p_script_name,
									     'Entered',
									     '',
                                         SYSDATE,
                                         FND_GLOBAL.USER_ID,
                                         SYSDATE,
                                         FND_GLOBAL.USER_ID);
	  COMMIT;
      fnd_file.put_line (fnd_file.LOG , 'End of Procedure : insert_record_log'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
   EXCEPTION
      WHEN OTHERS THEN
	     ROLLBACK;
	     l_errmsg := SUBSTR(SQLERRM,1,2000);
	     fnd_file.put_line (fnd_file.LOG , 'When Others Error for header_id:'||p_order_header_id||' Line_id:'||p_order_line_id||'=>'||l_errmsg);
   END insert_record_log;
    /*************************************************************************
      PROCEDURE Name: Correct_awaiting_Order

      PURPOSE:   Update line attributes to process stucked order line

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        18-May-2016    Niraj             Initial Version
   ****************************************************************************/
   PROCEDURE execute_data_fixes ( p_errbuf         OUT VARCHAR2
                                 ,p_retcode        OUT NUMBER
                                 ,p_start_date     IN  VARCHAR2
                                 )
   IS
      e_user_exception         EXCEPTION;
      l_err_msg                VARCHAR2 (2000);
      l_err_code               VARCHAR2(200);
      l_sec                    VARCHAR2 (150);
	  l_request_id             NUMBER;
	  l_script_name            VARCHAR2(50);
	  l_execution_date         DATE;
	  l_delivery_detail_id     NUMBER;
	  l_updated_rowcount       NUMBER;
	  l_org_id                 NUMBER;
	  l_return_status          BOOLEAN;
	  l_locked_line            VARCHAR2(2);

	  
	  --ITS_DELIVERY_CLOSED
	  CURSOR cr_its_del_closed(p_exc_date VARCHAR2)
	  IS
	     SELECT ool.header_id,ool.line_id,ool.flow_status_code,ool.rowid line_rowid
          FROM oe_order_lines ool
          WHERE  1=1
		  AND TRUNC(ool.last_update_date) = TO_DATE(p_exc_date,'DD-MON-YY')
          AND ool.ordered_quantity = 0
          AND ool.flow_status_code IN ('CANCELLED', 'AWAITING_SHIPPING','PICKED')
		  AND EXISTS(SELECT 1
                     FROM wsh_delivery_details wdd
                     WHERE wdd.source_line_id = ool.line_id
					 AND   wdd.released_status = 'C');

      --ITS_DELIVERY_CANCELED			   
      CURSOR cr_its_del_canceled(p_exc_date VARCHAR2)
	  IS
	     SELECT ool.header_id,ool.line_id,ool.flow_status_code,ool.rowid line_rowid
          FROM oe_order_lines ool
          WHERE  1=1
		  AND TRUNC(ool.last_update_date) = TO_DATE(p_exc_date,'DD-MON-YY')
          AND ool.ordered_quantity = 0
          AND ool.flow_status_code IN ('AWAITING_SHIPPING')
		  AND EXISTS(SELECT 1
                     FROM wsh_delivery_details wdd
                     WHERE wdd.source_line_id = ool.line_id
					 AND   wdd.released_status = 'D');

      --ITS_INTERFACED_FLAG
      CURSOR cr_interfaced_flag(p_exc_date VARCHAR2)
	  IS
	     SELECT ool.header_id,ool.line_id,ool.flow_status_code,ool.rowid line_rowid
         FROM  oe_order_lines ool
         WHERE 1=1
         AND   TRUNC(ool.last_update_date) = TO_DATE(p_exc_date,'DD-MON-YY')
         AND   ool.flow_status_code NOT IN ('AWAITING_SHIPPING',
                                            'BOOKED',
                                            'SHIPPED',
											'CANCELLED')
         AND   EXISTS(SELECT 1 FROM apps.wsh_delivery_Details wdd
                      WHERE wdd.source_line_id   = ool.line_id
                      AND wdd.oe_interfaced_flag = 'N'
                      AND wdd.released_status    = 'C'
                     );

	  --DROP_SHIP_LINE_FIX
      CURSOR cr_drop_ship_line_fix(p_exc_date VARCHAR2)
	  IS
	      SELECT ool.header_id,ool.line_id,ool.rowid line_rowid
          FROM oe_order_lines ool
          WHERE 1 = 1
          AND   TRUNC(ool.last_update_date) = TO_DATE(p_exc_date,'DD-MON-YY')
          AND   ool.flow_status_code = 'BOOKED'
          AND   EXISTS(SELECT 1
                    FROM rcv_transactions rcv,
                         po_line_locations poll,
                         oe_drop_ship_sources oed
                   WHERE    ool.source_type_code = 'EXTERNAL'
                         AND NVL (ool.shipped_quantity, 0) = 0
                         AND oed.line_id = ool.line_id
                         AND oed.line_location_id IS NOT NULL
                         AND poll.line_location_id = oed.line_location_id
                         AND rcv.po_line_location_id = poll.line_location_id
                         AND rcv.TRANSACTION_TYPE = 'DELIVER');

      CURSOR pending_receipts(p_order_line_id NUMBER)
      IS
         SELECT rcv.transaction_id
           FROM rcv_transactions rcv,
                po_line_locations poll,
                oe_drop_ship_sources oed,
                oe_order_lines oel
          WHERE     oel.source_type_code = 'EXTERNAL'
                AND NVL (oel.shipped_quantity, 0) = 0
                AND oed.line_id = oel.line_id
                AND oed.line_location_id IS NOT NULL
                AND poll.line_location_id = oed.line_location_id
                AND rcv.po_line_location_id = poll.line_location_id
                AND oel.line_id = p_order_line_id
                AND rcv.TRANSACTION_TYPE = 'DELIVER';
		  
      --AWAITING_INVOICE_FIX
	  CURSOR cr_awaiting_invoice_fix(p_exc_date VARCHAR2)
      IS
	  SELECT ool.header_id,ool.line_id,ool.rowid line_rowid
      FROM   apps.oe_order_lines ool,apps.oe_order_headers ooh
      WHERE  1 = 1
      AND    ool.header_id = ooh.header_id
      AND   TRUNC(ool.last_update_date) >= TO_DATE(p_exc_date,'DD-MON-YY')
      AND   ool.flow_status_code = 'INVOICE_HOLD'
      AND   EXISTS(SELECT  /*+ INDEX(rctl XXWC_RA_CUST_TRX_LINES_N13)*/ 1
                   FROM apps.ra_customer_trx_lines rctl
                   WHERE rctl.interface_line_attribute6 = to_char(ool.line_id)
                   AND   rctl.interface_line_attribute1 = to_char(ooh.order_number)
                  );
	  /*SELECT ool.header_id,ool.line_id,ool.rowid line_rowid
      FROM   oe_order_lines_all ool
      WHERE  1 = 1
      AND   TRUNC(ool.last_update_date) = TO_DATE(p_exc_date,'DD-MON-YY')
      AND   ool.flow_status_code = 'INVOICE_HOLD'
      AND   EXISTS(SELECT  /*+ INDEX(rctl XXWC_RA_CUST_TRX_LINES_N14)*/ --1
                   /*FROM ra_customer_trx_lines_all rctl
                   WHERE rctl.interface_line_attribute6 = to_char(ool.line_id)
                  );*/
				  				  
   BEGIN
      fnd_file.put_line (fnd_file.LOG ,'=========================================================================================');
      fnd_file.put_line (fnd_file.LOG , 'Start of Procedure : execute_data_fixes'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      l_request_id := fnd_global.conc_request_id;
	  fnd_file.put_line (fnd_file.LOG , 'l_request_id:'||l_request_id);
	  fnd_file.put_line (fnd_file.LOG , 'p_start_date:'||p_start_date);
	  l_execution_date := fnd_date.canonical_to_date(p_start_date);
	  fnd_file.put_line (fnd_file.LOG , 'l_execution_date:'||l_execution_date);
	  
      l_sec := 'Set policy context - 162';	   
      apps.mo_global.set_policy_context('S',162);
	  
	  ---------------------------------------------------------------------------------------------------------------------------
	  --1.1	Line has been cancelled, however the delivery has been created and trying to ship for ZERO quantity, line status in 
	  --    Picked/Cancelled status (Interface trip stops completes warning) - ITS_DELIVERY_CLOSED
	  ---------------------------------------------------------------------------------------------------------------------------
	  l_script_name := 'ITS_DELIVERY_CLOSED';
	  l_sec := 'Start loop cr_its_del_closed';
	  fnd_file.put_line (fnd_file.LOG , 'Start of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
	  FOR rec_its_del_closed IN cr_its_del_closed(l_execution_date)
	  LOOP
	     BEGIN
            l_sec := l_script_name||' - '||'Check if record is locked';
		    l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec_its_del_closed.line_rowid, 'OE_ORDER_LINES_ALL');
			IF l_locked_line = 'N' THEN
		       l_sec := 'Insert record for '||l_script_name;
		       insert_record_log( p_order_header_id   => rec_its_del_closed.header_id
			                     ,p_order_line_id     => rec_its_del_closed.line_id
			                     ,p_request_id        => l_request_id
			                     ,p_script_name       => l_script_name
			                    );
	           BEGIN
			      l_sec := l_script_name||' - '||'Fetch delivery_detail_id';
	              SELECT delivery_detail_id INTO l_delivery_detail_id
                    FROM wsh_delivery_details wdd
                   WHERE wdd.source_line_id = rec_its_del_closed.line_id;
		       EXCEPTION
		          WHEN NO_DATA_FOUND THEN
		  	         l_err_msg := l_sec||' - '||'Delivery not found for line id:'||rec_its_del_closed.line_id;
			   	     RAISE e_user_exception;
				  WHEN TOO_MANY_ROWS THEN
				     l_err_msg := l_sec||' - '||'More than one delivery id found for line id:'||rec_its_del_closed.line_id;
			   	     RAISE e_user_exception;
		       END;
			   l_updated_rowcount := 0;
			   l_sec := 'UPDATE apps.wsh_delivery_details';
               UPDATE apps.wsh_delivery_details
                  SET released_status = 'D',
                      src_requested_quantity = 0,
                      requested_quantity = 0,
                      shipped_quantity = 0,
                      cycle_count_quantity = 0,
                      cancelled_quantity = 0,
                      subinventory = NULL,
                      locator_id = NULL,
                      lot_number = NULL,
                      revision = NULL,
                      inv_interfaced_flag = 'X',
                      oe_interfaced_flag = 'X'
                WHERE delivery_detail_id = l_delivery_detail_id;
			    l_updated_rowcount := SQL%ROWCOUNT;
			    
			    IF l_updated_rowcount > 0 THEN
			       l_updated_rowcount := 0;
			       l_sec := 'UPDATE apps.wsh_delivery_assignments';
			       UPDATE apps.wsh_delivery_assignments
                     SET delivery_id = NULL, parent_delivery_detail_id = NULL
                   WHERE delivery_detail_id = l_delivery_detail_id;
			       l_updated_rowcount := SQL%ROWCOUNT;
                END IF;
               
                IF rec_its_del_closed.flow_status_code = 'AWAITING_SHIPPING' AND l_updated_rowcount > 0 THEN
                   l_sec := 'UPDATE apps.oe_order_lines_all';
                   UPDATE apps.oe_order_lines_all
                    SET flow_status_code = 'CANCELLED', cancelled_flag = 'Y'
                    WHERE line_id = rec_its_del_closed.line_id
			   	 AND header_id = rec_its_del_closed.header_id
                    AND ordered_quantity = 0
                    AND flow_status_code <> 'CANCELLED';
			   	   l_updated_rowcount := SQL%ROWCOUNT;
			    END IF;
			    
			    IF l_updated_rowcount > 0 THEN
			       l_sec := l_script_name||' - '||'update_record_status - Success';
			       update_record_status( p_order_header_id => rec_its_del_closed.header_id
			                            ,p_order_line_id   => rec_its_del_closed.line_id
			                            ,p_request_id      => l_request_id
			     	 				     ,p_data_fix_script => l_script_name
			                            ,p_status          => 'Success'
			                            ,p_errmsg          => ''
			                           );
			    END IF;
			    COMMIT;
            END IF;
         EXCEPTION
		    WHEN e_user_exception THEN
		       ROLLBACK;
			   update_record_status( p_order_header_id => rec_its_del_closed.header_id
			                        ,p_order_line_id   => rec_its_del_closed.line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
		    WHEN OTHERS THEN
			   ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   update_record_status( p_order_header_id => rec_its_del_closed.header_id
			                        ,p_order_line_id   => rec_its_del_closed.line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_script_name||' - '||l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
		 END;
	  END LOOP;
      fnd_file.put_line (fnd_file.LOG , 'End of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
	  
	  ---------------------------------------------------------------------------------------------------------------------------
	  --1.2	Line has been cancelled, however the delivery has been created and trying to ship for ZERO quantity, line status in 
	  --    Picked/Cancelled status (Interface trip stops completes warning) - ITS_DELIVERY_CANCELED
	  ---------------------------------------------------------------------------------------------------------------------------
	  l_script_name := 'ITS_DELIVERY_CANCELED';  			   
	  l_sec := 'Start loop cr_its_del_closed';
	  fnd_file.put_line (fnd_file.LOG , 'Start of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
	  FOR rec_its_del_canceled IN cr_its_del_canceled(l_execution_date)
	  LOOP
	     BEGIN
            l_sec := l_script_name||' - '||'Check if record is locked';
		    l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec_its_del_canceled.line_rowid, 'OE_ORDER_LINES_ALL');
			IF l_locked_line = 'N' THEN
		       l_sec := 'Insert record for '||l_script_name;
		       insert_record_log( p_order_header_id   => rec_its_del_canceled.header_id
			                     ,p_order_line_id     => rec_its_del_canceled.line_id
			                     ,p_request_id        => l_request_id
			                     ,p_script_name       => l_script_name
			                    );

               l_sec := l_script_name||' - '||'UPDATE apps.oe_order_lines_all';
               UPDATE apps.oe_order_lines_all
               SET flow_status_code = 'CANCELLED', cancelled_flag = 'Y'
               WHERE line_id = rec_its_del_canceled.line_id
               AND ordered_quantity = 0
               AND flow_status_code <> 'CANCELLED';
			   l_updated_rowcount := SQL%ROWCOUNT;
			   
			   IF l_updated_rowcount > 0 THEN
			      l_sec := l_script_name||' - '||'update_record_status - Success';
			      update_record_status( p_order_header_id => rec_its_del_canceled.header_id
			                           ,p_order_line_id   => rec_its_del_canceled.line_id
			                           ,p_request_id      => l_request_id
			    	 				     ,p_data_fix_script => l_script_name
			                           ,p_status          => 'Success'
			                           ,p_errmsg          => ''
			                          );
			   END IF;
			   COMMIT;
            END IF;
         EXCEPTION
		    WHEN OTHERS THEN
			   ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   update_record_status( p_order_header_id => rec_its_del_canceled.header_id
			                        ,p_order_line_id   => rec_its_del_canceled.line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_script_name||' - '||l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
		 END;
	  END LOOP;
      fnd_file.put_line (fnd_file.LOG , 'End of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

	  ------------------------------------------------------------------------------------------------------------------------------
	  --2.	Line stuck in Picked status and the associated delivery detaild id has the OE_INTERFACED_FLAG set to N 
	  --    (Interface trip stops completes warning) (ITS_INTERFACED_FLAG)
	  ------------------------------------------------------------------------------------------------------------------------------
	  l_script_name := 'ITS_INTERFACED_FLAG';
	  l_sec := 'Start loop cr_interfaced_flag';
	  fnd_file.put_line (fnd_file.LOG , 'Start of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
	  FOR rec_interfaced_flag IN cr_interfaced_flag(l_execution_date)
	  LOOP
	     BEGIN
            l_sec := l_script_name||' - '||'Check if record is locked';
		    l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec_interfaced_flag.line_rowid, 'OE_ORDER_LINES_ALL');
			IF l_locked_line = 'N' THEN
		       l_sec := 'Insert record for '||l_script_name;
		       insert_record_log( p_order_header_id   => rec_interfaced_flag.header_id
			                     ,p_order_line_id     => rec_interfaced_flag.line_id
			                     ,p_request_id        => l_request_id
			                     ,p_script_name       => l_script_name
			                    );
			   l_updated_rowcount := 0;
			   l_sec := l_script_name||' - '||'UPDATE apps.wsh_delivery_details';				 
               UPDATE apps.wsh_delivery_Details
               SET oe_interfaced_flag = 'Y'
               WHERE source_line_id     = rec_interfaced_flag.line_id
               AND oe_interfaced_flag = 'N'
               AND released_status    = 'C';
               l_updated_rowcount := SQL%ROWCOUNT;
			   IF l_updated_rowcount > 0 THEN
			      l_sec := l_script_name||' - '||'update_record_status - Success';
			      update_record_status( p_order_header_id => rec_interfaced_flag.header_id
			                           ,p_order_line_id   => rec_interfaced_flag.line_id
			                           ,p_request_id      => l_request_id
			      				        ,p_data_fix_script => l_script_name
			                           ,p_status          => 'Success'
			                           ,p_errmsg          => ''
			                          );
			   END IF;
			   COMMIT;
            END IF;
         EXCEPTION
		    WHEN OTHERS THEN
		       ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   update_record_status( p_order_header_id => rec_interfaced_flag.header_id
			                        ,p_order_line_id   => rec_interfaced_flag.line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_script_name||' - '||l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
		 END;
      END LOOP;
	  fnd_file.put_line (fnd_file.LOG , 'End of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

	  ---------------------------------------------------------------------------------------------------------------------------
       --3. Line has been received, however the associated sales order line has been stuck in BOOKED status - DROP_SHIP_LINE_FIX
      ---------------------------------------------------------------------------------------------------------------------------
	  l_script_name := 'DROP_SHIP_LINE_FIX';
	  l_sec := 'Start loop cr_drop_ship_line_fix';
	  fnd_file.put_line (fnd_file.LOG , 'Start of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
	  FOR rec_drop_ship_line_fix IN cr_drop_ship_line_fix(l_execution_date)
	  LOOP
	     BEGIN
            l_sec := l_script_name||' - '||'Check if record is locked';
		    l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec_drop_ship_line_fix.line_rowid, 'OE_ORDER_LINES_ALL');
			IF l_locked_line = 'N' THEN
		       l_sec := 'Insert record for '||l_script_name;
		       insert_record_log( p_order_header_id   => rec_drop_ship_line_fix.header_id
			                     ,p_order_line_id     => rec_drop_ship_line_fix.line_id
			                     ,p_request_id        => l_request_id
			                     ,p_script_name       => l_script_name
			                    );
               
			   l_updated_rowcount := 0;
			   l_sec := l_script_name||' - '||'UPDATE oe_order_lines_all';
               UPDATE oe_order_lines_all oeol
                  SET oeol.flow_status_code = 'AWAITING_RECEIPT'
                WHERE     oeol.line_id = rec_drop_ship_line_fix.line_id
                  AND oeol.flow_status_code <> 'AWAITING_RECEIPT';
			   
			   l_updated_rowcount := SQL%ROWCOUNT; 
			   
			   IF l_updated_rowcount > 0 THEN
                  l_sec := l_script_name||' - '||'Fetch cursor pending_receipts';
			      FOR all_lines IN pending_receipts(rec_drop_ship_line_fix.line_id)
                  LOOP
                     l_return_status := OE_DS_PVT.DROPSHIPRECEIVE (all_lines.transaction_id, 'INV');
                  END LOOP;
			   END IF;
			   IF  l_return_status = TRUE THEN
			       l_sec := l_script_name||' - '||'update_record_status - Success';
			       update_record_status( p_order_header_id => rec_drop_ship_line_fix.header_id
			                            ,p_order_line_id   => rec_drop_ship_line_fix.line_id
			                            ,p_request_id      => l_request_id
			     	 				     ,p_data_fix_script => l_script_name
			                            ,p_status          => 'Success'
			                            ,p_errmsg          => ''
			                           );
               ELSE
			      l_sec := l_script_name||' - '||'update_record_status - Error';
			       update_record_status( p_order_header_id => rec_drop_ship_line_fix.header_id
			                            ,p_order_line_id   => rec_drop_ship_line_fix.line_id
			                            ,p_request_id      => l_request_id
			     	 				     ,p_data_fix_script => l_script_name
			                            ,p_status          => 'Error'
			                            ,p_errmsg          => 'OE_DS_PVT.DROPSHIPRECEIVE has not executed successfully'
			                           );
			   END IF;
               COMMIT;
            END IF;
         EXCEPTION
		    WHEN OTHERS THEN
		       ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   update_record_status( p_order_header_id => rec_drop_ship_line_fix.header_id
			                        ,p_order_line_id   => rec_drop_ship_line_fix.line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_script_name||' - '||l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
		 END;      
      END LOOP;
	  fnd_file.put_line (fnd_file.LOG , 'End of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
	  
	  ------------------------------------------------------------------------------------------------------------------------
	  --4.	Line is shipped, invoiced and the workflow is closed, however the line status is not closed - AWAITING_INVOICE_FIX
	  ------------------------------------------------------------------------------------------------------------------------
	  l_script_name := 'AWAITING_INVOICE_FIX';
	  l_sec := 'Start loop cr_awaiting_invoice_fix';
	  fnd_file.put_line (fnd_file.LOG , 'Start of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
	  FOR rec_awaiting_invoice_fix IN cr_awaiting_invoice_fix(l_execution_date)
	  LOOP
	     BEGIN
            l_sec := l_script_name||' - '||'Check if record is locked';
		    l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec_awaiting_invoice_fix.line_rowid, 'OE_ORDER_LINES_ALL');
			IF l_locked_line = 'N' THEN
		       l_sec := 'Insert record for '||l_script_name;
		       insert_record_log( p_order_header_id   => rec_awaiting_invoice_fix.header_id
			                     ,p_order_line_id     => rec_awaiting_invoice_fix.line_id
			                     ,p_request_id        => l_request_id
			                     ,p_script_name       => l_script_name
			                    );
               
			   l_updated_rowcount := 0;
			   l_sec := l_script_name||' - '||'UPDATE oe_order_lines_all';
               UPDATE apps.oe_order_lines_all ool
                  SET ool.invoice_interface_status_code = 'YES',
                      ool.open_flag = 'N',
                      ool.flow_status_code = 'CLOSED',
                      ool.invoiced_quantity = ool.shipped_quantity
                WHERE     line_id = rec_awaiting_invoice_fix.line_id
                AND ool.flow_status_code = 'INVOICE_HOLD';
			   
			   l_updated_rowcount := SQL%ROWCOUNT;
			   
		       IF l_updated_rowcount > 0 THEN
			       l_sec := l_script_name||' - '||'update_record_status - Success';
			       update_record_status( p_order_header_id => rec_awaiting_invoice_fix.header_id
			                            ,p_order_line_id   => rec_awaiting_invoice_fix.line_id
			                            ,p_request_id      => l_request_id
			     	 				     ,p_data_fix_script => l_script_name
			                            ,p_status          => 'Success'
			                            ,p_errmsg          => ''
			                           );
			   END IF;
			   COMMIT;
            END IF;
         EXCEPTION
		    WHEN OTHERS THEN
		       ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   update_record_status( p_order_header_id => rec_awaiting_invoice_fix.header_id
			                        ,p_order_line_id   => rec_awaiting_invoice_fix.line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_script_name||' - '||l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
		 END;      
      END LOOP;
	  fnd_file.put_line (fnd_file.LOG , 'End of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
	  fnd_file.put_line (fnd_file.LOG , 'End of Procedure : execute_data_fixes'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (fnd_file.LOG ,'==========================================================================================');
   EXCEPTION
       WHEN OTHERS THEN
       p_errbuf := 'Error';
       p_retcode := '2';
	   l_err_code := SQLCODE;
       l_err_msg  := SUBSTR(SQLERRM,1,2000);
       fnd_file.put_line (fnd_file.LOG, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
       fnd_file.put_line (fnd_file.output, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
	   ROLLBACK;
       xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_ORDERS_DATAFIX_PKG.EXECUTE_DATA_FIXES'
           ,p_calling             => l_sec
           ,p_request_id          => l_request_id
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error while executing data fix'
           ,p_distribution_list   => g_dflt_email
           ,p_module              => 'XXWC');
   END execute_data_fixes;
END xxwc_om_orders_datafix_pkg;
/