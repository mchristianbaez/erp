 /**********************************************************************************
  -- Synonym Script: XXWC_OM_ORDERS_DATAFIX_LOG_SYN.sql
  -- *******************************************************************************
  --
  -- PURPOSE: Synonym for table XXWC.XXWC_OM_ORDERS_DATAFIX_LOG_TBL
  -- HISTORY
  -- ===============================================================================
  -- ===============================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -----------------------------------------
  -- 1.0     22-Sep-2016   Niraj K Ranjan   Initial Version - TMS#20160911-00002   Data fix scripts into Concurrent program
************************************************************************************/
CREATE OR REPLACE SYNONYM apps.xxwc_om_orders_datafix_log_tbl FOR xxwc.xxwc_om_orders_datafix_log_tbl;