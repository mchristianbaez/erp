/*******************************************************************************************************
  -- Table Name XXWC_OM_ORDERS_DATAFIX_LOG_TBL
  -- ***************************************************************************************************
  --
  -- PURPOSE: To log script execution information
  -- HISTORY
  -- ===================================================================================================
  -- ===================================================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------------
  -- 1.0     22-Sep-2016   Niraj K Ranjan   Initial Version - TMS#20160911-00002  Data fix scripts into Concurrent program
********************************************************************************************************/
CREATE TABLE xxwc.xxwc_om_orders_datafix_log_tbl
             ( ORDER_HEADER_ID       NUMBER
              ,ORDER_LINE_ID         NUMBER
              ,REQUEST_ID            NUMBER
              ,DATA_FIX_SCRIPT        VARCHAR2(1000)
			  ,STATUS                VARCHAR2(30)
			  ,ERROR_MESSAGE         VARCHAR2(2000)
			  ,CREATION_DATE         DATE
			  ,CREATED_BY            NUMBER
			  ,LAST_UPDATE_DATE      DATE
			  ,LAST_UPDATED_BY       NUMBER
			 );

