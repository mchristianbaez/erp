CREATE OR REPLACE PACKAGE xxwc_om_orders_datafix_pkg AUTHID CURRENT_USER
AS
   /********************************************************************************************************************************
      $Header XXWC_OM_ORDERS_DATAFIX_PKG.PKS $
      Module Name: XXWC_OM_ORDERS_DATAFIX_PKG.PKS

      PURPOSE:   This package is used for data fix of order management

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------------------------------------------------------------
      1.0        22-Sep-2016 Niraj K Ranjan          Initial Version - TMS#20160911-00002   Data fix scripts into Concurrent program
   *********************************************************************************************************************************/
   
   PROCEDURE execute_data_fixes ( p_errbuf         OUT VARCHAR2
                                 ,p_retcode        OUT NUMBER
                                 ,p_start_date     IN  VARCHAR2
                                 );
								 
END xxwc_om_orders_datafix_pkg;
/
