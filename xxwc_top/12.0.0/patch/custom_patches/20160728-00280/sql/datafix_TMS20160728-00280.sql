/* ************************************************************************************************
   Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        17-Aug-2016  Neha Saini       TMS#20160728-00280  Order stuck in Pre-bill acceptance
  **************************************************************************************************/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;
BEGIN

UPDATE apps.oe_order_lines_all
            SET 
            shipping_quantity = ordered_quantity,
                shipped_quantity = ordered_quantity,
                actual_shipment_date = TO_DATE('01/06/2016 11:18:48','MM/DD/YYYY HH24:MI:SS'),
                shipping_quantity_uom = order_quantity_uom,
                fulfilled_flag = 'Y',
                fulfillment_date = TO_DATE('01/06/2016 11:18:48','MM/DD/YYYY HH24:MI:SS'),
                fulfilled_quantity = ordered_quantity,
                last_updated_by = -1,
                last_update_date = SYSDATE
--                ACCEPTED_BY = 4716,
--                REVREC_SIGNATURE = 'TODD MYERS',
--                REVREC_SIGNATURE_DATE = '07-FEB-2013'
          WHERE line_id = 71937992
          and headeR_id=43952856;

dbms_output.put_line(' No of rows updated  -> '|| SQL%ROWCOUNT);

COMMIT;


EXCEPTION
  WHEN others THEN
    dbms_output.put_line('Error occured while updating STATUS: '||SQLERRM);
END;
/