CREATE OR REPLACE FORCE VIEW APPS.PO_HEADERS_XML
/* 
-- ************************************************************************
-- $Header po_headers_xml $
-- Module Name: po_headers_xml

-- REVISIONS:
-- Ver        Date        Author           Description
-- ---------  ----------  ----------       ----------------
-- 1.1        08/18/2014  Veera C          Modified for TMS#20140715-00024 Update PO Document 
--                                        for Drop-ship POs with customer name and  phone number
-- 1.2        09/18/2014  Maha             TMS#20140715-00024 Duplicate line issue in PRD
-- 1.3        12/14/2014  Bala Seshadri    ESMS: 258131 : Add GSC related custom fields
                                            a) XXCUS_GSC_UNIQUE_SHIPTO
                                            b) XXCUS_GSC_PROD_AMOUNT
                                            c) XXCUS_GSC_FRT_AMOUNT
                                            d) XXCUS_GSC_TAX_AMOUNT
-- 1.4       01/23/2015  Bala Seshadri     ESMS 275677,  Add GSC custom fields REQ_ATTN_TO and PO_DOC_OWNER   
-- 1.5       06/29/2015  Bala Seshadri     ESMS 289744, Add one time address related changes 
-- 1.6 	     08/18/2015  Manjula C         TMS# 20131120-00027 - Purchase Discounts 
                                     
-- ************************************************************************* 
*/
(
   TYPE_LOOKUP_CODE,
   SEGMENT1,
   REVISION_NUM,
   PRINT_COUNT,
   CREATION_DATE,
   PRINTED_DATE,
   REVISED_DATE,
   START_DATE,
   END_DATE,
   NOTE_TO_VENDOR_HEADER,
   DOCUMENT_BUYER_FIRST_NAME,
   DOCUMENT_BUYER_LAST_NAME,
   DOCUMENT_BUYER_TITLE,
   DOCUMENT_BUYER_AGENT_ID,
   ARCHIVE_BUYER_AGENT_ID,
   ARCHIVE_BUYER_FIRST_NAME,
   ARCHIVE_BUYER_LAST_NAME,
   ARCHIVE_BUYER_TITLE,
   AMOUNT_AGREED,
   CANCEL_FLAG,
   CONFIRMING_ORDER_FLAG,
   ACCEPTANCE_REQUIRED_FLAG,
   ACCEPTANCE_DUE_DATE,
   CURRENCY_CODE,
   CURRENCY_NAME,
   RATE,
   SHIP_VIA,
   FOB,
   FREIGHT_TERMS,
   PAYMENT_TERMS,
   CUSTOMER_NUM,
   VENDOR_NUM,
   VENDOR_NAME,
   VENDOR_ADDRESS_LINE1,
   VENDOR_ADDRESS_LINE2,
   VENDOR_ADDRESS_LINE3,
   VENDOR_CITY,
   VENDOR_STATE,
   VENDOR_POSTAL_CODE,
   VENDOR_COUNTRY,
   VENDOR_PHONE,
   VENDOR_CONTACT_FIRST_NAME,
   VENDOR_CONTACT_LAST_NAME,
   VENDOR_CONTACT_TITLE,
   VENDOR_CONTACT_PHONE,
   SHIP_TO_LOCATION_ID,
   SHIP_TO_LOCATION_NAME,
   SHIP_TO_ADDRESS_LINE1,
   SHIP_TO_ADDRESS_LINE2,
   SHIP_TO_ADDRESS_LINE3,
   SHIP_TO_ADDRESS_LINE4,
   SHIP_TO_ADDRESS_INFO,
   SHIP_TO_COUNTRY,
   BILL_TO_LOCATION_ID,
   BILL_TO_LOCATION_NAME,
   BILL_TO_ADDRESS_LINE1,
   BILL_TO_ADDRESS_LINE2,
   BILL_TO_ADDRESS_LINE3,
   BILL_TO_ADDRESS_LINE4,
   BILL_TO_ADDRESS_INFO,
   BILL_TO_COUNTRY,
   ATTRIBUTE1,
   ATTRIBUTE2,
   ATTRIBUTE3,
   ATTRIBUTE4,
   ATTRIBUTE5,
   ATTRIBUTE6,
   ATTRIBUTE7,
   ATTRIBUTE8,
   ATTRIBUTE9,
   ATTRIBUTE10,
   ATTRIBUTE11,
   ATTRIBUTE12,
   ATTRIBUTE13,
   ATTRIBUTE14,
   ATTRIBUTE15,
   VENDOR_SITE_ID,
   PO_HEADER_ID,
   APPROVED_FLAG,
   LANGUAGE,
   VENDOR_ID,
   CLOSED_CODE,
   USSGL_TRANSACTION_CODE,
   GOVERNMENT_CONTEXT,
   REQUEST_ID,
   PROGRAM_APPLICATION_ID,
   PROGRAM_ID,
   PROGRAM_UPDATE_DATE,
   ORG_ID,
   COMMENTS,
   REPLY_DATE,
   REPLY_METHOD_LOOKUP_CODE,
   RFQ_CLOSE_DATE,
   QUOTE_TYPE_LOOKUP_CODE,
   QUOTATION_CLASS_CODE,
   QUOTE_WARNING_DELAY_UNIT,
   QUOTE_WARNING_DELAY,
   QUOTE_VENDOR_QUOTE_NUMBER,
   CLOSED_DATE,
   USER_HOLD_FLAG,
   APPROVAL_REQUIRED_FLAG,
   FIRM_STATUS_LOOKUP_CODE,
   FIRM_DATE,
   FROZEN_FLAG,
   EDI_PROCESSED_FLAG,
   EDI_PROCESSED_STATUS,
   ATTRIBUTE_CATEGORY,
   CREATED_BY,
   VENDOR_CONTACT_ID,
   TERMS_ID,
   FOB_LOOKUP_CODE,
   FREIGHT_TERMS_LOOKUP_CODE,
   STATUS_LOOKUP_CODE,
   RATE_TYPE,
   RATE_DATE,
   FROM_HEADER_ID,
   FROM_TYPE_LOOKUP_CODE,
   AUTHORIZATION_STATUS,
   APPROVED_DATE,
   AMOUNT_LIMIT,
   MIN_RELEASE_AMOUNT,
   NOTE_TO_AUTHORIZER,
   NOTE_TO_RECEIVER,
   VENDOR_ORDER_NUM,
   LAST_UPDATE_DATE,
   LAST_UPDATED_BY,
   SUMMARY_FLAG,
   ENABLED_FLAG,
   SEGMENT2,
   SEGMENT3,
   SEGMENT4,
   SEGMENT5,
   START_DATE_ACTIVE,
   END_DATE_ACTIVE,
   LAST_UPDATE_LOGIN,
   SUPPLY_AGREEMENT_FLAG,
   GLOBAL_ATTRIBUTE_CATEGORY,
   GLOBAL_ATTRIBUTE1,
   GLOBAL_ATTRIBUTE2,
   GLOBAL_ATTRIBUTE3,
   GLOBAL_ATTRIBUTE4,
   GLOBAL_ATTRIBUTE5,
   GLOBAL_ATTRIBUTE6,
   GLOBAL_ATTRIBUTE7,
   GLOBAL_ATTRIBUTE8,
   GLOBAL_ATTRIBUTE9,
   GLOBAL_ATTRIBUTE10,
   GLOBAL_ATTRIBUTE11,
   GLOBAL_ATTRIBUTE12,
   GLOBAL_ATTRIBUTE13,
   GLOBAL_ATTRIBUTE14,
   GLOBAL_ATTRIBUTE15,
   GLOBAL_ATTRIBUTE16,
   GLOBAL_ATTRIBUTE17,
   GLOBAL_ATTRIBUTE18,
   GLOBAL_ATTRIBUTE19,
   GLOBAL_ATTRIBUTE20,
   INTERFACE_SOURCE_CODE,
   REFERENCE_NUM,
   WF_ITEM_TYPE,
   WF_ITEM_KEY,
   PCARD_ID,
   PRICE_UPDATE_TOLERANCE,
   MRC_RATE_TYPE,
   MRC_RATE_DATE,
   MRC_RATE,
   PAY_ON_CODE,
   XML_FLAG,
   XML_SEND_DATE,
   XML_CHANGE_SEND_DATE,
   GLOBAL_AGREEMENT_FLAG,
   CONSIGNED_CONSUMPTION_FLAG,
   CBC_ACCOUNTING_DATE,
   CONSUME_REQ_DEMAND_FLAG,
   CHANGE_REQUESTED_BY,
   SHIPPING_CONTROL,
   CONTERMS_EXIST_FLAG,
   CONTERMS_ARTICLES_UPD_DATE,
   CONTERMS_DELIV_UPD_DATE,
   PENDING_SIGNATURE_FLAG,
   OU_NAME,
   OU_ADDR1,
   OU_ADDR2,
   OU_ADDR3,
   OU_TOWN_CITY,
   OU_REGION2,
   OU_POSTALCODE,
   OU_COUNTRY,
   BUYER_LOCATION_ID,
   BUYER_ADDRESS_LINE1,
   BUYER_ADDRESS_LINE2,
   BUYER_ADDRESS_LINE3,
   BUYER_ADDRESS_LINE4,
   BUYER_CITY_STATE_ZIP,
   BUYER_CONTACT_PHONE,
   BUYER_CONTACT_EMAIL,
   BUYER_CONTACT_FAX,
   VENDOR_FAX,
   TOTAL_AMOUNT,
   BUYER_COUNTRY,
   VENDOR_ADDRESS_LINE4,
   VENDOR_AREA_CODE,
   VENDOR_CONTACT_AREA_CODE,
   LE_NAME,
   LE_ADDR1,
   LE_ADDR2,
   LE_ADDR3,
   LE_TOWN_CITY,
   LE_STAE_PROVINCE,
   LE_POSTALCODE,
   LE_COUNTRY,
   CANCEL_DATE,
   CHANGE_SUMMARY,
   DOCUMENT_CREATION_METHOD,
   ENCUMBRANCE_REQUIRED_FLAG,
   STYLE_DISPLAY_NAME,
   VENDOR_FAX_AREA_CODE,
   CF_DATE,
   CF_CREATION_DATE,
   CF_ATTRIBUTE1,
   CF_REVISED_DATE,
   CP_SHIP_LOC_PHONE,
   CP_SHIP_LOC_FAX,
   CP_LOC_CITY,
   CP_LOC_STATE,
   CP_LOC_COUNTRY,
   CP_LOC_ZIP,
   CP_BILL_LOC_PHONE,
   CP_BILL_LOC_FAX,
   CP_BILL_LOC_CITY,
   CP_BILL_LOC_STATE,
   CP_BILL_LOC_COUNTRY,
   CP_BILL_LOC_ZIP,
   CP_FREIGHT_MIN,
   CP_WEIGHT_MIN,
   CP_WEIGHT_UOM,
   CP_FIRST_DS_PLL_ID,
   CP_FIRST_DS_SHIP_LOC_ID,
   CP_DS_ADDR_LINE1,
   CP_DS_ADDR_LINE2,
   CP_DS_ADDR_LINE3,
   CP_DS_ADDR_LINE4,
   CP_DS_TERRITORY,
   CP_DS_LOCATION_NAME,
   CP_DS_ADDR_INFO,
   CP_DS_TOWN_CITY,
   CP_DS_POSTAL_CODE,
   CP_DS_STATE_PROVINCE,
   CP_DS_CUSTOMER_NAME,
   CP_VENDOR_CONTACT_ID,
   CP_VC_ADDRESS_LINE1,
   CP_VC_ADDRESS_LINE2,
   CP_VC_ADDRESS_LINE3,
   CP_VC_ADDRESS_LINE4,
   CP_VC_TOWN_CITY,
   CP_VC_STATE_PROVICE,
   CP_VC_POSTAL_CODE,
   CP_VC_COUNTRY,
   CP_VC_PHONE,
   CP_VC_ADDRESS_INFO,
   CUSTOMER_PHONE_NUMBER,
   CUSTOMER_CONTACT,
   ORDER_REQUEST_DATE,
   DROP_SHIP_FLAG,
   CARRIER_DESCIPTION,
   XXCUS_GSC_UNIQUE_SHIPTO,
   XXCUS_GSC_PROD_AMOUNT,
   XXCUS_GSC_FRT_AMOUNT,
   XXCUS_GSC_TAX_AMOUNT,
   REQ_ATTN_TO,
   PO_DOC_OWNER,
-- Added DISCOUNT_AMOUNT , DISCOUNT_LABEL and NET_AMOUNT for Ver 1.6   
   DISCOUNT_AMOUNT, 
   DISCOUNT_LABEL, 
   NET_AMOUNT 
)
AS
   SELECT ph.type_lookup_code,
          ph.segment1,
          ph.revision_num,
          ph.print_count,
          TO_CHAR (ph.creation_date, 'DD-MON-YYYY HH24:MI:SS') creation_date,
          ph.printed_date,
          TO_CHAR (ph.revised_date, 'DD-MON-YYYY HH24:MI:SS') revised_date,
          TO_CHAR (ph.start_date, 'DD-MON-YYYY HH24:MI:SS') start_date,
          TO_CHAR (ph.end_date, 'DD-MON-YYYY HH24:MI:SS') end_date,
          ph.note_to_vendor,
          hre.first_name document_buyer_first_name,
          hre.last_name document_buyer_last_name,
          NULL,
          --HRL.MEANING DOCUMENT_BUYER_TITLE, --Removed 6/26/2013 to help performance
          ph.agent_id document_buyer_agent_id,
          DECODE (NVL (ph.revision_num, 0),
                  0, NULL,
                  po_communication_pvt.getarcbuyeragentid (ph.po_header_id))
             archive_buyer_agent_id,
          DECODE (NVL (ph.revision_num, 0),
                  0, NULL,
                  po_communication_pvt.getarcbuyerfname ())
             archive_buyer_first_name,
          DECODE (NVL (ph.revision_num, 0),
                  0, NULL,
                  po_communication_pvt.getarcbuyerlname ())
             archive_buyer_last_name,
          DECODE (NVL (ph.revision_num, 0),
                  0, NULL,
                  po_communication_pvt.getarcbuyertitle ())
             archive_buyer_title,
          TO_CHAR (NVL (ph.blanket_total_amount, ''),
                   po_communication_pvt.getformatmask)
             amount_agreed,
          NVL (ph.cancel_flag, 'N'),
          ph.confirming_order_flag,
          NVL (ph.acceptance_required_flag, 'N'),
          TO_CHAR (ph.acceptance_due_date, 'DD-MON-YYYY HH24:MI:SS')
             acceptance_due_date,
          fcc.currency_code,
          fcc.NAME currency_name,
          TO_CHAR (ph.rate, po_communication_pvt.getformatmask) rate,
          NVL (ofc.freight_code_tl, ph.ship_via_lookup_code) ship_via,
          plc1.meaning fob,
          plc2.meaning freight_terms,
          t.NAME payment_terms,
          NVL (pvs.customer_num, vn.customer_num) customer_num,
          vn.segment1 vendor_num,
          vn.vendor_name,
          hl.address1 vendor_address_line1,
          hl.address2 vendor_address_line2,
          hl.address3 vendor_address_line3,
          hl.city vendor_city,
          DECODE (hl.state,
                  NULL, DECODE (hl.province, NULL, hl.county, hl.province),
                  hl.state)
             vendor_state,
          SUBSTR (hl.postal_code, 1, 20) vendor_postal_code,
          fte3.territory_short_name vendor_country,
          pvs.phone vendor_phone,
          pvc.first_name vendor_contact_first_name,
          pvc.last_name vendor_contact_last_name,
          plc4.meaning vendor_contact_title,
          pvc.phone vendor_contact_phone,
          DECODE (
             NVL (ph.ship_to_location_id, -1),
             -1, NULL,
             po_communication_pvt.getlocationinfo (ph.ship_to_location_id))
             ship_to_location_id,                      
         -- ESMS 289744 for ship_to_location_name         
          case      
              WHEN (
                       mo_global.get_current_org_id IN (163, 167) 
                    AND
                     (
                      (
                       SELECT COUNT (UNIQUE a.ship_to_location_id)
                       FROM   po_line_locations a
                       WHERE  1 = 1 
                         AND  a.po_header_id =ph.po_header_id
                         AND  exists
                          (
                             select '1'
                             from    hr_locations
                             where   1 =1
                               and   location_id =a.ship_to_location_id
                               and   location_code <>'ONE TIME ADDRESS'
                          )
                       ) =1
                      )
                     ) THEN
                          (
                            SELECT LOCATION_CODE
                            FROM   HR_LOCATIONS
                            WHERE  1 =1
                              AND  LOCATION_ID =
                                                 (
                                                     SELECT ship_to_location_id
                                                     FROM   PO_LINE_LOCATIONS
                                                     WHERE  1 =1
                                                       AND  PO_HEADER_ID =ph.po_header_id
                                                       AND  rownum <2 
                                                 )
                          )
              WHEN (
                       mo_global.get_current_org_id IN (163, 167) 
                    AND
                     (
                      (
                       SELECT COUNT (UNIQUE a.ship_to_location_id)
                       FROM   po_line_locations a
                       WHERE  1 = 1 
                         AND  a.po_header_id =ph.po_header_id
                         AND  exists
                          (
                             select '1'
                             from    hr_locations
                             where   1 =1
                               and   location_id =a.ship_to_location_id
                               and   location_code ='ONE TIME ADDRESS'
                          )
                       ) =1
                      )
                     ) THEN
                          (
                            SELECT LOCATION_CODE
                            FROM   HR_LOCATIONS
                            WHERE  1 =1
                              AND  LOCATION_ID =
                                                 (
                                                     SELECT ship_to_location_id
                                                     FROM   PO_LINE_LOCATIONS
                                                     WHERE  1 =1
                                                       AND  PO_HEADER_ID =ph.po_header_id
                                                       AND  rownum <2 
                                                 )
                          )                          
              ELSE
               DECODE (NVL (ph.ship_to_location_id, -1), -1, NULL, po_communication_pvt.getlocationname ())              
          end ship_to_location_name,
         --,DECODE (NVL (ph.ship_to_location_id, -1), -1, NULL, po_communication_pvt.getlocationname ()) ship_to_location_name
         -- ESMS 289744 for ship_to_location_name             
         -- ESMS 289744 for ship_to_address_line1 
          case      
              WHEN (
                       mo_global.get_current_org_id IN (163, 167) 
                    AND
                     (
                      (
                       SELECT COUNT (UNIQUE a.ship_to_location_id)
                       FROM   po_line_locations a
                       WHERE  1 = 1 
                         AND  a.po_header_id =ph.po_header_id
                         AND  exists
                          (
                             select '1'
                             from    hr_locations
                             where   1 =1
                               and   location_id =a.ship_to_location_id
                               and   location_code <>'ONE TIME ADDRESS'
                          )
                       ) =1
                      )
                     ) THEN
                          (
                            SELECT ADDRESS_LINE_1
                            FROM   HR_LOCATIONS
                            WHERE  1 =1
                              AND  LOCATION_ID =
                                                 (
                                                     SELECT ship_to_location_id
                                                     FROM   PO_LINE_LOCATIONS
                                                     WHERE  1 =1
                                                       AND  PO_HEADER_ID =ph.po_header_id
                                                       AND  rownum <2 
                                                 )
                          )
              WHEN (
                       mo_global.get_current_org_id IN (163, 167) 
                    AND
                     (
                      (
                       SELECT COUNT (UNIQUE a.ship_to_location_id)
                       FROM   po_line_locations a
                       WHERE  1 = 1 
                         AND  a.po_header_id =ph.po_header_id
                         AND  exists
                          (
                             select '1'
                             from    hr_locations
                             where   1 =1
                               and   location_id =a.ship_to_location_id
                               and   location_code ='ONE TIME ADDRESS'
                          )
                       ) =1
                      )
                     ) THEN
                          (
                            SELECT Substr(fnd_profile.value('XXCUS_PO_ONE_TIME_ADDRESS_TEXT'), 1, 40)
                            FROM   HR_LOCATIONS
                            WHERE  1 =1
                              AND  LOCATION_ID =
                                                 (
                                                     SELECT UNIQUE ship_to_location_id
                                                     FROM   PO_LINE_LOCATIONS
                                                     WHERE  1 =1
                                                       AND  PO_HEADER_ID =ph.po_header_id
                                                       AND  rownum <2 
                                                 )
                          )                          
              ELSE
               DECODE (NVL (ph.ship_to_location_id, -1), -1, NULL, po_communication_pvt.getaddressline1 ())              
          end ship_to_address_line1,         
         -- ESMS 289744 for ship_to_address_line1             
         -- ESMS 289744 for ship_to_address_line2
          case      
              WHEN (
                       mo_global.get_current_org_id IN (163, 167) 
                    AND
                     (
                      (
                       SELECT COUNT (UNIQUE a.ship_to_location_id)
                       FROM   po_line_locations a
                       WHERE  1 = 1 
                         AND  a.po_header_id =ph.po_header_id
                         AND  exists
                          (
                             select '1'
                             from    hr_locations
                             where   1 =1
                               and   location_id =a.ship_to_location_id
                               and   location_code <>'ONE TIME ADDRESS'
                          )
                       ) =1
                      )
                     ) THEN
                          (
                            SELECT ADDRESS_LINE_2
                            FROM   HR_LOCATIONS
                            WHERE  1 =1
                              AND  LOCATION_ID =
                                                 (
                                                     SELECT ship_to_location_id
                                                     FROM   PO_LINE_LOCATIONS
                                                     WHERE  1 =1
                                                       AND  PO_HEADER_ID =ph.po_header_id
                                                       AND  rownum <2 
                                                 )
                          )
              WHEN (
                       mo_global.get_current_org_id IN (163, 167) 
                    AND
                     (
                      (
                       SELECT COUNT (UNIQUE a.ship_to_location_id)
                       FROM   po_line_locations a
                       WHERE  1 = 1 
                         AND  a.po_header_id =ph.po_header_id
                         AND  exists
                          (
                             select '1'
                             from    hr_locations
                             where   1 =1
                               and   location_id =a.ship_to_location_id
                               and   location_code ='ONE TIME ADDRESS'
                          )
                       ) =1
                      )
                     ) THEN
                          (
                            SELECT Null
                            FROM   HR_LOCATIONS
                            WHERE  1 =1
                              AND  LOCATION_ID =
                                                 (
                                                     SELECT ship_to_location_id
                                                     FROM   PO_LINE_LOCATIONS
                                                     WHERE  1 =1
                                                       AND  PO_HEADER_ID =ph.po_header_id
                                                       AND  rownum <2 
                                                 )
                          )                          
              ELSE
               DECODE (NVL (ph.ship_to_location_id, -1), -1, NULL, po_communication_pvt.getaddressline2 ())              
          end ship_to_address_line2,         
         -- ESMS 289744 for ship_to_address_line2            
         -- ESMS 289744 for ship_to_address_line3
          case      
              WHEN (
                       mo_global.get_current_org_id IN (163, 167) 
                    AND
                     (
                      (
                       SELECT COUNT (UNIQUE a.ship_to_location_id)
                       FROM   po_line_locations a
                       WHERE  1 = 1 
                         AND  a.po_header_id =ph.po_header_id
                         AND  exists
                          (
                             select '1'
                             from    hr_locations
                             where   1 =1
                               and   location_id =a.ship_to_location_id
                               and   location_code <>'ONE TIME ADDRESS'
                          )
                       ) =1
                      )
                     ) THEN
                          (
                            SELECT ADDRESS_LINE_3
                            FROM   HR_LOCATIONS
                            WHERE  1 =1
                              AND  LOCATION_ID =
                                                 (
                                                     SELECT ship_to_location_id
                                                     FROM   PO_LINE_LOCATIONS
                                                     WHERE  1 =1
                                                       AND  PO_HEADER_ID =ph.po_header_id
                                                       AND  rownum <2 
                                                 )
                          )
              WHEN (
                       mo_global.get_current_org_id IN (163, 167) 
                    AND
                     (
                      (
                       SELECT COUNT (UNIQUE a.ship_to_location_id)
                       FROM   po_line_locations a
                       WHERE  1 = 1 
                         AND  a.po_header_id =ph.po_header_id
                         AND  exists
                          (
                             select '1'
                             from    hr_locations
                             where   1 =1
                               and   location_id =a.ship_to_location_id
                               and   location_code ='ONE TIME ADDRESS'
                          )
                       ) =1
                      )
                     ) THEN
                          (
                            SELECT Null
                            FROM   HR_LOCATIONS
                            WHERE  1 =1
                              AND  LOCATION_ID =
                                                 (
                                                     SELECT ship_to_location_id
                                                     FROM   PO_LINE_LOCATIONS
                                                     WHERE  1 =1
                                                       AND  PO_HEADER_ID =ph.po_header_id
                                                       AND  rownum <2 
                                                 )
                          )                          
              ELSE
               DECODE (NVL (ph.ship_to_location_id, -1), -1, NULL, po_communication_pvt.getaddressline3 ())              
          end ship_to_address_line3,         
         -- ESMS 289744 for ship_to_address_line3            
         -- ESMS 289744 for ship_to_address_line4
          case      
              WHEN (
                       mo_global.get_current_org_id IN (163, 167) 
                    AND
                     (
                      (
                       SELECT COUNT (UNIQUE a.ship_to_location_id)
                       FROM   po_line_locations a
                       WHERE  1 = 1 
                         AND  a.po_header_id =ph.po_header_id
                         AND  exists
                          (
                             select '1'
                             from    hr_locations
                             where   1 =1
                               and   location_id =a.ship_to_location_id
                               and   location_code <>'ONE TIME ADDRESS'
                          )
                       ) =1
                      )
                     ) THEN Null
              WHEN (
                       mo_global.get_current_org_id IN (163, 167) 
                    AND
                     (
                      (
                       SELECT COUNT (UNIQUE a.ship_to_location_id)
                       FROM   po_line_locations a
                       WHERE  1 = 1 
                         AND  a.po_header_id =ph.po_header_id
                         AND  exists
                          (
                             select '1'
                             from    hr_locations
                             where   1 =1
                               and   location_id =a.ship_to_location_id
                               and   location_code ='ONE TIME ADDRESS'
                          )
                       ) =1
                      )
                     ) THEN Null                                                 
              ELSE
               DECODE (NVL (ph.ship_to_location_id, -1), -1, NULL, po_communication_pvt.getaddressline4 ())              
          end ship_to_address_line4,         
         -- ESMS 289744 for ship_to_address_line4 
         -- ESMS 289744 for ship_to_address_info
          case      
              WHEN (
                       mo_global.get_current_org_id IN (163, 167) 
                    AND
                     (
                      (
                       SELECT COUNT (UNIQUE a.ship_to_location_id)
                       FROM   po_line_locations a
                       WHERE  1 = 1 
                         AND  a.po_header_id =ph.po_header_id
                         AND  exists
                          (
                             select '1'
                             from    hr_locations
                             where   1 =1
                               and   location_id =a.ship_to_location_id
                               and   location_code <>'ONE TIME ADDRESS'
                          )
                       ) =1
                      )
                     ) THEN 
                          (
                            SELECT TOWN_OR_CITY||' '||REGION_2||'  '||POSTAL_CODE
                            FROM   HR_LOCATIONS
                            WHERE  1 =1
                              AND  LOCATION_ID =
                                                 (
                                                     SELECT UNIQUE ship_to_location_id
                                                     FROM   PO_LINE_LOCATIONS
                                                     WHERE  1 =1
                                                       AND  PO_HEADER_ID =ph.po_header_id
                                                       AND  rownum <2 
                                                 )
                          )                     
              WHEN (
                       mo_global.get_current_org_id IN (163, 167) 
                    AND
                     (
                      (
                       SELECT COUNT (UNIQUE a.ship_to_location_id)
                       FROM   po_line_locations a
                       WHERE  1 = 1 
                         AND  a.po_header_id =ph.po_header_id
                         AND  exists
                          (
                             select '1'
                             from    hr_locations
                             where   1 =1
                               and   location_id =a.ship_to_location_id
                               and   location_code ='ONE TIME ADDRESS'
                          )
                       ) =1
                      )
                     ) THEN Null                                                
              ELSE
               DECODE (NVL (ph.ship_to_location_id, -1), -1, NULL, po_communication_pvt.getaddressinfo ())              
          end ship_to_address_info,         
         -- ESMS 289744 for ship_to_address_info                         
          DECODE (NVL (ph.ship_to_location_id, -1),
                  -1, NULL,
                  po_communication_pvt.getterritoryshortname ())
             ship_to_country,
          DECODE (
             NVL (ph.bill_to_location_id, -1),
             -1, NULL,
             po_communication_pvt.getlocationinfo (ph.bill_to_location_id))
             bill_to_location_id,
          DECODE (NVL (ph.ship_to_location_id, -1),
                  -1, NULL,
                  po_communication_pvt.getlocationname ())
             bill_to_location_name,
          DECODE (NVL (ph.bill_to_location_id, -1),
                  -1, NULL,
                  po_communication_pvt.getaddressline1 ())
             bill_to_address_line1,
          DECODE (NVL (ph.bill_to_location_id, -1),
                  -1, NULL,
                  po_communication_pvt.getaddressline2 ())
             bill_to_address_line2,
          DECODE (NVL (ph.bill_to_location_id, -1),
                  -1, NULL,
                  po_communication_pvt.getaddressline3 ())
             bill_to_address_line3,
          DECODE (NVL (ph.bill_to_location_id, -1),
                  -1, NULL,
                  po_communication_pvt.getaddressline4 ())
             bill_to_address_line4,
          DECODE (NVL (ph.bill_to_location_id, -1),
                  -1, NULL,
                  po_communication_pvt.getaddressinfo ())
             bill_to_address_info,
          DECODE (NVL (ph.bill_to_location_id, -1),
                  -1, NULL,
                  po_communication_pvt.getterritoryshortname ())
             bill_to_country,
          ph.attribute1,
          ph.attribute2,
          ph.attribute3,
          ph.attribute4,
          ph.attribute5,
          ph.attribute6,
          ph.attribute7,
          ph.attribute8,
          ph.attribute9,
          ph.attribute10,
          ph.attribute11,
          ph.attribute12,
          ph.attribute13,
          ph.attribute14,
          ph.attribute15,
          ph.vendor_site_id,
          ph.po_header_id,
          DECODE (ph.approved_flag, 'Y', 'Y', 'N') approved_flag,
          pvs.LANGUAGE,
          ph.vendor_id,
          ph.closed_code,
          ph.ussgl_transaction_code,
          ph.government_context,
          ph.request_id,
          ph.program_application_id,
          ph.program_id,
          ph.program_update_date,
          ph.org_id,
          ph.comments,
          TO_CHAR (ph.reply_date, 'DD-MON-YYYY HH24:MI:SS') reply_date,
          ph.reply_method_lookup_code,
          TO_CHAR (ph.rfq_close_date, 'DD-MON-YYYY HH24:MI:SS')
             rfq_close_date,
          ph.quote_type_lookup_code,
          ph.quotation_class_code,
          ph.quote_warning_delay_unit,
          ph.quote_warning_delay,
          ph.quote_vendor_quote_number,
          TO_CHAR (ph.closed_date, 'DD-MON-YYYY HH24:MI:SS') closed_date,
          ph.user_hold_flag,
          ph.approval_required_flag,
          ph.firm_status_lookup_code,
          TO_CHAR (ph.firm_date, 'DD-MON-YYYY HH24:MI:SS') firm_date,
          ph.frozen_flag,
          ph.edi_processed_flag,
          ph.edi_processed_status,
          ph.attribute_category,
          ph.created_by,
          ph.vendor_contact_id,
          ph.terms_id,
          ph.fob_lookup_code,
          ph.freight_terms_lookup_code,
          ph.status_lookup_code,
          ph.rate_type,
          TO_CHAR (ph.rate_date, 'DD-MON-YYYY HH24:MI:SS') rate_date,
          ph.from_header_id,
          ph.from_type_lookup_code,
          NVL (ph.authorization_status, 'N') authorization_status,
          TO_CHAR (ph.approved_date, 'DD-MON-YYYY HH24:MI:SS') approved_date,
          TO_CHAR (ph.amount_limit, po_communication_pvt.getformatmask)
             amount_limit,
          TO_CHAR (ph.min_release_amount, po_communication_pvt.getformatmask)
             min_release_amount,
          ph.note_to_authorizer,
          ph.note_to_receiver,
          ph.vendor_order_num,
          TO_CHAR (ph.last_update_date, 'DD-MON-YYYY HH24:MI:SS')
             last_update_date,
          ph.last_updated_by,
          ph.summary_flag,
          ph.enabled_flag,
          ph.segment2,
          ph.segment3,
          ph.segment4,
          ph.segment5,
          TO_CHAR (ph.start_date_active, 'DD-MON-YYYY HH24:MI:SS')
             start_date_active,
          TO_CHAR (ph.end_date_active, 'DD-MON-YYYY HH24:MI:SS')
             end_date_active,
          ph.last_update_login,
          ph.supply_agreement_flag,
          ph.global_attribute_category,
          ph.global_attribute1,
          ph.global_attribute2,
          ph.global_attribute3,
          ph.global_attribute4,
          ph.global_attribute5,
          ph.global_attribute6,
          ph.global_attribute7,
          ph.global_attribute8,
          ph.global_attribute9,
          ph.global_attribute10,
          ph.global_attribute11,
          ph.global_attribute12,
          ph.global_attribute13,
          ph.global_attribute14,
          ph.global_attribute15,
          ph.global_attribute16,
          ph.global_attribute17,
          ph.global_attribute18,
          ph.global_attribute19,
          ph.global_attribute20,
          ph.interface_source_code,
          ph.reference_num,
          ph.wf_item_type,
          ph.wf_item_key,
          ph.pcard_id,
          ph.price_update_tolerance,
          ph.mrc_rate_type,
          ph.mrc_rate_date,
          ph.mrc_rate,
          ph.pay_on_code,
          ph.xml_flag,
          TO_CHAR (ph.xml_send_date, 'DD-MON-YYYY HH24:MI:SS') xml_send_date,
          TO_CHAR (ph.xml_change_send_date, 'DD-MON-YYYY HH24:MI:SS')
             xml_change_send_date,
          ph.global_agreement_flag,
          ph.consigned_consumption_flag,
          TO_CHAR (ph.cbc_accounting_date, 'DD-MON-YYYY HH24:MI:SS')
             cbc_accounting_date,
          ph.consume_req_demand_flag,
          ph.change_requested_by,
          plc3.meaning shipping_control,
          ph.conterms_exist_flag,
          TO_CHAR (ph.conterms_articles_upd_date, 'DD-MON-YYYY HH24:MI:SS')
             conterms_articles_upd_date,
          TO_CHAR (ph.conterms_deliv_upd_date, 'DD-MON-YYYY HH24:MI:SS')
             conterms_deliv_upd_date,
          NVL (ph.pending_signature_flag, 'N'),
          DECODE (NVL (ph.org_id, -1),
                  -1, NULL,
                  po_communication_pvt.getoperationinfo (ph.org_id))
             ou_name,
          DECODE (NVL (ph.org_id, -1),
                  -1, NULL,
                  po_communication_pvt.getouaddressline1 ())
             ou_addr1,
          DECODE (NVL (ph.org_id, -1),
                  -1, NULL,
                  po_communication_pvt.getouaddressline2 ())
             ou_addr2,
          DECODE (NVL (ph.org_id, -1),
                  -1, NULL,
                  po_communication_pvt.getouaddressline3 ())
             ou_addr3,
          DECODE (NVL (ph.org_id, -1),
                  -1, NULL,
                  po_communication_pvt.getoutowncity ())
             ou_town_city,
          DECODE (NVL (ph.org_id, -1),
                  -1, NULL,
                  po_communication_pvt.getouregion2 ())
             ou_region2,
          DECODE (NVL (ph.org_id, -1),
                  -1, NULL,
                  po_communication_pvt.getoupostalcode ())
             ou_postalcode,
          DECODE (NVL (ph.org_id, -1),
                  -1, NULL,
                  po_communication_pvt.getoucountry ())
             ou_country,
          po_communication_pvt.getlocationinfo (pa.location_id)
             buyer_location_id,
          po_communication_pvt.getaddressline1 () buyer_address_line1,
          po_communication_pvt.getaddressline2 () buyer_address_line2,
          po_communication_pvt.getaddressline3 () buyer_address_line3,
          po_communication_pvt.getaddressline4 () buyer_address_line4,
          po_communication_pvt.getaddressinfo () buyer_city_state_zip,
          po_communication_pvt.getphone (pa.agent_id) buyer_contact_phone,
          po_communication_pvt.getemail () buyer_contact_email,
          NVL (po_communication_pvt.getfax (),
               (SELECT fu.fax
                  FROM fnd_user fu
                 WHERE fu.employee_id = pa.agent_id AND ROWNUM = 1)), --added LCG
          --(po_communication_pvt.getfax () buyer_contact_fax,
          pvs.fax vendor_fax,
          TO_CHAR (
             DECODE (ph.type_lookup_code,
                     'STANDARD', po_core_s.get_total ('H', ph.po_header_id),
                     NULL),
             po_communication_pvt.getformatmask)
             total_amount,
          po_communication_pvt.getterritoryshortname () buyer_country,
          hl.address4 vendor_address_line4,
          pvs.area_code vendor_area_code,
          pvc.area_code vendor_contact_area_code,
          DECODE (NVL (ph.org_id, -1),
                  -1, NULL,
                  po_communication_pvt.getlegalentitydetails (ph.org_id))
             le_name,
          DECODE (NVL (ph.org_id, -1),
                  -1, NULL,
                  po_communication_pvt.getleaddressline1 ())
             le_addr1,
          DECODE (NVL (ph.org_id, -1),
                  -1, NULL,
                  po_communication_pvt.getleaddressline2 ())
             le_addr2,
          DECODE (NVL (ph.org_id, -1),
                  -1, NULL,
                  po_communication_pvt.getleaddressline3 ())
             le_addr3,
          DECODE (NVL (ph.org_id, -1),
                  -1, NULL,
                  po_communication_pvt.getletownorcity ())
             le_town_city,
          DECODE (NVL (ph.org_id, -1),
                  -1, NULL,
                  po_communication_pvt.getlestateorprovince ())
             le_stae_province,
          DECODE (NVL (ph.org_id, -1),
                  -1, NULL,
                  po_communication_pvt.getlepostalcode ())
             le_postalcode,
          DECODE (NVL (ph.org_id, -1),
                  -1, NULL,
                  po_communication_pvt.getlecountry ())
             le_country,
          DECODE (
             ph.cancel_flag,
             'Y', po_communication_pvt.getpocanceldate (ph.po_header_id),
             NULL)
             cancel_date,
          ph.change_summary,
          ph.document_creation_method,
          ph.encumbrance_required_flag,
          psl.display_name style_display_name,
          pvs.fax_area_code vendor_fax_area_code,
          -------Added by LCG for PO Document
          TO_CHAR (SYSDATE, 'MM/DD/YYYY'),
          TO_CHAR (ph.creation_date, 'MM/DD/YYYY'),
          TO_CHAR (TO_DATE (ph.attribute1, 'YYYY/MM/DD HH24:MI:SS'),
                   'MM/DD/YYYY'),
          TO_CHAR (ph.revised_date, 'MM/DD/YYYY'),
          hl3.telephone_number_1,
          --Ship To Phone Number
          hl3.telephone_number_2,                         --Ship To Fax Number
          hl3.town_or_city,                                     --Ship To City
          hl3.region_2,
          --Ship to State

          --hl.country, --Ship to Country
          DECODE (NVL (ph.ship_to_location_id, -1),
                  -1, NULL,
                  po_communication_pvt.getterritoryshortname ())
             ship_to_country,                                               --
          -- hl2.country, --Billto Country
          hl3.postal_code,                               --Ship to Postal Code
          hl2.telephone_number_1,                       --Bill To Phone Number
          hl2.telephone_number_2,
          --Bill To Fax Number
          hl2.town_or_city,                                     --Bill To City
          hl2.region_2,                                        --Bill to State
          --hl2.country, --Billto Country,
          DECODE (NVL (ph.bill_to_location_id, -1),
                  -1, NULL,
                  po_communication_pvt.getterritoryshortname ())
             bill_to_country,
          hl2.postal_code,                               --Bill to Postal Code
          TO_CHAR (
             NVL (
                (SELECT vendor_min_dollar
                   FROM xxwc_po_vendor_minimum xpvm
                  WHERE     xpvm.vendor_id = ph.vendor_id
                        AND xpvm.vendor_site_id = ph.vendor_site_id
                        AND xpvm.organization_id =
                               hl3.inventory_organization_id
                        AND ROWNUM = 1),
                0),
             po_communication_pvt.getformatmask)
             vendor_min_amount,                        --can only return 1 row
          TO_CHAR (
             NVL (
                (SELECT freight_min_dollar
                   FROM xxwc_po_vendor_minimum xpvm
                  WHERE     xpvm.vendor_id = ph.vendor_id
                        AND xpvm.vendor_site_id = ph.vendor_site_id
                        AND xpvm.organization_id =
                               hl3.inventory_organization_id
                        AND ROWNUM = 1),
                0),
             po_communication_pvt.getformatmask)
             vendor_weight_amount,
          NVL (
             (SELECT freight_min_uom
                FROM xxwc_po_vendor_minimum xpvm
               WHERE     xpvm.vendor_id = ph.vendor_id
                     AND xpvm.vendor_site_id = ph.vendor_site_id
                     AND xpvm.organization_id = hl3.inventory_organization_id
                     AND ROWNUM = 1),
             0)
             vendor_weight_uom,
          NVL (
             xxwc_po_document_util.xxwc_get_first_ds_pll_id (ph.po_header_id),
             -1)
             cp_first_ds_pll_id,
          NVL (
             xxwc_po_document_util.xxwc_get_first_ds_ship_loc_id (
                xxwc_po_document_util.xxwc_get_first_ds_pll_id (
                   ph.po_header_id)),
             -1)
             cp_first_ds_ship_loc_id,
          xxwc_po_document_util.xxwc_get_address_info (
             xxwc_po_document_util.xxwc_get_first_ds_pll_id (ph.po_header_id),
             xxwc_po_document_util.xxwc_get_first_ds_ship_loc_id (
                xxwc_po_document_util.xxwc_get_first_ds_pll_id (
                   ph.po_header_id)),
             'ADDR_LINE1')
             xxwc_ds_addr_line1,
          xxwc_po_document_util.xxwc_get_address_info (
             xxwc_po_document_util.xxwc_get_first_ds_pll_id (ph.po_header_id),
             xxwc_po_document_util.xxwc_get_first_ds_ship_loc_id (
                xxwc_po_document_util.xxwc_get_first_ds_pll_id (
                   ph.po_header_id)),
             'ADDR_LINE2')
             xxwc_ds_addr_line2,
          xxwc_po_document_util.xxwc_get_address_info (
             xxwc_po_document_util.xxwc_get_first_ds_pll_id (ph.po_header_id),
             xxwc_po_document_util.xxwc_get_first_ds_ship_loc_id (
                xxwc_po_document_util.xxwc_get_first_ds_pll_id (
                   ph.po_header_id)),
             'ADDR_LINE3')
             xxwc_ds_addr_line3,
          xxwc_po_document_util.xxwc_get_address_info (
             xxwc_po_document_util.xxwc_get_first_ds_pll_id (ph.po_header_id),
             xxwc_po_document_util.xxwc_get_first_ds_ship_loc_id (
                xxwc_po_document_util.xxwc_get_first_ds_pll_id (
                   ph.po_header_id)),
             'ADDR_LINE4')
             xxwc_ds_addr_line4,
          xxwc_po_document_util.xxwc_get_address_info (
             xxwc_po_document_util.xxwc_get_first_ds_pll_id (ph.po_header_id),
             xxwc_po_document_util.xxwc_get_first_ds_ship_loc_id (
                xxwc_po_document_util.xxwc_get_first_ds_pll_id (
                   ph.po_header_id)),
             'TERRITORY')
             xxwc_ds_territory,
          xxwc_po_document_util.xxwc_get_address_info (
             xxwc_po_document_util.xxwc_get_first_ds_pll_id (ph.po_header_id),
             xxwc_po_document_util.xxwc_get_first_ds_ship_loc_id (
                xxwc_po_document_util.xxwc_get_first_ds_pll_id (
                   ph.po_header_id)),
             'LOC_NAME')
             xxwc_ds_location_name,
          xxwc_po_document_util.xxwc_get_address_info (
             xxwc_po_document_util.xxwc_get_first_ds_pll_id (ph.po_header_id),
             xxwc_po_document_util.xxwc_get_first_ds_ship_loc_id (
                xxwc_po_document_util.xxwc_get_first_ds_pll_id (
                   ph.po_header_id)),
             'ADDR_INFO')
             xxwc_ds_addr_info,
          xxwc_po_document_util.xxwc_get_address_info (
             xxwc_po_document_util.xxwc_get_first_ds_pll_id (ph.po_header_id),
             xxwc_po_document_util.xxwc_get_first_ds_ship_loc_id (
                xxwc_po_document_util.xxwc_get_first_ds_pll_id (
                   ph.po_header_id)),
             'TOWN_CITY')
             xxwc_ds_town_city,
          xxwc_po_document_util.xxwc_get_address_info (
             xxwc_po_document_util.xxwc_get_first_ds_pll_id (ph.po_header_id),
             xxwc_po_document_util.xxwc_get_first_ds_ship_loc_id (
                xxwc_po_document_util.xxwc_get_first_ds_pll_id (
                   ph.po_header_id)),
             'POSTAL_CODE')
             xxwc_ds_postal_code,
          xxwc_po_document_util.xxwc_get_address_info (
             xxwc_po_document_util.xxwc_get_first_ds_pll_id (ph.po_header_id),
             xxwc_po_document_util.xxwc_get_first_ds_ship_loc_id (
                xxwc_po_document_util.xxwc_get_first_ds_pll_id (
                   ph.po_header_id)),
             'STATE_PROVINCE')
             xxwc_ds_state_province,
          xxwc_po_document_util.xxwc_get_address_info (
             xxwc_po_document_util.xxwc_get_first_ds_pll_id (ph.po_header_id),
             xxwc_po_document_util.xxwc_get_first_ds_ship_loc_id (
                xxwc_po_document_util.xxwc_get_first_ds_pll_id (
                   ph.po_header_id)),
             'CUSTOMER')
             xxwc_ds_customer_name,
          xxwc_po_document_util.xxwc_get_vendor_contact_id (po_header_id)
             xxwc_vendor_contact_id,
          xxwc_po_document_util.xxwc_get_contact_address_info (
             xxwc_po_document_util.xxwc_get_vendor_contact_id (po_header_id),
             'ADDR_LINE1')
             vendor_contact_address_line1,
          xxwc_po_document_util.xxwc_get_contact_address_info (
             xxwc_po_document_util.xxwc_get_vendor_contact_id (po_header_id),
             'ADDR_LINE2')
             vendor_contact_address_line2,
          xxwc_po_document_util.xxwc_get_contact_address_info (
             xxwc_po_document_util.xxwc_get_vendor_contact_id (po_header_id),
             'ADDR_LINE3')
             vendor_contact_address_line3,
          xxwc_po_document_util.xxwc_get_contact_address_info (
             xxwc_po_document_util.xxwc_get_vendor_contact_id (po_header_id),
             'ADDR_LINE4')
             vendor_contact_address_line4,
          xxwc_po_document_util.xxwc_get_contact_address_info (
             xxwc_po_document_util.xxwc_get_vendor_contact_id (po_header_id),
             'TOWN_CITY')
             vendor_contact_town_city,
          xxwc_po_document_util.xxwc_get_contact_address_info (
             xxwc_po_document_util.xxwc_get_vendor_contact_id (po_header_id),
             'STATE_PROVINCE')
             vendor_contact_STATE_PROVICE,
          xxwc_po_document_util.xxwc_get_contact_address_info (
             xxwc_po_document_util.xxwc_get_vendor_contact_id (po_header_id),
             'POSTAL_CODE')
             vendor_contact_postal_code,
          xxwc_po_document_util.xxwc_get_contact_address_info (
             xxwc_po_document_util.xxwc_get_vendor_contact_id (po_header_id),
             'COUNTRY')
             vendor_contact_country,
          xxwc_po_document_util.xxwc_get_contact_address_info (
             xxwc_po_document_util.xxwc_get_vendor_contact_id (po_header_id),
             'PHONE')
             vendor_contact_phone,
          xxwc_po_document_util.xxwc_get_contact_address_info (
             xxwc_po_document_util.xxwc_get_vendor_contact_id (po_header_id),
             'ADDRESS_INFO')
             vendor_contact_address_info,
          --Added by Veera For TMS#20140715-00024
          contact_cust.customer_phone,
          contact_cust.customer_contact,
          contact_cust.request_date,
          contact_cust.drop_ship_flag,
          wcv.carrier_name,
          (CASE
              --ESMS Ticket#:258131
              -- Only when Operating Unit is HD Supply Corp USD - Org OR HD Supply Corp CAD - Org
              WHEN (mo_global.get_current_org_id IN (163, 167))
              THEN
                 (SELECT COUNT (UNIQUE ship_to_location_id)
                    FROM po_line_locations
                   WHERE 1 = 1 AND po_header_id = ph.po_header_id)
              ELSE
                 TO_NUMBER (NULL)
           END),
          --
          CASE
             --ESMS Ticket#:258131
             -- Only when Operating Unit is HD Supply Corp USD - Org OR HD Supply Corp CAD - Org
             WHEN (mo_global.get_current_org_id IN (163, 167))
             THEN
                (SELECT TO_CHAR (NVL (SUM (pl.unit_price * pl.quantity), 0),
                                 'FM999,990.90')
                   FROM po_lines pl
                  WHERE     1 = 1
                        AND pl.po_header_id = ph.po_header_id
                        AND NOT EXISTS
                                   (SELECT 1
                                      FROM mtl_categories_v
                                     WHERE     1 = 1
                                           AND category_id = pl.category_id
                                           AND category_concat_segs IN
                                                  ('ZZ.ZFRT', 'ZZ.ZTAX')))
             ELSE
                '0.00'                                 --XXCUS_GSC_PROD_AMOUNT
          END,
          --
          CASE
             --ESMS Ticket#:258131
             -- Only when Operating Unit is HD Supply Corp USD - Org OR HD Supply Corp CAD - Org
             WHEN (mo_global.get_current_org_id IN (163, 167))
             THEN
                (SELECT TO_CHAR (NVL (SUM (pl.unit_price * pl.quantity), 0),
                                 'FM999,990.90')
                   FROM po_lines pl
                  WHERE     1 = 1
                        AND pl.po_header_id = ph.po_header_id
                        AND EXISTS
                               (SELECT 1
                                  FROM mtl_categories_v
                                 WHERE     1 = 1
                                       AND category_id = pl.category_id
                                       AND category_concat_segs = 'ZZ.ZFRT'))
             ELSE
                '0.00'                                  --XXCUS_GSC_FRT_AMOUNT
          END,
          --
          CASE
             --ESMS Ticket#:258131
             -- Only when Operating Unit is HD Supply Corp USD - Org OR HD Supply Corp CAD - Org
             WHEN (mo_global.get_current_org_id IN (163, 167))
             THEN
                (SELECT TO_CHAR (NVL (SUM (pl.unit_price * pl.quantity), 0),
                                 'FM999,990.90')
                   FROM po_lines pl
                  WHERE     1 = 1
                        AND pl.po_header_id = ph.po_header_id
                        AND EXISTS
                               (SELECT 1
                                  FROM mtl_categories_v
                                 WHERE     1 = 1
                                       AND category_id = pl.category_id
                                       AND category_concat_segs = 'ZZ.ZTAX'))
             ELSE
                '0.00'                                  --XXCUS_GSC_TAX_AMOUNT
          END,
          CASE
             --ESMS Ticket#:275677
             -- Only when Operating Unit is HD Supply Corp USD - Org OR HD Supply Corp CAD - Org
             WHEN (mo_global.get_current_org_id IN (163, 167))
             THEN
                (SELECT 'Attn: ' || D.DESCRIPTION
                   --User who created the corresponding requisition for the PO
                   FROM FND_USER D,
                        PO_DISTRIBUTIONS_ALL C,
                        PO_REQ_DISTRIBUTIONS_ALL B
                  WHERE     1 = 1
                        AND D.USER_ID = B.CREATED_BY
                        AND B.DISTRIBUTION_ID = C.REQ_DISTRIBUTION_ID
                        AND C.PO_HEADER_ID = ph.po_header_id
                        AND ROWNUM = 1)
             ELSE
                NULL
          END
             REQ_ATTN_TO,
          doc_owner.description PO_DOC_OWNER,	
-- Added DISCOUNT_AMOUNT , DISCOUNT_LABEL and NET_AMOUNT for Ver 1.6		  
		  TO_CHAR(DECODE (ph.attribute7, 'PERCENT' , 
				(DECODE (ph.type_lookup_code, 'STANDARD', po_core_s.get_total ('H', ph.po_header_id), NULL)* TO_NUMBER(ph.attribute8)/100), 
				     NVL(TO_NUMBER(ph.attribute8),0)) 
					 , po_communication_pvt.getformatmask) DISCOUNT_AMOUNT,
          DECODE (ph.attribute7, 'PERCENT' , to_number(ph.attribute8)||' % Discount ' , 'Net Amount Discount ') DISCOUNT_LABEL,
          TO_CHAR(DECODE (ph.type_lookup_code, 'STANDARD', po_core_s.get_total ('H', ph.po_header_id), NULL) 
		        - DECODE (ph.attribute7, 'PERCENT' , 
				 (DECODE (ph.type_lookup_code, 'STANDARD', po_core_s.get_total ('H', ph.po_header_id), NULL)* TO_NUMBER(ph.attribute8)/100), 
				     NVL(TO_NUMBER(ph.attribute8),0)) , po_communication_pvt.getformatmask) NET_AMOUNT		  
     FROM fnd_lookup_values plc1,
          fnd_lookup_values plc2,
          fnd_currencies_tl fcc,
          ap_suppliers vn,
          ap_supplier_sites_all pvs,
          hz_locations hl,
          --PO_VENDOR_CONTACTS PVC, removed 6/26/2013 to help performance
          ap_supplier_contacts pvc,      --added 6/26/2013 to help performance
          per_all_people_f hre,
          ap_terms t,
          po_headers_all ph,
          fnd_territories_tl fte3,
          org_freight_tl ofc,
          po_agents pa,
          fnd_lookup_values plc3,
          po_doc_style_lines_tl psl,
          fnd_lookup_values plc4,
          --HR_LOOKUPS HRL, --Removed 6/26/2013 not needed
          ----HR Locations--- Added by LCG
          hr_locations hl3,                                          --Ship_to
          hr_locations hl2,                                         --Bill_to,
          --Added by Veera For TMS#20140715-00024
          (SELECT ph.po_header_id header_id,
                  TO_CHAR (h.request_date, 'MM/DD/YYYY') request_date,
                  'Y' drop_ship_flag,
                  cont_point.raw_phone_number customer_phone,
                  party.person_last_name || ',' || party.person_first_name
                     customer_contact
             FROM --oe_drop_ship_sources   ods                                            --Commented for version 1.2
                 oe_order_headers_all h,
                  po_headers_all ph,
                  hz_contact_points cont_point,
                  hz_cust_account_roles acct_role,
                  hz_parties party,
                  hz_parties rel_party,
                  hz_relationships rel,
                  hz_cust_accounts role_acct
            WHERE --           h.header_id          = ods.header_id                               --Commented and added below EXISTS for version 1.2
                      --    AND    ods.po_header_id     = ph.po_header_id
                      EXISTS
                         (SELECT 1
                            FROM oe_drop_ship_sources ODS
                           WHERE     h.header_id = ods.header_id
                                 AND ods.po_header_id = ph.po_header_id)
                  AND acct_role.PARTY_ID = rel.party_id
                  AND acct_role.role_type = 'CONTACT'
                  AND rel.subject_id = party.party_id
                  AND rel_party.party_id = rel.party_id
                  AND cont_point.owner_table_id(+) = rel_party.party_id
                  AND cont_point.contact_point_type(+) = 'PHONE'
                  AND cont_point.primary_flag(+) = 'Y'
                  AND cont_point.status(+) = 'A'
                  AND acct_role.status(+) = 'A'
                  AND rel_party.status(+) = 'A'
                  AND acct_role.cust_account_id = role_acct.cust_account_id
                  AND role_acct.party_id = REL.OBJECT_ID
                  AND cont_point.owner_table_name(+) = 'HZ_PARTIES'
                  AND acct_role.cust_account_role_id = h.ship_to_contact_id) contact_cust,
          wsh_carriers_v wcv,
          fnd_user doc_owner
    WHERE  --   HRL.LOOKUP_CODE(+)  = HRE.TITLE --Removed 6/26/2013 Not Needed
            --AND HRL.LOOKUP_TYPE(+)  ='TITLE' --Removed  6/26/2013 Not Needed
 --AND VN.VENDOR_ID(+)     = PH.VENDOR_ID Removed 6/26/2013 -- from outerjoin unique join
              ph.vendor_id = vn.vendor_id
          --Updated 6/26/2013 -- from outerjoin unique join
          --AND PVS.VENDOR_SITE_ID(+) = PH.VENDOR_SITE_ID-- Removed 6/26/2013 -- from outerjoin unique join
          AND ph.vendor_site_id = pvs.vendor_site_id
          --Updated 6/26/2013 -- from outerjoin unique join
          AND vn.vendor_id = pvs.vendor_id --Added 6/26/2013 to help performance
          AND pvs.location_id = hl.location_id       -- Removed 06/26/2013 (+)
          AND ph.vendor_contact_id = pvc.vendor_contact_id(+)
          --TMS Ticket 20130702-01128 Commented Out 7/2/2013 Due to some PO documents not generating PDF file
          --AND (   ph.vendor_contact_id IS NULL
          --     OR ph.vendor_site_id = pvc.vendor_site_id
          --  )
          AND hre.person_id = ph.agent_id
          AND TRUNC (SYSDATE) BETWEEN hre.effective_start_date
                                  AND hre.effective_end_date
          AND ph.terms_id = t.term_id(+)
          AND ph.type_lookup_code IN ('STANDARD', 'BLANKET', 'CONTRACT')
          AND fcc.currency_code = ph.currency_code
          AND plc1.lookup_code(+) = ph.fob_lookup_code
          AND plc1.lookup_type(+) = 'FOB'
          AND plc1.LANGUAGE(+) = USERENV ('LANG')
          AND plc1.view_application_id(+) = 201
          AND DECODE (plc1.lookup_code, NULL, 1, plc1.security_group_id) =
                 DECODE (
                    plc1.lookup_code,
                    NULL, 1,
                    fnd_global.lookup_security_group (
                       plc1.lookup_type,
                       plc1.view_application_id))
          AND plc2.lookup_code(+) = ph.freight_terms_lookup_code
          AND plc2.lookup_type(+) = 'FREIGHT TERMS'
          AND plc2.LANGUAGE(+) = USERENV ('LANG')
          AND plc2.view_application_id(+) = 201
          AND DECODE (plc2.lookup_code, NULL, 1, plc2.security_group_id) =
                 DECODE (
                    plc2.lookup_code,
                    NULL, 1,
                    fnd_global.lookup_security_group (
                       plc2.lookup_type,
                       plc2.view_application_id))
          AND SUBSTR (hl.country, 1, 25) = fte3.territory_code(+)
          AND DECODE (fte3.territory_code, NULL, '1', fte3.LANGUAGE) =
                 DECODE (fte3.territory_code, NULL, '1', USERENV ('LANG'))
          AND ofc.freight_code(+) = ph.ship_via_lookup_code
          AND ofc.organization_id(+) = ph.org_id
          AND pa.agent_id = ph.agent_id
          AND plc3.lookup_code(+) = ph.shipping_control
          AND plc3.lookup_type(+) = 'SHIPPING CONTROL'
          AND plc3.LANGUAGE(+) = USERENV ('LANG')
          AND plc3.view_application_id(+) = 201
          AND DECODE (plc3.lookup_code, NULL, 1, plc3.security_group_id) =
                 DECODE (
                    plc3.lookup_code,
                    NULL, 1,
                    fnd_global.lookup_security_group (
                       plc3.lookup_type,
                       plc3.view_application_id))
          AND fcc.LANGUAGE = USERENV ('LANG')
          AND ofc.LANGUAGE(+) = USERENV ('LANG')
          AND ph.style_id = psl.style_id(+)
          AND psl.LANGUAGE(+) = USERENV ('LANG')
          AND psl.document_subtype(+) = ph.type_lookup_code
          AND plc4.lookup_code(+) = pvc.prefix
          AND plc4.lookup_type(+) = 'CONTACT_TITLE'
          AND plc4.LANGUAGE(+) = USERENV ('LANG')
          AND plc4.view_application_id(+) = 222
          AND DECODE (plc4.lookup_code, NULL, 1, plc4.security_group_id) =
                 DECODE (
                    plc4.lookup_code,
                    NULL, 1,
                    fnd_global.lookup_security_group (
                       plc4.lookup_type,
                       plc4.view_application_id))
          -----Added by LCG
          AND ph.ship_to_location_id = hl3.location_id(+)
          --to pull the ship to phone and fax information
          AND ph.bill_to_location_id = hl2.location_id(+)
          --to pull the bill to phone and fax information -- Removed Semi colons for Ver 1.6 
          --AND rownum = 1 -- Removed Semi colons for Ver 1.6 
          --Added by Veera For TMS#20140715-00024
          AND contact_cust.header_id(+) = ph.po_header_id
          AND wcv.freight_code(+) = ph.ship_via_lookup_code
          AND doc_owner.user_id(+) = ph.created_by;
/		  

COMMENT ON TABLE APPS.PO_HEADERS_XML IS 'ESMS 289744';
/
