CREATE OR REPLACE PACKAGE APPS.XXWC_PO_DISCOUNT_PKG
/*************************************************************************
  $Header XXWC_PO_DISCOUNT_PKG.pks $
  Module Name: XXWC_PO_DISCOUNT_PKG

  PURPOSE: To Apply / Remove discount in PO lines

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        21-Jul-2015  Manjula Chellappan    Initial Version TMS # 20131120-00027

**************************************************************************/
AS
   PROCEDURE apply_line_discount (p_po_header_id   IN     NUMBER,
                                  x_status_msg        OUT VARCHAR2);


   PROCEDURE remove_line_discount (p_po_header_id   IN     NUMBER,
                                   x_status_msg        OUT VARCHAR2);


   FUNCTION check_po_discounted (p_po_header_id IN NUMBER)
      RETURN VARCHAR2;

   FUNCTION check_po_header_discounted (p_po_header_id IN NUMBER)
      RETURN VARCHAR2;


   FUNCTION check_po_lines_discounted (p_po_header_id IN NUMBER)
      RETURN VARCHAR2;


   FUNCTION get_po_header_discount (p_po_header_id IN NUMBER)
      RETURN VARCHAR2;
	  
   FUNCTION get_discount_note_to_ap (p_po_header_id IN NUMBER)
      RETURN VARCHAR2;	  
END XXWC_PO_DISCOUNT_PKG;
/
