CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_PRC_CONSTRAINTS_PKG
AS
   /********************************************************************************
   FILE NAME: XXWC_OM_PRC_CONSTRAINTS_PKG.pkg

   PROGRAM TYPE: PL/SQL Package Body

   PURPOSE: Restrict sales order cancellation,updation using processing constraints

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     1/07/2014       Harsha         Initial creation of the procedure
   1.1     10/30/2014    Pattabhi Avula   TMS# 20141002-00060 -- Canada Changes removed
                                          _ALL for org specific tables
   1.2     05/23/2017    Rakesh Patel     TMS# 20170323-00225 --Custom SOE entry - Phase2										  
   ********************************************************************************/

  

   --This function is to check the end user profile organization.

   FUNCTION XXWC_GET_USER_PROF_VAL (V_PROFILE_USER VARCHAR2)
      RETURN NUMBER
   IS
      v_errbuf   CLOB;
      V_VAL      NUMBER;
   BEGIN
      BEGIN
         SELECT VALUE
           INTO V_VAL
           FROM (SELECT PO.PROFILE_OPTION_NAME "NAME",
                        PO.USER_PROFILE_OPTION_NAME,
                        DECODE (TO_CHAR (POV.LEVEL_ID),
                                '10001', 'SITE',
                                '10002', 'APP',
                                '10003', 'RESP',
                                '10005', 'SERVER',
                                '10006', 'ORG',
                                '10004', 'USER',
                                '***')
                           "LEVEL",
                        DECODE (TO_CHAR (POV.LEVEL_ID),
                                '10001', '',
                                '10002', APP.APPLICATION_SHORT_NAME,
                                '10003', RSP.RESPONSIBILITY_KEY,
                                '10005', SVR.NODE_NAME,
                                '10006', ORG.NAME,
                                '10004', USR.USER_NAME,
                                '***')
                           "CONTEXT",
                        POV.PROFILE_OPTION_VALUE "VALUE"
                   FROM APPS.FND_PROFILE_OPTIONS_VL PO,
                        APPS.FND_PROFILE_OPTION_VALUES POV,
                        APPS.FND_USER USR,
                        APPS.FND_APPLICATION APP,
                        APPS.FND_RESPONSIBILITY RSP,
                        APPS.FND_NODES SVR,
                        APPS.HR_OPERATING_UNITS ORG
                  WHERE     1 = 1
                        AND POV.APPLICATION_ID = PO.APPLICATION_ID
                        AND POV.PROFILE_OPTION_ID = PO.PROFILE_OPTION_ID
                        AND USR.USER_ID(+) = POV.LEVEL_VALUE
                        AND RSP.APPLICATION_ID(+) =
                               POV.LEVEL_VALUE_APPLICATION_ID
                        AND RSP.RESPONSIBILITY_ID(+) = POV.LEVEL_VALUE
                        AND APP.APPLICATION_ID(+) = POV.LEVEL_VALUE
                        AND SVR.NODE_ID(+) = POV.LEVEL_VALUE
                        AND ORG.ORGANIZATION_ID(+) = POV.LEVEL_VALUE
                        AND PO.PROFILE_OPTION_NAME =
                               'XXWC_OM_DEFAULT_SHIPPING_ORG') xxwc
          WHERE xxwc.context = V_PROFILE_USER;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_val := 0;
         WHEN OTHERS
         THEN
            v_errbuf :=
                  'Error_Stack...'
               || CHR (10)
               || DBMS_UTILITY.format_error_stack ()
               || CHR (10)
               || ' Error_Backtrace...'
               || CHR (10)
               || DBMS_UTILITY.format_error_backtrace ();



            xxcus_error_pkg.xxcus_error_main_api (
               p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
               p_calling             => 'XXWC_GET_USER_PROF_VAL',
               p_request_id          => fnd_global.conc_request_id,
               p_ora_error_msg       => SUBSTR (
                                          REGEXP_REPLACE (v_errbuf,
                                                          '[[:cntrl:]]',
                                                          NULL),
                                          1,
                                          2000),
               p_error_desc          =>    'Error running '
                                        || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                        || '.'
                                        || 'XXWC_GET_USER_PROF_VAl',
               p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
               p_module              => 'ONT');


            v_val := 0;
      END;

      DBMS_OUTPUT.put_line ('v_val..' || v_val);


      RETURN V_VAL;
   END XXWC_GET_USER_PROF_VAL;


   --This Procedure is for restricting order header cancellation if enduser org id<> sales order line org id and order status is booked

   PROCEDURE CANCEL_HEADER (
      p_application_id                 IN            NUMBER,
      p_entity_short_name              IN            VARCHAR2,
      p_validation_entity_short_name   IN            VARCHAR2,
      p_validation_tmplt_short_name    IN            VARCHAR2,
      p_record_set_short_name          IN            VARCHAR2,
      p_scope                          IN            VARCHAR2,
      x_result                            OUT NOCOPY NUMBER)
   AS
      v_val           NUMBER := 0;
      v_flow_status   VARCHAR2 (30);
   BEGIN
      SELECT COUNT (1)
        INTO v_val
        FROM apps.oe_order_lines oel, apps.oe_order_headers oeh
       WHERE     oeh.order_number = oe_header_security.g_record.ORDER_NUMBER
             AND oel.header_id = oeh.header_id
             AND OEL.FLOW_STATUS_CODE!='CANCELLED'
             AND oel.SHIP_FROM_ORG_ID !=
                    XXWC_GET_USER_PROF_VAL (FND_GLOBAL.USER_NAME);


      BEGIN
         SELECT oh.flow_status_code
           INTO v_flow_status
           FROM apps.oe_order_headers oh
          WHERE     oh.header_id = oe_header_security.g_record.header_id
                AND oh.org_id = oe_header_security.g_record.org_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_flow_status := null;
     WHEN OTHERS
         THEN
            v_errbuf :=
                  'Error_Stack...'
               || CHR (10)
               || DBMS_UTILITY.format_error_stack ()
               || CHR (10)
               || ' Error_Backtrace...'
               || CHR (10)
               || DBMS_UTILITY.format_error_backtrace ();



            xxcus_error_pkg.xxcus_error_main_api (
               p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
               p_calling             => 'CANCEL_HEADER',
               p_request_id          => fnd_global.conc_request_id,
               p_ora_error_msg       => SUBSTR (
                                          REGEXP_REPLACE (v_errbuf,
                                                          '[[:cntrl:]]',
                                                          NULL),
                                          1,
                                          2000),
               p_error_desc          =>    'Error running '
                                        || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                        || '.'
                                        || 'CANCEL_HEADER',
               p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
               p_module              => 'ONT');
          v_flow_status := null;

         
      END;


      IF v_flow_status = 'BOOKED'
      THEN
         IF v_val = 0
         THEN
           
            x_result := 0;
         ELSE
            
            x_result := 1;
         END IF;
      ELSE
         
         x_result := 0;
      END IF;

     EXCEPTION
      WHEN OTHERS
         THEN
            v_errbuf :=
                  'Error_Stack...'
               || CHR (10)
               || DBMS_UTILITY.format_error_stack ()
               || CHR (10)
               || ' Error_Backtrace...'
               || CHR (10)
               || DBMS_UTILITY.format_error_backtrace ();



            xxcus_error_pkg.xxcus_error_main_api (
               p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
               p_calling             => 'CANCEL_HEADER',
               p_request_id          => fnd_global.conc_request_id,
               p_ora_error_msg       => SUBSTR (
                                          REGEXP_REPLACE (v_errbuf,
                                                          '[[:cntrl:]]',
                                                          NULL),
                                          1,
                                          2000),
               p_error_desc          =>    'Error running '
                                        || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                        || '.'
                                        || 'CANCEL_HEADER',
               p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
               p_module              => 'ONT');
   END CANCEL_HEADER;


   --This Procedure is for restricting order line cancellation if enduser org id<> sales order line org id and order status is booked

   PROCEDURE CANCEL_LINE (
      p_application_id                 IN            NUMBER,
      p_entity_short_name              IN            VARCHAR2,
      p_validation_entity_short_name   IN            VARCHAR2,
      p_validation_tmplt_short_name    IN            VARCHAR2,
      p_record_set_short_name          IN            VARCHAR2,
      p_scope                          IN            VARCHAR2,
      x_result                            OUT NOCOPY NUMBER)
   AS
      v_val           NUMBER := 0;
      v_flow_status   VARCHAR2 (30);
      v_qty           NUMBER := 0;
   BEGIN
      SELECT COUNT (1)
        INTO v_val
        FROM apps.oe_order_lines oel
       WHERE     oel.SHIP_FROM_ORG_ID !=
                    XXWC_GET_USER_PROF_VAL (FND_GLOBAL.USER_NAME)
             AND OEL.line_id = oe_line_security.g_record.line_id
             AND OEL.org_id = oe_line_security.g_record.org_id;





      BEGIN
         SELECT ol.ORDERED_QUANTITY, oh.flow_status_code
           INTO v_qty, v_flow_status
           FROM apps.oe_order_headers oh, apps.oe_order_lines ol
          WHERE     oh.header_id = oe_line_security.g_record.header_id
                AND oh.header_id = ol.header_id
                AND ol.line_id = oe_line_security.g_record.line_id
                AND ol.org_id = oe_line_security.g_record.org_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
             v_qty := 0;
         v_flow_status:=null;

    WHEN OTHERS
         THEN
            v_errbuf :=
                  'Error_Stack...'
               || CHR (10)
               || DBMS_UTILITY.format_error_stack ()
               || CHR (10)
               || ' Error_Backtrace...'
               || CHR (10)
               || DBMS_UTILITY.format_error_backtrace ();



            xxcus_error_pkg.xxcus_error_main_api (
               p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
               p_calling             => 'CANCEL_LINE',
               p_request_id          => fnd_global.conc_request_id,
               p_ora_error_msg       => SUBSTR (
                                          REGEXP_REPLACE (v_errbuf,
                                                          '[[:cntrl:]]',
                                                          NULL),
                                          1,
                                          2000),
               p_error_desc          =>    'Error running '
                                        || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                        || '.'
                                        || 'CANCEL_LINE',
               p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
               p_module              => 'ONT');
          v_qty := 0;
         v_flow_status:=null;


      END;

      



      IF v_flow_status = 'BOOKED'
      THEN
         IF v_val = 0
         THEN
           
            x_result := 0;
         ELSE

            x_result := 1;
         END IF;
      ELSE

         x_result := 0;
      END IF;

      EXCEPTION
       WHEN OTHERS
         THEN
            v_errbuf :=
                  'Error_Stack...'
               || CHR (10)
               || DBMS_UTILITY.format_error_stack ()
               || CHR (10)
               || ' Error_Backtrace...'
               || CHR (10)
               || DBMS_UTILITY.format_error_backtrace ();



            xxcus_error_pkg.xxcus_error_main_api (
               p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
               p_calling             => 'CANCEL_LINE',
               p_request_id          => fnd_global.conc_request_id,
               p_ora_error_msg       => SUBSTR (
                                          REGEXP_REPLACE (v_errbuf,
                                                          '[[:cntrl:]]',
                                                          NULL),
                                          1,
                                          2000),
               p_error_desc          =>    'Error running '
                                        || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                        || '.'
                                        || 'CANCEL_LINE',
               p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
               p_module              => 'ONT');
   END CANCEL_LINE;


   --This procedure is to restrict order line quantity updation if enduser org id <> sales order line org id and order status is booked

   PROCEDURE UPDATE_LINE (
      p_application_id                 IN            NUMBER,
      p_entity_short_name              IN            VARCHAR2,
      p_validation_entity_short_name   IN            VARCHAR2,
      p_validation_tmplt_short_name    IN            VARCHAR2,
      p_record_set_short_name          IN            VARCHAR2,
      p_scope                          IN            VARCHAR2,
      x_result                            OUT NOCOPY NUMBER)
   AS
      v_val           NUMBER := 0;
      v_errbuf        VARCHAR2 (2000);
      v_flow_status   VARCHAR2 (50);
   BEGIN
      


      BEGIN
           SELECT ol.ORDERED_QUANTITY, oh.flow_status_code
             INTO v_val, v_flow_status
             FROM apps.oe_order_headers oh, apps.oe_order_lines ol
            WHERE     oh.header_id = oe_line_security.g_record.header_id
                  AND oh.header_id = ol.header_id
                  AND ol.line_id = oe_line_security.g_record.line_id
                  AND ol.org_id = oe_line_security.g_record.org_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_val := 0;
            v_flow_status:=NULL;

           WHEN OTHERS
         THEN
            v_errbuf :=
                  'Error_Stack...'
               || CHR (10)
               || DBMS_UTILITY.format_error_stack ()
               || CHR (10)
               || ' Error_Backtrace...'
               || CHR (10)
               || DBMS_UTILITY.format_error_backtrace ();



            xxcus_error_pkg.xxcus_error_main_api (
               p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
               p_calling             => 'UPDATE_LINE',
               p_request_id          => fnd_global.conc_request_id,
               p_ora_error_msg       => SUBSTR (
                                          REGEXP_REPLACE (v_errbuf,
                                                          '[[:cntrl:]]',
                                                          NULL),
                                          1,
                                          2000),
               p_error_desc          =>    'Error running '
                                        || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                        || '.'
                                        || 'UPDATE_LINE',
               p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
               p_module              => 'ONT');

              v_val := 0;
            v_flow_status:=NULL;
      END;


      IF v_flow_status = 'BOOKED'
      THEN
         IF oe_line_security.g_record.ship_from_org_id !=
               XXWC_GET_USER_PROF_VAL (FND_GLOBAL.USER_NAME)
         THEN
            IF oe_line_security.g_record.ORDERED_QUANTITY != v_val
            THEN
                
               x_result := 1;
            ELSE
                 
               x_result := 0;
            END IF;
         ELSE                                             --USER PROFILE VALUE
              
            x_result := 0;
         END IF;
      ELSE
         
         x_result := 0;
      END IF;
   
 EXCEPTION
   WHEN OTHERS
         THEN
            v_errbuf :=
                  'Error_Stack...'
               || CHR (10)
               || DBMS_UTILITY.format_error_stack ()
               || CHR (10)
               || ' Error_Backtrace...'
               || CHR (10)
               || DBMS_UTILITY.format_error_backtrace ();



            xxcus_error_pkg.xxcus_error_main_api (
               p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
               p_calling             => 'UPDATE_LINE',
               p_request_id          => fnd_global.conc_request_id,
               p_ora_error_msg       => SUBSTR (
                                          REGEXP_REPLACE (v_errbuf,
                                                          '[[:cntrl:]]',
                                                          NULL),
                                          1,
                                          2000),
               p_error_desc          =>    'Error running '
                                        || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                        || '.'
                                        || 'UPDATE_LINE',
               p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
               p_module              => 'ONT');


   END update_LINE;
   
   --Ver 1.2 <Start 
   /*******************************************************************************
   Procedure Name   :   CHANGE_LINE_WAREHOUSE
   Description      :   This procedure is to restrict wharehouse line updation for counter order 

   Change History  :
   DATE         NAME              Modification
   --------     -------           ------------------------------------------
   15-Mar-17    Rakesh Patel      TMS# 20170323-00225 --Custom SOE entry - Phase2 
   ******************************************************************************/
   PROCEDURE CHANGE_LINE_WAREHOUSE (
         p_application_id                 IN            NUMBER,
         p_entity_short_name              IN            VARCHAR2,
         p_validation_entity_short_name   IN            VARCHAR2,
         p_validation_tmplt_short_name    IN            VARCHAR2,
         p_record_set_short_name          IN            VARCHAR2,
         p_scope                          IN            VARCHAR2,
         x_result                            OUT NOCOPY NUMBER)
      AS
         v_ship_from_org_id     apps.oe_order_headers.ship_from_org_id%TYPE;
         v_order_type_id        apps.oe_order_headers.order_type_id%TYPE;
         v_errbuf               VARCHAR2 (2000);
      BEGIN
         BEGIN
            SELECT oh.ship_from_org_id, oh.order_type_id
              INTO v_ship_from_org_id, v_order_type_id
              FROM apps.oe_order_headers oh
             WHERE oh.header_id = oe_line_security.g_record.header_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_ship_from_org_id := 0;
               v_order_type_id    := 0;
            WHEN OTHERS
            THEN
               v_errbuf :=
                     'Error_Stack...'
                  || CHR (10)
                  || DBMS_UTILITY.format_error_stack ()
                  || CHR (10)
                  || ' Error_Backtrace...'
                  || CHR (10)
                  || DBMS_UTILITY.format_error_backtrace ();
   
   
   
               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
                  p_calling             => 'CHANGE_LINE_WAREHOUSE',
                  p_request_id          => fnd_global.conc_request_id,
                  p_ora_error_msg       => SUBSTR (
                                             REGEXP_REPLACE (v_errbuf,
                                                             '[[:cntrl:]]',
                                                             NULL),
                                             1,
                                             2000),
                  p_error_desc          =>    'Error running '
                                           || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                           || '.'
                                           || 'CHANGE_LINE_WAREHOUSE',
                  p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
                  p_module              => 'ONT');
   
               v_ship_from_org_id := 0;
               v_order_type_id    := 0;
         END;
   
         IF oe_line_security.g_record.ship_from_org_id != v_ship_from_org_id AND v_order_type_id = 1004 THEN
            x_result := 1;
         ELSE
            x_result := 0;
         END IF;
   
      EXCEPTION
         WHEN OTHERS
         THEN
            v_errbuf :=
                  'Error_Stack...'
               || CHR (10)
               || DBMS_UTILITY.format_error_stack ()
               || CHR (10)
               || ' Error_Backtrace...'
               || CHR (10)
               || DBMS_UTILITY.format_error_backtrace ();
   
   
   
            xxcus_error_pkg.xxcus_error_main_api (
               p_called_from         => 'XXWC_OM_PRC_CONSTRAINTS_PKG',
               p_calling             => 'CHANGE_LINE_WAREHOUSE',
               p_request_id          => fnd_global.conc_request_id,
               p_ora_error_msg       => SUBSTR (
                                          REGEXP_REPLACE (v_errbuf,
                                                          '[[:cntrl:]]',
                                                          NULL),
                                          1,
                                          2000),
               p_error_desc          =>    'Error running '
                                        || 'XXWC_OM_PRC_CONSTRAINTS_PKG'
                                        || '.'
                                        || 'CHANGE_LINE_WAREHOUSE',
               p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
               p_module              => 'ONT');
   END CHANGE_LINE_WAREHOUSE;
   --Ver 1.2 >End
   
END XXWC_OM_PRC_CONSTRAINTS_PKG;
/
