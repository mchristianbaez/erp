CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_OZF_LOB_ROR_PKG
-- Scope: Oracle Trade Management / Rebates
-- Used by concurrent jobs HDS Rebates: Generate WC Rebate Rate of Return Extract
--
-- Change History:
--  Ticket                       Version Date          Author                         Notes
 --  =========================   ======  ===========   ============================== =========================================================
 --  TMS 20160809-00142          1.0     08/09/2016    Balaguru Seshadri              ESMS 430286 - New routine to extract White Cap ROR Data
 --  TMS 20160809-00292          1.1     08/10/2016    Balaguru Seshadri              ESMS 432568 - Pull data for current year only.
as
 --
  procedure print_log(p_message in varchar2) is
  begin
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||sqlerrm);
    dbms_output.put_line('Issue in print_log routine ='||sqlerrm);
  end print_log;
  --
  --
  -- Extract Whitecap rebate rate of return always for the current year
  --
  procedure extract_wc_ror
  (
    retcode               out number
   ,errbuf                out varchar2
  ) is
  -- Begin variables
      --
    type l_wc_ror_record is record
     (
         current_agreement_year    number
        ,mvid    varchar2 (30 byte)
        ,vendor    varchar2 (360 byte)
        ,plan_id    number
        ,offer_name    varchar2 (2000 byte)
        ,purchases    number
        ,total_accruals    number
        ,system_accruals    number
        ,manual_adjustments    number
        ,total_ror    number
        ,system_ror    number
        ,manual_ror    number
     );  
     --
     get_wc_ror_set_row l_wc_ror_record;
     --
      b_proceed           boolean;
      l_period                 varchar2(10) :=Null;
      l_partition            varchar2(3) :=Null;  
      l_partition_NA    varchar2(2) :=Null;
      --
      l_start_date       date;
      l_end_date         date;  
      n_count number :=0;
      n_user_id number :=fnd_global.user_id;
      --  
      cursor c_ror is
        select agreement_year current_agreement_year,
               mvid,
               vendor,
               plan_id,
               offer_name,
               purchases,
               total_accruals,
               system_accruals,
               manual_adjustments,
               round (
                    total_accruals
                  / case when purchases = 0 then 1 else purchases end
                  * 100,
                  2)
                  total_ror,
               round (
                    system_accruals
                  / case when purchases = 0 then 1 else purchases end
                  * 100,
                  2)
                  system_ror,
               round (
                    manual_adjustments
                  / case when purchases = 0 then 1 else purchases end
                  * 100,
                  2)
                  manual_ror
          from (select agreement_year,
                       mvid,
                       vendor,
                       plan_id,
                       offer_name,
                       (
                            select sum(selling_price*quantity) 
                            from   xxcus.xxcus_ozf_xla_accruals_b xla
                            where 1 =1
                            and xla.qp_list_header_id =plan_id  --FROM MAIN QRY                       
                            and calendar_year =agreement_year --FROM MAIN QRY
                            and xla.bill_to_party_id =2060 --ALWAYS 2060 FOR WC
                            and xla.ofu_mvid =mvid--FROM MAIN QRY
                            and xla.coop_yes_no ='N'           
                            ) purchases,
                       total_accruals,
                       system_accruals,
                       manual_adjustments
                  from (  select b.agreement_year,
                                 b.mvid,
                                 b.lob,
                                 c.party_name vendor,
                                 b.plan_id,
                                 qlhv.description offer_name,
                                 sum (accrual_amount) total_accruals,
                                 sum (
                                    case
                                       when b.ofu_attribute10 is null
                                       then
                                          b.accrual_amount
                                       else
                                          0
                                    end)
                                    system_accruals,
                                 sum (
                                    case
                                       when b.ofu_attribute10 is not null
                                       then
                                          b.accrual_amount
                                       else
                                          0
                                    end)
                                    manual_adjustments
                            from xxcus.xxcus_ytd_income_b b,
                                 xxcus.xxcus_rebate_customers c,
                                 ozf.ozf_offers oo,
                                 apps.qp_list_headers_vl qlhv
                           where     1 = 1
                                 and b.ofu_cust_account_id = c.customer_id
                                 and b.plan_id = oo.qp_list_header_id
                                 and b.lob_id = 2060
                                 and b.rebate_type = 'REBATE'
                                 --and b.agreement_year = to_number(to_char (sysdate, 'YYYY') -1) -- for non prod pull prev year..ALWAYS PULL FOR CURRENT YEAR AGREEMENTS ONLY --Ver 1.1
                                 and b.agreement_year = to_number(to_char (sysdate, 'YYYY')) --always pull for current year agreements only --ver 1.1
                                 and c.customer_attribute1 = 'HDS_MVID'
                                 and oo.status_code ='ACTIVE'
                                 and oo.qp_list_header_id  = qlhv.list_header_id 
                        group by b.agreement_year,
                                 b.mvid,
                                 c.party_name,
                                 b.plan_id,
                                 b.lob,
                                 qlhv.description) d);      
      --
  -- End variables
  begin
      --
       begin
        --
        execute immediate 'ALTER INDEX XXCUS.XXCUS_OZF_XLA_ACCRUALS_B_N19 VISIBLE';
        --
         b_proceed :=TRUE;
        --
         print_log('Alter Index...index xxcus.xxcus_ozf_xla_accruals_b_n19 is visible now ');        
        --
       exception
        when others then
         --
         b_proceed :=FALSE;
         --        
         print_log('Alter Index...trying to make  xxcus.xxcus_ozf_xla_accruals_b_n19 VISIBLE, error =>'||sqlerrm);
       end;
       -- 
       --
       --clean up temp table
       --
       if (b_proceed) then
            --
            begin
              --
              delete  xxcus.xxcus_ozf_wc_ror_b_intf;
              --  
              commit;    
              --
              b_proceed :=TRUE;
              --                      
            exception
             when others then
              print_log('Failed to delete staging table records from xxcus.xxcus_ozf_wc_ror_b_intf, message: '||sqlerrm);
              --
              b_proceed :=FALSE;
              --                
            end;
            --            
       end if;
       --
       -- Begin extract
       if (b_proceed) then
            begin
                 --
                 print_log('before opening cursor c_ror');        
                --                
                open c_ror;
                --
                    loop
                     --
                     fetch  c_ror
                      into get_wc_ror_set_row.current_agreement_year
                        ,get_wc_ror_set_row.mvid
                        ,get_wc_ror_set_row.vendor
                        ,get_wc_ror_set_row.plan_id
                        ,get_wc_ror_set_row.offer_name
                        ,get_wc_ror_set_row.purchases
                        ,get_wc_ror_set_row.total_accruals
                        ,get_wc_ror_set_row.system_accruals
                        ,get_wc_ror_set_row.manual_adjustments
                        ,get_wc_ror_set_row.total_ror
                        ,get_wc_ror_set_row.system_ror
                        ,get_wc_ror_set_row.manual_ror;              
                         --
                         exit when c_ror%notfound;
                     --
                       insert into xxcus.xxcus_ozf_wc_ror_b_intf 
                                   (
                                         current_agreement_year
                                        ,mvid
                                        ,vendor
                                        ,plan_id
                                        ,offer_name
                                        ,purchases
                                        ,total_accruals
                                        ,system_accruals
                                        ,manual_adjustments
                                        ,total_ror
                                        ,system_ror
                                        ,manual_ror
                                        ,created_by
                                        ,creation_date                           
                                   )
                        values
                                   ( 
                                         get_wc_ror_set_row.current_agreement_year
                                        ,get_wc_ror_set_row.mvid
                                        ,get_wc_ror_set_row.vendor
                                        ,get_wc_ror_set_row.plan_id
                                        ,get_wc_ror_set_row.offer_name
                                        ,get_wc_ror_set_row.purchases
                                        ,get_wc_ror_set_row.total_accruals
                                        ,get_wc_ror_set_row.system_accruals
                                        ,get_wc_ror_set_row.manual_adjustments
                                        ,get_wc_ror_set_row.total_ror
                                        ,get_wc_ror_set_row.system_ror
                                        ,get_wc_ror_set_row.manual_ror
                                        ,n_user_id
                                        ,sysdate                                 
                                   );
                     --
                            n_count :=n_count + sql%rowcount;
                     --
                    end loop;
                close c_ror;
                --
                 print_log('after opening cursor c_ror....total records inserted =>'||n_count);
                --
                commit;                           
                --
                b_proceed :=TRUE;
                --
            exception
             when others then
              print_log('Extract is not complete, error :'||sqlerrm);
              b_proceed :=FALSE;
            end;
            --             
       end if;
       --       
       -- End extract
       --
       if (b_proceed) then
            --
           begin
            --
            savepoint start_here;
            --
            delete xxcus.xxcus_ozf_wc_ror_b;
            --
            insert into xxcus.xxcus_ozf_wc_ror_b (select * from xxcus.xxcus_ozf_wc_ror_b_intf);
            --
             print_log('Successfully refreshed WC Rebates ROR extract, total records :'||sql%rowcount);        
            --
            commit;
            --
           exception
            when others then
             --
             rollback to start_here;
             --
             print_log('WC Rebates ROR extract is incomplete. Old data is retained. Please check with Rebates IT Support.');        
             --             
           end;        
            --        
       end if;
       --       
       if (b_proceed) then
            --
           begin
            --
            execute immediate 'ALTER INDEX XXCUS.XXCUS_OZF_XLA_ACCRUALS_B_N19 INVISIBLE';
            --
             print_log('Alter Index...index xxcus.xxcus_ozf_xla_accruals_b_n19 is invisible now ');        
            --
           exception
            when others then
             --  
             print_log('Alter Index...failed to set  xxcus.xxcus_ozf_xla_accruals_b_n19 INVISIBLE, error =>'||sqlerrm);
             --
           end;        
            --        
       end if;
       --
  exception
   when others then
    print_log('Outer block, error =>'||sqlerrm);
  end extract_wc_ror;  
  --
end XXCUS_OZF_LOB_ROR_PKG;
/