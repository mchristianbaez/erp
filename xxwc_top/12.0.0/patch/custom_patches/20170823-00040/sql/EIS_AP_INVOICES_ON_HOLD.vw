/**************************************************************************************************************
  $Header XXEIS.EIS_AP_INVOICES_ON_HOLD.vw $
  Module Name: Payables
  PURPOSE: WC - Invoices on Hold Report
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0		 8/28/2017	 Siva 					Initial Version 
  1.1		 8/28/2017   Siva					TMS#20170823-00040 
**************************************************************************************************************/
CREATE OR REPLACE VIEW XXEIS.EIS_AP_INVOICES_ON_HOLD (LEDGER_NAME, OPERATING_UNIT, BUYER, LOCATION_CODE, HOLD_NAME, BATCH_NAME, SUPPLIER_NAME, INVOICE_NUM, INVOICE_CURRENCY_CODE, INVOICE_CREATION_DATE, PONUMBER, INVOICE_DATE, INVOICE_AMOUNT, AMOUNT_REMAINING, INVOICE_DESCRIPTION, HELD_BY, HOLD_DATE, HOLD_REASON, LIABILITY_CCID, LIABILITY_ACCOUNT, GL_SEGMENT2, DUE_DATE, DISCOUNT_DATE, SOB_NAME, ORG_ID, VENDOR_ID, INVOICE_ID, BATCH_ID, APB_BATCH_ID, INV_INVOICE_ID, APS_INVOICE_ID, F_CURRENCY_CODE, POV_VENDOR_ID, APS_PAYMENT_NUM, COPYRIGHT)
AS
  SELECT gl.NAME LEDGER_NAME,
    /* --  xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_ou_name (inv.org_id) operating_unit,*/
    hr.name operating_unit,
    /*--hre.full_name buyer,*/
    xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_buyer(aph.invoice_id) buyer,
    /*--hrl.location_code,*/
    --xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_location(pov.vendor_id) location_code,
    NVL(xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_location(pov.vendor_id),xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_shipto_location(aph.invoice_id)) location_code,
    alc.displayed_field hold_name,
    apb.batch_name,
    pov.vendor_name SUPPLIER_NAME,
    inv.invoice_num,
    inv.invoice_currency_code,
    TRUNC (inv.creation_date) invoice_creation_date,
    /*--  DECODE(ap_invoices_pkg.get_po_number (inv.invoice_id),'UNMATCHED', NULL, ap_invoices_pkg.get_po_number(inv.invoice_id)) ponumber,*/
    NVL(ap_invoices_pkg.get_po_number (inv.invoice_id),NULL) ponumber,
    TRUNC(inv.invoice_date) invoice_date,
    DECODE (inv.invoice_currency_code, gl.currency_code, inv.invoice_amount, inv.base_amount ) invoice_amount,
    DECODE (inv.invoice_currency_code, gl.currency_code, inv.invoice_amount, inv.base_amount ) - DECODE (inv.payment_currency_code, gl.currency_code, NVL (inv.amount_paid, 0) + NVL (discount_amount_taken, 0), DECODE (f.minimum_accountable_unit, NULL, ROUND (( (DECODE (inv.payment_cross_rate_type, 'EMU FIXED', 1 / inv.payment_cross_rate, inv.exchange_rate ) ) * NVL (inv.amount_paid, 0) ), f.PRECISION ), ROUND ( ( (DECODE (inv.payment_cross_rate_type, 'EMU FIXED', 1 / inv.payment_cross_rate, inv.exchange_rate ) ) * NVL (inv.amount_paid, 0) ) / f.minimum_accountable_unit ) * f.minimum_accountable_unit ) + DECODE (f.minimum_accountable_unit, NULL, ROUND (( (DECODE (inv.payment_cross_rate_type, 'EMU FIXED', 1 / inv.payment_cross_rate, inv.exchange_rate ) ) * NVL (inv.discount_amount_taken, 0) ), f.PRECISION ), ROUND ( ( (DECODE (inv.payment_cross_rate_type, 'EMU FIXED', 1 / inv.payment_cross_rate, inv.exchange_rate ) ) * NVL (inv.discount_amount_taken, 0) ) / f.minimum_accountable_unit )
                                                                                               * f.minimum_accountable_unit ) ) amount_remaining,
    INV.DESCRIPTION INVOICE_DESCRIPTION,
    --hre1.full_name held_by,
    --Prasad(case#10116)
    fu.user_name held_by,
    TRUNC (aph.hold_date) hold_date,
    aph.hold_reason hold_reason,
    gcc.code_combination_id liability_ccid,
    --GCC.CONCATENATED_SEGMENTS LIABILITY_ACCOUNT,
    xxeis.eis_ap_con_seg_pkg.get_con_seg(gcc.code_combination_id) LIABILITY_ACCOUNT,
    /*--HRL.LOCATION_ID,*/
    GCC.SEGMENT2 GL_SEGMENT2,
    APS.DUE_DATE,
    APS.discount_date,
    GL.NAME SOB_NAME,
    --unique key added for component joins
    INV.ORG_ID,
    POV.VENDOR_ID,
    INV.INVOICE_ID,
    APB.BATCH_ID,
    APB.BATCH_ID APB_BATCH_ID,
    INV.INVOICE_ID INV_INVOICE_ID,
    APS.INVOICE_ID APS_INVOICE_ID,
    F.CURRENCY_CODE F_CURRENCY_CODE,
    Pov.Vendor_Id Pov_Vendor_Id,
    APS.PAYMENT_NUM APS_PAYMENT_NUM ,
    'Copyright(c) 2001-'
    ||TO_CHAR(SYSDATE,'YYYY')
    ||' '
    ||'EiS Technologies Inc. All rights reserved.' Copyright
    --gl#accountff#start
    --gl#accountff#end
  FROM PO_VENDORS POV,
    AP_INVOICES INV,
    --AP_BATCHES APB,  --Commented for version 1.1
    AP_BATCHES_ALL APB, --Added for version 1.1
    AP_PAYMENT_SCHEDULES APS,
    AP_HOLDS APH,
    AP_LOOKUP_CODES ALC,
    GL_CODE_COMBINATIONS_KFV GCC,
    /*--PO_LINE_LOCATIONS_ALL     POL,
    --PO_HEADERS_ALL            POH,
    --HR_EMPLOYEES              HRE,*/
    --  HR_EMPLOYEES              HRE1,
    FND_USER FU,
    /*--HR_LOCATIONS_ALL          HRL,*/
    GL_LEDGERS GL,
    FND_CURRENCIES F,
    HR_OPERATING_UNITS HR
  WHERE pov.vendor_id                   = inv.vendor_id
  AND aph.invoice_id                    = inv.invoice_id
  AND APB.BATCH_ID(+)                   = INV.BATCH_ID
  AND aps.invoice_id(+)                 = inv.invoice_id
  AND aph.release_lookup_code          IS NULL
  AND ALC.LOOKUP_TYPE                   = 'HOLD CODE'
  AND alc.lookup_code                   = aph.hold_lookup_code
  AND inv.accts_pay_code_combination_id = gcc.code_combination_id
  AND GCC.GL_ACCOUNT_TYPE               = 'L'
    /*--AND aph.line_location_id    = pol.line_location_id(+)
    --and POL.PO_HEADER_ID        = POH.PO_HEADER_ID(+)
    --and POH.AGENT_ID            = HRE.EMPLOYEE_ID(+)*/
    ---AND aph.held_by             = hre1.employee_id(+)
  AND aph.held_by = FU.USER_ID --Prasad(case#10116)
    /*--AND poh.ship_to_location_id = hrl.location_id(+)*/
  AND INV.SET_OF_BOOKS_ID = gl.ledger_id
  AND gl.CURRENCY_CODE    = F.CURRENCY_CODE
  AND INV.ORG_ID          =HR.ORGANIZATION_ID(+)
  AND gl.ledger_id        = hr.set_of_books_id
  /*AND xxeis.eis_gl_security_pkg.validate_access (inv.set_of_books_id, inv.accts_pay_code_combination_id ) = 'TRUE'*/
  UNION
  SELECT gl.NAME ledger_name,
    /*--  xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_ou_name (inv.org_id) operating_unit,*/
    hr.name operating_unit,
    '' buyer,
    /* --  hre.full_name Buyer    ,*/
    '' location_code,
    /*--, hrl.location_code*/
    'Invoices With Scheduled Payments Hold' hold_name,
    /* --apb.hold_lookup_code hold_lookup_code,*/
    apb.batch_name,
    pov.vendor_name supplier_name,
    inv.invoice_num,
    inv.invoice_currency_code,
    TRUNC (inv.creation_date) invoice_creation_date,
    /*-- DECODE(ap_invoices_pkg.get_po_number (inv.invoice_id),'UNMATCHED', NULL, ap_invoices_pkg.get_po_number(inv.invoice_id)) ponumber,*/
    NVL(ap_invoices_pkg.get_po_number (inv.invoice_id),NULL) ponumber,
    TRUNC(inv.invoice_date) invoice_date,
    DECODE (inv.invoice_currency_code, gl.currency_code, inv.invoice_amount, inv.base_amount ) invoice_amount,
    DECODE (inv.invoice_currency_code, gl.currency_code, inv.invoice_amount, inv.base_amount ) - DECODE (inv.payment_currency_code, gl.currency_code, NVL (inv.amount_paid, 0) + NVL (discount_amount_taken, 0), DECODE (f.minimum_accountable_unit, NULL, ROUND (( (DECODE (inv.payment_cross_rate_type, 'EMU FIXED', 1 / inv.payment_cross_rate, inv.exchange_rate ) ) * NVL (inv.amount_paid, 0) ), f.PRECISION ), ROUND ( ( (DECODE (inv.payment_cross_rate_type, 'EMU FIXED', 1 / inv.payment_cross_rate, inv.exchange_rate ) ) * NVL (inv.amount_paid, 0) ) / f.minimum_accountable_unit ) * f.minimum_accountable_unit ) + DECODE (f.minimum_accountable_unit, NULL, ROUND (( (DECODE (inv.payment_cross_rate_type, 'EMU FIXED', 1 / inv.payment_cross_rate, inv.exchange_rate ) ) * NVL (inv.discount_amount_taken, 0) ), f.PRECISION ), ROUND ( ( (DECODE (inv.payment_cross_rate_type, 'EMU FIXED', 1 / inv.payment_cross_rate, inv.exchange_rate ) ) * NVL (inv.discount_amount_taken, 0) ) / f.minimum_accountable_unit )
                                                                                               * f.minimum_accountable_unit ) ) amount_remaining ,
    inv.description,
    '' held_by,
    NULL hold_date,
    NULL hold_reason,
    gcc.code_combination_id liability_ccid,
    -- gcc.concatenated_segments liability_account,
    xxeis.eis_ap_con_seg_pkg.get_con_seg(gcc.code_combination_id) LIABILITY_ACCOUNT,
    /*--NULL,*/
    gcc.segment2 gl_segment2,
    APS.DUE_DATE,
    APS.discount_date,
    GL.NAME SOB_NAME,
    --unique key added for component joins
    INV.ORG_ID,
    POV.VENDOR_ID,
    INV.INVOICE_ID,
    APB.BATCH_ID,
    APB.BATCH_ID APB_BATCH_ID,
    INV.INVOICE_ID INV_INVOICE_ID,
    APS.INVOICE_ID APS_INVOICE_ID,
    F.CURRENCY_CODE F_CURRENCY_CODE,
    POV.VENDOR_ID POV_VENDOR_ID,
    APS.PAYMENT_NUM APS_PAYMENT_NUM ,
    'Copyright(c) 2001-'
    ||TO_CHAR(SYSDATE,'YYYY')
    ||' '
    ||'EiS Technologies Inc. All rights reserved.' Copyright
    --gl#accountff#start
    --gl#accountff#end
  FROM PO_VENDORS POV ,
    ap_invoices inv,
    /*--ap_holds aph,*/
    --AP_BATCHES APB, --Commented for version 1.1
    AP_BATCHES_ALL APB, --Added for version 1.1
    ap_payment_schedules aps,
    gl_code_combinations_kfv gcc,
    gl_ledgers gl,
    fnd_currencies f,
    hr_operating_units hr
  WHERE pov.vendor_id                   = inv.vendor_id
  AND apb.batch_id(+)                   = inv.batch_id
  AND aps.invoice_id                    = inv.invoice_id
  AND aps.hold_flag                     = 'Y'
  AND inv.accts_pay_code_combination_id = gcc.code_combination_id
  AND gcc.gl_account_type               = 'L'
  AND inv.set_of_books_id               = gl.ledger_id
  AND gl.currency_code                  = f.currency_code
  AND INV.ORG_ID                        =HR.ORGANIZATION_ID(+)
  AND gl.ledger_id                      = hr.set_of_books_id
  /*AND xxeis.eis_gl_security_pkg.validate_access (inv.set_of_books_id, inv.accts_pay_code_combination_id ) = 'TRUE'*/
  UNION
  SELECT gl.NAME ledger_name,
    /*-- xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_ou_name (inv.org_id) operating_unit,*/
    hr.name operating_unit,
    /*--hre.full_name buyer,*/
    xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_buyer(aph.invoice_id) buyer,
    /*--hrl.location_code,*/
    --xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_location(pov.vendor_id) location_code,
    NVL(xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_location(pov.vendor_id),xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_shipto_location(aph.invoice_id)) location_code,
    'Invoices With Supplier Site Set to Hold all Payments' hold_name,
    apb.batch_name,
    pov.vendor_name supplier_name,
    inv.invoice_num,
    inv.invoice_currency_code,
    TRUNC (inv.creation_date) invoice_creation_date,
    NVL(ap_invoices_pkg.get_po_number (inv.invoice_id),NULL) ponumber,
    /*-- DECODE(ap_invoices_pkg.get_po_number (inv.invoice_id),'UNMATCHED', NULL, ap_invoices_pkg.get_po_number(inv.invoice_id)) ponumber,*/
    TRUNC(inv.invoice_date) invoice_date,
    DECODE (inv.invoice_currency_code, gl.currency_code, inv.invoice_amount, inv.base_amount ) invoice_amount,
    DECODE (inv.invoice_currency_code, gl.currency_code, inv.invoice_amount, inv.base_amount ) - DECODE (inv.payment_currency_code, gl.currency_code, NVL (inv.amount_paid, 0) + NVL (discount_amount_taken, 0), DECODE (f.minimum_accountable_unit, NULL, ROUND (( (DECODE (inv.payment_cross_rate_type, 'EMU FIXED', 1 / inv.payment_cross_rate, inv.exchange_rate ) ) * NVL (inv.amount_paid, 0) ), f.PRECISION ), ROUND ( ( (DECODE (inv.payment_cross_rate_type, 'EMU FIXED', 1 / inv.payment_cross_rate, inv.exchange_rate ) ) * NVL (inv.amount_paid, 0) ) / f.minimum_accountable_unit ) * f.minimum_accountable_unit ) + DECODE (f.minimum_accountable_unit, NULL, ROUND (( (DECODE (inv.payment_cross_rate_type, 'EMU FIXED', 1 / inv.payment_cross_rate, inv.exchange_rate ) ) * NVL (inv.discount_amount_taken, 0) ), f.PRECISION ), ROUND ( ( (DECODE (inv.payment_cross_rate_type, 'EMU FIXED', 1 / inv.payment_cross_rate, inv.exchange_rate ) ) * NVL (inv.discount_amount_taken, 0) ) / f.minimum_accountable_unit )
                                                                                               * f.minimum_accountable_unit ) ) amount_remaining,
    INV.DESCRIPTION,
    -- hre1.full_name held_by,
    --Prasad(case#10116)
    fu.user_name held_by,
    TRUNC (aph.hold_date) hold_date,
    aph.hold_reason hold_reason,
    gcc.code_combination_id liability_ccid,
    --gcc.concatenated_segments liability_account,
    xxeis.eis_ap_con_seg_pkg.get_con_seg(gcc.code_combination_id) LIABILITY_ACCOUNT,
    /*--HRL.LOCATION_ID,*/
    gcc.segment2 gl_segment2,
    APS.DUE_DATE,
    APS.discount_date,
    GL.NAME SOB_NAME,
    --unique key added for component joins
    INV.ORG_ID,
    POV.VENDOR_ID,
    INV.INVOICE_ID,
    APB.BATCH_ID,
    APB.BATCH_ID APB_BATCH_ID,
    INV.INVOICE_ID INV_INVOICE_ID,
    APS.INVOICE_ID APS_INVOICE_ID,
    F.CURRENCY_CODE F_CURRENCY_CODE,
    POV.VENDOR_ID POV_VENDOR_ID,
    APS.PAYMENT_NUM APS_PAYMENT_NUM ,
    'Copyright(c) 2001-'
    ||TO_CHAR(SYSDATE,'YYYY')
    ||' '
    ||'EiS Technologies Inc. All rights reserved.' Copyright
    --gl#accountff#start
    --gl#accountff#end
  FROM PO_VENDORS POV,
    PO_VENDOR_SITES POVS,
    AP_INVOICES INV,
    --AP_BATCHES APB, --Commented for version 1.1
	AP_BATCHES_ALL APB, --Added for version 1.1
    AP_PAYMENT_SCHEDULES APS,
    /*--PO_HEADERS_ALL            POH,
    --PO_LINE_LOCATIONS_ALL     POL,*/
    ap_holds aph,
    GL_CODE_COMBINATIONS_KFV GCC,
    /*--HR_EMPLOYEES              HRE,*/
    --HR_EMPLOYEES              HRE1,
    FND_USER FU,
    /*--HR_LOCATIONS_ALL          HRL,*/
    gl_ledgers gl,
    fnd_currencies f,
    hr_operating_units hr
  WHERE pov.vendor_id            = inv.vendor_id
  AND aph.invoice_id(+)          = inv.invoice_id
  AND povs.vendor_id             = pov.vendor_id
  AND povs.vendor_site_id        = inv.vendor_site_id
  AND apb.batch_id(+)            = inv.batch_id
  AND aps.invoice_id(+)          = inv.invoice_id
  AND povs.hold_all_payments_flag= 'Y'
  AND inv.cancelled_date        IS NULL
  AND inv.payment_status_flag   != 'Y'
  AND aph.release_lookup_code   IS NULL
    /*--AND aph.line_location_id        = pol.line_location_id(+)
    --AND pol.po_header_id           = poh.po_header_id(+)*/
  AND inv.accts_pay_code_combination_id = gcc.code_combination_id
  AND gcc.gl_account_type               = 'L'
    /*--AND poh.agent_id         = hre.employee_id(+)*/
    --and APH.HELD_BY          = HRE1.EMPLOYEE_ID(+)
  AND aph.held_by = FU.USER_ID --Prasad(case#10116)
    /*--AND poh.ship_to_location_id  = hrl.location_id(+)*/
  AND inv.set_of_books_id = gl.ledger_id
  AND gl.currency_code    = f.currency_code
  AND INV.ORG_ID          =HR.ORGANIZATION_ID(+)
  AND gl.ledger_id        = hr.set_of_books_id
/
