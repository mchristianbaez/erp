-- TMS#20161011-00087 - XXWC INV Items Inactivation Program error.
-- To send concurrent program alerts, insert data into concurrent 
--programs monitoring table.
SET DEFINE OFF;
INSERT INTO XXCUS.XXCUS_MONITOR_CP_B (CONCURRENT_PROGRAM_ID,
                                      EMAIL_WHEN_WARNING,
                                      EMAIL_WHEN_ERROR,
                                      USER_ID,
                                      CREATED_BY,
                                      CREATION_DATE,
                                      LAST_UPDATED_BY,
                                      LAST_UPDATE_DATE,
                                      WARNING_EMAIL_ADDRESS,
                                      ERROR_EMAIL_ADDRESS,
                                      OWNED_BY,
                                      APPLICATION_ID)
     VALUES (287410,
             'N',
             'Y',
             NULL,
             15985,
             SYSDATE,
             15985,
             SYSDATE,
             'WC-ITBSAAlerts-U1@HDSupply.com',
             'WC-ITBSAAlerts-U1@HDSupply.com',
             'SALES AND FULFILLMENT',
             20005);

COMMIT;
