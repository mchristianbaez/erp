  /****************************************************************************************************
  Table: XXWC_INV_MTL_TRANSACTIONS##
  Description: To maintain inventory Item to inactivate.
  HISTORY
  File Name: XXWC_INV_MTL_TRANSACTIONS_TBL.sql
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  1.0     11-Oct-2016        P.Vamshidhar    Initial version TMS#20161011-00087
                                             XXWC INV Items Inactivation Program error.
  *****************************************************************************************************/
CREATE TABLE XXWC.XXWC_INV_MTL_TRANSACTIONS##
(
  INVENTORY_ITEM_ID  NUMBER                     NOT NULL,
  ORGANIZATION_ID    NUMBER                     NOT NULL,
  TRANSACTION_DATE   DATE
)
/
