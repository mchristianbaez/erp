  /****************************************************************************************************
  Table: XXWC_PO_ON_ORD_QTY##
  Description: To maintain inventory Item to inactivate.
  HISTORY
  File Name: XXWC_PO_ON_ORD_QTY_TBL.sql
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  1.0     11-Oct-2016        P.Vamshidhar    Initial version TMS#20161011-00087
                                             XXWC INV Items Inactivation Program error.
  *****************************************************************************************************/
CREATE TABLE XXWC.XXWC_PO_ON_ORD_QTY##
(
  ORG_ID             NUMBER,
  INVENTORY_ITEM_ID  NUMBER,
  ORGANIZATION_ID    NUMBER,
  ON_ORD             NUMBER
)
/
