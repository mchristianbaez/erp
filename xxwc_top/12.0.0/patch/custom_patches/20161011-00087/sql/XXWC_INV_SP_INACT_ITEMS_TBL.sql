  /****************************************************************************************************
  Table: XXWC_INV_SP_INACT_ITEMS##
  Description: To maintain inventory Item to inactivate.
  HISTORY
  File Name: XXWC_INV_SP_INACT_ITEM_TBL.sql
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  1.0     11-Oct-2016        P.Vamshidhar    Initial version TMS#20161011-00087
                                             XXWC INV Items Inactivation Program error.
  *****************************************************************************************************/
CREATE TABLE XXWC.XXWC_INV_SP_INACT_ITEMS##
(
  INVENTORY_ITEM_ID           NUMBER            NOT NULL,
  ORGANIZATION_ID             NUMBER            NOT NULL,
  ORGANIZATION_CODE           VARCHAR2(3 BYTE),
  PART_NUMBER                 VARCHAR2(40 BYTE),
  CREATION_DATE               DATE              NOT NULL,
  LAST_UPDATE_DATE            DATE              NOT NULL,
  LAST_ACTIVITY_DATE          DATE,
  OH_EXISTS                   VARCHAR2(1 BYTE),
  SO_EXISTS                   VARCHAR2(1 BYTE),
  QUOTE_EXISTS                VARCHAR2(1 BYTE),
  PO_EXISTS                   VARCHAR2(1 BYTE),
  REQ_EXISTS                  VARCHAR2(1 BYTE),
  WEBSITE_EXISTS              VARCHAR2(1 BYTE),
  ONE_YEAR_EXISTS             VARCHAR2(1 BYTE),
  ACTIVE_CSP_EXISTS           VARCHAR2(1 BYTE),
  INACTIVE_CSP_EXISTS         VARCHAR2(1 BYTE),
  INVENTORY_ITEM_STATUS_CODE  VARCHAR2(10 BYTE) NOT NULL,
  ITEM_TYPE                   VARCHAR2(30 BYTE),
  PRIMARY_UOM_CODE            VARCHAR2(3 BYTE),
  BUYER_ID                    NUMBER(9),
  PROCESS_FLAG                CHAR(1 BYTE)
)
/
