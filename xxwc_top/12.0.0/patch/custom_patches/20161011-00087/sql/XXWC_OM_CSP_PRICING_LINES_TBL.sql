  /****************************************************************************************************
  Table: XXWC_OM_CSP_PRICING_LINES##
  Description: To maintain inventory Item to inactivate.
  HISTORY
  File Name: XXWC_OM_CSP_PRICING_LINES_TBL.sql
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  1.0     11-Oct-2016        P.Vamshidhar    Initial version TMS#20161011-00087
                                             XXWC INV Items Inactivation Program error.
  *****************************************************************************************************/
CREATE TABLE XXWC.XXWC_OM_CSP_PRICING_LINES##
(
  AGREEMENT_LINE_ID    NUMBER                   NOT NULL,
  AGREEMENT_ID         NUMBER                   NOT NULL,
  AGREEMENT_TYPE       VARCHAR2(30 BYTE)        NOT NULL,
  VENDOR_QUOTE_NUMBER  VARCHAR2(30 BYTE),
  PRODUCT_ATTRIBUTE    VARCHAR2(30 BYTE)        NOT NULL,
  PRODUCT_VALUE        VARCHAR2(50 BYTE)        NOT NULL,
  PRODUCT_DESCRIPTION  VARCHAR2(240 BYTE),
  START_DATE           DATE,
  END_DATE             DATE,
  MODIFIER_TYPE        VARCHAR2(30 BYTE)        NOT NULL,
  LINE_STATUS          VARCHAR2(30 BYTE)        NOT NULL,
  CREATION_DATE        DATE                     NOT NULL
)
/
