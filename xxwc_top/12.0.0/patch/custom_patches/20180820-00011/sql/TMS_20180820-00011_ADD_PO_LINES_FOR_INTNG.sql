/*************************************************************************
  $Header TMS_20180820-00011_ADD_PO_LINES_FOR_INTNG.sql $
  Module Name: TMS_20180820-00011_ADD_PO_LINES_FOR_INTNG

  PURPOSE: Data fix to add lines to POs for cancelled intangibles
  
  ----------------------------------------------------------------
  Add lines to POs for cancelled 
												intangibles
  ----------------------------------------------------------------

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        21-AUG-2018  Pattabhi Avula        TMS#20180820-00011 - Data fix 
                                                Add lines to POs for cancelled 
												intangibles

**************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
   l_user_id               NUMBER;
   l_resp_id               NUMBER;
   l_appl_id               NUMBER;

   v_process_code          VARCHAR2 (25);
   v_header_id             NUMBER;
   v_po_status             VARCHAR2 (240);
   v_po_line_num           NUMBER := 0;
   v_need_by_date          DATE;
   v_uom                   VARCHAR2 (25);
   v_vendor_prod_num       VARCHAR2 (25);

   v_line_cnt              NUMBER;
   l_qty                   NUMBER;
   l_agent_id              NUMBER;
   l_item_id               NUMBER;

   -- Params for FND_CONCURRENT.wait_for_request
   l_request_id            NUMBER;
   l_conc_req_phase        VARCHAR2 (80);
   l_conc_req_status       VARCHAR2 (80);
   l_conc_req_dev_phase    VARCHAR2 (80);
   l_conc_req_dev_status   VARCHAR2 (80);
   l_conc_req_message      VARCHAR2 (80);

   l_return_status         VARCHAR2 (1);
   l_count_errors          NUMBER;

   l_on_open_lines         NUMBER;
   l_on_closed_lines       NUMBER;
   l_result                NUMBER;
   l_api_errors            PO_API_ERRORS_REC_TYPE;
   l_po_num                VARCHAR2 (20);
   l_line_num              NUMBER;
   l_revision_num          NUMBER;
   l_release_num           NUMBER;
   l_shipment_num          NUMBER;
   l_org_id                NUMBER;
   l_original_qty          NUMBER;


   l_consigned_flag        VARCHAR2 (1);
   l_count_consignment     NUMBER;
   l_MessageLevel          VARCHAR2 (2);
   l_item_key              VARCHAR2 (100);

   CURSOR cur_lines_add
   IS
        SELECT pha.SHIP_TO_LOCATION,
               pha.segment1 original_po,
               pla.line_num original_po_line,
               pla.item_number,
               SUM (quantity_cancelled) original_po_quantity,
               pla.unit_price original_po_price,
               plla.drop_ship_flag,
               plla.note_to_receiver,
               pha.VENDOR_NAME,
               pha.STATUS,
               pha.agent_name,
               pha.closed_code po_closure_status,
               pla.closed_code po_line_closure_status,
               pha.cancel_flag po_cancel_status,
               pla.cancel_flag po_line_cancel_status,
               pla.CANCEL_REASON,
               pla.cancel_date,
               pla.item_id,
               pha.po_header_id,
               pla.po_line_id,
               plla.line_location_id,
               plla.need_by_date,
               pla.ITEM_DESCRIPTION,
               pla.UNIT_MEAS_LOOKUP_CODE,
               pla.VENDOR_PRODUCT_NUM,
               pha.agent_id
          FROM po_headers_v pha, po_lines_v pla, po_line_locations_v plla
         WHERE     1 = 1
               --AND pla.created_by=15985
               AND pla.cancelled_by = 62503
               AND pla.po_header_id = pha.po_header_id
               AND pla.po_line_id = plla.po_line_id
               AND plla.cancelled_by = 62503
               AND pha.DOC_TYPE_NAME = 'Standard Purchase Order'
               --AND pha.segment1='2856636'
               AND NVL (plla.drop_ship_flag, 'N') = 'N'
               AND pla.CANCEL_REASON = 'CANCEL LINE TMS 20180808-00080'
               /*AND pla.last_update_date BETWEEN TO_DATE (
                                                   '19-AUG-2018 00:00:00',
                                                   'DD-MON-YYYY HH24:MI:SS')
                                            AND TO_DATE (
                                                   '19-AUG-2018 23:59:59',
                                                   'DD-MON-YYYY HH24:MI:SS')*/
               AND NOT EXISTS
                      (SELECT 1
                         FROM po_lines_all
                        WHERE     item_id = pla.item_id
                              AND po_header_id = pha.po_header_id
                              AND creation_date > pla.cancel_date
                              AND CANCEL_REASON = 'CANCEL LINE TMS 20180808-00080'
                              AND NVL (cancel_flag, 'N') = 'Y')
               AND NOT EXISTS
                      (SELECT 1
                         FROM po_lines_all
                        WHERE     item_id = pla.item_id
                              AND po_header_id = pha.po_header_id
                              --AND cancel_date > pla.cancel_date
							  AND NVL(cancel_date,SYSDATE) > pla.cancel_date
                              AND NVL (cancel_flag, 'N') = 'N')
               AND pla.item_number IN ('BROKEN BOX',
                                       'RBTYAVG',
                                       'REPAIR PARTS',
                                       '434LUMPSALE',
                                       'GALVCHARGE',
                                       'DETAILING',
                                       '12115225D',
                                       'TOOL REPAIR',
                                       'RRMOBILE',
                                       'ENGINEERING1',
                                       'FUEL SURCHG',
                                       '912CRATECHG',
                                       'SETUP CHARGE',
                                       'LABORSAFETY',
                                       'REPAIR LABOR',
                                       'RESTOCKING',
                                       'MISC STR EXP',
                                       'DAMAGED-RENT',
                                       'SFTYTRAINING',
                                       '910VENDORDIS',
                                       'FABLABOR',
                                       'ENGINEERING3',
                                       'REVISION ENG2',
                                       'LABOR',
                                       'CUTCHARGE',
                                       '3459',
                                       'GIFTWEBCR40',
                                       'TRANSFRT',
                                       '3455',
                                       'EPOXY CHARGE',
                                       'Rental Charge',
                                       'DELIVERY CHARGE',
                                       'FREIGHTOUT',
                                       'TOOLING CHG',
                                       'REVISION ENG3',
                                       'REVISION ENG1',
                                       'CUTTING',
                                       'SHIPPING OUT',
                                       'RBTYSP',
                                       'RLABOR-RNTL',
                                       'LABORSMT2',
                                       'PRINT CHARGE',
                                       'RSHIPPING-IN',
                                       'RBTYBS',
                                       'PACK MTERLS',
                                       'SHIPPING',
                                       'LOGOFEE',
                                       'PALLET CHG',
                                       'STAMP TOOL',
                                       'INSTALL',
                                       'PO TAX',
                                       'BULKBAG DEPO',
                                       'DIESEL',
                                       'FUEL SRCHG',
                                       'FREIGHTDIRECT',
                                       'RRCLEANCHG',
                                       'COATING',
                                       'ENGINEERING',
                                       'DELIVERY CHG',
                                       'FREIGHT',
                                       'LABORSMT1',
                                       'WELDING',
                                       'PROCESS',
                                       'BULK CHARGE',
                                       'SHOP SUPP',
                                       'RBTYMIN',
                                       'STEEL DETAIL',
                                       'REVISION ENG',
                                       '912BLOWOUT',
                                       '999EPADISP',
                                       'ENGINEERING2',
                                       'LUMP CREDIT',
                                       '435REBARCAGE',
                                       'JANITOR SUPP',
                                       '718MISC',
                                       'OFFSITERP',
                                       'RSHPPING-OUT',
                                       'OFFSITERL',
                                       'FABLABORCD',
                                       'AY05',
                                       'RR580000',
                                       'RUSHFEE',
                                       'PLATING',
                                       'LABORSMT3',
                                       'THREADING',
                                       'REPAIR-RENT',
                                       'SHOPDRAW',
                                       'LABORGALV',
                                       'ESTIMATING',
                                       'TYING',
                                       'CONTBILL',
                                       'REPAIRS ME',
                                       'RLABOR-RNTLL',
                                       'MAXRLADJ',
                                       'LABOR10',
                                       'PIGGYBACKFEE',
                                       'SCREENCHARGE',
                                       'RRENGCHG',
                                       'RRENG',
                                       '912SRNKWRAP',
                                       '937SETUP2',
                                       'TRANSPORTFEE',
                                       'OUTSIDELABOR',
                                       'BENDING',
                                       'FREEGOODS',
                                       '121DAC2920',
                                       '999FAB',
                                       '74110',
                                       '121DAC2921',
                                       '121EDU120',
                                       '121EDU140',
                                       'MINDETAIL')
      GROUP BY pha.SHIP_TO_LOCATION,
               pha.segment1,
               pla.line_num,
               pla.item_number,
               pla.unit_price,
               plla.drop_ship_flag,
               plla.note_to_receiver,
               pha.VENDOR_NAME,
               pha.STATUS,
               pha.agent_name,
               pha.closed_code,
               pla.closed_code,
               pha.cancel_flag,
               pla.cancel_flag,
               pla.CANCEL_REASON,
               pla.cancel_date,
               pla.item_id,
               pha.po_header_id,
               pla.po_line_id,
               plla.line_location_id,
               plla.need_by_date,
               pla.ITEM_DESCRIPTION,
               pla.UNIT_MEAS_LOOKUP_CODE,
               pla.VENDOR_PRODUCT_NUM,
               pha.agent_id;
BEGIN
   DBMS_OUTPUT.ENABLE (buffer_size => NULL);

   SELECT user_id
     INTO l_user_id
     FROM fnd_user
    WHERE user_name = 'XXWC_INT_SUPPLYCHAIN';

   SELECT responsibility_id, application_id
     INTO l_resp_id, l_appl_id
     FROM fnd_responsibility_vl
    WHERE responsibility_name = 'HDS Purchasing Super User - WC';

   fnd_global.apps_initialize (l_user_id, l_resp_id, l_appl_id);

   mo_global.init ('PO');
   mo_global.set_policy_context ('S', 162);

   FOR I IN cur_lines_add
   LOOP
      BEGIN
         DELETE FROM po.po_headers_interface
               WHERE po_header_id = i.po_header_id;

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            --            FND_FILE.PUT_LINE (
            --               FND_FILE.LOG,
            --                  'Error/no data found while deleting records from po_headers_interface-'
            --               || SQLERRM);
           -- NULL;
		   DBMS_OUTPUT.put_line ('Error in PO header interface delete for po_header_id '|| i.po_header_id||' Error details are '|| SQLERRM);
      END;

      BEGIN
         DELETE FROM po.po_lines_interface
               WHERE po_header_id = i.po_header_id;

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
		   DBMS_OUTPUT.put_line ('Error in PO Line interface delete for po_header_id '|| i.po_header_id||' Error details are '|| SQLERRM);
      END;

      BEGIN
         DELETE FROM po.po_distributions_interface
               WHERE po_header_id = i.po_header_id;

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
		   DBMS_OUTPUT.put_line ('Error in PO Distributions interface delete for po_header_id '|| i.po_header_id||' Error details are '|| SQLERRM);
      END;

      BEGIN
         v_po_line_num := 0;

         SELECT MAX (line_num)
           INTO v_po_line_num
           FROM po_lines_all
          WHERE po_header_id = I.po_header_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_po_status := NULL;
			DBMS_OUTPUT.put_line ('Error in getting max line_id for po_header_id '|| i.po_header_id||' Error details are '|| SQLERRM);
      END;


      BEGIN
	  DBMS_OUTPUT.put_line ('Before Inserting into  PO headers interface for  po_header_id '|| i.po_header_id); 
         INSERT INTO po.po_headers_interface (interface_header_id,
                                              batch_id,
                                              process_code,
                                              action,
                                              org_id,
                                              document_type_code,
                                              document_num,
                                              vendor_id,
                                              vendor_site_id)
            SELECT po_header_id,
                   po_header_id,
                   'PENDING',
                   'UPDATE',
                   org_id,
                   type_lookup_code,
                   segment1,
                   vendor_id,
                   vendor_site_id
              FROM po_headers_all
             WHERE po_header_id = I.po_header_id;
			 DBMS_OUTPUT.put_line ('Record inserted into po header interface for po_header_id '|| i.po_header_id);
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('Error in header interface ' || SQLERRM);
      END;

      DBMS_OUTPUT.put_line ('After insert to po_headers_interface');

      BEGIN
         IF TRUNC (i.need_by_date) >= TRUNC (SYSDATE)
         THEN
            v_need_by_date := NULL;
            v_need_by_date := i.need_by_date;
         ELSE
            v_need_by_date := SYSDATE;
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_need_by_date := NULL;
			DBMS_OUTPUT.put_line ('Error in fetching need_by_date for po_header_id '|| i.po_header_id);
      END;


      -- Adding Lines
      v_po_line_num := v_po_line_num + 1;
      DBMS_OUTPUT.put_line ('Before Record inser into po lines interface for po_header_id '|| i.po_header_id);
      INSERT INTO po.po_lines_interface (interface_line_id,
                                         interface_header_id,
                                         process_code,
                                         action,
                                         line_num,
                                         shipment_num,
                                         line_type,
                                         item_id,
                                         item_description,
                                         unit_of_measure,
                                         quantity,
                                         unit_price,
                                         promised_date,
                                         need_by_date,
                                         vendor_product_num,
                                         consigned_flag)
           VALUES (po.po_lines_interface_s.NEXTVAL,
                   I.po_header_id,
                   'PENDING',
                   'ADD',
                   v_po_line_num,
                   1,
                   'Goods',
                   i.ITEM_ID,
                   i.ITEM_DESCRIPTION,
                   i.UNIT_MEAS_LOOKUP_CODE,
                   i.original_po_quantity,
                   i.original_po_price,
                   v_need_by_date,
                   v_need_by_date,
                   i.VENDOR_PRODUCT_NUM,
                   NULL);

      DBMS_OUTPUT.put_line ('After insert to po_lines_interface');

      INSERT INTO po.po_distributions_interface (interface_header_id,
                                                 interface_line_id,
                                                 interface_distribution_id,
                                                 distribution_num,
                                                 quantity_ordered)
           VALUES (I.po_header_id,
                   po.po_lines_interface_s.CURRVAL,
                   po.po_distributions_interface_s.NEXTVAL,
                   1,
                   i.original_po_quantity);

      DBMS_OUTPUT.put_line ('After inser to po_distributions_interface');

      COMMIT;

      DBMS_OUTPUT.put_line ('After insert to l_item_id: ' || l_item_id);

      -- Get PO Number, Release Number, Revision Number, Line Number, Shipment Number and Org ID
      l_po_num := NULL;
      l_line_num := NULL;
      l_revision_num := NULL;
      l_release_num := NULL;
      l_shipment_num := NULL;
      l_org_id := NULL;
      l_original_qty := NULL;
      DBMS_OUTPUT.put_line ('Before PO_PDOI_GRP.start_process');



      PO_PDOI_GRP.start_process (p_api_version                  => 1.0,
                                 p_init_msg_list                => 'T',
                                 p_validation_level             => 100,
                                 p_commit                       => 'T',
                                 x_return_status                => l_return_status,
                                 p_gather_intf_tbl_stat         => 'F',
                                 p_calling_module               => 'UNKNOWN', --PO_PDOI_CONSTANTS.g_CALL_MOD_CONCURRENT_PRGM,
                                 p_selected_batch_id            => I.po_header_id, --v_header_id,
                                 p_batch_size                   => 5000, --PO_PDOI_CONSTANTS.g_DEF_BATCH_SIZE,
                                 p_buyer_id                     => NULL,
                                 p_document_type                => 'STANDARD',
                                 p_document_subtype             => NULL,
                                 p_create_items                 => 'N',
                                 p_create_sourcing_rules_flag   => 'N',
                                 p_rel_gen_method               => NULL,
                                 p_sourcing_level               => NULL,
                                 p_sourcing_inv_org_id          => NULL,
                                 p_approved_status              => 'APPROVED',
                                 p_process_code                 => 'PENDING', --PO_PDOI_CONSTANTS.g_process_code_PENDING,
                                 p_interface_header_id          => NULL,
                                 p_org_id                       => 162,
                                 p_ga_flag                      => NULL);

      DBMS_OUTPUT.put_line ('After PO_PDOI_GRP.start_process ');
	  DBMS_OUTPUT.put_line ('After PO_PDOI_GRP.start_process l_return_status Return value '||l_return_status);

      IF (l_return_status <> 'S')
      THEN
         DBMS_OUTPUT.put_line(
            'PO Import completed with error please review interface table...');

         COMMIT;
      ELSE
         l_count_consignment := 0;

         SELECT I.po_header_id || '-' || TO_CHAR (po_wf_itemkey_s.NEXTVAL)
           INTO l_item_key
           FROM DUAL;
         DBMS_OUTPUT.put_line( 'After PO line add, calling workflow process for automatic approval ');
         po_reqapproval_init1.start_wf_process (
            ItemType                 => 'POAPPRV',
            ItemKey                  => l_item_key,
            WorkflowProcess          => 'POAPPRV_TOP',
            ActionOriginatedFrom     => 'PO_FORM',
            DocumentID               => I.po_header_id         -- po_header_id
                                                      ,
            DocumentNumber           => l_po_num      -- Purchase Order Number
                                                ,
            PreparerID               => i.agent_id        -- Buyer/Preparer_id
                                                  ,
            DocumentTypeCode         => 'PO',
            DocumentSubtype          => 'STANDARD',
            SubmitterAction          => 'APPROVE',
            forwardToID              => NULL,
            forwardFromID            => NULL,
            DefaultApprovalPathID    => NULL,
            Note                     => NULL,
            PrintFlag                => 'N',
            FaxFlag                  => 'N',
            FaxNumber                => NULL,
            EmailFlag                => 'N',
            EmailAddress             => NULL,
            CreateSourcingRule       => 'N',
            ReleaseGenMethod         => 'N',
            UpdateSourcingRule       => 'N',
            MassUpdateReleases       => 'N',
            RetroactivePriceChange   => 'N',
            OrgAssignChange          => 'N',
            CommunicatePriceChange   => 'N',
            p_Background_Flag        => 'N',
            p_Initiator              => NULL,
            p_xml_flag               => NULL,
            FpdsngFlag               => 'N',
            p_source_type_code       => NULL);

         DBMS_OUTPUT.put_line (
            'After  po_reqapproval_init1.start_wf_process ');
      END IF;

      COMMIT;
   END LOOP;
EXCEPTION
   WHEN OTHERS
   THEN
     -- NULL;
	 DBMS_OUTPUT.put_line (' Unexpected Error while adding PO line, Error details are '||SQLERRM);
END;
/