/***********************************************************************************************************************************************
   NAME:       TMS_20170301-00211_Datafix.sql
   PURPOSE:  To re-build index on mtl_system_items_b

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        3/02/2017   P.Vamshidhar     TMS#20170301-00211 - Using new SQL profile and rebuilding index for XXWC Org attributes - Purchasing.
************************************************************************************************************************************************/
ALTER INDEX INV.MTL_SYSTEM_ITEMS_B_N1 REBUILD PARALLEL 8;
/
ALTER INDEX INV.MTL_SYSTEM_ITEMS_B_N1 NOPARALLEL
/