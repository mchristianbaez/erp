CREATE OR REPLACE PACKAGE BODY APPS.XXWC_PO_HAZMAT_SHIP_PKG
AS
   /*************************************************************************
     $Header APPS.XXWC_PO_HAZMAT_SHIP_PKG.pkb $
     Module Name: APPS.XXWC_PO_HAZMAT_SHIP_PKG

     PURPOSE: This Package will display the Hazard item details which is
              used in po shipping goods
              
     TMS Task Id :   20140715-00037

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        07/30/2014  Pattabhi Avula        Initial Version
     1.1        11/04/2014  Gopi Damuluri         TMS# 20141001-00254 Multi Org Changes
	 1.2        07/07/2015  Pattabhi Avula	      TMS# 20150622-00125 Proper shipping Name
												  added in Query and in Template, Line num 
												  column removed from template.
     1.3        07/21/2015  Pattabhi Avula        TMS# 20150717-00194 - Fix needed for PO 
												  HAZMAT Shipping report  -- Added vendor_contact_id
												  condition to avoid the duplicate records in 
												  vendor minimum table
   **************************************************************************/
   g_program_status      BOOLEAN;
   g_retcode             VARCHAR2(1);
   g_distribution_list   fnd_user.email_address%TYPE
                            := 'HDSOracleDevelopers@hdsupply.com';
   
   
   PROCEDURE ship_details_report(                   
                        errbuff           OUT    VARCHAR2,
                        retcode           OUT    VARCHAR2 ,
                        p_po_number       IN     po_headers_all.segment1%TYPE                       
                                   )
   /*************************************************************************
     $Header ship_details_report $
     Module Name: ship_details_report

     PURPOSE: Procedure to display the hazard shipping po details
     
     ESMS Task Id :  20140715-00037  replace(NAME,'&','&'||'amp;')

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        07/30/2014  Pattabhi Avula        Initial Version
     1.2        07/07/2015  Pattabhi Avula	      TMS# 20150622-00125 Proper shipping Name
												  added in Query and in Template, Line num 
												  column removed from template.
     1.3        07/21/2015  Pattabhi Avula        TMS# 20150717-00194 - Fix needed for PO 
												  HAZMAT Shipping report  -- Added vendor_contact_id
												  condition to avoid the duplicate records in 
												  vendor minimum table
   **************************************************************************/
   IS
   l_sec                          VARCHAR2 (100);
   
    CURSOR cur_hrd_head_txns
      IS
        SELECT pha.segment1 po_number,
               TO_CHAR (pha.creation_date, 'MM/DD/YYYY') po_date,
               TO_CHAR (TO_DATE (pha.attribute1, 'YYYY/MM/DD HH24:MI:SS'),'MM/DD/YYYY') expected_date,
               ship_to.location_code location,
               ppf.full_name buyer,
               pha.revision_num revision,
               pha.revised_date revision_date,
               pha.po_header_id,
               REPLACE(TRANSLATE(sup.vendor_name,'<>";{}[]\|=_*&^%$#@!~`, ','<>";{}[]\|=_*&^%$#@!~`, '),'&','&'||'amp;') vendor_name,
               REPLACE(TRANSLATE(aps.address_line1,'<>";{}[]\|=_*&^%$#@!~`, ','<>";{}[]\|=_*&^%$#@!~`, '),'&','&'||'amp;') vend_addr_line1,
               (INITCAP(aps.city)||' '||aps.state||' '||DECODE(aps.country,'US','United States',aps.country)||' '||aps.zip) vend_addr_line2,
               aps.phone v_phone,
               ship_to.location_code shp_loc_code,
               REPLACE(TRANSLATE(ship_to.address_line_1,'<>";{}[]\|=_*&^%$#@!~`, ','<>";{}[]\|=_*&^%$#@!~`, '),'&','&'||'amp;') shp_addr_line1,
               (ship_to.town_or_city||' '||ship_to.region_2||' '||DECODE(ship_to.country,'US','United States',ship_to.country)||' '||ship_to.postal_code) shp_addr_line2,
               ship_to.telephone_number_1 shp_tel_num1,      
               bill_to.location_code blto_loc_code,
               REPLACE(TRANSLATE(bill_to.address_line_1,'<>";{}[]\|=_*&^%$#@!~`, ','<>";{}[]\|=_*&^%$#@!~`, '),'&','&'||'amp;') blto_addr_line1,
               (bill_to.town_or_city||' '||bill_to.region_2||' '||DECODE(bill_to.country,'US','United States',bill_to.country)||' '||bill_to.postal_code) blto_addr_line2,
               bill_to.telephone_number_1 blto_tel_num1,
               pha.ship_via_lookup_code,
               pha.freight_terms_lookup_code,
               (SELECT NVL(vendor_min_dollar,0) FROM xxwc_po_vendor_minimum WHERE vendor_id=pha.vendor_id AND organization_id=ship_to.inventory_organization_id AND nvl(vendor_contact_id,0)=nvl(pha.vendor_contact_id,0)) prepaid_amount, -- Ver# 1.3 TMS#20150717-00194
               (SELECT NVL(freight_min_dollar,0) FROM xxwc_po_vendor_minimum WHERE vendor_id=pha.vendor_id AND organization_id=ship_to.inventory_organization_id AND nvl(vendor_contact_id,0)=nvl(pha.vendor_contact_id,0)) prepaid_weight,  -- Ver# 1.3  TMS#20150717-00194
               pha.fob_lookup_code,
               REPLACE(TRANSLATE(apt.name,'<>";{}[]\|=_*&^%$#@!~`, ','<>";{}[]\|=_*&^%$#@!~`, '),'&','&'||'amp;') payment_terms 
        FROM   po_headers pha, -- Version# 1.1
               per_people_f ppf, 
               ap_suppliers sup,
               ap_supplier_sites aps, -- Version# 1.1
               hr_locations ship_to,  -- Version# 1.1
               hr_locations  bill_to, -- Version# 1.1
               ap_terms apt
        WHERE  pha.agent_id = ppf.person_id 
        AND    pha.vendor_id=sup.vendor_id 
        AND    pha.vendor_site_id=aps.vendor_site_id 
        AND    pha.ship_to_location_id=ship_to.location_id
        AND    pha.bill_to_location_id=bill_to.location_id
        AND    pha.terms_id=apt.term_id(+)
      --  and    pha.segment1 in ('652','7702')
      AND    pha.segment1=p_po_number;
       
    CURSOR cur_hrd_line_txns(p_header_id NUMBER)
      IS 
        SELECT    pla.line_num,
                  msi.segment1 Item,
                  REPLACE(TRANSLATE(msi.description,'<>";{}[]\|=_*&^%$#@!~`,','<>";{}[]\|=_*&^%$#@!~`,'),'&','&'||'amp;') msi_discription,
                  pla.unit_meas_lookup_code,
                  pla.quantity,
                  pla.vendor_product_num,
                  REPLACE(TRANSLATE(POUN.un_number,'<>";{}[]\|=_*&^%$#@!~`,','<>";{}[]\|=_*&^%$#@!~`,'),'&','&'||'amp;') un_number,
                  (REPLACE(TRANSLATE(POUN.description,'<>";{}[]\|=_*&^%$#@!~`,','<>";{}[]\|=_*&^%$#@!~`,'),'&','&'||'amp;')||' '||'/'||' '||REPLACE(TRANSLATE(phc.description,'<>";{}[]\|=_*&^%$#@!~`,','<>";{}[]\|=_*&^%$#@!~`,'),'&','&'||'amp;')) un_disc,  -- Version# 1.2
                  phc.hazard_class,
                  msi.attribute9 packing_group,
                  msi.unit_weight weight_Ea,
                  (pla.quantity * msi.unit_weight) weight_Ttl
        FROM      po_line_types_b pltb,
                  po_line_types_tl pltt, -- Version# 1.1
                  mtl_units_of_measure muom1,
                  mtl_units_of_measure muom2,
                  po_un_numbers_tl poun,
                  po_hazard_classes_tl phc,
                  po_lookup_codes polc1,
                  po_lookup_codes polc2,
                  mtl_system_items_b msi,
                  financials_system_params_all fsp,
                  po_lines pla -- Version# 1.1
        WHERE     pla.line_type_id = pltb.line_type_id(+)
        AND       pla.line_type_id = pltt.line_type_id(+)
        AND       pltt.LANGUAGE(+) = USERENV ('LANG')
        AND       msi.inventory_item_id(+) = pla.item_id
        AND       nvl (msi.organization_id, fsp.inventory_organization_id) = fsp.inventory_organization_id
        AND       muom1.unit_of_measure(+) = pla.unit_meas_lookup_code
        AND       muom2.unit_of_measure(+) = msi.primary_unit_of_measure
        AND       poun.un_number_id(+) = pla.un_number_id
        AND       poun.LANGUAGE(+) = USERENV ('LANG')
        AND       phc.hazard_class_id(+) = pla.hazard_class_id
        AND       phc.LANGUAGE(+) = USERENV ('LANG')
        AND       polc1.lookup_type(+) = 'PRICE TYPE'
        AND       polc1.lookup_code(+) = pla.price_type_lookup_code
        AND       polc2.lookup_type(+) = 'TRANSACTION REASON'
        AND       polc2.lookup_code(+) = pla.transaction_reason_code
        AND       fsp.org_id = pla.org_id
        AND       pla.po_header_id=p_header_id
        ORDER BY  line_num; 
               
    CURSOR cur_hrd_line_sum1_txns(p1_header_id NUMBER)
      IS
        SELECT    REPLACE(TRANSLATE(POUN.un_number,'<>";{}[]\|=_*&^%$#@!~`, ','<>";{}[]\|=_*&^%$#@!~`, '),'&','&'||'amp;') sum_un_num,
                  SUM(pla.quantity) sum_qty,
                  REPLACE(TRANSLATE(POUN.description,'<>";{}[]\|=_*&^%$#@!~`, ','<>";{}[]\|=_*&^%$#@!~`, '),'&','&'||'amp;') sum_un_disc,
                  phc.hazard_class sum_hrd_class, 
                  msi.attribute9 sum_pack_group,
                  NVL(SUM(pla.quantity * msi.unit_weight),0) sum_weight_Ttl                
        FROM      po_line_types_b pltb,
                  po_line_types_tl pltt,
                  mtl_units_of_measure muom1,
                  mtl_units_of_measure muom2,
                  po_un_numbers_tl poun,
                  po_hazard_classes_tl phc,
                  po_lookup_codes polc1,
                  po_lookup_codes polc2,
                  mtl_system_items_b msi,
                  financials_system_params_all fsp,
                  po_lines pla -- Version# 1.1
        WHERE     pla.line_type_id = pltb.line_type_id(+)
        AND       pla.line_type_id = pltt.line_type_id(+)
        and       pla.un_number_id IS NOT NULL
        AND       pltt.LANGUAGE(+) = USERENV ('LANG')
        AND       msi.inventory_item_id(+) = pla.item_id
        AND       NVL (msi.organization_id, fsp.inventory_organization_id) = fsp.inventory_organization_id
        AND       muom1.unit_of_measure(+) = pla.unit_meas_lookup_code
        AND       muom2.unit_of_measure(+) = msi.primary_unit_of_measure
        AND       poun.un_number_id(+) = pla.un_number_id
        AND       poun.LANGUAGE(+) = USERENV ('LANG')
        AND       phc.hazard_class_id(+) = pla.hazard_class_id
        AND       phc.LANGUAGE(+) = USERENV ('LANG')
        AND       polc1.lookup_type(+) = 'PRICE TYPE'
        AND       polc1.lookup_code(+) = pla.price_type_lookup_code
        AND       polc2.lookup_type(+) = 'TRANSACTION REASON'
        AND       polc2.lookup_code(+) = pla.transaction_reason_code
        AND       fsp.org_id = pla.org_id
        AND       pla.po_header_id=p1_header_id
        GROUP BY  POUN.un_number,
                  poun.description,
                  phc.hazard_class,
                  msi.attribute9;
      --  ORDER BY  pla.line_num;
      
    CURSOR cur_hrd_line_sum2_txns(p2_header_id NUMBER)
      IS  
        SELECT    REPLACE(TRANSLATE(POUN.un_number,'<>";{}[]\|=_*&^%$#@!~`, ','<>";{}[]\|=_*&^%$#@!~`, '),'&','&'||'amp;') sum2_un_num,
                  sum(pla.quantity) sum2_qty,
                  REPLACE(TRANSLATE(POUN.description,'<>";{}[]\|=_*&^%$#@!~`, ','<>";{}[]\|=_*&^%$#@!~`, '),'&','&'||'amp;') sum2_un_disc,
                  phc.hazard_class sum2_hrd_class, 
                  msi.attribute9 sum2_pack_group,
                  NVL(SUM(pla.quantity * msi.unit_weight),0) sum2_weight_Ttl                
        FROM      po_line_types_b pltb,
                  po_line_types_tl pltt,
                  mtl_units_of_measure muom1,
                  mtl_units_of_measure muom2,
                  po_un_numbers_tl poun,
                  po_hazard_classes_tl phc,
                  po_lookup_codes polc1,
                  po_lookup_codes polc2,
                  mtl_system_items_b msi,
                  financials_system_params_all fsp,
                  po_lines pla -- Version# 1.1
        WHERE     pla.line_type_id = pltb.line_type_id(+)
        AND       pla.line_type_id = pltt.line_type_id(+)
        and       pla.UN_NUMBER_ID IS NULL
        AND       pltt.LANGUAGE(+) = USERENV ('LANG')
        AND       msi.inventory_item_id(+) = pla.item_id
        AND       NVL (msi.organization_id, fsp.inventory_organization_id) = fsp.inventory_organization_id
        AND       muom1.unit_of_measure(+) = pla.unit_meas_lookup_code
        AND       muom2.unit_of_measure(+) = msi.primary_unit_of_measure
        AND       poun.un_number_id(+) = pla.un_number_id
        AND       poun.LANGUAGE(+) = USERENV ('LANG')
        AND       phc.hazard_class_id(+) = pla.hazard_class_id
        AND       phc.LANGUAGE(+) = USERENV ('LANG')
        AND       polc1.lookup_type(+) = 'PRICE TYPE'
        AND       polc1.lookup_code(+) = pla.price_type_lookup_code
        AND       polc2.lookup_type(+) = 'TRANSACTION REASON'
        AND       polc2.lookup_code(+) = pla.transaction_reason_code
        AND       fsp.org_id = pla.org_id
        AND       pla.po_header_id=p2_header_id
        GROUP BY  POUN.un_number,
                  poun.description,
                  phc.hazard_class,
                  msi.attribute9;
               
  BEGIN
    FND_FILE.PUT_LINE(FND_FILE.LOG,'-------------------------------------------');
    FND_FILE.PUT_LINE(FND_FILE.LOG,'| Starting Concurrent Program Execution....');
    FND_FILE.PUT_LINE(FND_FILE.LOG,'-------------------------------------------');
    
    FND_FILE.PUT_LINE(FND_FILE.LOG,'Arguments');
    FND_FILE.PUT_LINE(FND_FILE.LOG,'-----------------------------');
    FND_FILE.PUT_LINE(FND_FILE.LOG,'PO Num:'||p_po_number);
    FND_FILE.PUT_LINE(FND_FILE.LOG,'-----------------------------');
  
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<?xml version="1.0" encoding="WINDOWS-1252"?>');
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<ROOT>');
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<Header>');
    
     l_sec := 'Calling Header Information';  
    FOR rec_hrd_hdr_dtls IN cur_hrd_head_txns 
    LOOP  -- Main Loop Started
    --FND_FILE.PUT_LINE(FND_FILE.LOG,'Inside loop  - line -156');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<PO_NUM>' ||REC_HRD_HDR_DTLS.PO_NUMBER || '</PO_NUM>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<PO_DATE>' ||REC_HRD_HDR_DTLS.PO_DATE || '</PO_DATE>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<EXPC_DATE>' ||REC_HRD_HDR_DTLS.EXPECTED_DATE || '</EXPC_DATE>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<LOC>' ||REC_HRD_HDR_DTLS.LOCATION || '</LOC>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<BUYER>' ||REC_HRD_HDR_DTLS.BUYER || '</BUYER>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<REV>' ||REC_HRD_HDR_DTLS.REVISION || '</REV>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<REV_DATE>' ||REC_HRD_HDR_DTLS.REVISION_DATE || '</REV_DATE>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<PO_HEADER_ID>' ||REC_HRD_HDR_DTLS.PO_HEADER_ID || '</PO_HEADER_ID>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<V_NAME>' ||REC_HRD_HDR_DTLS.VENDOR_NAME || '</V_NAME>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<V_AD_L1>' ||REC_HRD_HDR_DTLS.VEND_ADDR_LINE1 || '</V_AD_L1>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<V_AD_L2>' ||REC_HRD_HDR_DTLS.VEND_ADDR_LINE2 || '</V_AD_L2>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<V_PHONE>' ||REC_HRD_HDR_DTLS.V_PHONE || '</V_PHONE>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<SHP_LOC_CODE>' ||REC_HRD_HDR_DTLS.SHP_LOC_CODE || '</SHP_LOC_CODE>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<SHP_AD_L1>' ||REC_HRD_HDR_DTLS.SHP_ADDR_LINE1 || '</SHP_AD_L1>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<SHP_AD_L2>' ||REC_HRD_HDR_DTLS.SHP_ADDR_LINE2 || '</SHP_AD_L2>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<SHP_TLNUM1>' ||REC_HRD_HDR_DTLS.SHP_TEL_NUM1 || '</SHP_TLNUM1>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<BTO_LCOD>' ||REC_HRD_HDR_DTLS.BLTO_LOC_CODE || '</BTO_LCOD>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<BTO_AD_L1>' ||REC_HRD_HDR_DTLS.BLTO_ADDR_LINE1 || '</BTO_AD_L1>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<BTO_AD_L2>' ||REC_HRD_HDR_DTLS.BLTO_ADDR_LINE2 || '</BTO_AD_L2>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<BTO_TEL>' ||REC_HRD_HDR_DTLS.BLTO_TEL_NUM1 || '</BTO_TEL>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<SH_V_LK_CD>' ||REC_HRD_HDR_DTLS.SHIP_VIA_LOOKUP_CODE || '</SH_V_LK_CD>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<FR_TR_LK_CD>' ||REC_HRD_HDR_DTLS.FREIGHT_TERMS_LOOKUP_CODE || '</FR_TR_LK_CD>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<PRE_AMT>' ||REC_HRD_HDR_DTLS.PREPAID_AMOUNT || '</PRE_AMT>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<PRE_WEGT>' ||REC_HRD_HDR_DTLS.PREPAID_WEIGHT || '</PRE_WEGT>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<FOBLK_CD>' ||REC_HRD_HDR_DTLS.FOB_LOOKUP_CODE || '</FOBLK_CD>');
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<PMT_TRM>' ||REC_HRD_HDR_DTLS.PAYMENT_TERMS || '</PMT_TRM>');
      
     -- FND_FILE.PUT_LINE(FND_FILE.LOG,'Before child1 loop - line -183');
        l_sec := 'Calling Line Information'; 
        FOR rec_hrd_line_dtls IN cur_hrd_line_txns(rec_hrd_hdr_dtls.po_header_id)
        LOOP
         FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<G_Detail>');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<LNE_NUM>' ||REC_HRD_LINE_DTLS.LINE_NUM || '</LNE_NUM>');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<ITEM>' ||REC_HRD_LINE_DTLS.ITEM || '</ITEM>');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<MSI_DISC>' ||REC_HRD_LINE_DTLS.MSI_DISCRIPTION || '</MSI_DISC>');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<UMLC>' ||REC_HRD_LINE_DTLS.UNIT_MEAS_LOOKUP_CODE || '</UMLC>');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<QUTY>' ||REC_HRD_LINE_DTLS.QUANTITY || '</QUTY>');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<V_PRD_NUM>' ||REC_HRD_LINE_DTLS.VENDOR_PRODUCT_NUM || '</V_PRD_NUM>');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<UN_NUM>' ||REC_HRD_LINE_DTLS.UN_NUMBER || '</UN_NUM>');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<UN_DISC>' ||REC_HRD_LINE_DTLS.UN_DISC || '</UN_DISC>');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<HZRD_CL>' ||REC_HRD_LINE_DTLS.HAZARD_CLASS || '</HZRD_CL>');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<PCKG_GRP>' ||REC_HRD_LINE_DTLS.PACKING_GROUP || '</PCKG_GRP>');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<WGHT_EA>' ||REC_HRD_LINE_DTLS.WEIGHT_EA || '</WGHT_EA>');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<WGHT_TTL>' ||REC_HRD_LINE_DTLS.WEIGHT_TTL || '</WGHT_TTL>');
         FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'</G_Detail>');
        END LOOP;
        
        --FND_FILE.PUT_LINE(FND_FILE.LOG,'After and Before child1 and 2 loop - line -203');
        l_sec := 'Calling Hazmat PO line details'; 
        FOR rec_hrd_line_sum1_dtls IN cur_hrd_line_sum1_txns(rec_hrd_hdr_dtls.po_header_id)
        LOOP
         FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<G_Sum_one>');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<SM_UN_NUM>' ||REC_HRD_LINE_SUM1_DTLS.SUM_UN_NUM || '</SM_UN_NUM>');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<SM_QTY>' ||REC_HRD_LINE_SUM1_DTLS.SUM_QTY || '</SM_QTY>');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<SM_UN_DISC>' ||REC_HRD_LINE_SUM1_DTLS.SUM_UN_DISC || '</SM_UN_DISC>');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<SM_HRD_CL>' ||REC_HRD_LINE_SUM1_DTLS.SUM_HRD_CLASS || '</SM_HRD_CL>');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<SM_PCK_GRP>' ||REC_HRD_LINE_SUM1_DTLS.SUM_PACK_GROUP || '</SM_PCK_GRP>');
          FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<SM_WGHT_TTL>' ||REC_HRD_LINE_SUM1_DTLS.SUM_WEIGHT_TTL || '</SM_WGHT_TTL>');
         FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'</G_Sum_one>');
        END LOOP; 
        
		l_sec := 'Calling PO line Sum Quantity and Sum Amount details'; 
        FOR rec_hrd_line_sum2_dtls IN cur_hrd_line_sum2_txns(rec_hrd_hdr_dtls.po_header_id)
        LOOP
         FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<G_Sum_two>');
           FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<SM2_UN_NUM>' ||REC_HRD_LINE_SUM2_DTLS.SUM2_UN_NUM || '</SM2_UN_NUM>');
           FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<SM2_QTY>' ||REC_HRD_LINE_SUM2_DTLS.SUM2_QTY || '</SM2_QTY>');
           FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<SM2_UN_DISC>' ||REC_HRD_LINE_SUM2_DTLS.SUM2_UN_DISC || '</SM2_UN_DISC>');
           FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<SM2_HRD_CL>' ||REC_HRD_LINE_SUM2_DTLS.SUM2_HRD_CLASS || '</SM2_HRD_CL>');
           FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<SM2_PCK_GRP>' ||REC_HRD_LINE_SUM2_DTLS.SUM2_PACK_GROUP || '</SM2_PCK_GRP>');
           FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'<SM2_WGHT_TTL>' ||REC_HRD_LINE_SUM2_DTLS.SUM2_WEIGHT_TTL || '</SM2_WGHT_TTL>');
         FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'</G_Sum_two>');
        END LOOP;
        
    END LOOP;  --Main Loop Ended  
    
  FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'</Header>');
  FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'</ROOT>');
  
  --FND_FILE.PUT_LINE(FND_FILE.LOG,'After xml tags closing - line -223');
  EXCEPTION
  WHEN OTHERS THEN
  fnd_file.put_line(fnd_file.log,'For Procedure error : '||SUBSTR(sqlerrm,1,200));
  g_retcode:= '2';
  retcode:= g_retcode;
  g_program_Status := FND_CONCURRENT.SET_COMPLETION_STATUS('ERROR','Please verify the log file for Error Details');  
  
  xxcus_error_pkg.xxcus_error_main_api (
                 p_called_from         => 'XXWC_PO_HAZMAT_SHIP_PKG.SHIP_DETAILS_REPORT',
                 p_calling             => l_sec,
                 p_request_id          => NULL,
                 p_ora_error_msg       => SUBSTR (
                                           ' Error_Stack...'
                                          || DBMS_UTILITY.format_error_stack ()
                                          || ' Error_Backtrace...'
                                          || DBMS_UTILITY.format_error_backtrace (),
                                          1,
                                          2000),
                 p_error_desc          => SUBSTR (sqlerrm,
                                                  1,
                                                  240),
                 p_distribution_list   => g_distribution_list,
                 p_module              => 'PO');
  END ship_details_report;
  
  PROCEDURE po_hazmat_ship_submit(p_po_num IN VARCHAR2
                                  --,p_req_id  OUT NUMBER
                                  )
    /*************************************************************************
       $Header po_hazmat_ship_submit $
       Module Name: po_hazmat_ship_submit
    
       PURPOSE: Procedure to display the hazard shipping po details in RTF
       
       TMS Task Id :  20140715-00037
    
       REVISIONS:
       Ver        Date        Author                Description
       ---------  ----------  ------------------    ----------------
       1.0        08/12/2014  Pattabhi Avula        Initial Version
       
     **************************************************************************/

   IS
     l_request_id          NUMBER;
     l_xml_layout          BOOLEAN;
     l_po_number           VARCHAR2(50);
	 l_sec                 VARCHAR2 (100);
    BEGIN
     l_sec := 'Global Variables Initializing'; 
     FND_GLOBAL.apps_initialize (FND_GLOBAL.user_id,
                                  FND_GLOBAL.resp_id,
                                  FND_GLOBAL.resp_appl_id,
                                 0
                                  );
     


      l_sec := 'XML Laout Calling from Procedure'; 
     l_xml_layout := FND_REQUEST.ADD_LAYOUT('XXWC',
                                            'XXWC_PO_HAZMAT_SHIP',
                                            'en',
                                            'US',
                                            'PDF'
                                            );
 
     l_sec := 'Calling the program for submission'; 
     l_po_number := p_po_num;
     l_request_id:=FND_REQUEST.SUBMIT_REQUEST('XXWC',
                                              'XXWC_PO_HAZMAT_SHIP',
                                              'XXWC PO HAZMAT SHIPPING',
                                              SYSDATE,
                                              FALSE,
                                              l_po_number
                                             );
    --    RETURN (l_request_id);
       COMMIT; 
       
    EXCEPTION
      WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log,'For Procedure error : '||SUBSTR(sqlerrm,1,200));
	  
	  xxcus_error_pkg.xxcus_error_main_api (
                 p_called_from         => 'XXWC_PO_HAZMAT_SHIP_PKG.PO_HAZMAT_SHIP_SUBMIT',
                 p_calling             => l_sec,
                 p_request_id          => NULL,
                 p_ora_error_msg       => SUBSTR (
                                           ' Error_Stack...'
                                          || DBMS_UTILITY.format_error_stack ()
                                          || ' Error_Backtrace...'
                                          || DBMS_UTILITY.format_error_backtrace (),
                                          1,
                                          2000),
                 p_error_desc          => SUBSTR (sqlerrm,
                                                  1,
                                                  240),
                 p_distribution_list   => g_distribution_list,
                 p_module              => 'PO');
    END po_hazmat_ship_submit;
END XXWC_PO_HAZMAT_SHIP_PKG;
/
