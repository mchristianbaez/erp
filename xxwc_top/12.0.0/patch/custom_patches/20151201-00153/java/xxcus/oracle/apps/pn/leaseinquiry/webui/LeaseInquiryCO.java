/*===========================================================================================================================+
 |   Company: HD SUPPLY                                                                                                      |
 |   All rights reserved.                                                                                                    |
 |===========================================================================================================================+
 |  HISTORY                                                                                                                  |
 |  =======                                                                                                                  |
 |                                                                                                                           |
 |  TICKET#              Date         Author                   Comments                                                      |
 |  =======              ==========   ======================== ============================================================= |
 |  TMS 20151201-00153   16-FEB-2016  Pankaj Yadav (L&T)       OPN SuperSearch fixes                                         |
 |                                                                                                                           | 
 +===========================================================================================================================*/
package xxcus.oracle.apps.pn.leaseinquiry.webui;

import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.OAViewObject;
import oracle.apps.fnd.framework.webui.*;
import oracle.apps.fnd.framework.webui.beans.OAImageBean;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;
import oracle.apps.fnd.framework.webui.beans.layout.OAQueryBean;
import oracle.apps.fnd.framework.webui.beans.message.OAMessageChoiceBean;
import oracle.apps.fnd.framework.webui.beans.table.OATableBean;
import oracle.cabo.ui.UIConstants;

public class LeaseInquiryCO extends OAControllerImpl
{

    public LeaseInquiryCO()
    {
    }

    public void processRequest(OAPageContext pageContext, OAWebBean webBean)
    {
        super.processRequest(pageContext, webBean);
        OAApplicationModule am = pageContext.getApplicationModule(webBean);
        am.invokeMethod("initLobRollups");
        OATableBean tableBean = (OATableBean)webBean.findChildRecursive("tableLeaseSummary");
        OAImageBean m = (OAImageBean)tableBean.findChildRecursive("RerDocs");
        OAImageBean n = (OAImageBean)tableBean.findChildRecursive("LocDocs");
        OADataBoundValueViewObject tip1 = new OADataBoundValueViewObject(m, "RerDocs");
        OADataBoundValueViewObject tip2 = new OADataBoundValueViewObject(n, "LocDocs");
        m.setAttributeValue(UIConstants.TARGET_FRAME_ATTR, "_blank");
        n.setAttributeValue(UIConstants.TARGET_FRAME_ATTR, "_blank");
        m.setAttributeValue(UIConstants.DESTINATION_ATTR, tip1);
        n.setAttributeValue(UIConstants.DESTINATION_ATTR, tip2);
        
    }

    public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)
    {
        super.processFormRequest(pageContext, webBean);
        pageContext.writeDiagnostics(this, "In IF PFR LeaseSummaryVO1",4);
        OAApplicationModule ampfr = pageContext.getApplicationModule(webBean);
        OAViewObject ResultsVO = (OAViewObject) ampfr.findViewObject("LeaseSummaryVO1");
        OAQueryBean queryBean = (OAQueryBean) webBean.findChildRecursive("QueryRN");
        String idGo = queryBean.getGoButtonName();
        String idClear = queryBean.getClearButtonName();
        OAMessageChoiceBean SearchResults =(OAMessageChoiceBean)webBean.findIndexedChildRecursive("SearchResults");
        String xxSearchValue = (String)SearchResults.getValue(pageContext);
      if (pageContext.getParameter(idGo)!= null) {
            pageContext.writeDiagnostics(this, "In IF PFR LeaseSummaryVO1-QueryIs : "+ResultsVO.getQuery(),4);
        if(" Primary RER-LOC".equalsIgnoreCase(xxSearchValue)){
        ResultsVO.setWhereClause("HDS_OPN_PRIMARY_LOC_FOR_RER ='Y'");   
            pageContext.writeDiagnostics(this, "In IF PFR LeaseSummaryVO1-IF",4);
        ResultsVO.executeQuery();
        }
        else{
            ResultsVO.setWhereClause("HDS_OPN_PRIMARY_LOC_FOR_RER in ('Y','N')");
            pageContext.writeDiagnostics(this, "In IF PFR LeaseSummaryVO1-ELSE",4);
            ResultsVO.executeQuery();
        }
            
        }
        if (pageContext.getParameter(idClear)!= null) {
              pageContext.writeDiagnostics(this, "In Clear PFR LeaseSummaryVO1-QueryIs : "+ResultsVO.getQuery(),4);        
          ResultsVO.setWhereClause("1=2");   
              pageContext.writeDiagnostics(this, "In Clear PFR LeaseSummaryVO1-IF",4);
          ResultsVO.executeQuery();
          }
        if("eventAddress".equalsIgnoreCase(pageContext.getParameter(EVENT_PARAM))){
       //   SearchResults.setSelectedIndex(2); 
            if (SearchResults != null) {
                  pageContext.writeDiagnostics(this, "In IF PFR eventAddress",4);
                 SearchResults.setSelectedValue("ALL RER-LOC");
            //   SearchResults.setDisabled(true);
              }
     
        }
             pageContext.writeDiagnostics(this, "In IF PFR LeaseSummaryVO1-Query2Is : "+ResultsVO.getQuery(),4);
    }

    public static final String RCS_ID = "$Header$";
    public static final boolean RCS_ID_RECORDED = VersionInfo.recordClassVersion("$Header$", "%packagename%");

}
