/*===========================================================================================================================+
 |   Company: HD SUPPLY                                                                                                      |
 |   All rights reserved.                                                                                                    |
 |===========================================================================================================================+
 |  HISTORY                                                                                                                  |
 |  =======                                                                                                                  |
 |                                                                                                                           |
 |  TICKET#              Date         Author                   Comments                                                      |
 |  =======              ==========   ======================== ============================================================= |
 |  TMS 20151201-00153   16-FEB-2016  Pankaj Yadav (L&T)       OPN SuperSearch fixes                                         |
 |                                                                                                                           | 
 +===========================================================================================================================*/
package xxcus.oracle.apps.pn.leaseinquiry.server;

import oracle.apps.fnd.framework.OAException;
import oracle.apps.fnd.framework.server.OAViewObjectImpl;
import oracle.jbo.domain.Number;
import xxcus.oracle.apps.pn.leaseinquiry.LeaseFinancialVO;

public class LeaseFinancialVOImpl
  extends OAViewObjectImpl implements LeaseFinancialVO
{
    /**This is the default constructor (do not remove)
     */
    public LeaseFinancialVOImpl() {
    }

    public void initQuery(String leaseId)
  {
    Number leaId = null;
    try
    {
      leaId = new Number(leaseId);
    }
    catch (Exception e)
    {
      throw new OAException("AK", "FWK_TBX_INVALID_EMP_NUMBER");
    }
    setWhereClause("LEASE_ID = :1");
    setWhereClauseParams(null);
    setWhereClauseParam(0, leaId);
    executeQuery();
  }
}
