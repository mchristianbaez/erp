/*===========================================================================================================================+
 |   Company: HD SUPPLY                                                                                                      |
 |   All rights reserved.                                                                                                    |
 |===========================================================================================================================+
 |  HISTORY                                                                                                                  |
 |  =======                                                                                                                  |
 |                                                                                                                           |
 |  TICKET#              Date         Author                   Comments                                                      |
 |  =======              ==========   ======================== ============================================================= |
 |  TMS 20151201-00153   16-FEB-2016  Pankaj Yadav (L&T)       OPN SuperSearch fixes                                         |
 |                                                                                                                           | 
 +===========================================================================================================================*/
package xxcus.oracle.apps.pn.poplist.server;

import oracle.apps.fnd.framework.server.OAViewObjectImpl;

public class StateProvinceVOImpl extends OAViewObjectImpl {
    /**This is the default constructor (do not remove)
     */
    public StateProvinceVOImpl() {
    }
}

/* Location:           C:\Users\10621818\Desktop\
 * Qualified Name:     xxcus.oracle.apps.pn.poplist.server.StateProvinceVOImpl
 * JD-Core Version:    0.6.0
 */