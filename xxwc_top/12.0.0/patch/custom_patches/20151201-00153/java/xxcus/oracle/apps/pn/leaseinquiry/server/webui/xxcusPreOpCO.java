/*===========================================================================================================================+
 |   Company: HD SUPPLY                                                                                                      |
 |   All rights reserved.                                                                                                    |
 |===========================================================================================================================+
 |  HISTORY                                                                                                                  |
 |  =======                                                                                                                  |
 |                                                                                                                           |
 |  TICKET#              Date         Author                   Comments                                                      |
 |  =======              ==========   ======================== ============================================================= |
 |  TMS 20151201-00153   16-FEB-2016  Pankaj Yadav (L&T)       OPN SuperSearch fixes                                         |
 |                                                                                                                           | 
 +===========================================================================================================================*/
package xxcus.oracle.apps.pn.leaseinquiry.server.webui;

import java.io.Serializable;

import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.webui.OAControllerImpl;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;

/**
 * Controller for ...
 */
public class xxcusPreOpCO extends OAControllerImpl
{
  public static final String RCS_ID="$Header$";
  public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion(RCS_ID, "%packagename%");

  /**
   * Layout and page setup logic for a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processRequest(pageContext, webBean);
      String leaseId = pageContext.getParameter("RerData");
       pageContext.writeDiagnostics(this, "In IF PFR leaseId : "+leaseId,4);
          OAApplicationModule am = pageContext.getApplicationModule(webBean);
          Serializable[] parameters = { leaseId };
          

          am.invokeMethod("initPremisesOperations", parameters);
  }

  /**
   * Procedure to handle form submissions for form elements in
   * a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)
  {
  
    //  String RerId = pageContext.getParameter("RerId");
     String leaseId = pageContext.getParameter("RerData");
      pageContext.writeDiagnostics(this, "In IF PFR leaseId : "+leaseId,4);
         OAApplicationModule am = pageContext.getApplicationModule(webBean);
         Serializable[] parameters = { leaseId };
         

         am.invokeMethod("initPremisesOperations", parameters);
      super.processFormRequest(pageContext, webBean); 
  }

}
