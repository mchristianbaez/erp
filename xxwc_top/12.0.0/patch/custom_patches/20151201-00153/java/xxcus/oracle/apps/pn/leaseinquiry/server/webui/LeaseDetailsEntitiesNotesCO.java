/*===========================================================================================================================+
 |   Company: HD SUPPLY                                                                                                      |
 |   All rights reserved.                                                                                                    |
 |===========================================================================================================================+
 |  HISTORY                                                                                                                  |
 |  =======                                                                                                                  |
 |                                                                                                                           |
 |  TICKET#              Date         Author                   Comments                                                      |
 |  =======              ==========   ======================== ============================================================= |
 |  TMS 20151201-00153   16-FEB-2016  Pankaj Yadav (L&T)       OPN SuperSearch fixes                                         |
 |                                                                                                                           | 
 +===========================================================================================================================*/
package xxcus.oracle.apps.pn.leaseinquiry.server.webui;

import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.fnd.framework.webui.OAControllerImpl;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;
import java.io.PrintStream;
import java.io.Serializable;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.webui.OAControllerImpl;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;


/**
 * Controller for ...
 */
public class LeaseDetailsEntitiesNotesCO extends OAControllerImpl
{
  public static final String RCS_ID="$Header$";
  public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion(RCS_ID, "%packagename%");

  /**
   * Layout and page setup logic for a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processRequest(pageContext, webBean);
      String leaseId = pageContext.getParameter("RerData");
         OAApplicationModule am = pageContext.getApplicationModule(webBean);
         Serializable[] parameters = { leaseId };
         

         am.invokeMethod("initLeaseNotes", parameters);
  }

  /**
   * Procedure to handle form submissions for form elements in
   * a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processFormRequest(pageContext, webBean);
      String leaseId = pageContext.getParameter("RerData");
         System.out.println("***Options");
         OAApplicationModule am = pageContext.getApplicationModule(webBean);
         Serializable[] parameters = { leaseId };
         

         am.invokeMethod("initLeaseNotes", parameters);
         System.out.println("Link pressed");

  }

}
