/*******************************************************************************************************************************************************
  $Header TMS20160302_00061_DROP_BACKUP_TABLES.sql $

  PURPOSE Drop backup tables. 

  REVISIONS
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        02-MAR-2016  Niraj Ranjan          TMS#20160302_00061 - Drop backup tables created for parent TMS#20160216-00084.      

*******************************************************************************************************************************************************/
SET serveroutput ON;

prompt Droping table xxmtl_system_items_222;
DROP TABLE xxwc.xxwc_mtl_system_items_222;
prompt Droping table xxmtl_system_items_not_222;
DROP TABLE xxwc.xxmtl_system_items_not_222;
/
