   /* ************************************************************************
    HD Supply
    All rights reserved.
    Ticket                                         Ver          Date                 Author                                 Description
    ----------------------                  ----------   ----------          ------------------------           -------------------------     
    TMS 20151008-00085        1.0          11/19/2015   Balaguru Seshadri            Add two new fields
   ************************************************************************ */ 
ALTER TABLE XXCUS.XXCUSOZF_REBATE_ADJUST_TBL ADD END_CUST_PARTY_ID NUMBER;
--
COMMENT ON TABLE XXCUS.XXCUSOZF_REBATE_ADJUST_TBL  IS 'TMS 20151008-00085';