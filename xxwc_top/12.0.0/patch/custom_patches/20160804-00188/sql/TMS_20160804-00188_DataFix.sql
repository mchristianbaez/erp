/*************************************************************************
  $Header TMS_20160804-00188.sql $
  Module Name: TMS_20160804-00188  Data Fix script for TMS# 20160804-00188

  PURPOSE: Data Fix script for TMS# 20160804-00188

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        30-SEP-2016  Gopi Damuluri         TMS#20160804-00188 
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160804-00188    , Before Update');

UPDATE oe_order_lines_all
  SET project_id = 1, --Drop Project Number
      subinventory = 'Drop'
where line_id IN (80680440, 80680483, 80680884, 80680972, 80680729, 80680791, 80680840);

   DBMS_OUTPUT.put_line ('TMS: 20160804-00188  Sales order lines updated (Expected:7): '|| SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160804-00188    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160804-00188 , Errors : ' || SQLERRM);
END;
/