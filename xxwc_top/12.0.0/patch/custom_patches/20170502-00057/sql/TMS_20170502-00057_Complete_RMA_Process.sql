
/*************************************************************************
  $Header TMS_20170502-00057_Complete_RMA_Process.sql $
  Module Name: TMS_20170502-00057  Data Script Fix to complete the RMA 
               process and Return Billing Charge Lines.
               for sales order # 22512502

  PURPOSE: Data Script Fix to complete the RMA 
           process and Return Billing Charge Lines

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        05-MAY-2017  Sundaramoorthy        TMS#20170502-00057 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

DECLARE
   CURSOR lines is
   SELECT line_id
     FROM apps.oe_order_lines_all
   WHERE line_id=84661948
    AND open_flag='Y'
    AND flow_status_code='AWAITING_RETURN'
    AND Nvl(shipped_quantity,0)=0;

   l_result      VARCHAR2(30);
   l_activity_id  NUMBER;
   l_file_val  varchar2(500);

 BEGIN
DBMS_OUTPUT.put_line ('TMS: 20170502-00057   , Before Loop');
 FOR i IN lines LOOP
    OE_Standard_WF.OEOL_SELECTOR
    (p_itemtype => 'OEOL'
    ,p_itemkey => TO_CHAR(i.line_id)
    ,p_actid => 12345
    ,p_funcmode => 'SET_CTX'
    ,p_result => l_result
    );
 DBMS_OUTPUT.put_line('Retrun value of OEOL_selector function is'||l_result);
 
  UPDATE apps.oe_order_lines_all
     SET ordered_quantity =376
   WHERE line_id = i.line_id;

  UPDATE apps.oe_order_lines_all
     SET shipped_quantity   = ordered_quantity,
	     fulfilled_quantity = ordered_quantity,
		 flow_status_code   = 'AWAITING_FULFILLMENT',
         last_update_date   = SYSDATE,
		 last_updated_by    = 16516164
   WHERE line_id=i.line_id
     AND flow_status_code='AWAITING_RETURN';

    wf_engine.handleError('OEOL', TO_CHAR(i.line_id),'RMA_WAIT_FOR_RECEIVING','SKIP','COMPLETE');

    wf_engine.handleError('OEOL', TO_CHAR(i.line_id),'RMA_WAIT_FOR_INSPECTION','SKIP','COMPLETE');

    END LOOP;
	DBMS_OUTPUT.put_line ('TMS:20170502-00057    , After Loop');

UPDATE apps.oe_order_lines_all
   SET UNIT_LIST_PRICE = 0,
       UNIT_LIST_PRICE_PER_PQTY = NULL,
       link_to_line_id = 83065967,
       RETURN_ATTRIBUTE1 = 50874125,
       return_attribute2 = 83065967
 WHERE line_id = 95202053;
 
COMMIT;

DBMS_OUTPUT.put_line ('TMS: 20170502-00057 - number of records updated: '|| SQL%ROWCOUNT);
EXCEPTION
WHEN OTHERS THEN
DBMS_OUTPUT.PUT_LINE(' Error occured '||SUBSTR(SQLERRM,1,250)); 

END;
/ 