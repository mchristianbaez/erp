begin 
update ea_apex.XXWC_MATRIX_TEST_LOAD_FIX set branch = lpad(branch,3,'0');
commit;
  for i in (select distinct attribute14, r.branch, t.NAME, (select a.organization_id 
from org_organization_definitions a
where /*substr(a.ORGANIZATION_NAME,1,3)*/a.ORGANIZATION_CODE = r.branch/*lpad(r.branch,3,'0')*/) org_id
              from QP_LIST_HEADERS_ALL               t,
                   ea_apex.XXWC_MATRIX_TEST_LOAD_FIX r  -- is the local table I created to store data from excel
             where t.name = r.modifier_name) loop 
    if i.org_id is not null then
      update xxwc.XXWC_OM_CONTRACT_PRICING_HDR set organization_id = i.org_id
      where agreement_id = i.attribute14;
      commit;
  end if;    
      end loop;
      end;
/