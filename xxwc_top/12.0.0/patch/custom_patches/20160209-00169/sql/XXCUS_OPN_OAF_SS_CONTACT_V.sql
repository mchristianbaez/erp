  /*
  ===========================================================================
    Module: Oracle Property Manager
    Type: HDS GSC custom view
    PURPOSE: Used by HDS OPN Property Manager -Oracle Application Form SuperSearch UI TAB "Contacts"
    HISTORY
  ==============================================================================================================================
      VERSION DATE                  AUTHOR(S)                 DESCRIPTION                       TICKET
      -------        -----------           ---------------                ------------------------------    ---------------                          
      1.0            30-Dec-2015   Balaguru Seshadri  Created.                                 TMS 20160209-00169
  */  
CREATE OR REPLACE FORCE VIEW APPS.XXCUS_OPN_OAF_SS_CONTACT_V AS
SELECT  CON_ASG.LEASE_ROLE CONTACT_ROLE
              ,CONT.FIRST_NAME||' '||CONT.LAST_NAME NAME_ATTN
              ,CON_ASG.COMPANY_NAME||' '||CON_ASG.CONCATENATED_ADDRESS ADDRESS
             ,CONT_PHONE.PHONE_NUMBER PHONE
             ,CONT_FAX.PHONE_NUMBER FAX
             ,CONT_MOBILE.PHONE_NUMBER MOBILE                                     
              ,CONT.EMAIL_ADDRESS CONTACT_EMAIL_ADDRESS
              ,CONT.PRIMARY_FLAG  CONTACT_PRIMARY_FLAG 
              ,CONT.STATUS CONTACT_STATUS
              ,CON_ASG.STATUS CONTACT_ROLE_STATUS              
             ,CONT_PHONE.STATUS CONTACT_PHONE_STATUS
             ,CONT_PHONE.PRIMARY_FLAG PHONE_PRIMARY_FLAG
             ,CONT_FAX.STATUS CONTACT_FAX_STATUS
             ,CONT_FAX.PRIMARY_FLAG FAX_PRIMARY_FLAG
             ,CONT_MOBILE.STATUS CONTACT_MOBILE_STATUS
             ,CONT_MOBILE.PRIMARY_FLAG MOBILE_PRIMARY_FLAG 
             ,CONT_OTHERS.STATUS CONTACT_OTHER_STATUS
             ,CONT_OTHERS.PRIMARY_FLAG OTHER_PRIMARY_FLAG
             ,CONT_OTHERS.PHONE_NUMBER CONTACT_OTHER_NUMBER
              ,CON_ASG.SITE_NAME
              ,CON_ASG.COMPANY_NAME
              ,CON_ASG.LEASE_ROLE_TYPE                           
              ,CONT.CONTACT_ID                     
              ,CON_ASG.LEASE_ID
              ,CON_ASG.LEASE_CHANGE_ID
             ,CON_ASG.COMPANY_ID
             ,CON_ASG.CONTACT_ASSIGNMENT_ID
             ,CON_ASG.COMPANY_SITE_ID  
             ,CASE
                WHEN CONT_PHONE.PHONE_TYPE ='GEN' THEN CONT_PHONE.PHONE_ID
                WHEN CONT_FAX.PHONE_TYPE ='FAX' THEN CONT_FAX.PHONE_ID                
                WHEN CONT_MOBILE.PHONE_TYPE ='MOB' THEN CONT_MOBILE.PHONE_ID                
                WHEN CONT_OTHERS.PHONE_TYPE ='MOB' THEN CONT_OTHERS.PHONE_ID                
              END PHONE_ID
FROM PN_LEASE_CONTACT_ASSIGN_V CON_ASG
            ,PN_CONTACTS_V CONT
            ,PN_PHONES_V CONT_PHONE
            ,PN_PHONES_V CONT_FAX
            ,PN_PHONES_V CONT_MOBILE
            ,PN_PHONES_V CONT_OTHERS                       
WHERE 1 =1
      AND CON_ASG.STATUS ='A'
      AND CONT.COMPANY_SITE_ID(+) =CON_ASG.COMPANY_SITE_ID
      --AND NVL(CONT.STATUS, 'Z') ='A' --Active contacts only
      AND CONT_PHONE.CONTACT_ID(+) =CONT.CONTACT_ID
      AND CONT_PHONE.PHONE_TYPE(+) ='GEN'
      --AND NVL(CONT_PHONE.STATUS, 'Z') ='A' --Active phone number contacts only
      AND CONT_FAX.CONTACT_ID(+) =CONT.CONTACT_ID
      AND CONT_FAX.PHONE_TYPE(+) ='FAX'
      --AND NVL(CONT_FAX.STATUS, 'Z') ='A' --Active fax number contacts only      
      AND CONT_MOBILE.CONTACT_ID(+) =CONT.CONTACT_ID
      AND CONT_MOBILE.PHONE_TYPE(+) ='MOB'
      --AND NVL(CONT_MOBILE.STATUS, 'Z') ='A' --Active mobile number contacts only      
      AND CONT_OTHERS.CONTACT_ID(+) =CONT.CONTACT_ID
      AND CONT_OTHERS.PHONE_TYPE(+) ='OTH';
      --AND NVL(CONT_OTHERS.STATUS, 'Z') ='A'; --Active mobile number contacts only
--
COMMENT ON TABLE APPS.XXCUS_OPN_OAF_SS_CONTACT_V IS  'TMS 20160209-00169 OR ESMS: 195667';
--      