  /*
  ===========================================================================
    Module: Oracle Property Manager
    Type: HDS GSC custom view
    PURPOSE: Used by HDS OPN Property Manager -Oracle Application Form SuperSearch UI TAB  "Maintenance and Repairs"
    HISTORY
  ==============================================================================================================================
      VERSION DATE                  AUTHOR(S)                 DESCRIPTION                       TICKET
      -------        -----------           ---------------                ------------------------------    ---------------                          
      1.0            30-Dec-2015   Balaguru Seshadri  Created.                                 TMS 20160209-00169    
  */  
CREATE OR REPLACE FORCE VIEW APPS.XXCUS_OPN_OAF_SS_MAINTREPAIR_V AS
SELECT A.SERVICE_TYPE SERVICE_TYPE
             ,A.RESPONSIBILITY_TYPE RESPONSIBILITY_TYPE
             ,A.OBLIGATION_REFERENCE OBLIGATION_REFERENCE
             ,CASE
               WHEN UPPER(A.RESPONSIBILITY_MAINT) ='T' then 'Tenant'
               WHEN (UPPER(A.RESPONSIBILITY_MAINT) ='LL' or UPPER(A.RESPONSIBILITY_MAINT) ='L') then 'LandLord'               
              END  MAINTENANCE_RESPONSIBILITY             
             ,A.FINANCIAL_RESP_PARTY FINANCIAL_RESPONSIBILITY
             ,REGEXP_REPLACE(A.OBLIGATION_COMMENTS, '[[:cntrl:]]', '') OBLIGATION_COMMENTS
             ,A.LEASE_ID
             ,A.LEASE_CHANGE_ID                                 
FROM APPS.PN_LANDLORD_SERVICES_V A
WHERE 1 =1;
--
COMMENT ON TABLE APPS.XXCUS_OPN_OAF_SS_MAINTREPAIR_V IS  'TMS 20160209-00169 OR ESMS: 195667';
--