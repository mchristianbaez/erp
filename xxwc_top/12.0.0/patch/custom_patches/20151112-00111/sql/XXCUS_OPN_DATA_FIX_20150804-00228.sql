/*
 TMS: 20150804-00228 [Parent]
 TMS: 20151112-00111 [Child]
 Date: 08/07/2015
 Notes:  HDS GSC -Oracle Property Manager -Replace documentum link with the new apex url either @ location [attribute6] or lease detail level [attribute8]
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 n_count number :=0;
 n_location_id number;
 n_le_id number;
 n_le_dtl_id number;
 l_err_msg varchar2(2000) :=Null;
 b_go boolean;
 --
BEGIN --Main Processing...
   --
   b_go :=TRUE;
   --
   n_loc :=101;
   --
  if (b_go) then 
   --
   n_loc :=102;
   --
   begin
    --
    savepoint start1;
    --
    update xxcus.xxcus_tmp_tms_2015080400228
    set dctm_url =trim(regexp_replace(trim(dctm_url), '[[:cntrl:]]', ' '))
          ,apex_url =trim(regexp_replace(trim(apex_url), '[[:cntrl:]]', ' '));
    --
    n_loc :=104;
    --
    commit;
    --
   exception
    when others then
     --
     n_loc :=105;
     --
     dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
     --
     rollback to start1;
     --     
   end;
   --
   begin 
    --
    n_loc :=106;
    --
     for rec in (select * from  xxcus.xxcus_tmp_tms_2015080400228) loop 
      --
      begin 
       --
        select location_id
        into n_location_id
        from apps.pn_locations_all
        where 1 =1
        and attribute6 =rec.dctm_url
		and rownum <2;		
        --and sysdate between active_start_date and active_end_date;
        --
             update apps.pn_locations_all
             set attribute6 =rec.apex_url
             where 1 =1
             and location_id =n_location_id;
             --
             if (sql%rowcount >0) then
                 update xxcus.xxcus_tmp_tms_2015080400228
                        set  loc_url_status ='OK-UPDATE SUCCESS', location_id =n_location_id
                 where 1 =1
                      and dctm_url =rec.dctm_url;             
             else
                 update xxcus.xxcus_tmp_tms_2015080400228
                        set  loc_url_status ='OK-UPDATE FAILED', location_id =n_location_id
                 where 1 =1
                      and dctm_url =rec.dctm_url;             
             end if;
        --
      exception
            when no_data_found then
                    update xxcus.xxcus_tmp_tms_2015080400228
                    set  loc_url_status ='ERROR'
                          ,error_msg ='@LOC: No Data Found'
                    where dctm_url =rec.dctm_url;
            when too_many_rows then       
                    update xxcus.xxcus_tmp_tms_2015080400228
                    set loc_url_status ='ERROR'
                          ,error_msg ='@LOC: More than 1 rec.'
                    where dctm_url =rec.dctm_url;  
            when others then
                    --
                    l_err_msg :=substr(sqlerrm, 1, 2000);
                    --
                    update xxcus.xxcus_tmp_tms_2015080400228
                    set error_msg ='@LOC: '||l_err_msg, rer_url_status ='ERROR'
                    where dctm_url =rec.dctm_url;      
      end;
      --
      n_loc :=107;
      --            
     end loop;
    --      
    --
    n_loc :=108;
    --
     for rec in (select * from  xxcus.xxcus_tmp_tms_2015080400228) loop 
      --
      begin 
       --
        select lease_id, lease_detail_id
        into n_le_id, n_le_dtl_id
        from apps.pn_lease_details_all
        where 1 =1
        and attribute8 =rec.dctm_url
		and rownum <2;
        --
             update apps.pn_lease_details_all
             set attribute8 =rec.apex_url
             where 1 =1
             and lease_detail_id =n_le_dtl_id;
             --        
             if (sql%rowcount >0) then
                 update xxcus.xxcus_tmp_tms_2015080400228
                         set  rer_url_status ='OK-UPDATE SUCCESS', lease_id =n_le_id, lease_detail_id =n_le_dtl_id
                where dctm_url =rec.dctm_url;           
             else
                 update xxcus.xxcus_tmp_tms_2015080400228
                             set  rer_url_status ='OK-UPDATE FAILED', lease_id =n_le_id, lease_detail_id =n_le_dtl_id
                    where dctm_url =rec.dctm_url;             
             end if;
       --
      exception
            when no_data_found then
                    update xxcus.xxcus_tmp_tms_2015080400228
                    set  rer_url_status ='ERROR'
                          ,error_msg ='@RER, No Data Found'
                    where dctm_url =rec.dctm_url;
            when too_many_rows then
                    update xxcus.xxcus_tmp_tms_2015080400228
                    set rer_url_status ='ERROR'
                          ,error_msg ='@RER, More than 1 rec.'
                    where dctm_url =rec.dctm_url;  
            when others then
                    --
                    l_err_msg :=substr(sqlerrm, 1, 2000);
                    --            
                    update xxcus.xxcus_tmp_tms_2015080400228
                    set rer_url_status ='ERROR'
                          ,error_msg ='@RER, '||l_err_msg
                    where dctm_url =rec.dctm_url;      
      end;
      --
      n_loc :=109;
      --            
     end loop;
    --       
   exception
    when others then
     dbms_output.put_line(('****** @@@@@@ , '||sqlerrm));                  
   end;
   --
  else
   --
   n_loc :=103;
   --
   dbms_output.put_line('@b_go =FALSE, n_loc ='||n_loc);
   --
  end if;  --  if (b_go) then 
 --
 commit;
 --
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line (
         'TMS: 20150804-00228, Errors =' || SQLERRM);
END;
/