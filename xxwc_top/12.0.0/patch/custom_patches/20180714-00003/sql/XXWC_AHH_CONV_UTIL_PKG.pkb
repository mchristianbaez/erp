CREATE OR REPLACE PACKAGE BODY xxwc_ahh_conv_util_pkg IS
  ---
  -- PROCEDURE: Cleanup_Tables, cleans New Line, Tab and Space characters.
  ---
  PROCEDURE cleanup_tables(errbuf     OUT VARCHAR2
                          ,retcode    OUT NUMBER
                          ,p_table1   IN VARCHAR2
                          ,p_table2   IN VARCHAR2
                          ,p_table3   IN VARCHAR2
                          ,p_table4   IN VARCHAR2
                          ,p_table5   IN VARCHAR2
                          ,p_valueset IN VARCHAR2) IS
  
    l_cnt       NUMBER := NULL;
    sql_stmt    VARCHAR2(1000) := NULL; -- chr(13)
    usql_stmt   VARCHAR2(1000) := NULL; -- chr(13)
    tsql_stmt   VARCHAR2(1000) := NULL; -- chr(10)
    tusql_stmt  VARCHAR2(1000) := NULL; -- chr(10)
    ssql_stmt   VARCHAR2(1000) := NULL; -- Trim
    susql_stmt  VARCHAR2(1000) := NULL; -- Trim
    l_tablename VARCHAR2(250) := 'XXX';
    l_sec       VARCHAR2(10);
  
  
    CURSOR c_get_table_columns_list IS
      SELECT DISTINCT owner
                     ,table_name
                     ,column_name
        FROM all_tab_columns
       WHERE owner || '.' || table_name IN (p_table1
                                           ,p_table2
                                           ,p_table3
                                           ,p_table4
                                           ,p_table5)
          OR owner || '.' || table_name IN
             (SELECT ffv.description
                FROM fnd_flex_value_sets vset
                    ,fnd_flex_values_vl  ffv
               WHERE vset.flex_value_set_id = ffv.flex_value_set_id
                 AND vset.flex_value_set_name =
                     nvl(p_valueset
                        ,'XXWC_AHH_CONV_CLNUP_TABLE_NAMES')
                    --AND owner = 'XXWC'
                    --and table_name = ffv.DESCRIPTION
                    --AND owner || '.' || table_name = ffv.description
                 AND ffv.enabled_flag = 'Y'
                 AND SYSDATE BETWEEN nvl(ffv.start_date_active
                                        ,SYSDATE) AND nvl(ffv.end_date_active
                                                         ,SYSDATE))
       ORDER BY table_name
               ,column_name;
  BEGIN
    --
    fnd_file.put_line(fnd_file.log
                     ,' ****** Cleanup Tables *******');
    fnd_file.put_line(fnd_file.log
                     ,rpad('-'
                          ,100
                          ,'-'));
  
    FOR i IN c_get_table_columns_list LOOP
      BEGIN
        l_sec := '20';
        --
        IF l_tablename <> i.owner || '.' || i.table_name THEN
          l_tablename := i.owner || '.' || i.table_name;
        
          fnd_file.put_line(fnd_file.log
                           ,rpad('---'
                                ,100
                                ,'-'));
          fnd_file.put_line(fnd_file.log
                           ,' Scanning through..' || i.owner || '.' || i.table_name);
        END IF;
      
      
        l_sec := '30';
        --
        -- Remove Chr(13) -- Start <<
        --
        sql_stmt := 'SELECT COUNT(1) FROM ' || i.owner || '.' || i.table_name || ' WHERE ' ||
                    i.column_name || ' LIKE ''%'' || CHR(13) || ''%''';
        --
        EXECUTE IMMEDIATE sql_stmt
          INTO l_cnt;
        --
        l_sec := '40';
        IF l_cnt > 0 THEN
          l_sec := '10';
          --
          usql_stmt := 'UPDATE ' || i.owner || '.' || i.table_name || ' SET ' || i.column_name ||
                       ' = TRIM(REPLACE(' || i.column_name || ',CHR(13),''''))' || ' WHERE ' ||
                       i.column_name || ' LIKE ''%'' || CHR(13) || ''%''';
          --
          fnd_file.put_line(fnd_file.log
                           ,' ***    Updated chr(13), ' || l_cnt || ' records for ' || i.owner || '.' ||
                            i.table_name || ' - ' || i.column_name);
          --
          EXECUTE IMMEDIATE usql_stmt;
          --
          COMMIT;
          --
          l_sec := '50';
        END IF;
        --
        -- Remove Chr(13) -- End >>
        --
      
        --
        -- Remove Chr(10) -- Start <<
        --
        l_sec     := '60';
        tsql_stmt := 'SELECT COUNT(1) FROM ' || i.owner || '.' || i.table_name || ' WHERE ' ||
                     i.column_name || ' LIKE ''%'' || CHR(10) || ''%''';
        --
        l_sec := '70';
        EXECUTE IMMEDIATE tsql_stmt
          INTO l_cnt;
        --
        l_sec := '80';
        IF l_cnt > 0 THEN
          --
          l_sec      := '90';
          tusql_stmt := 'UPDATE ' || i.owner || '.' || i.table_name || ' SET ' || i.column_name ||
                        ' = TRIM(REPLACE(' || i.column_name || ',CHR(10),''''))' || ' WHERE ' ||
                        i.column_name || ' LIKE ''%'' || CHR(10) || ''%''';
          --
          fnd_file.put_line(fnd_file.log
                           ,' ***    Updated chr(10), ' || l_cnt || ' records for ' || i.owner || '.' ||
                            i.table_name || ' - ' || i.column_name);
          --
          EXECUTE IMMEDIATE tusql_stmt;
          --
          COMMIT;
          --
          l_sec := '100';
        END IF;
        --
        COMMIT;
      
        --
        -- Remove Chr(10) -- End >>
        --
      
        --
        -- Remove Leading and Trailing Spaces -- Start <<
        --
        l_sec     := '110';
        ssql_stmt := 'SELECT COUNT(1) FROM ' || i.owner || '.' || i.table_name ||
                     ' WHERE regexp_like(' || i.column_name || ',''(^ | $)'')';
        --
        l_sec := '120';
        EXECUTE IMMEDIATE ssql_stmt
          INTO l_cnt;
        --
        l_sec := '130';
        IF l_cnt > 0 THEN
          --
          l_sec      := '140';
          susql_stmt := 'UPDATE ' || i.owner || '.' || i.table_name || ' SET ' || i.column_name ||
                        ' = TRIM(' || i.column_name || ')' || ' WHERE regexp_like(' ||
                        i.column_name || ',''(^ | $)'')';
          --
          fnd_file.put_line(fnd_file.log
                           ,' ***    Updated TRIM, ' || l_cnt || ' records for ' || i.owner || '.' ||
                            i.table_name || ' - ' || i.column_name);
          --
          EXECUTE IMMEDIATE susql_stmt;
          --
          COMMIT;
          --
          l_sec := '150';
        END IF;
        --
        COMMIT;
        l_sec := '160';
      
        --
        -- Remove Leading and Trailing Spaces -- End >>
        --
      
      EXCEPTION
        WHEN OTHERS THEN
          errbuf  := SQLERRM;
          retcode := 1;
          fnd_file.put_line(fnd_file.log
                           ,' ***    ' || ': Error Occurred: ' || l_sec || '-' || SQLERRM);
        
      END;
    END LOOP;
    l_sec := '170';
  
    fnd_file.put_line(fnd_file.log
                     ,rpad('-'
                          ,100
                          ,'-'));
    fnd_file.put_line(fnd_file.log
                     ,' ****** Cleanup Tables *******');
    fnd_file.put_line(fnd_file.log
                     ,' ');
  
  EXCEPTION
    WHEN OTHERS THEN
      errbuf  := SQLERRM;
      retcode := 2;
      fnd_file.put_line(fnd_file.log
                       ,' Error: ' || l_sec || '-' || SQLERRM);
      fnd_file.put_line(fnd_file.log
                       ,' ****** Cleanup Tables ERROR *******');
    
  END cleanup_tables;
END xxwc_ahh_conv_util_pkg;
/
