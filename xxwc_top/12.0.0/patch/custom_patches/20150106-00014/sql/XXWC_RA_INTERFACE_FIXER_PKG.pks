CREATE OR REPLACE PACKAGE APPS.XXWC_RA_INTERFACE_FIXER_PKG
IS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_RA_INTERFACE_FIXER_PKG.pkb $
   *   Module Name: XXWC_RA_INTERFACE_FIXER_PKG.pkb
   *
   *   PURPOSE:    This package is a fixer package. And used to fix ra_interface_lines_error ERROR after Auto invoice.
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        05/03/2016  Neha Saini               Initial Version
   * ***************************************************************************/

   --Define variables for logging debug messages
   g_level_unexpected CONSTANT   NUMBER := 6;
   g_level_error CONSTANT        NUMBER := 5;
   g_level_exception CONSTANT    NUMBER := 4;
   g_LEVEL_EVENT CONSTANT        NUMBER := 3;
   g_LEVEL_PROCEDURE CONSTANT    NUMBER := 2;
   g_LEVEL_STATEMENT CONSTANT    NUMBER := 1;

   --Global variable to store the org id
   g_org_id                      NUMBER := fnd_profile.VALUE ('ORG_ID');

    /**************************************************************************
    *   procedure Name: check_invoice_errors
    *
    *   PURPOSE:   This procedure is used to fix ra_interface_lines_error ERROR after Auto invoice.
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
    *   1.0        05/03/2016  Neha Saini             Initial Version
    * ***************************************************************************/
   PROCEDURE check_invoice_errors (errbuf OUT VARCHAR2, retcode OUT VARCHAR2);
 
END XXWC_RA_INTERFACE_FIXER_PKG;
/
