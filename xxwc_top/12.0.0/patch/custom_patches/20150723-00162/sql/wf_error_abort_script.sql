SET SERVEROUTPUT ON SIZE 1000000

/* This script is a customized version of the script found on Oracle Support
in note: How to Purge WFERROR (System: Error) Workflow Items? (Doc ID 804622.1)

It will abort all error workflows w/o parents older than 30 days. This will commit
in batches of 1000, and will only process the first 2,000,000. This can be run repeatedly
until everything is finished.
*/

DECLARE
    theCount NUMBER := 0;
    
     CURSOR Ab_wf IS
     SELECT item_key 
        FROM wf_items
     WHERE item_type = 'WFERROR'
          AND end_date is null
          AND begin_date <= CURRENT_DATE - 7
          AND parent_item_type is null
		  AND ROWNUM <= 2000000
	ORDER BY begin_date asc;     
BEGIN
        dbms_output.put_line('Starting abort script.');
         FOR i IN Ab_wf LOOP
         theCount := theCount + 1;
               WF_ENGINE.abortProcess('WFERROR', i.item_key);
               
               IF MOD(theCount, 1000) = 0 THEN
                dbms_output.put_line('Processed a thousand rows. Committing. Current count: ' || theCount);
                COMMIT;
               END IF;
               
         END LOOP;
        dbms_output.put_line('Finished abort script. Final commit. Processed this many lines: ' || theCount);
COMMIT;
END; 
/
