/********************************************************************************
   $Header XXWC_B2B_STATS_VW.VW $
   Module Name: XXWC_B2B_STATS_VW

   PURPOSE:   View to derive B2B Statistics Information.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        08/09/2015  Gopi Damuluri           TMS# 20150615-00088
                                                  Initial Version
   1.1        11/04/2015  Gopi Damuluri           TMS# 20151104-00089 
                                                  Modify B2B Metrics to provide XML file counts and amounts
   1.2        11/10/2015  Gopi Damuluri           TMS# 20151110-00285 Resolve NPL Stats Issue
********************************************************************************/

CREATE OR REPLACE FORCE VIEW APPS.XXWC_B2B_STATS_VW
(
   PARTY_NAME,
   PARTY_NUMBER,
   ACCOUNT_NAME,
   ACCOUNT_NUMBER,
   TP_NAME,
   ORDER_NUMBER,
   ORDER_TYPE,
   ORDER_SOURCE,
   INVOICE_COUNT,
   INVOICE_AMOUNT,
   CREATION_DATE,
   SALE_AMOUNT
)
AS
   SELECT hp.party_name,
          hp.party_number,
          hca.account_name,
          hca.account_number,
          b2bc.tp_name,
          ooh.order_number,
          'STANDARD ORDER' Order_Type,
          ooh.order_Source_id ORDER_SOURCE,
          (SELECT COUNT (1)
             FROM ra_customer_trx_all rcta
            WHERE     rcta.interface_header_attribute1 =
                         TO_CHAR (ooh.order_number)
                  AND NVL (b2bc.deliver_invoice, 'N') = 'Y'
                  AND rcta.attribute10 IS NOT NULL)
             INVOICE_COUNT,
          NVL (
             (SELECT SUM (apsa.amount_due_original)
                FROM ra_customer_trx_all rcta, ar_payment_schedules_all apsa
               WHERE     rcta.interface_header_attribute1 =
                            TO_CHAR (ooh.order_number)
                     AND apsa.customer_trx_id = rcta.customer_trx_id
                     AND apsa.org_id = rcta.org_id
                     AND NVL (b2bc.deliver_invoice, 'N') = 'Y'
                     AND rcta.attribute10 IS NOT NULL),
             0)
             INVOICE_AMOUNT,
          ooh.creation_date,
          NVL (
             (SELECT SUM (
                        ROUND (
                             (ORDERED_QUANTITY * UNIT_SELLING_PRICE)
                           + TAX_VALUE,
                           2))
                FROM oe_order_lines_all ool
               WHERE ool.header_id = ooh.header_id),
             0)
             SALE_AMOUNT
     FROM oe_order_headers_all ooh,
          xxwc.xxwc_b2b_cust_info_tbl b2bc,
          hz_cust_accounts_all hca,
          hz_parties hp
    WHERE     1 = 1
          AND (DELIVER_SOA = 'Y' OR DELIVER_ASN = 'Y') -- Version# 1.2
          AND EXISTS
                 (SELECT '1'
                    FROM xxwc.xxwc_b2b_so_delivery_info_tbl b2bd
                   WHERE     ooh.header_id = b2bd.header_id
                         AND (   soa_delivered = '1'
                              OR asn_delivered IN ('1', '3')))
          AND ooh.sold_to_org_id = hca.cust_account_id
          AND hca.party_id = b2bc.party_id
          AND hca.party_id = hp.party_id
          AND ooh.order_type_id IN (1001, 1004)
          AND ooh.flow_status_code != 'CANCELLED'
          AND SYSDATE BETWEEN NVL (b2bc.start_date_active, SYSDATE - 10)
                          AND NVL (b2bc.end_date_active, SYSDATE + 10)
UNION
   SELECT hp.party_name,
          hp.party_number,
          hca.account_name,
          hca.account_number,
          b2bc.tp_name,
          ooh.order_number,
          'STANDARD ORDER' Order_Type,
          ooh.order_Source_id ORDER_SOURCE,
          (SELECT COUNT (1)
             FROM ra_customer_trx_all rcta
            WHERE     rcta.interface_header_attribute1 =
                         TO_CHAR (ooh.order_number)
                  AND NVL (b2bc.deliver_invoice, 'N') = 'Y'
                  AND rcta.attribute10 IS NOT NULL)
             INVOICE_COUNT,
          NVL (
             (SELECT SUM (apsa.amount_due_original)
                FROM ra_customer_trx_all rcta, ar_payment_schedules_all apsa
               WHERE     rcta.interface_header_attribute1 =
                            TO_CHAR (ooh.order_number)
                     AND apsa.customer_trx_id = rcta.customer_trx_id
                     AND apsa.org_id = rcta.org_id
                     AND NVL (b2bc.deliver_invoice, 'N') = 'Y'
                     AND rcta.attribute10 IS NOT NULL),
             0)
             INVOICE_AMOUNT,
          ooh.creation_date,
          NVL (
             (SELECT SUM (
                        ROUND (
                             (ORDERED_QUANTITY * UNIT_SELLING_PRICE)
                           + TAX_VALUE,
                           2))
                FROM oe_order_lines_all ool
               WHERE ool.header_id = ooh.header_id),
             0)
             SALE_AMOUNT
     FROM oe_order_headers_all ooh,
          xxwc.xxwc_b2b_cust_info_tbl b2bc,
          hz_cust_accounts_all hca,
          hz_parties hp
    WHERE     1 = 1
          AND (DELIVER_SOA = 'N' AND DELIVER_ASN = 'N')
          AND NOT EXISTS
                 (SELECT '1'
                    FROM xxwc.xxwc_b2b_so_delivery_info_tbl b2bd
                   WHERE     ooh.header_id = b2bd.header_id
                         AND (   soa_delivered = '1'
                              OR asn_delivered IN ('1', '3')))
          AND ooh.sold_to_org_id = hca.cust_account_id
          AND hca.party_id = b2bc.party_id
          AND hca.party_id = hp.party_id
          AND ooh.order_type_id IN (1001, 1004)
          AND ooh.flow_status_code != 'CANCELLED'
          AND SYSDATE BETWEEN NVL (b2bc.start_date_active, SYSDATE - 10)
                          AND NVL (b2bc.end_date_active, SYSDATE + 10)

/
