/*************************************************************************
  $Header TMS_20160524-00034_Data_Fix_to_Process_unprocessed_transactions.sql $
  Module Name: TMS_20160524-00034  Data Fix script for I678336

  PURPOSE: Data Fix script for I678336

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        24-MAY-2016  Raghav Velichetti         TMS#20160524-00034 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160524-00034    , Before Update');

UPDATE apps.wsh_delivery_details
SET released_status = 'D',
src_requested_quantity = 0,
requested_quantity = 0,
shipped_quantity = 0,
cycle_count_quantity = 0,
cancelled_quantity = 0,
subinventory = null,
locator_id = null,
lot_number = null,
revision = null,
inv_interfaced_flag = 'X',
oe_interfaced_flag = 'X'
WHERE delivery_detail_id =15798753;

--1 row expected to be updated

update apps.wsh_delivery_assignments
set delivery_id = null,
parent_delivery_detail_id = null
where delivery_detail_id = 15798753;

--1 row expected to be updated

update apps.oe_order_lines_all
set flow_status_code='CANCELLED',
cancelled_flag='Y'
where line_id=70313868
and headeR_id=42973819;

--1 row expected to be updated

   DBMS_OUTPUT.put_line (
         'TMS: 20160524-00034  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160524-00034    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160524-00034 , Errors : ' || SQLERRM);
END;
/