CREATE OR REPLACE PACKAGE APPS.XXWC_OM_RCT_ALLOCATION_PKG
AS
   /*************************************************************************
   *   $Header XXWC_OM_RCT_ALLOCATION_PKG.pks $
   *   Module Name: xxwc OM Automatic Receipt allocation package
   *
   *   PURPOSE:   Used in conc program to auto allocate on receipt of material
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        08/01/2013  Shankar Hariharan       Initial Version
   * ***************************************************************************/
   PROCEDURE print_pick_ticket (errbuf                    OUT VARCHAR2
                              , retcode                   OUT VARCHAR2);
--                              , i_shipment_header_id   IN     NUMBER);
END;
/
