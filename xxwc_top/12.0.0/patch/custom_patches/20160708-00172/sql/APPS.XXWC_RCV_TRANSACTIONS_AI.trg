CREATE OR REPLACE TRIGGER APPS.XXWC_RCV_TRANSACTIONS_AI
   /*************************************************************************************************************************************************
         NAME:       XXWC_RCV_SHIPMENT_HEADERS_AI

         PURPOSE:    To Print the White Cap Receipt Traveller report after receipt

         Logic:     1) Print the Traveller After a Record has been inserted into RCV_SHIPMENT_HEADERS
                    2) Print a lot label after has been inserted into RCV_LOT_NUMBERS

         REVISIONS:
         Ver        Date        Author           Description
         ---------  ----------  ---------------  ------------------------------------
         1.0        19-JUN-12  Lee Spitzer       1. Created this trigger.  This trigger replaces after insert trigger on RCV_SHIPMENT_HEADERS

         1.1        29-OCT-12  Lee Spitzer       2. Update logic to print 2 copies

         1.2        18-NOV-13  Consuelo Gonzalez 3. TMS 20131118-00061: Updated logic to print 1 copy instead of 2 after allocation implementation

         1.3        03-SEP-15  P.Vamshidhar      4. TMS#20150826-00110 - RF - Receipt Traveller Generation Issue

         *********************************************************************************************************************************************/
   AFTER INSERT
   ON PO.RCV_TRANSACTIONS
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
   WHEN (NEW.TRANSACTION_TYPE = 'DELIVER')
DECLARE
   X_REQUEST_ID           NUMBER;
   l_XML_Layout           BOOLEAN;
   l_applicable           VARCHAR2 (1);
   l_user_id              NUMBER;
   l_set_mode             BOOLEAN;
   l_label_printer        VARCHAR2 (30);
   l_lot_exists           NUMBER;

   l_xml_layout2          BOOLEAN;
   l_set_mode2            BOOLEAN;
   x_request_id2          NUMBER;
   l_exception            EXCEPTION;

   l_shipment_header_id   NUMBER;

   l_printer              VARCHAR2 (30);                    --added 10/29/2012
   l_set_print_options    BOOLEAN;                          --added 10/29/2012
   l_copies               NUMBER;                           --added 10/29/2012
BEGIN
   --Obtain the 'Y' or 'N' value FROM the profile option XXWC_RA_INTERFACE_LINES_TRIGGER
   l_applicable := fnd_profile.VALUE ('XXWC_PRINT_TRAVELLER');

   l_printer := fnd_profile.VALUE ('PRINTER');              --added 10/29/2012

   -- 11/18/2013 CG: TMS 20131118-00061: Updated from 2 copies to 1
   -- l_copies := 2; --added 10/29/2012
   l_copies := 1;                                           --added 10/29/2012

   --If the Profile Option is enabled then
   IF l_applicable = 'Y' AND NVL (:NEW.REASON_ID, 0) <> 54 -- Added by Vamshi in TMS#20150826-00110 V1.3
   THEN
      --Query RCV Shipment Header to see if we have processed the request yet for the traveller

      BEGIN
         SELECT shipment_header_id
           INTO l_shipment_header_id
           FROM rcv_shipment_headers
          WHERE     shipment_header_id = :NEW.shipment_header_id
                AND NVL (attribute1, 'N') = 'N';
      EXCEPTION
         WHEN OTHERS
         THEN
            RAISE l_exception;
      END;



      l_xml_layout :=
         FND_REQUEST.ADD_LAYOUT ('XXWC',
                                 'XXWC_INV_REC_TRAVL',
                                 'en',
                                 'US',
                                 'PDF');


      l_set_mode := FND_Request.Set_Mode (TRUE);


      --Added 10/29/2012
      l_set_print_options :=
         FND_Request.set_print_options (printer            => l_printer,
                                        style              => NULL,
                                        copies             => l_copies,
                                        save_output        => TRUE,
                                        print_together     => 'N',
                                        validate_printer   => 'RESOLVE');



      X_REQUEST_ID :=
         fnd_request.submit_request (
            APPLICATION   => 'XXWC',
            PROGRAM       => 'XXWC_INV_REC_TRAVL',
            DESCRIPTION   => 'XXWC INV Receipt Traveller Report',
            SUB_REQUEST   => FALSE,
            ARGUMENT1     => l_shipment_header_id);



      BEGIN
         --Update the RCV Shipment Header Attribute to flag that we have processed the record
         UPDATE rcv_shipment_headers
            SET attribute1 = 'Y'
          WHERE shipment_header_id = l_shipment_header_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            RAISE l_exception;
      END;
   END IF;
EXCEPTION
   WHEN l_exception
   THEN
      --exit and do nothing;
      NULL;
   WHEN OTHERS
   THEN
      NULL;
END;
/