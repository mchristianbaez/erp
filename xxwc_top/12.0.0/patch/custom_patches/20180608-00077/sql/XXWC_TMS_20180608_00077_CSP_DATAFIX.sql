/********************************************************************************
   FILE NAME: XXWC_TMS_20180608_00077_CSP_DATAFIX.sql
   PURPOSE: data fix

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     06/12/2018    Niraj K Ranjan    TMS#20180608-00077   CSP Agreement headers in Draft or awaiting approval status when lines are a
 ********************************************************************************/
 SET serveroutput on size 1000000;
 DECLARE
   CURSOR cr
   IS
     select distinct xx.agreement_id,xx.revision_number              
      from xxwc.xxwc_om_contract_pricing_hdr xx, xxwc.xxwc_om_contract_pricing_lines xxl
      where xx.agreement_Status in ('AWAITING_APPROVAL','DRAFT')
      and xx.agreement_id=xxl.agreement_id
      and xxl.end_Date is null
      and exists (select 1 from  xxwc.xxwc_om_contract_pricing_lines xl 
                  where xx.agreement_id=xl.agreement_id 
                  and LINE_STATUS='APPROVED' )
      and not exists (select 1 from  xxwc.xxwc_om_contract_pricing_lines xl 
                      where xx.agreement_id=xl.agreement_id 
                      and LINE_STATUS in ('AWAITING_APPROVAL','DRAFT'))
      order by xx.agreement_id;
 BEGIN
    dbms_output.put_line('TMS_20180608-00077 : '||'Start script'); 
    FOR rec IN cr
	LOOP
	   UPDATE xxwc.xxwc_om_contract_pricing_hdr
	   SET agreement_Status = 'APPROVED',
	       revision_number  = revision_number + 1
       WHERE agreement_id = rec.agreement_id;
	   
	   IF SQL%ROWCOUNT > 0 THEN
	      dbms_output.put_line('Updated csp header table for Agreement id:'||rec.agreement_id);
	   END IF;
	   
	   UPDATE xxwc.xxwc_om_csp_notifications_tbl
	   SET agreement_Status = 'APPROVED'
       WHERE agreement_id = rec.agreement_id
	   AND   revision_number = rec.revision_number
	   AND   agreement_Status NOT IN ('APPROVED','REJECTED');
	   
	   IF SQL%ROWCOUNT > 0 THEN
	      dbms_output.put_line('Updated Notification table for agreement id:'||rec.agreement_id);
	   END IF;
	END LOOP;
	COMMIT;
	dbms_output.put_line('TMS_20180608-00077 : '||'End script'); 
 EXCEPTION
    when others then
	   dbms_output.put_line('TMS_20180608-00077 Error: '||SQLERRM); 
	   ROLLBACK;
 END;
 /
 