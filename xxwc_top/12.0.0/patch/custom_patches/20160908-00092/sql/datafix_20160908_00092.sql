/************************************************************************************************************************************
  $Header datafix_20160908-00092.sql $
  Module Name: TMS_20160908-00092  Data Fix script.

  PURPOSE: To purge data from XXWC_INV_MIN_MAX_TEMP table

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------
  1.0        12-sep-2016  P.Vamshidhar          TMS#20160908-00092 - Implementing Purge Process for XXWC_INV_MIN_MAX_TEMP Table

***********************************************************************************************************************************/ 

SET SERVEROUTPUT ON SIZE 100000;

SET VERIFY OFF;

DECLARE
   ln_count   NUMBER := 0;
BEGIN
   DBMS_OUTPUT.put_line (
      'Before Delete ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

   EXECUTE IMMEDIATE
      'CREATE TABLE XXWC.XXWC_INV_MIN_MAX_TEMP_BKP_1209 PARALLEL (DEGREE 8) AS SELECT * FROM XXWC.XXWC_INV_MIN_MAX_TEMP';

   EXECUTE IMMEDIATE
      'ALTER TABLE XXWC.XXWC_INV_MIN_MAX_TEMP_BKP_1209 NOPARALLEL';

   EXECUTE IMMEDIATE
      'SELECT COUNT (1) FROM XXWC.XXWC_INV_MIN_MAX_TEMP_BKP_1209'
      INTO ln_count;

   IF ln_count > 1
   THEN
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_INV_MIN_MAX_TEMP';

      EXECUTE IMMEDIATE
         'INSERT /*+ APPEND */ INTO XXWC.XXWC_INV_MIN_MAX_TEMP (SELECT * FROM XXWC.XXWC_INV_MIN_MAX_TEMP_BKP_1209 WHERE TRUNC(CREATION_DATE)>TRUNC(SYSDATE)-8)';

      COMMIT;
   ELSE
      DBMS_OUTPUT.PUT_LINE ('Backup does not happend');
   END IF;

   DBMS_OUTPUT.put_line (
      'After Delete ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

EXCEPTION

   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (SUBSTR (SQLERRM, 1, 300));
END;
/











