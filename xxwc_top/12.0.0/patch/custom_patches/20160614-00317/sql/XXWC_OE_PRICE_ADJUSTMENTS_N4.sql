/********************************************************************************
   $Header XXWC_OE_PRICE_ADJUSTMENTS_N4.sql $
   Module Name: XXWC_OE_PRICE_ADJUSTMENTS_N4

   PURPOSE:   This index to improve performance when quering the data from OE_PRICE_ADJUSTMENTS table

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        06/14/2016  Rakesh Patel            TMS-20150122-00027/20160614-00317 Need to add a condition to the Price Type attribute
********************************************************************************/
CREATE INDEX APPS.XXWC_OE_PRICE_ADJUSTMENTS_N4 ON OE_PRICE_ADJUSTMENTS(TO_CHAR(line_id));