/*************************************************************************
$Header ALTER_XXWC_PRICING_SEGMENT_TBL.sql $
Module Name: ALTER_XXWC_PRICING_SEGMENT_TBL.sql

PURPOSE:   This Table is used to load Segment Modifiers to Oracle

REVISIONS:
Ver        Date        Author                  Description
---------  ----------  ---------------         -------------------------
1.1        11/23/2015  Gopi Damuluri           TMS# 20151123-00124
                                               Pricing QP Maintenance Tool changes - Modifier Header upload
****************************************************************************/

ALTER TABLE xxwc.xxwc_pricing_segment_tbl ADD (FILE_ID          NUMBER
                                             , CREATION_DATE    DATE
                                             , CREATED_BY       NUMBER
                                             , LAST_UPDATE_DATE DATE
                                             , LAST_UPDATED_BY  NUMBER
                                             , REQUEST_ID       NUMBER);
/