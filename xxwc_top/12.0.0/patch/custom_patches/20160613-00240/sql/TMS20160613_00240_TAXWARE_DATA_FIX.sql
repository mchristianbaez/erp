/****************************************************************************************************************************   
  Module Name TMS20160613-00240 - Tax:  Update Taxware Version Table

  PURPOSE Data fix to remove null record from custom table. 

  REVISIONS
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        14-Jun-2016  P.Vamshidhar          TMS20160613-00240 - Tax:  Update Taxware Version Table

****************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 100000

BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

   UPDATE TAXWARE.TAX_VERSION
      SET REL_NUM = '3.5.45', NOTES = 'Changed from 4.0.4 to 3.5.45 due to Oracle requirement'
    WHERE REL_NUM = '4.0.4';

   DBMS_OUTPUT.PUT_LINE ('Number of rows updated ' || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('After Update');
END;
/