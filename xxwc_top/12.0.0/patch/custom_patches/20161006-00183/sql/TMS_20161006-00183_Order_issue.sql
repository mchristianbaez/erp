/*************************************************************************
  $Header TMS_20161006-00183_Order_issue.sql $
  Module Name: TMS#20161006-00183 - Order # 21805450 -- Data fix Script 

  PURPOSE: Data fix to update the order status

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        10-OCT-2016 Pattabhi Avula         TMS#20161006-00183 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20161006-00183    , Before Update');

update apps.oe_order_lines_all
set FLOW_STATUS_CODE='CANCELLED',
CANCELLED_FLAG='Y'
where line_id = 78518505
and header_id= 48014578;
   DBMS_OUTPUT.put_line (
         'TMS: 20161006-00183  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20161006-00183    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161006-00183 , Errors : ' || SQLERRM);
END;
/