CREATE OR REPLACE PACKAGE BODY APPS.XXWC_WATCHDOG_PKG
AS
   /*************************************************************************

     **************************************************************************
       $Header xxwc_watchdog_pkg $
       Module Name: xxwc_watchdog_pkg.pks

       PURPOSE:   This package is called by the concurrent programs
                  XXWC Watchdog Monitor program

       REVISIONS:
       Ver        Date        Author             Description
       ---------  ----------  ---------------   -------------------------
        1.0       29/01/2016  Neha Saini         Initial Build - TMS# 20151019-00024

     /*************************************************************************



     /*************************************************************************
       Procedure : write_log

       PURPOSE   :This procedure is used to write log in concurrent program log file
       REVISIONS:
       Ver        Date        Author             Description
       ---------  ----------  ---------------   -------------------------
        1.0       29/01/2016  Neha Saini         Initial Build - TMS# 20151019-00024

     ************************************************************************/
     
   PROCEDURE write_log (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
   END write_log;

   /*************************************************************************
      Procedure : Write_Error

     PURPOSE:   This procedure logs error message
     Parameter:
            IN
                p_debug_msg      -- Debug Message
     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/01/2016  Neha Saini         Initial Build - TMS# 20151019-00024
   ************************************************************************/

   --add message to concurrent output file
   PROCEDURE write_error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'xxwc_watchdog_pkg';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running xxwc_watchdog_pkg with PROGRAM ERROR',
         p_distribution_list   => l_distro_list,
         p_module              => 'AR');
   END write_error;

   /*************************************************************************
      Procedure : send_email

     PURPOSE:   This procedure is used to send email
     Parameter:
            IN
                p_debug_msg      -- Debug Message
     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/01/2016  Neha Saini         Initial Build - TMS# 20151019-00024
   ************************************************************************/

   --add message to concurrent output file
   PROCEDURE send_email (p_distro_list   IN VARCHAR2,
                         p_sub           IN VARCHAR2,
                         p_message       IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_sid             VARCHAR2 (8);
      l_host            VARCHAR2 (256) := 'mailoutrelay.hdsupply.net';
      l_hostport        VARCHAR2 (20) := '25';
      l_sender          VARCHAR2 (500);
   BEGIN
      SELECT LOWER (NAME) INTO l_sid FROM v$database;
      
      l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';

      xxcus_misc_pkg.html_email (p_to              => p_distro_list,
                                 p_from            => l_sender,
                                 p_text            => 'XXWC AP Hold Workflow Notification Program has Error',
                                 p_subject         => p_sub||' Request ID: ' || l_req_id,
                                 p_html            => p_message || CHR (10),
                                 p_smtp_hostname   => l_host,
                                 p_smtp_portnum    => l_hostport);
   END send_email;

   /*************************************************************************
     Procedure : apin_workflow_watchdog

     PURPOSE   :This procedure is used to monitor XXWC AP Hold Workflow Notification Program
     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/01/2016  Neha Saini         Initial Build - TMS# 20151019-00024
   ************************************************************************/
   PROCEDURE apin_workflow_watchdog (errbuf                   OUT VARCHAR2,
                                     retcode                  OUT VARCHAR2,
                                     p_distribution_list   IN     VARCHAR2)
   IS
      CURSOR cur_prog_details
      IS
         SELECT user_concurrent_program_name,
                program_short_name,
                requestor user_name,
                concurrent_program_id,
                program_application_id,
                argument_text,
                responsibility_id,
                (SELECT meaning
                   FROM apps.fnd_lookup_values
                  WHERE     lookup_type = 'CP_PHASE_CODE'
                        AND language = 'US'
                        AND start_date_active IS NOT NULL
                        AND end_date_active IS NULL
                        AND enabled_flag = 'Y'
                        AND lookup_code = phase_code)
                   Phase_code,
                (SELECT meaning
                   FROM apps.fnd_lookup_values
                  WHERE     lookup_type = 'CP_STATUS_CODE'
                        AND language = 'US'
                        AND start_date_active IS NOT NULL
                        AND end_date_active IS NULL
                        AND enabled_flag = 'Y'
                        AND lookup_code = status_code)
                   Status_code,
                request_id,
                request_date,
                hold_flag,
                requested_start_date,
                DECODE (execution_method_code,
                        'I', 'PL/SQL',
                        'P', 'Reports',
                        'C', 'SQL Loader',
                        'Q', 'SQL Plus',
                        'K', 'Java',
                        'H', 'OS executable',
                        'B', 'Req. Set Stage',
                        'Unknown')
                   execution_method
           FROM apps.fnd_conc_req_summary_v
          WHERE     1 = 1
                --             AND phase_code IN ('P', 'R')
                --             AND status_code IN ('I',
                --                                 'Q',
                --                                 'P',
                --                                 'R',
                --                                 'C')
                --             AND hold_flag = 'N'
                AND (NVL (request_type, 'X') != 'S')
                AND requested_start_date >= TRUNC (SYSDATE)
                AND user_concurrent_program_name LIKE 'XXWC AP Hold Workflow Notification program';


      g_prog_exception    EXCEPTION;
      l_err_msg           VARCHAR2 (4000);
      l_run               NUMBER:=0;
   BEGIN
      -- printing in Parameters
      write_log ('Begining Apin Workflow monitor watchdog Program starts ');
      write_log ('========================================================');
      write_log ('Printing in parameter');
      write_log ('p_distribution_list:  ' || p_distribution_list);

      --Initialize the Out Parameter
      errbuf := NULL;
      retcode := '0';

      FOR rec IN cur_prog_details
      LOOP
         l_run:=1;
         IF (rec.hold_flag != 'N')
         THEN
            l_err_msg := 'Program: XXWC AP Hold Workflow Notification program is on Hold';
            RAISE g_prog_exception;
         END IF;
         IF (rec.phase_code= 'I')
         THEN
            l_err_msg := 'Program: XXWC AP Hold Workflow Notification program is Inactive';
            RAISE g_prog_exception;
         END IF;
         
          IF (rec.status_code in ('E','B','W','H','U','Z','A','M','D','X','S'))
         THEN
            l_err_msg := 'Program: XXWC AP Hold Workflow Notification program is not running.';
            RAISE g_prog_exception;
         END IF;
         
      END LOOP;
      IF l_run <> 1 THEN 
      
         l_err_msg := 'XXWC AP Hold Workflow Notification Program is not running.';
         
         RAISE g_prog_exception;
         
      END IF;
      retcode := '0';
      write_log ('Program Successfully completed');
   EXCEPTION
      WHEN g_prog_exception
      THEN
         errbuf := l_err_msg;
         retcode := '0';
         write_error (l_err_msg);
         write_log (l_err_msg);
         send_email (p_distro_list  =>p_distribution_list,
                         p_sub      => 'XXWC AP Hold Workflow Notification Program Error' ,
                         p_message  => l_err_msg || '. please check the schedule of the program ');
      WHEN OTHERS
      THEN
         l_err_msg := 'Error Msg :' || SQLERRM;
         errbuf := l_err_msg;
         retcode := '2';
         send_email (p_distro_list  =>p_distribution_list,
                         p_sub      => 'XXWC AP Hold Workflow Notification Program Error' ,
                         p_message  => l_err_msg || '. please check the schedule of the program ');
         write_error (l_err_msg);
         write_log (l_err_msg);
   END apin_workflow_watchdog;
END xxwc_watchdog_pkg;
/