CREATE OR REPLACE PACKAGE APPS.xxwc_watchdog_pkg
AS
   /*************************************************************************

   **************************************************************************
     $Header xxwc_watchdog_pkg $
     Module Name: xxwc_watchdog_pkg.pks

     PURPOSE:   This package is called by the concurrent programs
                XXWC Watchdog Monitor program

     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/01/2016  Neha Saini         Initial Build - TMS# 20151019-00024 

   /*************************************************************************



   /*************************************************************************
     Procedure : generate_invoice_file

     PURPOSE   :This procedure is used to monitor XXWC AP Hold Workflow Notification Program


   ************************************************************************/
   PROCEDURE apin_workflow_watchdog (
      errbuf                OUT      VARCHAR2,
      retcode               OUT      VARCHAR2,
      p_distribution_list   IN       VARCHAR2
   );
END xxwc_watchdog_pkg;
/