/*
	This will set up the XXCUS application to be compatible
	with ACMP. This file handles the SQL database recommendations.
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
	DBMS_OUTPUT.put_line ('Script 1 - Beginning');
	
	
	/* 4. The entry for product xxcus is not valid in FND_PRODUCT_INSTALLATIONS
		INDUSTRY = C
		TABLESPACE = XXCUS_DATA
		INDEX_TABLESPACE = XXCUS_IDX
		TEMPORARY_TABLESPACE = TEMP1
		SIZING_FACTOR = 100 */
		-- we are NOT setting oracle_id = application=id per our SR
	UPDATE FND_PRODUCT_INSTALLATIONS SET 
		INDUSTRY = 'C',
		TABLESPACE = 'XXCUS_DATA',
		INDEX_TABLESPACE = 'XXCUS_IDX',
		TEMPORARY_TABLESPACE = 'TEMP1',
		SIZING_FACTOR = 100,
		PRODUCT_VERSION = '12.0.0',
		INSTALL_GROUP_NUM = 0 
	WHERE  APPLICATION_ID = 20003;
	
	UPDATE FND_ORACLE_USERID SET ENABLED_FLAG = 'N'
		WHERE ORACLE_ID = 20044; -- note, not setting this for where oracle_id = 20003 since that's a different app. see the SR/bug
	
	-- HDS Fix
	-- It turns out that there IS a bug in ACMP's validation process. The Oracle Support
	-- team referred this to development and created a bug but did not come up with a solution.
	-- This is the solution I devised. Needs testing in FQA.
	UPDATE FND_ORACLE_USERID SET ORACLE_ID=20065 WHERE ORACLE_ID=20003;
	UPDATE FND_ORACLE_USERID SET ORACLE_ID=20003 WHERE ORACLE_ID=20044;
	UPDATE FND_PRODUCT_INSTALLATIONS SET ORACLE_ID = APPLICATION_ID WHERE  APPLICATION_ID = 20003;
	UPDATE FND_PRODUCT_INSTALLATIONs SET DB_STATUS='I', STATUS='I' WHERE ORACLE_ID=20003;
	
	COMMIT;
	DBMS_OUTPUT.put_line ('Script 1 - Committed');
EXCEPTION
	WHEN OTHERS
	THEN
		ROLLBACK;
		DBMS_OUTPUT.put_line ('Script 1 - Rolled Back');
		DBMS_OUTPUT.put_line ('Script 1, Errors =' || SQLERRM);
END;
/
