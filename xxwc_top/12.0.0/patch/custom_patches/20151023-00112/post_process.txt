echo include any Linux commands to be executed after the XPatch is applied.
echo Ending XPatch execution for 20151023-00112
mkdir /obase/ebiz/apps/apps_st/appl/xxcus/12.0.0/admin/driver
chmod a+r /obase/ebiz/apps/apps_st/appl/xxcus/12.0.0/admin/driver
mkdir /obase/ebiz/apps/apps_st/appl/xxcus/12.0.0/admin/sql
chmod a+r /obase/ebiz/apps/apps_st/appl/xxcus/12.0.0/admin/sql
echo '# Dummy xxcusfile.drv' > /obase/ebiz/apps/apps_st/appl/xxcus/12.0.0/admin/driver/xxcusfile.drv
echo 'commit;' > /obase/ebiz/apps/apps_st/appl/xxcus/12.0.0/sql/XXCUSNLINS.sql
echo 'exit;' >> /obase/ebiz/apps/apps_st/appl/xxcus/12.0.0/sql/XXCUSNLINS.sql
echo 'commit;' > /obase/ebiz/apps/apps_st/appl/xxcus/12.0.0/admin/sql/XXCUSNLADD.sql
echo 'exit;' >> /obase/ebiz/apps/apps_st/appl/xxcus/12.0.0/admin/sql/XXCUSNLADD.sql
cp -v content/xxcusprod.txt $APPL_TOP/admin/xxcusprod.txt
cp -v content/xxcusterr.txt $APPL_TOP/admin/xxcusterr.txt
