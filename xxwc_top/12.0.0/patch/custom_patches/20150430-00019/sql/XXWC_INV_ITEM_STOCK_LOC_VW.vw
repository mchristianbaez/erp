CREATE OR REPLACE VIEW APPS.XXWC_INV_ITEM_STOCK_LOC_VW as
/*************************************************************************
  $Header XXWC_INV_ITEM_STOCK_LOC_VW.vw $
  Module Name: XXWC_INV_ITEM_STOCK_LOC_VW

  PURPOSE: Item Stock Locator View for WEB ADI upload

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        18-May-2015  Manjula Chellappan    Initial Version TMS # 20150430-00019

**************************************************************************/
SELECT mp.organization_code,
       msi.organization_id,
       msi.inventory_item_id,
       msi.segment1 item_number,
       msi.description item_description,
       mil.segment1 cur_location,
       mil.subinventory_code cur_subinv,
       NULL delete_flag
  FROM mtl_secondary_locators msl,
       mtl_item_locations mil,
       mtl_system_items_b msi,
       mtl_parameters mp
 WHERE     mp.organization_id = msi.organization_id
       AND msi.organization_id = msl.organization_id
       AND msi.inventory_item_id = msl.inventory_item_id
       AND msl.secondary_locator = mil.inventory_location_id;