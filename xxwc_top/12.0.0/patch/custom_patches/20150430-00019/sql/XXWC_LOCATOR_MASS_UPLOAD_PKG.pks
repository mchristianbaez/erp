CREATE OR REPLACE PACKAGE APPS.XXWC_LOCATOR_MASS_UPLOAD_PKG AS

/*************************************************************************
     $Header XXWC_LOCATOR_MASS_UPLOAD_PKG $
     Module Name: XXWC_LOCATOR_MASS_UPLOAD_PKG.pkb

     PURPOSE:   This package is used by the WebADI For Mass Location/Assignment
                upload

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        08/23/2013  Lee Spitzer             Initial Version
     2.0        10/18/2013  Consuelo Gonzalez       Completed processing code
     3.0        11/26/2013  Consuelo Gonzalez       TMS 20131120-00024: Added ability to delete
                                                    bin assignment, added archive functionality,
                                                    added unique key per record, added WHO columns
     4.0        01/22/2014  Consuelo Gonzalez       TMS 20140122-00181: Updated to allow assignment of 
                                                    multiple 0- locatioins.
     5.0        05/18/2015  Manjula Chellappan      TMS 20150430-00019 : Create Copy of XXWC Stock Locator Assignment 
	                                                for Branch Relocation. New Procedure Upload_subinv_stock_loc added													
   **************************************************************************/

/**************************************************************************
 *
 * PROCEDURE
 *  Bin_Upload
 *
 * DESCRIPTION
 *  This procedure is used to upload the user-supplied data from the Excel spreadsheet into the custom interface table
 *
 * PARAMETERS
 * ==========
 * NAME                   TYPE     IN/OUT  DESCRIPTION
.* ---------------------- -------- ------  ---------------------------------------
 * P_ORG_CODE             VARCHAR2 IN      inventory organizatin code
 * P_ITEM_NUMBER          VARCHAR2 IN      inventory item number
 * P_OLD_LOCATION         VARCAHR2 IN      old stock locator
 * P_NEW_LOCATION         VARCAHR2 IN      new stock locator
 *
 * CALLED BY
 *  XXWC Item Location Upload Integrator during the WebADI upload process
 *
 *************************************************************************/
 
  G_LOC_MAX_LEVELS   NUMBER := TO_NUMBER(fnd_profile.value ('XXWC_INV_LOC_MASS_LOAD_LEVELS'));
 
  Procedure Bin_Upload(i_org_code       IN VARCHAR2
                      ,i_item_number    IN VARCHAR2
                      ,i_item_desc      IN VARCHAR2
                      ,i_cur_location   IN VARCHAR2
                      ,i_delete_flag    IN VARCHAR2);
                  

  Procedure Create_Locator
                      (p_organization_id    IN NUMBER
                      ,p_subinventory_code  IN VARCHAR2
                      ,p_stock_locator      IN VARCHAR2
                      ,x_inventory_loc_id   OUT NUMBER);
  
  -- 01/22/14 CG: TMS 20140122-00181: Added new procedure to regularly create the missing 0- locators
  procedure xxwc_created_0_loc (errbuf          OUT VARCHAR2, 
                                retcode         OUT NUMBER,
                                p_org_id        IN  NUMBER);
--<<Revision 5.0 Begin															
  Procedure Upload_subinv_stock_loc
                      (p_org_code       IN VARCHAR2
                      ,p_item_number    IN VARCHAR2                      					
                      ,p_cur_locator    IN VARCHAR2
					  ,p_cur_subinv     IN VARCHAR2
                      ,p_delete_flag    IN VARCHAR2);				    
  --Revision 5.0 End>>
END XXWC_LOCATOR_MASS_UPLOAD_PKG;
/


