--Report Name            : Report Runtimes - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Report Runtimes - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View XXEIS_WC_RUNTIME_V
xxeis.eis_rs_ins.v( 'XXEIS_WC_RUNTIME_V',85000,'','','','','MR020532','XXEIS','Xxeis Wc Runtime V','XWRV','','');
--Delete View Columns for XXEIS_WC_RUNTIME_V
xxeis.eis_rs_utility.delete_view_rows('XXEIS_WC_RUNTIME_V',85000,FALSE);
--Inserting View Columns for XXEIS_WC_RUNTIME_V
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','VIEW_NAME',85000,'View Name','VIEW_NAME','','','','MR020532','VARCHAR2','','','View Name','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','RESPONSIBILITY_NAME',85000,'Responsibility Name','RESPONSIBILITY_NAME','','','','MR020532','VARCHAR2','','','Responsibility Name','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','CATEGORY_NAME',85000,'Category Name','CATEGORY_NAME','','','','MR020532','VARCHAR2','','','Category Name','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','BUSINESS_GROUP_NAME',85000,'Business Group Name','BUSINESS_GROUP_NAME','','','','MR020532','VARCHAR2','','','Business Group Name','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','APPLICATION_NAME',85000,'Application Name','APPLICATION_NAME','','','','MR020532','VARCHAR2','','','Application Name','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','CONC_EXE_TIME_IN_MINUTES',85000,'Conc Exe Time In Minutes','CONC_EXE_TIME_IN_MINUTES','','','','MR020532','NUMBER','','','Conc Exe Time In Minutes','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','ACTUAL_COMPLETION_DATE',85000,'Actual Completion Date','ACTUAL_COMPLETION_DATE','','','','MR020532','VARCHAR2','','','Actual Completion Date','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','ACTUAL_START_DATE',85000,'Actual Start Date','ACTUAL_START_DATE','','','','MR020532','VARCHAR2','','','Actual Start Date','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','REQUESTED_START_DATE',85000,'Requested Start Date','REQUESTED_START_DATE','','','','MR020532','VARCHAR2','','','Requested Start Date','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','REQUEST_DATE',85000,'Request Date','REQUEST_DATE','','','','MR020532','VARCHAR2','','','Request Date','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','END_DATE',85000,'End Date','END_DATE','','','','MR020532','DATE','','','End Date','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','START_DATE',85000,'Start Date','START_DATE','','','','MR020532','DATE','','','Start Date','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','END_TIME',85000,'End Time','END_TIME','','','','MR020532','VARCHAR2','','','End Time','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','START_TIME',85000,'Start Time','START_TIME','','','','MR020532','VARCHAR2','','','Start Time','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','ORGANIZATION_ID',85000,'Organization Id','ORGANIZATION_ID','','','','MR020532','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','EMPLOYEE_FULL_NAME',85000,'Employee Full Name','EMPLOYEE_FULL_NAME','','','','MR020532','VARCHAR2','','','Employee Full Name','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','SUBMITTED_BY',85000,'Submitted By','SUBMITTED_BY','','','','MR020532','VARCHAR2','','','Submitted By','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','USER_ID',85000,'User Id','USER_ID','','','','MR020532','NUMBER','','','User Id','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','SESSION_ID',85000,'Session Id','SESSION_ID','','','','MR020532','NUMBER','','','Session Id','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','ROWS_RETRIEVED',85000,'Rows Retrieved','ROWS_RETRIEVED','','','','MR020532','NUMBER','','','Rows Retrieved','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','STATUS',85000,'Status','STATUS','','','','MR020532','VARCHAR2','','','Status','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','REPORT_NAME',85000,'Report Name','REPORT_NAME','','','','MR020532','VARCHAR2','','','Report Name','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','REPORT_ID',85000,'Report Id','REPORT_ID','','','','MR020532','NUMBER','','','Report Id','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','REQUEST_ID',85000,'Request Id','REQUEST_ID','','','','MR020532','NUMBER','','','Request Id','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','PROCESS_ID',85000,'Process Id','PROCESS_ID','','','','MR020532','NUMBER','','','Process Id','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_RUNTIME_V','REPORT_EXECUTION_TIM',85000,'Report Execution Tim','REPORT_EXECUTION_TIM','','','','MR020532','VARCHAR2','','','Report Execution Tim','','','');
--Inserting View Components for XXEIS_WC_RUNTIME_V
--Inserting View Component Joins for XXEIS_WC_RUNTIME_V
END;
/
set scan on define on
prompt Creating Report Data for Report Runtimes - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Report Runtimes - WC
xxeis.eis_rs_utility.delete_report_rows( 'Report Runtimes - WC' );
--Inserting Report - Report Runtimes - WC
xxeis.eis_rs_ins.r( 85000,'Report Runtimes - WC','','To see report runtimes for analysis','','','','MR020532','XXEIS_WC_RUNTIME_V','Y','','','MR020532','','N','WC Audit Reports','PDF,','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Report Runtimes - WC
xxeis.eis_rs_ins.rc( 'Report Runtimes - WC',85000,'APPLICATION_NAME','Application Name','Application Name','','','default','','14','N','','','','','','','','MR020532','N','N','','XXEIS_WC_RUNTIME_V','','');
xxeis.eis_rs_ins.rc( 'Report Runtimes - WC',85000,'CONC_EXE_TIME_IN_MINUTES','Conc Exe Time In Minutes','Conc Exe Time In Minutes','','~~~','default','','12','N','','','','','','','','MR020532','N','N','','XXEIS_WC_RUNTIME_V','','');
xxeis.eis_rs_ins.rc( 'Report Runtimes - WC',85000,'EMPLOYEE_FULL_NAME','Employee Full Name','Employee Full Name','','','default','','8','N','','','','','','','','MR020532','N','N','','XXEIS_WC_RUNTIME_V','','');
xxeis.eis_rs_ins.rc( 'Report Runtimes - WC',85000,'END_TIME','End Time','End Time','','','default','','11','N','','','','','','','','MR020532','N','N','','XXEIS_WC_RUNTIME_V','','');
xxeis.eis_rs_ins.rc( 'Report Runtimes - WC',85000,'ORGANIZATION_ID','Organization Id','Organization Id','','~~~','default','','9','N','','','','','','','','MR020532','N','N','','XXEIS_WC_RUNTIME_V','','');
xxeis.eis_rs_ins.rc( 'Report Runtimes - WC',85000,'PROCESS_ID','Process Id','Process Id','','~~~','default','','1','N','','','','','','','','MR020532','N','N','','XXEIS_WC_RUNTIME_V','','');
xxeis.eis_rs_ins.rc( 'Report Runtimes - WC',85000,'REPORT_ID','Report Id','Report Id','','~~~','default','','3','N','','','','','','','','MR020532','N','N','','XXEIS_WC_RUNTIME_V','','');
xxeis.eis_rs_ins.rc( 'Report Runtimes - WC',85000,'REPORT_NAME','Report Name','Report Name','','','default','','4','N','','','','','','','','MR020532','N','N','','XXEIS_WC_RUNTIME_V','','');
xxeis.eis_rs_ins.rc( 'Report Runtimes - WC',85000,'REQUEST_ID','Request Id','Request Id','','~~~','default','','2','N','','','','','','','','MR020532','N','N','','XXEIS_WC_RUNTIME_V','','');
xxeis.eis_rs_ins.rc( 'Report Runtimes - WC',85000,'ROWS_RETRIEVED','Rows Retrieved','Rows Retrieved','','~~~','default','','6','N','','','','','','','','MR020532','N','N','','XXEIS_WC_RUNTIME_V','','');
xxeis.eis_rs_ins.rc( 'Report Runtimes - WC',85000,'STATUS','Status','Status','','','default','','5','N','','','','','','','','MR020532','N','N','','XXEIS_WC_RUNTIME_V','','');
xxeis.eis_rs_ins.rc( 'Report Runtimes - WC',85000,'SUBMITTED_BY','Submitted By','Submitted By','','','default','','7','N','','','','','','','','MR020532','N','N','','XXEIS_WC_RUNTIME_V','','');
xxeis.eis_rs_ins.rc( 'Report Runtimes - WC',85000,'VIEW_NAME','View Name','View Name','','','default','','15','N','','','','','','','','MR020532','N','N','','XXEIS_WC_RUNTIME_V','','');
xxeis.eis_rs_ins.rc( 'Report Runtimes - WC',85000,'REPORT_EXECUTION_TIM','Report Execution Time','Report Execution Tim','','','default','','13','N','','','','','','','','MR020532','N','N','','XXEIS_WC_RUNTIME_V','','');
xxeis.eis_rs_ins.rc( 'Report Runtimes - WC',85000,'START_TIME','Start Time','Start Time','','','','','10','N','','','','','','','','MR020532','N','N','','XXEIS_WC_RUNTIME_V','','');
--Inserting Report Parameters - Report Runtimes - WC
xxeis.eis_rs_ins.rp( 'Report Runtimes - WC',85000,'Start Date','Start Date','START_DATE','>=','','','DATE','N','Y','1','','Y','CONSTANT','MR020532','Y','','','','');
xxeis.eis_rs_ins.rp( 'Report Runtimes - WC',85000,'End Date','End Date','END_DATE','<=','','','DATE','N','Y','2','','Y','CONSTANT','MR020532','Y','','','','');
xxeis.eis_rs_ins.rp( 'Report Runtimes - WC',85000,'Report Name','Report Name','REPORT_NAME','IN','','','VARCHAR2','N','Y','3','','Y','CONSTANT','MR020532','Y','','','','');
xxeis.eis_rs_ins.rp( 'Report Runtimes - WC',85000,'Organization ID','Organizaion ID','ORGANIZATION_ID','IN','','162','NUMERIC','N','Y','4','','Y','CONSTANT','MR020532','Y','','','','');
--Inserting Report Conditions - Report Runtimes - WC
xxeis.eis_rs_ins.rcn( 'Report Runtimes - WC',85000,'START_DATE','>=',':Start Date','','','Y','1','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Report Runtimes - WC',85000,'END_DATE','<=',':End Date','','','Y','2','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Report Runtimes - WC',85000,'REPORT_NAME','IN',':Report Name','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Report Runtimes - WC',85000,'ORGANIZATION_ID','IN',':Organization ID','','','Y','4','Y','MR020532');
--Inserting Report Sorts - Report Runtimes - WC
--Inserting Report Triggers - Report Runtimes - WC
--Inserting Report Templates - Report Runtimes - WC
--Inserting Report Portals - Report Runtimes - WC
--Inserting Report Dashboards - Report Runtimes - WC
--Inserting Report Security - Report Runtimes - WC
xxeis.eis_rs_ins.rsec( 'Report Runtimes - WC','20005','','50900',85000,'MR020532','','');
--Inserting Report Pivots - Report Runtimes - WC
END;
/
set scan on define on
