CREATE OR REPLACE PACKAGE BODY APPS."XXCUSHR_PS_INTERFACE_PKG" AS

  -- File Name: XXCUS_HRPEOPLESOFT_PKG
  -- HISTORY
  /**************************************************************************
   VERSION DATE          AUTHOR(S)          	DESCRIPTION
   ------- -----------   -----------------  ---------------------------------
   1.6     27-Aug-2014   Vijay Srinivasan   	Updating the cost centers for FM 
                                            	per ESMS Ticket 260732
   1.7      10-Oct-2014  Kathy Poling      	ESMS 266549 ERRORS ALERT cost center for WW 
                                           	correction in ps_emp_import   
   1.8      09-Mar-2015  Maharajan Shunmugam    ESMS#280593 HR Interface native dept issue         
   1.9      07-Mar-2018  P.Vamshidhar        TMS#20180226-00396 - T and E Oversight Feed
  **************************************************************************/
  --
  -- Package Variables
  --
  l_package VARCHAR2(33) := 'XXCUSHR_PS_INTERFACE_PKG.';
  --l_dflt_email VARCHAR2(200) := 'hds-HDSFASTap-u1@hdsupply.com'; --'Manny.Rodriguez@hdsupply.com'; --'HDSOracleDevelopers@hdsupply.com' ; -- email used for errors within
  l_dflt_email VARCHAR2(200) := 'HDSOracleDevelopers@hdsupply.com'; -- email used for errors within

  l_errorstatus   NUMBER;
  l_req_id        NUMBER NULL;
  l_phase         VARCHAR2(50);
  l_status        VARCHAR2(50);
  l_dev_status    VARCHAR2(50);
  l_dev_phase     VARCHAR2(50);
  l_message       VARCHAR2(250);
  l_error_message VARCHAR2(3000);
  --  l_message            VARCHAR2(150);
  l_host               VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
  l_errormessage       VARCHAR2(3000);
  l_sender             VARCHAR2(100);
  l_sid                VARCHAR2(3);
  l_can_submit_request BOOLEAN := TRUE;
  l_globalset          VARCHAR2(100);
  l_err_msg            VARCHAR2(3000);
  l_err_code           NUMBER;
  l_sec                VARCHAR2(255);

  l_body             VARCHAR2(32767);
  l_body_header      VARCHAR2(32767);
  l_body_detail      VARCHAR2(32767);
  l_body_footer      VARCHAR2(32767);
  l_body_header2     VARCHAR2(32767);
  l_body_detail2     VARCHAR2(32767);
  l_body_footer2     VARCHAR2(32767);
  l_audit_found      NUMBER;
  l_audit_found2     NUMBER;
  l_sequence         NUMBER;
  l_increment        NUMBER DEFAULT 0;
  l_statement        VARCHAR2(9000);
  l_msg_session      VARCHAR2(155);
  l_msg_session_init VARCHAR2(155) DEFAULT 'PSXFR_';
  l_time_start       DATE;
  l_time_end         DATE;
  l_time_lapse       NUMBER;
  l_responsibility   VARCHAR2(100); --Version 1.1
  l_user             VARCHAR2(100); --Version 1.1

  -- Error DEBUG
  l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUSHR_PS_INTERFACE_PKG';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  fatal_error EXCEPTION;

  -- -----------------------------------------------------------------------------
  -- |----------------------------< main_hr_processor  >-------------------------|
  -- -----------------------------------------------------------------------------
  --
  -- {Start Of Comments}
  --
  -- Description:
  --   This is the main procedure that fires the sub procedures.
  --   This procedure inserts the final worklist after the create_sequence
  --   procedure into the staging table.  It is also responsible for creating the
  --   Oracle_hr_prior_day table to be used by the oracle_activity_logger proc.
  --   It will then call the HR_Interface and finally, it cleans up the tables
  --   that were used by this package.
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- ---------------------------------
  -- 1.0     24-Apr-2011   Kathy Poling     Initial creation of the procedure copied
  --                                        from R11 modified for R12
  -- -----------------------------------------------------------------------------

  PROCEDURE main_hr_processor(p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER) IS
  
    --  Setup parameters for running FND JOBS!
  BEGIN
  
    SELECT responsibility_name
      INTO l_responsibility
      FROM fnd_responsibility_vl
     WHERE responsibility_id = fnd_global.resp_id();
  
    SELECT user_name
      INTO l_user
      FROM fnd_user
     WHERE user_id = fnd_global.user_id();
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user
                                                             ,l_responsibility);
    IF l_can_submit_request
    THEN
      l_globalset := 'Global Variables are set.';
    
    ELSE
    
      l_globalset := 'Global Variables are not set.';
      l_sec       := 'Global Variables are not set for the Responsibility of ' ||
                     l_responsibility || ' and the User of ' || l_user;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;
  
    fnd_file.put_line(fnd_file.log, l_globalset);
    fnd_file.put_line(fnd_file.output, l_globalset);
    --  END OF Setup parameters for running FND JOBS!
  
    --**********************************************************************************--
    -- STEP 1.  Run PeopleSoft Employee Import
    dbms_output.enable(1000000);
    BEGIN
      --RUN PS_EMP_IMPORT
    
      -- Get time start
      BEGIN
        SELECT SYSDATE INTO l_time_start FROM dual;
      EXCEPTION
        WHEN OTHERS THEN
          l_sec := 'Step 1 : Error getting time from dual' || chr(10) ||
                   SQLERRM;
          RAISE program_error;
      END;
    
      l_message := 'Running PeopleSoft Employee Import';
      --      l_message := l_message;
    
      -- Start data import from PeopleSoft view
      --dbms_output.put_line('START IMPORT');
      fnd_file.put_line(fnd_file.log, l_message);
      l_req_id := apps.fnd_request.submit_request('XXCUS'
                                                 ,'XXCUSHRPSIMPORT'
                                                 ,NULL
                                                 ,SYSDATE
                                                 ,FALSE);
      COMMIT;
      --dbms_output.put_line('after START IMPORT');
      IF (l_req_id != 0)
      THEN
        --dbms_output.put_line('line 123');
        IF fnd_concurrent.wait_for_request(l_req_id
                                          ,6
                                          ,15000
                                          ,l_phase
                                          ,l_status
                                          ,l_dev_phase
                                          ,l_dev_status
                                          ,l_message)
        THEN
          --dbms_output.put_line('line 126');
          l_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                             l_dev_phase || ' DStatus ' || l_dev_status ||
                             chr(10) || ' MSG - ' || l_message;
          --dbms_output.put_line('line 131');
          --dbms_output.put_line('l_dev_phase   ' || l_dev_phase);
          --dbms_output.put_line('l_dev_status   ' || l_dev_status);
        
          -- Error Returned
          IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
          THEN
            --dbms_output.put_line('line 135');
            l_statement := 'An error occured in the running of the HDS HR PEOPLESOFT EMPLOYEE IMPORT' ||
                           l_error_message || '.';
            --dbms_output.put_line('line 138');
            fnd_file.put_line(fnd_file.log, l_statement);
            --dbms_output.put_line('line 140:    ' || l_statement);
            fnd_file.put_line(fnd_file.output, l_statement);
            --dbms_output.put_line('line 142:  ' || l_statement);
            RAISE program_error;
          
          END IF;
          -- Then Success!
          --dbms_output.put_line('line 140');
        ELSE
          l_statement := 'An error occured running the HDS HR PEOPLESOFT EMPLOYEE IMPORT' ||
                         l_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
      
      ELSE
        l_statement := 'An error occured when trying to submitting the HDS HR PEOPLESOFT EMPLOYEE IMPORT';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      END IF;
    END;
    --dbms_output.put_line('before get time');
    -- Get time End
    BEGIN
      SELECT SYSDATE INTO l_time_end FROM dual;
    EXCEPTION
      WHEN OTHERS THEN
        l_sec := 'Step 1 : Error getting time end from dual' || chr(10) ||
                 SQLERRM;
        RAISE program_error;
    END;
  
    l_time_lapse := trunc((l_time_end - l_time_start) * 1440, 0);
    l_statement  := 'Completed Step 1: ' || l_message || ' in ' ||
                    l_time_lapse || ' minutes' || chr(10);
  
    fnd_file.put_line(fnd_file.log, l_statement);
    fnd_file.put_line(fnd_file.output, l_statement);
  
    -- STEP 2.  Call Oracle Filter
    --dbms_output.put_line('START ORACLE FILTER');
    --Initialize variables.
    l_req_id        := 0;
    l_phase         := NULL;
    l_status        := NULL;
    l_dev_status    := NULL;
    l_dev_phase     := NULL;
    l_message       := NULL;
    l_error_message := NULL;
  
    BEGIN
    
      -- Get time start
      BEGIN
        SELECT SYSDATE INTO l_time_start FROM dual;
      EXCEPTION
        WHEN OTHERS THEN
          l_sec := 'Step 2 : Error getting time from dual' || chr(10) ||
                   SQLERRM;
          RAISE program_error;
      END;
    
      l_message := 'Running Oracle Filter';
      l_message := l_message;
    
      fnd_file.put_line(fnd_file.log, l_message);
      l_req_id := apps.fnd_request.submit_request('XXCUS'
                                                 ,'XXCUSHRORAFILTER'
                                                 ,NULL
                                                 ,SYSDATE
                                                 ,FALSE);
      COMMIT;
    
      IF (l_req_id != 0)
      THEN
        IF fnd_concurrent.wait_for_request(l_req_id
                                          ,6
                                          ,15000
                                          ,l_phase
                                          ,l_status
                                          ,l_dev_phase
                                          ,l_dev_status
                                          ,l_message)
        THEN
          l_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                             l_dev_phase || ' DStatus ' || l_dev_status ||
                             chr(10) || ' MSG - ' || l_message;
          -- Error Returned
          IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
          THEN
            l_statement := 'An error occured in the running the HDS HR ORACLE EMPLOYEE FILTER' ||
                           l_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_statement);
            fnd_file.put_line(fnd_file.output, l_statement);
            RAISE program_error;
          
          END IF;
          -- Then Success!
        ELSE
          l_statement := 'An error occured running the HDS HR ORACLE EMPLOYEE FILTER' ||
                         l_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
      
      ELSE
        l_statement := 'An error occured when trying to submitting the HDS HR ORACLE EMPLOYEE FILTER';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      END IF;
    END;
  
    -- Get time End
    BEGIN
      SELECT SYSDATE INTO l_time_end FROM dual;
    EXCEPTION
      WHEN OTHERS THEN
        l_sec := 'Step 2 : Error getting time end from dual' || chr(10) ||
                 SQLERRM;
        RAISE program_error;
    END;
  
    l_time_lapse := trunc((l_time_end - l_time_start) * 1440, 0);
    l_statement  := 'Completed Step 2: ' || l_message || ' in ' ||
                    l_time_lapse || ' minutes' || chr(10);
  
    fnd_file.put_line(fnd_file.log, l_statement);
    fnd_file.put_line(fnd_file.output, l_statement);
  
    -- STEP 4.  Create worklist
    --dbms_output.put_line('Create worklist');
    --Initialize variables.
    l_req_id        := 0;
    l_phase         := NULL;
    l_status        := NULL;
    l_dev_status    := NULL;
    l_dev_phase     := NULL;
    l_message       := NULL;
    l_error_message := NULL;
  
    BEGIN
      --RUN CREATE_WORKLIST
    
      -- Get time start
      BEGIN
        SELECT SYSDATE INTO l_time_start FROM dual;
      EXCEPTION
        WHEN OTHERS THEN
          l_sec := 'Step 4 : Error getting time from dual' || chr(10) ||
                   SQLERRM;
          RAISE program_error;
      END;
    
      l_message := 'Running Oracle Worklist creator';
      l_message := l_message;
    
      -- delete unprocessed rows from data corruption
      DELETE FROM xxcus.xxcushr_worklist_tbl seqlist
       WHERE seqlist.stg_processed = 'N';
      COMMIT;
    
      fnd_file.put_line(fnd_file.log, l_message);
      l_req_id := apps.fnd_request.submit_request('XXCUS'
                                                 ,'XXCUSHRPSWORKLIST'
                                                 ,NULL
                                                 ,SYSDATE
                                                 ,FALSE);
      COMMIT;
    
      IF (l_req_id != 0)
      THEN
        IF fnd_concurrent.wait_for_request(l_req_id
                                          ,6
                                          ,85000
                                          ,l_phase
                                          ,l_status
                                          ,l_dev_phase
                                          ,l_dev_status
                                          ,l_message)
        THEN
          l_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                             l_dev_phase || ' DStatus ' || l_dev_status ||
                             chr(10) || ' MSG - ' || l_message;
          -- Error Returned
          IF upper(l_dev_phase) != 'COMPLETE' OR
             upper(l_dev_status) != 'NORMAL'
          THEN
            l_statement := 'An error occured in the running the HDS HR PEOPLESOFT CREATE WORKLIST process' ||
                           l_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_statement);
            fnd_file.put_line(fnd_file.output, l_statement);
            RAISE program_error;
          
          END IF;
          -- Then Success!
        ELSE
          l_statement := 'An error occured running the HDS HR PEOPLESOFT CREATE WORKLIST process' ||
                         l_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
      
      ELSE
        l_statement := 'An error occured when trying to submitting the HDS HR PEOPLESOFT CREATE WORKLIST process';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      END IF;
    END;
  
    -- Get time End
    BEGIN
      SELECT SYSDATE INTO l_time_end FROM dual;
    EXCEPTION
      WHEN OTHERS THEN
        l_sec := 'Step 4 : Error getting time end from dual' || chr(10) ||
                 SQLERRM;
        RAISE program_error;
    END;
  
    l_time_lapse := trunc((l_time_end - l_time_start) * 1440, 0);
    l_statement  := 'Completed Step 4: ' || l_message || ' in ' ||
                    l_time_lapse || ' minutes' || chr(10);
  
    fnd_file.put_line(fnd_file.log, l_statement);
    fnd_file.put_line(fnd_file.output, l_statement);
  
    -- STEP 5.  Check Supervisor
    --dbms_output.put_line('Check Supervisor');
    --Initialize variables.
    l_req_id        := 0;
    l_phase         := NULL;
    l_status        := NULL;
    l_dev_status    := NULL;
    l_dev_phase     := NULL;
    l_message       := NULL;
    l_error_message := NULL;
  
    BEGIN
      --check supervisor
    
      -- Get time start
      BEGIN
        SELECT SYSDATE INTO l_time_start FROM dual;
      EXCEPTION
        WHEN OTHERS THEN
          l_sec := 'Step 5 : Error getting time from dual' || chr(10) ||
                   SQLERRM;
          RAISE program_error;
      END;
    
      l_message := 'Running Check supervisor process';
      l_message := l_message;
    
      fnd_file.put_line(fnd_file.log, l_message);
      l_req_id := apps.fnd_request.submit_request('XXCUS'
                                                 ,'XXCUSHRPSCHECKSUP'
                                                 ,NULL
                                                 ,SYSDATE
                                                 ,FALSE);
      COMMIT;
    
      IF (l_req_id != 0)
      THEN
        IF fnd_concurrent.wait_for_request(l_req_id
                                          ,6
                                          ,15000
                                          ,l_phase
                                          ,l_status
                                          ,l_dev_phase
                                          ,l_dev_status
                                          ,l_message)
        THEN
          l_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                             l_dev_phase || ' DStatus ' || l_dev_status ||
                             chr(10) || ' MSG - ' || l_message;
          -- Error Returned
          IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
          THEN
            l_statement := 'An error occured in the running the HDS HR PEOPLESOFT CHECK SUPERVISOR' ||
                           l_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_statement);
            fnd_file.put_line(fnd_file.output, l_statement);
            RAISE program_error;
          
          END IF;
          -- Then Success!
        ELSE
          l_statement := 'An error occured running the HDS HR PEOPLESOFT CHECK SUPERVISOR' ||
                         l_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
      
      ELSE
        l_statement := 'An error occured when trying to submitting the HDS HR PEOPLESOFT CHECK SUPERVISOR';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      END IF;
    END;
  
    -- Get time End
    BEGIN
      SELECT SYSDATE INTO l_time_end FROM dual;
    EXCEPTION
      WHEN OTHERS THEN
        l_sec := 'Step 5 : Error getting time end from dual' || chr(10) ||
                 SQLERRM;
        RAISE program_error;
    END;
  
    l_time_lapse := trunc((l_time_end - l_time_start) * 1440, 0);
    l_statement  := 'Completed Step 5: ' || l_message || ' in ' ||
                    l_time_lapse || ' minutes' || chr(10);
  
    fnd_file.put_line(fnd_file.log, l_statement);
    fnd_file.put_line(fnd_file.output, l_statement);
  
    -- STEP 6.  Run Sequencer
    --dbms_output.put_line('Run Sequencer');
    --Initialize variables.
    l_req_id        := 0;
    l_phase         := NULL;
    l_status        := NULL;
    l_dev_status    := NULL;
    l_dev_phase     := NULL;
    l_message       := NULL;
    l_error_message := NULL;
  
    BEGIN
      --RUN CREATE_SEQUENCE
      -- Get time start
      BEGIN
        SELECT SYSDATE INTO l_time_start FROM dual;
      EXCEPTION
        WHEN OTHERS THEN
          l_sec := 'Step 6 : Error getting time from dual' || chr(10) ||
                   SQLERRM;
          RAISE program_error;
      END;
    
      l_message := 'Running Sequence process';
      l_message := l_message;
    
      fnd_file.put_line(fnd_file.log, l_message);
      l_req_id := apps.fnd_request.submit_request('XXCUS'
                                                 ,'XXCUSHRPSSEQ'
                                                 ,NULL
                                                 ,SYSDATE
                                                 ,FALSE);
      COMMIT;
    
      IF (l_req_id != 0)
      THEN
        IF fnd_concurrent.wait_for_request(l_req_id
                                          ,6
                                          ,15000
                                          ,l_phase
                                          ,l_status
                                          ,l_dev_phase
                                          ,l_dev_status
                                          ,l_message)
        THEN
          l_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                             l_dev_phase || ' DStatus ' || l_dev_status ||
                             chr(10) || ' MSG - ' || l_message;
          -- Error Returned
          IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
          THEN
            l_statement := 'An error occured in the running the HDS HR PEOPLESOFT CREATE SEQUENCE process' ||
                           l_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_statement);
            fnd_file.put_line(fnd_file.output, l_statement);
            RAISE program_error;
          
          END IF;
          -- Then Success!
        ELSE
          l_statement := 'An error occured running the HDS HR PEOPLESOFT CREATE SEQUENCE process' ||
                         l_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
      
      ELSE
        l_statement := 'An error occured when trying to submitting the HDS HR PEOPLESOFT CREATE SEQUENCE process';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      END IF;
    END;
  
    -- Get time End
    BEGIN
      SELECT SYSDATE INTO l_time_end FROM dual;
    EXCEPTION
      WHEN OTHERS THEN
        l_sec := 'Step 6 : Error getting time end from dual' || chr(10) ||
                 SQLERRM;
        RAISE program_error;
    END;
  
    l_time_lapse := trunc((l_time_end - l_time_start) * 1440, 0);
    l_statement  := 'Completed Step 6: ' || l_message || ' in ' ||
                    l_time_lapse || ' minutes' || chr(10);
  
    fnd_file.put_line(fnd_file.log, l_statement);
    fnd_file.put_line(fnd_file.output, l_statement);
  
    -- STEP 7.  copy sequenced STG table to copy with trx_id's and rows sorted.
    --dbms_output.put_line('STEP 7.  copy sequenced STG table to copy');
    -- Get time start
    BEGIN
      SELECT SYSDATE INTO l_time_start FROM dual;
    EXCEPTION
      WHEN OTHERS THEN
        l_sec := 'Step 7 : Error getting time from dual' || chr(10) ||
                 SQLERRM;
        RAISE program_error;
    END;
  
    l_message := 'Move worklist table data to Staging table';
    l_message := l_message;
    fnd_file.put_line(fnd_file.log, l_message);
  
    SELECT to_char(xxcushr_ps_worklist_s.nextval)
      INTO l_sequence
      FROM dual;
  
    FOR c_wklst_insert IN (SELECT seqlist.rowid, seqlist.*
                             FROM xxcus.xxcushr_worklist_tbl seqlist
                            WHERE seqlist.stg_processed = 'N'
                            ORDER BY seqlist.processing_sequence DESC)
    LOOP
    
      l_increment   := l_increment + 1;
      l_msg_session := l_msg_session_init || l_sequence;
    
      -- Update worklist data
      UPDATE xxcus.xxcushr_worklist_tbl seqlist2
         SET seqlist2.stg_trx_id = to_char(l_increment)
            ,seqlist2.msg_id     = l_msg_session
            ,seqlist2.session_id = l_msg_session
       WHERE seqlist2.rowid = c_wklst_insert.rowid;
    
      -- STEP 8.  Insert sequenced finalized worklist into staging table.
      --dbms_output.put_line('STEP 8.  Insert sequenced finalized worklist');
      -- Insert into staging
      INSERT INTO xxcus.xxcushr_interface_tbl
      VALUES
        (to_char(l_increment)
        ,c_wklst_insert.stg_status
        ,c_wklst_insert.stg_employee_number
        ,c_wklst_insert.stg_last_name
        ,c_wklst_insert.stg_first_name
        ,c_wklst_insert.stg_middle_names
        ,c_wklst_insert.stg_hire_date
        ,c_wklst_insert.stg_effective_start_date
        ,c_wklst_insert.stg_sex
        ,c_wklst_insert.stg_person_type
        ,c_wklst_insert.email_address
        ,c_wklst_insert.work_telephone
        ,c_wklst_insert.stg_attribute1
        ,c_wklst_insert.stg_attribute2
        ,c_wklst_insert.stg_attribute3
        ,c_wklst_insert.stg_attribute4
        ,c_wklst_insert.stg_attribute5
        ,c_wklst_insert.stg_attribute6
        ,c_wklst_insert.stg_attribute7
        ,c_wklst_insert.stg_attribute8
        ,c_wklst_insert.stg_attribute9
        ,c_wklst_insert.stg_process_flag
        ,c_wklst_insert.stg_number_of_records
        ,c_wklst_insert.creation_date
        ,c_wklst_insert.created_by
        ,c_wklst_insert.last_update_date
        ,c_wklst_insert.last_update_login
        ,l_msg_session
        ,l_msg_session
        ,c_wklst_insert.stg_error_comments
        ,c_wklst_insert.supervisor_id
        ,c_wklst_insert.job_class
        ,c_wklst_insert.job_class_code
        ,c_wklst_insert.stg_nt_login
        ,c_wklst_insert.stg_last_day_paid
        ,c_wklst_insert.processing_sequence);
    
      COMMIT;
    END LOOP;
    fnd_file.put_line(fnd_file.log
                     ,'Put ' || to_char(l_increment) ||
                      ' rows into worklist table');
    fnd_file.put_line(fnd_file.output
                     ,'Put ' || to_char(l_increment) ||
                      ' rows into worklist table');
  
    l_message := 'Inserting sequenced rows into staging table.';
    l_message := l_message;
    fnd_file.put_line(fnd_file.log, l_message);
  
    -- Get time End
    BEGIN
      SELECT SYSDATE INTO l_time_end FROM dual;
    EXCEPTION
      WHEN OTHERS THEN
        l_sec := 'Step 7 and 8 : Error getting time end from dual' ||
                 chr(10) || SQLERRM;
        RAISE program_error;
    END;
  
    l_time_lapse := trunc((l_time_end - l_time_start) * 1440, 0);
    l_statement  := 'Completed Steps 7 and 8: ' || l_message || ' in ' ||
                    l_time_lapse || ' minutes' || chr(10);
  
    fnd_file.put_line(fnd_file.log, l_statement);
    fnd_file.put_line(fnd_file.output, l_statement);
  
    -- STEP 9.  Run HR INTERFACE
    -- Call HR INTERFACE
    --dbms_output.put_line('STEP 9.  Run HR INTERFACE');
    BEGIN
      --RUN HR INTERFACE
    
      -- Get time start
      BEGIN
        SELECT SYSDATE INTO l_time_start FROM dual;
      EXCEPTION
        WHEN OTHERS THEN
          l_sec := 'Step 9 : Error getting time from dual' || chr(10) ||
                   SQLERRM;
          RAISE program_error;
      END;
    
      l_message := 'Running HR Interface';
      l_message := l_message;
    
      fnd_file.put_line(fnd_file.log, l_message);
      l_req_id := apps.fnd_request.submit_request(application => 'XXCUS'
                                                 ,program     => 'XXCUSHRINT'
                                                 ,description => NULL
                                                 ,start_time  => SYSDATE
                                                 ,sub_request => FALSE
                                                 ,argument1   => l_msg_session
                                                 ,argument2   => l_msg_session);
      COMMIT;
    
      IF (l_req_id != 0)
      THEN
        IF fnd_concurrent.wait_for_request(l_req_id
                                          ,6
                                          ,15000
                                          ,l_phase
                                          ,l_status
                                          ,l_dev_phase
                                          ,l_dev_status
                                          ,l_message)
        THEN
          l_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                             l_dev_phase || ' DStatus ' || l_dev_status ||
                             chr(10) || ' MSG - ' || l_message;
        
          -- Error Returned
          IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
          THEN
            l_statement := 'An error occured in the running the HDS HR INTERFACE' ||
                           l_error_message || '.';
          
            fnd_file.put_line(fnd_file.log, l_statement);
            fnd_file.put_line(fnd_file.output, l_statement);
          
          END IF;
          -- Then Success!
        
          --  Update worklist table with results if successful
          FOR l_update IN (SELECT *
                             FROM xxcus.xxcushr_interface_tbl
                            WHERE msg_id = l_msg_session)
          LOOP
          
            UPDATE xxcus.xxcushr_worklist_tbl seqlist
               SET seqlist.stg_status             = l_update.stg_status
                  ,seqlist.stg_error_comments     = l_update.stg_error_comments
                  ,seqlist.stg_processed          = 'Y'
                  ,seqlist.stg_processed_datetime = SYSDATE
             WHERE seqlist.msg_id = l_msg_session
               AND seqlist.stg_trx_id = l_update.stg_trx_id;
          
          END LOOP;
        
        ELSE
          l_statement := 'An error occured running the HDS HR INTERFACE ' ||
                         l_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
        
        END IF;
      
      ELSE
        l_statement := 'An error occured when trying to submitting the HDS HR INTERFACE';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
      
      END IF;
    END;
  
    -- Get time End
    BEGIN
      SELECT SYSDATE INTO l_time_end FROM dual;
    EXCEPTION
      WHEN OTHERS THEN
        l_sec := 'Step 9 : Error getting time end from dual' || chr(10) ||
                 SQLERRM;
        RAISE program_error;
    END;
  
    l_time_lapse := trunc((l_time_end - l_time_start) * 1440, 0);
    l_statement  := 'Completed Step 9: ' || l_message || ' in ' ||
                    l_time_lapse || ' minutes' || chr(10);
  
    fnd_file.put_line(fnd_file.log, l_statement);
    fnd_file.put_line(fnd_file.output, l_statement);
  
    -- LAST STEP 10.  Finished HR_INTERFACE.  NOW copy current table to prior.
  
    -- RUN ORACLE_FILTER AGAIN!
  
    --Initialize variables.
    l_req_id        := 0;
    l_phase         := NULL;
    l_status        := NULL;
    l_dev_status    := NULL;
    l_dev_phase     := NULL;
    l_message       := NULL;
    l_error_message := NULL;
    --dbms_output.put_line('LAST STEP 10.  Finished HR_INTERFACE. ');
    BEGIN
      --RUN Filter
    
      -- Get time start
      BEGIN
        SELECT SYSDATE INTO l_time_start FROM dual;
      EXCEPTION
        WHEN OTHERS THEN
          l_sec := 'Step 10 : Error getting time from dual' || chr(10) ||
                   SQLERRM;
          RAISE program_error;
      END;
    
      l_message := 'Running Oracle Filter for prior day population';
      l_message := l_message;
    
      fnd_file.put_line(fnd_file.log, l_message);
      l_req_id := apps.fnd_request.submit_request('XXCUS'
                                                 ,'XXCUSHRORAFILTER'
                                                 ,NULL
                                                 ,SYSDATE
                                                 ,FALSE);
      COMMIT;
    
      IF (l_req_id != 0)
      THEN
        IF fnd_concurrent.wait_for_request(l_req_id
                                          ,6
                                          ,15000
                                          ,l_phase
                                          ,l_status
                                          ,l_dev_phase
                                          ,l_dev_status
                                          ,l_message)
        THEN
          l_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                             l_dev_phase || ' DStatus ' || l_dev_status ||
                             chr(10) || ' MSG - ' || l_message;
          -- Error Returned
          IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
          THEN
            l_statement := 'An error occured in the running the HDS HR ORACLE EMPLOYEE FILTER for prior day population' ||
                           l_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_statement);
            fnd_file.put_line(fnd_file.output, l_statement);
            RAISE program_error;
          
          END IF;
          -- Then Success!
        ELSE
          l_statement := 'An error occured running the HDS HR ORACLE EMPLOYEE FILTER for pior day' ||
                         l_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
      
      ELSE
        l_statement := 'An error occured when trying to submitting the HDS HR ORACLE EMPLOYEE FILTER for prior day population';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      END IF;
    END;
  
    EXECUTE IMMEDIATE 'truncate table xxcus.xxcushr_per_people_prior_tbl';
    EXECUTE IMMEDIATE 'truncate table xxcus.xxcushr_per_assign_prior_tbl';
    COMMIT;
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcushr_per_people_prior_tbl
      SELECT * FROM xxcus.xxcushr_per_people_curr_tbl;
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcushr_per_assign_prior_tbl
      SELECT * FROM xxcus.xxcushr_per_assign_curr_tbl;
  
    -- Get time End
    BEGIN
      SELECT SYSDATE INTO l_time_end FROM dual;
    EXCEPTION
      WHEN OTHERS THEN
        l_sec := 'Step 10 : Error getting time end from dual' || chr(10) ||
                 SQLERRM;
        RAISE program_error;
    END;
  
    l_time_lapse := trunc((l_time_end - l_time_start) * 1440, 0);
    l_statement  := 'Completed Step 10: ' || l_message || ' in ' ||
                    l_time_lapse || ' minutes' || chr(10);
  
    fnd_file.put_line(fnd_file.log, l_statement);
    fnd_file.put_line(fnd_file.output, l_statement);
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      p_retcode       := l_err_code;
      p_errbuf        := l_err_msg;
      l_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running PSHR package with PROGRAM ERROR'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'HR');
    
      fnd_file.put_line(fnd_file.output, 'Now go fix the dango error!');
    
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      p_retcode       := l_err_code;
      p_errbuf        := l_err_msg;
      l_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => 'Error running PSHR package with OTHERS Exception'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'HR');
    
  END;

  /*******************************************************************************
  * Procedure:   Employee Import
  * Description: Import employee data from hdscmmn.hr_employee_all_vw
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     19-APR-2011   Kathy Poling    Initial creation of the procedure copied 
                                        from Luong Vu in R11 Version 1.1
  1.1     23-Feb-2012   Kathy Poling    Removed code to bring inactive employees
                                        from 1-1-2012 including some older the 
                                        credit card transactions 
  1.2     20-Aug-2012   Kathy Poling    Added columns to the staging table of employee 
                                        that can be used for Loss Prevention Reports
                                        in EIS.  Service Ticket 156630 
  1.3     13-Nov-2012   Kathy Poling    RFC# 35198  Service# 178471 changed to 
                                        include Joe DeAngelo for Loss Prevention Reports  
  1.4     01-May-2013   Kathy Poling    SR 201989 Task ID: 20130402-01503  change
                                        employee to use the <pref_first_name> <middle_init> 
                                        and <last_name>.
  1.5     21-Jun-2013   Luong Vu        Change lookup HDS_HR-INT PS-ORA query to use
                                        Meaning field instead of Description field for
                                        Canada OIE implementation     
                                        Change lookup HDS_HR_EMP_DEPT query to use description
                                        instead of meaning    
  1.6      27-Aug-2014    Vijaysrinivasan Updating the cost centers for FM per ESMS Ticket 260732
  1.7      10-Oct-2014   Kathy Poling    ESMS 266549 ERRORS ALERT cost center for WW
  1.9      07-Mar-2018   P.Vamshidhar    TMS#20180226-00396 - T and E Oversight Feed
                                         changed view owner name from apxcmmn to hdscmmn   
  ********************************************************************************/

  PROCEDURE ps_emp_import(p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER) IS
  
    --Intialize Variables
    l_err_msg   VARCHAR2(3000);
    l_err_code  NUMBER;
    l_sec       VARCHAR2(3000);
    l_emp_count NUMBER;
  
    --Main variables
    l_procedure_name VARCHAR2(75) := 'PS_EMP_IMPORT';
  
  BEGIN
    l_procedure_name := 'XXCUSHR_PS_INTERFACE_PKG.PS_EMP_IMPORT';
  
    --Truncate the Summary Table
    BEGIN
      l_sec := 'Truncate the summary table before loading employee from PS to ORA.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSHR_PS_EMP_ALL_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUS.XXCUSHR_PS_EMP_ALL_TBL : ' ||
                     SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
  
    --Truncate the Summary Table
    BEGIN
      l_sec := 'Truncate the summary table before loading employee from PS to ORA.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUSHR_PS_SPVR_ALL_TBL';
    
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate XXCUSHR_PS_SPVR_ALL_TBL : ' ||
                     SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
  
    l_sec := 'Importing employees from hdscmmn.hr_employee_all_vw';
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcushr_ps_emp_all_tbl
      (SELECT DISTINCT NULL trx_id
                      ,'NEW' status
                      ,TRIM(emp.emplid) employee_number
                      ,TRIM(emp.last_name) last_name
                       --,TRIM(emp.first_name) first_name      Version 1.4 
                       --,TRIM(emp.middle_name) middle_names   Version 1.4
                      ,TRIM(emp.pref_first_name) first_name --Version 1.4
                      ,TRIM(emp.middle_init) middle_names --Version 1.4
                      ,to_char(emp.last_hire_dt, 'MMDDYYYY') hire_date
                      ,to_char(trunc(SYSDATE), 'MMDDYYYY') effective_start_date
                      ,CASE emp.emp_sex
                         WHEN 'F' THEN
                          emp.emp_sex
                         WHEN 'M' THEN
                          emp.emp_sex
                         ELSE
                          'M'
                       END
                      ,'Employee' person_type
                      ,TRIM(emp.email_addr) email_address
                      ,TRIM(emp.work_phone) work_telephone
                      ,TRIM(emp.fru) attribute1
                      ,to_char(emp.termination_dt, 'MMDDYYYY') attribute2
                      ,'100' attribute3
                      ,NULL attribute4
                      ,NULL attribute5
                      ,NULL attribute6
                      ,NULL attribute7
                      ,NULL attribute8
                       /*,CASE TRIM(emp.lob)
                         WHEN 'INT' THEN
                          decode(emp.native_dept, 'NA', NULL,
                                 emp.native_dept)
                         WHEN 'FMA' THEN
                          decode(emp.native_dept, 'NA', NULL,
                                 emp.native_dept)
                         ELSE
                          NULL
                       END */
                      ,(CASE
                         WHEN TRIM(emp.business_unit) IN
                              (SELECT lookup_code
                                 FROM apps.fnd_lookup_values b
                                WHERE b.lookup_type = 'HDS_HR_EMP_DEPT' -- Created new set of values                       
                                  AND b.lookup_code = TRIM(emp.business_unit)
                                  AND b.enabled_flag = 'Y'
                                  AND nvl(b.end_date_active, SYSDATE) >=
                                      SYSDATE) THEN
                          (SELECT description --meaning      --V1.5      
                             FROM apps.fnd_lookup_values b
                            WHERE b.lookup_type = 'HDS_HR_EMP_DEPT' -- Created new set of values                       
                              AND b.lookup_code = TRIM(emp.business_unit)
                              AND b.enabled_flag = 'Y'
                              AND nvl(b.end_date_active, SYSDATE) >= SYSDATE) ||
                          (CASE     --Version 1.7 add case statement
                            WHEN length(emp.native_dept) > 3 THEN
                             --substr(emp.native_dept, -3) --Version 1.6
                               SUBSTR(emp.native_dept,1,3)                      --commented above and added this for ver# 1.8 
                            ELSE
                             emp.native_dept
                          END)
                         ELSE
                          '0000'
                       END) attribute9
                      ,TRIM(emp.hr_status) process_flag
                      ,NULL
                      ,to_char(TRIM(emp.creation_date)) creation_date
                      ,NULL created_by
                      ,NULL last_update_date
                      ,NULL last_update_login
                      ,NULL msg_id
                      ,NULL session_id
                      ,NULL error_comments
                      ,TRIM(emp.spvr_employee1) supervisor_id
                      ,substr(TRIM(emp.job_subfunc) || '-' ||
                              TRIM(emp.job_descr)
                             ,1
                             ,50) job_class
                      ,TRIM(emp.job_family) job_class_code
                      ,xxcus_misc_pkg.get_ccid(TRIM(emp.fru)
                                              ,TRIM(decode(emp.native_dept
                                                          ,'NA'
                                                          ,NULL
                                                          ,emp.native_dept))) code_combination_id_new
                      ,TRIM(emp.ntid)
                      ,TRIM(emp.last_day_worked) last_day_paid
                      ,TRIM(emp.business_unit) process_lev
                      ,TRIM(emp.job_descr) job_descr
                      ,TRIM(emp.job_family) job_family
                      ,TRIM(emp.job_family_descr) job_family_descr
         --FROM apxcmmn.hr_employee_all_vw@apxprd_lnk.hsi.hughessupply.com emp  commented in 1.9
		 FROM hdscmmn.hr_employee_all_vw@apxprd_lnk_hr.hsi.hughessupply.com emp  -- added in 1.9
        WHERE --emp.emplid <> '26191' -- Filter out Joe DeAngelo  --Version 1.3
        TRIM(emp.business_unit) IN
        (SELECT meaning --description           -- v 1.5   
           FROM apps.fnd_lookup_values b
          WHERE b.lookup_type = 'HDS_HR-INT PS-ORA' -- Created new set of values
               --AND b.meaning LIKE 'ORACLE_SYSTEM_%'
               --AND b.description = TRIM(emp.business_unit)
            AND b.meaning = TRIM(emp.business_unit)
            AND b.enabled_flag = 'Y'
            AND nvl(b.end_date_active, SYSDATE) >= SYSDATE));
  
    COMMIT;
  
    -- For conversion only need to bring over all employee regardless of status as of a certain cut off date
    -- need to remove after conversion
    /*   2/23/2012    16:56 removed conversion complete       --Version 1.1
      UPDATE xxcus.xxcushr_ps_emp_all_tbl
         SET process_flag = 'A', last_day_paid = NULL
       WHERE process_flag = 'I'
         AND last_day_paid > '31-Dec-2011'
          OR employee_number IN ('30047'
                                ,'51031'
                                ,'30008'
                                ,'53362'
                                ,'32481'
                                ,'30662'
                                ,'53687'
                                ,'26787'
                                ,'52151'
                                ,'710'
                                ,'16332'
                                ,'28468'
                                ,'41860'
                                ,'43177'
                                ,'48723'
                                ,'30834'
                                ,'26680'
                                ,'48356'
                                ,'43279'
                                ,'53951'
                                ,'36425'
                                ,'31524'
                                ,'43280'
                                ,'33010'
                                ,'48150'
                                ,'53376'
                                ,'34259'
                                ,'53766'
                                ,'719'
                                ,'15590'
                                ,'36715'
                                ,'35362'
                                ,'33114');
    
      COMMIT;
    */
    l_sec := 'Employee count is less than 1';
  
    --  Check connection to hdscmmn employee view
    SELECT COUNT(*) INTO l_emp_count FROM xxcus.xxcushr_ps_emp_all_tbl;
  
    IF l_emp_count < 1
    THEN
      RAISE program_error;
    END IF;
  
    l_sec := 'Importing supervisor from hdscmmn.hr_employee_all_vw';
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcushr_ps_spvr_all_tbl
      (SELECT to_char(emplid)
             ,TRIM(fru)
             ,TRIM(last_name)
             ,TRIM(first_name)
             ,TRIM(spvr_employee1)
             ,TRIM(spvr_employee2)
             ,TRIM(spvr_employee3)
             ,TRIM(spvr_employee4)
             ,TRIM(spvr_employee5)
             ,TRIM(spvr_employee6)
             ,TRIM(spvr_employee7)
             ,TRIM(spvr_employee8)
         --FROM apxcmmn.hr_employee_all_vw@apxprd_lnk.hsi.hughessupply.com spvr commented in 1.9
		 FROM hdscmmn.hr_employee_all_vw@apxprd_lnk_hr.hsi.hughessupply.com spvr   -- added in 1.9
        WHERE TRIM(spvr.business_unit) IN
              (SELECT description
                 FROM apps.fnd_lookup_values b
                WHERE b.lookup_type = 'HDS_HR-INT PS-ORA' -- Created new set of values
                  AND b.meaning LIKE 'ORACLE_SYSTEM_%'
                  AND b.description = TRIM(spvr.business_unit)
                  AND b.enabled_flag = 'Y'
                  AND nvl(b.end_date_active, SYSDATE) >= SYSDATE));
  
    COMMIT;
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_sec || ' Error: ' || l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      p_retcode := l_err_code;
      p_errbuf  := l_err_msg;
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      p_retcode := l_err_code;
      p_errbuf  := l_err_msg;
  END ps_emp_import;

  -- -----------------------------------------------------------------------------
  -- |----------------------------< Oracle_filter  >-----------------------------|
  -- -----------------------------------------------------------------------------
  --
  -- {Start Of Comments}
  --
  -- Description:
  --   This is the procedure that filters Oracle to current day.
  --   This process will be called back by the Oracle_activity_Logger later on.
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- ---------------------------------
  -- 1.0     19-Apr-2011   Kathy Poling     Initial creation of the procedure copied 
  --                                        from Manny R11
  -- 1.1     24-Mar-2014   Kathy Poling     Change to assignment table.  Shared install
  --                                        employee will only have one record.
  -- -----------------------------------------------------------------------------

  PROCEDURE oracle_filter(p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER) IS
  
  BEGIN
  
    --Clear contents of the current table
  
    EXECUTE IMMEDIATE 'truncate table XXCUS.xxcushr_per_people_curr_tbl';
    EXECUTE IMMEDIATE 'truncate table xxcus.xxcushr_per_assign_curr_tbl';
    COMMIT;
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcushr_per_people_curr_tbl
      SELECT *
        FROM hr.per_all_people_f
       WHERE effective_end_date = '31-DEC-4712';
  
    INSERT /*+ APPEND */
    INTO xxcus.xxcushr_per_assign_curr_tbl
      SELECT * FROM hr.per_all_assignments_f;
    -- WHERE effective_end_date = '31-DEC-4712';   Version 1.1  3/24/14
  
  EXCEPTION
    WHEN OTHERS THEN
      l_err_code := 2;
      l_err_msg  := ' ERROR ' || SQLCODE || substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      p_retcode := l_err_code;
      p_errbuf  := l_err_msg;
    
  END;

  -- -----------------------------------------------------------------------------
  -- |----------------------------< create_worklist  >---------------------------|
  -- -----------------------------------------------------------------------------
  --
  -- {Start Of Comments}
  --
  -- Description:
  --   This is the procedure that creates a worklist by comparing the current
  --   day activity for both PeopleSoft and Oracle.
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- ---------------------------------
  -- 1.0     19-Apr-2011   Kathy Poling     Initial creation of the procedure copied 
  --                                        from Manny R11 and include changes thru 
  --                                        version 1.4 from R11

  PROCEDURE create_worklist(p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER) IS
  
  BEGIN
    --dbms_output.put_line('IN BEGIN');
    -- Create new 'N' records.  
    --dbms_output.put_line('START NEW REC');
    INSERT INTO xxcus.xxcushr_worklist_tbl
      (SELECT ps.trx_id
             ,ps.status
             ,ps.employee_number
             ,ps.last_name
             ,ps.first_name
             ,ps.middle_names
             ,ps.hire_date
             ,ps.effective_start_date
             ,nvl(REPLACE(ps.sex, ' '), 'M')
             ,ps.person_type
             ,ps.email_address
             ,ps.work_telephone
             ,ps.attribute1
             ,ps.attribute2
             ,ps.attribute3
             ,ps.attribute4
             ,ps.attribute5
             ,ps.attribute6
             ,ps.attribute7
             ,ps.attribute8
             ,ps.attribute9
             ,'N'
             ,ps.number_of_records
             ,to_char(SYSDATE, 'MMDDYYYY')
             ,ps.created_by
             ,ps.last_update_date
             ,ps.last_update_login
             ,ps.msg_id
             ,ps.session_id
             ,ps.error_comments
             ,ps.supervisor_id
             ,ps.job_class
             ,ps.job_class_code
             ,NULL
             ,'N'
             ,NULL
             ,'N'
             ,NULL
             ,ps.nt_id
             ,ps.last_day_paid
         FROM xxcus.xxcushr_ps_emp_all_tbl ps
        WHERE ps.employee_number NOT IN
              (SELECT orac.employee_number
                 FROM xxcus.xxcushr_per_people_curr_tbl orac
                WHERE orac.employee_number = ps.employee_number)
          AND ps.process_flag <> 'I');
  
    COMMIT;
    --dbms_output.put_line('END NEW REC');
    -- insert terminate 'T' records
  
    INSERT INTO xxcus.xxcushr_worklist_tbl
      (SELECT ps.trx_id
             ,ps.status
             ,ps.employee_number
             ,ps.last_name
             ,ps.first_name
             ,ps.middle_names
             ,ps.hire_date
             ,ps.effective_start_date
             ,nvl(REPLACE(ps.sex, ' '), 'M')
             ,ps.person_type
             ,ps.email_address
             ,ps.work_telephone
             ,ps.attribute1
             ,ps.attribute2
             ,ps.attribute3
             ,ps.attribute4
             ,ps.attribute5
             ,ps.attribute6
             ,ps.attribute7
             ,ps.attribute8
             ,ps.attribute9
             ,'T'
             ,ps.number_of_records
             ,to_char(SYSDATE, 'MMDDYYYY')
             ,ps.created_by
             ,ps.last_update_date
             ,ps.last_update_login
             ,ps.msg_id
             ,ps.session_id
             ,ps.error_comments
             ,ps.supervisor_id
             ,ps.job_class
             ,ps.job_class_code
             ,NULL
             ,'N'
             ,NULL
             ,'N'
             ,NULL
             ,ps.nt_id
             ,ps.last_day_paid
         FROM xxcus.xxcushr_ps_emp_all_tbl ps
        WHERE ps.employee_number IN
              (SELECT orac.employee_number
                 FROM xxcus.xxcushr_per_people_curr_tbl orac
                WHERE orac.person_type_id = 6
                  AND orac.employee_number = ps.employee_number)
          AND ps.process_flag = 'I');
    COMMIT;
    --dbms_output.put_line('END TERM REC');
    -- insert Rehire 'R' records
  
    INSERT INTO xxcus.xxcushr_worklist_tbl
      (SELECT ps.trx_id
             ,ps.status
             ,ps.employee_number
             ,ps.last_name
             ,ps.first_name
             ,ps.middle_names
             ,ps.hire_date
             ,ps.effective_start_date
             ,nvl(REPLACE(ps.sex, ' '), 'M')
             ,ps.person_type
             ,ps.email_address
             ,ps.work_telephone
             ,ps.attribute1
             ,ps.attribute2
             ,ps.attribute3
             ,ps.attribute4
             ,ps.attribute5
             ,ps.attribute6
             ,ps.attribute7
             ,ps.attribute8
             ,ps.attribute9
             ,'R'
             ,ps.number_of_records
             ,to_char(SYSDATE, 'MMDDYYYY')
             ,ps.created_by
             ,ps.last_update_date
             ,ps.last_update_login
             ,ps.msg_id
             ,ps.session_id
             ,ps.error_comments
             ,ps.supervisor_id
             ,ps.job_class
             ,ps.job_class_code
             ,NULL
             ,'N'
             ,NULL
             ,'N'
             ,NULL
             ,ps.nt_id
             ,ps.last_day_paid
         FROM xxcus.xxcushr_ps_emp_all_tbl ps
        WHERE ps.employee_number IN
              (SELECT orac.employee_number
                 FROM xxcus.xxcushr_per_people_curr_tbl orac
                WHERE orac.person_type_id = 9
                  AND orac.employee_number = ps.employee_number)
          AND ps.process_flag = 'A');
    COMMIT;
    --dbms_output.put_line('END REHIRE REC');
    -- insert Update 'U' records
  
    INSERT INTO xxcus.xxcushr_worklist_tbl
      (SELECT ps.trx_id
             ,ps.status
             ,ps.employee_number
             ,ps.last_name
             ,ps.first_name
             ,ps.middle_names
             ,ps.hire_date
             ,ps.effective_start_date
             ,nvl(REPLACE(ps.sex, ' '), 'M')
             ,ps.person_type
             ,ps.email_address
             ,ps.work_telephone
             ,ps.attribute1
             ,ps.attribute2
             ,ps.attribute3
             ,ps.attribute4
             ,ps.attribute5
             ,ps.attribute6
             ,ps.attribute7
             ,ps.attribute8
             ,ps.attribute9
             ,'U'
             ,ps.number_of_records
             ,to_char(SYSDATE, 'MMDDYYYY')
             ,ps.created_by
             ,ps.last_update_date
             ,ps.last_update_login
             ,ps.msg_id
             ,ps.session_id
             ,ps.error_comments
             ,ps.supervisor_id
             ,ps.job_class
             ,ps.job_class_code
             ,NULL
             ,'N'
             ,NULL
             ,'N'
             ,NULL
             ,ps.nt_id
             ,ps.last_day_paid
         FROM xxcus.xxcushr_ps_emp_all_tbl ps
        WHERE ps.employee_number IN
              (SELECT orac.employee_number
                 FROM xxcus.xxcushr_per_people_curr_tbl orac
                     ,xxcus.xxcushr_per_assign_curr_tbl oraac
                WHERE orac.employee_number = ps.employee_number
                  AND orac.person_id = oraac.person_id
                  AND orac.person_type_id = 6
                  AND (nvl(upper(ps.last_name), upper(orac.last_name)) <>
                      upper(orac.last_name) OR
                      nvl(ps.first_name, orac.first_name) <>
                      orac.first_name OR
                      nvl(ps.middle_names, orac.middle_names) <>
                      orac.middle_names OR
                      nvl(ps.sex, orac.sex) <> orac.sex OR
                      nvl(ps.email_address, orac.email_address) <>
                      nvl(orac.email_address, 'x') OR
                      nvl(ps.attribute1, orac.attribute1) <>
                      nvl(orac.attribute1, 'x') OR
                      nvl(ps.attribute9, orac.attribute5) <>
                      nvl(orac.attribute5, 'x') OR
                      nvl(ps.work_telephone, orac.work_telephone) <>
                      (SELECT phone_number
                          FROM hr.per_phones
                         WHERE parent_id = orac.person_id
                           AND parent_table = 'PER_ALL_PEOPLE_F'
                           AND date_to IS NULL) OR
                      nvl(ps.code_combination_id
                          ,oraac.default_code_comb_id) <>
                      nvl(oraac.default_code_comb_id, 999) OR
                      nvl(ps.supervisor_id, 'x') <>
                      nvl((SELECT employee_number
                             FROM xxcus.xxcushr_per_people_curr_tbl
                            WHERE person_id = oraac.supervisor_id)
                          ,'x')))
          AND ps.process_flag = 'A');
    COMMIT;
    --dbms_output.put_line('END UPDATE REC');
  EXCEPTION
    WHEN OTHERS THEN
      l_err_code := 2;
      l_err_msg  := ' ERROR ' || SQLCODE || substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      p_retcode := l_err_code;
      p_errbuf  := l_err_msg;
    
  END;

  -- -----------------------------------------------------------------------------
  -- |----------------------------< Check_Supervisor  >---------------------------|
  -- -----------------------------------------------------------------------------
  --
  -- {Start Of Comments}
  --
  -- Description:
  --   This procedure uses the supervisor table to create a sequence and final
  --   worklist table.
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- ---------------------------------
  --  1.0    19-Apr-2011    Kathy Poling    Initial creation of the procedure copied 
  --                                        from Jason Sloan R11

  PROCEDURE check_supervisor(p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER) IS
  
    --Intialize Variables
    l_err_msg  VARCHAR2(2000);
    l_err_code NUMBER;
    l_sec      VARCHAR2(150);
  
    --Main variables
    l_procedure_name VARCHAR2(75) := 'XXCUS_PSHR_INTERFACE_PKG.Check_Supervisor';
  
    l_sup_found  VARCHAR2(10);
    l_sup_rank   NUMBER := 0;
    l_sup_return VARCHAR2(10);
    l_boolean    BOOLEAN;
  
    l_sup2_num   VARCHAR2(30);
    l_sup2_found VARCHAR2(10);
    l_sup3_num   VARCHAR2(30);
    l_sup3_found VARCHAR2(10);
    l_sup4_num   VARCHAR2(30);
    l_sup4_found VARCHAR2(10);
    l_sup5_num   VARCHAR2(30);
    l_sup5_found VARCHAR2(10);
    l_sup6_num   VARCHAR2(30);
    l_sup6_found VARCHAR2(10);
    l_sup7_num   VARCHAR2(30);
    l_sup7_found VARCHAR2(10);
    l_sup8_num   VARCHAR2(30);
    l_sup8_found VARCHAR2(10);
  
  BEGIN
    FOR c_seqeuence IN (SELECT a.rowid, a.*
                          FROM xxcus.xxcushr_worklist_tbl a
                         WHERE a.processed = 'N')
    LOOP
      NULL;
    
      l_sup_found := 'N';
    
      xxcushr_ps_interface_pkg.check_sup(c_seqeuence.supervisor_id
                                        ,l_sup_return);
    
      IF l_sup_return = 'Y'
      THEN
        l_sup_found := 'Y';
      ELSE
        l_sup_found := 'N';
      END IF;
    
      IF l_sup_found = 'Y'
      THEN
        l_sup_found := 'Y';
      ELSE
      
        BEGIN
          SELECT u.sup2_number
                ,u.sup3_number
                ,u.sup4_number
                ,u.sup5_number
                ,u.sup6_number
                ,u.sup7_number
                ,u.sup8_number
            INTO l_sup2_num
                ,l_sup3_num
                ,l_sup4_num
                ,l_sup5_num
                ,l_sup6_num
                ,l_sup7_num
                ,l_sup8_num
            FROM xxcus.xxcushr_ps_spvr_all_tbl u
           WHERE u.employee_number = c_seqeuence.stg_employee_number
             AND rownum < 2;
        EXCEPTION
          WHEN no_data_found THEN
            l_sup_found := 'N';
        END;
      
        l_sup_return := 'N';
      
        xxcushr_ps_interface_pkg.check_sup(l_sup2_num, l_sup_return);
      
        IF l_sup_return = 'Y'
        THEN
          UPDATE xxcus.xxcushr_worklist_tbl ff
             SET ff.supervisor_id = l_sup2_num
           WHERE ff.rowid = c_seqeuence.rowid;
        
          COMMIT;
        ELSE
          l_sup_return := 'N';
        
          xxcushr_ps_interface_pkg.check_sup(l_sup3_num, l_sup_return);
        
          IF l_sup_return = 'Y'
          THEN
            UPDATE xxcus.xxcushr_worklist_tbl ff
               SET ff.supervisor_id = l_sup3_num
             WHERE ff.rowid = c_seqeuence.rowid;
          
            COMMIT;
          ELSE
            l_sup_return := 'N';
          
            xxcushr_ps_interface_pkg.check_sup(l_sup4_num, l_sup_return);
          
            IF l_sup_return = 'Y'
            THEN
              UPDATE xxcus.xxcushr_worklist_tbl ff
                 SET ff.supervisor_id = l_sup4_num
               WHERE ff.rowid = c_seqeuence.rowid;
            
              COMMIT;
            ELSE
              l_sup_return := 'N';
            
              xxcushr_ps_interface_pkg.check_sup(l_sup5_num, l_sup_return);
            
              IF l_sup_return = 'Y'
              THEN
                UPDATE xxcus.xxcushr_worklist_tbl ff
                   SET ff.supervisor_id = l_sup5_num
                 WHERE ff.rowid = c_seqeuence.rowid;
              
                COMMIT;
              ELSE
                l_sup_return := 'N';
              
                xxcushr_ps_interface_pkg.check_sup(l_sup6_num
                                                  ,l_sup_return);
              
                IF l_sup_return = 'Y'
                THEN
                  UPDATE xxcus.xxcushr_worklist_tbl ff
                     SET ff.supervisor_id = l_sup6_num
                   WHERE ff.rowid = c_seqeuence.rowid;
                
                  COMMIT;
                ELSE
                  l_sup_return := 'N';
                
                  xxcushr_ps_interface_pkg.check_sup(l_sup7_num
                                                    ,l_sup_return);
                
                  IF l_sup_return = 'Y'
                  THEN
                    UPDATE xxcus.xxcushr_worklist_tbl ff
                       SET ff.supervisor_id = l_sup7_num
                     WHERE ff.rowid = c_seqeuence.rowid;
                  
                    COMMIT;
                  ELSE
                    l_sup_return := 'N';
                  
                    xxcushr_ps_interface_pkg.check_sup(l_sup8_num
                                                      ,l_sup_return);
                  
                    IF l_sup_return = 'Y'
                    THEN
                      UPDATE xxcus.xxcushr_worklist_tbl ff
                         SET ff.supervisor_id = l_sup8_num
                       WHERE ff.rowid = c_seqeuence.rowid;
                    
                      COMMIT;
                    END IF;
                  END IF;
                END IF;
              END IF;
            END IF;
          END IF;
        END IF;
      END IF;
    
      -- If Supervisor update was used, check to see if new change already matches oracle.
      -- Delete row since now update is neccessary.
      IF l_sup_found = 'N'
      THEN
        DELETE FROM xxcus.xxcushr_worklist_tbl wl
         WHERE wl.stg_employee_number IN
               (SELECT orac.employee_number
                  FROM xxcus.xxcushr_per_people_curr_tbl orac
                      ,xxcus.xxcushr_per_assign_curr_tbl oraac
                 WHERE orac.employee_number = wl.stg_employee_number
                   AND orac.person_id = oraac.person_id(+)
                   AND wl.supervisor_id =
                       (SELECT employee_number
                          FROM xxcus.xxcushr_per_people_curr_tbl
                         WHERE person_id = oraac.supervisor_id))
           AND wl.stg_process_flag <> 'T'
           AND wl.processed = 'N'
           AND wl.stg_employee_number = c_seqeuence.stg_employee_number;
      
      END IF;
    
    END LOOP;
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Error in program ' || l_procedure_name || ' Error: ' ||
                    l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      p_retcode := l_err_code;
      p_errbuf  := l_err_msg;
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      p_retcode := l_err_code;
      p_errbuf  := l_err_msg;
  END;

  -- -----------------------------------------------------------------------------
  -- |----------------------------< Create_sequence  >---------------------------|
  -- -----------------------------------------------------------------------------
  --
  -- {Start Of Comments}
  --
  -- Description:
  --   This procedure uses the supervisor table to create a sequence and final
  --   worklist table.
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- ---------------------------------
  --  1.0    19-Apr-2011    Kathy Poling    Initial creation of the procedure copied 
  --                                        from Jason Sloan R11

  PROCEDURE create_sequence(p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER) IS
  
    l_found    VARCHAR2(10);
    l_sup_rank NUMBER := 0;
  
  BEGIN
    FOR c_seqeuence IN (SELECT a.rowid, a.*
                          FROM xxcus.xxcushr_worklist_tbl a
                         WHERE a.processed = 'N'
                            OR a.processed IS NULL)
    LOOP
      NULL;
    
      l_found := 'N';
    
      BEGIN
        SELECT 'Y', 8
          INTO l_found, l_sup_rank
          FROM xxcus.xxcushr_ps_spvr_all_tbl t
         WHERE t.sup8_number = c_seqeuence.supervisor_id
           AND rownum < 2;
      EXCEPTION
        WHEN no_data_found THEN
          l_found := 'N';
      END;
    
      IF l_found = 'N'
      THEN
        BEGIN
          SELECT 'Y', 7
            INTO l_found, l_sup_rank
            FROM xxcus.xxcushr_ps_spvr_all_tbl t
           WHERE t.sup7_number = c_seqeuence.supervisor_id
             AND rownum < 2;
        EXCEPTION
          WHEN no_data_found THEN
            l_found := 'N';
        END;
      END IF;
    
      IF l_found = 'N'
      THEN
        BEGIN
          SELECT 'Y', 6
            INTO l_found, l_sup_rank
            FROM xxcus.xxcushr_ps_spvr_all_tbl t
           WHERE t.sup6_number = c_seqeuence.supervisor_id
             AND rownum < 2;
        EXCEPTION
          WHEN no_data_found THEN
            l_found := 'N';
        END;
      END IF;
    
      IF l_found = 'N'
      THEN
        BEGIN
          SELECT 'Y', 5
            INTO l_found, l_sup_rank
            FROM xxcus.xxcushr_ps_spvr_all_tbl t
           WHERE t.sup5_number = c_seqeuence.supervisor_id
             AND rownum < 2;
        EXCEPTION
          WHEN no_data_found THEN
            l_found := 'N';
        END;
      END IF;
    
      IF l_found = 'N'
      THEN
        BEGIN
          SELECT 'Y', 4
            INTO l_found, l_sup_rank
            FROM xxcus.xxcushr_ps_spvr_all_tbl t
           WHERE t.sup4_number = c_seqeuence.supervisor_id
             AND rownum < 2;
        EXCEPTION
          WHEN no_data_found THEN
            l_found := 'N';
        END;
      END IF;
    
      IF l_found = 'N'
      THEN
        BEGIN
          SELECT 'Y', 3
            INTO l_found, l_sup_rank
            FROM xxcus.xxcushr_ps_spvr_all_tbl t
           WHERE t.sup3_number = c_seqeuence.supervisor_id
             AND rownum < 2;
        EXCEPTION
          WHEN no_data_found THEN
            l_found := 'N';
        END;
      END IF;
    
      IF l_found = 'N'
      THEN
        BEGIN
          SELECT 'Y', 2
            INTO l_found, l_sup_rank
            FROM xxcus.xxcushr_ps_spvr_all_tbl t
           WHERE t.sup2_number = c_seqeuence.supervisor_id
             AND rownum < 2;
        EXCEPTION
          WHEN no_data_found THEN
            l_found := 'N';
        END;
      END IF;
    
      IF l_found = 'N'
      THEN
        BEGIN
          SELECT 'Y', 1
            INTO l_found, l_sup_rank
            FROM xxcus.xxcushr_ps_spvr_all_tbl t
           WHERE t.sup1_number = c_seqeuence.supervisor_id
             AND rownum < 2;
        EXCEPTION
          WHEN no_data_found THEN
            l_found := 'N';
        END;
      END IF;
    
      UPDATE xxcus.xxcushr_worklist_tbl ff
         SET ff.processing_sequence = l_sup_rank
            ,ff.processed           = 'Y'
            ,ff.processed_datetime  = SYSDATE
       WHERE ff.rowid = c_seqeuence.rowid;
    
      COMMIT;
    
    END LOOP;
  END;

  -- -----------------------------------------------------------------------------
  -- |----------------------------< Check_Sup  >---------------------------|
  -- -----------------------------------------------------------------------------
  --
  -- {Start Of Comments}
  --
  -- Description:
  --   This procedure reviews the supervisor for all worklist employees and then
  --   validates if the supervisor is in Oracle or current feed if not the employee's
  --   supervisor will move to the next highest level.
  --
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- ---------------------------------
  --  1.0    19-Apr-2011    Kathy Poling    Initial creation of the procedure copied 
  --                                        from Jason Sloan R11

  PROCEDURE check_sup(p_sup IN VARCHAR2, p_found OUT VARCHAR2) IS
  
    l_sup_found VARCHAR2(10);
    l_sup_rank  NUMBER := 0;
  
  BEGIN
  
    l_sup_found := 'N';
  
    BEGIN
      SELECT 'Y'
        INTO l_sup_found
        FROM xxcus.xxcushr_per_people_curr_tbl j
       WHERE j.employee_number = p_sup
         AND j.person_type_id = 6
         AND rownum < 2;
    EXCEPTION
      WHEN no_data_found THEN
        l_sup_found := 'N';
        p_found     := l_sup_found;
    END;
  
    IF l_sup_found = 'Y'
    THEN
      l_sup_found := 'Y';
    ELSE
      BEGIN
        SELECT 'Y'
          INTO l_sup_found
          FROM xxcus.xxcushr_worklist_tbl j
         WHERE j.processed = 'N'
           AND j.stg_employee_number = p_sup
           AND rownum < 2;
      EXCEPTION
        WHEN no_data_found THEN
          l_sup_found := 'N';
          p_found     := l_sup_found;
      END;
    
    END IF;
  
    p_found := l_sup_found;
  END;

END xxcushr_ps_interface_pkg;
/
