 /****************************************************************************************************
  Table: TMS20170512-00037_Data_Fix.sql     
  Description: batch to insert  category_ids to XXWC.XXWC_MTL_INV_CATE_UPDATE_LOG load 4
  HISTORY
  ======================================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ---------------------------------------------------------------------------
  1.0     17-May-2017        P.Vamshidhar    Initial version TMS#20170512-00037  
                                             batch to insert  category_ids to XXWC.XXWC_MTL_INV_CATE_UPDATE_LOG load 4
  ***********************************************************************************************************************/
SET SERVEROUT ON
  
BEGIN
   INSERT INTO XXWC.XXWC_MTL_INV_CATE_UPDATE_LOG (CATEGORY_ID,
                                                  EXPN_ACCT,
                                                  PROCESS_STATUS,
                                                  CREATION_DATE)
      SELECT DISTINCT mic.category_id,
                      mcv.attribute8,
                      'NEW',
                      SYSDATE
        FROM apps.MTL_SYSTEM_ITEMS_B MIB,
             apps.mtl_parameters orgs,
             apps.MTL_ITEM_CATEGORIES_V mic,
             apps.FND_USER FUSE,
             apps.MTL_CATEGORIES_V mcv,
             apps.gl_code_combinations_kfv COGS,
             apps.gl_code_combinations_kfv SALES,
             apps.gl_code_combinations_kfv EXPENSE
       WHERE     mib.organization_id = orgs.organization_id
             AND mib.inventory_item_id = mic.inventory_item_id
             AND mib.organization_id = mic.organization_id
             AND mib.created_by = fuse.user_id(+)
             -- AND mib.item_type = 'INTANGIBLE'
             AND mic.category_concat_segs = MCV.CATEGORY_CONCAT_SEGS
             AND mcv.structure_name = 'Item Categories'
             AND mic.category_set_name = 'Inventory Category'
             AND orgs.master_organization_id = 222
             AND orgs.organization_code NOT LIKE ('9%')
             AND mib.COST_OF_SALES_ACCOUNT = cogs.code_combination_id
             AND mib.SALES_ACCOUNT = sales.code_combination_id
             AND mib.EXPENSE_ACCOUNT = expense.code_combination_id
             AND mib.inventory_item_status_code <> 'Inactive'
             AND Expense.segment4 <> mcv.attribute8
             AND mcv.category_id IN (2541,
                                     46686,
                                     23725,
                                     23691,
                                     2916,
                                     4133,
                                     4241,
                                     3540,
                                     3584,
                                     2680,
                                     4136,
                                     3556,
                                     3536,
                                     4086,
                                     2877,
                                     4191,
                                     3534,
                                     3814,
                                     4045,
                                     3803,
                                     3712,
                                     4087,
                                     2548,
                                     3710,
                                     2694,
                                     3537,
                                     4168,
                                     3617,
                                     3884,
                                     53685,
                                     3838,
                                     4212,
                                     3688,
                                     2514,
                                     3544,
                                     2531,
                                     3587,
                                     3999,
                                     3674,
                                     4195,
                                     3554,
                                     3555,
                                     18691,
                                     2522,
                                     2496,
                                     53688,
                                     3744,
                                     18684,
                                     2903,
                                     2682,
                                     18693,
                                     3583,
                                     12703,
                                     3739,
                                     3543,
                                     18688,
                                     4242,
                                     3988,
                                     2526,
                                     5685,
                                     18696,
                                     53691,
                                     4414,
                                     3535,
                                     5683,
                                     5686,
                                     3546,
                                     53686,
                                     18702,
                                     12711,
                                     3843,
                                     4373,
                                     18705,
                                     18700,
                                     4235,
                                     18686,
                                     12699,
                                     18692,
                                     5681,
                                     5684,
                                     12695,
                                     18697,
                                     12687,
                                     5687);

   DBMS_OUTPUT.PUT_LINE ('Number of rows inserted ' || SQL%ROWCOUNT);

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error occured ' || SUBSTR (SQLERRM, 1, 250));
      ROLLBACK;
END;
/