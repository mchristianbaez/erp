 /*
 TMS # 20151011-00015 - Restore records to new ISR table.
 By Manjula on 11-Oct-15 
 */
 SET SERVEROUTPUT ON SIZE 1000000
 DECLARE
 BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20151011-00015  , Before ISR Data Insert');
   
 INSERT /*+ append */ INTO XXWC.XXWC_ISR_DETAILS_ALL 
                                      (ORG,
                                       PRE,
                                       ITEM_NUMBER,
                                       VENDOR_NUM,
                                       VENDOR_NAME,
                                       SOURCE,
                                       ST,
                                       DESCRIPTION,
                                       CAT,
                                       PPLT,
                                       PLT,
                                       UOM,
                                       CL,
                                       STK_FLAG,
                                       FLIP_DATE,
                                       LAST_SV_CHANGE_DATE,                                       
                                       PM,
                                       MINN,
                                       MAXN,
                                       AMU,
                                       MF_FLAG,
                                       HIT6_STORE_SALES,
                                       HIT6_OTHER_INV_SALES,
                                       AVER_COST,
                                       ITEM_COST,
                                       BPA,
                                       QOH,
                                       AVAILABLE,
                                       AVAILABLEDOLLAR,
                                       ONE_STORE_SALE,
                                       SIX_STORE_SALE,
                                       TWELVE_STORE_SALE,
                                       ONE_OTHER_INV_SALE,
                                       SIX_OTHER_INV_SALE,
                                       TWELVE_OTHER_INV_SALE,
                                       BIN_LOC,
                                       MC,
                                       FI_FLAG,
                                       FREEZE_DATE,
                                       RES,
                                       THIRTEEN_WK_AVG_INV,
                                       THIRTEEN_WK_AN_COGS,
                                       TURNS,
                                       BUYER,
                                       TS,
                                       JAN_STORE_SALE,
                                       FEB_STORE_SALE,
                                       MAR_STORE_SALE,
                                       APR_STORE_SALE,
                                       MAY_STORE_SALE,
                                       JUN_STORE_SALE,
                                       JUL_STORE_SALE,
                                       AUG_STORE_SALE,
                                       SEP_STORE_SALE,
                                       OCT_STORE_SALE,
                                       NOV_STORE_SALE,
                                       DEC_STORE_SALE,
                                       JAN_OTHER_INV_SALE,
                                       FEB_OTHER_INV_SALE,
                                       MAR_OTHER_INV_SALE,
                                       APR_OTHER_INV_SALE,
                                       MAY_OTHER_INV_SALE,
                                       JUN_OTHER_INV_SALE,
                                       JUL_OTHER_INV_SALE,
                                       AUG_OTHER_INV_SALE,
                                       SEP_OTHER_INV_SALE,
                                       OCT_OTHER_INV_SALE,
                                       NOV_OTHER_INV_SALE,
                                       DEC_OTHER_INV_SALE,
                                       HIT4_STORE_SALES,
                                       HIT4_OTHER_INV_SALES,
                                       INVENTORY_ITEM_ID,
                                       ORGANIZATION_ID,
                                       SET_OF_BOOKS_ID,
                                       ORG_NAME,
                                       DISTRICT,
                                       REGION,
                                       COMMON_OUTPUT_ID,
                                       PROCESS_ID,
                                       INV_CAT_SEG1,
                                       ON_ORD,
                                       BPA_COST,
                                       OPEN_REQ,
                                       WT,
                                       FML,
                                       SOURCING_RULE,
                                       SO,
                                       SS,
                                       CLT,
                                       AVAIL2,
                                       INT_REQ,
                                       DIR_REQ,
                                       DEMAND,
                                       ITEM_STATUS_CODE,
                                       SITE_VENDOR_NUM,
                                       VENDOR_SITE,
                                       CORE,                                        
                                       TIER,
                                       CAT_SBA_OWNER,    
                                       MFG_PART_NUMBER,
                                       MST_VENDOR,                                       
                                       MAKE_BUY,
                                       ORG_ITEM_STATUS,                                       
                                       ORG_USER_ITEM_TYPE,                                           
                                       MST_ITEM_STATUS,                                               
                                       MST_USER_ITEM_TYPE,
                                       LAST_RECEIPT_DATE,                                       
                                       ONHAND_GT_270,                                           
                                       SUPERSEDE_ITEM,                                           
                                       ORG_ID,
                                       OPERATING_UNIT
                                       )
   SELECT ORG,
          PRE,
          ITEM_NUMBER,
          VENDOR_NUM,
          VENDOR_NAME,
          SOURCE,
          ST,
          DESCRIPTION,
          CAT,
          PPLT,
          PLT,
          UOM,
          CL,
          STK_FLAG,
          FLIP_DATE,
          LAST_SV_CHANGE_DATE,                    
          PM,
          MINN,
          MAXN,
          AMU,
          NULL,
          HIT6_STORE_SALES,
          HIT6_OTHER_INV_SALES,
          AVER_COST,
          ITEM_COST,
          BPA,
          QOH,
          AVAILABLE,
          AVAILABLEDOLLAR,
          ONE_STORE_SALE,
          SIX_STORE_SALE,
          TWELVE_STORE_SALE,
          ONE_OTHER_INV_SALE,
          SIX_OTHER_INV_SALE,
          TWELVE_OTHER_INV_SALE,
          BIN_LOC,
          NULL MC,
          FI_FLAG,
          NULL FREEZE_DATE,
          RES,
          NULL THIRTEEN_WK_AVG_INV,
          NULL THIRTEEN_WK_AN_COGS,
          0 TURNS,
          BUYER,
          TS,
          JAN_STORE_SALE,
          FEB_STORE_SALE,
          MAR_STORE_SALE,
          APR_STORE_SALE,
          MAY_STORE_SALE,
          JUN_STORE_SALE,
          JUL_STORE_SALE,
          AUG_STORE_SALE,
          SEP_STORE_SALE,
          OCT_STORE_SALE,
          NOV_STORE_SALE,
          DEC_STORE_SALE,
          JAN_OTHER_INV_SALE,
          FEB_OTHER_INV_SALE,
          MAR_OTHER_INV_SALE,
          APR_OTHER_INV_SALE,
          MAY_OTHER_INV_SALE,
          JUN_OTHER_INV_SALE,
          JUL_OTHER_INV_SALE,
          AUG_OTHER_INV_SALE,
          SEP_OTHER_INV_SALE,
          OCT_OTHER_INV_SALE,
          NOV_OTHER_INV_SALE,
          DEC_OTHER_INV_SALE,
          HIT4_STORE_SALES,
          HIT4_OTHER_INV_SALES,
          INVENTORY_ITEM_ID,
          ORGANIZATION_ID,
          SET_OF_BOOKS_ID,
          ORG_NAME,
          DISTRICT,
          REGION,
          NULL COMMON_OUTPUT_ID,
          NULL PROCESS_ID,
          INV_CAT_SEG1,
          ON_ORD,
          BPA_COST,
          OPEN_REQ,
          WT,
          FML,
          SOURCING_RULE,
          SO,
          SS,
          CLT,
          AVAIL2,
          INT_REQ,
          DIR_REQ,
          DEMAND,
          ITEM_STATUS_CODE,
          SITE_VENDOR_NUM,
          VENDOR_SITE,
          CORE,                                        
          TIER,
          CAT_SBA_OWNER,    
          MFG_PART_NUMBER,
          MST_VENDOR,                                       
          MAKE_BUY,
          ORG_ITEM_STATUS,                                       
          ORG_USER_ITEM_TYPE,                                           
          MST_ITEM_STATUS,                                               
          MST_USER_ITEM_TYPE,
          LAST_RECEIPT_DATE,                                       
          ONHAND_GT_270,                                           
          SUPERSEDE_ITEM,                                           
          ORG_ID,
          OPERATING_UNIT
     FROM XXWC.XXWC_ISR_DETAILS_ALL## ;     

DBMS_OUTPUT.put_line ('TMS: 20151008-00048  , After ISR Data Insert : Rows Inserted '||SQL%ROWCOUNT);

COMMIT;

EXCEPTION WHEN OTHERS THEN
   
	ROLLBACK;
    DBMS_OUTPUT.put_line ('TMS: 20151008-00048, Errors : '||SQLERRM);   
    
END;
/

    
     