--Report Name            : White Cap Credit Memo Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for White Cap Credit Memo Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_CREDIT_MEMO_DTLS_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V',222,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Credit Memo Dtls V','EXACMDV','','');
--Delete View Columns for EIS_XXWC_AR_CREDIT_MEMO_DTLS_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_CREDIT_MEMO_DTLS_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_CREDIT_MEMO_DTLS_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','TAKEN_BY',222,'Taken By','TAKEN_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Taken By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','MASTER_NAME',222,'Master Name','MASTER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Master Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','CUSTOMER_TERRITORY',222,'Customer Territory','CUSTOMER_TERRITORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Territory','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','SALES_REP_NO',222,'Sales Rep No','SALES_REP_NO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Rep No','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','QTY_SHIPPED',222,'Qty Shipped','QTY_SHIPPED','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty Shipped','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','INVOICE_LINE_AMOUNT',222,'Invoice Line Amount','INVOICE_LINE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Line Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','CREDIT_REASON_SALE',222,'Credit Reason Sale','CREDIT_REASON_SALE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Credit Reason Sale','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','PART_NUMBER',222,'Part Number','PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','ORIGINAL_INVOICE_DATE',222,'Original Invoice Date','ORIGINAL_INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Original Invoice Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','ORIGINAL_INVOICE_NUMBER',222,'Original Invoice Number','ORIGINAL_INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Original Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','ORDER_NUMBER',222,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','INVOICE_NUMBER',222,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','TAX_AMT',222,'Tax Amt','TAX_AMT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Tax Amt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','AMOUNT_PAID',222,'Amount Paid','AMOUNT_PAID','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amount Paid','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','LOCATION',222,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','INVOICE_TOTAL',222,'Invoice Total','INVOICE_TOTAL','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Total','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','FREIGHT_REMAINING',222,'Freight Remaining','FREIGHT_REMAINING','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Freight Remaining','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','FREIGHT_ORIGINAL',222,'Freight Original','FREIGHT_ORIGINAL','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Freight Original','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','BUSINESS_DATE',222,'Business Date','BUSINESS_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Business Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','INVOICE_DATE',222,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','SHIP_NAME',222,'Ship Name','SHIP_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','FISCAL_MONTH',222,'Fiscal Month','FISCAL_MONTH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Fiscal Month','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','FISCAL_YEAR',222,'Fiscal Year','FISCAL_YEAR','','','','XXEIS_RS_ADMIN','NUMBER','','','Fiscal Year','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','ORIGIN_DATE',222,'Origin Date','ORIGIN_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Origin Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','RESTOCK_FEE_PERCENT',222,'Restock Fee Percent','RESTOCK_FEE_PERCENT','','','','XXEIS_RS_ADMIN','NUMBER','','','Restock Fee Percent','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','AR_CUSTOMER_NUMBER',222,'Ar Customer Number','AR_CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ar Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','INVOICE_TYPE',222,'Invoice Type','INVOICE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','ORDER_DATE',222,'Order Date','ORDER_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Order Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','SHIP_DATE',222,'Ship Date','SHIP_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ship Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','CREATION_DATE',222,'Creation Date','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','LINE_TYPE',222,'Line Type','LINE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Line Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','TAX_RATE',222,'Tax Rate','TAX_RATE','','','','XXEIS_RS_ADMIN','NUMBER','','','Tax Rate','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','ORGINAL_ORDER_NUMBER',222,'Orginal Order Number','ORGINAL_ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Orginal Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','C_LINE_NUMBER',222,'C Line Number','C_LINE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','C Line Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','O_AMOUNT_APPLIED',222,'O Amount Applied','O_AMOUNT_APPLIED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','O Amount Applied','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','TRX_DATE',222,'Trx Date','TRX_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Trx Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','PROCESS_ID',222,'Process Id','PROCESS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Process Id','','','');
--Inserting View Components for EIS_XXWC_AR_CREDIT_MEMO_DTLS_V
--Inserting View Component Joins for EIS_XXWC_AR_CREDIT_MEMO_DTLS_V
END;
/
set scan on define on
prompt Creating Report LOV Data for White Cap Credit Memo Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - White Cap Credit Memo Report
xxeis.eis_rs_ins.lov( 222,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'SELECT DISTINCT GPS.PERIOD_NAME
FROM      HR_OPERATING_UNITS          HOU,
          GL_LEDGERS                  GL,
          gl_period_statuses          gps
WHERE
 GL.LEDGER_ID                      = HOU.SET_OF_BOOKS_ID
AND GL.LEDGER_ID                      = GPS.SET_OF_BOOKS_ID
AND GL.ACCOUNTED_PERIOD_TYPE          = GPS.PERIOD_TYPE
order by GPS.PERIOD_NAME','','Fiscal Month','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'SELECT DISTINCT GPS.PERIOD_YEAR
FROM      HR_OPERATING_UNITS          HOU,
          GL_LEDGERS                  GL,
          gl_period_statuses          gps
WHERE
 GL.LEDGER_ID                      = HOU.SET_OF_BOOKS_ID
AND GL.LEDGER_ID                      = GPS.SET_OF_BOOKS_ID
AND GL.ACCOUNTED_PERIOD_TYPE          = GPS.PERIOD_TYPE
order by GPS.PERIOD_YEAR
','','Fiscal Year','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct apply_date
    from ar_receivable_applications
    where application_type=''CM''','','Origin Date','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct apply_date
    from ar_receivable_applications
    where application_type=''CM''','','Origin Date','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for White Cap Credit Memo Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - White Cap Credit Memo Report
xxeis.eis_rs_utility.delete_report_rows( 'White Cap Credit Memo Report' );
--Inserting Report - White Cap Credit Memo Report
xxeis.eis_rs_ins.r( 222,'White Cap Credit Memo Report','','The purpose of this report is to provide White Cap District Managers with data regarding customer credit activities for their approvals on Credit Memos.  The report serves to provide SOX control/compliance.','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - White Cap Credit Memo Report
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'AMOUNT_PAID','Amount Paid','Amount Paid','','~T~D~2','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'BUSINESS_DATE','Business Date','Business Date','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'CREDIT_REASON_SALE','Credit Reason Sale','Credit Reason Sale','','','default','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','32','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'CUSTOMER_TERRITORY','Customer Territory','Customer Territory','','','default','','31','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'FISCAL_MONTH','Fiscal Month','Fiscal Month','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'FISCAL_YEAR','Fiscal Year','Fiscal Year','','~~~','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'FREIGHT_ORIGINAL','Freight Original','Freight Original','','~T~D~2','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'FREIGHT_REMAINING','Freight Remaining','Freight Remaining','','~T~D~2','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'INVOICE_LINE_AMOUNT','Invoice Line Amount','Invoice Line Amount','','~T~D~2','default','','28','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'INVOICE_TOTAL','Invoice Total','Invoice Total','','~~~','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'LOCATION','Location','Location','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'MASTER_NAME','Master Name','Master Name','','','default','','33','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'ORIGINAL_INVOICE_DATE','Original Invoice Date','Original Invoice Date','','','default','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'ORIGINAL_INVOICE_NUMBER','Original Invoice Number','Original Invoice Number','','','default','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'PART_NUMBER','Part Number','Part Number','','','default','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'QTY_SHIPPED','Qty Shipped','Qty Shipped','','~~~','default','','29','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'SALES_REP_NO','Sales Rep No','Sales Rep No','','','default','','30','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'SHIP_NAME','Ship Name','Ship Name','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'TAKEN_BY','Taken By','Taken By','','','default','','34','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'TAX_AMT','Tax Amt','Tax Amt','','~T~D~2','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'TAX_RATE','Tax Rate','Tax Rate','','~T~D~2','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'ORIGIN_DATE','Origin Date','Origin Date','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'RESTOCK_FEE_PERCENT','Restock Fee Percent','Restock Fee Percent','','~~~','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'AR_CUSTOMER_NUMBER','Ar Customer Number','Ar Customer Number','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'INVOICE_TYPE','Invoice Type','Invoice Type','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'ORDER_DATE','Booked Date','Order Date','','','default','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'SHIP_DATE','Actual Shipment Date','Ship Date','','','default','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'CREATION_DATE','Order Creation Date','Creation Date','','','default','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Credit Memo Report',222,'LINE_TYPE','Line Type','Line Type','','','default','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','');
--Inserting Report Parameters - White Cap Credit Memo Report
xxeis.eis_rs_ins.rp( 'White Cap Credit Memo Report',222,'Fiscal Year','Fiscal Year','FISCAL_YEAR','IN','Fiscal Year','SELECT distinct period_year FROM GL_PERIOD_STATUSES WHERE  APPLICATION_ID=101 AND CLOSING_STATUS=''O'' and sysdate between start_date and end_date','NUMERIC','Y','Y','1','','N','SQL','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap Credit Memo Report',222,'Location','Location','LOCATION','IN','AR Organizaion Code LOV','','VARCHAR2','N','Y','5','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap Credit Memo Report',222,'Origin Date From','Origin Date From','ORIGIN_DATE','>=','Origin Date','','DATE','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap Credit Memo Report',222,'Origin Date To','Origin Date To','ORIGIN_DATE','<=','Origin Date','','DATE','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap Credit Memo Report',222,'Fiscal Month','Fiscal Month','','IN','Fiscal Month','SELECT distinct period_name FROM GL_PERIOD_STATUSES WHERE  APPLICATION_ID=101 AND CLOSING_STATUS=''O'' and sysdate between start_date and end_date','VARCHAR2','N','Y','2','','N','SQL','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - White Cap Credit Memo Report
xxeis.eis_rs_ins.rcn( 'White Cap Credit Memo Report',222,'','','','','AND PROCESS_ID = :SYSTEM.PROCESS_ID','Y','0','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap Credit Memo Report',222,'ORIGIN_DATE','>=',':Origin Date From','','','Y','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap Credit Memo Report',222,'ORIGIN_DATE','<=',':Origin Date To','','','Y','4','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - White Cap Credit Memo Report
--Inserting Report Triggers - White Cap Credit Memo Report
xxeis.eis_rs_ins.rt( 'White Cap Credit Memo Report',222,'begin
XXEIS.EIS_XXWC_CREDIT_MEMO_DET_PKG.POPULATE_CREDIT_INVOICES( P_PROCESS_ID => :SYSTEM.PROCESS_ID,
P_PERIOD_YEAR => :Fiscal Year,
P_PERIOD_MONTH => :Fiscal Month,
P_ORIGIN_DATE_FROM => :Origin Date From,
P_ORIGIN_DATE_TO => :Origin Date To,
p_location => :Location);
end;
','B','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rt( 'White Cap Credit Memo Report',222,'begin
XXEIS.EIS_XXWC_CREDIT_MEMO_DET_PKG.clear_temp_tables(P_PROCESS_ID=> :SYSTEM.PROCESS_ID);
end;	','A','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - White Cap Credit Memo Report
--Inserting Report Portals - White Cap Credit Memo Report
--Inserting Report Dashboards - White Cap Credit Memo Report
--Inserting Report Security - White Cap Credit Memo Report
xxeis.eis_rs_ins.rsec( 'White Cap Credit Memo Report','401','','50840',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Credit Memo Report','222','','51004',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Credit Memo Report','222','','50871',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Credit Memo Report','401','','50872',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Credit Memo Report','20005','','50880',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Credit Memo Report','222','','20678',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Credit Memo Report','401','','50832',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Credit Memo Report','401','','50835',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Credit Memo Report','401','','50895',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Credit Memo Report','1','','50962',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Credit Memo Report','222','','50622',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Credit Memo Report','222','','50638',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Credit Memo Report','222','','50894',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Credit Memo Report','401','','50849',222,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - White Cap Credit Memo Report
END;
/
set scan on define on
