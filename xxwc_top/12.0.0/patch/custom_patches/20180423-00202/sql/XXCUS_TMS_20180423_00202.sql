/*
 TMS:  20180423-00202
 Date: 04/23/2018
 Notes:  Prod Issue: OPN Admin cannot end date locations GA201.1.1 and GA201.1.ASSET, So need a data fix script to end date with 16-APR-2018.
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 l_sql varchar2(2000) :=Null;
 l_count number :=0;
 --
BEGIN --Main Processing...
   --
   n_loc :=101;
   --
	update apps.pn_space_assign_emp_all
	set emp_assign_end_date ='16-APR-18'
	where 1 =1
	and location_id =(select location_id from apps.pn_locations_all where location_code='GA201.1.1')
	and emp_assign_end_date is null
   ;
   --
   n_loc :=102;
   --   
   dbms_output.put_line
   (
     case when sql%rowcount >0 then 'Location GA201.1.1 end dated to 4/16/2018'
     else 'Cannot update location GA201.1.1 with end date 4/16/2018'
     end 
   );
   --
	update apps.pn_space_assign_emp_all
	set emp_assign_end_date ='16-APR-18'
	where 1 =1
	and location_id =(select location_id from apps.pn_locations_all where location_code='GA201.1.ASSET')
	and emp_assign_end_date is null
   ;
   --
   n_loc :=103;
   --   
   dbms_output.put_line
   (
     case when sql%rowcount >0 then 'Location GA201.1.ASSET end dated to 4/16/2018'
     else 'Cannot update location GA201.1.ASSET with end date 4/16/2018'
     end 
   );
   --
   commit;
   --
EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20180423-00202, Errors =' || SQLERRM);
END;
/