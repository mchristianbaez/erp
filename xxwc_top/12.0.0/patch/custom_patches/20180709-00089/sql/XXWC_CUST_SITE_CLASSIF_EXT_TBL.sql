 /********************************************************************************
  FILE NAME: XXWC_CUST_SITE_CLASSIF_EXT_TBL.sql

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    P.Vamshidhar  TMS#20180709-00089 - AHH Customer Conversion External Tables.
  ********************************************************************************/
CREATE TABLE XXWC.XXWC_CUST_SITE_CLASSIF_EXT_TBL
(
  AHH_SITE_CLASSIFICATION  VARCHAR2(10 BYTE),
  CUSTOMER_NAME            VARCHAR2(240 BYTE),
  CUST_SITE_NAME           VARCHAR2(30 BYTE),
  CUSTOMER_NUMBER          VARCHAR2(20 BYTE),
  CUST_SITE_NUM            VARCHAR2(30 BYTE),
  CITY                     VARCHAR2(50 BYTE),
  STATE                    VARCHAR2(50 BYTE),
  ENTER_DATE               VARCHAR2(50 BYTE),
  TERMS_TYPE               VARCHAR2(20 BYTE),
  SELL_TYPE                VARCHAR2(10 BYTE),
  HIGH_BALANCE             VARCHAR2(20 BYTE),
  AVG_PAY_DAYS             VARCHAR2(10 BYTE),
  TOTAL_CURR_BALANCE       VARCHAR2(20 BYTE),
  LAST_SALE_DT             VARCHAR2(50 BYTE),
  LAST_PAY_DT              VARCHAR2(50 BYTE),
  NAICS                    VARCHAR2(20 BYTE),
  SLSREPOUT                VARCHAR2(20 BYTE),
  HOLDPER_CD               VARCHAR2(20 BYTE),
  SIC_1                    VARCHAR2(20 BYTE),
  SIC_2                    VARCHAR2(20 BYTE),
  SIC_3                    VARCHAR2(20 BYTE),
  SALES_YTD                VARCHAR2(20 BYTE),
  LAST_SALES_YTD           VARCHAR2(20 BYTE),
  CREDIT_LIMIT             VARCHAR2(20 BYTE),
  COLLECTOR_ID             VARCHAR2(20 BYTE),
  SITE_CLASS               VARCHAR2(50 BYTE)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY XXWC_AR_AHH_CASH_RCPT_CONV_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_CUST_SITE_CLASSIFICATION_CONV.bad'
    DISCARDFILE 'XXWC_CUST_SITE_CLASSIFICATION_CONV.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                      )
     LOCATION (XXWC_AR_AHH_CASH_RCPT_CONV_DIR:'XXWC_CUST_SITE_CLASSIFICATION_CONV.csv')
  )
REJECT LIMIT UNLIMITED
NOPARALLEL
NOMONITORING;
/
GRANT SELECT ON XXWC.XXWC_CUST_SITE_CLASSIF_EXT_TBL TO EA_APEX;
/