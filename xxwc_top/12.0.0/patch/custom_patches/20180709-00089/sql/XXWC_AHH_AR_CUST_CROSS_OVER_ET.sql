 /********************************************************************************
  FILE NAME: XXWC_AHH_AR_CUST_CROSS_OVER_ET.sql

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    P.Vamshidhar  TMS#20180709-00089 - AHH Customer Conversion External Tables.
  ********************************************************************************/
CREATE TABLE XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_ET
(
  CUST_NUM         VARCHAR2(20 BYTE),
  CUST_SITE        VARCHAR2(50 BYTE),
  CUST_NAME        VARCHAR2(240 BYTE),
  CUST_ADD_LINE1   VARCHAR2(240 BYTE),
  ORACLE_CUST_NUM  VARCHAR2(20 BYTE)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY XXWC_AR_AHH_CASH_RCPT_CONV_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_AHH_AR_CUST_CROSS_OVER.bad'
    DISCARDFILE 'XXWC_AHH_AR_CUST_CROSS_OVER.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                     )
     LOCATION (XXWC_AR_AHH_CASH_RCPT_CONV_DIR:'XXWC_AHH_AR_CUST_CROSS_OVER.csv')
  )
REJECT LIMIT UNLIMITED
NOPARALLEL
NOMONITORING;
/
GRANT SELECT ON XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_ET TO EA_APEX;
/