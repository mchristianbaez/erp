/*
 TMS: 20150720-00161
 Date: 07/21/2015
 Notes: ESMS S294632 Deduction Timeframe Data Fix for June Deductions
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20150720-00161, Script 1 -Before Update');
   UPDATE XXCUS.XXCUSOZF_RBT_PAYMENTS_TBL
      SET PAYMENT_TIME_FRAME =
             TO_CHAR (ADD_MONTHS (TO_DATE ('01-' || PAYMENT_TIME_FRAME), -1),
                      'MON-YYYY')
    WHERE     1 = 1
          AND payment_method = 'REBATE_DEDUCTION'
          AND payment_time_frame IS NOT NULL;
   DBMS_OUTPUT.put_line ('TMS: 20150720-00161, Script 1 -After Update, rows modified: '||sql%rowcount);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20150720-00161, Script 1, Errors ='||SQLERRM);
END;
/