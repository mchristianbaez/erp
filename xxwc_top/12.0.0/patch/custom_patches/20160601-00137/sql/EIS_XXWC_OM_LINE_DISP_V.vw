---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_OM_LINE_DISP_V $
  PURPOSE	  : Lines Disposition Report
  TMS Task Id : 20160426-00102 , 20160601-00137 
  REVISIONS   :
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     18-Apr-2016        Pramod   		 TMS#20160426-00102, TMS#20160601-00137   Performance Tuning
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_OM_LINE_DISP_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_OM_LINE_DISP_V (PROCESS_ID,ITEM_NUMBER, ITEM_DESCRIPTION, VELOCITY, BRANCH, PRICING_ZONE, QTY_OF_ITEMS_CANCELLED, QTY_OF_ITEMS_DISPOSITIONED, TIMES_LINE_CANCELLED, TIMES_LINE_DISPOSITIONED, MIN, MAX, TOTAL_SALES, REORDER_POINT, ORGANIZATION_ID, INVENTORY_ITEM_ID)
AS
  SELECT    
    process_id  process_id,
    item_number ,
    item_description ,
    velocity ,
    branch ,
    pricing_zone ,
    SUM (qty_of_items_cancelled) qty_of_items_cancelled ,
    SUM (qty_of_items_dispositioned) qty_of_items_dispositioned ,
    SUM (times_line_cancelled) times_line_cancelled ,
    SUM (times_line_dispositioned) times_line_dispositioned ,
    MIN ,
    MAX ,
    SUM (total_sales) total_sales,
    reorder_point ,
    organization_id ,
    INVENTORY_ITEM_ID
  FROM XXEIS.EIS_XXWC_OM_LINE_DISP_TAB
  GROUP BY 
    process_id,    --TMS#20160426-00102    by PRAMOD  on 04-18-2016
	item_number ,    
    item_description ,
    velocity ,
    branch ,    
    pricing_zone ,
    MIN ,
    MAX ,
    reorder_point,
	organization_id ,
	inventory_item_id 
/
