--Report Name            : Lines Disposition Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Lines Disposition Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_LINE_DISP_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_LINE_DISP_V',660,'','','','','SA059956','XXEIS','Eis Xxwc Om Line Disp V','EXOLDV','','');
--Delete View Columns for EIS_XXWC_OM_LINE_DISP_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_LINE_DISP_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_LINE_DISP_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','MAX',660,'Max','MAX','','','','SA059956','NUMBER','','','Max','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','MIN',660,'Min','MIN','','','','SA059956','NUMBER','','','Min','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','TOTAL_SALES',660,'Total Sales','TOTAL_SALES','','','','SA059956','NUMBER','','','Total Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','TIMES_LINE_DISPOSITIONED',660,'Times Line Dispositioned','TIMES_LINE_DISPOSITIONED','','','','SA059956','NUMBER','','','Times Line Dispositioned','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','TIMES_LINE_CANCELLED',660,'Times Line Cancelled','TIMES_LINE_CANCELLED','','','','SA059956','NUMBER','','','Times Line Cancelled','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','QTY_OF_ITEMS_DISPOSITIONED',660,'Qty Of Items Dispositioned','QTY_OF_ITEMS_DISPOSITIONED','','','','SA059956','NUMBER','','','Qty Of Items Dispositioned','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','QTY_OF_ITEMS_CANCELLED',660,'Qty Of Items Cancelled','QTY_OF_ITEMS_CANCELLED','','','','SA059956','NUMBER','','','Qty Of Items Cancelled','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','BRANCH',660,'Branch','BRANCH','','','','SA059956','VARCHAR2','','','Branch','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','SA059956','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','SA059956','VARCHAR2','','','Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','REORDER_POINT',660,'Reorder Point','REORDER_POINT','','','','SA059956','NUMBER','','','Reorder Point','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','PRICING_ZONE',660,'Pricing Zone','PRICING_ZONE','','','','SA059956','VARCHAR2','','','Pricing Zone','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','VELOCITY',660,'Velocity','VELOCITY','','','','SA059956','VARCHAR2','','','Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','SA059956','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','SA059956','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','PROCESS_ID',660,'Process Id','PROCESS_ID','','','','SA059956','NUMBER','','','Process Id','','','');
--Inserting View Components for EIS_XXWC_OM_LINE_DISP_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_LINE_DISP_V','MTL_SYSTEM_ITEMS_KFV',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','SA059956','SA059956','-1','Inventory Item Definitions','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_LINE_DISP_V','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','SA059956','SA059956','-1','Oe Order Lines All Stores Information For All Orde','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_LINE_DISP_V','HZ_CUST_ACCOUNTS',660,'HZ_CUST_ACCOUNTS','HCA','HCA','SA059956','SA059956','-1','Stores Information About Customer Accounts.','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_LINE_DISP_V','HZ_PARTIES',660,'HZ_PARTIES','HZP','HZP','SA059956','SA059956','-1','Information About Parties Such As Organizations, P','','','','');
--Inserting View Component Joins for EIS_XXWC_OM_LINE_DISP_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_LINE_DISP_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOLDV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_LINE_DISP_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOLDV.ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_LINE_DISP_V','OE_ORDER_LINES','OL',660,'EXOLDV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_LINE_DISP_V','HZ_CUST_ACCOUNTS','HCA',660,'EXOLDV.CUST_ACCOUNT_ID','=','HCA.CUST_ACCOUNT_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_LINE_DISP_V','HZ_PARTIES','HZP',660,'EXOLDV.PARTY_ID','=','HZP.PARTY_ID(+)','','','','Y','SA059956','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Lines Disposition Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Lines Disposition Report
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT DISTINCT MSI.SEGMENT1   ITEM_NUMBER,
MSI.DESCRIPTION  ITEM_DESCRIPTION from mtl_system_items_b msi','','OM Item Number LOV','Order Item numbers','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct attribute6 pricing_zone from mtl_parameters','','OM Pricing Zone','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select DESCRIPTION ITEM_DESCRIPTION from MTL_SYSTEM_ITEMS_KFV
where exists (Select 1 from XXEIS.EIS_ORG_ACCESS_V where organization_id = organization_id)
','','OM ITEM DESCRIPTION','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Lines Disposition Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Lines Disposition Report
xxeis.eis_rs_utility.delete_report_rows( 'Lines Disposition Report' );
--Inserting Report - Lines Disposition Report
xxeis.eis_rs_ins.r( 660,'Lines Disposition Report','','The purpose of this report is to list items that have been line dispositioned (line item fulfilled in a different branch) or cancelled.','','','','SA059956','EIS_XXWC_OM_LINE_DISP_V','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Lines Disposition Report
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'ITEM_NUMBER','Item Number','Item Number','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'MAX','Max','Max','','~~~','default','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'MIN','Min','Min','','~~~','default','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'QTY_OF_ITEMS_CANCELLED','Qty Of Items Cancelled','Qty Of Items Cancelled','','~~~','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'QTY_OF_ITEMS_DISPOSITIONED','Qty Of Items Dispositioned','Qty Of Items Dispositioned','','~~~','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'TIMES_LINE_CANCELLED','Times Line Cancelled','Times Line Cancelled','','~~~','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'TIMES_LINE_DISPOSITIONED','Times Line Dispositioned','Times Line Dispositioned','','~~~','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'TOTAL_SALES','Total Location Sales','Total Sales','','$~,~.~2','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'TOTAL_TIMES_LINE_CANC_OR_DISP','Total Times Line Canc or Disp','Total Sales','NUMBER','~~~','default','','7','Y','','','','','','','(EXOLDV.TIMES_LINE_CANCELLED + EXOLDV.TIMES_LINE_DISPOSITIONED)','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'TOTAL_QTY_LINE_CANC_OR_DISP','Total Qty Line canc or Disp','Total Sales','NUMBER','~~~','default','','8','Y','','','','','','','(EXOLDV.QTY_OF_ITEMS_CANCELLED + EXOLDV.QTY_OF_ITEMS_DISPOSITIONED)','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'REORDER_POINT','Reorder Point','Reorder Point','','~~~','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'VELOCITY','Velocity','Velocity','','','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'TOTAL_PRICING_ZONE_SALES','Pricing Zone Sales','Velocity','NUMBER','$~,~.~2','default','','10','Y','','','','','','','xxeis.EIS_RS_XXWC_COM_UTIL_PKG.GET_TOT_PRIZINGZONE_SALES_DLR(EXOLDV.INVENTORY_ITEM_ID,EXOLDV.ORGANIZATION_ID,EXOLDV.PRICING_ZONE)','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
--Inserting Report Parameters - Lines Disposition Report
xxeis.eis_rs_ins.rp( 'Lines Disposition Report',660,'Location','Location','BRANCH','IN','OM WAREHOUSE','','VARCHAR2','Y','Y','3','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Lines Disposition Report',660,'Item Number','Item Number','ITEM_NUMBER','IN','OM Item Number LOV','','VARCHAR2','N','Y','5','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Lines Disposition Report',660,'Pricing Zone','Pricing Zone','PRICING_ZONE','IN','OM Pricing Zone','','VARCHAR2','N','Y','4','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Lines Disposition Report',660,'Item Description','Item Description','ITEM_DESCRIPTION','IN','OM ITEM DESCRIPTION','','VARCHAR2','N','Y','6','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Lines Disposition Report',660,'Ordered Date From','Ordered Date From','','IN','','','DATE','Y','Y','1','','N','CONSTANT','SA059956','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Lines Disposition Report',660,'Ordered Date To','Ordered Date To','','IN','','','DATE','Y','Y','2','','N','CONSTANT','SA059956','Y','N','','End Date','');
--Inserting Report Conditions - Lines Disposition Report
xxeis.eis_rs_ins.rcn( 'Lines Disposition Report',660,'','','','','and PROCESS_ID= :SYSTEM.PROCESS_ID','Y','1','','SA059956');
--Inserting Report Sorts - Lines Disposition Report
xxeis.eis_rs_ins.rs( 'Lines Disposition Report',660,'ITEM_NUMBER','ASC','SA059956','','');
--Inserting Report Triggers - Lines Disposition Report
xxeis.eis_rs_ins.rt( 'Lines Disposition Report',660,'Begin
xxeis.EIS_XXWC_OM_LINE_DISP_PKG.OM_LINE_DISP (
P_PROCESS_ID => :SYSTEM.PROCESS_ID,
p_Ordered_Date_From => :Ordered Date From ,
p_Ordered_Date_To => :Ordered Date To,
p_location => :Location ,
p_pricing_zone => :Pricing Zone ,
p_item_number   => :Item Number ,
p_Item_Description     => :Item Description 
                          );

end;
','B','Y','SA059956');
xxeis.eis_rs_ins.rt( 'Lines Disposition Report',660,'begin
XXEIS.EIS_XXWC_OM_LINE_DISP_PKG.clear_temp_tables(P_PROCESS_ID=> :SYSTEM.PROCESS_ID);
end;','A','Y','SA059956');
--Inserting Report Templates - Lines Disposition Report
--Inserting Report Portals - Lines Disposition Report
--Inserting Report Dashboards - Lines Disposition Report
--Inserting Report Security - Lines Disposition Report
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50926',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50927',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50928',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50929',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50931',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50930',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','21623',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','701','','50546',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50856',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50857',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50858',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50859',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50860',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50861',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','20005','','50880',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','','LC053655','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','','10010432','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','','RB054040','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','','RV003897','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','','SS084202','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','','SO004816','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50886',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50901',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50870',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50871',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50869',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','20005','','50900',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','51044',660,'SA059956','','');
--Inserting Report Pivots - Lines Disposition Report
xxeis.eis_rs_ins.rpivot( 'Lines Disposition Report',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Lines Disposition Report',660,'Pivot','ITEM_NUMBER','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Lines Disposition Report',660,'Pivot','QTY_OF_ITEMS_CANCELLED','DATA_FIELD','SUM','','4','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Lines Disposition Report',660,'Pivot','QTY_OF_ITEMS_DISPOSITIONED','DATA_FIELD','SUM','','3','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Lines Disposition Report',660,'Pivot','TIMES_LINE_CANCELLED','DATA_FIELD','SUM','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Lines Disposition Report',660,'Pivot','TIMES_LINE_DISPOSITIONED','DATA_FIELD','SUM','','1','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
