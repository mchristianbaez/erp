/*
 TMS: 20160712-00148 Delete incorrect records from Inventory open interface
 Date: 7/12/2016
 */
SET SERVEROUTPUT ON SIZE 1000000;

DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('Script 1 -Before Delete');

   DELETE FROM mtl_transactions_interface
         WHERE     source_code = 'Subinventory Transfer'
               AND process_flag = '3'
               AND error_explanation LIKE
                      'Transfer subinventory is not valid for the given transfer organization%';

   DBMS_OUTPUT.put_line (
      'Script 1 -After Delete, rows deleted: ' || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('Script 2 -Before Delete');

   DELETE FROM mtl_transactions_interface
         WHERE     source_code = 'Subinventory Transfer'
               AND process_flag = '3'
               AND error_explanation LIKE
                      'Subinventory code is either not entered or not valid%';

   DBMS_OUTPUT.put_line (
      'Script 2 -After Delete, rows deleted: ' || SQL%ROWCOUNT);

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('Error =' || SQLERRM);
END;
/