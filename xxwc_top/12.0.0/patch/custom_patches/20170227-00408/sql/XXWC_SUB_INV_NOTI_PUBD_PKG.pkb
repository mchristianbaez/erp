create or replace PACKAGE BODY  APPS.XXWC_SUB_INV_NOTI_PUBD_PKG
IS
   /*************************************************************************
   *   $Header XXWC_SUB_INV_NOTI_PUBD_PKG.pkb $
   *   Module Name: XXWC_SUB_INV_NOTI_PUBD_PKG.pkb
   *
   *   PURPOSE:   This package is used to send a email notification to the branch operations manager,
   *              Whenever there is transaction happened to 'PUBD' subinventory.
   *
   *   REVISIONS:
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -------------------------
   *   1.0        18-Nov-2015  Kishorebabu V          Initial Version
   *                                                  TMS# 20151023-00047 Subinventory Transfer Notification for PUBD
   *   2.0        25-Jul-2016  Niraj K Ranjan         TMS#20160623-00160  PUBD Email notification to the buyer
   *   2.1        03-oct-2017  Neha Saini             TMS# 20170227-00408 PUBD Subinventory quantity change
   * ***************************************************************************/

-- Declaring Package Global Variables

g_req_id       NUMBER(20)    := fnd_global.conc_request_id;
g_distro_list  VARCHAR2(100) := 'HDSOracleDevelopers@hdsupply.com';


   PROCEDURE XXWC_SUB_INV_NOTI_PUBD_PRC (errbuf   OUT VARCHAR2
                                        ,retcode  OUT VARCHAR2)
   IS
      /* **************************************************************************
         *   Procedure Name: XXWC_SUB_INV_NOTI_PUBD
         *
         *   PURPOSE:   This procedure called from a conc prog to to send a email notification to the branch operations manager
         *              whenever there is transaction happened to 'PUBD' subinventory.
         *
         *   REVISIONS:
         *   Ver        Date          Author               Description
         *   ---------  ----------    ---------------      -------------------------
         *   1.0        18-Nov-2015  Kishorebabu V          Initial Version
         *                                                  TMS# 20151023-00047 Subinventory Transfer Notification for PUBD
         *   2.0        25-Jul-2016  Niraj K Ranjan         TMS#20160623-00160  PUBD Email notification to the buyer
         *   2.1        03-oct-2017  Neha Saini             TMS# 20170227-00408 PUBD Subinventory quantity change
         * ***************************************************************************/

      l_sec                  VARCHAR2 (1000);
      l_error_msg            VARCHAR2 (4000);
      l_exception            EXCEPTION;
      l_result               VARCHAR2(2000);
      l_result_msg           VARCHAR2(2000);
      ln_count               NUMBER := 0;
      ln_trans_cnt           NUMBER := 0;
      wfile_handle           UTL_FILE.FILE_TYPE;
      l_file_location        VARCHAR2(256) := 'XXWC_SUB_INV_NOTI_PUBD';
      l_file_name            VARCHAR2(256);
      l_directory_path       VARCHAR2(1000);
      l_email_address        VARCHAR2(100);
      max_line_length         BINARY_INTEGER                         := 32767;

      CURSOR cur_pubd_trans (cp_organization_id IN NUMBER)
      IS
        SELECT org
        ,      item_number
        ,      description
        ,      shelf_life_days
        ,      SUM(PRIMARY_TRANSACTION_QUANTITY) quantity_transferred_to_pubd --ver2.1
        ,      remaining_on_hand_quantity
        ,      uom
        ,      SUM(total_value_of_transfer) total_value_of_transfer
        FROM   (SELECT TO_CHAR(ood.organization_code)    org
                ,      msib.segment1                     item_number
                ,      REPLACE(msib.description,'-',' ') description
                ,      msib.shelf_life_days              shelf_life_days
                ,      moq.PRIMARY_TRANSACTION_QUANTITY         quantity_transferred_to_pubd  --ver2.1
                ,      moq.PRIMARY_TRANSACTION_QUANTITY --ver2.1
                ,      (SELECT SUM(transaction_quantity)
                        FROM   apps.mtl_onhand_quantities
                        WHERE  inventory_item_id        = moq.inventory_item_id
                        AND    organization_id          = moq.organization_id
                        AND    UPPER(subinventory_code)  <> 'PUBD') remaining_on_hand_quantity
                ,      msib.primary_uom_code                        uom
                ,      (mmt.transaction_quantity*cic.item_cost)     total_value_of_transfer
                ,      mmt.transaction_date
                FROM   apps.mtl_material_transactions    mmt
                ,      apps.org_organization_definitions ood
                ,      apps.mtl_system_items_b           msib
                ,      apps.mtl_onhand_quantities_detail moq
                ,      apps.cst_item_costs               cic
                ,      apps.cst_cost_types               cct
                WHERE  mmt.organization_id          = msib.organization_id
                AND    msib.inventory_item_id       = mmt.inventory_item_id
                AND    mmt.transaction_reference is null --ver2.1
                AND    TRUNC(mmt.transaction_date)  = (TRUNC(SYSDATE)-1)
                AND    mmt.organization_id          = ood.organization_id
                AND    ood.organization_id          = cp_organization_id
                AND    mmt.inventory_item_id        = moq.inventory_item_id
                AND    mmt.organization_id          = moq.organization_id
                AND    mmt.transaction_id           = moq.update_transaction_id
                AND    ((mmt.subinventory_code      = 'PUBD'
                         AND mmt.transaction_type_id = 2)
                       OR(mmt.subinventory_code = 'PUBD'
                          AND mmt.transaction_type_id = 15))
                AND    msib.organization_id          = cic.organization_id
                AND    msib.inventory_item_id        = cic.inventory_item_id
                AND    cct.cost_type_id              = cic.cost_type_id
                AND    UPPER(cct.cost_type)          = 'AVERAGE')
        GROUP BY org
        ,      item_number
        ,      description
        ,      shelf_life_days
        ,      remaining_on_hand_quantity
        ,      uom;

        CURSOR cur_pubd_org
      IS
        SELECT DISTINCT ood.organization_id
        ,      ood.organization_code
        ,      ood.organization_name
        FROM   apps.mtl_material_transactions    mmt
               ,apps.mtl_onhand_quantities_detail moq  --ver2.1
               ,apps.org_organization_definitions ood
        WHERE  1=1
        AND    mmt.inventory_item_id        = moq.inventory_item_id   --ver2.1
        AND    mmt.organization_id          = moq.organization_id     --ver2.1
        AND    mmt.transaction_id           = moq.update_transaction_id   --ver2.1
        AND mmt.transaction_reference is null  --ver2.1
        AND   TRUNC(mmt.transaction_date)  = (TRUNC(SYSDATE)-1)
        AND    mmt.organization_id          = ood.organization_id
        AND    ((mmt.subinventory_code      = 'PUBD'
                 AND mmt.transaction_type_id = 2)
               OR(mmt.subinventory_code = 'PUBD'
                  AND mmt.transaction_type_id = 15))
        ORDER BY ood.organization_id;

      CURSOR cur_email_addr(cp_organization_id IN NUMBER)
      IS
        SELECT DISTINCT fu.email_address
        FROM   mtl_parameters mp
        ,      fnd_user fu
        WHERE  mp.attribute13             = fu.employee_id
        AND    mp.master_organization_id  = 222
        AND    mp.organization_id         = cp_organization_id
        AND    fu.email_address           IS NOT NULL
        AND    SYSDATE BETWEEN NVL(fu.start_date,SYSDATE) AND NVL(fu.end_date,SYSDATE);

   BEGIN

     BEGIN
       l_sec := 'Fetchng the directory path';
       SELECT directory_path
       INTO   l_directory_path
       FROM   all_directories
       WHERE  directory_name  = 'XXWC_SUB_INV_NOTI_PUBD';
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         l_error_msg := ' No Data Found' || SUBSTR (SQLERRM, 1, 250);
         RAISE l_exception;
       WHEN OTHERS THEN
         l_error_msg := ' Error occured while fetch the directory path' ||SQLERRM;
         RAISE l_exception;
     END;

     apps.fnd_file.put_line(fnd_file.LOG,'l_directory_path :'|| l_directory_path);
     
     BEGIN
       l_sec := 'Opening the utl file';
       FOR rec_pubd_org IN cur_pubd_org LOOP
         apps.fnd_file.put_line(fnd_file.LOG,'Fetching all the eligible records and writing in to the file');
         
         ln_trans_cnt := ln_trans_cnt +1;
         l_file_name  := NULL;
         l_file_name  := 'PUBD'||'_'||rec_pubd_org.organization_code||'_'|| TO_CHAR (SYSDATE, 'YYYYMMDDHH24MISS')||'.csv';
         apps.fnd_file.put_line(fnd_file.LOG,'l_file_name: '|| l_file_name);
         wfile_handle := utl_file.fopen(l_file_location,l_file_name,'W', max_line_length);
         utl_file.put_line(wfile_handle,'Org'||','||
                                        'Item Number'||','||
                                        'Description'||','||
                                        'Shelf Life Days'||','||
                                        'Quantity Transferred to PUBD'||','||
                                        'UOM'||','||
                                        'Remaining On Hand Quantity'||','||
                                        'Total Value of Transfer');
         l_sec := 'Fetching all the eligible records and writing in to the file';
         FOR rec_pubd_trans IN cur_pubd_trans(rec_pubd_org.organization_id) LOOP
           ln_count := ln_count+1;
           utl_file.put_line(wfile_handle,rec_pubd_trans.org||',"'||
                                          rec_pubd_trans.item_number||'",'||
                                          REPLACE(rec_pubd_trans.description,',')||','||
                                          rec_pubd_trans.shelf_life_days||','||
                                          rec_pubd_trans.quantity_transferred_to_pubd||','||
                                          rec_pubd_trans.uom||','||
                                          rec_pubd_trans.remaining_on_hand_quantity||','||
                                          rec_pubd_trans.total_value_of_transfer);
         END LOOP;
         COMMIT;
         utl_file.fclose(wfile_handle);
         l_sec := 'closing the utl file';

         l_email_address := NULL;
           FOR rec_email_addr IN cur_email_addr (rec_pubd_org.organization_id) LOOP
             l_email_address := rec_email_addr.email_address||';'|| l_email_address;
           END LOOP;

         IF ln_count = 0 THEN
           apps.fnd_file.put_line(fnd_file.LOG,'*****************NO DATA FOUND***********************');
         ELSE
           apps.fnd_file.put_line(fnd_file.LOG,'rec_pubd_org.organization_name :' ||rec_pubd_org.organization_name);
           apps.fnd_file.put_line(fnd_file.LOG,'Sending email to :' ||l_email_address);
           xxcus_misc_pkg.send_email_attachment (p_sender        => 'no-reply@whitecap.net'
                                                ,p_recipients    => l_email_address
                                                ,p_subject       => 'PUBD Subinventory Transfers for '||rec_pubd_org.organization_name||' on '||(TRUNC(SYSDATE)-1)
                                                ,p_message       => 'The attached items were transferred into the PUBD Subinventory yesterday:'
                                                ,p_attachments   => l_directory_path||'/'||l_file_name
                                                ,x_result        => l_result
                                                ,x_result_msg    => l_result_msg
                                                ,p_directory     => 'XXWC_SUB_INV_NOTI_PUBD');
           apps.fnd_file.put_line(fnd_file.LOG,'l_result: ' ||l_result);
           apps.fnd_file.put_line(fnd_file.LOG,'l_result_msg: ' ||l_result_msg);                                     
         END IF;
       END LOOP;
       IF ln_trans_cnt = 0 THEN
         apps.fnd_file.put_line(fnd_file.LOG,'*****************NO DATA FOUND***********************');
       END IF;

     EXCEPTION
       WHEN UTL_FILE.INVALID_PATH THEN
         apps.fnd_file.put_line(fnd_file.LOG,'INVALID PATH');
         utl_file.fclose_all;
       WHEN UTL_FILE.INVALID_MODE THEN
        apps.fnd_file.put_line(fnd_file.LOG,'INVALID MODE');
        utl_file.fclose_all;
     END;
     
     --Start of ver 2.0
     l_sec := 'Calling xxwc_pubd_inv_noti_buyer_prc to send email to buyer';
     apps.fnd_file.put_line(fnd_file.LOG,'===========================================================');
     apps.fnd_file.put_line(fnd_file.LOG,'Calling xxwc_pubd_inv_noti_buyer_prc to send email to buyers');
     BEGIN
        xxwc_sub_inv_noti_pubd_pkg.xxwc_pubd_inv_noti_buyer_prc(p_retmsg => l_error_msg);
        IF l_error_msg IS NOT NULL THEN
           apps.fnd_file.put_line(fnd_file.LOG,'Error Inside xxwc_pubd_inv_noti_buyer_prc=>'||l_error_msg);
           RAISE l_exception;
        END IF;
     EXCEPTION
        WHEN OTHERS THEN
           l_error_msg := 'Error while calling xxwc_pubd_inv_noti_buyer_prc=>'||SUBSTR(SQLERRM,1,3000);
           apps.fnd_file.put_line(fnd_file.LOG,l_error_msg);
           RAISE l_exception;
     END;
     --End of ver 2.0

   EXCEPTION
     WHEN l_exception THEN
      apps.fnd_file.put_line(fnd_file.LOG,'Error '||SUBSTR (DBMS_UTILITY.format_error_stack ()||DBMS_UTILITY.format_error_backtrace (),1,2000));
     
      xxcus_error_pkg.xxcus_error_main_api (p_called_from         => 'XXWC_SUB_INV_NOTI_PUBD_PKG.XXWC_SUB_INV_NOTI_PUBD_PRC'
                                           ,p_calling             => l_sec
                                           ,p_request_id          => g_req_id
                                           ,p_ora_error_msg       => SUBSTR (DBMS_UTILITY.format_error_stack ()||
                                                                             DBMS_UTILITY.format_error_backtrace (),1,2000)
                                           ,p_error_desc          => l_error_msg
                                           ,p_distribution_list   => g_distro_list
                                           ,p_module              => 'INV');
     WHEN OTHERS THEN
      apps.fnd_file.put_line(fnd_file.LOG,'Error '||SUBSTR (DBMS_UTILITY.format_error_stack ()||DBMS_UTILITY.format_error_backtrace (),1,2000));
      xxcus_error_pkg.xxcus_error_main_api (p_called_from         => 'XXWC_SUB_INV_NOTI_PUBD_PKG.XXWC_SUB_INV_NOTI_PUBD_PRC'
                                           ,p_calling             => l_sec
                                           ,p_request_id          => g_req_id
                                           ,p_ora_error_msg       => SUBSTR (DBMS_UTILITY.format_error_stack ()||
                                                                             DBMS_UTILITY.format_error_backtrace (),1,2000)
                                           ,p_error_desc          => l_error_msg
                                           ,p_distribution_list   => g_distro_list
                                           ,p_module              => 'INV');

   END XXWC_SUB_INV_NOTI_PUBD_PRC;
   
   PROCEDURE XXWC_PUBD_INV_NOTI_BUYER_PRC (p_retmsg   OUT VARCHAR2)
   IS
      /* **************************************************************************
         *   Procedure Name: XXWC_PUBD_INV_NOTI_BUYER_PRC
         *
         *   PURPOSE:   This procedure called internally to send a email notification to the buyer
         *              whenever there is transaction happened to 'PUBD' subinventory.
         *
         *   REVISIONS:
         *   Ver        Date          Author               Description
         *   ---------  ----------    ---------------      -------------------------
         *   2.0        25-Jul-2016  Niraj K Ranjan        Initial TMS#20160623-00160  PUBD Email notification to the buyer
         * ***************************************************************************/

      l_sec                  VARCHAR2 (1000);
      l_error_msg            VARCHAR2 (4000);
      l_exception            EXCEPTION;
      l_result               VARCHAR2(2000);
      l_result_msg           VARCHAR2(2000);
      ln_count               NUMBER := 0;
      ln_trans_cnt           NUMBER := 0;
      wfile_handle           UTL_FILE.FILE_TYPE;
      l_file_location        VARCHAR2(256) := 'XXWC_SUB_INV_NOTI_PUBD';
      l_file_name            VARCHAR2(256);
      l_directory_path       VARCHAR2(1000);
      l_email_address        VARCHAR2(2000);
      max_line_length         BINARY_INTEGER                         := 32767;

      CURSOR cur_pubd_trans (p_buyer_id IN NUMBER)
      IS
        SELECT org
        ,      item_number
        ,      description
        ,      shelf_life_days
        ,      SUM(quantity_transferred_to_pubd) quantity_transferred_to_pubd
        ,      remaining_on_hand_quantity
        ,      uom
        ,      SUM(total_value_of_transfer) total_value_of_transfer
        FROM   (SELECT TO_CHAR(ood.organization_code)    org
                ,      msib.segment1                     item_number
                ,      REPLACE(msib.description,'-',' ') description
                ,      msib.shelf_life_days              shelf_life_days
                ,      mmt.transaction_quantity          quantity_transferred_to_pubd
                ,      (SELECT SUM(transaction_quantity)
                        FROM   mtl_onhand_quantities
                        WHERE  inventory_item_id        = moq.inventory_item_id
                        AND    organization_id          = moq.organization_id
                        AND    UPPER(subinventory_code)  <> 'PUBD') remaining_on_hand_quantity
                ,      msib.primary_uom_code                        uom
                ,      (mmt.transaction_quantity*cic.item_cost)     total_value_of_transfer
                ,      mmt.transaction_date
                FROM   mtl_material_transactions    mmt
                ,      org_organization_definitions ood
                ,      mtl_system_items_b           msib
                ,      mtl_onhand_quantities_detail moq
                ,      cst_item_costs               cic
                ,      cst_cost_types               cct
                WHERE  mmt.organization_id          = msib.organization_id
                AND    msib.inventory_item_id       = mmt.inventory_item_id
                AND    TRUNC(mmt.transaction_date)  = (TRUNC(SYSDATE)-1)
                AND    mmt.organization_id          = ood.organization_id
                AND    msib.buyer_id                = p_buyer_id
                --AND    ood.organization_id          = cp_organization_id
                AND    mmt.inventory_item_id        = moq.inventory_item_id
                AND    mmt.organization_id          = moq.organization_id
                AND    mmt.transaction_id           = moq.update_transaction_id
                AND    ((mmt.subinventory_code      = 'PUBD'
                         AND mmt.transaction_type_id = 2)
                       OR(mmt.subinventory_code = 'PUBD'
                          AND mmt.transaction_type_id = 15))
                AND    msib.organization_id          = cic.organization_id
                AND    msib.inventory_item_id        = cic.inventory_item_id
                AND    cct.cost_type_id              = cic.cost_type_id
                AND    UPPER(cct.cost_type)          = 'AVERAGE')
        GROUP BY org
        ,      item_number
        ,      description
        ,      shelf_life_days
        ,      remaining_on_hand_quantity
        ,      uom;

        CURSOR cur_buyer
        IS
        SELECT DISTINCT msi.buyer_id 
         FROM  mtl_material_transactions mmt
              ,mtl_system_items_b msi
         WHERE 1=1
         AND   TRUNC(mmt.transaction_date)  = (TRUNC(SYSDATE)-1)
         AND    ((mmt.subinventory_code      = 'PUBD'
                          AND mmt.transaction_type_id = 2
                 )
                        OR(mmt.subinventory_code = 'PUBD'
                           AND mmt.transaction_type_id = 15)
               )
         AND  mmt.inventory_item_id = msi.inventory_item_id
         AND  mmt.organization_id = msi.organization_id
         AND  msi.buyer_id IS NOT NULL;

      CURSOR cur_email_addr(p_buyer_id IN NUMBER)
      IS
        SELECT DISTINCT fu.email_address
        FROM   fnd_user fu
        WHERE  employee_id = p_buyer_id
        AND    fu.email_address IS NOT NULL                   
        AND    TRUNC(SYSDATE) BETWEEN TRUNC(NVL(fu.start_date,SYSDATE)) AND TRUNC(NVL(fu.end_date,SYSDATE));

   BEGIN
     l_error_msg := NULL;
     BEGIN
       l_sec := 'Fetchng the directory path';
       SELECT directory_path
       INTO   l_directory_path
       FROM   all_directories
       WHERE  directory_name  = 'XXWC_SUB_INV_NOTI_PUBD';
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         l_error_msg := ' No Data Found' || SUBSTR (SQLERRM, 1, 250);
         RAISE l_exception;
       WHEN OTHERS THEN
         l_error_msg := ' Error occured while fetch the directory path' ||SQLERRM;
         RAISE l_exception;
     END; 

     apps.fnd_file.put_line(fnd_file.LOG,'Directory path for buyer file:'|| l_directory_path);
     
     BEGIN
       l_sec := 'Opening the utl file for buyer';
       FOR rec_buyer IN cur_buyer LOOP
         apps.fnd_file.put_line(fnd_file.LOG,'Fetching all the eligible records for buyer id: '||rec_buyer.buyer_id);
         
         ln_trans_cnt := ln_trans_cnt +1;
         l_file_name  := NULL;
         l_file_name  := 'PUBD'||'_'||rec_buyer.buyer_id||'_'|| TO_CHAR (SYSDATE, 'YYYYMMDDHH24MISS')||'.csv';
         apps.fnd_file.put_line(fnd_file.LOG,'l_file_name: '|| l_file_name);
         wfile_handle := utl_file.fopen(l_file_location,l_file_name,'W', max_line_length);
         utl_file.put_line(wfile_handle,'Org'||','||
                                        'Item Number'||','||
                                        'Description'||','||
                                        'Shelf Life Days'||','||
                                        'Quantity Transferred to PUBD'||','||
                                        'UOM'||','||
                                        'Remaining On Hand Quantity'||','||
                                        'Total Value of Transfer');
         l_sec := 'Fetching all the eligible records and writing in to the file';
         FOR rec_pubd_trans IN cur_pubd_trans(rec_buyer.buyer_id) LOOP
           ln_count := ln_count+1;
           utl_file.put_line(wfile_handle,rec_pubd_trans.org||',"'||
                                          rec_pubd_trans.item_number||'",'||
                                          REPLACE(rec_pubd_trans.description,',')||','||
                                          rec_pubd_trans.shelf_life_days||','||
                                          rec_pubd_trans.quantity_transferred_to_pubd||','||
                                          rec_pubd_trans.uom||','||
                                          rec_pubd_trans.remaining_on_hand_quantity||','||
                                          rec_pubd_trans.total_value_of_transfer);
         END LOOP;
         COMMIT;
         utl_file.fclose(wfile_handle);
         l_sec := 'closing the utl file';

         l_email_address := NULL;
           FOR rec_email_addr IN cur_email_addr (rec_buyer.buyer_id) LOOP
             l_email_address := rec_email_addr.email_address||';'|| l_email_address;
           END LOOP;

         IF ln_count = 0 THEN
           apps.fnd_file.put_line(fnd_file.LOG,'*****************NO DATA FOUND FOR BUYER***********************');
         ELSE
           apps.fnd_file.put_line(fnd_file.LOG,'rec_buyer.buyer_id :' ||rec_buyer.buyer_id);
           apps.fnd_file.put_line(fnd_file.LOG,'Sending email to :' ||l_email_address);
           xxcus_misc_pkg.send_email_attachment (p_sender        => 'no-reply@whitecap.net'
                                                ,p_recipients    => l_email_address
                                                ,p_subject       => 'PUBD Subinventory Transfers on '||(TRUNC(SYSDATE)-1)
                                                ,p_message       => 'The attached items were transferred into the PUBD Subinventory yesterday.'
                                                ,p_attachments   => l_directory_path||'/'||l_file_name
                                                ,x_result        => l_result
                                                ,x_result_msg    => l_result_msg
                                                ,p_directory     => 'XXWC_SUB_INV_NOTI_PUBD');
           apps.fnd_file.put_line(fnd_file.LOG,'l_result for buyer: ' ||l_result);
           apps.fnd_file.put_line(fnd_file.LOG,'l_result_msg for buyer: ' ||l_result_msg);                                     
         END IF;
       END LOOP;
       IF ln_trans_cnt = 0 THEN
         apps.fnd_file.put_line(fnd_file.LOG,'*****************NO DATA FOUND FOR BUYER***********************');
       END IF;

     EXCEPTION
       WHEN UTL_FILE.INVALID_PATH THEN
         l_error_msg := 'INVALID PATH';
         apps.fnd_file.put_line(fnd_file.LOG,l_error_msg);
         utl_file.fclose_all;
       WHEN UTL_FILE.INVALID_MODE THEN
        l_error_msg := 'INVALID MODE';
        apps.fnd_file.put_line(fnd_file.LOG,l_error_msg);
        utl_file.fclose_all;
     END;
     p_retmsg := l_error_msg;
   EXCEPTION
     WHEN l_exception THEN
        p_retmsg := l_error_msg;
     WHEN OTHERS THEN
        l_error_msg := SUBSTR(SQLERRM,1,3000);
        p_retmsg := l_error_msg;

   END XXWC_PUBD_INV_NOTI_BUYER_PRC;

END XXWC_SUB_INV_NOTI_PUBD_PKG;
/