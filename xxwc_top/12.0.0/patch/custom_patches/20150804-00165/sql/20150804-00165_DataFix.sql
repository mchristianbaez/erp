-- TMS# 20150804-00165 BY Manjula on 5-Aug-15 
-- Datafix to sync the data for statement_header_id 157522, 157523
-------------------------------------------------------------------------------------------
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
dbms_output.put_line('Before Delete');
DELETE FROM xxcus.xxcusce_statement_headers
WHERE ( statement_header_id in (157522,157523)
	AND EXISTS 
	(SELECT 'x' FROM v$database WHERE name ='EBIZPRD'
    ))
OR ( statement_header_id in (144562,144666)
	AND EXISTS 
	(SELECT 'x' FROM v$database WHERE name ='EBIZFQA'
    ))	;

-- 2 Rows will be deleted

dbms_output.put_line('After Delete : No Of records '||SQL%ROWCOUNT);
dbms_output.put_line('Before Insert');

INSERT INTO  xxcus.xxcusce_statement_headers
SELECT * FROM ce.ce_statement_headers 
WHERE ( statement_header_id in (157522,157523)
	AND EXISTS 
	(SELECT 'x' FROM v$database WHERE name ='EBIZPRD'
    ))
OR ( statement_header_id in (144562,144666)
	AND EXISTS 
	(SELECT 'x' FROM v$database WHERE name ='EBIZFQA'
    ))	;
-- 2 Rows will be inserted

dbms_output.put_line('After Insert : No Of records '||SQL%ROWCOUNT);

COMMIT;	
dbms_output.put_line('Commit completed');
END;
/
