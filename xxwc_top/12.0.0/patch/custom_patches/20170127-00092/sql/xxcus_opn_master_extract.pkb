CREATE OR REPLACE PACKAGE BODY APPS.xxcus_opn_master_extract as
/*
 -- Author: Balaguru Seshadri
 -- Concurrent Job: HDS OPN: Extract Locations.
 -- Scope: This package extracts the entire OPN properties data and denormalizes it for use by OPN user community or systems like
 --        Location directory OR SECAT system.
 -- Modification History
 -- ESMS         RFC     Date           Version   Comments
 -- =========== ======== ============== =======   ============================================================================================================
 -- 253936               19-JUN-2014    1.0       New script.
 -- 253936               05-AUG-2014    1.1       Changes to main routine related to custom table xxcus.xxcus_opn_fru_loc_all.
 -- 253936               15-AUG-2014    1.2      Update logic change for loc_bu_primary_ops_fru and primary_fru_flag
 -- 253936               15-AUG-2014    1.3      Update logic change for primary_fru_flag using min rowid
 -- 253936               03-SEP-2014    1.4      Adjust script to rename columns min_loc_start_date to hds_bldg_land_min_start
 --                                             max_loc_end_date to hds_bldg_land_max_end. Added a new column hds_max_loc_start
 -- 253936               16-SEP-2014    1.5     Update fields hds_prop_fru, hds_prop_fru_prr_flag and hds_prop_fru_pri_ops
 -- 253936               06-OCT-2014    1.6     Update the following items.
 --                                              1) HDS_ST_PRV field
 --                                              2) rename of few xxcus_opn_properties_all.current_flag to hds_current_flag
 --                                             3) update of space_type_tag and space_type fields
 -- 253936               15-OCT-2014    1.7      Update of hds_section)current_flag and new populate new custom table xxcus.xxcus_opn_lease_tenancies_all
 -- 253936               28-OCT-2014    1.8      Update of hds_fru_section_current_flag and populate additional columns in custom table xxcus.xxcus_opn_lease_tenancies_all
 -- 253936               29-OCT-2014    1.9      Change of logic to update hds_pri_fru_flag based on hds_looc_lob_abb and hds_fru_section_current_flag
 -- 253936               04-NOV-2014    2.0     Add new field loc_site_type_code, loc_site_type
 -- 269300               05-NOV-2014    2.1     Add routine apex_secat_re_tbl_refresh to refresh secat table
 -- 269389               06-NOV-2014    2.2     Add new fields bldg_land_type and floor_parcel_type to xxcus.xxcus_opn_fru_loc_all table.
 -- 271684               02-DEC-2014    2.3     Logic to fix bl_ld_area_gross_acreage only for location types LAND, PARCEL or YARD and rename table
 --                                             xxcus.xxcus_opn_lease_tenancies_all to xxcus.xxcus_opn_rer_locations_all
 -- 272717               12/11/2014    2.4      Add the routines and invoke them at the end of the extract
                                                   1)  xxcus_opn_lease_tab_insr
                                                   2)  xxcus_opn_lease_tab_rights
                                                   3)  xxcus_opn_lease_tab_oblig
                                                   4)  xxcus_opn_lease_tab_options
                                                   5)  xxcus_opn_lease_tab_notes
                                                   6)  xxcus_opn_lease_tab_billings   --This one is for future use and is not invoked so far.
--  272937              12/12/2014     2.5     Add trunc function to the xxcus.xxcus_opn_fru_loc_all main extract
--  273012              12/15/2014     2.6     Add hds_ops_status field to the sort by condition in the cursor get_primary_fru
--  273010              12/17/2014     2.7    Populate HDS_PROP_FRU fields that were left blank after all post updates.
--  273067              12/17/2014     2.8    Add space type to the apex_secat_re_tbl_refresh routine
--  274232              01/06/2015     2.9   1) Modify logic to update hds_fru_section_current_flag, hds_bldg_land_status
--                                           2) Add new fields active_property, loc_suite_name and country_code
--  275161             01/12/2015      3.0   Add logic to populate the column hds_pri_loc_flag in xxcus__opn_properties_all table.
--  277377             02/11/2015     3.1    Add column pri_sub_bu_rank to xxcus_opn_fru_loc_all table
--                                          Update sort order in the cursor used for updating hds_pri_fru_flag
--  TMS 20150909-00140   09/11/2015 3.2 Remove fields from XXCUS_OPN_RER_LOCATIONS_ALL, add some new fields, change mapping logic [ESMS 301196]
--  TMS 20151014-00024   10/14/2015 3.3 Edit logic for  XXCUS_OPN_RER_LOCATIONS_ALL.OPN_PRIMARY_LOC_FOR_RER field [ESMS 304543]
--  TMS 20151029-00200   10/29/2015 3.3 Edit logic for  XXCUS_OPN_FRU_LOC_ALL.hds_prop_fru, hds_ops_status and hds_loc_bu_pri_ops  fields [ESMS 304798]
--  TMS 20160112-00123 / ESMS 313314 3.4, Date: 01/12/2016, Balaguru Seshadri Add new column HDS_PROP_CURRENT_FLAG VARCHAR2(1)
--  TMS 20160203-00038 /  ESMS S315719 3.5 Date: 02/03/2016, Balaguru Seshadri Add procedure extract_premises_and_ops and blank all date columns when value is 12/31/4712
--  TMS 20160302-00017 / ESMS 318695 3.6 Date: 03/02/2016  Balaguru Seshadri Add procedures fruloc_addnl_props_set1 and fruloc_addnl_props_set2
--  TMS 20160920-00072 / ESMS 477934 3.7 Fix to display GSC offices as external locations for use in location finder
--  TMS 20161102-00171 / ESMS 482331  3.8 Fix to display FireProtection branches as external locations. Also fixes WC BU HQ issue for both WC and Brafasco.
--  TMS  20170127-00092 / ESMS S513581  3.9 Fix FRU-LOC Master - Update "HDS_Feed_To" Flag logic
*/
 --
 g_flexfield_exists  varchar2(1) :='N';
 --
 procedure print_log(p_message in varchar2) is
 begin
  if xxcus_opn_master_extract.g_debug ='Y' then
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.log, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
  else
   Null;
  end if;
 end print_log;
 --
 -- Begin Ver 3.3
/*
 ************************************************************************************************
 *
 * PROCEDURE: print_error_log
 * PARAMETERS : Message
 * ==========
 * History
 * =======================================================================
 * ESMS                                    Date               Version   Notes
 * ----------                               -----------         -------      ---------------------------------------
 * TMS 20151014-00024   11/10/2015    3.3        Created.
 */
 procedure print_error_log(p_message in varchar2) is
 begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.log, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
 end print_error_log;
-- End Ver 3.3
 --
 procedure Insert_Building_Land_Features
  (
   p_loc_id      in number
  ,p_loc_code    in varchar2
  ,p_loc_type    in varchar2
  ,p_prop_id     in number
  ,p_prop_code   in varchar2
  ,p_feature     in varchar2
  ,p_table       in varchar2
  ,p_dff_exists  in varchar2
  ) is
  --
  --
  g_loc_id    NUMBER :=p_loc_id;
  g_loc_type  VARCHAR(40); --:='BUILDING';
  g_loc_code  VARCHAR(40); --:='FL300A';
  g_prop_code VARCHAR(40); --:='FL300';
  g_prop_id   NUMBER :=p_prop_id;
  g_feature   VARCHAR2(80);
  v_cr VARCHAR2(2) :='
';
  v_final_sql VARCHAR2(10000) :=Null;
  --
  cursor get_dff (p_feature_lookup_meaning in varchar2) is
    select ','||c.application_column_name||' '||upper(replace(replace(replace(c.FORM_LEFT_PROMPT, ' - ', '_'), ' ', '_'),'-','')) dff_dtl
           ,','||upper(replace(replace(replace(c.FORM_LEFT_PROMPT, ' - ', '_'), ' ', '_'),'-','')) dff_hdr
    from   fnd_descriptive_flexs_vl a
          ,fnd_descr_flex_contexts_vl b
          ,fnd_descr_flex_col_usage_vl c
    where  1 =1
      and  a.application_id =240
      and  a.application_table_name ='PN_LOCATION_FEATURES_ALL'
      and  a.descriptive_flexfield_name ='PN_LOCATION_FEATURES'
      and  a.context_column_name ='ATTRIBUTE_CATEGORY'
      and  a.default_context_field_name ='LOCATION_FEATURE_LOOKUP_CODE'
      and  b.descriptive_flexfield_name =a.descriptive_flexfield_name
      --and  b.descriptive_flex_context_code ='VALUE'
      and  b.descriptive_flex_context_name =p_feature_lookup_meaning
      and  c.descriptive_flex_context_code  =b.descriptive_flex_context_code
      and  c.descriptive_flexfield_name =a.descriptive_flexfield_name
      order by c.column_seq_num;
  --
 CURSOR get_features is
    select a.location_feature, b.table_name, flexfield_exists
    from   pn_location_features_v a
          ,xxcus.xxcus_hds_pn_lookup_objects B
    where  1 =2 --do not change this. we just need the field names and data type for reference in the plsql table
     and   b.lookup_type        ='PN_LOCATION_FEATURE_TYPE'
     and   b.table_description  =a.location_feature
     --and   a.location_feature   ='Assessed Values' --IN ('Assessed Values','Ceiling Clear Height')
    group by a.location_feature, b.table_name, flexfield_exists;
 --
 type g_fea_tbl is table of get_features%rowtype index by binary_integer;
 g_fea_rec g_fea_tbl;
 --
  b_proceed   BOOLEAN :=FALSE;
  v_dff_hdr   VARCHAR2(2000) :=Null;
  v_dff_dtl   VARCHAR2(2000) :=Null;
  --
  --
  v_dff varchar2(2000) :=Null;
  v_sql_hdr2 varchar2(5000);
  v_sql_dtl varchar2(5000);
  n_loc number :=100;
  --
  v_sql_hdr1 VARCHAR2(200) :=
  'INSERT INTO ';
  --
  v_sql_tbl varchar2(5000) :=
    'from pn_location_features_v fea
    where 1 =1
      and fea.location_id       ='||G_loc_id||v_cr;
  --
 begin
  --
  begin
   --
   --mo_global.set_policy_context('S', 163);
   --
   n_loc        :=101;
   G_loc_type   :=p_loc_type;
   G_loc_code   :=p_loc_code;
   G_prop_id    :=p_prop_id;
   G_prop_code  :=p_prop_code;
   --
   n_loc :=102;
   --
  v_sql_hdr2 :='
  (
   location_id
  ,location_code
  ,location_type_lookup_code
  ,property_id
  ,property_code
  ,location_feature_id
  ,location_feature_lookup_code
  ,location_feature
  ,description
  ,condition
  ,quantity
  ,uom_code
  ,unit_of_measure';
  --
  v_sql_dtl :=
  '  (
   SELECT fea.location_id '||'
  ,'||''''||g_loc_code||''''||' location_code '||'
  ,'||''''||g_loc_type||''''||' location_type_lookup_code '||'
  ,'||g_prop_id||' property_id '||'
  ,'||''''||g_prop_code||''''||' property_code '||'
  ,fea.location_feature_id
  ,fea.location_feature_lookup_code
  ,fea.location_feature
  ,fea.description
  ,fea.feature_condition
  ,fea.quantity
  ,fea.uom_code
  ,fea.unit_of_measure ';
   --
      n_loc :=103;
   --
   g_fea_rec.delete;
   --
   --print_log('@ 1.'||n_loc);
   g_fea_rec(1).location_feature :=p_feature; --'Assessed Values'; --'Ceiling Clear Height';
   g_fea_rec(1).table_name       :=p_table; --'XXCUS.HDS_PN_LFT_VALUE'; --'XXCUS.HDS_PN_LFT_CEILH';
   g_fea_rec(1).flexfield_exists :=p_dff_exists; --'Y'; --'N';
   --
   if g_fea_rec.count >0 then
    --
    n_loc :=104;
    --
    --print_log('@'||n_loc);
    --
    for idx1 in 1 .. g_fea_rec.count loop
     --
     n_loc :=105;
     --
     --print_log('@'||n_loc);
     --
     g_feature :=g_fea_rec(idx1).location_feature;
     --
     --print_log('g_feature ='||g_feature);
     --
      if g_fea_rec(idx1).flexfield_exists ='Y' then
       --
       n_loc :=106;
       --
       --print_log('@'||n_loc);
       --
       begin
          --
          for rec in get_dff (p_feature_lookup_meaning =>g_fea_rec(idx1).location_feature) loop
           --
            n_loc :=107;
           --
            --print_log('@'||n_loc);
           --
           v_dff_hdr :=v_dff_hdr||v_cr||'  '||rec.dff_hdr;
           v_dff_dtl :=v_dff_dtl||v_cr||'  '||rec.dff_dtl;
          --
          end loop;
          --
      exception
       when others then
        print_log('@1001, msg ='||sqlerrm);
       --
      end;
       --
      end if;
     --
     v_final_sql :=Null;
     --
       --print_log('@108');
       v_final_sql :=v_final_sql
                  ||v_sql_hdr1
                  ||' '
                  ||g_fea_rec(idx1).table_name
                  ||v_sql_hdr2
                  ||case when g_fea_rec(idx1).flexfield_exists ='Y' then v_dff_hdr||v_cr||' )' else  ' ) '||v_cr end
                  ||v_sql_dtl
                  ||case when g_fea_rec(idx1).flexfield_exists ='Y' then v_dff_dtl||v_cr else v_cr end
                  ||v_sql_tbl
                  ||'and fea.location_feature  ='||''''||g_feature||''''
                  ||v_cr
                  ||' )';
     --
      print_log(' ');
      print_log('@'||g_feature||', SQL Text:');
      print_log('============================');
      print_log(v_final_sql);
     --
     print_log('Before calling dynamic sql, insert of records for '||g_feature);
     execute immediate v_final_sql;
     print_log('After calling dynamic sql -@'||g_feature||', records inserted ='||sql%rowcount);

     --commit;
    --
    end loop; --for idx1 in 1 .. g_fea_rec.count loop
   --
   else
    print_log('@Insert_Building_Land_Features, plsql table is empty for location_id '||p_loc_id);
   end if;
   --
  exception
   when others then
    print_log ('@Insert_Building_Land_Features -When Others, location_id ='||p_loc_id||', msg ='||sqlerrm);
  end;
  --
  commit;
  --
 exception
  when others then
   print_log ('Outer block, @Insert_Building_Land_Features, Location_id ='||p_loc_id||', msg ='||sqlerrm);
 end Insert_Building_Land_Features;
 --
 procedure Insert_Building_Land_Contacts
  (
   p_loc_id      in number
  ,p_loc_code    in varchar2
  ,p_loc_type    in varchar2
  ,p_prop_id     in number
  ,p_prop_code   in varchar2
  ,p_role_type   in varchar2
  ,p_table       in varchar2
  ,p_dff_exists  in varchar2
  ) is
  --
  --
  g_loc_id    NUMBER :=p_loc_id;
  g_loc_type  VARCHAR(40); --:='BUILDING';
  g_loc_code  VARCHAR(40); --:='FL300A';
  g_prop_code VARCHAR(40); --:='FL300';
  g_prop_id   NUMBER :=p_prop_id;
  g_role_type VARCHAR2(80);
  v_cr VARCHAR2(2) :='
';
  v_final_sql VARCHAR2(10000) :=Null;
  --
  cursor get_dff (p_contact_lookup_meaning in varchar2) is
    select ','||c.application_column_name||' '||upper(replace(replace(replace(c.FORM_LEFT_PROMPT, ' - ', '_'), ' ', '_'),'-','')) dff_dtl
           ,','||upper(replace(replace(replace(c.FORM_LEFT_PROMPT, ' - ', '_'), ' ', '_'),'-','')) dff_hdr
    from   fnd_descriptive_flexs_vl a--fnd_descr_flex_contexts_vl
          ,fnd_descr_flex_contexts_vl b
          ,fnd_descr_flex_col_usage_vl c
    where  1 =1
      and  a.application_id =240
      and  a.application_table_name ='PN_CONTACTS_ASSIGNMENTS_ALL'
      and  a.descriptive_flexfield_name ='PN_CONTACTS_ASSIGNMENTS'
      and  a.context_column_name ='ATTRIBUTE_CATEGORY'
      and  a.default_context_field_name ='LEASE_ROLE_TYPE'
      and  b.descriptive_flexfield_name =a.descriptive_flexfield_name
      --and  b.descriptive_flex_context_code ='VALUE'
      and  b.descriptive_flex_context_name =p_contact_lookup_meaning
      and  c.descriptive_flex_context_code  =b.descriptive_flex_context_code
      and  c.descriptive_flexfield_name =a.descriptive_flexfield_name
      order by c.column_seq_num;
  --
 CURSOR get_contacts is
   select a.lease_role, b.table_name, b.flexfield_exists
    from   pn_location_contact_assign_v a
          ,xxcus.xxcus_hds_pn_lookup_objects B
    where  1 =1 --do not change this. we just need the field names and data type for reference in the plsql table
     and   b.lookup_type        ='PN_LEASE_ROLE_TYPE'
     and   b.table_description  =a.lease_role
--     and   a.lease_role   ='Site Contacts' --IN ('Assessed Values','Ceiling Clear Height')
    group by a.lease_role, b.table_name, flexfield_exists;
 --
 type g_fea_tbl is table of get_contacts%rowtype index by binary_integer;
 g_fea_rec g_fea_tbl;
 --
  b_proceed   BOOLEAN :=FALSE;
  v_dff_hdr   VARCHAR2(2000) :=Null;
  v_dff_dtl   VARCHAR2(2000) :=Null;
  --
  --
  v_dff varchar2(2000) :=Null;
  v_sql_hdr2 varchar2(5000);
  v_sql_dtl varchar2(5000);
  n_loc number :=100;
  --
  v_sql_hdr1 VARCHAR2(200) :=
  'INSERT INTO ';
  --
  v_sql_tbl varchar2(5000) :=
    'from pn_location_contact_assign_v cont
    where 1 =1
      and cont.location_id       ='||G_loc_id||v_cr;
  --
 begin
  --
  begin
   --
   --mo_global.set_policy_context('S', 163);
   --
   n_loc        :=101;
   G_loc_type   :=p_loc_type;
   G_loc_code   :=p_loc_code;
   G_prop_id    :=p_prop_id;
   G_prop_code  :=p_prop_code;
   --
   n_loc :=102;
   --
  v_sql_hdr2 :='
  (
   location_id
  ,location_code
  ,location_type_lookup_code
  ,property_id
  ,property_code
  ,company_name
  ,company_site
  ,status
  ,lease_role
  ,lease_role_type
  ,company_id
  ,company_site_id
  ,contact_assignment_id';
  --
  v_sql_dtl :=
  '  (
   SELECT cont.location_id '||'
  ,'||''''||g_loc_code||''''||' location_code '||'
  ,'||''''||g_loc_type||''''||' location_type_lookup_code '||'
  ,'||g_prop_id||' property_id '||'
  ,'||''''||g_prop_code||''''||' property_code '||'
  ,cont.company_name
  ,cont.site_name
  ,cont.status
  ,cont.lease_role
  ,cont.lease_role_type
  ,cont.company_id
  ,cont.company_site_id
  ,cont.contact_assignment_id';
   --
      n_loc :=103;
   --
   g_fea_rec.delete;
   --
   --print_log('@ 1.'||n_loc);
   g_fea_rec(1).lease_role :=p_role_type; --'Assessed Values'; --'Ceiling Clear Height';
   g_fea_rec(1).table_name       :=p_table; --'XXCUS.HDS_PN_LFT_VALUE'; --'XXCUS.HDS_PN_LFT_CEILH';
   g_fea_rec(1).flexfield_exists :=p_dff_exists; --'Y'; --'N';
   --
   if g_fea_rec.count >0 then
    --
    n_loc :=104;
    --
    --print_log('@'||n_loc);
    --
    for idx1 in 1 .. g_fea_rec.count loop
     --
     n_loc :=105;
     --
     --print_log('@'||n_loc);
     --
     g_role_type :=g_fea_rec(idx1).lease_role;
     --
     --print_log('g_feature ='||g_feature);
     --
      if g_fea_rec(idx1).flexfield_exists ='Y' then
       --
       n_loc :=106;
       --
       --print_log('@'||n_loc);
       --
       begin
          --
          for rec in get_dff (p_contact_lookup_meaning =>g_fea_rec(idx1).lease_role) loop
           --
            n_loc :=107;
           --
            --print_log('@'||n_loc);
           --
           v_dff_hdr :=v_dff_hdr||v_cr||'  '||rec.dff_hdr;
           v_dff_dtl :=v_dff_dtl||v_cr||'  '||rec.dff_dtl;
          --
          end loop;
          --
      exception
       when others then
        print_log('@1001, msg ='||sqlerrm);
       --
      end;
       --
      end if;
     --
     v_final_sql :=Null;
     --
       --print_log('@108');
       v_final_sql :=v_final_sql
                  ||v_sql_hdr1
                  ||' '
                  ||g_fea_rec(idx1).table_name
                  ||v_sql_hdr2
                  ||case when g_fea_rec(idx1).flexfield_exists ='Y' then v_dff_hdr||v_cr||' )' else  ' ) '||v_cr end
                  ||v_sql_dtl
                  ||case when g_fea_rec(idx1).flexfield_exists ='Y' then v_dff_dtl||v_cr else v_cr end
                  ||v_sql_tbl
                  ||'and cont.lease_role  ='||''''||g_role_type||''''
                  ||v_cr
                  ||' )';
     --
      print_log(' ');
      print_log('@'||g_role_type||', SQL Text:');
      print_log('==============================');
      print_log(v_final_sql);
     --
     print_log('Before calling dynamic sql, insert of records for '||g_role_type);
     execute immediate v_final_sql;
     print_log('After calling dynamic sql -@'||g_role_type||', records inserted ='||sql%rowcount);

     --commit;
    --
    end loop; --for idx1 in 1 .. g_fea_rec.count loop
   --
   else
    print_log('@Insert_Building_Land_Contacts, plsql table is empty for location_id '||p_loc_id);
   end if;
   --
  exception
   when others then
    print_log ('@Insert_Building_Land_Contacts -When Others, location_id ='||p_loc_id||', msg ='||sqlerrm);
  end;
  --
  commit;
  --
 exception
  when others then
   print_log ('Outer block, @Insert_Building_Land_Contacts, Location_id ='||p_loc_id||', msg ='||sqlerrm);
 end Insert_Building_Land_Contacts;
 --
 procedure extract_features is
  cursor c_fea is
    select max(nvl(loc_active_end_date, trunc(sysdate)))    loc_active_end_date
          ,a.opn_id                      prop_id
          ,a.opn_code                    prop_code
          ,a.location_id                 loc_id
          ,a.location_code               loc_code
          ,a.location_type_lookup_code   loc_type
          ,b.table_name                  loc_dff_tbl_name
          ,b.flexfield_exists            loc_dff_exists
          ,c.location_feature            loc_feature
    from   xxcus.xxcus_opn_properties_all a
          ,xxcus.xxcus_hds_pn_lookup_objects b
          ,pn_location_features_v c
    where  1 =1
      and  a.opn_type                  ='PROPERTY'
      and  a.location_type_lookup_code in ('BUILDING', 'LAND')
     and   b.lookup_type        ='PN_LOCATION_FEATURE_TYPE'
     and   b.table_description  =c.location_feature
     and   c.location_id        =a.location_id
     and   nvl(loc_active_end_date, trunc(sysdate)) >=trunc(sysdate)
    group by a.opn_id
          ,a.opn_code
          ,a.location_id
          ,a.location_code
          ,a.location_type_lookup_code
          ,b.table_name
          ,b.flexfield_exists
          ,c.location_feature
    order by a.location_type_lookup_code asc, a.opn_id asc, a.location_id asc;
  --
  type c_fea_tbl is table of c_fea%rowtype index by binary_integer;
  c_fea_rec c_fea_tbl;
  --
  v_final_sql varchar2(2000);
  --

  --
 begin
  --
  open  c_fea;
  fetch c_fea bulk collect into c_fea_rec;
  close c_fea;
  --
  if c_fea_rec.count >0 then
  --
   for idx in 1 .. c_fea_rec.count loop
   --
    begin
    --
     --v_final_sql :='TRUNCATE TABLE '||c_fea_rec(idx).loc_dff_tbl_name;
     --execute immediate v_final_sql;
     --print_log('@'||c_fea_rec(idx).loc_feature||', Message: Deleted old feature records.');
    --
    Insert_Building_Land_Features
      (
       p_loc_id      =>c_fea_rec(idx).loc_id --in number
      ,p_loc_code    =>c_fea_rec(idx).loc_code --in varchar2
      ,p_loc_type    =>c_fea_rec(idx).loc_type --in varchar2
      ,p_prop_id     =>c_fea_rec(idx).prop_id --in number
      ,p_prop_code   =>c_fea_rec(idx).prop_code --in varchar2
      ,p_feature     =>c_fea_rec(idx).loc_feature --in varchar2
      ,p_table       =>c_fea_rec(idx).loc_dff_tbl_name --in varchar2
      ,p_dff_exists  =>c_fea_rec(idx).loc_dff_exists --in varchar2
      );
    --
    exception
     when others then
      print_log('Issue in calling Insert_Building_Land_Features, msg ='||sqlerrm);
    end;
   --
   end loop;
  --
  end if;
  --
 exception
  when others then
   print_log('@extract_features, message ='||sqlerrm);
   rollback;
 end extract_features;
 --
 procedure extract_contacts is
  cursor c_roletypes is
    select max(nvl(loc_active_end_date, trunc(sysdate)))    loc_active_end_date
          ,a.opn_id                      prop_id
          ,a.opn_code                    prop_code
          ,a.location_id                 loc_id
          ,a.location_code               loc_code
          ,a.location_type_lookup_code   loc_type
          ,b.table_name                  loc_dff_tbl_name
          ,b.flexfield_exists            loc_dff_exists
          ,c.lease_role                  lease_role
    from   xxcus.xxcus_opn_properties_all a
          ,xxcus.xxcus_hds_pn_lookup_objects b
          ,pn_location_contact_assign_v c
    where  1 =1
     and   a.opn_type                  ='PROPERTY'
     and   a.location_type_lookup_code in ('BUILDING', 'LAND')
     and   b.lookup_type        ='PN_LEASE_ROLE_TYPE'
     and   b.table_description  =c.lease_role
     and   c.location_id        =a.location_id
     and   nvl(loc_active_end_date, trunc(sysdate)) >=trunc(sysdate)
    group by a.opn_id
          ,a.opn_code
          ,a.location_id
          ,a.location_code
          ,a.location_type_lookup_code
          ,b.table_name
          ,b.flexfield_exists
          ,c.lease_role
    order by a.location_type_lookup_code asc, a.opn_id asc, a.location_id asc;
  --
  type c_fea_tbl is table of c_roletypes%rowtype index by binary_integer;
  c_fea_rec c_fea_tbl;
  --
  v_final_sql varchar2(2000);
  --
  --
 begin
  --
  open  c_roletypes;
  fetch c_roletypes bulk collect into c_fea_rec;
  close c_roletypes;
  --
  if c_fea_rec.count >0 then
  --
   for idx in 1 .. c_fea_rec.count loop
   --
    begin
    --
    Insert_Building_Land_Contacts
      (
       p_loc_id      =>c_fea_rec(idx).loc_id --in number
      ,p_loc_code    =>c_fea_rec(idx).loc_code --in varchar2
      ,p_loc_type    =>c_fea_rec(idx).loc_type --in varchar2
      ,p_prop_id     =>c_fea_rec(idx).prop_id --in number
      ,p_prop_code   =>c_fea_rec(idx).prop_code --in varchar2
      ,p_role_type   =>c_fea_rec(idx).lease_role --in varchar2
      ,p_table       =>c_fea_rec(idx).loc_dff_tbl_name --in varchar2
      ,p_dff_exists  =>c_fea_rec(idx).loc_dff_exists --in varchar2
      );
    --
    exception
     when others then
      print_log('Issue in calling Insert_Building_Land_Contacts, msg ='||sqlerrm);
    end;
   --
   end loop;
  --
  end if;
  --
 exception
  when others then
   print_log('@extract_contacts, message ='||sqlerrm);
   rollback;
 end extract_contacts;
 --
 procedure setup_indexes is
 --
  b_proceed boolean :=FALSE;
  n_loc NUMBER :=0;
  n_count NUMBER :=0;
  v_index_name VARCHAR2(40);
 --
  v_index1 varchar2(100) :='XXCUS.XXCUS_OPN_PROPERTIES_ALL_N1';
  v_index2 varchar2(100) :='XXCUS.XXCUS_OPN_PROPERTIES_ALL_N2';
  v_index3 varchar2(100) :='XXCUS.XXCUS_OPN_PROPERTIES_ALL_N3';
  v_index4 varchar2(100) :='XXCUS.XXCUS_OPN_PROPERTIES_ALL_N4';
  v_index5 varchar2(100) :='XXCUS.XXCUS_OPN_PROPERTIES_ALL_N5';
 --
     v_index_sql1 varchar2(20000) :='CREATE INDEX XXCUS.XXCUS_OPN_PROPERTIES_ALL_N1 ON XXCUS.XXCUS_OPN_PROPERTIES_ALL
    (OPN_ID, LOC_ACTIVE_START_DATE)
    NOLOGGING
    STORAGE    (
                INITIAL          2M
                NEXT             2M
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
    NOPARALLEL';
 --
    v_index_sql2 varchar2(20000) :='CREATE INDEX XXCUS.XXCUS_OPN_PROPERTIES_ALL_N2 ON XXCUS.XXCUS_OPN_PROPERTIES_ALL
    (OPN_ID, LOCATION_ID, LOCATION_TYPE_LOOKUP_CODE, LOC_ACTIVE_START_DATE)
    NOLOGGING
    STORAGE    (
                INITIAL          2M
                NEXT             2M
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
    NOPARALLEL';
 --
    v_index_sql3 varchar2(20000) :='CREATE INDEX XXCUS.XXCUS_OPN_PROPERTIES_ALL_N3 ON XXCUS.XXCUS_OPN_PROPERTIES_ALL
    (OPN_ID, LOCATION_ID, PARENT_LOCATION_ID, LOCATION_TYPE_LOOKUP_CODE)
    NOLOGGING
    STORAGE    (
                INITIAL          2M
                NEXT             2M
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
    NOPARALLEL';
 --
    v_index_sql4 varchar2(20000) :='CREATE INDEX XXCUS.XXCUS_OPN_PROPERTIES_ALL_N4 ON XXCUS.XXCUS_OPN_PROPERTIES_ALL
    (OPN_TYPE, LOCATION_TYPE_LOOKUP_CODE)
    NOLOGGING
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
    NOPARALLEL';
 --
    v_index_sql5 varchar2(20000) :='CREATE INDEX XXCUS.XXCUS_OPN_PROPERTIES_ALL_N5 ON XXCUS.XXCUS_OPN_PROPERTIES_ALL
    (OPN_TYPE, LOCATION_ID)
    NOLOGGING
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
    NOPARALLEL';
 --
 begin
  --
  n_loc :=101;
  --
  begin
    select index_name
    into   v_index_name
    from   all_indexes
    where  1 =1
      and  owner||'.'||index_name =v_index1;
   --
    print_log('Before dropping index1');
    v_index1 :='DROP INDEX '||v_index1;
     execute immediate v_index1;
    print_log('After dropping index1');
   --
   b_proceed :=TRUE;
   --
    if (b_proceed) then
      print_log('Before creation of index1');
     execute immediate v_index_sql1;
      print_log('After creation of index1');
    end if;
   --
  exception
   when no_data_found then
    print_log('No existing index1, before creation of index1');
    execute immediate v_index_sql1;
    print_log('No existing index1, after creation of index1');
   when others then
    print_log('@First index creation, error ='||sqlerrm);
  end;
  --
  n_loc :=102;
  --
  begin
    select index_name
    into   v_index_name
    from   all_indexes
    where  1 =1
      and  owner||'.'||index_name =v_index2;
   --
    print_log('Before dropping index2');
    v_index2 :='DROP INDEX '||v_index2;
     execute immediate v_index2;
    print_log('After dropping index2');
   --
   b_proceed :=TRUE;
   --
    if (b_proceed) then
      print_log('Before creation of index2');
     execute immediate v_index_sql2;
      print_log('After creation of index2');
    end if;
   --
  exception
   when no_data_found then
    print_log('No existing index2, before creation of index2');
    execute immediate v_index_sql2;
    print_log('No existing index2, after creation of index2');
   when others then
    print_log('@Second index creation, error ='||sqlerrm);
  end;
  --
  n_loc :=103;
  --
  begin
    select index_name
    into   v_index_name
    from   all_indexes
    where  1 =1
      and  owner||'.'||index_name =v_index3;
   --
    print_log('Before dropping index3');
    v_index3 :='DROP INDEX '||v_index3;
     execute immediate v_index3;
    print_log('After dropping index3');
   --
   b_proceed :=TRUE;
   --
    if (b_proceed) then
      print_log('Before creation of index3');
     execute immediate v_index_sql3;
      print_log('After creation of index3');
    end if;
   --
  exception
   when no_data_found then
    print_log('No existing index3, before creation of index3');
    execute immediate v_index_sql3;
    print_log('No existing index3, after creation of index3');
   when others then
    print_log('@Third index creation, error ='||sqlerrm);
  end;
  --
  n_loc :=104;
  --
  begin
    select index_name
    into   v_index_name
    from   all_indexes
    where  1 =1
      and  owner||'.'||index_name =v_index4;
   --
    print_log('Before dropping index4');
    v_index4 :='DROP INDEX '||v_index4;
     execute immediate v_index4;
    print_log('After dropping index4');
   --
   b_proceed :=TRUE;
   --
    if (b_proceed) then
      print_log('Before creation of index4');
     execute immediate v_index_sql4;
      print_log('After creation of index4');
    end if;
   --
  exception
   when no_data_found then
    print_log('No existing index4, before creation of index4');
    execute immediate v_index_sql4;
    print_log('No existing index4, after creation of index4');
   when others then
    print_log('@Fourth index creation, error ='||sqlerrm);
  end;
  --
  n_loc :=105;
  --
  begin
    select index_name
    into   v_index_name
    from   all_indexes
    where  1 =1
      and  owner||'.'||index_name =v_index5;
   --
    print_log('Before dropping index5');
    v_index5 :='DROP INDEX '||v_index5;
     execute immediate v_index5;
    print_log('After dropping index5');
   --
   b_proceed :=TRUE;
   --
    if (b_proceed) then
      print_log('Before creation of index5');
     execute immediate v_index_sql5;
      print_log('After creation of index5');
    end if;
   --
  exception
   when no_data_found then
    print_log('No existing index5, before creation of index5');
    execute immediate v_index_sql5;
    print_log('No existing index5, after creation of index5');
   when others then
    print_log('@Fifth index creation, error ='||sqlerrm);
  end;
  --
 exception
  when others then
   print_log('@error in setup_indexes, pointer = '||n_loc||', msg ='||sqlerrm);
 end setup_indexes;
 --
 procedure store_table_matrix
   (
     p_caller             in varchar2
    ,p_lookup_type        in varchar2
    ,p_lookup_code        in varchar2
    ,p_table_name         in varchar2
    ,p_table_description  in varchar2
    ,p_flexfield_exists   in varchar2
    ,p_created_by         in number
    ,p_creation_date      in date
   ) is
 begin
  savepoint start_here;
  insert into xxcus.xxcus_hds_pn_lookup_objects
   (
      lookup_type
     ,lookup_code
     ,table_name
     ,table_description
     ,flexfield_exists
     ,created_by
     ,creation_date
   )
  values
   (
     p_lookup_type
    ,p_lookup_code
    ,p_table_name
    ,p_table_description
    ,p_flexfield_exists
    ,p_created_by
    ,p_creation_date
   );
 exception
  when others then
   rollback to start_here;
   print_log ('When Others @store_table_matrix: p_caller =>'
              ||p_caller
              ||', p_lookup_type ='
              ||p_lookup_type
              ||', lookup_code ='
              ||p_lookup_code
              ||', table_name ='
              ||p_table_name
              ||', table_description ='
              ||p_table_description
             );
   print_log('When Others @store_table_matrix: message stack: '||sqlerrm);
 end store_table_matrix;
 --
 function create_loc_contact_tables (p_tbl_prepend in varchar2) return boolean is
 --
  cursor get_contact_lookup_codes is
  select lookup_code
        ,meaning table_description
        ,SUBSTR(p_tbl_prepend||upper(replace(replace(replace(lookup_code, ' - ', '_'), ' ', '_'),'-','')), 1, 30) table_name
  from   fnd_lookup_values_vl
  where  1 =1
    and  lookup_type ='PN_LEASE_ROLE_TYPE'
  order by lookup_code asc;
 --
 v_tbl_create        varchar2(4000);
 v_dff               varchar2(4000);
 v_tbl_name          varchar2(30);
 v_lookup_code       varchar2(80);
 v_table_description varchar2(240);
 v_exec              BOOLEAN;
 v_posn              number :=1;
 n_count             number :=0;
 v_comment           varchar2(4000);
 v_enter             varchar2(1) :='
';
 --
 begin
  --
  print_log('');
  print_log('Begin -Create location contact tables.');
  --
  for rec in get_contact_lookup_codes loop
   --
   v_tbl_name :=rec.table_name;
   --
   v_lookup_code :=rec.lookup_code;
   --
   v_table_description :=rec.table_description;
   --
   --print_log('');
   --print_log('Table Name ='||v_tbl_name||' , Lookup Code ='||v_lookup_code||' , Lookup Description ='||v_table_description);
   --
    begin
     --
     /*
     v_posn :=1;
     for dff_rec in get_dff_columns (p_feature_lookup_meaning =>v_table_description) loop
      if v_posn =1 then
       v_dff :=dff_rec.DFF_COL||v_enter;
       v_posn :=2;
      else
       v_dff :=v_dff||dff_rec.DFF_COL||v_enter;
      end if;
     end loop; */
     --
       v_tbl_create
        := 'CREATE TABLE '
           ||v_tbl_name
           ||v_enter
           ||'( '
           ||v_enter
           ||'  company_name varchar2(80)'
           ||v_enter
           ||', company_site varchar2(40)'
           ||v_enter
           ||', status varchar2(30)'
           ||v_enter
           ||', lease_role varchar2(80)'
           ||v_enter
           ||', lease_role_type  varchar2(30)'
           ||v_enter
           ||', location_id number'
           ||v_enter
           ||', location_code  varchar2(90)'
           ||v_enter
           ||', location_type_lookup_code varchar2(40)'
           ||v_enter
           ||', property_id number'
           ||v_enter
           ||', property_code varchar2(50)'
           ||v_enter
           ||', company_id number'
           ||v_enter
           ||', company_site_id number'
           ||v_enter
           ||', contact_assignment_id number'
           ||
           case
              when v_dff is not null then
                 v_enter
               ||v_dff
               ||' )'
              else
                 v_enter
               ||' )' end;
         --
         begin
          select case when v_dff is not null then 'Y' else 'N' end
          into   g_flexfield_exists
          from   dual;
         exception
           when others then
           g_flexfield_exists :='N';
         end;
        --
        v_dff :=null;
        --
    exception
    when others then
      print_log('frame contact table SQL for lookup code ='||v_lookup_code||', message ='||sqlerrm);
      v_exec :=FALSE;
    end;
   --
   --print_log(v_tbl_create);
   --print_log('');
   --
   begin
    select count(1)
    into   n_count
    from   all_tables
    where  1 =1
      and  owner||'.'||table_name =v_tbl_name;
   exception
    when no_data_found then
     n_count :=0;
    when others then
     n_count :=0;
   end;
   --
    if n_count >0 then
     --print_log('@contact, Inside n_count >0');
     -- truncate old data, drop and recreate the table
     begin
      --
      begin
        execute immediate 'TRUNCATE TABLE '||v_tbl_name;
          begin
            execute immediate 'DROP TABLE '||v_tbl_name;
              begin
                execute immediate v_tbl_create;
                  begin
                   v_comment :='COMMENT ON TABLE '||v_tbl_name||' IS '||''''||' Table to hold HDS location contacts for '||v_table_description||'''';
                   --print_log('COMMENT SQL: '||v_comment);
                   execute immediate v_comment;
                  exception
                   when others then
                    print_log ('Comment on table '||v_tbl_name||' failed, Error =>'||sqlerrm);
                    v_exec :=FALSE;
                  end;
                --print_log ('Table: '||v_tbl_name||' created.');
                  store_table_matrix
                   (
                     p_caller             =>'LOCATION_CONTACT' --in varchar2
                    ,p_lookup_type        =>'PN_LEASE_ROLE_TYPE' --in varchar2
                    ,p_lookup_code        =>v_lookup_code --in varchar2
                    ,p_table_name         =>v_tbl_name --in varchar2
                    ,p_table_description  =>v_table_description --in varchar2
                    ,p_flexfield_exists   =>g_flexfield_exists --in varchar2
                    ,p_created_by         =>fnd_global.user_id --in number
                    ,p_creation_date      =>sysdate --in date
                   );
                  --
                  v_exec :=TRUE;
                  --
              exception
               when others then
                print_log ('Create table '||v_tbl_name||'failed, Error =>'||sqlerrm);
                v_exec :=FALSE;
              end;
          exception
           when others then
            print_log ('Drop table '||v_tbl_name||'failed, Error =>'||sqlerrm);
            v_exec :=FALSE;
          end;
      exception
       when others then
        print_log ('Truncate table '||v_tbl_name||'failed, Error =>'||sqlerrm);
        v_exec :=FALSE;
      end;
      --
     exception
      when others then
       print_log ('Table: '||v_tbl_name||' creation failed, Error =>'||sqlerrm);
       v_exec :=FALSE;
     end;
     print_log('');
    else
     --print_log('Inside n_count =0');
     -- first time create new feature table
     begin
      --
      execute immediate v_tbl_create;
      execute immediate 'COMMENT ON TABLE '||v_tbl_name||' IS '||''''||' Table to hold HDS location contacts for '||v_table_description||'''';
      --print_log ('Table: '||v_tbl_name||' created.');
      --
      store_table_matrix
       (
         p_caller             =>'LOCATION_CONTACT' --in varchar2
        ,p_lookup_type        =>'PN_LEASE_ROLE_TYPE' --in varchar2
        ,p_lookup_code        =>v_lookup_code --in varchar2
        ,p_table_name         =>v_tbl_name --in varchar2
        ,p_table_description  =>v_table_description --in varchar2
        ,p_flexfield_exists   =>g_flexfield_exists --in varchar2
        ,p_created_by         =>fnd_global.user_id --in number
        ,p_creation_date      =>sysdate --in date
       );
      --
       v_exec :=TRUE;
      --
     exception
      when others then
       print_log ('Table: '||v_tbl_name||' creation failed, Error ='||sqlerrm);
       v_exec :=FALSE;
     end;
     print_log('');
    end if;
   --
   v_exec :=TRUE;
   --
  end loop;
  --
  print_log('End -Create location contact tables.');
  print_log('');
  --
  RETURN v_exec;
  --
 exception
  --
  when others then
    print_log('');
     return FALSE;
  --
 end create_loc_contact_tables;
 --
 function create_loc_feature_tables (p_tbl_prepend in varchar2) return boolean is
 --
  cursor get_feature_lookup_codes is
  select lookup_code
        ,meaning table_description
        ,SUBSTR(p_tbl_prepend||upper(replace(replace(replace(lookup_code, ' - ', '_'), ' ', '_'),'-','')), 1, 30) table_name
  from   fnd_lookup_values_vl
  where  1 =1
    and  lookup_type ='PN_LOCATION_FEATURE_TYPE'
    --and  lookup_code ='ALARMB'
    --and  meaning ='Assessed Values'
  order by lookup_code asc;
 --
    cursor get_dff_columns (p_feature_lookup_meaning in varchar2) is
    select ', '||upper(replace(replace(replace(c.FORM_LEFT_PROMPT, ' - ', '_'), ' ', '_'),'-',''))||' VARCHAR2(150)' DFF_COL
    from   fnd_descriptive_flexs_vl a--fnd_descr_flex_contexts_vl
          ,fnd_descr_flex_contexts_vl b
          ,fnd_descr_flex_col_usage_vl c
    where  1 =1
      and  a.application_id =240
      and  a.application_table_name ='PN_LOCATION_FEATURES_ALL'
      and  a.descriptive_flexfield_name ='PN_LOCATION_FEATURES'
      and  a.context_column_name ='ATTRIBUTE_CATEGORY'
      and  a.default_context_field_name ='LOCATION_FEATURE_LOOKUP_CODE'
      and  b.descriptive_flexfield_name =a.descriptive_flexfield_name
      --and  b.descriptive_flex_context_code ='VALUE'
      and  b.descriptive_flex_context_name =p_feature_lookup_meaning
      and  c.descriptive_flex_context_code  =b.descriptive_flex_context_code
      and  c.descriptive_flexfield_name =a.descriptive_flexfield_name;
 --
 v_tbl_create        varchar2(4000);
 v_dff               varchar2(4000);
 v_tbl_name          varchar2(30);
 v_lookup_code       varchar2(80);
 v_table_description varchar2(240);
 v_exec              BOOLEAN;
 v_posn              number :=1;
 n_count             number :=0;
 v_comment           varchar2(4000);
 v_enter             varchar2(1) :='
';
 --
 begin
  --
  g_flexfield_exists :='N';
  --
  print_log('');
  print_log('Begin -Create location feature tables.');
  --
  for rec in get_feature_lookup_codes loop
   --
   v_tbl_name :=rec.table_name;
   --
   v_lookup_code :=rec.lookup_code;
   --
   v_table_description :=rec.table_description;
   --
   --print_log('');
   --print_log('Table Name ='||v_tbl_name||' , Lookup Code ='||v_lookup_code||' , Lookup Description ='||v_table_description);
   --
   begin
     --
     v_posn :=1;
     for dff_rec in get_dff_columns (p_feature_lookup_meaning =>v_table_description) loop
      if v_posn =1 then
       v_dff :=dff_rec.DFF_COL||v_enter;
       v_posn :=2;
      else
       v_dff :=v_dff||dff_rec.DFF_COL||v_enter;
      end if;
     end loop;
     --
       v_tbl_create
        := 'CREATE TABLE '
           ||v_tbl_name
           ||v_enter
           ||'( '
           ||v_enter
           ||'  location_id number'
           ||v_enter
           ||', location_code varchar2(90)'
           ||v_enter
           ||', location_type_lookup_code varchar2(40)'
           ||v_enter
           ||', property_id number'
           ||v_enter
           ||', property_code varchar2(50)'
           ||v_enter
           ||', location_feature_id number'
           ||v_enter
           ||', location_feature_lookup_code varchar2(30)'
           ||v_enter
           ||', location_feature varchar2(60)'
           ||v_enter
           ||', description varchar2(60)'
           ||v_enter
           ||', condition   varchar2(240)'
           ||v_enter
           ||', quantity    number'
           ||v_enter
           ||', uom_code    varchar2(3)'
           ||v_enter
           ||', unit_of_measure   varchar2(25)'
           ||
           case
              when v_dff is not null then
                 v_enter
               ||v_dff
               ||' )'
              else
                 v_enter
               ||' )' end;
     --
         begin
          select case when v_dff is not null then 'Y' else 'N' end
          into   g_flexfield_exists
          from   dual;
         exception
           when others then
           g_flexfield_exists :='N';
         end;
     --
     v_dff :=null;
     --
   exception
    when others then
      print_log('Build feature table SQL for lookup code ='||v_lookup_code||', message ='||sqlerrm);
      v_exec :=FALSE;
   end;
   --
   --print_log(v_tbl_create);
   --print_log('');
   --
   begin
    select count(1)
    into   n_count
    from   all_tables
    where  1 =1
      and  owner||'.'||table_name =v_tbl_name;
   exception
    when no_data_found then
     n_count :=0;
    when others then
     n_count :=0;
   end;
   --
    if n_count >0 then
     --print_log('@feature, Inside n_count >0');
     -- truncate old data, drop and recreate the table
     begin
      --
      begin
        execute immediate 'TRUNCATE TABLE '||v_tbl_name;
          begin
            execute immediate 'DROP TABLE '||v_tbl_name;
              begin
                execute immediate v_tbl_create;
                  begin
                   v_comment :='COMMENT ON TABLE '||v_tbl_name||' IS '||''''||' Table to hold HDS location features for '||v_table_description||'''';
                   --print_log('COMMENT SQL: '||v_comment);
                   execute immediate v_comment;
                  exception
                   when others then
                    print_log ('Comment on table '||v_tbl_name||' failed, Error =>'||sqlerrm);
                    v_exec :=FALSE;
                  end;
                --print_log ('Table: '||v_tbl_name||' created.');
                  store_table_matrix
                   (
                     p_caller             =>'LOCATION_FEATURES' --in varchar2
                    ,p_lookup_type        =>'PN_LOCATION_FEATURE_TYPE' --in varchar2
                    ,p_lookup_code        =>v_lookup_code --in varchar2
                    ,p_table_name         =>v_tbl_name --in varchar2
                    ,p_table_description  =>v_table_description --in varchar2
                    ,p_flexfield_exists   =>g_flexfield_exists --in varchar2
                    ,p_created_by         =>fnd_global.user_id --in number
                    ,p_creation_date      =>sysdate --in date
                   );
                  --
                  v_exec :=TRUE;
                  --
              exception
               when others then
                print_log ('Create table '||v_tbl_name||'failed, Error =>'||sqlerrm);
                v_exec :=FALSE;
              end;
          exception
           when others then
            print_log ('Drop table '||v_tbl_name||'failed, Error =>'||sqlerrm);
            v_exec :=FALSE;
          end;
      exception
       when others then
        print_log ('Truncate table '||v_tbl_name||'failed, Error =>'||sqlerrm);
        v_exec :=FALSE;
      end;
      --
     exception
      when others then
       print_log ('Table: '||v_tbl_name||' creation failed, Error =>'||sqlerrm);
       v_exec :=FALSE;
     end;
     print_log('');
    else
     --print_log('@feature, Inside n_count =0');
     -- first time create new feature table
     begin
      --
      execute immediate v_tbl_create;
      execute immediate 'COMMENT ON TABLE '||v_tbl_name||' IS '||''''||' Table to hold HDS location features for '||v_table_description||'''';
      --print_log ('Table: '||v_tbl_name||' created.');
      --
      store_table_matrix
       (
         p_caller             =>'LOCATION_FEATURES' --in varchar2
        ,p_lookup_type        =>'PN_LOCATION_FEATURE_TYPE' --in varchar2
        ,p_lookup_code        =>v_lookup_code --in varchar2
        ,p_table_name         =>v_tbl_name --in varchar2
        ,p_table_description  =>v_table_description --in varchar2
        ,p_flexfield_exists   =>g_flexfield_exists --in varchar2
        ,p_created_by         =>fnd_global.user_id --in number
        ,p_creation_date      =>sysdate --in date
       );
      --
       v_exec :=TRUE;
      --
     exception
      when others then
       print_log ('Table: '||v_tbl_name||' creation failed, Error =>'||SQLERRM);
       v_exec :=FALSE;
     end;
     print_log('');
    end if;
   --
   v_exec :=TRUE;
   --
  end loop;
  --
  print_log('End -Create location feature tables.');
  print_log('');
  --
  RETURN v_exec;
  --
 exception
  --
  when others then
    print_log('');
     return FALSE;
  --
 end create_loc_feature_tables;
 --
 function get_dff(p_opn_type in varchar2, p_attribute_id in number, p_value in varchar2) return varchar2 is
  --
    v_end_user_column_name xxcus.xxcus_opn_properties_all.prop_attribute6%type;
    p_context varchar2(80);
    p_dff     varchar2(80);
    p_app_id  number :=240; --Property Manager Application
  --
  begin
   --
   if p_opn_type ='PROPERTY' then
    p_context :='Global Data Elements';
    p_dff     :='PN_PROPERTIES';
   else
    p_context :='Global Data Elements';
    p_dff     :='PN_PROPERTIES';
   end if;
   --
    select end_user_column_name
    into   v_end_user_column_name
    from   fnd_descr_flex_col_usage_vl
    where  1 =1
      and  application_id                =p_app_id
      and  descriptive_flex_context_code =p_context --'Global Data Elements'
      and  descriptive_flexfield_name    =p_dff     --'PN_PROPERTIES'
      and  application_column_name       ='ATTRIBUTE'||to_char(p_attribute_id)
      order by column_seq_num asc;
   --
   v_end_user_column_name :=substr(v_end_user_column_name||': '||p_value, 1, 150);
   --
   return v_end_user_column_name;
   --
  exception
   --
   when no_data_found then
    return p_value;
   --
   when others then
    print_log('@get_dff, When Others: descriptive_flexfield_name ='||p_dff||', application_column_name =attribute'||p_attribute_id);
    print_log('@get_dff, When Others: message ='||sqlerrm);
    return p_value;
   --
  end get_dff;
 --
 function get_hier_tree_max_level (l_opn_id in number) return number is
  --
    n_level number :=0;
  --
  begin
   --
    select max(level)
    into   n_level
    from   pn_locations_all dtl
    where  1 =1
    connect by prior dtl.parent_location_id =dtl.location_id
    start with dtl.location_id in
     (
      select location_id
      from   pn_locations_all
      where  1 =1
        and  location_id =l_opn_id
     );
   --
   return n_level;
   --
  exception
   --
   when no_data_found then
    return n_level;
   --
   when others then
    print_log('@get_hier_tree_max_level, When Others: opn_id ='||l_opn_id||', message ='||sqlerrm);
    return n_level;
   --
  end get_hier_tree_max_level;
 --
 procedure bldg_post_process is
 --
    cursor c_get_bldgs is
    select opn_id
          ,opn_type
          ,loc_active_start_date
          ,count(*)               total
    from   xxcus.xxcus_opn_properties_all
    where  1 =1
      --and  opn_id =372
      and  opn_type ='BUILDING'
      and  location_type_lookup_code ='BUILDING'
    group by opn_id, opn_type, loc_active_start_date;
 --
 type bldgs is table of c_get_bldgs%rowtype index by binary_integer;
 hds_bldgs bldgs;
 --
 -- Variables
   b_proceed BOOLEAN :=FALSE;
 --
 begin
  --
  begin
   --
   open c_get_bldgs;
   fetch c_get_bldgs bulk collect into hds_bldgs;
   close c_get_bldgs;
   --
   if hds_bldgs.count >0 then
    --
    for a1 in 1 .. hds_bldgs.count loop
    --
     begin
      --
        merge into xxcus.xxcus_opn_properties_all t
        using (
                select a.*
                from   apps.xxcus_opn_bldg_details_v a
                where  1 =1
                  and  a.location_id        =hds_bldgs(a1).opn_id
                  and  a.active_start_date  =hds_bldgs(a1).loc_active_start_date
               ) s
        on (
                t.opn_id                 =s.location_id --Update buidling details for the building rows
            --and t.loc_active_start_date  =hds_bldgs(a1).loc_active_start_date
            and t.opn_type               ='BUILDING'
           )
        when matched then update set
         t.bl_ld_lob_nickname              =s.name
        ,t.bl_ld_ID                        =s.ID
        ,t.bl_ld_tenancy_code              =s.tenancy_code
        ,t.bl_ld_tenancy                   =s.tenancy
        ,t.bl_ld_complexity_code           =s.complexity_code
        ,t.bl_ld_complexity                =s.complexity
        ,t.bl_ld_area_uom_code             =s.area_uom_code
        ,t.bl_ld_area_uom                  =s.area_uom
        ,t.bl_ld_area_gross                =s.area_gross
        ,t.bl_ld_area_rentable             =s.area_rentable
        ,t.bl_ld_area_usable               =s.area_usable
        ,t.bl_ld_area_assignable           =s.area_assignable
        ,t.bl_ld_area_sft_vacant           =s.area_sft_vacant
        ,t.bl_ld_area_percent_vacant       =s.area_percent_vacant
        ,t.bl_ld_area_levels               =s.area_levels
        ,t.bl_ld_area_units                =s.area_units
        ,t.bl_ld_area_load_factor          =s.area_load_factor
        ,t.bl_ld_occu_status_code          =s.occu_status_code
        ,t.bl_ld_occu_status               =s.occu_status
        ,t.bl_ld_occu_emp_assignable       =s.occu_emp_assignable
        ,t.bl_ld_occu_dept_assignable      =s.occu_dept_assignable
        ,t.bl_ld_occu_cust_assignable      =s.occu_cust_assignable
        ,t.bl_ld_occu_disposition_code     =s.occu_disposition_code
        ,t.bl_ld_occu_disposition          =s.occu_disposition
        ,t.bl_ld_occu_acc_treatment_code   =s.occu_acc_treatment_code
        ,t.bl_ld_occu_acc_treatment        =s.occu_acc_treatment
        ,t.bl_ld_occu_maximum              =s.occu_maximum
        ,t.bl_ld_occu_optimum              =s.occu_optimum
        ,t.bl_ld_occu_utilized             =s.occu_utilized
        ,t.bl_ld_occu_percent_max          =s.occu_percent_max
        ,t.bl_ld_occu_area_utilized        =s.occu_area_utilized
        ,t.bl_ld_occu_max_vacancy          =s.occu_max_vacancy
        ,t.address_id                      =s.address_id
        ,t.address_country                 =s.address_country
        ,t.country_code                    =s.country
        ,t.address_line1                   =s.address_line1
        ,t.address_line2                   =s.address_line2
        ,t.address_line3                   =s.address_line3
        ,t.city                            =s.city
        ,t.state                           =s.state
        ,t.province                        =s.province
        ,t.postal_code                     =s.postal_code
        ,t.county                          =s.county
        ,t.bldg_attribute_category         =case when s.attribute_category ='BUILDING' then s.attribute_category else Null end
        ,t.bldg_longitude                  =case when s.attribute_category ='BUILDING' then s.attribute1 else Null end
        ,t.bldg_latitude                   =case when s.attribute_category ='BUILDING' then s.attribute2 else Null end
        ,t.bldg_attribute3                 =case when s.attribute_category ='BUILDING' then s.attribute3 else Null end
        ,t.bldg_attribute4                 =case when s.attribute_category ='BUILDING' then s.attribute4 else Null end
        ,t.bldg_attribute5                 =case when s.attribute_category ='BUILDING' then s.attribute5 else Null end
        ,t.bldg_documentum_link            =case when s.attribute_category ='BUILDING' then s.attribute6 else Null end
        ,t.bldg_photo_link                 =case when s.attribute_category ='BUILDING' then s.attribute7 else Null end
        ,t.bldg_attribute8                 =case when s.attribute_category ='BUILDING' then s.attribute8 else Null end
        ,t.bldg_attribute9                 =case when s.attribute_category ='BUILDING' then s.attribute9 else Null end
        ,t.bldg_attribute10                =case when s.attribute_category ='BUILDING' then s.attribute10 else Null end
        ,t.bldg_attribute11                =case when s.attribute_category ='BUILDING' then s.attribute11 else Null end
        ,t.bldg_attribute12                =case when s.attribute_category ='BUILDING' then s.attribute12 else Null end
        ,t.bldg_attribute13                =case when s.attribute_category ='BUILDING' then s.attribute13 else Null end
        ,t.bldg_attribute14                =case when s.attribute_category ='BUILDING' then s.attribute14 else Null end
        ,t.bldg_attribute15                =case when s.attribute_category ='BUILDING' then s.attribute15 else Null end
        ,t.land_attribute_category         =case when s.attribute_category ='LAND' then s.attribute_category else Null end
        ,t.land_longitude                  =case when s.attribute_category ='LAND' then s.attribute1 else Null end
        ,t.land_latitude                   =case when s.attribute_category ='LAND' then s.attribute2 else Null end
        ,t.land_attribute3                 =case when s.attribute_category ='LAND' then s.attribute3 else Null end
        ,t.land_attribute4                 =case when s.attribute_category ='LAND' then s.attribute4 else Null end
        ,t.land_attribute5                 =case when s.attribute_category ='LAND' then s.attribute5 else Null end
        ,t.land_attribute6                 =case when s.attribute_category ='LAND' then s.attribute6 else Null end
        ,t.land_attribute7                 =case when s.attribute_category ='LAND' then s.attribute7 else Null end
        ,t.land_insignificant              =case when s.attribute_category ='LAND' then s.attribute8 else Null end
        ,t.land_attribute9                 =case when s.attribute_category ='LAND' then s.attribute9 else Null end
        ,t.land_attribute10                =case when s.attribute_category ='LAND' then s.attribute10 else Null end
        ,t.land_attribute11                =case when s.attribute_category ='LAND' then s.attribute11 else Null end
        ,t.land_attribute12                =case when s.attribute_category ='LAND' then s.attribute12 else Null end
        ,t.land_attribute13                =case when s.attribute_category ='LAND' then s.attribute13 else Null end
        ,t.land_attribute14                =case when s.attribute_category ='LAND' then s.attribute14 else Null end
        ,t.land_attribute15                =case when s.attribute_category ='LAND' then s.attribute15 else Null end;
      --
      print_log('Merged attributes for building rows, location_id =>'||hds_bldgs(a1).opn_id||', rowcount ='||sql%rowcount);
      --
      commit;
      --
     exception
      when others then
       print_log('Failed to merge attributes for bldg rows, location_id =>'||hds_bldgs(a1).opn_id||', msg ='||sqlerrm);
     end;
    --
    end loop;
    --
   end if;
   --
    b_proceed :=TRUE;
   --
  exception
   when others then
    --
     print_log('Issue in cursor c_get_bldgs'||sqlerrm);
     b_proceed :=FALSE;
    --
  end;
  --
 exception
  when others then
   print_log('@ routine bldg_post_process, msg ='||sqlerrm);
 end bldg_post_process;
 --
 procedure floor_post_process is
 --
    cursor c_get_floors is
    select opn_id
          ,opn_type
          ,opn_code
          ,parent_location_id     building_id
          ,loc_active_start_date
          ,count(*)               total
    from   xxcus.xxcus_opn_properties_all
    where  1 =1
      --and  opn_id =1499
      and  opn_type ='FLOOR'
      and  location_type_lookup_code ='FLOOR'
    group by opn_id, opn_type, opn_code, parent_location_id, loc_active_start_date;
 --
 type floors is table of c_get_floors%rowtype index by binary_integer;
 hds_floors floors;
 --
 -- Variables
   b_proceed BOOLEAN :=FALSE;
 --
 begin
  --
  begin
   --
   open c_get_floors;
   fetch c_get_floors bulk collect into hds_floors;
   close c_get_floors;
   --
   if hds_floors.count >0 then
    --
    for a1 in 1 .. hds_floors.count loop
    --
    begin
    --
     print_log('hds_floors('||a1||').building_id ='||hds_floors(a1).building_id);
    --
     b_proceed :=TRUE;
    --
    exception
     when others then
      print_log('Issue in fetch of building details for floor, location_id ='||hds_floors(a1).opn_id);
      print_log('Issue in fetch of building details for floor, message ='||sqlerrm);
      b_proceed :=FALSE;
    end;
    --
     if (b_proceed) then
       begin
          --
            merge into xxcus.xxcus_opn_properties_all t
            using (
                    select a.*
                    from   apps.xxcus_opn_bldg_details_v a
                    where  1 =1
                      and  a.location_id        =hds_floors(a1).building_id
                      and  rownum <2
                      --and  a.active_start_date  =hds_floors(a1).loc_active_start_date
                      --and  nvl(active_end_date, trunc(sysdate)) >=trunc(sysdate)
                   ) s
            on (
                    t.opn_id                 =hds_floors(a1).opn_id --Update building details for the floor rows
                --and t.loc_active_start_date  =hds_floors(a1).loc_active_start_date
                and t.opn_type               ='FLOOR'
               )
            when matched then update set
             t.bl_ld_lob_nickname              =s.name
            ,t.bl_ld_ID                        =s.ID
            ,t.bl_ld_tenancy_code              =s.tenancy_code
            ,t.bl_ld_tenancy                   =s.tenancy
            ,t.bl_ld_complexity_code           =s.complexity_code
            ,t.bl_ld_complexity                =s.complexity
            ,t.bl_ld_area_uom_code             =s.area_uom_code
            ,t.bl_ld_area_uom                  =s.area_uom
            ,t.fl_pr_area_uom_code             =s.area_uom_code --Null
            ,t.fl_pr_area_uom                  =s.area_uom      --Null
            ,t.bl_ld_area_gross                =s.area_gross
            ,t.bl_ld_area_rentable             =s.area_rentable
            ,t.bl_ld_area_usable               =s.area_usable
            ,t.bl_ld_area_assignable           =s.area_assignable
            ,t.bl_ld_area_sft_vacant           =s.area_sft_vacant
            ,t.bl_ld_area_percent_vacant       =s.area_percent_vacant
            ,t.bl_ld_area_levels               =s.area_levels
            ,t.bl_ld_area_units                =s.area_units
            ,t.bl_ld_area_load_factor          =s.area_load_factor
            ,t.bl_ld_occu_status_code          =s.occu_status_code
            ,t.bl_ld_occu_status               =s.occu_status
            ,t.bl_ld_occu_emp_assignable       =s.occu_emp_assignable
            ,t.bl_ld_occu_dept_assignable      =s.occu_dept_assignable
            ,t.bl_ld_occu_cust_assignable      =s.occu_cust_assignable
            ,t.bl_ld_occu_disposition_code     =s.occu_disposition_code
            ,t.bl_ld_occu_disposition          =s.occu_disposition
            ,t.bl_ld_occu_acc_treatment_code   =s.occu_acc_treatment_code
            ,t.bl_ld_occu_acc_treatment        =s.occu_acc_treatment
            ,t.bl_ld_occu_maximum              =s.occu_maximum
            ,t.bl_ld_occu_optimum              =s.occu_optimum
            ,t.bl_ld_occu_utilized             =s.occu_utilized
            ,t.bl_ld_occu_percent_max          =s.occu_percent_max
            ,t.bl_ld_occu_area_utilized        =s.occu_area_utilized
            ,t.bl_ld_occu_max_vacancy          =s.occu_max_vacancy
            ,t.address_id                      =s.address_id
            ,t.country_code                    =s.country
            ,t.address_country                 =s.address_country
            ,t.address_line1                   =s.address_line1
            ,t.address_line2                   =s.address_line2
            ,t.address_line3                   =s.address_line3
            ,t.city                            =s.city
            ,t.state                           =s.state
            ,t.province                        =s.province
            ,t.postal_code                     =s.postal_code
            ,t.county                          =s.county
            ,t.bldg_attribute_category         =case when s.attribute_category ='BUILDING' then s.attribute_category else Null end
            ,t.bldg_longitude                  =case when s.attribute_category ='BUILDING' then s.attribute1 else Null end
            ,t.bldg_latitude                   =case when s.attribute_category ='BUILDING' then s.attribute2 else Null end
            ,t.bldg_attribute3                 =case when s.attribute_category ='BUILDING' then s.attribute3 else Null end
            ,t.bldg_attribute4                 =case when s.attribute_category ='BUILDING' then s.attribute4 else Null end
            ,t.bldg_attribute5                 =case when s.attribute_category ='BUILDING' then s.attribute5 else Null end
            ,t.bldg_documentum_link            =case when s.attribute_category ='BUILDING' then s.attribute6 else Null end
            ,t.bldg_photo_link                 =case when s.attribute_category ='BUILDING' then s.attribute7 else Null end
            ,t.bldg_attribute8                 =case when s.attribute_category ='BUILDING' then s.attribute8 else Null end
            ,t.bldg_attribute9                 =case when s.attribute_category ='BUILDING' then s.attribute9 else Null end
            ,t.bldg_attribute10                =case when s.attribute_category ='BUILDING' then s.attribute10 else Null end
            ,t.bldg_attribute11                =case when s.attribute_category ='BUILDING' then s.attribute11 else Null end
            ,t.bldg_attribute12                =case when s.attribute_category ='BUILDING' then s.attribute12 else Null end
            ,t.bldg_attribute13                =case when s.attribute_category ='BUILDING' then s.attribute13 else Null end
            ,t.bldg_attribute14                =case when s.attribute_category ='BUILDING' then s.attribute14 else Null end
            ,t.bldg_attribute15                =case when s.attribute_category ='BUILDING' then s.attribute15 else Null end
            ,t.land_attribute_category         =case when s.attribute_category ='LAND' then s.attribute_category else Null end
            ,t.land_longitude                  =case when s.attribute_category ='LAND' then s.attribute1 else Null end
            ,t.land_latitude                   =case when s.attribute_category ='LAND' then s.attribute2 else Null end
            ,t.land_attribute3                 =case when s.attribute_category ='LAND' then s.attribute3 else Null end
            ,t.land_attribute4                 =case when s.attribute_category ='LAND' then s.attribute4 else Null end
            ,t.land_attribute5                 =case when s.attribute_category ='LAND' then s.attribute5 else Null end
            ,t.land_attribute6                 =case when s.attribute_category ='LAND' then s.attribute6 else Null end
            ,t.land_attribute7                 =case when s.attribute_category ='LAND' then s.attribute7 else Null end
            ,t.land_insignificant              =case when s.attribute_category ='LAND' then s.attribute8 else Null end
            ,t.land_attribute9                 =case when s.attribute_category ='LAND' then s.attribute9 else Null end
            ,t.land_attribute10                =case when s.attribute_category ='LAND' then s.attribute10 else Null end
            ,t.land_attribute11                =case when s.attribute_category ='LAND' then s.attribute11 else Null end
            ,t.land_attribute12                =case when s.attribute_category ='LAND' then s.attribute12 else Null end
            ,t.land_attribute13                =case when s.attribute_category ='LAND' then s.attribute13 else Null end
            ,t.land_attribute14                =case when s.attribute_category ='LAND' then s.attribute14 else Null end
            ,t.land_attribute15                =case when s.attribute_category ='LAND' then s.attribute15 else Null end;
          --
          print_log(
                     'Merged building details for floor records, location_id =>'
                      ||hds_floors(a1).opn_id
                      ||', opn_code ='
                      ||hds_floors(a1).opn_code
                      ||', opn_type ='
                      ||hds_floors(a1).opn_type
                      ||', rowcount ='||sql%rowcount
                   );
          --
          commit;
          --
            merge into xxcus.xxcus_opn_properties_all t
            using (
                    select a.*
                    from   apps.xxcus_opn_floors_details_v a
                    where  1 =1
                      and  a.location_id        =hds_floors(a1).opn_id
                      and  a.active_start_date  =hds_floors(a1).loc_active_start_date
                   ) s
            on (
                    t.opn_id                 =hds_floors(a1).opn_id --Update floors details for the floor rows
                and t.loc_active_start_date  =hds_floors(a1).loc_active_start_date
               )
            when matched then update set
                 t.fl_pr_name                     =s.name
                ,t.fl_pr_id                       =s.ID
                ,t.fl_pr_area_gross               =Null
                ,t.fl_pr_area_rentable            =s.area_rentable
                ,t.fl_pr_area_usable              =s.area_usable
                ,t.fl_pr_area_assignable          =s.area_assignable
                ,t.fl_pr_area_sft_vacant          =s.vacant_area
                ,t.fl_pr_area_percent_vacant      =Null
                ,t.fl_pr_area_levels              =Null
                ,t.fl_pr_area_units               =Null
                ,t.fl_pr_area_load_factor         =Null
                ,t.fl_pr_occu_status_code         =s.occu_status_code
                ,t.fl_pr_occu_status              =s.occu_status
                ,t.fl_pr_occu_emp_assignable      =s.occu_emp_assignable
                ,t.fl_pr_occu_dept_assignable     =s.occu_dept_assignable
                ,t.fl_pr_occu_cust_assignable     =s.occu_cust_assignable
                ,t.fl_pr_occu_disposition_code    =s.occu_disposition_code
                ,t.fl_pr_occu_disposition         =s.occu_disposition
                ,t.fl_pr_occu_acc_treatment_code  =s.occu_acc_treatment_code
                ,t.fl_pr_occu_acc_treatment       =s.occu_acc_treatment
                ,t.fl_pr_occu_maximum             =s.occu_maximum
                ,t.fl_pr_occu_optimum             =s.occu_optimum
                ,t.fl_pr_occu_utilized            =s.occu_area_utilized
                ,t.fl_pr_occu_area_utilized       =s.occu_area_utilized
                ,t.fl_pr_occu_max_vacancy         =s.occu_max_vacancy
                ,t.fl_pr_vacant_area              =s.vacant_area
                ,t.fl_pr_common                   =s.common
                ,t.fl_pr_primary_circulation      =s.primary_circulation
                ,t.fl_pr_primary_ops_type_code    =s.primary_ops_type_code
                ,t.fl_pr_primary_ops_type         =s.primary_ops_type
                ,t.fl_pr_function_ops_type_code   =s.function_ops_type_code
                ,t.fl_pr_function_type            =s.function_type
                ,t.fl_pr_standard_type_type_code  =s.standard_type_type_code
                ,t.fl_pr_standard_type            =s.standard_type
                ,t.floor_attribute_category       =case when s.attribute_category ='FLOOR' then s.attribute_category else Null end
                ,t.floor_attribute1               =case when s.attribute_category ='FLOOR' then s.attribute1 else Null end
                ,t.floor_attribute2               =case when s.attribute_category ='FLOOR' then s.attribute2 else Null end
                ,t.floor_attribute3               =case when s.attribute_category ='FLOOR' then s.attribute3 else Null end
                ,t.floor_attribute4               =case when s.attribute_category ='FLOOR' then s.attribute4 else Null end
                ,t.floor_attribute5               =case when s.attribute_category ='FLOOR' then s.attribute5 else Null end
                ,t.floor_attribute6               =case when s.attribute_category ='FLOOR' then s.attribute6 else Null end
                ,t.floor_attribute7               =case when s.attribute_category ='FLOOR' then s.attribute7 else Null end
                ,t.floor_attribute8               =case when s.attribute_category ='FLOOR' then s.attribute8 else Null end
                ,t.floor_attribute9               =case when s.attribute_category ='FLOOR' then s.attribute9 else Null end
                ,t.floor_attribute10              =case when s.attribute_category ='FLOOR' then s.attribute1 else Null end
                ,t.floor_attribute11              =case when s.attribute_category ='FLOOR' then s.attribute11 else Null end
                ,t.floor_attribute12              =case when s.attribute_category ='FLOOR' then s.attribute12 else Null end
                ,t.floor_attribute13              =case when s.attribute_category ='FLOOR' then s.attribute13 else Null end
                ,t.floor_attribute14              =case when s.attribute_category ='FLOOR' then s.attribute14 else Null end
                ,t.floor_attribute15              =case when s.attribute_category ='FLOOR' then s.attribute15 else Null end
                ,t.parcel_attribute_category      =case when s.attribute_category ='PARCEL' then s.attribute_category else Null end
                ,t.parcel_parcel                  =case when s.attribute_category ='PARCEL' then s.attribute1 else Null end
                ,t.parcel_attribute2              =case when s.attribute_category ='PARCEL' then s.attribute2 else Null end
                ,t.parcel_attribute3              =case when s.attribute_category ='PARCEL' then s.attribute3 else Null end
                ,t.parcel_attribute4              =case when s.attribute_category ='PARCEL' then s.attribute4 else Null end
                ,t.parcel_attribute5              =case when s.attribute_category ='PARCEL' then s.attribute5 else Null end
                ,t.parcel_attribute6              =case when s.attribute_category ='PARCEL' then s.attribute6 else Null end
                ,t.parcel_attribute7              =case when s.attribute_category ='PARCEL' then s.attribute7 else Null end
                ,t.parcel_attribute8              =case when s.attribute_category ='PARCEL' then s.attribute8 else Null end
                ,t.parcel_tax_assessor            =case when s.attribute_category ='PARCEL' then s.attribute9 else Null end
                ,t.parcel_attribute10             =case when s.attribute_category ='PARCEL' then s.attribute10 else Null end
                ,t.parcel_attribute11             =case when s.attribute_category ='PARCEL' then s.attribute11 else Null end
                ,t.parcel_attribute12             =case when s.attribute_category ='PARCEL' then s.attribute12 else Null end
                ,t.parcel_attribute13             =case when s.attribute_category ='PARCEL' then s.attribute13 else Null end
                ,t.parcel_attribute14             =case when s.attribute_category ='PARCEL' then s.attribute14 else Null end
                ,t.parcel_attribute15             =case when s.attribute_category ='PARCEL' then s.attribute15 else Null end;
          --
          print_log('Merged floor details, location_id =>'||hds_floors(a1).opn_id||', rowcount ='||sql%rowcount);
          --
          commit;
          --
        exception
          when others then
           print_log('Failed to merge attributes for floor records, location_id =>'||hds_floors(a1).opn_id||', msg ='||sqlerrm);
        end;
        --
     else
       print_log('@floor_post_process, b_proceed is FALSE, ');
     end if;
    --
    end loop;
    --
   end if;
   --
    b_proceed :=TRUE;
   --
  exception
   when others then
    --
     print_log('Issue in cursor c_get_floors'||sqlerrm);
     b_proceed :=FALSE;
    --
  end;
  --
 exception
  when others then
   print_log('@ routine floor_post_process, msg ='||sqlerrm);
 end floor_post_process;
 --
 procedure office_post_process is
 --
    cursor c_get_offices is
    select opn_id
          ,opn_type
          ,opn_code
          ,to_number(null)        building_id
          ,parent_location_id     floor_id
          ,loc_active_start_date
          ,count(*)               total
    from   xxcus.xxcus_opn_properties_all
    where  1 =1
      --and  opn_id IN (2896, 13257)
      and  opn_type ='OFFICE'
      and  location_type_lookup_code ='OFFICE'
    group by opn_id, opn_type, opn_code, parent_location_id, loc_active_start_date;
 --
 type offices is table of c_get_offices%rowtype index by binary_integer;
 hds_offices offices;
 --
 -- Variables
   b_proceed BOOLEAN :=FALSE;
 --
 begin
  --
  begin
   --
   open c_get_offices;
   fetch c_get_offices bulk collect into hds_offices;
   close c_get_offices;
   --
   if hds_offices.count >0 then
    --
    for a1 in 1 .. hds_offices.count loop
    --
    begin
    --
     --print_log('hds_offices('||a1||').floor_id ='||hds_offices(a1).floor_id);
    --
        begin
        --
         select parent_location_id
         into   hds_offices(a1).building_id
         from   pn_floors_v
         where  1 =1
           and  location_id          =hds_offices(a1).floor_id
           and  rownum <2;
           --and  active_start_date  =hds_offices(a1).loc_active_start_date;
           --and  nvl(active_end_date, trunc(sysdate)) >=trunc(sysdate);
        --
         print_log('hds_offices('||a1||').building_id ='||hds_offices(a1).building_id||', floor_id ='||hds_offices(a1).floor_id);
        --
        exception
         when others then
          print_log('Issue in fetch of building details for office, location_id ='||hds_offices(a1).opn_id);
          print_log('Issue in fetch of building details for office, message ='||sqlerrm);
          b_proceed :=FALSE;
        end;
    --
     b_proceed :=TRUE;
    --
    exception
     when others then
      print_log('Issue in fetch of floor details for office, location_id ='||hds_offices(a1).opn_id);
      print_log('Issue in fetch of floor details for office, message ='||sqlerrm);
      b_proceed :=FALSE;
    end;
    --
     if (b_proceed) then
         begin
          --
            merge into xxcus.xxcus_opn_properties_all t
            using (
                    select a.*
                    from   apps.xxcus_opn_bldg_details_v a
                    where  1 =1
                      and  a.location_id        =hds_offices(a1).building_id
                      and  rownum <2
                      --and  a.active_start_date  =hds_offices(a1).loc_active_start_date
                      --and  nvl(active_end_date, trunc(sysdate)) >=trunc(sysdate)
                   ) s
            on (
                    t.opn_id                 =hds_offices(a1).opn_id --Update building details for the office rows
                --and t.loc_active_start_date  =hds_offices(a1).loc_active_start_date
                and t.opn_type               ='OFFICE'
               )
            when matched then update set
             t.bl_ld_lob_nickname              =s.name
            ,t.bl_ld_ID                        =s.ID
            ,t.bl_ld_tenancy_code              =s.tenancy_code
            ,t.bl_ld_tenancy                   =s.tenancy
            ,t.bl_ld_complexity_code           =s.complexity_code
            ,t.bl_ld_complexity                =s.complexity
            ,t.bl_ld_area_uom_code             =s.area_uom_code
            ,t.bl_ld_area_uom                  =s.area_uom
            ,t.fl_pr_area_uom_code             =s.area_uom_code --flooring UOM will always be the same as building
            ,t.fl_pr_area_uom                  =s.area_uom      --flooring UOM will always be the same as building
            ,t.bl_ld_area_gross                =s.area_gross
            ,t.bl_ld_area_rentable             =s.area_rentable
            ,t.bl_ld_area_usable               =s.area_usable
            ,t.bl_ld_area_assignable           =s.area_assignable
            ,t.bl_ld_area_sft_vacant           =s.area_sft_vacant
            ,t.bl_ld_area_percent_vacant       =s.area_percent_vacant
            ,t.bl_ld_area_levels               =s.area_levels
            ,t.bl_ld_area_units                =s.area_units
            ,t.bl_ld_area_load_factor          =s.area_load_factor
            ,t.bl_ld_occu_status_code          =s.occu_status_code
            ,t.bl_ld_occu_status               =s.occu_status
            ,t.bl_ld_occu_emp_assignable       =s.occu_emp_assignable
            ,t.bl_ld_occu_dept_assignable      =s.occu_dept_assignable
            ,t.bl_ld_occu_cust_assignable      =s.occu_cust_assignable
            ,t.bl_ld_occu_disposition_code     =s.occu_disposition_code
            ,t.bl_ld_occu_disposition          =s.occu_disposition
            ,t.bl_ld_occu_acc_treatment_code   =s.occu_acc_treatment_code
            ,t.bl_ld_occu_acc_treatment        =s.occu_acc_treatment
            ,t.bl_ld_occu_maximum              =s.occu_maximum
            ,t.bl_ld_occu_optimum              =s.occu_optimum
            ,t.bl_ld_occu_utilized             =s.occu_utilized
            ,t.bl_ld_occu_percent_max          =s.occu_percent_max
            ,t.bl_ld_occu_area_utilized        =s.occu_area_utilized
            ,t.bl_ld_occu_max_vacancy          =s.occu_max_vacancy
            ,t.address_id                      =s.address_id
            ,t.country_code                    =s.country
            ,t.address_country                 =s.address_country
            ,t.address_line1                   =s.address_line1
            ,t.address_line2                   =s.address_line2
            ,t.address_line3                   =s.address_line3
            ,t.city                            =s.city
            ,t.state                           =s.state
            ,t.province                        =s.province
            ,t.postal_code                     =s.postal_code
            ,t.county                          =s.county
            ,t.bldg_attribute_category         =case when s.attribute_category ='BUILDING' then s.attribute_category else Null end
            ,t.bldg_longitude                  =case when s.attribute_category ='BUILDING' then s.attribute1 else Null end
            ,t.bldg_latitude                   =case when s.attribute_category ='BUILDING' then s.attribute2 else Null end
            ,t.bldg_attribute3                 =case when s.attribute_category ='BUILDING' then s.attribute3 else Null end
            ,t.bldg_attribute4                 =case when s.attribute_category ='BUILDING' then s.attribute4 else Null end
            ,t.bldg_attribute5                 =case when s.attribute_category ='BUILDING' then s.attribute5 else Null end
            ,t.bldg_documentum_link            =case when s.attribute_category ='BUILDING' then s.attribute6 else Null end
            ,t.bldg_photo_link                 =case when s.attribute_category ='BUILDING' then s.attribute7 else Null end
            ,t.bldg_attribute8                 =case when s.attribute_category ='BUILDING' then s.attribute8 else Null end
            ,t.bldg_attribute9                 =case when s.attribute_category ='BUILDING' then s.attribute9 else Null end
            ,t.bldg_attribute10                =case when s.attribute_category ='BUILDING' then s.attribute10 else Null end
            ,t.bldg_attribute11                =case when s.attribute_category ='BUILDING' then s.attribute11 else Null end
            ,t.bldg_attribute12                =case when s.attribute_category ='BUILDING' then s.attribute12 else Null end
            ,t.bldg_attribute13                =case when s.attribute_category ='BUILDING' then s.attribute13 else Null end
            ,t.bldg_attribute14                =case when s.attribute_category ='BUILDING' then s.attribute14 else Null end
            ,t.bldg_attribute15                =case when s.attribute_category ='BUILDING' then s.attribute15 else Null end
            ,t.land_attribute_category         =case when s.attribute_category ='LAND' then s.attribute_category else Null end
            ,t.land_longitude                  =case when s.attribute_category ='LAND' then s.attribute1 else Null end
            ,t.land_latitude                   =case when s.attribute_category ='LAND' then s.attribute2 else Null end
            ,t.land_attribute3                 =case when s.attribute_category ='LAND' then s.attribute3 else Null end
            ,t.land_attribute4                 =case when s.attribute_category ='LAND' then s.attribute4 else Null end
            ,t.land_attribute5                 =case when s.attribute_category ='LAND' then s.attribute5 else Null end
            ,t.land_attribute6                 =case when s.attribute_category ='LAND' then s.attribute6 else Null end
            ,t.land_attribute7                 =case when s.attribute_category ='LAND' then s.attribute7 else Null end
            ,t.land_insignificant              =case when s.attribute_category ='LAND' then s.attribute8 else Null end
            ,t.land_attribute9                 =case when s.attribute_category ='LAND' then s.attribute9 else Null end
            ,t.land_attribute10                =case when s.attribute_category ='LAND' then s.attribute10 else Null end
            ,t.land_attribute11                =case when s.attribute_category ='LAND' then s.attribute11 else Null end
            ,t.land_attribute12                =case when s.attribute_category ='LAND' then s.attribute12 else Null end
            ,t.land_attribute13                =case when s.attribute_category ='LAND' then s.attribute13 else Null end
            ,t.land_attribute14                =case when s.attribute_category ='LAND' then s.attribute14 else Null end
            ,t.land_attribute15                =case when s.attribute_category ='LAND' then s.attribute15 else Null end;
          --
          print_log(
                     'Merged building details for office rows, location_id =>'
                      ||hds_offices(a1).opn_id
                      ||', rowcount ='
                      ||sql%rowcount
                   );
          --
          commit;
          --
         exception
          when others then
           print_log('Failed to merge attributes for floor records, office_location_id =>'||hds_offices(a1).opn_id||', msg ='||sqlerrm);
         end;
        --
         begin
          --
            merge into xxcus.xxcus_opn_properties_all t
            using (
                    select a.*
                    from   apps.xxcus_opn_floors_details_v a
                    where  1 =1
                      and  a.location_id       =hds_offices(a1).floor_id
                      and  rownum <2
                      --and  a.active_start_date =hds_offices(a1).loc_active_start_date
                      --and  nvl(active_end_date, trunc(sysdate)) >=trunc(sysdate)
                   ) s
            on (
                    t.opn_id                 =hds_offices(a1).opn_id --Update floors details for the office rows
                --and t.loc_active_start_date  =hds_offices(a1).loc_active_start_date
                and t.opn_type               ='OFFICE'
               )
            when matched then update set
                 t.fl_pr_name                     =s.name
                ,t.fl_pr_id                       =s.ID
                ,t.fl_pr_area_gross               =Null
                ,t.fl_pr_area_rentable            =s.area_rentable
                ,t.fl_pr_area_usable              =s.area_usable
                ,t.fl_pr_area_assignable          =s.area_assignable
                ,t.fl_pr_area_sft_vacant          =s.vacant_area
                ,t.fl_pr_area_percent_vacant      =Null
                ,t.fl_pr_area_levels              =Null
                ,t.fl_pr_area_units               =Null
                ,t.fl_pr_area_load_factor         =Null
                ,t.fl_pr_occu_status_code         =s.occu_status_code
                ,t.fl_pr_occu_status              =s.occu_status
                ,t.fl_pr_occu_emp_assignable      =s.occu_emp_assignable
                ,t.fl_pr_occu_dept_assignable     =s.occu_dept_assignable
                ,t.fl_pr_occu_cust_assignable     =s.occu_cust_assignable
                ,t.fl_pr_occu_disposition_code    =s.occu_disposition_code
                ,t.fl_pr_occu_disposition         =s.occu_disposition
                ,t.fl_pr_occu_acc_treatment_code  =s.occu_acc_treatment_code
                ,t.fl_pr_occu_acc_treatment       =s.occu_acc_treatment
                ,t.fl_pr_occu_maximum             =s.occu_maximum
                ,t.fl_pr_occu_optimum             =s.occu_optimum
                ,t.fl_pr_occu_utilized            =s.occu_area_utilized
                ,t.fl_pr_occu_area_utilized       =s.occu_area_utilized
                ,t.fl_pr_occu_max_vacancy         =s.occu_max_vacancy
                ,t.fl_pr_vacant_area              =s.vacant_area
                ,t.fl_pr_common                   =s.common
                ,t.fl_pr_primary_circulation      =s.primary_circulation
                ,t.fl_pr_primary_ops_type_code    =s.primary_ops_type_code
                ,t.fl_pr_primary_ops_type         =s.primary_ops_type
                ,t.fl_pr_function_ops_type_code   =s.function_ops_type_code
                ,t.fl_pr_function_type            =s.function_type
                ,t.fl_pr_standard_type_type_code  =s.standard_type_type_code
                ,t.fl_pr_standard_type            =s.standard_type
                ,t.floor_attribute_category       =case when s.attribute_category ='FLOOR' then s.attribute_category else Null end
                ,t.floor_attribute1               =case when s.attribute_category ='FLOOR' then s.attribute1 else Null end
                ,t.floor_attribute2               =case when s.attribute_category ='FLOOR' then s.attribute2 else Null end
                ,t.floor_attribute3               =case when s.attribute_category ='FLOOR' then s.attribute3 else Null end
                ,t.floor_attribute4               =case when s.attribute_category ='FLOOR' then s.attribute4 else Null end
                ,t.floor_attribute5               =case when s.attribute_category ='FLOOR' then s.attribute5 else Null end
                ,t.floor_attribute6               =case when s.attribute_category ='FLOOR' then s.attribute6 else Null end
                ,t.floor_attribute7               =case when s.attribute_category ='FLOOR' then s.attribute7 else Null end
                ,t.floor_attribute8               =case when s.attribute_category ='FLOOR' then s.attribute8 else Null end
                ,t.floor_attribute9               =case when s.attribute_category ='FLOOR' then s.attribute9 else Null end
                ,t.floor_attribute10              =case when s.attribute_category ='FLOOR' then s.attribute1 else Null end
                ,t.floor_attribute11              =case when s.attribute_category ='FLOOR' then s.attribute11 else Null end
                ,t.floor_attribute12              =case when s.attribute_category ='FLOOR' then s.attribute12 else Null end
                ,t.floor_attribute13              =case when s.attribute_category ='FLOOR' then s.attribute13 else Null end
                ,t.floor_attribute14              =case when s.attribute_category ='FLOOR' then s.attribute14 else Null end
                ,t.floor_attribute15              =case when s.attribute_category ='FLOOR' then s.attribute15 else Null end
                ,t.parcel_attribute_category      =case when s.attribute_category ='PARCEL' then s.attribute_category else Null end
                ,t.parcel_parcel                  =case when s.attribute_category ='PARCEL' then s.attribute1 else Null end
                ,t.parcel_attribute2              =case when s.attribute_category ='PARCEL' then s.attribute2 else Null end
                ,t.parcel_attribute3              =case when s.attribute_category ='PARCEL' then s.attribute3 else Null end
                ,t.parcel_attribute4              =case when s.attribute_category ='PARCEL' then s.attribute4 else Null end
                ,t.parcel_attribute5              =case when s.attribute_category ='PARCEL' then s.attribute5 else Null end
                ,t.parcel_attribute6              =case when s.attribute_category ='PARCEL' then s.attribute6 else Null end
                ,t.parcel_attribute7              =case when s.attribute_category ='PARCEL' then s.attribute7 else Null end
                ,t.parcel_attribute8              =case when s.attribute_category ='PARCEL' then s.attribute8 else Null end
                ,t.parcel_tax_assessor            =case when s.attribute_category ='PARCEL' then s.attribute9 else Null end
                ,t.parcel_attribute10             =case when s.attribute_category ='PARCEL' then s.attribute10 else Null end
                ,t.parcel_attribute11             =case when s.attribute_category ='PARCEL' then s.attribute11 else Null end
                ,t.parcel_attribute12             =case when s.attribute_category ='PARCEL' then s.attribute12 else Null end
                ,t.parcel_attribute13             =case when s.attribute_category ='PARCEL' then s.attribute13 else Null end
                ,t.parcel_attribute14             =case when s.attribute_category ='PARCEL' then s.attribute14 else Null end
                ,t.parcel_attribute15             =case when s.attribute_category ='PARCEL' then s.attribute15 else Null end;
          --
          print_log('Merged floor details for office rows, location_id =>'||hds_offices(a1).opn_id
                      ||', rowcount ='
                      ||sql%rowcount
                   );
          --
          commit;
         exception
          when others then
           print_log('Failed to merge attributes for floor records, office_location_id =>'||hds_offices(a1).opn_id||', msg ='||sqlerrm);
         end;
        --
         begin
            merge into xxcus.xxcus_opn_properties_all t
            using (
                    select a.*
                    from   apps.xxcus_opn_office_details_v a
                    where  1 =1
                      and  a.location_id =hds_offices(a1).opn_id
                      and  a.active_start_date =hds_offices(a1).loc_active_start_date
                   ) s
            on (
                      t.opn_id                =hds_offices(a1).opn_id --Update office details for the office rows
                  and t.loc_active_start_date =hds_offices(a1).loc_active_start_date
               )
            when matched then update set
                 t.sc_yd_name                     =s.name
                ,t.sc_yd_id                       =s.ID
                ,t.sc_yd_area_rentable            =s.rentable_area
                ,t.sc_yd_area_usable              =s.usable_area
                ,t.sc_yd_area_assignable          =s.assignable_area
                ,t.sc_yd_area_sft_vacant          =s.vacant_area
                ,t.sc_yd_occu_status_code         =s.occu_status_code
                ,t.sc_yd_occu_status              =s.occu_status
                ,t.sc_yd_occu_emp_assignable      =s.occu_emp_assignable
                ,t.sc_yd_occu_dept_assignable     =s.occu_dept_assignable
                ,t.sc_yd_occu_cust_assignable     =s.occu_cust_assignable
                ,t.sc_yd_occu_disposition_code    =s.occu_disposition_code
                ,t.sc_yd_occu_disposition         =s.occu_disposition
                ,t.sc_yd_occu_acc_treatment_code  =s.occu_acc_treatment_code
                ,t.sc_yd_occu_acc_treatment       =s.occu_acc_treatment
                ,t.sc_yd_occu_maximum             =s.occu_maximum
                ,t.sc_yd_occu_optimum             =s.occu_optimum
                ,t.sc_yd_occu_utilized            =s.occu_area_utilized
                ,t.sc_yd_occu_area_utilized       =s.occu_area_utilized
                ,t.sc_yd_occu_max_vacancy         =s.occu_max_vacancy
                ,t.sc_yd_vacant_area              =s.vacant_area
                ,t.sc_yd_common                   =s.common
                ,t.sc_yd_secondary_circulation    =s.secondary_circulation
                ,t.sc_yd_space_type_code          =s.space_type_code
                ,t.sc_yd_space_type               =s.space_type
                ,t.sc_yd_site_type_code           =s.site_type_code
                ,t.sc_yd_site_type                =s.site_type
                ,t.sc_yd_assignment_type_code     =s.assignment_type_code
                ,t.sc_yd_assignment_type          =s.assignment_type
                ,t.office_attribute_category      =case when s.attribute_category ='OFFICE' then s.attribute_category else Null end
                ,t.office_attribute1              =case when s.attribute_category ='OFFICE' then s.attribute1 else Null end
                ,t.office_attribute2              =case when s.attribute_category ='OFFICE' then s.attribute2 else Null end
                ,t.office_operating_hours         =case when s.attribute_category ='OFFICE' then s.attribute3 else Null end
                ,t.office_walkin                  =case when s.attribute_category ='OFFICE' then s.attribute4 else Null end
                ,t.office_customer_site           =case when s.attribute_category ='OFFICE' then s.attribute5 else Null end
                ,t.office_attribute6              =case when s.attribute_category ='OFFICE' then s.attribute6 else Null end
                ,t.office_attribute7              =case when s.attribute_category ='OFFICE' then s.attribute7 else Null end
                ,t.office_attribute8              =case when s.attribute_category ='OFFICE' then s.attribute8 else Null end
                ,t.office_attribute9              =case when s.attribute_category ='OFFICE' then s.attribute9 else Null end
                ,t.office_attribute10             =case when s.attribute_category ='OFFICE' then s.attribute10 else Null end
                ,t.office_attribute11             =case when s.attribute_category ='OFFICE' then s.attribute11 else Null end
                ,t.office_attribute12             =case when s.attribute_category ='OFFICE' then s.attribute12 else Null end
                ,t.office_attribute13             =case when s.attribute_category ='OFFICE' then s.attribute13 else Null end
                ,t.office_attribute14             =case when s.attribute_category ='OFFICE' then s.attribute14 else Null end
                ,t.office_attribute15             =case when s.attribute_category ='OFFICE' then s.attribute15 else Null end
                ,t.section_attribute_category     =case when s.attribute_category ='SECTION' then s.attribute_category else Null end
                ,t.section_attribute1             =case when s.attribute_category ='SECTION' then s.attribute1 else Null end
                ,t.section_attribute2             =case when s.attribute_category ='SECTION' then s.attribute2 else Null end
                ,t.section_operating_hours        =case when s.attribute_category ='SECTION' then s.attribute3 else Null end
                ,t.section_walkin                 =case when s.attribute_category ='SECTION' then s.attribute4 else Null end
                ,t.section_customer_site          =case when s.attribute_category ='SECTION' then s.attribute5 else Null end
                ,t.section_attribute6             =case when s.attribute_category ='SECTION' then s.attribute6 else Null end
                ,t.section_attribute7             =case when s.attribute_category ='SECTION' then s.attribute7 else Null end
                ,t.section_attribute8             =case when s.attribute_category ='SECTION' then s.attribute8 else Null end
                ,t.section_attribute9             =case when s.attribute_category ='SECTION' then s.attribute9 else Null end
                ,t.section_attribute10            =case when s.attribute_category ='SECTION' then s.attribute10 else Null end
                ,t.section_attribute11            =case when s.attribute_category ='SECTION' then s.attribute11 else Null end
                ,t.section_attribute12            =case when s.attribute_category ='SECTION' then s.attribute12 else Null end
                ,t.section_attribute13            =case when s.attribute_category ='SECTION' then s.attribute13 else Null end
                ,t.section_attribute14            =case when s.attribute_category ='SECTION' then s.attribute14 else Null end
                ,t.section_attribute15            =case when s.attribute_category ='SECTION' then s.attribute15 else Null end;
          --
          print_log('Merged office details for location_id =>'||hds_offices(a1).opn_id||', rowcount ='||sql%rowcount);
          --
          commit;
          --
         exception
          when others then
           --rollback to start_office_post_process;
           print_log('Failed to merge attributes for office records, office_location_id =>'||hds_offices(a1).opn_id||', msg ='||sqlerrm);
         end;
        --
     else
       print_log('@office_post_process, b_proceed is FALSE, ');
     end if;
    --
    end loop;
    --
   end if;
   --
    b_proceed :=TRUE;
   --
  exception
   when others then
    --
     print_log('Issue in cursor c_get_offices'||sqlerrm);
     b_proceed :=FALSE;
    --
  end;
  --
 exception
  when others then
   print_log('@ routine office_post_process, msg ='||sqlerrm);
 end office_post_process;
 --
 procedure land_post_process is
 --
    cursor c_get_lands is
    select opn_id, opn_type, loc_active_start_date, count(*) total
    from   xxcus.xxcus_opn_properties_all
    where  1 =1
      and  opn_type ='LAND'
    group by opn_id, opn_type, loc_active_start_date;
 --
 type lands is table of c_get_lands%rowtype index by binary_integer;
 hds_lands lands;
 --
 -- Variables
   b_proceed BOOLEAN :=FALSE;
 --
 begin
  --
  begin
   --
   open c_get_lands;
   fetch c_get_lands bulk collect into hds_lands;
   close c_get_lands;
   --
   if hds_lands.count >0 then
    --
    for a1 in 1 .. hds_lands.count loop
    --
     begin
      --
        merge into xxcus.xxcus_opn_properties_all t
        using (
                select a.*
                from   apps.xxcus_opn_bldg_details_v a
                where  1 =1
                  and  a.location_id        =hds_lands(a1).opn_id
                  and  a.active_start_date  =hds_lands(a1).loc_active_start_date
               ) s
        on (
                t.opn_id                  =s.location_id --Update land details for the land rows
            --and t.loc_active_start_date   =hds_lands(a1).loc_active_start_date
            and t.opn_type                ='LAND'
           )
        when matched then update set
         t.bl_ld_lob_nickname              =s.name
        ,t.bl_ld_ID                        =s.ID
        ,t.bl_ld_tenancy_code              =s.tenancy_code
        ,t.bl_ld_tenancy                   =s.tenancy
        ,t.bl_ld_complexity_code           =s.complexity_code
        ,t.bl_ld_complexity                =s.complexity
        ,t.bl_ld_area_uom_code             =s.area_uom_code
        ,t.bl_ld_area_uom                  =s.area_uom
        ,t.bl_ld_area_gross                =s.area_gross
        ,t.bl_ld_area_rentable             =s.area_rentable
        ,t.bl_ld_area_usable               =s.area_usable
        ,t.bl_ld_area_assignable           =s.area_assignable
        ,t.bl_ld_area_sft_vacant           =s.area_sft_vacant
        ,t.bl_ld_area_percent_vacant       =s.area_percent_vacant
        ,t.bl_ld_area_levels               =s.area_levels
        ,t.bl_ld_area_units                =s.area_units
        ,t.bl_ld_area_load_factor          =s.area_load_factor
        ,t.bl_ld_occu_status_code          =s.occu_status_code
        ,t.bl_ld_occu_status               =s.occu_status
        ,t.bl_ld_occu_emp_assignable       =s.occu_emp_assignable
        ,t.bl_ld_occu_dept_assignable      =s.occu_dept_assignable
        ,t.bl_ld_occu_cust_assignable      =s.occu_cust_assignable
        ,t.bl_ld_occu_disposition_code     =s.occu_disposition_code
        ,t.bl_ld_occu_disposition          =s.occu_disposition
        ,t.bl_ld_occu_acc_treatment_code   =s.occu_acc_treatment_code
        ,t.bl_ld_occu_acc_treatment        =s.occu_acc_treatment
        ,t.bl_ld_occu_maximum              =s.occu_maximum
        ,t.bl_ld_occu_optimum              =s.occu_optimum
        ,t.bl_ld_occu_utilized             =s.occu_utilized
        ,t.bl_ld_occu_percent_max          =s.occu_percent_max
        ,t.bl_ld_occu_area_utilized        =s.occu_area_utilized
        ,t.bl_ld_occu_max_vacancy          =s.occu_max_vacancy
        ,t.address_id                      =s.address_id
        ,t.country_code                    =s.country
        ,t.address_country                 =s.address_country
        ,t.address_line1                   =s.address_line1
        ,t.address_line2                   =s.address_line2
        ,t.address_line3                   =s.address_line3
        ,t.city                            =s.city
        ,t.state                           =s.state
        ,t.province                        =s.province
        ,t.postal_code                     =s.postal_code
        ,t.county                          =s.county
        ,t.bldg_attribute_category         =case when s.attribute_category ='BUILDING' then s.attribute_category else Null end
        ,t.bldg_longitude                  =case when s.attribute_category ='BUILDING' then s.attribute1 else Null end
        ,t.bldg_latitude                   =case when s.attribute_category ='BUILDING' then s.attribute2 else Null end
        ,t.bldg_attribute3                 =case when s.attribute_category ='BUILDING' then s.attribute3 else Null end
        ,t.bldg_attribute4                 =case when s.attribute_category ='BUILDING' then s.attribute4 else Null end
        ,t.bldg_attribute5                 =case when s.attribute_category ='BUILDING' then s.attribute5 else Null end
        ,t.bldg_documentum_link            =case when s.attribute_category ='BUILDING' then s.attribute6 else Null end
        ,t.bldg_photo_link                 =case when s.attribute_category ='BUILDING' then s.attribute7 else Null end
        ,t.bldg_attribute8                 =case when s.attribute_category ='BUILDING' then s.attribute8 else Null end
        ,t.bldg_attribute9                 =case when s.attribute_category ='BUILDING' then s.attribute9 else Null end
        ,t.bldg_attribute10                =case when s.attribute_category ='BUILDING' then s.attribute10 else Null end
        ,t.bldg_attribute11                =case when s.attribute_category ='BUILDING' then s.attribute11 else Null end
        ,t.bldg_attribute12                =case when s.attribute_category ='BUILDING' then s.attribute12 else Null end
        ,t.bldg_attribute13                =case when s.attribute_category ='BUILDING' then s.attribute13 else Null end
        ,t.bldg_attribute14                =case when s.attribute_category ='BUILDING' then s.attribute14 else Null end
        ,t.bldg_attribute15                =case when s.attribute_category ='BUILDING' then s.attribute15 else Null end
        ,t.land_attribute_category         =case when s.attribute_category ='LAND' then s.attribute_category else Null end
        ,t.land_longitude                  =case when s.attribute_category ='LAND' then s.attribute1 else Null end
        ,t.land_latitude                   =case when s.attribute_category ='LAND' then s.attribute2 else Null end
        ,t.land_attribute3                 =case when s.attribute_category ='LAND' then s.attribute3 else Null end
        ,t.land_attribute4                 =case when s.attribute_category ='LAND' then s.attribute4 else Null end
        ,t.land_attribute5                 =case when s.attribute_category ='LAND' then s.attribute5 else Null end
        ,t.land_attribute6                 =case when s.attribute_category ='LAND' then s.attribute6 else Null end
        ,t.land_attribute7                 =case when s.attribute_category ='LAND' then s.attribute7 else Null end
        ,t.land_insignificant              =case when s.attribute_category ='LAND' then s.attribute8 else Null end
        ,t.land_attribute9                 =case when s.attribute_category ='LAND' then s.attribute9 else Null end
        ,t.land_attribute10                =case when s.attribute_category ='LAND' then s.attribute10 else Null end
        ,t.land_attribute11                =case when s.attribute_category ='LAND' then s.attribute11 else Null end
        ,t.land_attribute12                =case when s.attribute_category ='LAND' then s.attribute12 else Null end
        ,t.land_attribute13                =case when s.attribute_category ='LAND' then s.attribute13 else Null end
        ,t.land_attribute14                =case when s.attribute_category ='LAND' then s.attribute14 else Null end
        ,t.land_attribute15                =case when s.attribute_category ='LAND' then s.attribute15 else Null end;
      --
      commit;
      --
      print_log('Merged attributes for land rows, location_id =>'||hds_lands(a1).opn_id);
      --
     exception
      when others then
       print_log('Failed to merge attributes for land rows, location_id =>'||hds_lands(a1).opn_id||', msg ='||sqlerrm);
     end;
    --
    end loop;
    --
   end if;
   --
    b_proceed :=TRUE;
   --
  exception
   when others then
    --
     print_log('Issue in cursor c_get_lands'||sqlerrm);
     b_proceed :=FALSE;
    --
  end;
  --
 exception
  when others then
   print_log('@ routine land_post_process, msg ='||sqlerrm);
 end land_post_process;
 --
 procedure parcel_post_process is
 --
    cursor c_get_parcels is
    select opn_id
          ,opn_type
          ,opn_code
          ,parent_location_id land_id --to_number(null) land_id
          ,loc_active_start_date
          ,count(*) total
    from   xxcus.xxcus_opn_properties_all
    where  1 =1
      --and  opn_id =1499
      and  opn_type ='PARCEL'
    group by opn_id, opn_type, opn_code, parent_location_id, loc_active_start_date;
 --
 type parcels is table of c_get_parcels%rowtype index by binary_integer;
 hds_parcels parcels;
 --
 -- Variables
   b_proceed BOOLEAN :=FALSE;
 --
 begin
  --
  begin
   --
   open c_get_parcels;
   fetch c_get_parcels bulk collect into hds_parcels;
   close c_get_parcels;
   --
   if hds_parcels.count >0 then
    --
    for a1 in 1 .. hds_parcels.count loop
    --
    /*
    begin
    --
     select parent_location_id
     into   hds_parcels(a1).land_id
     from   pn_floors_v
     where  1 =1
       and  location_id =hds_parcels(a1).opn_id;
    --
     print_log('hds_parcels('||a1||').land_id ='||hds_parcels(a1).land_id);
    --
     b_proceed :=TRUE;
    --
    exception
     when others then
      print_log('Issue in fetch of land details for parcel, location_id ='||hds_parcels(a1).opn_id);
      print_log('Issue in fetch of land details for parcel, message ='||sqlerrm);
      b_proceed :=FALSE;
    end;
    */
    --
     if (b_proceed) then
         begin
          --
            merge into xxcus.xxcus_opn_properties_all t
            using (
                    select a.*
                    from   apps.xxcus_opn_bldg_details_v a
                    where  1 =1
                      and  a.location_id =hds_parcels(a1).land_id
                      and  rownum <2
                      --and  nvl(active_end_date, trunc(sysdate)) >=trunc(sysdate)
                   ) s
            on (
                    t.opn_id                 =hds_parcels(a1).opn_id --Update land details for the parcel rows
                --and t.loc_active_start_date  =hds_parcels(a1).loc_active_start_date
                and t.opn_type               ='PARCEL'
               )
            when matched then update set
             t.bl_ld_lob_nickname              =s.name
            ,t.bl_ld_ID                        =s.ID
            ,t.bl_ld_tenancy_code              =s.tenancy_code
            ,t.bl_ld_tenancy                   =s.tenancy
            ,t.bl_ld_complexity_code           =s.complexity_code
            ,t.bl_ld_complexity                =s.complexity
            ,t.bl_ld_area_uom_code             =s.area_uom_code
            ,t.bl_ld_area_uom                  =s.area_uom
            ,t.fl_pr_area_uom_code             =s.area_uom_code --Null
            ,t.fl_pr_area_uom                  =s.area_uom      --Null
            ,t.bl_ld_area_gross                =s.area_gross
            ,t.bl_ld_area_rentable             =s.area_rentable
            ,t.bl_ld_area_usable               =s.area_usable
            ,t.bl_ld_area_assignable           =s.area_assignable
            ,t.bl_ld_area_sft_vacant           =s.area_sft_vacant
            ,t.bl_ld_area_percent_vacant       =s.area_percent_vacant
            ,t.bl_ld_area_levels               =s.area_levels
            ,t.bl_ld_area_units                =s.area_units
            ,t.bl_ld_area_load_factor          =s.area_load_factor
            ,t.bl_ld_occu_status_code          =s.occu_status_code
            ,t.bl_ld_occu_status               =s.occu_status
            ,t.bl_ld_occu_emp_assignable       =s.occu_emp_assignable
            ,t.bl_ld_occu_dept_assignable      =s.occu_dept_assignable
            ,t.bl_ld_occu_cust_assignable      =s.occu_cust_assignable
            ,t.bl_ld_occu_disposition_code     =s.occu_disposition_code
            ,t.bl_ld_occu_disposition          =s.occu_disposition
            ,t.bl_ld_occu_acc_treatment_code   =s.occu_acc_treatment_code
            ,t.bl_ld_occu_acc_treatment        =s.occu_acc_treatment
            ,t.bl_ld_occu_maximum              =s.occu_maximum
            ,t.bl_ld_occu_optimum              =s.occu_optimum
            ,t.bl_ld_occu_utilized             =s.occu_utilized
            ,t.bl_ld_occu_percent_max          =s.occu_percent_max
            ,t.bl_ld_occu_area_utilized        =s.occu_area_utilized
            ,t.bl_ld_occu_max_vacancy          =s.occu_max_vacancy
            ,t.address_id                      =s.address_id
            ,t.country_code                    =s.country
            ,t.address_country                 =s.address_country
            ,t.address_line1                   =s.address_line1
            ,t.address_line2                   =s.address_line2
            ,t.address_line3                   =s.address_line3
            ,t.city                            =s.city
            ,t.state                           =s.state
            ,t.province                        =s.province
            ,t.postal_code                     =s.postal_code
            ,t.county                          =s.county
            ,t.bldg_attribute_category         =case when s.attribute_category ='BUILDING' then s.attribute_category else Null end
            ,t.bldg_longitude                  =case when s.attribute_category ='BUILDING' then s.attribute1 else Null end
            ,t.bldg_latitude                   =case when s.attribute_category ='BUILDING' then s.attribute2 else Null end
            ,t.bldg_attribute3                 =case when s.attribute_category ='BUILDING' then s.attribute3 else Null end
            ,t.bldg_attribute4                 =case when s.attribute_category ='BUILDING' then s.attribute4 else Null end
            ,t.bldg_attribute5                 =case when s.attribute_category ='BUILDING' then s.attribute5 else Null end
            ,t.bldg_documentum_link            =case when s.attribute_category ='BUILDING' then s.attribute6 else Null end
            ,t.bldg_photo_link                 =case when s.attribute_category ='BUILDING' then s.attribute7 else Null end
            ,t.bldg_attribute8                 =case when s.attribute_category ='BUILDING' then s.attribute8 else Null end
            ,t.bldg_attribute9                 =case when s.attribute_category ='BUILDING' then s.attribute9 else Null end
            ,t.bldg_attribute10                =case when s.attribute_category ='BUILDING' then s.attribute10 else Null end
            ,t.bldg_attribute11                =case when s.attribute_category ='BUILDING' then s.attribute11 else Null end
            ,t.bldg_attribute12                =case when s.attribute_category ='BUILDING' then s.attribute12 else Null end
            ,t.bldg_attribute13                =case when s.attribute_category ='BUILDING' then s.attribute13 else Null end
            ,t.bldg_attribute14                =case when s.attribute_category ='BUILDING' then s.attribute14 else Null end
            ,t.bldg_attribute15                =case when s.attribute_category ='BUILDING' then s.attribute15 else Null end
            ,t.land_attribute_category         =case when s.attribute_category ='LAND' then s.attribute_category else Null end
            ,t.land_longitude                  =case when s.attribute_category ='LAND' then s.attribute1 else Null end
            ,t.land_latitude                   =case when s.attribute_category ='LAND' then s.attribute2 else Null end
            ,t.land_attribute3                 =case when s.attribute_category ='LAND' then s.attribute3 else Null end
            ,t.land_attribute4                 =case when s.attribute_category ='LAND' then s.attribute4 else Null end
            ,t.land_attribute5                 =case when s.attribute_category ='LAND' then s.attribute5 else Null end
            ,t.land_attribute6                 =case when s.attribute_category ='LAND' then s.attribute6 else Null end
            ,t.land_attribute7                 =case when s.attribute_category ='LAND' then s.attribute7 else Null end
            ,t.land_insignificant              =case when s.attribute_category ='LAND' then s.attribute8 else Null end
            ,t.land_attribute9                 =case when s.attribute_category ='LAND' then s.attribute9 else Null end
            ,t.land_attribute10                =case when s.attribute_category ='LAND' then s.attribute10 else Null end
            ,t.land_attribute11                =case when s.attribute_category ='LAND' then s.attribute11 else Null end
            ,t.land_attribute12                =case when s.attribute_category ='LAND' then s.attribute12 else Null end
            ,t.land_attribute13                =case when s.attribute_category ='LAND' then s.attribute13 else Null end
            ,t.land_attribute14                =case when s.attribute_category ='LAND' then s.attribute14 else Null end
            ,t.land_attribute15                =case when s.attribute_category ='LAND' then s.attribute15 else Null end;
          --
          print_log(
                     'Merged land details for parcel records, location_id =>'
                      ||hds_parcels(a1).opn_id
                   );
          --
          commit;
          --
            merge into xxcus.xxcus_opn_properties_all t
            using (
                    select a.*
                    from   apps.xxcus_opn_floors_details_v a
                    where  1 =1
                      and  a.location_id        =hds_parcels(a1).opn_id
                      and  a.active_start_date  =hds_parcels(a1).loc_active_start_date
                   ) s
            on (
                     t.opn_id                 =hds_parcels(a1).opn_id --Update parcel details for the parcel rows
                and  t.loc_active_start_date  =hds_parcels(a1).loc_active_start_date
                --and  t.opn_type               ='PARCEL'
               )
            when matched then update set
                 t.fl_pr_name                     =s.name
                ,t.fl_pr_id                       =s.ID
                ,t.fl_pr_area_gross               =Null
                ,t.fl_pr_area_rentable            =s.area_rentable
                ,t.fl_pr_area_usable              =s.area_usable
                ,t.fl_pr_area_assignable          =s.area_assignable
                ,t.fl_pr_area_sft_vacant          =s.vacant_area
                ,t.fl_pr_area_percent_vacant      =Null
                ,t.fl_pr_area_levels              =Null
                ,t.fl_pr_area_units               =Null
                ,t.fl_pr_area_load_factor         =Null
                ,t.fl_pr_occu_status_code         =s.occu_status_code
                ,t.fl_pr_occu_status              =s.occu_status
                ,t.fl_pr_occu_emp_assignable      =s.occu_emp_assignable
                ,t.fl_pr_occu_dept_assignable     =s.occu_dept_assignable
                ,t.fl_pr_occu_cust_assignable     =s.occu_cust_assignable
                ,t.fl_pr_occu_disposition_code    =s.occu_disposition_code
                ,t.fl_pr_occu_disposition         =s.occu_disposition
                ,t.fl_pr_occu_acc_treatment_code  =s.occu_acc_treatment_code
                ,t.fl_pr_occu_acc_treatment       =s.occu_acc_treatment
                ,t.fl_pr_occu_maximum             =s.occu_maximum
                ,t.fl_pr_occu_optimum             =s.occu_optimum
                ,t.fl_pr_occu_utilized            =s.occu_area_utilized
                ,t.fl_pr_occu_area_utilized       =s.occu_area_utilized
                ,t.fl_pr_occu_max_vacancy         =s.occu_max_vacancy
                ,t.fl_pr_vacant_area              =s.vacant_area
                ,t.fl_pr_common                   =s.common
                ,t.fl_pr_primary_circulation      =s.primary_circulation
                ,t.fl_pr_primary_ops_type_code    =s.primary_ops_type_code
                ,t.fl_pr_primary_ops_type         =s.primary_ops_type
                ,t.fl_pr_function_ops_type_code   =s.function_ops_type_code
                ,t.fl_pr_function_type            =s.function_type
                ,t.fl_pr_standard_type_type_code  =s.standard_type_type_code
                ,t.fl_pr_standard_type            =s.standard_type
                ,t.floor_attribute_category       =case when s.attribute_category ='FLOOR' then s.attribute_category else Null end
                ,t.floor_attribute1               =case when s.attribute_category ='FLOOR' then s.attribute1 else Null end
                ,t.floor_attribute2               =case when s.attribute_category ='FLOOR' then s.attribute2 else Null end
                ,t.floor_attribute3               =case when s.attribute_category ='FLOOR' then s.attribute3 else Null end
                ,t.floor_attribute4               =case when s.attribute_category ='FLOOR' then s.attribute4 else Null end
                ,t.floor_attribute5               =case when s.attribute_category ='FLOOR' then s.attribute5 else Null end
                ,t.floor_attribute6               =case when s.attribute_category ='FLOOR' then s.attribute6 else Null end
                ,t.floor_attribute7               =case when s.attribute_category ='FLOOR' then s.attribute7 else Null end
                ,t.floor_attribute8               =case when s.attribute_category ='FLOOR' then s.attribute8 else Null end
                ,t.floor_attribute9               =case when s.attribute_category ='FLOOR' then s.attribute9 else Null end
                ,t.floor_attribute10              =case when s.attribute_category ='FLOOR' then s.attribute1 else Null end
                ,t.floor_attribute11              =case when s.attribute_category ='FLOOR' then s.attribute11 else Null end
                ,t.floor_attribute12              =case when s.attribute_category ='FLOOR' then s.attribute12 else Null end
                ,t.floor_attribute13              =case when s.attribute_category ='FLOOR' then s.attribute13 else Null end
                ,t.floor_attribute14              =case when s.attribute_category ='FLOOR' then s.attribute14 else Null end
                ,t.floor_attribute15              =case when s.attribute_category ='FLOOR' then s.attribute15 else Null end
                ,t.parcel_attribute_category      =case when s.attribute_category ='PARCEL' then s.attribute_category else Null end
                ,t.parcel_parcel                  =case when s.attribute_category ='PARCEL' then s.attribute1 else Null end
                ,t.parcel_attribute2              =case when s.attribute_category ='PARCEL' then s.attribute2 else Null end
                ,t.parcel_attribute3              =case when s.attribute_category ='PARCEL' then s.attribute3 else Null end
                ,t.parcel_attribute4              =case when s.attribute_category ='PARCEL' then s.attribute4 else Null end
                ,t.parcel_attribute5              =case when s.attribute_category ='PARCEL' then s.attribute5 else Null end
                ,t.parcel_attribute6              =case when s.attribute_category ='PARCEL' then s.attribute6 else Null end
                ,t.parcel_attribute7              =case when s.attribute_category ='PARCEL' then s.attribute7 else Null end
                ,t.parcel_attribute8              =case when s.attribute_category ='PARCEL' then s.attribute8 else Null end
                ,t.parcel_tax_assessor            =case when s.attribute_category ='PARCEL' then s.attribute9 else Null end
                ,t.parcel_attribute10             =case when s.attribute_category ='PARCEL' then s.attribute10 else Null end
                ,t.parcel_attribute11             =case when s.attribute_category ='PARCEL' then s.attribute11 else Null end
                ,t.parcel_attribute12             =case when s.attribute_category ='PARCEL' then s.attribute12 else Null end
                ,t.parcel_attribute13             =case when s.attribute_category ='PARCEL' then s.attribute13 else Null end
                ,t.parcel_attribute14             =case when s.attribute_category ='PARCEL' then s.attribute14 else Null end
                ,t.parcel_attribute15             =case when s.attribute_category ='PARCEL' then s.attribute15 else Null end;
          --
          commit;
          --
          print_log('Merged parcel details, location_id =>'||hds_parcels(a1).opn_id);
          --
         exception
          when others then
           print_log('Failed to merge attributes for parcel records, location_id =>'||hds_parcels(a1).opn_id||', msg ='||sqlerrm);
         end;
        --
     else
       print_log('@parcel_post_process, b_proceed is FALSE, ');
     end if;
    --
    end loop;
    --
   end if;
   --
    b_proceed :=TRUE;
   --
  exception
   when others then
    --
     print_log('Issue in cursor c_get_parcels'||sqlerrm);
     b_proceed :=FALSE;
    --
  end;
  --
 exception
  when others then
   print_log('@ routine parcel_post_process, msg ='||sqlerrm);
 end parcel_post_process;
 --
 procedure yard_post_process is
 --
    cursor c_get_yards is
    select opn_id
          ,opn_type
          ,opn_code
          ,to_number(null)        land_id
          ,parent_location_id     parcel_id
          ,loc_active_start_date  loc_active_start_date
          ,count(*)               total
    from   xxcus.xxcus_opn_properties_all
    where  1 =1
      --and  opn_id =2896
      and  opn_type ='SECTION'
    group by opn_id, opn_type, opn_code, parent_location_id, loc_active_start_date;
 --
 type yards is table of c_get_yards%rowtype index by binary_integer;
 hds_yards yards;
 --
 -- Variables
   b_proceed BOOLEAN :=FALSE;
 --
 begin
  --
  begin
   --
   open c_get_yards;
   fetch c_get_yards bulk collect into hds_yards;
   close c_get_yards;
   --
   if hds_yards.count >0 then
    --
    for a1 in 1 .. hds_yards.count loop
    --
    begin
    --
    /*
     select parent_location_id
     into   hds_yards(a1).parcel_id
     from   pn_offices_v
     where  1 =1
       and  location_id =hds_yards(a1).opn_id;
       */
    --
     --print_log('hds_offices('||a1||').floor_id ='||hds_offices(a1).floor_id);
    --
        begin
        --
         select parent_location_id
         into   hds_yards(a1).land_id
         from   pn_floors_v
         where  1 =1
           and  location_id =hds_yards(a1).parcel_id
           and  rownum <2;
        --
         print_log('hds_yards('||a1||').land_id ='||hds_yards(a1).land_id||', parcel_id ='||hds_yards(a1).parcel_id);
        --
        exception
         when others then
          print_log('Issue in fetch of land details for yard, location_id ='||hds_yards(a1).opn_id);
          print_log('Issue in fetch of land details for yard, message ='||sqlerrm);
          b_proceed :=FALSE;
        end;
    --
     b_proceed :=TRUE;
    --
    exception
     when others then
      print_log('Issue in fetch of parcel details for yard, location_id ='||hds_yards(a1).opn_id);
      print_log('Issue in fetch of parcel details for yard, message ='||sqlerrm);
      b_proceed :=FALSE;
    end;
    --
     if (b_proceed) then
         begin
          --
            merge into xxcus.xxcus_opn_properties_all t
            using (
                    select a.*
                    from   apps.xxcus_opn_bldg_details_v a
                    where  1 =1
                      and  a.location_id =hds_yards(a1).land_id
                      and  rownum <2
                      --and  nvl(active_end_date, trunc(sysdate)) >=trunc(sysdate)
                   ) s
            on (
                    t.opn_id                 =hds_yards(a1).opn_id --Update land details for the yard rows
                and t.loc_active_start_date  =hds_yards(a1).loc_active_start_date
                and t.opn_type               ='SECTION'
               )
            when matched then update set
             t.bl_ld_lob_nickname              =s.name
            ,t.bl_ld_ID                        =s.ID
            ,t.bl_ld_tenancy_code              =s.tenancy_code
            ,t.bl_ld_tenancy                   =s.tenancy
            ,t.bl_ld_complexity_code           =s.complexity_code
            ,t.bl_ld_complexity                =s.complexity
            ,t.bl_ld_area_uom_code             =s.area_uom_code
            ,t.bl_ld_area_uom                  =s.area_uom
            ,t.fl_pr_area_uom_code             =s.area_uom_code --parcel UOM will always be the same as land
            ,t.fl_pr_area_uom                  =s.area_uom      --parcel UOM will always be the same as land
            ,t.bl_ld_area_gross                =s.area_gross
            ,t.bl_ld_area_rentable             =s.area_rentable
            ,t.bl_ld_area_usable               =s.area_usable
            ,t.bl_ld_area_assignable           =s.area_assignable
            ,t.bl_ld_area_sft_vacant           =s.area_sft_vacant
            ,t.bl_ld_area_percent_vacant       =s.area_percent_vacant
            ,t.bl_ld_area_levels               =s.area_levels
            ,t.bl_ld_area_units                =s.area_units
            ,t.bl_ld_area_load_factor          =s.area_load_factor
            ,t.bl_ld_occu_status_code          =s.occu_status_code
            ,t.bl_ld_occu_status               =s.occu_status
            ,t.bl_ld_occu_emp_assignable       =s.occu_emp_assignable
            ,t.bl_ld_occu_dept_assignable      =s.occu_dept_assignable
            ,t.bl_ld_occu_cust_assignable      =s.occu_cust_assignable
            ,t.bl_ld_occu_disposition_code     =s.occu_disposition_code
            ,t.bl_ld_occu_disposition          =s.occu_disposition
            ,t.bl_ld_occu_acc_treatment_code   =s.occu_acc_treatment_code
            ,t.bl_ld_occu_acc_treatment        =s.occu_acc_treatment
            ,t.bl_ld_occu_maximum              =s.occu_maximum
            ,t.bl_ld_occu_optimum              =s.occu_optimum
            ,t.bl_ld_occu_utilized             =s.occu_utilized
            ,t.bl_ld_occu_percent_max          =s.occu_percent_max
            ,t.bl_ld_occu_area_utilized        =s.occu_area_utilized
            ,t.bl_ld_occu_max_vacancy          =s.occu_max_vacancy
            ,t.address_id                      =s.address_id
            ,t.country_code                    =s.country
            ,t.address_country                 =s.address_country
            ,t.address_line1                   =s.address_line1
            ,t.address_line2                   =s.address_line2
            ,t.address_line3                   =s.address_line3
            ,t.city                            =s.city
            ,t.state                           =s.state
            ,t.province                        =s.province
            ,t.postal_code                     =s.postal_code
            ,t.county                          =s.county
            ,t.bldg_attribute_category         =case when s.attribute_category ='BUILDING' then s.attribute_category else Null end
            ,t.bldg_longitude                  =case when s.attribute_category ='BUILDING' then s.attribute1 else Null end
            ,t.bldg_latitude                   =case when s.attribute_category ='BUILDING' then s.attribute2 else Null end
            ,t.bldg_attribute3                 =case when s.attribute_category ='BUILDING' then s.attribute3 else Null end
            ,t.bldg_attribute4                 =case when s.attribute_category ='BUILDING' then s.attribute4 else Null end
            ,t.bldg_attribute5                 =case when s.attribute_category ='BUILDING' then s.attribute5 else Null end
            ,t.bldg_documentum_link            =case when s.attribute_category ='BUILDING' then s.attribute6 else Null end
            ,t.bldg_photo_link                 =case when s.attribute_category ='BUILDING' then s.attribute7 else Null end
            ,t.bldg_attribute8                 =case when s.attribute_category ='BUILDING' then s.attribute8 else Null end
            ,t.bldg_attribute9                 =case when s.attribute_category ='BUILDING' then s.attribute9 else Null end
            ,t.bldg_attribute10                =case when s.attribute_category ='BUILDING' then s.attribute10 else Null end
            ,t.bldg_attribute11                =case when s.attribute_category ='BUILDING' then s.attribute11 else Null end
            ,t.bldg_attribute12                =case when s.attribute_category ='BUILDING' then s.attribute12 else Null end
            ,t.bldg_attribute13                =case when s.attribute_category ='BUILDING' then s.attribute13 else Null end
            ,t.bldg_attribute14                =case when s.attribute_category ='BUILDING' then s.attribute14 else Null end
            ,t.bldg_attribute15                =case when s.attribute_category ='BUILDING' then s.attribute15 else Null end
            ,t.land_attribute_category         =case when s.attribute_category ='LAND' then s.attribute_category else Null end
            ,t.land_longitude                  =case when s.attribute_category ='LAND' then s.attribute1 else Null end
            ,t.land_latitude                   =case when s.attribute_category ='LAND' then s.attribute2 else Null end
            ,t.land_attribute3                 =case when s.attribute_category ='LAND' then s.attribute3 else Null end
            ,t.land_attribute4                 =case when s.attribute_category ='LAND' then s.attribute4 else Null end
            ,t.land_attribute5                 =case when s.attribute_category ='LAND' then s.attribute5 else Null end
            ,t.land_attribute6                 =case when s.attribute_category ='LAND' then s.attribute6 else Null end
            ,t.land_attribute7                 =case when s.attribute_category ='LAND' then s.attribute7 else Null end
            ,t.land_insignificant              =case when s.attribute_category ='LAND' then s.attribute8 else Null end
            ,t.land_attribute9                 =case when s.attribute_category ='LAND' then s.attribute9 else Null end
            ,t.land_attribute10                =case when s.attribute_category ='LAND' then s.attribute10 else Null end
            ,t.land_attribute11                =case when s.attribute_category ='LAND' then s.attribute11 else Null end
            ,t.land_attribute12                =case when s.attribute_category ='LAND' then s.attribute12 else Null end
            ,t.land_attribute13                =case when s.attribute_category ='LAND' then s.attribute13 else Null end
            ,t.land_attribute14                =case when s.attribute_category ='LAND' then s.attribute14 else Null end
            ,t.land_attribute15                =case when s.attribute_category ='LAND' then s.attribute15 else Null end;
          --
          print_log(
                     'Merged land details for yard rows, location_id =>'
                      ||hds_yards(a1).opn_id
                   );
          --
          commit;
          --
         exception
          when others then
           print_log('Failed to merge attributes for land records, yard_location_id =>'||hds_yards(a1).opn_id||', msg ='||sqlerrm);
         end;
        --
         begin
          --
            merge into xxcus.xxcus_opn_properties_all t
            using (
                    select a.*
                    from   apps.xxcus_opn_floors_details_v a
                    where  1 =1
                      and  a.location_id =hds_yards(a1).parcel_id
                      and  rownum <2
                      --and  nvl(active_end_date, trunc(sysdate)) >=trunc(sysdate)
                   ) s
            on (
                    t.opn_id                 =hds_yards(a1).opn_id --Update parcel details for the yard rows
                --and t.loc_active_start_date  =hds_yards(a1).loc_active_start_date
                and t.opn_type               ='SECTION'
               )
            when matched then update set
                 t.fl_pr_name                     =s.name
                ,t.fl_pr_id                       =s.ID
                ,t.fl_pr_area_gross               =Null
                ,t.fl_pr_area_rentable            =s.area_rentable
                ,t.fl_pr_area_usable              =s.area_usable
                ,t.fl_pr_area_assignable          =s.area_assignable
                ,t.fl_pr_area_sft_vacant          =s.vacant_area
                ,t.fl_pr_area_percent_vacant      =Null
                ,t.fl_pr_area_levels              =Null
                ,t.fl_pr_area_units               =Null
                ,t.fl_pr_area_load_factor         =Null
                ,t.fl_pr_occu_status_code         =s.occu_status_code
                ,t.fl_pr_occu_status              =s.occu_status
                ,t.fl_pr_occu_emp_assignable      =s.occu_emp_assignable
                ,t.fl_pr_occu_dept_assignable     =s.occu_dept_assignable
                ,t.fl_pr_occu_cust_assignable     =s.occu_cust_assignable
                ,t.fl_pr_occu_disposition_code    =s.occu_disposition_code
                ,t.fl_pr_occu_disposition         =s.occu_disposition
                ,t.fl_pr_occu_acc_treatment_code  =s.occu_acc_treatment_code
                ,t.fl_pr_occu_acc_treatment       =s.occu_acc_treatment
                ,t.fl_pr_occu_maximum             =s.occu_maximum
                ,t.fl_pr_occu_optimum             =s.occu_optimum
                ,t.fl_pr_occu_utilized            =s.occu_area_utilized
                ,t.fl_pr_occu_area_utilized       =s.occu_area_utilized
                ,t.fl_pr_occu_max_vacancy         =s.occu_max_vacancy
                ,t.fl_pr_vacant_area              =s.vacant_area
                ,t.fl_pr_common                   =s.common
                ,t.fl_pr_primary_circulation      =s.primary_circulation
                ,t.fl_pr_primary_ops_type_code    =s.primary_ops_type_code
                ,t.fl_pr_primary_ops_type         =s.primary_ops_type
                ,t.fl_pr_function_ops_type_code   =s.function_ops_type_code
                ,t.fl_pr_function_type            =s.function_type
                ,t.fl_pr_standard_type_type_code  =s.standard_type_type_code
                ,t.fl_pr_standard_type            =s.standard_type
                ,t.floor_attribute_category       =case when s.attribute_category ='FLOOR' then s.attribute_category else Null end
                ,t.floor_attribute1               =case when s.attribute_category ='FLOOR' then s.attribute1 else Null end
                ,t.floor_attribute2               =case when s.attribute_category ='FLOOR' then s.attribute2 else Null end
                ,t.floor_attribute3               =case when s.attribute_category ='FLOOR' then s.attribute3 else Null end
                ,t.floor_attribute4               =case when s.attribute_category ='FLOOR' then s.attribute4 else Null end
                ,t.floor_attribute5               =case when s.attribute_category ='FLOOR' then s.attribute5 else Null end
                ,t.floor_attribute6               =case when s.attribute_category ='FLOOR' then s.attribute6 else Null end
                ,t.floor_attribute7               =case when s.attribute_category ='FLOOR' then s.attribute7 else Null end
                ,t.floor_attribute8               =case when s.attribute_category ='FLOOR' then s.attribute8 else Null end
                ,t.floor_attribute9               =case when s.attribute_category ='FLOOR' then s.attribute9 else Null end
                ,t.floor_attribute10              =case when s.attribute_category ='FLOOR' then s.attribute1 else Null end
                ,t.floor_attribute11              =case when s.attribute_category ='FLOOR' then s.attribute11 else Null end
                ,t.floor_attribute12              =case when s.attribute_category ='FLOOR' then s.attribute12 else Null end
                ,t.floor_attribute13              =case when s.attribute_category ='FLOOR' then s.attribute13 else Null end
                ,t.floor_attribute14              =case when s.attribute_category ='FLOOR' then s.attribute14 else Null end
                ,t.floor_attribute15              =case when s.attribute_category ='FLOOR' then s.attribute15 else Null end
                ,t.parcel_attribute_category      =case when s.attribute_category ='PARCEL' then s.attribute_category else Null end
                ,t.parcel_parcel                  =case when s.attribute_category ='PARCEL' then s.attribute1 else Null end
                ,t.parcel_attribute2              =case when s.attribute_category ='PARCEL' then s.attribute2 else Null end
                ,t.parcel_attribute3              =case when s.attribute_category ='PARCEL' then s.attribute3 else Null end
                ,t.parcel_attribute4              =case when s.attribute_category ='PARCEL' then s.attribute4 else Null end
                ,t.parcel_attribute5              =case when s.attribute_category ='PARCEL' then s.attribute5 else Null end
                ,t.parcel_attribute6              =case when s.attribute_category ='PARCEL' then s.attribute6 else Null end
                ,t.parcel_attribute7              =case when s.attribute_category ='PARCEL' then s.attribute7 else Null end
                ,t.parcel_attribute8              =case when s.attribute_category ='PARCEL' then s.attribute8 else Null end
                ,t.parcel_tax_assessor            =case when s.attribute_category ='PARCEL' then s.attribute9 else Null end
                ,t.parcel_attribute10             =case when s.attribute_category ='PARCEL' then s.attribute10 else Null end
                ,t.parcel_attribute11             =case when s.attribute_category ='PARCEL' then s.attribute11 else Null end
                ,t.parcel_attribute12             =case when s.attribute_category ='PARCEL' then s.attribute12 else Null end
                ,t.parcel_attribute13             =case when s.attribute_category ='PARCEL' then s.attribute13 else Null end
                ,t.parcel_attribute14             =case when s.attribute_category ='PARCEL' then s.attribute14 else Null end
                ,t.parcel_attribute15             =case when s.attribute_category ='PARCEL' then s.attribute15 else Null end;
          --
          print_log('Merged parcel details for yard rows, location_id =>'||hds_yards(a1).opn_id);
          --
          commit;
         exception
          when others then
           print_log('Failed to merge attributes for parcel records, yard_location_id =>'||hds_yards(a1).opn_id||', msg ='||sqlerrm);
         end;
        --
         begin
            merge into xxcus.xxcus_opn_properties_all t
            using (
                    select a.*
                    from   apps.xxcus_opn_office_details_v a
                    where  1 =1
                      and  a.location_id =hds_yards(a1).opn_id
                      and  a.active_start_date =hds_yards(a1).loc_active_start_date
                   ) s
            on (
                     t.opn_id                 =hds_yards(a1).opn_id --Update yard details for the yard rows
                and  t.loc_active_start_date  =hds_yards(a1).loc_active_start_date
               )
            when matched then update set
                 t.sc_yd_name                     =s.name
                ,t.sc_yd_id                       =s.ID
                ,t.sc_yd_area_rentable            =s.rentable_area
                ,t.sc_yd_area_usable              =s.usable_area
                ,t.sc_yd_area_assignable          =s.assignable_area
                ,t.sc_yd_area_sft_vacant          =s.vacant_area
                ,t.sc_yd_occu_status_code         =s.occu_status_code
                ,t.sc_yd_occu_status              =s.occu_status
                ,t.sc_yd_occu_emp_assignable      =s.occu_emp_assignable
                ,t.sc_yd_occu_dept_assignable     =s.occu_dept_assignable
                ,t.sc_yd_occu_cust_assignable     =s.occu_cust_assignable
                ,t.sc_yd_occu_disposition_code    =s.occu_disposition_code
                ,t.sc_yd_occu_disposition         =s.occu_disposition
                ,t.sc_yd_occu_acc_treatment_code  =s.occu_acc_treatment_code
                ,t.sc_yd_occu_acc_treatment       =s.occu_acc_treatment
                ,t.sc_yd_occu_maximum             =s.occu_maximum
                ,t.sc_yd_occu_optimum             =s.occu_optimum
                ,t.sc_yd_occu_utilized            =s.occu_area_utilized
                ,t.sc_yd_occu_area_utilized       =s.occu_area_utilized
                ,t.sc_yd_occu_max_vacancy         =s.occu_max_vacancy
                ,t.sc_yd_vacant_area              =s.vacant_area
                ,t.sc_yd_common                   =s.common
                ,t.sc_yd_secondary_circulation    =s.secondary_circulation
                ,t.sc_yd_space_type_code          =s.space_type_code
                ,t.sc_yd_space_type               =s.space_type
                ,t.sc_yd_site_type_code           =s.site_type_code
                ,t.sc_yd_site_type                =s.site_type
                ,t.sc_yd_assignment_type_code     =s.assignment_type_code
                ,t.sc_yd_assignment_type          =s.assignment_type
                ,t.office_attribute_category      =case when s.attribute_category ='OFFICE' then s.attribute_category else Null end
                ,t.office_attribute1              =case when s.attribute_category ='OFFICE' then s.attribute1 else Null end
                ,t.office_attribute2              =case when s.attribute_category ='OFFICE' then s.attribute2 else Null end
                ,t.office_operating_hours         =case when s.attribute_category ='OFFICE' then s.attribute3 else Null end
                ,t.office_walkin                  =case when s.attribute_category ='OFFICE' then s.attribute4 else Null end
                ,t.office_customer_site           =case when s.attribute_category ='OFFICE' then s.attribute5 else Null end
                ,t.office_attribute6              =case when s.attribute_category ='OFFICE' then s.attribute6 else Null end
                ,t.office_attribute7              =case when s.attribute_category ='OFFICE' then s.attribute7 else Null end
                ,t.office_attribute8              =case when s.attribute_category ='OFFICE' then s.attribute8 else Null end
                ,t.office_attribute9              =case when s.attribute_category ='OFFICE' then s.attribute9 else Null end
                ,t.office_attribute10             =case when s.attribute_category ='OFFICE' then s.attribute10 else Null end
                ,t.office_attribute11             =case when s.attribute_category ='OFFICE' then s.attribute11 else Null end
                ,t.office_attribute12             =case when s.attribute_category ='OFFICE' then s.attribute12 else Null end
                ,t.office_attribute13             =case when s.attribute_category ='OFFICE' then s.attribute13 else Null end
                ,t.office_attribute14             =case when s.attribute_category ='OFFICE' then s.attribute14 else Null end
                ,t.office_attribute15             =case when s.attribute_category ='OFFICE' then s.attribute15 else Null end
                ,t.section_attribute_category     =case when s.attribute_category ='SECTION' then s.attribute_category else Null end
                ,t.section_attribute1             =case when s.attribute_category ='SECTION' then s.attribute1 else Null end
                ,t.section_attribute2             =case when s.attribute_category ='SECTION' then s.attribute2 else Null end
                ,t.section_operating_hours        =case when s.attribute_category ='SECTION' then s.attribute3 else Null end
                ,t.section_walkin                 =case when s.attribute_category ='SECTION' then s.attribute4 else Null end
                ,t.section_customer_site          =case when s.attribute_category ='SECTION' then s.attribute5 else Null end
                ,t.section_attribute6             =case when s.attribute_category ='SECTION' then s.attribute6 else Null end
                ,t.section_attribute7             =case when s.attribute_category ='SECTION' then s.attribute7 else Null end
                ,t.section_attribute8             =case when s.attribute_category ='SECTION' then s.attribute8 else Null end
                ,t.section_attribute9             =case when s.attribute_category ='SECTION' then s.attribute9 else Null end
                ,t.section_attribute10            =case when s.attribute_category ='SECTION' then s.attribute10 else Null end
                ,t.section_attribute11            =case when s.attribute_category ='SECTION' then s.attribute11 else Null end
                ,t.section_attribute12            =case when s.attribute_category ='SECTION' then s.attribute12 else Null end
                ,t.section_attribute13            =case when s.attribute_category ='SECTION' then s.attribute13 else Null end
                ,t.section_attribute14            =case when s.attribute_category ='SECTION' then s.attribute14 else Null end
                ,t.section_attribute15            =case when s.attribute_category ='SECTION' then s.attribute15 else Null end;
          --
          print_log('Merged office details for location_id =>'||hds_yards(a1).opn_id);
          --
          commit;
          --
         exception
          when others then
           print_log('Failed to merge attributes for yard records, yard_location_id =>'||hds_yards(a1).opn_id||', msg ='||sqlerrm);
         end;
        --
     else
       print_log('@yard_post_process, b_proceed is FALSE, ');
     end if;
    --
    end loop;
    --
   end if;
   --
    b_proceed :=TRUE;
   --
  exception
   when others then
    --
     print_log('Issue in cursor c_get_yards'||sqlerrm);
     b_proceed :=FALSE;
    --
  end;
  --
 exception
  when others then
   print_log('@ routine yard_post_process, msg ='||sqlerrm);
 end yard_post_process;
 --
 procedure update_property_attributes is
 --
    cursor c_get_nodes is
        select opn_id                     opn_id
              ,location_id                location_id
              ,parent_location_id         parent_loc_id
              ,location_type_lookup_code  loc_type
              ,to_number(null)            bldg_id
              ,loc_active_start_date      loc_active_start_date
              ,loc_active_end_date        loc_active_end_date
              ,count(*)                   total
        from   xxcus.xxcus_opn_properties_all
        where  1 =1
          and  opn_type ='PROPERTY'
          --and  location_id IN (372, 1499, 2896, 10885, 11012, 11013)
        group by opn_id, location_id, parent_location_id, location_type_lookup_code, loc_active_start_date, loc_active_end_date
        order by case
              when location_type_lookup_code ='PROPERTY' then 1
              when location_type_lookup_code ='BUILDING' then 2
              when location_type_lookup_code ='FLOOR' then 3
              when location_type_lookup_code ='OFFICE' then 4
              when location_type_lookup_code='LAND' then 5
              when location_type_lookup_code ='PARCEL' then 6
              when location_type_lookup_code ='SECTION' then 7
             end asc;
 --
 type prop_nodes is table of c_get_nodes%rowtype index by binary_integer;
 hds_child prop_nodes;
 --
   cursor get_location_attributes (p_locn_id in number, p_loc_type in varchar2, p_date in date) is
   select
     p_loc_type attribute_category
    ,attribute1
    ,attribute2
    ,attribute3
    ,attribute4
    ,attribute5
    ,attribute6
    ,attribute7
    ,attribute8
    ,attribute9
    ,attribute10
    ,attribute11
    ,attribute12
    ,attribute13
    ,attribute14
    ,attribute15
   from pn_locations_all
   where 1 =1
     and location_id =p_locn_id
     --and active_end_date >trunc(sysdate)
     and active_start_date =p_date;
   --
  attributes_rec       get_location_attributes%rowtype :=Null;
  --
  -- Variables
   b_proceed BOOLEAN :=FALSE;
   n_count NUMBER :=0;
   d_latest_start_date date :=Null;
  --
 begin
  --
  begin
   --
   open c_get_nodes;
   fetch c_get_nodes bulk collect into hds_child;
   close c_get_nodes;
   --
   if hds_child.count >0 then
    --
    for a1 in 1 .. hds_child.count loop
    --
     begin
      -- if building then update the building attributes of the property on the property record where location_type_lookup_code =BUILDING
      if hds_child(a1).loc_type IN ('BUILDING', 'LAND') then
       --
       print_log('hds_child(a1).opn_id ='||hds_child(a1).opn_id);
       print_log('hds_child(a1).loc_type ='||hds_child(a1).loc_type);
       print_log('hds_child(a1).location_id ='||hds_child(a1).location_id);
       print_log('hds_child(a1).loc_active_start_date ='||hds_child(a1).loc_active_start_date);
       --
        merge into xxcus.xxcus_opn_properties_all t
        using (
                select a.*
                from   apps.xxcus_opn_bldg_details_v a
                where  1 =1
                  and  a.location_id        =hds_child(a1).location_id
                  and  a.active_start_date  =hds_child(a1).loc_active_start_date
               ) s
        on (
                t.opn_id                     =hds_child(a1).opn_id
            and t.location_id                =s.location_id         --Update buidling details for the property rows where location type =BUILDING / LAND
            and t.location_type_lookup_code  =hds_child(a1).loc_type
            and t.loc_active_start_date      =s.active_start_date
           )
        when matched then update set
         t.bl_ld_lob_nickname              =s.name
        ,t.bl_ld_ID                        =s.ID
        ,t.bl_ld_tenancy_code              =s.tenancy_code
        ,t.bl_ld_tenancy                   =s.tenancy
        ,t.bl_ld_complexity_code           =s.complexity_code
        ,t.bl_ld_complexity                =s.complexity
        ,t.bl_ld_area_uom_code             =s.area_uom_code
        ,t.bl_ld_area_uom                  =s.area_uom
        ,t.bl_ld_area_gross                =s.area_gross
        ,t.bl_ld_area_rentable             =s.area_rentable
        ,t.bl_ld_area_usable               =s.area_usable
        ,t.bl_ld_area_assignable           =s.area_assignable
        ,t.bl_ld_area_sft_vacant           =s.area_sft_vacant
        ,t.bl_ld_area_percent_vacant       =s.area_percent_vacant
        ,t.bl_ld_area_levels               =s.area_levels
        ,t.bl_ld_area_units                =s.area_units
        ,t.bl_ld_area_load_factor          =s.area_load_factor
        ,t.bl_ld_occu_status_code          =s.occu_status_code
        ,t.bl_ld_occu_status               =s.occu_status
        ,t.bl_ld_occu_emp_assignable       =s.occu_emp_assignable
        ,t.bl_ld_occu_dept_assignable      =s.occu_dept_assignable
        ,t.bl_ld_occu_cust_assignable      =s.occu_cust_assignable
        ,t.bl_ld_occu_disposition_code     =s.occu_disposition_code
        ,t.bl_ld_occu_disposition          =s.occu_disposition
        ,t.bl_ld_occu_acc_treatment_code   =s.occu_acc_treatment_code
        ,t.bl_ld_occu_acc_treatment        =s.occu_acc_treatment
        ,t.bl_ld_occu_maximum              =s.occu_maximum
        ,t.bl_ld_occu_optimum              =s.occu_optimum
        ,t.bl_ld_occu_utilized             =s.occu_utilized
        ,t.bl_ld_occu_percent_max          =s.occu_percent_max
        ,t.bl_ld_occu_area_utilized        =s.occu_area_utilized
        ,t.bl_ld_occu_max_vacancy          =s.occu_max_vacancy
        ,t.address_id                      =s.address_id
        ,t.country_code                    =s.country
        ,t.address_country                 =s.address_country
        ,t.address_line1                   =s.address_line1
        ,t.address_line2                   =s.address_line2
        ,t.address_line3                   =s.address_line3
        ,t.city                            =s.city
        ,t.state                           =s.state
        ,t.province                        =s.province
        ,t.postal_code                     =s.postal_code
        ,t.county                          =s.county
        ,t.bldg_attribute_category         =case when s.attribute_category ='BUILDING' then s.attribute_category else Null end
        ,t.bldg_longitude                  =case when s.attribute_category ='BUILDING' then s.attribute1 else Null end
        ,t.bldg_latitude                   =case when s.attribute_category ='BUILDING' then s.attribute2 else Null end
        ,t.bldg_attribute3                 =case when s.attribute_category ='BUILDING' then s.attribute3 else Null end
        ,t.bldg_attribute4                 =case when s.attribute_category ='BUILDING' then s.attribute4 else Null end
        ,t.bldg_attribute5                 =case when s.attribute_category ='BUILDING' then s.attribute5 else Null end
        ,t.bldg_documentum_link            =case when s.attribute_category ='BUILDING' then s.attribute6 else Null end
        ,t.bldg_photo_link                 =case when s.attribute_category ='BUILDING' then s.attribute7 else Null end
        ,t.bldg_attribute8                 =case when s.attribute_category ='BUILDING' then s.attribute8 else Null end
        ,t.bldg_attribute9                 =case when s.attribute_category ='BUILDING' then s.attribute9 else Null end
        ,t.bldg_attribute10                =case when s.attribute_category ='BUILDING' then s.attribute10 else Null end
        ,t.bldg_attribute11                =case when s.attribute_category ='BUILDING' then s.attribute11 else Null end
        ,t.bldg_attribute12                =case when s.attribute_category ='BUILDING' then s.attribute12 else Null end
        ,t.bldg_attribute13                =case when s.attribute_category ='BUILDING' then s.attribute13 else Null end
        ,t.bldg_attribute14                =case when s.attribute_category ='BUILDING' then s.attribute14 else Null end
        ,t.bldg_attribute15                =case when s.attribute_category ='BUILDING' then s.attribute15 else Null end
        ,t.land_attribute_category         =case when s.attribute_category ='LAND' then s.attribute_category else Null end
        ,t.land_longitude                  =case when s.attribute_category ='LAND' then s.attribute1 else Null end
        ,t.land_latitude                   =case when s.attribute_category ='LAND' then s.attribute2 else Null end
        ,t.land_attribute3                 =case when s.attribute_category ='LAND' then s.attribute3 else Null end
        ,t.land_attribute4                 =case when s.attribute_category ='LAND' then s.attribute4 else Null end
        ,t.land_attribute5                 =case when s.attribute_category ='LAND' then s.attribute5 else Null end
        ,t.land_attribute6                 =case when s.attribute_category ='LAND' then s.attribute6 else Null end
        ,t.land_attribute7                 =case when s.attribute_category ='LAND' then s.attribute7 else Null end
        ,t.land_insignificant              =case when s.attribute_category ='LAND' then s.attribute8 else Null end
        ,t.land_attribute9                 =case when s.attribute_category ='LAND' then s.attribute9 else Null end
        ,t.land_attribute10                =case when s.attribute_category ='LAND' then s.attribute10 else Null end
        ,t.land_attribute11                =case when s.attribute_category ='LAND' then s.attribute11 else Null end
        ,t.land_attribute12                =case when s.attribute_category ='LAND' then s.attribute12 else Null end
        ,t.land_attribute13                =case when s.attribute_category ='LAND' then s.attribute13 else Null end
        ,t.land_attribute14                =case when s.attribute_category ='LAND' then s.attribute14 else Null end
        ,t.land_attribute15                =case when s.attribute_category ='LAND' then s.attribute15 else Null end;
        --
        print_log('Exit: Populate building-land details for property_id ='
                    ||hds_child(a1).opn_id||', location_id ='
                    ||hds_child(a1).location_id
                    ||', rowcount ='||sql%rowcount
                 );
        --
        commit;
        --
      elsif hds_child(a1).loc_type IN ('FLOOR', 'PARCEL') then
       --
            merge into xxcus.xxcus_opn_properties_all t
            using (
                    select a.*
                    from   apps.xxcus_opn_bldg_details_v a
                    where  1 =1
                      and  a.location_id        =hds_child(a1).parent_loc_id
                      and  rownum <2
                      --and  a.active_start_date  =hds_child(a1).loc_active_start_date
                      --and  nvl(a.active_end_date, trunc(sysdate)) >=trunc(sysdate) --@floor level to get bldg info, you don't need to worry about end_dated building records
                   ) s
            on (
                    t.opn_id                    =hds_child(a1).opn_id      --@property level
                and t.location_id               =hds_child(a1).location_id --look for a FLOOR record
                and t.parent_location_id        =s.location_id             --where parent is BUILDING
                and t.location_type_lookup_code =hds_child(a1).loc_type
               )
            when matched then update set
             t.bl_ld_lob_nickname              =s.name
            ,t.bl_ld_ID                        =s.ID
            ,t.bl_ld_tenancy_code              =s.tenancy_code
            ,t.bl_ld_tenancy                   =s.tenancy
            ,t.bl_ld_complexity_code           =s.complexity_code
            ,t.bl_ld_complexity                =s.complexity
            ,t.bl_ld_area_uom_code             =s.area_uom_code
            ,t.bl_ld_area_uom                  =s.area_uom
            ,t.bl_ld_area_gross                =s.area_gross
            ,t.bl_ld_area_rentable             =s.area_rentable
            ,t.bl_ld_area_usable               =s.area_usable
            ,t.bl_ld_area_assignable           =s.area_assignable
            ,t.bl_ld_area_sft_vacant           =s.area_sft_vacant
            ,t.bl_ld_area_percent_vacant       =s.area_percent_vacant
            ,t.bl_ld_area_levels               =s.area_levels
            ,t.bl_ld_area_units                =s.area_units
            ,t.bl_ld_area_load_factor          =s.area_load_factor
            ,t.bl_ld_occu_status_code          =s.occu_status_code
            ,t.bl_ld_occu_status               =s.occu_status
            ,t.bl_ld_occu_emp_assignable       =s.occu_emp_assignable
            ,t.bl_ld_occu_dept_assignable      =s.occu_dept_assignable
            ,t.bl_ld_occu_cust_assignable      =s.occu_cust_assignable
            ,t.bl_ld_occu_disposition_code     =s.occu_disposition_code
            ,t.bl_ld_occu_disposition          =s.occu_disposition
            ,t.bl_ld_occu_acc_treatment_code   =s.occu_acc_treatment_code
            ,t.bl_ld_occu_acc_treatment        =s.occu_acc_treatment
            ,t.bl_ld_occu_maximum              =s.occu_maximum
            ,t.bl_ld_occu_optimum              =s.occu_optimum
            ,t.bl_ld_occu_utilized             =s.occu_utilized
            ,t.bl_ld_occu_percent_max          =s.occu_percent_max
            ,t.bl_ld_occu_area_utilized        =s.occu_area_utilized
            ,t.bl_ld_occu_max_vacancy          =s.occu_max_vacancy
            ,t.address_id                      =s.address_id
            ,t.country_code                    =s.country
            ,t.address_country                 =s.address_country
            ,t.address_line1                   =s.address_line1
            ,t.address_line2                   =s.address_line2
            ,t.address_line3                   =s.address_line3
            ,t.city                            =s.city
            ,t.state                           =s.state
            ,t.province                        =s.province
            ,t.postal_code                     =s.postal_code
            ,t.county                          =s.county
            ,t.bldg_attribute_category         =case when s.attribute_category ='BUILDING' then s.attribute_category else Null end
            ,t.bldg_longitude                  =case when s.attribute_category ='BUILDING' then s.attribute1 else Null end
            ,t.bldg_latitude                   =case when s.attribute_category ='BUILDING' then s.attribute2 else Null end
            ,t.bldg_attribute3                 =case when s.attribute_category ='BUILDING' then s.attribute3 else Null end
            ,t.bldg_attribute4                 =case when s.attribute_category ='BUILDING' then s.attribute4 else Null end
            ,t.bldg_attribute5                 =case when s.attribute_category ='BUILDING' then s.attribute5 else Null end
            ,t.bldg_documentum_link            =case when s.attribute_category ='BUILDING' then s.attribute6 else Null end
            ,t.bldg_photo_link                 =case when s.attribute_category ='BUILDING' then s.attribute7 else Null end
            ,t.bldg_attribute8                 =case when s.attribute_category ='BUILDING' then s.attribute8 else Null end
            ,t.bldg_attribute9                 =case when s.attribute_category ='BUILDING' then s.attribute9 else Null end
            ,t.bldg_attribute10                =case when s.attribute_category ='BUILDING' then s.attribute10 else Null end
            ,t.bldg_attribute11                =case when s.attribute_category ='BUILDING' then s.attribute11 else Null end
            ,t.bldg_attribute12                =case when s.attribute_category ='BUILDING' then s.attribute12 else Null end
            ,t.bldg_attribute13                =case when s.attribute_category ='BUILDING' then s.attribute13 else Null end
            ,t.bldg_attribute14                =case when s.attribute_category ='BUILDING' then s.attribute14 else Null end
            ,t.bldg_attribute15                =case when s.attribute_category ='BUILDING' then s.attribute15 else Null end
            ,t.land_attribute_category         =case when s.attribute_category ='LAND' then s.attribute_category else Null end
            ,t.land_longitude                  =case when s.attribute_category ='LAND' then s.attribute1 else Null end
            ,t.land_latitude                   =case when s.attribute_category ='LAND' then s.attribute2 else Null end
            ,t.land_attribute3                 =case when s.attribute_category ='LAND' then s.attribute3 else Null end
            ,t.land_attribute4                 =case when s.attribute_category ='LAND' then s.attribute4 else Null end
            ,t.land_attribute5                 =case when s.attribute_category ='LAND' then s.attribute5 else Null end
            ,t.land_attribute6                 =case when s.attribute_category ='LAND' then s.attribute6 else Null end
            ,t.land_attribute7                 =case when s.attribute_category ='LAND' then s.attribute7 else Null end
            ,t.land_insignificant              =case when s.attribute_category ='LAND' then s.attribute8 else Null end
            ,t.land_attribute9                 =case when s.attribute_category ='LAND' then s.attribute9 else Null end
            ,t.land_attribute10                =case when s.attribute_category ='LAND' then s.attribute10 else Null end
            ,t.land_attribute11                =case when s.attribute_category ='LAND' then s.attribute11 else Null end
            ,t.land_attribute12                =case when s.attribute_category ='LAND' then s.attribute12 else Null end
            ,t.land_attribute13                =case when s.attribute_category ='LAND' then s.attribute13 else Null end
            ,t.land_attribute14                =case when s.attribute_category ='LAND' then s.attribute14 else Null end
            ,t.land_attribute15                =case when s.attribute_category ='LAND' then s.attribute15 else Null end;
       --
        print_log('Exit: Populate building-land details for property/floor-parcel, opn_id ='
                    ||hds_child(a1).opn_id||', location_id ='
                    ||hds_child(a1).location_id
                    ||', rowcount ='||sql%rowcount
                 );
       --
        commit;
       --
            merge into xxcus.xxcus_opn_properties_all t
            using (
                    select a.*
                    from   apps.xxcus_opn_floors_details_v a
                    where  1 =1
                      and  a.location_id        =hds_child(a1).location_id
                      and  a.active_start_date  =hds_child(a1).loc_active_start_date
                   ) s
            on (
                   t.opn_id                    =hds_child(a1).opn_id
               and t.location_id               =s.location_id         --Update buidling details for the property rows where location type = FLOOR / PARCEL
               and t.location_type_lookup_code =hds_child(a1).loc_type
               and t.loc_active_start_date     =s.active_start_date
               )
            when matched then update set
                 t.fl_pr_name                     =s.name
                ,t.fl_pr_id                       =s.ID
                ,t.fl_pr_area_gross               =Null
                ,t.fl_pr_area_rentable            =s.area_rentable
                ,t.fl_pr_area_usable              =s.area_usable
                ,t.fl_pr_area_assignable          =s.area_assignable
                ,t.fl_pr_area_sft_vacant          =s.vacant_area
                ,t.fl_pr_area_percent_vacant      =Null
                ,t.fl_pr_area_levels              =Null
                ,t.fl_pr_area_units               =Null
                ,t.fl_pr_area_load_factor         =Null
                ,t.fl_pr_occu_status_code         =s.occu_status_code
                ,t.fl_pr_occu_status              =s.occu_status
                ,t.fl_pr_occu_emp_assignable      =s.occu_emp_assignable
                ,t.fl_pr_occu_dept_assignable     =s.occu_dept_assignable
                ,t.fl_pr_occu_cust_assignable     =s.occu_cust_assignable
                ,t.fl_pr_occu_disposition_code    =s.occu_disposition_code
                ,t.fl_pr_occu_disposition         =s.occu_disposition
                ,t.fl_pr_occu_acc_treatment_code  =s.occu_acc_treatment_code
                ,t.fl_pr_occu_acc_treatment       =s.occu_acc_treatment
                ,t.fl_pr_occu_maximum             =s.occu_maximum
                ,t.fl_pr_occu_optimum             =s.occu_optimum
                ,t.fl_pr_occu_utilized            =s.occu_area_utilized
                ,t.fl_pr_occu_area_utilized       =s.occu_area_utilized
                ,t.fl_pr_occu_max_vacancy         =s.occu_max_vacancy
                ,t.fl_pr_vacant_area              =s.vacant_area
                ,t.fl_pr_common                   =s.common
                ,t.fl_pr_primary_circulation      =s.primary_circulation
                ,t.fl_pr_primary_ops_type_code    =s.primary_ops_type_code
                ,t.fl_pr_primary_ops_type         =s.primary_ops_type
                ,t.fl_pr_function_ops_type_code   =s.function_ops_type_code
                ,t.fl_pr_function_type            =s.function_type
                ,t.fl_pr_standard_type_type_code  =s.standard_type_type_code
                ,t.fl_pr_standard_type            =s.standard_type
                ,t.floor_attribute_category       =case when s.attribute_category ='FLOOR' then s.attribute_category else Null end
                ,t.floor_attribute1               =case when s.attribute_category ='FLOOR' then s.attribute1 else Null end
                ,t.floor_attribute2               =case when s.attribute_category ='FLOOR' then s.attribute2 else Null end
                ,t.floor_attribute3               =case when s.attribute_category ='FLOOR' then s.attribute3 else Null end
                ,t.floor_attribute4               =case when s.attribute_category ='FLOOR' then s.attribute4 else Null end
                ,t.floor_attribute5               =case when s.attribute_category ='FLOOR' then s.attribute5 else Null end
                ,t.floor_attribute6               =case when s.attribute_category ='FLOOR' then s.attribute6 else Null end
                ,t.floor_attribute7               =case when s.attribute_category ='FLOOR' then s.attribute7 else Null end
                ,t.floor_attribute8               =case when s.attribute_category ='FLOOR' then s.attribute8 else Null end
                ,t.floor_attribute9               =case when s.attribute_category ='FLOOR' then s.attribute9 else Null end
                ,t.floor_attribute10              =case when s.attribute_category ='FLOOR' then s.attribute1 else Null end
                ,t.floor_attribute11              =case when s.attribute_category ='FLOOR' then s.attribute11 else Null end
                ,t.floor_attribute12              =case when s.attribute_category ='FLOOR' then s.attribute12 else Null end
                ,t.floor_attribute13              =case when s.attribute_category ='FLOOR' then s.attribute13 else Null end
                ,t.floor_attribute14              =case when s.attribute_category ='FLOOR' then s.attribute14 else Null end
                ,t.floor_attribute15              =case when s.attribute_category ='FLOOR' then s.attribute15 else Null end
                ,t.parcel_attribute_category      =case when s.attribute_category ='PARCEL' then s.attribute_category else Null end
                ,t.parcel_parcel                  =case when s.attribute_category ='PARCEL' then s.attribute1 else Null end
                ,t.parcel_attribute2              =case when s.attribute_category ='PARCEL' then s.attribute2 else Null end
                ,t.parcel_attribute3              =case when s.attribute_category ='PARCEL' then s.attribute3 else Null end
                ,t.parcel_attribute4              =case when s.attribute_category ='PARCEL' then s.attribute4 else Null end
                ,t.parcel_attribute5              =case when s.attribute_category ='PARCEL' then s.attribute5 else Null end
                ,t.parcel_attribute6              =case when s.attribute_category ='PARCEL' then s.attribute6 else Null end
                ,t.parcel_attribute7              =case when s.attribute_category ='PARCEL' then s.attribute7 else Null end
                ,t.parcel_attribute8              =case when s.attribute_category ='PARCEL' then s.attribute8 else Null end
                ,t.parcel_tax_assessor            =case when s.attribute_category ='PARCEL' then s.attribute9 else Null end
                ,t.parcel_attribute10             =case when s.attribute_category ='PARCEL' then s.attribute10 else Null end
                ,t.parcel_attribute11             =case when s.attribute_category ='PARCEL' then s.attribute11 else Null end
                ,t.parcel_attribute12             =case when s.attribute_category ='PARCEL' then s.attribute12 else Null end
                ,t.parcel_attribute13             =case when s.attribute_category ='PARCEL' then s.attribute13 else Null end
                ,t.parcel_attribute14             =case when s.attribute_category ='PARCEL' then s.attribute14 else Null end
                ,t.parcel_attribute15             =case when s.attribute_category ='PARCEL' then s.attribute15 else Null end;
       --
        print_log('Exit: Populate floor details for property/floor-parcel, opn_id ='
                    ||hds_child(a1).opn_id||', location_id ='
                    ||hds_child(a1).location_id
                    ||', rowcount ='||sql%rowcount
                 );
       --
        commit;
       --
      elsif hds_child(a1).loc_type IN ('OFFICE', 'SECTION') then
       --
        begin
         select parent_location_id
         into   hds_child(a1).bldg_id --get building id for the office by connecting thru the floor_id
         from   pn_locations_all
         where  1 =1
           and  location_id        =hds_child(a1).parent_loc_id --this is floor id
           and  rownum <2;
           --and  active_start_date  =hds_child(a1).loc_active_start_date;
           --and  nvl(active_end_date, trunc(sysdate)) >=trunc(sysdate);
        exception
         when no_data_found then
          hds_child(a1).bldg_id :=0;
         when others then
          hds_child(a1).bldg_id :=0;
          print_log('@get bldg details for office_id /yard_id ='||hds_child(a1).opn_id||', msg ='||sqlerrm);
        end;
       --
            merge into xxcus.xxcus_opn_properties_all t
            using (
                    select a.*
                    from   apps.xxcus_opn_bldg_details_v a
                    where  1 =1
                      and  a.location_id =hds_child(a1).bldg_id
                      and  rownum <2
                      --and  a.active_start_date  =hds_child(a1).loc_active_start_date
                      --and  nvl(a.active_end_date, trunc(sysdate)) >=trunc(sysdate) --@floor level to get bldg info, you don't need to worry about end_dated building records
                   ) s
            on (
                    t.opn_id                    =hds_child(a1).opn_id         --@property level
                and t.location_id               =hds_child(a1).location_id    --look for an OFFICE record
                and t.parent_location_id        =hds_child(a1).parent_loc_id  --where parent is FLOOR
                and t.location_type_lookup_code =hds_child(a1).loc_type
               )
            when matched then update set
             t.bl_ld_lob_nickname              =s.name
            ,t.bl_ld_ID                        =s.ID
            ,t.bl_ld_tenancy_code              =s.tenancy_code
            ,t.bl_ld_tenancy                   =s.tenancy
            ,t.bl_ld_complexity_code           =s.complexity_code
            ,t.bl_ld_complexity                =s.complexity
            ,t.bl_ld_area_uom_code             =s.area_uom_code
            ,t.bl_ld_area_uom                  =s.area_uom
            ,t.bl_ld_area_gross                =s.area_gross
            ,t.bl_ld_area_rentable             =s.area_rentable
            ,t.bl_ld_area_usable               =s.area_usable
            ,t.bl_ld_area_assignable           =s.area_assignable
            ,t.bl_ld_area_sft_vacant           =s.area_sft_vacant
            ,t.bl_ld_area_percent_vacant       =s.area_percent_vacant
            ,t.bl_ld_area_levels               =s.area_levels
            ,t.bl_ld_area_units                =s.area_units
            ,t.bl_ld_area_load_factor          =s.area_load_factor
            ,t.bl_ld_occu_status_code          =s.occu_status_code
            ,t.bl_ld_occu_status               =s.occu_status
            ,t.bl_ld_occu_emp_assignable       =s.occu_emp_assignable
            ,t.bl_ld_occu_dept_assignable      =s.occu_dept_assignable
            ,t.bl_ld_occu_cust_assignable      =s.occu_cust_assignable
            ,t.bl_ld_occu_disposition_code     =s.occu_disposition_code
            ,t.bl_ld_occu_disposition          =s.occu_disposition
            ,t.bl_ld_occu_acc_treatment_code   =s.occu_acc_treatment_code
            ,t.bl_ld_occu_acc_treatment        =s.occu_acc_treatment
            ,t.bl_ld_occu_maximum              =s.occu_maximum
            ,t.bl_ld_occu_optimum              =s.occu_optimum
            ,t.bl_ld_occu_utilized             =s.occu_utilized
            ,t.bl_ld_occu_percent_max          =s.occu_percent_max
            ,t.bl_ld_occu_area_utilized        =s.occu_area_utilized
            ,t.bl_ld_occu_max_vacancy          =s.occu_max_vacancy
            ,t.address_id                      =s.address_id
            ,t.country_code                    =s.country
            ,t.address_country                 =s.address_country
            ,t.address_line1                   =s.address_line1
            ,t.address_line2                   =s.address_line2
            ,t.address_line3                   =s.address_line3
            ,t.city                            =s.city
            ,t.state                           =s.state
            ,t.province                        =s.province
            ,t.postal_code                     =s.postal_code
            ,t.county                          =s.county
            --assign building attributes
            ,t.bldg_attribute_category         =case when s.attribute_category ='BUILDING' then s.attribute_category else Null end
            ,t.bldg_longitude                  =case when s.attribute_category ='BUILDING' then s.attribute1 else Null end
            ,t.bldg_latitude                   =case when s.attribute_category ='BUILDING' then s.attribute2 else Null end
            ,t.bldg_attribute3                 =case when s.attribute_category ='BUILDING' then s.attribute3 else Null end
            ,t.bldg_attribute4                 =case when s.attribute_category ='BUILDING' then s.attribute4 else Null end
            ,t.bldg_attribute5                 =case when s.attribute_category ='BUILDING' then s.attribute5 else Null end
            ,t.bldg_documentum_link            =case when s.attribute_category ='BUILDING' then s.attribute6 else Null end
            ,t.bldg_photo_link                 =case when s.attribute_category ='BUILDING' then s.attribute7 else Null end
            ,t.bldg_attribute8                 =case when s.attribute_category ='BUILDING' then s.attribute8 else Null end
            ,t.bldg_attribute9                 =case when s.attribute_category ='BUILDING' then s.attribute9 else Null end
            ,t.bldg_attribute10                =case when s.attribute_category ='BUILDING' then s.attribute10 else Null end
            ,t.bldg_attribute11                =case when s.attribute_category ='BUILDING' then s.attribute11 else Null end
            ,t.bldg_attribute12                =case when s.attribute_category ='BUILDING' then s.attribute12 else Null end
            ,t.bldg_attribute13                =case when s.attribute_category ='BUILDING' then s.attribute13 else Null end
            ,t.bldg_attribute14                =case when s.attribute_category ='BUILDING' then s.attribute14 else Null end
            ,t.bldg_attribute15                =case when s.attribute_category ='BUILDING' then s.attribute15 else Null end
            ,t.land_attribute_category         =case when s.attribute_category ='LAND' then s.attribute_category else Null end
            ,t.land_longitude                  =case when s.attribute_category ='LAND' then s.attribute1 else Null end
            ,t.land_latitude                   =case when s.attribute_category ='LAND' then s.attribute2 else Null end
            ,t.land_attribute3                 =case when s.attribute_category ='LAND' then s.attribute3 else Null end
            ,t.land_attribute4                 =case when s.attribute_category ='LAND' then s.attribute4 else Null end
            ,t.land_attribute5                 =case when s.attribute_category ='LAND' then s.attribute5 else Null end
            ,t.land_attribute6                 =case when s.attribute_category ='LAND' then s.attribute6 else Null end
            ,t.land_attribute7                 =case when s.attribute_category ='LAND' then s.attribute7 else Null end
            ,t.land_insignificant              =case when s.attribute_category ='LAND' then s.attribute8 else Null end
            ,t.land_attribute9                 =case when s.attribute_category ='LAND' then s.attribute9 else Null end
            ,t.land_attribute10                =case when s.attribute_category ='LAND' then s.attribute10 else Null end
            ,t.land_attribute11                =case when s.attribute_category ='LAND' then s.attribute11 else Null end
            ,t.land_attribute12                =case when s.attribute_category ='LAND' then s.attribute12 else Null end
            ,t.land_attribute13                =case when s.attribute_category ='LAND' then s.attribute13 else Null end
            ,t.land_attribute14                =case when s.attribute_category ='LAND' then s.attribute14 else Null end
            ,t.land_attribute15                =case when s.attribute_category ='LAND' then s.attribute15 else Null end;
       --
        print_log('Exit: Populate building-land details for property/office-yard, opn_id ='
                    ||hds_child(a1).opn_id||', location_id ='
                    ||hds_child(a1).location_id
                    ||', rowcount ='||sql%rowcount
                 );
       --
        commit;
       --
            merge into xxcus.xxcus_opn_properties_all t
            using (
                    select a.*
                    from   apps.xxcus_opn_floors_details_v a
                    where  1 =1
                      and  a.location_id        =hds_child(a1).parent_loc_id
                      and  rownum <2
                      --and  a.active_end_date    =hds_child(a1).loc_active_start_date
                      --and  nvl(a.active_end_date, trunc(sysdate)) >=trunc(sysdate)
                   ) s
            on (
                   t.opn_id                    =hds_child(a1).opn_id         --@property
               and t.location_id               =hds_child(a1).location_id    --look for an OFFICE record
               and t.parent_location_id        =hds_child(a1).parent_loc_id  --where parent is FLOOR
               and t.location_type_lookup_code =hds_child(a1).loc_type
               )
            when matched then update set
                 t.fl_pr_name                     =s.name
                ,t.fl_pr_id                       =s.ID
                ,t.fl_pr_area_gross               =Null
                ,t.fl_pr_area_rentable            =s.area_rentable
                ,t.fl_pr_area_usable              =s.area_usable
                ,t.fl_pr_area_assignable          =s.area_assignable
                ,t.fl_pr_area_sft_vacant          =s.vacant_area
                ,t.fl_pr_area_percent_vacant      =Null
                ,t.fl_pr_area_levels              =Null
                ,t.fl_pr_area_units               =Null
                ,t.fl_pr_area_load_factor         =Null
                ,t.fl_pr_occu_status_code         =s.occu_status_code
                ,t.fl_pr_occu_status              =s.occu_status
                ,t.fl_pr_occu_emp_assignable      =s.occu_emp_assignable
                ,t.fl_pr_occu_dept_assignable     =s.occu_dept_assignable
                ,t.fl_pr_occu_cust_assignable     =s.occu_cust_assignable
                ,t.fl_pr_occu_disposition_code    =s.occu_disposition_code
                ,t.fl_pr_occu_disposition         =s.occu_disposition
                ,t.fl_pr_occu_acc_treatment_code  =s.occu_acc_treatment_code
                ,t.fl_pr_occu_acc_treatment       =s.occu_acc_treatment
                ,t.fl_pr_occu_maximum             =s.occu_maximum
                ,t.fl_pr_occu_optimum             =s.occu_optimum
                ,t.fl_pr_occu_utilized            =s.occu_area_utilized
                ,t.fl_pr_occu_area_utilized       =s.occu_area_utilized
                ,t.fl_pr_occu_max_vacancy         =s.occu_max_vacancy
                ,t.fl_pr_vacant_area              =s.vacant_area
                ,t.fl_pr_common                   =s.common
                ,t.fl_pr_primary_circulation      =s.primary_circulation
                ,t.fl_pr_primary_ops_type_code    =s.primary_ops_type_code
                ,t.fl_pr_primary_ops_type         =s.primary_ops_type
                ,t.fl_pr_function_ops_type_code   =s.function_ops_type_code
                ,t.fl_pr_function_type            =s.function_type
                ,t.fl_pr_standard_type_type_code  =s.standard_type_type_code
                ,t.fl_pr_standard_type            =s.standard_type
                ,t.floor_attribute_category       =case when s.attribute_category ='FLOOR' then s.attribute_category else Null end
                ,t.floor_attribute1               =case when s.attribute_category ='FLOOR' then s.attribute1 else Null end
                ,t.floor_attribute2               =case when s.attribute_category ='FLOOR' then s.attribute2 else Null end
                ,t.floor_attribute3               =case when s.attribute_category ='FLOOR' then s.attribute3 else Null end
                ,t.floor_attribute4               =case when s.attribute_category ='FLOOR' then s.attribute4 else Null end
                ,t.floor_attribute5               =case when s.attribute_category ='FLOOR' then s.attribute5 else Null end
                ,t.floor_attribute6               =case when s.attribute_category ='FLOOR' then s.attribute6 else Null end
                ,t.floor_attribute7               =case when s.attribute_category ='FLOOR' then s.attribute7 else Null end
                ,t.floor_attribute8               =case when s.attribute_category ='FLOOR' then s.attribute8 else Null end
                ,t.floor_attribute9               =case when s.attribute_category ='FLOOR' then s.attribute9 else Null end
                ,t.floor_attribute10              =case when s.attribute_category ='FLOOR' then s.attribute1 else Null end
                ,t.floor_attribute11              =case when s.attribute_category ='FLOOR' then s.attribute11 else Null end
                ,t.floor_attribute12              =case when s.attribute_category ='FLOOR' then s.attribute12 else Null end
                ,t.floor_attribute13              =case when s.attribute_category ='FLOOR' then s.attribute13 else Null end
                ,t.floor_attribute14              =case when s.attribute_category ='FLOOR' then s.attribute14 else Null end
                ,t.floor_attribute15              =case when s.attribute_category ='FLOOR' then s.attribute15 else Null end
                ,t.parcel_attribute_category      =case when s.attribute_category ='PARCEL' then s.attribute_category else Null end
                ,t.parcel_parcel                  =case when s.attribute_category ='PARCEL' then s.attribute1 else Null end
                ,t.parcel_attribute2              =case when s.attribute_category ='PARCEL' then s.attribute2 else Null end
                ,t.parcel_attribute3              =case when s.attribute_category ='PARCEL' then s.attribute3 else Null end
                ,t.parcel_attribute4              =case when s.attribute_category ='PARCEL' then s.attribute4 else Null end
                ,t.parcel_attribute5              =case when s.attribute_category ='PARCEL' then s.attribute5 else Null end
                ,t.parcel_attribute6              =case when s.attribute_category ='PARCEL' then s.attribute6 else Null end
                ,t.parcel_attribute7              =case when s.attribute_category ='PARCEL' then s.attribute7 else Null end
                ,t.parcel_attribute8              =case when s.attribute_category ='PARCEL' then s.attribute8 else Null end
                ,t.parcel_tax_assessor            =case when s.attribute_category ='PARCEL' then s.attribute9 else Null end
                ,t.parcel_attribute10             =case when s.attribute_category ='PARCEL' then s.attribute10 else Null end
                ,t.parcel_attribute11             =case when s.attribute_category ='PARCEL' then s.attribute11 else Null end
                ,t.parcel_attribute12             =case when s.attribute_category ='PARCEL' then s.attribute12 else Null end
                ,t.parcel_attribute13             =case when s.attribute_category ='PARCEL' then s.attribute13 else Null end
                ,t.parcel_attribute14             =case when s.attribute_category ='PARCEL' then s.attribute14 else Null end
                ,t.parcel_attribute15             =case when s.attribute_category ='PARCEL' then s.attribute15 else Null end;
       --
        print_log('Exit: Populate floor details for property/office-yard, opn_id ='
                    ||hds_child(a1).opn_id||', location_id ='
                    ||hds_child(a1).location_id
                    ||', rowcount ='||sql%rowcount
                 );
       --
        commit;
       --
            merge into xxcus.xxcus_opn_properties_all t
            using (
                    select a.*
                    from   apps.xxcus_opn_office_details_v a
                    where  1 =1
                      and  a.location_id        =hds_child(a1).location_id
                      and  a.active_start_date  =hds_child(a1).loc_active_start_date
                   ) s
            on (
                   t.opn_id                    =hds_child(a1).opn_id
               and t.location_id               =s.location_id         --Update office details for the property rows where location type OFFICE /YARD
               and t.location_type_lookup_code =hds_child(a1).loc_type
               and t.loc_active_start_date     =s.active_start_date
               )
            when matched then update set
                 t.sc_yd_name                     =s.name
                ,t.sc_yd_id                       =s.ID
                ,t.sc_yd_area_rentable            =s.rentable_area
                ,t.sc_yd_area_usable              =s.usable_area
                ,t.sc_yd_area_assignable          =s.assignable_area
                ,t.sc_yd_area_sft_vacant          =s.vacant_area
                ,t.sc_yd_occu_status_code         =s.occu_status_code
                ,t.sc_yd_occu_status              =s.occu_status
                ,t.sc_yd_occu_emp_assignable      =s.occu_emp_assignable
                ,t.sc_yd_occu_dept_assignable     =s.occu_dept_assignable
                ,t.sc_yd_occu_cust_assignable     =s.occu_cust_assignable
                ,t.sc_yd_occu_disposition_code    =s.occu_disposition_code
                ,t.sc_yd_occu_disposition         =s.occu_disposition
                ,t.sc_yd_occu_acc_treatment_code  =s.occu_acc_treatment_code
                ,t.sc_yd_occu_acc_treatment       =s.occu_acc_treatment
                ,t.sc_yd_occu_maximum             =s.occu_maximum
                ,t.sc_yd_occu_optimum             =s.occu_optimum
                ,t.sc_yd_occu_utilized            =s.occu_area_utilized
                ,t.sc_yd_occu_area_utilized       =s.occu_area_utilized
                ,t.sc_yd_occu_max_vacancy         =s.occu_max_vacancy
                ,t.sc_yd_vacant_area              =s.vacant_area
                ,t.sc_yd_common                   =s.common
                ,t.sc_yd_secondary_circulation    =s.secondary_circulation
                ,t.sc_yd_space_type_code          =s.space_type_code
                ,t.sc_yd_space_type               =s.space_type
                ,t.sc_yd_site_type_code           =s.site_type_code
                ,t.sc_yd_site_type                =s.site_type
                ,t.sc_yd_assignment_type_code     =s.assignment_type_code
                ,t.sc_yd_assignment_type          =s.assignment_type
                ,t.office_attribute_category      =case when s.attribute_category ='OFFICE' then s.attribute_category else Null end
                ,t.office_attribute1              =case when s.attribute_category ='OFFICE' then s.attribute1 else Null end
                ,t.office_attribute2              =case when s.attribute_category ='OFFICE' then s.attribute2 else Null end
                ,t.office_operating_hours         =case when s.attribute_category ='OFFICE' then s.attribute3 else Null end
                ,t.office_walkin                  =case when s.attribute_category ='OFFICE' then s.attribute4 else Null end
                ,t.office_customer_site           =case when s.attribute_category ='OFFICE' then s.attribute5 else Null end
                ,t.office_attribute6              =case when s.attribute_category ='OFFICE' then s.attribute6 else Null end
                ,t.office_attribute7              =case when s.attribute_category ='OFFICE' then s.attribute7 else Null end
                ,t.office_attribute8              =case when s.attribute_category ='OFFICE' then s.attribute8 else Null end
                ,t.office_attribute9              =case when s.attribute_category ='OFFICE' then s.attribute9 else Null end
                ,t.office_attribute10             =case when s.attribute_category ='OFFICE' then s.attribute10 else Null end
                ,t.office_attribute11             =case when s.attribute_category ='OFFICE' then s.attribute11 else Null end
                ,t.office_attribute12             =case when s.attribute_category ='OFFICE' then s.attribute12 else Null end
                ,t.office_attribute13             =case when s.attribute_category ='OFFICE' then s.attribute13 else Null end
                ,t.office_attribute14             =case when s.attribute_category ='OFFICE' then s.attribute14 else Null end
                ,t.office_attribute15             =case when s.attribute_category ='OFFICE' then s.attribute15 else Null end
                ,t.section_attribute_category     =case when s.attribute_category ='SECTION' then s.attribute_category else Null end
                ,t.section_attribute1             =case when s.attribute_category ='SECTION' then s.attribute1 else Null end
                ,t.section_attribute2             =case when s.attribute_category ='SECTION' then s.attribute2 else Null end
                ,t.section_operating_hours        =case when s.attribute_category ='SECTION' then s.attribute3 else Null end
                ,t.section_walkin                 =case when s.attribute_category ='SECTION' then s.attribute4 else Null end
                ,t.section_customer_site          =case when s.attribute_category ='SECTION' then s.attribute5 else Null end
                ,t.section_attribute6             =case when s.attribute_category ='SECTION' then s.attribute6 else Null end
                ,t.section_attribute7             =case when s.attribute_category ='SECTION' then s.attribute7 else Null end
                ,t.section_attribute8             =case when s.attribute_category ='SECTION' then s.attribute8 else Null end
                ,t.section_attribute9             =case when s.attribute_category ='SECTION' then s.attribute9 else Null end
                ,t.section_attribute10            =case when s.attribute_category ='SECTION' then s.attribute10 else Null end
                ,t.section_attribute11            =case when s.attribute_category ='SECTION' then s.attribute11 else Null end
                ,t.section_attribute12            =case when s.attribute_category ='SECTION' then s.attribute12 else Null end
                ,t.section_attribute13            =case when s.attribute_category ='SECTION' then s.attribute13 else Null end
                ,t.section_attribute14            =case when s.attribute_category ='SECTION' then s.attribute14 else Null end
                ,t.section_attribute15            =case when s.attribute_category ='SECTION' then s.attribute15 else Null end;
       --
        print_log('Exit: Populate office details for property/office-yard, opn_id ='
                    ||hds_child(a1).opn_id||', location_id ='
                    ||hds_child(a1).location_id
                    ||', rowcount ='||sql%rowcount
                 );
       --
        commit;
       --
      else
       Null;
      end if;
      --
      commit;
      --
      --print_log('Merged attributes for PROPERTY rows, location_id =>'||hds_child(a1).opn_id);
      --
     exception
      when others then
       print_log('Failed to merge attributes for PROPERTY rows, location_id =>'||hds_child(a1).opn_id||', msg ='||sqlerrm);
     end;
    --
    end loop;
    --
   end if;
   --
    b_proceed :=TRUE;
   --
  exception
   when others then
    --
     print_log('Issue in cursor c_get_nodes'||sqlerrm);
     b_proceed :=FALSE;
    --
  end;
  --
 exception
  when others then
   print_log('@ routine update_property_attributes, msg ='||sqlerrm);
 end update_property_attributes;
 --
/*
 ************************************************************************************************
 *
 * PROCEDURE: lease_tenancies --Ver 3.3
 * DESCRIPTION: To populate lease tenancies table
 * PARAMETERS : None
 * ==========
 * History
 * =======================================================================
 * ESMS                                    Date               Version   Notes
 * ----------                               -----------         -------      ---------------------------------------
 * 253936                               10/13/2014    1.0         Created.
 * TMS 20150909-00140   09/11/2015    3.2        Remove fields from XXCUS_OPN_RER_LOCATIONS_ALL, add some new fields, change mapping logic [ESMS 301196]
 * TMS 20151014-00024   10/21/2015   3.3        Edit logic for OPN_PRIMARY_LOC_FOR_RER field [ESMS 304543]
* TMS 20160302-00017  03/04/2016   3.6        Add extract date field. [ ESMS 318695 ]
 * =======================================================================
 * --
 * CALLED BY: HDS OPN: Extract Locations [Concurrent Program]. In other words the main routine
 ************************************************************************************************
 */
  procedure lease_tenancies as
  --
  l_module        VARCHAR2(24) :='OPN';
  l_err_msg       CLOB;
  l_sec           VARCHAR2(255);
  --
  l_err_callfrom  VARCHAR2(75) DEFAULT 'apps.xxcus_opn_master_extract.lease_tenancies';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  --
  v_building pn_locations_all.building%type :=Null;
  v_floor    pn_locations_all.floor%type :=Null;
  v_office   pn_locations_all.office%type :=Null;
  --
  -- Begin Ver 3.5
  cursor inactive_loctbl_info is
      select locdir.fru             fru
            ,locdir.fru_descr       fru_descr
            ,locdir.lob_branch      lob_branch
            ,locdir.entrp_loc              entrp_loc
      from  xxcus.xxcus_location_code_tbl  locdir
               ,(
                    select oracle_id
                    from  xxcus.xxcus_opn_rer_locations_all
                    where 1 =1
                    and (oracle_id is not null and (fru is null or lob_branch is null))
                    group by oracle_id
                ) locdir_b
      where  1 =1
         and  locdir.inactive  ='Y'
         and  locdir_b.oracle_id =locdir.entrp_loc
    group by
             locdir.fru
            ,locdir.fru_descr
            ,locdir.lob_branch
            ,locdir.entrp_loc
    having count(entrp_loc) =1;
  -- End Ver 3.5
  --
  cursor orig_loctbl_info is
  select locdir.fru             fru
        ,locdir.fru_descr       fru_descr
        ,locdir.lob_branch      lob_branch
        ,entrp_loc              entrp_loc
  from  xxcus.xxcus_location_code_tbl  locdir
  where  1 =1
     and  locdir.inactive(+)  ='N';
  --
  -- Begin Ver 3.3
  --
   type le_rec_t is record
    (
       le_rowid varchar2(240)
      ,le_id        number
    );
   type le_rowid_tbl is table of le_rec_t index by binary_integer;
   le_rec le_rowid_tbl;
   --
   n_count  number :=0;
   n_tie_brkr_count1 number :=0;
   n_tie_brkr_count2 number :=0;
   --
    cursor le_rec3 is
    select c.rowid, c.lease_id
    from   xxcus.xxcus_opn_rer_locations_all  c
    where 1 =1
    and exists
    (
    select a.lease_id --use this to fix remaining rows with Z flag...
    from xxcus.xxcus_opn_rer_locations_all  a
    where 1 =1
    and a.hds_opn_primary_loc_for_rer ='Z'
    and a.lease_id =c.lease_id
    and not exists
    (
    select 1
    from xxcus.xxcus_opn_rer_locations_all b
    where 1 =1
    and b.lease_id =a.lease_id
    and b.hds_opn_primary_loc_for_rer ='Y'
    )
    group by a.lease_id
    having count(a.lease_id) =1 --pull only when we've exactly one row per lease with a flag of Z
    );
   --
    cursor le_rec4 is
    select a.rowid, a.lease_id  --use this to fix remaining rows with Z flag...
    from xxcus.xxcus_opn_rer_locations_all  a
    where 1 =1
    and a.pri_loc_code like substr(a.loc_code, 1, instr(a.loc_code, '.')-1)
    and a.hds_opn_primary_loc_for_rer ='Z'
    and a.hds_ten_loc_rentable_area_sf
                                        =(
                                             select max(d.hds_ten_loc_rentable_area_sf)
                                             from    xxcus.xxcus_opn_rer_locations_all  d
                                             where 1 =1
                                                  and d.pri_loc_code like substr(d.loc_code, 1, instr(d.loc_code, '.')-1)
                                                  and d.hds_opn_primary_loc_for_rer ='Z'
                                                  and d.lease_id                                           =a.lease_id
                                          )
    and 1 = --this will ensure we pick a lease record that has one and only max rentable square footage...
                  (
                        select count(1)
                        from   xxcus.xxcus_opn_rer_locations_all  b
                        where 1 =1
                              and b.hds_opn_primary_loc_for_rer ='Z'
                              and b.lease_id =a.lease_id
                              and b.pri_loc_code like substr(b.loc_code, 1, instr(b.loc_code, '.')-1)
                              and b.hds_ten_loc_rentable_area_sf
                                        =(
                                             select max(c.hds_ten_loc_rentable_area_sf)
                                             from    xxcus.xxcus_opn_rer_locations_all  c
                                            where 1 =1
                                                  and c.hds_opn_primary_loc_for_rer ='Z'
                                                  and c.lease_id =b.lease_id
                                                  and c.pri_loc_code like substr(c.loc_code, 1, instr(c.loc_code, '.')-1)
                                          )
                  );
  --
    cursor all_tenancies is
    select  lease_id
                ,sum  (
                  case
                    when loc_type_lookup_code in ('BUILDING', 'FLOOR', 'OFFICE') then hds_ten_loc_rentable_area_sf else 0
                  end
                 )  total_bldg_sf
                ,sum (
                  case
                   when loc_type_lookup_code in ('LAND', 'PARCEL', 'SECTION') then hds_ten_loc_rentable_area_sf else 0
                  end
                 ) total_land_sf
    from  xxcus.xxcus_opn_rer_locations_all
    where 1 =1
    group by lease_id;
  --
  -- End Ver 3.3
  v_entrp_loc xxcus.xxcus_location_code_tbl.entrp_loc%type :=Null;
  --
begin
  --
  Execute Immediate 'TRUNCATE TABLE xxcus.xxcus_opn_rer_locations_all'; --xxcus.xxcus_opn_lease_tenancies_all';
  --
  begin
   --
    insert into xxcus.xxcus_opn_rer_locations_all --xxcus.xxcus_opn_lease_tenancies_all
    (
    select lea.lease_number               rer_id
          ,lea.lease_name                 rer_name
          ,ple.lease_number               master_rer_id
          ,ple.lease_name                 master_rer_name
          /* -- Begin Ver 3.3
          ,case
             when ten.location_id  =lea.location_id then 'Y'
             else 'N'
           end                                         opn_primary_loc_for_rer
          */ -- End Ver 3.3
          ,'Z' hds_opn_primary_loc_for_rer -- Ver 3.3
          ,lea.lease_type                 lease_type
          ,lea.lease_class                lease_class
          ,lea.user_abstracted            abstracted_by
          ,Null                           approval_status
          ,Null                           lease_status
          ,lea.calendar_year_start_date   scheduled_year_start_date
          ,loc1.location_code             pri_loc_code
          ,(
             select building||floor||office loc_name
             from   pn_locations_all a
             where  1 =1
               and  a.location_id =loc1.location_id
               and  a.active_start_date =
                (
                 select max(active_start_date)
                 from   pn_locations_all
                 where  1 =1
                   and  location_id =a.location_id
                )
           )                              pri_loc_name
          ,lea.lease_execution_date       lease_execution
          ,lea.lease_commencement_date    lease_commencement
          ,lea.lease_termination_date     lease_termination
          ,lea.lease_extension_end_date   lease_extn_end_date
          ,Null                           gl_expense_product
          ,Null                           gl_expense
          ,Null                           gl_liability
          ,Null                           gl_accrued_liability
          --,Null                           bu           --Ver 3.2
          --,Null                           bu_description --Ver 3.2
          ,Null                           lob_abb --Ver 3.2
          ,Null                           lob_dba   --Ver 3.2
          ,Null                           lob_branch
          ,Null                           sublob_abb  --Ver 3.2
          ,Null                           sublob_dba  --Ver 3.2
          ,Null                           primary_sub_bu_rank  --Ver 3.2
          ,Null                           oracle_id
          ,Null                           fru
          ,Null                           fru_description
          ,Null                           rem_ntid
          ,Null                           rem_name
          ,loc.location_type_lookup_code  loc_type_lookup_code
          ,(
             select meaning
             from   fnd_lookup_values_vl
             where  1 =1
               and  lookup_type ='PN_LOCATION_TYPE'
               and  lookup_code =loc.location_type_lookup_code
           )                              loc_type
          ,loc.location_code              loc_code
          -- Begin Ver 3.2
          ,case
              when loc.location_type_lookup_code IN ('OFFICE', 'SECTION') then loc.suite
              else Null
           end                                        section_suite
          ,Null                                        opn_tenancy_suite
          --End Ver 3.2
          ,ten.primary_flag         tenancy_pri_flag
          ,ten.tenancy_usage_type         tenancy_usage
         -- ,ten.estimated_occupancy_date   est_occu_date --Ver 3.2
          --,ten.occupancy_date             actual_occu_date --Ver 3.2
          --,ten.expiration_date            exp_date --Ver 3.2
          --,ten.fin_oblig_end_date         fin_oblig_end_date --Ver 3.2
          --,ten.recovery_type              recovery_type --Ver 3.2
          --,ten.recovery_space_std         recovery_space_std --Ver 3.2
          --,ten.customer_number            customer_number --Ver 3.2
          --,ten.customer_name              customer_name --Ver 3.2
          --,ten.customer_site_use          bill_to_site --Ver 3.2
          ,Null                           address_line1
          ,Null                           address_line2
          ,Null                           address_line3
          ,Null                           city
          ,Null                           postal_code
          ,Null                           county
          ,Null                           state_province
          ,Null                           country_code
          ,Null                           country
          --,Null                           region --Ver 3.2
          --,Null                           office_park --Ver 3.2
          --,lea.property_name_disp         property_name --Ver 3.2
          --,Null                           bldg_land_name --Ver 3.2
          --,Null                           floor_parcel_name --Ver 3.2
          --,Null                           section_yard_name --Ver 3.2
          ,lea.attribute1                 legacy_id
          ,lea.attribute2                 mortgaged
          ,lea.attribute3                 related_party
          --,lea.attribute4                 related_party_name --Ver 3.2
          ,Null                                   related_party_name   --Ver 3.2
          ,lea.attribute5                 related_party_comments
          ,lea.attribute6                 sale_leaseback
          ,lea.attribute7                 guarantor
          ,lea.attribute8                 rer_doc_link
          ,lea.attribute9                 capital_lease
          ,lea.attribute10                rent_type
          -- begin fields for technical purpose
          ,lea.lease_status               lease_status_code
          ,lea.status                     approval_status_code
          ,lea.lease_type_code            lease_type_code
          ,lea.lease_class_code           lease_class_code
          ,ten.recovery_space_std_code    recovery_space_std_code
          ,ten.recovery_type_code         recovery_type_code
          ,ten.tenancy_usage_lookup_code  tenancy_usage_code
          --Begin Ver 3.3
          ,0 hds_total_bldg_sf_for_rer
          ,0 hds_total_land_sf_for_rer
          ,nvl
           (
              case
                when loc.location_type_lookup_code IN ('BUILDING', 'LAND')
                           then
                             (
                                 select nvl(ten_loc.gross_area, 0)
                                 from pn_tenancies_all ten1, pn_locations_all ten_loc
                                 where     1 = 1
                                      and ten1.tenancy_id =ten.tenancy_id
                                      and ten_loc.location_id(+) = ten1.location_id
                                      and ten_loc.active_start_date =loc.loc_active_start_date
                                      and ten_loc.active_end_date   =loc.loc_active_end_date
                             )
                when loc.location_type_lookup_code IN ('FLOOR',  'PARCEL', 'OFFICE', 'SECTION')
                             then
                            (
                                 select nvl(ten_loc.rentable_area, 0)
                                 from pn_tenancies_all ten1, pn_locations_all ten_loc
                                 where     1 = 1
                                      and ten1.tenancy_id =ten.tenancy_id
                                      and ten_loc.location_id(+) = ten1.location_id
                                      and ten_loc.active_start_date =loc.loc_active_start_date
                                      and ten_loc.active_end_date   =loc.loc_active_end_date
                            )
                else 0 --everything else
               end
            ,0
            ) hds_ten_loc_rentable_area_sf
         ,case
                     -- For all Buildings we transform the capital letters [if there is no value then use constant capital A] to their ascii and subtract 65 to get the seq.
                     -- For all floor, parcel and land, use the last character
                     -- For all offices or yard, use the last two numbers separated by a period.
                      when loc.location_type_lookup_code ='OFFICE' then
                       TO_NUMBER (NVL ( regexp_substr(loc.location_code , '(*)[0-9]\.+[0-9]$') , '1.1'))  --Example FL300A.1.1 --extract 1.1 here
                      when loc.location_type_lookup_code ='SECTION' then
                       TO_NUMBER (NVL ( regexp_substr(loc.location_code , '(*)[0-9]\.+[0-9]$') , '1.1')+30)  --Example FL300A.1.1 --extract 1.1 here
                      when loc.location_type_lookup_code ='FLOOR' then
                       TO_NUMBER (NVL ( regexp_substr(loc.location_code , '[0-9]{1}$') , '1'))  --Example FL300A.1 or FL300L1.1 --extract 1 here
                      when loc.location_type_lookup_code ='PARCEL' then
                       TO_NUMBER (NVL ( regexp_substr(loc.location_code , '[0-9]{1}$') , '1'))  --Example FL300A.1 or FL300L1.1 --extract 1 here
                      when loc.location_type_lookup_code in ('LAND') then
                       TO_NUMBER (NVL( regexp_substr(loc.location_code , '[0-9]{1}$') , '1')+30)  --Example FL300AL1 --extract 1.1 here
                      when loc.location_type_lookup_code in ('BUILDING') then
                       TO_NUMBER(TO_CHAR( ASCII(NVL( regexp_substr(loc.location_code , '[A-Z]{1}$') , 'A')) -65 ))   --Example FL300A --get ascii of A and subtract 65 to get the sequence number
                      else 0
          end hds_loc_tie_breaker_seq
            --End Ver 3.3
          ,ten.tenancy_id
          ,ten.location_id                ten_locn_id
          ,lea.location_id                lea_locn_id
          ,ten.lease_id
          ,ten.lease_change_id
          ,ten.customer_id
          ,ten.customer_site_use_id
          ,lea.abstracted_by_user_id
          ,ten.org_id
          ,loc.address_id
          ,lea.expense_account_id    lea_expense_ccid
          ,lea.receivable_account_id lea_liability_ccid
          ,lea.accrual_account_id    lea_accrual_ccid
          ,lea.responsible_user_id   rem_user_id
          ,loc.loc_active_start_date loc_active_start_date --Ver 3.3
          ,loc.loc_active_end_date   loc_active_end_date --Ver 3.3
          ,sysdate --extract_date --Ver 3.6
          -- end fields for technical purpose
    from
           --Begin Ver 3.2
          (
                   SELECT
                          ten.tenancy_id tenancy_id,
                          ten.last_update_date last_update_date,
                          ten.last_updated_by last_updated_by,
                          ten.creation_date creation_date,
                          ten.created_by created_by,
                          ten.last_update_login last_update_login,
                          ten.location_id location_id,
                          ten.lease_id lease_id,
                          ten.lease_change_id lease_change_id,
                          ten.tenancy_usage_lookup_code tenancy_usage_lookup_code,
                          tenlook.meaning tenancy_usage_type,
                          ten.primary_flag primary_flag,
                          ten.estimated_occupancy_date estimated_occupancy_date,
                          ten.occupancy_date occupancy_date,
                          ten.expiration_date expiration_date,
                          ten.assignable_flag assignable_flag,
                          ten.subleaseable_flag subleaseable_flag,
                          ten.tenants_proportionate_share tenants_proportionate_share,
                          ten.status status,
                          ten.recovery_type_code recovery_type_code,
                          rectype.meaning recovery_type,
                          ten.recovery_space_std_code recovery_space_std_code,
                          recsstd.meaning recovery_space_std,
                          ten.fin_oblig_end_date fin_oblig_end_date,
                          ten.customer_id customer_id,
                          ten.customer_site_use_id customer_site_use_id,
                          ten.lease_rentable_area lease_rentable_area,
                          ten.lease_usable_area lease_usable_area,
                          ten.lease_assignable_area lease_assignable_area,
                          ten.lease_load_factor lease_load_factor,
                          ten.location_rentable_area location_rentable_area,
                          ten.location_usable_area location_usable_area,
                          ten.location_assignable_area location_assignable_area,
                          ten.location_load_factor location_load_factor,
                          ten.attribute_category attribute_category,
                          ten.attribute1 attribute1,
                          ten.attribute2 attribute2,
                          ten.attribute3 attribute3,
                          ten.attribute4 attribute4,
                          ten.attribute5 attribute5,
                          ten.attribute6 attribute6,
                          ten.attribute7 attribute7,
                          ten.attribute8 attribute8,
                          ten.attribute9 attribute9,
                          ten.attribute10 attribute10,
                          ten.attribute11 attribute11,
                          ten.attribute12 attribute12,
                          ten.attribute13 attribute13,
                          ten.attribute14 attribute14,
                          ten.attribute15 attribute15,
                          apps. pn_tenancies_pkg.Auto_Allocated_Area (ten.tenancy_id) allocated_area,
                          apps.pn_tenancies_pkg.Auto_Allocated_Area_Pct (ten.tenancy_id)   allocated_area_pct,
                          ten.org_id org_id,
                          (SELECT meaning
                             FROM apps.fnd_lookups
                            WHERE     lookup_type = 'PN_SPACE_TYPE'
                                  AND enabled_flag = 'Y'
                                  AND TRUNC (SYSDATE) BETWEEN TRUNC (
                                                                 NVL (start_date_active,
                                                                      SYSDATE))
                                                          AND TRUNC (
                                                                 NVL (end_date_active,
                                                                      SYSDATE))
                                  AND lookup_code =
                                         (SELECT SPACE_TYPE_LOOKUP_CODE
                                            FROM apps.pn_locations_all
                                           WHERE location_id = ten.location_id AND ROWNUM < 2))
                             space_type
                     FROM
                          apps.pn_tenancies_all ten
                          ,apps.fnd_lookups recsstd
                          ,apps.fnd_lookups rectype
                          ,apps.fnd_lookups tenlook
                    WHERE 1 =1
                          AND tenlook.lookup_type = 'PN_TENANCY_USAGE_TYPE'
                          AND tenlook.lookup_code = ten.tenancy_usage_lookup_code
                          AND rectype.lookup_type(+) = 'PN_RECOVERY_TYPE'
                          AND rectype.lookup_code(+) = ten.recovery_type_code
                          AND recsstd.lookup_type(+) = 'PN_RECOVERY_SPACE_STD_TYPE'
                          AND recsstd.lookup_code(+) = ten.recovery_space_std_code
          ) ten
           --pn_tenancies_v ten
           --End Ver 3.2
          ,pn_leases_v    lea
          ,pn_leases_v    ple
          /*  -- Begin Ver 3.3
          ,(
            select location_id
                  ,location_type_lookup_code
                  ,location_code
                  ,address_id
                  ,suite --Ver 3.2
            from   pn_locations_all
            group by location_id, location_type_lookup_code, location_code, address_id
            ,suite --Ver 3.2
           ) loc
           */ -- End Ver 3.3
          -- Begin Ver 3.3
          ,(
                select a.location_id, a.location_code, a.address_id, a.loc_active_start_date, a.loc_active_end_date, a.location_type_lookup_code
                           ,(
                            select suite
                            from  pn_locations_all
                            where 1 =1
                                  and location_id =a.location_id
                                  and active_start_date =a.loc_active_start_date
                                  and active_end_date  =a.loc_active_end_date
                            ) suite
                from xxcus.xxcus_opn_properties_all a
                where 1 =1
                and opn_type = 'PROPERTY'
                and hds_current_flag ='Y'
                group by a.location_id, a.location_code, a.address_id, a.loc_active_start_date, a.loc_active_end_date, a.location_type_lookup_code
           ) loc
          -- End Ver 3.3
          ,(
            /* --Ver 3.3
            select location_id
                  ,location_type_lookup_code
                  ,location_code
                  --,building||floor||office loc_name
            from   pn_locations_all
            group by location_id
                    ,location_type_lookup_code
                    ,location_code
                    --,building||floor||office
             */ --Ver 3.3
          -- Begin Ver 3.3
                select a.location_id, a.location_code, a.location_type_lookup_code
                from xxcus.xxcus_opn_properties_all a
                where 1 =1
                and opn_type = 'PROPERTY'
                and hds_current_flag ='Y'
                group by a.location_id, a.location_code, a.location_type_lookup_code
          -- End Ver 3.3
           ) loc1
    where  1 =1
      --and  ten.lease_id        =1358
      and  lea.lease_id        =ten.lease_id
      and  ple.lease_id(+)     =lea.parent_lease_id
      and  loc.location_id(+)  =ten.location_id
      and  loc1.location_id(+) =lea.location_id
    );
   --
   print_log('@main insert of lease_tenancies, total rows inserted : '||sql%rowcount); --Ver 3.3
   --
   commit;
     --
     -- update gl_expense, gl_expense_product and oracle_id
     --
      begin
       --
            savepoint init_here;
       --
            merge into xxcus.xxcus_opn_rer_locations_all t --xxcus.xxcus_opn_lease_tenancies_all t
            using (
                    select code_combination_id, concatenated_segments expense_cc, segment1 exp_product, segment2 exp_locn
                    from   gl_code_combinations_kfv
                    where  1 =1
                   ) s
            on (
                    t.lea_expense_ccid =s.code_combination_id
               )
            when matched then update set
             t.gl_expense           =s.expense_cc
            ,t.gl_expense_product   =s.exp_product
            ,t.oracle_id            =s.exp_locn;
        --
        commit;
        --
      exception
       when others then
        rollback to init_here;
        print_log('@lease_tenancies, Error in update of expense account info, msg ='||sqlerrm);
      end;
     --
     -- update rem_ntid and rem_name
     --
      begin
       --
            savepoint init_here;
       --
            merge into xxcus.xxcus_opn_rer_locations_all t --xxcus.xxcus_opn_lease_tenancies_all t
            using (
                    select user_id, user_name, description
                    from   fnd_user
                    where  1 =1
                   ) s
            on (
                    t.rem_user_id =s.user_id
               )
            when matched then update set
              t.rem_ntid  =s.user_name
             ,t.rem_name  =s.description;
        --
        commit;
        --
      exception
       when others then
        rollback to init_here;
        print_log('@lease_tenancies, Error in update of REM_NAME and REM_NTID, msg ='||sqlerrm);
      end;
     --
     -- update lob_abb and lob_dba info --Ver 3.2
     --
      begin
       --
            savepoint init_here;
       --
            merge into xxcus.xxcus_opn_rer_locations_all t --xxcus.xxcus_opn_lease_tenancies_all t
            using (
                    -- Begin Ver 3.2
                    --select flex_value, attribute3 bu_code, attribute4 bu_desc
                    --from   fnd_flex_values_vl
                    --where  1 =1
                      --and  flex_value_set_id =1014547   --Value set XXCUS_GL_PRODUCT which is part of Key Accounting Flexfield
                    select fps_id        pnld_fps_id
                               ,lob_abb    pnld_lob_abb
                               ,lob_dba    pnld_lob_dba
                    from  hdsoracle.xxhsi_pnld_lobrollup@apxprd_lnk.hsi.hughessupply.com
                    where 1 =1
                    -- End Ver 3.2
                   ) s
            on (
                    --t.gl_expense_product =s.flex_value --Ver 3.2
                    t.gl_expense_product =s.pnld_fps_id --Ver 3.2
               )
            when matched then update set
            --Begin Ver 3.2
              t.lob_abb =s.pnld_lob_abb
             ,t.lob_dba =s.pnld_lob_dba;
             -- t.bu             =s.bu_code
             --,t.bu_description =s.bu_desc;
            --End Ver 3.2
        --
        commit;
        --
      exception
       when others then
        rollback to init_here;
        --print_log('@lease_tenancies, Error in update of BU and BU_DESCRIPTION, msg ='||sqlerrm);
        print_log('@lease_tenancies, Error in update of LOB_ABB and LOB_DBA, msg ='||sqlerrm);
      end;
     --
     -- Begin Ver 3.2
     --
     -- update sublob_abb and sublob_dba and sub_bu_rank info
     --
      begin
       --
            savepoint init_here;
       --
            merge into xxcus.xxcus_opn_rer_locations_all t
            using (
                    select fps_id                                pnld_fps_id
                               ,sublob_abb                     pnld_sublob_abb
                               ,sublob_dba                     pnld_sublob_dba
                               ,primary_sub_bu_rank pnld_sub_bu_rank
                    from  hdsoracle.xxhsi_pnld_lobrollup@apxprd_lnk.hsi.hughessupply.com
                    where 1 =1
                   ) s
            on (
                    t.gl_expense_product =s.pnld_fps_id
               )
            when matched then update set
              t.sublob_abb =s.pnld_sublob_abb
             ,t.sublob_dba =s.pnld_sublob_dba
             ,t.sub_bu_rank =s.pnld_sub_bu_rank;
        --
        commit;
        --
      exception
       when others then
        rollback to init_here;
        print_log('@lease_tenancies, Error in update of SUBLOB_ABB, SUBLOB_DBA and SUB_BU_RANK, msg ='||sqlerrm);
      end;
     --
     -- End Ver 3.2
     --
     -- update fru, fru_description and lob branch
     --
      begin
       --
        for s in orig_loctbl_info loop
          --
          v_entrp_loc :=s.entrp_loc;
          --
          begin
          --
          savepoint init_here;
          --
          /*
            merge into xxcus.xxcus_opn_lease_tenancies_all t
            using (
                    select locdir.fru             fru
                          ,locdir.fru_descr       fru_descr
                          ,locdir.lob_branch      lob_branch
                          ,entrp_loc              entrp_loc
                    from  xxcus.xxcus_location_code_tbl  locdir
                    where  1 =1
                      and  locdir.inactive(+)  ='N'
                   ) s
            on (
                    t.oracle_id =s.entrp_loc
               )
            when matched then update set
              t.fru             =s.fru
             ,t.fru_description =s.fru_descr
             ,t.lob_branch      =s.lob_branch;
         */
          --
            update xxcus.xxcus_opn_rer_locations_all t --xxcus.xxcus_opn_lease_tenancies_all t
            set t.fru             =s.fru
               ,t.fru_description =s.fru_descr
               ,t.lob_branch      =s.lob_branch
            where 1 =1
              and t.oracle_id =s.entrp_loc;
          --
          exception
           --
           when no_data_found then
            Null;
           when too_many_rows then
            rollback to init_here;
           when others then
            rollback to init_here;
            print_log('@lease_tenancies, Error in update of FRU and LOB_BRANCH, v_entrp_loc ='||v_entrp_loc||', msg ='||sqlerrm);
           --
          end;
        --
        end loop;
       --
        commit;
        --
      exception
       when others then
        rollback;
        print_log('@Outer block of lease_tenancies, v_entrp_loc ='||v_entrp_loc||', msg ='||sqlerrm);
      end;
     --
     -- Begin Ver 3.5
     -- update fru, fru_description and lob branch for all inactive unique locations assigned in enterprise translator
     --
      begin
       --
        for s in inactive_loctbl_info loop
          --
          v_entrp_loc :=s.entrp_loc;
          --
          begin
          --
          savepoint init_here;
          --
            update xxcus.xxcus_opn_rer_locations_all t
            set t.fru             =s.fru
               ,t.fru_description =s.fru_descr
               ,t.lob_branch      =s.lob_branch
            where 1 =1
              and t.oracle_id =s.entrp_loc;
          --
          exception
           --
           when no_data_found then
            Null;
           when too_many_rows then
            rollback to init_here;
           when others then
            rollback to init_here;
            print_log('@lease_tenancies: Inactive cost centers from enterprise translator, error in update of FRU and LOB_BRANCH, v_entrp_loc ='||v_entrp_loc||', msg ='||sqlerrm);
           --
          end;
        --
        end loop;
       --
        commit;
        --
      exception
       when others then
        rollback;
        print_log('@Outer block of lease_tenancies, v_entrp_loc ='||v_entrp_loc||', msg ='||sqlerrm);
      end;
     --
     -- End Ver 3.5
     --
     -- update approval_status
     --
      begin
       --
            savepoint init_here;
       --
            merge into xxcus.xxcus_opn_rer_locations_all t --xxcus.xxcus_opn_lease_tenancies_all t
            using (
                    select lookup_code lea_approval_status_code
                          ,meaning     lea_approval_status_desc
                    from   fnd_lookup_values_vl
                    where 1 =1
                      and lookup_type ='PN_LEASE_STATUS_TYPE'
                   ) s
            on (
                    t.approval_status_code =s.lea_approval_status_code
               )
            when matched then update set
             t.approval_status =s.lea_approval_status_desc;
        --
        commit;
        --
      exception
       when others then
        rollback to init_here;
        print_log('@lease_tenancies, Error in update of approval_status, msg ='||sqlerrm);
      end;
     --
     -- update lease_status
     --
      begin
       --
            savepoint init_here;
       --
            merge into xxcus.xxcus_opn_rer_locations_all t --xxcus.xxcus_opn_lease_tenancies_all t
            using (
                    select lookup_code lea_status_code
                          ,meaning     lea_status_desc
                    from   fnd_lookup_values_vl
                    where 1 =1
                      and lookup_type ='PN_LEASESTATUS_TYPE'
                   ) s
            on (
                    t.lease_status_code =s.lea_status_code
               )
            when matched then update set
             t.lease_status =s.lea_status_desc;
        --
        commit;
        --
      exception
       when others then
        rollback to init_here;
        print_log('@lease_tenancies, Error in update of lease_status, msg ='||sqlerrm);
      end;
     --
     -- update gl_liability
     --
      begin
       --
            savepoint init_here;
       --
            merge into xxcus.xxcus_opn_rer_locations_all t --xxcus.xxcus_opn_lease_tenancies_all t
            using (
                    select code_combination_id, concatenated_segments liability_cc
                    from   gl_code_combinations_kfv
                    where  1 =1
                   ) s
            on (
                    t.lea_liability_ccid =s.code_combination_id
               )
            when matched then update set
             t.gl_liability =s.liability_cc;
        --
        commit;
        --
      exception
       when others then
        rollback to init_here;
        print_log('@lease_tenancies, Error in update of liability account info, msg ='||sqlerrm);
      end;
     --
     -- update gl_accrued_liability
     --
      begin
       --
            savepoint init_here;
       --
            merge into xxcus.xxcus_opn_rer_locations_all t --xxcus.xxcus_opn_lease_tenancies_all t
            using (
                    select code_combination_id, concatenated_segments accrued_liability_cc
                    from   gl_code_combinations_kfv
                    where  1 =1
                   ) s
            on (
                    t.lea_accrual_ccid =s.code_combination_id
               )
            when matched then update set
             t.gl_accrued_liability =s.accrued_liability_cc;
        --
        commit;
        --
      exception
       when others then
        rollback to init_here;
        print_log('@lease_tenancies, Error in update of accrued_liability account info, msg ='||sqlerrm);
      end;
     --
     -- update address information
     --
      begin
       --
            savepoint init_here;
       --
            merge into xxcus.xxcus_opn_rer_locations_all t --xxcus.xxcus_opn_lease_tenancies_all t
            using (
                    select address_id      addr_id
                          ,address_line1   addr_line1
                          ,address_line2   addr_line2
                          ,address_line3   addr_line3
                          ,county          county
                          ,city            city
                          ,state||province state_prov
                          ,zip_code        postal_code
                          ,country         country_code
                          ,(
                             select territory_short_name
                             from   fnd_territories_tl
                             where  1 =1
                               and  territory_code =country
                               and  language ='US'
                           ) country
                    from   pn_addresses_all
                    where  1 =1
                   ) s
            on (
                    t.address_id =s.addr_id
               )
            when matched then update set
             t.address_line1  =s.addr_line1
            ,t.address_line2  =s.addr_line2
            ,t.address_line3  =s.addr_line3
            ,t.county         =s.county
            ,t.city           =s.city
            ,t.state_province =s.state_prov
            ,t.postal_code    =s.postal_code
            ,t.country_code   =s.country_code
            ,t.country        =s.country;
        --
        commit;
        --
      exception
       when others then
        rollback to init_here;
        print_log('@lease_tenancies, Error in update of address info, msg ='||sqlerrm);
      end;
     -- Begin Ver 3.2
     -- update address info when location is either OFFICE or YARD
     --
      begin
       --
            savepoint init_here;
       --
            merge into xxcus.xxcus_opn_rer_locations_all t
            using (
                        select location_id, country_code, address_country, address_line1, address_line2, address_line3, city, postal_code, county, hds_st_prv
                        from xxcus.xxcus_opn_properties_all
                        where 1 =1
                        and opn_type in ('OFFICE', 'SECTION') --Offices and Yard only
                        group by location_id, country_code, address_country, address_line1, address_line2, address_line3, city, postal_code, county, hds_st_prv
                   ) s
            on (
                    t.ten_locn_id =s.location_id
               )
            when matched then update set
             t.address_line1   =s.address_line1
            ,t.address_line2   =s.address_line2
            ,t.address_line3   =s.address_line3
            ,t.city                       =s.city
            ,t.postal_code       =s.postal_code
            ,t.county                 =s.county
            ,t.state_province  =s.hds_st_prv
            ,t.country_code    =s.country_code
            ,t.country                =s.address_country
            ;
        --
        commit;
        --
      exception
       when others then
        rollback to init_here;
        print_log('@lease_tenancies, Error in update of address info for locations of type OFFICE/YARD, msg ='||sqlerrm);
      end;
      --
     -- update related party name
     --
      begin
       --
            savepoint init_here;
       --
            merge into xxcus.xxcus_opn_rer_locations_all t
            using (
                          select b.flex_value related_party_code, c.description related_party_desc
                          from apps.fnd_flex_values b, apps.fnd_flex_values_tl c
                           where 1 =1
                                and b.flex_value_set_id = (
                                                                                    select flex_value_set_id
                                                                                    from apps.fnd_flex_value_sets
                                                                                    where 1 =1
                                                                                        and  flex_value_set_name ='xxcus_pn_rel_party'
                                                                                 )
                                and c.flex_value_id =b.flex_value_id
                   ) s
            on (
                    t.related_party =s.related_party_code
                  )
            when matched then update set
             t.related_party_name  =s.related_party_desc;
        --
        commit;
        --
      exception
       when others then
        rollback to init_here;
        print_log('@lease_tenancies, Error in update of related_party_name, msg ='||sqlerrm);
      end;
      --
      -- update opn_tenancy_suite
      --
      begin
          --
          savepoint init_here;
          --
            update xxcus.xxcus_opn_rer_locations_all t --xxcus.xxcus_opn_lease_tenancies_all t
            set t.opn_tenancy_suite  =case
                                                                  when loc_type_lookup_code IN ('BUILDING', 'LAND') then address_line2
                                                                  when loc_type_lookup_code IN ('OFFICE', 'SECTION') then section_suite
                                                                  when loc_type_lookup_code IN ('FLOOR', 'PARCEL') then  address_line2
                                                                  else Null
                                                              end;
         --
        commit;
        --
      exception
       --
       when others then
        rollback to init_here;
        print_log('@lease_tenancies, Error in update of opn_tenancy_suite, msg ='||sqlerrm);
       --
      end;
      --
     /*
     --
     -- update bldg_land_name
     --
      begin
        --
        for rec in (
                       select ten_locn_id location_id
                       from   xxcus.xxcus_opn_rer_locations_all --xxcus.xxcus_opn_lease_tenancies_all
                       where  1 =1
                         and  loc_type_lookup_code IN ('BUILDING', 'LAND')
                       group by ten_locn_id
                   )
        loop
         --
         begin
          --
          select building
          into   v_building
          from   pn_locations_all
          where  1 =1
            and  location_id =rec.location_id
            and  location_type_lookup_code IN ('BUILDING', 'LAND')
          group by building;
          --
         exception
          when others then
           v_building :=Null;
         end;
         --
         begin
          --
          savepoint init_here;
          --
          update xxcus.xxcus_opn_rer_locations_all --xxcus.xxcus_opn_lease_tenancies_all
          set bldg_land_name =v_building
          where 1 =1
            and  ten_locn_id =rec.location_id
            and  loc_type_lookup_code IN ('BUILDING', 'LAND');
          --
         exception
          when others then
           rollback to init_here;
         end;
         --
        end loop;
        --
        commit;
        --
      exception
       when others then
        rollback to init_here;
        print_log('@lease_tenancies, Error in update of bldg/land name, msg ='||sqlerrm);
      end;
      */
     -- End Ver 3.2
      -- Begin Ver 3.3
      begin
          --
           for le_rec in all_tenancies loop
                --
                begin
                    --
                    savepoint sqr1;
                    --
                    update xxcus.xxcus_opn_rer_locations_all
                    set hds_total_bldg_sf_for_rer =le_rec.total_bldg_sf
                         ,hds_total_land_sf_for_rer =le_rec.total_land_sf
                    where 1 =1
                          and lease_id =le_rec.lease_id;
                    --
                exception
                 when others then
                  rollback to sqr1;
                  print_log('error during update of hds_total_land_sf_for_rer and hds_total_bldg_sf_for_rer, lease_id  ='||le_rec.lease_id||', msg ='||sqlerrm);
                end;
                --
           end loop;
           --
           commit;
           --
      exception
       when others then
        print_error_log('Outer block, error during update of hds_total_land_sf_for_rer and hds_total_bldg_sf_for_rer, msg ='||sqlerrm);
      end;
      -- begin update of hds_opn_primary_loc_for_rer
       --
       begin
        --
            update xxcus.xxcus_opn_rer_locations_all
            set hds_opn_primary_loc_for_rer ='Y'
            where 1 =1
                  and pri_loc_code =loc_code
                  and hds_opn_primary_loc_for_rer ='Z'
               returning rowid, lease_id bulk collect into le_rec; --save rowid's for further updates...
        --
        print_error_log('@Primary flag update for RER record, first pass : pri_loc_code =loc_code, an exact match : records updated  :' ||sql%rowcount);
        --
        commit;
        --
          for i in le_rec.first .. le_rec.last --loop index "i"
          --
          loop
           --
            begin
             --
             savepoint sqr1;
             --
                update xxcus.xxcus_opn_rer_locations_all
                set hds_opn_primary_loc_for_rer ='N' --When a primary row is found for RER, flag all other rows for the lease with a N
                where 1 =1
                      and lease_id =le_rec(i).le_id
                      and rowid <>le_rec(i).le_rowid
                      and hds_opn_primary_loc_for_rer ='Z';
             --
             n_count :=n_count + sql%rowcount;
             --
            exception
             when others then
              print_error_log('@ location 102, row id ='||le_rec(i).le_rowid||', error message =>' ||sqlerrm);
              rollback to sqr1;
            end;
            --
          end loop; --loop index "i"
          --
           print_error_log('Total rows updated with primary RER to N [pri_loc_code =loc_code] when we''ve a primary RER exists for a lease, @scenario 1 :'||n_count);
          --
          commit;
          --
          le_rec.delete; --start with a clean slate.
          n_count :=0; --reset to zero
          --
          open le_rec3;
          fetch le_rec3 bulk collect into le_rec;
          close le_rec3;
          --
          if le_rec.count >0 then --cursor  le_rec3
           print_error_log('@Data set le_rec3, current value for le_rec.count :'||le_rec.count);
           --
            for j in le_rec.first .. le_rec.last loop
                --
                begin
                 --
                 savepoint sqr1;
                 --
                    update xxcus.xxcus_opn_rer_locations_all
                    set hds_opn_primary_loc_for_rer ='Y'
                    where 1 =1
                          and lease_id =le_rec(j).le_id
                          and rowid = le_rec(j).le_rowid
                          and hds_opn_primary_loc_for_rer ='Z';
                 --
                 n_count :=n_count +sql%rowcount;
                 --
                exception
                 when others then
                  print_error_log('@ location 103-A, rowid ='||le_rec(j).le_rowid||', error message =>' ||sqlerrm);
                  rollback to sqr1;
                end;
                --
            end loop; --loop index "j" first update
           --
           print_error_log('Total rows updated from data set le_rec3 for primary RER flag to Y :'||n_count);
           --
           n_count :=0;
           --
           commit;
           --
            for j in le_rec.first .. le_rec.last loop
                --
                begin
                 --
                 savepoint sqr1;
                 --
                    update xxcus.xxcus_opn_rer_locations_all
                    set hds_opn_primary_loc_for_rer ='N' --When a primary row is found for RER, flag all other rows for the lease with a N
                    where 1 =1
                          and lease_id =le_rec(j).le_id
                          and rowid <> le_rec(j).le_rowid
                          and hds_opn_primary_loc_for_rer ='Z';
                 --
                 n_count :=n_count +sql%rowcount;
                 --
                exception
                 when others then
                  print_error_log('@ location 103-B, rowid ='||le_rec(j).le_rowid||', error message =>' ||sqlerrm);
                  rollback to sqr1;
                end;
                --
            end loop; --loop index "j" --second update
           --
           print_error_log('Total rows updated from data set le_rec3 for primary RER flag to N :'||n_count);
           --
          end if; --cursor  le_rec3
          --
          commit;
          --
          le_rec.delete; --start with a clean slate.
          n_count :=0; --reset to zero *****
          --
          open le_rec4;
          fetch le_rec4 bulk collect into le_rec;
          close le_rec4;
          --
          if le_rec.count >0 then --cursor  le_rec4
           print_error_log('@Data set le_rec4, current value for le_rec.count :'||le_rec.count);
           --
            for j in le_rec.first .. le_rec.last loop
                --
                begin
                 --
                 savepoint sqr1;
                 --
                    update xxcus.xxcus_opn_rer_locations_all
                    set hds_opn_primary_loc_for_rer ='Y'
                    where 1 =1
                         and  lease_id =le_rec(j).le_id
                          and rowid = le_rec(j).le_rowid
                          and hds_opn_primary_loc_for_rer ='Z';
                 --
                 n_count :=n_count +sql%rowcount;
                 --
                exception
                 when others then
                  print_error_log('@ location 104, rowid ='||le_rec(j).le_rowid||', error message =>' ||sqlerrm);
                  rollback to sqr1;
                end;
                --
            end loop; --loop index "j" first update
           --
           commit;
           --
           n_count :=0;
           --
           print_error_log('Total rows updated from data set le_rec4 with primary RER flag to Y :'||n_count);
           --
            for j in le_rec.first .. le_rec.last loop
                --
                begin
                 --
                 savepoint sqr1;
                 --
                    update xxcus.xxcus_opn_rer_locations_all
                    set hds_opn_primary_loc_for_rer ='N' --When a primary row is found for RER, flag all other rows for the lease with a N
                    where 1 =1
                          and lease_id =le_rec(j).le_id
                          and rowid <> le_rec(j).le_rowid
                          and hds_opn_primary_loc_for_rer ='Z';
                 --
                 n_count :=n_count +sql%rowcount;
                 --
                exception
                 when others then
                  print_error_log('@ location 104, rowid ='||le_rec(j).le_rowid||', error message =>' ||sqlerrm);
                  rollback to sqr1;
                end;
                --
            end loop; --loop index "j" first update
           --
           print_error_log('Total rows updated from data set le_rec4 with primary RER flag to N :'||n_count);
           --
           commit;
           --
          end if; --cursor  le_rec4
          --
         n_tie_brkr_count1 :=0;
         n_tie_brkr_count2 :=0;
          --
          for k_rec in (  select a.lease_id
                            from xxcus.xxcus_opn_rer_locations_all  a
                            where 1 =1
                                  and a.hds_opn_primary_loc_for_rer ='Z'
                            group by a.lease_id
                        )
           loop
           --
           for m_rec in
            (
                select a.rowid row_id, a.lease_id
                from xxcus.xxcus_opn_rer_locations_all  a
                where 1 =1
                and a.lease_id =k_rec.lease_id
                and a.hds_opn_primary_loc_for_rer ='Z'
                and hds_loc_tie_breaker_seq =
                              (
                                    select min(hds_loc_tie_breaker_seq)
                                    from   xxcus.xxcus_opn_rer_locations_all  b
                                    where 1 =1
                                          and b.hds_opn_primary_loc_for_rer ='Z'
                                          and b.lease_id =a.lease_id
                              )
            ) loop
                --
                begin
                 --
                 savepoint sqr1;
                 --
                    update xxcus.xxcus_opn_rer_locations_all
                    set hds_opn_primary_loc_for_rer ='Y'
                    where 1 =1
                         and  lease_id =m_rec.lease_id
                          and rowid = m_rec.row_id
                          and hds_opn_primary_loc_for_rer ='Z';
                 --
                 n_tie_brkr_count1 :=n_tie_brkr_count1 +sql%rowcount;
                 --
                exception
                 when others then
                  print_error_log('@ location 105, rowid ='||m_rec.row_id||', lease_id ='||m_rec.lease_id||', error message =>' ||sqlerrm);
                  rollback to sqr1;
                end;
                --
                begin
                 --
                 savepoint sqr1;
                 --
                    update xxcus.xxcus_opn_rer_locations_all
                    set hds_opn_primary_loc_for_rer ='N'
                    where 1 =1
                         and  lease_id =m_rec.lease_id
                          and rowid <> m_rec.row_id
                          and hds_opn_primary_loc_for_rer ='Z';
                 --
                 n_tie_brkr_count2 :=n_tie_brkr_count2 +sql%rowcount;
                 --
                exception
                 when others then
                  print_error_log('@ location 106, rowid <>'||m_rec.row_id||'and lease_id ='||m_rec.lease_id||', error message =>' ||sqlerrm);
                  rollback to sqr1;
                end;
                --
            end loop; --end of loop index "m_rec"
           --
          end loop;  --end of loop index "k_rec"
          --
          commit;
          --
           print_error_log('Total rows updated from tie breaker with primary RER flag to Y :'||n_tie_brkr_count1);
           --
           print_error_log('Total rows updated from tie breaker with primary RER flag to N :'||n_tie_brkr_count2);
           --
       exception
        when others then
         print_error_log('when others, @ outer block of primary flag upate for RER-LOC, first pass , error msg ='||sqlerrm);
       end;
      -- end update of hds_opn_primary_loc_for_rer
      --
      -- End Ver 3.3
     --
  exception
   when others then
    print_error_log('@before outer block of lease_tenancies, error message ='||sqlerrm); --Ver 3.3
  end;
  --
 exception
  when others then
   print_error_log('Error in xxcus_opn_master_extract.lease_tenancies, message ='||sqlerrm); --Ver 3.3
    l_err_msg := l_sec;
        l_err_msg := l_err_msg || ' ...Error_Stack...' ||
                     dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                     dbms_utility.format_error_backtrace();
        print_error_log(l_err_msg); --Ver 3.3
        l_err_callpoint := l_sec;
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => l_err_callpoint
                                            ,p_request_id        => fnd_global.conc_request_id
                                            ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                           dbms_utility.format_error_stack() ||
                                                                           ' Error_Backtrace...' ||
                                                                           dbms_utility.format_error_backtrace()
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => substr(l_err_msg
                                                                          ,1
                                                                          ,240)
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => l_module);
 end lease_tenancies;
 --
 --
/*
 ************************************************************************************************
 *
 * FUNCTION: apex_secat_re_tbl_refresh
 * DESCRIPTION: To populate lease tenancies table
 * PARAMETERS : None
 * ==========
 * History
 * =======================================================================
 * ESMS       Date        Version Notes
 * ---------- ----------- ------- ---------------------------------------
 * 269300     11/05/2014  1.0     Created.
 * =======================================================================
 * --
 * CALLED BY: HDS OPN: Extract Locations [Concurrent Program]. In other words the main routine
 ************************************************************************************************
 */
 procedure apex_secat_re_tbl_refresh as
  --
  l_module        VARCHAR2(24) :='OPN';
  l_err_msg       CLOB;
  l_sec           VARCHAR2(255);
  --
  l_err_callfrom  VARCHAR2(75) DEFAULT 'apps.xxcus_opn_master_extract.apex_secat_re_tbl_refresh';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  --
 begin
  --
  savepoint init_here;
  --
  delete xxcus.xxcus_apex_secat_re_tbl;
  --
  insert into xxcus.xxcus_apex_secat_re_tbl
  (
select ops_status,
          lc_business_unit,
          bu_id,
          loc_bu,
          location_code,
          primary_fru,
          lob_branch,
          primary_physical_address1,
          primary_physical_address2,
          city,
          st_prv,
          zip,
          county,
          country,
          phone,
          fax,
          longitude,
          latitude,
          timezone,
          year_built,
          year_reno,
          rer_type,
          pmweb_rer_type,
          (select description
             from apps.fnd_lookup_values_vl
            where     1 = 1
                  and lookup_type = 'SECAT_LEASE_TYP'
                  and meaning = nvl (pmweb_rer_type, rer_type))
             secat_rer_type,
          subleasee_name,
          customer_site,
          purchase_lease_start_date,
          owner_name,
          owner_address,
          owner_city,
          owner_state,
          owner_zip,
          OWNER_PHONE,
          OWNER_FAX,
          space_type
     from (
             select ops_status,
                    lc_business_unit,
                    bu_id,
                    loc_bu,
                    location_code,
                    primary_fru,
                    lob_branch,
                    primary_physical_address1,
                    primary_physical_address2,
                    city,
                    st_prv,
                    zip,
                    county,
                    country,
                    phone,
                    fax,
                    longitude,
                    latitude,
                    timezone,
                    year_built,
                    year_reno,
                    regexp_replace (
                       listagg (rer_type, '/')
                          within group (order by location_code),
                       '([^/]+)(/\1)+',
                       '\1')
                       as rer_type,
                    regexp_replace (
                       listagg (pmweb_rer_type, '/')
                          within group (order by location_code),
                       '([^/]+)(/\1)+',
                       '\1')
                       as pmweb_rer_type,
                    listagg (subleasee_name, '/')
                       within group (order by location_code)
                       as subleasee_name,
                    customer_site,
                    max (purchase_lease_start_date) purchase_lease_start_date,
                    listagg (owner_name, '/')
                       within group (order by location_code)
                       as owner_name,
                    listagg (owner_address, '/')
                       within group (order by location_code)
                      as owner_address,
                    listagg (owner_city, '/')
                       within group (order by location_code)
                       as owner_city,
                    listagg (owner_state, '/')
                       within group (order by location_code)
                       as owner_state,
                    listagg (owner_zip, '/')
                       within group (order by location_code)
                       as owner_zip,
                    listagg (owner_phone, '/')
                       within group (order by location_code)
                       as owner_phone,
                    listagg (owner_fax, '/')
                       within group (order by location_code)
                       AS OWNER_FAX
                       ,space_type
               from (select distinct
                            opa.location_id,
                            loc.hds_ops_status ops_status,
                            loc.lc_business_unit lc_business_unit,
                            loc.bu_id bu_id,
                            loc.bu_desc loc_bu,
                            opa.location_code,
                            loc.opn_fru primary_fru,
                            loc.lc_lob_branch lob_branch,
                            opa.address_line1 primary_physical_address1,
                            opa.address_line2 primary_physical_address2,
                            opa.city city,
                            coalesce (opa.state, opa.province) st_prv,
                            opa.postal_code zip,
                            opa.county county,
                            opa.country country,
                            loc.primary_phone_number phone,
                            null fax,
                            opa.bldg_longitude longitude,
                            opa.bldg_latitude latitude,
                            opa.zone_name timezone,
                            case
                               when (   regexp_like (blt.description,
                                                     '^-?[[:digit:],.]*$')
                                     or blt.description is null)
                               then
                                  blt.description
                               else
                                  '9999'
                            end
                               as year_built,
                            case
                               when (   regexp_like (reno.description,
                                                     '^-?[[:digit:],.]*$')
                                     or reno.description is null)
                               then
                                  reno.description
                               else
                                  '9999'
                            end
                               as year_reno,
                            opa.bl_ld_tenancy rer_type,
                            ls_all.lease_type pmweb_rer_type,
                            subt.company_name subleasee_name,
                            opa.office_customer_site customer_site,
                            ls_all.lease_commencement purchase_lease_start_date,
                            ll.company_name owner_name,
                            ll.address_line1 owner_address,
                            ll.city owner_city,
                            ll.state owner_state,
                            ll.postal_code owner_zip,
                            null owner_phone,
                            null owner_fax,
                            LS_ALL.TENANCY_PRI_FLAG TENANCY,
                            loc.space_type space_type         --ESMS 273067  15-DEC-2014 Including Space Type column
                       from xxcus.xxcus_opn_properties_all opa,
                            xxcus.hds_pn_lft_year_blt blt,
                            xxcus.hds_pn_lft_year reno,
                            xxcus.xxcus_opn_fru_loc_all loc,
                            xxcus.xxcus_opn_rer_locations_all ls_all,
                            xxcus.hds_le_cont_role_ll ll,
                            xxcus.hds_le_cont_role_subt subt
                      where     1 = 1
                            and opa.location_id = loc.bldg_land_id
                            and opa.location_id = blt.location_id(+)
                            and opa.location_id = reno.location_id(+)
                            and opa.location_id = ls_all.ten_locn_id(+)
                            and ls_all.lease_id = ll.lease_id(+)
                            and ls_all.lease_id = subt.lease_id(+)
                            and opa.hds_current_flag ='Y'
                            and loc.hds_pri_fru_flag = 'Y'
                            and ll.status(+) = 'A'
                            and subt.status(+) = 'A'
                            and loc.hds_fru_section_current_flag = 'Y'
                            and (   nvl (ls_all.tenancy_pri_flag, 'Y') = 'Y'
                                 or ls_all.lease_type in
                                       ('Income Sublease', 'Income Lease'))
                            and opa.country in ('United States', 'Canada')
                            and opa.opn_type in ('PROPERTY')
                            AND OPA.LOCATION_TYPE_LOOKUP_CODE IN ('BUILDING')
                            AND LOC.SPACE_TYPE_LOOKUP_CODE not IN    ---ESMS 273067 15-DEC-2015 Excluding specific space types
                            (SELECT DISTINCT LOOKUP_CODE
                                FROM   APPS.fnd_lookup_values_vl
                                WHERE 1=1
                                AND LOOKUP_TYPE = 'PN_NONSECAT_SPACE_TYPE'
                                )
                            )
              where     1 = 1
                    and lc_business_unit not in
                           (select distinct lookup_code
                              from apps.fnd_lookup_values_vl
                             where     1 = 1
                                   AND LOOKUP_TYPE = 'XXCUS_OPN_NONSECAT_BU')

           group by location_code,
                    ops_status,
                    lc_business_unit,
                    bu_id,
                    loc_bu,
                    location_code,
                    primary_fru,
                    lob_branch,
                    primary_physical_address1,
                    primary_physical_address2,
                    city,
                    st_prv,
                    zip,
                    county,
                    country,
                    phone,
                    fax,
                    longitude,
                    latitude,
                    timezone,
                    year_built,
                    year_reno,
                    CUSTOMER_SITE,
                    space_type
          )
  );
  --
  commit;
  --
 exception
  when others then
   rollback to init_here;
   print_log('Error in xxcus_opn_master_extract.lease_tenancies, message ='||sqlerrm);
    l_err_msg := l_sec;
        l_err_msg := l_err_msg || ' ...Error_Stack...' ||
                     dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                     dbms_utility.format_error_backtrace();
        fnd_file.put_line(fnd_file.log, l_err_msg);
        l_err_callpoint := l_sec;
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => l_err_callpoint
                                            ,p_request_id        => fnd_global.conc_request_id
                                            ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                           dbms_utility.format_error_stack() ||
                                                                           ' Error_Backtrace...' ||
                                                                           dbms_utility.format_error_backtrace()
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => substr(l_err_msg
                                                                          ,1
                                                                          ,240)
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => l_module);
 end apex_secat_re_tbl_refresh;
  --
/*
 ************************************************************************************************
 *
 * FUNCTION: xxcus_opn_lease_tab_insr
 * DESCRIPTION: To populate xxcus.xxcus_opn_lease_tab_insurance table
 * PARAMETERS : None
 * ==========
 * History
 * =======================================================================
 * ESMS       Date        Version Notes
 * ---------- ----------- ------- ---------------------------------------
 * 269300     12/11/2014  1.0     Created.
 * =======================================================================
 * --
 * CALLED BY: HDS OPN: Extract Locations [Concurrent Program]. In other words the main routine
 ************************************************************************************************
 */
  procedure xxcus_opn_lease_tab_insr (p_extract_date in date, p_user_id in number) is
   --
  begin
   --
   EXECUTE IMMEDIATE 'TRUNCATE TABLE xxcus.xxcus_opn_lease_tab_insurance';
   --
   savepoint init_here;
   --
    insert into xxcus.xxcus_opn_lease_tab_insurance
    (
    select lea.lease_name                 lease_name
          ,lea.lease_number               lease_number
          ,loc.location_code              primary_loc_code
          ,insr.insurance_type            insurance_type
          ,insr.insurer_name              insurer_name
          ,insr.policy_number             policy_number
          ,insr.status                    policy_status
          ,insr.policy_start_date         start_date
          ,insr.policy_expiration_date    expiration_date
          ,insr.required_amount           coverage_amount_required
          ,insr.insured_amount            coverage_amount_purchased
          ,insr.attribute1                additional_insured
          ,insr.attribute2                loss_payee
          ,regexp_replace
            (
               insr.insurance_comments
              ,'([^[:print:]])',' '
            )                             comments
          ,lea.lease_id                   lease_id
          ,loc.location_id                primary_loc_id
          ,insr.insurance_requirement_id  insr_requirement_id
          ,p_extract_date                    extract_date
          ,p_user_id                         created_by
    from   pn_insurance_requiremnts_v insr
          ,pn_leases_v                lea
          ,(
            select location_code location_code
                  ,location_type location_type
                  ,location_id   location_id
            from   pn_locations_v loc
            where  1 =1
            group by location_id, location_code, location_type
           )  loc
    where  1 =1
      and  lea.lease_id        =insr.lease_id
      and  loc.location_id(+)  =lea.location_id
    );
   --
   print_log('@xxcus_opn_lease_tab_insr, total records inserted ='||sql%rowcount);
   --
   commit;
    --
  exception
   when others then
    rollback to init_here;
  end xxcus_opn_lease_tab_insr;
  --
/*
 ************************************************************************************************
 *
 * FUNCTION: xxcus_opn_lease_tab_rights
 * DESCRIPTION: To populate xxcus.xxcus_opn_lease_tab_rights table
 * PARAMETERS : None
 * ==========
 * History
 * =======================================================================
 * ESMS       Date        Version Notes
 * ---------- ----------- ------- ---------------------------------------
 * 269300     12/11/2014  1.0     Created.
 * =======================================================================
 * --
 * CALLED BY: HDS OPN: Extract Locations [Concurrent Program]. In other words the main routine
 ************************************************************************************************
 */
  procedure xxcus_opn_lease_tab_rights (p_extract_date in date, p_user_id in number) is
   --
  begin
   --
   EXECUTE IMMEDIATE 'TRUNCATE TABLE xxcus.xxcus_opn_lease_tab_rights';
   --
   savepoint init_here;
   --
    insert into xxcus.xxcus_opn_lease_tab_rights
    (
    select lea.lease_name               lease_name
          ,lea.lease_number             lease_number
          ,loc.location_code            primary_loc_code
          ,rts.right_num                right_num
          ,rts.right_type               right_type
          ,rts.right_status             grant_code
          ,rts.right_reference          reference
          ,regexp_replace
            (
               rts.right_comments
              ,'([^[:print:]])',' '
            )                           comments
          ,rts.start_date               start_date
          ,rts.expiration_date          expiration_date
          ,rts.right_status_code        right_status_code
          ,rts.right_type_code          right_type_code
          ,lea.lease_id                 lease_id
          ,loc.location_id              primary_loc_id
          ,rts.right_id                 right_id
          ,p_extract_date               extract_date
          ,p_user_id                    created_by
    from   pn_rights_v                rts
          ,pn_leases_v                lea
          ,(
            select location_code location_code
                  ,location_type location_type
                  ,location_id   location_id
            from   pn_locations_v loc
            where  1 =1
            group by location_id, location_code, location_type
           )  loc
    where  1 =1
      and  lea.lease_id        =rts.lease_id
      and  loc.location_id(+)  =lea.location_id
    );
   --
   print_log('@xxcus_opn_lease_tab_rights, total records inserted ='||sql%rowcount);
   --
   commit;
    --
  exception
   when others then
    rollback to init_here;
  end xxcus_opn_lease_tab_rights;
  --
/*
 ************************************************************************************************
 *
 * PROCEDURE: xxcus_opn_lease_tab_oblig
 * DESCRIPTION: To populate xxcus.xxcus_opn_lease_tab_oblig table
 * PARAMETERS : None
 * ==========
 * History
 * =======================================================================
 * ESMS       Date        Version Notes
 * ---------- ----------- ------- ---------------------------------------
 * 269300     12/11/2014  1.0     Created.
 * =======================================================================
 * --
 * CALLED BY: HDS OPN: Extract Locations [Concurrent Program]. In other words the main routine
 ************************************************************************************************
 */
  procedure xxcus_opn_lease_tab_oblig (p_extract_date in date, p_user_id in number) is
   --
  begin
   --
   EXECUTE IMMEDIATE 'TRUNCATE TABLE xxcus.xxcus_opn_lease_tab_oblig';
   --
   savepoint init_here;
   --
    insert into xxcus.xxcus_opn_lease_tab_oblig
    (
    select lea.lease_name               lease_name
          ,lea.lease_number             lease_number
          ,loc.location_code            primary_loc_code
          ,obl.obligation_num           obligation_num
          ,obl.service_type             obligation_type
          ,obl.responsibility_prov_name service_provider_name
          ,obl.common_area_resp         common_area
          ,obl.start_date               start_date
          ,obl.end_date                 end_date
          ,obl.obligation_reference     reference
          ,obl.responsibility_type      responsibility_type
          ,obl.financial_resp_party     financial_responsibility
          ,obl.responsibility_maint     maintenance_responsibility
          ,obl.status                   status
          ,obl.financial_pct_resp       responsibility_percent
          ,regexp_replace
            (
               obl.obligation_comments
              ,'([^[:print:]])',' '
            )                           comments
          ,obl.service_type_lookup_code service_type_code
          ,lea.lease_id                 lease_id
          ,loc.location_id              primary_loc_id
          ,obl.landlord_service_id      landlord_service_id
          ,p_extract_date               extract_date
          ,p_user_id                    created_by
    from   pn_landlord_services_v     obl
          ,pn_leases_v                lea
          ,(
            select location_code location_code
                  ,location_type location_type
                  ,location_id   location_id
            from   pn_locations_v loc
            where  1 =1
            group by location_id, location_code, location_type
           )  loc
    where  1 =1
      and  lea.lease_id        =obl.lease_id
      and  loc.location_id(+)  =lea.location_id
    );
   --
   print_log('@xxcus_opn_lease_tab_oblig, total records inserted ='||sql%rowcount);
   --
   commit;
    --
  exception
   when others then
    rollback to init_here;
  end xxcus_opn_lease_tab_oblig;
  --
/*
 ************************************************************************************************
 *
 * PROCEDURE: xxcus_opn_lease_tab_options
 * DESCRIPTION: To populate xxcus.xxcus_opn_lease_tab_options table
 * PARAMETERS : None
 * ==========
 * History
 * =======================================================================
 * ESMS       Date        Version Notes
 * ---------- ----------- ------- ---------------------------------------
 * 269300     12/11/2014  1.0     Created.
 * =======================================================================
 * --
 * CALLED BY: HDS OPN: Extract Locations [Concurrent Program]. In other words the main routine
 ************************************************************************************************
 */
  procedure xxcus_opn_lease_tab_options (p_extract_date in date, p_user_id in number) is
   --
  begin
   --
   EXECUTE IMMEDIATE 'TRUNCATE TABLE xxcus.xxcus_opn_lease_tab_options';
   --
   savepoint init_here;
   --
    insert into xxcus.xxcus_opn_lease_tab_options
    (
    select lea.lease_name               lease_name
          ,lea.lease_number             lease_number
          ,loc.location_code            primary_loc_code
          ,opt.option_num               option_num
          ,opt.option_type              option_type
          ,opt.start_date               start_date
          ,opt.expiration_date          end_date
          ,opt.option_term              term
          ,opt.option_reference         reference
          ,opt.option_status_type       status
          ,opt.option_notice_reqd       notice_given
          ,opt.option_size              option_size
          ,opt.option_exer_start_date   exercise_begins
          ,opt.option_exer_end_date     exercise_ends
          ,opt.option_action_date       action_taken_date
          ,opt.unit_of_measure          UOM
          ,opt.option_cost              cost
          ,opt.option_area_change       area_change
          ,regexp_replace
            (
               opt.option_comments
              ,'([^[:print:]])',' '
            )                             comments
          ,opt.option_status_lookup_code  option_status_code
          ,lea.lease_id                   lease_id
          ,loc.location_id                primary_loc_id
          ,opt.option_id                  option_id
          ,p_extract_date                 extract_date
          ,p_user_id                      created_by
    from   pn_options_v       opt
          ,pn_leases_v        lea
          ,(
            select location_code location_code
                  ,location_type location_type
                  ,location_id   location_id
            from   pn_locations_v loc
            where  1 =1
            group by location_id, location_code, location_type
           )  loc
    where  1 =1
      and  lea.lease_id        =opt.lease_id
      and  loc.location_id(+)  =lea.location_id
    );
   --
   print_log('@xxcus_opn_lease_tab_options, total records inserted ='||sql%rowcount);
   --
   commit;
    --
  exception
   when others then
    rollback to init_here;
  end xxcus_opn_lease_tab_options;
  --
/*
 ************************************************************************************************
 *
 * PROCEDURE: xxcus_opn_lease_tab_notes
 * DESCRIPTION: To populate xxcus.xxcus_opn_lease_tab_notes table
 * PARAMETERS : None
 * ==========
 * History
 * =======================================================================
 * ESMS       Date        Version Notes
 * ---------- ----------- ------- ---------------------------------------
 * 269300     12/11/2014  1.0     Created.
 * =======================================================================
 * --
 * CALLED BY: HDS OPN: Extract Locations [Concurrent Program]. In other words the main routine
 ************************************************************************************************
 */
  procedure xxcus_opn_lease_tab_notes (p_extract_date in date, p_user_id in number) is
   --
  begin
   --
   EXECUTE IMMEDIATE 'TRUNCATE TABLE xxcus.xxcus_opn_lease_tab_notes';
   --
   savepoint init_here;
   --
    insert into xxcus.xxcus_opn_lease_tab_notes
    (
    select lea.lease_name                                      lease_name
          ,lea.lease_number                                    lease_number
          ,priloc.location_code                                primary_location
          ,nh.note_type                                        note_type
          ,nh.note_date                                        note_date
          ,nh.updated_by_user_name                             updated_by_user
          ,regexp_replace
            (
               nd.text
              ,'([^[:print:]])',' '
            )                                                  description
          ,nh.note_header_id                                   note_header_id
          ,nd.note_detail_id                                   note_detail_id
          ,nh.lease_id                                         lease_id
          ,priloc.location_id                                  primary_loc_id
          ,p_extract_date                                      extract_date
          ,p_user_id                                           created_by
    from   pn_note_headers_v     nh
          ,pn_note_details_vl    nd
          ,pn_leases_v           lea
          ,(
            select location_code location_code
                  ,location_type location_type
                  ,location_id   location_id
            from   pn_locations_v loc
            where  1 =1
            group by location_id, location_code, location_type
           ) priloc
    where  1 =1
      and  nd.note_header_id     =nh.note_header_id
      and  lea.lease_id          =nh.lease_id
      and  priloc.location_id(+) =lea.location_id
    );
   --
   print_log('@xxcus_opn_lease_tab_notes, total records inserted ='||sql%rowcount);
   --
   commit;
    --
  exception
   when others then
    rollback to init_here;
  end xxcus_opn_lease_tab_notes;
  --
/*
 ************************************************************************************************
 *
 * PROCEDURE: xxcus_opn_lease_tab_notes
 * DESCRIPTION: To populate xxcus.xxcus_opn_lease_tab_billings table
 * PARAMETERS : None
 * ==========
 * History
 * =======================================================================
 * ESMS       Date        Version Notes
 * ---------- ----------- ------- ---------------------------------------
 * 269300     12/11/2014  1.0     Created.
 * =======================================================================
 * --
 * CALLED BY: None [Future use]
 ************************************************************************************************
 */
  procedure xxcus_opn_lease_tab_billings (p_extract_date in date, p_user_id in number) is
   --
  begin
   --
   EXECUTE IMMEDIATE 'TRUNCATE TABLE xxcus.xxcus_opn_lease_tab_billings';
   --
   savepoint init_here;
   --
    insert into xxcus.xxcus_opn_lease_tab_billings
    (
    select lea.lease_name                                      lease_name
          ,lea.lease_number                                    lease_number
          ,priloc.location_code                                primary_location
          ,pmt.term_template                                   term_template
          ,loc.location_code                                   location_code
          ,pmt.payment_purpose                                 purpose
          ,pmt.payment_term_type                               type
          ,pmt.frequency_type                                  frequency
          ,pmt.normalize                                       normalize
          ,pmt.include_in_var_rent                             natural_breakpoint_basis
          ,pmt.schedule_day                                    schedule
          ,pmt.start_date                                      start_date
          ,pmt.end_date                                        end_date
          ,lkup1.description                                   source_module_desc
          ,pmt.agreement_name                                  agreement_name
          ,pmt.agreement_number                                agreement_number
          ,pmt.payment_function                                payment_function
          ,pmt.recon_period                                    reconciliation_period
          ,pmt.vendor_name                                     supplier_name
          ,pmt.vendor_number                                   supplier_number
          ,pmt.vendor_site                                     supplier_site
          ,pmt.customer_name                                   customer_name
          ,pmt.customer_number                                 customer_number
          ,pmt.customer_site_use                               bill_to_site
          ,pmt.cust_ship_site_use                              ship_to_site
          ,(
             case
              when pmt.customer_id is not null then
               (
                  select name
                  from   ra_terms
                  where  1 =1
                    and  term_id =pmt.ap_ar_term_id
               )
              when pmt.vendor_id is not null then
               (
                  select name
                  from   ap_terms
                  where  1 =1
                    and  term_id =pmt.ap_ar_term_id
               )
             else null
             end
           )                                                   payment_term
          ,(
             case
              when (pmt.cust_trx_type_id is not null) then
               (
                  select name
                  from   ra_cust_trx_types
                  where  1 =1
                    and  cust_trx_type_id =pmt.cust_trx_type_id
               )
              else null
             end
           )                                                   ar_transaction_type
          ,pmt.estimated_amount                                estimated_amount
          ,pmt.actual_amount                                   actual_amount
          ,pmt.target_date                                     target_date
          ,0                                                   annual_amount
          ,pmt.area                                            area
          ,0                                                   annual_area
          ,pmt.currency_code                                   currency
          ,pmt.rate                                            rate
          ,pmt.recoverable_flag                                recoverable
          ,pmt.inv_group_name                                  invoice_grouping_name
          ,regexp_replace
            (
               pmt.term_comments
              ,'([^[:print:]])',' '
            )                                                  comments
          ,pmt.payment_purpose_code                            payment_purpose_code
          ,pmt.payment_term_type_code                          payment_term_type_code
          ,pmt.frequency_code                                  frequency_code
          ,pmt.source_module                                   source_module_code
          ,lea.lease_id                                        lease_id
          ,priloc.location_id                                  primary_loc_id
          ,pmt.term_template_id                                term_template_id
          ,pmt.location_id                                     location_id
          ,pmt.ap_ar_term_id                                   ap_ar_term_id
          ,pmt.cust_trx_type_id                                ar_cust_trx_type_id
          ,pmt.payment_term_id                                 lease_pmt_term_id
          ,pmt.vendor_id                                       vendor_id
          ,pmt.vendor_site_id                                  vendor_site_id
          ,pmt.customer_id                                     customer_id
          ,pmt.customer_site_use_id                            bill_to_site_use_id
          ,pmt.cust_ship_site_id                               ship_to_site_use_id
          ,p_extract_date                                      extract_date
          ,p_user_id                                           created_by
    from   pn_payment_terms_v    pmt
          ,pn_leases_v           lea
          ,(
            select location_code location_code
                  ,location_type location_type
                  ,location_id   location_id
            from   pn_locations_v loc
            where  1 =1
            group by location_id, location_code, location_type
           )  priloc
          ,(
            select location_code location_code
                  ,location_type location_type
                  ,location_id   location_id
            from   pn_locations_v loc
            where  1 =1
            group by location_id, location_code, location_type
           )  loc
          ,fnd_lookup_values_vl  lkup1
    where  1 =1
      and  lea.lease_id          =pmt.lease_id
      and  loc.location_id(+)    =pmt.location_id
      and  priloc.location_id(+) =lea.location_id
      and  lkup1.lookup_type(+)  ='PN_TERM_SOURCE_MODULE'
      and  lkup1.lookup_code(+)  =pmt.source_module
    );
   --
   print_log('@xxcus_opn_lease_tab_billings, total records inserted ='||sql%rowcount);
   --
   commit;
    --
  exception
   when others then
    rollback to init_here;
  end xxcus_opn_lease_tab_billings;
  -- Begin Ver 3.5
/*
 *************************************************************************
 *
 * PROCEDURE: extract_premises_and_ops
 * DESCRIPTION: To populate premises and operations table data for use by OPN Supersearch OAF UI
 * History
 * =======================================================================
 * ESMS       Date        Version Notes
 * ---------- ----------- ------- ---------------------------------------
 * 315719     06/19/2014  1.0     Created.
 * =======================================================================
 * --
 * CALLED BY: HDS OPN: Extract Locations [Concurrent Program]
 ************************************************************************
 */
 procedure extract_premises_and_ops is
  --
 cursor c_lease is
     select lease_id, rer_id
     from  APPS.XXCUS_OPN_OAF_SS_PREM_OPER_V --apps.xxcus_opn_oaf_ss_prop_oper_v
     where 1 =1
     group by lease_id, rer_id;
  --
 cursor c_loc (p_lease_id in number) is
    select loc
               , loc_type
               ,address||' '||address_line2 address
               , sf_acreage
               , RANK() OVER (ORDER BY loc asc) header_seq
    from APPS.XXCUS_OPN_OAF_SS_PREM_OPER_V
    where 1 =1
    and lease_id =p_lease_id
    group by loc, loc_type, address||' '||address_line2, sf_acreage;
  --
  cursor c_prem_and_ops_rec (p_lease_id in number, p_loc in varchar2) is
  select null loc
           ,null loc_type
           ,address
           ,null  sf_acreage
           ,section
           ,bu
           ,oracle_id
           ,br
           ,fru
           ,fru_description
           ,ops_type
           ,ops_sf
           ,ops_start
           ,ops_end
           ,ops_status
           ,comments
           ,lease_id
           ,lease_change_id
           ,to_number(null) display_seq
    from APPS.XXCUS_OPN_OAF_SS_PREM_OPER_V
    where 1 =1
    and lease_id =p_lease_id
    and loc =p_loc
    order by section asc;
  --
  l_start number :=0;
  l_address varchar2(240) :=Null;
  --
 begin
  --
  delete xxcus.xxcus_opn_oaf_ss_prem_oper_all;
  --
  for lease_rec in c_lease loop
   --
   for loc_rec in c_loc (p_lease_id=>lease_rec.lease_id) loop
     --
     l_start :=(100 * loc_rec.header_seq) + 1 ;
         --
         begin
            savepoint start1;
            insert into xxcus.xxcus_opn_oaf_ss_prem_oper_all
               (
                loc
               ,loc_type
               ,rer_id
               ,address
               ,sf_acreage
               ,lease_id
               ,display_seq
               )
              values
              (
                     loc_rec.loc
                   , loc_rec.loc_type
                   ,lease_rec.rer_id
                   ,loc_rec.address
                   ,loc_rec.sf_acreage
                   ,lease_rec.lease_id
                   ,l_start
              );
         exception
          when others then
           print_log('@ header record insert of premises and operations, lease_id ='||lease_rec.lease_id||', loc ='||loc_rec.loc||', msg ='||sqlerrm);
           rollback to start1;
         end;
     --
       for ops_rec in c_prem_and_ops_rec (p_lease_id =>lease_rec.lease_id, p_loc =>loc_rec.loc) loop
             --
             begin
                --
                savepoint start2;
                --
                l_start := l_start +1;
                --
                begin
                        select ops_rec.address||' '||b.loc_suite_name
                        into     l_address
                        from xxcus.xxcus_opn_fru_loc_all b
                        where 1 =1
                        and b.bldg_land_code =loc_rec.loc
                        and b.hds_fru_section_current_flag ='Y'
                        and b.location_code =ops_rec.section
                        and rownum <2;
                exception
                 when no_data_found then
                  l_address :=ops_rec.address;
                 when others then
                  fnd_file.put_line(fnd_file.log, 'Issue in fetching suite name for loc '||loc_rec.loc||', section '||ops_rec.section);
                  l_address :=ops_rec.address;
                end;
                --
                   --
                insert into xxcus.xxcus_opn_oaf_ss_prem_oper_all
                   (
                        loc
                       ,loc_type
                       ,rer_id
                       ,address
                       ,sf_acreage
                       ,section
                       ,bu
                       ,oracle_id
                       ,br
                       ,fru
                       ,fru_description
                       ,ops_type
                       ,ops_sf
                       ,ops_start
                       ,ops_end
                       ,ops_status
                       ,comments
                       ,lease_id
                       ,lease_change_id
                       ,display_seq
                   )
                  values
                  (
                        Null --Intentionally left blank for presentation purpose in the OAF UI
                       ,Null --Intentionally left blank for presentation purpose in the OAF UI
                       ,lease_rec.rer_id
                       ,l_address --ops_rec.address
                       ,Null --Intentionally left blank for presentation purpose in the OAF UI
                       ,ops_rec.section
                       ,ops_rec.bu
                       ,ops_rec.oracle_id
                       ,ops_rec.br
                       ,ops_rec.fru
                       ,ops_rec.fru_description
                       ,ops_rec.ops_type
                       ,ops_rec.ops_sf
                       ,ops_rec.ops_start
                       ,ops_rec.ops_end
                       ,ops_rec.ops_status
                       ,ops_rec.comments
                       ,lease_rec.lease_id
                       ,ops_rec.lease_change_id
                       ,l_start
                  );
             exception
              when others then
               print_log('@ detail record insert of premises and operations, lease_id ='||lease_rec.lease_id||', loc ='||loc_rec.loc||', msg ='||sqlerrm);
               rollback to start2;
             end;
             --
       end loop;
     --
   end loop;  --loc_rec in c_loc loop
   --
  end loop; --lease_rec in c_lease loop
  --
  commit;
  --
 exception
   when others then
    rollback;
    fnd_file.put_line(fnd_file.log, '@extract_premises_and_ops, outer block, msg :'||sqlerrm);
    print_log('@extract_premises_and_ops, outer block, msg :'||sqlerrm);
 end extract_premises_and_ops;
  -- End Ver 3.5
  -- Begin Ver 3.6
/*
 *************************************************************************
 *
 * PROCEDURE: fruloc_addnl_props_set1
 * DESCRIPTION: To populate hds_bldg_count, hds_active_bldg_total_sf_prp, hds_active_acres_prp and hds_yard_count in xxcus_opn_fru_loc_all table
 * History
 * =======================================================================
 * ESMS         Date               Notes
 * ----------    -----------         ---------------------------------------
 * 318695     02/23/2016  Created. [TMS 20160302-00017]
 * =======================================================================
 * --
 * CALLED BY: HDS OPN: Extract Locations [Concurrent Program]
 ************************************************************************
 */
 procedure fruloc_addnl_props_set1 is
  --
  --A blank value in active_property field is considered an active property.
  --But for some reason there are few properties with a value of A. So we going to assume A and all blank values as an active property and coded as "A".
  -- I means Inactive Property
 cursor all_prp is
    select opn_code, nvl(active_property, 'A') prop_status --03/02/2016
    from   xxcus.xxcus_opn_properties_all
    where 1 =1
    and opn_type ='PROPERTY'
    --and hds_current_flag ='Y'
    group by opn_code, nvl(active_property, 'A')  --03/02/2016
    order by opn_code;
 --
  cursor get_val (p_opn_code in varchar2, p_prp_status in varchar2) is --03/02/2016
  select opn_code --ADD TO FRU LOC TABLE
           ,(
                select count(distinct location_code)
                from xxcus.xxcus_opn_properties_all
                where 1 =1
                and opn_code =p_opn_code
                and opn_type ='PROPERTY'
                and nvl(active_property, 'A') =p_prp_status --03/02/2016
                and location_type_lookup_code ='BUILDING'
                and hds_current_flag ='Y'
                --and trunc(sysdate) between loc_active_start_date and nvl(loc_active_end_date, trunc(sysdate))
                and
                  (
                    (p_prp_status ='A' and trunc(sysdate) between loc_active_start_date and nvl(loc_active_end_date, trunc(sysdate)))
                    OR
                    (p_prp_status !='A' and 2 =2)
                  )
            ) hds_bldg_count
           ,(
                select ROUND(sum(bl_ld_area_gross), 2)
                from xxcus.xxcus_opn_properties_all B
                where 1 =1
                and opn_code =p_opn_code
                and opn_type ='PROPERTY'
                and nvl(active_property, 'A') =p_prp_status --03/02/2016
                and location_type_lookup_code ='BUILDING'
                --and trunc(sysdate) between loc_active_start_date and nvl(loc_active_end_date, trunc(sysdate))
                and hds_current_flag ='Y'
                and
                  (
                    (p_prp_status ='A' and trunc(sysdate) between loc_active_start_date and nvl(loc_active_end_date, trunc(sysdate)))
                    OR
                    (p_prp_status !='A' and 2 =2)
                  )
            ) hds_active_bldg_total_sf_prp
            ,(
                select ROUND((sum(bl_ld_area_gross)/43560), 2)
                from xxcus.xxcus_opn_properties_all B
                where 1 =1
                and opn_code =p_opn_code
                and opn_type ='PROPERTY'
                and nvl(active_property, 'A') =p_prp_status --03/02/2016
                and location_type_lookup_code ='LAND'
                and hds_current_flag ='Y'
                --and trunc(sysdate) between loc_active_start_date and nvl(loc_active_end_date, trunc(sysdate))
                and
                  (
                    (p_prp_status ='A' and trunc(sysdate) between loc_active_start_date and nvl(loc_active_end_date, trunc(sysdate)))
                    OR
                    (p_prp_status !='A' and 2 =2)
                  )
             ) hds_active_acres_prp
           ,(
                select count(location_code)
                from xxcus.xxcus_opn_properties_all
                where 1 =1
                and opn_code =p_opn_code
                and opn_type ='PROPERTY'
                and nvl(active_property, 'A') =p_prp_status --03/02/2016
                and location_type_lookup_code ='SECTION'
                and hds_current_flag ='Y'
                --and trunc(sysdate) between loc_active_start_date and nvl(loc_active_end_date, trunc(sysdate))
                and
                  (
                    (p_prp_status ='A' and trunc(sysdate) between loc_active_start_date and nvl(loc_active_end_date, trunc(sysdate)))
                    OR
                    (p_prp_status !='A' and 2 =2)
                  )
            ) hds_yard_count
    from xxcus.xxcus_opn_properties_all A
    where 1 =1
    and opn_code =p_opn_code
    and opn_type ='PROPERTY'
    group by opn_code;
  --
 begin
  --
    for rec in all_prp loop
        --
        for rec_val in get_val (p_opn_code =>rec.opn_code, p_prp_status =>rec.prop_status) loop
         --
         begin
           savepoint sqr1;
           update xxcus.xxcus_opn_fru_loc_all
           set hds_bldg_count =rec_val.hds_bldg_count
                ,hds_active_bldg_total_sf_prp =rec_val.hds_active_bldg_total_sf_prp
                ,hds_active_acres_prp =rec_val.hds_active_acres_prp
                ,hds_yard_count =rec_val.hds_yard_count
          where 1 =1
           and opn_code =rec.opn_code;
         exception
          when others then
           dbms_output.put_line('Failed to update 4 fields for opn_code :'||rec.opn_code||', msg ='||sqlerrm);
           rollback to sqr1;
         end;
         --
        end loop;
        --
    end loop;
  --
  commit;
  --
 exception
   when others then
    rollback;
    fnd_file.put_line(fnd_file.log, '@fruloc_addnl_props_set1, outer block, msg :'||sqlerrm);
    dbms_output.put_line('@fruloc_addnl_props_set1, outer block, msg :'||sqlerrm);
    print_log('@fruloc_addnl_props_set1, outer block, msg :'||sqlerrm);
 end fruloc_addnl_props_set1;
--
/*
 *************************************************************************
 *
 * PROCEDURE: fruloc_addnl_props_set2
 * DESCRIPTION: To populate hds_pri_rer_type, hds_pri_rer_id and hds_pri_rer_expiry in xxcus_opn_fru_loc_all table
 * History
 * =======================================================================
 * ESMS         Date               Notes
 * ----------    -----------         ---------------------------------------
 * 318695     02/23/2016  Created. [TMS 20160302-00017]
 * =======================================================================
 * --
 * CALLED BY: HDS OPN: Extract Locations [Concurrent Program]
 ************************************************************************
 */
 procedure fruloc_addnl_props_set2 is
  --
 cursor all_prp is
    select opn_code
    from   xxcus.xxcus_opn_properties_all
    where 1 =1
    and opn_type ='PROPERTY'
    and hds_current_flag ='Y'
    group by opn_code
    order by opn_code;
  --
    cursor prp_loc (p_opn_code in varchar2) is
    select bldg_land_code loc_code
    from xxcus.xxcus_opn_fru_loc_all
    where 1 =1
    and opn_code =p_opn_code
    group by bldg_land_code;
    --
    l_hds_pri_rer_type    xxcus.xxcus_opn_rer_locations_all.lease_type%type :=Null;
    l_hds_pri_rer_id         xxcus.xxcus_opn_rer_locations_all.rer_id%type :=Null;
    l_hds_pri_rer_expiry xxcus.xxcus_opn_rer_locations_all.lease_termination%type :=Null;
    l_move_fwd                 BOOLEAN :=FALSE;
    --
 begin
  --
    for rec in all_prp loop
        --
        for rec_val in prp_loc (p_opn_code =>rec.opn_code) loop
         --
                 l_hds_pri_rer_type :=Null;
                 l_hds_pri_rer_id :=Null;
                 l_hds_pri_rer_expiry :=Null;
                 l_move_fwd :=FALSE;
         --
             begin
                select rer_id hds_pri_rer_id, lease_type,  lease_termination
                into l_hds_pri_rer_id ,l_hds_pri_rer_type ,l_hds_pri_rer_expiry
                from xxcus.xxcus_opn_rer_locations_all
                where 1 =1
                and tenancy_pri_flag ='Y'
                and loc_code =rec_val.loc_code;
               l_move_fwd :=TRUE;
             exception
              when no_data_found then
               dbms_output.put_line('Failed to fetch 3 fields for '||' opn_code :'||rec.opn_code||', loc_code :'||rec_val.loc_code||', msg 101 ='||sqlerrm);
               l_move_fwd :=FALSE;
              when too_many_rows then
               dbms_output.put_line('Failed to fetch 3 fields for '||' opn_code :'||rec.opn_code||', loc_code :'||rec_val.loc_code||', msg 102 ='||sqlerrm);
               l_move_fwd :=FALSE;
              when others then
               dbms_output.put_line('Failed to fetch 3 fields for '||' opn_code :'||rec.opn_code||', loc_code :'||rec_val.loc_code||', msg 103 ='||sqlerrm);
               l_move_fwd :=FALSE;
             end;
         --
              if (l_move_fwd) then
                --
                 begin
                   savepoint sqr1;
                   update xxcus.xxcus_opn_fru_loc_all
                   set hds_pri_rer_type   =l_hds_pri_rer_type
                        ,hds_pri_rer_id         =l_hds_pri_rer_id
                        ,hds_pri_rer_expiry =l_hds_pri_rer_expiry
                  where 1 =1
                   and opn_code =rec.opn_code
                   and bldg_land_code =rec_val.loc_code;
                 exception
                  when others then
                   dbms_output.put_line('Failed to update 3 fields for '||' opn_code :'||rec.opn_code||', loc_code :'||rec_val.loc_code||', msg ='||sqlerrm);
                   rollback to sqr1;
                 end;
                 --
              end if;
         --
        end loop;
        --
    end loop;
  --
  commit;
  --
 exception
   when others then
    rollback;
    fnd_file.put_line(fnd_file.log, '@fruloc_addnl_props_set2, outer block, msg :'||sqlerrm);
    dbms_output.put_line('@fruloc_addnl_props_set2, outer block, msg :'||sqlerrm);
    print_log('@fruloc_addnl_props_set2, outer block, msg :'||sqlerrm);
 end fruloc_addnl_props_set2;
 --
 /*
 *************************************************************************
 *
 * PROCEDURE: fruloc_addnl_props_set3
 * DESCRIPTION: To populate hds_active_bldg_sf_fru_prp in xxcus_opn_fru_loc_all table
 * History
 * =======================================================================
 * ESMS         Date               Notes
 * ----------    -----------         ---------------------------------------
 * 318695     02/23/2016  Created. [TMS 20160302-00017]
 * =======================================================================
 * --
 * CALLED BY: HDS OPN: Extract Locations [Concurrent Program]
 ************************************************************************
 */
 procedure fruloc_addnl_props_set3 is
  --
 cursor all_prp_fru is
    select hds_prop_fru, sum(area_allocated) active_bldg_sf_fru_prp
    from xxcus.xxcus_opn_fru_loc_all
    where 1=1
    and hds_fru_section_current_flag ='Y'
    and location_type NOT IN ('Yard')
    and HDS_OPS_STATUS='Active'
    group by hds_prop_fru;
  --
    l_hds_pri_rer_type    xxcus.xxcus_opn_rer_locations_all.lease_type%type :=Null;
    l_hds_pri_rer_id         xxcus.xxcus_opn_rer_locations_all.rer_id%type :=Null;
    l_hds_pri_rer_expiry xxcus.xxcus_opn_rer_locations_all.lease_termination%type :=Null;
    l_move_fwd                 BOOLEAN :=FALSE;
    --
 begin
  --
    for rec in all_prp_fru loop
        --
                --
                 begin
                   savepoint sqr1;
                   update xxcus.xxcus_opn_fru_loc_all
                   set hds_active_bldg_sf_fru_prp   =rec.active_bldg_sf_fru_prp
                  where 1 =1
                   and hds_prop_fru =rec.hds_prop_fru;
                 exception
                  when others then
                   fnd_file.put_line(fnd_file.log, 'Failed to update hds_active_bldg_sf_fru_prp field for '||' hds_prop_fru :'||rec.hds_prop_fru||', msg ='||sqlerrm);
                   rollback to sqr1;
                 end;
                 --
        --
    end loop;
  --
  commit;
  --
 exception
   when others then
    rollback;
    fnd_file.put_line(fnd_file.log, '@fruloc_addnl_props_set3, outer block, msg :'||sqlerrm);
    dbms_output.put_line('@fruloc_addnl_props_set3, outer block, msg :'||sqlerrm);
    print_log('@fruloc_addnl_props_set3, outer block, msg :'||sqlerrm);
 end fruloc_addnl_props_set3;
  -- End Ver 3.6
/*
 *************************************************************************
 *
 * FUNCTION: main
 * DESCRIPTION: To convert and populate the Oracle property manager locations hierarchical table as a denormalized table.
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * p_property_code   IN      HDS Property code
 * p_debug           IN     Need debug copied to the concurrent log [YES OR NO]
 * retcode           OUT    standard concurrent program return variable
 * errbuf            OUT    standard concurrent program return variable
 * RETURN VALUE: Mentioned above
 * History
 * =======================================================================
 * ESMS       Date        Version Notes
 * ---------- ----------- ------- ---------------------------------------
 * 253936     06/19/2014  1.0     Created.
 * 253936    07/18/2014   1.1    Multiple Items.
                                  a)  Modified main cursor prop_denorm_sql
 *                                   to bring all records for building/land and
                                     not just the earliest start dated record
 *                               b) Added columns min_loc_start_date and
 *                                 max_loc_end_date to cursor prop_denorm_sql
 *                              c) Fix routines Insert_Building_Land_Features
 *                                 and Insert_Building_Land_Contacts.
 * 253936    08/15/2014   1.2  Fix logic to update loc_bu_primary_ops_fru
 *                             and primary_fru_flag
 * 253936    08/15/2014   1.3  Fix logic to update primary_fru_flag by
 *                            using rowid after sorting by space tag ascending
 *                            and area_allocated desc
 * 253936    09/05/2014  1.4  Fix logic to current_flag, hds_bldg_land_min_start
 *                            , hds_bldg_land_max_end and hds_max_loc_start fields
 * 304798  11/18/2015 Update hds_prop_fru, hds_ops_status and hds_loc_bu_pri_ops fields.
 * TMS 20160112-00123 / ESMS 313314, 01/12/2016, Balaguru Seshadri , Version 3.4
                         Add new column HDS_PROP_CURRENT_FLAG VARCHAR2(1)
 * =======================================================================
 * --
 * CALLED BY: HDS OPN: Extract Locations [Concurrent Program]
 ************************************************************************
 */
 procedure main
   (
      retcode          out varchar2
     ,errbuf           out varchar2
     ,p_property_code  in  varchar2
     ,p_debug          in  varchar2 default 'N'
   ) as
  -- cursor all_properties
  cursor all_properties is
  select *
  from   pn_properties_v
  where  1 =1
    --and  property_code IN ('TN023', 'TX089'); --, 'BC004', 'FL300');
    and  property_code =nvl(p_property_code, property_code);
  --
  type prop_tbl is table of all_properties%rowtype index by binary_integer;
  prop_rec prop_tbl;
  -- cursor prop_denorm_sql
  cursor prop_denorm_sql
   (
     l_opn_id         in number
    ,l_opn_code       in varchar2
    ,l_opn_type       in varchar2
    ,l_loc_start_date in date
    ,l_loc_end_date   in date
    ,l_req_id         in number
    ,l_user_id        in number
    ,l_org_name       in varchar2
    ,l_prop_id        in number
   ) is
    select l_opn_id                            opn_id
          ,l_opn_code                          opn_code
          ,l_opn_type                          opn_type
          ,rownum                              display_seq
          ,level                               node_level
          ,dtl.location_type_lookup_code       location_type_lookup_code
          ,null                                location_type
          ,dtl.location_code                   location_code
          ,dtl.location_id                     location_id
          ,dtl.parent_location_id              parent_location_id
          /*
          ,case
            when (nvl(trunc(dtl.active_end_date), trunc(to_date('12/31/4712', 'mm/dd/yyyy'))) =to_date('12/31/4712', 'mm/dd/yyyy')) then 'Y'
            else 'N'
           end                                 current_flag
          */
          ,null                                hds_current_flag
          ,null                                hds_pri_loc_flag
          ,trunc(dtl.active_start_date)        loc_active_start_date
          /*
          ,case
            when (nvl(trunc(dtl.active_end_date), trunc(to_date('12/31/4712', 'mm/dd/yyyy'))) =to_date('12/31/4712', 'mm/dd/yyyy')) then to_date(null)
            else  trunc(dtl.active_end_date)
           end                                 loc_active_end_date
           */
          ,trunc(dtl.active_end_date)          loc_active_end_date
          ,null                                hds_bldg_land_min_start
          ,null                                hds_bldg_land_max_end
          ,null                                hds_max_loc_start
          -- property info begin
          ,null                                property_name
          ,null                                zone_name
          ,null                                district
          ,null                                country
          ,null                                description
          ,null                                portfolio
          ,null                                portfolio_name
          ,null                                tenure
          ,null                                property_tenancy
          ,null                                prop_class
          ,null                                prop_class_name
          ,null                                site_status
          ,null                                condition_name
          ,null                                office_park_name
          ,null                                region_name
          -- property info end
          -- location address begin
          ,null                                address_country
          ,null                                address_line1
          ,null                                address_line2
          ,null                                address_line3
          ,null                                city
          ,null                                state
          ,Null                                province
          ,null                                postal_code
          ,null                                county
          ,null                                hds_st_prv --New added on 10/06/2014
          -- location address end
          -- building/land generic attributes begin
          ,null                                bl_ld_lob_nickname --name
          ,null                                bl_ld_id
          ,null                                bl_ld_tenancy_code
          ,null                                bl_ld_tenancy
          ,null                                bl_ld_complexity_code
          ,null                                bl_ld_complexity
          -- building/land generic attributes end
          -- building/land AREA TAB begin
          ,null                                bl_ld_area_uom_code
          ,null                                bl_ld_area_uom
          ,null                                bl_ld_area_gross
          ,null                                bl_ld_area_rentable
          ,null                                bl_ld_area_usable
          ,null                                bl_ld_area_assignable
          ,null                                bl_ld_area_sft_vacant
          ,null                                bl_ld_area_percent_vacant
          ,null                                bl_ld_area_levels
          ,null                                bl_ld_area_units
          ,null                                bl_ld_area_load_factor
          -- building/land AREA TAB end
          -- building/land OCCUPANCY TAB begin
          ,null                                bl_ld_occu_status_code
          ,null                                bl_ld_occu_status
          ,null                                bl_ld_occu_emp_assignable
          ,null                                bl_ld_occu_dept_assignable
          ,null                                bl_ld_occu_cust_assignable
          ,null                                bl_ld_occu_disposition_code
          ,null                                bl_ld_occu_disposition
          ,null                                bl_ld_occu_acc_treatment_code
          ,null                                bl_ld_occu_acc_treatment
          ,null                                bl_ld_occu_maximum
          ,null                                bl_ld_occu_optimum
          ,null                                bl_ld_occu_utilized
          ,null                                bl_ld_occu_percent_max
          ,null                                bl_ld_occu_area_utilized
          ,null                                bl_ld_occu_max_vacancy
          -- building/land OCCUPANCY TAB end
          --floor/parcel generic attributes begin
          ,null                                fl_pr_name
          ,null                                fl_pr_id
          --floor/parcel generic attributes end
          -- floor/parcel AREA TAB begin
          ,null                                fl_pr_area_uom_code
          ,null                                fl_pr_area_uom
          ,null                                fl_pr_area_gross
          ,null                                fl_pr_area_rentable
          ,null                                fl_pr_area_usable
          ,null                                fl_pr_area_assignable
          ,null                                fl_pr_area_sft_vacant
          ,null                                fl_pr_area_percent_vacant
          ,null                                fl_pr_area_levels
          ,null                                fl_pr_area_units
          ,null                                fl_pr_area_load_factor
          -- floor/parcel AREA TAB end
          -- floor/parcel OCCUPANCY TAB begin
          ,null                                fl_pr_occu_status_code
          ,null                                fl_pr_occu_status
          ,null                                fl_pr_occu_emp_assignable
          ,null                                fl_pr_occu_dept_assignable
          ,null                                fl_pr_occu_cust_assignable
          ,null                                fl_pr_occu_disposition_code
          ,null                                fl_pr_occu_disposition
          ,null                                fl_pr_occu_acc_treatment_code
          ,null                                fl_pr_occu_acc_treatment
          ,null                                fl_pr_occu_maximum
          ,null                                fl_pr_occu_optimum
          ,null                                fl_pr_occu_utilized
          ,null                                fl_pr_occu_area_utilized
          ,null                                fl_pr_occu_max_vacancy
          ,null                                fl_pr_vacant_area
          ,null                                fl_pr_common
          ,null                                fl_pr_primary_circulation
          ,null                                fl_pr_primary_ops_type_code
          ,null                                fl_pr_primary_ops_type
          ,null                                fl_pr_function_ops_type_code
          ,null                                fl_pr_function_type
          ,null                                fl_pr_standard_type_type_code
          ,null                                fl_pr_standard_type
          -- floor/parcel OCCUPANCY TAB end
          -- office/yard generic details begin
          ,null                                sc_yd_name
          ,null                                sc_yd_id
          -- office/yard generic details end
          -- office/yard AREA TAB begin
          ,null                                sc_yd_area_uom_code
          ,null                                sc_yd_area_uom
          ,null                                sc_yd_area_rentable
          ,null                                sc_yd_area_usable
          ,null                                sc_yd_area_assignable
          ,null                                sc_yd_area_sft_vacant
          -- office/yard AREA TAB end
          -- office/yard OCCUPANCY TAB begin
          ,null                                sc_yd_occu_status_code
          ,null                                sc_yd_occu_status
          ,null                                sc_yd_occu_emp_assignable
          ,null                                sc_yd_occu_dept_assignable
          ,null                                sc_yd_occu_cust_assignable
          ,null                                sc_yd_occu_disposition_code
          ,null                                sc_yd_occu_disposition
          ,null                                sc_yd_occu_acc_treatment_code
          ,null                                sc_yd_occu_acc_treatment
          ,null                                sc_yd_occu_maximum
          ,null                                sc_yd_occu_optimum
          ,null                                sc_yd_occu_utilized
          ,null                                sc_yd_occu_area_utilized
          ,null                                sc_yd_occu_max_vacancy
          ,null                                sc_yd_vacant_area
          ,null                                sc_yd_bookable_flag
          ,null                                sc_yd_common_area_flag
          ,null                                sc_yd_common
          ,null                                sc_yd_primary_circulation
          ,null                                sc_yd_secondary_circulation
          ,Null                                sc_yd_space_type_code
          ,Null                                sc_yd_space_type
          ,Null                                sc_yd_site_type_code
          ,Null                                sc_yd_site_type
          ,Null                                sc_yd_assignment_type_code
          ,Null                                sc_yd_assignment_type
          -- office/yard OCCUPANCY TAB end
          --PROPERTY DFF'S begin
          ,null                                prop_attribute_category
          ,null                                prop_metro_area
          ,null                                prop_currency
          ,null                                prop_primary_language
          ,null                                prop_primary_building_code
          ,null                                prop_market_cluster
          ,null                                prop_attribute6
          ,null                                prop_attribute7
          ,null                                prop_attribute8
          ,null                                prop_attribute9
          ,null                                prop_attribute10
          ,null                                prop_attribute11
          ,null                                prop_attribute12
          ,null                                prop_attribute13
          ,null                                prop_attribute14
          ,null                                prop_attribute15
          --PROPERTY DFF'S end
          --BUILDING DFF'S begin
          ,null                                bldg_attribute_category
          ,null                                bldg_longitude
          ,null                                bldg_latitude
          ,null                                bldg_attribute3
          ,null                                bldg_attribute4
          ,null                                bldg_attribute5
          ,null                                bldg_documentum_link
          ,null                                bldg_photo_link
          ,null                                bldg_attribute8
          ,null                                bldg_attribute9
          ,null                                bldg_attribute10
          ,null                                bldg_attribute11
          ,null                                bldg_attribute12
          ,null                                bldg_attribute13
          ,null                                bldg_attribute14
          ,null                                bldg_attribute15
          --BUILDING DFF'S end
          --FLOOR DFF'S begin
          ,null                                floor_attribute_category
          ,null                                floor_attribute1
          ,null                                floor_attribute2
          ,null                                floor_attribute3
          ,null                                floor_attribute4
          ,null                                floor_attribute5
          ,null                                floor_attribute6
          ,null                                floor_attribute7
          ,null                                floor_attribute8
          ,null                                floor_attribute9
          ,null                                floor_attribute10
          ,null                                floor_attribute11
          ,null                                floor_attribute12
          ,null                                floor_attribute13
          ,null                                floor_attribute14
          ,null                                floor_attribute15
          --FLOOR DFF'S end
          --OFFICE DFF'S begin
          ,null                                office_attribute_category
          ,null                                office_attribute1
          ,null                                office_attribute2
          ,null                                office_operating_hours
          ,null                                office_walkin
          ,null                                office_customer_site
          ,null                                office_attribute6
          ,null                                office_attribute7
          ,null                                office_attribute8
          ,null                                office_attribute9
          ,null                                office_attribute10
          ,null                                office_attribute11
          ,null                                office_attribute12
          ,null                                office_attribute13
          ,null                                office_attribute14
          ,null                                office_attribute15
          --OFFICE DFF'S end
          --LAND DFF'S begin
          ,null                                land_attribute_category
          ,null                                land_longitude
          ,null                                land_latitude
          ,null                                land_attribute3
          ,null                                land_attribute4
          ,null                                land_attribute5
          ,null                                land_attribute6
          ,null                                land_attribute7
          ,null                                land_insignificant
          ,null                                land_attribute9
          ,null                                land_attribute10
          ,null                                land_attribute11
          ,null                                land_attribute12
          ,null                                land_attribute13
          ,null                                land_attribute14
          ,null                                land_attribute15
          --LAND DFF'S end
          --PARCEL DFF'S begin
          ,null                                parcel_attribute_category
          ,null                                parcel_parcel
          ,null                                parcel_attribute2
          ,null                                parcel_attribute3
          ,null                                parcel_attribute4
          ,null                                parcel_attribute5
          ,null                                parcel_attribute6
          ,null                                parcel_attribute7
          ,null                                parcel_attribute8
          ,null                                parcel_tax_assessor
          ,null                                parcel_attribute10
          ,null                                parcel_attribute11
          ,null                                parcel_attribute12
          ,null                                parcel_attribute13
          ,null                                parcel_attribute14
          ,null                                parcel_attribute15
          --PARCEL DFF'S end
          --SECTION DFF'S begin
          ,null                                section_attribute_category
          ,null                                section_attribute1
          ,null                                section_attribute2
          ,null                                section_operating_hours
          ,null                                section_walkin
          ,null                                section_customer_site
          ,null                                section_attribute6
          ,null                                section_attribute7
          ,null                                section_attribute8
          ,null                                section_attribute9
          ,null                                section_attribute10
          ,null                                section_attribute11
          ,null                                section_attribute12
          ,null                                section_attribute13
          ,null                                section_attribute14
          ,null                                section_attribute15
          --SECTION DFF'S end
          ,dtl.org_id                          org_id
          ,l_org_name                          org_name
          ,l_req_id                            request_id
          ,sysdate                             extract_date
          /*
          ,case
            when (dtl.location_type_lookup_code IN ('BUILDING', 'LAND') and trunc(dtl.active_start_date) >trunc(sysdate)) then 'Future'
            when (dtl.location_type_lookup_code IN ('BUILDING', 'LAND') and trunc(sysdate) between trunc(dtl.active_start_date) and nvl(trunc(dtl.active_end_date), trunc(sysdate))) then 'Active'
            when (dtl.location_type_lookup_code IN ('BUILDING', 'LAND') and trunc(dtl.active_end_date) <trunc(sysdate)) then 'Complete' --Replaced text Inactive to Complete on 10/06/14
            else Null
           end                                 hds_bldg_land_status
          */
          ,Null                                hds_bldg_land_status
          ,Null                                address_id
          ,dtl.space_type_lookup_code          space_type_lookup_code
          ,Null                                space_type_tag --New added on 10/06/2014
          ,Null                                cbsa
          ,Null                                cbsa_name
          ,Null                                msa
          ,Null                                msa_name
          ,Null                                bl_ld_area_gross_acreage
          ,Null                                space_type --New added on 10/06/2014
          ,Null                                active_property --New added on 01/05/2015 , ESMS: 274232
          ,dtl.suite                           loc_suite_name --New added on 01/05/2015, ESMS: 274232
          ,case
            when l_opn_type ='PROPERTY' then l_opn_id
            else l_prop_id
           end                                 property_id
          ,Null                                country
          ,Null                                hds_prop_current_flag --Ver 3.4
    from   (
               select a.*
               from   pn_locations_all a
               where  1 =1
                 --and  a.location_type_lookup_code NOT IN ('BUILDING', 'LAND')
           ) dtl
    where  1 =1
      and  (
               (
                     l_opn_type <>'PROPERTY'
                 and active_start_date >=l_loc_start_date
                 and active_end_date   <=l_loc_end_date
               )
            OR
               (l_opn_type ='PROPERTY' and 2 =2)
           )
    connect by prior dtl.location_id =dtl.parent_location_id
    start with dtl.location_id in
     (
      select location_id
      from   pn_locations_all
      where  1 =1
        and
         case
          when l_opn_type ='PROPERTY' then property_id
          else location_id
         end =l_opn_id
     )
    order siblings by dtl.location_code;
  --
  type prop_denorm_tbl is table of xxcus.xxcus_opn_properties_all%rowtype index by binary_integer;
  prop_denorm_rows  prop_denorm_tbl;
  child_denorm_rows prop_denorm_tbl;
  -- cursor get_multiple_props
  cursor get_multiple_props (l_opn_id in number) is
    select location_id, location_code, location_type_lookup_code loc_type_code
    from   pn_locations_all dtl
    where  1 =1
    connect by prior dtl.parent_location_id =dtl.location_id
    start with dtl.location_id in
     (
      select location_id
      from   pn_locations_all
      where  1 =1
        and  location_id =l_opn_id
        and  active_end_date >trunc(sysdate)
     );
  --
  type get_multiple_props_tbl is table of get_multiple_props%rowtype index by binary_integer;
  props_rec get_multiple_props_tbl;
  -- cursor get_location_attributes
   cursor get_location_attributes (p_locn_id in number, p_loc_type in varchar2, p_date in date) is
   select
     p_loc_type attribute_category
    ,attribute1
    ,attribute2
    ,attribute3
    ,attribute4
    ,attribute5
    ,attribute6
    ,attribute7
    ,attribute8
    ,attribute9
    ,attribute10
    ,attribute11
    ,attribute12
    ,attribute13
    ,attribute14
    ,attribute15
   from pn_locations_all
   where 1 =1
     and location_id =p_locn_id
     --and active_end_date >trunc(sysdate)
     and active_start_date =p_date;
  -- cursor c_update_opn_dates
  cursor c_update_opn_dates is
    select opn_id, location_code, location_type_lookup_code loc_type, location_id
    from   xxcus.xxcus_opn_properties_all
    where  1 =1
      and  opn_type ='PROPERTY'
      and  location_type_lookup_code IN ('BUILDING', 'LAND')
    group by opn_id, location_code, location_type_lookup_code, location_id
    order by
      opn_id asc
     ,case
              when location_type_lookup_code='BUILDING' then 1
              when location_type_lookup_code='LAND' then 2
              else 3
      end asc, location_code asc;
  --
  v_update_opn_dates c_update_opn_dates%rowtype;
  --
  -- cursor c_update_get_loc [ get all floor/office for a building OR parcel/yard rows for a land]
        cursor c_update_get_loc (p_opn_id in number, p_loc_id in number) is
        select tbl.*
        from
        (
            select *
            from
            (
                select location_id  ,location_type_lookup_code -- unique floor rows for a building
                from   xxcus.xxcus_opn_properties_all
                where  1 =1
                  and  opn_id                     =p_opn_id
                  and  opn_type                   ='PROPERTY'
                  and  location_type_lookup_code  ='FLOOR'
                  and  parent_location_id         =p_loc_id
                group by location_id, location_type_lookup_code
                union
                select location_id ,location_type_lookup_code -- unique office rows for a floor
                from   xxcus.xxcus_opn_properties_all
                where  1 =1
                  and  opn_id                     =p_opn_id
                  and  opn_type                   ='PROPERTY'
                  and  location_type_lookup_code  ='OFFICE'
                  and  parent_location_id IN
                       (
                        select location_id
                        from   xxcus.xxcus_opn_properties_all
                        where  1 =1
                          and  opn_id                     =p_opn_id
                          and  opn_type                   ='PROPERTY'
                          and  location_type_lookup_code  ='FLOOR'
                          and  parent_location_id         =p_loc_id
                        group by location_id
                       )
                group by location_id, location_type_lookup_code
            )
            union
            select *
            from
            (
                select location_id, location_type_lookup_code  -- unique parcel rows for a land
                from   xxcus.xxcus_opn_properties_all
                where  1 =1
                  and  opn_id                     =p_opn_id
                  and  opn_type                   ='PROPERTY'
                  and  location_type_lookup_code  ='PARCEL'
                  and  parent_location_id         =p_loc_id
                group by location_id, location_type_lookup_code
                union
                select location_id, location_type_lookup_code -- unique yard rows for a parcel
                from   xxcus.xxcus_opn_properties_all
                where  1 =1
                  and  opn_id                     =p_opn_id
                  and  opn_type                   ='PROPERTY'
                  and  location_type_lookup_code  ='SECTION'
                  and  parent_location_id IN
                       (
                        select location_id
                        from   xxcus.xxcus_opn_properties_all
                        where  1 =1
                          and  opn_id                     =p_opn_id
                          and  opn_type                   ='PROPERTY'
                          and  location_type_lookup_code  ='PARCEL'
                          and  parent_location_id         =p_loc_id
                        group by location_id
                       )
                group by location_id, location_type_lookup_code
            )
        ) tbl;
    -- cursor c_min_max_dates
        cursor c_min_max_dates (p_loc_id in number) is
        select min(trunc(active_start_date))  bldg_land_min_start
              ,max(trunc(active_end_date))    bldg_land_max_end
              ,max(trunc(active_start_date))  bldg_land_max_start
        from   pn_locations_all
        where  1 =1
          and  location_id =p_loc_id;
    --
        v_min_max_dates c_min_max_dates%rowtype;
    --
    -- cursor c_loc_max_start
        cursor c_loc_max_start (p_loc_id in number) is
        select max(trunc(active_start_date)) max_loc_start_date
        from   pn_locations_all
        where  1 =1
          and  location_id =p_loc_id;
    --
        v_loc_max_start c_loc_max_start%rowtype;
    --
        cursor c_get_loc_ids (p_opn_id in number, p_loc_type in varchar2) is
        select listagg(t.loc_id, ',') within group(order by loc_id)
        from
        (
        select location_id loc_id
        from   xxcus.xxcus_opn_properties_all
        where  1 =1
          and  opn_type =p_loc_type
          and  opn_id   =p_opn_id
        group by location_id
        ) t;
    -- cursor get_primary_fru
        cursor get_primary_fru (p_hds_loc_lob_abb in varchar2) is
        select hds_loc_lob_abb             hds_loc_lob_abb
              ,hds_loc_bu_pri_ops          hds_loc_bu_pri_ops
              ,rowid                       prim_fru_flag_rowid
              ,space_tag                   space_tag
              ,lc_fru                      loc_bu_pri_ops_fru
              ,emp_assign_start_date       emp_assign_start
              ,(
                  select max(emp_assign_start_date)
                  from   xxcus.xxcus_opn_fru_loc_all
                  where  1 =1
                    and  hds_loc_lob_abb =p_hds_loc_lob_abb
                    --and  space_type_lookup_code !='LAWSONADMIN' --Commented on 10/22/2014
               )                           emp_asg_max_start
        from   xxcus.xxcus_opn_fru_loc_all
        where  1 =1
          and  hds_loc_lob_abb        =p_hds_loc_lob_abb
          and  hds_fru_section_current_flag ='Y' --added 10/29/2014
          --and  space_type_lookup_code !='LAWSONADMIN' --Commented on 10/22/2014
        order by
             hds_ops_status asc
            ,space_tag asc
            ,primary_sub_bu_rank asc
            ,area_allocated desc
            ,emp_assign_start_date desc
            ,loc_active_start_date asc
            ,rowid asc;
--             hds_ops_status asc
--            ,space_tag asc
--            ,loc_active_start_date desc
--            ,emp_assign_start_date desc
--            ,area_allocated desc
--            ,rowid asc;
        --
        type get_primary_fru_tbl is table of get_primary_fru%rowtype index by binary_integer;
        get_primary_fru_rec get_primary_fru_tbl;
    --
    cursor all_fru_sections is
        select a.opn_id                                         opn_id
              ,a.hds_fru_section                                hds_fru_section
              ,max(trunc(nvl(emp_assign_start_date, sysdate)))  max_emp_asg_start
              ,max(trunc(nvl(emp_assign_end_date, sysdate)))    max_emp_asg_end
        from   xxcus.xxcus_opn_fru_loc_all a
        where  1 =1
        and  a.opn_type       ='PROPERTY'
        --and  a.location_code  ='AB007.1.1'
        group by
         a.opn_id
        ,a.hds_fru_section;
    --
    cursor all_space_asg_locs is
        select a.opn_id
              ,a.location_id
              ,a.LOC_TYPE_LOOKUP_CODE
              ,(
                 select hds_current_flag
                 from   xxcus.xxcus_opn_properties_all
                 where  1 =1
                   and  opn_id                =a.opn_id
                   and  opn_type              ='PROPERTY'
                   and  location_id           =a.location_id
                   and  loc_active_start_date =a.loc_active_start_date
                   and  loc_active_end_date   =a.loc_active_end_date
               ) hds_current_flag
              ,a.loc_active_start_date
              ,a.loc_active_end_date
              ,(
                 select max(trunc(nvl(emp_assign_start_date, sysdate)))
                 from   pn_space_assign_emp_v
                 where  1 =1
                   and  emp_space_assign_id is not null
                   and  location_location_id      =a.location_id
                   and  active_start_date         =a.loc_active_start_date
                   and  active_end_date           =a.loc_active_end_date
                   and  location_type_lookup_code =a.LOC_TYPE_LOOKUP_CODE
               ) max_emp_asg_start
             ,(
                 select max(trunc(nvl(emp_assign_end_date, sysdate)))
                 from   pn_space_assign_emp_v
                 where  1 =1
                   and  emp_space_assign_id is not null
                   and  location_location_id      =a.location_id
                   and  active_start_date         =a.loc_active_start_date
                   and  active_end_date           =a.loc_active_end_date
                   and  location_type_lookup_code =a.LOC_TYPE_LOOKUP_CODE
               ) max_emp_asg_end
              ,nvl(emp_assign_end_date, trunc(sysdate)) emp_asg_end
        from   xxcus.xxcus_opn_fru_loc_all a
        where  1 =1
          and  a.opn_type ='PROPERTY'
          --and  a.location_code ='AB007.1.1'
          and  a.LOC_TYPE_LOOKUP_CODE IN ('OFFICE', 'SECTION');
  --

  --
  attributes_rec       get_location_attributes%rowtype :=Null;
  --
  building_attributes  get_location_attributes%rowtype :=Null;
  --
  floor_attributes     get_location_attributes%rowtype :=Null;
  --
  office_attributes    get_location_attributes%rowtype :=Null;
  --
  land_attributes      get_location_attributes%rowtype :=Null;
  --
  parcel_attributes    get_location_attributes%rowtype :=Null;
  --
  section_attributes   get_location_attributes%rowtype :=Null;
  --
  n_prop_idx number :=0;
  --
  p_limit    Number :=5000;
  n_req_id   Number :=fnd_global.conc_request_id;
  n_user_id  Number :=fnd_global.user_id;
  --
  ex_dml_errors      exception;
  PRAGMA EXCEPTION_INIT(ex_dml_errors, -24381);
  l_error_count Number :=0;
  v_org_name hr_operating_units.name%type;
  --
  v_exec boolean;
  n_max_level number :=0;
  --
  v_primary_phone_num   xxcus.xxcus_opn_fru_loc_all.primary_phone_number%type :=Null;
  v_fax_number   xxcus.xxcus_opn_fru_loc_all.primary_phone_number%type :=Null; --Ver 3.4
  v_loc_bu_pri_ops      xxcus.xxcus_opn_fru_loc_all.hds_loc_bu_pri_ops%type :=Null;
  v_prop_bu_pri_ops     xxcus.xxcus_opn_fru_loc_all.HDS_LOC_BU_PRI_OPS_FRU%type :=Null;
  --
  l_module        VARCHAR2(24) :='OPN';
  l_err_msg       CLOB;
  l_sec           VARCHAR2(255);
  --
  l_err_callfrom  VARCHAR2(75) DEFAULT 'apps.xxcus_opn_master_extract.main';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  --
  n_bldg_land_id    pn_locations_all.location_id%type;
  v_bldg_land_code  pn_locations_all.location_code%type;
  v_bldg_land_type  pn_locations_all.location_type_lookup_code%type;
  --
  v_prop_rec pn_properties_v%rowtype;
  --
 begin
  --
  print_log('calling main...');
  print_log('p_property_code ='||p_property_code);
  print_log('p_debug         ='||p_debug);
  --
  -- Assign the debug parameter to the package variable to trigger on /off the debug statements
  xxcus_opn_master_extract.g_debug :=p_debug;
  --
  print_log('');
  begin
   --
   EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUS_OPN_PROPERTIES_ALL';
   --
   DELETE XXCUS.XXCUS_HDS_PN_LOOKUP_OBJECTS WHERE LOOKUP_TYPE IN ('PN_LEASE_ROLE_TYPE', 'PN_LOCATION_FEATURE_TYPE');
   --
   print_log('Old data removed from the table XXCUS.XXCUS_OPN_PROPERTIES_ALL.');
   print_log('Old data removed from the table XXCUS.XXCUS_HDS_PN_LOOKUP_OBJECTS for lookup types PN_LEASE_ROLE_TYPE and PN_LOCATION_FEATURE_TYPE.');
   --
  exception
   --
   when others then
    print_log('Issue in truncating table xxcus.xxcus_opn_properties_all, message ='||sqlerrm);
   --
  end;
  --
  begin
   print_log('mo_global.get_current_org_id ='||mo_global.get_current_org_id);
   select name
   into   v_org_name
   from   hr_operating_units
   where  1 =1
     and  organization_id =nvl(mo_global.get_current_org_id, 163);
   MO_GLOBAL.SET_POLICY_CONTEXT('S', 163);
   --
   print_log('Operating unit name ='||v_org_name);
  exception
   when others then
    print_log('Error in fetching operating unit description, message ='||sqlerrm);
    v_org_name :=Null;
  end;
  --
  v_exec :=create_loc_feature_tables ('XXCUS.HDS_PN_LFT_');
  --
  if (v_exec) then
   --
   print_log ('Location: create_loc_feature_tables returned TRUE, Message: Success');
   --
   v_exec :=create_loc_contact_tables ('XXCUS.HDS_PN_LRT_');
   --
  --
   if (v_exec) then
   --
   print_log ('Location: create_loc_contact_tables returned TRUE, Message: Success');
   --
      open all_properties;
      --
      fetch all_properties bulk collect into prop_rec limit p_limit;
      --
      close all_properties;
      --
       if prop_rec.count >0 then
        --
         for idx_p in prop_rec.first .. prop_rec.last loop
          -- Stage1 begin
          begin
           --
           open prop_denorm_sql
             (
               l_opn_id         =>prop_rec(idx_p).property_id
              ,l_opn_code       =>prop_rec(idx_p).property_code
              ,l_opn_type       =>'PROPERTY'
              ,l_loc_start_date =>Null
              ,l_loc_end_date   =>Null
              ,l_req_id         =>n_req_id
              ,l_user_id        =>n_user_id
              ,l_org_name       =>v_org_name
              ,l_prop_id        =>Null --Because we already have the property id passed into the variable l_opn_id
             );
           --
           fetch prop_denorm_sql bulk collect into prop_denorm_rows;
           --
           close prop_denorm_sql;
           --
           if prop_denorm_rows.count >0 then
            -- start: assign property attributes
             begin
              for i in 1 .. prop_denorm_rows.count loop
               --
                prop_denorm_rows(i).property_name           :=prop_rec(idx_p).property_name;
                prop_denorm_rows(i).zone_name               :=prop_rec(idx_p).zone_name;
                prop_denorm_rows(i).district                :=prop_rec(idx_p).district;
                prop_denorm_rows(i).country                 :=prop_rec(idx_p).territory_short_name; --prop_rec(idx_p).country;
                prop_denorm_rows(i).description             :=prop_rec(idx_p).description;
                prop_denorm_rows(i).portfolio               :=prop_rec(idx_p).portfolio;
                prop_denorm_rows(i).portfolio_name          :=prop_rec(idx_p).portfolio_name;
                prop_denorm_rows(i).tenure                  :=prop_rec(idx_p).tenure;
                prop_denorm_rows(i).property_tenancy        :=prop_rec(idx_p).tenure_name;
                prop_denorm_rows(i).prop_class              :=prop_rec(idx_p).class;
                prop_denorm_rows(i).prop_class_name         :=prop_rec(idx_p).class_name;
                prop_denorm_rows(i).site_status             :=prop_rec(idx_p).property_status_name;
                prop_denorm_rows(i).condition_name          :=prop_rec(idx_p).condition_name;
                prop_denorm_rows(i).office_park_name        :=prop_rec(idx_p).office_park_name;
                prop_denorm_rows(i).region_name             :=prop_rec(idx_p).region_name;
                prop_denorm_rows(i).active_property         :=prop_rec(idx_p).active_property;
                -- PROPERTY DFF assignment begin
                prop_denorm_rows(i).prop_attribute_category :=prop_rec(idx_p).attribute_category;
                prop_denorm_rows(i).prop_metro_area            :=prop_rec(idx_p).attribute1;
                prop_denorm_rows(i).prop_currency              :=prop_rec(idx_p).attribute2;
                prop_denorm_rows(i).prop_primary_language      :=prop_rec(idx_p).attribute3;
                prop_denorm_rows(i).prop_primary_building_code :=prop_rec(idx_p).attribute4;
                prop_denorm_rows(i).prop_market_cluster        :=prop_rec(idx_p).attribute5;
                prop_denorm_rows(i).prop_attribute6            :=prop_rec(idx_p).attribute6;
                prop_denorm_rows(i).prop_attribute7            :=prop_rec(idx_p).attribute7;
                prop_denorm_rows(i).prop_attribute8            :=prop_rec(idx_p).attribute8;
                prop_denorm_rows(i).prop_attribute9            :=prop_rec(idx_p).attribute9;
                prop_denorm_rows(i).prop_attribute10           :=prop_rec(idx_p).attribute10;
                prop_denorm_rows(i).prop_attribute11           :=prop_rec(idx_p).attribute11;
                prop_denorm_rows(i).prop_attribute12           :=prop_rec(idx_p).attribute12;
                prop_denorm_rows(i).prop_attribute13           :=prop_rec(idx_p).attribute13;
                prop_denorm_rows(i).prop_attribute14           :=prop_rec(idx_p).attribute14;
                prop_denorm_rows(i).prop_attribute15           :=prop_rec(idx_p).attribute15;
                -- PROPERTY DFF assignment begin
                prop_denorm_rows(i).org_name                   :=v_org_name;
                -- get_dff(p_opn_type =>'PROPERTY', p_attribute_id =>1, p_value =>prop_rec(idx_p).attribute1);
              end loop;
             exception
              when others then
                print_log('Issue in assing of property attributes for property_code ='||prop_rec(idx_p).property_code);
                null;
             end;
            -- end: assign property attributes
             begin
              forall idx in prop_denorm_rows.first .. prop_denorm_rows.last --save exceptions
                insert into  xxcus.xxcus_opn_properties_all values prop_denorm_rows (idx);
                print_log('Successfully bulk inserted denorm data for property_id ='||prop_rec(idx_p).property_id);
                print_log('');
             exception
              when ex_dml_errors then
               l_error_count := SQL%BULK_EXCEPTIONS.count;
               print_log ('@ Property_Id ='||prop_rec(idx_p).property_id||', number of errors ='||l_error_count);
                  for i in 1 .. l_error_count loop
                    print_log ('Error: ' || i ||
                      ' Error Index: ' || sql%bulk_exceptions(i).error_index ||
                      ' Message: ' || sqlerrm(-sql%bulk_exceptions(i).error_code));
                  end loop;
               when others then
                 print_log('Issue in bulk insert @ property_id '||prop_rec(idx_p).property_id||', message ='||sqlerrm);
             end;
           else
            --
            print_log(''||prop_denorm_rows.count);
            --
           end if;
           --
          exception
           --
           when others then
            --
             print_log('Error in fetching property denormalized data for property_id ='||prop_rec(idx_p).property_id);
            --
          end;
          -- Stage1 end
          --
         end loop; --for idx_p in prop_rec.first .. prop_rec.last loop
        --
       else
        --
         print_log('No properties to process, count ='||prop_rec.count);
        --
       end if; --prop_rec.count >0 then
      --
      begin
       --
       print_log('Before calling setup_indexes');
       --
       setup_indexes;
       --
       print_log('After calling setup_indexes');
       --
      exception
       when others then
        print_log('@setup_indexes'||sqlerrm);
      end;
     --
     --Added 07/21/2014 remove duplicate rows
      begin
       savepoint remove_duplicate_rows;
        delete xxcus.xxcus_opn_properties_all a
        where  1 =1
          and rowid >any
                      (
                        select b.rowid
                        from   xxcus.xxcus_opn_properties_all b
                        where  1 =1
                          and  a.opn_id                     =b.opn_id
                          and  a.opn_type                   =b.opn_type
                          and  a.opn_code                   =b.opn_code
                          and  a.location_type_lookup_code  =b.location_type_lookup_code
                          and  a.location_id                =b.location_id
                          and  a.location_code              =b.location_code
                          and  a.loc_active_start_date      =b.loc_active_start_date
                          and  a.loc_active_end_date        =b.loc_active_end_date
                      );
        print_log('@removed duplicate rows, total records ='||sql%rowcount);
      exception
       when others then
        print_log('@remove_duplicate_rows, msg ='||sqlerrm);
        rollback to remove_duplicate_rows;
      end;
      --Added 07/21/2014
      -- update current flag, min and max loc dates
      begin
       savepoint start_here;
       open c_update_opn_dates;
       loop
        --
        fetch c_update_opn_dates into v_update_opn_dates;
        --
        exit when c_update_opn_dates%notfound;
        --
        -- get building or land level min_loc_start and max loc end dates
        --
        begin
         --
         open c_min_max_dates (p_loc_id =>v_update_opn_dates.location_id);
         fetch c_min_max_dates into v_min_max_dates;
         close c_min_max_dates;
         --
        exception
         when others then
          v_min_max_dates :=Null;
        end;
        --
        begin
          -- this update will take care of all bldg / land records where opn_type =PROPERTY
          -- and update them with min/max loc active start and max loc end date
          savepoint start_rec2;
          update xxcus.xxcus_opn_properties_all
           set hds_bldg_land_min_start =v_min_max_dates.bldg_land_min_start
              ,hds_bldg_land_max_end   =v_min_max_dates.bldg_land_max_end
              ,hds_max_loc_start       =v_min_max_dates.bldg_land_max_start
              ,hds_current_flag =case when loc_active_start_date >=v_min_max_dates.bldg_land_max_start then 'Y' else 'N' end
           where 1 =1
             and opn_id       =v_update_opn_dates.opn_id
             and opn_type     ='PROPERTY'
             and location_id  =v_update_opn_dates.location_id;
           print_log('@start_rec2 update, location_id ='||v_update_opn_dates.location_id||',Msg: '||sqlerrm);
           commit;
        exception
         when others then
          print_log('@start_rec2, msg ='||sqlerrm);
          rollback to start_rec2;
        end;
        --
        -- loop thru floor/office for a building or parcel/yard for a land and update date fields where opn_type ='PROPERTY'
        --
        for rec1 in  c_update_get_loc (p_opn_id =>v_update_opn_dates.opn_id, p_loc_id =>v_update_opn_dates.location_id) loop
         --
         begin
          --
          -- get max location start date for all locations where location_type_lookup_code is FLOOR/OFFICE/PARCLE/SECTION
          --
          begin
             --
             open c_loc_max_start (p_loc_id =>rec1.location_id);
             fetch c_loc_max_start into v_loc_max_start;
             close c_loc_max_start;
             --
          exception
           when others then
              v_loc_max_start :=Null;
          end;
          --
          begin
              -- this update will take care of all bldg / land records where opn_type =PROPERTY
              -- and update them with min/max loc active start and max end date
              savepoint start_rec3;
              update xxcus.xxcus_opn_properties_all
               set hds_bldg_land_min_start =v_min_max_dates.bldg_land_min_start
                  ,hds_bldg_land_max_end   =v_min_max_dates.bldg_land_max_end
                  ,hds_max_loc_start       =v_loc_max_start.max_loc_start_date
                  ,hds_current_flag =case when loc_active_start_date >=v_loc_max_start.max_loc_start_date then 'Y' else 'N' end
               where 1 =1
                 and opn_id                     =v_update_opn_dates.opn_id
                 and opn_type                   ='PROPERTY'
                 and location_id                =rec1.location_id
                 and location_type_lookup_code  =rec1.location_type_lookup_code;
               print_log('@start_rec3 update, msg: '||sqlerrm);
               commit;
          exception
           when others then
            print_log('@start_rec3, msg ='||sqlerrm);
            rollback to start_rec3;
          end;           --
          --
         exception
          when others then
           print_log('Within loop of cursor c_update_get_loc, opn_id ='||v_update_opn_dates.opn_id
                                                    ||', location_id ='||v_update_opn_dates.location_id
                                                    ||', msg ='||sqlerrm
                    );
         end;
         --
        end loop; --end loop for cursor c_update_get_loc
        --
       end loop; --end loop for cursor c_update_opn_dates
       close c_update_opn_dates;
      exception
       when others then
        print_log ('@Outer block of c_update_opn_dates, msg ='||sqlerrm);
        rollback to start_here;
      end;
      --
      -- begin children denormalization logic 07/23/2014
      -- Stage2 begin
      -- Loop thru the hierarchical tree to create a denormalized table...
      --
      for i in prop_rec.first .. prop_rec.last loop
       prop_denorm_rows.delete;
       begin
        select *
        bulk collect into prop_denorm_rows
        from   xxcus.xxcus_opn_properties_all
        where  1 =1
          and  opn_type ='PROPERTY'
          and  opn_id   =prop_rec(i).property_id
        order by node_level asc;
        print_log('prop_denorm_rows.count ='||prop_denorm_rows.count);
       exception
        when others then
         print_log('Failed to retrieve property and its children rows for further denormalization, message ='||sqlerrm);
       end;
       --
       begin
        --
        if prop_denorm_rows.count >0 then
         --
         for idx in prop_denorm_rows.first .. prop_denorm_rows.last loop
             -- =======================
             -- traverse the property hierarchical tree for each location
             -- ======================
              begin
               --
               print_log('Current child location id/code/type: '
                         ||prop_denorm_rows(idx).location_id||', '
                         ||prop_denorm_rows(idx).location_code||', '
                         ||prop_denorm_rows(idx).location_type_lookup_code
                        );
               open prop_denorm_sql
                 (
                   l_opn_id         =>prop_denorm_rows(idx).location_id
                  ,l_opn_code       =>prop_denorm_rows(idx).location_code
                  ,l_opn_type       =>prop_denorm_rows(idx).location_type_lookup_code
                  ,l_loc_start_date =>prop_denorm_rows(idx).loc_active_start_date
                  ,l_loc_end_date   =>prop_denorm_rows(idx).loc_active_end_date
                  ,l_req_id         =>n_req_id
                  ,l_user_id        =>n_user_id
                  ,l_org_name       =>v_org_name
                  ,l_prop_id        =>prop_denorm_rows(idx).opn_id
                 );
               --
               fetch prop_denorm_sql bulk collect into child_denorm_rows;
               --
               close prop_denorm_sql;
               -- reset all attribute PLSQL record types so we can repopulate them.
               --
               if child_denorm_rows.count >0 then
                --
                print_log('');
                for idx in child_denorm_rows.first .. child_denorm_rows.last loop
                    --
                    begin
                         --Begin copy of property data to each of the child records
                        child_denorm_rows (idx).property_name           :=prop_denorm_rows(idx).property_name;
                        child_denorm_rows (idx).zone_name               :=prop_denorm_rows(idx).zone_name;
                        child_denorm_rows (idx).district                :=prop_denorm_rows(idx).district;
                        child_denorm_rows (idx).country                 :=prop_denorm_rows(idx).country;
                        child_denorm_rows (idx).description             :=prop_denorm_rows(idx).description;
                        child_denorm_rows (idx).portfolio               :=prop_denorm_rows(idx).portfolio;
                        child_denorm_rows (idx).portfolio_name          :=prop_denorm_rows(idx).portfolio_name;
                        child_denorm_rows (idx).tenure                  :=prop_denorm_rows(idx).tenure;
                        child_denorm_rows (idx).property_tenancy        :=prop_denorm_rows(idx).property_tenancy;
                        child_denorm_rows (idx).prop_class              :=prop_denorm_rows(idx).prop_class;
                        child_denorm_rows (idx).prop_class_name         :=prop_denorm_rows(idx).prop_class_name;
                        child_denorm_rows (idx).site_status             :=prop_denorm_rows(idx).site_status;
                        child_denorm_rows (idx).condition_name          :=prop_denorm_rows(idx).condition_name;
                        child_denorm_rows (idx).office_park_name        :=prop_denorm_rows(idx).office_park_name;
                        child_denorm_rows (idx).region_name             :=prop_denorm_rows(idx).region_name;
                        child_denorm_rows (idx).active_property         :=prop_denorm_rows(idx).active_property;
                        --assign property attributes
                        child_denorm_rows (idx).prop_attribute_category    :=prop_denorm_rows(idx).prop_attribute_category;
                        child_denorm_rows (idx).prop_metro_area            :=prop_denorm_rows(idx).prop_metro_area;
                        child_denorm_rows (idx).prop_currency              :=prop_denorm_rows(idx).prop_currency;
                        child_denorm_rows (idx).prop_primary_language      :=prop_denorm_rows(idx).prop_primary_language;
                        child_denorm_rows (idx).prop_primary_building_code :=prop_denorm_rows(idx).prop_primary_building_code;
                        child_denorm_rows (idx).prop_market_cluster        :=prop_denorm_rows(idx).prop_market_cluster;
                        child_denorm_rows (idx).prop_attribute6            :=prop_denorm_rows(idx).prop_attribute6;
                        child_denorm_rows (idx).prop_attribute7            :=prop_denorm_rows(idx).prop_attribute7;
                        child_denorm_rows (idx).prop_attribute8            :=prop_denorm_rows(idx).prop_attribute8;
                        child_denorm_rows (idx).prop_attribute9            :=prop_denorm_rows(idx).prop_attribute9;
                        child_denorm_rows (idx).prop_attribute10           :=prop_denorm_rows(idx).prop_attribute10;
                        child_denorm_rows (idx).prop_attribute11           :=prop_denorm_rows(idx).prop_attribute11;
                        child_denorm_rows (idx).prop_attribute12           :=prop_denorm_rows(idx).prop_attribute12;
                        child_denorm_rows (idx).prop_attribute13           :=prop_denorm_rows(idx).prop_attribute13;
                        child_denorm_rows (idx).prop_attribute14           :=prop_denorm_rows(idx).prop_attribute14;
                        child_denorm_rows (idx).prop_attribute15           :=prop_denorm_rows(idx).prop_attribute15;
                        --
                        child_denorm_rows (idx).hds_current_flag           :=prop_denorm_rows(idx).hds_current_flag;
                        child_denorm_rows (idx).hds_max_loc_start          :=prop_denorm_rows(idx).hds_max_loc_start;
                        child_denorm_rows (idx).hds_bldg_land_min_start    :=prop_denorm_rows(idx).hds_bldg_land_min_start;
                        child_denorm_rows (idx).hds_bldg_land_max_end      :=prop_denorm_rows(idx).hds_bldg_land_max_end;
                        --
                    exception
                     when others then
                      print_log(  'Error in assignment of property/bldg/land/office/section/parcel attributes to location_id ='
                                 ||prop_denorm_rows(idx).location_id
                                 ||', property id ='
                                 ||prop_rec(idx).property_id
                               );
                      print_log('Error in assignment of property attributes to location, error ='||sqlerrm);
                    end;

                end loop;
                print_log('');
                --print_log('Before bulk insert of the child location tree id ='||prop_denorm_rows(idx).location_id);
                 begin
                  forall idx in child_denorm_rows.first .. child_denorm_rows.last --save exceptions
                    insert into  xxcus.xxcus_opn_properties_all values child_denorm_rows (idx);
                    print_log('Successfully bulk inserted denorm data for location_id ='||prop_denorm_rows(idx).location_id
                               ||', location_code ='||prop_denorm_rows(idx).location_code||', Count ='||sql%rowcount
                               ||', Loc Start =>'||prop_denorm_rows(idx).loc_active_start_date
                               ||', Loc End   =>'||prop_denorm_rows(idx).loc_active_end_date
                             );
                    print_log('');
                 exception
                  when ex_dml_errors then
                   l_error_count := SQL%BULK_EXCEPTIONS.count;
                   print_log ('@Location_Id ='||child_denorm_rows(idx).location_id||', number of errors ='||l_error_count);
                      for i in 1 .. l_error_count loop
                        print_log ('Error: ' || i ||
                          ' Error Index: ' || sql%bulk_exceptions(i).error_index ||
                          ' Message: ' || sqlerrm(-sql%bulk_exceptions(i).error_code));
                      end loop;
                   when others then
                     print_log('Issue in bulk insert @ location_id '||child_denorm_rows(idx).location_id||', message ='||sqlerrm);
                 end;
                 --print_log('After bulk insert of the child location tree id ='||prop_denorm_rows(idx).location_id);
               end if;
               --
              exception
               --
               when others then
                --
                 print_log('Error in fetching property denormalized data for location_id ='||prop_denorm_rows(idx).location_id||', message ='||sqlerrm);
                --
              end;
              --
         end loop;
        else
         --
          Null;
         --
        end if;
         --
       exception
        --
        when others then
         --
          print_log('Operation: Traverse nodes, message ='||sqlerrm);
         --
       end;
      --Stage2 end
      -- end children rows denormazation logic 07/23/2014
      end loop;
      --
      --
--      begin
--        savepoint init_here;
--        --
--        merge into xxcus.xxcus_opn_properties_all t
--        using (
--                select a.*
--                from   pn_properties_v a
--                where  1 =1
--               ) s
--        on (
--                t.opn_type    !='PROPERTY'
--            and t.property_id =s.property_id --Update buidling details for the building rows
--           )
--        when matched then update set
--            t.property_name           =s.property_name,
--            t.zone_name               =s.zone_name,
--            t.district                =s.district,
--            t.country                 =s.country,
--            t.description             =s.description,
--            t.portfolio               =s.portfolio,
--            t.portfolio_name          =s.portfolio_name,
--            t.tenure                  =s.tenure,
--            t.property_tenancy        =s.tenure_name,
--            t.prop_class              =s.class,
--            t.prop_class_name         =s.class_name,
--            t.site_status             =s.property_status_name,
--            t.condition_name          =s.condition_name,
--            t.office_park_name        =s.office_park_name,
--            t.region_name             =s.region_name,
--            t.active_property         =s.active_property,
--            --assign property attributes
--            t.prop_attribute_category    =s.attribute_category,
--            t.prop_metro_area            =s.attribute1,
--            t.prop_currency              =s.attribute2,
--            t.prop_primary_language      =s.attribute3,
--            t.prop_primary_building_code =s.attribute4,
--            t.prop_market_cluster        =s.attribute5,
--            t.prop_attribute6            =s.attribute6,
--            t.prop_attribute7            =s.attribute7,
--            t.prop_attribute8            =s.attribute8,
--            t.prop_attribute9            =s.attribute9,
--            t.prop_attribute10           =s.attribute10,
--            t.prop_attribute11           =s.attribute11,
--            t.prop_attribute12           =s.attribute12,
--            t.prop_attribute13           =s.attribute13,
--            t.prop_attribute14           =s.attribute14,
--            t.prop_attribute15           =s.attribute15;
--        --
--        commit;
--        --
--      exception
--       when others then
--        rollback to init_here;
--      end;
      --
      -- Merge details for building.
      begin
       bldg_post_process;
      exception
       when others then
        print_log('@Caller:bldg_post_process, message ='||sqlerrm);
      end;
      --
      -- Merge details for floors.
      begin
       floor_post_process;
      exception
       when others then
        print_log('@Caller,floor_post_process, message ='||sqlerrm);
      end;
      --
      -- Merge details for office.
      begin
       office_post_process;
      exception
       when others then
        print_log('@caller:office_post_process, message ='||sqlerrm);
      end;
      --
      -- Merge details for LAND.
      begin
       land_post_process;
       Null;
      exception
       when others then
        print_log('@caller:land_post_process, message ='||sqlerrm);
      end;
      --
      -- Merge details for PARCEL.
      begin
       parcel_post_process;
       Null;
      exception
       when others then
        print_log('@caller:parcel_post_process, message ='||sqlerrm);
      end;
      --
      -- Merge details for YARD.
      begin
       yard_post_process;
       Null;
      exception
       when others then
        print_log('@caller:parcel_post_process, message ='||sqlerrm);
      end;
      --
      -- Merge details for PROPERTY.
      begin
       update_property_attributes;
      exception
       when others then
        print_log('@caller:update_property_attributes, message ='||sqlerrm);
      end;
     --
     -- Extract BUILDING / LAND features
     --
     print_log(' ');
     print_log('Before calling extract_features');
     extract_features;
     print_log('After calling extract_features');
     print_log(' ');
     --
     -- Extract BUILDING / LAND contacts
     --
     print_log(' ');
     print_log('Before calling extract_contacts');
     extract_contacts;
     print_log('After calling extract_contacts');
     print_log(' ');
     --
     -- update location_type verbeage
     --
     begin
      savepoint start_here;
        merge into xxcus.xxcus_opn_properties_all t
        using (
            select a.meaning loc_type, a.lookup_code loc_type_lkup_code
            from   fnd_lookup_values_vl a
            where  1 =1
              and  lookup_type ='PN_LOCATION_TYPE'
           ) s
        on (
            t.location_type_lookup_code =s.loc_type_lkup_code
        )
        when matched then update set t.location_type =s.loc_type;
     exception
      when others then
       rollback to start_here;
     end;
     --
     print_log('After update of location type lookup decsription');
     --
     commit;
     --
     -- Update CBSA, CBSA_NAME, MSA, MSA_NAME fields based on ZIPCODE
     --
     begin
      savepoint start_here;
        merge into xxcus.xxcus_opn_properties_all t
        using (
                SELECT DISTINCT cbsanamer.cbsa cbsa,
                                REPLACE (cbsanamer.cbsaname, '"') cbsaname,
                                msa,
                                msaname,
                                zipcode
                FROM xxcus.xxcuspn_metro_area_zips_tbl cbsanamer
                WHERE 1 =1
                  AND cbsanamer.primaryrecord = 'P'
           ) s
        on (
            t.prop_metro_area =s.zipcode
        )
        when matched then update set t.cbsa =s.cbsa, t.cbsa_name =s.cbsaname, t.msa =s.msa, t.msa_name =s.msaname;
     exception
      when others then
       rollback to start_here;
     end;
     --
     print_log('After update of CBSA, CBSA_NAME, MSA, MSA_NAME based on zipcode');
     --
     commit;
     --
     -- Update BUILDING/LAND area gross acreage only when location is LAND OR PARCEL OR YARD
     --
     begin
      savepoint start_here;
       update xxcus.xxcus_opn_properties_all
       set bl_ld_area_gross_acreage =round(nvl(bl_ld_area_gross, 0)/43560, 2)
      where 1 =1
        and location_type_lookup_code in ('LAND', 'PARCEL', 'SECTION');
     exception
      when others then
       rollback to start_here;
     end;
     --
     print_log('After update of building /land area gross acreage.');
     --
     commit;
     --
     -- begin first update of office_customer_site
     -- update office_customer_site for all records of a property other than OFFICE.
     begin
       --
       for rec in
        (
            select a.opn_id, a.office_customer_site
            from   xxcus.xxcus_opn_properties_all a
            where  1 =1
            and opn_type                   ='PROPERTY'
            and location_type_lookup_code  ='OFFICE'
            and a.office_customer_site is not null
            group by a.opn_id, a.office_customer_site
        )
       loop
        --
        begin
         --
         savepoint init_here;
         --
            update xxcus.xxcus_opn_properties_all a
            set    office_customer_site =rec.office_customer_site
            where 1 =1
            and opn_id   =rec.opn_id
            and opn_type ='PROPERTY'
            and location_type_lookup_code in ('BUILDING', 'FLOOR');
         --
        exception
         when others then
           print_log('@update of office_customer_site for Buildings /Floors where opn_type =PROPERTY, msg ='||sqlerrm);
           rollback to init_here;
        end;
        --
       end loop;
       --
       commit;
       --
     exception
      when others then
       print_log('@update of office_customer_site, error in fetch of main select, where opn_type =PROPERTY, msg ='||sqlerrm);
     end;
     -- end first update of office_customer_site
     --
     -- begin second update of office_customer_site
     --
     begin
       --
       for rec in
        (
            select a.location_id, a.office_customer_site
            from   xxcus.xxcus_opn_properties_all a
            where  1 =1
            and opn_type                  ='PROPERTY'
            and location_type_lookup_code ='BUILDING'
            and a.office_customer_site is not null
            group by a.location_id, a.office_customer_site
        )
       loop
        --
        begin
         --
         savepoint init_here;
         --
            update xxcus.xxcus_opn_properties_all a
            set    office_customer_site =rec.office_customer_site
            where 1 =1
            and opn_type  ='BUILDING'
            and opn_id    =rec.location_id;
         --
        exception
         when others then
           print_log('@update of office_customer_site for Buildings/Floors where opn_type =BUILDING, msg ='||sqlerrm);
           rollback to init_here;
        end;
        --
       end loop;
       --
       commit;
       --
     exception
      when others then
       print_log('@update of office_customer_site, error in fetch of main select, where opn_type =BUILDING, msg ='||sqlerrm);
     end;
     -- end second update of office_customer_site
     --
     --
     -- begin third update of office_customer_site
     --
     begin
       --
       for rec in
        (
            select a.location_id, a.office_customer_site
            from   xxcus.xxcus_opn_properties_all a
            where  1 =1
            and opn_type                  ='PROPERTY'
            and location_type_lookup_code ='FLOOR'
            and a.office_customer_site is not null
            group by a.location_id, a.office_customer_site
        )
       loop
        --
        begin
         --
         savepoint init_here;
         --
            update xxcus.xxcus_opn_properties_all a
            set    office_customer_site =rec.office_customer_site
            where 1 =1
            and opn_type  ='FLOOR'
            and opn_id    =rec.location_id;
         --
        exception
         when others then
           print_log('@update of office_customer_site for Floors/Offices where opn_type =FLOOR, msg ='||sqlerrm);
           rollback to init_here;
        end;
        --
       end loop;
       --
       commit;
       --
     exception
      when others then
       print_log('@update of office_customer_site, error in fetch of main select, where opn_type =FLOOR, msg ='||sqlerrm);
       rollback to init_here;
     end;
     -- end third update of office_customer_site
     --
     -- populate the hds_st_prv field. We will just concatenate the state and the province field.
     begin
         --
         savepoint init_here;
         --
            update xxcus.xxcus_opn_properties_all a
            set    hds_st_prv =trim(state)||trim(province)
            where 1 =1;
         --
         commit;
         --
     exception
         when others then
           print_log('@update of hds_st_prv field, msg ='||sqlerrm);
           rollback to init_here;
     end;
     --
     -- Populate space type tag and description
     begin
          --
          savepoint init_here;
          --
            merge into xxcus.xxcus_opn_properties_all t
            using (
                        select nvl(tag, 999) tag, lookup_code, meaning
                        from   fnd_lookup_values
                        where  1 =1
                          and  lookup_type ='PN_SPACE_TYPE'
                          and  enabled_flag ='Y'
                   ) s
            on (
                    t.space_type_lookup_code =s.lookup_code
               )
            when matched then update set
             t.space_type_tag =s.tag
            ,t.space_type =s.meaning;
          --
--          commit;
          --
     exception
      when others then
       print_log('@ update of space_type_tag and meaning, message ='||sqlerrm);
       rollback to init_here;
     end;
     --
     commit;
     --
     --
     -- populate the hds_bldg_land_status field.
     begin
         --
         savepoint init_here;
         --
            update xxcus.xxcus_opn_properties_all a
            set    hds_bldg_land_status =
            case
            when (hds_current_flag ='Y' and location_type_lookup_code IN ('BUILDING', 'LAND') and trunc(loc_active_start_date) >trunc(sysdate)) then 'Future'
            when (hds_current_flag ='Y' and location_type_lookup_code IN ('BUILDING', 'LAND') and trunc(sysdate) between trunc(loc_active_start_date) and nvl(trunc(loc_active_end_date), trunc(sysdate))) then 'Active'
            when (hds_current_flag ='Y' and location_type_lookup_code IN ('BUILDING', 'LAND') and trunc(loc_active_end_date) <trunc(sysdate)) then 'Complete' --Replaced text Inactive to Complete on 10/06/14
            else Null
           end;
         --
         commit;
         --
     exception
         when others then
           print_log('@update of hds_bldg_land_status, msg ='||sqlerrm);
           rollback to init_here;
     end;
     --
     --
     -- populate the hds_bldg_land_status field. We will just concatenate the state and the province field.
     begin
         --
       for rec in
        (
            select a.bl_ld_id
              ,a.hds_bldg_land_status
              ,a.opn_id
              ,a.opn_type
              ,a.hds_current_flag
            from   xxcus.xxcus_opn_properties_all a
            where 1 =1
              --and a.opn_type ='PROPERTY'
            --and a.opn_code like 'FL300%'
            and a.location_type_lookup_code in ('BUILDING', 'LAND')
            group by a.bl_ld_id
                ,a.hds_bldg_land_status
                ,a.opn_id
                ,a.opn_type
                ,a.hds_current_flag
        )
       loop
        begin
         --
         savepoint init_here;
         --
         update xxcus.xxcus_opn_properties_all
            set hds_bldg_land_status =rec.hds_bldg_land_status
          where 1 =1
            and opn_id =rec.opn_id
            and opn_type =rec.opn_type
            and bl_ld_id =rec.bl_ld_id
            and hds_current_flag =rec.hds_current_flag
            and hds_bldg_land_status is null;
         --
        exception
         when others then
          print_log('Error in update of hds_bldg_land_status for all levels below BULDING and LAND, msg ='||sqlerrm);
          rollback to init_here;
        end;
       end loop;
       --
         commit;
       --
     exception
         when others then
           print_log('@update of hds_bldg_land_status, msg ='||sqlerrm);
           rollback to init_here;
     end;
     --
     --
     -- populate the hds_pri_loc_flag field.
     begin
         --
         savepoint init_here;
         --
            update xxcus.xxcus_opn_properties_all a
            set    hds_pri_loc_flag =
              case
               when a.bl_ld_id =a.prop_primary_building_code then 'Y'
               else 'N'
              end;
         --
         commit;
         --
     exception
         when others then
           print_log('@update of hds_pri_loc_flag, msg ='||sqlerrm);
           rollback to init_here;
     end;
     -- Begin Ver 3.4
     --
     -- populate hds_prop_current_flag field.
     --
       begin
         --
         savepoint init_here;
         --
            update xxcus.xxcus_opn_properties_all a
            set    hds_prop_current_flag =
              case
               when (a.opn_type ='PROPERTY' and a.location_type_lookup_code IN ('OFFICE', 'SECTION') and a.hds_current_flag ='Y') then 'Y'
               else 'N'
              end;
         --
         commit;
         --
     exception
         when others then
           print_log('@update of hds_prop_current_flag, msg ='||sqlerrm);
           rollback to init_here;
     end;
     -- End Ver 3.4
     --
     -- Populate the FRU / LOCATION data
     --
     begin
      --
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUS_OPN_FRU_LOC_ALL';
      --
      INSERT INTO xxcus.xxcus_opn_fru_loc_all
       (
        SELECT
                opn_tbl.opn_id                         opn_id
               ,opn_tbl.opn_code                       opn_code
               ,opn_tbl.opn_type                       opn_type
               ,locdir.fru||'-'||locasg.location_code  hds_fru_section
               --,SUBSTR(locasg.location_code, 1, INSTR(locasg.location_code, '.')-1)||'-'||nvl(rollups.lob_ms, 'NONE') loc_lob_ms
               ,SUBSTR(locasg.location_code, 1, INSTR(locasg.location_code, '.')-1)||'-'||nvl(rollups.lob_abb, 'NONE') hds_loc_lob_abb
               ,Null                                                 hds_prop_fru
               --,opn_tbl.opn_code||'-'||nvl(rollups.bu_id_ms, 'NONE') prop_bu
               ,opn_tbl.opn_code||'-'||nvl(rollups.sublob_abb, 'NONE') HDS_PROP_SUBLOB_ABB
               ,locasg.location_code                 location_code
               ,loc_lkup.meaning                     location_type
               ,Null                                 bldg_land_id
               ,Null                                 bldg_land_code
               ,Null                                 bldg_land_type
               ,Null                                 floor_parcel_id
               ,Null                                 floor_parcel_code
               ,Null                                 floor_parcel_type
               ,null                                 hds_emp_assign_current_flag
               ,null                                 hds_fru_section_current_flag
               ,spc.space_type                       space_type
               ,spc.space_tag                        space_tag
               ,(
                  select meaning
                  from   fnd_lookup_values
                  where  1 =1
                    and  lookup_type ='PN_FUNCTION_TYPE'
                    and  lookup_code =pnl.function_type_lookup_code
                )                                    site_type
               ,(
                  select meaning
                  from   fnd_lookup_values
                  where  1 =1
                    and  lookup_type ='PN_STANDARD_TYPE'
                    and  lookup_code =pnl.standard_type_lookup_code
                )                                    assignment_type
               ,locasg.cost_center_code              opn_cost_center
               ,locasg.attribute1                    opn_fru
              -- ,locdir.fru                           lc_fru --Ver 3.4
               ,lpad(locdir.fru, 4, '0')                  lc_fru --Ver 3.4
               ,locdir.fru_descr                     lc_fru_description
               ,Null                                 hds_pri_fru_flag
               ,Null                                 hds_loc_bu_pri_ops_fru
               ,Null                                 primary_phone_number
               ,Null                                 hds_prop_fru_pri_flag
               ,Null                                 hds_prop_fru_pri_ops
               ,Null                                 hds_loc_bu_pri_ops
               --,Null                                 HDS_PROP_SUBLOB_ABB_pri_ops --Ver 3.3
               ,Null                                 hds_prop_bu_pri_ops --Ver 3.3. Looks like a change was requested to rename the column but never completed. Keep the column to original name hds_prop_bu_pri_ops
               ,locasg.attribute2                    opn_hr_based_fru
               ,case
                 when (trunc(locasg.emp_assign_start_date) >trunc(sysdate)) then 'Future'
                 when (trunc(sysdate) between trunc(locasg.emp_assign_start_date) and nvl(trunc(locasg.emp_assign_end_date), trunc(sysdate))) then 'Active'
                 when (trunc(locasg.emp_assign_end_date) <=trunc(sysdate)) then 'Inactive' --Ver 3.3
                 else Null
                end                                  hds_ops_status
               ,locasg.location_suite_name           loc_suite_name
               ,trunc(locasg.emp_assign_start_date)  emp_assign_start_date
               ,trunc(locasg.emp_assign_end_date)    emp_assign_end_date
               ,locdir.inactive                      lc_inactive_flag
               ,locdir.lob_branch                    lc_lob_branch
               ,locdir.lob_dept                      lc_lob_dept
               ,locdir.entrp_entity                  lc_entrp_entity
               ,locdir.entrp_loc                     lc_entrp_loc
               ,locdir.entrp_cc                      lc_entrp_cc
               ,locdir.ap_docum_flag                 lc_ap_docum_flag
               ,locdir.hc_flag                       lc_hc_flag
               ,locdir.creation_dt                   lc_creation_dt
               ,locdir.system_cd                     lc_system_cd
               ,locdir.iexp_fru_override             lc_iexp_fru_override
               ,locdir.country                       lc_country
               ,locdir.state                         lc_state
               ,locdir.effdt                         lc_effective_date
               ,locdir.business_unit                 lc_business_unit
               ,rollups.bu_id_ms                     bu_id_ms
               ,rollups.fps_id                       fps_id
               ,rollups.fps_desc                     fps_desc
               ,rollups.sbu_id                       sbu_id
               ,rollups.sbu_desc                     sbu_desc
               ,rollups.bu_id                        bu_id
               ,rollups.bu_desc                      bu_desc
               ,rollups.tax_id                       tax_id
               ,rollups.legal_entity                 legal_entity
               ,rollups.evp                          evp
               ,rollups.entity_classification        entity_classification
               ,rollups.active_flag                  fps_active_flag
               ,rollups.lob_ms                       lob_ms
               ,trunc(locasg.active_start_date)      loc_active_start_date
               ,trunc(locasg.active_end_date)        loc_active_end_date
               ,locasg.employee_number               emp_number
               ,locasg.last_name                     emp_last_name
               ,locasg.first_name                    emp_first_name
               ,locasg.full_name                     emp_full_name
               ,locasg.uom_code                      uom_code
               ,locasg.allocated_area_pct            area_pct
               ,locasg.allocated_area                area_allocated
               ,locasg.utilized_area                 area_utilized
               ,locasg.emp_space_comments            emp_space_comments
               ,locasg.attribute_category            attribute_category
               ,opn_tbl.space_type_lookup_code       space_type_lookup_code
               ,locasg.location_type_lookup_code     loc_type_lookup_code
               ,locasg.location_id                   location_id
               ,locasg.location_location_id          loc_locn_id
               ,locasg.emp_space_assign_id           emp_space_assign_id
               ,locasg.person_id                     person_id
               ,locasg.project_id                    pa_project_id
               ,locasg.task_id                       pa_task_id
               ,locasg.org_id                        org_id
               --,SUBSTR(locasg.location_code, 1, INSTR(locasg.location_code, '.')-1)||'-'||nvl(rollups.bu_id_ms, 'NONE') loc_bu
               ,SUBSTR(locasg.location_code, 1, INSTR(locasg.location_code, '.')-1)||'-'||nvl(rollups.sublob_abb, 'NONE') loc_bu
               ,rollups.lob_abb                      lob_abb
               ,rollups.lob_dba                      lob_dba
               ,rollups.sublob_abb                   sublob_abb
               ,rollups.sublob_dba                   sublob_dba
               ,Null                                 hds_current_flag
               ,pnl.function_type_lookup_code        site_type_code
               ,pnl.standard_type_lookup_code        assignment_type_code
               ,rollups.primary_sub_bu_rank          primary_sub_bu_rank
               ,Null  hds_fru_loc --Ver 3.4
               ,Null hds_feed_to_extended --Ver 3.4
               ,Null hds_feed_to_external --Ver 3.4
               ,Null fax_number --Ver 3.4
               ,Null --hds_bldg_count number --Ver 3.6
               ,Null --hds_active_bldg_total_sf_prp number --Ver 3.6
               ,Null --hds_active_acres_prp number --Ver 3.6
               ,Null --hds_yard_count number --Ver 3.6
               ,Null --hds_pri_rer_type varchar2(30) --Ver 3.6
               ,Null --hds_pri_rer_id varchar2(80) --Ver 3.6
               ,Null --hds_pri_rer_expiry date --Ver 3.6
               ,0 --hds_active_bldg_sf_fru_prp --Ver 3.6
               ,sysdate --extract_date --Ver 3.6
        FROM   pn_space_assign_emp_v        locasg
              ,apps.xxcus_location_code_vw  locdir
              ,pn_locations_all             pnl
              ,hdsoracle.xxhsi_pnld_lobrollup@apxprd_lnk.hsi.hughessupply.com rollups --APEX TABLE USING DB LINK
              ,fnd_lookup_values_vl         loc_lkup
              ,(
                  select lookup_code space_lkup_code, meaning space_type, to_number(nvl(tag, 999)) space_tag
                  from   fnd_lookup_values
                  where  1 =1
                    and  lookup_type ='PN_SPACE_TYPE'
                    --and  enabled_flag ='Y' --Ver 3.3 --Was causing blank values to be populated in fields space_type and space_tag  when they are inactive.
                ) spc
              ,(
                SELECT  location_id            loc_id
                       ,opn_id
                       ,opn_code
                       ,opn_type
                       ,loc_active_start_date  loc_start_date
                       ,loc_active_end_date    loc_end_date
                       ,space_type_lookup_code space_type_lookup_code
                FROM   xxcus.xxcus_opn_properties_all
                WHERE  1 =1
                  AND  opn_type ='PROPERTY'
                  AND  location_type_lookup_code IN ('OFFICE', 'SECTION') --only records of properties where location type is Section within a Building / Yard within a Land
                GROUP BY location_id
                        ,opn_id
                        ,opn_code
                        ,opn_type
                        ,loc_active_start_date
                        ,loc_active_end_date
                        ,space_type_lookup_code
               ) opn_tbl
        WHERE  1 =1
          AND  locasg.emp_space_assign_id is not null
          --and  (locasg.active_start_date <= nvl ('', sysdate) and  locasg.active_end_date >= nvl ('', sysdate))
          AND  locdir.entrp_loc          =locasg.cost_center_code
          AND  loc_lkup.lookup_type      ='PN_LOCATION_TYPE'
          AND  loc_lkup.lookup_code      =locasg.location_type_lookup_code
          AND  rollups.fps_id(+)         =locdir.entrp_entity
          AND  opn_tbl.loc_id(+)         =locasg.location_location_id
          AND  opn_tbl.loc_start_date    =trunc(locasg.active_start_date)
          AND  opn_tbl.loc_end_date      =trunc(locasg.active_end_date)
          AND  spc.space_lkup_code(+)    =opn_tbl.space_type_lookup_code
          AND  pnl.location_id(+)        =opn_tbl.loc_id
          AND  pnl.active_start_date(+)  =opn_tbl.loc_start_date
          AND  pnl.active_end_date(+)    =opn_tbl.loc_end_date
       );
       --
       print_log('@Main fru loc insert, total records ='||sql%rowcount);
       print_log(' ');
       --
       v_exec :=TRUE;
       --
     exception
      when others then
       v_exec :=FALSE;
       print_log ('Error in insert of FRU/LOCATION data, message ='||sqlerrm);
     end;
     --
     commit;
     --
     -- copy hds_current_flag from the table xxcus.xxcus_opn_properties_all
     --
        begin
         --
         for rec in all_space_asg_locs loop
          --
             begin
              --
              savepoint init_here;
              --
              update xxcus.xxcus_opn_fru_loc_all
                 set hds_current_flag =rec.hds_current_flag
              where  1 =1
                and  opn_id =rec.opn_id
                and  location_id =rec.location_id
                and  loc_active_start_date =rec.loc_active_start_date
                and  loc_active_end_date =rec.loc_active_end_date;
              --
             exception
              when others then
               rollback to init_here;
             end;
          --
         end loop;
         --
         commit;
         --
        exception
         when others then
          --
          rollback;
          print_log('@ update of hds_current_flag, outer block, message ='||sqlerrm);
          --
        end;
     --
     -- update hds_emp_assign_current_flag and hds_fru_section_current_flag
     --
     --
        begin
         --
         for rec in all_fru_sections loop
          --
          -- update hds_emp_assign_current_flag and hds_fru_section_current_flag
          --
             begin
              --
              savepoint init_here;
              --
              update xxcus.xxcus_opn_fru_loc_all
                 set hds_emp_assign_current_flag =case
                                                   when (emp_assign_start_date >=rec.max_emp_asg_start and nvl(emp_assign_end_date, trunc(sysdate)) =rec.max_emp_asg_end) then 'Y'
                                                   else 'N'
                                                  end
                    ,hds_fru_section_current_flag =case
                                                   when ( emp_assign_start_date >=rec.max_emp_asg_start and nvl(emp_assign_end_date, trunc(sysdate)) =rec.max_emp_asg_end and hds_current_flag ='Y') then 'Y'
                                                   --when ( space_type_lookup_code !='LAWSONADMIN' and emp_assign_start_date >=rec.max_emp_asg_start and nvl(emp_assign_end_date, trunc(sysdate)) =rec.max_emp_asg_end and hds_current_flag ='Y') then 'Y'
                                                   else 'N'
                                                  end
              where  1 =1
                and  opn_id =rec.opn_id
                and  hds_fru_section =rec.hds_fru_section;
              --
             exception
              when others then
               rollback to init_here;
             end;
          --
         end loop;
         --
         commit;
         --
        exception
         when others then
          --
          rollback;
          print_log('@block cursor all_fru_sections, message ='||sqlerrm);
          --
        end;
     --
     if (v_exec) then --what this means is main data was successfully inserted for XXCUS.XXCUS_OPN_FRU_LOC_ALL table
      -- begin update of attributes for XXCUS.XXCUS_OPN_FRU_LOC_ALL table
         BEGIN
          SAVEPOINT BEGIN_HERE;
          EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUS_OPN_FRULOC_PHONE';
           INSERT INTO XXCUS.XXCUS_OPN_FRULOC_PHONE
           (
             select *
             from hdsoracle.hds_phone_tbl@apxprd_lnk.hsi.hughessupply.com --@apxprd_lnk
             where 1 =1
               --and phone_type ='Main Number' --Ver 3.4 -- Commented out to bring in Fax numbers as well.
           );
           COMMIT;
          EXECUTE IMMEDIATE 'ALTER INDEX XXCUS.XXCUS_OPN_FRULOC_PHONE_N1 REBUILD';
         EXCEPTION
          WHEN OTHERS THEN
           fnd_file.put_line(fnd_file.log, 'Issue in copying phone/fax data to fru loc table, msg ='||sqlerrm); --Ver 3.4
           ROLLBACK TO BEGIN_HERE;
         END;
         --
         print_log('After copy of fru loc phone data from APEX db link.');
         --
         -- UPDATE Primary Phone for the LOCATION / FRU combo. If we have more than one we will pick the first one returned.
         --
         begin
          for rec in (
                        select replace(substr(location_code, 1, 6), '.', '') loc_code_1_6
                              ,lc_fru fru
                              --,opn_fru
                        from   xxcus.xxcus_opn_fru_loc_all
                        group by replace(substr(location_code, 1, 6), '.', '')
                                ,lc_fru
                                --,opn_fru
                     )
          loop
           --
           begin
            --
            SELECT REGEXP_REPLACE(phone_number ,'([[:digit:]]{3})([[:digit:]]{3})([[:digit:]]{4})' ,'(\1) \2-\3')
            INTO   v_primary_phone_num
            FROM   xxcus.xxcus_opn_fruloc_phone --hdsoracle.hds_phone_tbl@apxprd_lnk
            WHERE 1 =1
              AND phone_type ='Main Number' --Ver 3.4
              AND locid      =rec.loc_code_1_6
              AND fru        =rec.fru
              AND rownum <2;
            --
              begin
               savepoint start_update;
               update xxcus.xxcus_opn_fru_loc_all
               set primary_phone_number =v_primary_phone_num
               where 1 =1
                 and replace(substr(location_code, 1, 6), '.', '') =rec.loc_code_1_6
                 and opn_fru                                       =rec.fru;
              exception
               when others then
                rollback to start_update;
              end;
           exception
            when no_data_found then
             Null;
            when others then
             Null;
           end;
          end loop;
          --
         exception
          when others then
           Null;
         end;
         --
         print_log('After update of fru loc primary phone.');
         --
         commit;
         --
         -- Begin Ver 3.4
                 --
                 -- UPDATE Primary fax for the LOCATION / FRU combo. If we have more than one we will pick the first one returned.
                 --
                 begin
                  for rec in (
                                select replace(substr(location_code, 1, 6), '.', '') loc_code_1_6
                                      ,lc_fru fru
                                      --,opn_fru
                                from   xxcus.xxcus_opn_fru_loc_all
                                group by replace(substr(location_code, 1, 6), '.', '')
                                        ,lc_fru
                                        --,opn_fru
                             )
                  loop
                   --
                   begin
                    --
                    SELECT REGEXP_REPLACE(phone_number ,'([[:digit:]]{3})([[:digit:]]{3})([[:digit:]]{4})' ,'(\1) \2-\3')
                    INTO   v_fax_number
                    FROM   xxcus.xxcus_opn_fruloc_phone
                    WHERE 1 =1
                      AND phone_type ='FAX'
                      AND locid      =rec.loc_code_1_6
                      AND fru        =rec.fru
                      AND rownum <2;
                    --
                      begin
                       savepoint start_update;
                       update xxcus.xxcus_opn_fru_loc_all
                       set fax_number =v_fax_number
                       where 1 =1
                         and replace(substr(location_code, 1, 6), '.', '') =rec.loc_code_1_6
                         and opn_fru                                       =rec.fru;
                      exception
                       when others then
                        rollback to start_update;
                      end;
                   exception
                    when no_data_found then
                     Null;
                    when others then
                     Null;
                   end;
                  end loop;
                  --
                 exception
                  when others then
                   Null;
                 end;
                 --
                 print_log('After update of fru loc fax number.');
                 --
                 commit;
                 --
         -- End Ver 3.4
         -- update loc_bu_primary_ops
         --
         begin
              for rec in
               (
                select hds_loc_lob_abb --loc_lob_ms --loc_bu
                from xxcus.xxcus_opn_fru_loc_all
                where 1 =1
                 --AND LOC_TYPE_LOOKUP_CODE IN ('OFFICE', 'FLOOR', 'BUILDING') --Ver 3.3
                group by hds_loc_lob_abb --loc_lob_ms --loc_bu
               )
              loop
               --
               begin
                select distinct a.space_type
                into   v_loc_bu_pri_ops
                from xxcus.xxcus_opn_fru_loc_all a
                where 1 =1
                 --and a.loc_bu                  =rec.loc_bu --'FL300A-CRP'
                 --and a.loc_lob_ms             =rec.loc_lob_ms --'FL300A-CRP'
                 and a.hds_loc_lob_abb         =rec.hds_loc_lob_abb
                 --and a.loc_type_lookup_code in ('OFFICE', 'FLOOR', 'BUILDING') --Ver 3.3
                 --and a.space_type_lookup_code != 'LAWSONADMIN' --Ver 3.3
                 and a.space_tag =(
                                  select min(b.space_tag)
                                  from   xxcus.xxcus_opn_fru_loc_all b
                                  where  1 =1
                                     --and b.loc_lob_ms              =a.loc_lob_ms
                                     --and b.loc_bu                  =a.loc_bu
                                     and b.hds_loc_lob_abb         =a.hds_loc_lob_abb
                                     and b.loc_type_lookup_code    =a.loc_type_lookup_code
                                     --and b.space_type_lookup_code  != 'LAWSONADMIN' --Ver 3.3
                                )
                 and rownum <2;
                --
               exception
                when no_data_found then
                 v_loc_bu_pri_ops :=Null;
                when too_many_rows then
                 v_loc_bu_pri_ops :=Null;
                when others then
                 v_loc_bu_pri_ops :=Null;
               end;
               --
               --
               -- update the fru/loc table with the loc_bu_primary_ops
               --
               begin
                  --
                  savepoint init_here;
                  --
                  update xxcus.xxcus_opn_fru_loc_all
                  set hds_loc_bu_pri_ops =v_loc_bu_pri_ops
                  where 1 =1
                    --and loc_bu =rec.loc_bu
                    --and loc_lob_ms =rec.loc_lob_ms
                    and hds_loc_lob_abb        =rec.hds_loc_lob_abb; --Ver 3.3
                    --and loc_type_lookup_code   in ('OFFICE', 'FLOOR', 'BUILDING'); --Ver 3.3
                    --and space_type_lookup_code != 'LAWSONADMIN'; --Ver 3.3
                  --
               exception
                  when others then
                    rollback to init_here;
               end;
                --
              end loop;
              --
         exception
           when others then
            print_log('Error in pulling unique loc_bu before update of loc_bu_primary_ops, msg ='||sqlerrm);
         end;
         --
         print_log('After update of location bu primary ops');
         --
          commit;
         --
         -- update prop_bu_primary_ops
         --
         begin
          for rec in
           (
            select HDS_PROP_SUBLOB_ABB
            from xxcus.xxcus_opn_fru_loc_all
            where 1 =1
             --AND LOC_TYPE_LOOKUP_CODE IN ('OFFICE', 'FLOOR', 'BUILDING') --Ver 3.3
            group by HDS_PROP_SUBLOB_ABB
           )
          loop
           --
           begin
            select distinct a.space_type
            into   v_prop_bu_pri_ops
            from xxcus.xxcus_opn_fru_loc_all a
            where 1 =1
             and a.HDS_PROP_SUBLOB_ABB             =rec.HDS_PROP_SUBLOB_ABB --'FL300A-CRP'
             --and a.loc_type_lookup_code    in ('OFFICE', 'FLOOR', 'BUILDING') --Ver 3.3
             --and a.space_type_lookup_code  != 'LAWSONADMIN' --Ver 3.3
             and a.space_tag =(
                              select min(b.space_tag)
                              from   xxcus.xxcus_opn_fru_loc_all b
                              where  1 =1
                                 and b.HDS_PROP_SUBLOB_ABB             =a.HDS_PROP_SUBLOB_ABB
                                 and b.loc_type_lookup_code    =a.loc_type_lookup_code
                                 --and b.space_type_lookup_code  != 'LAWSONADMIN' --Ver 3.3
                            )
             and rownum <2;
           exception
            when no_data_found then
             v_prop_bu_pri_ops :=Null;
            when too_many_rows then
             v_prop_bu_pri_ops :=Null;
            when others then
             v_prop_bu_pri_ops :=Null;
           end;
           --
           --
           -- update the fru/loc table with the prop_bu_primary_ops
           --
           begin
              --
              savepoint init_here;
              --
              update xxcus.xxcus_opn_fru_loc_all
              set HDS_LOC_BU_PRI_OPS_FRU =v_prop_bu_pri_ops
              where 1 =1
                and HDS_PROP_SUBLOB_ABB            =rec.HDS_PROP_SUBLOB_ABB; --Ver 3.3
                --and loc_type_lookup_code   in ('OFFICE', 'FLOOR', 'BUILDING') --Ver 3.3
                --and space_type_lookup_code != 'LAWSONADMIN'; --Ver 3.3
              --
           exception
              when others then
                rollback to init_here;
           end;
           --
          end loop;
          --
          commit;
          --
         exception
          when others then
           print_log('Error in pulling unique prop_bu before update of prop_bu_primary_ops, msg ='||sqlerrm);
         end;
         --
         print_log('After update of property bu primary ops');
         --
          begin
           --
                savepoint upd_floor_parcel_info;
           --
                merge into xxcus.xxcus_opn_fru_loc_all t
                using (
                        select flr_parcel.location_id   flr_parcel_id
                              ,flr_parcel.location_code flr_parcel_code--distinct location_id, location_code, parent_location_id
                              ,flr_parcel.loc_type      flr_parcel_type
                              ,section_yard.location_id office_yard_id
                        from   (
                                  select parent_location_id, location_id
                                  from pn_locations_all
                                  where 1 =1
                                    and location_type_lookup_code IN ('OFFICE', 'SECTION')
                                  group by parent_location_id, location_id
                               ) section_yard
                              ,(
                                  select parent_location_id, location_id, location_code, location_type_lookup_code loc_type
                                  from pn_locations_all
                                  where 1 =1
                                    and location_type_lookup_code IN ('FLOOR', 'PARCEL')
                                  group by parent_location_id, location_id, location_code, location_type_lookup_code
                               ) flr_parcel
                        where 1 =1
                          and flr_parcel.location_id   =section_yard.parent_location_id
                          --and sysdate between section_yard.active_start_date and section_yard.active_end_date
                          --and sysdate between flr_parcel.active_start_date and flr_parcel.active_end_date
                       ) s
                on (
                        t.loc_locn_id =s.office_yard_id --Update floor or parcel details for the office or yard space assignments
                   )
                when matched then update set
                 t.floor_parcel_code =s.flr_parcel_code
                ,t.floor_parcel_id   =s.flr_parcel_id
                ,t.floor_parcel_type =s.flr_parcel_type;
            --
            print_log('Total records updated with floor_parcel_code: '||sql%rowcount);
            --
          exception
           when others then
            rollback to upd_floor_parcel_info;
            print_log('@upd_floor_parcel_info, Error in update floor_parcel_code and id, Msg ='||sqlerrm);
          end;
         --
         commit;
         --
         print_log('After update of floor /parcel id and code.');
         --
         --
          begin
           --
                begin
                 --
                    for rec in (select opn_id, floor_parcel_id from xxcus.xxcus_opn_fru_loc_all group by opn_id, floor_parcel_id) loop
                     --
                     begin
                      --
                        select flr.parent_location_id          bldg_land_id
                              ,bldg.location_code              bldg_land_code
                              ,bldg.location_type_lookup_code  bldg_land_type
                        into   n_bldg_land_id
                              ,v_bldg_land_code
                              ,v_bldg_land_type
                        from   pn_locations_all flr
                              ,pn_locations_all bldg
                        where 1 =1
                          and flr.location_id                =rec.floor_parcel_id
                          and flr.location_type_lookup_code  IN ('FLOOR', 'PARCEL')
                          and flr.parent_location_id         =bldg.location_id
                          and bldg.location_type_lookup_code IN ('BUILDING', 'LAND')
                        group by flr.parent_location_id, bldg.location_code, bldg.location_type_lookup_code;
                      --
                         begin
                          --
                          savepoint init_here;
                          --
                          update xxcus.xxcus_opn_fru_loc_all
                          set bldg_land_id    =n_bldg_land_id
                             ,bldg_land_code  =v_bldg_land_code
                             ,bldg_land_type  =v_bldg_land_type
                          where 1 =1
                            and opn_id          =rec.opn_id
                            and floor_parcel_id =rec.floor_parcel_id;
                          --
                         exception
                          when others then
                           --
                           rollback to init_here;
                         end;
                      --
                     exception
                      when no_data_found then
                       null;
                      when others then
                       dbms_output.put_line('failed to select bldg info for opn_id /floor_parcel_id ='||rec.opn_id||' /'||rec.floor_parcel_id||', Message ='||SQLERRM);
                     end;
                     --
                    end loop;
                 --
                 commit;
                 --
                exception
                 --
                 when others then
                  rollback;
                  dbms_output.put_line('Main Block, Message ='||SQLERRM);
                end;
           --
            print_log('Total records updated with bldg_land_code: '||sql%rowcount);
            --
          exception
           when others then
            print_log('@upd_bldg_land_info, Error in update bldg_land_id and code, Msg ='||sqlerrm);
          end;
         --
         commit;
         --
         print_log('After update of building /land id and code.');
         -- update loc_bu_primary_ops_fru
         print_log(' ');
         --
         begin
              for rec in
               (
                select hds_loc_lob_abb --loc_lob_ms --loc_bu
                from xxcus.xxcus_opn_fru_loc_all
                where 1 =1
                 --AND LOC_TYPE_LOOKUP_CODE IN ('OFFICE', 'FLOOR', 'BUILDING')
                group by hds_loc_lob_abb --loc_lob_ms --loc_bu
               )
              loop
               open get_primary_fru (p_hds_loc_lob_abb =>rec.hds_loc_lob_abb);
               fetch get_primary_fru bulk collect into get_primary_fru_rec;
               close get_primary_fru;
               --
               if get_primary_fru_rec.count >0 then
                --
                 for idx in get_primary_fru_rec.first .. get_primary_fru_rec.last loop
                  --
                  if idx =1 then --we only need to look at the first rec bcoz the cursor get_primary_fru is sorted by space_tag asc
                   --
                   -- update the fru/loc table with the loc_bu_primary_ops_fru
                   --
                   begin
                      --
                      savepoint init_here;
                      --
                      update xxcus.xxcus_opn_fru_loc_all a
                      set a.hds_loc_bu_pri_ops_fru =get_primary_fru_rec(idx).loc_bu_pri_ops_fru
                      where 1 =1
                        and a.hds_loc_lob_abb  =rec.hds_loc_lob_abb;
                      --
                      commit;
                      --
                   exception
                      when others then
                        rollback to init_here;
                   end;
                   --
                   begin
                      --
                      update xxcus.xxcus_opn_fru_loc_all a
                      set a.hds_pri_fru_flag ='Y'
                      where 1 =1
                        and a.hds_loc_lob_abb =rec.hds_loc_lob_abb
                        and a.rowid =get_primary_fru_rec(idx).prim_fru_flag_rowid;
                      --
                      commit;
                      --
                      update xxcus.xxcus_opn_fru_loc_all a
                      set a.hds_pri_fru_flag ='N'
                      where 1 =1
                        and a.hds_loc_lob_abb =rec.hds_loc_lob_abb
                        and a.rowid <> get_primary_fru_rec(idx).prim_fru_flag_rowid;
                      --
                      commit;
                      --
                   exception
                      when others then
                        rollback;
                   end;
                   --
                  else --ignore and move to next lob_bu
                   Null;
                  end if;
                  --
                 end loop;
                --
               end if;
               --
              end loop;
              --
         exception
           when others then
            print_log('Error in pulling unique loc_bu before update of loc_bu_primary_ops_fru, msg ='||sqlerrm);
         end;
         --
         commit;
         --
         print_log('After update of location bu primary ops fru');
         --
         -- update of hds_prop_fru
         --
        -- Begin Ver 3.3
         --
         begin
          --
          savepoint init_here;
          --
          update xxcus.xxcus_opn_fru_loc_all
            set hds_prop_fru =opn_code||'-'||lc_fru;
            --
          commit;
          --
         exception
          when others then
           print_error_log('@Error in update of HDS_PROP_FRU field, Msg ='||sqlerrm);
           rollback to init_here;
         end;
         --
        -- End Ver 3.3
        /* --Ver 3.3
        --
         begin
          --
          savepoint init_here;
          --
          update xxcus.xxcus_opn_fru_loc_all
            set hds_prop_fru =opn_code||'-'||hds_loc_bu_pri_ops_fru
            where 1 =1
              and space_type !='Administrative'
              and hds_loc_bu_pri_ops_fru is not null;
          --
          commit;
          --
         exception
          when others then
           print_log('@Error in update of HDS_PROP_FRU field, Msg ='||sqlerrm);
           rollback to init_here;
         end;
         --
         */ --Ver 3.3
         --
         -- update of hds_prop_fru_pri_flag and hds_prop_fru_pri_ops columns
         --
         begin
          --
          savepoint init_here;
          --
          for rec in
           (
                select tbl.hds_prop_fru                           hds_prop_fru
                      ,tbl.space_tag                              space_tag
                      ,(
                        select meaning
                        from   fnd_lookup_values
                        where  1 =1
                        and  lookup_type ='PN_SPACE_TYPE'
                        and  to_number(nvl(tag, 999))
                               =tbl.space_tag
                        --and  enabled_flag ='Y' --Ver 3.3
                       )                                          space_type
                from
                (
                select hds_prop_fru
                      ,min(space_tag) space_tag
                from   xxcus.xxcus_opn_fru_loc_all
                where 1 =1
                  --and opn_code ='MD300'
                  --and space_type !='Administrative' --Ver 3.3
                  and hds_prop_fru is not null
                group by hds_prop_fru
                ) tbl
           )
          loop
           --
            begin
             --
             savepoint init_here;
             --
             update xxcus.xxcus_opn_fru_loc_all
              set hds_prop_fru_pri_flag =case when space_tag =rec.space_tag then 'Y' else 'N' end
                ,hds_prop_fru_pri_ops  =rec.space_type
             where 1 =1
               and hds_prop_fru =rec.hds_prop_fru;
             --
            exception
             when others then
              print_log('@Error in update of hds_prop_fru_pri_flag and hds_prop_fru_pri_ops fiedls, Msg ='||sqlerrm);
              rollback to init_here;
            end;
           --
          end loop;
          --
          commit;
          --
         exception
          when others then
           print_log('@Error in update of HDS_PROP_FRU field, Msg ='||sqlerrm);
           rollback to init_here;
         end;
         --
      -- end update of attributes for XXCUS.XXCUS_OPN_FRU_LOC_ALL table*
         /* --Ver 3.3
         -- update hds_prop_fru for administrative sections only
         --
         begin
          --
          savepoint init_here;
          --
          update xxcus.xxcus_opn_fru_loc_all
            set hds_prop_fru =opn_code||'-'||lc_fru
            where 1 =1
              --and space_type ='Administrative' --Ver  3.3 --Update for all rows where hds_pro_fru is blank.
              and hds_prop_fru is null;
          --
          commit;
          --
         exception
          when others then
           print_log('@Error in update of HDS_PROP_FRU field, Msg ='||sqlerrm);
           rollback to init_here;
         end;
         --
         */ --Ver 3.3
         -- update hds_pri_fru_flag if null.
         --
         begin
          --
          savepoint init_here;
          --
          update xxcus.xxcus_opn_fru_loc_all
            set hds_pri_fru_flag ='N'
            where 1 =1
              and hds_pri_fru_flag is null;
          --
          commit;
          --
         exception
          when others then
           print_log('@Error in update of hds_pri_fru_flag when blank, Msg ='||sqlerrm);
           rollback to init_here;
         end;
         --
         --
         -- update hds_prop_fru_pri_flag if null.
         --
         begin
          --
          savepoint init_here;
          --
          update xxcus.xxcus_opn_fru_loc_all
            set hds_prop_fru_pri_flag ='N'
            where 1 =1
              and hds_prop_fru_pri_flag is null;
          --
          commit;
          --
         exception
          when others then
           print_log('@Error in update of hds_prop_fru_pri_flag when blank, Msg ='||sqlerrm);
           rollback to init_here;
         end;
         --
                 -- Begin Ver 3.4
                 --
                 -- populate hds_fru_loc,  hds_feed_to_extended, hds_feed_to_external fields
                 --
                   begin
                     --
                     savepoint init_here;
                     --
                        update xxcus.xxcus_opn_fru_loc_all a
                        set    hds_fru_loc =lc_fru||'-'||bldg_land_code
                                ,hds_feed_to_extended =
                          case
                           when (a.hds_fru_section_current_flag ='Y' 
                           and (a.site_type IN ('Selling', 'Support') OR (a.space_type_lookup_code ='ADM' and a.hds_pri_fru_flag ='Y')) --Ver 3.9 
                           and a.loc_type_lookup_code ='OFFICE' 
                           and  a.hds_ops_status !='Inactive'
                           ) then 'Y'
                           else 'N'
                          end
                                ,hds_feed_to_external =
                                         -- Begin --Ver 3.7
                          case
                           when (
                                                 a.hds_fru_section_current_flag ='Y'
                                         and a.loc_type_lookup_code ='OFFICE'
                                         and a.hds_ops_status ='Active' --Ver 3.9
                                         --and a.hds_pri_fru_flag ='Y' --Ver 3.8
                                         and (a.site_type ='Selling' OR (a.space_type_lookup_code ='ADM' and a.hds_pri_fru_flag ='Y'))  --Ver 3.8
                                         and exists
                                          (
                                                 select '1'
                                                 from  xxcus.xxcus_opn_properties_all
                                                 where 1 =1
                                                       and opn_id =a.opn_id
                                                       and location_id =a.location_id
                                                       --and office_customer_site is null --Ver 3.8
                                          )
                                       ) then 'Y'
                                          -- End Ver 3.7
                           else 'N'
                          end;
                     --
                     commit;
                     --
                 exception
                     when others then
                       print_log('@update of hds_fru_loc,  hds_feed_to_extended, hds_feed_to_external fields, msg ='||sqlerrm);
                       rollback to init_here;
                 end;
                 -- End Ver 3.4
     else
      --
       print_log('Failed @Main SQL to insert XXCUS.XXCUS_OPN_FRU_LOC_ALL, message ='||sqlerrm);
      --
     end if;
     --
   else
       print_log ('Location: create_loc_contact_tables returned FALSE, Message: '||sqlerrm);
   end if;
      --
  else
   print_log ('Location: create_loc_feature_tables returned FALSE, Message: '||sqlerrm);
  end if;
  --
  -- Populate custom lease tenancies table.
  --
  print_log('Before extract of lease tenancies to custom table xxcus.xxcus_opn_lease_tenancies_all');
  --
  lease_tenancies;
  --
  print_log('After extract of lease tenancies to custom table xxcus.xxcus_opn_lease_tenancies_all');
  --
  print_log('Enter routine apex_secat_re_tbl_refresh to populate xxcus.xxcus_apex_secat_re_tbl table');
  --
  apex_secat_re_tbl_refresh;
  --
  print_log('Exit routine apex_secat_re_tbl_refresh to populate xxcus.xxcus_apex_secat_re_tbl table');
  --
     begin
      print_log('Begin routine xxcus_opn_lease_tab_insr');
      xxcus_opn_lease_tab_insr (p_extract_date =>sysdate, p_user_id =>fnd_global.user_id);
      print_log('End routine xxcus_opn_lease_tab_insr');
     exception
      when others then
       print_log ('@Populate lease TAB insurance table xxcus.xxcus_opn_lease_tab_insurance , msg ='||sqlerrm);
     end;
     --
     begin
      print_log('Begin routine xxcus_opn_lease_tab_rights');
      xxcus_opn_lease_tab_rights (p_extract_date =>sysdate, p_user_id =>fnd_global.user_id);
      print_log('End routine xxcus_opn_lease_tab_rights');
     exception
      when others then
       print_log ('@Populate lease TAB rights table xxcus.xxcus_opn_lease_tab_rights , msg ='||sqlerrm);
     end;
     --
     begin
      print_log('Begin routine xxcus_opn_lease_tab_oblig');
      xxcus_opn_lease_tab_oblig (p_extract_date =>sysdate, p_user_id =>fnd_global.user_id);
      print_log('End routine xxcus_opn_lease_tab_oblig');
     exception
      when others then
       print_log ('@Populate lease TAB obligations table xxcus.xxcus_opn_lease_tab_oblig , msg ='||sqlerrm);
     end;
     --
     begin
      print_log('Begin routine xxcus_opn_lease_tab_options');
      xxcus_opn_lease_tab_options (p_extract_date =>sysdate, p_user_id =>fnd_global.user_id);
      print_log('End routine xxcus_opn_lease_tab_options');
     exception
      when others then
       print_log ('@Populate lease TAB options table xxcus.xxcus_opn_lease_tab_options , msg ='||sqlerrm);
     end;
     --
     begin
      print_log('Begin routine xxcus_opn_lease_tab_notes');
      xxcus_opn_lease_tab_notes (p_extract_date =>sysdate, p_user_id =>fnd_global.user_id);
      print_log('End routine xxcus_opn_lease_tab_notes');
     exception
      when others then
       print_log ('@Populate lease TAB options table xxcus.xxcus_opn_lease_tab_notes , msg ='||sqlerrm);
     end;
     --
     -- Begin ver 3.5
     begin
      print_log('Begin routine extract_premises_and_ops');
      extract_premises_and_ops;
      print_log('End routine extract_premises_and_ops');
     exception
      when others then
       print_log ('@Routine extract_premises_and_ops , msg ='||sqlerrm);
     end;
     --
     --
     begin
      fnd_file.put_line(fnd_file.log, 'Begin update of loc_active_end_date column to blank when date is 12/31/4712');
      savepoint last_step;
      update xxcus.xxcus_opn_properties_all
      set loc_active_end_date =Null
      where 1 =1
            and to_char(loc_active_end_date, 'mm/dd/yyyy') ='12/31/4712';
      fnd_file.put_line(fnd_file.log, 'End update of loc_active_end_date column to blank when date is 12/31/4712');
      commit;
     exception
      when others then
       rollback to last_step;
       print_log ('@Error in update of loc_active_end_date column to blank when date is 12/31/4712, msg ='||sqlerrm);
     end;
     --
     begin
      fnd_file.put_line(fnd_file.log, 'Begin update of hds_bldg_land_max_end column to blank when date is 12/31/4712');
      savepoint last_step;
      update xxcus.xxcus_opn_properties_all
      set hds_bldg_land_max_end =Null
      where 1 =1
            and to_char(hds_bldg_land_max_end, 'mm/dd/yyyy') ='12/31/4712';
      fnd_file.put_line(fnd_file.log, 'End update of hds_bldg_land_max_end column to blank when date is 12/31/4712');
      commit;
     exception
      when others then
       rollback to last_step;
       print_log ('@Error in update of hds_bldg_land_max_end column to blank when date is 12/31/4712, msg ='||sqlerrm);
     end;
     --
     begin
      fnd_file.put_line(fnd_file.log, 'Begin update of scheduled_year_start_date column to blank when date is 12/31/4712');
      savepoint last_step;
      update xxcus.xxcus_opn_rer_locations_all
      set scheduled_year_start_date =Null
      where 1 =1
            and to_char(scheduled_year_start_date, 'mm/dd/yyyy') ='12/31/4712';
      fnd_file.put_line(fnd_file.log, 'End update of scheduled_year_start_date column to blank when date is 12/31/4712');
      commit;
     exception
      when others then
       rollback to last_step;
       print_log ('@Error in update of scheduled_year_start_date column to blank when date is 12/31/4712, msg ='||sqlerrm);
     end;
     --
     begin
      fnd_file.put_line(fnd_file.log, 'Begin update of lease_execution column to blank when date is 12/31/4712');
      savepoint last_step;
      update xxcus.xxcus_opn_rer_locations_all
      set lease_execution =Null
      where 1 =1
            and to_char(lease_execution, 'mm/dd/yyyy') ='12/31/4712';
      fnd_file.put_line(fnd_file.log, 'End update of lease_execution column to blank when date is 12/31/4712');
      commit;
     exception
      when others then
       rollback to last_step;
       print_log ('@Error in update of lease_execution column to blank when date is 12/31/4712, msg ='||sqlerrm);
     end;
     --
     begin
      fnd_file.put_line(fnd_file.log, 'Begin update of lease_commencement column to blank when date is 12/31/4712');
      savepoint last_step;
      update xxcus.xxcus_opn_rer_locations_all
      set lease_commencement =Null
      where 1 =1
            and to_char(lease_commencement, 'mm/dd/yyyy') ='12/31/4712';
      fnd_file.put_line(fnd_file.log, 'End update of lease_commencement column to blank when date is 12/31/4712');
      commit;
     exception
      when others then
       rollback to last_step;
       print_log ('@Error in update of lease_commencement column to blank when date is 12/31/4712, msg ='||sqlerrm);
     end;
     --
     begin
      fnd_file.put_line(fnd_file.log, 'Begin update of lease_termination column to blank when date is 12/31/4712');
      savepoint last_step;
      update xxcus.xxcus_opn_rer_locations_all
      set lease_termination =Null
      where 1 =1
            and to_char(lease_termination, 'mm/dd/yyyy') ='12/31/4712';
      fnd_file.put_line(fnd_file.log, 'End update of lease_termination column to blank when date is 12/31/4712');
      commit;
     exception
      when others then
       rollback to last_step;
       print_log ('@Error in update of lease_termination column to blank when date is 12/31/4712, msg ='||sqlerrm);
     end;
     --
     begin
      fnd_file.put_line(fnd_file.log, 'Begin update of lease_extn_end_date column to blank when date is 12/31/4712');
      savepoint last_step;
      update xxcus.xxcus_opn_rer_locations_all
      set lease_extn_end_date =Null
      where 1 =1
            and to_char(lease_extn_end_date, 'mm/dd/yyyy') ='12/31/4712';
      fnd_file.put_line(fnd_file.log, 'End update of lease_extn_end_date column to blank when date is 12/31/4712');
      commit;
     exception
      when others then
       rollback to last_step;
       print_log ('@Error in update of lease_extn_end_date column to blank when date is 12/31/4712, msg ='||sqlerrm);
     end;
     --
     begin
      fnd_file.put_line(fnd_file.log, 'Begin update of loc_active_end_date column to blank when date is 12/31/4712');
      savepoint last_step;
      update xxcus.xxcus_opn_rer_locations_all
      set loc_active_end_date =Null
      where 1 =1
            and to_char(loc_active_end_date, 'mm/dd/yyyy') ='12/31/4712';
      fnd_file.put_line(fnd_file.log, 'End update of loc_active_end_date column to blank when date is 12/31/4712');
      commit;
     exception
      when others then
       rollback to last_step;
       print_log ('@Error in update of loc_active_end_date column to blank when date is 12/31/4712, msg ='||sqlerrm);
     end;
     --
     -- End ver 3.5
     -- Begin Ver 3.6
     begin
      print_log('Begin routine fruloc_addnl_props_set1');
      fruloc_addnl_props_set1;
      print_log('End routine fruloc_addnl_props_set1');
     exception
      when others then
       print_log ('@Routine fruloc_addnl_props_set1 , msg ='||sqlerrm);
     end;
     --
     begin
      print_log('Begin routine fruloc_addnl_props_set2');
      fruloc_addnl_props_set2;
      print_log('End routine fruloc_addnl_props_set2');
     exception
      when others then
       print_log ('@Routine fruloc_addnl_props_set2 , msg ='||sqlerrm);
     end;
    --
     begin
      print_log('Begin routine fruloc_addnl_props_set3');
      fruloc_addnl_props_set3;
      print_log('End routine fruloc_addnl_props_set3');
     exception
      when others then
       print_log ('@Routine fruloc_addnl_props_set3 , msg ='||sqlerrm);
     end;
     --
     --End Ver 3.6
 exception
  when others then
   print_log('Error in xxcus_opn_master_extract.main, message ='||sqlerrm);
    l_err_msg := l_sec;
        l_err_msg := l_err_msg || ' ...Error_Stack...' ||
                     dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                     dbms_utility.format_error_backtrace();
        fnd_file.put_line(fnd_file.log, l_err_msg);
        l_err_callpoint := l_sec;
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => l_err_callpoint
                                            ,p_request_id        => fnd_global.conc_request_id
                                            ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                           dbms_utility.format_error_stack() ||
                                                                           ' Error_Backtrace...' ||
                                                                           dbms_utility.format_error_backtrace()
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => substr(l_err_msg
                                                                          ,1
                                                                          ,240)
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => l_module);
 end main;
 --
end xxcus_opn_master_extract;
/