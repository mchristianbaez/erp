-- RUN IN XXWC_DEV_ADMIN schema
-- This is the incremental version and does no DDL.
SET SERVEROUTPUT ON SIZE 1000000

GRANT INSERT, UPDATE, DELETE ON XXWC_DEV_ADMIN.bind_variable_code_temp to APPS;

DELETE from XXWC_DEV_ADMIN.bind_variable_code_temp;
commit;

INSERT INTO XXWC_DEV_ADMIN.bind_variable_code_temp select a.*, '' from gv$sqlarea a;
commit;

update XXWC_DEV_ADMIN.bind_variable_code_temp set sql_text_wo_constants = XXWC_DEV_ADMIN.remove_constants(sql_text);
commit;

commit;
/