--Report Name            : Open Sales Orders Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_OM_OPEN_ORDERS_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_OPEN_ORDERS_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Open Orders V','EXOOOV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_OPEN_ORDERS_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_LINE_STATUS',660,'Order Line Status','ORDER_LINE_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Line Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_AMOUNT',660,'Order Amount','ORDER_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Order Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SCHEDULE_SHIP_DATE',660,'Schedule Ship Date','SCHEDULE_SHIP_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Schedule Ship Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SALES_PERSON_NAME',660,'Sales Person Name','SALES_PERSON_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Person Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ZIP_CODE',660,'Zip Code','ZIP_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Zip Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SHIP_TO_CITY',660,'Ship To City','SHIP_TO_CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To City','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUSTOMER_JOB_NAME',660,'Customer Job Name','CUSTOMER_JOB_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Job Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','WAREHOUSE',660,'Warehouse','WAREHOUSE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Warehouse','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SHIPPING_METHOD',660,'Shipping Method','SHIPPING_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Shipping Method','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CREATED_BY',660,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Created By','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','QUOTE_NUMBER',660,'Quote Number','QUOTE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Quote Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_HEADER_STATUS',660,'Order Header Status','ORDER_HEADER_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Header Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','PAYMENT_TERMS',660,'Payment Terms','PAYMENT_TERMS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Terms','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','QTY',660,'Qty','QTY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Qty','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','REQUEST_DATE',660,'Request Date','REQUEST_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Request Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','USER_ITEM_DESCRIPTION',660,'User Item Description','USER_ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','User Item Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','DELIVERY_DOC_DATE',660,'Delivery Doc Date','DELIVERY_DOC_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Delivery Doc Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZPS_SHP_TO_PRT_ID',660,'Hzps Shp To Prt Id','HZPS_SHP_TO_PRT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Hzps Shp To Prt Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZCS_SHIP_TO_STE_ID',660,'Hzcs Ship To Ste Id','HZCS_SHIP_TO_STE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Hzcs Ship To Ste Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','MTP_ORGANIZATION_ID',660,'Mtp Organization Id','MTP_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Mtp Organization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','TRANSACTION_TYPE_ID',660,'Transaction Type Id','TRANSACTION_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Transaction Type Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SALESREP_ID',660,'Salesrep Id','SALESREP_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Salesrep Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUST_ACCT_SITE_ID',660,'Cust Acct Site Id','CUST_ACCT_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Acct Site Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_LINE_ID',660,'Order Line Id','ORDER_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Line Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_HEADER_ID',660,'Order Header Id','ORDER_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','LINE_CREATION_DATE',660,'Line Creation Date','LINE_CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Line Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','HEADER_STATUS',660,'Header Status','HEADER_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Header Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','PROCESS_ID',660,'Process Id','PROCESS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Process Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','TRANSACTION_PHASE_CODE',660,'Transaction Phase Code','TRANSACTION_PHASE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Transaction Phase Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','REGION',660,'Region','REGION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Region','','','','','');
--Inserting Object Components for EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_CUST_ACCOUNTS',660,'HZ_CUST_ACCOUNTS','HCA','HCA','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Stores Information About Customer Accounts.','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_PARTIES',660,'HZ_PARTIES','HZP','HZP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information About Parties Such As Organizations, P','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_OPEN_ORDERS_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','MTP','MTP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Control Options And Defaults','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_CUST_ACCT_SITES',660,'HZ_CUST_ACCT_SITES_ALL','HCAS_SHIP_TO','HCAS_SHIP_TO','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Stores All Customer Account Sites Across All Opera','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_CUST_ACCOUNTS','HCA',660,'EXOOOV.CUST_ACCOUNT_ID','=','HCA.CUST_ACCOUNT_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_PARTIES','HZP',660,'EXOOOV.PARTY_ID','=','HZP.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_OPEN_ORDERS_V','MTL_PARAMETERS','MTP',660,'EXOOOV.MTP_ORGANIZATION_ID','=','MTP.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_CUST_ACCT_SITES','HCAS_SHIP_TO',660,'EXOOOV.CUST_ACCT_SITE_ID','=','HCAS_SHIP_TO.CUST_ACCT_SITE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Open Sales Orders Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Open Sales Orders Report
xxeis.eis_rsc_ins.lov( 660,'select  RS.Name,SALESREP_ID  from  RA_SALESREPS RS
WHERE  RS.NAME is not null','','OM SALES REP','This gives the sales representative name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct ott.name order_type,ott.description description,ott.transaction_type_id order_type_id from oe_transaction_types_tl ott','','OM ORDER TYPE','This gives the Order Type','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select meaning ship_method,description from FND_LOOKUP_VALUES_vl where lookup_type=''SHIP_METHOD''','','OM SHIP METHOD','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT ppf.full_name  created_by, fu.user_name user_name
  FROM FND_USER fu, per_people_f ppf
 WHERE TRUNC(SYSDATE) BETWEEN fu.START_DATE AND NVL(fu.end_date, hr_general.end_of_time)
   and TRUNC(SYSDATE) BETWEEN ppf.effective_start_date AND ppf.effective_end_date
   and fu.employee_id = ppf.person_id (+)
   and exists
          (select 1
             from fnd_user_resp_groups furg,
                  fnd_responsibility_vl fr
            where furg.responsibility_id   = fr.responsibility_id
              and fu.user_id = furg.user_id
              and fr.responsibility_name like ''XXEIS%'')','','Created By Lov','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'Select  name, description from ra_terms_vl','','WC OM Payment Terms','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT DISTINCT LOCATION FROM hz_cust_site_uses','','OM Customer Job Name LOV','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT meaning Status
  FROM OE_LOOKUPS lv
 WHERE lookup_type = ''LINE_FLOW_STATUS''
  AND lookup_code not in(''CLOSED'', ''CANCELLED'')','','WC Open Order Line Status','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT NVL(cust_acct.account_name,party.party_name) customer_name,
  cust_acct.account_number,
  cust_acct.cust_account_id
FROM hz_parties party,
  hz_cust_accounts cust_acct
WHERE cust_acct.party_id = party.party_id
AND party.status         =''A''','','XXOM CUSTOMER NAME','This gives the Customer Name','ANONYMOUS',NULL,'Y','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT cust_acct.account_number,
  NVL(cust_acct.account_name,party.party_name) customer_name,
  cust_acct.cust_account_id
FROM hz_parties party,
  hz_cust_accounts cust_acct
WHERE cust_acct.party_id = party.party_id
AND party.status         =''A''','','XXOM CUSTOMER NUMBER','This gives the Customer Number','ANONYMOUS',NULL,'Y','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT oel.meaning Status
FROM oe_lookups oel
WHERE oel.lookup_type    = ''FLOW_STATUS''
AND oel.lookup_code NOT IN(''CANCELLED'', ''CLOSED'')','','XXWC ORDER FLOW STATUS CODE','','ANONYMOUS',NULL,'Y','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT DISTINCT mp.attribute9 region
FROM mtl_parameters mp
WHERE mp.attribute9 IS NOT NULL
ORDER BY 1','','EIS XXWC Region LOV','This LOV Lists all region details.','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Open Sales Orders Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Open Sales Orders Report
xxeis.eis_rsc_utility.delete_report_rows( 'Open Sales Orders Report',660 );
--Inserting Report - Open Sales Orders Report
xxeis.eis_rsc_ins.r( 660,'Open Sales Orders Report','','Open orders report by customer, by job, by salesperson, by created by, by shipping method, by promise date.','','','','SA059956','EIS_XXWC_OM_OPEN_ORDERS_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,Pivot Excel,','N','','','','','','N','','US','','','','','','','  1#$# or 2#$# ','','','','','','','','','');
--Inserting Report Columns - Open Sales Orders Report
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'ORDER_HEADER_STATUS','Order Header Status','Order Header Status','','','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'PAYMENT_TERMS','Payment Terms','Payment Terms','','','default','','17','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'ITEM_NUMBER','Item Number','Item Number','','','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'QTY','Qty','Qty','','~~~','default','','15','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'REQUEST_DATE','Requested Date','Request Date','','','default','','8','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','18','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','default','','7','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'ORDER_AMOUNT','Order Amount','Order Amount','','~T~D~2','default','','21','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'ORDER_LINE_STATUS','Order Line Status','Order Line Status','','','default','','16','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'ORDER_TYPE','Order Type','Order Type','','','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'SCHEDULE_SHIP_DATE','Schedule Ship Date','Schedule Ship Date','','','default','','25','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'SHIPPING_METHOD','Shipping Method','Shipping Method','','','default','','22','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'ZIP_CODE','Zip Code','Zip Code','','','default','','24','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'WAREHOUSE','Warehouse','Warehouse','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'SHIP_TO_CITY','Ship To City','Ship To City','','','default','','23','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'CUSTOMER_JOB_NAME','Customer Job Name','Customer Job Name','','','default','','20','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','19','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'SALES_PERSON_NAME','Sales Person Name','Sales Person Name','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'QUOTE_NUMBER','Quote Number','Quote Number','','~~~','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'CREATED_BY','Created By','Created By','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'USER_ITEM_DESCRIPTION','User Item Description','User Item Description','','','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'DELIVERY_DOC_DATE','Delivery Doc Date','Delivery Doc Date','','','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Open Sales Orders Report',660,'REGION','Region','Region','','','','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','US','','');
--Inserting Report Parameters - Open Sales Orders Report
xxeis.eis_rsc_ins.rp( 'Open Sales Orders Report',660,'Job Name','Job Name','CUSTOMER_JOB_NAME','IN','OM Customer Job Name LOV','','VARCHAR2','N','Y','6','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Sales Orders Report',660,'Customer Name','Customer Name','CUSTOMER_NAME','IN','XXOM CUSTOMER NAME','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Sales Orders Report',660,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','XXOM CUSTOMER NUMBER','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Sales Orders Report',660,'Ordered Date From','Ordered Date From','ORDERED_DATE','>=','','','DATE','N','Y','12','Y','N','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_OM_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Sales Orders Report',660,'Order Type','Order Type','ORDER_TYPE','IN','OM ORDER TYPE','','VARCHAR2','N','Y','3','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Sales Orders Report',660,'Sales Person','Sales Person','SALES_PERSON_NAME','IN','OM SALES REP','','VARCHAR2','N','Y','7','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Sales Orders Report',660,'Schedule Ship Date From','Schedule Ship Date From','SCHEDULE_SHIP_DATE','>=','','','DATE','N','Y','10','Y','N','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_OM_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Sales Orders Report',660,'Shipping Method','Shipping Method','SHIPPING_METHOD','IN','OM SHIP METHOD','','VARCHAR2','N','Y','9','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Sales Orders Report',660,'Warehouse','Warehouse','WAREHOUSE','IN','OM Warehouse All','','VARCHAR2','N','Y','2','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Sales Orders Report',660,'Created By','Created By','CREATED_BY','IN','Created By Lov','','VARCHAR2','N','Y','8','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Sales Orders Report',660,'Ordered Date To','Ordered Date To','ORDERED_DATE','<=','','','DATE','N','Y','13','Y','N','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_OM_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Sales Orders Report',660,'Schedule Ship Date To','Schedule Ship Date To','SCHEDULE_SHIP_DATE','<=','','','DATE','N','Y','11','Y','N','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_OM_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Sales Orders Report',660,'Order Header Status','Order Header Status','ORDER_HEADER_STATUS','IN','XXWC ORDER FLOW STATUS CODE','','VARCHAR2','N','Y','15','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Sales Orders Report',660,'Payment Terms','Payment Terms','PAYMENT_TERMS','IN','WC OM Payment Terms','','VARCHAR2','N','Y','16','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Sales Orders Report',660,'Order Line Status','Order Line Status','ORDER_LINE_STATUS','IN','WC Open Order Line Status','','VARCHAR2','N','Y','14','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Open Sales Orders Report',660,'Region','Region','','','EIS XXWC Region LOV','','VARCHAR2','N','Y','1','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_OPEN_ORDERS_V','','','US','');
--Inserting Dependent Parameters - Open Sales Orders Report
--Inserting Report Conditions - Open Sales Orders Report
xxeis.eis_rsc_ins.rcnh( 'Open Sales Orders Report',660,'ORDER_LINE_STATUS IN :Order Line Status ','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','ORDER_LINE_STATUS','','Order Line Status','','','','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','','','IN','Y','Y','','','','','1',660,'Open Sales Orders Report','ORDER_LINE_STATUS IN :Order Line Status ');
xxeis.eis_rsc_ins.rcnh( 'Open Sales Orders Report',660,'PAYMENT_TERMS IN :Payment Terms ','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','PAYMENT_TERMS','','Payment Terms','','','','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','','','IN','Y','Y','','','','','1',660,'Open Sales Orders Report','PAYMENT_TERMS IN :Payment Terms ');
xxeis.eis_rsc_ins.rcnh( 'Open Sales Orders Report',660,'Free Text ','FREE_TEXT','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','and PROCESS_ID = :SYSTEM.PROCESS_ID','1',660,'Open Sales Orders Report','Free Text ');
xxeis.eis_rsc_ins.rcnh( 'Open Sales Orders Report',660,'EXOOOV.CUSTOMER_NAME IN Customer Name','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_NAME','','Customer Name','','','','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','','','IN','Y','Y','','','','','1',660,'Open Sales Orders Report','EXOOOV.CUSTOMER_NAME IN Customer Name');
xxeis.eis_rsc_ins.rcnh( 'Open Sales Orders Report',660,'EXOOOV.CUSTOMER_NUMBER IN Customer Number','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_NUMBER','','Customer Number','','','','','EIS_XXWC_OM_OPEN_ORDERS_V','','','','','','IN','Y','Y','','','','','1',660,'Open Sales Orders Report','EXOOOV.CUSTOMER_NUMBER IN Customer Number');
--Inserting Report Sorts - Open Sales Orders Report
xxeis.eis_rsc_ins.rs( 'Open Sales Orders Report',660,'WAREHOUSE','ASC','SA059956','1','');
xxeis.eis_rsc_ins.rs( 'Open Sales Orders Report',660,'ORDER_NUMBER','ASC','SA059956','2','');
--Inserting Report Triggers - Open Sales Orders Report
xxeis.eis_rsc_ins.rt( 'Open Sales Orders Report',660,'begin
xxeis.eis_xxwc_om_open_sales_ord_pkg.OPEN_SALES_ORDER_PRE_PROC(
p_process_id          => :SYSTEM.PROCESS_ID,
p_org_id                => :Warehouse,
p_order_type          => :Order Type,
p_cust_name          =>   :Customer Name,
p_cust_num            => :Customer Number,
p_cust_job_name    => :Job Name,
p_salesrep_name    =>    :Sales Person,
P_CREATED_BY                =>  :Created By,
p_shipping_method          =>  :Shipping Method,
p_schedule_date_from     => :Schedule Ship Date From,
p_schedule_date_to         => :Schedule Ship Date To,
p_ordered_date_from      =>  :Ordered Date From,
p_ordered_date_to          =>  :Ordered Date To,
p_order_line_status         => :Order Line Status,
p_order_header_status    =>  :Order Header Status,
p_payment_terms      =>   :Payment Terms,
p_region			 => :Region);
end;','B','Y','SA059956','BQ');
--inserting report templates - Open Sales Orders Report
--Inserting Report Portals - Open Sales Orders Report
--inserting report dashboards - Open Sales Orders Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Open Sales Orders Report','660','EIS_XXWC_OM_OPEN_ORDERS_V','EIS_XXWC_OM_OPEN_ORDERS_V','N','');
--inserting report security - Open Sales Orders Report
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','660','','ORDER_MGMT_SUPER_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','701','','CLN_OM_3A6_ADMINISTRATOR',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','20005','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','','LC053655','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','','10010432','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','','RB054040','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','','RV003897','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','','SS084202','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','','SO004816','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','660','','XXWC_AO_OEENTRY_PO_RPT',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','660','','XXWC_AO_OEENTRY_REC',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','660','','XXWC_AO_OEENTRY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Open Sales Orders Report','','SA059956','',660,'SA059956','','','');
--Inserting Report Pivots - Open Sales Orders Report
xxeis.eis_rsc_ins.rpivot( 'Open Sales Orders Report',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Sales Orders Report',660,'Pivot','ORDER_AMOUNT','DATA_FIELD','SUM','','1','','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Sales Orders Report',660,'Pivot','CREATED_BY','ROW_FIELD','','','1','','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Sales Orders Report',660,'Pivot','CUSTOMER_NAME','ROW_FIELD','','','2','','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Sales Orders Report',660,'Pivot','ORDER_NUMBER','ROW_FIELD','','','3','','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
xxeis.eis_rsc_ins.rpivot( 'Open Sales Orders Report',660,'Region','2','1,0|1,2,0','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Region
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Sales Orders Report',660,'Region','ORDER_AMOUNT','DATA_FIELD','SUM','','1','','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Sales Orders Report',660,'Region','REGION','ROW_FIELD','','','1','1|2','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Open Sales Orders Report',660,'Region','ORDER_NUMBER','ROW_FIELD','','','2','1|2','','');
--Inserting Report Summary Calculation Columns For Pivot- Region
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Open Sales Orders Report
xxeis.eis_rsc_ins.rv( 'Open Sales Orders Report','','Open Sales Orders Report','SA059956','23-MAR-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
