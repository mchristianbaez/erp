  /*
  ===========================================================================
    Module: Inventory
    Type: Custom View 
    PURPOSE: To improve performance to item search in Clone Parts form.
    HISTORY
  ==============================================================================================================================
      VERSION        DATE          AUTHOR(S)          DESCRIPTION                                TICKET
      -------        -----------   ---------------    ------------------------------            ------------------------------                          
       1.1           27-Feb-2016   P.Vamshidhar       Clone Parts Process Speed Improvement     TMS 20160209-00146           
  */ 
CREATE OR REPLACE VIEW APPS.XXWC_INV_MAST_ITEMS_CLONE_VW
AS
   (SELECT inventory_item_id,
           segment1,
           description,
           primary_uom_code
      FROM mtl_system_items a
     WHERE     a.organization_id = 222
           AND inventory_item_status_code NOT IN ('Inactive', 'Discontinu')
           AND inventory_item_status_code NOT LIKE 'New%');