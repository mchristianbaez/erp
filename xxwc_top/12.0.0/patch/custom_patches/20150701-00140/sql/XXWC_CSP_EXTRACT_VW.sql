   /*************************************************************************
   *   $Header XXWC_CSP_EXTRACT_VW $
   *   Module Name: XXWC_CSP_EXTRACT_VW
   *
   *   PURPOSE:   This View is used in CSP extract
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        08/01/2014  Gopi Damuluri             Initial Version   
   *   2.0        06/25/2015  Manjula Chellappan        TMS# 20150624-00229 - CSP Enhancement Bundle - Item #1 
   * ***************************************************************************/
-- 'MASTER'  -- $$$$$$$$$
CREATE OR REPLACE VIEW APPS.XXWC_CSP_EXTRACT_VW
    AS
SELECT DISTINCT customer_name
||'|'||customer_number
||'|'||party_site_number
||'|'||location
||'|'||salesrep_name
||'|'||salesrep_id
||'|'||price_type
||'|'||organization_code 
||'|'||order_gross_margin
||'|'||incompatability
||'|'||agreement_id
||'|'||agreement_line_id
||'|'||agreement_type
||'|'||vq_number 
||'|'||item_attribute
||'|'||item_number
||'|'||item_description
||'|'||start_date
||'|'||end_date
||'|'||list_price
||'|'||modifier_type
||'|'||modifier_value
||'|'||selling_price
||'|'||special_cost
||'|'||vendor_number  -- ***
||'|'||average_cost
||'|'||gross_margin
||'|'||last_updated_by  -- ***
||'|'||last_update_date
||'|'||latest_rec_flag
||'|'||modifier_name
||'|'||sale_amount_last_6months
||'|'||units_sold_in_last_6months
||'|'||cost_amount_last_6months
||'|'||last_purchase_date
||'|'||creation_date
||'|'||revision_number
||'|'||qp_modifier_type  -- Added for Revision 2.0 
||'|'||qp_modifier_value -- Added for Revision 2.0 
 REC_LINE
, ORG_ID
  FROM xxwc.xxwc_qp_ool_gtt_tbl ool
 WHERE 1 = 1
   AND (  NVL(end_date,TRUNC(SYSDATE+1)) >= TRUNC(sysdate) OR 
          (                      end_date < TRUNC(sysdate) AND SALE_AMOUNT_LAST_6MONTHS >0 )
       )
--ORDER BY price_type, agreement_id
;