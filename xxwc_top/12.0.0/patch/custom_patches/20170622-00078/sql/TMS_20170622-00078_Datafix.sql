 /****************************************************************************************************
  Script: TMS_20170622-00078_Datafix.sql
  Description: Costing error -CST_MATCH_TXFR_CG_TXFR_ORG
  HISTORY
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  -- 1.0     22-JUN-2017   P.Vamshidhar       TMS#20170622-00078 - Costing error -CST_MATCH_TXFR_CG_TXFR_ORG
  *****************************************************************************************************/
SET SERVEROUT ON
  
BEGIN

DBMS_OUTPUT.PUT_LINE('Before Update');

UPDATE MTL_MATERIAL_TRANSACTIONS MMT
   SET TRANSFER_COST_GROUP_ID =
          (SELECT default_cost_group_id
             FROM MTL_SECONDARY_INVENTORIES
            WHERE     ORGANIZATION_ID = 318
                  AND SECONDARY_INVENTORY_NAME = 'General'),
       costed_flag = 'N',
       ERROR_CODE = NULL,
       error_explanation = NULL,
       transaction_group_id = NULL,
       transaction_set_id = NULL
 WHERE     MMT.TRANSACTION_ID = 592204573
       AND MMT.COSTED_FLAG = 'E'
       AND MMT.ORGANIZATION_ID = 548
       AND NOT EXISTS
              (SELECT 1
                 FROM MTL_TRANSACTION_ACCOUNTS MTA
                WHERE MMT.TRANSACTION_ID = MTA.TRANSACTION_ID); 
				
DBMS_OUTPUT.PUT_LINE('After Update - Records: '||SQL%ROWCOUNT);
COMMIT;
DBMS_OUTPUT.PUT_LINE('Commit completed');
EXCEPTION
WHEN OTHERS THEN
ROLLBACK;
DBMS_OUTPUT.PUT_LINE('Error Occred '||SUBSTR(SQLERRM,1,250));
END;
/
