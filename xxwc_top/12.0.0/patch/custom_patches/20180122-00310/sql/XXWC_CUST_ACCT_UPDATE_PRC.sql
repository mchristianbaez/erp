CREATE OR REPLACE PROCEDURE XXWC_CUST_ACCT_UPDATE (
   P_CUST_ACCOUNT_ID   IN NUMBER,
   P_STATUS            IN VARCHAR2)
IS
   /*******************************************************************************
     * Procedure:   XXWC_CUST_ACCT_UPDATE
     =============================================================================================================
     VERSION DATE               AUTHOR(S)       DESCRIPTION
     ------- -----------------  --------------- ------------------------------------------------------------------
     1.0     23-Jan-2018        P.Vamshidhar    TMS#20180122-00310 - Credit Online Application API - bug fix
                                                Initial creation of the procedure
     *************************************************************************************************************/
   PRAGMA AUTONOMOUS_TRANSACTION;
   ln_count          NUMBER := 0;
   pcustaccountrec   hz_cust_account_v2pub.cust_account_rec_type;
   o_ret_status      VARCHAR2 (100);
   o_msg_count       NUMBER;
   o_msg_data        VARCHAR2 (32767);

   CURSOR CUR_CUST
   IS
      SELECT OBJECT_VERSION_NUMBER, STATUS
        FROM HZ_CUST_ACCOUNTS
       WHERE CUST_ACCOUNT_ID = P_CUST_ACCOUNT_ID;
BEGIN
   FOR REC_CUST IN CUR_CUST
   LOOP
      pcustaccountrec := NULL;

      IF P_STATUS = 'A' AND REC_CUST.STATUS = 'I'
      THEN
         pcustaccountrec.status := 'A';
      ELSIF P_STATUS = 'I' AND REC_CUST.STATUS = 'A'
      THEN
         pcustaccountrec.status := 'I';
      END IF;

      IF P_STATUS IN ('A', 'I') AND pcustaccountrec.status IS NOT NULL
      THEN

         pcustaccountrec.cust_account_id := P_CUST_ACCOUNT_ID;

         HZ_CUST_ACCOUNT_V2PUB.update_cust_account (
            p_init_msg_list           => 'T',
            p_cust_account_rec        => pcustaccountrec,
            p_object_version_number   => REC_CUST.OBJECT_VERSION_NUMBER,
            x_return_status           => o_ret_status,
            x_msg_count               => o_msg_count,
            x_msg_data                => o_msg_data);
         COMMIT;
      END IF;
   END LOOP;
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/