/*
Task ID 20180109-00156 - Stuck direct order lines
Created by Vamshi.
*/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
DBMS_OUTPUT.put_line ('Before update');

UPDATE po_requisitions_interface_all
SET request_id = NULL, process_flag = NULL
WHERE transaction_id IN (SELECT transaction_id
FROM po_requisitions_interface_all
WHERE INTERFACE_SOURCE_CODE = 'ORDER ENTRY'
AND process_flag = 'IN PROCESS'
and request_id IN (197864106,197839267)); 

DBMS_OUTPUT.put_line ('Records updated1-' || SQL%ROWCOUNT);

COMMIT;
EXCEPTION
WHEN OTHERS
THEN
DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
END;
/