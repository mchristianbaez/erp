/*************************************************************************
  $Header datafix_TMS_20160715_00234.sql $
  Module Name: TMS_20160715_00234


  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
   1.0       05-APR-2017  Niraj K Ranjan        TMS#20160715_00234   TRANSFER STUCK IN SHIPPED STATUS - 22873606
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160715_00234   , Before Update');

   update apps.oe_order_headers_all
   set booked_flag='Y',
       flow_status_Code='CLOSED'
   where header_id=13408772;

   DBMS_OUTPUT.put_line (
         'TMS: 20160715_00234  Sales order updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160715_00234    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160715_00234 , Errors : ' || SQLERRM);
END;
/
