/*************************************************************************
  $Header TMS_20180227-00268_PURGE_MTL_SYSTEM_ITEMS_INTERFACE.sql $
  Module Name: TMS_20180227-00268 Data Fix Script

  PURPOSE: Datafix for purging the data in MTL_SYSTEM_ITEMS_INTERFACE table

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        28-FEB-2018  Pattabhi Avula         TMS#20180227-00268

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE

	CURSOR cur_mtl_int_err
   IS
      SELECT ROWID 
	    FROM inv.MTL_INTERFACE_ERRORS MIE 
	   WHERE MIE.TABLE_NAME='MTL_SYSTEM_ITEMS_INTERFACE' 
	   AND MIE.ERROR_MESSAGE IS NOT NULL
       AND MIE.LAST_UPDATE_DATE <= SYSDATE - 7 
       AND EXISTS (SELECT 1 FROM APPS.MTL_SYSTEM_ITEMS_INTERFACE MSII WHERE MSII.TRANSACTION_ID = MIE.TRANSACTION_ID AND PROCESS_FLAG IN (3,4) AND LAST_UPDATE_DATE <= SYSDATE - 7);



   CURSOR cur_mtl_int
   IS
      SELECT rowid FROM inv.MTL_SYSTEM_ITEMS_INTERFACE 
               WHERE  PROCESS_FLAG IN (3,4) AND LAST_UPDATE_DATE <= SYSDATE - 7;
			     
	  
	  TYPE rec_Table1 IS TABLE OF cur_mtl_int%ROWTYPE
      INDEX BY PLS_INTEGER;
	  
	   TYPE rec_Table2 IS TABLE OF cur_mtl_int_err%ROWTYPE
      INDEX BY PLS_INTEGER;

	 
   WORKING_REC_TABLE_1   REC_TABLE1;
   working_rec_table_2   rec_Table2;
   
   ln_counter1 NUMBER:=0;
   ln_counter2 NUMBER:=0;

BEGIN
      	  
   
   OPEN cur_mtl_int_err;

   LOOP
      FETCH cur_mtl_int_err BULK COLLECT INTO working_rec_table_2 LIMIT 50000;
	  
	        DBMS_OUTPUT.put_line ('working_rec_table_2 count '||working_rec_table_2.COUNT);

           ln_counter2:=ln_counter2+working_rec_table_2.COUNT;

      FORALL i IN working_rec_table_2.FIRST .. working_rec_table_2.LAST
         DELETE /*+ PARALLEL */  FROM inv.MTL_INTERFACE_ERRORS 
               WHERE ROWID = working_rec_table_2 (i).ROWID;

      
    COMMIT;
      WORKING_REC_TABLE_2.DELETE;
	  
	  EXIT WHEN cur_mtl_int_err%NOTFOUND;
   END LOOP;

   CLOSE cur_mtl_int_err;
    DBMS_OUTPUT.put_line ('Total Records Deleted in table MTL_INTERFACE_ERRORS -'||ln_counter2);
    DBMS_OUTPUT.put_line ('Process MTL_INTERFACE_ERRORS end'||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));   	  


      DBMS_OUTPUT.put_line ('Process MTL_INTERFACE TABLES Purge begin '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));

   OPEN cur_mtl_int;

   LOOP
      FETCH cur_mtl_int BULK COLLECT INTO working_rec_table_1 LIMIT 50000;
	  
	  DBMS_OUTPUT.put_line ('working_rec_table_1 count '||working_rec_table_1.COUNT);
	  ln_counter1:=ln_counter1+working_rec_table_1.COUNT;

      FORALL i IN working_rec_table_1.FIRST .. working_rec_table_1.LAST
         DELETE /*+ PARALLEL */  FROM inv.MTL_SYSTEM_ITEMS_INTERFACE
          WHERE ROWID = working_rec_table_1 (i).rowid;

    COMMIT;
      working_rec_table_1.delete;
	  EXIT WHEN cur_mtl_int%NOTFOUND;
   END LOOP;


   CLOSE cur_mtl_int;
   
      DBMS_OUTPUT.put_line ('Total Records Deleted in table MTL_SYSTEM_ITEMS_INTERFACE -'||ln_counter1);
      DBMS_OUTPUT.put_line ('Process MTL_SYSTEM_ITEMS_INTERFACE end '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));   
  
   
   EXCEPTION
   WHEN OTHERS
   THEN
      IF cur_mtl_int%ISOPEN
      THEN
         CLOSE CUR_MTL_INT;
      END IF;
	  
	  IF cur_mtl_int_err%ISOPEN
      THEN
         CLOSE cur_mtl_int_err;
      END IF;
	  
	        ROLLBACK;
      DBMS_OUTPUT.put_line ('Exception '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS'));   	  	  
      DBMS_OUTPUT.put_line ('Unable to delete the records ' || SQLERRM);
END;
/