/*************************************************************************
  $Header TMS_20180907-00005_B2B_DATAFIX.sql $
  Module Name: TMS_20180907-00005_B2B_DATAFIX

  PURPOSE: Load testing

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        07-SEP-2018  Pattabhi Avula         TMS#20180907-00005

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20180907-00005   , Before Update');

UPDATE xxwc.xxwc_b2b_so_hdrs_stg_tbl 
   SET process_flag = 'E'
      ,error_message = 'Check if the Customer is set up as B2B'
 WHERE customer_po_number = '4501221205'
   AND process_flag = 'N'
   AND error_message IS NULL;
   
      DBMS_OUTPUT.put_line (
         'TMS: 20180907-00005 xxwc_b2b_so_hdrs_stg_tbl updated (Expected:1): '
      || SQL%ROWCOUNT);

UPDATE xxwc.xxwc_b2b_so_lines_stg_tbl 
   SET process_flag = 'E'
      ,error_message = 'Check if the Customer is set up as B2B'
 WHERE customer_po_number = '4501221205'
   AND process_flag = 'N'
   AND error_message IS NULL;
   
    DBMS_OUTPUT.put_line (
         'TMS: 20180907-00005 xxwc_b2b_so_lines_stg_tbl updated (Expected:1): '
      || SQL%ROWCOUNT);
	  
	  COMMIT;
   
   UPDATE xxwc.xxwc_b2b_so_hdrs_stg_tbl 
   SET process_flag = 'E'
      ,error_message = 'Check if the Customer is set up as B2B'
 WHERE customer_po_number = '52491'
   AND process_flag = 'N'
   AND error_message IS NULL;
   
   DBMS_OUTPUT.put_line (
         'TMS: 20180907-00005 xxwc_b2b_so_hdrs_stg_tbl updated (Expected:1): '
      || SQL%ROWCOUNT);

   
UPDATE xxwc.xxwc_b2b_so_lines_stg_tbl 
   SET process_flag = 'E'
      ,error_message = 'Check if the Customer is set up as B2B'
 WHERE customer_po_number = '52491'
   AND process_flag = 'N'
   AND error_message IS NULL;

-- Expected to update on row

   DBMS_OUTPUT.put_line (
         'TMS: 20180907-00005 xxwc_b2b_so_lines_stg_tbl (Expected:3): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20180907-00005   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20180907-00005, Errors : ' || SQLERRM);
END;
/