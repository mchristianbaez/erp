   /*****************************************************************************************************************************************
   -- File Name: ALTER_XXWC_CHG_MTL_CROSS_REF.sql
   --
   -- PROGRAM TYPE: SQL Script to alter table by adding 2 columns
   -- TMS#20151201-00053 - PCAM error page displayed editing xref owner field
   ******************************************************************************************************************************************/

ALTER TABLE XXWC.XXWC_CHG_MTL_CROSS_REF ADD (MFG_DELETE_FLAG  VARCHAR2 (100 BYTE),BAR_DELETE_FLAG VARCHAR2(100));
/
