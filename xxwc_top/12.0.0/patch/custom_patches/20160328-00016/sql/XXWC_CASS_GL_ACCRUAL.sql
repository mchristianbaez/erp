 /*************************************************************************

   **************************************************************************
     $Header XXWC_CASS_GL_ACCRUAL $
     Module Name: XXWC_CASS_GL_ACCRUAL.sql

     PURPOSE:   This script creates staging table for CASS interface

     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/03/2016  Neha Saini         Initial Build - TMS# 20160328-00016

   /*************************************************************************/
DROP TABLE XXWC.XXWC_CASS_GL_ACCRUAL CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWC_CASS_GL_ACCRUAL
(
  NOTIFICATION_DATE       VARCHAR2(500 BYTE),
  CARRIER_NAME            VARCHAR2(500 BYTE),
  SHIP_DATE               VARCHAR2(500 BYTE),
  INVOICE_DATE            VARCHAR2(500 BYTE),
  CARRIER_PRO_NUMBER      VARCHAR2(500 BYTE),
  SHIPMENT_NUMBER         VARCHAR2(500 BYTE),
  SHIPMENT_MODE           VARCHAR2(500 BYTE),
  MOVEMENT_TYPE           VARCHAR2(500 BYTE),
  ORIGIN_NAME             VARCHAR2(500 BYTE),
  ORIGIN_CITY             VARCHAR2(500 BYTE),
  ORIGIN_STATE            VARCHAR2(500 BYTE),
  ORIGIN_POSTAL_CODE      VARCHAR2(500 BYTE),
  ORIGIN_CC               VARCHAR2(500 BYTE),
  DESTINATION_NAME        VARCHAR2(500 BYTE),
  DESTINATION_CITY        VARCHAR2(500 BYTE),
  DESTINATION_STATE       VARCHAR2(500 BYTE),
  DESTINATION_POSTAL_CODE VARCHAR2(500 BYTE),
  DESTINATION_CC          VARCHAR2(500 BYTE),
  GL_ACCOUNT_CODE         VARCHAR2(500 BYTE),
  GL_STRING               VARCHAR2(500 BYTE),
  GL1                     VARCHAR2(500 BYTE),
  GL2                     VARCHAR2(500 BYTE),
  BILLED_AMOUNT           VARCHAR2(500 BYTE),
  PAID_AMOUNT             VARCHAR2(500 BYTE),
  GST                     VARCHAR2(500 BYTE),
  HST                     VARCHAR2(500 BYTE),
  QST                     VARCHAR2(500 BYTE),
  FSC                     VARCHAR2(500 BYTE),
  LINE_HAUL               VARCHAR2(500 BYTE),
  OTHER                   VARCHAR2(500 BYTE),
  FISCAL_MONTH            VARCHAR2(500 BYTE),
  AUDIT_MATCH             VARCHAR2(500 BYTE),
  SHIPMENT_ESTIMATE       VARCHAR2(500 BYTE),
  HDS_LINE_HAUL           VARCHAR2(500 BYTE),
  HDS_FSC                 VARCHAR2(500 BYTE),
  HDS_ACCESSORIALS        VARCHAR2(500 BYTE),
  CUSTOMER_NAME           VARCHAR2(500 BYTE),
  SALES_ORDER_NUMBER      VARCHAR2(500 BYTE),
  VENDOR_NAME             VARCHAR2(500 BYTE),
  PURCHASE_ORDER_NUMBER   VARCHAR2(500 BYTE),
  SENDER_OR_RECEIVER      VARCHAR2(500 BYTE),
  RECEIVING_DATE          VARCHAR2(500 BYTE),
  PRO_RECEIVED_DATE       VARCHAR2(500 BYTE),
  SCHEDULED_PAY_DATE      VARCHAR2(500 BYTE),
  INVOICE_DUE_DATE        VARCHAR2(500 BYTE),
  BILL_OF_LADING_NO       VARCHAR2(500 BYTE),
  BILLED_WEIGHT           VARCHAR2(500 BYTE),
  WEIGHT_UOM              VARCHAR2(500 BYTE),
  BILLED_CUBE_WEIGHT      VARCHAR2(500 BYTE),
  CUBE_UOM                VARCHAR2(500 BYTE),
  LENGTH                  VARCHAR2(500 BYTE),
  WIDTH                   VARCHAR2(500 BYTE),
  HEIGHT                  VARCHAR2(500 BYTE),
  FUNDS_TYPE              VARCHAR2(500 BYTE),
  BILL_NAME               VARCHAR2(500 BYTE),
  BILL_TO_FRU             VARCHAR2(500 BYTE),
  CONSIGNEE_NAME          VARCHAR2(500 BYTE),
  NO_OF_PIECES            VARCHAR2(500 BYTE),
  QTY_UOM                 VARCHAR2(500 BYTE),
  TARGET_ERP_SYSTEM       VARCHAR2(500 BYTE),
  CARRIER_STATUS          VARCHAR2(500 BYTE),
  RECORD_ID                NUMBER PRIMARY KEY,
  CREATION_DATE            DATE,
  CREATED_BY               NUMBER,
  STATUS                   VARCHAR2(200 BYTE)
);