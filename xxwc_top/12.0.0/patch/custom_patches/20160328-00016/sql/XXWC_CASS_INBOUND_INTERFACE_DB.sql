 /*************************************************************************

   **************************************************************************
     $Header XXWC_CASS_INBOUND_INTERFACE $
     Module Name: XXWC_CASS_INBOUND_INTERFACE.sql

     PURPOSE:   This script will create a DBA Directory for CASS interface

     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/03/2016  Neha Saini         Initial Build - TMS# 20160328-00016

   /*************************************************************************/

DECLARE
   l_db_name       VARCHAR2 (20) := NULL;
   l_path_wkly     VARCHAR2 (240) := NULL;
   l_path_mnthly   VARCHAR2 (240) := NULL;
   l_sql           VARCHAR2 (240) := NULL;
--
BEGIN
   SELECT LOWER (name) INTO l_db_name FROM v$database;

   l_path_wkly := '/xx_iface/' || l_db_name || '/inbound/CASS/Weekly';
   l_path_mnthly := '/xx_iface/' || l_db_name || '/inbound/CASS/Monthly';

   l_sql :=
         'CREATE OR REPLACE DIRECTORY XXWC_CASS_INBOUND_WKLY_IFACE as'
      || ' '
      || ''''
      || l_path_wkly
      || '''';

   DBMS_OUTPUT.put_line ('DBA Directory Path: ' || l_path_wkly);
   DBMS_OUTPUT.put_line (' ');
   DBMS_OUTPUT.put_line ('SQL: ' || l_sql);
   DBMS_OUTPUT.put_line (' ');
   DBMS_OUTPUT.put_line (
      'Begin setup of directory XXWC_CASS_INBOUND_WKLY_IFACE');

   EXECUTE IMMEDIATE l_sql;

   DBMS_OUTPUT.put_line (
      'End setup of directory XXWC_CASS_INBOUND_WKLY_IFACE');

   l_sql :=
         'CREATE OR REPLACE DIRECTORY XXWC_CASS_INBOUND_MTHLY_IFACE as'
      || ' '
      || ''''
      || l_path_mnthly
      || '''';

   DBMS_OUTPUT.put_line ('DBA Directory Path: ' || l_path_mnthly);
   DBMS_OUTPUT.put_line (' ');
   DBMS_OUTPUT.put_line ('SQL: ' || l_sql);
   DBMS_OUTPUT.put_line (' ');
   DBMS_OUTPUT.put_line (
      'Begin setup of directory XXWC_CASS_INBOUND_MTHLY_IFACE');

   EXECUTE IMMEDIATE l_sql;

   DBMS_OUTPUT.put_line (
      'End setup of directory XXWC_CASS_INBOUND_MTHLY_IFACE');
END;
/
GRANT ALL
   ON DIRECTORY XXWC_CASS_INBOUND_WKLY_IFACE
   TO PUBLIC
   WITH GRANT OPTION;
GRANT ALL
   ON DIRECTORY XXWC_CASS_INBOUND_MTHLY_IFACE
   TO PUBLIC
   WITH GRANT OPTION;
/