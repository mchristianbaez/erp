 /*************************************************************************

   **************************************************************************
     $Header XXWC_CASS_DET_INTF_EXT_TBL $
     Module Name: XXWC_CASS_DET_INTF_EXT_TBL.sql

     PURPOSE:   This script creates external table for CASS interface

     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       29/03/2016  Neha Saini         Initial Build - TMS# 20160328-00016

   /*************************************************************************/
DROP TABLE XXWC.XXWC_CASS_DET_INTF_EXT_TBL CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWC_CASS_DET_INTF_EXT_TBL
(
  NOTIFICATION_DATE        CHAR(500 BYTE),
  CARRIER_NAME             CHAR(500 BYTE),
  SHIP_DATE                CHAR(500 BYTE),
  INVOICE_DATE             CHAR(500 BYTE),
  CARRIER_PRO_NUMBER       CHAR(500 BYTE),
  SHIPMENT_NUMBER          CHAR(500 BYTE),
  SHIPMENT_MODE            CHAR(500 BYTE),
  MOVEMENT_TYPE            CHAR(500 BYTE),
  ORIGIN_NAME              CHAR(500 BYTE),
  ORIGIN_CITY              CHAR(500 BYTE),
  ORIGIN_STATE             CHAR(500 BYTE),
  ORIGIN_POSTAL_CODE       CHAR(500 BYTE),
  ORIGIN_CC                CHAR(500 BYTE),
  DESTINATION_NAME         CHAR(500 BYTE),
  DESTINATION_CITY         CHAR(500 BYTE),
  DESTINATION_STATE        CHAR(500 BYTE),
  DESTINATION_POSTAL_CODE  CHAR(500 BYTE),
  DESTINATION_CC           CHAR(500 BYTE),
  GL_ACCOUNT_CODE          CHAR(500 BYTE),
  GL_STRING                CHAR(500 BYTE),
  GL1                      CHAR(500 BYTE),
  GL2                      CHAR(500 BYTE),
  BILLED_AMOUNT            CHAR(500 BYTE),
  PAID_AMOUNT              CHAR(500 BYTE),
  GST                      CHAR(500 BYTE),
  HST                      CHAR(500 BYTE),
  QST                      CHAR(500 BYTE),
  FSC                      CHAR(500 BYTE),
  LINE_HAUL                CHAR(500 BYTE),
  OTHER                    CHAR(500 BYTE),
  FISCAL_MONTH             CHAR(500 BYTE),
  AUDIT_MATCH              CHAR(500 BYTE),
  SHIPMENT_ESTIMATE        CHAR(500 BYTE),
  HDS_LINE_HAUL            CHAR(500 BYTE),
  HDS_FSC                  CHAR(500 BYTE),
  HDS_ACCESSORIALS         CHAR(500 BYTE),
  CUSTOMER_NAME            CHAR(500 BYTE),
  SALES_ORDER_NUMBER       CHAR(500 BYTE),
  VENDOR_NAME              CHAR(500 BYTE),
  PURCHASE_ORDER_NUMBER    CHAR(500 BYTE),
  SENDER_OR_RECEIVER       CHAR(500 BYTE),
  RECEIVING_DATE           CHAR(500 BYTE),
  PRO_RECEIVED_DATE        CHAR(500 BYTE),
  SCHEDULED_PAY_DATE       CHAR(500 BYTE),
  INVOICE_DUE_DATE         CHAR(500 BYTE),
  BILLED_WEIGHT            CHAR(500 BYTE),
  WEIGHT_UOM               CHAR(500 BYTE),
  BILLED_CUBE_WEIGHT       CHAR(500 BYTE),
  CUBE_UOM                 CHAR(500 BYTE),
  LENGTH                   CHAR(500 BYTE),
  WIDTH                    CHAR(500 BYTE),
  HEIGHT                   CHAR(500 BYTE),
  FUNDS_TYPE               CHAR(500 BYTE),
  BILL_NAME                CHAR(500 BYTE),
  BILL_TO_FRU              CHAR(500 BYTE),
  CONSIGNEE_NAME           CHAR(500 BYTE),
  NO_OF_PIECES             CHAR(500 BYTE),
  QTY_UOM                  CHAR(500 BYTE),
  TARGET_ERP_SYSTEM        CHAR(500 BYTE),
  CARRIER_STATUS           CHAR(500 BYTE)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY XXWC_CASS_INBOUND_WKLY_IFACE
     ACCESS PARAMETERS
       ( RECORDS DELIMITED BY NEWLINE
            SKIP 1 FIELDS TERMINATED BY '|' 
            MISSING FIELD VALUES ARE NULL
            ( NOTIFICATION_DATE        CHAR(500 ),
              CARRIER_NAME             CHAR(500 ),
              SHIP_DATE                CHAR(500 ),
              INVOICE_DATE             CHAR(500 ),
              CARRIER_PRO_NUMBER       CHAR(500 ),
              SHIPMENT_NUMBER          CHAR(500 ),
              SHIPMENT_MODE            CHAR(500 ),
              MOVEMENT_TYPE            CHAR(500 ),
              ORIGIN_NAME              CHAR(500 ),
              ORIGIN_CITY              CHAR(500 ),
              ORIGIN_STATE             CHAR(500 ),
              ORIGIN_POSTAL_CODE       CHAR(500 ),
              ORIGIN_CC                CHAR(500 ),
              DESTINATION_NAME         CHAR(500 ),
              DESTINATION_CITY         CHAR(500 ),
              DESTINATION_STATE        CHAR(500 ),
              DESTINATION_POSTAL_CODE  CHAR(500 ),
              DESTINATION_CC           CHAR(500 ),
              GL_ACCOUNT_CODE          CHAR(500 ),
              GL_STRING                CHAR(500 ),
              GL1                      CHAR(500 ),
              GL2                      CHAR(500 ),
              BILLED_AMOUNT            CHAR(500 ),
              PAID_AMOUNT              CHAR(500 ),
              GST                      CHAR(500 ),
              HST                      CHAR(500 ),
              QST                      CHAR(500 ),
              FSC                      CHAR(500 ),
              LINE_HAUL                CHAR(500 ),
              OTHER                    CHAR(500 ),
              FISCAL_MONTH             CHAR(500 ),
              AUDIT_MATCH              CHAR(500 ),
              SHIPMENT_ESTIMATE        CHAR(500 ),
              HDS_LINE_HAUL            CHAR(500 ),
              HDS_FSC                  CHAR(500 ),
              HDS_ACCESSORIALS         CHAR(500 ),
              CUSTOMER_NAME            CHAR(500 ),
              SALES_ORDER_NUMBER       CHAR(500 ),
              VENDOR_NAME              CHAR(500 ),
              PURCHASE_ORDER_NUMBER    CHAR(500 ),
              SENDER_OR_RECEIVER       CHAR(500 ),
              RECEIVING_DATE           CHAR(500 ),
              PRO_RECEIVED_DATE        CHAR(500 ),
              SCHEDULED_PAY_DATE       CHAR(500 ),
              INVOICE_DUE_DATE         CHAR(500 ),
              BILLED_WEIGHT            CHAR(500 ),
              WEIGHT_UOM               CHAR(500 ),
              BILLED_CUBE_WEIGHT       CHAR(500 ),
              CUBE_UOM                 CHAR(500 ),
              LENGTH                   CHAR(500 ),
              WIDTH                    CHAR(500 ),
              HEIGHT                   CHAR(500 ),
              FUNDS_TYPE               CHAR(500 ),
              BILL_NAME                CHAR(500 ),
              BILL_TO_FRU              CHAR(500 ),
              CONSIGNEE_NAME           CHAR(500 ),
              NO_OF_PIECES             CHAR(500 ),
              QTY_UOM                  CHAR(500 ),
              TARGET_ERP_SYSTEM        CHAR(500 ),
              CARRIER_STATUS           CHAR(500 )
            )
             )
     LOCATION (XXWC_CASS_INBOUND_WKLY_IFACE:'HDS_PAYMENT_DETAILS_FILE.txt')
  )
REJECT LIMIT UNLIMITED
NOPARALLEL
NOMONITORING;