 /****************************************************************************************************
  Table: TMS20170512-00042_Data_Fix.sql     
  Description: batch to insert  category_ids to XXWC.XXWC_MTL_INV_CATE_UPDATE_LOG load 7
  HISTORY
  ======================================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ---------------------------------------------------------------------------
  1.0     17-May-2017        P.Vamshidhar    Initial version TMS#20170512-00042
                                             batch to insert  category_ids to XXWC.XXWC_MTL_INV_CATE_UPDATE_LOG load 7
  ***********************************************************************************************************************/
SET SERVEROUT ON
  
BEGIN
   INSERT INTO XXWC.XXWC_MTL_INV_CATE_UPDATE_LOG (CATEGORY_ID,
                                                  EXPN_ACCT,
                                                  PROCESS_STATUS,
                                                  CREATION_DATE)
      SELECT DISTINCT mic.category_id,
                      mcv.attribute8,
                      'NEW',
                      SYSDATE
        FROM apps.MTL_SYSTEM_ITEMS_B MIB,
             apps.mtl_parameters orgs,
             apps.MTL_ITEM_CATEGORIES_V mic,
             apps.FND_USER FUSE,
             apps.MTL_CATEGORIES_V mcv,
             apps.gl_code_combinations_kfv COGS,
             apps.gl_code_combinations_kfv SALES,
             apps.gl_code_combinations_kfv EXPENSE
       WHERE     mib.organization_id = orgs.organization_id
             AND mib.inventory_item_id = mic.inventory_item_id
             AND mib.organization_id = mic.organization_id
             AND mib.created_by = fuse.user_id(+)
             -- AND mib.item_type = 'INTANGIBLE'
             AND mic.category_concat_segs = MCV.CATEGORY_CONCAT_SEGS
             AND mcv.structure_name = 'Item Categories'
             AND mic.category_set_name = 'Inventory Category'
             AND orgs.master_organization_id = 222
             AND orgs.organization_code NOT LIKE ('9%')
             AND mib.COST_OF_SALES_ACCOUNT = cogs.code_combination_id
             AND mib.SALES_ACCOUNT = sales.code_combination_id
             AND mib.EXPENSE_ACCOUNT = expense.code_combination_id
             AND mib.inventory_item_status_code <> 'Inactive'
             AND Expense.segment4 <> mcv.attribute8
             AND mcv.category_id IN (12698, 3725, 12708);

   DBMS_OUTPUT.PUT_LINE ('Number of rows inserted ' || SQL%ROWCOUNT);

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error occured ' || SUBSTR (SQLERRM, 1, 250));
      ROLLBACK;
END;
/