CREATE OR REPLACE PACKAGE BODY APPS.xxwc_ebs_isr_intf_pkg
--//============================================================================
--//
--// Object Name         :: xxwc_ebs_isr_intf_pkg
--//
--// Object Type         :: Package Specification
--//
--// Object Description  :: This is an automation for Weekly ISR report..
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Krishna     01/07/2014         Initial Build - TMS#20130709-01006
--// 2.0     Hari Prasad M    03/09/2015    TMS # 20150803-00193 Extract ISR for Pricing-Update Weekly Pricing Extract to reduce rows and columns 
--// 3.0     Manjula C        10/05/2015    TMS # 20151005-00003 - Performance Improvement
--//============================================================================
AS

g_dflt_email            VARCHAR2(50) := 'HDSOracleDevelopers@hdsupply.com';
--//============================================================================
--//
--// Object Name         :: create_file
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure creates file for the ISR report
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Krishna          01/07/2014          Initial Build - TMS#20130709-01006
--// 2.0     Hari Prasad M    03/09/2015          TMS # 20150803-00193 Extract ISR for Pricing-Update Weekly Pricing Extract to reduce rows and columns
--// 3.0     Manjula C        10/05/2015          TMS # 20151005-00003 - Performance Improvement
--//============================================================================
PROCEDURE create_file (p_errbuf           OUT  VARCHAR2
                      ,p_retcode          OUT  NUMBER
                      ,p_directory_name   IN   VARCHAR2
                      )
IS

      -- Intialize Variables
      l_err_msg               VARCHAR2 (2000);
      l_err_code              NUMBER;
      l_err_callfrom          VARCHAR2(50) ;
      l_err_callpoint         VARCHAR2(50) ;

      --File Variables
      l_filehandle             UTL_FILE.file_type;
      l_filename               VARCHAR2 (240);
      l_base_filename          VARCHAR2 (240);
      l_count                  NUMBER;
      l_query                  VARCHAR2 (2000);

      l_program_error          EXCEPTION;
      l_tab_cnt                NUMBER;
      l_hdr                    VARCHAR2(5000);
      
      CURSOR c1 is 
        SELECT v_line.details
        FROM
        (SELECT
--        (EXPIV.ORG||'|'||QH.NAME||'|'||EXPIV.PRE||'|'||EXPIV.ITEM_NUMBER||'|'||EXPIV.VENDOR_NUM -- Commented for Ver 3.0
        (EXPIV.ORG||'|'||EXPIV.PRE||'|'||EXPIV.ITEM_NUMBER||'|'||EXPIV.VENDOR_NUM -- Added for Ver 3.0
        ||'|'||EXPIV.VENDOR_NAME
        --||'|'||EXPIV.SOURCING_RULE commented for Ver# 2.0
        --||'|'||EXPIV.SOURCE    commented for Ver# 2.0
        ||'|'||EXPIV.ST
        ||'|'||EXPIV.DESCRIPTION
        ||'|'||EXPIV.CAT
		---commented begin for Ver# 2.0
        /*||'|'||EXPIV.PPLT
        ||'|'||EXPIV.PLT
        ||'|'||EXPIV.CLT*/
		--commented end for Ver# 2.0
        ||'|'||EXPIV.SS
        ||'|'||EXPIV.UOM
        ||'|'||EXPIV.WT
		---commented begin for Ver# 2.0
        /*||'|'||EXPIV.FML
        ||'|'||EXPIV.SO
        ||'|'||EXPIV.CL*/
		--commented end for Ver# 2.0
        ||'|'||EXPIV.STK_FLAG
        ||'|'||EXPIV.PM
        ||'|'||EXPIV.MINN
        ||'|'||EXPIV.MAXN
        ||'|'||EXPIV.AMU
		--commented begin for Ver# 2.0
        /*||'|'||EXPIV.HIT6_SALES
        ||'|'||EXPIV.HIT4_SALES*/
	    --commented end for Ver# 2.0
        ||'|'||EXPIV.AVER_COST
        ||'|'||EXPIV.ITEM_COST
        ||'|'||EXPIV.BPA_COST
        --||'|'||EXPIV.BPA commented for Ver# 2.0
        ||'|'||EXPIV.QOH
        ||'|'||EXPIV.OPEN_REQ
        ||'|'||EXPIV.ON_ORD
		--commented begin for Ver# 2.0
        /*||'|'||EXPIV.AVAILABLE
        ||'|'||EXPIV.AVAIL2
        ||'|'||EXPIV.AVAILABLEDOLLAR
        ||'|'||EXPIV.ONE_SALES
        ||'|'||EXPIV.SIX_SALES
        ||'|'||EXPIV.TWELVE_SALES*/
		--commented end for Ver# 2.0
        ||'|'||EXPIV.BIN_LOC
		--commented begin for Ver# 2.0
        /*||'|'||EXPIV.MC
        ||'|'||EXPIV.FI_FLAG
        ||'|'||TO_CHAR(EXPIV.FREEZE_DATE
                 ,'YYYY/MM/DD HH:MI:SS AM')
        ||'|'||EXPIV.RES
        ||'|'||EXPIV.THIRTEEN_WK_AVG_INV
        ||'|'||EXPIV.THIRTEEN_WK_AN_COGS*/
		--commented end for Ver# 2.0		
        ||'|'||EXPIV.TURNS)  details ---Added for ver # 2.0		
		--commented begin for Ver# 2.0
        /*||'|'||EXPIV.BUYER
        ||'|'||EXPIV.TS
        ||'|'||EXPIV.JAN_SALES
        ||'|'||EXPIV.FEB_SALES
        ||'|'||EXPIV.MAR_SALES
        ||'|'||EXPIV.APR_SALES
        ||'|'||EXPIV.MAY_SALES
        ||'|'||EXPIV.JUNE_SALES
        ||'|'||EXPIV.JUL_SALES
        ||'|'||EXPIV.AUG_SALES
        ||'|'||EXPIV.SEP_SALES
        ||'|'||EXPIV.OCT_SALES
        ||'|'||EXPIV.NOV_SALES
        ||'|'||EXPIV.DEC_SALES
        ||'|'||EXPIV.INT_REQ
        ||'|'||EXPIV.DIR_REQ
        ||'|'||EXPIV.SITE_VENDOR_NUM
        ||'|'||EXPIV.VENDOR_SITE) details*/
		--commented end for Ver# 2.0
        FROM
          XXEIS.EIS_XXWC_PO_ISR_RPT_V EXPIV
        WHERE EXPIV.PROCESS_ID = -1512) v_line; --Added for v 3.0		  
-- commented for Ver 3.0 Begin
/*		  
		  , MTL_SYSTEM_ITEMS_B MSIB
		  , QP_QUALIFIERS_V  QQV
		  , QP_LIST_HEADERS QH
		  --, MTL_PARAMETERS MP
        --WHERE EXPIV.PROCESS_ID = -1512) v_line; ---Commented for v 2.0
		----Added below condition for v 2.0
		WHERE  MSIB.INVENTORY_ITEM_ID=EXPIV.INVENTORY_ITEM_ID
		AND    MSIB.ORGANIZATION_ID=EXPIV.ORGANIZATION_ID
		--AND    MSIB.ORGANIZATION_ID = MP.ORGANIZATION_ID
		AND    QQV.LIST_HEADER_ID = QH.LIST_HEADER_ID
		AND    MSIB.ITEM_TYPE !='Special' 
		AND    MSIB.INVENTORY_ITEM_STATUS_CODE !='Inactive'
		AND    QQV.list_header_id = qh.list_header_id
		AND    QQV.QUALIFIER_CONTEXT = 'ORDER'
		AND    QQV.QUALIFIER_ATTRIBUTE = 'QUALIFIER_ATTRIBUTE18'
		AND    QQV.COMPARISION_OPERATOR_CODE = '='
		AND    QQV.EXCLUDER_FLAG = 'N'
		AND    QH.LIST_TYPE_CODE = 'PRL'
		AND    QH.AUTOMATIC_FLAG = 'Y'
		AND    NVL (QH.END_DATE_ACTIVE, TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
		AND    QQV.QUALIFIER_ATTR_VALUE =MSIB.ORGANIZATION_ID
		AND    EXPIV.PROCESS_ID = -1512) v_line;
*/
-- commented for Ver 3.0 End
		 
      	---Added end condition for v 2.0
   BEGIN

    SELECT  count(1)
    INTO    l_tab_cnt
    FROM    XXEIS.EIS_RS_COMMON_OUTPUTS 
    WHERE   process_id = -1512;
    
    DBMS_OUTPUT.put_line  ('Number of rowns in temp table '|| to_char(l_tab_cnt));
    
    /* ===========
    Cleanup Data
    ============ */
    delete from XXEIS.EIS_RS_COMMON_OUTPUTS where process_id = -1512;

    commit;
    
    DBMS_OUTPUT.put_line  ('Data is cleaned in Temp Table');
    
    /* ===========
    Generate Data
    ============ */
    
    begin
        xxeis.eis_po_xxwc_isr_util_pkg.G_isr_rpt_dc_mod_sub := 'No';
        xxeis.eis_po_xxwc_isr_pkg.Isr_Rpt_Proc(P_process_id      => -1512
                                        ,P_Region                => ''
                                        ,P_District              => ''
                                        ,P_Location              => ''
                                        ,P_Dc_Mode               => 'No'
                                        ,P_Tool_Repair           => 'Include'
                                        ,P_Time_Sensitive        => 'Include'
                                        ,P_Stk_Items_With_Hit4   => ''
                                        ,p_Report_Condition      => 'All items'
                                        ,P_Report_Criteria       => ''
                                        ,P_Report_Criteria_val   => ''
                                        ,p_start_bin_loc         => ''
                                        ,p_end_bin_loc           => ''
                                        ,p_vendor                => ''
                                        ,p_item                  => ''
                                        ,p_cat_class             => ''
                                        ,p_org_list              => ''
                                        ,P_ITEM_LIST             => ''
                                        ,P_SUPPLIER_LIST         => ''
                                        ,P_CAT_CLASS_LIST        => ''
                                        ,P_SOURCE_LIST           => ''
                                        ,P_INTANGIBLES           => 'Include');
     end;
    
    
    DBMS_OUTPUT.put_line  ('Data Generated in the view');
    
    l_err_callfrom := 'XXWC_EBS_ISR_INTF_PKG.CREATE_FILE';
    l_err_callpoint:= 'Create File';

      --File Name is same. So hardcoded the file name.
      l_filename :=  'ISR424483.txt';
      

       -- Delete if file already exists
        BEGIN
          
          UTL_FILE.fremove (p_directory_name,l_filename);
        EXCEPTION
        WHEN OTHERS THEN
          NULL;
        END;
    --commented begin for Ver# 2.0
      /*l_hdr := 'ORG|PRE|ITEM_NUMBER|VENDOR_NUM|VENDOR_NAME|SOURCING_RULE|SOURCE|ST|DESCRIPTION|CAT|PPLT|PLT|CLT|SS|UOM|WT|FML|SO|CL|STK_FLAG|PM|MINN|MAXN|AMU|HIT6_SALES|HIT4_SALES|AVER_COST|ITEM_COST|BPA_COST|BPA|QOH|OPEN_REQ|ON_ORD|AVAILABLE|AVAIL2|AVAILABLEDOLLAR|ONE_SALES|SIX_SALES|TWELVE_SALES|BIN_LOC|MC|FI_FLAG|TO_CHAR(EXPIV.FREEZE_DATE,''YYYY/MM/DDHH:MI:SSAM'')|RES|THIRTEEN_WK_AVG_INV|THIRTEEN_WK_AN_COGS|TURNS|BUYER|TS|JAN_SALES|FEB_SALES|MAR_SALES|APR_SALES|MAY_SALES|JUNE_SALES|JUL_SALES|AUG_SALES|SEP_SALES|OCT_SALES|NOV_SALES|DEC_SALES|INT_REQ|DIR_REQ|SITE_VENDOR_NUM|DETAILS';*/
	  --commented end for Ver# 2.0
--Commented for Ver 3.0 Begin
/*	  
	  ---Added below for ver# 2.0
	   l_hdr := 'ORG|Oracle_Price_Zone| PRE|ITEM_NUMBER|VENDOR_NUM|VENDOR_NAME|ST|DESCRIPTION|CAT|SS|UOM|WT|STK_FLAG|PM|MINN|MAXN|AMU|AVER_COST|ITEM_COST|BPA_COST|QOH|OPEN_REQ|ON_ORD|BIN_LOC|TURNS';   ---Added end for ver# 2.0
*/
--Commented for Ver 3.0 End

--Added the below line for Ver 3.0

l_hdr := 'ORG|PRE|ITEM_NUMBER|VENDOR_NUM|VENDOR_NAME|ST|DESCRIPTION|CAT|SS|UOM|WT|STK_FLAG|PM|MINN|MAXN|AMU|AVER_COST|ITEM_COST|BPA_COST|QOH|OPEN_REQ|ON_ORD|BIN_LOC|TURNS'; 
	   
      --Open the file handler
      l_filehandle := UTL_FILE.fopen (LOCATION     => p_directory_name,
                                      filename     => l_filename,
                                      open_mode    => 'w',
                                      max_linesize => 32767);
      
      DBMS_OUTPUT.PUT_LINE ('File is created');
      
      UTL_FILE.PUT_line(l_filehandle,l_hdr) ;
      
      DBMS_OUTPUT.PUT_LINE ('File HDR is created');

      DBMS_OUTPUT.put_line  ('Writing to the file ... Opening Cursor');
      l_count := 0;
      
      for j in c1
      
        loop
        
        UTL_FILE.put_line(l_filehandle,j.details);
        l_count := l_count +1;
           
        end loop;

      DBMS_OUTPUT.put_line  ('Wrote ' || l_count || ' records to file');
      DBMS_OUTPUT.put_line  ('Closing File Handler');

      --Closing the file handler
      UTL_FILE.fclose (l_filehandle);

      
    /* ===========
    Cleanup Data
    ============ */
    delete from XXEIS.EIS_RS_COMMON_OUTPUTS where process_id = -1512;

    commit;

      p_retcode := 0;

   EXCEPTION
    WHEN l_program_error THEN
      l_err_code := 2;
      p_retcode  := 2;

      utl_file.fclose(l_filehandle);
      utl_file.fremove(p_directory_name, l_base_filename);
	  
	  l_err_msg :=
            SUBSTR ((l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000)),
                    1,
                    2000
                   );
      DBMS_OUTPUT.put_line (l_err_msg);

    WHEN OTHERS THEN
      l_err_code := 2;
      p_retcode  := 2;
      l_err_msg  := ' ERROR ' || SQLCODE ||  substr(SQLERRM, 1, 2000);

      utl_file.fclose(l_filehandle);
      utl_file.fremove(p_directory_name, l_base_filename);

      XXCUS_error_pkg.XXCUS_error_main_api (
         p_called_from         => l_err_callfrom
        ,p_calling             => l_err_callpoint
        ,p_request_id          => -1
        ,p_ora_error_msg       => SQLERRM
        ,p_error_desc          => l_err_msg
        ,p_distribution_list   => g_dflt_email
        ,p_module              => 'APPS');

END CREATE_FILE;

--//============================================================================
--//
--// Object Name         :: main
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure is called by UC4 to initiate file creation.
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Krishna    01/07/2014    Initial Build - TMS#20130709-01006 
--//============================================================================
PROCEDURE main (p_errbuf              OUT VARCHAR2
               ,p_retcode             OUT NUMBER
               ,p_ob_directory_name   IN  VARCHAR2
               )
IS

      -- Variable definitions
      l_err_msg             VARCHAR2 (3000);
      l_err_code            NUMBER;      
      l_dir_exists          NUMBER;
      l_err_callfrom        VARCHAR2(50) ;
      l_err_callpoint       VARCHAR2(50) ;

   BEGIN

    l_err_callfrom := 'XXWC_EBS_ISR_INTF_PKG.MAIN';
    l_err_callpoint:= 'Main';

      DBMS_OUTPUT.put_line ('Entering main...');
      
      DBMS_OUTPUT.put_line ('p_ob_directory_name: ' || p_ob_directory_name);
      
      l_dir_exists := 0;
        
        BEGIN
          SELECT COUNT(1)
            INTO l_dir_exists
            FROM all_directories
           WHERE directory_name = p_ob_directory_name;
        EXCEPTION
        WHEN OTHERS THEN
          l_err_msg := 'Error validating directory   :'||SQLERRM;
          RAISE program_error;
        END;

        -- Raise ProgramError if directory path does not exist
        IF l_dir_exists = 0 THEN
          l_err_msg := 'Directory does not exist in Oracle :'||p_ob_directory_name;
          RAISE program_error;
        END IF;
        
     DBMS_OUTPUT.put_line ( 'Entering CREATE_FILE...'  );

      CREATE_FILE (p_errbuf => p_errbuf,
                  p_retcode => p_retcode,
                  p_directory_name  =>p_ob_directory_name
                  );

      DBMS_OUTPUT.put_line ( 'Exiting CREATE_FILE...'  );
      DBMS_OUTPUT.put_line ('Exiting main...');

      p_retcode := 0;

   EXCEPTION
      WHEN program_error
      THEN
         ROLLBACK;
         l_err_code := 2;
         l_err_msg :=
            SUBSTR ((l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000)),
                    1,
                    2000
                   );
         DBMS_OUTPUT.put_line (l_err_msg);
        p_retcode := 2;
      WHEN OTHERS
      THEN
         l_err_code := 2;
         l_err_msg :=
            SUBSTR ((l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000)),
                    1,
                    2000
                   );
         DBMS_OUTPUT.put_line (l_err_msg);
         p_retcode := 2;

      XXCUS_error_pkg.XXCUS_error_main_api (
         p_called_from         => l_err_callfrom
        ,p_calling             => l_err_callpoint
        ,p_request_id          => -1
        ,p_ora_error_msg       => SQLERRM
        ,p_error_desc          => l_err_msg
        ,p_distribution_list   => g_dflt_email
        ,p_module              => 'APPS');

   END main;
   
   END xxwc_ebs_isr_intf_pkg;
/
GRANT execute on APPS.xxwc_ebs_isr_intf_pkg to interface_prism
/
GRANT execute on APPS.xxwc_ebs_isr_intf_pkg to interface_xxcus
/