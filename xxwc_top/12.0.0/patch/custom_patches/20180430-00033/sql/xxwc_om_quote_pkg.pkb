CREATE OR REPLACE PACKAGE BODY APPS.xxwc_om_quote_pkg
AS
   /*************************************************************************
   *   $Header xxwc_om_quote_pkg.pkb $
   *   Module Name: xxwc OM Custom Quote package
   *
   *   PURPOSE:   Used in Quotes form
   *
   *   REVISIONS:
   *   Ver        Date        Author                Description
   *   ---------  ----------  ---------------    -------------------------
   *  1.0        01/25/2013  Shankar Hariharan    Initial Version
   *  2.0        10/08/2013  Ram Talluri          TMS-20131003-00186 - Custom Quote to SO Imports - Price Adjustments Missing
   *  2.1        11/12/2013  Ram Talluri          TMS#20131112-00332
   *  2.2        02/19/2014  Ram Talluri          TMS #20140219-00158 - Imoort price adjustments correctly
   *  2.3        02/26/2014  Shankar Hariharan    TMS #20140107-00097 - Rounding off selling price to 2 decimals
   *  2.4        04/01/2014  Ram Talluri           TMS #20140308-00014 - Price rounding to three decimal places.
   *  2.5        04/10/2014  Maharajan Shunmugam   TMS# 20140407-00346 - Performance issue
   *  2.6        04/14/2014  Ram Talluri          TMS #20140414-00019 4/14/2014- Price rounding from 3 to 5.
   *  2.7        10/20/2014  Maharajan Shunmgam   TMS# 20141001-00162 Canada Multi Org Changes  
   *  2.8        03/10/2016  Rakesh Patel         TMS#  20160114-00041 WC Quote notes to sales order  
   *  2.9        04/02/2018  Niraj K Ranjan       TMS#20180321-00087   WC Custom Quote Enhancements
   *  3.0        05/02/2018  Niraj K Ranjan       TMS#20180430-00033 WC Quote expiry notifications enhancement
   * ***************************************************************************/

   PROCEDURE calculate_tax (i_quote_number    IN     NUMBER,
                            i_call_mode       IN     VARCHAR,
                            o_return_status      OUT VARCHAR,
                            o_msg_data           OUT VARCHAR)
   IS
      /* Initialize the proper Context */
      --l_org_id                 NUMBER := fnd_profile.VALUE ('ORG_ID');          -- commented and added below for ver# 2.4
      --l_application_id           NUMBER := fnd_profile.VALUE ('RESP_APPL_ID');
      l_org_id                   NUMBER := MO_GLOBAL.GET_CURRENT_ORG_ID;
      l_application_id           NUMBER := fnd_global.resp_appl_id; 
      /* MZ4Md211 */
      --l_responsibility_id        NUMBER := fnd_profile.VALUE ('RESP_ID');        -- commented and added below for ver# 2.4
      --l_user_id                  NUMBER := fnd_profile.VALUE ('USER_ID');
        l_responsibility_id        NUMBER := fnd_global.resp_id;
        l_user_id                  NUMBER := fnd_global.user_id;

      /* Initialize the record to G_MISS to enable defaulting */
      l_header_rec               OE_ORDER_PUB.Header_Rec_Type
                                    := OE_ORDER_PUB.G_MISS_HEADER_REC;
      l_old_header_rec           OE_ORDER_PUB.Header_Rec_Type;

      l_line_tbl                 OE_ORDER_PUB.Line_Tbl_Type;
      l_old_line_tbl             OE_ORDER_PUB.Line_Tbl_Type;

      l_action_request_tbl       OE_ORDER_PUB.Request_Tbl_Type;

      x_header_rec               OE_ORDER_PUB.Header_Rec_Type;
      x_header_val_rec           OE_ORDER_PUB.Header_Val_Rec_Type;
      x_Header_Adj_tbl           OE_ORDER_PUB.Header_Adj_Tbl_Type;
      x_Header_Adj_val_tbl       OE_ORDER_PUB.Header_Adj_Val_Tbl_Type;
      x_Header_price_Att_tbl     OE_ORDER_PUB.Header_Price_Att_Tbl_Type;
      x_Header_Adj_Att_tbl       OE_ORDER_PUB.Header_Adj_Att_Tbl_Type;
      x_Header_Adj_Assoc_tbl     OE_ORDER_PUB.Header_Adj_Assoc_Tbl_Type;
      x_Header_Scredit_tbl       OE_ORDER_PUB.Header_Scredit_Tbl_Type;
      x_Header_Scredit_val_tbl   OE_ORDER_PUB.Header_Scredit_Val_Tbl_Type;
      x_line_tbl                 OE_ORDER_PUB.Line_Tbl_Type;
      x_line_val_tbl             OE_ORDER_PUB.Line_Val_Tbl_Type;
      x_Line_Adj_tbl             OE_ORDER_PUB.Line_Adj_Tbl_Type;
      x_Line_Adj_val_tbl         OE_ORDER_PUB.Line_Adj_Val_Tbl_Type;
      x_Line_price_Att_tbl       OE_ORDER_PUB.Line_Price_Att_Tbl_Type;
      x_Line_Adj_Att_tbl         OE_ORDER_PUB.Line_Adj_Att_Tbl_Type;
      x_Line_Adj_Assoc_tbl       OE_ORDER_PUB.Line_Adj_Assoc_Tbl_Type;
      x_Line_Scredit_tbl         OE_ORDER_PUB.Line_Scredit_Tbl_Type;
      x_Line_Scredit_val_tbl     OE_ORDER_PUB.Line_Scredit_Val_Tbl_Type;
      x_Lot_Serial_tbl           OE_ORDER_PUB.Lot_Serial_Tbl_Type;
      x_Lot_Serial_val_tbl       OE_ORDER_PUB.Lot_Serial_Val_Tbl_Type;
      x_action_request_tbl       OE_ORDER_PUB.Request_Tbl_Type;
      
      l_om_tax_tbl_type       xxwc_om_quote_pkg.XXWC_OM_TAX_TBL_TYPE := xxwc_om_quote_pkg.G_MISS_XXWC_OM_TAX_TBL; --Added by Maha for ver 2.5

      l_return_status            VARCHAR2 (2000);
      l_msg_count                NUMBER;
      l_msg_data                 VARCHAR2 (2000);
      l_return_msg               VARCHAR2 (2000);
      lv_msg_data                VARCHAR2 (2000);

      l_line_cnt                 NUMBER := 0;
      l_top_model_line_index     NUMBER;
      l_link_to_line_index       NUMBER;

      l_msg_index_out            NUMBER (10);
      

      --PRAGMA AUTONOMOUS_TRANSACTION;

      CURSOR c1
      IS
         SELECT *
           FROM xxwc_om_quote_headers
          WHERE quote_number = i_quote_number;

      CURSOR c2
      IS
         SELECT *
           FROM xxwc_om_quote_lines
          WHERE quote_number = i_quote_number;
          
--Added below cursor by Maha for ver 2.5
      CURSOR cur_load (p_header_id NUMBER)
      IS SELECT b.line_number,    
                b.attribute4,           
                b.ordered_item_id,                
                b.tax_value,
                b.ordered_quantity,
                b.attribute18                 
          FROM oe_order_lines b
           WHERE header_id = p_header_id;
   BEGIN
      fnd_global.apps_initialize (l_user_id,
                                  l_responsibility_id,
                                  l_application_id,
                                  NULL);
      mo_global.init ('ONT');
      mo_global.set_policy_context ('S', l_org_id);


      FOR c1_rec IN c1
      LOOP
         l_header_rec := oe_order_pub.g_miss_header_rec;
         l_header_rec.order_type_id := 1001;
         l_header_rec.sold_to_org_id := c1_rec.cust_account_id;
         l_header_rec.ship_to_org_id := c1_rec.site_use_id;
         l_header_rec.shipping_method_code := c1_rec.shipping_method;
         l_header_rec.ship_from_org_id := c1_rec.organization_id;
         l_header_rec.invoice_to_contact_id := c1_rec.contact_id;
         l_header_rec.ship_to_contact_id := c1_rec.contact_id;
         l_header_rec.cust_po_number :=
            'QUOTE:' || i_quote_number || '-' || SYSDATE;
         --l_header_rec.price_list_id := 26463;                             --commented and added belowe for ver#2.3
           l_header_rec.price_list_id := fnd_profile.VALUE('XXWC_GLOBAL_PRICE_LIST');
         l_header_rec.operation := oe_globals.g_opr_create;
         l_header_rec.shipping_instructions := c1_rec.notes;

         FOR c2_rec IN c2
         LOOP
            l_line_cnt := l_line_cnt + 1;
            l_line_tbl (l_line_cnt) := OE_ORDER_PUB.G_MISS_LINE_REC;
            l_line_tbl (l_line_cnt).operation := OE_GLOBALS.G_OPR_CREATE;
            --l_line_tbl(l_line_cnt).header_id := i_line_tbl(i).header_id;
            l_line_tbl (l_line_cnt).inventory_item_id :=
               c2_rec.inventory_item_id;
            l_line_tbl (l_line_cnt).ship_from_org_id := c1_rec.organization_id;
            l_line_tbl (l_line_cnt).ordered_quantity := c2_rec.line_quantity;
            l_line_tbl (l_line_cnt).unit_selling_price :=
               --ROUND (c2_rec.gm_selling_price, 3);--Rounding factor chnaged from 2 to 3 by Ram Talluri on 4/1/2014 for TMS #20140308-00014--Version 2.6
               ROUND (c2_rec.gm_selling_price, 5);--Version 2.6
            l_line_tbl (l_line_cnt).unit_list_price :=
               --ROUND (c2_rec.list_price, 3);--Rounding factor chnaged from 2 to 3 by Ram Talluri on 4/1/2014 for TMS #20140308-00014--Version 2.6
               ROUND (c2_rec.list_price, 5);--Version 2.6
            --l_line_tbl (l_line_cnt).calculate_price_flag := 'N';--Commented by Ram Talluri on 10/8/2013 for TMS-20131003-00186
            l_line_tbl (l_line_cnt).calculate_price_flag := 'Y'; --Added by Ram Talluri on 10/8/2013 for TMS-20131003-00186
            l_line_tbl (l_line_cnt).attribute18 := c2_rec.line_seq; --Added by Ram Talluri on 10/8/2013 for TMS-20131003-00186
            l_line_tbl (l_line_cnt).packing_instructions := c2_rec.notes; --Added by Rakesh Patel on 03/10/2016 for TMS-20160114-00041 


            --IF i_call_mode <> 'O'   --commented and added below by Maha for ver 2.5
            IF i_call_mode  = 'T'
            THEN
               l_line_tbl (l_line_cnt).attribute4 := c2_rec.line_seq;
               l_line_tbl (l_line_cnt).calculate_price_flag := 'N'; -- Ram Talluri 11/12/2013 TMS #20131112-00332
            END IF;
         END LOOP;
         

         OE_ORDER_PUB.Process_Order (
            p_api_version_number       => 1,
            p_org_id                   => l_org_id,
            p_init_msg_list            => FND_API.G_TRUE,
            p_return_values            => FND_API.G_TRUE,
            p_action_commit            => FND_API.G_TRUE,
            x_return_status            => l_return_status,
            x_msg_count                => l_msg_count,
            x_msg_data                 => l_msg_data,
            p_action_request_tbl       => l_action_request_tbl,
            p_header_rec               => l_header_rec,
            p_old_header_rec           => l_old_header_rec,
            p_line_tbl                 => l_line_tbl,
            p_old_line_tbl             => l_old_line_tbl,
            x_header_rec               => x_header_rec,
            x_header_val_rec           => x_header_val_rec,
            x_Header_Adj_tbl           => x_Header_Adj_tbl,
            x_Header_Adj_val_tbl       => x_Header_Adj_val_tbl,
            x_Header_price_Att_tbl     => x_Header_Price_Att_Tbl,
            x_Header_Adj_Att_tbl       => x_Header_Adj_Att_Tbl,
            x_Header_Adj_Assoc_tbl     => x_Header_Adj_Assoc_Tbl,
            x_Header_Scredit_tbl       => x_Header_Scredit_Tbl,
            x_Header_Scredit_val_tbl   => x_Header_Scredit_Val_Tbl,
            x_line_tbl                 => x_Line_Tbl,
            x_line_val_tbl             => x_Line_Val_Tbl,
            x_Line_Adj_tbl             => x_Line_Adj_Tbl,
            x_Line_Adj_val_tbl         => x_Line_Adj_Val_Tbl,
            x_Line_price_Att_tbl       => x_Line_Price_Att_Tbl,
            x_Line_Adj_Att_tbl         => x_Line_Adj_Att_Tbl,
            x_Line_Adj_Assoc_tbl       => x_Line_Adj_Assoc_Tbl,
            x_Line_Scredit_tbl         => x_Line_Scredit_Tbl,
            x_Line_Scredit_val_tbl     => x_Line_Scredit_Val_Tbl,
            x_Lot_Serial_tbl           => x_Lot_Serial_Tbl,
            x_Lot_Serial_val_tbl       => x_Lot_Serial_Val_Tbl,
            x_action_request_tbl       => x_action_request_tbl);

         IF l_return_status = FND_API.G_RET_STS_SUCCESS
         THEN
            o_msg_data := NULL;
            o_return_status := l_return_status;
            
            COMMIT;
            
            om_tax_util.calculate_tax (x_header_rec.header_id,
                                       l_return_status);
          
            IF     l_return_status = FND_API.G_RET_STS_SUCCESS
               --AND i_call_mode <> 'O' ---Ram Talluri 11/12/2013 TMS -20131112-00332
              --commented above and added below by Maha for ver 2.5
             AND i_call_mode  = 'T'
            THEN
            --commented below by Maha for ver 2.5
            /*   FOR c3_rec IN (SELECT b.line_number,
                                     b.attribute4,
                                     b.ordered_item_id,
                                     b.tax_value,
                                     b.attribute18 --Added by Ram Talluri on 10/8/2013 for TMS-20131003-00186
                                FROM oe_order_lines_all b
                               WHERE header_id = x_header_rec.header_id)
               LOOP
                  UPDATE xxwc_om_quote_lines
                     SET tax_amount = c3_rec.tax_value
                   WHERE     quote_number = i_quote_number
                         AND TO_CHAR (line_seq) = c3_rec.attribute18 --Added by Ram Talluri on 10/8/2013 for TMS-20131003-00186
                         AND inventory_item_id = c3_rec.ordered_item_id;
               END LOOP;*/
         
         --Added below by Maha for ver 2.5
               
             OPEN cur_load(x_header_rec.header_id);
        LOOP
            FETCH cur_load BULK COLLECT INTO l_om_tax_tbl_type;
            FORALL n IN 1..l_om_tax_tbl_type.COUNT
            
            UPDATE xxwc_om_quote_lines
                     SET tax_amount = l_om_tax_tbl_type(n).tax_amount
                   WHERE     quote_number = i_quote_number
                         AND TO_CHAR (line_seq) = l_om_tax_tbl_type(n).attribute18 
                         AND inventory_item_id =l_om_tax_tbl_type(n).inventory_item_id;
                         
            EXIT WHEN cur_load%NOTFOUND;
        END LOOP;
        CLOSE cur_load;      

               COMMIT;               
             
            ELSE
               o_msg_data := 'Error during tax calculation';
               o_return_status := l_return_status;
            END IF;

            --IF i_call_mode <> 'O' --Commented and added below by Maha for ver 2.5
            IF i_call_mode = 'T'
            THEN
               oe_order_pub.Delete_Order (
                  p_header_id        => x_header_rec.header_id,
                  p_org_id           => l_org_id,
                  p_operating_unit   => NULL                           -- MOAC
                                            ,
                  x_return_status    => l_return_status,
                  x_msg_count        => l_msg_count,
                  x_msg_data         => l_msg_data);

               IF l_return_status = FND_API.G_RET_STS_SUCCESS
               THEN
                  o_msg_data := NULL;
                  o_return_status := l_return_status;
                  COMMIT;
               ELSE
                  o_msg_data :=
                        'Issue with deleting Order '
                     || x_header_rec.order_number;
                  o_return_status := l_return_status;
               END IF;          
    
  
           -- ELSE     --commented and added ELSIF for ver 2.5

              ELSIF i_call_mode = 'O'  THEN
               UPDATE xxwc_om_quote_headers
                  SET attribute2 = x_header_rec.order_number
                WHERE quote_number = i_quote_number; 
     

               COMMIT;
               o_msg_data :=
                  'Order Number ' || x_header_rec.order_number || ' created.';
               o_return_status := l_return_status;

--commented below for ver 2.5
--            END IF;
--            IF i_call_mode = 'O'   
--            THEN --Added by start Ram Talluri on 10/8/2013 for TMS-20131003-00186

               BEGIN
                  xxwc_om_quote_pkg.process_price_adjustments (
                     i_quote_number,
                     l_return_status,
                     l_return_msg);

                  IF l_return_status <> FND_API.G_RET_STS_SUCCESS
                  THEN
                     o_msg_data :=
                        'API failure in updating calculate price flag and price adjustments';
                     o_return_status := l_return_status;
                  ELSE         --Added else and commit by Maha
                  COMMIT;
                  END IF;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     NULL;
                  WHEN OTHERS
                  THEN
                     o_msg_data :=
                        'API failure in updating calculate price flag and price adjustments';
                     o_return_status := l_return_status;
               END;

--commented below by Maha for ver 2.5

/*               BEGIN
                  xxwc_om_quote_pkg.update_calc_price_flag (i_quote_number,
                                                            l_return_status,
                                                            l_return_msg);

                  IF l_return_status <> FND_API.G_RET_STS_SUCCESS
                  THEN
                     o_msg_data :=
                        'API failure in updating calculate price flag';
                     o_return_status := l_return_status;
                  END IF;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     NULL;
                  WHEN OTHERS
                  THEN
                     o_msg_data :=
                        'API failure in updating calculate price flag';
                     o_return_status := l_return_status;
               END; */

            END IF; --Added by end Ram Talluri on 10/8/2013 for TMS-20131003-00186

         ELSE
            -- Retrieve messages
            FOR i IN 1 .. l_msg_count
            LOOP
               Oe_Msg_Pub.get (p_msg_index       => i,
                               p_encoded         => Fnd_Api.G_FALSE,
                               p_data            => l_msg_data,
                               p_msg_index_out   => l_msg_index_out);
               lv_msg_data :=
                  lv_msg_data || ':' || SUBSTR (l_msg_data, 1, 100);
            END LOOP;

            o_msg_data := lv_msg_data;
            o_return_status := l_return_status;
         END IF;
      END LOOP;
   END calculate_tax;

   /*************************************************************************
      *   Function: xxwc_to_number (p_in_string VARCHAR2)
      *   PURPOSE:   Used to convert char value to to number.
      *
      *   REVISIONS:
      *   Ver        Date        Author                     Description
      *   ---------  ----------  ---------------         -------------------------
      *  1.0        10/08/2013  Ram Talluri               TMS-20131003-00186 - Custom Quote to SO Imports - Price Adjustments Missing
      **********************************************************************/
   FUNCTION xxwc_to_number (p_in_string VARCHAR2)
      RETURN NUMBER
   IS
      v_in_string       VARCHAR2 (2000);
      v_return_number   NUMBER := NULL;
   BEGIN
      --v_in_string := NVL (p_in_string, '0');
      v_in_string := p_in_string;

      BEGIN
         SELECT TO_NUMBER (v_in_string) INTO v_return_number FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_return_number := NULL;
      END;

      RETURN v_return_number;
   END xxwc_to_number;

   /*************************************************************************
      *  Procedure: process_price_adjustments
      *   PURPOSE:   Used to convert char value to to number.
      *
      *   REVISIONS:
      *   Ver        Date        Author                     Description
      *   ---------  ----------  ---------------         -------------------------
      *  1.0        10/08/2013  Ram Talluri               Added new function and a procedure -TMS-20131003-00186 - Custom Quote to SO Imports - Price Adjustments Missing
      *  2.2        02/19/2014  Ram Talluri                 TMS #20140219-00158 - Imoort price adjustments correctly
      *  2.3        04/10/2014  Maharajan Shunmugam         TMS# 20140407-00346  - Performace issue
      *  2.4        10/20/2014  Maharajan Shunmugam         TMS# 20141001-00162 Canada Multi Org changes
      **********************************************************************/
   PROCEDURE process_price_adjustments (i_quote_number    IN     NUMBER,
                                        o_return_status      OUT VARCHAR,
                                        o_msg_data           OUT VARCHAR)
   IS

        /* Initialize the proper Context */
--      l_org_id                         NUMBER := fnd_profile.VALUE ('ORG_ID');      -- commented and added below for ver# 2.4
--      l_application_id                 NUMBER := fnd_profile.VALUE ('RESP_APPL_ID');
        l_org_id                         NUMBER := MO_GLOBAL.GET_CURRENT_ORG_ID;
        l_application_id                 NUMBER := fnd_global.resp_appl_id; 
      /* MZ4Md211 */
     -- l_responsibility_id              NUMBER := fnd_profile.VALUE ('RESP_ID');         -- commented and added below for ver# 2.4
     -- l_user_id                        NUMBER := fnd_profile.VALUE ('USER_ID');
        l_responsibility_id              NUMBER := fnd_global.resp_id;
        l_user_id                        NUMBER := fnd_global.user_id;

      /* Initialize the record to G_MISS to enable defaulting */
      l_header_rec                     OE_ORDER_PUB.Header_Rec_Type
                                          := OE_ORDER_PUB.G_MISS_HEADER_REC;
      l_old_header_rec                 OE_ORDER_PUB.Header_Rec_Type;

      l_line_tbl                       OE_ORDER_PUB.Line_Tbl_Type;
      l_old_line_tbl                   OE_ORDER_PUB.Line_Tbl_Type;

      l_action_request_tbl             OE_ORDER_PUB.Request_Tbl_Type;

      l_line_Adj_Tbl                   oe_order_pub.line_adj_tbl_type;

      x_header_rec                     OE_ORDER_PUB.Header_Rec_Type;
      x_header_val_rec                 OE_ORDER_PUB.Header_Val_Rec_Type;
      x_Header_Adj_tbl                 OE_ORDER_PUB.Header_Adj_Tbl_Type;
      x_Header_Adj_val_tbl             OE_ORDER_PUB.Header_Adj_Val_Tbl_Type;
      x_Header_price_Att_tbl           OE_ORDER_PUB.Header_Price_Att_Tbl_Type;
      x_Header_Adj_Att_tbl             OE_ORDER_PUB.Header_Adj_Att_Tbl_Type;
      x_Header_Adj_Assoc_tbl           OE_ORDER_PUB.Header_Adj_Assoc_Tbl_Type;
      x_Header_Scredit_tbl             OE_ORDER_PUB.Header_Scredit_Tbl_Type;
      x_Header_Scredit_val_tbl         OE_ORDER_PUB.Header_Scredit_Val_Tbl_Type;
      x_line_tbl                       OE_ORDER_PUB.Line_Tbl_Type;
      x_line_val_tbl                   OE_ORDER_PUB.Line_Val_Tbl_Type;
      x_Line_Adj_tbl                   OE_ORDER_PUB.Line_Adj_Tbl_Type;
      x_Line_Adj_val_tbl               OE_ORDER_PUB.Line_Adj_Val_Tbl_Type;
      x_Line_price_Att_tbl             OE_ORDER_PUB.Line_Price_Att_Tbl_Type;
      x_Line_Adj_Att_tbl               OE_ORDER_PUB.Line_Adj_Att_Tbl_Type;
      x_Line_Adj_Assoc_tbl             OE_ORDER_PUB.Line_Adj_Assoc_Tbl_Type;
      x_Line_Scredit_tbl               OE_ORDER_PUB.Line_Scredit_Tbl_Type;
      x_Line_Scredit_val_tbl           OE_ORDER_PUB.Line_Scredit_Val_Tbl_Type;
      x_Lot_Serial_tbl                 OE_ORDER_PUB.Lot_Serial_Tbl_Type;
      x_Lot_Serial_val_tbl             OE_ORDER_PUB.Lot_Serial_Val_Tbl_Type;
      x_action_request_tbl             OE_ORDER_PUB.Request_Tbl_Type;
      
      l_om_tax_tbl_type       xxwc_om_quote_pkg.XXWC_OM_TAX_TBL_TYPE := xxwc_om_quote_pkg.G_MISS_XXWC_OM_TAX_TBL;  --Added below by Maha for ver 2.3

      l_return_status                  VARCHAR2 (2000);
      l_msg_count                      NUMBER;
      l_msg_data                       VARCHAR2 (2000);
      lv_msg_data                      VARCHAR2 (2000);

      l_line_cnt                       NUMBER := 0;
      l_top_model_line_index           NUMBER;
      l_link_to_line_index             NUMBER;

      l_msg_index_out                  NUMBER (10);

      l_record_exists                  VARCHAR2 (1) := 'N';

      l_control_rec                    oe_globals.control_rec_type;

      l_header_id                      NUMBER; ---Ram Talluri 11/12/2013 TMS -20131112-00332

      --added below variables  02/19/2014  Ram Talluri                 TMS #20140219-00158 - Imoort price adjustments correctly
      l_old_operand                    NUMBER;
      l_old_adjusted_amount            NUMBER;
      l_old_operand_per_pqty           NUMBER;
      l_old_adjusted_amount_per_pqty   NUMBER;
      l_old_arithmetic_operator        VARCHAR2 (100);
      
      CURSOR c1
      IS
         SELECT /*+ index(a XXWC_OE_ORDER_LN_LUD1) */--Ram Talluri 7/30/2014 TMS #20140609-00203 index hint updated to reflect the latest index name changes.
                oha.header_id,
                oha.order_number,
                ola.line_id,
                ola.UNIT_SELLING_PRICE,
                ola.unit_list_price,
                xoql.line_seq,
                xoql.inventory_item_id,
                xoql.average_cost,
                xoql.special_cost,
                --ROUND(xoql.gm_selling_price,3) gm_selling_price,--Rounding factor chnaged from 2 to 3 by Ram Talluri on 4/1/2014 for TMS #20140308-00014--version 2.6
                ROUND(xoql.gm_selling_price,5) gm_selling_price,
                xoql.attribute1,
                xoql.line_quantity,
                xoql.gm_percent,
                xoql.list_price,
                xoql.tax_amount,
                --added below column by Maha for ver 2.3
                /*NVL (      ROUND (
                             xxwc_om_quote_pkg.xxwc_to_number (
                                xoql.attribute1),
                             3),
                          ROUND (ola.unit_selling_price, 3)) gm_changed_selling_price*/--version 2.6
                NVL (      ROUND (
                             xxwc_om_quote_pkg.xxwc_to_number (
                                xoql.attribute1),
                             5),
                          ROUND (ola.unit_selling_price, 5)) gm_changed_selling_price--version 2.6
           FROM xxwc_om_quote_headers xoqh,
                oe_order_headers oha,
                oe_order_lines ola,
                xxwc_om_quote_lines xoql
          WHERE     1 = 1
                AND xoqh.attribute2 = TO_CHAR (oha.order_number)
                AND xoqh.quote_number = xoql.quote_number
                AND xoql.inventory_item_id = ola.inventory_item_id
                AND xoql.LINE_QUANTITY = ola.ordered_quantity
                AND xoqh.organization_id = ola.ship_from_org_id
                AND oha.header_id = ola.header_id
                --AND ola.creation_date >= SYSDATE - 0.006 -- commented and Added below by Maha for ver 2.3
              AND TRUNC(ola.last_update_date)>= TRUNC(SYSDATE)
               AND xoqh.quote_number = i_quote_number       --Added and commented below by Maha for ver 2.3
               -- AND xoql.quote_number = i_quote_number
                AND TO_CHAR (xoql.line_seq) = ola.attribute18;
                /*AND ROUND (xoql.gm_selling_price, 2) <>
                       NVL (
                          ROUND (
                             xxwc_om_quote_pkg.xxwc_to_number (
                                xoql.attribute1),
                             5),
                          ROUND (ola.unit_selling_price, 2));*/--Commented by Ram Talluri on 3/20/2014 for TMS #20140308-00014
--commented below by Maha for ver 2.3
   /*             AND ROUND (xoql.gm_selling_price, 3) <>
                       NVL (
                          ROUND ( xxwc_om_quote_pkg.xxwc_to_number (
                                xoql.attribute1),
                             3),
                          ROUND (ola.unit_selling_price, 3));*/--Added by Ram Talluri on 3/20/2014 for TMS #20140308-00014
   --Added below by Maha for ver 2.3
   CURSOR cur_load(p_header_id NUMBER) IS
     SELECT b.line_number,
            b.attribute4,
            b.ordered_item_id,
            b.tax_value,
            b.ordered_quantity,
            b.attribute18
     FROM   oe_order_lines b
      WHERE header_id = p_header_id ;

 BEGIN
      fnd_global.apps_initialize (l_user_id,
                                  l_responsibility_id,
                                  l_application_id,
                                  NULL);
      mo_global.init ('ONT');
      mo_global.set_policy_context ('S', l_org_id);

      oe_msg_pub.initialize;

      l_record_exists := 'N';
   
      FOR c1_rec IN c1
      LOOP
         EXIT WHEN c1%NOTFOUND;

       --Added below by Maha for ver 2.3
       IF c1_rec.gm_selling_price <> c1_rec.gm_changed_selling_price
       THEN

         --added below block  02/19/2014  Ram Talluri                 TMS #20140219-00158 - Imoort price adjustments correctly

         BEGIN
            SELECT operand,
                   adjusted_amount,
                   operand_per_pqty,
                   adjusted_amount_per_pqty,
                   arithmetic_operator
              INTO l_old_operand,
                   l_old_adjusted_amount,
                   l_old_operand_per_pqty,
                   l_old_adjusted_amount_per_pqty,
                   l_old_arithmetic_operator
              FROM oe_price_adjustments opj
             WHERE     1 = 1
                   AND header_id = c1_rec.header_id
                   AND line_id = c1_rec.line_id
                   AND LIST_LINE_TYPE_CODE = 'DIS'
                   AND automatic_flag = 'Y';
         EXCEPTION
            WHEN OTHERS
            THEN
               l_old_operand := NULL;
               l_old_adjusted_amount := NULL;
               l_old_operand_per_pqty := NULL;
               l_old_adjusted_amount_per_pqty := NULL;
               l_old_arithmetic_operator := NULL;
               l_old_arithmetic_operator := NULL;
         END;


         l_line_cnt := l_line_cnt + 1;
         l_line_tbl (l_line_cnt) := OE_ORDER_PUB.G_MISS_LINE_REC;
         l_line_tbl (l_line_cnt).header_id := c1_rec.header_id;
         l_line_tbl (l_line_cnt).line_id := c1_rec.line_id;
         l_line_tbl (l_line_cnt).calculate_price_flag := 'N';
         l_line_tbl (l_line_cnt).unit_selling_price := c1_rec.gm_selling_price;
         l_line_tbl (l_line_cnt).operation := OE_GLOBALS.G_OPR_UPDATE;

         --Price adjustments
         l_line_Adj_Tbl (l_line_cnt) := OE_ORDER_PUB.G_MISS_LINE_ADJ_REC;
         l_line_Adj_Tbl (l_line_cnt).operation := OE_GLOBALS.G_OPR_CREATE;
         l_line_Adj_Tbl (l_line_cnt).price_adjustment_id := fnd_api.g_miss_num;
         l_Line_Adj_Tbl (l_line_cnt).header_id := c1_rec.header_id; --header_id of the sales order
         l_line_Adj_Tbl (l_line_cnt).line_id := c1_rec.line_id; --line_id of the sales order line
         l_line_Adj_Tbl (l_line_cnt).automatic_flag := 'N';
         l_line_Adj_Tbl (l_line_cnt).applied_flag := 'Y';
         l_line_Adj_Tbl (l_line_cnt).updated_flag := 'Y'; --Optional, this is the fixed flag.
         l_line_Adj_Tbl (l_line_cnt).list_header_id := 26473; --list_header_id of the adjustment -NEW_PRICE_DISCOUNT modifier
         l_line_Adj_Tbl (l_line_cnt).list_line_id := 22909; --list_line_id of the adjustment - NEW_PRICE_DISCOUNT modifier line
         l_line_Adj_Tbl (l_line_cnt).list_line_type_code := 'DIS';
         l_line_Adj_tbl (l_line_cnt).modifier_level_code := 'LINE';


         --added below logic for price adjustments-  02/19/2014  Ram Talluri TMS #20140219-00158 - Imoort price adjustments correctly
         --Rounding factor chnaged from 2 to 3 by Ram Talluri on 4/1/2014 for TMS #20140308-00014
         --Version 2.6 rounding factor changed from 3 to 5

         IF l_old_OPERAND IS NULL
         THEN
            l_line_Adj_Tbl (l_line_cnt).operand :=
               ROUND (
                    c1_rec.unit_list_price
                  - (c1_rec.unit_list_price - c1_rec.gm_selling_price),
                  5);

            l_line_Adj_Tbl (l_line_cnt).OPERAND_PER_PQTY :=
               ROUND (
                    c1_rec.unit_list_price
                  - (c1_rec.unit_list_price - c1_rec.gm_selling_price),
                  5);

            l_Line_Adj_Tbl (l_line_cnt).adjusted_amount :=
               ROUND (c1_rec.gm_selling_price - c1_rec.unit_list_price, 5);

            l_Line_Adj_Tbl (l_line_cnt).adjusted_amount_per_pqty :=
               ROUND (c1_rec.gm_selling_price - c1_rec.unit_list_price, 5);

            l_line_Adj_Tbl (l_line_cnt).arithmetic_operator := 'NEWPRICE';
         ELSIF     l_old_OPERAND IS NOT NULL
               AND l_old_ARITHMETIC_OPERATOR NOT IN ('%', 'AMT')
         THEN
            l_line_Adj_Tbl (l_line_cnt).operand :=
               ROUND (
                    c1_rec.unit_list_price
                  - (l_old_OPERAND - c1_rec.gm_selling_price),
                  5);

            l_line_Adj_Tbl (l_line_cnt).OPERAND_PER_PQTY :=
               ROUND (
                    c1_rec.unit_list_price
                  - (l_old_OPERAND - c1_rec.gm_selling_price),
                  5);

            l_Line_Adj_Tbl (l_line_cnt).adjusted_amount :=
               ROUND (c1_rec.gm_selling_price - l_old_OPERAND, 5);

            l_Line_Adj_Tbl (l_line_cnt).adjusted_amount_per_pqty :=
               ROUND (c1_rec.gm_selling_price - l_old_OPERAND, 5);

            l_line_Adj_Tbl (l_line_cnt).arithmetic_operator := 'NEWPRICE';
         ELSIF     l_old_OPERAND IS NOT NULL
               AND l_old_ARITHMETIC_OPERATOR IN ('%', 'AMT')
         THEN
            l_line_Adj_Tbl (l_line_cnt).operand :=
               ROUND (
                    c1_rec.unit_list_price
                  - (c1_rec.unit_selling_price - c1_rec.gm_selling_price),
                  5);

            l_line_Adj_Tbl (l_line_cnt).OPERAND_PER_PQTY :=
               ROUND (
                    c1_rec.unit_list_price
                  - (c1_rec.unit_selling_price - c1_rec.gm_selling_price),
                  5);

            l_Line_Adj_Tbl (l_line_cnt).adjusted_amount :=
               ROUND (c1_rec.gm_selling_price - c1_rec.unit_selling_price, 5);

            l_Line_Adj_Tbl (l_line_cnt).adjusted_amount_per_pqty :=
               ROUND (c1_rec.gm_selling_price - c1_rec.unit_selling_price, 5);

            l_line_Adj_Tbl (l_line_cnt).arithmetic_operator := 'NEWPRICE';
         END IF;

         --Commented old logic for price adjustments-  02/19/2014  Ram Talluri TMS #20140219-00158 - Imoort price adjustments correctly
         /*

                 l_Line_Adj_Tbl (l_line_cnt).adjusted_amount :=
                      c1_rec.gm_selling_price
                    - NVL (xxwc_om_quote_pkg.xxwc_to_number (c1_rec.attribute1),
                           c1_rec.unit_selling_price);

                 l_Line_Adj_Tbl (l_line_cnt).adjusted_amount_per_pqty :=
                      c1_rec.gm_selling_price
                    - NVL (xxwc_om_quote_pkg.xxwc_to_number (c1_rec.attribute1),
                           c1_rec.unit_selling_price);

                 l_line_Adj_Tbl (l_line_cnt).arithmetic_operator := 'NEWPRICE';

                 l_line_Adj_Tbl (l_line_cnt).operand :=
                      c1_rec.unit_list_price
                    + (  c1_rec.gm_selling_price
                       - NVL (xxwc_om_quote_pkg.xxwc_to_number (c1_rec.attribute1),
                              c1_rec.unit_selling_price));

                 l_line_Adj_Tbl (l_line_cnt).OPERAND_PER_PQTY :=
                      c1_rec.unit_list_price
                    + (  c1_rec.gm_selling_price
                       - NVL (xxwc_om_quote_pkg.xxwc_to_number (c1_rec.attribute1),
                              c1_rec.unit_selling_price));*/

       
         l_record_exists := 'Y';

    --ADDED below ELSIF by Maha for ver 2.3
        ELSIF c1_rec.gm_selling_price =  c1_rec.gm_changed_selling_price
        THEN
        
          l_line_cnt := l_line_cnt + 1;
         l_line_tbl (l_line_cnt) := OE_ORDER_PUB.G_MISS_LINE_REC;
         l_line_tbl (l_line_cnt).operation := OE_GLOBALS.G_OPR_UPDATE;
         l_line_tbl (l_line_cnt).header_id := c1_rec.header_id;
         l_line_tbl (l_line_cnt).line_id := c1_rec.line_id;
         l_line_tbl (l_line_cnt).calculate_price_flag := 'N';

         l_record_exists := 'Y';
         
         END IF;

      END LOOP;

      IF l_record_exists = 'Y' AND l_line_cnt > 0 
      THEN
         OE_ORDER_PUB.Process_Order (
            p_api_version_number       => 1,
            p_org_id                   => l_org_id,
            p_init_msg_list            => FND_API.G_TRUE,
            p_return_values            => FND_API.G_TRUE,
            p_action_commit            => FND_API.G_TRUE,
            x_return_status            => l_return_status,
            x_msg_count                => l_msg_count,
            x_msg_data                 => l_msg_data,
            p_action_request_tbl       => l_action_request_tbl,
            p_header_rec               => l_header_rec,
            --p_old_header_rec           => l_old_header_rec,
            p_line_tbl                 => l_line_tbl,
            --p_old_line_tbl             => l_old_line_tbl,
            p_line_adj_tbl             => l_line_Adj_tbl,
            x_header_rec               => x_header_rec,
            x_header_val_rec           => x_header_val_rec,
            x_Header_Adj_tbl           => x_Header_Adj_tbl,
            x_Header_Adj_val_tbl       => x_Header_Adj_val_tbl,
            x_Header_price_Att_tbl     => x_Header_Price_Att_Tbl,
            x_Header_Adj_Att_tbl       => x_Header_Adj_Att_Tbl,
            x_Header_Adj_Assoc_tbl     => x_Header_Adj_Assoc_Tbl,
            x_Header_Scredit_tbl       => x_Header_Scredit_Tbl,
            x_Header_Scredit_val_tbl   => x_Header_Scredit_Val_Tbl,
            x_line_tbl                 => x_Line_Tbl,
            x_line_val_tbl             => x_Line_Val_Tbl,
            x_Line_Adj_tbl             => x_Line_Adj_Tbl,
            x_Line_Adj_val_tbl         => x_Line_Adj_Val_Tbl,
            x_Line_price_Att_tbl       => x_Line_Price_Att_Tbl,
            x_Line_Adj_Att_tbl         => x_Line_Adj_Att_Tbl,
            x_Line_Adj_Assoc_tbl       => x_Line_Adj_Assoc_Tbl,
            x_Line_Scredit_tbl         => x_Line_Scredit_Tbl,
            x_Line_Scredit_val_tbl     => x_Line_Scredit_Val_Tbl,
            x_Lot_Serial_tbl           => x_Lot_Serial_Tbl,
            x_Lot_Serial_val_tbl       => x_Lot_Serial_Val_Tbl,
            x_action_request_tbl       => x_action_request_tbl);
      END IF;

      IF l_return_status = FND_API.G_RET_STS_SUCCESS        --Changed condition for ver 2.3 as API status is U
      THEN
         l_msg_data := 'Order_line Price adjustments created successfully';
         l_return_status := l_return_status;
         
        COMMIT;
         
         --commented below for ver 2.3
        /*   
         BEGIN                   ---Ram Talluri 11/12/2013 TMS -20131112-00332
            SELECT header_id
              INTO l_header_id
              FROM oe_order_headers_all
             WHERE order_number = (SELECT MAX (TO_NUMBER (attribute2))
                                     FROM xxwc.xxwc_om_quote_headers
                                    WHERE quote_number = i_quote_number);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_msg_data := 'ERROR IN RETRIEVING ORDER NUMBER';
         END;

         om_tax_util.calculate_tax (l_header_id, l_return_status);
         
       
         COMMIT;                 ---Ram Talluri 11/12/2013 TMS -20131112-00332

         IF l_return_status = FND_API.G_RET_STS_SUCCESS
         THEN
            FOR c2_rec IN (SELECT b.line_number,
                                  b.attribute4,
                                  b.ordered_item_id,
                                  b.tax_value,
                                  b.ordered_quantity,
                                  b.attribute18
                             FROM oe_order_lines_all b
                            WHERE header_id = x_header_rec.header_id)
            LOOP
            
               UPDATE xxwc_om_quote_lines
                  SET tax_amount = c2_rec.tax_value
                WHERE     quote_number = i_quote_number
                      AND inventory_item_id = c2_rec.ordered_item_id
                      AND line_quantity = c2_rec.ordered_quantity
                      AND TO_CHAR (line_seq) = c2_rec.attribute18;
            END LOOP;
            
            COMMIT;
            
         ELSE 
            l_msg_data := 'Error during tax calculation';
            l_return_status := l_return_status;
            NULL;
         END IF; */
         
      ELSE
         -- Retrieve messages
         FOR i IN 1 .. l_msg_count
         LOOP
            Oe_Msg_Pub.get (p_msg_index       => i,
                            p_encoded         => Fnd_Api.G_FALSE,
                            p_data            => l_msg_data,
                            p_msg_index_out   => l_msg_index_out);
            lv_msg_data := lv_msg_data || ':' || SUBSTR (l_msg_data, 1, 100);
         END LOOP;

         l_msg_data := lv_msg_data;
         l_return_status := l_return_status;
      END IF;
    --commented below for ver 2.3  
  /*    IF l_record_exists = 'Y' AND l_line_cnt > 0
      THEN
         OE_ORDER_PUB.Process_Order (
            p_api_version_number       => 1,
            p_org_id                   => l_org_id,
            p_init_msg_list            => FND_API.G_TRUE,
            p_return_values            => FND_API.G_TRUE,
            p_action_commit            => FND_API.G_TRUE,
            x_return_status            => l_return_status,
            x_msg_count                => l_msg_count,
            x_msg_data                 => l_msg_data,
            p_action_request_tbl       => l_action_request_tbl,
            p_header_rec               => l_header_rec,
            --p_old_header_rec           => l_old_header_rec,
            p_line_tbl                 => l_line_tbl,
            --p_old_line_tbl             => l_old_line_tbl,
            --p_line_adj_tbl             => l_line_Adj_tbl,
            x_header_rec               => x_header_rec,
            x_header_val_rec           => x_header_val_rec,
            x_Header_Adj_tbl           => x_Header_Adj_tbl,
            x_Header_Adj_val_tbl       => x_Header_Adj_val_tbl,
            x_Header_price_Att_tbl     => x_Header_Price_Att_Tbl,
            x_Header_Adj_Att_tbl       => x_Header_Adj_Att_Tbl,
            x_Header_Adj_Assoc_tbl     => x_Header_Adj_Assoc_Tbl,
            x_Header_Scredit_tbl       => x_Header_Scredit_Tbl,
            x_Header_Scredit_val_tbl   => x_Header_Scredit_Val_Tbl,
            x_line_tbl                 => x_Line_Tbl,
            x_line_val_tbl             => x_Line_Val_Tbl,
            x_Line_Adj_tbl             => x_Line_Adj_Tbl,
            x_Line_Adj_val_tbl         => x_Line_Adj_Val_Tbl,
            x_Line_price_Att_tbl       => x_Line_Price_Att_Tbl,
            x_Line_Adj_Att_tbl         => x_Line_Adj_Att_Tbl,
            x_Line_Adj_Assoc_tbl       => x_Line_Adj_Assoc_Tbl,
            x_Line_Scredit_tbl         => x_Line_Scredit_Tbl,
            x_Line_Scredit_val_tbl     => x_Line_Scredit_Val_Tbl,
            x_Lot_Serial_tbl           => x_Lot_Serial_Tbl,
            x_Lot_Serial_val_tbl       => x_Lot_Serial_Val_Tbl,
            x_action_request_tbl       => x_action_request_tbl);
      END IF;

      IF l_return_status = FND_API.G_RET_STS_SUCCESS
      THEN
         l_msg_data := 'Order_line calculate price flag update is success';
         l_return_status := l_return_status;
         COMMIT;
      ELSE
         -- Retrieve messages
         FOR i IN 1 .. l_msg_count
         LOOP
            Oe_Msg_Pub.get (p_msg_index       => i,
                            p_encoded         => Fnd_Api.G_FALSE,
                            p_data            => l_msg_data,
                            p_msg_index_out   => l_msg_index_out);
            lv_msg_data := lv_msg_data || ':' || SUBSTR (l_msg_data, 1, 100);
         END LOOP;

         l_msg_data := lv_msg_data;
         l_return_status := l_return_status;
      END IF;  */
      
    --  END IF;
      
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END process_price_adjustments;

   /*************************************************************************
      *  Procedure: update_calc_price_flag
      *   PURPOSE:   Used to convert char value to to number.
      *
      *   REVISIONS:
      *   Ver        Date        Author                     Description
      *   ---------  ----------  ---------------         -------------------------
      *  1.0        10/08/2013  Ram Talluri               Added new function and a procedure -TMS-20131003-00186 - Custom Quote to SO Imports - Price Adjustments Missing
      *  1.1        10/20/2014  Maharajan Shunmugam       TMS# 20141001-00162 Canada Multi Org Changes
      **********************************************************************/
   PROCEDURE update_calc_price_flag (i_quote_number    IN     NUMBER,
                                     o_return_status      OUT VARCHAR,
                                     o_msg_data           OUT VARCHAR)
   IS
      /* Initialize the proper Context */
     -- l_org_id                   NUMBER := fnd_profile.VALUE ('ORG_ID');              --commented and added below for ver#1.1
     -- l_application_id           NUMBER := fnd_profile.VALUE ('RESP_APPL_ID');
        l_org_id                   NUMBER := MO_GLOBAL.GET_CURRENT_ORG_ID;
        l_application_id           NUMBER := fnd_global.resp_appl_id; 

      /* MZ4Md211 */
     -- l_responsibility_id        NUMBER := fnd_profile.VALUE ('RESP_ID');               --commented and added below for ver#1.1
     -- l_user_id                  NUMBER := fnd_profile.VALUE ('USER_ID');
        l_responsibility_id        NUMBER := fnd_global.resp_id;
        l_user_id                  NUMBER := fnd_global.user_id;


      /* Initialize the record to G_MISS to enable defaulting */
      l_header_rec               OE_ORDER_PUB.Header_Rec_Type
                                    := OE_ORDER_PUB.G_MISS_HEADER_REC;
      l_old_header_rec           OE_ORDER_PUB.Header_Rec_Type;

      l_line_tbl                 OE_ORDER_PUB.Line_Tbl_Type;
      l_old_line_tbl             OE_ORDER_PUB.Line_Tbl_Type;

      l_action_request_tbl       OE_ORDER_PUB.Request_Tbl_Type;

      l_line_Adj_Tbl             oe_order_pub.line_adj_tbl_type;

      x_header_rec               OE_ORDER_PUB.Header_Rec_Type;
      x_header_val_rec           OE_ORDER_PUB.Header_Val_Rec_Type;
      x_Header_Adj_tbl           OE_ORDER_PUB.Header_Adj_Tbl_Type;
      x_Header_Adj_val_tbl       OE_ORDER_PUB.Header_Adj_Val_Tbl_Type;
      x_Header_price_Att_tbl     OE_ORDER_PUB.Header_Price_Att_Tbl_Type;
      x_Header_Adj_Att_tbl       OE_ORDER_PUB.Header_Adj_Att_Tbl_Type;
      x_Header_Adj_Assoc_tbl     OE_ORDER_PUB.Header_Adj_Assoc_Tbl_Type;
      x_Header_Scredit_tbl       OE_ORDER_PUB.Header_Scredit_Tbl_Type;
      x_Header_Scredit_val_tbl   OE_ORDER_PUB.Header_Scredit_Val_Tbl_Type;
      x_line_tbl                 OE_ORDER_PUB.Line_Tbl_Type;
      x_line_val_tbl             OE_ORDER_PUB.Line_Val_Tbl_Type;
      x_Line_Adj_tbl             OE_ORDER_PUB.Line_Adj_Tbl_Type;
      x_Line_Adj_val_tbl         OE_ORDER_PUB.Line_Adj_Val_Tbl_Type;
      x_Line_price_Att_tbl       OE_ORDER_PUB.Line_Price_Att_Tbl_Type;
      x_Line_Adj_Att_tbl         OE_ORDER_PUB.Line_Adj_Att_Tbl_Type;
      x_Line_Adj_Assoc_tbl       OE_ORDER_PUB.Line_Adj_Assoc_Tbl_Type;
      x_Line_Scredit_tbl         OE_ORDER_PUB.Line_Scredit_Tbl_Type;
      x_Line_Scredit_val_tbl     OE_ORDER_PUB.Line_Scredit_Val_Tbl_Type;
      x_Lot_Serial_tbl           OE_ORDER_PUB.Lot_Serial_Tbl_Type;
      x_Lot_Serial_val_tbl       OE_ORDER_PUB.Lot_Serial_Val_Tbl_Type;
      x_action_request_tbl       OE_ORDER_PUB.Request_Tbl_Type;

      l_return_status            VARCHAR2 (2000);
      l_msg_count                NUMBER;
      l_msg_data                 VARCHAR2 (2000);
      lv_msg_data                VARCHAR2 (2000);

      l_line_cnt                 NUMBER := 0;
      l_top_model_line_index     NUMBER;
      l_link_to_line_index       NUMBER;

      l_msg_index_out            NUMBER (10);

      l_record_exists            VARCHAR2 (1) := 'N';

      CURSOR c1
      IS
         SELECT oha.header_id,
                oha.order_number,
                ola.line_id,
                ola.UNIT_SELLING_PRICE,
                ola.unit_list_price,
                xoql.line_seq,
                xoql.inventory_item_id,
                xoql.average_cost,
                xoql.special_cost,
                xoql.gm_selling_price,
                xoql.attribute1,
                xoql.line_quantity,
                xoql.gm_percent,
                xoql.list_price,
                xoql.tax_amount
           FROM xxwc_om_quote_headers xoqh,
                oe_order_headers oha,
                oe_order_lines ola,
                xxwc_om_quote_lines xoql
          WHERE     1 = 1
                AND xoqh.attribute2 = TO_CHAR (oha.order_number)
                AND xoqh.quote_number = xoql.quote_number
                AND xoql.inventory_item_id = ola.inventory_item_id
                AND xoql.LINE_QUANTITY = ola.ordered_quantity
                AND xoqh.organization_id = ola.ship_from_org_id
                AND oha.header_id = ola.header_id
                AND ola.creation_date >= SYSDATE - 0.005
                --AND xoql.quote_number = i_quote_number Maha
                AND xoqh.quote_number = i_quote_number
                AND TO_CHAR (xoql.line_seq) = ola.attribute18
                /*AND ROUND (xoql.gm_selling_price, 3) =--Rounding factor chnaged from 2 to 3 by Ram Talluri on 4/1/2014 for TMS #20140308-00014
                       NVL (
                          ROUND (
                             xxwc_om_quote_pkg.xxwc_to_number (
                                xoql.attribute1),
                             3),
                          ROUND (ola.unit_selling_price, 3));--Rounding factor chnaged from 2 to 3 by Ram Talluri on 4/1/2014 for TMS #20140308-00014*/--version 2.6
                AND ROUND (xoql.gm_selling_price, 5) =--Rounding factor chnaged from 2 to 3 by Ram Talluri on 4/1/2014 for TMS #20140308-00014
                       NVL (
                          ROUND (
                             xxwc_om_quote_pkg.xxwc_to_number (
                                xoql.attribute1),
                             5),
                          ROUND (ola.unit_selling_price, 5));--version 2.6
   BEGIN
      fnd_global.apps_initialize (l_user_id,
                                  l_responsibility_id,
                                  l_application_id,
                                  NULL);
      mo_global.init ('ONT');
      mo_global.set_policy_context ('S', l_org_id);

      oe_msg_pub.initialize;

      l_record_exists := 'N';


      FOR c1_rec IN c1
      LOOP
         EXIT WHEN c1%NOTFOUND;
         l_line_cnt := l_line_cnt + 1;
         l_line_tbl (l_line_cnt) := OE_ORDER_PUB.G_MISS_LINE_REC;
         l_line_tbl (l_line_cnt).operation := OE_GLOBALS.G_OPR_UPDATE;
         l_line_tbl (l_line_cnt).header_id := c1_rec.header_id;
         l_line_tbl (l_line_cnt).line_id := c1_rec.line_id;
         l_line_tbl (l_line_cnt).calculate_price_flag := 'N';

         l_record_exists := 'Y';
      END LOOP;

      IF l_record_exists = 'Y' AND l_line_cnt > 0
      THEN
         OE_ORDER_PUB.Process_Order (
            p_api_version_number       => 1,
            p_org_id                   => l_org_id,
            p_init_msg_list            => FND_API.G_TRUE,
            p_return_values            => FND_API.G_TRUE,
            p_action_commit            => FND_API.G_TRUE,
            x_return_status            => l_return_status,
            x_msg_count                => l_msg_count,
            x_msg_data                 => l_msg_data,
            p_action_request_tbl       => l_action_request_tbl,
            p_header_rec               => l_header_rec,
            --p_old_header_rec           => l_old_header_rec,
            p_line_tbl                 => l_line_tbl,
            --p_old_line_tbl             => l_old_line_tbl,
            --p_line_adj_tbl             => l_line_Adj_tbl,
            x_header_rec               => x_header_rec,
            x_header_val_rec           => x_header_val_rec,
            x_Header_Adj_tbl           => x_Header_Adj_tbl,
            x_Header_Adj_val_tbl       => x_Header_Adj_val_tbl,
            x_Header_price_Att_tbl     => x_Header_Price_Att_Tbl,
            x_Header_Adj_Att_tbl       => x_Header_Adj_Att_Tbl,
            x_Header_Adj_Assoc_tbl     => x_Header_Adj_Assoc_Tbl,
            x_Header_Scredit_tbl       => x_Header_Scredit_Tbl,
            x_Header_Scredit_val_tbl   => x_Header_Scredit_Val_Tbl,
            x_line_tbl                 => x_Line_Tbl,
            x_line_val_tbl             => x_Line_Val_Tbl,
            x_Line_Adj_tbl             => x_Line_Adj_Tbl,
            x_Line_Adj_val_tbl         => x_Line_Adj_Val_Tbl,
            x_Line_price_Att_tbl       => x_Line_Price_Att_Tbl,
            x_Line_Adj_Att_tbl         => x_Line_Adj_Att_Tbl,
            x_Line_Adj_Assoc_tbl       => x_Line_Adj_Assoc_Tbl,
            x_Line_Scredit_tbl         => x_Line_Scredit_Tbl,
            x_Line_Scredit_val_tbl     => x_Line_Scredit_Val_Tbl,
            x_Lot_Serial_tbl           => x_Lot_Serial_Tbl,
            x_Lot_Serial_val_tbl       => x_Lot_Serial_Val_Tbl,
            x_action_request_tbl       => x_action_request_tbl);
      END IF;

      IF l_return_status = FND_API.G_RET_STS_SUCCESS
      THEN
         l_msg_data := 'Order_line calculate price flag update is success';
         l_return_status := l_return_status;
         COMMIT;
      ELSE
         -- Retrieve messages
         FOR i IN 1 .. l_msg_count
         LOOP
            Oe_Msg_Pub.get (p_msg_index       => i,
                            p_encoded         => Fnd_Api.G_FALSE,
                            p_data            => l_msg_data,
                            p_msg_index_out   => l_msg_index_out);
            lv_msg_data := lv_msg_data || ':' || SUBSTR (l_msg_data, 1, 100);
         END LOOP;

         l_msg_data := lv_msg_data;
         l_return_status := l_return_status;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END update_calc_price_flag;
   
   /*************************************************************************
    *   PROCEDURE Name: get_am_detail
    *
    *   PURPOSE:   get account manager name and email id
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    2.9     04/02/2018     Niraj K Ranjan        TMS#20180321-00087   WC Custom Quote Enhancements
   *****************************************************************************/
   PROCEDURE get_am_detail(p_quote_num   IN  NUMBER
                                ,p_am_name   OUT VARCHAR2
                                ,p_am_email  OUT VARCHAR2
								,p_am_designation  OUT VARCHAR2
								,p_resource_name   OUT VARCHAR2
								,p_retmsg          OUT VARCHAR2) 
   IS
      l_salesrep_name   jtf_rs_resource_extns_vl.source_name%TYPE;
	  l_resource_name   jtf_rs_resource_extns_vl.resource_name%TYPE;
      l_salesrep_email  jtf_rs_resource_extns_vl.source_email%TYPE;
	  l_sr_designation  xxwc_sr_salesreps.sr_title%TYPE;
      l_errmsg          VARCHAR2(2000);
      e_no_cust_id      EXCEPTION;
	  l_sec             VARCHAR2(100);
   BEGIN    
      l_salesrep_name  := '';
      l_salesrep_email := '';
      l_errmsg := '';
      IF p_quote_num IS NULL THEN
	     l_errmsg := 'Error@xxwc_om_quote_pkg.get_am_detail: '||'Quote Number is null';
         RAISE e_no_cust_id;
      END IF;
      
      BEGIN  --1st Begin
	     l_sec := 'fetch account manager detail based on cust id and cust site id';
         SELECT DISTINCT jrre.source_name  salesrep_name ,
                         REPLACE(jrre.source_email, ',', '') salesrep_email,
						 jrre.resource_name resource_name,
						 sr.sr_title sr_designation
                INTO l_salesrep_name,l_salesrep_email,l_resource_name,l_sr_designation
          FROM xxwc.XXWC_OM_QUOTE_HEADERS xoqh
		     , hz_cust_acct_sites_all     hcas
             , apps.hz_cust_accounts_all  hca
             , hz_cust_site_uses_all      hcsu
             , jtf_rs_salesreps           jrs
             , jtf_rs_resource_extns_vl   jrre
			 , xxwc_sr_salesreps sr
         WHERE 1 = 1
		   AND xoqh.quote_number        = p_quote_num
           AND xoqh.CUST_ACCOUNT_ID     = hca.cust_account_id
		   AND xoqh.SITE_USE_ID = hcsu.site_use_id
           AND hcas.cust_account_id     = hca.cust_account_id
           AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id
           AND hcsu.primary_salesrep_id = jrs.salesrep_id
           AND jrs.resource_id          = jrre.resource_id
           AND jrs.org_id               = hcas.org_id
           AND hcas.status              = 'A'
           AND hcsu.status              = 'A'
           AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)
		   AND jrs.salesrep_id  = sr.salesrep_id
           AND sr.active_status = 'A';
      EXCEPTION
         WHEN OTHERS THEN 
              l_salesrep_name  := '';
   		      l_salesrep_email := '';
			  l_resource_name  := '';
			  l_sr_designation := '';
			  l_errmsg := 'Error@xxwc_om_quote_pkg.get_am_detail:=> '||l_sec||' =>'||SUBSTR(SQLERRM,1,2000);
			  RAISE e_no_cust_id;
      END; 
      p_am_name  := l_salesrep_name;
   	  p_am_email := l_salesrep_email;
	  p_am_designation := l_sr_designation;
	  p_resource_name  := l_resource_name;
   EXCEPTION
      WHEN e_no_cust_id THEN
	     p_retmsg := l_errmsg;
      WHEN others THEN
   		   l_errmsg := SUBSTR(SQLERRM,1,2000);
		   p_retmsg := 'Error@xxwc_om_quote_pkg.get_am_detail=> '||l_sec||' =>'||l_errmsg;
   END get_am_detail;
   
   /*************************************************************************
    *   PROCEDURE Name: get_quote_lines_email_body
    *
    *   PURPOSE:   get quote lines detail in html tabular format to append in email body
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    2.9     04/02/2018     Niraj K Ranjan        TMS#20180321-00087   WC Custom Quote Enhancements
	3.0     05/02/2018     Niraj K Ranjan        TMS#20180430-00033   WC Quote expiry notifications enhancement
   *****************************************************************************/
   PROCEDURE get_quote_lines_email_body(p_quote_number       IN  NUMBER,
                                        p_organization_id    IN  NUMBER,
									  p_quote_line_msg_body OUT VARCHAR2,
									  p_retmsg            OUT VARCHAR2
									 )
   IS
	 
	  l_msg_header          VARCHAR2(2900);
	  l_msg_detail          VARCHAR2(25000);
	  l_msg_detail_temp     VARCHAR2(25000);
	  l_msg_footer          VARCHAR2(100);
	  l_full_body           VARCHAR2(28000);
	  l_errmsg              VARCHAR2(2000);
	  l_ora_exception       EXCEPTION; --Ver 3.0
	  PRAGMA EXCEPTION_INIT(l_ora_exception,-06502); --Ver 3.0
	  
	  CURSOR cr_quote_lines
	  IS
	    SELECT msi.segment1 product_value
               ,xoql.ITEM_DESC product_desc
			   ,CASE length(decode(instr(xoql.list_price,'.'),0,'00',substr(xoql.list_price,instr(xoql.list_price,'.')+1)))
                  WHEN 1 THEN 
                   '$'||trunc(xoql.list_price)||'.'||decode(instr(xoql.list_price,'.'),0,'00',substr(xoql.list_price,instr(xoql.list_price,'.')+1))||'0'
                  ELSE 
                   DECODE(xoql.list_price,NULL,'','$')||trunc(xoql.list_price)||DECODE(xoql.list_price,NULL,'','.')||decode(instr(xoql.list_price,'.'),0,'00',substr(xoql.list_price,instr(xoql.list_price,'.')+1))
                END list_price
			   /*,CASE length(decode(instr(xoql.average_cost,'.'),0,'00',substr(xoql.average_cost,instr(xoql.average_cost,'.')+1)))
                 WHEN 1 THEN 
                   '$'||trunc(xoql.average_cost)||'.'||decode(instr(xoql.average_cost,'.'),0,'00',substr(xoql.average_cost,instr(xoql.average_cost,'.')+1))||'0'
                  ELSE 
                   DECODE(xoql.average_cost,NULL,'','$')||trunc(xoql.average_cost)||DECODE(xoql.average_cost,NULL,'','.')||decode(instr(xoql.average_cost,'.'),0,'00',substr(xoql.average_cost,instr(xoql.average_cost,'.')+1))
                END average_cost*/
               ,CASE length(decode(instr(xoql.GM_SELLING_PRICE,'.'),0,'00',substr(xoql.GM_SELLING_PRICE,instr(xoql.GM_SELLING_PRICE,'.')+1)))
                  WHEN 1 THEN 
                   '$'||trunc(xoql.GM_SELLING_PRICE)||'.'||decode(instr(xoql.GM_SELLING_PRICE,'.'),0,'00',substr(xoql.GM_SELLING_PRICE,instr(xoql.GM_SELLING_PRICE,'.')+1))||'0'
                  ELSE 
                   DECODE(xoql.GM_SELLING_PRICE,NULL,'','$')||trunc(xoql.GM_SELLING_PRICE)||DECODE(xoql.GM_SELLING_PRICE,NULL,'','.')||decode(instr(xoql.GM_SELLING_PRICE,'.'),0,'00',substr(xoql.GM_SELLING_PRICE,instr(xoql.GM_SELLING_PRICE,'.')+1))
                END selling_price
               ,trunc(xoql.GM_PERCENT)||DECODE(xoql.GM_PERCENT,NULL,'','.')||decode(instr(round(xoql.GM_PERCENT,1),'.'),0,'0',substr(round(xoql.GM_PERCENT,1),instr(round(xoql.GM_PERCENT,1),'.')+1))||trunc(xoql.GM_PERCENT)||DECODE(xoql.GM_PERCENT,NULL,'','%') gross_margin
         FROM xxwc.XXWC_OM_QUOTE_LINES xoql,
              apps.mtl_system_items_b msi
         WHERE xoql.quote_number =  p_quote_number
         AND  msi.organization_id = p_organization_id
         AND  xoql.INVENTORY_ITEM_ID = msi.INVENTORY_ITEM_ID
		 ORDER BY xoql.LINE_SEQ;
         
   BEGIN
      
	  l_errmsg := '';
	  
	  l_msg_header := SUBSTR('<style type="text/css">
     .style1 {
     	border-style: solid;
     	border-width: 1px;
     }
     .style2 {
     	border: 1px solid #000000;
		background-color: #808080;
     }
     .style3 {
     	border: 1px solid #000000;
     }
     .style4 {
     	color: #FFFF00;
     	border: 1px solid #000000;
     	background-color: #000000;
     }
     .style5 {
     	font-size: large;
     }
     .style6 {
     	font-size: xx-large;
     }
     .style7 {
     	color: #FFCC00;
     }
     </style>
     <BR><table border="2" cellpadding="2" cellspacing="2" width="100%">'       ||
          '<td class="style2"><FONT size="2"><B>SKU Number</B></FONT></td>'     ||
          '<td class="style2"><FONT size="2"><B>SKU Description</B></FONT></td>'||
          '<td class="style2"><FONT size="2"><B>Market Price</B></FONT></td>'   ||
          '<td class="style2"><FONT size="2"><B>Selling Price</B></FONT></td>'  ||
          '<td class="style2"><FONT size="2"><B>Margin</B></FONT></td>'         ||
          '<TR>',1,2900); --Ver 3.0 added substr
	  
	  FOR rec_quote_lines IN cr_quote_lines
	  LOOP
	     --Ver 3.0 added begin,exception,end
	     BEGIN
            l_msg_detail_temp := l_msg_detail_temp || 
             '<td class="style3">'||'<FONT size="2">'||rec_quote_lines.product_value||'</FONT>'||'</td>'||
		     '<td class="style3">'||'<FONT size="2">'||rec_quote_lines.product_desc||'</FONT>'||'</td>'|| 
             '<td class="style3">'||'<FONT size="2">'||rec_quote_lines.list_price||'</FONT>'||'</td>'||
		     '<td class="style3">'||'<FONT size="2">'||rec_quote_lines.selling_price||'</FONT>'||'</td>'||
		     '<td class="style3">'||'<FONT size="2">'||rec_quote_lines.gross_margin||'</FONT>'||'</td>'||
             '<TR>'; --Ver 3.0 added substr
		  EXCEPTION
		     WHEN l_ora_exception THEN
				p_retmsg := 'Email body length exceeded';
			    EXIT;
		  END;
		  l_msg_detail := l_msg_detail_temp; --Ver 3.0
	  END LOOP;
      l_msg_footer :=  '</tr></tr></table>';
	  IF l_msg_detail IS NOT NULL THEN 
         l_full_body       := SUBSTR(l_msg_header || l_msg_detail || l_msg_footer,1,28000); --Ver 3.0 added substr
      END IF;  
      p_quote_line_msg_body := l_full_body;
   EXCEPTION
      WHEN OTHERS THEN
         p_quote_line_msg_body := NULL;
		 l_errmsg := SUBSTR(SQLERRM,1,2000);
		 p_retmsg := l_errmsg;
   END get_quote_lines_email_body;
   
   /*************************************************************************
    *   PROCEDURE Name: send_html_email
    *
    *   PURPOSE:   send html email to multiple recepient.
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
     2.9     04/02/2018     Niraj K Ranjan        TMS#20180321-00087   WC Custom Quote Enhancements
   *****************************************************************************/
   PROCEDURE send_html_email (
	  p_to              IN   VARCHAR2,
	  p_cc              IN   VARCHAR2 DEFAULT NULL,
      p_bcc             IN   VARCHAR2 DEFAULT NULL,
      p_from            IN   VARCHAR2,
      p_subject         IN   VARCHAR2,
      p_text            IN   VARCHAR2 DEFAULT NULL,
      p_html            IN   VARCHAR2 DEFAULT NULL,
      p_smtp_hostname   IN   VARCHAR2,
      p_smtp_portnum    IN   VARCHAR2,
	  p_retmsg          OUT  VARCHAR2 --ver 5.9
   )
   IS
      l_boundary     VARCHAR2 (255)      DEFAULT 'a1b2c3d4e3f2g1';
      l_connection   UTL_SMTP.connection;
      l_body_html    CLOB                := EMPTY_CLOB;
      --This will be the email message
      l_offset       NUMBER;
      l_ammount      NUMBER;
      l_temp         VARCHAR2 (32767)    DEFAULT NULL;
	  l_sec          VARCHAR2(200); --ver 5.9
	  CURSOR cr_emails 
	  IS (SELECT regexp_substr(p_to,'[^;]+', 1, LEVEL) email_id
          FROM dual 
		  CONNECT BY regexp_substr(p_to, '[^;]+', 1, LEVEL) IS NOT NULL);
   BEGIN
      l_sec := 'Open Connection'; --ver 5.9
      l_connection := UTL_SMTP.open_connection (p_smtp_hostname, p_smtp_portnum);
      UTL_SMTP.helo (l_connection, p_smtp_hostname);
      UTL_SMTP.mail (l_connection, p_from);
	  l_sec := 'Set UTL_SMTP rcpt email id'; --ver 5.9
	  FOR rec IN cr_emails
      LOOP
	     IF TRIM(rec.email_id) IS NOT NULL THEN
		    UTL_SMTP.rcpt (l_connection, TRIM(rec.email_id));
		 END IF;
      END LOOP;
	  --UTL_SMTP.rcpt (l_connection, TRIM(p_cc));
	  --UTL_SMTP.rcpt (l_connection, TRIM(p_bcc));
      l_temp := l_temp || 'MIME-Version: 1.0' || CHR (13) || CHR (10);
      l_temp := l_temp || 'To: ' || p_to || CHR (13) || CHR (10);
      l_temp := l_temp || 'From: ' || p_from || CHR (13) || CHR (10);
      l_temp := l_temp || 'Subject: ' || p_subject || CHR (13) || CHR (10);
      l_temp := l_temp || 'Reply-To: ' || p_from || CHR (13) || CHR (10);
      l_temp :=
            l_temp
         || 'Content-Type: multipart/alternative; boundary='
         || CHR (34)
         || l_boundary
         || CHR (34)
         || CHR (13)
         || CHR (10);
      ----------------------------------------------------
      -- Write the headers
      l_sec := 'call dbms_lob.createtemporary'; --ver 5.9
      DBMS_LOB.createtemporary (l_body_html, FALSE, 10);
	  l_sec := 'call dbms_lob.write 1'; --ver 5.9
      DBMS_LOB.WRITE (l_body_html, LENGTH (l_temp), 1, l_temp);
      ----------------------------------------------------
      -- Write the text boundary
      l_offset := DBMS_LOB.getlength (l_body_html) + 1;
      l_temp := '--' || l_boundary || CHR (13) || CHR (10);
      l_temp :=
            l_temp
         || 'content-type: text/plain; charset=us-ascii'
         || CHR (13)
         || CHR (10)
         || CHR (13)
         || CHR (10);
	  l_sec := 'call dbms_lob.write 2'; --ver 5.9
      DBMS_LOB.WRITE (l_body_html, LENGTH (l_temp), l_offset, l_temp);
      ----------------------------------------------------
      -- Write the plain text portion of the email
      l_offset := DBMS_LOB.getlength (l_body_html) + 1;
	  l_sec := 'call dbms_lob.write 3'; --ver 5.9
      DBMS_LOB.WRITE (l_body_html, LENGTH (p_text), l_offset, p_text);
      ----------------------------------------------------
      -- Write the HTML boundary
      l_temp :=
            CHR (13)
         || CHR (10)
         || CHR (13)
         || CHR (10)
         || '--'
         || l_boundary
         || CHR (13)
         || CHR (10);
      l_temp :=
            l_temp
         || 'content-type: text/html;'
         || CHR (13)
         || CHR (10)
         || CHR (13)
         || CHR (10);
      l_offset := DBMS_LOB.getlength (l_body_html) + 1;
	  l_sec := 'call dbms_lob.write 4'; --ver 5.9
      DBMS_LOB.WRITE (l_body_html, LENGTH (l_temp), l_offset, l_temp);
      ----------------------------------------------------
      -- Write the HTML portion of the message
      l_offset := DBMS_LOB.getlength (l_body_html) + 1;
	  l_sec := 'call dbms_lob.write 5'; --ver 5.9
      DBMS_LOB.WRITE (l_body_html, LENGTH (p_html), l_offset, p_html);
      ----------------------------------------------------
      -- Write the final html boundary
      l_temp := CHR (13) || CHR (10) || '--' || l_boundary || '--' || CHR (13);
      l_offset := DBMS_LOB.getlength (l_body_html) + 1;
	  l_sec := 'call dbms_lob.write 6'; --ver 5.9
      DBMS_LOB.WRITE (l_body_html, LENGTH (l_temp), l_offset, l_temp);
      ----------------------------------------------------
       -- Send the email in 1900 byte chunks to UTL_SMTP
      l_offset := 1;
      l_ammount := 1900;
      UTL_SMTP.open_data (l_connection);
      l_sec := 'call utl_smtp.write_data'; --ver 5.9
      WHILE l_offset < DBMS_LOB.getlength (l_body_html)
      LOOP
         UTL_SMTP.write_data (l_connection,
                              DBMS_LOB.SUBSTR (l_body_html,
                                               l_ammount,
                                               l_offset
                                              )
                             );
         l_offset := l_offset + l_ammount;
         l_ammount :=
                     LEAST (1900, DBMS_LOB.getlength (l_body_html) - l_ammount);
      END LOOP;
      l_sec := 'Close connection'; --ver 5.9
      UTL_SMTP.close_data (l_connection);
      UTL_SMTP.quit (l_connection);
      DBMS_LOB.freetemporary (l_body_html);
   EXCEPTION --ver 5.9
      WHEN OTHERS THEN --ver 5.9
	     p_retmsg := SUBSTR(l_sec||'=>'||SQLERRM,1,2000); --ver 5.9
   END send_html_email;
     
   /*************************************************************************
    *   PROCEDURE Name: alert_quote_expiration
    *
    *   PURPOSE:   send FYI notifications 1 week prior of quote expiration.
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    2.9     04/02/2018     Niraj K Ranjan        TMS#20180321-00087   WC Custom Quote Enhancements
	3.0     05/02/2018     Niraj K Ranjan        TMS#20180430-00033   WC Quote expiry notifications enhancement
   *****************************************************************************/
   PROCEDURE alert_quote_expiration(errbuf              OUT VARCHAR2,
                                    retcode             OUT NUMBER) 
   IS
       g_dflt_email   fnd_user.email_address%TYPE  := 'HDSOracleDevelopers@hdsupply.com';
	   l_retcode NUMBER;
	   l_errbuf  CLOB;
       l_retmsg VARCHAR2(2000);
	   l_debug_stmt VARCHAR2(500); 
	   l_email_mime_type  varchar2(150) :='html; charset=us-ascii';
	   l_email_from     VARCHAR2(240) := 'dontreply@hdsupply.com';
       l_email_to       VARCHAR2(4000);
       l_email_cc       VARCHAR2(240);
       l_email_subject  VARCHAR2(240);
	   l_header_email_body  VARCHAR2(4000);
	   l_line_email_body VARCHAR2(28000) := NULL;
       l_email_body     VARCHAR2(32767);
       l_am_name   jtf_rs_resource_extns_vl.source_name%TYPE;
       l_am_email  jtf_rs_resource_extns_vl.source_email%TYPE;
	   l_resource_name   jtf_rs_resource_extns_vl.resource_name%TYPE;
	   l_am_designation      xxwc_sr_salesreps.sr_title%TYPE;
	   l_submitter_email_id       per_all_people_f.email_address%TYPE;
	   l_host            VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
       l_hostport        VARCHAR2(20) := '25';
       e_prc_exception     EXCEPTION;
	   l_Customer_number    apps.hz_cust_accounts.account_number%type;
	   l_Customer_name      apps.hz_parties.party_name%type;
	   l_Customer_Job_Site  apps.hz_cust_site_uses_all.location%type;
	   l_submited_by         fnd_user.description%type;
	   l_branch_name         org_organization_definitions.organization_name%TYPE;
	   l_quote_expire_days   NUMBER := fnd_profile.value('XXWC_OM_QUOTE_EXPIRATION_NTF_DAYS');
	   --l_contact_string      VARCHAR2(200) := 'Please contact Contract_Pricing@whitecap.com for any questions';
	   
	   CURSOR cr_quote_hdr
	   IS
	      select quote_number,cust_account_id,primary_site_use_id,site_use_id,salesrep_id,organization_id,created_by,valid_until,expire_alert_sent
          from xxwc.XXWC_OM_QUOTE_HEADERS xoqh
          where 1=1
          and (((trunc(xoqh.VALID_UNTIL) -  trunc(sysdate)) > 0 AND (trunc(xoqh.VALID_UNTIL) -  trunc(sysdate)) <= fnd_profile.value('XXWC_OM_QUOTE_EXPIRATION_NTF_DAYS') AND NVL(EXPIRE_ALERT_SENT,'N') = 'N') OR
               ((trunc(xoqh.VALID_UNTIL) -  trunc(sysdate)) = 1 AND NVL(EXPIRE_ALERT_SENT,'N') = 'Y')
			  );
   BEGIN
      fnd_file.put_line(fnd_file.log,'Start Program '||to_char(sysdate,'DD-MON-YYYY HH24:MI:SS'));
      l_retcode := 0;
	  l_errbuf  := NULL;
      l_debug_stmt := 'fetch list of eligible quote';
	  FOR rec_quote_hdr IN cr_quote_hdr
	  LOOP
	     BEGIN
		    l_debug_stmt := 'Initialize variables';
		    l_retmsg                  := null;
		    l_Customer_number         := null;
	        l_Customer_name           := null;
	        l_Customer_Job_Site       := null;
	        l_branch_name             := null;
			l_am_name                 := null;
			l_am_email                := null;
			l_am_designation          := null;
			l_resource_name           := null;
			l_submited_by             := null;
			l_submitter_email_id      := null;
			l_email_to                := null;
			l_email_subject           := null;
			l_header_email_body       := null;
			l_line_email_body         := null;
			l_email_body              := null;
			
	        BEGIN
	   	      select hca.account_number Customer_number,
                     hp.party_name Customer_name,
                     hcsu.LOCATION Customer_Job_Site
	   	      INTO l_Customer_number,l_Customer_name,l_Customer_Job_Site
              from hz_parties hp,
                   hz_cust_accounts hca,
                   hz_locations hl,
                   hz_party_sites hps,
                   hz_cust_acct_sites_all hcas,
                   apps.hz_cust_site_uses_all hcsu  
              where 1=1
              and hca.cust_account_id = rec_quote_hdr.CUST_ACCOUNT_ID
              and hcsu.site_use_id = rec_quote_hdr.SITE_USE_ID
              AND hp.party_id = hca.party_id
              AND HL.LOCATION_ID = HPS.LOCATION_ID
              and hp.party_id = hps.party_id
              AND hca.cust_account_id = hcas.cust_account_id
              AND hps.party_site_id = hcas.party_site_id
              and hcas.cust_acct_site_id = hcsu.cust_acct_site_id;
	   	    EXCEPTION
	   	       WHEN OTHERS THEN
	   		      l_Customer_number := NULL;
	   		      l_Customer_name := NULL;
	   		      l_Customer_Job_Site := NULL;
	   		      l_retmsg := substr('Error while fetching Account/Site detail=>'||sqlerrm,1,2000);
	   		      fnd_file.put_line(fnd_file.log,'Error for quote# '||rec_quote_hdr.quote_number||'=>'||l_retmsg);
				  l_retmsg := NULL;
	   	    END;
	   	 
	   	    BEGIN
	           SELECT ood.organization_name
	   	       INTO l_branch_name
               FROM org_organization_definitions ood
               WHERE ood.organization_id = rec_quote_hdr.organization_id;
	        EXCEPTION
	         when OTHERS THEN
               l_branch_name := NULL;
	   		   l_retmsg := substr('Error while fetching branch name=>'||sqlerrm,1,2000);
	   		   fnd_file.put_line(fnd_file.log,'Error for quote# '||rec_quote_hdr.quote_number||'=>'||l_retmsg);
			   l_retmsg := NULL;
	        END;
	   	 
	   	    l_debug_stmt := 'Get Account Manager Detail';
	   	    get_am_detail(p_quote_num   => rec_quote_hdr.quote_number
                                      ,p_am_name         => l_am_name
                                      ,p_am_email        => l_am_email
	   	  	 					      ,p_am_designation  => l_am_designation
	   	  	 					      ,p_resource_name   => l_resource_name
	   	  	 					      ,p_retmsg          => l_retmsg ) ;
	   	    
	   	     IF l_retmsg IS NOT NULL THEN
	    	   fnd_file.put_line(fnd_file.log,'Error for quote# '||rec_quote_hdr.quote_number||'=>'||l_retmsg);
			   l_retmsg := NULL;
	    	 END IF;
	   	 
	   	     l_debug_stmt := 'get email id of created';
	         BEGIN
	   	        SELECT fu.description, NVL(fu.email_address,pap.email_address)
	   		    INTO l_submited_by,l_submitter_email_id
                FROM  apps.fnd_user fu,
                      apps.per_all_people_f pap
                WHERE fu.user_id = rec_quote_hdr.created_by
                AND   fu.person_party_id = pap.party_id
                AND   trunc(sysdate) between trunc(nvl(pap.effective_start_date,sysdate)) and trunc(nvl(pap.effective_end_date,sysdate));
	   	     EXCEPTION
	   	       WHEN OTHERS THEN
			     l_submited_by := null;
				 l_submitter_email_id := null;
	   		     l_retmsg := substr('Error while fetching Creater email id=>'||sqlerrm,1,2000);
	   		     fnd_file.put_line(fnd_file.log,'Error for quote# '||rec_quote_hdr.quote_number||'=>'||l_retmsg);
				 l_retmsg := NULL;
	   		     --RAISE e_prc_exception;
	   	     END;
	   	 
	   	    IF l_am_email IS NULL AND l_submitter_email_id IS NULL THEN
	   	       l_retmsg := substr('Non email id found to send notification',1,2000);
	   	  	   fnd_file.put_line(fnd_file.log,'Error for quote# '||rec_quote_hdr.quote_number||'=>'||l_retmsg);
			   l_retmsg := NULL;
	   	  	   RAISE e_prc_exception;
	   	    END IF;
	   	 
	   	    --Concatenate sales rep email address
	        l_debug_stmt := 'Concatenate account manager email address';
            IF l_am_email IS NOT NULL THEN
               l_email_to := l_am_email;
            END IF;
	   	    l_debug_stmt := 'Concatenate submitter email address';
	   	    IF l_submitter_email_id IS NOT NULL THEN
	           IF l_email_to IS NOT NULL THEN
                  l_email_to := l_email_to||';'||l_submitter_email_id;
               ELSE
                  l_email_to := l_submitter_email_id;
	           END IF;
            END IF;
	   	 
	   	    --Send email notification
	   	    IF l_email_to IS NOT NULL THEN
               --l_email_from    := 'dontreply@hdsupply.com';
	   	       l_debug_stmt := 'get email subject';
               l_email_subject := 'Action Required: Quote# '||rec_quote_hdr.quote_number||' will expire on '|| to_char(rec_quote_hdr.valid_until,'DD-MON-YYYY') ;  
	   		   l_debug_stmt := 'Set email body header';
               l_header_email_body   := substr('<p style="font-size:14.5px">'||
	                      'Customer Name : '||'<B>'||l_customer_name||'</B>'||'<BR>'||
   	                      'Customer Job Site : '||'<B>'||l_Customer_Job_Site||'</B>'||'<BR>'||
   	                      'Customer Number : '||'<B>'||l_customer_number||'</B>'||'<BR>'||
   	                      'Quote Number : '||'<B>'||rec_quote_hdr.quote_number||'</B>'||'<BR>'||
   	                      'Submitted By : '||'<B>'||l_submited_by||'</B>'||'<BR>'||
   	                      'Sales Rep Name : '||'<B>'||NVL(l_am_name,l_resource_name)||'</B>'||'<BR>'||
	   				   'Branch Name : '||'<B>'||l_branch_name||'</B>'||'<BR>'||'</p>',1,4000); --ver 3.0 added substr
	           l_debug_stmt := 'get email body line';				   
               get_quote_lines_email_body(p_quote_number           => rec_quote_hdr.quote_number,
                                          p_organization_id        => rec_quote_hdr.organization_id,
	   	    						    p_quote_line_msg_body    => l_line_email_body,
	   	    							p_retmsg                 => l_retmsg);
	   	    							
               IF l_retmsg IS NOT NULL THEN
	   	          fnd_file.put_line(fnd_file.log,'Error for quote# '||rec_quote_hdr.quote_number||'=>'||l_debug_stmt||'=>'||l_retmsg);
				  l_retmsg := NULL;
	   	       END IF;
	   	 
	   	       IF l_line_email_body IS NOT NULL THEN
	   	   	      l_debug_stmt := 'set l_email_body';
	   	          l_email_body := l_header_email_body||
	   	   	                '<p style="font-size:14.5px">'||
	   	   	                'Line Details: '||
	   	   					'</p>'||
	   	   	                l_line_email_body;
	   	       ELSE
	   	         l_email_body := l_header_email_body;
	   	      END IF;
	   	      l_debug_stmt := 'call send_html_email';--Ver 5.9
              IF l_email_body IS NOT NULL THEN --Ver 5.9
                  send_html_email ( p_to             => l_email_to,
	                                p_cc              => NULL,
                                    p_bcc             => NULL,
                                    p_from            => l_email_from,
                                    p_subject         => l_email_subject,
                                    p_text            => 'test',
                                    p_html            => l_email_body,
                                    p_smtp_hostname   => l_host,
                                    p_smtp_portnum    => l_hostport,
	   	       				        p_retmsg          => l_retmsg 
	   	       				      );
                  IF l_retmsg IS NOT NULL THEN
	   	  	         fnd_file.put_line(fnd_file.log,'Error for quote# '||rec_quote_hdr.quote_number||'=>'||l_debug_stmt||'=>'||l_retmsg);
					 l_retmsg := NULL;
                     RAISE e_prc_exception;
                  ELSE --ver 3.0 added else
				     fnd_file.put_line(fnd_file.log,'Email sent for quote# '||rec_quote_hdr.quote_number);
	   	          END IF;
				  IF NVL(rec_quote_hdr.expire_alert_sent,'N') = 'N' THEN
				     UPDATE xxwc.XXWC_OM_QUOTE_HEADERS xoqh2
				     SET    xoqh2.EXPIRE_ALERT_SENT = 'Y' --alert sent first time
				     WHERE xoqh2.quote_number = rec_quote_hdr.quote_number;
                  ELSIF NVL(rec_quote_hdr.expire_alert_sent,'N') = 'Y' THEN
				     UPDATE xxwc.XXWC_OM_QUOTE_HEADERS xoqh2
				     SET    xoqh2.EXPIRE_ALERT_SENT = 'R' --Reminder alert sent
				     WHERE xoqh2.quote_number = rec_quote_hdr.quote_number;
				  END IF;
				  COMMIT;
               END IF;
            END IF;
         EXCEPTION
		    WHEN e_prc_exception THEN
			   ROLLBACK;
		 END;
	  END LOOP;
	  fnd_file.put_line(fnd_file.log,'End Program '||to_char(sysdate,'DD-MON-YYYY HH24:MI:SS'));
	  errbuf  := l_errbuf;
	  retcode := l_retcode;
   EXCEPTION
       WHEN OTHERS THEN
	      l_retcode := 2;
		  l_retmsg := substr(l_debug_stmt||'=>'||SQLERRM,1,2000);
	      l_errbuf := l_retmsg;
		  errbuf  := l_errbuf;
	      retcode := l_retcode;
		  ROLLBACK;
		  xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_QUOTE_PKG.ALERT_QUOTE_EXPIRATION',
            p_calling             => l_debug_stmt,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_retmsg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'OM');
		  
   END alert_quote_expiration;
 
END xxwc_om_quote_pkg;
/
