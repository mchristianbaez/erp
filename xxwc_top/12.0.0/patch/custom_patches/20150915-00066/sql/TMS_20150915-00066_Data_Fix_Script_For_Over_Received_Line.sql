declare

  l_line_id       number  ;
  l_ordered_qty   number;
  l_shipped_quantity oe_order_lines_all.shipped_quantity%type;
  l_result        VARCHAR2(50);
  l_file_val 	    VARCHAR2(500);


  cursor lines
  is
 select ooh.order_number,
          ool.line_number||'.'|| ool.shipment_number ||'.'||
ool.option_number ||'.'|| ool.component_number ||'.'||ool.service_number
line_num,
   ool.header_id
         ,ool.line_id
        ,ool.ordered_quantity
        ,ool.fulfilled_quantity
        ,ool.shipped_quantity
  from  oe_order_lines_all ool ,
        oe_order_headers_all ooh
  where ooh.header_id = ool.header_id
  AND ool.flow_status_code         = 'AWAITING_RETURN'
  AND ool.shipped_quantity IS NULL
  AND ool.fulfilled_quantity IS NOT NULL
  AND ool.line_id =50108408
  and   exists    (
          SELECT '1'
          FROM    wf_item_activity_statuses_v wf
          WHERE   item_type = 'OEOL'
          AND     item_key = To_Char(ool.line_id )
          AND     activity_name = 'RMA_WAIT_FOR_RECEIVING'
          AND     activity_status_code IN('ERROR', 'NOTIFIED')
    ) ;

  l_user_id       number;
  l_resp_id       number;
  l_appl_id       number;
  x_return_status varchar2(10);
  x_msg_count     number;
  x_msg_data      varchar2(2000);

  Begin

  oe_debug_pub.debug_on;
  oe_debug_pub.initialize;
  l_file_val    := OE_DEBUG_PUB.Set_Debug_Mode('FILE');
  oe_Debug_pub.setdebuglevel(5);
  dbms_output.put_line(' Log File : '||l_file_val);
  dbms_output.put_line(' Log File name '||OE_DEBUG_PUB.G_DIR||'/'||OE_DEBUG_PUB.G_FILE);

  for i in lines loop

    	UPDATE  oe_order_lines_all SET
                shipped_quantity     = ORDERED_QUANTITY,
                shipping_quantity_uom= ORDER_QUANTITY_UOM,
		  fulfilled_quantity     = ORDERED_QUANTITY,
                actual_shipment_date = SYSDATE ,
                flow_status_code = 'RETURNED',
                last_updated_by      =  -1,
                last_update_date     =  SYSDATE
	WHERE line_id = i.line_id;

 oe_debug_pub.add ('Setting context ');

  OE_Standard_WF.OEOL_SELECTOR
  	(p_itemtype => 'OEOL'
	,p_itemkey => TO_CHAR(i.line_id)
  	,p_actid => 12345
	,p_funcmode => 'SET_CTX'
	,p_result => l_result
	);

    Oe_debug_pub.ADD('Result: '||l_result);

     wf_engine.handleError('OEOL', to_char(i.line_id),'RMA_WAIT_FOR_RECEIVING', 'SKIP', 'COMPLETE' );
    Oe_debug_pub.ADD('Skip 2: ');

     wf_engine.handleError('OEOL', to_char(i.line_id),'RMA_WAIT_FOR_INSPECTION', 'SKIP', 'COMPLETE' );

    commit;

  end loop;

  dbms_output.put_line(' Script execution completed ');
  oe_debug_pub.add('### Script execution completed ');
Exception
  when others then
    dbms_output.put_line(' Error in the base script : '||sqlerrm);
    oe_debug_pub.add('### Error in the base script : '||sqlerrm);
End;
/