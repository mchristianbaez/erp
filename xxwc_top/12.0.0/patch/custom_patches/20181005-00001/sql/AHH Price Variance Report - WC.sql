--Report Name            : AHH Price Variance Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_OM_AHH_PRICE_VAR_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_AHH_PRICE_VAR_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_AHH_PRICE_VAR_V',660,'','','','','SA059956','XXEIS','Eis Xxwc Om Ahh Price Var V','EXOAPVV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_OM_AHH_PRICE_VAR_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_AHH_PRICE_VAR_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_AHH_PRICE_VAR_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','UNIT_SELLING_PRICE',660,'Unit Selling Price','UNIT_SELLING_PRICE','','','','SA059956','NUMBER','','','Unit Selling Price','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','DISTRICT',660,'District','DISTRICT','','','','SA059956','VARCHAR2','','','District','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','BRANCH_NAME',660,'Branch Name','BRANCH_NAME','','','','SA059956','VARCHAR2','','','Branch Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','PRICE_SOURCE_TYPE',660,'Price Source Type','PRICE_SOURCE_TYPE','','','','SA059956','VARCHAR2','','','Price Source Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','PRICE_TYPE',660,'Price Type','PRICE_TYPE','','','','SA059956','VARCHAR2','','','Price Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','SYSTEM_PRICE',660,'System Price','SYSTEM_PRICE','','','','SA059956','NUMBER','','','System Price','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','SXE_LAST_PRICE_PAID',660,'Sxe Last Price Paid','SXE_LAST_PRICE_PAID','','','','SA059956','NUMBER','','','Sxe Last Price Paid','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','SA059956','NUMBER','','','Order Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','CREATED_BY',660,'Created By','CREATED_BY','','','','SA059956','VARCHAR2','','','Created By','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Customer Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','AVERAGE_COST',660,'Average Cost','AVERAGE_COST','','','','SA059956','NUMBER','','','Average Cost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','SALESREP_NAME',660,'Salesrep Name','SALESREP_NAME','','','','SA059956','VARCHAR2','','','Salesrep Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','SALES',660,'Sales','SALES','','','','SA059956','NUMBER','','','Sales','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','ITEM',660,'Item','ITEM','','','','SA059956','VARCHAR2','','','Item','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','ITEM_DESC',660,'Item Desc','ITEM_DESC','','','','SA059956','VARCHAR2','','','Item Desc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','QTY',660,'Qty','QTY','','','','SA059956','NUMBER','','','Qty','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','LINE_TYPE',660,'Line Type','LINE_TYPE','','','','SA059956','VARCHAR2','','','Line Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','SALE_COST',660,'Sale Cost','SALE_COST','','','','SA059956','NUMBER','','','Sale Cost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','LINE_FLOW_STATUS_CODE',660,'Line Flow Status Code','LINE_FLOW_STATUS_CODE','','','','SA059956','VARCHAR2','','','Line Flow Status Code','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','SA059956','DATE','','','Ordered Date','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','SA059956','VARCHAR2','','','Order Type','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','WAREHOUSE',660,'Warehouse','WAREHOUSE','','','','SA059956','VARCHAR2','','','Warehouse','','','','','');
--Inserting Object Components for EIS_XXWC_OM_AHH_PRICE_VAR_V
--Inserting Object Component Joins for EIS_XXWC_OM_AHH_PRICE_VAR_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for AHH Price Variance Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - AHH Price Variance Report - WC
xxeis.eis_rsc_ins.lov( 660,'select  RS.Name,SALESREP_ID  from  RA_SALESREPS RS
WHERE  RS.NAME is not null','','OM SALES REP','This gives the sales representative name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT DISTINCT attribute7 created_by
FROM apps.oe_order_headers_all','','Order Created By Lov','','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for AHH Price Variance Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - AHH Price Variance Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'AHH Price Variance Report - WC',660 );
--Inserting Report - AHH Price Variance Report - WC
xxeis.eis_rsc_ins.r( 660,'AHH Price Variance Report - WC','','AHH Price Variance Report - WC','','','','SA059956','EIS_XXWC_OM_AHH_PRICE_VAR_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - AHH Price Variance Report - WC
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'AVERAGE_COST','Product Unit Cost','Average Cost','','$~T~D~2','default','','20','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'BRANCH_NAME','Branch Name','Branch Name','','','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'CREATED_BY','Created By','Created By','','','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'DISTRICT','District','District','','','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'ITEM','SKU Number','Item','','','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'ITEM_DESC','SKU Description','Item Desc','','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'PRICE_SOURCE_TYPE','Original Price Source','Price Source Type','','','default','','10','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'PRICE_TYPE','Final Price Type','Price Type','','','default','','11','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'QTY','Order Qty','Qty','','~~~','default','','13','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'SALES','Sales$','Sales','','$~T~D~2','default','','12','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'SALESREP_NAME','Salesrep Name','Salesrep Name','','','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'SXE_LAST_PRICE_PAID','SXE Last Price Paid','Sxe Last Price Paid','','$~T~D~2','default','','16','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'SYSTEM_PRICE','Original Selling Price','System Price','','$~T~D~2','default','','14','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'UNIT_SELLING_PRICE','Final Selling price','Unit Selling Price','','$~T~D~2','default','','15','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'Final_GM%','Final GM%','GM%','NUMBER','~T~D~1','default','','21','Y','Y','','','','','','(case when (NVL(EXOAPVV.sales,0) !=0  AND NVL(EXOAPVV.sale_cost,0) !=0) then  decode(EXOAPVV.line_type,''CREDIT ONLY'',(-1*(((EXOAPVV.sales-EXOAPVV.sale_cost)/(EXOAPVV.sales))*100)),(((EXOAPVV.sales-EXOAPVV.sale_cost)/(EXOAPVV.sales))*100)) WHEN NVL(EXOAPVV.sale_cost,0) =0  THEN decode(EXOAPVV.line_type,''CREDIT ONLY'',(-1*100),100)  WHEN nvl(EXOAPVV.sales,0) != 0  THEN decode(EXOAPVV.line_type,''CREDIT ONLY'',(-1*(((EXOAPVV.sales-EXOAPVV.sale_cost)/(EXOAPVV.sales))*100)),(((EXOAPVV.sales-EXOAPVV.sale_cost)/(EXOAPVV.sales))*100))  else 0 end)','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'variance_delivered_price','Variance % (SXE LPP & Delivered Price)','variance_delivered_price','NUMBER','~T~D~1','default','','17','Y','Y','','','','','','(DECODE(NVL(EXOAPVV.system_price,0),0,0,((NVL(EXOAPVV.system_price,0)      - NVL(EXOAPVV.sxe_last_price_paid,0))/NVL(EXOAPVV.system_price,0))*100))','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'variance_final_sell_price','Variance % (SXE LPP & Final Price)','variance_final_sell_price','NUMBER','~T~D~1','default','','18','Y','Y','','','','','','(DECODE(NVL(EXOAPVV.unit_selling_price,0),0,0,((NVL(EXOAPVV.unit_selling_price,0)-NVL(EXOAPVV.sxe_last_price_paid,0))/NVL(EXOAPVV.unit_selling_price,0))*100))','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'Sales_Impact_Final_Price','Sales $ Impact (SXE LPP & Final Price)','sales_impact_delivered_price','NUMBER','$~T~D~2','default','','19','Y','Y','','','','','','((NVL(EXOAPVV.qty,0) * NVL(EXOAPVV.unit_selling_price,0))-(NVL(EXOAPVV.qty,0)*NVL(EXOAPVV.sxe_last_price_paid,0)))','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
--Inserting Report Parameters - AHH Price Variance Report - WC
xxeis.eis_rsc_ins.rp( 'AHH Price Variance Report - WC',660,'Created By','Created By','CREATED_BY','IN','Order Created By Lov','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','US','');
xxeis.eis_rsc_ins.rp( 'AHH Price Variance Report - WC',660,'Salesrep Name','Salesrep Name','SALESREP_NAME','IN','OM SALES REP','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','US','');
xxeis.eis_rsc_ins.rp( 'AHH Price Variance Report - WC',660,'As Of Date','As Of Date','','','','','DATE','Y','Y','2','Y','N','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','US','');
xxeis.eis_rsc_ins.rp( 'AHH Price Variance Report - WC',660,'Warehouse','Warehouse','','','OM Warehouse All','''All''','VARCHAR2','Y','Y','1','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','US','');
--Inserting Dependent Parameters - AHH Price Variance Report - WC
--Inserting Report Conditions - AHH Price Variance Report - WC
xxeis.eis_rsc_ins.rcnh( 'AHH Price Variance Report - WC',660,'FreeText','FREE_TEXT','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','Y','','','','AND ( ''All'' IN (:Warehouse) OR (WAREHOUSE IN (:Warehouse)))
AND (EXOAPVV.ORDER_TYPE != ''STANDARD ORDER'' OR  (EXOAPVV.ORDER_TYPE = ''STANDARD ORDER''
AND EXOAPVV.ORDERED_DATE < =:As Of Date)
 OR (EXOAPVV.ORDER_TYPE = ''STANDARD ORDER''
 AND EXOAPVV.LINE_FLOW_STATUS_CODE IN (''CLOSED'',''INVOICE_DELIVERY'',''INVOICE_HOLD'',''INVOICE_INCOMPLETE'',''INVOICE_NOT_APPLICABLE'',''INVOICE_RFR'',''INVOICE_UNEXPECTED_ERROR'',''PARTIAL_INVOICE_RFR'')))
and NVL(EXOAPVV.QTY,0)>0
and  NVL(EXOAPVV.SALES,0)<>0','1',660,'AHH Price Variance Report - WC','FreeText');
xxeis.eis_rsc_ins.rcnh( 'AHH Price Variance Report - WC',660,'EXOAPVV.CREATED_BY IN Created By','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','CREATED_BY','','Created By','','','','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','','','IN','Y','Y','','','','','1',660,'AHH Price Variance Report - WC','EXOAPVV.CREATED_BY IN Created By');
xxeis.eis_rsc_ins.rcnh( 'AHH Price Variance Report - WC',660,'EXOAPVV.SALESREP_NAME IN Salesrep Name','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','SALESREP_NAME','','Salesrep Name','','','','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','','','IN','Y','Y','','','','','1',660,'AHH Price Variance Report - WC','EXOAPVV.SALESREP_NAME IN Salesrep Name');
--Inserting Report Sorts - AHH Price Variance Report - WC
xxeis.eis_rsc_ins.rs( 'AHH Price Variance Report - WC',660,'CREATED_BY','ASC','SA059956','1','');
xxeis.eis_rsc_ins.rs( 'AHH Price Variance Report - WC',660,'ORDER_NUMBER','ASC','SA059956','2','');
--Inserting Report Triggers - AHH Price Variance Report - WC
xxeis.eis_rsc_ins.rt( 'AHH Price Variance Report - WC',660,'begin
XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.set_date_from(:As Of Date);
end;','B','Y','SA059956','BQ');
--inserting report templates - AHH Price Variance Report - WC
--Inserting Report Portals - AHH Price Variance Report - WC
--inserting report dashboards - AHH Price Variance Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'AHH Price Variance Report - WC','660','EIS_XXWC_OM_AHH_PRICE_VAR_V','EIS_XXWC_OM_AHH_PRICE_VAR_V','N','');
--inserting report security - AHH Price Variance Report - WC
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','','SN059503','',660,'SA059956','','N','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_AO_OEENTRY_REC',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_AO_OEENTRY_PO_RPT',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_AO_OEENTRY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','ORDER_MGMT_SUPER_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','20005','','XXWC_SALES_SUPPORT_ADMIN',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'SA059956','','','');
--Inserting Report Pivots - AHH Price Variance Report - WC
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- AHH Price Variance Report - WC
xxeis.eis_rsc_ins.rv( 'AHH Price Variance Report - WC','','AHH Price Variance Report - WC','SA059956','08-OCT-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
