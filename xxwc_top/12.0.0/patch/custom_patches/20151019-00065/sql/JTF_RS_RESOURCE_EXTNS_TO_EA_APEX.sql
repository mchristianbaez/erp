/********************************************************************************
   $Header JTF_RS_RESOURCE_EXTNS_TO_EA_APEX.sql $
   Module Name: JTF_RS_RESOURCE_EXTNS

   PURPOSE:  Grant to Apex on table - JTF_RS_RESOURCE_EXTNS.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        10/20/2015  Gopi Damuluri           TMS# 20151019-00065
                                                  Initial Version
********************************************************************************/

GRANT SELECT ON APPS.JTF_RS_RESOURCE_EXTNS TO EA_APEX WITH GRANT OPTION;