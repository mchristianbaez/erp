/********************************************************************************
   $Header JTF_RS_SALESREPS_TO_EA_APEX.sql $
   Module Name: JTF_RS_SALESREPS

   PURPOSE:  Grant to Apex on table - JTF_RS_SALESREPS.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        10/20/2015  Gopi Damuluri           TMS# 20151019-00065
                                                  Initial Version
********************************************************************************/

GRANT SELECT ON APPS.JTF_RS_SALESREPS TO EA_APEX WITH GRANT OPTION;