/*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header xxwc_bill_trust_inv_lines_vw  $
     Module Name: xxwc_bill_trust_inv_lines_vw 

     PURPOSE:   This view is used by the BT Interfaces

     REVISIONS:
     Ver        Date       Author                 Description
     ---------  ---------- ---------------        -------------------------
     1.0                                          Initial Version
     2.0       05/29/2015  Maharajan Shunmugam    TMS#20150527-00031 OM Rental Receipt Bug Fix
     3.0       07/23/2018  Pattabhi Avula         TMS#20180723-00037 - Changes to BillTrust 
	                                              Invoice Outbound Interface for AHH Transactions
   **************************************************************************/
CREATE OR REPLACE FORCE EDITIONABLE VIEW APPS.XXWC_BILL_TRUST_INV_LINES_VW (
LINE_RECORD_TYPE, 
INVOICE_ID, 
INVOICE_LINE_ID, 
LINE_NUMBER, 
PART_NUMBER, 
PART_DESCRIPTION, 
BRANCH, 
UOM_CODE, 
QTY_ORD, 
QTY_SHIPPED, 
QTY_BACKORDERED, 
PRICE, 
EXTENDED_PRICE, 
DISCOUNT_AMOUNT, 
DISCOUNT_DATE, 
TAX_RATE, 
TAX_AMOUNT, 
TAXABLE, 
NOTE_RECORD_TYPE, 
SHIPPING_INSTRUCTIONS, 
PACKING_INSTRUCTIONS, 
SERVICE_TXN_COMMENTS, 
REVREC_COMMENTS, 
BRANCH_CODE, 
BRANCH_NAME, 
BRANCH_ADDRESS1, 
BRANCH_ADDRESS2, 
BRANCH_CITY, 
BRANCH_STATE, 
BRANCH_ZIP_CODE, 
BRANCH_REGION_1, 
BRANCH_COUNTRY, 
BRANCH_PHONE_NUMBER, 
INVOICE_BATCH_SOURCE, 
RENTAL_SHIP_DATE, 
RENTAL_RETURN_DATE, 
PRISM_LINE_NUMBER, 
OE_ORDER_LINE_ID, 
OE_ORDER_LINE_NUM, 
NEW_LINE_NUMBER)
AS
  SELECT 'D' line_record_type ,
    rcta.customer_trx_id invoice_id ,
    rctla.customer_trx_line_id invoice_line_id, -- 06/09/2012 CG Changed to be able to resort
    -- rctla.line_number,
    (
    CASE
     -- WHEN rbsa.name LIKE 'PRISM%'  -- Ver#3.0
	  WHEN rbsa.name LIKE '%HARRIS%'  -- Ver#3.0
      THEN NVL (TO_NUMBER (rctla.interface_line_attribute13) , rctla.line_number)
      ELSE rctla.line_number
    END) line_number , -- 06/18/2012 CG: altered to pull the rental charge item associated
    -- true item number and description
    (
    CASE
      -- WHEN rbsa.name LIKE 'PRISM%'  -- Ver#3.0
	  WHEN rbsa.name LIKE '%HARRIS%'   -- Ver#3.0
      THEN rctla.interface_line_attribute4
     -- WHEN rbsa.name NOT LIKE 'PRISM%'  -- Ver#3.0
	  WHEN rbsa.name NOT LIKE '%HARRIS%'  -- Ver#3.0
      AND ol.line_id           IS NOT NULL
      AND UPPER (msib.segment1) = UPPER ('Rental Charge')
      THEN NVL ( xxwc_mv_routines_pkg.get_om_rental_item (ol.line_id , 'NUMBER') , msib.segment1 )
      ELSE msib.segment1
    END) part_number
    -- 02/18/2013: Added a wrapping to remove line fine characters
    ,
    REPLACE( (
    CASE
     -- WHEN rbsa.name NOT LIKE 'PRISM%'  -- Ver#3.0
	  WHEN rbsa.name NOT LIKE '%HARRIS%'  -- Ver#3.0
      AND ol.line_id           IS NOT NULL
      AND UPPER (msib.segment1) = UPPER ('Rental Charge')
      THEN NVL ( xxwc_mv_routines_pkg.get_om_rental_item (ol.line_id , 'DESC') , NVL (msib.description, rctla.description) )
      ELSE NVL (msib.description, rctla.description)
    END) , chr(10), ' ') part_description , -- 06/18/2012 CG
    -- 05/07/2012 CG: Had to change to accomodate noninventory locations from HARRIS
    --mp.organization_code branch,
    SUBSTR (hla.location_code, 1, INSTR (hla.location_code , '-' , 1 , 1) - 2) branch , -- 06/06/2012 CG: Changed to pull the prism line UOM code from the staging table
    --        also added a catch all
    NVL ( (
    CASE
     -- WHEN rbsa.name LIKE 'PRISM%'  -- Ver#3.0
	  WHEN rbsa.name LIKE '%HARRIS%'  -- Ver#3.0
      THEN SUBSTR ( xxwc_mv_routines_pkg.get_prism_inv_line_uom ( rcta.trx_number , rctla.interface_line_attribute13 ) , 1 , 3 )
      ELSE rctla.uom_code
    END) , 'EA' ) uom_code ,
    (
    CASE
      -- WHEN rbsa.name LIKE 'PRISM%'  -- Ver#3.0
	  WHEN rbsa.name LIKE '%HARRIS%'   -- Ver#3.0
      -- THEN DECODE(rcta.cust_trx_type_id,2,       -1,1)*TO_NUMBER (rctla.interface_line_attribute2)       -- Ver#3.0
      THEN DECODE(rcta.cust_trx_type_id,2,       -1,1)*rctla.quantity_ordered       -- Ver#3.0
      ELSE DECODE(ol.line_category_code,'RETURN',-1,1)*NVL (ol.ordered_quantity, rctla.quantity_ordered) -- TMS# 20130823-00477
    END) qty_ord ,
    DECODE(rcta.cust_trx_type_id,2,-1,1)*rctla.quantity_invoiced qty_shipped -- TMS# 20130823-00477
    ,
    (
    CASE
     -- WHEN rbsa.name LIKE 'PRISM%'  -- Ver#3.0
	  WHEN rbsa.name LIKE '%HARRIS%'  -- Ver#3.0
      -- THEN DECODE(rcta.cust_trx_type_id,2,       -1,1)*TO_NUMBER (rctla.interface_line_attribute3)                                   -- Ver#3.0
      THEN DECODE(rcta.cust_trx_type_id,2,       -1,1)*rctla.quantity_ordered                                   -- TMS# Ver#3.0
      ELSE DECODE(ol.line_category_code,'RETURN',-1,1)*(NVL (ol.ordered_quantity, rctla.quantity_ordered) - rctla.quantity_invoiced) -- TMS# 20130823-00477
    END) qty_backordered ,                                                                                                           -- 08/08/2012 CG: Changed to pull from Prism staging table and order line
    -- rctla.unit_selling_price price,
    (
    CASE
      -- WHEN rbsa.name LIKE 'PRISM%'  -- Ver#3.0
	  WHEN rbsa.name LIKE '%HARRIS%'  -- Ver#3.0	
      THEN NVL ( apps.xxwc_mv_routines_pkg.get_prism_line_unit_price ( rcta.trx_number , rctla.interface_line_attribute13 ) , rctla.unit_selling_price )
      ELSE NVL (ol.unit_selling_price, rctla.unit_selling_price)
    END) price ,
    rctla.extended_amount extended_price -- 01/23/2012 CGonzalez added
    ,
    0 discount_amount ,
    TO_DATE ('01/01/1952', 'MM/DD/YYYY') discount_date ,
    xxwc_mv_routines_pkg.get_tax_rate (rcta.customer_trx_id , rctla.customer_trx_line_id) tax_rate -- 02/23/2012 CG
    ,
    xxwc_mv_routines_pkg.get_tax_amount (rcta.customer_trx_id , rctla.customer_trx_line_id) tax_amount -- 02/20/2012 CG
    ,
    (
    CASE
      WHEN NVL ( xxwc_mv_routines_pkg.get_tax_rate ( rcta.customer_trx_id , rctla.customer_trx_line_id ) , 0 ) > 0
      THEN 'T'
      ELSE 'N'
    END) taxable -- 01/23/2012 CGonzalez
    ,
    'N' note_record_type ,
    ol.shipping_instructions ,
    ol.packing_instructions ,
    ol.service_txn_comments ,
    ol.revrec_comments
    -- 02/23/2012 CG
    -- 05/07/2012 CG: Had to change to accomodate noninventory locations from PRISM
    --, mp.organization_code branch_code
    ,
    SUBSTR (hla.location_code, 1, INSTR (hla.location_code , '-' , 1 , 1) - 2) branch_code ,
    hla.location_code branch_name -- 01/17/12 CGonzalez
    ,
    hla.address_line_1 branch_address1 ,
    ( hla.address_line_2
    || DECODE (hla.address_line_3, NULL, '', '|')
    || hla.address_line_3) branch_address2 ,
    hla.town_or_city branch_city ,
    hla.region_2 branch_state ,
    hla.postal_code branch_zip_code ,
    hla.region_1 branch_region_1 ,
    hla.country branch_country ,
    hla.telephone_number_1 branch_phone_number ,
    rbsa.name invoice_batch_source , -- 04/12/2012 CGonzalez added to pull rental line dates
    (
    CASE
      WHEN rbsa.name IN ('ORDER MANAGEMENT', 'STANDARD OM SOURCE')
      THEN
        --xxwc_mv_routines_pkg.get_rental_ship_date (ol.line_id)                      --commented and added below by Maha for ver 2.0
        xxwc_mv_routines_pkg.get_rental_ship_date (ol.line_id,ol.link_to_line_id)
      -- WHEN rbsa.name LIKE 'PRISM%'  -- Ver#3.0
	  WHEN rbsa.name LIKE '%HARRIS%'   -- Ver#3.0
      THEN xxwc_mv_routines_pkg.get_prism_rental_date ( 'FROM' , rcta.trx_number )
      ELSE NULL
    END) rental_ship_date ,
    (
    CASE
      WHEN rbsa.name IN ('ORDER MANAGEMENT', 'STANDARD OM SOURCE')
      THEN xxwc_mv_routines_pkg.get_rental_return_date (ol.line_id)
      -- WHEN rbsa.name LIKE 'PRISM%'  -- Ver#3.0
	  WHEN rbsa.name LIKE '%HARRIS%'  -- Ver#3.0
      THEN xxwc_mv_routines_pkg.get_prism_rental_date ( 'TO' , rcta.trx_number )
      ELSE NULL
    END) rental_return_date ,
    rctla.interface_line_attribute13 prism_line_number
    -- 06/18/2012 CG Added to be able to pull instructions from main table instead of view
    ,
    ol.line_id oe_order_line_id
    -- 02/12/2013 CG: Added to use the order line numbers to sort and display
    ,
    ol.line_number oe_order_line_num ,
    (
    CASE
      WHEN rbsa.name IN ('ORDER MANAGEMENT', 'STANDARD OM SOURCE')
      THEN NVL (ol.line_number, rctla.line_number)
     -- WHEN rbsa.name LIKE 'PRISM%'  -- Ver#3.0
	  WHEN rbsa.name LIKE '%HARRIS%'  -- Ver#3.0
      THEN NVL (TO_NUMBER (rctla.interface_line_attribute13) , rctla.line_number)
      ELSE rctla.line_number
    END) new_line_number
  FROM ar.ra_customer_trx_all rcta ,
    ar.ra_customer_trx_lines_all rctla ,
    inv.mtl_system_items_b msib , -- 05/07/2012 CG: Had to change to accomodate noninventory locations from PRISM
    --inv.mtl_parameters mp,
    ont.oe_order_lines_all ol ,      -- 01/17/12 CGonzalez
    hr.hr_locations_all hla ,        -- Issue 759: 08/01/2012 CG: Changed to correct issue of branch mult locs
   -- hr_all_organization_units haou , -- Issue 759  -- Ver#3.0
    ar.ra_batch_sources_all rbsa
  WHERE rcta.status_trx               = 'OP'
  AND rcta.complete_flag              = 'Y'
  AND rcta.customer_trx_id            = rctla.customer_trx_id
  AND rctla.line_type                 = 'LINE'
  AND rctla.inventory_item_id         = msib.inventory_item_id(+)
  AND NVL (rctla.warehouse_id, 222)   = msib.organization_id(+)
  AND rctla.interface_line_attribute6 = TO_CHAR (ol.line_id(+))
  AND rcta.batch_source_id            = rbsa.batch_source_id
  AND rcta.org_id                     = rbsa.org_id
    -- 01/17/12 CGonzalez
    -- 03/08/2012 CGonzalez adjusted to accomodate data from PRISM
    --AND NVL (rctla.warehouse_id, 181) = mp.organization_id(+)
    -- 03/08/2012 CGonzalez adjusted to accomodate data from PRISM
  -- AND ( (rbsa.name NOT LIKE 'PRISM%' -- Ver#3.0
  AND ( (rbsa.name NOT LIKE '%HARRIS%'  -- Ver#3.0
  AND hla.BILL_TO_SITE_FLAG = 'Y'       -- Ver#3.0
  -- AND NVL (rctla.warehouse_id ,      -- Ver#3.0
  AND NVL (rcta.interface_header_attribute10,  -- Ver#3.0
    --(SELECT organization_id FROM mtl_parameters WHERE organization_code = 'WCC' -- Ver#3.0
	(SELECT TO_CHAR (organization_id)
    FROM mtl_parameters
    WHERE organization_code = 'WCC'  -- Ver#3.0
    ) -- Issue 759: 08/01/2012 CG: Changed to correct issue of branch mult locs
    --) = TO_CHAR (hla.inventory_organization_id)
   /* --<START> -- Ver#3.0 )                  = haou.organization_id
  AND haou.location_id = hla.location_id -- Issue 759
    -- 05/07/2012 CG: Had to change to accomodate noninventory locations from PRISM
    --) = mp.organization_id
    )
  OR ( rbsa.name LIKE 'PRISM%'
    -- 06/05/2012 CG NVL for Trx without Branch Code
    -- AND NVL (rcta.interface_header_attribute7, 'WCC') =
  AND NVL (LPAD (rcta.interface_header_attribute7, 3, '0') , 'WCC') = -- Issue 759: 08/01/2012 CG: Changed to correct issue of branch mult locs
    --SUBSTR (hla.location_code,
    -- 1,
    -- INSTR (hla.location_code, '-', 1, 1) - 2
    -- )
    SUBSTR (haou.name, 1, INSTR (haou.name , '-' , 1 , 1) - 2)
  AND haou.location_id = hla.location_id -- Issue 759
    -- 05/07/2012 CG: Had to change to accomodate noninventory locations from PRISM
    --AND rcta.interface_header_attribute7 = mp.organization_code
    ) --OR mp.organization_code = 'MST'
    )
    -- 05/07/2012 CG: Had to change to accomodate noninventory locations from PRISM
    -- AND mp.organization_id = hla.inventory_organization_id(+)
  ORDER BY rcta.trx_number */
  ) =
    (SELECT TO_CHAR (haou.organization_id)
    FROM hr_all_organization_units haou
    WHERE haou.location_id = hla.location_id
    AND ROWNUM             = 1
    ) )
  OR (rbsa.name LIKE '%HARRIS%'
  AND hla.BILL_TO_SITE_FLAG                    = 'Y'
  AND UPPER(rcta.interface_header_attribute7) IN
    (SELECT UPPER(stg.ahh_warehouse_number)
    FROM xxwc.xxwc_ap_branch_list_stg_tbl stg
    WHERE SUBSTR (hla.location_code, 1, 3) = stg.oracle_branch_number
    ) ))
  ORDER BY rcta.trx_number -- <END>  -- Ver#3.0 
  /