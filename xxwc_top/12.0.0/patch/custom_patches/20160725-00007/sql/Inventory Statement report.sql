--Report Name            : WC - Inventory Statement report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for WC - Inventory Statement report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_LISTING_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_LISTING_V',401,'','','','','SA059956','XXEIS','Eis Rs Xxwc Inv Listing V','EXILV','','');
--Delete View Columns for EIS_XXWC_INV_LISTING_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_LISTING_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_LISTING_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','AVERAGECOST',401,'Averagecost','AVERAGECOST','','~T~D~2','','SA059956','NUMBER','','','Averagecost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','BRANCH_NUMBER',401,'Branch Number','BRANCH_NUMBER','','','','SA059956','VARCHAR2','','','Branch Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','CAT_CLASS',401,'Cat Class','CAT_CLASS','','','','SA059956','VARCHAR2','','','Cat Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','CAT_CLASS_DESCRIPTION',401,'Cat Class Description','CAT_CLASS_DESCRIPTION','','','','SA059956','VARCHAR2','','','Cat Class Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','CONSIGNED',401,'Consigned','CONSIGNED','','','','SA059956','VARCHAR2','','','Consigned','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','SA059956','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','LOCATION_STATE',401,'Location State','LOCATION_STATE','','','','SA059956','VARCHAR2','','','Location State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','MFG_NUMBER',401,'Mfg Number','MFG_NUMBER','','','','SA059956','NUMBER','','','Mfg Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','ONHAND',401,'Onhand','ONHAND','','','','SA059956','NUMBER','','','Onhand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','PART_NUMBER',401,'Part Number','PART_NUMBER','','','','SA059956','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','SALE_UNITS',401,'Sale Units','SALE_UNITS','','','','SA059956','NUMBER','','','Sale Units','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','SEGMENT_LOCATION',401,'Segment Location','SEGMENT_LOCATION','','','','SA059956','VARCHAR2','','','Segment Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','SELLING_PRICE',401,'Selling Price','SELLING_PRICE','','','','SA059956','NUMBER','','','Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','SHELF_LIFE',401,'Shelf Life','SHELF_LIFE','','','','SA059956','NUMBER','','','Shelf Life','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','SUBINVENTORY_CODE',401,'Subinventory Code','SUBINVENTORY_CODE','','','','SA059956','VARCHAR2','','','Subinventory Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','UOM',401,'Uom','UOM','','','','SA059956','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','SA059956','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','BIN1',401,'Bin1','BIN1','','','','SA059956','VARCHAR2','','','Bin1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','BIN2',401,'Bin2','BIN2','','','','SA059956','VARCHAR2','','','Bin2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','BIN3',401,'Bin3','BIN3','','','','SA059956','VARCHAR2','','','Bin3','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','ON_ORDER',401,'On Order','ON_ORDER','','','','SA059956','NUMBER','','','On Order','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','BIN0',401,'Bin0','BIN0','','','','SA059956','VARCHAR2','','','Bin0','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','OPEN_DEMAND',401,'Open Demand','OPEN_DEMAND','','','','SA059956','CHAR','','','Open Demand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','MAKE_BUY',401,'Make Buy','MAKE_BUY','','','','SA059956','VARCHAR2','','','Make Buy','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','FINAL_END_QTY',401,'Final End Qty','FINAL_END_QTY','','','','SA059956','NUMBER','','','Final End Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','CONTAINER_TYPE',401,'Container Type','CONTAINER_TYPE','','','','SA059956','VARCHAR2','','','Container Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','LOCATION',401,'Location','LOCATION','','','','SA059956','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','UN_NUMBER',401,'Un Number','UN_NUMBER','','','','SA059956','VARCHAR2','','','Un Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','WEIGHT',401,'Weight','WEIGHT','','','','SA059956','NUMBER','','','Weight','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','SA059956','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','ONHAND_TYPE',401,'Onhand Type','ONHAND_TYPE','','','','SA059956','VARCHAR2','','','Onhand Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','ORGANIZATION_ID',401,'Organization Id','ORGANIZATION_ID','','','','SA059956','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','TONHAND',401,'Tonhand','TONHAND','','','','SA059956','NUMBER','','','Tonhand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','PROCESS_ID',401,'Process Id','PROCESS_ID','','','','SA059956','NUMBER','','','Process Id','','','');
--Inserting View Components for EIS_XXWC_INV_LISTING_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_LISTING_V','GL_CODE_COMBINATIONS_KFV',401,'GL_CODE_COMBINATIONS','GCC','GCC','SA059956','SA059956','-1','Account Combinations','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_LISTING_V','GL_PERIOD_STATUSES',401,'GL_PERIOD_STATUSES','GPS','GPS','SA059956','SA059956','-1','Calendar Period Statuses','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_LISTING_V','MTL_CATEGORIES',401,'MTL_CATEGORIES_B','MCV','MCV','SA059956','SA059956','-1','Categories','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_LISTING_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSI','MSI','SA059956','SA059956','-1','Items','','','','');
--Inserting View Component Joins for EIS_XXWC_INV_LISTING_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','GL_CODE_COMBINATIONS_KFV','GCC',401,'EXILV.CODE_COMBINATION_ID','=','GCC.CODE_COMBINATION_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','GL_PERIOD_STATUSES','GPS',401,'EXILV.APPLICATION_ID','=','GPS.APPLICATION_ID(+)','','','','','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','GL_PERIOD_STATUSES','GPS',401,'EXILV.SET_OF_BOOKS_ID','=','GPS.SET_OF_BOOKS_ID(+)','','','','','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','GL_PERIOD_STATUSES','GPS',401,'EXILV.FISCAL_MONTH','=','GPS.PERIOD_NAME(+)','','','','','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','MTL_CATEGORIES','MCV',401,'EXILV.CATEGORY_ID','=','MCV.CATEGORY_ID(+)','','','','','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXILV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXILV.INV_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','SA059956','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for WC - Inventory Statement report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - WC - Inventory Statement report
xxeis.eis_rs_ins.lov( 401,'select SECONDARY_INVENTORY_NAME SUB_INVENTORY,HAOU.NAME ORGANIZATION_NAME
from MTL_SECONDARY_INVENTORIES MSI,
     HR_ALL_ORGANIZATION_UNITS HAOU
WHERE 1=1
AND MSI.ORGANIZATION_ID = HAOU.organization_id','','EIS_INV_SUBINVENTORY_LOV','List of All SubInventories.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT ''Negative Onhand Items'' onhand_type FROM dual
UNION
SELECT ''Positive Onhand Items'' onhand_type FROM dual
UNION
SELECT ''ALL Items'' onhand_type FROM dual','','INV ONHAND TYPE','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT ORGANIZATION_CODE WAREHOUSE,
  ORGANIZATION_NAME
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1)
AND EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
UNION
SELECT ''All'', ''All Organizations'' FROM Dual','','XXWC INV ORGANIZATIONS LOV1','List of All Inventory Orgs under a given operating unit.','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'','On Hand,No on Hand,ALL,Negative on hand','ON_HAND_TYPE_CSV','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for WC - Inventory Statement report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - WC - Inventory Statement report
xxeis.eis_rs_utility.delete_report_rows( 'WC - Inventory Statement report' );
--Inserting Report - WC - Inventory Statement report
xxeis.eis_rs_ins.r( 401,'WC - Inventory Statement report','','Provide a listing of inventory related information ( Negative On Hand , HAZMAT list, Cat/Class list, Extended Description, Inventory Explosion, etc.)
Selection - Location, Prod, Ven, Cat/Class
Columns - stock level, negative avail, on hand, on order, turns, cat/class, descrip, extended description (Negative On Hand, HAZMAT list, Cat/Class list, Inventory Explosion  etc)
','','','','SA059956','EIS_XXWC_INV_LISTING_V','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - WC - Inventory Statement report
xxeis.eis_rs_ins.rc( 'WC - Inventory Statement report',401,'CAT_CLASS','CatClass','Cat Class','','','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Statement report',401,'DESCRIPTION','Description','Description','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Statement report',401,'ONHAND','Qty on Hand','Onhand','','~~~','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Statement report',401,'PART_NUMBER','Part','Part Number','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Statement report',401,'SEGMENT_LOCATION','Segment Location','Segment Location','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Statement report',401,'UOM','UOM','Uom','','','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Statement report',401,'CAT_CLASS_DESCRIPTION','Cat Class Description','Cat Class Description','','','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Statement report',401,'CONSIGNED','Consigned','Consigned','','','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Statement report',401,'FINAL END VALUE','Final End Value','Consigned','NUMBER','~T~D~2','default','','12','Y','','','','','','','(EXILV.AVERAGECOST*EXILV.ONHAND)','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Statement report',401,'AVERAGECOST','Averagecost','Averagecost','','~T~D~2','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Statement report',401,'BRANCH_NUMBER','Location','Branch Number','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Statement report',401,'SUBINVENTORY_CODE','Subinventory Code','Subinventory Code','','','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Statement report',401,'BIN1','Bin1','Bin1','','','default','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Statement report',401,'BIN2','Bin2','Bin2','','','default','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Statement report',401,'BIN3','Bin3','Bin3','','','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','');
--Inserting Report Parameters - WC - Inventory Statement report
xxeis.eis_rs_ins.rp( 'WC - Inventory Statement report',401,'Organization','Organization','','IN','XXWC INV ORGANIZATIONS LOV1','','VARCHAR2','Y','Y','1','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'WC - Inventory Statement report',401,'Include All Items with a Positive On Hand','Include All Items with a Positive On Hand','','IN','INV ONHAND TYPE','''ALL Items''','VARCHAR2','N','N','4','','N','CONSTANT','SA059956','N','N','','','');
xxeis.eis_rs_ins.rp( 'WC - Inventory Statement report',401,'Subinventory','Subinventory','SUBINVENTORY_CODE','IN','EIS_INV_SUBINVENTORY_LOV','','VARCHAR2','N','Y','3','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'WC - Inventory Statement report',401,'Quantity on Hand','Quantity on Hand','','IN','ON_HAND_TYPE_CSV','','VARCHAR2','Y','Y','2','','N','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - WC - Inventory Statement report
xxeis.eis_rs_ins.rcn( 'WC - Inventory Statement report',401,'SUBINVENTORY_CODE','IN',':Subinventory','','','Y','3','Y','SA059956');
--Inserting Report Sorts - WC - Inventory Statement report
xxeis.eis_rs_ins.rs( 'WC - Inventory Statement report',401,'PART_NUMBER','ASC','SA059956','1','');
--Inserting Report Triggers - WC - Inventory Statement report
xxeis.eis_rs_ins.rt( 'WC - Inventory Statement report',401,'begin
xxeis.eis_xxwc_inv_list_rpt_pkg.g_process_id := :SYSTEM.PROCESS_ID;
xxeis.eis_xxwc_inv_list_rpt_pkg.g_qty_onhand := :Quantity on Hand;
xxeis.eis_xxwc_inv_list_rpt_pkg.populate_Inv_List_Details(P_process_id    => :SYSTEM.PROCESS_ID,
                                    p_organization  => :Organization,
                                    p_qty_on_hand           => :Quantity on Hand,
                                    p_subinv  => :Subinventory);
end;','B','Y','SA059956');
xxeis.eis_rs_ins.rt( 'WC - Inventory Statement report',401,'begin
xxeis.eis_xxwc_inv_list_rpt_pkg.CLEAR_TEMP_TABLES(:SYSTEM.PROCESS_ID);
end;','A','Y','SA059956');
--Inserting Report Templates - WC - Inventory Statement report
--Inserting Report Portals - WC - Inventory Statement report
--Inserting Report Dashboards - WC - Inventory Statement report
--Inserting Report Security - WC - Inventory Statement report
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','50924',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','51052',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','50879',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','50852',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','50821',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','20005','','50880',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','51029',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','20005','','50900',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','50865',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','50862',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','50864',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','50849',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','660','','50871',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','50851',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','50619',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','50882',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','50883',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','50981',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','50855',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','50884',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','660','','50857',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','50895',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','222','','51208',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','222','','50894',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','660','','50886',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','50867',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','401','','50868',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Statement report','201','','50892',401,'SA059956','','');
--Inserting Report Pivots - WC - Inventory Statement report
xxeis.eis_rs_ins.rpivot( 'WC - Inventory Statement report',401,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'WC - Inventory Statement report',401,'Pivot','CAT_CLASS','PAGE_FIELD','','','5','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'WC - Inventory Statement report',401,'Pivot','DESCRIPTION','PAGE_FIELD','','','4','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'WC - Inventory Statement report',401,'Pivot','ONHAND','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'WC - Inventory Statement report',401,'Pivot','PART_NUMBER','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'WC - Inventory Statement report',401,'Pivot','SEGMENT_LOCATION','PAGE_FIELD','','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'WC - Inventory Statement report',401,'Pivot','BRANCH_NUMBER','ROW_FIELD','','','1','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
