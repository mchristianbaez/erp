CREATE OR REPLACE PACKAGE XXEIS.EIS_XXWC_INV_LIST_RPT_PKG
AS
--//============================================================================
--//  
--// Change Request 			:: Performance Issue 
--//
--// Object Usage 				:: This Object Referred by "Inventory Listing Report"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_INV_LIST_RPT_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       Pramod  	       05/06/2016   Initial Build --TMS#20160503-00085 --Performance Tuning
--// 1.2  	   Siva			   26/07/2016    TMS#20160725-00007 
--//============================================================================

g_process_id number;
g_qty_onhand varchar2(100); --added for version 1.2

Function get_process_id return number;
--//============================================================================
--//
--// Object Name           :: get_process_id  
--//
--// Object Usage 		   :: This Object Referred by "Inventory Listing Report"
--//
--// Object Type           :: Function
--//
--// Object Description   ::  This  Function is created based on the EIS_XXWC_INV_LISTING_V View to populate
--//                          the process_id in view.
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       Pramod  	    05/06/2016   Initial Build --TMS#20160503-00085 --Performance Tuning
--//============================================================================	
function get_qty_on_hand return varchar2;
--//============================================================================
--//
--// Object Name           :: get_qty_on_hand  
--//
--// Object Usage 		     :: This Object Referred by "Inventory Listing Report"
--//
--// Object Type           :: Function
--//
--// Object Description    ::  This  Function is created based on the EIS_XXWC_INV_LISTING_V View to populate
--//                          the get_qty_on_hand in view.
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.2  	   Siva			     26/07/2016    TMS#20160725-00007 
--//============================================================================	

PROCEDURE POPULATE_INV_LIST_DETAILS(
      P_PROCESS_ID   IN NUMBER,
      P_ORGANIZATION IN VARCHAR2,
      P_SUBINV       IN VARCHAR2,
      P_QTY_ON_HAND  IN VARCHAR2 );
--//============================================================================
--//
--// Object Name         :: POPULATE_INV_LIST_DETAILS  
--//
--// Object Usage 		 :: This Object Referred by "Inventory Listing Report"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_INV_LISTING_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	  05/06/2016   Initial Build --TMS#20160503-00085 --Performance Tuning
--//============================================================================
	  

TYPE CURSOR_TYPE4
IS
  REF
  CURSOR;
  TYPE T_CASHTYPE_VLDN_TBL
IS
  TABLE OF VARCHAR2(100) INDEX BY VARCHAR2(100);
  G_LHQTY_VLDN_TBL T_CASHTYPE_VLDN_TBL;
  G_LHQTY      NUMBER;
  G_END_DATE   DATE;
  G_START_DATE DATE;
FUNCTION GET_END_DATE
    RETURN DATE;
FUNCTION GET_LHQTY(
      P_INVENTORY_ITEM_ID NUMBER)
    RETURN NUMBER;
	
PROCEDURE CLEAR_TEMP_TABLES(
      P_PROCESS_ID IN NUMBER);
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	   05/06/2016   Initial Build --TMS#20160503-00085 --Performance Tuning
--//============================================================================ 

END;
/
