--Report Name            : Low Qty Manual Price Change Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V',660,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Low Qty Man Pri Chg V','EXLQMPCV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','QTY_TYPE',660,'Qty Type','QTY_TYPE','','','','ANONYMOUS','VARCHAR2','','','Qty Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','REGION',660,'Region','REGION','','','','ANONYMOUS','VARCHAR2','','','Region','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','DISTRICT',660,'District','DISTRICT','','','','ANONYMOUS','VARCHAR2','','','District','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','FINAL_GM',660,'Final Gm','FINAL_GM','','','','ANONYMOUS','NUMBER','','','Final Gm','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','FINAL_SELLING_PRICE',660,'Final Selling Price','FINAL_SELLING_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Final Selling Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','MODIFER_NUMBER',660,'Modifer Number','MODIFER_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Modifer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ORIGINAL_UNIT_SELLING_PRICE',660,'Original Unit Selling Price','ORIGINAL_UNIT_SELLING_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Original Unit Selling Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','SALES_QTY',660,'Sales Qty','SALES_QTY','','~T~D~2','','ANONYMOUS','NUMBER','','','Sales Qty','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Item Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','VENDOR_NAME',660,'Vendor Name','VENDOR_NAME','','','','ANONYMOUS','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','VENDOR_NUMBER',660,'Vendor Number','VENDOR_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','INVOICE_DATE',660,'Invoice Date','INVOICE_DATE','','','','ANONYMOUS','DATE','','','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','INVOICE_NUMBER',660,'Invoice Number','INVOICE_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','ANONYMOUS','DATE','','','Ordered Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Customer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','ANONYMOUS','VARCHAR2','','','Customer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','REL_RES_CODE',660,'Rel Res Code','REL_RES_CODE','','','','ANONYMOUS','VARCHAR2','','','Rel Res Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','CREATED_BY',660,'Created By','CREATED_BY','','','','ANONYMOUS','VARCHAR2','','','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ACCOUNT_MANAGER',660,'Account Manager','ACCOUNT_MANAGER','','','','ANONYMOUS','VARCHAR2','','','Account Manager','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','BRANCH',660,'Branch','BRANCH','','','','ANONYMOUS','VARCHAR2','','','Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','APPROVED_BY',660,'Approved By','APPROVED_BY','','','','ANONYMOUS','VARCHAR2','','','Approved By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','BRANCH_NAME',660,'Branch Name','BRANCH_NAME','','','','ANONYMOUS','VARCHAR2','','','Branch Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','PRICE_SOURCE_TYPE',660,'Price Source Type','PRICE_SOURCE_TYPE','','','','ANONYMOUS','VARCHAR2','','','Price Source Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','FINAL_EXTENDED_PRICE',660,'Final Extended Price','FINAL_EXTENDED_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Final Extended Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ORGINAL_EXTEND_PRICE',660,'Orginal Extend Price','ORGINAL_EXTEND_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Orginal Extend Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','BASE_PRICE',660,'Base Price','BASE_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Base Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','SHIP_METHOD',660,'Ship Method','SHIP_METHOD','','','','ANONYMOUS','VARCHAR2','','','Ship Method','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','ANONYMOUS','VARCHAR2','','','Order Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','HOLD_NAME',660,'Hold Name','HOLD_NAME','','','','ANONYMOUS','VARCHAR2','','','Hold Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','PRICING_RESPONSIBILITY',660,'Pricing Responsibility','PRICING_RESPONSIBILITY','','','','ANONYMOUS','VARCHAR2','','','Pricing Responsibility','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Order Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ORDERED_QUANTITY',660,'Ordered Quantity','ORDERED_QUANTITY','','~T~D~2','','ANONYMOUS','NUMBER','','','Ordered Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','SHIP_FROM_ORG_ID',660,'Ship From Org Id','SHIP_FROM_ORG_ID','','','','ANONYMOUS','NUMBER','','','Ship From Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','ANONYMOUS','NUMBER','','','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','DOLLAR_LOST',660,'Dollar Lost','DOLLAR_LOST','','','','ANONYMOUS','NUMBER','','','Dollar Lost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','REL_RES_COMM',660,'Rel Res Comm','REL_RES_COMM','','','','ANONYMOUS','VARCHAR2','','','Rel Res Comm','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ORGINIAL_GM',660,'Orginial Gm','ORGINIAL_GM','','','','ANONYMOUS','NUMBER','','','Orginial Gm','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','UNIT_COST',660,'Unit Cost','UNIT_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Unit Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','LHQTY',660,'Lhqty','LHQTY','','~T~D~2','','ANONYMOUS','NUMBER','','','Lhqty','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','PROCESS_ID',660,'Process Id','PROCESS_ID','','','','ANONYMOUS','NUMBER','','','Process Id','','','','US');
--Inserting Object Components for EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V
--Inserting Object Component Joins for EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Low Qty Manual Price Change Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Low Qty Manual Price Change Report
xxeis.eis_rsc_ins.lov( 660,'select  RS.Name,SALESREP_ID  from  RA_SALESREPS RS
WHERE  RS.NAME is not null','','OM SALES REP','This gives the sales representative name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select  cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','OM CUSTOMER NUMBER','This gives the Customer Number','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select  fu.USER_NAME,FU.USER_ID    from  fnd_user fu','','OM USER NAME','This gives the user''s names','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT DISTINCT MSI.SEGMENT1   ITEM_NUMBER,
MSI.DESCRIPTION  ITEM_DESCRIPTION from mtl_system_items_b msi','','OM Item Number LOV','Order Item numbers','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select DESCRIPTION ITEM_DESCRIPTION from MTL_SYSTEM_ITEMS_KFV
where exists (Select 1 from XXEIS.EIS_ORG_ACCESS_V where organization_id = organization_id)
','','OM ITEM DESCRIPTION','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct ATTRIBUTE9 region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct pov.segment1 vendor_number, pov.vendor_name vendor_name  from po_vendors pov, mtl_system_items_b msi, mrp_sr_assignments msa,
    mrp_sr_receipt_org msro,
    mrp_sr_source_org msso,
    mrp_sourcing_rules msr
WHERE msi.inventory_item_id         = msa.inventory_item_id
  AND msi.organization_id           = msa.organization_id
  AND msa.sourcing_rule_id          = msro.sourcing_rule_id
  AND msa.sourcing_rule_id          = msr.sourcing_rule_id
  AND msro.sr_receipt_id            = msso.sr_receipt_id
  AND msso.vendor_id                = pov.vendor_id
  AND msi.source_type               = 2','','XXWC VENDOR NUMBER','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT ''LOW'' X FROM DUAL
UNION
select ''HIGH'' x from dual','','XXWC_QTY_TYPE','This is custom lov','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct pov.vendor_name vendor_name  ,pov.segment1 vendor_number from po_vendors pov, mtl_system_items_b msi, mrp_sr_assignments msa,
    mrp_sr_receipt_org msro,
    mrp_sr_source_org msso,
    mrp_sourcing_rules msr
WHERE msi.inventory_item_id         = msa.inventory_item_id
  AND msi.organization_id           = msa.organization_id
  AND msa.sourcing_rule_id          = msro.sourcing_rule_id
  AND msa.sourcing_rule_id          = msr.sourcing_rule_id
  AND msro.sr_receipt_id            = msso.sr_receipt_id
  AND msso.vendor_id                = pov.vendor_id
  AND msi.source_type               = 2','','XXWC_VEN_NAME','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' FROM DUAL','','XXWC All ORG LIST','XXWC All ORG LIST','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Low Qty Manual Price Change Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Low Qty Manual Price Change Report
xxeis.eis_rsc_utility.delete_report_rows( 'Low Qty Manual Price Change Report' );
--Inserting Report - Low Qty Manual Price Change Report
xxeis.eis_rsc_ins.r( 660,'Low Qty Manual Price Change Report','','The purpose of this report is to distinguish between low quantity and high quanity manual price adjustments, as well as review the corresponding reason codes for the manual price changes that went on hold.','','','','SA059956','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','Y','','','SA059956','','Y','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Low Qty Manual Price Change Report
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'ACCOUNT_MANAGER','Account Manager','Account Manager','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'CREATED_BY','Created By','Created By','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'DISTRICT','District','District','','','default','','17','N','','','','','','','','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'FINAL_GM','Final GM%','Final Gm','','~,~.~2','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'FINAL_SELLING_PRICE','Final Selling Price','Final Selling Price','','~,~.~5','default','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'ITEM_NUMBER','Item Number','Item Number','','','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'MODIFER_NUMBER','Modifer Number','Modifer Number','','','default','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'ORIGINAL_UNIT_SELLING_PRICE','Original Unit Selling Price','Original Unit Selling Price','','~,~.~5','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'REGION','Region','Region','','','default','','18','N','','','','','','','','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'SALES_QTY','Sales Qty','Sales Qty','','~T~D~0','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'APPROVED_BY','Approved By','Approved By','','','default','','19','N','','','','','','','','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'REL_RES_CODE','Hold Release Reason Code','Rel Res Code','','','default','','20','N','','','','','','','','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'DOLLARS_LOST','Dollar Lost','Rel Res Code','NUMBER','~T~D~5','default','','16','Y','','','','','','','(NVL(EXLQMPCV.ORGINAL_EXTEND_PRICE,0)-NVL(EXLQMPCV.FINAL_EXTENDED_PRICE,0))','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'BRANCH_NAME','Branch Name','Branch Name','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'QTY_TYPE','Qty Type','Qty Type','','','','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Low Qty Manual Price Change Report',660,'PRICE_SOURCE_TYPE','Price Source Type','Price Source Type','','','','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','GROUP_BY','US','');
--Inserting Report Parameters - Low Qty Manual Price Change Report
xxeis.eis_rsc_ins.rp( 'Low Qty Manual Price Change Report',660,'Account Manager','Account Manager','ACCOUNT_MANAGER','IN','OM SALES REP','','VARCHAR2','N','Y','6','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Low Qty Manual Price Change Report',660,'Branch #','Branch','BRANCH','IN','XXWC All ORG LIST','','VARCHAR2','Y','Y','1','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Low Qty Manual Price Change Report',660,'Created By','Created By','CREATED_BY','IN','OM USER NAME','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Low Qty Manual Price Change Report',660,'Customer Name','Customer Name','CUSTOMER_NAME','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','15','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Low Qty Manual Price Change Report',660,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','OM CUSTOMER NUMBER','','VARCHAR2','N','Y','14','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Low Qty Manual Price Change Report',660,'Item Description','Item Description','ITEM_DESCRIPTION','IN','OM ITEM DESCRIPTION','','VARCHAR2','N','Y','13','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Low Qty Manual Price Change Report',660,'Item #','Item Number','ITEM_NUMBER','IN','OM Item Number LOV','','VARCHAR2','N','Y','12','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Low Qty Manual Price Change Report',660,'Order Start Date','Order Start Date','ORDERED_DATE','>=','','','DATE','N','Y','2','Y','N','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Low Qty Manual Price Change Report',660,'Qty Type','Qty Type','QTY_TYPE','IN','XXWC_QTY_TYPE','','VARCHAR2','N','Y','16','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Low Qty Manual Price Change Report',660,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','8','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Low Qty Manual Price Change Report',660,'Vendor Name','Vendor Name','VENDOR_NAME','IN','XXWC_VEN_NAME','','VARCHAR2','N','Y','11','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Low Qty Manual Price Change Report',660,'Vendor #','Vendor Number','VENDOR_NUMBER','IN','XXWC VENDOR NUMBER','','VARCHAR2','N','Y','10','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Low Qty Manual Price Change Report',660,'Order End Date','Order End Date','ORDERED_DATE','<=','','','DATE','N','Y','3','Y','N','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Low Qty Manual Price Change Report',660,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','9','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Low Qty Manual Price Change Report',660,'Invoice Start Date','Invoice Start Date','INVOICE_DATE','>=','','','DATE','N','Y','4','Y','N','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Low Qty Manual Price Change Report',660,'Invoice End Date','Invoice End Date','INVOICE_DATE','<=','','','DATE','N','Y','5','Y','N','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','US','');
--Inserting Dependent Parameters - Low Qty Manual Price Change Report
--Inserting Report Conditions - Low Qty Manual Price Change Report
xxeis.eis_rsc_ins.rcnh( 'Low Qty Manual Price Change Report',660,'CREATED_BY IN :Created By ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATED_BY','','Created By','','','','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','','','','IN','Y','Y','','','','','1',660,'Low Qty Manual Price Change Report','CREATED_BY IN :Created By ');
xxeis.eis_rsc_ins.rcnh( 'Low Qty Manual Price Change Report',660,'QTY_TYPE IN :Qty Type ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','QTY_TYPE','','Qty Type','','','','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','','','','IN','Y','Y','','','','','1',660,'Low Qty Manual Price Change Report','QTY_TYPE IN :Qty Type ');
xxeis.eis_rsc_ins.rcnh( 'Low Qty Manual Price Change Report',660,'VENDOR_NAME IN :Vendor Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NAME','','Vendor Name','','','','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','','','','IN','Y','Y','','','','','1',660,'Low Qty Manual Price Change Report','VENDOR_NAME IN :Vendor Name ');
xxeis.eis_rsc_ins.rcnh( 'Low Qty Manual Price Change Report',660,'VENDOR_NUMBER IN :Vendor # ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NUMBER','','Vendor #','','','','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','','','','','IN','Y','Y','','','','','1',660,'Low Qty Manual Price Change Report','VENDOR_NUMBER IN :Vendor # ');
xxeis.eis_rsc_ins.rcnh( 'Low Qty Manual Price Change Report',660,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND PROCESS_ID=:SYSTEM.PROCESS_ID','1',660,'Low Qty Manual Price Change Report','Free Text ');
--Inserting Report Sorts - Low Qty Manual Price Change Report
--Inserting Report Triggers - Low Qty Manual Price Change Report
xxeis.eis_rsc_ins.rt( 'Low Qty Manual Price Change Report',660,'begin
xxeis.eis_rs_xxwc_low_qty_pri_pkg.low_qty_pri_par (
P_PROCESS_ID => :SYSTEM.PROCESS_ID
,p_branch => :Branch #
,p_order_date_from  	=> :Order Start Date
,p_order_date_to   		=> :Order End Date
,p_invoice_date_from    => :Invoice Start Date
,p_invoice_date_to      => :Invoice End Date
,p_Account_Manager      => :Account Manager
,p_Region  				=> :Region
,p_District   			=> :District
,p_Item_num   			=> :Item #
,p_Item_Description  	=> :Item Description
,p_Customer_Number   	=> :Customer Number
,p_Customer_Name  		=> :Customer Name
);
end;','B','Y','SA059956','AQ');
--inserting report templates - Low Qty Manual Price Change Report
--Inserting Report Portals - Low Qty Manual Price Change Report
--inserting report dashboards - Low Qty Manual Price Change Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Low Qty Manual Price Change Report','660','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','N','');
--inserting report security - Low Qty Manual Price Change Report
xxeis.eis_rsc_ins.rsec( 'Low Qty Manual Price Change Report','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Low Qty Manual Price Change Report','661','','XXWC_PRICING_MANAGER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Low Qty Manual Price Change Report','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Low Qty Manual Price Change Report','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Low Qty Manual Price Change Report','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Low Qty Manual Price Change Report','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Low Qty Manual Price Change Report','','PP018915','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Low Qty Manual Price Change Report','20005','','XXWC_IT_OPERATIONS_ANALYST',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Low Qty Manual Price Change Report','','SS084202','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Low Qty Manual Price Change Report','','SA059956','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Low Qty Manual Price Change Report','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',660,'SA059956','','','');
--Inserting Report Pivots - Low Qty Manual Price Change Report
--Inserting Report   Version details- Low Qty Manual Price Change Report
xxeis.eis_rsc_ins.rv( 'Low Qty Manual Price Change Report','','Low Qty Manual Price Change Report','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
