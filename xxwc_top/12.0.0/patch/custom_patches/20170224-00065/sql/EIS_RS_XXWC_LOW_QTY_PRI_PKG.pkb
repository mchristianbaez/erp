CREATE OR REPLACE PACKAGE BODY XXEIS.EIS_RS_XXWC_LOW_QTY_PRI_PKG
AS
--//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "Low Qty Manual Price Change Report"
--//
--// Object Name         		:: XXEIS.EIS_RS_XXWC_LOW_QTY_PRI_PKG
--//
--// Object Type         		:: Package Body
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author              Date            Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	          04/12/2016      Initial Build  TMS#20150928-00191  --Performance Tuning
--// 1.1		Siva			  8/05/2016	       20160525-00244 
--// 1.2     	Siva 			  15-Mar-2017     TMS#20170224-00065 
--//============================================================================ 
  FUNCTION get_empolyee_name(p_employee_id IN NUMBER, p_date IN DATE) RETURN varchar2
IS
L_HASH_INDEX varchar2(100);
L_SQL varchar2(32000);
begin
G_empolyee_name := NULL;

l_sql :='SELECT ppf.full_name
        FROM per_all_people_f ppf,fnd_user fu
       WHERE     1 = 1
             AND ppf.person_id = fu.employee_id
             and fu.user_id=:1
             and (TRUNC (:2) between TRUNC (PPF.EFFECTIVE_START_DATE)
                                     AND NVL (ppf.effective_end_date, :3))';
    BEGIN
        l_hash_index:=p_employee_id||'-'||p_date;
        G_EMPOLYEE_NAME := G_EMPOLYEE_NAME_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO G_empolyee_name USING p_employee_id,p_date,p_date;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_EMPOLYEE_NAME :=null;
    WHEN OTHERS THEN
    G_EMPOLYEE_NAME :=null;
    END;      
                      l_hash_index:=p_employee_id||'-'||p_date;
                       G_EMPOLYEE_NAME_VLDN_TBL(L_HASH_INDEX) := G_EMPOLYEE_NAME;
    END;
     return  G_EMPOLYEE_NAME;
     EXCEPTION when OTHERS then
      G_EMPOLYEE_NAME:=null;
      RETURN  G_EMPOLYEE_NAME;

end get_empolyee_name;


  FUNCTION get_adj_auto_modifier_amt(P_HEADER_ID number, P_LINE_ID number) RETURN NUMBER
IS
L_HASH_INDEX varchar2(100);
L_SQL varchar2(32000);
begin
G_ADJ_AUTO_MODIFIER_AMT := NULL;

l_sql :=' SELECT NVL ( (SUM (adj_auto.adjusted_amount) * (-1)), 0)
        FROM oe_price_adjustments_v adj_auto
       WHERE     adj_auto.header_id = :1
             AND adj_auto.line_id = :2
             AND adj_auto.automatic_flag = ''Y''';
    BEGIN
        l_hash_index:=P_HEADER_ID||'-'||P_LINE_ID;
        G_ADJ_AUTO_MODIFIER_AMT := G_ADJ_AUTO_MOD_AMT_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO G_ADJ_AUTO_MODIFIER_AMT USING p_header_id,P_LINE_ID;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_ADJ_AUTO_MODIFIER_AMT :=0;
    WHEN OTHERS THEN
    G_ADJ_AUTO_MODIFIER_AMT :=0;
    END;      
                      l_hash_index:=p_header_id||'-'||P_LINE_ID;
                       G_ADJ_AUTO_MOD_AMT_VLDN_TBL(L_HASH_INDEX) := G_ADJ_AUTO_MODIFIER_AMT;
    END;
     return  G_ADJ_AUTO_MODIFIER_AMT;
     EXCEPTION WHEN OTHERS THEN
      G_ADJ_AUTO_MODIFIER_AMT:=0;
      RETURN  G_ADJ_AUTO_MODIFIER_AMT;

end get_adj_auto_modifier_amt;

  FUNCTION get_adj_auto_modifier_name(P_HEADER_ID number, P_LINE_ID number) RETURN VARCHAR2
IS
L_HASH_INDEX varchar2(100);
L_SQL varchar2(32000);
begin
G_ADJ_AUTO_MODIFIER_NAME := NULL;

l_sql :=' SELECT MAX (adjustment_name)
        FROM oe_price_adjustments_v adj_auto
       WHERE     adj_auto.header_id = :1
             AND adj_auto.line_id = :2
             AND adj_auto.automatic_flag = ''Y''';
    BEGIN
        l_hash_index:=P_HEADER_ID||'-'||P_LINE_ID;
        G_ADJ_AUTO_MODIFIER_NAME := G_ADJ_AUTO_MOD_NAME_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO G_ADJ_AUTO_MODIFIER_NAME USING p_header_id,P_LINE_ID;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_ADJ_AUTO_MODIFIER_NAME :=null;
    WHEN OTHERS THEN
    G_ADJ_AUTO_MODIFIER_NAME :=null;
    END;      
                      l_hash_index:=p_header_id||'-'||P_LINE_ID;
                       G_ADJ_AUTO_MOD_NAME_VLDN_TBL(L_HASH_INDEX) := G_ADJ_AUTO_MODIFIER_NAME;
    END;
     return  G_ADJ_AUTO_MODIFIER_NAME;
     EXCEPTION WHEN OTHERS THEN
      G_ADJ_AUTO_MODIFIER_NAME:=null;
      RETURN  G_ADJ_AUTO_MODIFIER_NAME;

end get_adj_auto_modifier_name;


  FUNCTION get_lhqty(p_inventory_item_id number) RETURN VARCHAR2
IS
L_HASH_INDEX varchar2(100);
L_SQL varchar2(32000);
begin
G_LHQTY := NULL;

l_sql:='SELECT  MAX (cross_reference) cross_reference
               FROM inv.mtl_cross_references_b
              where CROSS_REFERENCE_TYPE = ''XXWC_QP_LOW_QTY_TBL''
              and inventory_item_id = :1
           GROUP BY inventory_item_id';
    BEGIN
        l_hash_index:=p_inventory_item_id;
        G_LHQTY :=G_LHQTY_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    BEGIN      
    EXECUTE IMMEDIATE L_SQL INTO G_LHQTY USING p_inventory_item_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_LHQTY :=NULL;
    WHEN OTHERS THEN
    G_LHQTY :=NULL;
    END;      
                      l_hash_index:=p_inventory_item_id;
                       G_LHQTY_VLDN_TBL(L_HASH_INDEX) := G_LHQTY;
    END;
     return  G_LHQTY;
     EXCEPTION WHEN OTHERS THEN
      G_LHQTY:=NULL;
      RETURN  G_LHQTY;

end get_lhqty;

FUNCTION GET_price_source_type(P_HEADER_ID number, P_LINE_ID number) RETURN VARCHAR2
IS  
--//============================================================================
--//
--// Object Name           :: GET_price_source_type  
--//
--// Object Usage 		   :: This Object Referred by "Low Qty Manual Price Change Report"
--//
--// Object Type           :: Function
--//
--// Object Description   ::  This  Function is created to get the price_source_type column.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.1		Siva			8/05/2016	20160525-00244 
--//============================================================================
L_HASH_INDEX varchar2(100);
L_SQL varchar2(32000);
begin
g_price_source_type := NULL;

l_sql :='SELECT MAX(VS.DESCRIPTION) DESCRIPTION
FROM OE_PRICE_ADJUSTMENTS_V ADJ,
  (SELECT ATTRIBUTE10,
    NAME,
    LIST_HEADER_ID
  FROM QP_SECU_LIST_HEADERS_VL LH
  WHERE CONTEXT     =162
  AND AUTOMATIC_FLAG=''Y''
  )LH ,
  (SELECT NVL(FFV.DESCRIPTION,''MKT'') DESCRIPTION,
    FLEX_VALUE
  FROM APPS.FND_FLEX_VALUES_VL FFV
  WHERE FLEX_VALUE_SET_ID=1015252
  AND ENABLED_FLAG       =''Y''
  ) VS
WHERE ADJ.HEADER_ID =:1
AND ADJ.LINE_ID =:2
AND ADJ.LIST_HEADER_ID  = LH.LIST_HEADER_ID
AND LH.ATTRIBUTE10      = VS.FLEX_VALUE
AND ADJ.APPLIED_FLAG    =''Y''
AND ADJ.LIST_LINE_TYPE_CODE<>''FREIGHT_CHARGE''';
        
    BEGIN
        l_hash_index:=P_HEADER_ID||'-'||P_LINE_ID;
        g_price_source_type := g_price_source_type_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO g_price_source_type USING p_header_id,P_LINE_ID;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_price_source_type :=null;
    WHEN OTHERS THEN
    g_price_source_type :=null;
    END;      
                      l_hash_index:=p_header_id||'-'||P_LINE_ID;
                       g_price_source_type_VLDN_TBL(L_HASH_INDEX) := g_price_source_type;
    END;
     return  g_price_source_type;
     EXCEPTION WHEN OTHERS THEN
      g_price_source_type:=null;
      RETURN  g_price_source_type;

end GET_price_source_type;

PROCEDURE LOW_QTY_PRI_PAR (p_process_id number,
                          P_BRANCH      IN VARCHAR2
                          ,P_ORDER_DATE_FROM   IN DATE
                          ,p_order_date_to   IN date
                          ,p_invoice_date_from   IN date
                          ,p_invoice_date_to   in date   
                          ,p_Account_Manager   IN VARCHAR2
                          ,p_Region  				IN VARCHAR2
                          ,p_District   		IN VARCHAR2
                          ,p_Item_num   		IN VARCHAR2
                          ,P_ITEM_DESCRIPTION  	IN VARCHAR2
                          ,p_Customer_Number   	IN VARCHAR2
                          ,P_CUSTOMER_NAME  		IN VARCHAR2
                          )
	as	
--//============================================================================
--//
--// Object Name         :: LOW_QTY_PRI_PAR  
--//
--// Object Usage 		 :: This Object Referred by "Low Qty Manual Price Change Report"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  	04/12/2016   Initial Build --TMS#20150928-00191  --Performance Tuning
--// 1.1		Siva			  8/05/2016	       20160525-00244 
--// 1.2     	Siva 			  15-Mar-2017     TMS#20170224-00065 
--//============================================================================	
  l_non_invoiced_order_csr1 SYS_REFCURSOR;
  l_non_invoiced_order_csr2 SYS_REFCURSOR;
  l_invoiced_order_csr SYS_REFCURSOR;
   L_HEADER_ID_CSR SYS_REFCURSOR;
   L_CUSTOMER_TRX_ID_CSR SYS_REFCURSOR;
   lc_where_cond  VARCHAR2(32000);
    lc_order_date  VARCHAR2(32000);
    lc_invoice_date  varchar2(32000);
    L_DELETE_STMT varchar2(32000);
      
	type l_order_id_type
   IS
  record
  ( process_id number,header_id number  );
      type l_order_id_type_tbl is  table of l_order_id_type index by binary_integer;
   l_order_id_rec_tbl  l_order_id_type_tbl;

  type l_header_id_type
   IS
  record
  ( header_id number  ); 
  
    TYPE l_header_id_type_tbl IS  table of l_header_id_type index by binary_integer;
l_header_id_rec_tbl  l_header_id_type_tbl;

 
  
  
  	type L_customer_trx_id_TYPE
   IS
  RECORD
  (
  CUSTOMER_TRX_ID NUMBER,
  HEADER_ID  NUMBER,
  LINE_ID  NUMBER,
  customer_trx_line_id  NUMBER
  );
  
      TYPE l_customer_trx_id_type_tbl IS
  TABLE OF L_customer_trx_id_TYPE INDEX BY BINARY_INTEGER;
 L_customer_trx_id_REC_TBL  l_customer_trx_id_type_tbl;

-- l_customer_trx_id_qry
  
  type l_ord_low_qty_pri_tbl
is
  table of XXEIS.XXWC_LOW_QTY_MAN_PRIC_TBL%rowtype index by BINARY_INTEGER;
  l_ord_low_qty_pri_rec_tbl l_ord_low_qty_pri_tbl;
   
  -- Identify the driving table and Filtering the data based on the parameters
  l_header_id_qry VARCHAR2(32000):='SELECT /*+INDEX(L OE_ORDER_LINES_N1) INDEX(OH OE_ORDER_HOLDS_ALL_N1) INDEX(HS OE_HOLD_SOURCES_U1,OE_HOLD_SOURCES_N1) USE_NL(HS,HD)*/
  '||P_PROCESS_ID ||' process_id,h.header_id
FROM OE_ORDER_HEADERS_ALL H ,
  OE_ORDER_LINES_ALL L ,
  OE_ORDER_HOLDS OH,
  OE_HOLD_DEFINITIONS HD,
  OE_HOLD_SOURCES HS
WHERE H.FLOW_STATUS_CODE  <> ''CANCELLED''
AND TRUNC(H.ORDERED_DATE) >= '''||p_order_date_from||'''
AND TRUNC(H.ORDERED_DATE) <= '''||p_order_date_to||'''
AND L.HEADER_ID            = H.HEADER_ID
AND oh.header_id(+)        = l.header_id
AND OH.LINE_ID(+)          = L.LINE_ID
AND OH.HOLD_SOURCE_ID      = HS.HOLD_SOURCE_ID(+)
AND HS.HOLD_ID             = HD.HOLD_ID(+)
AND UPPER(HD.name(+))         =''PRICING GUARDRAIL HOLD''
AND EXISTS
  (SELECT ''1''
  FROM OE_PRICE_ADJUSTMENTS_V OPA
  WHERE opa.header_id(+)         = h.header_id
  AND (OPA.LINE_ID              IS NULL
  OR opa.line_id                 = l.line_id)
  AND opa.list_line_type_code(+) = ''DIS''
  AND opa.adjustment_name       IN (''AMOUNT_LINE_DISCOUNT'' ,''NEW PRICE'' ,''PERCENT_LINE_DISCOUNT'' ,''NEW_PRICE_DISCOUNT'' ,''PERCENT_ORDER_DISCOUNT'')
  )
';
		
 l_order_id_qry VARCHAR2(32000):='select header_id from XXEIS.EIS_XXWC_LOW_QTY_HDR_TBL where process_id='||P_PROCESS_ID ||'';
     										  
  -- Identify the driving table and Filtering the data based on the parameters
 l_customer_trx_id_qry VARCHAR2(32000):= 'SELECT RCt.customer_trx_id customer_trx_id,h.header_id header_id,l.line_id,rct.customer_trx_line_id
from OE_ORDER_HEADERS_ALL H ,
  oe_order_lines_all l,
  RA_CUSTOMER_TRX_LINES_aLL RCT,
  oe_holds_history_v ho
where h.flow_status_code <> ''CANCELLED''
and trunc(rct.creation_date)>= '''||p_invoice_date_from||'''
and trunc(rct.creation_date)<= '''||p_invoice_date_to||'''
and l.header_id           =h.header_id 
and rct.interface_line_context = ''ORDER ENTRY''
and to_char (h.order_number)  =    rct.interface_line_attribute1 
AND TO_CHAR(L.LINE_ID) =    rct.interface_line_attribute6
and  L.HEADER_ID          = HO.HEADER_ID(+)     
and  L.LINE_ID            =  HO.LINE_ID(+)       
and upper(ho.hold_name(+))=''PRICING GUARDRAIL HOLD''
AND EXISTS
  (SELECT ''1''
  FROM OE_PRICE_ADJUSTMENTS_V OPA
  WHERE opa.header_id(+) = h.header_id
 AND (opa.line_id IS NULL OR opa.line_id = l.line_id)
  AND opa.list_line_type_code(+) = ''DIS''
  and opa.adjustment_name       in (''AMOUNT_LINE_DISCOUNT'' ,''NEW PRICE'' ,''PERCENT_LINE_DISCOUNT'' ,''NEW_PRICE_DISCOUNT'' ,''PERCENT_ORDER_DISCOUNT''))';
  

  -- Process the driving data to below query
   L_NON_INVOICED_ORDER_QRY1 VARCHAR2(32000):=' SELECT 
          '||p_process_id||' p_process_id,
          ood.organization_name branch_name,
          mp.organization_code branch,
          rs.name account_manager,
          xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.get_empolyee_name (oh.created_by,
                                                            oh.creation_date)
             created_by, --fu.user_name                                                             CREATED_BY,
          NULL pricing_responsibility, --xxeis.eis_rs_xxwc_com_util_pkg.get_pricing_resp(oh.created_by) pricing_responsibility,
          DECODE (ohh.hold_name, ''Pricing Guardrail Hold'', ohh.hold_name)
             hold_name, -- DECODE(OHH.HOLD_NAME,''Pricing Guardrail Hold'',OHH.RELEASED_BY)          APPROVED_BY,
          --DECODE(OHH.HOLD_NAME,''Pricing Guardrail Hold'',xxeis.eis_rs_xxwc_com_util_pkg.get_releaseby(OHH.RELEASED_BY,OHH.CREATION_DATE))APPROVED_BY,
          NULL approved_by, --DECODE(OHH.HOLD_NAME,''Pricing Guardrail Hold'',FLVR.MEANING) REL_RES_CODE,
          NULL rel_res_code, --DECODE(OHH.HOLD_NAME,''Pricing Guardrail Hold'',OHH.RELEASE_COMMENT) REL_RES_COMM,
          NULL rel_res_comm,
          hp.party_name customer_name,
          hca.account_number customer_number,
          TO_CHAR (oh.order_number) order_number,
          TRUNC (oh.ordered_date) ordered_date,
          rct.trx_number invoice_number,
          TRUNC (rct.trx_date) invoice_date,
          ott.name order_type,
          CASE
             WHEN    UPPER (flv.meaning) LIKE ''%WALK%IN%''
                  OR UPPER (flv.meaning) LIKE ''%WILL%CALL%''
             THEN
                flv.meaning
             ELSE
                ''Delivery''
          END
             ship_method, --DECODE(flv.meaning,''5. Walk In'',''5. Walk In'',''8.  WCD-Will Call'',''8.  WCD-Will Call'', ''0. Will Call'',''0. Will Call'',''Delivery'') SHIP_METHOD,
          --xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id) vendor_name,
          --xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number(msi.inventory_item_id,msi.organization_id) vendor_number,
         isr.vendor_name vendor_name,
         isr.vendor_num vendor_number,
        -- xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.xxget_isr_vendor_details(msi.inventory_item_id,msi.organization_id,''VENDORNAME'') vendor_name,
        -- xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.xxget_isr_vendor_details(msi.inventory_item_id,msi.organization_id,''VENDORNUM'') vendor_number,
          msi.concatenated_segments item_number, --ol.ordered_item item_number,
          msi.description item_description,
          NVL (
             DECODE (ol.line_category_code,
                     ''RETURN'', (ol.ordered_quantity * -1),
                     ol.ordered_quantity),
             0)
             sales_qty,
          ol.unit_list_price base_price,
          (  NVL (ol.unit_list_price, 0)
           - xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.get_adj_auto_modifier_amt (
                ol.header_id,
                ol.line_id))
             original_unit_selling_price,
            (  NVL (ol.unit_list_price, 0)
             - xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.get_adj_auto_modifier_amt (
                  ol.header_id,
                  ol.line_id))
          * NVL (
               DECODE (ol.line_category_code,
                       ''RETURN'', (ol.ordered_quantity * -1),
                       ol.ordered_quantity),
               0)
             orginal_extend_price, --(NVL(ol.unit_list_price,0) -xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_amt(ol.header_id,ol.line_id))-NVL(ol.unit_selling_price,0) dollar_lost,
          0 dollar_lost,
          xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.get_adj_auto_modifier_name (
             ol.header_id,
             ol.line_id)
             modifer_number,
          ol.unit_selling_price final_selling_price,
            NVL (ol.unit_selling_price, 0)
          * NVL (
               DECODE (ol.line_category_code,
                       ''RETURN'', (ol.ordered_quantity * -1),
                       ol.ordered_quantity),
               0)
             final_extended_price,
            (NVL (ol.unit_selling_price, 0) - NVL (ol.unit_cost, 0))
          * 100
          / DECODE (NVL (ol.unit_selling_price, 0),
                    0, 1,
                    ol.unit_selling_price)
             final_gm, --((NVL(OL.UNIT_LIST_PRICE,0)   -XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ADJ_AUTO_MODIFIER_AMT(OL.HEADER_ID,OL.LINE_ID)) -NVL(OL.UNIT_COST,0))*100/DECODE(NVL(OL.UNIT_SELLING_PRICE,0),0,1,OL.UNIT_SELLING_PRICE) ORGINIAL_GM,
          0 orginial_gm /*,apps.qp_qp_form_pricing_attr.get_meaning (opa.arithmetic_operator, ''ARITHMETIC_OPERATOR'')
                              application_method*/
                   -- Commented by Mahesh for TMS#20140320-00210 on 10/13/2014
          , --(OL.UNIT_LIST_PRICE -XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ADJ_AUTO_MODIFIER_AMT(OL.HEADER_ID,OL.LINE_ID))-OL.UNIT_SELLING_PRICE DOLLAR_LOST,
          mp.attribute8 district,
          mp.attribute9 region,
          ol.inventory_item_id,
          ol.ship_from_org_id,
          ol.ordered_quantity, --XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LOW_HIGH_F(MSI.CONCATENATED_SEGMENTS,DECODE(OL.LINE_CATEGORY_CODE,''RETURN'',(OL.ORDERED_QUANTITY*-1),OL.ORDERED_QUANTITY)) QTY_TYPE,
          CASE
             WHEN (   (    NVL (
                              DECODE (ol.line_category_code,
                                      ''RETURN'', (ol.ordered_quantity * -1),
                                      ol.ordered_quantity),
                              0) > NVL (TO_NUMBER (xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.GET_LHQTY(msi.inventory_item_id)), 0)
                       AND NVL (TO_NUMBER (xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.GET_LHQTY(msi.inventory_item_id)), 0) != 0)
                   OR (    NVL (TO_NUMBER (xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.GET_LHQTY(msi.inventory_item_id)), 0) = 0
                       AND NVL (
                              DECODE (ol.line_category_code,
                                      ''RETURN'', (ol.ordered_quantity * -1),
                                      ol.ordered_quantity),
                              0) > 3))
             THEN
                ''HIGH''
             ELSE
                ''LOW''
          END
             qty_type,
          NVL (ol.unit_cost, 0) unit_cost,
          NVL (TO_NUMBER (xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.GET_LHQTY(msi.inventory_item_id)), 0) lhqty
		  ,XXEIS.EIS_RS_XXWC_LOW_QTY_PRI_PKG.GET_price_source_type(ol.header_id,ol.line_id) price_source_type--added for version 1.1      
     FROM oe_order_headers oh,
          oe_order_lines ol,
          oe_transaction_types_vl ott,
          fnd_lookup_values flv,
          hz_cust_accounts hca,
          hz_parties hp,
          oe_holds_history_v ohh,
          ra_salesreps rs,
          org_organization_definitions ood,
          mtl_parameters mp,
          mtl_system_items_b_kfv msi --,oe_price_adjustments_v opa               -- Commented by Mahesh for TMS#20140320-00210 on 10/13/2014
                                    ,
          ra_customer_trx rct,
          ra_customer_trx_lines rctl,
         -- fnd_user fu,
          XXEIS.EIS_XXWC_PO_ISR_TAB ISR          
--          (  SELECT inventory_item_id, MAX (cross_reference) cross_reference
--               FROM inv.mtl_cross_references_b
--              WHERE cross_reference_type = ''XXWC_QP_LOW_QTY_TBL''
--           GROUP BY inventory_item_id) lhqty
    --    xxeis_wc_price_adj_v pri_adj
    --  FND_LOOKUP_VALUES FLVR
    WHERE     oh.header_id = ol.header_id
          AND ott.transaction_type_id = oh.order_type_id
          AND flv.lookup_type = ''SHIP_METHOD''
          AND flv.lookup_code = ol.shipping_method_code
          AND hca.cust_account_id = ol.sold_to_org_id
          AND hca.party_id = hp.party_id
          AND ohh.header_id(+) = ol.header_id
          AND ohh.line_id(+) = ol.line_id
          AND rs.salesrep_id(+) = ol.salesrep_id
          AND rs.org_id(+) = ol.org_id
          AND mp.organization_id = ol.ship_from_org_id
          AND msi.organization_id = ol.ship_from_org_id
          AND msi.inventory_item_id = ol.inventory_item_id
          AND msi.organization_id = isr.organization_id(+)
          AND msi.inventory_item_id = isr.inventory_item_id(+)
--           and msi.inventory_item_id=2976137
--          AND ood.organization_id=228
         -- AND msi.inventory_item_id = lhqty.inventory_item_id(+)              --Commented by Pramod on 28/03/2016 to increase performance
          --AND opa.header_id(+) = ol.header_id                                 -- Commented by Mahesh for TMS#20140320-00210 on 10/13/2014
          --AND (opa.line_id IS NULL OR opa.line_id = ol.line_id)               -- Commented by Mahesh for TMS#20140320-00210 on 10/13/2014
          --AND opa.line_id(+)                                = ol.line_id
          AND ood.organization_id = mp.organization_id
          and oh.header_id      =:header_id
          /*AND opa.list_line_type_code(+) = ''DIS''
          AND opa.adjustment_name IN (''AMOUNT_LINE_DISCOUNT''
                                     ,''NEW PRICE''
                                     ,''PERCENT_LINE_DISCOUNT''
                                     ,''NEW_PRICE_DISCOUNT''
                                     ,''PERCENT_ORDER_DISCOUNT'')*/
          -- Commented by Mahesh for TMS#20140320-00210 on 10/13/2014
          AND EXISTS
                 (SELECT ''1''
                    FROM oe_price_adjustments_v opa
                   WHERE     opa.header_id(+) = ol.header_id
                         AND (opa.line_id IS NULL OR opa.line_id = ol.line_id)
                         AND opa.list_line_type_code(+) = ''DIS''
                         AND opa.adjustment_name IN (''AMOUNT_LINE_DISCOUNT'',
                                                     ''NEW PRICE'',
                                                     ''PERCENT_LINE_DISCOUNT'',
                                                     ''NEW_PRICE_DISCOUNT'',
                                                     ''PERCENT_ORDER_DISCOUNT'')) -- Added by Mahesh for TMS#20140320-00210 on 10/13/2014
          AND rct.interface_header_context = ''ORDER ENTRY''
          AND rct.interface_header_attribute1 = TO_CHAR (oh.order_number)
          AND rctl.interface_line_context = ''ORDER ENTRY''
          AND UPPER (ol.flow_status_code) <> ''CANCELLED''
          AND rctl.sales_order = oh.order_number
          AND rctl.interface_line_attribute6 = TO_CHAR (ol.line_id)
          AND rctl.interface_line_attribute1 = TO_CHAR (oh.order_number)
          --AND xxeis.eis_rs_xxwc_com_util_pkg.get_date_from IS NULL            -- Commented by Mahesh for TMS#20140320-00210 on 10/13/2014
		  --Eis Commented
         /* AND (   xxeis.eis_rs_xxwc_com_util_pkg.get_date_from IS NULL
               OR xxeis.eis_rs_xxwc_com_util_pkg.get_date_to IS NULL) */-- Added by Mahesh for TMS#20140320-00210 on 10/20/2014
		 --and TRUNC (oh.ordered_date) >=	xxeis.eis_rs_xxwc_com_util_pkg.get_order_date_from+0.25
		 --and TRUNC (oh.ordered_date) <=	xxeis.eis_rs_xxwc_com_util_pkg.get_order_date_to +1.25
          AND rctl.customer_trx_id = rct.customer_trx_id
         -- AND fu.user_id = oh.created_by
          AND UPPER (ohh.hold_name(+)) = ''PRICING GUARDRAIL HOLD''
          ';
		  
    -- Process the driving data to below query      
	L_non_invoiced_order_QRY2 VARCHAR2(32000):=
		 'SELECT 
      '||p_process_id||' p_process_id,
          ood.organization_name branch_name,
          mp.organization_code branch,
          rs.name account_manager,
          xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.get_empolyee_name (oh.created_by,
                                                            oh.creation_date)
             created_by, -- fu.user_name                                                             CREATED_BY,
          NULL pricing_responsibility, --xxeis.eis_rs_xxwc_com_util_pkg.get_pricing_resp(oh.created_by) pricing_responsibility,
          DECODE (ohh.hold_name, ''Pricing Guardrail Hold'', ohh.hold_name)
             hold_name, --DECODE(OHH.HOLD_NAME,''Pricing Guardrail Hold'',xxeis.eis_rs_xxwc_com_util_pkg.get_releaseby(OHH.RELEASED_BY,OHH.CREATION_DATE))APPROVED_BY,
          NULL approved_by, --DECODE(OHH.HOLD_NAME,''Pricing Guardrail Hold'',FLVR.MEANING) REL_RES_CODE,
          NULL rel_res_code, --DECODE(OHH.HOLD_NAME,''Pricing Guardrail Hold'',OHH.RELEASE_COMMENT) REL_RES_COMM,
          NULL rel_res_comm,
          hp.party_name customer_name,
          hca.account_number customer_number,
          TO_CHAR (oh.order_number) order_number,
          TRUNC (oh.ordered_date) ordered_date,
          NULL invoice_number,
          NULL invoice_date,
          ott.name order_type,
          CASE
             WHEN    UPPER (flv.meaning) LIKE ''%WALK%IN%''
                  OR UPPER (flv.meaning) LIKE ''%WILL%CALL%''
             THEN
                flv.meaning
             ELSE
                ''Delivery''
          END
             ship_method,
          isr.vendor_name vendor_name,
         isr.vendor_num vendor_number,
       --  xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.xxget_isr_vendor_details(msi.inventory_item_id,msi.organization_id,''VENDORNAME'') vendor_name,
       --  xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.xxget_isr_vendor_details(msi.inventory_item_id,msi.organization_id,''VENDORNUM'') vendor_number,
          msi.concatenated_segments item_number, --ol.ordered_item item_number,
          msi.description item_description,
          DECODE (ol.line_category_code,
                  ''RETURN'', (ol.ordered_quantity * -1),
                  ol.ordered_quantity)
             sales_qty, --NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY) SALES_QTY,
          ol.unit_list_price base_price,
          (  NVL (ol.unit_list_price, 0)
           - xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.get_adj_auto_modifier_amt (
                ol.header_id,
                ol.line_id))
             original_unit_selling_price,
            (  NVL (ol.unit_list_price, 0)
             - xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.get_adj_auto_modifier_amt (
                  ol.header_id,
                  ol.line_id))
          * NVL (
               DECODE (ol.line_category_code,
                       ''RETURN'', (ol.ordered_quantity * -1),
                       ol.ordered_quantity),
               0)
             orginal_extend_price, --(NVL(ol.unit_list_price,0) -xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_amt(ol.header_id,ol.line_id))-NVL(ol.unit_selling_price,0) dollar_lost,
          0 dollar_lost,
          xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.get_adj_auto_modifier_name (
             ol.header_id,
             ol.line_id)
             modifer_number,
          ol.unit_selling_price final_selling_price,
            NVL (ol.unit_selling_price, 0)
          * NVL (
               DECODE (ol.line_category_code,
                       ''RETURN'', (ol.ordered_quantity * -1),
                       ol.ordered_quantity),
               0)
             final_extended_price,
            (NVL (ol.unit_selling_price, 0) - NVL (ol.unit_cost, 0))
          * 100
          / DECODE (NVL (ol.unit_selling_price, 0),
                    0, 1,
                    ol.unit_selling_price)
             final_gm, --((NVL(OL.UNIT_LIST_PRICE,0)   -XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ADJ_AUTO_MODIFIER_AMT(OL.HEADER_ID,OL.LINE_ID)) -NVL(OL.UNIT_COST,0))*100/DECODE(NVL(OL.UNIT_SELLING_PRICE,0),0,1,OL.UNIT_SELLING_PRICE) ORGINIAL_GM,
          0 orginial_gm /*,apps.qp_qp_form_pricing_attr.get_meaning (opa.arithmetic_operator, ''ARITHMETIC_OPERATOR'')
                              application_method*/
                   -- Commented by Mahesh for TMS#20140320-00210 on 10/13/2014
          , --(OL.UNIT_LIST_PRICE -XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ADJ_AUTO_MODIFIER_AMT(OL.HEADER_ID,OL.LINE_ID))-OL.UNIT_SELLING_PRICE DOLLAR_LOST,
          mp.attribute8 district,
          mp.attribute9 region,
          ol.inventory_item_id,
          ol.ship_from_org_id,
          ol.ordered_quantity, --XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LOW_HIGH_F(MSI.CONCATENATED_SEGMENTS,DECODE(OL.LINE_CATEGORY_CODE,''RETURN'',(OL.ORDERED_QUANTITY*-1),OL.ORDERED_QUANTITY)) QTY_TYPE,
          CASE
             WHEN (   (    NVL (
                              DECODE (ol.line_category_code,
                                      ''RETURN'', (ol.ordered_quantity * -1),
                                      ol.ordered_quantity),
                              0) > NVL (TO_NUMBER (xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.GET_LHQTY(msi.inventory_item_id)), 0)
                       AND NVL (TO_NUMBER (xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.GET_LHQTY(msi.inventory_item_id)), 0) != 0)
                   OR (    NVL (TO_NUMBER (xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.GET_LHQTY(msi.inventory_item_id)), 0) = 0
                       AND NVL (
                              DECODE (ol.line_category_code,
                                      ''RETURN'', (ol.ordered_quantity * -1),
                                      ol.ordered_quantity),
                              0) > 3))
             THEN
                ''HIGH''
             ELSE
                ''LOW''
          END
             qty_type,
          NVL (ol.unit_cost, 0) unit_cost,
          NVL (TO_NUMBER (xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.GET_LHQTY(msi.inventory_item_id)), 0) lhqty
		 ,XXEIS.EIS_RS_XXWC_LOW_QTY_PRI_PKG.GET_price_source_type(ol.header_id,ol.line_id) price_source_type --added for version 1.1   
     FROM oe_order_headers oh,
          oe_order_lines ol,
          oe_transaction_types_vl ott,
          fnd_lookup_values flv,
          hz_cust_accounts hca,
          hz_parties hp,
          oe_holds_history_v ohh,
          ra_salesreps rs,
          org_organization_definitions ood,
          mtl_parameters mp,
          mtl_system_items_b_kfv msi --,oe_price_adjustments_v opa                                          -- Commented by Mahesh for TMS#20140320-00210 on 10/13/2014
                                    ,
          --fnd_user fu,
         xxeis.eis_xxwc_po_isr_tab isr
--          (  SELECT inventory_item_id, MAX (cross_reference) cross_reference
--               FROM inv.mtl_cross_references_b
--              WHERE cross_reference_type = ''XXWC_QP_LOW_QTY_TBL''
--           GROUP BY inventory_item_id) lhqty
    WHERE     oh.header_id = ol.header_id
          AND ott.transaction_type_id = oh.order_type_id
          AND flv.lookup_type = ''SHIP_METHOD''
          AND flv.lookup_code = ol.shipping_method_code
          AND hca.cust_account_id = ol.sold_to_org_id
          AND hca.party_id = hp.party_id
          AND ohh.header_id(+) = ol.header_id
          AND ohh.line_id(+) = ol.line_id
          AND rs.salesrep_id(+) = ol.salesrep_id
          AND rs.org_id(+) = ol.org_id
          AND mp.organization_id = ol.ship_from_org_id
          AND msi.organization_id = ol.ship_from_org_id
          AND msi.inventory_item_id = ol.inventory_item_id
          AND msi.organization_id = isr.organization_id(+)
          AND msi.inventory_item_id = isr.inventory_item_id(+)
--           and msi.inventory_item_id=2976137
--          AND ood.organization_id=228
--          AND msi.inventory_item_id = lhqty.inventory_item_id(+)
          --AND xxeis.eis_rs_xxwc_com_util_pkg.get_date_from IS NULL            -- Commented by Mahesh for TMS#20140320-00210 on 10/13/2014
--          AND (   xxeis.eis_rs_xxwc_com_util_pkg.get_date_from IS NULL
--               OR xxeis.eis_rs_xxwc_com_util_pkg.get_date_to IS NULL) -- Added by Mahesh for TMS#20140320-00210 on 10/20/2014
          --AND opa.header_id(+) = ol.header_id                                 -- Commented by Mahesh for TMS#20140320-00210 on 10/13/2014
          --AND (opa.line_id IS NULL OR opa.line_id = ol.line_id)               -- Commented by Mahesh for TMS#20140320-00210 on 10/13/2014
          --AND opa.line_id(+)                                = ol.line_id
        -- and trunc (oh.ordered_date) >=	xxeis.eis_rs_xxwc_com_util_pkg.get_order_date_from+0.25
        -- and TRUNC (oh.ordered_date) <=	xxeis.eis_rs_xxwc_com_util_pkg.get_order_date_to +1.25
          AND UPPER (ol.flow_status_code) <> ''CANCELLED''
          AND ood.organization_id = mp.organization_id
          /*AND opa.list_line_type_code(+) = ''DIS''
          AND opa.adjustment_name IN (''AMOUNT_LINE_DISCOUNT''
                                     ,''NEW PRICE''
                                     ,''PERCENT_LINE_DISCOUNT''
                                     ,''NEW_PRICE_DISCOUNT''
                                     ,''PERCENT_ORDER_DISCOUNT'')*/
          -- Commented by Mahesh for TMS#20140320-00210 on 10/13/2014
             and oh.header_id      =:header_id
          AND EXISTS
                 (SELECT ''1''
                    FROM oe_price_adjustments_v opa
                   WHERE     opa.header_id(+) = ol.header_id
                         AND (opa.line_id IS NULL OR opa.line_id = ol.line_id)
                         AND opa.list_line_type_code(+) = ''DIS''
                         AND opa.adjustment_name IN (''AMOUNT_LINE_DISCOUNT'',
                                                     ''NEW PRICE'',
                                                     ''PERCENT_LINE_DISCOUNT'',
                                                     ''NEW_PRICE_DISCOUNT'',
                                                     ''PERCENT_ORDER_DISCOUNT'')) -- Added by Mahesh for TMS#20140320-00210 on 10/13/2014
--          AND fu.user_id = oh.created_by
          AND UPPER (ohh.hold_name(+)) = ''PRICING GUARDRAIL HOLD''
          AND (   ol.invoice_interface_status_code IS NULL
               OR (NOT EXISTS
                      (SELECT 1
                         FROM ra_customer_trx_lines ril
                        WHERE     TO_CHAR (ol.line_id) =
                                     ril.interface_line_attribute6
                              and ril.interface_line_context = ''ORDER ENTRY'')))';
  
    L_INVOICED_ORDER_QRY VARCHAR2(32000):=' SELECT 
           '||p_process_id||' p_process_id,
          ood.organization_name branch_name,
          mp.organization_code branch,
          rs.name account_manager,
          xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.get_empolyee_name (oh.created_by,
                                                            oh.creation_date)
             created_by, --fu.user_name                                                             CREATED_BY,
          NULL pricing_responsibility, --xxeis.eis_rs_xxwc_com_util_pkg.get_pricing_resp(oh.created_by) pricing_responsibility,
          DECODE (ohh.hold_name, ''Pricing Guardrail Hold'', ohh.hold_name)
             hold_name, -- DECODE(OHH.HOLD_NAME,''Pricing Guardrail Hold'',OHH.RELEASED_BY)          APPROVED_BY,
          --DECODE(OHH.HOLD_NAME,''Pricing Guardrail Hold'',xxeis.eis_rs_xxwc_com_util_pkg.get_releaseby(OHH.RELEASED_BY,OHH.CREATION_DATE))APPROVED_BY,
          NULL approved_by, --DECODE(OHH.HOLD_NAME,''Pricing Guardrail Hold'',FLVR.MEANING) REL_RES_CODE,
          NULL rel_res_code, --DECODE(OHH.HOLD_NAME,''Pricing Guardrail Hold'',OHH.RELEASE_COMMENT) REL_RES_COMM,
          NULL rel_res_comm,
          hp.party_name customer_name,
          hca.account_number customer_number,
          TO_CHAR (oh.order_number) order_number,
          TRUNC (oh.ordered_date) ordered_date,
          rct.trx_number invoice_number,
          TRUNC (rct.trx_date) invoice_date,
          ott.name order_type,
          CASE
             WHEN    UPPER (flv.meaning) LIKE ''%WALK%IN%''
                  OR UPPER (flv.meaning) LIKE ''%WILL%CALL%''
             THEN
                flv.meaning
             ELSE
                ''Delivery''
          END
             ship_method, --DECODE(flv.meaning,''5. Walk In'',''5. Walk In'',''8.  WCD-Will Call'',''8.  WCD-Will Call'', ''0. Will Call'',''0. Will Call'',''Delivery'') SHIP_METHOD,
          --xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id) vendor_name,
          --xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number(msi.inventory_item_id,msi.organization_id) vendor_number,
          isr.vendor_name vendor_name,
          isr.vendor_num vendor_number,
       --  xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.xxget_isr_vendor_details(msi.inventory_item_id,msi.organization_id,''VENDORNAME'') vendor_name,
       --  xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.xxget_isr_vendor_details(msi.inventory_item_id,msi.organization_id,''VENDORNUM'') vendor_number,
          msi.concatenated_segments item_number, --ol.ordered_item item_number,
          msi.description item_description,
          NVL (
             DECODE (ol.line_category_code,
                     ''RETURN'', (ol.ordered_quantity * -1),
                     ol.ordered_quantity),
             0)
             sales_qty,
          ol.unit_list_price base_price,
          (  NVL (ol.unit_list_price, 0)
           - xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.get_adj_auto_modifier_amt (
                ol.header_id,
                ol.line_id))
             original_unit_selling_price,
            (  NVL (ol.unit_list_price, 0)
             - xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.get_adj_auto_modifier_amt (
                  ol.header_id,
                  ol.line_id))
          * NVL (
               DECODE (ol.line_category_code,
                       ''RETURN'', (ol.ordered_quantity * -1),
                       ol.ordered_quantity),
               0)
             orginal_extend_price, --(NVL(ol.unit_list_price,0) -xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_amt(ol.header_id,ol.line_id))-NVL(ol.unit_selling_price,0) dollar_lost,
          0 dollar_lost,
          xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.get_adj_auto_modifier_name (
             ol.header_id,
             ol.line_id)
             modifer_number,
          ol.unit_selling_price final_selling_price,
            NVL (ol.unit_selling_price, 0)
          * NVL (
               DECODE (ol.line_category_code,
                       ''RETURN'', (ol.ordered_quantity * -1),
                       ol.ordered_quantity),
               0)
             final_extended_price,
            (NVL (ol.unit_selling_price, 0) - NVL (ol.unit_cost, 0))
          * 100
          / DECODE (NVL (ol.unit_selling_price, 0),
                    0, 1,
                    ol.unit_selling_price)
             final_gm, --((NVL(OL.UNIT_LIST_PRICE,0)   -XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ADJ_AUTO_MODIFIER_AMT(OL.HEADER_ID,OL.LINE_ID)) -NVL(OL.UNIT_COST,0))*100/DECODE(NVL(OL.UNIT_SELLING_PRICE,0),0,1,OL.UNIT_SELLING_PRICE) ORGINIAL_GM,
          0 orginial_gm /*,apps.qp_qp_form_pricing_attr.get_meaning (opa.arithmetic_operator, ''ARITHMETIC_OPERATOR'')
                              application_method*/
                   -- Commented by Mahesh for TMS#20140320-00210 on 10/13/2014
          , --(OL.UNIT_LIST_PRICE -XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ADJ_AUTO_MODIFIER_AMT(OL.HEADER_ID,OL.LINE_ID))-OL.UNIT_SELLING_PRICE DOLLAR_LOST,
          mp.attribute8 district,
          mp.attribute9 region,
          ol.inventory_item_id,
          ol.ship_from_org_id,
          ol.ordered_quantity, --XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LOW_HIGH_F(MSI.CONCATENATED_SEGMENTS,DECODE(OL.LINE_CATEGORY_CODE,''RETURN'',(OL.ORDERED_QUANTITY*-1),OL.ORDERED_QUANTITY)) QTY_TYPE,
          CASE
             WHEN (   (    NVL (
                              DECODE (ol.line_category_code,
                                      ''RETURN'', (ol.ordered_quantity * -1),
                                      ol.ordered_quantity),
                              0) > NVL (TO_NUMBER (xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.GET_LHQTY(msi.inventory_item_id)), 0)
                       AND NVL (TO_NUMBER (xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.GET_LHQTY(msi.inventory_item_id)), 0) != 0)
                   OR (    NVL (TO_NUMBER (xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.GET_LHQTY(msi.inventory_item_id)), 0) = 0
                       AND NVL (
                              DECODE (ol.line_category_code,
                                      ''RETURN'', (ol.ordered_quantity * -1),
                                      ol.ordered_quantity),
                              0) > 3))
             THEN
                ''HIGH''
             ELSE
                ''LOW''
          END
             qty_type,
          NVL (ol.unit_cost, 0) unit_cost,
          NVL (TO_NUMBER (xxeis.EIS_RS_XXWC_LOW_QTY_PRI_PKG.GET_LHQTY(msi.inventory_item_id)), 0) lhqty
		 ,XXEIS.EIS_RS_XXWC_LOW_QTY_PRI_PKG.GET_price_source_type(ol.header_id,ol.line_id) price_source_type --added for version 1.1   
     FROM oe_order_headers oh,
          oe_order_lines ol,
          oe_transaction_types_vl ott,
          fnd_lookup_values flv,
          hz_cust_accounts hca,
          hz_parties hp,
          oe_holds_history_v ohh,
          ra_salesreps rs,
          org_organization_definitions ood,
          mtl_parameters mp,
          mtl_system_items_b_kfv msi --,oe_price_adjustments_v opa                  -- Commented by Mahesh for TMS#20140320-00210 on 10/13/2014
                                    ,
          ra_customer_trx rct,
          ra_customer_trx_lines rctl,
--          fnd_user fu,
          xxeis.eis_xxwc_po_isr_tab isr
--          (  SELECT inventory_item_id, MAX (cross_reference) cross_reference
--               FROM inv.mtl_cross_references_b
--              WHERE cross_reference_type = ''XXWC_QP_LOW_QTY_TBL''
--           GROUP BY inventory_item_id) lhqty
    --    xxeis_wc_price_adj_v pri_adj
    --  FND_LOOKUP_VALUES FLVR
    WHERE     oh.header_id = ol.header_id
          AND ott.transaction_type_id = oh.order_type_id
          AND flv.lookup_type = ''SHIP_METHOD''
          AND flv.lookup_code = ol.shipping_method_code
          AND hca.cust_account_id = ol.sold_to_org_id
          AND hca.party_id = hp.party_id
          AND ohh.header_id(+) = ol.header_id
          AND ohh.line_id(+) = ol.line_id
          AND rs.salesrep_id(+) = ol.salesrep_id
          AND rs.org_id(+) = ol.org_id
          AND mp.organization_id = ol.ship_from_org_id
          AND msi.organization_id = ol.ship_from_org_id
          AND msi.inventory_item_id = ol.inventory_item_id
          AND msi.organization_id = isr.organization_id(+)
          AND msi.inventory_item_id = isr.inventory_item_id(+)
--           and msi.inventory_item_id=2976137
--          AND ood.organization_id=228
--          AND msi.inventory_item_id = lhqty.inventory_item_id(+)
          --AND opa.header_id(+) = ol.header_id                                 -- Commented by Mahesh for TMS#20140320-00210 on 10/13/2014
          --AND (opa.line_id IS NULL OR opa.line_id = ol.line_id)               -- Commented by Mahesh for TMS#20140320-00210 on 10/13/2014
          --AND opa.line_id(+)          = ol.line_id
          AND ood.organization_id = mp.organization_id
          and rct.CUSTOMER_TRX_ID  = :CUSTOMER_TRX_ID
          and oh.HEADER_ID        = :HEADER_ID
          and ol.LINE_ID          = :LINE_ID
          and rctl.customer_trx_line_id = :customer_trx_line_id
--          AND (   xxeis.eis_rs_xxwc_com_util_pkg.get_date_from IS NOT NULL
--               OR xxeis.eis_rs_xxwc_com_util_pkg.get_date_to IS NOT NULL) -- Added by Mahesh for TMS#20140320-00210 on 10/20/2014
          /*AND rct.creation_date >=
                   TO_DATE (
                      TO_CHAR (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
                              ,xxeis.eis_rs_utility.get_date_format || '' HH24:MI:SS'')
                     ,xxeis.eis_rs_utility.get_date_format || '' HH24:MI:SS'')
                 + 0.25
          AND rct.creation_date <=
                   TO_DATE (
                      TO_CHAR (xxeis.eis_rs_xxwc_com_util_pkg.get_date_to
                              ,xxeis.eis_rs_utility.get_date_format || '' HH24:MI:SS'')
                     ,xxeis.eis_rs_utility.get_date_format || '' HH24:MI:SS'')
                 + 1.25*/
          -- Commented by Mahesh for TMS#20140320-00210 on 10/20/2014
--          AND rct.creation_date >=
--                   TO_DATE (
--                         xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
--                      || '' 00:00:00'',
--                      ''DD-MM-YYYY HH24:MI:SS'')
--                 + 0.25 -- Added by Mahesh for TMS#20140320-00210 on 10/20/2014
--          AND rct.creation_date <=
--                   TO_DATE (
--                         xxeis.eis_rs_xxwc_com_util_pkg.get_date_to
--                      || '' 00:00:00'',
--                      ''DD-MM-YYYY HH24:MI:SS'')
--                 + 1.25 -- Added by Mahesh for TMS#20140320-00210 on 10/20/2014
--and trunc (rct.creation_date) >=	xxeis.eis_rs_xxwc_com_util_pkg.get_date_from+0.25
--and TRUNC (rct.creation_date) <=	xxeis.eis_rs_xxwc_com_util_pkg.get_date_to +1.25
          /*AND opa.list_line_type_code(+) = ''DIS''
          AND opa.adjustment_name IN (''AMOUNT_LINE_DISCOUNT''
                                     ,''NEW PRICE''
                                     ,''PERCENT_LINE_DISCOUNT''
                                     ,''NEW_PRICE_DISCOUNT''
                                     ,''PERCENT_ORDER_DISCOUNT'')*/
          -- Commented by Mahesh for TMS#20140320-00210 on 10/13/2014
          AND EXISTS
                 (SELECT ''1''
                    FROM oe_price_adjustments_v opa
                   WHERE     opa.header_id(+) = oh.header_id
                         AND (opa.line_id IS NULL OR opa.line_id = ol.line_id)
                         AND opa.list_line_type_code(+) = ''DIS''
                         AND opa.adjustment_name IN (''AMOUNT_LINE_DISCOUNT'',
                                                     ''NEW PRICE'',
                                                     ''PERCENT_LINE_DISCOUNT'',
                                                     ''NEW_PRICE_DISCOUNT'',
                                                     ''PERCENT_ORDER_DISCOUNT'')) -- Added by Mahesh for TMS#20140320-00210 on 10/13/2014
          AND rct.interface_header_context = ''ORDER ENTRY''
          AND rct.interface_header_attribute1 = TO_CHAR (oh.order_number)
          AND rctl.interface_line_context = ''ORDER ENTRY''
          AND UPPER (ol.flow_status_code) <> ''CANCELLED''
          AND rctl.sales_order = oh.order_number
          AND rctl.interface_line_attribute6 = TO_CHAR (ol.line_id)
          AND rctl.interface_line_attribute1 = TO_CHAR (oh.order_number)
          AND rctl.customer_trx_id = rct.customer_trx_id
--          AND fu.user_id = oh.created_by
          AND UPPER (ohh.hold_name(+)) = ''PRICING GUARDRAIL HOLD''
          ';

begin

--Dbms_Output.Put_Line('sart time '||To_Char(Sysdate,'DD-MON-YY HH24:MI:SS'));

  IF ((P_ORDER_DATE_FROM  IS  NULL AND P_ORDER_DATE_TO IS NULL) AND (P_INVOICE_DATE_FROM IS NULL AND P_INVOICE_DATE_TO IS NULL))   THEN
    fnd_file.put_line(fnd_file.log,'Please pass either Order Date or Invoice Date parameters');
  RETURN;
  end if;
      
      
  IF P_BRANCH     IS NOT NULL THEN
   IF P_BRANCH ='All'
   then
   LC_WHERE_COND:= LC_WHERE_COND||' and 1=1';
   else 
    LC_WHERE_COND:= LC_WHERE_COND||' and mp.organization_code in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_BRANCH)||' )';
    end if;
  END IF;

  IF p_invoice_date_from      IS NOT NULL THEN
    lc_invoice_date:= lc_invoice_date||' and trunc (rct.creation_date) >= ('||xxeis.eis_rs_utility.get_param_values(p_invoice_date_from)||' )';
  END IF;

  IF p_invoice_date_to      IS NOT NULL THEN
    lc_invoice_date:= lc_invoice_date||' and trunc (rct.creation_date) <= ('||Xxeis.Eis_Rs_Utility.Get_Param_Values(p_invoice_date_to)||' )';
  END IF;		  
	

  IF p_order_date_from      IS NOT NULL THEN
    lc_order_date:= lc_order_date||' and trunc (oh.ordered_date)>=('||xxeis.eis_rs_utility.get_param_values(p_order_date_from)||' )';
--    Dbms_Output.Put_Line('lc_order_date '||p_order_date_from);
  END IF;

  IF p_order_date_to      IS NOT NULL THEN
    lc_order_date:= lc_order_date||' and trunc (oh.ordered_date)<=('||Xxeis.Eis_Rs_Utility.Get_Param_Values(p_order_date_to)||' )';
  END IF;	
-- Dbms_Output.Put_Line('lc_order_date '||lc_order_date);

  IF P_ACCOUNT_MANAGER    IS NOT NULL THEN
    lc_where_cond:= lc_where_cond||' and rs.name in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_Account_Manager)||' )';
  END IF;	

  IF p_Region    IS NOT NULL THEN
    LC_WHERE_COND:= LC_WHERE_COND||' and mp.attribute9 in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_Region)||' )';
  END IF;	
  
    IF p_District    IS NOT NULL THEN
    LC_WHERE_COND:= LC_WHERE_COND||' and mp.attribute8 in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_District)||' )';
  END IF;	
  
  IF p_Item_num    IS NOT NULL THEN
    LC_WHERE_COND:= LC_WHERE_COND||' and msi.concatenated_segments in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_Item_num)||' )';
  END IF;	
  
    IF p_Item_Description    IS NOT NULL THEN
    LC_WHERE_COND:= LC_WHERE_COND||' and msi.description in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_Item_Description)||' )';
  END IF;	
  
   IF P_CUSTOMER_NUMBER    IS NOT NULL THEN
    LC_WHERE_COND:= LC_WHERE_COND||' and HCA.ACCOUNT_NUMBER in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_Customer_Number)||' )';
  END IF;	
  
  IF p_Customer_Name    IS NOT NULL THEN
    LC_WHERE_COND:= LC_WHERE_COND||' and hp.party_name in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_Customer_Name)||' )';
  END IF;	
  
  
   l_ord_low_qty_pri_rec_tbl.DELETE;
   L_HEADER_ID_REC_TBL.DELETE;
   l_customer_trx_id_rec_tbl.delete;
   l_order_id_rec_tbl.delete;
   
     L_non_invoiced_order_QRY1 := L_non_invoiced_order_QRY1||' '||lc_order_date||' '||lc_where_cond;
        
	   L_non_invoiced_order_QRY2 := L_non_invoiced_order_QRY2||' '||lc_order_date||' '||lc_where_cond;
	   
	   L_invoiced_order_QRY 	:= L_invoiced_order_QRY||' '||lc_invoice_date||' '||lc_where_cond;
	   
  
--  dbms_output.put_line('l_header_id_qry'||l_header_id_qry);
  
     open l_header_id_csr for l_header_id_qry;
     loop
     fetch l_header_id_csr bulk collect into l_order_id_rec_tbl limit 10000;
--     dbms_output.put_line('l_header_id_qry'||l_order_id_rec_tbl);
      if l_order_id_rec_tbl.COUNT  >= 1  then
                  FORALL J IN 1 .. l_order_id_rec_tbl.COUNT 
                     
                     INSERT INTO xxeis.EIS_XXWC_LOW_QTY_HDR_TBL
                          values l_order_id_rec_tbl(j);    
             
       end if; 
    	   exit when l_header_id_csr%notfound;
      end loop;
      --commit;  --Commented for version 1.2
     close l_header_id_csr;
     

      
--Removing duplicates from header table EIS_XXWC_LOW_QTY_HDR_TBL
BEGIN
  l_delete_stmt:= 'DELETE FROM xxeis.EIS_XXWC_LOW_QTY_HDR_TBL
   WHERE ROWID IN (
              SELECT rid
                FROM (SELECT ROWID RID,
                             ROW_NUMBER () OVER (PARTITION BY HEADER_ID ORDER BY ROWID) RN
                        FROM xxeis.EIS_XXWC_LOW_QTY_HDR_TBL where process_id='||P_PROCESS_ID ||')
               WHERE RN <> 1)
   AND PROCESS_ID ='||P_PROCESS_ID ||'';
   
   EXECUTE IMMEDIATE L_DELETE_STMT;
   
--   dbms_output.put_line('l_delete_statement is' ||l_delete_stmt);
  -- commit;  --Commented for version 1.2
        EXCEPTION WHEN OTHERS THEN
     FND_FILE.PUT_LINE(FND_FILE.LOG,'Removing Duplicates '||SQLCODE||SQLERRM);
--dbms_output.PUT_LINE('Removing Duplicates '||SQLCODE||SQLERRM);

END; 
 
   
  
   OPEN l_header_id_csr FOR l_order_id_qry;
    FETCH l_header_id_csr BULK COLLECT INTO l_header_id_rec_tbl;
  CLOSE l_header_id_csr; 
  
     OPEN L_CUSTOMER_TRX_ID_CSR FOR l_customer_trx_id_qry;
    FETCH L_CUSTOMER_TRX_ID_CSR BULK COLLECT INTO L_customer_trx_id_REC_TBL;
  CLOSE L_CUSTOMER_TRX_ID_CSR; 
  

  
--   Dbms_Output.Put_Line('l_header_id_rec_tbl count '||l_header_id_rec_tbl.count);
   
  IF ((P_ORDER_DATE_FROM  IS NOT NULL AND P_ORDER_DATE_TO   IS NOT NULL) AND ( P_INVOICE_DATE_FROM IS NULL AND P_INVOICE_DATE_TO IS NULL))
  THEN  
--   FND_FILE.PUT_LINE(FND_FILE.LOG,'l_header_id_rec_tbl '||L_HEADER_ID_REC_TBL.count);
--  Fnd_File.Put_Line(Fnd_File.Log,'L_customer_trx_id_REC_TBL '||L_customer_trx_id_REC_TBL.count);
    if l_header_id_rec_tbl.count>0
  then
    FOR I IN 1..l_header_id_rec_tbl.COUNT
  LOOP
  OPEN l_non_invoiced_order_csr1 FOR L_non_invoiced_order_QRY1 USING l_header_id_rec_tbl(i).header_id;
--  Dbms_Output.Put_Line('L_non_invoiced_order_QRY1 count '||l_ord_low_qty_pri_rec_tbl.count);
  loop
    FETCH l_non_invoiced_order_csr1 BULK COLLECT INTO l_ord_low_qty_pri_rec_tbl limit 10000;
	  if l_ord_low_qty_pri_rec_tbl.COUNT>0
  Then   
      FORALL J IN 1..l_ord_low_qty_pri_rec_tbl.COUNT 
  Insert Into Xxeis.XXWC_LOW_QTY_MAN_PRIC_TBL Values l_ord_low_qty_pri_rec_tbl(J);
      END IF;
	   exit WHEN l_non_invoiced_order_csr1%notfound;
    END LOOP;
  	--commit; --Commented for version 1.2
  CLOSE l_non_invoiced_order_csr1;
  	END loop;
 	end if;
  
  
      if l_header_id_rec_tbl.count>0
  then
    FOR I IN 1..l_header_id_rec_tbl.COUNT
  LOOP
    OPEN l_non_invoiced_order_csr2 FOR L_non_invoiced_order_QRY2 USING l_header_id_rec_tbl(i).header_id;
	loop
    FETCH l_non_invoiced_order_csr2 BULK COLLECT INTO l_ord_low_qty_pri_rec_tbl limit 10000;
	  if l_ord_low_qty_pri_rec_tbl.COUNT>0
  Then   
      FORALL J IN 1..l_ord_low_qty_pri_rec_tbl.COUNT 
  Insert Into Xxeis.XXWC_LOW_QTY_MAN_PRIC_TBL Values l_ord_low_qty_pri_rec_tbl(J);
      END IF;
	   exit when l_non_invoiced_order_csr2%notfound;
    END LOOP;
 -- commit; --Commented for version 1.2
  CLOSE l_non_invoiced_order_csr2;
  	END loop;
  END IF;

  END IF;
  
--  Dbms_Output.Put_Line('L_non_invoiced_order_QRY2 '||L_non_invoiced_order_QRY2);
--  Dbms_Output.Put_Line('L_non_invoiced_order_QRY1 '||L_non_invoiced_order_QRY1);
--    Dbms_Output.Put_Line('l_customer_trx_id_qry '||l_customer_trx_id_qry);
  
  IF ( (p_invoice_date_from  IS NOT NULL and p_invoice_date_to is not null ) and (p_order_date_from is null and  p_order_date_to  IS  NULL)) THEN
    
   IF L_customer_trx_id_REC_TBL.COUNT>0
  then
    FOR I IN 1..L_customer_trx_id_REC_TBL.COUNT
  LOOP
  
   OPEN L_INVOICED_ORDER_CSR FOR L_INVOICED_ORDER_QRY  USING L_CUSTOMER_TRX_ID_REC_TBL(I).CUSTOMER_TRX_ID,L_CUSTOMER_TRX_ID_REC_TBL(I).header_id,
   L_CUSTOMER_TRX_ID_REC_TBL(I).LINE_ID,L_CUSTOMER_TRX_ID_REC_TBL(I).CUSTOMER_TRX_LINE_ID;
   
   loop
    FETCH l_invoiced_order_csr BULK COLLECT INTO l_ord_low_qty_pri_rec_tbl;
		  if l_ord_low_qty_pri_rec_tbl.COUNT>0
  Then   
      FORALL J IN 1..l_ord_low_qty_pri_rec_tbl.COUNT 
  Insert Into Xxeis.XXWC_LOW_QTY_MAN_PRIC_TBL Values l_ord_low_qty_pri_rec_tbl(J);
      END IF;
	   exit when l_invoiced_order_csr%notfound;
    END LOOP;
      -- commit; --Commented for version 1.2
  CLOSE L_INVOICED_ORDER_CSR;
  	END loop;
  END IF;  
  end if;
  
  IF ( (p_invoice_date_from  IS NOT NULL and p_invoice_date_to is not null ) and (p_order_date_from is not null and  p_order_date_to  IS not NULL)) THEN
  if l_header_id_rec_tbl.count>0
  then
    FOR I IN 1..l_header_id_rec_tbl.COUNT
  LOOP
  OPEN l_non_invoiced_order_csr1 FOR L_non_invoiced_order_QRY1 USING l_header_id_rec_tbl(i).header_id;
  loop
    FETCH l_non_invoiced_order_csr1 BULK COLLECT INTO l_ord_low_qty_pri_rec_tbl limit 10000;
	  if l_ord_low_qty_pri_rec_tbl.COUNT>0
  Then   
      FORALL J IN 1..l_ord_low_qty_pri_rec_tbl.COUNT 
  Insert Into Xxeis.XXWC_LOW_QTY_MAN_PRIC_TBL Values l_ord_low_qty_pri_rec_tbl(J);
      END IF;
--        Dbms_Output.Put_Line('l_ord_low_qty_pri_rec_tbl '||l_ord_low_qty_pri_rec_tbl.count);
	   exit WHEN l_non_invoiced_order_csr1%notfound;
    END LOOP;
  	--commit; --Commented for version 1.2
  CLOSE l_non_invoiced_order_csr1;
  	END loop;
  	end if;
  
      if l_header_id_rec_tbl.count>0
  then
    FOR I IN 1..l_header_id_rec_tbl.COUNT
  LOOP
    OPEN l_non_invoiced_order_csr2 FOR L_non_invoiced_order_QRY2 USING l_header_id_rec_tbl(i).header_id;
	loop
    FETCH l_non_invoiced_order_csr2 BULK COLLECT INTO l_ord_low_qty_pri_rec_tbl limit 10000;
	  if l_ord_low_qty_pri_rec_tbl.COUNT>0
  Then   
      FORALL J IN 1..l_ord_low_qty_pri_rec_tbl.COUNT 
  Insert Into Xxeis.XXWC_LOW_QTY_MAN_PRIC_TBL Values l_ord_low_qty_pri_rec_tbl(J);
      END IF;
	   exit when l_non_invoiced_order_csr2%notfound;
    END LOOP;
    -- commit; --Commented for version 1.2
    CLOSE l_non_invoiced_order_csr2;
    	END loop;
   END IF;
   
   IF L_customer_trx_id_REC_TBL.COUNT>0
  then
    FOR I IN 1..L_customer_trx_id_REC_TBL.COUNT
  LOOP
  
   OPEN L_INVOICED_ORDER_CSR FOR L_INVOICED_ORDER_QRY  USING L_CUSTOMER_TRX_ID_REC_TBL(I).CUSTOMER_TRX_ID,L_CUSTOMER_TRX_ID_REC_TBL(I).header_id,
   L_CUSTOMER_TRX_ID_REC_TBL(I).LINE_ID,L_CUSTOMER_TRX_ID_REC_TBL(I).CUSTOMER_TRX_LINE_ID;
   
   loop
    FETCH l_invoiced_order_csr BULK COLLECT INTO l_ord_low_qty_pri_rec_tbl;
		  if l_ord_low_qty_pri_rec_tbl.COUNT>0
  Then   
      FORALL J IN 1..l_ord_low_qty_pri_rec_tbl.COUNT 
  Insert Into Xxeis.XXWC_LOW_QTY_MAN_PRIC_TBL Values l_ord_low_qty_pri_rec_tbl(J);
      END IF;
	   exit when l_invoiced_order_csr%notfound;
    END LOOP;
      -- commit; --Commented for version 1.2
  CLOSE L_INVOICED_ORDER_CSR;
  	END loop;
  END IF;  
  END IF;
  
--  Dbms_Output.Put_Line('end time '||To_Char(Sysdate,'DD-MON-YY HH24:MI:SS'));
  
    EXCEPTION
      when OTHERS
      THEN
	  Fnd_File.Put_Line(FND_FILE.log,'The Error is..'||SQLERRM||SQLCODE);
END low_qty_pri_par;

/* --Commented code for version 1.2		    
PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER)
AS
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete 
--//						all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  	04/12/2016   Initial Build --TMS#20150928-00191  --Performance Tuning
--//============================================================================   
  BEGIN  
  delete from xxeis.XXWC_LOW_QTY_MAN_PRIC_TBL where process_id=p_process_id;
  delete from xxeis.EIS_XXWC_LOW_QTY_HDR_TBL where process_id=p_process_id;  
  
  COMMIT;
END ;    
*/ --Commented code for version 1.2
    
end  eis_rs_xxwc_low_qty_pri_pkg;
/
