create or replace 
package body  XXEIS.EIS_XXWC_DATA_MANG_RPT_PKG as
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue 
--//
--// Object Usage 				:: This Object Referred by "Data Management Audit Report"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_DATA_MANG_RPT_PKG
--//
--// Object Type         		:: Package Body
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160418-00110 --Performance Tuning
--// 1.1     Siva     	08/25/2016   TMS#20160824-00003
--// 1.2     Siva  		15-Mar-2017    TMS#20170224-00065 
--//============================================================================

 FUNCTION website_item_hierarchy(P_INVENTORY_ITEM_ID in NUMBER,P_organization_id in number,p_type in varchar2) RETURN varchar2
IS
--//============================================================================
--//
--// Object Name         :: WEBSITE_ITEM_HIERARCHY  
--//
--// Object Usage 		   :: This Object Referred by "Data Management Audit Report"
--//
--// Object Type         :: Function
--//
--// Object Description  ::  This  Function is created based on the EIS_XXWC_DATA_MGMT_AUDIT_V View to populate
--//                          the website_item and wc_web_hierarchy
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160418-00110 --Performance Tuning
--//============================================================================		
L_HASH_INDEX varchar2(100);
l_sql varchar2(32000);
begin
G_WEBSITE_ITEM := NULL;
--G_WEBSITE_hierarchy:=null;
l_sql :='SELECT DECODE(mcv.segment2,
                          NULL, mcv.segment1,
                          mcv.segment1 || ''.'' || mcv.segment2)
             FROM mtl_item_categories mic,
                  mtl_categories_vl mcv,
                  mtl_category_sets mcs
            WHERE     mic.inventory_item_id = :1
                  AND mic.organization_id = :2
                  AND mic.category_id = mcv.category_id
                  AND mic.category_set_id = mcs.category_set_id
                  AND mcs.structure_id = mcv.structure_id
                  and MCS.CATEGORY_SET_NAME = :3';
    BEGIN
        l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID||'-'||P_TYPE;
        G_WEBSITE_ITEM := G_WEBSITE_ITEM_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO G_WEBSITE_ITEM USING P_INVENTORY_ITEM_ID,P_ORGANIZATION_ID,P_TYPE;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_WEBSITE_ITEM :=null;
    WHEN OTHERS THEN
    G_WEBSITE_ITEM :=null;
    END;      
                      l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID||'-'||P_TYPE;
                       G_WEBSITE_ITEM_VLDN_TBL(L_HASH_INDEX) := G_WEBSITE_ITEM;
    END;
     RETURN  G_WEBSITE_ITEM;
    EXCEPTION when OTHERS then
      G_WEBSITE_ITEM:=null;
      RETURN  G_WEBSITE_ITEM;

end website_item_hierarchy;

FUNCTION coo_meaning(p_emsy varchar2) RETURN varchar2
IS
--//============================================================================
--//
--// Object Name         :: COO_MEANING  
--//
--// Object Usage 		   :: This Object Referred by "Data Management Audit Report"
--//
--// Object Type         :: Function
--//
--// Object Description  ::  This  Function is created based on the EIS_XXWC_DATA_MGMT_AUDIT_V View to populate
--//                          the country_of_origin values.
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160418-00110 --Performance Tuning
--// 1.1     Siva     	08/25/2016   TMS#20160824-00003
--//============================================================================	
L_HASH_INDEX varchar2(100);
l_sql varchar2(32000);
begin
G_COO_MEANING := NULL;
--G_WEBSITE_hierarchy:=null;
l_sql :='SELECT  meaning
             FROM apps.FND_LOOKUP_VALUES
            WHERE lookup_type = ''XXWC_TERRITORIES''
            and lookup_code = :1'; -- changed for version 1.1
    begin
        l_hash_index:=p_emsy;
        G_COO_MEANING := G_COO_MEANING_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO G_COO_MEANING USING p_emsy;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_COO_MEANING :=null;
    WHEN OTHERS THEN
    G_COO_MEANING :=null;
    end;      
                      l_hash_index:=p_emsy;
                       G_COO_MEANING_VLDN_TBL(L_HASH_INDEX) := G_COO_MEANING;
    END;
     RETURN  G_COO_MEANING;
    EXCEPTION when OTHERS then
      G_COO_MEANING:=null;
      RETURN  G_COO_MEANING;

end coo_meaning;


FUNCTION GET_INV_CAT_CLASS (P_INVENTORY_ITEM_ID in NUMBER,P_organization_id in number) RETURN VARCHAR2
IS
--//============================================================================
--//
--// Object Name         :: GET_INV_CAT_CLASS  
--//
--// Object Usage 		   :: This Object Referred by "Data Management Audit Report"
--//
--// Object Type         :: Function
--//
--// Object Description  ::  This  Function is created based on the EIS_XXWC_DATA_MGMT_AUDIT_V View to populate
--//                          the INVENTORY_CATEGORY values.
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160418-00110 --Performance Tuning
--// 1.1     Siva     	08/25/2016   TMS#20160824-00003
--//============================================================================	
L_HASH_INDEX varchar2(100);
l_sql varchar2(32000);
begin
G_CONCATENATED_SEGMENTS := NULL;
l_sql :=' SELECT Mcv.concatenated_segments
            from  Mtl_Categories_Kfv Mcv,
                  MTL_CATEGORY_SETS MCS,
                  Mtl_Item_Categories Mic
            Where   Mcs.Category_Set_Name      = ''Inventory Category''
          And Mcs.Structure_Id                 = Mcv.Structure_Id
          And Mic.Inventory_Item_Id            = :1
          And Mic.Organization_Id              = :2
          AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
          And Mic.Category_Id                  = Mcv.Category_Id';
    begin
        l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID; -- added for version 1.1
        G_CONCATENATED_SEGMENTS := G_cat_class_VLDN_TBL(l_hash_index);
    exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO G_CONCATENATED_SEGMENTS USING P_INVENTORY_ITEM_ID,P_organization_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_CONCATENATED_SEGMENTS :=null;
    WHEN OTHERS THEN
    G_CONCATENATED_SEGMENTS :=null;
    END;     
    
                       L_HASH_INDEX:= P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID;  -- added for version 1.1
                       G_CAT_CLASS_VLDN_TBL(L_HASH_INDEX) := G_CONCATENATED_SEGMENTS;
                        
    end;
    RETURN  G_CONCATENATED_SEGMENTS;
    EXCEPTION WHEN OTHERS THEN
      G_CONCATENATED_SEGMENTS:=null;
      RETURN  G_CONCATENATED_SEGMENTS;

END GET_INV_CAT_CLASS;


FUNCTION get_inv_cat_class_desc(P_INVENTORY_ITEM_ID in NUMBER,P_organization_id in number,P_request_type in varchar2)
 RETURN varchar2
IS
--//============================================================================
--//
--// Object Name         :: GET_INV_CAT_CLASS_DESC  
--//
--// Object Usage 		   :: This Object Referred by "Data Management Audit Report"
--//
--// Object Type         :: Function
--//
--// Object Description  ::  This  Function is created based on the EIS_XXWC_DATA_MGMT_AUDIT_V View to populate
--//                          the INVENTORY_CATEGORY_DESCRIPTION values.
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  				04/12/2016   Initial Build --TMS#20160418-00110 --Performance Tuning
--// 1.1     Siva     			08/25/2016   TMS#20160824-00003
--//============================================================================	
L_HASH_INDEX varchar2(200);
l_sql varchar2(32000);
begin
g_inv_cat_class_desc := null;
g_flex_enabled_flag  := null;
g_flex_summary_flag  := NULL;
l_sql :='SELECT flex_val.description,flex_val.enabled_flag,flex_val.summary_flag
        FROM mtl_categories_kfv mcv,
             mtl_category_sets mcs,
             mtl_item_categories mic,
             fnd_flex_values_vl flex_val,
             fnd_flex_value_sets flex_name
       WHERE     mcs.category_set_name = ''Inventory Category''
             AND mcs.structure_id = mcv.structure_id
             AND mic.category_set_id = mcs.category_set_id
             AND mic.category_id = mcv.category_id
             AND flex_value = mcv.segment2
             AND mic.inventory_item_id = :1
             AND mic.organization_id = :2
             AND flex_name.flex_value_set_id = flex_val.flex_value_set_id
             AND flex_name.flex_value_set_name = ''XXWC_CATEGORY_CLASS''';
    BEGIN
        L_HASH_INDEX:=P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID; -- Changed for version 1.1
        if P_request_type='DESCRIPTION' then
           G_INV_CAT_CLASS_DESC := G_INV_CAT_CLASS_DESC_VLDN_TBL(L_HASH_INDEX);
        ELSif P_request_type='ENABLED_FLAG' then
            g_flex_enabled_flag := G_flex_enabled_flag_VLDN_TBL(L_HASH_INDEX);
        ELSif P_request_type='SUMMARY_FLAG' then
            g_flex_summary_flag := G_flex_summary_flag_VLDN_TBL(L_HASH_INDEX);
        END IF;
		
        
 exception
    when no_data_found
    THEN
    BEGIN      
    EXECUTE IMMEDIATE L_SQL INTO G_INV_CAT_CLASS_DESC,g_flex_enabled_flag,g_flex_summary_flag USING P_INVENTORY_ITEM_ID,P_ORGANIZATION_ID; -- Changed for version 1.1
    EXCEPTION WHEN NO_DATA_FOUND THEN
	if P_request_type='DESCRIPTION' then
    G_INV_CAT_CLASS_DESC := null;
	elsif P_request_type='ENABLED_FLAG' then
    g_flex_enabled_flag := null;
	elsif P_request_type='SUMMARY_FLAG' then
    g_flex_summary_flag := null;
	end if;
    end;
    
		l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID; -- Changed for version 1.1
			G_INV_CAT_CLASS_DESC_VLDN_TBL(L_HASH_INDEX) := G_INV_CAT_CLASS_DESC;
      		G_flex_enabled_flag_VLDN_TBL(L_HASH_INDEX)  := g_flex_enabled_flag; 
			G_flex_summary_flag_VLDN_TBL(L_HASH_INDEX)  := g_flex_summary_flag;
    END;
     	if P_request_type='DESCRIPTION' then
    return  G_INV_CAT_CLASS_DESC ;
	elsif P_request_type='ENABLED_FLAG' then
    return  g_flex_enabled_flag ;
	elsif P_request_type='SUMMARY_FLAG' then
   return  g_flex_summary_flag ;
   end if;
    EXCEPTION when OTHERS then
  	if P_request_type='DESCRIPTION' then
    G_INV_CAT_CLASS_DESC := null;
	return  G_INV_CAT_CLASS_DESC ;
	elsif P_request_type='ENABLED_FLAG' then
    g_flex_enabled_flag := null;
	return  g_flex_enabled_flag ;
	elsif P_request_type='SUMMARY_FLAG' then
    g_flex_summary_flag := null;
	return  g_flex_summary_flag ;
	end if;

END GET_INV_CAT_CLASS_DESC;

FUNCTION get_flex_val_start_end_date(P_INVENTORY_ITEM_ID in NUMBER,P_organization_id in number,P_request_type in varchar2)
 RETURN date
IS
--//============================================================================
--//
--// Object Name         :: get_flex_val_start_end_date  
--//
--// Object Usage 		   :: This Object Referred by "Data Management Audit Report"
--//
--// Object Type         :: Function
--//
--// Object Description  ::  This  Function is created based on the EIS_XXWC_DATA_MGMT_AUDIT_V View to populate
--//                          the INVENTORY_CATEGORY_DESCRIPTION values.
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  			04/12/2016   Initial Build --TMS#20160418-00110 --Performance Tuning
--// 1.1     Siva     			08/25/2016   TMS#20160824-00003
--//============================================================================	
L_HASH_INDEX varchar2(100);
l_sql varchar2(32000);
begin
G_FLEX_START_DATE := NULL;
G_FLEX_END_DATE   := NULL;

l_sql :='SELECT flex_val.start_date_active,flex_val.end_date_Active 
        FROM mtl_categories_kfv mcv,
             mtl_category_sets mcs,
             mtl_item_categories mic,
             fnd_flex_values_vl flex_val,
             fnd_flex_value_sets flex_name
       WHERE     mcs.category_set_name = ''Inventory Category''
             AND mcs.structure_id = mcv.structure_id
             AND mic.category_set_id = mcs.category_set_id
             AND mic.category_id = mcv.category_id
             AND flex_value = mcv.segment2
             AND mic.inventory_item_id = :1
             AND mic.organization_id = :2
             AND flex_name.flex_value_set_id = flex_val.flex_value_set_id
             AND flex_name.flex_value_set_name = ''XXWC_CATEGORY_CLASS''';
    BEGIN
             L_HASH_INDEX:=P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID; --Changed for version 1.1 
	    	if P_request_type	='START_DATE' then
           G_FLEX_START_DATE := G_FLEX_START_DATE_VLDN_TBL(L_HASH_INDEX);
        ELSif P_request_type='END_DATE' then
           G_FLEX_END_DATE 	 := G_FLEX_END_DATE_VLDN_TBL(L_HASH_INDEX);
        END IF;
 exception
    when no_data_found
    THEN
    BEGIN      
    EXECUTE IMMEDIATE L_SQL INTO G_FLEX_START_DATE,G_FLEX_END_DATE USING P_INVENTORY_ITEM_ID,P_ORGANIZATION_ID; --Changed for version 1.1 
    EXCEPTION WHEN NO_DATA_FOUND THEN
	if P_request_type='START_DATE' then
    G_FLEX_START_DATE := null;
	elsif P_request_type='END_DATE' then
    G_FLEX_END_DATE := null;
	end if;
    end;
    
		l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID; --Changed for version 1.1 
			G_FLEX_START_DATE_VLDN_TBL(L_HASH_INDEX) := G_FLEX_START_DATE;
		    G_FLEX_END_DATE_VLDN_TBL(L_HASH_INDEX)  := G_FLEX_END_DATE;
			
    END;
     	if P_request_type='START_DATE' then
    return  G_FLEX_START_DATE ;
	elsif P_request_type='END_DATE' then
    return  G_FLEX_END_DATE ;
   end if;
    EXCEPTION when OTHERS then
  	if P_request_type='START_DATE' then
    G_FLEX_START_DATE := null;
	return  G_FLEX_START_DATE ;
	elsif P_request_type='END_DATE' then
    G_FLEX_END_DATE := null;
	return  G_FLEX_END_DATE ;
	end if;

END get_flex_val_start_end_date;






procedure GET_DATA_MANG_DTLS (P_PROCESS_ID                 IN NUMBER,
                              P_ITEM_LIST                  IN VARCHAR2,
                              P_MASTER_VENDOR_LIST         IN VARCHAR2,
                              P_ORGANIZATION_LIST 	       IN VARCHAR2,
                              P_SOURCE_LIST 				       IN VARCHAR2,
                              P_SHORT_DESC 			           IN VARCHAR2,
                              P_Item_Number                IN VARCHAR2,
                              P_Master_Vendor_Number       IN VARCHAR2,
                              P_Source 	                   IN VARCHAR2,
                              P_Organization				       IN VARCHAR2,
                              P_Item_Status 			         IN VARCHAR2,
                              P_User_Item_Type             IN VARCHAR2,
                              P_Website_Item               IN VARCHAR2,
                              P_Master_Vendor_Name		     IN VARCHAR2
                            ) as
--//============================================================================
--//
--// Object Name         :: GET_DATA_MANG_DTLS  
--//
--// Object Usage 		 :: This Object Referred by "Data Management Audit Report"
--//
--// Object Type         :: Procedure
--//
--// Object Description  ::  This  procedure is created based on the EIS_XXWC_DATA_MGMT_AUDIT_V View.Due to the 
--// 						 performance issue we have converted the view to trigger based report.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160418-00110  --Performance Tuning
--// 1.1     Siva     	08/25/2016   TMS#20160824-00003
--//============================================================================							
                            
l_cust_sql              VARCHAR2(32000);
l_accruals_sql          VARCHAR2(32000);
l_purchase_sql          VARCHAR2(32000);
l_condition_str         VARCHAR2(32000);
l_condition_str1        VARCHAR2(32000);
l_agreement_year        NUMBER;
l_period_id             NUMBER;
l_mvid                  VARCHAR2(150);
l_cust_id               NUMBER;
l_vendor                VARCHAR2(150);
l_lob                   VARCHAR2(150) ;
l_bu                    VARCHAR2(150);
l_accrual_purchases     NUMBER;
l_rebate                NUMBER;
l_offer_name            VARCHAR2(240) ;
l_coop                  NUMBER;
l_total                 number;
l_count                 NUMBER;
l_agreement_year_param  VARCHAR2(32000);
l_lob_param             VARCHAR2(32000);
l_vendor_param          VARCHAR2(32000);
l_bu_parmp              VARCHAR2(32000);
l_mvid_parmp            VARCHAR2(32000);
l_lob_paramp            VARCHAR2(32000);
l_agreement_year_paramp VARCHAR2(32000);
l_party_name            VARCHAR2(240);
l_party_id              NUMBER;
l_process_id            NUMBER;
l_mvid_parm             VARCHAR2(32000);
L_BU_PARM               VARCHAR2(32000);
L_FROM_CONDITION             VARCHAR2(32000);
L_WHERE_MASTER_VENDOR_LIST   VARCHAR2(32000);
L_WHERE_ITEM_LIST            VARCHAR2(32000);
L_WHERE_ORGANIZATION_LIST    VARCHAR2(32000);
L_WHERE_SOURCE_LIST          VARCHAR2(32000);
L_WHERE_MASTER_VENDOR_NAME   VARCHAR2(1000);
L_WHERE_WEBSITE_ITEM         VARCHAR2(1000);
L_WHERE_USER_ITEM_TYPE       VARCHAR2(1000);
L_WHERE_ITEM_STATUS          VARCHAR2(1000);
L_WHERE_ORGANIZATION         VARCHAR2(1000);
L_WHERE_SOURCE               VARCHAR2(1000);
L_WHERE_MASTER_VENDOR_NUMBER VARCHAR2(1000);
L_WHERE_ITEM_NUMBER          VARCHAR2(1000);
L_SHORT_DESC                 VARCHAR2(1000);
L_DATA_SQL VARCHAR2(32000);


type DATA_TABLE_REC is RECORD
( 
PROCESS_ID                              number,
ITEM                                    VARCHAR2(40)   ,
SHORT_DESCRIPTION                       varchar2(240)  ,
LONG_DESCRIPTION                        varchar2(4000) ,
UOM                                     VARCHAR2(25)   ,
WEIGHT                                  NUMBER         ,
WEIGHT_UOM_CODE                         VARCHAR2(25)   ,
SHELF_LIFE_DAYS                         NUMBER         ,
AVP_CODE                                VARCHAR2(240)  ,
COUNTRY_OF_ORIGIN                       VARCHAR2(80)   ,
MASTER_VENDOR_NUMBER                    VARCHAR2(150)  ,
SOURCE                                  VARCHAR2(240)  ,
SOURCING_VENDOR_NUMBER                  VARCHAR2(240)  ,
SOURCING_VENDOR_NAME                    varchar2(240)  ,
ORG                                     varchar2(3)    ,
ITEM_STATUS                             varchar2(10)   ,
USER_ITEM_TYPE                          VARCHAR2(80)   ,
ENGINEERING_ITEM_FLAG                   varchar2(3)    ,
ORG_DESCRIPTION                         VARCHAR2(240)  ,
DISTRICT                                VARCHAR2(150)  ,
REGION                                  VARCHAR2(150)  ,
MFG_PART_NUMBER                         VARCHAR2(765)  ,
FIXED_LOT_MULTIPLIER                    NUMBER         ,
LIST_PRICE_PER_UNIT                     NUMBER         ,
AVERAGE_COST                            NUMBER         ,
BPA_COST                                NUMBER         ,
BPA_NUMBER                              VARCHAR2(240)  ,
INVENTORY_CATEGORY                      VARCHAR2(4000) ,
INVENTORY_CATEGORY_DESCRIPTION          VARCHAR2(4000) ,
WEBSITE_ITEM                            VARCHAR2(81)   ,
WC_WEB_HIERARCHY                        VARCHAR2(81)   ,
BRAND                                   VARCHAR2(150)  ,
PRODUCT_LINE                            VARCHAR2(150)  ,
WEB_MFG_PART_NUMBER                     VARCHAR2(150)  ,
HAZMAT_FLAG                             VARCHAR2(150)  ,
BULKY_ITEM_FLAG                         VARCHAR2(150)  ,
CYBERSOURCE_TAX_CODE                    VARCHAR2(150)  ,
FULLSIZE_IMAGE_1                        VARCHAR2(150)  ,
THUMBNAIL_IMAGE_1                       VARCHAR2(150)  ,
WEB_SEQUENCE                            NUMBER         ,
WEB_SHORT_DESCRIPTION                   VARCHAR2(150)  ,
WEB_KEYWORDS                            VARCHAR2(1000) ,
WEB_LONG_DESCRIPTION                    VARCHAR2(1000) ,
ITEM_CREATION_DATE                      DATE           ,
COGS_ACCOUNT                            VARCHAR2(4000) ,
SALES_ACCOUNT                           varchar2(4000) ,
EXPENSE_ACCOUNT                         varchar2(4000) ,
INVENTORY_ITEM_ID                       number ,        
ITEM_LEVEL                              VARCHAR2(150)  ,
FULFILLMENT                             varchar2(150)  ,
RETAIL_SELLING_UOM                      VARCHAR2(150)  ,
CONFLICT_MINERALS                       varchar2(150) , 
xpar                                    varchar2(40) ,  
created_by                              varchar2(240),
enabled_flag                            varchar(40), 
summary_flag                            varchar(40), 
start_date_active                       date,
END_DATE_ACTIVE                         DATE
);



    L_REF_CURSOR1                 CURSOR_TYPE4;
    L_REF_CURSOR2                 CURSOR_TYPE4;
    L_REF_CURSOR3                 CURSOR_TYPE4;
    

  type data_table_rec_tab is table of data_table_rec;  
       data_table_tab data_table_rec_tab  :=  data_table_rec_tab();


l_counter number;
a_counter number;
l_test    varchar2(240) ;
l_testing varchar2(240) ;
pr_vendor varchar2(240) ;

BEGIN
  DATA_TABLE_TAB.DELETE;
  
  IF (P_ITEM_LIST  IS NOT NULL OR
                              P_MASTER_VENDOR_LIST   IS NOT NULL OR
                              P_ORGANIZATION_LIST 	 IS NOT NULL OR
                              P_SOURCE_LIST 	 is not null)
  then         
        --DBMS_OUTPUT.PUT_LINE('P_ITEM_LIST');
  if P_ITEM_LIST is not null then  
        L_WHERE_ITEM_LIST:= L_WHERE_ITEM_LIST||' and (('||xxeis.eis_rs_utility.get_param_values(P_ITEM_LIST)||' ) is null or exists (select 1  from
                         XXEIS.EIS_XXWC_PARAM_PARSE_LIST PARM
                         where PARM.LIST_NAME in ('||xxeis.eis_rs_utility.get_param_values(P_ITEM_LIST)||' )
                            and upper(PARM.LIST_TYPE) =''ITEM''
                           and PARM.PROCESS_ID = '||p_process_id||'
                           AND parm.list_value= msi.segment1))
        ';
          XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(p_process_id,(xxeis.eis_rs_utility.get_param_values(P_ITEM_LIST) ),'Item');
          else
         L_WHERE_ITEM_LIST:=' and 1=1';
      end if;   
        
        
        
       -- DBMS_OUTPUT.PUT_LINE('P_MASTER_VENDOR_LIST');
    IF P_MASTER_VENDOR_LIST IS NOT NULL THEN  
        L_WHERE_MASTER_VENDOR_LIST:= L_WHERE_MASTER_VENDOR_LIST||'  and (('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_MASTER_VENDOR_LIST)||' ) is null or exists (select 1  from
                         XXEIS.EIS_XXWC_PARAM_PARSE_LIST PARM
                         where  PARM.LIST_NAME= ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_MASTER_VENDOR_LIST)||' )
                            and upper(PARM.LIST_TYPE) =''SUPPLIER''
                            and PARM.PROCESS_ID = '||p_process_id||'
                            AND emsy.c_ext_attr1= parm.list_value))';
                            
         XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(p_process_id,(XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_MASTER_VENDOR_LIST)),'Supplier');                   
          else
         L_WHERE_MASTER_VENDOR_LIST:=' and 1=1';
      end if; 
    
   
    
   -- DBMS_OUTPUT.PUT_LINE('P_ORGANIZATION_LIST');
        IF P_ORGANIZATION_LIST IS NOT NULL THEN  
        L_WHERE_ORGANIZATION_LIST:= L_WHERE_ORGANIZATION_LIST||'  and (('||xxeis.eis_rs_utility.get_param_values(P_ORGANIZATION_LIST)||' ) is null or exists (select 1  from
                         XXEIS.EIS_XXWC_PARAM_PARSE_LIST PARM
                         where  PARM.LIST_NAME= ('||xxeis.eis_rs_utility.get_param_values(P_ORGANIZATION_LIST)||' )
                            and upper(PARM.LIST_TYPE) =''ORG''
                            and PARM.PROCESS_ID = '||p_process_id||'
                            AND LPAD(mp.organization_code,3,''0'')= parm.list_value)) --Changed for version 1.1 
        ';
		--Changed above column alias name odd. to mp. for version 1.1 
        XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(p_process_id,(xxeis.eis_rs_utility.get_param_values(P_ORGANIZATION_LIST) ),'Org');                   
        
          else
         L_WHERE_ORGANIZATION_LIST:=' and 1=1';
      end if;



--DBMS_OUTPUT.PUT_LINE('P_SOURCE_LIST');
        IF P_SOURCE_LIST IS NOT NULL THEN   
        L_WHERE_SOURCE_LIST:= L_WHERE_SOURCE_LIST||'  and (('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_SOURCE_LIST)||' ) is null or exists (select 1  from
                         XXEIS.EIS_XXWC_PARAM_PARSE_LIST PARM
                         where  PARM.LIST_NAME= ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_SOURCE_LIST)||' )
                            and upper(PARM.LIST_TYPE) =''SOURCE''
                            and PARM.PROCESS_ID = '||p_process_id||'
                            AND (EXPV.SOURCE= parm.list_value))
            ';
            XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(p_process_id,(XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_SOURCE_LIST)),'Source');  
        else
          L_WHERE_SOURCE_LIST:=' and 1=1';
        end if;
        
         
      IF P_SHORT_DESC IS NOT NULL THEN  
      L_SHORT_DESC:= L_SHORT_DESC||'  AND (msi.description=DECODE(('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_SHORT_DESC)||' ), NULL,msi.description)
        OR ( msi.description LIKE  ''%''||REPLACE(('||xxeis.eis_rs_utility.get_param_values(P_SHORT_DESC)||' ),'''','''''''')||''%''))'
;
        else
          L_SHORT_DESC:=' and 1=1';
    END IF;

  
END IF; 




IF P_ITEM_NUMBER IS NOT NULL THEN   
        L_WHERE_ITEM_NUMBER:= L_WHERE_ITEM_NUMBER||'  and msi.segment1 in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_ITEM_NUMBER)||' ) ';
        else
          L_WHERE_ITEM_NUMBER:=' and 1=1';
        END IF;
        
        
IF P_MASTER_VENDOR_NUMBER IS NOT NULL THEN   
        L_WHERE_MASTER_VENDOR_NUMBER:= L_WHERE_MASTER_VENDOR_NUMBER||'  and emsy.c_ext_attr1 in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_MASTER_VENDOR_NUMBER)||' ) ';
        else
          L_WHERE_MASTER_VENDOR_NUMBER:=' and 1=1';
        end if;
        

        
if P_ORGANIZATION is not null then   
        L_WHERE_ORGANIZATION:= L_WHERE_ORGANIZATION||'  and mp.organization_code in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_ORGANIZATION)||' ) '; 
		--Changed above column alias name odd. to mp. for version 1.1 
        else
          L_WHERE_ORGANIZATION:=' and 1=1';
        end if;


IF P_ITEM_STATUS IS NOT NULL THEN   
        L_WHERE_ITEM_STATUS:= L_WHERE_ITEM_STATUS||'  and  msi.inventory_item_status_code in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_ITEM_STATUS)||' ) ';
        else
          L_WHERE_ITEM_STATUS:=' and 1=1';
        end if;
        
IF P_USER_ITEM_TYPE IS NOT NULL THEN   
        L_WHERE_USER_ITEM_TYPE:= L_WHERE_USER_ITEM_TYPE||'  and (SELECT meaning
             FROM fnd_lookup_values
            WHERE lookup_type = ''ITEM_TYPE'' AND lookup_code = msi.item_type) in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_USER_ITEM_TYPE)||' ) ';
        else
          L_WHERE_USER_ITEM_TYPE:=' and 1=1';
        end if;
        
IF P_WEBSITE_ITEM IS NOT NULL THEN   
        L_WHERE_WEBSITE_ITEM:= L_WHERE_WEBSITE_ITEM||'  and xxeis.eis_xxwc_data_mang_rpt_pkg.website_item_hierarchy(msi.inventory_item_id,msi.organization_id,''Website Item'') in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_WEBSITE_ITEM)||' ) ';
        else
          L_WHERE_WEBSITE_ITEM:=' and 1=1';
        end if;

        
                
IF P_SOURCE IS NOT NULL THEN   
        L_WHERE_SOURCE:= L_WHERE_SOURCE||'  and EXPV.SOURCE in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_SOURCE)||' ) ';
        else
          L_WHERE_SOURCE:=' and 1=1';
        end if;
        
if P_MASTER_VENDOR_NAME is not null then   
        L_WHERE_MASTER_VENDOR_NAME:= L_WHERE_MASTER_VENDOR_NAME||'  and expv.vendor_name in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_MASTER_VENDOR_NAME)||' ) ';
        else
          L_WHERE_MASTER_VENDOR_NAME:=' and 1=1';
        end if;

       


L_DATA_SQL:='SELECT /*+ use_nl(msi,msit) use_nl(msi,uomw) use_nl(msi,ood) use_nl(msi,mp) use_nl(msi,fu) use_nl(msi,mt) use_nl(msi,emsy) use_nl(msi,emsy2) use_nl(msi,EMSTL) use_nl(msi,expv)*/  ----added for version 1.1 
          '||p_process_id||' process_id,
          msi.segment1 item,
          msi.description short_description,
          msit.long_description long_description,
          mt.unit_of_measure uom,
          unit_weight weight,
          uomw.unit_of_measure_tl weight_uom_code,
          msi.shelf_life_days,
          msi.attribute22 avp_code,
          XXEIS.EIS_XXWC_DATA_MANG_RPT_PKG.coo_meaning(emsy.c_ext_attr2) country_of_origin,
          emsy.c_ext_attr1 master_vendor_number,
          EXPV.SOURCE,
          expv.vendor_num sourcing_vendor_number,
          expv.vendor_name sourcing_vendor_name, --,expv.org                      -- Commented by Mahesh for TMS#20150310-00182  on 04/17/2015
          --ood.organization_code org -- commented for version 1.1
          mp.organization_code org, -- added for version 1.1           
          msi.inventory_item_status_code item_status,
--          (SELECT meaning
--             FROM fnd_lookup_values
--            WHERE lookup_type = ''ITEM_TYPE'' AND lookup_code = msi.item_type)
--             user_item_type, -- commented for version 1.1
          flv.meaning user_item_type ,  -- added for version 1.1           
          DECODE (msi.eng_item_flag,  ''N'', ''No'',  ''Y'', ''Yes'')
             engineering_item_flag,
		 --ood.organization_name org_description, -- commented for version 1.1
          ood.name org_description,  -- added for version 1.1           
          mp.attribute8 district,
          mp.attribute9 region,
          SUBSTR (
             apps.xxwc_mv_routines_pkg.get_item_cross_reference (
                msi.inventory_item_id,
                ''VENDOR'',
                1),
             1,
             255)
             mfg_part_number,             -- mmp.mfg_part_num Mfg_Part_Number,
          msi.fixed_lot_multiplier,
          msi.list_price_per_unit,
          NVL (
             apps.cst_cost_api.get_item_cost (1,
                                              msi.inventory_item_id,
                                              msi.organization_id),
             0)
             average_cost,
             expv.bpa_cost,
          expv.bpa bpa_number,
           xxeis.EIS_XXWC_DATA_MANG_RPT_PKG.get_inv_cat_class (
             msi.inventory_item_id,
             msi.organization_id)
             INVENTORY_CATEGORY,
          xxeis.EIS_XXWC_DATA_MANG_RPT_PKG.get_inv_cat_class_desc (
             msi.inventory_item_id,
             msi.organization_id,''DESCRIPTION'')
             INVENTORY_CATEGORY_DESCRIPTION,
          xxeis.EIS_XXWC_DATA_MANG_RPT_PKG.website_item_hierarchy(msi.inventory_item_id,msi.organization_id,''Website Item'') website_item,
          xxeis.EIS_XXWC_DATA_MANG_RPT_PKG.website_item_hierarchy(msi.inventory_item_id,msi.organization_id,''WC Web Hierarchy'') wc_web_hierarchy,
          emsy2.c_ext_attr1 brand,
          emsy2.c_ext_attr7 product_line,
          emsy2.c_ext_attr6 web_mfg_part_number,
          emsy2.c_ext_attr5 hazmat_flag,
          emsy2.c_ext_attr3 bulky_item_flag,
          emsy2.c_ext_attr11 cybersource_tax_code,
          emsy2.c_ext_attr15 fullsize_image_1,
          emsy2.c_ext_attr20 thumbnail_image_1,
          emsy2.n_ext_attr1 web_sequence,
          emsy2.c_ext_attr14 web_short_description,
          emstl.tl_ext_attr3 web_keywords,
          emstl.tl_ext_attr2 web_long_description,
          TRUNC (msi.creation_date) item_creation_date,
          xxeis.eis_rs_xxwc_com_util_pkg.get_item_acc (
             msi.cost_of_sales_account)
             cogs_account,
          xxeis.eis_rs_xxwc_com_util_pkg.get_item_acc (msi.sales_account)
             sales_account,
          xxeis.eis_rs_xxwc_com_util_pkg.get_item_acc (msi.expense_account)
             expense_account,
          msi.inventory_item_id,
          emsy.c_ext_attr6 item_level -- Added by Mahender for TMS#20150310-00182  on 05/06/2015
                                     ,
          emsy.c_ext_attr7 fulfillment -- Added by Mahender for TMS#20150310-00182  on 05/06/2015
                                      ,
          emsy.c_ext_attr10 retail_selling_uom -- Added by Mahender for TMS#20150310-00182  on 05/06/2015
                                              ,
          emsy.c_ext_attr11 conflict_minerals -- Added by Mahender for TMS#20150310-00182  on 05/06/2015,
                ,
          (SELECT d.segment1
             FROM apps.mtl_item_catalog_groups_b d
            WHERE msi.item_catalog_group_id = d.item_catalog_group_id(+))
             xpar   -- Added by Mahender for TMS#20150310-00182  on 05/06/2015
                 ,
          fu.description created_by                           -- Added by Mahender for TMS#20150702-00174   on 07/28/2015
          ,xxeis.eis_xxwc_data_mang_rpt_pkg.get_inv_cat_class_desc(msi.inventory_item_id, msi.organization_id,''ENABLED_FLAG'') enabled_flag, 
          xxeis.eis_xxwc_data_mang_rpt_pkg.get_inv_cat_class_desc(msi.inventory_item_id, msi.organization_id,''SUMMARY_FLAG'') summary_flag, 
          xxeis.eis_xxwc_data_mang_rpt_pkg.get_flex_val_start_end_date(msi.inventory_item_id, msi.organization_id,''START_DATE'') start_date_active,
          XXEIS.EIS_XXWC_DATA_MANG_RPT_PKG.get_flex_val_start_end_date(msi.inventory_item_id, msi.organization_id,''END_DATE'') END_DATE_ACTIVE 
     FROM mtl_system_items_b msi,
          mtl_system_items_tl msit,
          mtl_units_of_measure_tl uomw,
		  --org_organization_definitions ood, --commented for version 1.1
          hr_all_organization_units ood, --Added for version 1.1
          mtl_parameters mp,
          fnd_user fu                                         -- Added by Mahender for TMS#20150702-00174   on 07/28/2015
                     ,
          mtl_units_of_measure_tl mt, --changed _vl table to _tl table for verrsion 1.1
          apps.ego_mtl_sy_items_ext_b emsy,
          apps.ego_mtl_sy_items_ext_b emsy2,
          APPS.EGO_MTL_SY_ITEMS_EXT_TL EMSTL,
          xxeis.eis_xxwc_po_isr_tab expv,
          fnd_lookup_values flv --Added for version 1.1
--          (SELECT lookup_code, meaning
--             FROM apps.FND_LOOKUP_VALUES
--            WHERE lookup_type = ''XXWC_TERRITORIES'') coo -- Added by Mahender for TMS#20150310-00182  on 05/06/2015
    WHERE  msi.inventory_item_id    = msit.inventory_item_id
      and MSI.ORGANIZATION_ID         = MSIT.ORGANIZATION_ID
      and MSIT.LANGUAGE               = USERENV (''LANG'')
      and MSI.WEIGHT_UOM_CODE         = UOMW.UOM_CODE(+)           
      and UOMW.LANGUAGE(+)            = USERENV(''LANG'')
      and MSI.ORGANIZATION_ID         = OOD.ORGANIZATION_ID
      and MSI.ORGANIZATION_ID         = MP.ORGANIZATION_ID
      and MSI.CREATED_BY              = FU.USER_ID -- Added by Mahender for TMS#20150702-00174   on 07/28/2015
      and MSI.PRIMARY_UOM_CODE        = MT.UOM_CODE                 
      and mt.LANGUAGE                 = USERENV(''LANG'') --Added for version 1.1
      AND emsy.inventory_item_id(+)   = msi.inventory_item_id
      AND emsy.organization_id(+)     = msi.organization_id
      AND emsy.attr_group_id(+)       = 861
      AND emsy2.inventory_item_id(+)  = msi.inventory_item_id
      AND emsy2.organization_id(+)    = msi.organization_id
      AND emsy2.attr_group_id(+)      = 479
      AND emstl.inventory_item_id(+)  = msi.inventory_item_id
      AND emstl.organization_id(+)    = msi.organization_id
      AND emstl.attr_group_id(+)      = 479
      AND msi.inventory_item_id       = expv.inventory_item_id(+) -- Added Outer Join by Mahesh for TMS#20150310-00182  on 04/17/2015
      and MSI.ORGANIZATION_ID         = EXPV.ORGANIZATION_ID(+)   -- Added Outer Join by Mahesh for TMS#20150310-00182  on 04/17/2015
      and MSI.ITEM_TYPE               = FLV.LOOKUP_CODE(+)  --Added for version 1.1
      and FLV.LOOKUP_TYPE(+)          = ''ITEM_TYPE''  --Added for version 1.1
      AND ( TRUNC (msi.creation_date) = DECODE (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from, NULL, TRUNC (msi.creation_date))
        OR ( TRUNC (msi.creation_date) >= (XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM)
              AND TRUNC (MSI.CREATION_DATE)  <= TRUNC (sysdate) ) )
        '||L_WHERE_ITEM_LIST||'
        '||L_WHERE_MASTER_VENDOR_LIST||'
        '||L_WHERE_ORGANIZATION_LIST||'
        '||L_WHERE_SOURCE_LIST||'
        '||L_SHORT_DESC||'
        '||L_WHERE_MASTER_VENDOR_NAME||'
        '||L_WHERE_WEBSITE_ITEM||'
        '||L_WHERE_USER_ITEM_TYPE||'
        '||L_WHERE_ITEM_STATUS||'
        '||L_WHERE_ORGANIZATION||'
        '||L_WHERE_SOURCE||'
        '||L_WHERE_MASTER_VENDOR_NUMBER||'
        '||L_WHERE_ITEM_NUMBER||'
        ';
                   
                   
        --fnd_file.put_line (fnd_file.log, 'L_DATA_SQL  '||L_DATA_SQL);
                   
                  -- dbms_output.put_line('L_DATA_SQL'||L_DATA_SQL);
                   
  OPEN L_REF_CURSOR1 FOR L_DATA_SQL;
  LOOP
  FETCH L_REF_CURSOR1 bulk collect INTO data_table_tab limit 10000;
  IF data_table_tab.COUNT>0 THEN --Added for version 1.1
    FORALL S IN 1 .. data_table_tab.COUNT
    INSERT INTO XXEIS.EIS_DATA_MANAGMNT_DTLS VALUES DATA_TABLE_TAB(S);
	--Added below code for version 1.1
    --COMMIT; --Commented for version 1.2
  END IF;
   -- exit when  l_ref_cursor1%NOTFOUND; --commented for version 1.1
    IF l_ref_cursor1%notfound THEN
    CLOSE l_ref_cursor1;
    EXIT;
  END IF;
  --code end for version 1.1
  END LOOP;
  --  DBMS_OUTPUT.PUT_LINE('EIS_DATA_MANAGMNT_DTLS count'||EIS_DATA_MANAGMNT_DTLS.count);
 


EXCEPTION WHEN OTHERS THEN
fnd_file.put_line (fnd_file.log,'THE ERROR IS'||SQLCODE||SQLERRM);
--COMMIT; --Commented for version 1.2
    
END ; 

/* --Commented code for version 1.2
procedure clear_temp_tables ( p_process_id in number)
as
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/12/2016   Initial Build --TMS#20160418-00110  --Performance Tuning
--//============================================================================ 
  begin  
  delete from xxeis.EIS_DATA_MANAGMNT_DTLS where process_id=p_process_id;
  commit;
end ;
*/ --Commented code for version 1.2

end EIS_XXWC_DATA_MANG_RPT_PKG;
/
