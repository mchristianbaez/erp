CREATE OR REPLACE 
PACKAGE BODY  XXEIS.EIS_XXWC_BIN_LOCATION_PKG
AS
--//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "Bin location"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_BIN_LOCATION_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	        18-may-2016       Initial Build TMS#20160429-00037
--// 1.1		 Siva			17-Jun-2016		  TMS#20160602-00001	
--// 1.2		 Siva			18-Jul-2016		  TMS#20160714-00255 
--// 1.3     	Siva 			15-Mar-2017       TMS#20170224-00065
--//============================================================================
  PROCEDURE SPLIT_SEGMENTS(
      P_STRING IN VARCHAR2,
      P_SEGMENT1 OUT VARCHAR2,
      P_SEGMENT2 OUT VARCHAR2,
      P_SEGMENT3 OUT VARCHAR2,
      P_SEGMENT4 OUT VARCHAR2 )
  IS
 --//============================================================================
--//
--// Object Name         :: SPLIT_SEGMENTS  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure will call as private program to split the concatenated segments
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  				05-May-2016 	Initial Build   
--//  1.1		 Siva			17-Jun-2016		 TMS#20160602-00001
--// 1.3     	Siva 			15-Mar-2017       TMS#20170224-00065	
--//============================================================================ 
    l_string_text   VARCHAR2(32000);
    l_string_length NUMBER;
    I               NUMBER;
    L_STR           VARCHAR2(10000);
    L_POS_START     NUMBER;
    l_pos_end       NUMBER;
  BEGIN
    l_string_text   :=P_STRING||'.';
    l_string_length := dbms_lob.getlength(l_string_text);
    I               := 1;
    L_POS_START     := 1;
    l_pos_end       := dbms_lob.instr(l_string_text,'.',1,i);
    LOOP
      l_str        :=dbms_lob.substr(l_string_text,(l_pos_end-l_pos_start),l_pos_start ); --TMS#20160429-00037    by siva  on 04-24-2016
      L_POS_START  := L_POS_END                              + 1;
      I            := I                                      + 1;
      L_POS_END    := DBMS_LOB.INSTR(l_string_text,'.',1,I);
      IF I          =2 THEN
        p_SEGMENT1 :=L_STR;
      END IF;
      IF I          =3 THEN
        p_SEGMENT2 :=L_STR;
      END IF;
      IF I          =4 THEN
        p_SEGMENT3 :=L_STR;
      END IF;
      IF I          =5 THEN
        p_SEGMENT4 :=L_STR;
      END IF;
      IF l_pos_end =0 THEN
        EXIT;
      END IF;
    END LOOP;
  END SPLIT_SEGMENTS;
  
  
 /* FUNCTION get_last_received_date(
      p_inventory_item_id IN NUMBER,
      p_organization_id   IN NUMBER)
    RETURN DATE
  IS
--//============================================================================
--//
--// Object Name         :: get_last_received_date  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================  
    l_hash_index VARCHAR2(100);
    l_sql        VARCHAR2(32000);
  BEGIN
    g_last_received_date := NULL;
    l_sql                :='select max (prt.transaction_date)               
from     po.rcv_transactions prt,                          
po.po_lines_all ppla               
where      prt.organization_id = :1                      
and prt.po_line_id      = ppla.po_line_id                      
and ppla.item_id        =  :2                      
and prt.destination_type_code = ''INVENTORY''';
    BEGIN
      l_hash_index         :=p_inventory_item_id||'-'||p_organization_id;
      g_last_received_date := g_last_received_date_vldn_tbl(l_hash_index);
    EXCEPTION
    WHEN no_data_found THEN
      BEGIN
        EXECUTE immediate l_sql INTO g_last_received_date USING p_organization_id,p_inventory_item_id;
      EXCEPTION
      WHEN no_data_found THEN
        g_last_received_date :=NULL;
      WHEN OTHERS THEN
        g_last_received_date :=NULL;
      END;
      l_hash_index                                :=p_inventory_item_id||'-'||p_organization_id;
      g_last_received_date_vldn_tbl(l_hash_index) := g_last_received_date;
    END;
    RETURN g_last_received_date;
  EXCEPTION
  WHEN OTHERS THEN
    g_last_received_date:=NULL;
    RETURN g_last_received_date;
  END get_last_received_date;
  
  FUNCTION get_item_cost(
      p_num               IN NUMBER,
      p_inventory_item_id IN NUMBER,
      p_organization_id   IN NUMBER)
    RETURN NUMBER
  IS
--//============================================================================
--//
--// Object Name         :: GET_ITEM_COST  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================  
    l_hash_index VARCHAR2(100);
    l_sql        VARCHAR2(32000);
  BEGIN
    g_item_cost := NULL;
    BEGIN
      l_hash_index:=p_inventory_item_id||'-'||p_organization_id;
      g_item_cost :=g_item_cost_vldn_tbl(l_hash_index);
    EXCEPTION
    WHEN no_data_found THEN
      BEGIN
        g_item_cost:=apps.cst_cost_api.get_item_cost(p_num,p_inventory_item_id,p_organization_id);
      EXCEPTION
      WHEN no_data_found THEN
        g_item_cost :=0;
      WHEN OTHERS THEN
        g_item_cost :=0;
      END;
      l_hash_index                       :=p_inventory_item_id||'-'||p_organization_id;
      g_item_cost_vldn_tbl(l_hash_index) := g_item_cost;
    END;
    RETURN g_item_cost;
  EXCEPTION
  WHEN OTHERS THEN
    g_item_cost:=0;
    RETURN g_item_cost;
  END get_item_cost;
 */ --commented for version 1.1   
  
  FUNCTION get_primary_bin_loc(
      p_inventory_item_id IN NUMBER,
      p_organization_id   IN NUMBER)
    RETURN VARCHAR2
  IS
--//============================================================================
--//
--// Object Name         :: GET_PRIMARY_BIN_LOC  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//  1.1		 Siva			17-Jun-2016		 TMS#20160602-00001	
--//============================================================================  
    l_hash_index VARCHAR2(100);
    l_sql        VARCHAR2(32000);
  BEGIN
    g_primary_bin_loc := NULL;
    l_sql             :='select mil.segment1        
from    mtl_item_locations_kfv mil,                
mtl_secondary_locators msl        
where  msl.inventory_item_id = :1             
and msl.organization_id   = :2             
and msl.secondary_locator = mil.inventory_location_id             
and mil.segment1 like ''1-%''
and rownum=1'; --added for version 1.1
    BEGIN
      l_hash_index      :=p_inventory_item_id||'-'||p_organization_id;
      g_primary_bin_loc := g_primary_bin_loc_vldn_tbl(l_hash_index);
    EXCEPTION
    WHEN no_data_found THEN
      BEGIN
        EXECUTE immediate l_sql INTO g_primary_bin_loc USING p_inventory_item_id,p_organization_id;
      EXCEPTION
      WHEN no_data_found THEN
        g_primary_bin_loc :=NULL;
      WHEN OTHERS THEN
        g_primary_bin_loc :=NULL;
      END;
      l_hash_index                             :=p_inventory_item_id||'-'||p_organization_id;
      g_primary_bin_loc_vldn_tbl(l_hash_index) := g_primary_bin_loc;
    END;
    RETURN g_primary_bin_loc;
  EXCEPTION
  WHEN OTHERS THEN
    g_primary_bin_loc:=NULL;
    RETURN g_primary_bin_loc;
  END GET_PRIMARY_BIN_LOC;
  
 /* FUNCTION GET_ALTERNATE_BIN_LOC(
      P_INVENTORY_ITEM_ID IN NUMBER,
      P_organization_id   IN NUMBER)
    RETURN VARCHAR2
  IS
    --//============================================================================
    --//
    --// Object Name         :: get_alternate_bin_loc
    --//
    --// Object Usage    :: This Object Referred by "BIN LOCATION"
    --//
    --// Object Type         :: Function
    --//
    --// Object Description  :: This Function is created for EIS_XXWC_INV_BIN_LOC_V View to get the alternate_bin_loc values.
    --//
    --//
    --// Version Control
    --//============================================================================
    --// Version    Author           Date          Description
    --//----------------------------------------------------------------------------
    --// 1.0        Siva     05/10/2016       Initial Build  --TMS#20160429-00037
    --//============================================================================
    L_HASH_INDEX VARCHAR2(100);
    L_SQL        VARCHAR2(32000);
    C1 SYS_REFCURSOR;
    L_STR VARCHAR2(1000);
  BEGIN
    G_ALTERNATE_BIN_LOC := NULL;
			L_SQL               :='SELECT mil.concatenated_segments              
		FROM mtl_item_locations_kfv mil, mtl_secondary_locators msl              
		WHERE     msl.inventory_item_id = :1                    
		AND msl.organization_id   = :2                    
		AND msl.secondary_locator = mil.inventory_location_id                    
		AND mil.segment1 NOT LIKE ''1-%''';
    BEGIN
      l_hash_index        :=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
      G_ALTERNATE_BIN_LOC := G_ALTERNATE_BIN_LOC_VLDN_TBL(L_HASH_INDEX);
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      BEGIN
        OPEN c1 FOR L_SQL USING P_INVENTORY_ITEM_ID,P_organization_id;
        LOOP
          FETCH C1 INTO L_STR;
          EXIT
        WHEN c1%NOTFOUND;
          G_ALTERNATE_BIN_LOC:=G_ALTERNATE_BIN_LOC||','||L_STR;
          G_ALTERNATE_BIN_LOC:=ltrim(G_ALTERNATE_BIN_LOC,',');
        END LOOP;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        G_ALTERNATE_BIN_LOC :=NULL;
      WHEN OTHERS THEN
        G_ALTERNATE_BIN_LOC :=NULL;
      END;
      l_hash_index                               :=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
      G_ALTERNATE_BIN_LOC_VLDN_TBL(L_HASH_INDEX) := G_ALTERNATE_BIN_LOC;
    END;
    RETURN G_ALTERNATE_BIN_LOC;
  EXCEPTION
  WHEN OTHERS THEN
    G_ALTERNATE_BIN_LOC:=NULL;
    RETURN G_ALTERNATE_BIN_LOC;
  END get_alternate_bin_loc;
*/ --commented for version 1.1  
  
  FUNCTION GET_START_BIN
    RETURN VARCHAR2
  AS
--//============================================================================
--//
--// Object Name         :: GET_START_BIN  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================  
  BEGIN
    RETURN G_START_BIN;
  EXCEPTION
  WHEN OTHERS THEN
    RETURN NULL;
  END;
  
  FUNCTION GET_END_BIN
    RETURN VARCHAR2
  AS
  --//============================================================================
--//
--// Object Name         :: GET_END_BIN  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
  BEGIN
    RETURN G_END_BIN;
  EXCEPTION
  WHEN OTHERS THEN
    RETURN NULL;
  END;
  PROCEDURE get_bin_location_info(
      p_process_id       IN NUMBER,
      p_category_from    IN VARCHAR2 DEFAULT NULL,
      p_category_to      IN VARCHAR2 DEFAULT NULL,
      p_organization     IN VARCHAR2 DEFAULT NULL,
      p_category_set     IN VARCHAR2 DEFAULT NULL,
      p_quantity_on_hand IN VARCHAR2 )
  AS
--//============================================================================
--//
--// Object Name         :: get_bin_location_info  
--//
--// Object Usage 		 :: This Object Referred by "Bin lOcation"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure will call in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         siva  			05-May-2016 		Initial Build  
--// 1.1		 Siva			17-Jun-2016		  TMS#20160602-00001	
--// 1.2		 Siva			18-Jul-2016		  TMS#20160714-00255  
--//============================================================================  
  type org_list_rec
IS
  record
  (
    process_id      NUMBER,
    organization_id NUMBER );
type org_list_rec_tab
IS
  TABLE OF org_list_rec INDEX BY binary_integer;
  ORG_LIST_TAB ORG_LIST_REC_TAB;
TYPE GET_BIN_LOC_FLAG_REC
IS
  RECORD
  (
    process_id        NUMBER,
    INVENTORY_ITEM_ID NUMBER,
    organization_id   NUMBER );
TYPE GET_BIN_LOC_FLAG_REC_TAB
IS
  TABLE OF GET_BIN_LOC_FLAG_REC INDEX BY BINARY_INTEGER;
  get_bin_loc_flag_tab get_bin_loc_flag_rec_tab;
type inv_onhand_dtls_rec
IS
  record
  (
    PROCESS_ID        NUMBER,
    onhand            NUMBER,
    INVENTORY_ITEM_ID NUMBER,
    organization_id   NUMBER );
TYPE INV_ONHAND_DTLS_REC_TAB
IS
  TABLE OF INV_ONHAND_DTLS_REC INDEX BY BINARY_INTEGER;
  inv_onhand_dtls_tab inv_onhand_dtls_rec_tab;
  L_LOCATION_ID  VARCHAR2(32000);
  l_category_set VARCHAR2(32000);
  L_REF_CURSOR1 CURSOR_TYPE4;
  L_REF_CURSOR2 CURSOR_TYPE4;
  L_REF_CURSOR3 CURSOR_TYPE4;
  L_REF_CURSOR4 CURSOR_TYPE4;
  L_STMT1         VARCHAR2(32000);
  L_STMT2         VARCHAR2(32000);
  L_STMT3         VARCHAR2(32000);
  L_STMT4         VARCHAR2(32000);
  L_SEGMENT1      VARCHAR2(200);
  L_SEGMENT2      VARCHAR2(200);
  L_SEGMENT3      VARCHAR2(200);
  L_SEGMENT4      VARCHAR2(200);
  L_SEGMENT1_FROM VARCHAR2(10000);
  L_SEGMENT2_FROM VARCHAR2(10000);
  L_SEGMENT3_FROM VARCHAR2(10000);
  L_SEGMENT4_FROM VARCHAR2(10000);
  L_SEGMENT1_TO   VARCHAR2(10000);
  L_SEGMENT2_TO   VARCHAR2(10000);
  L_SEGMENT3_TO   VARCHAR2(10000);
  l_segment4_to   VARCHAR2(10000);
  l_delete_stmt   VARCHAR2(32000);
TYPE BIN_DATA_INFO
IS
  TABLE OF XXEIS.EIS_XXWC_BIN_LOC_DATA_TAB%ROWTYPE INDEX BY BINARY_INTEGER;
  bin_data_info_tab bin_data_info;
BEGIN
  inv_onhand_dtls_tab.delete;
  bin_data_info_tab.delete;
  GET_BIN_LOC_FLAG_TAB.DELETE;
  ORG_LIST_TAB.DELETE;
  IF p_organization='All' THEN
    l_location_id :='and 1=1';
  ELSE
    l_location_id := 'and moq.organization_id in (select organization_id from mtl_parameters where organization_code in('||xxeis.eis_rs_utility.get_param_values(p_organization)||' ))' ; --need to correct one this
  END IF;
IF P_ORGANIZATION!='All' THEN
  L_STMT1        := 'select '||P_PROCESS_ID||',organization_id from xxeis.eis_org_access_v where  organization_code in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_ORGANIZATION)||' )';
ELSE
  L_STMT1:= 'select '||P_PROCESS_ID||',organization_id from xxeis.eis_org_access_v';
END IF;
IF P_CATEGORY_SET IS NOT NULL THEN
  L_CATEGORY_SET  :='and mcats.category_set_name in  ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_CATEGORY_SET)||')';
END IF;
IF p_category_from IS NOT NULL THEN
  SPLIT_SEGMENTS ( P_CATEGORY_FROM,L_SEGMENT1,L_SEGMENT2 ,L_SEGMENT3,l_segment4);
  IF L_SEGMENT1 IS NOT NULL THEN
    --     and nvl(mcvs.segment1,'''||l_segment1||''')
    L_SEGMENT1_FROM:='and nvl(mcvs.segment1,'''||L_SEGMENT1||''') >='''||L_SEGMENT1||'''';
  END IF;
  IF l_segment2    IS NOT NULL THEN
    L_SEGMENT2_FROM:='and nvl(mcvs.segment2,'''||L_SEGMENT2||''') >='''||l_segment2||'''';
  END IF;
  IF l_segment3    IS NOT NULL THEN
    L_SEGMENT3_FROM:='and nvl(mcvs.segment3,'''||L_SEGMENT3||''') >='''||l_segment3||'''';
  END IF;
  IF l_segment4    IS NOT NULL THEN
    L_SEGMENT4_FROM:='and nvl(mcvs.segment4,'''||L_SEGMENT4||''') >='''||l_segment4||'''';
  END IF;
END IF;
IF p_category_to IS NOT NULL THEN
  split_segments ( p_category_to,l_segment1,l_segment2 ,l_segment3,l_segment4);
  IF l_segment1  IS NOT NULL THEN
    L_SEGMENT1_to:='and nvl(mcvs.segment1,'''||L_SEGMENT1||''') <='''||L_SEGMENT1||'''';
  END IF;
  IF l_segment2  IS NOT NULL THEN
    L_SEGMENT2_to:='and nvl(mcvs.segment2,'''||L_SEGMENT2||''')  <='''||l_segment2||'''';
  END IF;
  IF l_segment3  IS NOT NULL THEN
    L_SEGMENT3_to:='and nvl(mcvs.segment3,'''||L_SEGMENT3||''') <='''||l_segment3||'''';
  END IF;
  IF l_segment4  IS NOT NULL THEN
    L_SEGMENT4_to:='and nvl(mcvs.segment4,'''||L_SEGMENT4||''') <='''||l_segment4||'''';
  END IF;
END IF;
--dbms_output.put_line('l_stmt1'||l_stmt1);
OPEN l_ref_cursor1 FOR l_stmt1 ;
LOOP
  FETCH l_ref_cursor1 bulk collect INTO org_list_tab limit 10000;
  IF org_list_tab.count >= 1 THEN
    forall j IN 1 .. org_list_tab.count
    INSERT INTO xxeis.eis_xxwc_valid_bin_orgs VALUES org_list_tab(j);
    --COMMIT; --Commented for version 1.3 
  END IF;
  IF l_ref_cursor1%notfound THEN
    CLOSE l_ref_cursor1;
    EXIT;
  END IF;
END LOOP;
---load bin orgs info
--  dbms_output.put_line('bin start');
IF xxeis.EIS_XXWC_BIN_LOCATION_PKG.GET_START_BIN IS NULL AND xxeis.EIS_XXWC_BIN_LOCATION_PKG.GET_END_BIN IS NULL THEN
  NULL;
elsif xxeis.EIS_XXWC_BIN_LOCATION_PKG.GET_START_BIN IS NOT NULL AND xxeis.EIS_XXWC_BIN_LOCATION_PKG.GET_END_BIN IS NOT NULL THEN
		  L_STMT2                                           :='select '||p_process_id||' process_id ,              
		msl.inventory_item_id,              
		msl.organization_id            
		from mtl_item_locations_kfv mil, mtl_secondary_locators msl            
		where  msl.secondary_locator = mil.inventory_location_id            
		and substr (mil.segment1, 3) between xxeis.EIS_XXWC_BIN_LOCATION_PKG.GET_START_BIN and xxeis.EIS_XXWC_BIN_LOCATION_PKG.GET_END_BIN';

elsif xxeis.EIS_XXWC_BIN_LOCATION_PKG.GET_START_BIN IS NOT NULL AND xxeis.EIS_XXWC_BIN_LOCATION_PKG.GET_END_BIN IS NULL THEN
		  L_STMT2                                           :='select '||p_process_id||' process_id ,                
		msl.inventory_item_id,                
		msl.organization_id            
		from mtl_item_locations_kfv mil, mtl_secondary_locators msl              
		where  msl.secondary_locator = mil.inventory_location_id              
		and substr (mil.segment1, 3) >= xxeis.EIS_XXWC_BIN_LOCATION_PKG.GET_START_BIN';
elsif xxeis.EIS_XXWC_BIN_LOCATION_PKG.GET_START_BIN IS NULL AND xxeis.EIS_XXWC_BIN_LOCATION_PKG.GET_END_BIN IS NOT NULL THEN
			L_STMT2                                           :='select '||p_process_id||' process_id ,              
				msl.inventory_item_id,              
				msl.organization_id           
				from mtl_item_locations_kfv mil, mtl_secondary_locators msl           
				where  msl.secondary_locator = mil.inventory_location_id          
				and substr (mil.segment1, 3) <= xxeis.EIS_XXWC_BIN_LOCATION_PKG.GET_END_BIN';
END IF;
IF XXEIS.EIS_XXWC_BIN_LOCATION_PKG.GET_START_BIN IS NULL AND XXEIS.EIS_XXWC_BIN_LOCATION_PKG.GET_END_BIN IS NULL THEN
  NULL;
ELSE
  OPEN l_ref_cursor2 FOR L_STMT2 ;
  LOOP
    FETCH l_ref_cursor2 bulk collect INTO get_bin_loc_flag_tab limit 10000;
    IF get_bin_loc_flag_tab.count >= 1 THEN
      forall j IN 1 .. get_bin_loc_flag_tab.count
      INSERT INTO xxeis.eis_xxwc_bin_orgs_tab VALUES get_bin_loc_flag_tab(j);
      --COMMIT; --Commented for version 1.3 
    END IF;
    IF l_ref_cursor2%notfound THEN
      CLOSE l_ref_cursor2;
      EXIT;
    END IF;
  END LOOP;
END IF;

--Removing duplicates from  table eis_xxwc_bin_orgs_tab
BEGIN
			  l_delete_stmt:= 'DELETE FROM xxeis.eis_xxwc_bin_orgs_tab   
			WHERE ROWID IN (              
			SELECT rid                
			FROM (SELECT ROWID RID,                             
			ROW_NUMBER () OVER (PARTITION BY inventory_item_id,organization_id ORDER BY ROWID) RN                        
			FROM xxeis.eis_xxwc_bin_orgs_tab where process_id='||P_PROCESS_ID ||')               
			WHERE RN <> 1)   
			AND PROCESS_ID ='||P_PROCESS_ID ||'';
  EXECUTE IMMEDIATE L_DELETE_STMT;
  --   dbms_output.put_line('l_delete_statement is' ||l_delete_stmt);
  --COMMIT; --Commented for version 1.3 
EXCEPTION
WHEN OTHERS THEN
  FND_FILE.PUT_LINE(FND_FILE.LOG,'Removing Duplicates '||SQLCODE||SQLERRM);
  --dbms_output.PUT_LINE('Removing Duplicates '||SQLCODE||SQLERRM);
END;

		IF XXEIS.EIS_XXWC_BIN_LOCATION_PKG.GET_START_BIN IS NULL AND XXEIS.EIS_XXWC_BIN_LOCATION_PKG.GET_END_BIN IS NULL THEN
				  l_stmt3  := 'select '||p_process_id||' process_id,onhand,inventory_item_id,organization_id             
				from(                
				select /*+ index(moq mtl_onhand_quantities_n4)*/                       
				nvl (sum (moq.transaction_quantity), 0) onhand ,                      
				moq.inventory_item_id,                      
				moq.organization_id                      
				from mtl_onhand_quantities_detail moq       
				where 1=1                    
				'||l_location_id||'                  
				group by moq.inventory_item_id,moq.organization_id)x                  
				';
--  dbms_output.put_line('l_stmt3'||l_stmt3); --commented for version 1.1
		ELSE
				  l_stmt3 := 'select '||p_process_id||' process_id,onhand,inventory_item_id,organization_id               
				from(                  
				select /*+ index(moq mtl_onhand_quantities_n4)*/                   
				nvl (sum (moq.transaction_quantity), 0) onhand ,                  
				moq.inventory_item_id,                  
				moq.organization_id            
				from mtl_onhand_quantities_detail moq,                
				xxeis.eis_xxwc_bin_orgs_tab tab --- create index on item and org            
				where tab.inventory_item_id=moq.inventory_item_id              
				and tab.organization_id=moq.organization_id              
				and tab.process_id ='||P_process_id||'                
				'||l_location_id||'              
				group by moq.inventory_item_id,moq.organization_id) x            
				';
		END IF;
    
IF P_QUANTITY_ON_HAND    ='On Hand' THEN
  l_stmt3               := l_stmt3||chr(10)||'where x.onhand>0' ;
ELSIF P_QUANTITY_ON_HAND ='No on Hand' THEN
  l_stmt3               := l_stmt3||chr(10)|| 'where x.onhand=0' ;
ELSIF P_QUANTITY_ON_HAND ='Negative on hand' THEN
  l_stmt3               := l_stmt3||chr(10)|| 'where x.onhand<0' ;
END IF;

OPEN l_ref_cursor3 FOR l_stmt3 ;
LOOP
  FETCH l_ref_cursor3 bulk collect INTO inv_onhand_dtls_tab limit 10000;
  IF inv_onhand_dtls_tab.count >= 1 THEN
    forall j IN 1 .. inv_onhand_dtls_tab.count
    INSERT INTO xxeis.eis_xxwc_bin_onhand_tab VALUES inv_onhand_dtls_tab(j);
    --COMMIT; --Commented for version 1.3 
  END IF;
  IF l_ref_cursor3%notfound THEN
    CLOSE l_ref_cursor3;
    EXIT;
  END IF;
END LOOP;


IF P_QUANTITY_ON_HAND    ='ALL' THEN  -- Added for version 1.1

IF XXEIS.EIS_XXWC_BIN_LOCATION_PKG.GET_START_BIN IS NULL AND XXEIS.EIS_XXWC_BIN_LOCATION_PKG.GET_END_BIN IS NULL THEN
			  l_stmt4   := 'select /*+ INDEX(mcvs MTL_CATEGORIES_B_U1) INDEX(boh EIS_XXWC_BIN_ONHAND_TAB_N3)*/ -- added version 1.2
			  '||p_process_id||' process_id,    
			ood.organization_code organization,    
			msi.segment1 part_number,    
			msi.description,    
			msi.primary_uom_code uom,    
			msi.list_price_per_unit selling_price, 
			--xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id) vendor_name,    
			--xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number(msi.inventory_item_id,msi.organization_id) vendor_number,    
			isr.vendor_name vendor_name,    
			isr.vendor_num vendor_number,    
			min_minmax_quantity,    
			max_minmax_quantity, 
            NVL (apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0) averagecost , --added for version 1.1
			--nvl ( xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0) averagecost,--commented for version 1.1
			msi.unit_weight weight,
            --xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class(msi.inventory_item_id,msi.organization_id) cat,    
			isr.inv_cat_seg1    
			|| ''.''    
			|| isr.cat cat,    
			--    xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_onhand_inv ( msi.inventory_item_id, msi.organization_id) onhand,  --tms#20160429-00037    by siva  on 04-24-2016    
			nvl(boh.onhand,0) onhand,     --added for version 1.1
			null mtd_sales,    
			null ytd_sales,    
			--xxeis.eis_rs_xxwc_com_util_pkg.get_mtd_sales(msi.inventory_item_id,msi.organization_id,gps.start_date,gps.end_date) mtd_sales,    
			--xxeis.eis_rs_xxwc_com_util_pkg.get_ytd_sales(msi.inventory_item_id,msi.organization_id,gps.year_start_date) ytd_sales,    
			--          xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc (    
			--             msi.inventory_item_id,    
			--             msi.organization_id)    
			--             primary_bin_loc1,  --commented for tms#20160429-00037    by siva  on 04-24-2016    
			xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) primary_bin_loc,  --tms#20160429-00037    by siva  on 04-24-2016    
			--          case    
			--             when (   (xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc (    
			--                          msi.inventory_item_id,    
			--                          msi.organization_id) like    
			--                          (''1-WC%''))    
			--                   or (xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc (    
			--                          msi.inventory_item_id,    
			--                          msi.organization_id) like    
			--                          (''1-ZZZ%''))    
			--                   or (xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc (    
			--                          msi.inventory_item_id,    
			--                          msi.organization_id)    
			--                          is null))    
			--             then    
			--                ''Yes''    
			--             else    
			--                ''No''    
			--          end    
			--             no_bin  -- added for tms#20140501-00151 by mahender on 06-24-2014  --commented for tms#20160429-00037  by siva  on 04-24-2016    
			--                   ,    
			case  --tms#20160429-00037    by siva  on 04-24-2016      
			when ( (xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) like (''1-WC%''))      
			or (xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) like (''1-ZZZ%''))      
			or (xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) is null))      
			then ''Yes''      
			else ''No''    
			end no_bin -- added for tms#20140501-00151 by mahender on 06-24-2014    
			,    
			--          xxeis.eis_rs_xxwc_com_util_pkg.get_alternate_bin_loc (    
			--             msi.inventory_item_id,    
			--             msi.organization_id)    
			--             alternate_bin_loc, --commented --tms#20160429-00037    by siva  on 04-24-2016    
      -- xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_alternate_bin_loc ( msi.inventory_item_id, msi.organization_id) alternate_bin_loc,   --commented for version 1.1
      (SELECT mil.concatenated_segments              
        FROM mtl_item_locations_kfv mil, mtl_secondary_locators msl              
        WHERE     msl.inventory_item_id = msi.inventory_item_id                    
          AND msl.organization_id   = msi.organization_id                    
          AND msl.secondary_locator = mil.inventory_location_id                    
          AND mil.segment1 NOT LIKE ''1-%''
          and rownum=1) alternate_bin_loc,  --Added for version 1.1
			substr (msi.segment1, 3) bin_loc,    
			ood.organization_name location,    
			case      
			when (mcvs.segment1 in (''1'', ''2'', ''3'', ''4'', ''5'', ''6'', ''7'', ''8'', ''9'', ''C'', ''B''))      
			then ''Y''      
			when ( mcvs.segment1    in (''E'')      
			and (min_minmax_quantity = 0      
			and max_minmax_quantity  = 0))      
			then ''N''      
			when ( mcvs.segment1    in (''E'')      
			and (min_minmax_quantity > 0      
			and max_minmax_quantity  > 0))      
			then ''Y''      
			when (mcvs.segment1 in (''N'', ''Z''))        
			--and item_type                                                                                           =''non-stock'')      
			then ''N''      
			else ''N''    
			end stk,
      --xxeis.eis_rs_xxwc_com_util_pkg.get_open_order_status(msi.inventory_item_id, msi.organization_id) open_order,    
			case      
			when nvl (isr.demand, 0) != 0      
			then ''Y''      
			else ''N''    
			end open_order,    
			case      
			when nvl (isr.demand, 0) != 0      
			then ''Y''      
			else ''N''    
			end open_demand, --added    
			mcvs.concatenated_segments category,    
			mcats.category_set_name category_set_name, --primary keys    
			msi.inventory_item_id,    
			msi.organization_id inv_organization_id,    
			ood.organization_id org_organization_id,  
			--  gps.application_id,      
			101 application_id,      
			ood.set_of_books_id,  
			--  gps.set_of_books_id ----------------added by diane 1/23/2014      
      (SELECT MAX(transaction_date)
      FROM apps.rcv_transactions rt
      WHERE rt.destination_type_code = ''INVENTORY''
        AND rt.organization_id         = msi.organization_id
        AND EXISTS
        (SELECT 1
            FROM apps.rcv_shipment_lines rsl
          WHERE item_id              = msi.inventory_item_id
        AND to_organization_id       = msi.organization_id
        AND rsl.shipment_line_id     = rt.shipment_line_id
        AND rsl.shipment_header_id   = rt.shipment_header_id
        AND rsl.po_line_location_id IS NOT NULL ) )last_received_date  -- added version 1.2
      --	xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_last_received_date(msi.inventory_item_id,msi.organization_id) last_received_date --commented for version 1.1
			--descr#flexfield#start    
			--descr#flexfield#end    
			--gl#accountff#start    
			--gl#accountff#end  
			from  mtl_system_items_kfv msi,        
			org_organization_definitions ood,  
			-- gl_period_statuses gps,  
			mtl_categories_kfv mcvs,  
			mtl_item_categories mics,  
			xxeis.eis_xxwc_bin_onhand_tab boh,  
			xxeis.eis_xxwc_po_isr_tab isr, --added    
			xxeis.eis_xxwc_valid_bin_orgs xvbo,  
			mtl_category_sets mcats  
			where 1                   = 1  
			and msi.organization_id   = ood.organization_id  
			and msi.organization_id   = isr.organization_id(+)  
			and msi.inventory_item_id = isr.inventory_item_id(+)  
			--and application_id        = 101  
			--and gps.set_of_books_id   = ood.set_of_books_id  
			--and trunc(sysdate) between gps.start_date and gps.end_date  
			-- and xxeis.eis_rs_xxwc_com_util_pkg.get_bin_loc_flag ( msi.inventory_item_id, msi.organization_id) = ''Y''  
			and msi.organization_id = mics.organization_id(+)  
			and msi.inventory_item_id = mics.inventory_item_id(+)  
			and mics.category_id = mcvs.category_id(+)  
			and mics.category_set_id = mcats.category_set_id(+)          --added  
			and msi.inventory_item_id = boh.inventory_item_id(+)  --added outer joins  for vesion 1.1
			and msi.organization_id  = boh.organization_id(+)    --added outer joins  for vesion 1.1
			and boh.process_id (+)      ='||p_process_id||'  --added outer joins  for vesion 1.1
			and msi.organization_id  = xvbo.organization_id    
			and xvbo.process_id       ='||p_process_id||'  
			'||l_category_set||'  
			'||l_segment1_from||'  
			'||l_segment2_from||'  
			'||l_segment3_from||'  
			'||l_segment4_from||'  
			'||l_segment1_to||'  
			'||l_segment2_to||'  
			'||l_segment3_to||'  
			'||l_segment4_to||'';
ELSE
		  L_STMT4:= 'select /*+ INDEX(mcvs MTL_CATEGORIES_B_U1) INDEX(boh EIS_XXWC_BIN_ONHAND_TAB_N3)*/ -- added version 1.2
		  '||P_PROCESS_ID||' process_id,--tms#20160429-00037    by siva  on 04-24-2016    
		ood.organization_code organization,    
		msi.segment1 part_number,    
		msi.description,    
		msi.primary_uom_code uom,    
		msi.list_price_per_unit selling_price, 
        --xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id) vendor_name,    
		--xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number(msi.inventory_item_id,msi.organization_id) vendor_number,    
		isr.vendor_name vendor_name,    
		isr.vendor_num vendor_number,    
		min_minmax_quantity,    
		max_minmax_quantity,  
    	NVL (apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0) averagecost , --Added for version 1.1
    	--nvl ( xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0) averagecost, --commented for version 1.1
		msi.unit_weight weight,
    	--xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class(msi.inventory_item_id,msi.organization_id) cat,    
		isr.inv_cat_seg1    
		|| ''.''    
		|| isr.cat cat,  
		--    xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_onhand_inv ( msi.inventory_item_id, msi.organization_id) onhand,  --tms#20160429-00037    by siva  on 04-24-2016   
		nvl(boh.onhand,0) onhand, --added for version 1.1      
		null mtd_sales,    
		null ytd_sales,
        --xxeis.eis_rs_xxwc_com_util_pkg.get_mtd_sales(msi.inventory_item_id,msi.organization_id,gps.start_date,gps.end_date) mtd_sales,    
		--xxeis.eis_rs_xxwc_com_util_pkg.get_ytd_sales(msi.inventory_item_id,msi.organization_id,gps.year_start_date) ytd_sales,    
		--          xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc (    
		--             msi.inventory_item_id,    
		--             msi.organization_id)    
		--             primary_bin_loc1,  --commented for tms#20160429-00037    by siva  on 04-24-2016    
		xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) primary_bin_loc,  --tms#20160429-00037    by siva  on 04-24-2016    
		--          case    
		--             when (   (xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc (    
		--                          msi.inventory_item_id,    
		--                          msi.organization_id) like    
		--                          (''1-WC%''))    
		--                   or (xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc (    
		--                          msi.inventory_item_id,    
		--                          msi.organization_id) like    
		--                          (''1-ZZZ%''))    
		--                   or (xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc (    
		--                          msi.inventory_item_id,    
		--                          msi.organization_id)    
		--                          is null))    
		--             then    
		--                ''Yes''    
		--             else    
		--                ''No''    
		--          end    
		--             no_bin  -- added for tms#20140501-00151 by mahender on 06-24-2014      
		--                   ,    
		case        
		when ( (xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) like (''1-WC%''))      
		or (xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) like (''1-ZZZ%''))      
		or (xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) is null))      
		then ''Yes''      
		else ''No''    
		end no_bin -- added for tms#20140501-00151 by mahender on 06-24-2014    
		,    
		--          xxeis.eis_rs_xxwc_com_util_pkg.get_alternate_bin_loc (    
		--             msi.inventory_item_id,    
		--             msi.organization_id)    
		--             alternate_bin_loc,    
		-- xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_alternate_bin_loc ( msi.inventory_item_id, msi.organization_id) alternate_bin_loc,   --tms#20160429-00037    by siva  on 04-24-2016    
    	(SELECT mil.concatenated_segments              
          FROM mtl_item_locations_kfv mil, mtl_secondary_locators msl              
          WHERE     msl.inventory_item_id = msi.inventory_item_id                    
          	AND msl.organization_id   = msi.organization_id                    
          	AND msl.secondary_locator = mil.inventory_location_id                    
          	AND mil.segment1 NOT LIKE ''1-%''
          	and rownum=1) alternate_bin_loc,
		substr (msi.segment1, 3) bin_loc,    
		ood.organization_name location,    
		case      
		when (mcvs.segment1 in (''1'', ''2'', ''3'', ''4'', ''5'', ''6'', ''7'', ''8'', ''9'', ''c'', ''b''))      
		then ''Y''      
		when ( mcvs.segment1    in (''E'')      
		and (min_minmax_quantity = 0      
		and max_minmax_quantity  = 0))      
		then ''N''      
		when ( mcvs.segment1    in (''E'')      
		and (min_minmax_quantity > 0      
		and max_minmax_quantity  > 0))      
		then ''Y''      
		when (mcvs.segment1 in (''N'', ''Z''))        
		--and item_type                                                                                           =''non-stock'')      
		then ''N''      
		else ''N''    
		end stk,
    	--xxeis.eis_rs_xxwc_com_util_pkg.get_open_order_status(msi.inventory_item_id, msi.organization_id) open_order,    
		case      
		when nvl (isr.demand, 0) != 0      
		then ''Y''      
		else ''N''    
		end open_order,    
		case      
		when nvl (isr.demand, 0) != 0      
		then ''Y''      
		else ''N''    
		end open_demand, --added    
		mcvs.concatenated_segments category,    
		mcats.category_set_name category_set_name, --primary keys    
		msi.inventory_item_id,    
		msi.organization_id inv_organization_id,    
		ood.organization_id org_organization_id,  
		--  gps.application_id,    
		101 application_id,    
		ood.set_of_books_id,  
		--  gps.set_of_books_id ----------------added by diane 1/23/2014    
     (SELECT MAX(transaction_date)
      FROM apps.rcv_transactions rt
      WHERE rt.destination_type_code = ''INVENTORY''
        AND rt.organization_id         = msi.organization_id
        AND EXISTS
        (SELECT 1
            FROM apps.rcv_shipment_lines rsl
          WHERE item_id              = msi.inventory_item_id
        AND to_organization_id       = msi.organization_id
        AND rsl.shipment_line_id     = rt.shipment_line_id
        AND rsl.shipment_header_id   = rt.shipment_header_id
        AND rsl.po_line_location_id IS NOT NULL ) )last_received_date -- added version 1.2
    	--	xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_last_received_date(msi.inventory_item_id,msi.organization_id) last_received_date --commented for version 1.1
		--descr#flexfield#start    
		--descr#flexfield#end    
		--gl#accountff#start    
		--gl#accountff#end  
		from mtl_system_items_kfv msi,    
		org_organization_definitions ood,   
		-- gl_period_statuses gps,    
		mtl_categories_kfv mcvs,    
		mtl_item_categories mics,    
		xxeis.eis_xxwc_bin_onhand_tab boh,    
		xxeis.eis_xxwc_po_isr_tab isr, --added    
		xxeis.eis_xxwc_bin_orgs_tab ftab, --- added    
		xxeis.eis_xxwc_valid_bin_orgs xvbo,    
		mtl_category_sets mcats  
		where 1                   = 1  
		and msi.organization_id   = ood.organization_id  
		and msi.organization_id   = isr.organization_id(+)  
		and msi.inventory_item_id = isr.inventory_item_id(+) 
		-- and application_id        = 101 
		-- and gps.set_of_books_id   = ood.set_of_books_id 
		-- and trunc(sysdate) between gps.start_date and gps.end_date 
		-- and xxeis.eis_rs_xxwc_com_util_pkg.get_bin_loc_flag ( msi.inventory_item_id, msi.organization_id) = ''Y''  
		and msi.organization_id = mics.organization_id(+)  
		and msi.inventory_item_id = mics.inventory_item_id(+)  
		and mics.category_id = mcvs.category_id(+)  
		and mics.category_set_id = mcats.category_set_id(+)          --added  
		and msi.inventory_item_id = boh.inventory_item_id (+) --added outer joins  for vesion 1.1
		and msi.organization_id  = boh.organization_id (+)  --added outer joins  for vesion 1.1
		and boh.process_id (+)   ='||p_process_id||'  --added outer joins  for vesion 1.1
		and msi.inventory_item_id  = ftab.inventory_item_id  
		and msi.organization_id    = ftab.organization_id  
		and ftab.process_id       ='||p_process_id||'  
		and msi.organization_id  = xvbo.organization_id    
		and xvbo.process_id       ='||p_process_id||'   
		'||l_category_set||'    
		'||l_segment1_from||'    
		'||l_segment2_from||'    
		'||l_segment3_from||'    
		'||l_segment4_from||'    
		'||l_segment1_to||'    
		'||l_segment2_to||'    
		'||l_segment3_to||'    
		'||l_segment4_to||'    
		';
END IF;
END IF;

IF P_QUANTITY_ON_HAND != 'ALL' THEN  -- Added for version 1.1
--Added below query for version 1.1
IF XXEIS.EIS_XXWC_BIN_LOCATION_PKG.GET_START_BIN IS NULL AND XXEIS.EIS_XXWC_BIN_LOCATION_PKG.GET_END_BIN IS NULL THEN
			  l_stmt4   := 'select /*+ INDEX(mcvs MTL_CATEGORIES_B_U1) INDEX(boh EIS_XXWC_BIN_ONHAND_TAB_N3)*/ -- added version 1.2
			  '||p_process_id||' process_id,    
			ood.organization_code organization,    
			msi.segment1 part_number,    
			msi.description,    
			msi.primary_uom_code uom,    
			msi.list_price_per_unit selling_price, 
			--xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id) vendor_name,    
			--xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number(msi.inventory_item_id,msi.organization_id) vendor_number,    
			isr.vendor_name vendor_name,    
			isr.vendor_num vendor_number,    
			min_minmax_quantity,    
			max_minmax_quantity, 
            NVL (apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0) averagecost , --added for version 1.1
			--nvl ( xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0) averagecost,--commented for version 1.1
			msi.unit_weight weight,
            --xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class(msi.inventory_item_id,msi.organization_id) cat,    
			isr.inv_cat_seg1    
			|| ''.''    
			|| isr.cat cat,    
			--    xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_onhand_inv ( msi.inventory_item_id, msi.organization_id) onhand,  --tms#20160429-00037    by siva  on 04-24-2016    
			nvl(boh.onhand,0) onhand,     --added for version 1.1
			null mtd_sales,    
			null ytd_sales,    
			--xxeis.eis_rs_xxwc_com_util_pkg.get_mtd_sales(msi.inventory_item_id,msi.organization_id,gps.start_date,gps.end_date) mtd_sales,    
			--xxeis.eis_rs_xxwc_com_util_pkg.get_ytd_sales(msi.inventory_item_id,msi.organization_id,gps.year_start_date) ytd_sales,    
			--          xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc (    
			--             msi.inventory_item_id,    
			--             msi.organization_id)    
			--             primary_bin_loc1,  --commented for tms#20160429-00037    by siva  on 04-24-2016    
			xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) primary_bin_loc,  --tms#20160429-00037    by siva  on 04-24-2016    
			--          case    
			--             when (   (xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc (    
			--                          msi.inventory_item_id,    
			--                          msi.organization_id) like    
			--                          (''1-WC%''))    
			--                   or (xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc (    
			--                          msi.inventory_item_id,    
			--                          msi.organization_id) like    
			--                          (''1-ZZZ%''))    
			--                   or (xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc (    
			--                          msi.inventory_item_id,    
			--                          msi.organization_id)    
			--                          is null))    
			--             then    
			--                ''Yes''    
			--             else    
			--                ''No''    
			--          end    
			--             no_bin  -- added for tms#20140501-00151 by mahender on 06-24-2014  --commented for tms#20160429-00037  by siva  on 04-24-2016    
			--                   ,    
			case  --tms#20160429-00037    by siva  on 04-24-2016      
			when ( (xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) like (''1-WC%''))      
			or (xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) like (''1-ZZZ%''))      
			or (xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) is null))      
			then ''Yes''      
			else ''No''    
			end no_bin -- added for tms#20140501-00151 by mahender on 06-24-2014    
			,    
			--          xxeis.eis_rs_xxwc_com_util_pkg.get_alternate_bin_loc (    
			--             msi.inventory_item_id,    
			--             msi.organization_id)    
			--             alternate_bin_loc, --commented --tms#20160429-00037    by siva  on 04-24-2016    
      -- xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_alternate_bin_loc ( msi.inventory_item_id, msi.organization_id) alternate_bin_loc,   --commented for version 1.1
      (SELECT mil.concatenated_segments              
        FROM mtl_item_locations_kfv mil, mtl_secondary_locators msl              
        WHERE     msl.inventory_item_id = msi.inventory_item_id                    
          AND msl.organization_id   = msi.organization_id                    
          AND msl.secondary_locator = mil.inventory_location_id                    
          AND mil.segment1 NOT LIKE ''1-%''
          and rownum=1) alternate_bin_loc,  --Added for version 1.1
			substr (msi.segment1, 3) bin_loc,    
			ood.organization_name location,    
			case      
			when (mcvs.segment1 in (''1'', ''2'', ''3'', ''4'', ''5'', ''6'', ''7'', ''8'', ''9'', ''C'', ''B''))      
			then ''Y''      
			when ( mcvs.segment1    in (''E'')      
			and (min_minmax_quantity = 0      
			and max_minmax_quantity  = 0))      
			then ''N''      
			when ( mcvs.segment1    in (''E'')      
			and (min_minmax_quantity > 0      
			and max_minmax_quantity  > 0))      
			then ''Y''      
			when (mcvs.segment1 in (''N'', ''Z''))        
			--and item_type                                                                                           =''non-stock'')      
			then ''N''      
			else ''N''    
			end stk,
      --xxeis.eis_rs_xxwc_com_util_pkg.get_open_order_status(msi.inventory_item_id, msi.organization_id) open_order,    
			case      
			when nvl (isr.demand, 0) != 0      
			then ''Y''      
			else ''N''    
			end open_order,    
			case      
			when nvl (isr.demand, 0) != 0      
			then ''Y''      
			else ''N''    
			end open_demand, --added    
			mcvs.concatenated_segments category,    
			mcats.category_set_name category_set_name, --primary keys    
			msi.inventory_item_id,    
			msi.organization_id inv_organization_id,    
			ood.organization_id org_organization_id,  
			--  gps.application_id,      
			101 application_id,      
			ood.set_of_books_id,  
			--  gps.set_of_books_id ----------------added by diane 1/23/2014  
      (SELECT MAX(transaction_date)
      FROM apps.rcv_transactions rt
      WHERE rt.destination_type_code = ''INVENTORY''
        AND rt.organization_id         = msi.organization_id
        AND EXISTS
        (SELECT 1
            FROM apps.rcv_shipment_lines rsl
          WHERE item_id              = msi.inventory_item_id
        AND to_organization_id       = msi.organization_id
        AND rsl.shipment_line_id     = rt.shipment_line_id
        AND rsl.shipment_header_id   = rt.shipment_header_id
        AND rsl.po_line_location_id IS NOT NULL ) )last_received_date -- added version 1.2
      		--	xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_last_received_date(msi.inventory_item_id,msi.organization_id) last_received_date --commented for version 1.1
			--descr#flexfield#start    
			--descr#flexfield#end    
			--gl#accountff#start    
			--gl#accountff#end  
			from  mtl_system_items_kfv msi,        
			org_organization_definitions ood,  
			-- gl_period_statuses gps,  
			mtl_categories_kfv mcvs,  
			mtl_item_categories mics,  
			xxeis.eis_xxwc_bin_onhand_tab boh,  
			xxeis.eis_xxwc_po_isr_tab isr, --added    
			xxeis.eis_xxwc_valid_bin_orgs xvbo,  
			mtl_category_sets mcats  
			where 1                   = 1  
			and msi.organization_id   = ood.organization_id  
			and msi.organization_id   = isr.organization_id(+)  
			and msi.inventory_item_id = isr.inventory_item_id(+)  
			--and application_id        = 101  
			--and gps.set_of_books_id   = ood.set_of_books_id  
			--and trunc(sysdate) between gps.start_date and gps.end_date  
			-- and xxeis.eis_rs_xxwc_com_util_pkg.get_bin_loc_flag ( msi.inventory_item_id, msi.organization_id) = ''Y''  
			and msi.organization_id = mics.organization_id(+)  
			and msi.inventory_item_id = mics.inventory_item_id(+)  
			and mics.category_id = mcvs.category_id(+)  
			and mics.category_set_id = mcats.category_set_id(+)          --added  
			and msi.inventory_item_id = boh.inventory_item_id
			and msi.organization_id  = boh.organization_id
			and boh.process_id       ='||p_process_id||'  
			and msi.organization_id  = xvbo.organization_id    
			and xvbo.process_id       ='||p_process_id||'  
			'||l_category_set||'  
			'||l_segment1_from||'  
			'||l_segment2_from||'  
			'||l_segment3_from||'  
			'||l_segment4_from||'  
			'||l_segment1_to||'  
			'||l_segment2_to||'  
			'||l_segment3_to||'  
			'||l_segment4_to||'';
ELSE
		  L_STMT4:= 'select /*+ INDEX(mcvs MTL_CATEGORIES_B_U1) INDEX(boh EIS_XXWC_BIN_ONHAND_TAB_N3)*/ -- added version 1.2
		  '||P_PROCESS_ID||' process_id,--tms#20160429-00037    by siva  on 04-24-2016    
		ood.organization_code organization,    
		msi.segment1 part_number,    
		msi.description,    
		msi.primary_uom_code uom,    
		msi.list_price_per_unit selling_price, 
        --xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id) vendor_name,    
		--xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number(msi.inventory_item_id,msi.organization_id) vendor_number,    
		isr.vendor_name vendor_name,    
		isr.vendor_num vendor_number,    
		min_minmax_quantity,    
		max_minmax_quantity,  
    	NVL (apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0) averagecost , --Added for version 1.1
    	--nvl ( xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0) averagecost, --commented for version 1.1
		msi.unit_weight weight,
    	--xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class(msi.inventory_item_id,msi.organization_id) cat,    
		isr.inv_cat_seg1    
		|| ''.''    
		|| isr.cat cat,  
		--    xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_onhand_inv ( msi.inventory_item_id, msi.organization_id) onhand,  --tms#20160429-00037    by siva  on 04-24-2016   
		nvl(boh.onhand,0) onhand, --added for version 1.1      
		null mtd_sales,    
		null ytd_sales,
        --xxeis.eis_rs_xxwc_com_util_pkg.get_mtd_sales(msi.inventory_item_id,msi.organization_id,gps.start_date,gps.end_date) mtd_sales,    
		--xxeis.eis_rs_xxwc_com_util_pkg.get_ytd_sales(msi.inventory_item_id,msi.organization_id,gps.year_start_date) ytd_sales,    
		--          xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc (    
		--             msi.inventory_item_id,    
		--             msi.organization_id)    
		--             primary_bin_loc1,  --commented for tms#20160429-00037    by siva  on 04-24-2016    
		xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) primary_bin_loc,  --tms#20160429-00037    by siva  on 04-24-2016    
		--          case    
		--             when (   (xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc (    
		--                          msi.inventory_item_id,    
		--                          msi.organization_id) like    
		--                          (''1-WC%''))    
		--                   or (xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc (    
		--                          msi.inventory_item_id,    
		--                          msi.organization_id) like    
		--                          (''1-ZZZ%''))    
		--                   or (xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc (    
		--                          msi.inventory_item_id,    
		--                          msi.organization_id)    
		--                          is null))    
		--             then    
		--                ''Yes''    
		--             else    
		--                ''No''    
		--          end    
		--             no_bin  -- added for tms#20140501-00151 by mahender on 06-24-2014      
		--                   ,    
		case        
		when ( (xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) like (''1-WC%''))      
		or (xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) like (''1-ZZZ%''))      
		or (xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) is null))      
		then ''Yes''      
		else ''No''    
		end no_bin -- added for tms#20140501-00151 by mahender on 06-24-2014    
		,    
		--          xxeis.eis_rs_xxwc_com_util_pkg.get_alternate_bin_loc (    
		--             msi.inventory_item_id,    
		--             msi.organization_id)    
		--             alternate_bin_loc,    
		-- xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_alternate_bin_loc ( msi.inventory_item_id, msi.organization_id) alternate_bin_loc,   --tms#20160429-00037    by siva  on 04-24-2016    
    	(SELECT mil.concatenated_segments              
          FROM mtl_item_locations_kfv mil, mtl_secondary_locators msl              
          WHERE     msl.inventory_item_id = msi.inventory_item_id                    
          	AND msl.organization_id   = msi.organization_id                    
          	AND msl.secondary_locator = mil.inventory_location_id                    
          	AND mil.segment1 NOT LIKE ''1-%''
          	and rownum=1) alternate_bin_loc,
		substr (msi.segment1, 3) bin_loc,    
		ood.organization_name location,    
		case      
		when (mcvs.segment1 in (''1'', ''2'', ''3'', ''4'', ''5'', ''6'', ''7'', ''8'', ''9'', ''c'', ''b''))      
		then ''Y''      
		when ( mcvs.segment1    in (''E'')      
		and (min_minmax_quantity = 0      
		and max_minmax_quantity  = 0))      
		then ''N''      
		when ( mcvs.segment1    in (''E'')      
		and (min_minmax_quantity > 0      
		and max_minmax_quantity  > 0))      
		then ''Y''      
		when (mcvs.segment1 in (''N'', ''Z''))        
		--and item_type                                                                                           =''non-stock'')      
		then ''N''      
		else ''N''    
		end stk,
    	--xxeis.eis_rs_xxwc_com_util_pkg.get_open_order_status(msi.inventory_item_id, msi.organization_id) open_order,    
		case      
		when nvl (isr.demand, 0) != 0      
		then ''Y''      
		else ''N''    
		end open_order,    
		case      
		when nvl (isr.demand, 0) != 0      
		then ''Y''      
		else ''N''    
		end open_demand, --added    
		mcvs.concatenated_segments category,    
		mcats.category_set_name category_set_name, --primary keys    
		msi.inventory_item_id,    
		msi.organization_id inv_organization_id,    
		ood.organization_id org_organization_id,  
		--  gps.application_id,    
		101 application_id,    
		ood.set_of_books_id,  
		--  gps.set_of_books_id ----------------added by diane 1/23/2014    
    (SELECT MAX(transaction_date)
      FROM apps.rcv_transactions rt
      WHERE rt.destination_type_code = ''INVENTORY''
        AND rt.organization_id         = msi.organization_id
        AND EXISTS
        (SELECT 1
            FROM apps.rcv_shipment_lines rsl
          WHERE item_id              = msi.inventory_item_id
        AND to_organization_id       = msi.organization_id
        AND rsl.shipment_line_id     = rt.shipment_line_id
        AND rsl.shipment_header_id   = rt.shipment_header_id
        AND rsl.po_line_location_id IS NOT NULL ) )last_received_date -- added version 1.2
    	--	xxeis.EIS_XXWC_BIN_LOCATION_PKG.get_last_received_date(msi.inventory_item_id,msi.organization_id) last_received_date --commented for version 1.1
		--descr#flexfield#start    
		--descr#flexfield#end    
		--gl#accountff#start    
		--gl#accountff#end  
		from mtl_system_items_kfv msi,    
		org_organization_definitions ood,   
		-- gl_period_statuses gps,    
		mtl_categories_kfv mcvs,    
		mtl_item_categories mics,    
		xxeis.eis_xxwc_bin_onhand_tab boh,    
		xxeis.eis_xxwc_po_isr_tab isr, --added    
		xxeis.eis_xxwc_bin_orgs_tab ftab, --- added    
		xxeis.eis_xxwc_valid_bin_orgs xvbo,    
		mtl_category_sets mcats  
		where 1                   = 1  
		and msi.organization_id   = ood.organization_id  
		and msi.organization_id   = isr.organization_id(+)  
		and msi.inventory_item_id = isr.inventory_item_id(+) 
		-- and application_id        = 101 
		-- and gps.set_of_books_id   = ood.set_of_books_id 
		-- and trunc(sysdate) between gps.start_date and gps.end_date 
		-- and xxeis.eis_rs_xxwc_com_util_pkg.get_bin_loc_flag ( msi.inventory_item_id, msi.organization_id) = ''Y''  
		and msi.organization_id = mics.organization_id(+)  
		and msi.inventory_item_id = mics.inventory_item_id(+)  
		and mics.category_id = mcvs.category_id(+)  
		and mics.category_set_id = mcats.category_set_id(+)          --added  
		and msi.inventory_item_id = boh.inventory_item_id 
		and msi.organization_id  = boh.organization_id 
		and boh.process_id    ='||p_process_id||' 
		and msi.inventory_item_id  = ftab.inventory_item_id  
		and msi.organization_id    = ftab.organization_id  
		and ftab.process_id       ='||p_process_id||'  
		and msi.organization_id  = xvbo.organization_id    
		and xvbo.process_id       ='||p_process_id||'   
		'||l_category_set||'    
		'||l_segment1_from||'    
		'||l_segment2_from||'    
		'||l_segment3_from||'    
		'||l_segment4_from||'    
		'||l_segment1_to||'    
		'||l_segment2_to||'    
		'||l_segment3_to||'    
		'||l_segment4_to||'    
		';
END IF;
END IF;

FND_FILE.PUT_LINE(FND_FILE.LOG,'l_stmt4'||l_stmt4);

OPEN l_ref_cursor4 FOR l_stmt4 ;
LOOP
  FETCH l_ref_cursor4 bulk collect INTO bin_data_info_tab limit 10000;
  IF bin_data_info_tab.count >= 1 THEN
    forall j IN 1 .. bin_data_info_tab.count
    INSERT INTO xxeis.EIS_XXWC_BIN_LOC_DATA_TAB VALUES bin_data_info_tab(j);
    --COMMIT; --Commented for version 1.3 
  END IF;
  IF L_REF_CURSOR4%NOTFOUND THEN
    CLOSE l_ref_cursor4;
    EXIT;
  END IF;
END LOOP;
--COMMIT; --Commented for version 1.3 
END GET_BIN_LOCATION_INFO;

/* --Commented code for version 1.3  
PROCEDURE CLEAR_TEMP_TABLES
  (
    P_PROCESS_ID IN NUMBER
  )
AS
--//============================================================================
--//
--// Object Name         :: CLEAR_TEMP_TABLES  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure will call in Afteer report and delete data from Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  				05-May-2016 	Initial Build   
--//============================================================================
BEGIN
  DELETE FROM XXEIS.EIS_XXWC_VALID_BIN_ORGS WHERE PROCESS_ID=P_PROCESS_ID;
  DELETE FROM XXEIS.EIS_XXWC_BIN_ORGS_TAB WHERE PROCESS_ID=P_PROCESS_ID;
  DELETE FROM XXEIS.EIS_XXWC_BIN_ONHAND_TAB WHERE PROCESS_ID=P_PROCESS_ID;
  DELETE FROM XXEIS.EIS_XXWC_BIN_LOC_DATA_TAB WHERE PROCESS_ID=P_PROCESS_ID;
  COMMIT;
END ;
*/ --Commented code for version 1.3

END EIS_XXWC_BIN_LOCATION_PKG;
/
