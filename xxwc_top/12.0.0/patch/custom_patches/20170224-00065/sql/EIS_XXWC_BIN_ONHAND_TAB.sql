---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_BIN_ONHAND_TAB $
  Module Name : Inventory
  PURPOSE	  : Bin Location
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0 	  18-May-2016         Siva			  TMS#20160429-00037
  1.1     15-Mar-2017      	  Siva   		 TMS#20170224-00065
**************************************************************************************************************/
DROP TABLE XXEIS.EIS_XXWC_BIN_ONHAND_TAB CASCADE CONSTRAINTS  --added for version 1.1
/

CREATE GLOBAL TEMPORARY TABLE XXEIS.EIS_XXWC_BIN_ONHAND_TAB --added for version 1.1
  (
    PROCESS_ID        NUMBER,
    ONHAND            NUMBER,
    INVENTORY_ITEM_ID NUMBER,
    ORGANIZATION_ID   NUMBER
  )
ON COMMIT PRESERVE ROWS  --added for version 1.1
/
