--Report Name            : White Cap Credit Memo Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_AR_CREDIT_MEMO_DTLS_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_AR_CREDIT_MEMO_DTLS_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V',222,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Ar Credit Memo Dtls V','EXACMDV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_AR_CREDIT_MEMO_DTLS_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_AR_CREDIT_MEMO_DTLS_V',222,FALSE);
--Inserting Object Columns for EIS_XXWC_AR_CREDIT_MEMO_DTLS_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','TAKEN_BY',222,'Taken By','TAKEN_BY','','','','ANONYMOUS','VARCHAR2','','','Taken By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','MASTER_NAME',222,'Master Name','MASTER_NAME','','','','ANONYMOUS','VARCHAR2','','','Master Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','ANONYMOUS','VARCHAR2','','','Customer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','CUSTOMER_TERRITORY',222,'Customer Territory','CUSTOMER_TERRITORY','','','','ANONYMOUS','VARCHAR2','','','Customer Territory','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','SALES_REP_NO',222,'Sales Rep No','SALES_REP_NO','','','','ANONYMOUS','VARCHAR2','','','Sales Rep No','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','QTY_SHIPPED',222,'Qty Shipped','QTY_SHIPPED','','~T~D~2','','ANONYMOUS','NUMBER','','','Qty Shipped','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','INVOICE_LINE_AMOUNT',222,'Invoice Line Amount','INVOICE_LINE_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','','','Invoice Line Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','CREDIT_REASON_SALE',222,'Credit Reason Sale','CREDIT_REASON_SALE','','','','ANONYMOUS','VARCHAR2','','','Credit Reason Sale','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','PART_NUMBER',222,'Part Number','PART_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Part Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','ORIGINAL_INVOICE_DATE',222,'Original Invoice Date','ORIGINAL_INVOICE_DATE','','','','ANONYMOUS','DATE','','','Original Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','ORIGINAL_INVOICE_NUMBER',222,'Original Invoice Number','ORIGINAL_INVOICE_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Original Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','ORDER_NUMBER',222,'Order Number','ORDER_NUMBER','','','','ANONYMOUS','NUMBER','','','Order Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','INVOICE_NUMBER',222,'Invoice Number','INVOICE_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','TAX_AMT',222,'Tax Amt','TAX_AMT','','~T~D~2','','ANONYMOUS','NUMBER','','','Tax Amt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','AMOUNT_PAID',222,'Amount Paid','AMOUNT_PAID','','~T~D~2','','ANONYMOUS','NUMBER','','','Amount Paid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','LOCATION',222,'Location','LOCATION','','','','ANONYMOUS','VARCHAR2','','','Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','INVOICE_TOTAL',222,'Invoice Total','INVOICE_TOTAL','','~T~D~2','','ANONYMOUS','NUMBER','','','Invoice Total','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','FREIGHT_REMAINING',222,'Freight Remaining','FREIGHT_REMAINING','','~T~D~2','','ANONYMOUS','NUMBER','','','Freight Remaining','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','FREIGHT_ORIGINAL',222,'Freight Original','FREIGHT_ORIGINAL','','~T~D~2','','ANONYMOUS','NUMBER','','','Freight Original','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','TAX_RATE',222,'Tax Rate','TAX_RATE','','~T~D~2','','ANONYMOUS','NUMBER','','','Tax Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','BUSINESS_DATE',222,'Business Date','BUSINESS_DATE','','','','ANONYMOUS','DATE','','','Business Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','INVOICE_DATE',222,'Invoice Date','INVOICE_DATE','','','','ANONYMOUS','DATE','','','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','SHIP_NAME',222,'Ship Name','SHIP_NAME','','','','ANONYMOUS','VARCHAR2','','','Ship Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Customer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','FISCAL_MONTH',222,'Fiscal Month','FISCAL_MONTH','','','','ANONYMOUS','VARCHAR2','','','Fiscal Month','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','FISCAL_YEAR',222,'Fiscal Year','FISCAL_YEAR','','','','ANONYMOUS','NUMBER','','','Fiscal Year','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','ORIGIN_DATE',222,'Origin Date','ORIGIN_DATE','','','','ANONYMOUS','DATE','','','Origin Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','RESTOCK_FEE_PERCENT',222,'Restock Fee Percent','RESTOCK_FEE_PERCENT','','~T~D~2','','ANONYMOUS','NUMBER','','','Restock Fee Percent','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','AR_CUSTOMER_NUMBER',222,'Ar Customer Number','AR_CUSTOMER_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Ar Customer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','INVOICE_TYPE',222,'Invoice Type','INVOICE_TYPE','','','','ANONYMOUS','VARCHAR2','','','Invoice Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','ORDER_DATE',222,'Order Date','ORDER_DATE','','','','ANONYMOUS','DATE','','','Order Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','SHIP_DATE',222,'Ship Date','SHIP_DATE','','','','ANONYMOUS','DATE','','','Ship Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','CREATION_DATE',222,'Creation Date','CREATION_DATE','','','','ANONYMOUS','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','LINE_TYPE',222,'Line Type','LINE_TYPE','','','','ANONYMOUS','VARCHAR2','','','Line Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','ORGINAL_ORDER_NUMBER',222,'Orginal Order Number','ORGINAL_ORDER_NUMBER','','','','ANONYMOUS','NUMBER','','','Orginal Order Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','C_LINE_NUMBER',222,'C Line Number','C_LINE_NUMBER','','','','ANONYMOUS','NUMBER','','','C Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','O_AMOUNT_APPLIED',222,'O Amount Applied','O_AMOUNT_APPLIED','','~T~D~2','','ANONYMOUS','NUMBER','','','O Amount Applied','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','TRX_DATE',222,'Trx Date','TRX_DATE','','','','ANONYMOUS','DATE','','','Trx Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','PROCESS_ID',222,'Process Id','PROCESS_ID','','','','ANONYMOUS','NUMBER','','','Process Id','','','','US');
--Inserting Object Components for EIS_XXWC_AR_CREDIT_MEMO_DTLS_V
--Inserting Object Component Joins for EIS_XXWC_AR_CREDIT_MEMO_DTLS_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report LOV Data for White Cap Credit Memo Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - White Cap Credit Memo Report
xxeis.eis_rsc_ins.lov( 222,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT DISTINCT GPS.PERIOD_NAME
FROM      HR_OPERATING_UNITS          HOU,
          GL_LEDGERS                  GL,
          gl_period_statuses          gps
WHERE
 GL.LEDGER_ID                      = HOU.SET_OF_BOOKS_ID
AND GL.LEDGER_ID                      = GPS.SET_OF_BOOKS_ID
AND GL.ACCOUNTED_PERIOD_TYPE          = GPS.PERIOD_TYPE
order by GPS.PERIOD_NAME','','Fiscal Month','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT DISTINCT GPS.PERIOD_YEAR
FROM      HR_OPERATING_UNITS          HOU,
          GL_LEDGERS                  GL,
          gl_period_statuses          gps
WHERE
 GL.LEDGER_ID                      = HOU.SET_OF_BOOKS_ID
AND GL.LEDGER_ID                      = GPS.SET_OF_BOOKS_ID
AND GL.ACCOUNTED_PERIOD_TYPE          = GPS.PERIOD_TYPE
order by GPS.PERIOD_YEAR
','','Fiscal Year','','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report Data for White Cap Credit Memo Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - White Cap Credit Memo Report
xxeis.eis_rsc_utility.delete_report_rows( 'White Cap Credit Memo Report' );
--Inserting Report - White Cap Credit Memo Report
xxeis.eis_rsc_ins.r( 222,'White Cap Credit Memo Report','','The purpose of this report is to provide White Cap District Managers with data regarding customer credit activities for their approvals on Credit Memos.  The report serves to provide SOX control/compliance.','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - White Cap Credit Memo Report
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'AMOUNT_PAID','Amount Paid','Amount Paid','','~T~D~2','default','','16','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'BUSINESS_DATE','Business Date','Business Date','','','default','','8','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'CREDIT_REASON_SALE','Credit Reason Sale','Credit Reason Sale','','','default','','27','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','32','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'CUSTOMER_TERRITORY','Customer Territory','Customer Territory','','','default','','31','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'FISCAL_MONTH','Fiscal Month','Fiscal Month','','','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'FISCAL_YEAR','Fiscal Year','Fiscal Year','','~~~','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'FREIGHT_ORIGINAL','Freight Original','Freight Original','','~T~D~2','default','','10','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'FREIGHT_REMAINING','Freight Remaining','Freight Remaining','','~T~D~2','default','','11','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','7','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'INVOICE_LINE_AMOUNT','Invoice Line Amount','Invoice Line Amount','','~T~D~2','default','','28','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','18','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'INVOICE_TOTAL','Invoice Total','Invoice Total','','~T~D~2','default','','12','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'LOCATION','Location','Location','','','default','','15','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'MASTER_NAME','Master Name','Master Name','','','default','','33','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','19','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'ORIGINAL_INVOICE_DATE','Original Invoice Date','Original Invoice Date','','','default','','25','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'ORIGINAL_INVOICE_NUMBER','Original Invoice Number','Original Invoice Number','','','default','','24','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'PART_NUMBER','Part Number','Part Number','','','default','','26','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'QTY_SHIPPED','Qty Shipped','Qty Shipped','','~~~','default','','29','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'SALES_REP_NO','Sales Rep No','Sales Rep No','','','default','','30','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'SHIP_NAME','Ship Name','Ship Name','','','default','','5','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'TAKEN_BY','Taken By','Taken By','','','default','','34','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'TAX_AMT','Tax Amt','Tax Amt','','~T~D~2','default','','17','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'TAX_RATE','Tax Rate','Tax Rate','','~T~D~2','default','','9','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'ORIGIN_DATE','Origin Date','Origin Date','','','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'RESTOCK_FEE_PERCENT','Restock Fee Percent','Restock Fee Percent','','~~~','default','','14','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'AR_CUSTOMER_NUMBER','Ar Customer Number','Ar Customer Number','','','default','','13','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'INVOICE_TYPE','Invoice Type','Invoice Type','','','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'ORDER_DATE','Booked Date','Order Date','','','default','','20','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'SHIP_DATE','Actual Shipment Date','Ship Date','','','default','','22','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'CREATION_DATE','Order Creation Date','Creation Date','','','default','','21','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap Credit Memo Report',222,'LINE_TYPE','Line Type','Line Type','','','default','','23','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','US','');
--Inserting Report Parameters - White Cap Credit Memo Report
xxeis.eis_rsc_ins.rp( 'White Cap Credit Memo Report',222,'Fiscal Year','Fiscal Year','FISCAL_YEAR','IN','Fiscal Year','SELECT distinct period_year FROM GL_PERIOD_STATUSES WHERE  APPLICATION_ID=101 AND CLOSING_STATUS=''O'' and sysdate between start_date and end_date','NUMBER','Y','Y','1','Y','N','SQL','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'White Cap Credit Memo Report',222,'Location','Location','LOCATION','IN','AR Organizaion Code LOV','','VARCHAR2','N','Y','5','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'White Cap Credit Memo Report',222,'Origin Date From','Origin Date From','ORIGIN_DATE','>=','','','DATE','N','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'White Cap Credit Memo Report',222,'Origin Date To','Origin Date To','ORIGIN_DATE','<=','','','DATE','N','Y','4','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'White Cap Credit Memo Report',222,'Fiscal Month','Fiscal Month','','IN','Fiscal Month','SELECT distinct period_name FROM GL_PERIOD_STATUSES WHERE  APPLICATION_ID=101 AND CLOSING_STATUS=''O'' and sysdate between start_date and end_date','VARCHAR2','N','Y','2','Y','N','SQL','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','US','');
--Inserting Dependent Parameters - White Cap Credit Memo Report
--Inserting Report Conditions - White Cap Credit Memo Report
xxeis.eis_rsc_ins.rcnh( 'White Cap Credit Memo Report',222,'ORIGIN_DATE >= :Origin Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORIGIN_DATE','','Origin Date From','','','','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',222,'White Cap Credit Memo Report','ORIGIN_DATE >= :Origin Date From ');
xxeis.eis_rsc_ins.rcnh( 'White Cap Credit Memo Report',222,'ORIGIN_DATE <= :Origin Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORIGIN_DATE','','Origin Date To','','','','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',222,'White Cap Credit Memo Report','ORIGIN_DATE <= :Origin Date To ');
xxeis.eis_rsc_ins.rcnh( 'White Cap Credit Memo Report',222,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND PROCESS_ID = :SYSTEM.PROCESS_ID','1',222,'White Cap Credit Memo Report','Free Text ');
--Inserting Report Sorts - White Cap Credit Memo Report
--Inserting Report Triggers - White Cap Credit Memo Report
xxeis.eis_rsc_ins.rt( 'White Cap Credit Memo Report',222,'begin
XXEIS.EIS_XXWC_CREDIT_MEMO_DET_PKG.POPULATE_CREDIT_INVOICES( P_PROCESS_ID => :SYSTEM.PROCESS_ID,
P_PERIOD_YEAR => :Fiscal Year,
P_PERIOD_MONTH => :Fiscal Month,
P_ORIGIN_DATE_FROM => :Origin Date From,
P_ORIGIN_DATE_TO => :Origin Date To,
p_location => :Location);
end;
','B','Y','XXEIS_RS_ADMIN','AQ');
--inserting report templates - White Cap Credit Memo Report
--Inserting Report Portals - White Cap Credit Memo Report
--inserting report dashboards - White Cap Credit Memo Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'White Cap Credit Memo Report','222','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V','N','');
--inserting report security - White Cap Credit Memo Report
xxeis.eis_rsc_ins.rsec( 'White Cap Credit Memo Report','401','','',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap Credit Memo Report','222','','',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap Credit Memo Report','222','','',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap Credit Memo Report','401','','',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap Credit Memo Report','20005','','',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap Credit Memo Report','222','','RECEIVABLES_MANAGER',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap Credit Memo Report','401','','',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap Credit Memo Report','401','','',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap Credit Memo Report','401','','XXWC_RECEIVING_ASSOCIATE',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap Credit Memo Report','1','','XXWC_RECEIVABLES_TAX',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap Credit Memo Report','222','','HDS_RCVBLS_MNGR',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap Credit Memo Report','222','','HDS_RCVBLS_MNGR_CAN',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap Credit Memo Report','222','','XXWC_RECEIVABLES_INQUIRY_WC',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap Credit Memo Report','401','','XXWC_AO_INV_ADJ_REC',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap Credit Memo Report','','LA023190','',222,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - White Cap Credit Memo Report
--Inserting Report   Version details- White Cap Credit Memo Report
xxeis.eis_rsc_ins.rv( 'White Cap Credit Memo Report','','White Cap Credit Memo Report','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
