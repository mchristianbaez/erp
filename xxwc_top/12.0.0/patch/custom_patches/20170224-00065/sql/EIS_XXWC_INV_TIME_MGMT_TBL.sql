---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_INV_TIME_MGMT_TBL $
  Module Name : Inventory
  PURPOSE	  : Time Sensitive Mgmt - WC
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0 	  15-May-2016         Venu			  TMS#20160315-00168
  1.1     15-Mar-2017      	  Siva   		  TMS#20170224-00065 
**************************************************************************************************************/
DROP TABLE XXEIS.EIS_XXWC_INV_TIME_MGMT_TBL;

CREATE GLOBAL TEMPORARY TABLE XXEIS.EIS_XXWC_INV_TIME_MGMT_TBL
  (
    PROCESS_ID             NUMBER,
    INV_ORG_NAME             VARCHAR2(240) ,
    ORGANIZATION_ID          NUMBER ,
    ORGANIZATION_CODE        VARCHAR2(3) ,
    BUYER                    VARCHAR2(240) ,
    MASTER_ORG_CODE          VARCHAR2(3) ,
    SUBINVENTORY_CODE        VARCHAR2(10) ,
    LOCATOR_ID               NUMBER ,
    LOCATOR                  VARCHAR2(81) ,
    INVENTORY_ITEM_ID        NUMBER ,
    ITEM                     VARCHAR2(40) ,
    ITEM_DESCRIPTION         VARCHAR2(240) ,
    REVISION                 VARCHAR2(3) ,
    UNIT_OF_MEASURE          VARCHAR2(25) ,
    ON_HAND                  NUMBER ,
    PRIMARY_BIN_LOC          VARCHAR2(4000) ,
    ALTERNATE_BIN_LOC        VARCHAR2(4000) ,
    OLDEST_BORN_DATE         DATE ,
    SHELF_LIFE_DAYS          NUMBER ,
    VENDOR_NAME              VARCHAR2(4000) ,
    VENDOR_NUMBER            VARCHAR2(4000) ,
    UNPACKED                 NUMBER ,
    PACKED                   NUMBER ,
    COST_GROUP_ID            NUMBER ,
    LOT_NUMBER               VARCHAR2(80) ,
    SUBINVENTORY_STATUS_ID   NUMBER ,
    LOCATOR_STATUS_ID        NUMBER ,
    LOT_STATUS_ID            NUMBER ,
    PLANNING_TP_TYPE         NUMBER ,
    PLANNING_ORGANIZATION_ID NUMBER ,
    PLANNING_ORGANIZATION    VARCHAR2(240) ,
    OWNING_TP_TYPE           NUMBER ,
    OWNING_ORGANIZATION_ID   NUMBER ,
    OWNING_ORGANIZATION      VARCHAR2(240) ,
    LOT_EXPIRY_DATE          DATE ,
    LPN                      VARCHAR2(30) ,
    LPN_ID                   NUMBER ,
    STATUS_CODE              VARCHAR2(80) ,
    UNIT_NUMBER              VARCHAR2(30) ,
    SUBINVENTORY_TYPE        NUMBER ,
    SECONDARY_INVENTORY_NAME VARCHAR2(10) ,
    DATE_RECEIVED            DATE ,
    ONHAND_QUANTITIES_ID     NUMBER ,
    INVENTORY_LOCATION_ID    NUMBER ,
    LANGUAGE                 VARCHAR2(4) ,
    MIN_MINMAX_QUANTITY      NUMBER ,
    MAX_MINMAX_QUANTITY      NUMBER ,
    LIST_PRICE_PER_UNIT      NUMBER ,
    MP_ORGANIZATION_ID       NUMBER ,
    MIL_ORGANIZATION_ID      NUMBER ,
    MSI_ORGANIZATION_ID      NUMBER ,
    MSIV_INVENTORY_ITEM_ID   NUMBER ,
    MSIV_ORGANIZATION_ID     NUMBER ,
    MMS_STATUS_ID            NUMBER ,
    OOD_ORGANIZATION_ID      NUMBER(15) ,
    OODW_ORGANIZATION_ID     NUMBER(15) ,
    OODP_ORGANIZATION_ID     NUMBER(15) ,
    HAOU_ORGANIZATION_ID     NUMBER(15) ,
    MLN_INVENTORY_ITEM_ID    NUMBER ,
    MLN_ORGANIZATION_ID      NUMBER ,
    MLN_LOT_NUMBER           VARCHAR2(80) ,
    REGION                   VARCHAR2(150) ,
    DAYS_AGED                NUMBER ,
    AT_RISK                  CHAR(1) ,
    AVGCOST                  NUMBER ,
    UBD                      DATE ,
    AT_RISK_N                CHAR(1) ,
    SHORT_DATE               CHAR(1)
  )
ON COMMIT PRESERVE ROWS  --added for version 1.1
/
