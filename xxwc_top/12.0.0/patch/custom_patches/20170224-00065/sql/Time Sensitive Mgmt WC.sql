--Report Name            : Time Sensitive Mgmt - WC
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_XXWC_INV_TIME_MGMT_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_INV_TIME_MGMT_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_INV_TIME_MGMT_V',401,'On-Hand quantities of Inventory Items','','','','ANONYMOUS','XXEIS','Eis Inv Onhand Quantity V','EIOQV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_INV_TIME_MGMT_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_INV_TIME_MGMT_V',401,FALSE);
--Inserting Object Columns for EIS_XXWC_INV_TIME_MGMT_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ORGANIZATION_CODE',401,'Organization Code','ORGANIZATION_CODE','','','','ANONYMOUS','VARCHAR2','','','Organization Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','SUBINVENTORY_CODE',401,'Subinventory Code','SUBINVENTORY_CODE','','','','ANONYMOUS','VARCHAR2','','','Subinventory Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ITEM',401,'Item','ITEM','','','','ANONYMOUS','VARCHAR2','','','Item','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','UNIT_OF_MEASURE',401,'Unit Of Measure','UNIT_OF_MEASURE','','','','ANONYMOUS','VARCHAR2','','','Unit Of Measure','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ON_HAND',401,'On Hand','ON_HAND','','','','ANONYMOUS','NUMBER','','','On Hand','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ALTERNATE_BIN_LOC',401,'Alternate Bin Loc','ALTERNATE_BIN_LOC','','','','ANONYMOUS','VARCHAR2','','','Alternate Bin Loc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','BUYER',401,'Buyer','BUYER','','','','ANONYMOUS','VARCHAR2','','','Buyer','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ITEM_DESCRIPTION',401,'Item Description','ITEM_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OLDEST_BORN_DATE',401,'Oldest Born Date','OLDEST_BORN_DATE','','','','ANONYMOUS','DATE','','','Oldest Born Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','PRIMARY_BIN_LOC',401,'Primary Bin Loc','PRIMARY_BIN_LOC','','','','ANONYMOUS','VARCHAR2','','','Primary Bin Loc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','REGION',401,'Region','REGION','','','','ANONYMOUS','VARCHAR2','','','Region','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','SHELF_LIFE_DAYS',401,'Shelf Life Days','SHELF_LIFE_DAYS','','','','ANONYMOUS','NUMBER','','','Shelf Life Days','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','ANONYMOUS','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','VENDOR_NUMBER',401,'Vendor Number','VENDOR_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','AT_RISK',401,'At Risk','AT_RISK','','','','ANONYMOUS','CHAR','','','At Risk','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','DAYS_AGED',401,'Days Aged','DAYS_AGED','','','','ANONYMOUS','NUMBER','','','Days Aged','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','AVGCOST',401,'Avgcost','AVGCOST','','~T~D~2','','ANONYMOUS','NUMBER','','','Avgcost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','DATE_RECEIVED',401,'Date Received','DATE_RECEIVED','','','','ANONYMOUS','DATE','','','Date Received','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','AT_RISK_N',401,'At Risk N','AT_RISK_N','','','','ANONYMOUS','CHAR','','','At Risk N','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','SHORT_DATE',401,'Short Date','SHORT_DATE','','','','ANONYMOUS','CHAR','','','Short Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','UBD',401,'Ubd','UBD','','','','ANONYMOUS','DATE','','','Ubd','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LOT_NUMBER',401,'Lot Number','LOT_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Lot Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LOT_EXPIRY_DATE',401,'Lot Expiry Date','LOT_EXPIRY_DATE','','','','ANONYMOUS','DATE','','','Lot Expiry Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','STATUS_CODE',401,'Status Code','STATUS_CODE','','','','ANONYMOUS','VARCHAR2','','','Status Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','UNIT_NUMBER',401,'Unit Number','UNIT_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Unit Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','SUBINVENTORY_TYPE',401,'Subinventory Type','SUBINVENTORY_TYPE','','','','ANONYMOUS','NUMBER','','','Subinventory Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','INV_ORG_NAME',401,'Inv Org Name','INV_ORG_NAME','','','','ANONYMOUS','VARCHAR2','','','Inv Org Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MASTER_ORG_CODE',401,'Master Org Code','MASTER_ORG_CODE','','','','ANONYMOUS','VARCHAR2','','','Master Org Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LOCATOR',401,'Locator','LOCATOR','','','','ANONYMOUS','VARCHAR2','','','Locator','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','REVISION',401,'Revision','REVISION','','','','ANONYMOUS','VARCHAR2','','','Revision','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OWNING_ORGANIZATION',401,'Owning Organization','OWNING_ORGANIZATION','','','','ANONYMOUS','VARCHAR2','','','Owning Organization','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','PLANNING_ORGANIZATION',401,'Planning Organization','PLANNING_ORGANIZATION','','','','ANONYMOUS','VARCHAR2','','','Planning Organization','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','UNPACKED',401,'Unpacked','UNPACKED','','','','ANONYMOUS','NUMBER','','','Unpacked','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','PACKED',401,'Packed','PACKED','','','','ANONYMOUS','NUMBER','','','Packed','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MIL_ORGANIZATION_ID',401,'Mil Organization Id','MIL_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Mil Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MMS_STATUS_ID',401,'Mms Status Id','MMS_STATUS_ID','','','','ANONYMOUS','NUMBER','','','Mms Status Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MP_ORGANIZATION_ID',401,'Mp Organization Id','MP_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Mp Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MSIV_INVENTORY_ITEM_ID',401,'Msiv Inventory Item Id','MSIV_INVENTORY_ITEM_ID','','','','ANONYMOUS','NUMBER','','','Msiv Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MSIV_ORGANIZATION_ID',401,'Msiv Organization Id','MSIV_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Msiv Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MSI_ORGANIZATION_ID',401,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Msi Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OODP_ORGANIZATION_ID',401,'Oodp Organization Id','OODP_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Oodp Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OODW_ORGANIZATION_ID',401,'Oodw Organization Id','OODW_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Oodw Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OOD_ORGANIZATION_ID',401,'Ood Organization Id','OOD_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Ood Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MLN_INVENTORY_ITEM_ID',401,'Mln Inventory Item Id','MLN_INVENTORY_ITEM_ID','','','','ANONYMOUS','NUMBER','','','Mln Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MLN_LOT_NUMBER',401,'Mln Lot Number','MLN_LOT_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Mln Lot Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MLN_ORGANIZATION_ID',401,'Mln Organization Id','MLN_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Mln Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ORGANIZATION_ID',401,'Organization Id','ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LOCATOR_ID',401,'Locator Id','LOCATOR_ID','','','','ANONYMOUS','NUMBER','','','Locator Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','ANONYMOUS','NUMBER','','','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','INVENTORY_LOCATION_ID',401,'Inventory Location Id','INVENTORY_LOCATION_ID','','','','ANONYMOUS','NUMBER','','','Inventory Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LANGUAGE',401,'Language','LANGUAGE','','','','ANONYMOUS','VARCHAR2','','','Language','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LIST_PRICE_PER_UNIT',401,'List Price Per Unit','LIST_PRICE_PER_UNIT','','~T~D~2','','ANONYMOUS','NUMBER','','','List Price Per Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LPN',401,'Lpn','LPN','','','','ANONYMOUS','VARCHAR2','','','Lpn','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LPN_ID',401,'Lpn Id','LPN_ID','','','','ANONYMOUS','NUMBER','','','Lpn Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MAX_MINMAX_QUANTITY',401,'Max Minmax Quantity','MAX_MINMAX_QUANTITY','','~T~D~2','','ANONYMOUS','NUMBER','','','Max Minmax Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','MIN_MINMAX_QUANTITY',401,'Min Minmax Quantity','MIN_MINMAX_QUANTITY','','~T~D~2','','ANONYMOUS','NUMBER','','','Min Minmax Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','ONHAND_QUANTITIES_ID',401,'Onhand Quantities Id','ONHAND_QUANTITIES_ID','','','','ANONYMOUS','NUMBER','','','Onhand Quantities Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','SECONDARY_INVENTORY_NAME',401,'Secondary Inventory Name','SECONDARY_INVENTORY_NAME','','','','ANONYMOUS','VARCHAR2','','','Secondary Inventory Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','COST_GROUP_ID',401,'Cost Group Id','COST_GROUP_ID','','~T~D~2','','ANONYMOUS','NUMBER','','','Cost Group Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','SUBINVENTORY_STATUS_ID',401,'Subinventory Status Id','SUBINVENTORY_STATUS_ID','','','','ANONYMOUS','NUMBER','','','Subinventory Status Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LOCATOR_STATUS_ID',401,'Locator Status Id','LOCATOR_STATUS_ID','','','','ANONYMOUS','NUMBER','','','Locator Status Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','LOT_STATUS_ID',401,'Lot Status Id','LOT_STATUS_ID','','','','ANONYMOUS','NUMBER','','','Lot Status Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','PLANNING_TP_TYPE',401,'Planning Tp Type','PLANNING_TP_TYPE','','','','ANONYMOUS','NUMBER','','','Planning Tp Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','PLANNING_ORGANIZATION_ID',401,'Planning Organization Id','PLANNING_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Planning Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OWNING_TP_TYPE',401,'Owning Tp Type','OWNING_TP_TYPE','','','','ANONYMOUS','NUMBER','','','Owning Tp Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','OWNING_ORGANIZATION_ID',401,'Owning Organization Id','OWNING_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Owning Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','HAOU_ORGANIZATION_ID',401,'Haou Organization Id','HAOU_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Haou Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_TIME_MGMT_V','PROCESS_ID',401,'Process Id','PROCESS_ID','','','','ANONYMOUS','NUMBER','','','Process Id','','','','US');
--Inserting Object Components for EIS_XXWC_INV_TIME_MGMT_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','PA_TASKS',401,'PA_TASKS','MTV','','ANONYMOUS','ANONYMOUS','-1','','N','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_PARAMETERS',401,'MTL_PARAMETERS','MP','MP','ANONYMOUS','ANONYMOUS','-1','','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','CST_COST_GROUPS',401,'CST_COST_GROUPS','CCG','CCG','ANONYMOUS','ANONYMOUS','-1','','','','','','CCG','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_MATERIAL_STATUSES_VL',401,'MTL_MATERIAL_STATUSES_B','MMS','MMS','ANONYMOUS','ANONYMOUS','-1','','','','','','MMSV','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_ITEM_LOCATIONS_KFV',401,'MTL_ITEM_LOCATIONS','MIL','MIL','ANONYMOUS','ANONYMOUS','-1','','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','PJM_UNIT_NUMBERS',401,'PJM_UNIT_NUMBERS','PUN','PUN','ANONYMOUS','ANONYMOUS','-1','','','','','','PUN','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_SECONDARY_INVENTORIES',401,'MTL_SECONDARY_INVENTORIES','MSI','MSI','ANONYMOUS','ANONYMOUS','-1','','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','OOD','OOD','ANONYMOUS','ANONYMOUS','-1','master_organization_id','N','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','OODW','OODW','ANONYMOUS','ANONYMOUS','-1','owning_organization_id','N','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_LOT_NUMBERS',401,'MTL_LOT_NUMBERS','MLN','MLN','ANONYMOUS','ANONYMOUS','-1','','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','OODP','OOP','ANONYMOUS','ANONYMOUS','-1','planning_organization_id','N','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSIV','MSIV','ANONYMOUS','ANONYMOUS','-1','','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','CST_COST_GROUP_ACCOUNTS',401,'CST_COST_GROUP_ACCOUNTS','CCGA','CCGA','ANONYMOUS','ANONYMOUS','-1','','','','','','CCGA','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','HAOU','HAOU','ANONYMOUS','ANONYMOUS','-1','','N','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_INV_TIME_MGMT_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_PARAMETERS','MP',401,'EIOQV.MP_ORGANIZATION_ID','=','MP.ORGANIZATION_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','CST_COST_GROUPS','CCG',401,'EIOQV.CCG_COST_GROUP_ID','=','CCG.COST_GROUP_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_MATERIAL_STATUSES_VL','MMS',401,'EIOQV.MMS_STATUS_ID','=','MMS.STATUS_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_ITEM_LOCATIONS_KFV','MIL',401,'EIOQV.INVENTORY_LOCATION_ID','=','MIL.INVENTORY_LOCATION_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_ITEM_LOCATIONS_KFV','MIL',401,'EIOQV.MIL_ORGANIZATION_ID','=','MIL.ORGANIZATION_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','PJM_UNIT_NUMBERS','PUN',401,'EIOQV.UNIT_NUMBER','=','PUN.UNIT_NUMBER(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_SECONDARY_INVENTORIES','MSI',401,'EIOQV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_SECONDARY_INVENTORIES','MSI',401,'EIOQV.SECONDARY_INVENTORY_NAME','=','MSI.SECONDARY_INVENTORY_NAME(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS','OOD',401,'EIOQV.OOD_ORGANIZATION_ID','=','OOD.ORGANIZATION_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS','OODW',401,'EIOQV.OODW_ORGANIZATION_ID','=','OODW.ORGANIZATION_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_LOT_NUMBERS','MLN',401,'EIOQV.MLN_INVENTORY_ITEM_ID','=','MLN.INVENTORY_ITEM_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_LOT_NUMBERS','MLN',401,'EIOQV.MLN_LOT_NUMBER','=','MLN.LOT_NUMBER(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_LOT_NUMBERS','MLN',401,'EIOQV.MLN_ORGANIZATION_ID','=','MLN.ORGANIZATION_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS','OODP',401,'EIOQV.OODP_ORGANIZATION_ID','=','OODP.ORGANIZATION_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_SYSTEM_ITEMS_KFV','MSIV',401,'EIOQV.MSIV_INVENTORY_ITEM_ID','=','MSIV.INVENTORY_ITEM_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','MTL_SYSTEM_ITEMS_KFV','MSIV',401,'EIOQV.MSIV_ORGANIZATION_ID','=','MSIV.ORGANIZATION_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','CST_COST_GROUP_ACCOUNTS','CCGA',401,'EIOQV.CCGA_COST_GROUP_ID','=','CCGA.COST_GROUP_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','CST_COST_GROUP_ACCOUNTS','CCGA',401,'EIOQV.CCGA_ORGANIZATION_ID','=','CCGA.ORGANIZATION_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','HR_ORGANIZATION_UNITS','HAOU',401,'EIOQV.HAOU_ORGANIZATION_ID','=','HAOU.ORGANIZATION_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_TIME_MGMT_V','PA_TASKS','MTV',401,'EIOQV.MTV_TASK_ID','=','MTV.TASK_ID(+)','','','','','ANONYMOUS');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report LOV Data for Time Sensitive Mgmt - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Time Sensitive Mgmt - WC
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT concatenated_segments item, description FROM mtl_system_items_kfv msi,
                org_organization_definitions ood
          WHERE msi.organization_id = ood.organization_id
            AND ood.operating_unit = fnd_profile.VALUE (''ORG_ID'')
       ORDER BY concatenated_segments','','EIS_INV_ITEM_LOV','List of all inventory items.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select SECONDARY_INVENTORY_NAME SUB_INVENTORY,HAOU.NAME ORGANIZATION_NAME
from MTL_SECONDARY_INVENTORIES MSI,
     HR_ALL_ORGANIZATION_UNITS HAOU
WHERE 1=1
AND MSI.ORGANIZATION_ID = HAOU.organization_id','','EIS_INV_SUBINVENTORY_LOV','List of All SubInventories.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Supplier'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Supplier List','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select vendor_name from po_vendors','','XXWC Vendors','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'','Y,N,All','XXWC AT Risk Lov','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' FROM DUAL','','XXWC All ORG LIST','XXWC All ORG LIST','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report Data for Time Sensitive Mgmt - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Time Sensitive Mgmt - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Time Sensitive Mgmt - WC' );
--Inserting Report - Time Sensitive Mgmt - WC
xxeis.eis_rsc_ins.r( 401,'Time Sensitive Mgmt - WC','','The Onhand Quantity Report displays the total quantity of an item in a subinventory.','','','','SA059956','EIS_XXWC_INV_TIME_MGMT_V','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Time Sensitive Mgmt - WC
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'ITEM','Item','Item','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'UNIT_OF_MEASURE','UOM','Unit Of Measure','','','default','','17','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'ALTERNATE_BIN_LOC','Bin2','Alternate Bin Loc','','','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'BUYER','Buyer','Buyer','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'OLDEST_BORN_DATE','Oldest Born Date','Oldest Born Date','','','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'ON_HAND','On Hand','On Hand','','~~~','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'ORGANIZATION_CODE','Org','Organization Code','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'SUBINVENTORY_CODE','Subinventory Code','Subinventory Code','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'SHELF_LIFE_DAYS','SL Days','Shelf Life Days','','~~~','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'VENDOR_NAME','Supplier Name','Vendor Name','','','default','','19','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'VENDOR_NUMBER','Supplier Number','Vendor Number','','','default','','18','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'PRIMARY_BIN_LOC','Bin1','Primary Bin Loc','','','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'DAYS_AGED','Days Aged','Days Aged','','~~~','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'AVGCOST','Avgcost','Avgcost','','$~,~.~2','default','','7','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'EXTENDED COST','Extended Cost','Avgcost','NUMBER','$~.~,~2','default','','8','Y','Y','','','','','','Avgcost*on_hand','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'DATE_RECEIVED','Date Received','Date Received','','','default','','20','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'UBD','UBD','Ubd','','','default','','16','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'AT_RISK_N','At Risk','At Risk N','','','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Time Sensitive Mgmt - WC',401,'SHORT_DATE','Short Date','Short Date','','','default','','15','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_TIME_MGMT_V','','','','US','');
--Inserting Report Parameters - Time Sensitive Mgmt - WC
xxeis.eis_rsc_ins.rp( 'Time Sensitive Mgmt - WC',401,'Organization','Inventory Organization','ORGANIZATION_CODE','IN','XXWC All ORG LIST','','VARCHAR2','Y','Y','1','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_TIME_MGMT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Time Sensitive Mgmt - WC',401,'Item Num','Inventory Item','ITEM','IN','EIS_INV_ITEM_LOV','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_TIME_MGMT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Time Sensitive Mgmt - WC',401,'SubInventory','Subinventory','SUBINVENTORY_CODE','IN','EIS_INV_SUBINVENTORY_LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_TIME_MGMT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Time Sensitive Mgmt - WC',401,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_TIME_MGMT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Time Sensitive Mgmt - WC',401,'Location List','Location List','','IN','XXWC Org List','','VARCHAR2','N','Y','6','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_TIME_MGMT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Time Sensitive Mgmt - WC',401,'Supplier List','Supplier List','','=','XXWC Supplier List','','VARCHAR2','N','Y','8','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_TIME_MGMT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Time Sensitive Mgmt - WC',401,'Item List','Item List','','IN','XXWC Item List','','VARCHAR2','N','Y','7','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_TIME_MGMT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Time Sensitive Mgmt - WC',401,'Supplier Name','Supplier','VENDOR_NAME','IN','XXWC Vendors','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_TIME_MGMT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Time Sensitive Mgmt - WC',401,'At Risk','At Risk','AT_RISK','IN','XXWC AT Risk Lov','''Y''','VARCHAR2','N','Y','9','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_TIME_MGMT_V','','','US','');
--Inserting Dependent Parameters - Time Sensitive Mgmt - WC
--Inserting Report Conditions - Time Sensitive Mgmt - WC
xxeis.eis_rsc_ins.rcnh( 'Time Sensitive Mgmt - WC',401,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND PROCESS_ID   = :SYSTEM.PROCESS_ID
','1',401,'Time Sensitive Mgmt - WC','Free Text ');
xxeis.eis_rsc_ins.rcnh( 'Time Sensitive Mgmt - WC',401,'EIOQV.ITEM IN Item Num','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ITEM','','Item Num','','','','','EIS_XXWC_INV_TIME_MGMT_V','','','','','','IN','Y','Y','','','','','1',401,'Time Sensitive Mgmt - WC','EIOQV.ITEM IN Item Num');
xxeis.eis_rsc_ins.rcnh( 'Time Sensitive Mgmt - WC',401,'EIOQV.SUBINVENTORY_CODE IN SubInventory','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SUBINVENTORY_CODE','','SubInventory','','','','','EIS_XXWC_INV_TIME_MGMT_V','','','','','','IN','Y','Y','','','','','1',401,'Time Sensitive Mgmt - WC','EIOQV.SUBINVENTORY_CODE IN SubInventory');
xxeis.eis_rsc_ins.rcnh( 'Time Sensitive Mgmt - WC',401,'EIOQV.REGION IN Region','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','REGION','','Region','','','','','EIS_XXWC_INV_TIME_MGMT_V','','','','','','IN','Y','Y','','','','','1',401,'Time Sensitive Mgmt - WC','EIOQV.REGION IN Region');
xxeis.eis_rsc_ins.rcnh( 'Time Sensitive Mgmt - WC',401,'EIOQV.VENDOR_NAME IN Supplier Name','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NAME','','Supplier Name','','','','','EIS_XXWC_INV_TIME_MGMT_V','','','','','','IN','Y','Y','','','','','1',401,'Time Sensitive Mgmt - WC','EIOQV.VENDOR_NAME IN Supplier Name');
--Inserting Report Sorts - Time Sensitive Mgmt - WC
--Inserting Report Triggers - Time Sensitive Mgmt - WC
xxeis.eis_rsc_ins.rt( 'Time Sensitive Mgmt - WC',401,'BEGIN
  xxeis.EIS_XXWC_INV_TIME_MGMT_PKG.MAIN(
    P_PROCESS_ID => :SYSTEM.PROCESS_ID,
    P_ORGANIZATION => :Organization,
    P_ITEM => :Item Num,
    P_SUBINVENTORY =>:SubInventory,
    P_REGION => :Region,
    P_SUPPLIER => :Supplier Name,
    P_LOCATION_LIST => :Location List,
    P_ITEM_LIST => :Item List,
    P_SUPPLIER_LIST => :Supplier List,
    P_AT_RISK => :At Risk  );
END;','B','Y','SA059956','AQ');
--inserting report templates - Time Sensitive Mgmt - WC
xxeis.eis_rsc_ins.r_tem( 'Time Sensitive Mgmt - WC','Time Sensitive Mgmt - WC','Seeded template for Time Sensitive Mgmt - WC','','','','','','','','','','','Time Sensitive Mgmt - WC.rtf','SA059956','X','','','Y','Y','N','N');
--Inserting Report Portals - Time Sensitive Mgmt - WC
--inserting report dashboards - Time Sensitive Mgmt - WC
xxeis.eis_rsc_ins.R_dash( 'Time Sensitive Mgmt - WC','Dynamic 702','Dynamic 702','pie','large','Unit Number','Unit Number','Subinventory Code','Subinventory Code','Count','SA059956');
xxeis.eis_rsc_ins.R_dash( 'Time Sensitive Mgmt - WC','Dynamic 706','Dynamic 706','vertical stacked bar','large','Subinventory Code','Subinventory Code','On Hand','On Hand','Sum','SA059956');
xxeis.eis_rsc_ins.R_dash( 'Time Sensitive Mgmt - WC','Dynamic 707','Dynamic 707','vertical stacked bar','large','Subinventory Code','Subinventory Code','Item','Item','Count','SA059956');
xxeis.eis_rsc_ins.R_dash( 'Time Sensitive Mgmt - WC','Dynamic 701','Dynamic 701','vertical percent bar','large','Cost Group','Cost Group','Cost Group','Cost Group','Count','SA059956');
xxeis.eis_rsc_ins.R_dash( 'Time Sensitive Mgmt - WC','Dynamic 703','Dynamic 703','horizontal stacked bar','large','Unit Number','Unit Number','Subinventory Code','Subinventory Code','Count','SA059956');
xxeis.eis_rsc_ins.R_dash( 'Time Sensitive Mgmt - WC','Dynamic 704','Dynamic 704','vertical stacked bar','large','Unit Number','Unit Number','Subinventory Code','Subinventory Code','Count','SA059956');
xxeis.eis_rsc_ins.R_dash( 'Time Sensitive Mgmt - WC','Dynamic 705','Dynamic 705','vertical stacked bar','large','Subinventory Code','Subinventory Code','On Hand','On Hand','Count','SA059956');
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Time Sensitive Mgmt - WC','401','EIS_XXWC_INV_TIME_MGMT_V','EIS_XXWC_INV_TIME_MGMT_V','N','');
--inserting report security - Time Sensitive Mgmt - WC
xxeis.eis_rsc_ins.rsec( 'Time Sensitive Mgmt - WC','401','','XXWC_INVENTORY_SUPER_USER',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Time Sensitive Mgmt - WC','401','','XXWC_INVENTORY_SPEC_SCC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Time Sensitive Mgmt - WC','401','','XXWC_INV_PLANNER',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Time Sensitive Mgmt - WC','401','','XXWC_INVENTORY_CONTROL_SR_MGR',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Time Sensitive Mgmt - WC','401','','XXWC_INVENTORY_CONTROL_INQUIRY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Time Sensitive Mgmt - WC','401','','XXWC_INV_ACCOUNTANT',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Time Sensitive Mgmt - WC','401','','XXWC_AO_INV_ADJ',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Time Sensitive Mgmt - WC','401','','XXWC_AO_INV_ADJ_REC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Time Sensitive Mgmt - WC','401','','XXWC_AO_INV_ADJ_PO_RPT',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Time Sensitive Mgmt - WC','401','','XXWC_AO_BIN_MTN_CYCLE_INV_ADJ',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Time Sensitive Mgmt - WC','707','','XXWC_COST_MANAGEMENT_INQ',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Time Sensitive Mgmt - WC','201','','XXWC_PURCHASING_INQUIRY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Time Sensitive Mgmt - WC','201','','XXWC_PURCHASING_SR_MRG_WC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Time Sensitive Mgmt - WC','201','','XXWC_PURCHASING_MGR',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Time Sensitive Mgmt - WC','401','','XXWC_AO_BIN_MTN_PO_RPT',401,'SA059956','','','');
--Inserting Report Pivots - Time Sensitive Mgmt - WC
xxeis.eis_rsc_ins.rpivot( 'Time Sensitive Mgmt - WC',401,'Pivot','1','1,0|1,1,0','1,1,1,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','SUBINVENTORY_CODE','PAGE_FIELD','','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','OLDEST_BORN_DATE','ROW_FIELD','','','3','0','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','SHELF_LIFE_DAYS','ROW_FIELD','','','6','0','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','ORGANIZATION_CODE','ROW_FIELD','','','1','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','ITEM','ROW_FIELD','','','4','0','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'Pivot','ON_HAND','DATA_FIELD','SUM','','1','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
xxeis.eis_rsc_ins.rpivot( 'Time Sensitive Mgmt - WC',401,'At Risk','2','0,0|1,0,0','1,1,0,0|PivotStyleMedium4|2');
--Inserting Report Pivot Details For Pivot - At Risk
xxeis.eis_rsc_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'At Risk','ON_HAND','DATA_FIELD','SUM','Sum of On Hand','1','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'At Risk','ITEM','ROW_FIELD','','','1','0','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'At Risk','ITEM_DESCRIPTION','ROW_FIELD','','','2','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'At Risk','PRIMARY_BIN_LOC','ROW_FIELD','','','3','0','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'At Risk','ALTERNATE_BIN_LOC','ROW_FIELD','','','4','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Time Sensitive Mgmt - WC',401,'At Risk','AVGCOST','DATA_FIELD','SUM','Sum of Avg Cost','2','','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- At Risk
--Inserting Report   Version details- Time Sensitive Mgmt - WC
xxeis.eis_rsc_ins.rv( 'Time Sensitive Mgmt - WC','','Time Sensitive Mgmt - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
