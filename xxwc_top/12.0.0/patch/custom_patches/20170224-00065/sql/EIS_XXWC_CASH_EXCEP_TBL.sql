-------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_CASH_EXCEP_TBL
  Description: This table is used to get data from XXEIS.EIS_XXWC_CASH_EXCEPTION_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     12-Apr-2016        Siva   		TMS#20160411-00103  Performance Tuning
  1.1     15-Mar-2017      	 Siva   		 TMS#20170224-00065
********************************************************************************/
DROP TABLE XXEIS.EIS_XXWC_CASH_EXCEP_TBL CASCADE CONSTRAINTS --added for version 1.1
/

CREATE GLOBAL TEMPORARY TABLE XXEIS.EIS_XXWC_CASH_EXCEP_TBL   --added for version 1.1
   (	PROCESS_ID NUMBER, 
	CASH_RECEIPT_ID NUMBER, 
	PAYMENT_SET_ID NUMBER
)
ON COMMIT PRESERVE ROWS  --added for version 1.1
/
