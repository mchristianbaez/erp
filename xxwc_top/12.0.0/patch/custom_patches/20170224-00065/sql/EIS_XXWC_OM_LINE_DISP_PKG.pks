CREATE OR REPLACE 
PACKAGE     XXEIS.EIS_XXWC_OM_LINE_DISP_PKG
IS
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "LINES DISPOSITION REPORT"
--//
--// Object Name         		:: EIS_XXWC_OM_LINE_DISP_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	    04/18/2016      Initial Build  --TMS#20160426-00102  --Performance Tuning
--// 1.1     	Siva 			15-Mar-2017     TMS#20170224-00065 
--//============================================================================

    TYPE cursor_type IS REF CURSOR;

        

TYPE VIEW_REC IS RECORD(
process_id number,
HEADER_ID   number,
Line_id   number)
;

type VIEW_TAB is table of VIEW_REC
        index by BINARY_INTEGER;
        
   G_VIEW_TAB     VIEW_TAB;
   G_VIEW_TAB2     VIEW_TAB;
   G_VIEW_TAB3     VIEW_TAB;
   
  type MAIN_VIEW_REC is RECORD
   (
process_id                 number,
item_number         VARCHAR2(240),
item_description    VARCHAR2(240),
branch              VARCHAR2(240),
pricing_zone        VARCHAR2(240),
qty_of_items_cancelled     number,
qty_of_items_dispositioned number,
times_line_cancelled       number,
times_line_dispositioned   number,
total_sales                NUMBER,
velocity            VARCHAR2(240),
min                        NUMBER,
max                        NUMBER,
reorder_point              NUMBER,
header_branch       VARCHAR2(240),
INVENTORY_ITEM_ID          number,
ORGANIZATION_ID             number
   );
   
  type MAIN_VIEW_TAB is table of MAIN_VIEW_REC
        index by BINARY_INTEGER;
        
   G_MAIN_VIEW_TAB     MAIN_VIEW_TAB; 
        
    procedure OM_LINE_DISP (P_PROCESS_ID            in number
                           ,p_Ordered_Date_From     IN date
                           ,p_Ordered_Date_To       IN date
                           ,p_location              in varchar2
                           ,p_pricing_zone          in varchar2
                           ,p_item_number           in varchar2
                           ,p_Item_Description      in varchar2
                          );
--//============================================================================
--//
--// Object Name         :: om_line_disp  
--//
--// Object Usage 		 :: This Object Referred by "LINES DISPOSITION REPORT"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_OM_LINE_DISP_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	  04/22/2016       Initial Build  --TMS#20160426-00102  --Performance Tuning
--//============================================================================						  

/* --Commented code for version 1.1                          
    PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER);
	--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	   04/22/2016      Initial Build  --TMS#20160426-00102  --Performance Tuning
--//============================================================================    
*/ --Commented code for version 1.1
   
END EIS_XXWC_OM_LINE_DISP_PKG;
/
