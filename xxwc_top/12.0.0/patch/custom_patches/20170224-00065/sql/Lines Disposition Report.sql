--Report Name            : Lines Disposition Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_OM_LINE_DISP_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_LINE_DISP_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_LINE_DISP_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Line Disp V','EXOLDV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_OM_LINE_DISP_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_LINE_DISP_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_LINE_DISP_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','MAX',660,'Max','MAX','','','','XXEIS_RS_ADMIN','NUMBER','','','Max','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','MIN',660,'Min','MIN','','','','XXEIS_RS_ADMIN','NUMBER','','','Min','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','TOTAL_SALES',660,'Total Sales','TOTAL_SALES','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Total Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','TIMES_LINE_DISPOSITIONED',660,'Times Line Dispositioned','TIMES_LINE_DISPOSITIONED','','','','XXEIS_RS_ADMIN','NUMBER','','','Times Line Dispositioned','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','TIMES_LINE_CANCELLED',660,'Times Line Cancelled','TIMES_LINE_CANCELLED','','','','XXEIS_RS_ADMIN','NUMBER','','','Times Line Cancelled','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','QTY_OF_ITEMS_DISPOSITIONED',660,'Qty Of Items Dispositioned','QTY_OF_ITEMS_DISPOSITIONED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Qty Of Items Dispositioned','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','QTY_OF_ITEMS_CANCELLED',660,'Qty Of Items Cancelled','QTY_OF_ITEMS_CANCELLED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Qty Of Items Cancelled','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','BRANCH',660,'Branch','BRANCH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','REORDER_POINT',660,'Reorder Point','REORDER_POINT','','','','XXEIS_RS_ADMIN','NUMBER','','','Reorder Point','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','PRICING_ZONE',660,'Pricing Zone','PRICING_ZONE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pricing Zone','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','VELOCITY',660,'Velocity','VELOCITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Velocity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','PROCESS_ID',660,'Process Id','PROCESS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Process Id','','','','US');
--Inserting Object Components for EIS_XXWC_OM_LINE_DISP_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_LINE_DISP_V','MTL_SYSTEM_ITEMS_KFV',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_LINE_DISP_V','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Lines All Stores Information For All Orde','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_LINE_DISP_V','HZ_CUST_ACCOUNTS',660,'HZ_CUST_ACCOUNTS','HCA','HCA','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Stores Information About Customer Accounts.','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_LINE_DISP_V','HZ_PARTIES',660,'HZ_PARTIES','HZP','HZP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information About Parties Such As Organizations, P','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_OM_LINE_DISP_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_LINE_DISP_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOLDV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_LINE_DISP_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOLDV.ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_LINE_DISP_V','OE_ORDER_LINES','OL',660,'EXOLDV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_LINE_DISP_V','HZ_CUST_ACCOUNTS','HCA',660,'EXOLDV.CUST_ACCOUNT_ID','=','HCA.CUST_ACCOUNT_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_LINE_DISP_V','HZ_PARTIES','HZP',660,'EXOLDV.PARTY_ID','=','HZP.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Lines Disposition Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Lines Disposition Report
xxeis.eis_rsc_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT DISTINCT MSI.SEGMENT1   ITEM_NUMBER,
MSI.DESCRIPTION  ITEM_DESCRIPTION from mtl_system_items_b msi','','OM Item Number LOV','Order Item numbers','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct attribute6 pricing_zone from mtl_parameters','','OM Pricing Zone','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select DESCRIPTION ITEM_DESCRIPTION from MTL_SYSTEM_ITEMS_KFV
where exists (Select 1 from XXEIS.EIS_ORG_ACCESS_V where organization_id = organization_id)
','','OM ITEM DESCRIPTION','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Lines Disposition Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Lines Disposition Report
xxeis.eis_rsc_utility.delete_report_rows( 'Lines Disposition Report' );
--Inserting Report - Lines Disposition Report
xxeis.eis_rsc_ins.r( 660,'Lines Disposition Report','','The purpose of this report is to list items that have been line dispositioned (line item fulfilled in a different branch) or cancelled.','','','','SA059956','EIS_XXWC_OM_LINE_DISP_V','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Lines Disposition Report
xxeis.eis_rsc_ins.rc( 'Lines Disposition Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Lines Disposition Report',660,'ITEM_NUMBER','Item Number','Item Number','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Lines Disposition Report',660,'MAX','Max','Max','','~~~','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Lines Disposition Report',660,'MIN','Min','Min','','~~~','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Lines Disposition Report',660,'QTY_OF_ITEMS_CANCELLED','Qty Of Items Cancelled','Qty Of Items Cancelled','','~~~','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Lines Disposition Report',660,'QTY_OF_ITEMS_DISPOSITIONED','Qty Of Items Dispositioned','Qty Of Items Dispositioned','','~~~','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Lines Disposition Report',660,'TIMES_LINE_CANCELLED','Times Line Cancelled','Times Line Cancelled','','~~~','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Lines Disposition Report',660,'TIMES_LINE_DISPOSITIONED','Times Line Dispositioned','Times Line Dispositioned','','~~~','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Lines Disposition Report',660,'TOTAL_SALES','Total Location Sales','Total Sales','','$~,~.~2','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Lines Disposition Report',660,'TOTAL_TIMES_LINE_CANC_OR_DISP','Total Times Line Canc or Disp','Total Sales','NUMBER','~~~','default','','7','Y','Y','','','','','','(EXOLDV.TIMES_LINE_CANCELLED + EXOLDV.TIMES_LINE_DISPOSITIONED)','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Lines Disposition Report',660,'TOTAL_QTY_LINE_CANC_OR_DISP','Total Qty Line canc or Disp','Total Sales','NUMBER','~~~','default','','8','Y','Y','','','','','','(EXOLDV.QTY_OF_ITEMS_CANCELLED + EXOLDV.QTY_OF_ITEMS_DISPOSITIONED)','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Lines Disposition Report',660,'REORDER_POINT','Reorder Point','Reorder Point','','~~~','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Lines Disposition Report',660,'VELOCITY','Velocity','Velocity','','','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Lines Disposition Report',660,'TOTAL_PRICING_ZONE_SALES','Pricing Zone Sales','Velocity','NUMBER','$~,~.~2','default','','10','Y','Y','','','','','','xxeis.EIS_RS_XXWC_COM_UTIL_PKG.GET_TOT_PRIZINGZONE_SALES_DLR(EXOLDV.INVENTORY_ITEM_ID,EXOLDV.ORGANIZATION_ID,EXOLDV.PRICING_ZONE)','SA059956','N','N','','EIS_XXWC_OM_LINE_DISP_V','','','','US','');
--Inserting Report Parameters - Lines Disposition Report
xxeis.eis_rsc_ins.rp( 'Lines Disposition Report',660,'Location','Location','BRANCH','IN','OM WAREHOUSE','','VARCHAR2','Y','Y','3','','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_LINE_DISP_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Lines Disposition Report',660,'Item Number','Item Number','ITEM_NUMBER','IN','OM Item Number LOV','','VARCHAR2','N','Y','5','','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_LINE_DISP_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Lines Disposition Report',660,'Pricing Zone','Pricing Zone','PRICING_ZONE','IN','OM Pricing Zone','','VARCHAR2','N','Y','4','','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_LINE_DISP_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Lines Disposition Report',660,'Item Description','Item Description','ITEM_DESCRIPTION','IN','OM ITEM DESCRIPTION','','VARCHAR2','N','Y','6','','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_LINE_DISP_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Lines Disposition Report',660,'Ordered Date From','Ordered Date From','','IN','','','DATE','Y','Y','1','','N','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_OM_LINE_DISP_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Lines Disposition Report',660,'Ordered Date To','Ordered Date To','','IN','','','DATE','Y','Y','2','','N','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_OM_LINE_DISP_V','','','US','');
--Inserting Dependent Parameters - Lines Disposition Report
--Inserting Report Conditions - Lines Disposition Report
xxeis.eis_rsc_ins.rcnh( 'Lines Disposition Report',660,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','and PROCESS_ID= :SYSTEM.PROCESS_ID','1',660,'Lines Disposition Report','Free Text ');
--Inserting Report Sorts - Lines Disposition Report
xxeis.eis_rsc_ins.rs( 'Lines Disposition Report',660,'ITEM_NUMBER','ASC','SA059956','1','');
--Inserting Report Triggers - Lines Disposition Report
xxeis.eis_rsc_ins.rt( 'Lines Disposition Report',660,'Begin
xxeis.EIS_XXWC_OM_LINE_DISP_PKG.OM_LINE_DISP (
P_PROCESS_ID => :SYSTEM.PROCESS_ID,
p_Ordered_Date_From => :Ordered Date From ,
p_Ordered_Date_To => :Ordered Date To,
p_location => :Location ,
p_pricing_zone => :Pricing Zone ,
p_item_number   => :Item Number ,
p_Item_Description     => :Item Description
                          );
end;
','B','Y','SA059956','BQ');
--inserting report templates - Lines Disposition Report
--Inserting Report Portals - Lines Disposition Report
--inserting report dashboards - Lines Disposition Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Lines Disposition Report','660','EIS_XXWC_OM_LINE_DISP_V','EIS_XXWC_OM_LINE_DISP_V','N','');
--inserting report security - Lines Disposition Report
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','660','','ORDER_MGMT_SUPER_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','701','','CLN_OM_3A6_ADMINISTRATOR',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','20005','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','','LC053655','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','','10010432','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','','RB054040','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','','RV003897','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','','SS084202','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','','SO004816','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','660','','XXWC_AO_OEENTRY_PO_RPT',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','660','','XXWC_AO_OEENTRY_REC',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','660','','XXWC_AO_OEENTRY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Lines Disposition Report','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'SA059956','','','');
--Inserting Report Pivots - Lines Disposition Report
xxeis.eis_rsc_ins.rpivot( 'Lines Disposition Report',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Lines Disposition Report',660,'Pivot','ITEM_NUMBER','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Lines Disposition Report',660,'Pivot','QTY_OF_ITEMS_CANCELLED','DATA_FIELD','SUM','','4','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Lines Disposition Report',660,'Pivot','QTY_OF_ITEMS_DISPOSITIONED','DATA_FIELD','SUM','','3','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Lines Disposition Report',660,'Pivot','TIMES_LINE_CANCELLED','DATA_FIELD','SUM','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Lines Disposition Report',660,'Pivot','TIMES_LINE_DISPOSITIONED','DATA_FIELD','SUM','','1','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- Lines Disposition Report
xxeis.eis_rsc_ins.rv( 'Lines Disposition Report','','Lines Disposition Report','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
