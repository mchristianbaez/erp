--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_SUBINV_ONHAND_DTLS
  Description: This table is used to get data from XXEIS.EIS_XXWC_INV_LIST_RPT_PKG Package.
  HISTORY
  ===============================================================================
  VERSION 		DATE            AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     	16-Apr-2016        Pramod   		TMS#20160503-00085  Performance Tuning
  1.1       15-Mar-2017      	Siva   		 	 TMS#20170224-00065
********************************************************************************/
DROP TABLE XXEIS.EIS_XXWC_SUBINV_ONHAND_DTLS --added for version 1.1
/

CREATE GLOBAL TEMPORARY TABLE XXEIS.EIS_XXWC_SUBINV_ONHAND_DTLS  --added for version 1.1
   (	PROCESS_ID NUMBER, 
	ONHAND NUMBER, 
	IS_CONSIGNED VARCHAR2(20 BYTE), 
	SUBINVENTORY_CODE VARCHAR2(240 BYTE), 
	ORGANIZATION_ID NUMBER, 
	INVENTORY_ITEM_ID NUMBER
   )
ON COMMIT PRESERVE ROWS  --added for version 1.1
/
