--Report Name            : Inventory Listing Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_INV_LISTING_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_INV_LISTING_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_INV_LISTING_V',401,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Rs Xxwc Inv Listing V','EXILV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_INV_LISTING_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_INV_LISTING_V',401,FALSE);
--Inserting Object Columns for EIS_XXWC_INV_LISTING_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','SHELF_LIFE',401,'Shelf Life','SHELF_LIFE','','','','XXEIS_RS_ADMIN','NUMBER','','','Shelf Life','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','ONHAND',401,'Onhand','ONHAND','','','','XXEIS_RS_ADMIN','NUMBER','','','Onhand','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','SELLING_PRICE',401,'Selling Price','SELLING_PRICE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Selling Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','UOM',401,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Uom','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','CAT_CLASS',401,'Cat Class','CAT_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat Class','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','PART_NUMBER',401,'Part Number','PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Part Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','SEGMENT_LOCATION',401,'Segment Location','SEGMENT_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','MFG_NUMBER',401,'Mfg Number','MFG_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Mfg Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','SALE_UNITS',401,'Sale Units','SALE_UNITS','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Sale Units','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','BRANCH_NUMBER',401,'Branch Number','BRANCH_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','CAT_CLASS_DESCRIPTION',401,'Cat Class Description','CAT_CLASS_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat Class Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','CONSIGNED',401,'Consigned','CONSIGNED','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Consigned','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','AVERAGECOST',401,'Averagecost','AVERAGECOST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Averagecost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','LOCATION_STATE',401,'Location State','LOCATION_STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location State','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','SUBINVENTORY_CODE',401,'Subinventory Code','SUBINVENTORY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Subinventory Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','BIN1',401,'Bin1','BIN1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bin1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','BIN2',401,'Bin2','BIN2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bin2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','BIN3',401,'Bin3','BIN3','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bin3','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','ON_ORDER',401,'On Order','ON_ORDER','','','','XXEIS_RS_ADMIN','NUMBER','','','On Order','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','BIN0',401,'Bin0','BIN0','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bin0','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','OPEN_DEMAND',401,'Open Demand','OPEN_DEMAND','','','','XXEIS_RS_ADMIN','CHAR','','','Open Demand','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','MAKE_BUY',401,'Make Buy','MAKE_BUY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Make Buy','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','FINAL_END_QTY',401,'Final End Qty','FINAL_END_QTY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Final End Qty','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','CONTAINER_TYPE',401,'Container Type','CONTAINER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Container Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','LOCATION',401,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','UN_NUMBER',401,'Un Number','UN_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Un Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','WEIGHT',401,'Weight','WEIGHT','','','','XXEIS_RS_ADMIN','NUMBER','','','Weight','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','ONHAND_TYPE',401,'Onhand Type','ONHAND_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Onhand Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','ORGANIZATION_ID',401,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','TONHAND',401,'Tonhand','TONHAND','','','','XXEIS_RS_ADMIN','NUMBER','','','Tonhand','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_LISTING_V','PROCESS_ID',401,'Process Id','PROCESS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Process Id','','','','US');
--Inserting Object Components for EIS_XXWC_INV_LISTING_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_LISTING_V','GL_CODE_COMBINATIONS_KFV',401,'GL_CODE_COMBINATIONS','GCC','GCC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Account Combinations','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_LISTING_V','GL_PERIOD_STATUSES',401,'GL_PERIOD_STATUSES','GPS','GPS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Calendar Period Statuses','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_LISTING_V','MTL_CATEGORIES',401,'MTL_CATEGORIES_B','MCV','MCV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Categories','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_LISTING_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Items','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_INV_LISTING_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_LISTING_V','GL_CODE_COMBINATIONS_KFV','GCC',401,'EXILV.CODE_COMBINATION_ID','=','GCC.CODE_COMBINATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_LISTING_V','GL_PERIOD_STATUSES','GPS',401,'EXILV.APPLICATION_ID','=','GPS.APPLICATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_LISTING_V','GL_PERIOD_STATUSES','GPS',401,'EXILV.SET_OF_BOOKS_ID','=','GPS.SET_OF_BOOKS_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_LISTING_V','GL_PERIOD_STATUSES','GPS',401,'EXILV.FISCAL_MONTH','=','GPS.PERIOD_NAME(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_LISTING_V','MTL_CATEGORIES','MCV',401,'EXILV.CATEGORY_ID','=','MCV.CATEGORY_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_LISTING_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXILV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_LISTING_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXILV.INV_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report LOV Data for Inventory Listing Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Inventory Listing Report
xxeis.eis_rsc_ins.lov( 401,'select SECONDARY_INVENTORY_NAME SUB_INVENTORY,HAOU.NAME ORGANIZATION_NAME
from MTL_SECONDARY_INVENTORIES MSI,
     HR_ALL_ORGANIZATION_UNITS HAOU
WHERE 1=1
AND MSI.ORGANIZATION_ID = HAOU.organization_id','','EIS_INV_SUBINVENTORY_LOV','List of All SubInventories.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT ''Negative Onhand Items'' onhand_type FROM dual
UNION
SELECT ''Positive Onhand Items'' onhand_type FROM dual
UNION
SELECT ''ALL Items'' onhand_type FROM dual','','INV ONHAND TYPE','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT ORGANIZATION_CODE WAREHOUSE,
  ORGANIZATION_NAME
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1)
AND EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
UNION
SELECT ''All'', ''All Organizations'' FROM Dual','','XXWC INV ORGANIZATIONS LOV1','List of All Inventory Orgs under a given operating unit.','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'','On Hand,No on Hand,ALL,Negative on hand','ON_HAND_TYPE_CSV','','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report Data for Inventory Listing Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Inventory Listing Report
xxeis.eis_rsc_utility.delete_report_rows( 'Inventory Listing Report' );
--Inserting Report - Inventory Listing Report
xxeis.eis_rsc_ins.r( 401,'Inventory Listing Report','','Provide a listing of inventory related information ( Negative On Hand , HAZMAT list, Cat/Class list, Extended Description, Inventory Explosion, etc.)
Selection - Location, Prod, Ven, Cat/Class
Columns - stock level, negative avail, on hand, on order, turns, cat/class, descrip, extended description (Negative On Hand, HAZMAT list, Cat/Class list, Inventory Explosion  etc)
','','','','SA059956','EIS_XXWC_INV_LISTING_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,Pivot Excel,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - Inventory Listing Report
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'BRANCH_NUMBER','Location','Branch Number','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'SUBINVENTORY_CODE','Subinventory Code','Subinventory Code','','','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'BIN1','Bin1','Bin1','','','default','','21','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'BIN2','Bin2','Bin2','','','default','','22','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'BIN3','Bin3','Bin3','','','default','','23','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'BIN0','Bin0','Bin0','','','default','','20','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'CAT_CLASS','CatClass','Cat Class','','','default','','8','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'DESCRIPTION','Description','Description','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'ONHAND','Qty on Hand','Onhand','','~~~','default','','15','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'PART_NUMBER','Part','Part Number','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'SEGMENT_LOCATION','Segment Location','Segment Location','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'SELLING_PRICE','Selling Price','Selling Price','','~T~D~2','default','','17','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'SHELF_LIFE','Shelf Life','Shelf Life','','~~~','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'UOM','UOM','Uom','','','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'LOCATION_STATE','Location State','Location State','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'SALE_UNITS','Sale Units','Sale Units','','~~~','default','','16','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'CAT_CLASS_DESCRIPTION','Cat Class Description','Cat Class Description','','','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'CONSIGNED','Consigned','Consigned','','','default','','7','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'FINAL END VALUE','Final End Value','Consigned','NUMBER','~T~D~2','default','','19','Y','Y','','','','','','(EXILV.AVERAGECOST*EXILV.ONHAND)','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'AVERAGECOST','Averagecost','Averagecost','','~T~D~2','default','','18','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'MFG_NUMBER','Mfg Number','Mfg Number','','~~~','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'ON_ORDER','On Order','On Order','','~~~','','','24','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'OPEN_DEMAND','Open Demand','Open Demand','','','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory Listing Report',401,'MAKE_BUY','Make Buy','Make Buy','','','','','25','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_LISTING_V','','','','US','');
--Inserting Report Parameters - Inventory Listing Report
xxeis.eis_rsc_ins.rp( 'Inventory Listing Report',401,'Organization','Organization','BRANCH_NUMBER','IN','XXWC INV ORGANIZATIONS LOV1','','VARCHAR2','Y','Y','1','','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_LISTING_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Listing Report',401,'Include All Items with a Positive On Hand','Include All Items with a Positive On Hand','','IN','INV ONHAND TYPE','''ALL Items''','VARCHAR2','N','N','4','','N','CONSTANT','SA059956','N','N','','','','EIS_XXWC_INV_LISTING_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Listing Report',401,'Subinventory','Subinventory','SUBINVENTORY_CODE','IN','EIS_INV_SUBINVENTORY_LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_LISTING_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory Listing Report',401,'Quantity on Hand','Quantity on Hand','','IN','ON_HAND_TYPE_CSV','','VARCHAR2','Y','Y','2','','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_LISTING_V','','','US','');
--Inserting Dependent Parameters - Inventory Listing Report
--Inserting Report Conditions - Inventory Listing Report
xxeis.eis_rsc_ins.rcnh( 'Inventory Listing Report',401,'SUBINVENTORY_CODE IN :Subinventory ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SUBINVENTORY_CODE','','Subinventory','','','','','EIS_XXWC_INV_LISTING_V','','','','','','IN','Y','Y','','','','','1',401,'Inventory Listing Report','SUBINVENTORY_CODE IN :Subinventory ');
--Inserting Report Sorts - Inventory Listing Report
xxeis.eis_rsc_ins.rs( 'Inventory Listing Report',401,'PART_NUMBER','ASC','SA059956','1','');
--Inserting Report Triggers - Inventory Listing Report
xxeis.eis_rsc_ins.rt( 'Inventory Listing Report',401,'begin
xxeis.eis_xxwc_inv_list_rpt_pkg.g_process_id := :SYSTEM.PROCESS_ID;
xxeis.eis_xxwc_inv_list_rpt_pkg.g_qty_onhand := :Quantity on Hand;
xxeis.eis_xxwc_inv_list_rpt_pkg.populate_Inv_List_Details(P_process_id    => :SYSTEM.PROCESS_ID,
                                    p_organization  => :Organization,
                                    p_qty_on_hand           => :Quantity on Hand,
                                    p_subinv  => :Subinventory);
end;','B','Y','SA059956','AQ');
--inserting report templates - Inventory Listing Report
--Inserting Report Portals - Inventory Listing Report
xxeis.eis_rsc_ins.r_port( 'Inventory Listing Report','XXWC_PUR_TOP_RPTS','401','Inventory Listing','Negative On-Hand','OA.jsp?page=/eis/oracle/apps/xxeis/central/reporting/webui/EISRSCLaunchPG&EisProduct=Reporting&Portal=Yes&mod=Inventory','','Pivot Excel,EXCEL,','CONC','N','SA059956');
--inserting report dashboards - Inventory Listing Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Inventory Listing Report','401','EIS_XXWC_INV_LISTING_V','EIS_XXWC_INV_LISTING_V','N','');
--inserting report security - Inventory Listing Report
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','XXWC_AO_BIN_MTN_REC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','XXWC_AO_BIN_MTN_CYCLE_INV_ADJ',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','XXWC_AO_BIN_MTN_CYCLE_REC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','XXWC_AO_INV_ADJ_REC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','660','','XXWC_AO_OEENTRY_REC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','XXWC_AO_REC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','HDS_INVNTRY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','XXWC_INVENTORY_CONTROL_INQUIRY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','XXWC_INVENTORY_CONTROL_SR_MGR',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','XXWC_INV_PLANNER',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','XXWC_INVENTORY_SPEC_SCC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','XXWC_INVENTORY_SUPER_USER',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','660','','XXWC_ORDER_MGMT_PRICING_FULL',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','XXWC_RECEIVING_ASSOCIATE',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','222','','HDS_RCVBLS_MNGR_WC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','222','','XXWC_RECEIVABLES_INQUIRY_WC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','20005','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','660','','XXWC_ORDER_MGMT_PRICING_SUPER',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','XXWC_AO_INV_ADJ',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','XXWC_AO_INV_ADJ_PO_RPT',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','20005','','XXWC_VIEW_ALL_EIS_REPORTS',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','201','','XXWC_PURCHASING_INQUIRY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory Listing Report','401','','XXWC_AO_BIN_MTN_PO_RPT',401,'SA059956','','','');
--Inserting Report Pivots - Inventory Listing Report
xxeis.eis_rsc_ins.rpivot( 'Inventory Listing Report',401,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory Listing Report',401,'Pivot','CAT_CLASS','PAGE_FIELD','','','4','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory Listing Report',401,'Pivot','DESCRIPTION','PAGE_FIELD','','','3','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory Listing Report',401,'Pivot','ONHAND','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory Listing Report',401,'Pivot','PART_NUMBER','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory Listing Report',401,'Pivot','SEGMENT_LOCATION','PAGE_FIELD','','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory Listing Report',401,'Pivot','VENDOR_NAME','PAGE_FIELD','','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory Listing Report',401,'Pivot','BRANCH_NUMBER','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory Listing Report',401,'Pivot','SUBINVENTORY_CODE','ROW_FIELD','','','1','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- Inventory Listing Report
xxeis.eis_rsc_ins.rv( 'Inventory Listing Report','','Inventory Listing Report','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
