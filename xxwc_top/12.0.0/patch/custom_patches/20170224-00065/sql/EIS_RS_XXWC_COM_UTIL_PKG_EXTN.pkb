CREATE OR REPLACE PACKAGE BODY  XXEIS.EIS_RS_XXWC_COM_UTIL_PKG_EXTN AS
--//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "Specials Report"
--//
--// Object Name         		:: XXEIS.EIS_RS_XXWC_COM_UTIL_PKG_EXTN
--//
--// Object Type         		:: Package Body
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	      04/12/2016    Initial Build --TMS#20151216-00168  --performance tuning
--// 1.1        Siva              06/14/2016     TMS#20160614-00010 --performance tuning
--// 1.2        Siva              08/02/2016     TMS#20160708-00081	
--// 1.3     	Siva 			 15-Mar-2017     TMS#20170224-00065	
--//============================================================================
FUNCTION GET_INV_CAT_CLASS (P_INVENTORY_ITEM_ID in NUMBER,P_organization_id in number) RETURN VARCHAR2
  IS
  --//============================================================================
--//
--// Object Name         :: GET_INV_CAT_CLASS  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       PRAMOD       	04/12/2016     Initial Build --TMS#20151216-00168 --performance tuning
--// 1.2        Siva              08/02/2016     TMS#20160708-00081		
--//============================================================================
L_HASH_INDEX varchar2(200);
l_sql varchar2(32000);
begin
G_CONCATENATED_SEGMENTS := NULL;
l_sql :=' SELECT Mcv.concatenated_segments
            from  Mtl_Categories_Kfv Mcv,
                  MTL_CATEGORY_SETS MCS,
                  Mtl_Item_Categories Mic
            Where   Mcs.Category_Set_Name      = ''Inventory Category''
          And Mcs.Structure_Id                 = Mcv.Structure_Id
          And Mic.Inventory_Item_Id            = :1
          And Mic.Organization_Id              = :2
          AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
          And Mic.Category_Id                  = Mcv.Category_Id';
    begin
        l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_organization_id; --added for version 1.2
        G_CONCATENATED_SEGMENTS := G_cat_class_VLDN_TBL(l_hash_index);
    exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO G_CONCATENATED_SEGMENTS USING P_INVENTORY_ITEM_ID,P_organization_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_CONCATENATED_SEGMENTS :=null;
    WHEN OTHERS THEN
    G_CONCATENATED_SEGMENTS :=null;
    END;     
    
                       L_HASH_INDEX:= P_INVENTORY_ITEM_ID||'-'||P_organization_id;  --added for version 1.2
                       G_CAT_CLASS_VLDN_TBL(L_HASH_INDEX) := G_CONCATENATED_SEGMENTS;
                        
    end;
    RETURN  G_CONCATENATED_SEGMENTS;
    EXCEPTION WHEN OTHERS THEN
      G_CONCATENATED_SEGMENTS:=null;  
      
      RETURN  G_CONCATENATED_SEGMENTS;

end GET_INV_CAT_CLASS;


  FUNCTION GET_LIST_PRICE (P_INVENTORY_ITEM_ID in NUMBER,P_organization_id in number) RETURN number
IS
--//============================================================================
--//
--// Object Name         :: GET_LIST_PRICE  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       PRAMOD       	04/12/2016     Initial Build --TMS#20151216-00168 --performance tuning
--//============================================================================
L_HASH_INDEX varchar2(200);
l_sql varchar2(32000);
begin
G_LIST_PRICE := NULL;
l_sql :=' select nvl(unit_list_price,0)
      from oe_order_lines
      where line_id in
            (
             select max(oel.line_id)
            from oe_order_lines oel
            where oel.inventory_item_id = :1
            and oel.ship_from_org_id    = :2
            )';
    begin
        l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
        G_LIST_PRICE := G_LIST_PRICE_VLDN_TBL(l_hash_index);
    exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO G_LIST_PRICE USING P_INVENTORY_ITEM_ID,P_organization_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_LIST_PRICE :=0;
    WHEN OTHERS THEN
    G_LIST_PRICE :=0;
    END;      
                       l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
                       G_LIST_PRICE_VLDN_TBL(L_HASH_INDEX) := G_LIST_PRICE;
    end;
    RETURN  G_LIST_PRICE;
    EXCEPTION WHEN OTHERS THEN
      G_LIST_PRICE:=0;
      RETURN  G_LIST_PRICE;

end GET_LIST_PRICE;

/* --Commented for version 1.1
  FUNCTION GET_PO_VENDOR_NAME (P_INVENTORY_ITEM_ID in NUMBER,P_organization_id in number) RETURN VARCHAR2
IS
--//============================================================================
--//
--// Object Name         :: GET_PO_VENDOR_NAME  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       PRAMOD       	04/12/2016     Initial Build --TMS#20151216-00168 --performance tuning
--//============================================================================
L_HASH_INDEX varchar2(100);
l_sql varchar2(32000);
begin
G_VENDOR_NAME := NULL;
l_sql :=' select max(PV.VENDOR_NAME)
from PO_VENDORS PV,
PO_LINES_ALL POL,
PO_LINE_LOCATIONS_ALL POLL,
po_headers_all poh
where POL.ITEM_ID = :1
and POLL.SHIP_TO_ORGANIZATION_ID = :2
and POL.PO_LINE_ID = POLL.PO_LINE_ID
and POH.PO_HEADER_ID = POL.PO_HEADER_ID
and poh.vendor_id = pv.vendor_id';
    begin
        l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
        G_VENDOR_NAME := G_VENDOR_NAME_VLDN_TBL(l_hash_index);
    exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO G_VENDOR_NAME USING P_INVENTORY_ITEM_ID,P_organization_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_VENDOR_NAME :=null;
    WHEN OTHERS THEN
    G_VENDOR_NAME :=null;
    END;      
                       l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
                       G_VENDOR_NAME_VLDN_TBL(L_HASH_INDEX) := G_VENDOR_NAME;
    end;
    RETURN  G_VENDOR_NAME;
    EXCEPTION WHEN OTHERS THEN
      G_VENDOR_NAME:=null;
      RETURN  G_VENDOR_NAME;

end GET_PO_VENDOR_NAME;


  FUNCTION GET_PO_VENDOR_NUMBER (P_INVENTORY_ITEM_ID in NUMBER,P_organization_id in number) RETURN VARCHAR2
IS
--//============================================================================
--//
--// Object Name         :: GET_PO_VENDOR_NUMBER  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       PRAMOD       	04/12/2016     Initial Build --TMS#20151216-00168 --performance tuning
--//============================================================================
L_HASH_INDEX varchar2(100);
l_sql varchar2(32000);
begin
G_VENDOR_NUMBER := NULL;
l_sql :=' select max(PV.segment1)
from PO_VENDORS PV,
PO_LINES_ALL POL,
PO_LINE_LOCATIONS_ALL POLL,
po_headers_all poh
where POL.ITEM_ID = :1
and POLL.SHIP_TO_ORGANIZATION_ID = :2
and POL.PO_LINE_ID = POLL.PO_LINE_ID
and POH.PO_HEADER_ID = POL.PO_HEADER_ID
and poh.vendor_id = pv.vendor_id';
    begin
        l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
        G_VENDOR_NUMBER := G_VENDOR_NUMBER_VLDN_TBL(l_hash_index);
    exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO G_VENDOR_NUMBER USING P_INVENTORY_ITEM_ID,P_organization_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_VENDOR_NUMBER :=null;
    WHEN OTHERS THEN
    G_VENDOR_NUMBER :=null;
    END;      
                       l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
                       G_VENDOR_NUMBER_VLDN_TBL(L_HASH_INDEX) := G_VENDOR_NUMBER;
    end;
    RETURN  G_VENDOR_NUMBER;
    EXCEPTION WHEN OTHERS THEN
      G_VENDOR_NUMBER:=null;
      RETURN  G_VENDOR_NUMBER;

end GET_PO_VENDOR_NUMBER;
*/--Commented for version 1.1

  FUNCTION GET_ONHAND_DATE (P_INVENTORY_ITEM_ID in NUMBER,P_organization_id in number) RETURN date
IS
--//============================================================================
--//
--// Object Name         :: GET_ONHAND_DATE  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       PRAMOD       	04/12/2016     Initial Build --TMS#20151216-00168 --performance tuning
--//============================================================================
L_HASH_INDEX varchar2(100);
l_sql varchar2(32000);
begin
G_ONHAND_DATE := NULL;
l_sql :=' select max(date_received)
    from mtl_onhand_quantities_detail moq
    Where Inventory_Item_Id          =:1
    and organization_id              =:2';
    begin
        l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
        G_ONHAND_DATE := G_ONHAND_DATE_VLDN_TBL(l_hash_index);
    exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO G_ONHAND_DATE USING P_INVENTORY_ITEM_ID,P_organization_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_ONHAND_DATE :=to_date(null);
    WHEN OTHERS THEN
    G_ONHAND_DATE :=to_date(null);
    END;      
                       l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
                       G_ONHAND_DATE_VLDN_TBL(L_HASH_INDEX) := G_ONHAND_DATE;
    end;
    RETURN  G_ONHAND_DATE;
    EXCEPTION WHEN OTHERS THEN
      G_ONHAND_DATE:=to_date(null);
      RETURN  G_ONHAND_DATE;

end GET_ONHAND_DATE;


  FUNCTION GET_OLDEST_BORN_DATE (P_INVENTORY_ITEM_ID in NUMBER,P_organization_id in number) RETURN date
IS
--//============================================================================
--//
--// Object Name         :: GET_OLDEST_BORN_DATE  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       PRAMOD       	04/12/2016     Initial Build --TMS#20151216-00168 --performance tuning
--//============================================================================
L_HASH_INDEX varchar2(100);
l_sql varchar2(32000);
begin
G_OLDEST_BORN_DATE := NULL;
l_sql :=' select trunc(min(orig_date_received))
    from mtl_onhand_quantities_detail moq
    Where Inventory_Item_Id          =:1
    and organization_id              =:2';
    begin
        l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
        G_OLDEST_BORN_DATE := G_OLDEST_BORN_DATE_VLDN_TBL(l_hash_index);
    exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO G_OLDEST_BORN_DATE USING P_INVENTORY_ITEM_ID,P_organization_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_OLDEST_BORN_DATE :=to_date(null);
    WHEN OTHERS THEN
    G_OLDEST_BORN_DATE :=to_date(null);
    END;      
                       l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
                       G_OLDEST_BORN_DATE_VLDN_TBL(L_HASH_INDEX) := G_OLDEST_BORN_DATE;
    end;
     RETURN  G_OLDEST_BORN_DATE;
    EXCEPTION WHEN OTHERS THEN
      G_OLDEST_BORN_DATE:=to_date(null);
      RETURN  G_OLDEST_BORN_DATE;

end GET_OLDEST_BORN_DATE;

  FUNCTION GET_OPEN_SALES_ORDERS (P_INVENTORY_ITEM_ID in NUMBER,P_organization_id in number) RETURN number
IS
--//============================================================================
--//
--// Object Name         :: GET_OPEN_SALES_ORDERS  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       PRAMOD       	04/12/2016     Initial Build --TMS#20151216-00168 --performance tuning
--//============================================================================
L_HASH_INDEX varchar2(100);
l_sql varchar2(32000);
begin
G_OPEN_SALES_ORDERS := NULL;
l_sql :='SELECT COUNT (oh.order_number)
        FROM oe_order_headers oh, oe_order_lines ol
       WHERE     oh.header_id = ol.header_id
             AND ol.inventory_item_id = :1
             AND ol.ship_from_org_id = :2
             AND ol.flow_status_code NOT IN (''CLOSED'',''CANCELLED'')';
    begin
        l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
        G_OPEN_SALES_ORDERS := G_OPEN_SALES_ORDERS_VLDN_TBL(l_hash_index);
    exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO G_OPEN_SALES_ORDERS USING P_INVENTORY_ITEM_ID,P_organization_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_OPEN_SALES_ORDERS :=0;
    WHEN OTHERS THEN
    G_OPEN_SALES_ORDERS :=0;
    END;      
                       l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
                       G_OPEN_SALES_ORDERS_VLDN_TBL(L_HASH_INDEX) := G_OPEN_SALES_ORDERS;
    end;
     RETURN  G_OPEN_SALES_ORDERS;
    EXCEPTION WHEN OTHERS THEN
      G_OPEN_SALES_ORDERS:=0;
      RETURN  G_OPEN_SALES_ORDERS;

end GET_OPEN_SALES_ORDERS;


  FUNCTION GET_ITEM_PURCHASED (P_INVENTORY_ITEM_ID in NUMBER,P_organization_id in number) RETURN number
IS
--//============================================================================
--//
--// Object Name         :: GET_ITEM_PURCHASED  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       PRAMOD       	04/12/2016     Initial Build --TMS#20151216-00168 --performance tuning
--//============================================================================
L_HASH_INDEX varchar2(100);
l_sql varchar2(32000);
begin
G_ITEM_PURCHASED := NULL;
l_sql :=' SELECT count(DISTINCT poh.po_header_id)
        FROM po_headers poh,
        po_lines pol,
        po_line_locations poll,
        mtl_system_items_kfv msi
        WHERE poh.po_header_id=pol.po_header_id
        AND pol.po_line_id=poll.po_line_id
        AND msi.inventory_item_id=pol.item_id
        AND msi.organization_id=poll.ship_to_organization_id
        AND msi.inventory_item_id=:1
        and MSI.ORGANIZATION_ID=:2
        AND trunc(poh.creation_date) >= sysdate -365';
    begin
        l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
        G_ITEM_PURCHASED := G_ITEM_PURCHASED_VLDN_TBL(l_hash_index);
    exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO G_ITEM_PURCHASED USING P_INVENTORY_ITEM_ID,P_organization_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_ITEM_PURCHASED :=0;
    WHEN OTHERS THEN
    G_ITEM_PURCHASED :=0;
    END;      
                       l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
                       G_ITEM_PURCHASED_VLDN_TBL(L_HASH_INDEX) := G_ITEM_PURCHASED;
    end;
     RETURN  G_ITEM_PURCHASED;
    EXCEPTION WHEN OTHERS THEN
      G_ITEM_PURCHASED:=0;
      RETURN  G_ITEM_PURCHASED;

end GET_ITEM_PURCHASED;


  FUNCTION GET_ITEM_COST(p_num in number,P_INVENTORY_ITEM_ID in NUMBER,P_organization_id in number) RETURN number
IS
--//============================================================================
--//
--// Object Name         :: GET_ITEM_COST  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       PRAMOD       	04/12/2016     Initial Build --TMS#20151216-00168 --performance tuning
--//============================================================================
L_HASH_INDEX varchar2(100);
L_SQL varchar2(32000);
begin
G_ITEM_COST := NULL;

    BEGIN
        l_hash_index:=p_inventory_item_id||'-'||P_organization_id;
        G_ITEM_COST :=G_ITEM_COST_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    BEGIN      
G_ITEM_COST:=APPS.CST_COST_API.GET_ITEM_COST(P_NUM,P_INVENTORY_ITEM_ID,P_ORGANIZATION_ID);

    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_ITEM_COST :=0;
    WHEN OTHERS THEN
    G_ITEM_COST :=0;
    END;      
                      l_hash_index:=p_inventory_item_id||'-'||P_organization_id;
                       G_ITEM_COST_VLDN_TBL(L_HASH_INDEX) := G_ITEM_COST;
    END;
     return  G_ITEM_COST;
     EXCEPTION WHEN OTHERS THEN
      G_ITEM_COST:=0;
      RETURN  G_ITEM_COST;

end get_item_cost;


FUNCTION GET_PO_VENDOR_DTLS (P_INVENTORY_ITEM_ID in NUMBER,P_organization_id in number,P_TYPE VARCHAR2) RETURN VARCHAR2
IS
--//============================================================================
--//
--// Object Name         :: GET_PO_VENDOR_DTLS  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.1       Siva       	06/14/2016     Initial Build --20160614-00010 --performance tuning		
--//============================================================================
L_HASH_INDEX varchar2(100);
l_sql varchar2(32000);
begin
G_VENDOR_NAME := NULL;
G_VENDOR_NUMBER := NULL;
l_sql :='SELECT PV.VENDOR_NAME,PV.segment1
    FROM PO_VENDORS PV,
      PO_LINES_ALL POL,
      PO_LINE_LOCATIONS_ALL POLL,
      po_headers_all poh
    WHERE POL.ITEM_ID                = :1
    AND POLL.SHIP_TO_ORGANIZATION_ID = :2
    AND POL.PO_LINE_ID               = POLL.PO_LINE_ID
    AND POL.PO_HEADER_ID             = poh.PO_HEADER_ID
    AND POH.VENDOR_ID                = PV.VENDOR_ID
    AND rownum                       = 1';
  begin
        l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
	IF P_TYPE           = 'VENDORNAME' THEN
     G_VENDOR_NAME   := G_VENDOR_NAME_VLDN_TBL(l_hash_index);
    ELSIF P_TYPE        = 'VENDORNUM' THEN
     G_VENDOR_NUMBER := G_VENDOR_NUMBER_VLDN_TBL(l_hash_index);
	END IF;	    
    exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO G_VENDOR_NAME,G_VENDOR_NUMBER USING P_INVENTORY_ITEM_ID,P_organization_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    IF P_TYPE           = 'VENDORNAME' THEN
    G_VENDOR_NAME 		:= NULL;
    ELSIF P_TYPE        = 'VENDORNUM' THEN
    G_VENDOR_NUMBER     := NULL;
	END IF;
    END;      
                       l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_organization_id;
                       G_VENDOR_NAME_VLDN_TBL(L_HASH_INDEX)   := G_VENDOR_NAME;
                       G_VENDOR_NUMBER_VLDN_TBL(l_hash_index) := G_VENDOR_NUMBER;
    end;
	  IF P_TYPE='VENDORNAME' THEN
    RETURN G_VENDOR_NAME;
  ELSIF P_TYPE='VENDORNUM' THEN
    RETURN G_VENDOR_NUMBER ;
	  END IF;
    EXCEPTION WHEN OTHERS THEN
     IF P_TYPE      ='VENDORNAME' THEN
    G_VENDOR_NAME := NULL;
    RETURN G_VENDOR_NAME;
  ELSIF P_TYPE   ='VENDORNUM' THEN
    G_VENDOR_NUMBER    := NULL;
    RETURN G_VENDOR_NUMBER ;
  END IF;
END GET_PO_VENDOR_DTLS;

Function get_process_id return number as
--//============================================================================
--//
--// Object Name         :: get_process_id  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and populates to view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.2        Siva             08/02/2016     TMS#20160708-00081	
--//============================================================================
l_process_id  number;
begin
l_process_id:=g_process_id;
return l_process_id;
EXCEPTION
WHEN OTHERS THEN
RETURN NULL;
END get_process_id;


PROCEDURE SPECIAL_REP_PRC (P_PROCESS_ID       IN NUMBER
                          ,P_ORGANIZATION_CODE      IN VARCHAR2)
IS
--//============================================================================
--//
--// Object Name         :: SPECIAL_REP_PRC  
--//
--// Object Usage 		 :: This Object Referred by "Specials Report"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure dump the data in to custom table XXWC_INV_SPCL_ONHAND_TBL
--//                       and populates in view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.2        Siva             08/02/2016     TMS#20160708-00081	
--// 1.3     	Siva 			 15-Mar-2017     TMS#20170224-00065 
--//============================================================================
l_spcl_onhand_csr SYS_REFCURSOR; 

LC_WHERE_COND VARCHAR2(32000);

  TYPE XXWC_SPCL_ONHAND_TYPE
is
  TABLE OF XXEIS.XXWC_INV_SPCL_ONHAND_TBL%ROWTYPE INDEX BY BINARY_INTEGER;
  xxwc_spcl_onhand_rec_tab XXWC_SPCL_ONHAND_TYPE;
  
  l_spcl_onhand_qry VARCHAR2(32000):='SELECT '||p_process_id ||' process_id,
xxeis.eis_po_xxwc_isr_util_qa_pkg.get_planning_quantity (2 ,1 ,msi.organization_id ,NULL ,msi.inventory_item_id) onhand,
msi.INVENTORY_ITEM_ID,
msi.ORGANIZATION_ID
FROM 
apps.mtl_system_items_b msi
where MSI.ITEM_TYPE = ''SPECIAL''
';
  

BEGIN
xxwc_spcl_onhand_rec_tab.DELETE;

IF P_ORGANIZATION_CODE!='All' THEN
  LC_WHERE_COND        := 'and msi.organization_id in( select organization_id from mtl_parameters where organization_code in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_ORGANIZATION_CODE)||' ))';
ELSE
  LC_WHERE_COND:= 'and 1=1';
END IF;

 l_spcl_onhand_qry := l_spcl_onhand_qry||' '||LC_WHERE_COND;
 
 dbms_output.put_line('l_spcl_onhand_qry '|| l_spcl_onhand_qry);
  
 OPEN l_spcl_onhand_csr FOR l_spcl_onhand_qry ;
LOOP
  FETCH l_spcl_onhand_csr BULK COLLECT INTO xxwc_spcl_onhand_rec_tab LIMIT 10000;
  dbms_output.put_line('xxwc_spcl_onhand_rec_tab'||xxwc_spcl_onhand_rec_tab.count);
  IF xxwc_spcl_onhand_rec_tab.count >= 1 THEN
    forall j IN 1 .. xxwc_spcl_onhand_rec_tab.count
    INSERT INTO xxeis.XXWC_INV_SPCL_ONHAND_TBL VALUES xxwc_spcl_onhand_rec_tab(j);
   -- COMMIT; --Commented for version 1.3   
  END IF;
  IF l_spcl_onhand_csr%notfound THEN
    CLOSE l_spcl_onhand_csr;
    EXIT;
  END IF;
END LOOP; 
  
END;


/* --Commented code for version 1.3                           
   PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER)
   AS
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.2      02-Aug-2016        Siva   		TMS#20160708-00081
--//============================================================================ 
  BEGIN  
  DELETE FROM XXEIS.XXWC_INV_SPCL_ONHAND_TBL WHERE PROCESS_ID=P_PROCESS_ID;
  COMMIT;
END ;   
*/ --Commented code for version 1.3   

end eis_rs_xxwc_com_util_pkg_extn;
/
