CREATE OR REPLACE 
PACKAGE    XXEIS.EIS_XXWC_OM_OPEN_SALES_ORD_PKG
IS
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "OPEN SALES ORDERS REPORT"
--//
--// Object Name         		:: EIS_XXWC_OM_OPEN_SALES_ORD_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//								and also provides some values in the view through functions.
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        SIVA  	    06/17/2016    Initial Build  --TMS#20160617-00018    --performance Tuning
--// 1.1     	Siva 		15-Mar-2017     TMS#20170224-00065 
--//============================================================================

  TYPE T_CASHTYPE_VLDN_TBL IS TABLE OF VARCHAR2(100) INDEX BY VARCHAR2(100);
  G_EMPOLYEE_NAME_VLDN_TBL T_CASHTYPE_VLDN_TBL;
  G_EMPOLYEE_NAME  varchar2(200);
  
   Type CURSOR_TYPE is ref cursor;
   
   function GET_CREATED_BY_NAME (P_EMPLOYEE_ID in number,P_DATE in date)  RETURN VARCHAR2;
   --//============================================================================
--//
--// Object Name         :: get_created_by_name  
--//
--// Object Usage 		 :: This Object Referred by "OPEN SALES ORDERS REPORT"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This  function is created for EIS_XXWC_OM_OPEN_ORDERS_V View to get the Created by name value.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        SIVA  	  06/17/2016       Initial Build  --TMS#20160617-00018    --performance Tuning
--//============================================================================
   
   PROCEDURE OPEN_SALES_ORDER_PRE_PROC(p_process_id          number,
                                       p_org_id              VARCHAR2,
                                       p_salesrep_name       varchar2,
                                       p_ordered_date_from   date,
                                       p_ordered_date_to     date,
                                       p_order_line_status   varchar2,
                                       p_order_header_status varchar2,
                                       p_order_type          varchar2,
                                       p_cust_num            varchar2,
                                       p_cust_name           varchar2,      
                                       p_cust_job_name       VARCHAR2,
                                       P_CREATED_BY          varchar2,
                                       p_shipping_method     varchar2,
                                       p_schedule_date_from  date,
                                       p_schedule_date_to    date,
                                       p_payment_terms       varchar2);
--//============================================================================
--//
--// Object Name         :: OPEN_SALES_ORDER_PRE_PROC  
--//
--// Object Usage 		 :: This Object Referred by "OPEN SALES ORDERS REPORT"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_OM_OPEN_ORDERS_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        SIVA  	  06/17/2016       Initial Build  --TMS#20160617-00018   --performance Tuning
--//============================================================================

/* --Commented code for version 1.1
procedure CLEAR_TEMP_TABLES ( P_PROCESS_ID in number);
--//============================================================================
--//
--// Object Name         :: CLEAR_TEMP_TABLES  
--//
--// Object Usage 		   :: This Object Referred by "OPEN SALES ORDERS REPORT"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure will call in After report and delete data from Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        SIVA  	     06/17/2016       Initial Build  --TMS#20160617-00018   --performance Tuning
--//============================================================================
*/ --Commented code for version 1.1

END EIS_XXWC_OM_OPEN_SALES_ORD_PKG;
/
