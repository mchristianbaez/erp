--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: XXEIS.XXWC_INV_SPCL_ONHAND_TBL
  Description: This table is used to get data from XXEIS.EIS_RS_XXWC_COM_UTIL_PKG_EXTN Package.
				All rows for each process are deleted after each run. This table should normally have 0 rows.
  HISTORY
  ===============================================================================
  VERSION 		DATE            AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0	  	03-Aug-2016		 	Siva			 TMS#20160708-00081
  1.1      15-Mar-2017      	Siva   		 	 TMS#20170224-00065 
********************************************************************************/
DROP TABLE XXEIS.XXWC_INV_SPCL_ONHAND_TBL CASCADE CONSTRAINTS --added for version 1.1
/
CREATE GLOBAL TEMPORARY TABLE XXEIS.XXWC_INV_SPCL_ONHAND_TBL	--added for version 1.1
  (
    PROCESS_ID        NUMBER,
    ONHAND            NUMBER,
    INVENTORY_ITEM_ID NUMBER,
    ORGANIZATION_ID   NUMBER
  )
  ON COMMIT PRESERVE ROWS  --added for version 1.1
/

COMMENT ON TABLE XXEIS.XXWC_INV_SPCL_ONHAND_TBL IS 'This table is used to get data from XXEIS.EIS_RS_XXWC_COM_UTIL_PKG_EXTN Package.
All rows for each process are deleted after each run. This table should normally have 0 rows.'  
/
