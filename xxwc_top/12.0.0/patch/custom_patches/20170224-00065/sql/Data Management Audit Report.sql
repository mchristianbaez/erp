--Report Name            : Data Management Audit Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_DATA_MGMT_AUDIT_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_DATA_MGMT_AUDIT_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_DATA_MGMT_AUDIT_V',401,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Data Mgmt Audit V','EXDMAV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_DATA_MGMT_AUDIT_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_DATA_MGMT_AUDIT_V',401,FALSE);
--Inserting Object Columns for EIS_XXWC_DATA_MGMT_AUDIT_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','EXPENSE_ACCOUNT',401,'Expense Account','EXPENSE_ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Expense Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','SALES_ACCOUNT',401,'Sales Account','SALES_ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Sales Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','COGS_ACCOUNT',401,'Cogs Account','COGS_ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Cogs Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','ITEM_CREATION_DATE',401,'Item Creation Date','ITEM_CREATION_DATE','','','','ANONYMOUS','DATE','','','Item Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','WEB_LONG_DESCRIPTION',401,'Web Long Description','WEB_LONG_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Web Long Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','WEB_KEYWORDS',401,'Web Keywords','WEB_KEYWORDS','','','','ANONYMOUS','VARCHAR2','','','Web Keywords','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','WEB_SHORT_DESCRIPTION',401,'Web Short Description','WEB_SHORT_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Web Short Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','WEB_SEQUENCE',401,'Web Sequence','WEB_SEQUENCE','','','','ANONYMOUS','NUMBER','','','Web Sequence','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','THUMBNAIL_IMAGE_1',401,'Thumbnail Image 1','THUMBNAIL_IMAGE_1','','','','ANONYMOUS','VARCHAR2','','','Thumbnail Image 1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','FULLSIZE_IMAGE_1',401,'Fullsize Image 1','FULLSIZE_IMAGE_1','','','','ANONYMOUS','VARCHAR2','','','Fullsize Image 1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','CYBERSOURCE_TAX_CODE',401,'Cybersource Tax Code','CYBERSOURCE_TAX_CODE','','','','ANONYMOUS','VARCHAR2','','','Cybersource Tax Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','BULKY_ITEM_FLAG',401,'Bulky Item Flag','BULKY_ITEM_FLAG','','','','ANONYMOUS','VARCHAR2','','','Bulky Item Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','HAZMAT_FLAG',401,'Hazmat Flag','HAZMAT_FLAG','','','','ANONYMOUS','VARCHAR2','','','Hazmat Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','WEB_MFG_PART_NUMBER',401,'Web Mfg Part Number','WEB_MFG_PART_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Web Mfg Part Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','BRAND',401,'Brand','BRAND','','','','ANONYMOUS','VARCHAR2','','','Brand','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','WC_WEB_HIERARCHY',401,'Wc Web Hierarchy','WC_WEB_HIERARCHY','','','','ANONYMOUS','VARCHAR2','','','Wc Web Hierarchy','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','WEBSITE_ITEM',401,'Website Item','WEBSITE_ITEM','','','','ANONYMOUS','VARCHAR2','','','Website Item','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','INVENTORY_CATEGORY_DESCRIPTION',401,'Inventory Category Description','INVENTORY_CATEGORY_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Inventory Category Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','INVENTORY_CATEGORY',401,'Inventory Category','INVENTORY_CATEGORY','','','','ANONYMOUS','VARCHAR2','','','Inventory Category','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','BPA_NUMBER',401,'Bpa Number','BPA_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Bpa Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','BPA_COST',401,'Bpa Cost','BPA_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Bpa Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','AVERAGE_COST',401,'Average Cost','AVERAGE_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Average Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','LIST_PRICE_PER_UNIT',401,'List Price Per Unit','LIST_PRICE_PER_UNIT','','~T~D~2','','ANONYMOUS','NUMBER','','','List Price Per Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','FIXED_LOT_MULTIPLIER',401,'Fixed Lot Multiplier','FIXED_LOT_MULTIPLIER','','','','ANONYMOUS','NUMBER','','','Fixed Lot Multiplier','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','MFG_PART_NUMBER',401,'Mfg Part Number','MFG_PART_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Mfg Part Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','REGION',401,'Region','REGION','','','','ANONYMOUS','VARCHAR2','','','Region','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','DISTRICT',401,'District','DISTRICT','','','','ANONYMOUS','VARCHAR2','','','District','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','ORG_DESCRIPTION',401,'Org Description','ORG_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Org Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','ENGINEERING_ITEM_FLAG',401,'Engineering Item Flag','ENGINEERING_ITEM_FLAG','','','','ANONYMOUS','VARCHAR2','','','Engineering Item Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','USER_ITEM_TYPE',401,'User Item Type','USER_ITEM_TYPE','','','','ANONYMOUS','VARCHAR2','','','User Item Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','ITEM_STATUS',401,'Item Status','ITEM_STATUS','','','','ANONYMOUS','VARCHAR2','','','Item Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','ORG',401,'Org','ORG','','','','ANONYMOUS','VARCHAR2','','','Org','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','SOURCING_VENDOR_NAME',401,'Sourcing Vendor Name','SOURCING_VENDOR_NAME','','','','ANONYMOUS','VARCHAR2','','','Sourcing Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','SOURCING_VENDOR_NUMBER',401,'Sourcing Vendor Number','SOURCING_VENDOR_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Sourcing Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','SOURCE',401,'Source','SOURCE','','','','ANONYMOUS','VARCHAR2','','','Source','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','MASTER_VENDOR_NUMBER',401,'Master Vendor Number','MASTER_VENDOR_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Master Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','COUNTRY_OF_ORIGIN',401,'Country Of Origin','COUNTRY_OF_ORIGIN','','','','ANONYMOUS','VARCHAR2','','','Country Of Origin','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','SHELF_LIFE_DAYS',401,'Shelf Life Days','SHELF_LIFE_DAYS','','','','ANONYMOUS','NUMBER','','','Shelf Life Days','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','WEIGHT_UOM_CODE',401,'Weight Uom Code','WEIGHT_UOM_CODE','','','','ANONYMOUS','VARCHAR2','','','Weight Uom Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','WEIGHT',401,'Weight','WEIGHT','','','','ANONYMOUS','NUMBER','','','Weight','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','UOM',401,'Uom','UOM','','','','ANONYMOUS','VARCHAR2','','','Uom','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','LONG_DESCRIPTION',401,'Long Description','LONG_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Long Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','SHORT_DESCRIPTION',401,'Short Description','SHORT_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Short Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','ITEM',401,'Item','ITEM','','','','ANONYMOUS','VARCHAR2','','','Item','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','PRODUCT_LINE',401,'Product Line','PRODUCT_LINE','','','','ANONYMOUS','VARCHAR2','','','Product Line','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','AVP_CODE',401,'Avp Code','AVP_CODE','','','','ANONYMOUS','VARCHAR2','','','Avp Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','CONFLICT_MINERALS',401,'Conflict Minerals','CONFLICT_MINERALS','','','','ANONYMOUS','VARCHAR2','','','Conflict Minerals','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','FULFILLMENT',401,'Fulfillment','FULFILLMENT','','','','ANONYMOUS','VARCHAR2','','','Fulfillment','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','ITEM_LEVEL',401,'Item Level','ITEM_LEVEL','','','','ANONYMOUS','VARCHAR2','','','Item Level','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','RETAIL_SELLING_UOM',401,'Retail Selling Uom','RETAIL_SELLING_UOM','','','','ANONYMOUS','VARCHAR2','','','Retail Selling Uom','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','XPAR',401,'Xpar','XPAR','','','','ANONYMOUS','VARCHAR2','','','Xpar','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','CREATED_BY',401,'Created By','CREATED_BY','','','','ANONYMOUS','VARCHAR2','','','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','ENABLED_FLAG',401,'Enabled Flag','ENABLED_FLAG','','','','ANONYMOUS','VARCHAR2','','','Enabled Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','END_DATE_ACTIVE',401,'End Date Active','END_DATE_ACTIVE','','','','ANONYMOUS','DATE','','','End Date Active','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','START_DATE_ACTIVE',401,'Start Date Active','START_DATE_ACTIVE','','','','ANONYMOUS','DATE','','','Start Date Active','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','SUMMARY_FLAG',401,'Summary Flag','SUMMARY_FLAG','','','','ANONYMOUS','VARCHAR2','','','Summary Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','ANONYMOUS','NUMBER','','','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','PROCESS_ID',401,'Process Id','PROCESS_ID','','','','ANONYMOUS','NUMBER','','','Process Id','','','','US');
--Inserting Object Components for EIS_XXWC_DATA_MGMT_AUDIT_V
--Inserting Object Component Joins for EIS_XXWC_DATA_MGMT_AUDIT_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report LOV Data for Data Management Audit Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Data Management Audit Report
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT concatenated_segments item, description FROM mtl_system_items_kfv msi,
                org_organization_definitions ood
          WHERE msi.organization_id = ood.organization_id
            AND ood.operating_unit = fnd_profile.VALUE (''ORG_ID'')
       ORDER BY concatenated_segments','','EIS_INV_ITEM_LOV','List of all inventory items.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select INVENTORY_ITEM_STATUS_CODE
from mtl_item_status','','EIS_INV_ITEM_STATUS_LOV','Lists  the item status','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select distinct fcl.meaning
from fnd_common_lookups fcl
where fcl.lookup_type = ''ITEM_TYPE''
','','EIS_INV_USERITEM_TYPE_LOV','List values for User Item Type','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select vendor_name
from po_vendors
order by  vendor_name
','','EIS_INV_VENDOR_NAME_LOV','List all the Vendors','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select segment1 vendor_number from po_vendors','','EIS_INV_VENDOR_NUM_LOV','List the Vendor Numbers','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT organization_code code,organization_name name
    FROM  ORG_ORGANIZATION_DEFINITIONS OOD,
      HR_OPERATING_UNITS HOU
    WHERE  OOD.OPERATING_UNIT = HOU.ORGANIZATION_ID
      AND HOU.ORGANIZATION_ID  = FND_PROFILE.VALUE(''ORG_ID'')
      ORDER BY organization_code','','XXWC Inventory Org List','List of All Inventory Orgs under a given operating unit.','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Supplier'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Supplier List','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT  description Short_Description
             ,CONCATENATED_SEGMENTS ITEM
             FROM MTL_SYSTEM_ITEMS_KFV MSI,
                  ORG_ORGANIZATION_DEFINITIONS OOD,
                  HR_OPERATING_UNITS HOU
           WHERE msi.organization_id = ood.organization_id
             AND OOD.OPERATING_UNIT = HOU.ORGANIZATION_ID
            AND HOU.ORGANIZATION_ID  = FND_PROFILE.VALUE(''ORG_ID'')
             ORDER BY concatenated_segments','','EIS_INV_ITEM_DESC_LOV','EIS_INV_ITEM_DESC_LOV','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Source'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Source List','XXWC Source List','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select distinct source
       from xxeis.eis_xxwc_po_isr_tab expi,
           org_organization_definitions ood
          WHERE expi.organization_id = ood.organization_id
            and ood.operating_unit = fnd_profile.value (''ORG_ID'')','','EIS_SOURCE_LOV','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT DECODE(MCV.SEGMENT2,NULL,MCV.SEGMENT1,MCV.SEGMENT1
      ||''.''
      ||MCV.SEGMENT2) Website_Item
    FROM mtl_item_categories mic,
      mtl_categories_vl mcv,
      mtl_category_sets mcs
    WHERE 1=1
    AND mic.category_id         = mcv.category_id
    AND mic.category_set_id     = mcs.category_set_id
    AND mcs.structure_id        = mcv.structure_id
    AND mcs.category_set_name   = ''Website Item''','','EIS_INV_WEB_ITEM_CAT_LOV','','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report Data for Data Management Audit Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Data Management Audit Report
xxeis.eis_rsc_utility.delete_report_rows( 'Data Management Audit Report' );
--Inserting Report - Data Management Audit Report
xxeis.eis_rsc_ins.r( 401,'Data Management Audit Report','','Provides a comprehensive report of master, organization, classification, and website attributes.','','','','VP038429','EIS_XXWC_DATA_MGMT_AUDIT_V','Y','','','VP038429','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','XXEIS_RS_SUBMIT_REPORT','','','US','Y','','','');
--Inserting Report Columns - Data Management Audit Report
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'BULKY_ITEM_FLAG','Bulky Item Flag','Bulky Item Flag','','','default','','39','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'COGS_ACCOUNT','Cogs Account','Cogs Account','','','default','','49','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'COUNTRY_OF_ORIGIN','Country Of Origin','Country Of Origin','','','default','','9','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'CYBERSOURCE_TAX_CODE','Cybersource Tax Code','Cybersource Tax Code','','','default','','40','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'DISTRICT','District','District','','','default','','21','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'ENGINEERING_ITEM_FLAG','Engineering Item Flag','Engineering Item Flag','','','default','','19','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'EXPENSE_ACCOUNT','Expense Account','Expense Account','','','default','','51','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'FIXED_LOT_MULTIPLIER','Fixed Lot Multiplier','Fixed Lot Multiplier','','~~~','default','','24','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'FULLSIZE_IMAGE_1','Fullsize Image 1','Fullsize Image 1','','','default','','41','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'HAZMAT_FLAG','Hazmat Flag','Hazmat Flag','','','default','','38','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'INVENTORY_CATEGORY','Inventory Category','Inventory Category','','','default','','31','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'INVENTORY_CATEGORY_DESCRIPTION','Inventory Category Description','Inventory Category Description','','','default','','32','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'ITEM','Item','Item','','','default','','1','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'ITEM_CREATION_DATE','Item Creation Date','Item Creation Date','','','default','','47','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'ITEM_STATUS','Item Status','Item Status','','','default','','16','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'LIST_PRICE_PER_UNIT','List Price','List Price Per Unit','','~,~.~4','default','','25','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'LONG_DESCRIPTION','Long Description','Long Description','','','default','','3','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'MASTER_VENDOR_NUMBER','Master Vendor Number','Master Vendor Number','','','default','','10','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'MFG_PART_NUMBER','Mfg Part Number','Mfg Part Number','','','default','','23','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'ORG','Org','Org','','','default','','15','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'ORG_DESCRIPTION','Org Description','Org Description','','','default','','20','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'REGION','Region','Region','','','default','','22','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'SALES_ACCOUNT','Sales Account','Sales Account','','','default','','50','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'SHELF_LIFE_DAYS','Shelf Life Days','Shelf Life Days','','~~~','default','','7','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'SHORT_DESCRIPTION','Short Description','Short Description','','','default','','2','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'SOURCE','Source','Source','','','default','','11','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'SOURCING_VENDOR_NAME','Sourcing Vendor Name','Sourcing Vendor Name','','','default','','13','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'SOURCING_VENDOR_NUMBER','Sourcing Vendor Number','Sourcing Vendor Number','','','default','','12','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'THUMBNAIL_IMAGE_1','Thumbnail Image 1','Thumbnail Image 1','','','default','','42','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'UOM','Uom','Uom','','','default','','4','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'USER_ITEM_TYPE','User Item Type','User Item Type','','','default','','17','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'WC_WEB_HIERARCHY','Wc Web Hierarchy','Wc Web Hierarchy','','','default','','34','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'WEBSITE_ITEM','Website Item','Website Item','','','default','','33','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'WEB_KEYWORDS','Web Keywords','Web Keywords','','','default','','43','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'WEB_LONG_DESCRIPTION','Web Long Description','Web Long Description','','','default','','46','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'WEB_MFG_PART_NUMBER','Web Mfg Part Number','Web Mfg Part Number','','','default','','37','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'WEB_SEQUENCE','Web Sequence','Web Sequence','','~~~','default','','44','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'WEB_SHORT_DESCRIPTION','Web Short Description','Web Short Description','','','default','','45','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'WEIGHT','Weight','Weight','','~,~.~2','default','','5','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'WEIGHT_UOM_CODE','Weight Uom Code','Weight Uom Code','','','default','','6','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'PRODUCT_LINE','Product Line','Product Line','','','default','','36','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'AVP_CODE','Avp Code','Avp Code','','','default','','8','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'CONFLICT_MINERALS','Conflict Minerals','Conflict Minerals','','','default','','52','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'FULFILLMENT','Fulfillment','Fulfillment','','','default','','14','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'ITEM_LEVEL','Item Level','Item Level','','','default','','18','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'RETAIL_SELLING_UOM','Retail Selling Uom','Retail Selling Uom','','','default','','29','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'XPAR','Ext Prod Attr Grp','Xpar','','','default','','30','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'CREATED_BY','Created By','Created By','','','default','','48','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'AVERAGE_COST','Average Cost','Average Cost','','~,~.~4','default','','26','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'BPA_COST','Bpa Cost','Bpa Cost','','~,~.~4','default','','27','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'BPA_NUMBER','Bpa Number','Bpa Number','','','default','','28','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'BRAND','Brand','Brand','','','default','','35','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'ENABLED_FLAG','Enabled Flag','Enabled Flag','','','default','','53','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'END_DATE_ACTIVE','End Date Active','End Date Active','','','default','','56','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'START_DATE_ACTIVE','Start Date Active','Start Date Active','','','default','','55','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Data Management Audit Report',401,'SUMMARY_FLAG','Summary Flag','Summary Flag','','','default','','54','N','Y','','','','','','','VP038429','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','','US','');
--Inserting Report Parameters - Data Management Audit Report
xxeis.eis_rsc_ins.rp( 'Data Management Audit Report',401,'Item Number','Item','ITEM','IN','EIS_INV_ITEM_LOV','','VARCHAR2','N','Y','1','Y','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Data Management Audit Report',401,'Item Creation Date','Item Creation Date','','IN','','','DATE','N','Y','13','Y','N','CONSTANT','VP038429','Y','N','','Start Date','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Data Management Audit Report',401,'Item Status','Item Status','ITEM_STATUS','IN','EIS_INV_ITEM_STATUS_LOV','','VARCHAR2','N','Y','10','Y','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Data Management Audit Report',401,'Organization','Org','ORG','IN','XXWC Inventory Org List','','VARCHAR2','N','Y','8','Y','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Data Management Audit Report',401,'Source','Source','SOURCE','IN','EIS_SOURCE_LOV','','VARCHAR2','N','Y','6','Y','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Data Management Audit Report',401,'User Item Type','User Item Type','USER_ITEM_TYPE','IN','EIS_INV_USERITEM_TYPE_LOV','','VARCHAR2','N','Y','11','Y','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Data Management Audit Report',401,'Website Item','Website Item','WEBSITE_ITEM','IN','EIS_INV_WEB_ITEM_CAT_LOV','','VARCHAR2','N','Y','12','Y','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Data Management Audit Report',401,'Item List','Item List','','IN','XXWC Item List','','VARCHAR2','N','Y','3','Y','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Data Management Audit Report',401,'SourceList','Source List','','IN','XXWC Source List','','VARCHAR2','N','Y','7','Y','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Data Management Audit Report',401,'Master Vendor List','Master Vendor List','','IN','XXWC Supplier List','','VARCHAR2','N','Y','5','Y','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Data Management Audit Report',401,'OrganizationList','Organization List','','IN','XXWC Org List','','VARCHAR2','N','Y','9','Y','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Data Management Audit Report',401,'Master Vendor Number','Master Vendor Number','MASTER_VENDOR_NUMBER','IN','EIS_INV_VENDOR_NUM_LOV','','VARCHAR2','N','Y','4','Y','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Data Management Audit Report',401,'Master Vendor Name','Master Vendor Name','SOURCING_VENDOR_NAME','IN','EIS_INV_VENDOR_NAME_LOV','','VARCHAR2','N','Y','14','Y','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Data Management Audit Report',401,'Short Description','Short Description','SHORT_DESCRIPTION','LIKE','EIS_INV_ITEM_DESC_LOV','','VARCHAR2','N','Y','2','Y','N','CONSTANT','VP038429','Y','N','','','','EIS_XXWC_DATA_MGMT_AUDIT_V','','','US','');
--Inserting Dependent Parameters - Data Management Audit Report
--Inserting Report Conditions - Data Management Audit Report
xxeis.eis_rsc_ins.rcnh( 'Data Management Audit Report',401,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND PROCESS_ID    = :SYSTEM.PROCESS_ID','1',401,'Data Management Audit Report','Free Text ');
--Inserting Report Sorts - Data Management Audit Report
xxeis.eis_rsc_ins.rs( 'Data Management Audit Report',401,'ITEM','ASC','VP038429','1','1');
xxeis.eis_rsc_ins.rs( 'Data Management Audit Report',401,'ORG','ASC','VP038429','2','2');
--Inserting Report Triggers - Data Management Audit Report
xxeis.eis_rsc_ins.rt( 'Data Management Audit Report',401,'BEGIN
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.SET_DATE_FROM(:Item Creation Date);
XXEIS.EIS_XXWC_DATA_MANG_RPT_PKG.GET_DATA_MANG_DTLS(
P_PROCESS_ID    => (:SYSTEM.PROCESS_ID),
                              P_ITEM_LIST   => :Item List,
                              P_MASTER_VENDOR_LIST    => :Master Vendor List,
                              P_ORGANIZATION_LIST => :OrganizationList,
                              P_SOURCE_LIST =>:SourceList,
                              P_SHORT_DESC =>:Short Description,
                              P_Item_Number          =>:Item Number,
                              P_Master_Vendor_Number =>:Master Vendor Number,
                              P_Source 	      =>:Source,
                              P_Organization	=>:Organization,
                              P_Item_Status 	=>:Item Status,
                              P_User_Item_Type   =>:User Item Type,
                              P_Website_Item         =>:Website Item,
                              P_Master_Vendor_Name  =>:Master Vendor Name
);
END;','B','Y','VP038429','AQ');
xxeis.eis_rsc_ins.rt( 'Data Management Audit Report',401,'begin
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.parse_cleanup_table(:SYSTEM.PROCESS_ID);
end;','A','Y','VP038429','AQ');
--inserting report templates - Data Management Audit Report
--Inserting Report Portals - Data Management Audit Report
--inserting report dashboards - Data Management Audit Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Data Management Audit Report','401','EIS_XXWC_DATA_MGMT_AUDIT_V','EIS_XXWC_DATA_MGMT_AUDIT_V','N','');
--inserting report security - Data Management Audit Report
xxeis.eis_rsc_ins.rsec( 'Data Management Audit Report','','PP018915','',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Data Management Audit Report','401','','XXWC_DATA_MNGT_SC',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Data Management Audit Report','201','','XXWC_PUR_SUPER_USER',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Data Management Audit Report','','SG019472','',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Data Management Audit Report','20005','','XXWC_IT_OPERATIONS_ANALYST',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Data Management Audit Report','20005','','XXWC_VIEW_ALL_EIS_REPORTS',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Data Management Audit Report','','JM014915','',401,'VP038429','','','');
xxeis.eis_rsc_ins.rsec( 'Data Management Audit Report','','SS084202','',401,'VP038429','','','');
--Inserting Report Pivots - Data Management Audit Report
--Inserting Report   Version details- Data Management Audit Report
xxeis.eis_rsc_ins.rv( 'Data Management Audit Report','','Data Management Audit Report','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
