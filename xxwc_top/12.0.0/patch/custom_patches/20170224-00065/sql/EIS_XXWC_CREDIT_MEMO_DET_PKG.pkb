CREATE OR REPLACE PACKAGE BODY  XXEIS.EIS_XXWC_CREDIT_MEMO_DET_PKG as
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "White Cap Credit Memo Report"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_CREDIT_MEMO_DET_PKG
--//
--// Object Type         		:: Package Body
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     siva  			04/12/2016   Initial Build --TMS#20151216-00168  
--// 1.1     Siva 			15-Mar-2017     TMS#20170224-00065 
--//============================================================================
PROCEDURE POPULATE_CREDIT_INVOICES(P_PROCESS_ID in number,
                          P_PERIOD_YEAR in number,
                          P_PERIOD_MONTH in varchar2,
                          P_ORIGIN_DATE_FROM in date,
                          P_ORIGIN_DATE_TO in date,
                          p_location in varchar2
                          ) is
--//============================================================================
--//
--// Object Name         :: POPULATE_CREDIT_INVOICES  
--//
--// Object Usage 		 :: This Object Referred by "White Cap Credit Memo Report"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_AR_CREDIT_MEMO_DTLS_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  	04/12/2016   Initial Build --TMS#20151216-00168 
--// 1.1     Siva 			15-Mar-2017     TMS#20170224-00065  
--//============================================================================							  
l_stmt 				varchar2(32000);
L_PERIOD_COND 		VARCHAR2(32000);
l_stmt_hdr_qry1		VARCHAR2(32000);
l_stmt_hdr_qry2  VARCHAR2(32000);
LC_WHERE_COND       VARCHAR2(32000);
LC_PERIOD_NAME   VARCHAR2(32000);
L_REF_CURSOR1       CURSOR_TYPE4;
L_REF_CURSOR2       CURSOR_TYPE4;
L_REF_CURSOR3       CURSOR_TYPE4;

  LP_START_DATE varchar2(32);
  lp_end_date  varchar2(32);

TYPE TRX_REC IS RECORD
(CUSTOMER_TRX_ID NUMBER);

type trx_rec_tab is table of trx_rec Index By Binary_Integer;
	trx_tab trx_rec_tab;

type l_credit_memo_det_type
is
  table of XXEIS.EIS_XXWC_CREDIT_MEMO_DET_TAB%rowtype index by BINARY_INTEGER;
  l_credit_memo_det_tbl l_credit_memo_det_type;



 Begin
 TRX_TAB.DELETE;
 l_credit_memo_det_tbl.DELETE;
 
    IF P_PERIOD_YEAR      IS NOT NULL THEN
    LC_WHERE_COND:= LC_WHERE_COND||'and gp.period_year in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_PERIOD_YEAR)||' )';
  end if;
  
      IF P_PERIOD_MONTH      IS NOT NULL THEN
    LC_WHERE_COND:= LC_WHERE_COND||'and gp.period_name in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_PERIOD_MONTH)||' )';
  END IF;
 
   IF p_location      IS NOT NULL THEN
    LC_WHERE_COND:= LC_WHERE_COND||'and loc.organization_code in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_location)||' )';
  end if;
  
--  fnd_file.put_line(fnd_file.log,'Start');

-- Identify the driving table and Filtering the data based on the parameters
if P_ORIGIN_DATE_FROM is  null and P_ORIGIN_DATE_TO is null then
if P_PERIOD_MONTH is not null then 
select min(START_DATE),max(END_DATE) 
into lp_start_date,lp_end_date
from GL_PERIODS 
where PERIOD_NAME=P_PERIOD_MONTH;

else if P_PERIOD_YEAR is not null then
select min(START_DATE),max(END_DATE) 
into LP_START_DATE,LP_END_DATE
from GL_PERIODS 
where PERIOD_YEAR=P_PERIOD_YEAR;
end if;
end if;
end if;

--FND_FILE.PUT_LINE(FND_FILE.log,'LP_START_DATE'||LP_START_DATE);
--fnd_file.put_line(fnd_file.log,'LP_END_DATE'||LP_END_DATE);

if P_ORIGIN_DATE_FROM is not null and P_ORIGIN_DATE_TO is not null then
		l_stmt :='select max(ara.customer_trx_id) from
ar_receivable_applications ara,
ra_customer_trx rctr
where rctr.CUST_TRX_TYPE_ID =2
and ara.customer_trx_id =rctr.customer_trx_id
and application_type=''CM''
and ara.apply_date  between '''||P_ORIGIN_DATE_FROM||''' and '''||P_ORIGIN_DATE_TO||'''
group by ara.customer_trx_id';

elsif P_ORIGIN_DATE_FROM is  null and P_ORIGIN_DATE_TO is null then
		 		l_stmt :='select max(rctgr.customer_trx_id) from
ra_cust_trx_line_gl_dist     rctgr
where   1=1
and ''REC''                           = RCTGR.ACCOUNT_CLASS
AND ''Y''                             = rctgr.latest_rec_flag
AND rctgr.gl_date between '''||lp_start_date||''' and '''||LP_END_DATE||'''
group by rctgr.customer_trx_id';

end if;

-- fnd_file.put_line(fnd_file.log,'l_stmt'||l_stmt);
-- Process the driving data to below query

l_stmt_hdr_qry1 :='SELECT distinct '||P_Process_id||' process_id,
		     gp.period_year                     FISCAL_YEAR,
         gp.period_name                     FISCAL_MONTH,
         RCTO.TRX_DATE                      trx_date,
         PARTY_SITES.PARTY_SITE_NUMBER      CUSTOMER_NUMBER,
         CASE
         WHEN PSO.AMOUNT_DUE_REMAINING >0
         THEN PARTY_SITEB.PARTY_SITE_NUMBER
         ELSE TO_CHAR(0)
         END                                AR_CUSTOMER_NUMBER,
         sites.location                     ship_name,
         nvl(xocr.PAYMENT_TYPE_CODE,''ON ACCOUNT'') invoice_type,
         TRUNC(ohr.creation_date)          creation_date,
         TRUNC(rctr.trx_date)               invoice_date, 
         TRUNC(RCTGO.GL_DATE)               BUSINESS_DATE,
         XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CREDIT_MEMO_TAX_VAL (''TAXRATE'',rctlr.customer_trx_id,rctlr.customer_trx_line_id) tax_rate,
         ps.freight_original                freight_original,
         PS.FREIGHT_REMAINING               FREIGHT_REMAINING,
         --NVL(ps.amount_applied,0)           invoice_total,
		 nvl(rctgr.acctd_amount,rctgr.amount) invoice_total,
         loc.organization_code              location,
         PSO.AMOUNT_APPLIED                 O_AMOUNT_APPLIED,
         CASE
         WHEN PS.AMOUNT_APPLIED IS NOT NULL
         THEN PS.AMOUNT_APPLIED      *-1
         ELSE PSO.AMOUNT_DUE_ORIGINAL-PSO.AMOUNT_DUE_REMAINING
         END                                AMOUNT_PAID,
         RCTLR.LINE_NUMBER                  C_LINE_NUMBER,
         XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CREDIT_MEMO_TAX_VAL (''TAXAMT'',rctlr.customer_trx_id,rctlr.customer_trx_line_id) tax_amt,
         rctr.trx_number                    invoice_number,
         ohr.order_number                   order_number, 
         rcto.trx_number                    original_invoice_number,
         rcto.trx_date                      original_invoice_date,
         olr.ordered_item                   part_number,      
         olr.Return_Reason_Code             credit_reason_sale,
         rctlr.extended_amount              invoice_line_amount,
         NVL(rctlr.quantity_invoiced,rctlr.quantity_credited) qty_shipped,
         jsr.salesrep_number                sales_rep_no,
         ac.name                            customer_territory,
         CASE
         WHEN instr(sites.location,''-'') > 0
         THEN SUBSTR(sites.location,1,(instr(sites.location,''-'')-1))
         ELSE sites.location
         END                                customer_name,
         custb.account_name master_name,
         oho.order_number orginal_order_number,
         FU.USER_NAME TAKEN_BY,
         xxeis.eis_rs_xxwc_com_util_pkg.xxwc_cal_restock_per(olr.header_id) restock_fee_percent,
         (SELECT MAX(apply_date)
            FROM ar_receivable_applications
           WHERE application_type=''CM''
             AND customer_trx_id   =rctr.customer_trx_id
         ) origin_date,
         DECODE(oho.order_type_id,1004,TRUNC(olo.schedule_ship_date), TRUNC(olo.actual_shipment_date)) Ship_date,
         TRUNC(ohr.ordered_date) order_date,
         ott.name                line_type
  FROM oe_order_headers             oho,
       oe_order_lines               olo,
       oe_order_headers             ohr,
       oe_order_lines               olr,
       ra_customer_trx              rctr,
       ra_customer_trx_lines        rctlr,
       ra_cust_trx_line_gl_dist     rctgr,
       ra_customer_trx              rcto,
       ra_customer_trx_lines        rctlo,
       ra_cust_trx_line_gl_dist     rctgo,
       ar_payment_schedules         ps,
       ar_payment_schedules         pso,
       hz_cust_accounts             custs,
       hz_parties                   partys,
       hz_cust_site_uses            sites,
       hz_cust_acct_sites           cust_sites,
       hz_party_sites               party_sites,
       hz_locations                 locs,
       hz_cust_accounts             custb,
       hz_parties                   partyb,
       hz_cust_site_uses            siteb,
       hz_cust_acct_sites           cust_siteb,
       hz_party_sites               party_siteb,
       hz_locations                 locb,
       hz_customer_profiles         hzp,
       AR_COLLECTORS                AC,
       jtf_rs_salesreps             jsr,
       org_organization_definitions loc,
       fnd_user                     fu,
       RA_CUST_TRX_TYPES            ctt,
       gl_periods                   gp,
       oe_transaction_types_vl      ott,
       XXWC_OM_CASH_REFUND_TBL      xocr
  WHERE ohr.header_id                   = olr.header_id
    --AND ohr.order_number                = 10000924
    AND olr.reference_header_id         = olo.header_id
    AND olr.line_type_id                = ott.transaction_type_id
    AND olr.reference_line_id           = olo.line_id
    AND oho.header_id                   = olo.header_id
    AND ohr.header_id                   = xocr.return_header_id(+)
    AND rctr.CUST_TRX_TYPE_ID           = CTT.CUST_TRX_TYPE_ID
    and rctr.org_id                     = ctt.org_id 
    AND ctt.name                        = ''Credit Memo''
    AND rctr.interface_header_context   = ''ORDER ENTRY''
    AND TO_CHAR(ohr.order_number)       = rctr.interface_header_attribute1
    AND TO_CHAR(olr.line_id)            = rctlr.interface_line_attribute6
    AND rctr.customer_trx_id            = rctlr.customer_trx_id
    AND rcto.INTERFACE_header_CONTEXT   = ''ORDER ENTRY''
    AND TO_CHAR(oho.order_number)       = rcto.interface_header_attribute1
    AND TO_CHAR(olo.line_id)            = rctlo.interface_line_attribute6
    AND rcto.customer_trx_id            = rctlo.customer_trx_id
    AND rctgo.customer_trx_id           = rcto.customer_trx_id
    AND ''REC''                           = rctgo.account_class
    AND ''Y''                             = rctgo.latest_rec_flag
    AND rctgr.customer_trx_id           = rctr.customer_trx_id
    AND ''REC''                           = rctgr.account_class
    AND ''Y''                             = rctgr.latest_rec_flag    
    AND ps.customer_trx_id              = rctr.customer_trx_id
    AND pso.customer_trx_id             = rcto.customer_trx_id
    AND rctr.ship_to_customer_id        = custs.cust_account_id
    AND custs.party_id                  = partys.party_id
    AND RCTR.SHIP_TO_SITE_USE_ID        = SITES.SITE_USE_ID(+)
    AND CUST_SITES.CUST_ACCT_SITE_ID(+) = SITES.CUST_ACCT_SITE_ID    
    AND cust_sites.party_site_id        = party_sites.party_site_id(+)
    AND party_sites.location_id         = locs.location_id(+)
    AND rctr.bill_to_customer_id        = custb.cust_account_id
    AND custb.party_id                  = partyb.party_id
    AND rctr.bill_to_site_use_id        = siteb.site_use_id
    AND CUST_SITEB.CUST_ACCT_SITE_ID    = SITEB.CUST_ACCT_SITE_ID
    AND cust_siteb.party_site_id        = party_siteb.party_site_id
    AND party_siteb.location_id         = locb.location_id
    AND hzp.cust_account_id             = custb.cust_account_id
    AND hzp.site_use_id                 = siteb.site_use_id
    AND AC.COLLECTOR_ID                 = HZP.COLLECTOR_ID(+)
    AND jsr.salesrep_id                 = rctr.primary_salesrep_id
    AND jsr.org_id                      = rctr.org_id
    AND loc.organization_id             = olr.ship_from_org_id
    AND fu.user_id                      = ohr.created_by
    AND gp.period_set_name              = ''4-4-QTR''
    AND rctgr.gl_date between gp.start_date and gp.end_date
    and rctr.customer_trx_id            = :1
		  ';
      
   l_stmt_hdr_qry2:='SELECT distinct '||P_Process_id||' process_id,
		gp.period_year       FISCAL_YEAR,
         gp.period_name      FISCAL_MONTH,
    RCTR.TRX_DATE trx_date,
    PARTY_SITES.PARTY_SITE_NUMBER CUSTOMER_NUMBER,
    CASE
      WHEN PS.AMOUNT_DUE_REMAINING >0
      THEN PARTY_SITEB.PARTY_SITE_NUMBER
      ELSE TO_CHAR(0)
    END AR_CUSTOMER_NUMBER,
    sites.location ship_name,
    ''PRISM RETURN'' invoice_type,
    -- NVL(OP.PAYMENT_TYPE_CODE,''ON ACCOUNT'') INVOICE_TYPE,
    TRUNC(ohr.creation_date) creation_date,
    TRUNC(rctr.trx_date) invoice_date, --C_INVOICE_DATE,
    TRUNC(RCTGr.GL_DATE) BUSINESS_DATE,--CREDIT_GL_DATE,
    -- zl.tax_rate tax_rate,
    -- null tax_rate,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CREDIT_MEMO_TAX_VAL (''TAXRATE'',rctlr.customer_trx_id,rctlr.customer_trx_line_id) tax_rate,
    ps.freight_original freight_original,  --C_FREIGHT_ORIGINAL,
    PS.FREIGHT_REMAINING FREIGHT_REMAINING,--C_FREIGHT_REMAINING,
	nvl(rctgr.acctd_amount,rctgr.amount) invoice_total,
    --NVL(ps.amount_applied,0) invoice_total,
	--C_AMOUNT_APPLIED,
    -- CALC_ORDERED_AMOUNT(OHR.HEADER_ID)INVOICE_TOTAL,--C_AMOUNT_APPLIED,
    /*   (SELECT DISTINCT organization_code
    FROM org_organization_definitions
    WHERE organization_id=olr.ship_from_org_id
    )*/
    loc.organization_code location,
    NULL O_AMOUNT_APPLIED,
    -- pso.amount_applied - xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining ( pso.payment_schedule_id, pso.due_date, null, null, (pso.amount_due_original)*nvl(pso.exchange_rate,1), pso.amount_applied, pso.amount_credited, pso.amount_adjusted, pso.class ) amount_paid,
    --PSO.AMOUNT_DUE_ORIGINAL+PS.AMOUNT_DUE_REMAINING amount_paid,
    CASE
      WHEN PS.AMOUNT_APPLIED IS NOT NULL
      THEN PS.AMOUNT_APPLIED     *-1
      ELSE PS.AMOUNT_DUE_ORIGINAL-PS.AMOUNT_DUE_REMAINING
    END AMOUNT_PAID,
    RCTLR.LINE_NUMBER C_LINE_NUMBER,
    -- zl.tax_amt tax_amt,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CREDIT_MEMO_TAX_VAL (''TAXAMT'',rctlr.customer_trx_id,rctlr.customer_trx_line_id) tax_amt,
    rctr.trx_number invoice_number,--  C_INVOICE_NUMBER,
    ohr.order_number order_number, -- RETURN_ORDER_NUMBER,
    NULL original_invoice_number,
    -- rcto.trx_number original_invoice_number,
    --rcto.trx_date original_invoice_date,
    NULL original_invoice_number,
    olr.ordered_item part_number,              -- C_ORDERED_ITEM,
    olr.Return_Reason_Code credit_reason_sale, --C_REASON_CODE,
    --olo.return_reason_code credit_reason_sale, --C_REASON_CODE,
    rctlr.extended_amount invoice_line_amount,                        --C_INVOICE_LINE_AMOUNT,
    NVL(rctlr.quantity_invoiced,rctlr.quantity_credited) qty_shipped, --C_QUNATITY_INVOICED,
    jsr.salesrep_number sales_rep_no,
    ac.name customer_territory,
    CASE
      WHEN instr(sites.location,''-'') > 0
      THEN SUBSTR(sites.location,1,(instr(sites.location,''-'')-1))
      ELSE sites.location
    END customer_name,
    custb.account_name master_name,
    NULL orginal_order_number,
    --oho.order_number orginal_order_number,
    FU.USER_NAME TAKEN_BY,
    xxeis.eis_rs_xxwc_com_util_pkg.xxwc_cal_restock_per(olr.header_id) restock_fee_percent,
    (SELECT MAX(apply_date)
    FROM ar_receivable_applications
    WHERE application_type=''CM''
    AND customer_trx_id   =rctr.customer_trx_id
    ) origin_date,
    NULL Ship_date,
    TRUNC(ohr.ordered_date) order_date,
    ott.name                line_type
  FROM
    oe_order_headers ohr,
    oe_order_lines olr,
    ra_customer_trx rctr,
    ra_customer_trx_lines rctlr,
    ra_cust_trx_line_gl_dist rctgr,
    ar_payment_schedules ps,
    hz_cust_accounts custs,
    hz_parties partys,
    hz_cust_site_uses sites,
    hz_cust_acct_sites cust_sites,
    hz_party_sites party_sites,
    hz_locations locs,
    hz_cust_accounts custb,
    hz_parties partyb,
    hz_cust_site_uses siteb,
    hz_cust_acct_sites cust_siteb,
    hz_party_sites party_siteb,
    hz_locations locb,
    hz_customer_profiles hzp,
    AR_COLLECTORS AC,
    jtf_rs_salesreps jsr,
    org_organization_definitions loc,
    fnd_user fu,
       RA_CUST_TRX_TYPES            ctt,
       gl_periods                   gp,
       oe_transaction_types_vl      ott  
  WHERE ohr.header_id                   = olr.header_id
    AND olr.reference_header_id         IS NULL
    AND olr.line_type_id                = ott.transaction_type_id
    AND rctr.interface_header_context   = ''ORDER ENTRY''    
    AND TO_CHAR(ohr.order_number)       = rctr.interface_header_attribute1
    AND TO_CHAR(olr.line_id)            = rctlr.interface_line_attribute6
    AND rctr.CUST_TRX_TYPE_ID           = CTT.CUST_TRX_TYPE_ID
    and rctr.org_id                     = ctt.org_id 
    AND ctt.name                        = ''Credit Memo'' 
    AND rctgr.customer_trx_id           = rctr.customer_trx_id
    AND ''REC''                           = rctgr.account_class
    AND ''Y''                             = rctgr.latest_rec_flag
    AND ps.customer_trx_id              = rctr.customer_trx_id
    AND rctr.ship_to_customer_id        = custs.cust_account_id
    AND custs.party_id                  = partys.party_id
    AND RCTR.SHIP_TO_SITE_USE_ID        = SITES.SITE_USE_ID(+)
    AND CUST_SITES.CUST_ACCT_SITE_ID(+) = SITES.CUST_ACCT_SITE_ID
    AND cust_sites.party_site_id        = party_sites.party_site_id(+)
    AND party_sites.location_id         = locs.location_id(+)
    AND rctr.bill_to_customer_id        = custb.cust_account_id
    AND custb.party_id                  = partyb.party_id
    AND rctr.bill_to_site_use_id        = siteb.site_use_id
    AND CUST_SITEB.CUST_ACCT_SITE_ID    = SITEB.CUST_ACCT_SITE_ID
    AND cust_siteb.party_site_id        = party_siteb.party_site_id
    AND party_siteb.location_id         = locb.location_id
    AND hzp.cust_account_id             = custb.cust_account_id
    AND hzp.site_use_id                 = siteb.site_use_id
    AND AC.COLLECTOR_ID                 = HZP.COLLECTOR_ID(+)
    AND jsr.salesrep_id                 = rctr.primary_salesrep_id
    AND jsr.org_id                      = rctr.org_id
    AND loc.organization_id             = olr.ship_from_org_id
    AND fu.user_id                      = ohr.created_by
    AND ohr.order_type_id NOT IN (1013, 1014)
    AND gp.period_set_name              =''4-4-QTR''
    and rctgr.gl_date between gp.start_date and gp.end_date
    and rctr.customer_trx_id =:1
    AND NOT EXISTS
    (select 1 from xxwc_om_cash_refund_tbl where return_header_id =ohr.header_id
    )';
   
   
      l_stmt_hdr_qry1:= l_stmt_hdr_qry1||' '||LC_WHERE_COND;  
      
      l_stmt_hdr_qry2:= l_stmt_hdr_qry2||' '||LC_WHERE_COND; 
      
      fnd_file.put_line(fnd_file.log,'l_stmt_hdr_qry1'||l_stmt_hdr_qry1);
       fnd_file.put_line(fnd_file.log,'l_stmt_hdr_qry2'||l_stmt_hdr_qry2);
--Dbms_Output.Put_Line('l_stmt_hdr_qry1'||l_stmt_hdr_qry1);
--Dbms_Output.Put_Line('l_stmt_hdr_qry2'||l_stmt_hdr_qry2);

  OPEN L_REF_CURSOR1 FOR l_stmt;
    FETCH L_REF_CURSOR1 BULK COLLECT INTO TRX_TAB;
  CLOSE L_REF_CURSOR1;

 IF TRX_TAB.COUNT>0
  THEN
  
  FOR I IN 1..TRX_TAB.COUNT
  LOOP
     OPEN L_REF_CURSOR2 FOR l_stmt_hdr_qry1 USING 
  TRX_TAB(i).CUSTOMER_TRX_ID;
  loop
    FETCH L_REF_CURSOR2 BULK COLLECT INTO l_credit_memo_det_tbl limit 10000;
   
  if l_credit_memo_det_tbl.COUNT>0
  Then  
       FORALL J IN 1..l_credit_memo_det_tbl.COUNT 
  Insert Into xxeis.EIS_XXWC_CREDIT_MEMO_DET_TAB Values l_credit_memo_det_tbl(J);
 
   END IF;
    exit when L_REF_CURSOR2%notfound;
    END LOOP;
       --commit;    --Commented for version 1.1 
    CLOSE L_REF_CURSOR2;
 END LOOP; 
 END IF;
 
l_credit_memo_det_tbl.DELETE;
     
 IF TRX_TAB.COUNT>0
  THEN
   FOR x IN 1..TRX_TAB.COUNT
  LOOP   
   OPEN L_REF_CURSOR3 FOR l_stmt_hdr_qry2 USING 
  TRX_TAB(x).CUSTOMER_TRX_ID;
  loop
    FETCH L_REF_CURSOR3 BULK COLLECT INTO l_credit_memo_det_tbl limit 10000;
   
  if l_credit_memo_det_tbl.COUNT>0
  Then  
       FORALL K IN 1..l_credit_memo_det_tbl.COUNT 
  Insert Into xxeis.EIS_XXWC_CREDIT_MEMO_DET_TAB Values l_credit_memo_det_tbl(K);
 
   END IF;
    exit when L_REF_CURSOR3%notfound;
    END LOOP;
       --COMMIT;  --Commented for version 1.1   
    CLOSE L_REF_CURSOR3;   
 END LOOP; 
 END IF;

exception when others then
Fnd_File.Put_Line(FND_FILE.log,'The ERROR '||sqlcode||sqlerrm);
END;
         
/* --Commented code for version 1.1     
PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER)
AS
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  	04/12/2016   Initial Build --TMS#20151216-00168  
--//============================================================================    
  BEGIN  
  DELETE FROM XXEIS.EIS_XXWC_CREDIT_MEMO_DET_TAB WHERE PROCESS_ID=P_PROCESS_ID;
  COMMIT;
END ;
*/ --Commented code for version 1.1

 end  EIS_XXWC_CREDIT_MEMO_DET_PKG;
/
 