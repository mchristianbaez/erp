--Report Name            : Cash Drawer Exception Report
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_XXWC_CASH_DRAWN_EXCE_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_CASH_DRAWN_EXCE_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_CASH_DRAWN_EXCE_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Cash Drwr Recon V','EXACDRV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_CASH_DRAWN_EXCE_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_CASH_DRAWN_EXCE_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_CASH_DRAWN_EXCE_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CASH_DATE',660,'Cash Date','CASH_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Cash Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','BRANCH_NUMBER',660,'Branch Number','BRANCH_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','INVOICE_DATE',660,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','INVOICE_NUMBER',660,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CASH_AMOUNT',660,'Cash Amount','CASH_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Cash Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CARD_ISSUER_NAME',660,'Card Issuer Name','CARD_ISSUER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Issuer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CASH_TYPE',660,'Cash Type','CASH_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cash Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','PAYMENT_TYPE_CODE',660,'Payment Type Code','PAYMENT_TYPE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','TAKEN_BY',660,'Taken By','TAKEN_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Taken By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','SEGMENT_NUMBER',660,'Segment Number','SEGMENT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CHK_CARDNO',660,'Chk Cardno','CHK_CARDNO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Chk Cardno','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','ORDER_AMOUNT',660,'Order Amount','ORDER_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Order Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','NAME',660,'Name','NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','ORDER_DATE',660,'Order Date','ORDER_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Order Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','DIFF',660,'Diff','DIFF','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Diff','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','ON_ACCOUNT',660,'On Account','ON_ACCOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','On Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','DIST_DATE',660,'Dist Date','DIST_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Dist Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','PAYMENT_CHANNEL_NAME',660,'Payment Channel Name','PAYMENT_CHANNEL_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Channel Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CHECK_NUMBER',660,'Check Number','CHECK_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Check Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CARD_ISSUER_CODE',660,'Card Issuer Code','CARD_ISSUER_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Issuer Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CARD_NUMBER',660,'Card Number','CARD_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','ORG_ID',660,'Org Id','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','PAYMENT_TYPE_CODE_NEW',660,'Payment Type Code New','PAYMENT_TYPE_CODE_NEW','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type Code New','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','PROCESS_ID',660,'Process Id','PROCESS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Process Id','','','','US');
--Inserting Object Components for EIS_XXWC_CASH_DRAWN_EXCE_V
--Inserting Object Component Joins for EIS_XXWC_CASH_DRAWN_EXCE_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Cash Drawer Exception Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Cash Drawer Exception Report
xxeis.eis_rsc_ins.lov( 660,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Cash Drawer Exception Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Cash Drawer Exception Report
xxeis.eis_rsc_utility.delete_report_rows( 'Cash Drawer Exception Report' );
--Inserting Report - Cash Drawer Exception Report
xxeis.eis_rsc_ins.r( 660,'Cash Drawer Exception Report','','This report provides a total listing of all Cash, Check and Credit Card Sales by Branch (Customer, Taken By, Order Number, Cash Date, Cash Amount, Cash Type, Invoice Number, Invoice Date, Payment Type, Check/Credit Card No.)','','','','XXEIS_RS_ADMIN','EIS_XXWC_CASH_DRAWN_EXCE_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Cash Drawer Exception Report
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'BRANCH_NUMBER','Branch Number','Branch Number','','','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'CASH_AMOUNT','Cash Amount','Cash Amount','','~T~D~2','default','','9','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'CASH_DATE','Cash Date','Cash Date','','','default','','7','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'CASH_TYPE','Cash Type','Cash Type','','','default','','17','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'CHK_CARDNO','Check Card No','Chk Cardno','','','default','','14','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','18','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'NAME','Receipt Method','Name','','','default','','16','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'ORDER_AMOUNT','Order Amount','Order Amount','','~T~D~2','default','','8','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'PAYMENT_TYPE_CODE','Payment Type','Payment Type Code','','','default','','13','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'SEGMENT_NUMBER','Segment Number','Segment Number','','','default','','20','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'TAKEN_BY','Created By','Taken By','','','default','','5','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'CARD_ISSUER_NAME','Card Type','Card Issuer Name','','','default','','15','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'DIFF','Diff','Diff','','~T~D~2','default','','10','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'ORDER_DATE','Order Date','Order Date','','','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','19','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'ON_ACCOUNT','On Account','On Account','','~T~D~2','default','','11','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Exception Report',660,'DIST_DATE','Discrepancy Date','Dist Date','','','default','','12','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','','US','');
--Inserting Report Parameters - Cash Drawer Exception Report
xxeis.eis_rsc_ins.rp( 'Cash Drawer Exception Report',660,'Location','Branch Number','BRANCH_NUMBER','IN','AR Organizaion Code LOV','','VARCHAR2','Y','Y','1','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cash Drawer Exception Report',660,'As of Date','As of Date','CASH_DATE','>=','','','DATE','Y','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','','EIS_XXWC_CASH_DRAWN_EXCE_V','','','US','');
--Inserting Dependent Parameters - Cash Drawer Exception Report
--Inserting Report Conditions - Cash Drawer Exception Report
xxeis.eis_rsc_ins.rcnh( 'Cash Drawer Exception Report',660,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','and PROCESS_ID= :SYSTEM.PROCESS_ID','1',660,'Cash Drawer Exception Report','Free Text ');
--Inserting Report Sorts - Cash Drawer Exception Report
--Inserting Report Triggers - Cash Drawer Exception Report
xxeis.eis_rsc_ins.rt( 'Cash Drawer Exception Report',660,'begin
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.G_CASH_TO_DATE:=:As of Date;
XXEIS.eis_xxwc_cash_exception_pkg.process_cash_exception_orders(P_PROCESS_ID=> :SYSTEM.PROCESS_ID,
p_location => :Location,
p_as_of_date => :As of Date );
End;','B','Y','XXEIS_RS_ADMIN','AQ');
--inserting report templates - Cash Drawer Exception Report
xxeis.eis_rsc_ins.r_tem( 'Cash Drawer Exception Report','Cash Drawer Reconciliation','Cash Drawer Reconciliation','','','','','','','','','','','Cash Drawer Exception Report.rtf','XXEIS_RS_ADMIN','X','','','Y','Y','','');
--Inserting Report Portals - Cash Drawer Exception Report
--inserting report dashboards - Cash Drawer Exception Report
xxeis.eis_rsc_ins.R_dash( 'Cash Drawer Exception Report','Dynamic 757','Dynamic 757','scatter','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.R_dash( 'Cash Drawer Exception Report','Dynamic 759','Dynamic 759','horizontal stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.R_dash( 'Cash Drawer Exception Report','Dynamic 756','Dynamic 756','stacked line','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.R_dash( 'Cash Drawer Exception Report','Dynamic 758','Dynamic 758','vertical stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.R_dash( 'Cash Drawer Exception Report','Dynamic 754','Dynamic 754','absolute line','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.R_dash( 'Cash Drawer Exception Report','Dynamic 751','Dynamic 751','pie','large','Cash Amount','Cash Amount','Customer Name','Customer Name','Count','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.R_dash( 'Cash Drawer Exception Report','Dynamic 752','Dynamic 752','pie','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.R_dash( 'Cash Drawer Exception Report','Dynamic 753','Dynamic 753','vertical stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.R_dash( 'Cash Drawer Exception Report','Dynamic 755','Dynamic 755','point','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Cash Drawer Exception Report','660','EIS_XXWC_CASH_DRAWN_EXCE_V','EIS_XXWC_CASH_DRAWN_EXCE_V','N','');
--inserting report security - Cash Drawer Exception Report
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','','LB048272','',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','101','','GENERAL_LEDGER_SUPER_USER',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','101','','GENERAL_LEDGER_USER',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','660','','',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','660','','',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','660','','',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','660','','',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','660','','',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','660','','',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','101','','XXCUS_GL_ACCOUNTANT_CAD',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','101','','GNRL_LDGR_LTMR_ACCNTNT',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','101','','XXCUS_GL_ACCOUNTANT_USD',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','101','','XXCUS_GL_INQUIRY',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','101','','HDS GL INQUIRY',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','660','','',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','260','','HDS+_CMNGNT_WC',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','260','','HDS_CSH_MNGMNET_SPR_WC',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','401','','',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','660','','',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','401','','',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','101','','GNRL_LDGR_LTMR_NQR',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','101','','XXCUS_GL_MANAGER',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','101','','HDS_CAD_MNTH_END_PROCS',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','101','','GNRL_LDGR_FSS',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','101','','GNRL_LDGR_LTMR_FSS',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','701','','CLN_OM_3A6_ADMINISTRATOR',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','660','','ORDER_MGMT_SUPER_USER',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','20005','','',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','','TW008334','',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','660','','XXWC_ORDER_MGMT_SUPER_USER',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Exception Report','','KG013546','',660,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - Cash Drawer Exception Report
xxeis.eis_rsc_ins.rpivot( 'Cash Drawer Exception Report',660,'By Payment Type','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - By Payment Type
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','CASH_TYPE','PAGE_FIELD','','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','CUSTOMER_NUMBER','PAGE_FIELD','','','2','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','NAME','PAGE_FIELD','','','3','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','CUSTOMER_NAME','PAGE_FIELD','','','4','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','CASH_AMOUNT','DATA_FIELD','SUM','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','BRANCH_NUMBER','ROW_FIELD','','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','PAYMENT_TYPE_CODE','ROW_FIELD','','','2','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','CARD_ISSUER_NAME','ROW_FIELD','','','3','','');
--Inserting Report Summary Calculation Columns For Pivot- By Payment Type
--Inserting Report   Version details- Cash Drawer Exception Report
xxeis.eis_rsc_ins.rv( 'Cash Drawer Exception Report','','Cash Drawer Exception Report','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
