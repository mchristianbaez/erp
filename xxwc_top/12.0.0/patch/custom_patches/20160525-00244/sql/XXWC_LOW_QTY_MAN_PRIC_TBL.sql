--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: XXWC_LOW_QTY_MAN_PRIC_TBL
  Description: This table is used to load data from XXEIS.EIS_RS_XXWC_LOW_QTY_PRI_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     12-Apr-2016        Siva       --TMS#20150928-00191   Performance Tuning
  1.1     31-May-2016        Siva	    --TMS#20160525-00244  
********************************************************************************/
DROP TABLE XXEIS.XXWC_LOW_QTY_MAN_PRIC_TBL CASCADE CONSTRAINTS;

CREATE TABLE XXEIS.XXWC_LOW_QTY_MAN_PRIC_TBL 
   (PROCESS_ID NUMBER, 
	BRANCH_NAME VARCHAR2(240 BYTE), 
	BRANCH VARCHAR2(3 BYTE), 
	ACCOUNT_MANAGER VARCHAR2(360 BYTE), 
	CREATED_BY VARCHAR2(4000 BYTE), 
	PRICING_RESPONSIBILITY VARCHAR2(240 BYTE), 
	HOLD_NAME VARCHAR2(240 BYTE), 
	APPROVED_BY VARCHAR2(240 BYTE), 
	REL_RES_CODE VARCHAR2(240 BYTE), 
	REL_RES_COMM VARCHAR2(240 BYTE), 
	CUSTOMER_NAME VARCHAR2(360 BYTE), 
	CUSTOMER_NUMBER VARCHAR2(30 BYTE), 
	ORDER_NUMBER VARCHAR2(40 BYTE), 
	ORDERED_DATE DATE, 
	INVOICE_NUMBER VARCHAR2(150 BYTE), 
	INVOICE_DATE DATE, 
	ORDER_TYPE VARCHAR2(150 BYTE), 
	SHIP_METHOD VARCHAR2(240 BYTE), 
	VENDOR_NAME VARCHAR2(4000 BYTE), 
	VENDOR_NUMBER VARCHAR2(4000 BYTE), 
	ITEM_NUMBER VARCHAR2(240 BYTE), 
	ITEM_DESCRIPTION VARCHAR2(240 BYTE), 
	SALES_QTY NUMBER, 
	BASE_PRICE NUMBER, 
	ORIGINAL_UNIT_SELLING_PRICE NUMBER, 
	ORGINAL_EXTEND_PRICE NUMBER, 
	DOLLAR_LOST NUMBER, 
	MODIFER_NUMBER VARCHAR2(4000 BYTE), 
	FINAL_SELLING_PRICE NUMBER, 
	FINAL_EXTENDED_PRICE NUMBER, 
	FINAL_GM NUMBER, 
	ORGINIAL_GM NUMBER, 
	DISTRICT VARCHAR2(150 BYTE), 
	REGION VARCHAR2(150 BYTE), 
	INVENTORY_ITEM_ID NUMBER, 
	SHIP_FROM_ORG_ID NUMBER, 
	ORDERED_QUANTITY NUMBER, 
	QTY_TYPE VARCHAR2(4 BYTE), 
	UNIT_COST NUMBER, 
	LHQTY NUMBER,
	PRICE_SOURCE_TYPE	VARCHAR2(1000 BYTE) --added for version 1.1
);

comment on table XXEIS.XXWC_LOW_QTY_MAN_PRIC_TBL is 'This table is used to get data from XXEIS.EIS_RS_XXWC_LOW_QTY_PRI_PKG Package.
All rows for each process are deleted after each run. This table should normally have 0 rows.'
/
