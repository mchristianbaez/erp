CREATE OR REPLACE PACKAGE    XXEIS.EIS_RS_XXWC_LOW_QTY_PRI_PKG
   AUTHID CURRENT_USER
   IS
--//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "Low Qty Manual Price Change Report"
--//
--// Object Name         		:: XXEIS.EIS_RS_XXWC_LOW_QTY_PRI_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  			4/12/2016   Initial Build --TMS#20150928-00191  --Performance Tuning
--// 1.1		Siva			8/05/2016	20160525-00244 
--//============================================================================

TYPE T_CASHTYPE_VLDN_TBL IS TABLE OF VARCHAR2(100) INDEX BY VARCHAR2(100);
G_EMPOLYEE_NAME_VLDN_TBL       T_CASHTYPE_VLDN_TBL;
G_ADJ_AUTO_MOD_AMT_VLDN_TBL    T_CASHTYPE_VLDN_TBL;
G_ADJ_AUTO_MOD_NAME_VLDN_TBL   T_CASHTYPE_VLDN_TBL;
G_LHQTY_VLDN_TBL               T_CASHTYPE_VLDN_TBL;
g_price_source_type_VLDN_TBL   T_CASHTYPE_VLDN_TBL;  --added for version 1.1

G_EMPOLYEE_NAME  varchar2(200);
G_ADJ_AUTO_MODIFIER_AMT  number;
G_ADJ_AUTO_MODIFIER_NAME VARCHAR2(200);
G_LHQTY  varchar2(200);
g_price_source_type varchar2(1000); --added for version 1.1

function GET_EMPOLYEE_NAME (P_EMPLOYEE_ID in number,P_DATE in date)  RETURN VARCHAR2;
function GET_ADJ_AUTO_MODIFIER_AMT (P_HEADER_ID number, P_LINE_ID number)      return number;
function GET_ADJ_AUTO_MODIFIER_NAME (P_HEADER_ID number, P_LINE_ID number)      return varchar2;
FUNCTION GET_LHQTY(P_INVENTORY_ITEM_ID NUMBER) RETURN VARCHAR2;
function GET_price_source_type (P_HEADER_ID number, P_LINE_ID number)      return varchar2; 
--//============================================================================
--//
--// Object Name           :: GET_price_source_type  
--//
--// Object Usage 		   :: This Object Referred by "Low Qty Manual Price Change Report"
--//
--// Object Type           :: Function
--//
--// Object Description   ::  This  Function is created to get the price_source_type column.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.1		Siva			8/05/2016	20160525-00244 
--//============================================================================      
PROCEDURE LOW_QTY_PRI_PAR (P_PROCESS_ID NUMBER
                          , P_BRANCH      IN VARCHAR2
                          ,P_ORDER_DATE_FROM   IN DATE
                          ,p_order_date_to   IN date
                          ,p_invoice_date_from   IN date
                          ,p_invoice_date_to   in date   
                          ,p_Account_Manager   IN VARCHAR2
                          ,p_Region  				IN VARCHAR2
                          ,p_District   		IN VARCHAR2
                          ,p_Item_num   		IN VARCHAR2
                          ,P_ITEM_DESCRIPTION  	IN VARCHAR2
                          ,p_Customer_Number   	IN VARCHAR2
                          ,P_CUSTOMER_NAME  		IN VARCHAR2
                          );
--//============================================================================
--//
--// Object Name         :: LOW_QTY_PRI_PAR  
--//
--// Object Usage 		 :: This Object Referred by "Low Qty Manual Price Change Report"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  	04/12/2016   Initial Build --TMS#20150928-00191 --Performance Tuning
--//============================================================================
						  
procedure clear_temp_tables ( p_process_id in number);  
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  	04/12/2016   Initial Build --TMS#20150928-00191  --Performance Tuning
--//============================================================================                            
END eis_rs_xxwc_low_qty_pri_pkg;
/
