create or replace PACKAGE           XXWC_LABEL_PRINTERS_PKG AS

   /*****************************************************************************************************************************************
   *   $Header XXWC_LABEL_PRINTERS_PKG $                                                                                                    *
   *   Module Name: XXWC_LABEL_PRINTERS_PKG                                                                                                 *
   *                                                                                                                                        *
   *   PURPOSE:   This package is used by the extensions in MSCA for Label Printing                                                         *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *   1.1        11-FEB-2016  Lee Spitzer               TMS#20151103-00188 - RF Enhancements                                               *
   *****************************************************************************************************************************************/

  g_package                   VARCHAR2(30) := 'XXWC_LABEL_PRINTERS_PKG';
  g_call_from                 VARCHAR2(175);
  g_call_point                VARCHAR2(175);
  g_distro_list               VARCHAR2 (80) := 'hdsoracledevelopers@hdsupply.com';
  g_module                    VARCHAR2 (80) := 'inv';
  g_sequence                  NUMBER;
  g_debug                     VARCHAR2(1) := nvl(FND_PROFILE.VALUE('AFLOG_ENABLED'),'N'); --Fnd Debug Log = Yes / No
  g_log_level                 NUMBER := nvl(FND_PROFILE.VALUE('AFLOG_LEVEL'),6); --Fnd Debug Log Level
  g_sqlerrm                   VARCHAR2(1000);
  g_sqlcode                   NUMBER;   
  g_message                   VARCHAR2(2000);
  g_exception                 EXCEPTION; 
  g_language                  VARCHAR2(10) := userenv('LANG');
  TYPE t_genref IS REF CURSOR;
  
                               
   /*****************************************************************************************************************************************
   *   PROCEDURE GET_PRINTERS_LOV                                                                                                           *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to get the printers lov for receiving mobile forms                                                 *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/
  
    PROCEDURE GET_PRINTERS_LOV ( x_printer      OUT NOCOPY t_genref
                               , p_printer_name IN VARCHAR2);
    

   /*****************************************************************************************************************************************
   *   PROCEDURE set_printers                                                                                                               *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to set the printers lov for receiving mobile forms                                                 *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/

    PROCEDURE set_printers ( p_printer_name IN VARCHAR2
                           , p_profile_option_name IN VARCHAR2
                           , x_return OUT NUMBER
                           , x_message OUT VARCHAR2);
    
   /*****************************************************************************************************************************************
   *   PROCEDURE get_default_printers                                                                                                       *
   *                                                                                                                                        *
   *   PURPOSE:   This function is used to get the default printer profile option                                                           *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/

    FUNCTION get_default_printer ( p_profile_option_name IN VARCHAR2)
      RETURN VARCHAR2;
      
      
  
   /*****************************************************************************************************************************************
   *   PROCEDURE validate_lbl_qty_val                                                                                                       *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to validate the receiving qty value                                                                *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/
  
    PROCEDURE validate_lbl_qty_val  ( p_qty     IN VARCHAR2
                                    , x_return  OUT NUMBER
                                    , x_message OUT VARCHAR2
                                    );



     /*****************************************************************************************************************************************
     *  PROCEDURE GET_UBD_STATUS_LOV                                                                                                            *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for list of values for time sensitive label list of valus on the mobile device, C or H                *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : x_flexvalus                                                                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
     *                                                     RF - Receiving                                                                     *
     *****************************************************************************************************************************************/


    PROCEDURE GET_UBD_STATUS_LOV ( x_flexvalues             OUT    NOCOPY t_genref --0
                                 , p_flex_value             IN     VARCHAR2
                                 );
                               
                               
    
    
     /*****************************************************************************************************************************************
     *  PROCEDURE PROCESS_UBD_CCR                                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used to submit the XXWC UBD Label Report from a RF Device                                                  *
     *                                                                                                                                        *
     *    Parameters: p_date    IN VARCHAR2 --Use by date                                                                                     *                                        *
     *                p_label_copies    IN VARCHAR2 --Number of Copies                                                                        *
     *                p_status_flag     IN VARCHAR2 --F or S -- parameters to determine label size                                            *
     *    Out : x_return -- 0 = Success, 1 = Warning, 2 = Error                                                                               *
     *          x_message --Message out                                                                                                       *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        16-FEB-201  Lee Spitzer               TMS Ticket 20150302-00152                                                           *
     *                                                     RF - Receiving                                                                     *
     *****************************************************************************************************************************************/

     PROCEDURE PROCESS_UBD_CCR
                                                 ( p_date               IN  VARCHAR2
                                                 , p_label_copies       IN  VARCHAR2
                                                 , p_status_flag        IN VARCHAR2
                                                 , x_return             OUT NUMBER
                                                 , x_message            OUT VARCHAR2
                                                 );



     /*****************************************************************************************************************************************
     *  PROCEDURE GET_ITEM_LBL_STATUS_LOV                                                                                                     *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used for list of values for time sensitive label list of valus on the mobile device, C or H                *
     *                                                                                                                                        *
     *    Parameters: None                                                                                                                    *
     *    Return : x_flexvalus                                                                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
     *                                                     RF - Receiving                                                                     *
     *****************************************************************************************************************************************/


    PROCEDURE GET_ITEM_LBL_STATUS_LOV ( x_flexvalues             OUT    NOCOPY t_genref --0
                                      , p_flex_value             IN     VARCHAR2
                                      );
                               

   /*****************************************************************************************************************************************
   *   PROCEDURE GET_YES_NO_LOV                                                                                                             *
   *                                                                                                                                        *
   *   PURPOSE:  This procedure is used to get the Yes or No List of Values                                                                 *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/
  
    PROCEDURE GET_YES_NO_LOV  ( x_default                   OUT    NOCOPY t_genref --0
                              ,  p_description               IN      VARCHAR2
                              );



     /*****************************************************************************************************************************************
     *  PROCEDURE PROCESS_ITEM_LABEL_CCR                                                                                                      *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used to submit the XXWC UBD Label Report from a RF Device                                                  *
     *                                                                                                                                        *
     *    Parameters: p_date    IN VARCHAR2 --Use by date                                                                                     *                                        *
     *                p_label_copies    IN VARCHAR2 --Number of Copies                                                                        *
     *                p_status_flag     IN VARCHAR2 --F or S -- parameters to determine label size                                            *
     *    Out : x_return -- 0 = Success, 1 = Warning, 2 = Error                                                                               *
     *          x_message --Message out                                                                                                       *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        16-FEB-2015 Lee Spitzer               TMS Ticket 20150302-00152                                                           *
     *                                                     RF - Receiving                                                                     *
     *****************************************************************************************************************************************/

     PROCEDURE PROCESS_ITEM_LABEL_CCR
                                                 ( p_organization_id               IN  VARCHAR2
                                                 , p_inventory_item_id             IN  VARCHAR2
                                                 , P_INV_ENABLE_BIN_LOC            IN  VARCHAR2
                                                 , P_BIN_LOC_SELECTED              IN  VARCHAR2
                                                 , P_ENABLE_BIN_LOC_RANGE          IN  VARCHAR2
                                                 , P_BIN_LOC_LOW                   IN  VARCHAR2
                                                 , P_BIN_LOC_HIGH                  IN  VARCHAR2
                                                 , P_PRICE                         IN  VARCHAR2
                                                 , P_FLAG                          IN  VARCHAR2
                                                 , P_PRINT_LOCATORS                IN  VARCHAR2
                                                 , P_SHELF_PRICING                 IN  VARCHAR2
                                                 , P_COPIES                        IN  VARCHAR2
                                                 , x_return                        OUT NUMBER
                                                 , x_message                       OUT VARCHAR2
                                                 );


     /*****************************************************************************************************************************************
     *  PROCEDURE VAL_UBD_DATE                                                                                                                *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure used to validate the UBD date format and return a message                                                  *
     *                                                                                                                                        *
     *    Parameters: p_date    IN VARCHAR2 --Use by date                                                                                     *                                        
     *                p_label_copies    IN VARCHAR2 --Number of Copies                                                                        *
     *                p_status_flag     IN VARCHAR2 --F or S -- parameters to determine label size                                            *
     *    Out : x_return -- 0 = Success, 1 = Warning, 2 = Error                                                                               *
     *          x_message --Message out                                                                                                       *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        16-FEB-2015 Lee Spitzer               TMS Ticket 20150302-00152                                                           *
     *                                                     RF - Receiving                                                                     *
     *****************************************************************************************************************************************/

     PROCEDURE VAL_UBD_DATE
                                                 ( p_date               IN  VARCHAR2
                                                 , x_return             OUT NUMBER
                                                 , x_message            OUT VARCHAR2
                                                 , x_date               OUT VARCHAR2
                                                 );
                                                 
    /*****************************************************************************************************************************************
    *   PROCEDURE GET_WC_BIN_LOC                                                                                                        *
    *                                                                                                                                        *
    *   PURPOSE:   This procedure is used to get the branch bin assignments for an item organization combination                             *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
    *                                                     RF - Receiving                                                                     *
    *****************************************************************************************************************************************/
  


           PROCEDURE GET_BIN_LOC              ( x_locators               OUT    NOCOPY t_genref --0
                                              , p_organization_id        IN     NUMBER    --1
                                              , p_subinventory_code      IN     VARCHAR2  --2
                                              , p_restrict_locators_code IN     NUMBER    --3
                                              , p_inventory_item_id      IN     VARCHAR2    --4
                                              , p_concatenated_segments  IN     VARCHAR2  --5
                                              , p_project_id             IN     NUMBER    --6
                                              , p_task_id                IN     NUMBER    --7
                                              , p_hide_prefix            IN     VARCHAR2
                                              --, p_alias                  IN     VARCHAR2
                                            );


    /*****************************************************************************************************************************************
    *   PROCEDURE VAL_SHELF_AND_UBD                                                                                                        *
    *                                                                                                                                        *
    *   PURPOSE:   This procedure is used to get the branch bin assignments for an item organization combination                             *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
    *                                                     RF - Receiving                                                                     *
    *****************************************************************************************************************************************/
  

         PROCEDURE VAL_SHELF_AND_UBD ( p_shelf_life   IN VARCHAR2
                                     , p_ubd          IN VARCHAR2
                                     , x_return       OUT NUMBER
                                     , x_message      OUT VARCHAR2
                                     );
                                     

    /*****************************************************************************************************************************************
    *   PROCEDURE PROCESS_EXPECTED_RECEIPTS_CCR                                                                                              *
    *                                                                                                                                        *
    *   PURPOSE:   This procedure is used to launch  the expected receipts report concurrent program                                         *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
    *                                                     RF - Receiving                                                                     *
    *   1.1        01-JUN-2015  Lee Spitzer               TMS Ticket 20150601-0247                                                           *
    *                                                     RF / Break Fix Expected Receipts Runs Wide Open                                    *
    *****************************************************************************************************************************************/
  
        PROCEDURE PROCESS_EXPECTED_RECEIPTS_CCR ( p_organization_id IN NUMBER
                                                 , p_po_number       IN VARCHAR2
                                                 , p_req_number      IN VARCHAR2
                                                 , p_rma_number      IN VARCHAR2
                                                 , p_vendor          IN VARCHAR2
                                                 , p_structure_id    IN NUMBER
                                                 , x_return          OUT NUMBER
                                                 , x_message         OUT VARCHAR2
                                                 );



                                                 
    /*****************************************************************************************************************************************
    *   FUNCTION GET_WC_BIN_COUNT                                                                                                            *
    *                                                                                                                                        *
    *   PURPOSE:   This function is to get the count of locators assigned to an item                                                         *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        16-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
    *                                                     RF - Receiving                                                                     *
    *****************************************************************************************************************************************/
  


           FUNCTION GET_WC_BIN_COUNT         (  p_organization_id        IN     NUMBER    --1
                                              , p_subinventory_code      IN     VARCHAR2  --2
                                              , p_inventory_item_id      IN     VARCHAR2  --3
                                              )
              RETURN NUMBER;
              
    /*****************************************************************************************************************************************
    *   FUNCTION GET_ITEM_LABEL_DESC                                                                                                         *
    *                                                                                                                                        *
    *   PURPOSE:   This function is to get the count of locators assigned to an item                                                         *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        11-FEB-2016  Lee Spitzer               TMS#20151103-00188 - RF Enhancements                                               *
    *****************************************************************************************************************************************/

              
              FUNCTION GET_ITEM_LABEL_DESC (p_flex_value             IN     VARCHAR2) RETURN VARCHAR2;

    /*****************************************************************************************************************************************
    *   FUNCTION GET_YES_NO_MEAN                                                                                                             *
    *                                                                                                                                        *
    *   PURPOSE:   This function is to get the count of locators assigned to an item                                                         *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        11-FEB-2016  Lee Spitzer               TMS#20151103-00188 - RF Enhancements                                               *
    *****************************************************************************************************************************************/
              
              FUNCTION GET_YES_NO_MEAN (p_lookup_code               IN     VARCHAR2) RETURN VARCHAR2;

   

                                          
END XXWC_LABEL_PRINTERS_PKG;