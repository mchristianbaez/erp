create or replace PACKAGE           XXWC_INV_BIN_MAINTENANCE_PKG AS

   /*****************************************************************************************************************************************
   *   $Header XXWC_INV_BIN_MAINTENANCE_PKG $                                                                                               *
   *   Module Name: XXWC_INV_BIN_MAINTENANCE_PKG                                                                                            *
   *                                                                                                                                        *
   *   PURPOSE:   This package is used by the extensions in MSCA Bin Maintenance Mobile Pages                                               *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
   *                                                     RF - Bin maintenance form                                                          *
   *   1.1        08-JUL-2015  Lee Spitzer               TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2           *
   *   1.2        27-JAN-2016  Lee Spitzer               TMS Ticket 20151103-00188 Mobile Stocking Changes                                *
   *****************************************************************************************************************************************/

  g_package                   VARCHAR2(30) := 'XXWC_INV_BIN_MAINTENANCE_PKG';
  g_call_from                 VARCHAR2(175);
  g_call_point                VARCHAR2(175);
  g_distro_list               VARCHAR2 (80) := 'hdsoracledevelopers@hdsupply.com';
  g_module                    VARCHAR2 (80) := 'inv';
  g_sequence                  NUMBER;
  g_debug                     VARCHAR2(1) := nvl(FND_PROFILE.VALUE('AFLOG_ENABLED'),'N'); --Fnd Debug Log = Yes / No
  g_log_level                 NUMBER := nvl(FND_PROFILE.VALUE('AFLOG_LEVEL'),6); --Fnd Debug Log Level
  g_sqlerrm                   VARCHAR2(1000);
  g_sqlcode                   NUMBER;   
  g_message                   VARCHAR2(2000);
  g_exception                 EXCEPTION; 
  g_language                  VARCHAR2(10) := userenv('LANG');
  g_gtin_cross_ref_type VARCHAR2(25) := fnd_profile.VALUE('INV:GTIN_CROSS_REFERENCE_TYPE');
  g_gtin_code_length NUMBER := 14;
  TYPE t_genref IS REF CURSOR;
  

     /*****************************************************************************************************************************************
     *  FUNCTION WC_BRANCH_LOCATOR_CONTROL                                                                                                    *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the subinventory has locators assigned to it                                                *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                            *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Count of locators tied to subinventory for the organization                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
     *                                                     RF - Bin maintenance form                                                          *
     *****************************************************************************************************************************************/


      FUNCTION WC_BRANCH_LOCATOR_CONTROL ( p_organization_id      IN VARCHAR2
                                         , p_subinventory_code   IN VARCHAR2)
              RETURN NUMBER;
              
      
     /*****************************************************************************************************************************************
     *   PROCEDURE PROCESS_ITEM_LOCATOR_TIE                                                                                                    *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to update or insert records into the MTL_SECONDARY_LOCATORS                                        *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_locator_id IN NUMBER --LOCATOR_ID                                                                                     *
     *                                                                                                                                        *
     *    Out :  X_RETURN = 0 = Success, 1 = Error                                                                                            *
     *           X_MESSAGE = Return message                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
     *                                                     RF - Bin maintenance form                                                          *
     *********y********************************************************************************************************************************/

        PROCEDURE PROCESS_ITEM_LOCATOR_TIE       ( p_organization_id    IN VARCHAR2
                                                 , p_inventory_item_id  IN VARCHAR2
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_locator_id IN VARCHAR2
                                                 , x_return  OUT NUMBER
                                                 , x_message OUT VARCHAR2);


     /*****************************************************************************************************************************************
     *   PROCEDURE DELETE_ITEM_LOCATOR_TIE                                                                                                    *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to remove records into the MTL_SECONDARY_LOCATORS                                                  *
     *                                                                                                                                        *
     *    Parameters: p_organization_id  IN NUMBER - organization_id                                                                          *
     *                p_inventory_item_id IN NUMBER -- inventory_item_id                                                                      *
     *                p_subinventory_code IN VARCHAR2 - subinventory_code                                                                     *
     *                p_locator_id IN NUMBER --LOCATOR_ID                                                                                     *
     *                                                                                                                                        *
     *    Out :  X_RETURN = 0 = Success, 1 = Error                                                                                            *
     *           X_MESSAGE = Return message                                                                                                   *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
     *                                                     RF - Bin maintenance form                                                          *
     *********y********************************************************************************************************************************/

        PROCEDURE DELETE_ITEM_LOCATOR_TIE        ( p_organization_id    IN VARCHAR2
                                                 , p_inventory_item_id  IN VARCHAR2
                                                 , p_subinventory_code  IN VARCHAR2
                                                 , p_locator_id IN VARCHAR2
                                                 , x_return  OUT NUMBER
                                                 , x_message OUT VARCHAR2);



    /*****************************************************************************************************************************************
    *   PROCEDURE GET_WC_BIN_ITEM_LOC                                                                                                        *
    *                                                                                                                                        *
    *   PURPOSE:   This procedure is used to get the branch bin assignments for an item organization combination                             *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
    *                                                     RF - Bin maintenance form                                                          *
    *****************************************************************************************************************************************/
  


           PROCEDURE GET_WC_BIN_ITEM_LOC      ( x_locators               OUT    NOCOPY t_genref --0
                                              , p_organization_id        IN     NUMBER    --1
                                              , p_subinventory_code      IN     VARCHAR2  --2
                                              , p_restrict_locators_code IN     NUMBER    --3
                                              , p_inventory_item_id      IN     VARCHAR2    --4
                                              , p_concatenated_segments  IN     VARCHAR2  --5
                                              , p_project_id             IN     NUMBER    --6
                                              , p_task_id                IN     NUMBER    --7
                                              --, p_alias                  IN     VARCHAR2
                                            );

     /*****************************************************************************************************************************************
     *  FUNCTION WC_LOCATOR                                                                                                                   *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the locator exists                                                                          *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Count of locators tied to subinventory for the organization                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
     *                                                     RF - Bin maintenance form                                                          *
     *****************************************************************************************************************************************/


      FUNCTION WC_LOCATOR ( p_locator              IN VARCHAR2)
              RETURN VARCHAR2;
              
     
     /*****************************************************************************************************************************************
     *  FUNCTION WC_LOCATOR_PREFIX                                                                                                                   *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the locator exists                                                                          *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Count of locators tied to subinventory for the organization                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
     *                                                     RF - Bin maintenance form                                                          *
     *****************************************************************************************************************************************/


      FUNCTION WC_LOCATOR_PREFIX ( p_locator              IN VARCHAR2)
              RETURN VARCHAR2;
      

     /*****************************************************************************************************************************************
     *  PROCEDURE CREATE_WC_LOCATOR                                                                                                           *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the locator exists                                                                          *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Count of locators tied to subinventory for the organization                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
     *                                                     RF - Bin maintenance form                                                          *
     *****************************************************************************************************************************************/


      PROCEDURE CREATE_WC_LOCATOR ( p_organization_id IN VARCHAR2
                                  , p_subinventory_code IN VARCHAR2
                                  , p_locator IN VARCHAR2
                                  , x_return  OUT NUMBER
                                  , x_message OUT VARCHAR2);
     


    /*****************************************************************************************************************************************
    *   PROCEDURE GET_WC_BIN_LOC                                                                                                             *
    *                                                                                                                                        *
    *   PURPOSE:   This procedure is used to get the branch bin assignments for an item organization combination                             *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
    *                                                     RF - Bin maintenance form                                                          *
    *****************************************************************************************************************************************/
  


           PROCEDURE GET_WC_BIN_LOC           ( x_locators               OUT    NOCOPY t_genref --0
                                              , p_organization_id        IN     NUMBER    --1
                                              , p_subinventory_code      IN     VARCHAR2  --2
                                              , p_restrict_locators_code IN     NUMBER    --3
                                              , p_inventory_item_id      IN     VARCHAR2    --4
                                              , p_concatenated_segments  IN     VARCHAR2  --5
                                              , p_project_id             IN     NUMBER    --6
                                              , p_task_id                IN     NUMBER    --7
                                              , p_hide_prefix            IN     VARCHAR2
                                              --, p_alias                  IN     VARCHAR2
                                            );



    /*****************************************************************************************************************************************
    *   FUNCTION BIN_INQUIRY                                                                                                                 *
    *                                                                                                                                        *
    *   PURPOSE:   This function is used to return all assigned bins to an item in 1 function return separeted by |                          *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
    *                                                     RF - Bin maintenance form                                                          *
    *****************************************************************************************************************************************/
  


           FUNCTION BIN_INQUIRY           ( p_organization_id        IN     NUMBER    --1
                                          , p_inventory_item_id      IN     VARCHAR2  --2
                                          )
              RETURN VARCHAR2;

    
    
    /*****************************************************************************************************************************************
    *   FUNCTION NEXT_BIN_PREFIX                                                                                                             *
    *                                                                                                                                        *
    *   PURPOSE:   This function is used to return all assigned bins to an item in 1 function return separeted by |                          *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
    *                                                     RF - Bin maintenance form                                                          *
    *****************************************************************************************************************************************/
  


           FUNCTION NEXT_BIN_PREFIX           ( p_organization_id        IN     NUMBER    --1
                                              , p_inventory_item_id      IN     VARCHAR2  --2
                                              )
              RETURN VARCHAR2;
    
    
    
    /*****************************************************************************************************************************************
    *   FUNCTION VAL_BIN                                                                                                                     *
    *                                                                                                                                        *
    *   PURPOSE:   This function is used to validate if the bin entered by the user has a -1, -2, or -3 record                               *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
    *                                                     RF - Bin maintenance form                                                          *
    *****************************************************************************************************************************************/
  


           FUNCTION VAL_BIN                   ( p_organization_id        IN     NUMBER    --1
                                              , p_locator                IN     VARCHAR2  --2
                                              )
              RETURN NUMBER;


    /*****************************************************************************************************************************************
    *   FUNCTION GET_BIN_VAL_LOC                                                                                                             *
    *                                                                                                                                        *
    *   PURPOSE:   This function is used to validate if the bin entered by the user has a -1, -2, or -3 record                               *
    *                                                                                                                                        *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        22-OCT-2014  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
    *                                                     RF - Bin maintenance form                                                          *
    *****************************************************************************************************************************************/
  


           FUNCTION GET_BIN_VAL_LOC           ( p_organization_id        IN     NUMBER    --1
                                              , p_locator                IN     VARCHAR2  --2
                                              , p_locator_prefix         IN     VARCHAR2  --3
                                              )
              RETURN NUMBER;


     /*****************************************************************************************************************************************
     *  PROCEDURE CREATE_NEW_LOCATOR                                                                                                          *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the locator exists                                                                          *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Count of locators tied to subinventory for the organization                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
     *                                                     RF - Bin maintenance form                                                          *
     *   1.1        08-JUL-2015  Lee Spitzer               TMS Ticket 20150714-00142 RF - Implement fixes to include with release 2           *
     *****************************************************************************************************************************************/


      PROCEDURE CREATE_NEW_LOCATOR ( p_organization_id IN NUMBER
                                  , p_subinventory_code IN VARCHAR2
                                  , p_locator IN VARCHAR2
                                  , p_locator_prefix IN VARCHAR2
                                  , x_return  OUT NUMBER
                                  , x_message OUT VARCHAR2);

     /*****************************************************************************************************************************************
     *  PROCEDURE CHECK_FOR_DUP_ASSIGNMENTS                                                                                                   *
     *                                                                                                                                        *
     *   PURPOSE:   This function is to return if the locator exists                                                                          *
     *                                                                                                                                        *
     *    Parameters: p_organization_id IN  VARCHAR2                                                                                          *
     *                p_subinventory_code IN VARCHAR2                                                                                         *
     *    Return : Count of locators tied to subinventory for the organization                                                                *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00164                                                          *
     *                                                     RF - Bin maintenance form                                                          *
     *****************************************************************************************************************************************/


      PROCEDURE CHECK_FOR_DUP_ASSIGNMENTS ( p_organization_id IN NUMBER       --1
                                          , p_inventory_item_id IN VARCHAR2   --2
                                          , p_subinventory_code IN VARCHAR2   --3
                                          , p_locator IN VARCHAR2             --4
                                          , x_return  OUT NUMBER              --5
                                          , x_message OUT VARCHAR2);          --6
    

     /*****************************************************************************************************************************************
     *   PROCEDURE validate_loc_prefix_val                                                                                                    *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to validate the locator pre fix value                                                              *
     *                                                                                                                                        *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        27-JAN-2016  Lee Spitzer               TMS Ticket 20151103-00188 Mobile Stocking                                          *
     *                                                                                                                                        *
     *****************************************************************************************************************************************/
  
      PROCEDURE validate_loc_prefix_val  ( p_qty         IN VARCHAR2
                                         , x_return  OUT NUMBER
                                         , x_message OUT VARCHAR2
                                         );   

     /*****************************************************************************************************************************************
     *   PROCEDURE cascade_bin_assignments_add                                                                                                *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to cascade bin assignments                                                                         *
     *                                                                                                                                        *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        27-JAN-2016  Lee Spitzer               TMS Ticket 20151103-00188 Mobile Stocking                                          *
     *                                                                                                                                        *
     *****************************************************************************************************************************************/
  
      PROCEDURE cascade_bin_assignments_add   ( p_organization_id IN NUMBER   --1
                                              , p_inventory_item_id IN VARCHAR2   --2
                                              , p_subinventory_code IN VARCHAR2   --3
                                              , p_locator_id IN VARCHAR2          --4
                                              , x_return  OUT NUMBER              --5
                                              , x_message OUT VARCHAR2);          --6 

     /*****************************************************************************************************************************************
     *   PROCEDURE cascade_bin_assignments_delete                                                                                             *
     *                                                                                                                                        *
     *   PURPOSE:   This procedure is used to cascade bin assignments                                                                         *
     *                                                                                                                                        *
     *                                                                                                                                        *
     *   REVISIONS:                                                                                                                           *
     *   Ver        Date        Author                     Description                                                                        *
     *   ---------  ----------  ---------------         -------------------------                                                             *
     *   1.0        27-JAN-2016  Lee Spitzer               TMS Ticket 20151103-00188 Mobile Stocking                                          *
     *                                                                                                                                        *
     *****************************************************************************************************************************************/
  
      PROCEDURE cascade_bin_assignments_delete   ( p_organization_id IN NUMBER   --1
                                                , p_inventory_item_id IN VARCHAR2   --2
                                                , p_subinventory_code IN VARCHAR2   --3
                                                , p_locator_id IN VARCHAR2          --4
                                                , x_return  OUT NUMBER              --5
                                                , x_message OUT VARCHAR2);          --6 

END XXWC_INV_BIN_MAINTENANCE_PKG;
/