/*************************************************************************
   *   $Header XXXWCCycleCountFListener.java $
   *   Module Name: XXWCCycleCountPage
   *   
   *   Package: package xxwc.oracle.apps.inv.count.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWC Cycle Count Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
        1.1       02-MAR-2016   Lee Spitzer             TMS Ticket 20160309-00091 - RF Mass Bin Locating Screen   

**************************************************************************/
package xxwc.oracle.apps.inv.count.server;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.presentation.telnet.TelnetSession;
import oracle.jdbc.OraclePreparedStatement;
import oracle.sql.NUMBER;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.fnd.flexj.KeyFlexfield;

import xxwc.oracle.apps.inv.count.server.XXWCCycleCount;
import xxwc.oracle.apps.inv.count.server.XXWCCycleCountIterator;
import xxwc.oracle.apps.inv.count.server.XXWCCycleCountQueryManager;
import xxwc.oracle.apps.inv.lov.server.*;
import xxwc.oracle.apps.inv.rcv.server.XXWCRcvGenPage;

/*************************************************************************
 *   NAME: XXWCCycleCountFListener implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCCycleCountFListener
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
 **************************************************************************/


public class XXWCCycleCountFListener implements MWAFieldListener {
    XXWCCycleCountPage pg;
    Session ses;
    String dialogPageButtons[];
    String g_exit_page = "N";
    
    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.count.server";
     *
     *   PURPOSE:   private method to pass gPackage from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/
    
    private static String gPackage = "xxwc.oracle.apps.inv.count.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCCycleCountFListener";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150302-00173 - RF - Cyclecount

     **************************************************************************/

    private static String gCallFrom = "XXWCCycleCountFListener";

    /*************************************************************************
     *   NAME: public XXWCCycleCounttFListener()
     *
     *   PURPOSE:   public method XXWCCycleCountFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150302-00173 - RF - Cyclecount

     **************************************************************************/

    public XXWCCycleCountFListener() {
        $init$();
    }

    /*************************************************************************
     *   NAME:  public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException
     *
     *   PURPOSE:   field entered for XXWCPrimaryBinAssignmentPage
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150302-00173 - RF - Cyclecount

     **************************************************************************/
    
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        pg = (XXWCCycleCountPage)ses.getCurrentPage();
        String s = UtilFns.fieldEnterSource(ses);
        
        if (s.equals("XXWC.CYCLECOUNT_HEADER")) {
            enteredCycleCountHeader(mwaevent);
            return;
        }
        /*Item*/
        if (s.equals("XXWC.ITEM")) {
            enteredItem(mwaevent);
            return;
        }
        if (s.equals("XXWC.LOC")) {
            enteredBins(mwaevent);
        }
        /*Sub*/
           /*Default For*/
        if (s.equals("XXWC.QTY")) {
            return;
        }

    }

    /*************************************************************************
     *   NAME:      public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   field exited for XXWCPrimaryBinAssignmentPage
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150302-00173 - RF - Cyclecount

     **************************************************************************/

    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {
        String s = ((FieldBean)mwaevent.getSource()).getName();
        String vField = mwaevent.getAction();
        boolean flag = false;
        boolean flag1 = false;
        boolean flag3 = false;
        if (mwaevent.getAction().equals("MWA_SUBMIT")) {
            flag = true;
        } else if (mwaevent.getAction().equals("MWA_PREVIOUSFIELD")) {
            flag1 = true;
        } else if (mwaevent.getAction().equals("MWA_NEXTFIELD")) {
            flag3 = true;
        }
        /*Save Next*/
        if (s.equals("XXWC.CYCLECOUNT_HEADER")) {
            exitedCycleCountHeader(mwaevent, ses);
            return;
        }
        if (s.equals("XXWC.ITEM")) {
            exitedItem(mwaevent, ses);
            return;
        }
        if (s.equals("XXWC.LOC")) {
            exitedBins(mwaevent, ses);
        }
        if (s.equals("XXWC.QTY")) {
            exitedQty(mwaevent, ses);
            return;
        }
        if (flag && s.equals("XXWC.PREVIOUS")) {
            exitedPrevious(mwaevent, ses);
            return;
        }
        if (flag && s.equals("XXWC.NEXT")) {
            exitedNext(mwaevent, ses);
            return;
        }
        if (flag && s.equals("XXWC.CLOSE")) {
            exitedClose(mwaevent, ses);
            return;
        }
        if (flag && s.equals("XXWC.PROCESS")) {
            exitedProcess(mwaevent, ses);
            return;
        }
        if (flag && s.equals("XXWC.CREATE_BIN")) {
            exitedCreateBin(mwaevent, ses);
            return;
        }
        if (flag && s.equals("XXWC.ADD_ITEM")) {
            exitedAddItem(mwaevent, ses);
            return;
        }
        
        /*Done*/
         else {
            return;
        }

    }

    /*************************************************************************
     *   NAME:      private void $init$()
     *
     *   PURPOSE:   $init$ sets the diaglogButtons button for user prompt messages to OK
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    private void $init$() {
        dialogPageButtons = (new String[] { "OK" });
    }


    /*************************************************************************
     *   NAME:      public void enteredCycleCountHeader(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   Item List of values for cycle count page
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/



    public void enteredCycleCountHeader(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {
  


    String gMethod = "enteredCycleCountHeader";
    String gCallPoint = "Start";
    int l_max_count = 50;
    
    
            ses.putObject("MWA_AUTO_ENTER", Boolean.TRUE);
            
            try {
                //Calling Default Cycle Header Id
                String p_organization_id = (String)ses.getObject("ORGID");
                Integer x_return = 0;
                CallableStatement cstmt = null;
                Connection con = ses.getConnection();
                cstmt = con.prepareCall("{? = call XXWC_CC_MOBILE_PKG.GET_DEFAULT_CC_HEADER_ID(?)");
                cstmt.registerOutParameter(1, Types.INTEGER);
                cstmt.setString(2, p_organization_id);
                cstmt.execute();
                x_return = cstmt.getInt(1);
                cstmt.close();
                if (x_return <= 0) {
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    Integer k =
                        telnetsessionX.showPromptPage("Error!", "Unable to find default cycle count for this org.  Exiting Form.",
                                                      dialogPageButtons);
                    ses.setStatusMessage(pg.WMS_TXN_CANCEL);
                    ses.clearAllApplicationScopeObjects();
                    ses.setStatusMessage(XXWCCycleCountPage.WMS_TXN_CANCEL);
                    pg.getmItemFld().setNextPageName("|END_OF_TRANSACTION|");
                }
                else {
                    pg.getmCycleCountHeaderFld().setValue(x_return.toString());
                }
            }
            catch (Exception e) {
                UtilFns.error("Error in calling enteredItem" + e);
            }
            
            
            //Check if count is too high for cycle count or user will have to wait for minutes
            if (ses.getObject("TXN.DOCTYPE").equals("CYCLE")) {
               
                int l_count = getOpenCountQty(mwaevent, ses);
                
                if (l_count <= l_max_count) {
            
            
                    try {
                        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " calling countQuery");
                        countQuery(mwaevent , ses);
                    }
                    catch (Exception exception) {
                        UtilFns.error("Error in calling enteredCycleCountHeader " + exception);
                    }
                
                }
                //exit the form if count is too high for cycle count and use physical form
                else {
                        TelnetSession telnetsessionX1 = (TelnetSession)ses;
                        Integer k1 =
                        telnetsessionX1.showPromptPage("Error!", "The open counts of " + l_count + " are greater than " + l_max_count + ".  Please use the physical form for better performance.",
                                                                dialogPageButtons);
                            if (k1 == 0) {
                                g_exit_page = "Y";
                                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);

                            }
                }
    
            }
            else if ((ses.getObject("TXN.DOCTYPE").equals("PHYSICAL"))) {
                ses.setNextFieldName("XXWC.LOC");
                pg.getmLocFld().setEditable(true);
                pg.getmLocFld().setRequired(false);
            }
            
    }
    
    /*************************************************************************
     *   NAME:      public void enteredCycleCountHeader(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   Item List of values for cycle count page
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/



    public void exitedCycleCountHeader(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {
    


    String gMethod = "exitedCycleCountHeader";
    String gCallPoint = "Start";
    
        //Check if count is too high for cycle count or user will have to wait for minutes
        if  (g_exit_page.equals("Y")){       
            ses.setStatusMessage(pg.WMS_TXN_CANCEL);
            ses.clearAllApplicationScopeObjects();
            ses.setStatusMessage(XXWCRcvGenPage.WMS_TXN_CANCEL);
            ses.setNextFieldName("");
            ses.setRefreshScreen(true);
            pg.getmCycleCountHeaderFld().setNextPageName("|END_OF_TRANSACTION|");
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
        }
            
    }

    /*************************************************************************
     *   NAME:      public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   Item List of values for cycle count page
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/



    public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {

        
        pg.getmCycleCountHeaderFld().setHidden(true);
        ses.setRefreshScreen(true);
        
        try {
            XXWCItemLOV itemlov = pg.getmItemFld();
            itemlov.setValidateFromLOV(true);
            itemlov.setlovStatement("XXWC_CC_MOBILE_PKG.GET_WC_ITEM_LOV");
            String paramType[] = { "C", "N", "S", "S", "S", "S", "S" };
            String parameters[] =
            { " ", "ORGID", "xxwc.oracle.apps.inv.count.server.XXWCCycleCountPage.XXWC.ITEM", "xxwc.oracle.apps.inv.count.server.XXWCCycleCountPage.XXWC.CYCLECOUNT_HEADER", "TXN.ENTRY_STATUS_CODE", "xxwc.oracle.apps.inv.count.server.XXWCCycleCountPage.XXWC.WC_COUNT_SEQUENCE", "xxwc.oracle.apps.inv.count.server.XXWCCycleCountPage.XXWC.LOC" };
            itemlov.setInputParameterTypes(paramType);
            itemlov.setInputParameters(parameters);
        } catch (Exception e) {
            UtilFns.error("Error in calling enteredItem" + e);
        }
    }

    /*************************************************************************
     *   NAME: public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        try {
            
            if (pg.getmItemFld().getValue().equals(null) || pg.getmItemFld().getValue().equals("") ) {
                //pg.getmCountQtyFld().setEditable(false);
            }
            else {
                
                pg.getmItemFld().getValue().toUpperCase();
                
                String l_item = pg.getmItemFld().getValue().trim();
                String l_item_ro = pg.getmItemReadOnly().getValue().trim();
                
                if (!l_item.equals(l_item_ro)) {
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    Integer k =
                    telnetsessionX.showPromptPage("Error!", "Scanned item " + pg.getmItemFld().getValue() + " does not match current item record " + pg.getmItemReadOnly().getValue(),
                                                            dialogPageButtons);
                        if (k == 0) {
                            pg.getmItemFld().setValue("");
                            //pg.getmCountQtyFld().setEditable(true);
                            ses.setNextFieldName("XXWC.ITEM");
                        }
                    }
                else {
                    //pg.getmCountQtyFld().setEditable(true);
                    ses.setNextFieldName("XXWC.QTY");
                }
            }
    }
    catch (Exception e) {
            UtilFns.error("Error in calling exitedItem" + e);
        }
    }


    /*************************************************************************
     *   NAME: public void exitedQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

 
    public void exitedQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
        {
            String gMethod = "exitedQty";
            String gCallPoint = "INV.QTY start";
            Double d;
            String l_quantity = pg.getmCountQtyFld().getValue();
            FileLogger.getSystemLogger().trace(gMethod + " " + gCallPoint + " " + "l_quantity " + l_quantity);
            String exception_message = "";
            if (!l_quantity.isEmpty()) {
                    try {
                        d = Double.valueOf(pg.getmCountQtyFld().getValue());
                        if (d < 0) {
                                    exception_message = "Number is not positive";
                                       throw new AbortHandlerException(exception_message);
                            }
                        }
                     catch (Exception exception) {
                        gCallPoint = "Number value check";
                        UtilFns.error(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + exception);
                        //pg.mSaveNext.setHidden(true);
                        //pg.mDone.setHidden(true);
                        if (exception_message == "") {
                            ses.setStatusMessage(UtilFns.getMessage(ses, "INV", "INV_MWA_NUMBER_EXPECTED"));
                        }
                        else  {
                            ses.setStatusMessage(exception_message);
                        }    
                        throw new AbortHandlerException((new StringBuilder()).append(gMethod + " "  + gCallPoint).append(exception).toString());
                    }
                    //pg.mSaveNext.setHidden(false);
                    //pg.mDone.setHidden(false);
            }
            else {
                //pg.mSaveNext.setHidden(true);
                //pg.mDone.setHidden(true);
            }
            
            processCCEntriesTable(mwaevent, ses);
        }
    
    

    /*************************************************************************
     *   NAME: public void processCCEntriesTable(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                 DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    
    public void processCCEntriesTable(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
        {
            String gMethod = "processCCEntriesTable";
            String gCallPoint = "start";
            String p_cycle_count_header_id;
            String p_cycle_count_entry_id;
            String p_wc_count_list_sequence;
            String p_count_list_sequence; 
            String p_organization_id; 
            String p_inventory_item_id; 
            String p_revision; 
            String p_quantity;
            String p_primary_uom_code;
            String p_subinventory; 
            String p_locator_id;
            String p_lot_number;
            String p_cost_group_id;
            String p_user_id;
            Integer x_return;
            String x_message;
            
            p_cycle_count_header_id = pg.getmCycleCountHeaderFld().getValue();
            p_cycle_count_entry_id = pg.getmCycleCountEntryId().getValue();
            p_wc_count_list_sequence = pg.getmWCCycleCountSequence().getValue();
            p_count_list_sequence = pg.getmCycleCountSequence().getValue();
            p_organization_id = (String)ses.getObject("ORGID");
            p_inventory_item_id = pg.getmItemIdFld().getValue();
            p_revision = "";
            p_quantity = pg.getmCountQtyFld().getValue();
            p_primary_uom_code = pg.getmUOMFld().getValue();
            p_subinventory = pg.getmSubFld().getValue();
            p_locator_id = pg.getmInventoryLocationId().getValue();
            p_lot_number = pg.getmLotFld().getValue();
            p_cost_group_id = pg.getmCostGroupId().getValue();
            p_user_id = (String)ses.getObject("USERID");
            x_return = -1;
            x_message = "";
            
            try {
                  CallableStatement cstmt = null;
                  Connection con = ses.getConnection();
                  //                                                                       1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6    
                  cstmt = con.prepareCall("{call XXWC_CC_MOBILE_PKG.PROCESS_CC_ENTRIES_TBL(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                  cstmt.setString(1, p_cycle_count_header_id);
                  cstmt.setString(2, p_cycle_count_entry_id);
                  cstmt.setString(3, p_wc_count_list_sequence);
                  cstmt.setString(4, p_count_list_sequence);
                  cstmt.setString(5, p_organization_id);
                  cstmt.setString(6, p_inventory_item_id);
                  cstmt.setString(7, p_revision);
                  cstmt.setString(8, p_quantity);
                  cstmt.setString(9, p_primary_uom_code);
                  cstmt.setString(10, p_subinventory);
                  cstmt.setString(11, p_locator_id);
                  cstmt.setString(12, p_lot_number);
                  cstmt.setString(13, p_cost_group_id);
                  cstmt.setString(14, p_user_id);
                  cstmt.registerOutParameter(15, Types.INTEGER); //x_return
                  cstmt.registerOutParameter(16, Types.VARCHAR); //x_message
                  cstmt.execute();
                  x_return = cstmt.getInt(15);
                  x_message = cstmt.getString(16);
                  cstmt.close();
                  boolean showSubError = true;
                  if (x_return > 1) {
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    Integer k =
                    telnetsessionX.showPromptPage("Error!", x_message,
                                                            dialogPageButtons);
                        if (k == 0) {
                            pg.getmCountQtyFld().setValue(null);
                        }
                  }
            }
            catch (Exception e) {
                UtilFns.error("Error in validating " + gMethod + " " + e);
            }
                                            
        }
        


    /*************************************************************************
    *   NAME:     public void countQuery(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/
    
    public void countQuery(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String gMethod = "countQuery";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
                        
        try{
            gCallPoint = "Entering Try";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            //Call the QueryManager to execute the query
            java.sql.Connection con = ses.getConnection();
            XXWCCycleCountQueryManager XXWCCycleCountQueryManager = new XXWCCycleCountQueryManager(con);
            
            String p_organization_id = (String)ses.getObject("ORGID");
            String p_item = "";
            String p_cycle_count_header_id = pg.getmCycleCountHeaderFld().getValue();
            String p_entry_status_code = (String)ses.getObject("TXN.ENTRY_STATUS_CODE");
            String p_wc_count_list_sequence = pg.getmWCCycleCountSequence().getValue();
            String p_wc_locator = pg.getmLocFld().getmWCLocator();
            Vector vector1;
            gCallPoint = "about to call vector 1";
            //Execute the getmtlItemLocatorDefaultStr query and set the vector
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            vector1 =
                    XXWCCycleCountQueryManager.getmWCCycleCount(p_organization_id, p_item, p_cycle_count_header_id, p_entry_status_code, p_wc_count_list_sequence, p_wc_locator);
        
            gCallPoint = "results after vector 1";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " vector size " + vector1.size() );
            
            if (vector1.size() == 0) {
                gCallPoint = "vector1 is 0";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
                TelnetSession telnetsessionX = (TelnetSession)ses;
                Integer k =
                    telnetsessionX.showPromptPage("Error!", "No count records ", dialogPageButtons);
                //showSubError = true;
                if (k == 0) {
                    pg.getmNext().setHidden(true);
                    pg.getmPrevious().setHidden(true);
                    return;
                    
                } 
        }
            else {
                //If the vector has results then set session values, retun page
                gCallPoint = "vector1 found";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
                XXWCCycleCountIterator XXWCCycleCountIterator = new XXWCCycleCountIterator(vector1);
                ses.putObject("XXWCCYCLECOUNTSEQ", XXWCCycleCountIterator);
                XXWCCycleCountIterator mXXWCCycleCountSeq = (XXWCCycleCountIterator)ses.getObject("XXWCCYCLECOUNTSEQ");
                int i = mXXWCCycleCountSeq.getCurrentPos();
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " mXXWCCylceCountSeq " + mXXWCCycleCountSeq);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " i " + i);
                if (i == 0){
                    gCallPoint = "i == 0";
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " mXXWCCyleCountSeq == null");
                    try {
                         pg.mCurrentCycleCount = mXXWCCycleCountSeq.next();
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.mCurrentCycleCount " + pg.mCurrentCycleCount);
                         
                         //pg.getmItemFld().setValue(pg.getmCurrentXXWCCycleCount().getmItemNumber());
                         pg.getmCycleCountEntryId().setValue(pg.getmCurrentXXWCCycleCount().getmCycleCountEntryId());
                         pg.getmWCCycleCountSequence().setValue(pg.getmCurrentXXWCCycleCount().getmWCCountListSequence());
                         pg.getmCycleCountSequence().setValue(pg.getmCurrentXXWCCycleCount().getmCountListSequence());
                         pg.getmCostGroupId().setValue(pg.getmCurrentXXWCCycleCount().getmCostGroupId());
                         pg.getmStockLocator().setValue(pg.getmCurrentXXWCCycleCount().getmBin());
                         pg.getmLocFld().setValue(pg.getmCurrentXXWCCycleCount().getmWCBin());
                         pg.getmInventoryLocationId().setValue(pg.getmCurrentXXWCCycleCount().getmInventoryLocationId());
                         pg.getmItemReadOnly().setValue(pg.getmCurrentXXWCCycleCount().getmItemNumber());
                         pg.getmItemDescFld().setValue(pg.getmCurrentXXWCCycleCount().getmItemDescription());
                         pg.getmItemIdFld().setValue(pg.getmCurrentXXWCCycleCount().getmInventoryItemId());
                         pg.getmSubFld().setValue(pg.getmCurrentXXWCCycleCount().getmSubinventory());
                         pg.getmUOMFld().setValue(pg.getmCurrentXXWCCycleCount().getmPrimaryUOMCode());
                         pg.getmMinFld().setValue(pg.getmCurrentXXWCCycleCount().getmMinQty());
                         pg.getmMaxFld().setValue(pg.getmCurrentXXWCCycleCount().getmMaxQty());
                         pg.getmSVFld().setValue(pg.getmCurrentXXWCCycleCount().getmSalesVelocity());
                         pg.getmDueDateFld().setValue(pg.getmCurrentXXWCCycleCount().getmDueDate());
                         pg.getmPriorCountQuantityFld().setValue(pg.getmCurrentXXWCCycleCount().getmPriorCountQuantity());
                         pg.getmSSQty().setValue(pg.getmCurrentXXWCCycleCount().getmSnapShotQuantity());
                         //pg.getmCountQtyFld().setValue(pg.getmCurrentXXWCCycleCount().getmCountQuantity());
                        
                         
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmItemFld().getValue " + pg.getmItemFld().getValue());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmCycleCountEntryId().getValue " + pg.getmCycleCountEntryId().getValue());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmWCCycleCountSequence().getValue() " + pg.getmWCCycleCountSequence().getValue());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmCycleCountSequence().getValue " + pg.getmCycleCountSequence().getValue());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmCostGroupId().getValue " + pg.getmCostGroupId().getValue());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmStockLocator().getValue " + pg.getmStockLocator().getValue());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLocFld().getValue " + pg.getmLocFld().getValue());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmInventoryLocationId().getValue " + pg.getmInventoryLocationId().getValue());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmItemReadOnly().getValue " + pg.getmItemReadOnly().getValue());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmItemDescFld().getValue " + pg.getmItemDescFld().getValue());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmItemIdFld().getValue " + pg.getmItemIdFld().getValue());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmSubFld().getValue " + pg.getmSubFld().getValue()); 
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmUOMFld().getValue " + pg.getmUOMFld().getValue());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmMinFld().getValue " + pg.getmMinFld().getValue());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmMaxFld().getValue " + pg.getmMaxFld().getValue());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmSVFld().getValue " + pg.getmSVFld().getValue());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmDueDate().getValue " + pg.getmDueDateFld().getValue());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmPriorCountQuantityFld().getValue " + pg.getmPriorCountQuantityFld().getValue());
                         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmSSQty().getValue " + pg.getmSSQty().getValue());
                        
                         //FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmCountQtyFld().getValue " + pg.getmCountQtyFld().getValue());
                        
                        gCallPoint = " getmLotNumber ";
                        
                        if (pg.getmLotFld().getValue().equals(null) || pg.getmLotFld().getValue().equals("") ) {
                            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLotFld().setHidden(true); ");
                            pg.getmLotFld().setHidden(true);
                        }
                        else {
                            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLotFld().setHidden(false); ");
                            pg.getmLotFld().setHidden(false);
                        }
                        
                        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmCurrentXXWCCycleCount().getmCountQuantity() " + pg.getmCurrentXXWCCycleCount().getmCountQuantity());
                        
                        if (pg.getmCurrentXXWCCycleCount().getmCountQuantity().equals("BLANK") || pg.getmCurrentXXWCCycleCount().getmCountQuantity().equals("")) {
                            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_return is null");
                            pg.getmCountQtyFld().setValue("");
                        }
                        else {
                            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_return is not null");
                            //String l_return;
                            //l_return = Integer.toString(x_return);
                            pg.getmCountQtyFld().setValue(pg.getmCurrentXXWCCycleCount().getmCountQuantity());
                        }  
                        
                        if (vector1.size()==1){
                            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " Vector Size is 1");
                            pg.getmNext().setHidden(true);
                            pg.getmPrevious().setHidden(true);
                         }
                         if (vector1.size()>1) {
                             FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " Vector Size > 1 ");
                             pg.getmNext().setHidden(false);
                             pg.getmPrevious().setHidden(true);
                         }
                        
                        
                    } catch (Exception e) {
                        FileLogger.getSystemLogger().trace("Error calling " + gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + e);
                    }
                }
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + pg.getmCurrentXXWCCycleCount());
                
            }
            
            
            gCallPoint = "Getting Entry Status Code";
            
            String l_entry_status_code = (String)ses.getObject("TXN.ENTRY_STATUS_CODE");
            
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l_entry_status_code " + l_entry_status_code);
            
            if (l_entry_status_code.equals("3")) {
                pg.getmDueDateFld().setHidden(false);
                pg.getmSSQty().setHidden(false);
                pg.getmPriorCountQuantityFld().setHidden(false);
                pg.getmProcess().setHidden(true);
            }
            else {
                pg.getmDueDateFld().setHidden(true);
                pg.getmSSQty().setHidden(true);
                pg.getmPriorCountQuantityFld().setHidden(true);
                if (ses.getObject("TXN.DOCTYPE").equals("PHYSICAL")){
                    pg.getmProcess().setHidden(true);
                }
                if (ses.getObject("TXN.DOCTYPE").equals("CYCLE")){
                    pg.getmProcess().setHidden(false);
                }
            }
            
        } catch (Exception e) {
            ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_ERROR).append(" (").append(e).append(")").toString());
            UtilFns.error((new StringBuilder()).append("Error in countQuery: error - ").append(e).toString());
        }
        
        gCallPoint = "getTotalCountQty";
            
        try {
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            getTotalCountQty(mwaevent , ses);
        }
        catch (Exception exception) {
            UtilFns.error("Error in calling getTotalCountQty " + exception);
        }
        
    }


    /*************************************************************************
    *   NAME:     public void exitedNext(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/

    public void exitedNext(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String gMethod = "exitedNext";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
        
            
            XXWCCycleCountIterator XXWCCycleCountIterator = (XXWCCycleCountIterator)ses.getObject("XXWCCYCLECOUNTSEQ");
            
            int i = XXWCCycleCountIterator.getCurrentPos();
            int l = -1;
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + XXWCCycleCountIterator);
            //FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + XXWCCycleCount);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " i " + i);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l " + l);
            Connection connection = ses.getConnection();
            XXWCCycleCountQueryManager XXWCCycleCountQueryManager =
                new XXWCCycleCountQueryManager(connection);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " XXWCCycleCountQueryManager " + XXWCCycleCountQueryManager);
            try {
                XXWCCycleCount XXWCCycleCount1 = XXWCCycleCountIterator.next();
                l = XXWCCycleCountIterator.max_left();
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " XXWCCycleCount1 " + XXWCCycleCount1);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l " + l);
                if (XXWCCycleCount1 != null) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " XXWCCycleCount1 != null ");
                    pg.setCurrentXXWCCycleCount(XXWCCycleCount1);
                }
                if (l == 0) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l == 0");
                    pg.getmNext().setHidden(true);
                    pg.getmPrevious().setHidden(true);
                    ses.setRefreshScreen(true);
                    ses.setNextFieldName("XXWC.ITEM");
                } else {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l != 0");
                    pg.getmNext().setHidden(false);
                    ses.setNextFieldName("XXWC.ITEM");
                    ses.setRefreshScreen(true);
                }
                pg.getmPrevious().setHidden(false);
                //ses.setRefreshScreen(true);
                int j = XXWCCycleCountIterator.getCurrentPos();
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " j " + j);
                  
                pg.getmCycleCountEntryId().setValue(XXWCCycleCount1.getmCycleCountEntryId());
                pg.getmWCCycleCountSequence().setValue(XXWCCycleCount1.getmWCCountListSequence());
                pg.getmCycleCountSequence().setValue(XXWCCycleCount1.getmCountListSequence());
                pg.getmCostGroupId().setValue(XXWCCycleCount1.getmCostGroupId());
                pg.getmStockLocator().setValue(XXWCCycleCount1.getmBin());
                pg.getmLocFld().setValue(XXWCCycleCount1.getmWCBin());
                pg.getmInventoryLocationId().setValue(XXWCCycleCount1.getmInventoryLocationId());
                pg.getmItemReadOnly().setValue(XXWCCycleCount1.getmItemNumber());
                pg.getmItemDescFld().setValue(XXWCCycleCount1.getmItemDescription());
                pg.getmItemIdFld().setValue(XXWCCycleCount1.getmInventoryItemId());
                pg.getmSubFld().setValue(XXWCCycleCount1.getmSubinventory());
                pg.getmUOMFld().setValue(XXWCCycleCount1.getmPrimaryUOMCode());
                pg.getmMinFld().setValue(XXWCCycleCount1.getmMinQty());
                pg.getmMaxFld().setValue(XXWCCycleCount1.getmMaxQty());
                pg.getmSVFld().setValue(XXWCCycleCount1.getmSalesVelocity());
                pg.getmDueDateFld().setValue(XXWCCycleCount1.getmDueDate());
                pg.getmPriorCountQuantityFld().setValue(XXWCCycleCount1.getmPriorCountQuantity());
                pg.getmSSQty().setValue(pg.getmCurrentXXWCCycleCount().getmSnapShotQuantity());
                //pg.getmCountQtyFld().setValue(XXWCCycleCount1.getmCountQuantity());
                
                
                gCallPoint = " getmLotNumber ";
                
                if (pg.getmLotFld().getValue().equals(null) || pg.getmLotFld().getValue().equals("") ) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLotFld().setHidden(true); ");
                    pg.getmLotFld().setHidden(true);
                }
                else {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLotFld().setHidden(false); ");
                    pg.getmLotFld().setHidden(false);
                }
                

                gCallPoint = "GetCCQty";
                    
                CallableStatement cstmt = null;
                Connection con = ses.getConnection();
                cstmt = con.prepareCall("{? = call XXWC_CC_MOBILE_PKG.GET_CC_VALUE(?,?,?,?,?,?)");
                cstmt.registerOutParameter(1, Types.VARCHAR);
                cstmt.setString(2, pg.getmCycleCountHeaderFld().getValue());
                cstmt.setString(3, XXWCCycleCount1.getmCycleCountEntryId());
                cstmt.setString(4, XXWCCycleCount1.getmWCCountListSequence());
                cstmt.setString(5, XXWCCycleCount1.getmCountListSequence());
                cstmt.setString(6, (String)ses.getObject("ORGID"));
                cstmt.setString(7, XXWCCycleCount1.getmInventoryItemId());
                cstmt.execute();
                String x_return = cstmt.getString(1);
                cstmt.close();
                
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_return " + x_return);
                
                if (x_return.equals("BLANK") || x_return.equals("")) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_return is null");
                    pg.getmCountQtyFld().setValue("");
                }
                else {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_return is not null");
                    //String l_return;
                    //l_return = Integer.toString(x_return);
                    pg.getmCountQtyFld().setValue(x_return);
                }  
                
                } catch (Exception exception1) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " exception1 " + exception1);
                    UtilFns.log((new StringBuilder()).append(" ").append(exception1).toString());
                }
                
            
            pg.getmItemFld().setValue("");
            //pg.getmCountQtyFld().setEditable(false);
            ses.setRefreshScreen(true);
            
            if (pg.getmLocFld().isEditable()){
                pg.getmLocFld().setValue((String)ses.getObject("sessXXWC.LOC"));
            }
            
            //ses.setNextFieldName("XXWC.NEXT_ITEM");
            
            gCallPoint = "getTotalCountQty";
            
            try {
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
                getTotalCountQty(mwaevent , ses);
            }
            catch (Exception exception) {
                UtilFns.error("Error in calling getTotalCountQty " + exception);
            }
        }



    /*************************************************************************
    *   NAME:     public void exitedPrevious(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/
    
    public void exitedPrevious(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String gMethod = "exitedPrevious";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            
            XXWCCycleCountIterator XXWCCycleCountIterator = (XXWCCycleCountIterator)ses.getObject("XXWCCYCLECOUNTSEQ");
            
            int i = XXWCCycleCountIterator.getCurrentPos();
            int l = -1;
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + XXWCCycleCountIterator);
            //FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + XXWCBinPutAway);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " i " + i);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l " + l);
            Connection connection = ses.getConnection();
            XXWCCycleCountQueryManager XXWCCycleCountQueryManager =
                new XXWCCycleCountQueryManager(connection);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " XXWCCycleCountQueryManager " + XXWCCycleCountQueryManager);
            try {
                XXWCCycleCount XXWCCycleCount1 = XXWCCycleCountIterator.previous();
                l = XXWCCycleCountIterator.max_left();
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " XXWCCycleCount1 " + XXWCCycleCount1);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l " + l);
                if (XXWCCycleCount1 != null) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " XXWCCycleCount1 != null ");
                    pg.setCurrentXXWCCycleCount(XXWCCycleCount1);
                }
                if (l == 0) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l == 0");
                    pg.getmNext().setHidden(true);
                    pg.getmPrevious().setHidden(true);
                    ses.setRefreshScreen(true);
                    ses.setNextFieldName("XXWC.NEXT_ITEM");
                } else {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l != 0");
                    pg.getmNext().setHidden(false);
                    pg.getmPrevious().setHidden(false);
                    ses.setRefreshScreen(true);
                    ses.setNextFieldName("XXWC.PREVIOUS");
                }
                //pg.getmPrevious().setHidden(false);
                //ses.setRefreshScreen(true);
                int j = XXWCCycleCountIterator.getCurrentPos();
                if (j == 1 || j == 0) {
                    pg.getmPrevious().setHidden(true);
                    ses.setNextFieldName("XXWC.NEXT");
                }
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " j " + j);
                
                  
                pg.getmCycleCountEntryId().setValue(XXWCCycleCount1.getmCycleCountEntryId());
                pg.getmWCCycleCountSequence().setValue(XXWCCycleCount1.getmWCCountListSequence());
                pg.getmCycleCountSequence().setValue(XXWCCycleCount1.getmCountListSequence());
                pg.getmCostGroupId().setValue(XXWCCycleCount1.getmCostGroupId());
                pg.getmStockLocator().setValue(XXWCCycleCount1.getmBin());
                pg.getmLocFld().setValue(XXWCCycleCount1.getmWCBin());
                pg.getmInventoryLocationId().setValue(XXWCCycleCount1.getmInventoryLocationId());
                pg.getmItemReadOnly().setValue(XXWCCycleCount1.getmItemNumber());
                pg.getmItemDescFld().setValue(XXWCCycleCount1.getmItemDescription());
                pg.getmItemIdFld().setValue(XXWCCycleCount1.getmInventoryItemId());
                pg.getmSubFld().setValue(XXWCCycleCount1.getmSubinventory());
                pg.getmUOMFld().setValue(XXWCCycleCount1.getmPrimaryUOMCode());
                pg.getmMinFld().setValue(XXWCCycleCount1.getmMinQty());
                pg.getmMaxFld().setValue(XXWCCycleCount1.getmMaxQty());
                pg.getmSVFld().setValue(XXWCCycleCount1.getmSalesVelocity());
                pg.getmDueDateFld().setValue(XXWCCycleCount1.getmDueDate());
                pg.getmPriorCountQuantityFld().setValue(XXWCCycleCount1.getmPriorCountQuantity());
                pg.getmSSQty().setValue(pg.getmCurrentXXWCCycleCount().getmSnapShotQuantity());
                //pg.getmCountQtyFld().setValue(XXWCCycleCount1.getmCountQuantity());
                
                gCallPoint = " getmLotNumber ";
                
                if (pg.getmLotFld().getValue().equals(null) || pg.getmLotFld().getValue().equals("") ) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLotFld().setHidden(true); ");
                    pg.getmLotFld().setHidden(true);
                }
                else {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " pg.getmLotFld().setHidden(false); ");
                    pg.getmLotFld().setHidden(false);
                }
                
                gCallPoint = "GetCCQty";
                
                
                CallableStatement cstmt = null;
                Connection con = ses.getConnection();
                cstmt = con.prepareCall("{? = call XXWC_CC_MOBILE_PKG.GET_CC_VALUE(?,?,?,?,?,?)");
                cstmt.registerOutParameter(1, Types.VARCHAR);
                cstmt.setString(2, pg.getmCycleCountHeaderFld().getValue());
                cstmt.setString(3, XXWCCycleCount1.getmCycleCountEntryId());
                cstmt.setString(4, XXWCCycleCount1.getmWCCountListSequence());
                cstmt.setString(5, XXWCCycleCount1.getmCountListSequence());
                cstmt.setString(6, (String)ses.getObject("ORGID"));
                cstmt.setString(7, XXWCCycleCount1.getmInventoryItemId());
                cstmt.execute();
                String x_return = cstmt.getString(1);
                cstmt.close();
                
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_return " + x_return);
                
                if (x_return.equals("BLANK") || x_return.equals("")) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_return is null");
                    pg.getmCountQtyFld().setValue("");
                }
                else {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_return is not null");
                    //String l_return;
                    //l_return = Integer.toString(x_return);
                    pg.getmCountQtyFld().setValue(x_return);
                }  
                
                } catch (Exception exception1) {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " exception1 " + exception1);
                    UtilFns.log((new StringBuilder()).append(" ").append(exception1).toString());
                }
            
            if (pg.getmLocFld().isEditable()){
                pg.getmLocFld().setValue((String)ses.getObject("sessXXWC.LOC"));
            }
            
            pg.getmItemFld().setValue("");
            //pg.getmCountQtyFld().setEditable(false);
            
            //ses.setNextFieldName("XXWC.NEXT_ITEM");
            
            gCallPoint = "getTotalCountQty";
            
            try {
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
                getTotalCountQty(mwaevent , ses);
            }
            catch (Exception exception) {
                UtilFns.error("Error in calling getTotalCountQty " + exception);
            }
        }
    
    /*************************************************************************
    *   NAME: public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/

    public void exitedClose(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        String gMethod = "exitedClose";
        String gCallPoint = "Start";
        XXWCCycleCountPage _tmp = pg;
        ses.setStatusMessage("Close");
        ses.clearAllApplicationScopeObjects();
        ses.setStatusMessage("Close");
        pg.getmClose().setNextPageName("|END_OF_TRANSACTION|");
    }
    
    
    /*************************************************************************
    *   NAME: public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/

    public void exitedProcess(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        String gMethod = "exitedProcess";
        String gCallPoint = "Start";
        XXWCCycleCountPage _tmp = pg;
        
        TelnetSession telnetsessionX = (TelnetSession)ses;
        Integer k =
            telnetsessionX.showPromptPage("Process!", "Are you finished counting and ready to submit your counts? ", new String[] { "Yes", "No" });
        //showSubError = true;
        if (k == 0) {
            
            String p_cycle_count_header_id;
            String p_organization_id; 
            String p_entry_status_code;
            String p_user_id;
            Integer x_return;
            String x_message;
            
        
            p_cycle_count_header_id = pg.getmCycleCountHeaderFld().getValue();
            p_organization_id = (String)ses.getObject("ORGID");
            p_user_id = (String)ses.getObject("USERID");
            p_entry_status_code = (String)ses.getObject("TXN.ENTRY_STATUS_CODE");
            x_return = -1;
            x_message = "";
            
            try {
                  CallableStatement cstmt = null;
                  Connection con = ses.getConnection();
                  //                                                            1 2 3 4 5 6  
                  cstmt = con.prepareCall("{call XXWC_CC_MOBILE_PKG.PROCESS_API(?,?,?,?,?,?)}");
                  cstmt.setString(1, p_organization_id);
                  cstmt.setString(2, p_cycle_count_header_id);
                  cstmt.setString(3, p_entry_status_code);
                  cstmt.setString(4, p_user_id);
                  cstmt.registerOutParameter(5, Types.INTEGER); //x_return
                  cstmt.registerOutParameter(6, Types.VARCHAR); //x_message
                  cstmt.execute();
                  x_return = cstmt.getInt(5);
                  x_message = cstmt.getString(6);
                  cstmt.close();
                  boolean showSubError = true;
                  if (x_return > 1) {
                    TelnetSession telnetsessionX1 = (TelnetSession)ses;
                    Integer k1 =
                    telnetsessionX1.showPromptPage("Error!", x_message,
                                                            dialogPageButtons);
                        if (k1 == 0) {
                            return;
                        }
                  }
            
            ses.setStatusMessage("Yes");
            ses.clearAllApplicationScopeObjects();
            ses.setStatusMessage("Yes");
            pg.getmProcess().setNextPageName("|END_OF_TRANSACTION|");
            }
            catch (Exception exception){
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " exception " + exception);
            }
            
        }
        else if (k == 1) {
            ses.setStatusMessage("No");
            return;
        }
        else {
            return;
        }
        
        
    }

    /*************************************************************************
    *   NAME:     public void exitedCreateBin(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount

    **************************************************************************/
    
    protected void exitedCreateBin(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {
    try
            {
                String gMethod = "exitedCreateBin";
                String gCallPoint = "Start";
                String l_current_page = ses.getCurrentPageName();
                String l_subinventory = "";
                l_subinventory = pg.getmSubFld().getValue();
                //Added 5/12/2015
                    if (l_subinventory.equals(null) || l_subinventory.equals("")) {
                        l_subinventory = "General";
                    }
                FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallPoint).append(" ").append(gMethod).append(" ").append(gCallPoint).append(" l_subinventory ").append(l_subinventory).toString());
                ses.putSessionObject("sessPreviousPage", l_current_page);
                ses.putSessionObject("sessPreviousField", "XXWC.ITEM_RO");
                ses.putSessionObject("sessXXWC.ITEM", pg.getmItemReadOnly().getValue());
                ses.putSessionObject("sessInventoryItemId", pg.getmItemIdFld().getValue());
                ses.putSessionObject("sessXXWC.DESCRIPTION", pg.getmItemDescFld().getValue());
                ses.putSessionObject("sessXXWC.SUBINVENTORY", l_subinventory);
                //ses.putSessionObject("sessXXWC.SUBINVENTORY", pg.getmBinFld().getmSubinventory().toString());
                ses.putSessionObject("sessXXWC.LOC", "");
                ses.putSessionObject("sessXXWC.WC_PREFIX", "");
                FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallPoint).append(" ").append(gMethod).append(" ").append(gCallPoint).append(" sessPreviousPage ").append(ses.getObject("sessPreviousPage")).toString());
                FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallPoint).append(" ").append(gMethod).append(" ").append(gCallPoint).append(" sessPreviousField ").append(ses.getObject("sessPreviousField")).toString());
                FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallPoint).append(" ").append(gMethod).append(" ").append(gCallPoint).append(" sessXXWC.ITEM ").append(ses.getObject("sessXXWC.ITEM")).toString());
                FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallPoint).append(" ").append(gMethod).append(" ").append(gCallPoint).append(" sessInventoryItemId ").append(ses.getObject("sessInventoryItemId")).toString());
                FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallPoint).append(" ").append(gMethod).append(" ").append(gCallPoint).append(" sessXXWC.DESCRIPTION ").append(ses.getObject("sessXXWC.DESCRIPTION")).toString());
                FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallPoint).append(" ").append(gMethod).append(" ").append(gCallPoint).append(" sessXXWC.SUBINVENTORY ").append(ses.getObject("sessXXWC.SUBINVENTORY")).toString());
                FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallPoint).append(" ").append(gMethod).append(" ").append(gCallPoint).append(" sessXXWC.LOC ").append("").toString());
                FileLogger.getSystemLogger().trace((new StringBuilder()).append(gPackage).append(".").append(gCallPoint).append(" ").append(gMethod).append(" ").append(gCallPoint).append(" sessXXWC.WC_PREFIX ").append("").toString());
                pg.getmCreateBin().setNextPageName("xxwc.oracle.apps.inv.bins.server.XXWCBinAssignmentPage");
                ses.setNextFieldName("XXWC.LOC");
            }
            catch(Exception e)
            {
                UtilFns.error((new StringBuilder()).append("Error in validating Create Bin").append(e).toString());
                ses.putSessionObject("sessXXWC.ITEM", "");
                ses.putSessionObject("sessInventoryItemId", "");
                ses.putSessionObject("sessXXWC.LOCATION_ID", "");
                ses.putSessionObject("sessXXWC.DESCRIPTION", "");
                ses.putSessionObject("sessXXWC.SUBINVENTORY", "");
                ses.putSessionObject("sessXXWC.LOC", "");
                ses.putSessionObject("sessXXWC.WC_PREFIX", "");
            }
        }


    /*************************************************************************
    *   NAME:     public void getTotalCountQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount

    **************************************************************************/
    
    protected void getTotalCountQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {
            try
            {
                String gMethod = "getTotalCountQty";
                String gCallPoint = "Start";
                String x_return = "0";
                CallableStatement cstmt = null;
                Connection con = ses.getConnection();
                cstmt = con.prepareCall("{? = call XXWC_CC_MOBILE_PKG.GET_TOTAL_COUNT_QTY(?,?,?,?,?,?)");
                cstmt.registerOutParameter(1, Types.VARCHAR);
                cstmt.setString(2, pg.getmCycleCountHeaderFld().getValue());
                cstmt.setString(3, pg.getmCycleCountEntryId().getValue());
                cstmt.setString(4, (String)ses.getObject("ORGID"));
                cstmt.setString(5, pg.getmItemIdFld().getValue());
                cstmt.setString(6, pg.getmSubFld().getValue());
                cstmt.setString(7, pg.getmLotFld().getValue());
                cstmt.execute();
                x_return = cstmt.getString(1);
                cstmt.close();
                pg.getmTotalCountQuantityFld().setValue(x_return);
            }
            catch(Exception e)
            {
                UtilFns.error((new StringBuilder()).append("Error in validating getTotalCountQty").append(e).toString());

            }
        }


    /*************************************************************************
    *   NAME:     public void getOpenCountQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount

    **************************************************************************/
    
    protected Integer getOpenCountQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {
            String gMethod = "getOpenCountQty";
            String gCallPoint = "Start";
            int x_return = 0;
            
            try
            {
                CallableStatement cstmt = null;
                Connection con = ses.getConnection();
                cstmt = con.prepareCall("{? = call XXWC_CC_MOBILE_PKG.GET_OPEN_COUNT_QTY(?,?,?)");
                cstmt.registerOutParameter(1, Types.NUMERIC);
                cstmt.setString(2, pg.getmCycleCountHeaderFld().getValue());
                cstmt.setString(3, (String)ses.getObject("ORGID"));
                cstmt.setString(4, (String)ses.getObject("TXN.ENTRY_STATUS_CODE"));
                cstmt.execute();
                x_return = cstmt.getInt(1);
                cstmt.close();
            }
            catch(Exception e)
            {
                UtilFns.error((new StringBuilder()).append("Error in validating getOpenCountQty").append(e).toString());

            }
            
            return x_return;
        }
    
    /*************************************************************************
    *   NAME: public void enteredBins(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Clear For Field Listener Process to proces the Clear Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       01-JUN-2015   Lee Spitzer             Initial Version -
    **************************************************************************/


    public void enteredBins(MWAEvent mwaevent)
         throws AbortHandlerException, InterruptedHandlerException, DefaultOnlyHandlerException
     {
         pg.getmCycleCountHeaderFld().setHidden(true);
         ses.setRefreshScreen(true);
         
         try
         {
             //ses.putSessionObject("sessInventoryItemId", "");
             ses.putSessionObject("sessRestrictLocator", Integer.valueOf(1));
             ses.putSessionObject("sessHidePrefix", "Y");
             XXWCBinsLOV binsLOV = pg.getmLocFld();
             String paramType[] = {
                 "C", "N", "S", "N", "N", "S", "S", "S", "S", "S", "S"
             };
             String prompts[] = {
                 "WC_LOCATOR", "PREFIX", "LOCATOR_ID", "LOCATOR", "DESCRIPTION", "SUBINVENTORY"
             };
             boolean flag[] = {
                 true, false, false, false, false, false
             };
             String parameters[] = {
                 //0        1   2                      3   4                                                                5   6   7                 8                                                                             9                          0
                 " ", "ORGID", "", "sessRestrictLocator", "", "xxwc.oracle.apps.inv.count.server.XXWCCycleCountPage.XXWC.LOC", "", "", "sessHidePrefix", "xxwc.oracle.apps.inv.count.server.XXWCCycleCountPage.XXWC.CYCLECOUNT_HEADER", "TXN.ENTRY_STATUS_CODE"
             };
             binsLOV.setInputParameters(parameters);
             binsLOV.setlovStatement("XXWC_CC_MOBILE_PKG.GET_BIN_LOC");
             binsLOV.setInputParameterTypes(paramType);
             binsLOV.setSubfieldPrompts(prompts);
             binsLOV.setSubfieldDisplays(flag);
         }
         catch(Exception e)
         {
             UtilFns.error((new StringBuilder()).append("Error in calling enteredItem ").append(e).toString());
         }
     }
    

    /*************************************************************************
     *   NAME: public void exitedBins(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
              1.1       02-MAR-2016   Lee Spitzer             TMS Ticket 20160309-00091 - RF Mass Bin Locating Screen   
     **************************************************************************/

    public void exitedBins(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        String gMethod = "exitedBins";
        String gCallPoint = "Start";
        
        int l_count = getOpenCountQty(mwaevent, ses);
        
        pg.getmLocFld().getValue().toUpperCase(); //Added 02-MAR-2016 TMS Ticket 20160309-00091
        
        try {
            
            if (pg.getmLocFld().getValue().equals(null) || pg.getmLocFld().getValue().equals("") ) {
                //pg.getmCountQtyFld().setEditable(false);
                TelnetSession telnetsessionX1 = (TelnetSession)ses;
                Integer k1 =
                telnetsessionX1.showPromptPage("Error!", "You must enter a locator",
                                                        dialogPageButtons);
                    if (k1 == 0) {
                        ses.setNextFieldName("XXWC.LOC");
                    }
            }
            else {
                    
                    pg.getmItemReadOnly().setValue("");
                    pg.getmItemDescFld().setValue("");
                    pg.getmItemFld().clear();
                    pg.getmSubFld().setValue("");
                    pg.getmUOMFld().setValue("");
                    pg.getmTotalCountQuantityFld().setValue("");
                    pg.getmNext().setHidden(true);
                    pg.getmPrevious().setHidden(true);
                    
                    
                    try {
                        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " calling countQuery");
                        countQuery(mwaevent , ses);
                    }
                    catch (Exception exception) {
                        UtilFns.error("Error in calling exitedBins " + exception);
                    }
                
                }
            
        if (pg.getmLocFld().isEditable()){
            ses.putObject("sessXXWC.LOC", pg.getmLocFld().getValue());
        }
    }
    catch (Exception e) {
            UtilFns.error("Error in calling exitedBins" + e);
        }
    }
    
    /*************************************************************************
     *   NAME: public void exitedAddItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public void exitedAddItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        String gMethod = "exitedAddItem";
        String gCallPoint = "Start";
        
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
    
        pg.getmAddItem().setNextPageName("xxwc.oracle.apps.inv.count.server.XXWCCycleCountAddItemPage");
        
        ses.putSessionObject("sessXXWC.CYCLECOUNT_HEADER", pg.getmCycleCountHeaderFld().getValue());
    }

}