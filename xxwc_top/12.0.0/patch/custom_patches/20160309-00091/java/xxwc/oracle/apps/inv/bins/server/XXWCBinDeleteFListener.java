/*************************************************************************
   *   $Header XXWCBinDeleteFListener.java $
   *   Module Name: XXWCBinDeleteFListener
   *   
   *   Package: package xxwc.oracle.apps.inv.bins.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCBinDeletePage Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
        1.1       02-MAR-2016   Lee Spitzer             TMS Ticket ?20160309-00091 - RF Mass Bin Fixes
**************************************************************************/
package xxwc.oracle.apps.inv.bins.server;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.presentation.telnet.TelnetSession;
import oracle.jdbc.OraclePreparedStatement;
import oracle.sql.NUMBER;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.invinq.server.ItemOnhandQueryManager;

import xxwc.oracle.apps.inv.lov.server.XXWCBinsLOV;
import xxwc.oracle.apps.inv.lov.server.XXWCItemLOV;

/*************************************************************************
 *   NAME: XXWCBinDeleteFListener implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCBinDeleteFListener
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       25-JAN-2016   Lee Spitzer             Initial Version -
                                                          TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
 **************************************************************************/


public class XXWCBinDeleteFListener implements MWAFieldListener{
    XXWCBinDeletePage pg;
    Session ses;
    String dialogPageButtons[];

    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.bins.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.bins.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCBinDeleteFListener";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    private static String gCallFrom = "XXWCBinDeleteFListener";

    /*************************************************************************
     *   NAME: public XXWCBinDeleteFListener()
     *
     *   PURPOSE:   public method XXWCBinDeleteFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    public XXWCBinDeleteFListener() {
        $init$();
    }

    /*************************************************************************
     *   NAME:  public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException
     *
     *   PURPOSE:   field entered for XXWCBinDeleteFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/
    
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        pg = (XXWCBinDeletePage)ses.getCurrentPage();
        String s = UtilFns.fieldEnterSource(ses);
        /*Item*/
        
        if (s.equals("XXWC.ITEM")) {
            menuCancelPrompt(mwaevent, ses);
            enteredItem(mwaevent);
            return;
        }
        /*Sub*/
        if (s.equals("XXWC.SUB")) {
            menuCancelPrompt(mwaevent, ses);
            enteredSub(mwaevent);
            return;
        }
        /*Loc*/
        if (s.equals("XXWC.LOC")) {
            menuCancelPrompt(mwaevent, ses);
            enteredLocator(mwaevent);
            return;
        }
        /*Loc*/
        //if (s.equals("XXWC.WC_PREFIX")) {
        //    menuCancelPrompt(mwaevent, ses);
        //    enteredLocatorPrefix(mwaevent);
        //    return;
        //}
        
    }

    /*************************************************************************
     *   NAME:      public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   field exited for XXWCBinDeleteFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {
        String s = ((FieldBean)mwaevent.getSource()).getName();
        String vField = mwaevent.getAction();
        boolean flag = false;
        boolean flag1 = false;
        boolean flag3 = false;
        if (mwaevent.getAction().equals("MWA_SUBMIT")) {
            flag = true;
        } else if (mwaevent.getAction().equals("MWA_PREVIOUSFIELD")) {
            flag1 = true;
        } else if (mwaevent.getAction().equals("MWA_NEXTFIELD")) {
            flag3 = true;
        }
        /*Item*/
        if ((flag && s.equals("XXWC.ITEM")) || (flag3 && s.equals("XXWC.ITEM"))) {
            exitedItem(mwaevent, ses);
            return;
        }
        /*Sub*/
        if ((flag && s.equals("XXWC.SUB")) || (flag3 && s.equals("XXWC.SUB"))) {
            exitedSub(mwaevent, ses);
            return;
        }
        /*Loc*/
        if ((flag && s.equals("XXWC.LOC")) || (flag3 && s.equals("XXWC.LOC"))) {
            exitedLocator(mwaevent, ses);
            return;
        }
        //if ((flag && s.equals("XXWC.WC_PREFIX")) || (flag3 && s.equals("XXWC.WC_PREFIX"))){
        //    exitedLocatorPrefix(mwaevent, ses);
        //    return;
        //}
        /*Cancel*/
        if (flag && s.equals("XXWC.CANCEL")) {
            exitedCancel(mwaevent, ses);
            return;
        }
        /*Assign*/
        if (flag && s.equals("XXWC.DELETE")) {
            deleteItemLocatorTie(mwaevent, ses);
            clearFields(mwaevent, ses);
            return;
        }
        else {
            return;
        }
    }

    /*************************************************************************
     *   NAME:      private void $init$()
     *
     *   PURPOSE:   $init$ sets the diaglogButtons button for user prompt messages to OK
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
         1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    private void $init$() {
        dialogPageButtons = (new String[] { "OK" });
    }


    /*************************************************************************
     *   NAME: public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Item Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/


    public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {

        try {
            pg.getmSubFld().setHidden(true);
            XXWCItemLOV itemlov = pg.getmItemFld();
            itemlov.setValidateFromLOV(true);
            itemlov.setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_WC_ITEM_LOV");
            String paramType[] = { "C", "N", "S" };
            String parameters[] =
            { " ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCBinDeletePage.XXWC.ITEM" };
            itemlov.setInputParameterTypes(paramType);
            itemlov.setInputParameters(parameters);
        } catch (Exception e) {
            UtilFns.error("Error in calling enteredItem" + e);
        }
    }

    /*************************************************************************
     *   NAME: public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        if (pg.getmItemFld().getValue().equals(null) || pg.getmItemFld().getValue().equals("")) {
            return;
        }
        else {
            try {
                pg.getmItemFld().getValue().toUpperCase();
                pg.getmItemDescFld().setValue(pg.getmItemFld().getmItemDescription());
                pg.getmSubFld().setHidden(false);
                pg.getmSubFld().setValue("General");
                exitedSub(mwaevent, ses);
            } catch (Exception e) {
                UtilFns.error("Error in calling exitedItem" + e);
            }
        }
    }

    /*************************************************************************
    *   NAME: public void enteredSub(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Subinventory Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/

    public void enteredSub(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

      
       
       try {
           pg.getmLocFld().setHidden(true);
           pg.getmLocFldPreFix().setHidden(true);
           SubinventoryLOV sublov = pg.getmSubFld();
           sublov.setValidateFromLOV(true);
           sublov.setlovStatement("INV_UI_ITEM_SUB_LOC_LOVS.GET_VALID_SUBS");
           String paramType[] = { "C", "N", "S" };
           String parameters[] =
           { " ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCBinDeletePage.XXWC.SUB" };
           sublov.setInputParameterTypes(paramType);
           sublov.setInputParameters(parameters);
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredSub" + e);
       }
    }

    /*************************************************************************
    *   NAME: public void exitedSub(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/

    public void exitedSub(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
       
        if (pg.getmSubFld().getValue().equals(null) || pg.getmSubFld().getValue().equals("")) {
            return;
        }
        else {
               try {
                   Integer i = getLocatorControl(mwaevent, ses);
                   if (i == 1) {
                       pg.getmLocFld().setHidden(true);
                       pg.getmLocFld().clear();
                       pg.getmLocFld().setRequired(false);
                       pg.getmLocFldPreFix().setHidden(true);
                       pg.getmLocFldPreFix().setValue(null);
                       pg.getmLocFldPreFix().setRequired(false);
                          
                   } else {
                       pg.getmLocFld().setHidden(false);
                       pg.getmLocFld().setRequired(true);
                       pg.getmLocFldPreFix().setHidden(false);
                       pg.getmLocFldPreFix().setRequired(true);
                       ses.setNextFieldName("XXWC.LOC");
                   }
               } catch (Exception e) {
                   UtilFns.error("Error in calling exitedSub" + e);
               }
        }
    }

    /*************************************************************************
    *   NAME: public void getLocatorControl(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Determines if Subinventory has Locator Control turned on or off
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
          1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/

    private int getLocatorControl(MWAEvent mwavevent, Session session) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
       int i = 1;
       int j = 1;
       int k = 1;
       int x_count = 0;
       try {
           Long orgid = Long.parseLong((String)session.getObject("ORGID"));
           FileLogger.getSystemLogger().trace("orgid  " + orgid);
           FileLogger.getSystemLogger().trace("i  " + i);
           OrgParameters orgparameters = OrgParameters.getOrgParameters(session.getConnection(), orgid);
           i = orgparameters.getLocatorControlCode();
           FileLogger.getSystemLogger().trace("i  " + i);
           FileLogger.getSystemLogger().trace("j " + j);
           SubinventoryLOV sublov = pg.getmSubFld();
           if (pg.getmSubFld().getValue() != null | pg.getmSubFld().getValue() != "") {
               j = Integer.parseInt(sublov.getLocatorType());
           }
           FileLogger.getSystemLogger().trace("j " + j);
           XXWCItemLOV itemlov = pg.getmItemFld();
           FileLogger.getSystemLogger().trace("k " + k);
           if (pg.getmItemFld().getValue() != null | pg.getmItemFld().getValue() != "") {
               k = Integer.parseInt("2");
           }
           FileLogger.getSystemLogger().trace("k " + k);
           if (UtilFns.isTraceOn) {
               UtilFns.trace((new StringBuilder()).append("RCV: getLocatorControl orgLocControl:").append(i).append(":subLocControl:").append(j).append(":itemLocControl:").append(k).toString());
           }
           
       } catch (Exception e) {
           FileLogger.getSystemLogger().trace("error getLocatorControl " + e);
       }
        //Added for WC Branches
        try {
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            String p_organization_id = (String)ses.getObject("ORGID");
            String p_subinventory_code = (String)pg.getmSubFld().getValue();
            cstmt = con.prepareCall("{? = call XXWC_INV_BIN_MAINTENANCE_PKG.WC_BRANCH_LOCATOR_CONTROL(?,?)}");
            cstmt.setString(2, p_organization_id);
            cstmt.setString(3, p_subinventory_code);
            cstmt.registerOutParameter(1, Types.NUMERIC);
            cstmt.execute();
            x_count = cstmt.getInt(1);
            cstmt.close();
            FileLogger.getSystemLogger().trace("x_count " + x_count);
        }
        catch (Exception e) {
            UtilFns.error("Error in calling getLocatorControl" + e);
        }
        
        if (x_count > 0) {
            return 0;
        }
        else {
            return 1;
        }
    }

    /*************************************************************************
    *   NAME: public void enteredLocator(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Locator Field Listener Process to set the List of Values for the Locator Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/

    public void enteredLocator(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {

        try {
            ses.putSessionObject("sessInventoryItemId", pg.getmItemFld().getmInventoryItemId().toString());
            ses.putSessionObject("sessRestrictLocator", 1);
            ses.putSessionObject("sessHidePrefix","N");
            XXWCBinsLOV binsLOV = pg.getmLocFld();
            //                      0    1    2    3    4    5    6    7    8
            String paramType[] = { "C", "N", "S", "N", "N", "S", "S", "S", "S" };
            String prompts[] = { "WC_LOCATOR", "PREFIX", "LOCATOR_ID", "LOCATOR", "DESCRIPTION", "SUBINVENTORY_CODE" };
            boolean flag[] = {true, true, false, true, false, false};
            //                        0        1                                                              2 
            String parameters[] = { " ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCBinDeletePage.XXWC.SUB" ,"sessRestrictLocator", "sessInventoryItemId" , "xxwc.oracle.apps.inv.bins.server.XXWCBinDeletePage.XXWC.LOC", "", "", "sessHidePrefix" };
            //String parameters[] = { " ", "ORGID", "" ,"", "sessInventoryItemId" , "xxwc.oracle.apps.inv.lables.server.XXWCItemLabelPage.XXWC.BIN", "", "", "N" };
            binsLOV.setInputParameters(parameters);
            binsLOV.setlovStatement("XXWC_INV_BIN_MAINTENANCE_PKG.GET_WC_BIN_LOC");
            binsLOV.setInputParameterTypes(paramType);
            binsLOV.setSubfieldPrompts(prompts);
            binsLOV.setSubfieldDisplays(flag);
            
        } catch (Exception e) {
            UtilFns.error("Error in calling enteredItem " + e);
        }
    }

    /*************************************************************************
    *   NAME: public void exitedLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
            1.1       02-MAR-2016   Lee Spitzer             TMS Ticket 20160309-00091 - RF Mass Bin Fixes
    **************************************************************************/

    public void exitedLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
       
       
        String gMethod = "exitedLocator";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
        gCallPoint = "Upper and Trim Locator Field"; //added TMS Ticket 20160309-00091
        pg.getmLocFld().setValue(pg.getmLocFld().getValue().toUpperCase().trim()); //Added TMS Ticket 20160309-00091
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " locator field after triming and upper is " + pg.getmLocFld().getValue().toUpperCase().trim()); //added TMS Ticket 20160309-00091
          
       try {
           //If locator is null or blank then hide print and assign
           if ((pg.getmLocFld().getValue().equals(null)) || pg.getmLocFld().getValue().equals("")){
               gCallPoint = "Locator is null";
               FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
               pg.mDelete.setHidden(true);
               pg.getmLocFldPreFix().setHidden(true);
               return;
           } 
           else {
                gCallPoint = "Locator id populated from LOV";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                pg.getmLocFldPreFix().setValue(pg.getmLocFld().getmWCPreFix());
                pg.mDelete.setHidden(false); //Updated 5/13/2015
                ses.setNextFieldName("XXWC.DELETE"); //Updated 5/13/2015 to XXWC.DELETE
           }
        } catch (Exception e) {
           UtilFns.error("Error in calling exitedLocator" + e);
       }
    }

    /*************************************************************************
    *   NAME: public void enteredLocatorPrefix(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Locator Field Listener Process to set the List of Values for the Locator Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/

    public void enteredLocatorPrefix(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {
        
        String gMethod = "enteredLocatorPrefix";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
        
        
    }

    /*************************************************************************
    *   NAME: public void exitedLocatorPrefix(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/

    public void exitedLocatorPrefix(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
       try {
           if (pg.getmLocFldPreFix().getValue().equals(null)){
               pg.mDelete.setHidden(true);
           } else {
               
               String gMethod = "exitedLocatorPrefix";
               String gCallPoint = "Start";
               FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
               
               pg.getmLocFldPreFix().setValue(pg.getmLocFldPreFix().getValue().trim());
               if (pg.getmLocFldPreFix().getValue().equals(null)) {
                   return;
               } 
               else {
                   gCallPoint = "Calling XXWC_INV_BIN_MAINTENANCE_PKG.validate_loc_prefix_val ";
                   try {
                       CallableStatement cstmt = null;
                       Connection con = ses.getConnection();
                       cstmt = con.prepareCall("{call XXWC_INV_BIN_MAINTENANCE_PKG.validate_loc_prefix_val(?,?,?)}");
                       cstmt.setString(1, pg.getmLocFldPreFix().getValue());
                       cstmt.registerOutParameter(2, Types.INTEGER); //x_return
                       cstmt.registerOutParameter(3, Types.VARCHAR); //x_message
                       cstmt.execute();
                       Integer x_return = cstmt.getInt(2);
                       String  x_message = cstmt.getString(3);
                       cstmt.close();
                       boolean showSubError = true;
                       if (x_return > 0) {
                           TelnetSession telnetsessionX = (TelnetSession)ses;
                           Integer k =
                               telnetsessionX.showPromptPage("Error!", x_message,
                                                             dialogPageButtons);
                           if (k == 0) {
                               pg.getmLocFldPreFix().setValue(null);
                               ses.setNextFieldName("XXWC.WC_PREFIX");
                           } else {
                               return;
                           }
                       }
                       if (x_return == 0) {
                           pg.mDelete.setHidden(false);
                           ses.setNextFieldName("XXWC.DELETE");
                       }
                   }
                   catch (Exception e) {
                       UtilFns.error("Error in validating Quantity" + e);
                   }
               }
           }
       } catch (Exception e) {
           UtilFns.error("Error in calling exitedLocatorPrefix" + e);
       }
    }

    /*************************************************************************
    *   NAME: public void exitedClose(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/

    public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        
        try {
            String gMethod = "exitedCancel";
            String gCallPoint = "Start";
            String l_prompt = "";
            l_prompt = pg.getmMenu().getPrompt();
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " l_prompt " + l_prompt);
            if (l_prompt.equals("Menu")){
                XXWCBinDeletePage _tmp = pg;
                ses.setStatusMessage(pg.WMS_TXN_CANCEL);
                ses.clearAllApplicationScopeObjects();
                ses.setStatusMessage(XXWCBinDeletePage.WMS_TXN_CANCEL);
                pg.getmMenu().setNextPageName("|END_OF_TRANSACTION|");
            }
            else if (l_prompt.equals("Cancel")) {
                clearFields(mwaevent, ses);
            }
            else {
                return;
            }
        } catch (Exception e) {
            UtilFns.error("Error in calling exitedCancel " + e);
        }

    }
    
    /*************************************************************************
    *   NAME:     public void processItemLocatorTie(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                              TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/
    
    public void processItemLocatorTie(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String gMethod = "processItemLocatorTie";
        String gCallPoint = "Start";
        CallableStatement cstmt = null;
        Connection con = ses.getConnection();
        //Need to validate the locator_id exists, if it was populated by the LOV it exists
        String p_organization_id = (String)ses.getObject("ORGID");
        String p_inventory_item_id = (String)pg.getmItemFld().getmInventoryItemId();
        String p_subinventory_code = (String)pg.getmSubFld().getValue();
        String p_locator  = (String)pg.getmLocFld().getValue();
        String p_locator_prefix = pg.getmLocFldPreFix().getValue();
        Integer x_return;
        String x_message; 
        if (pg.getmLocFld().getmLocationId().equals(null) || pg.getmLocFld().getmLocationId().equals("")){
            //Need to get the locator id from the Locator Field and Locator Prefix; if that doesn't exist then need to pop up a message to create a new location
            try {
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                //                                                                              1 2 3
                Integer x_locator_id = -1;
                cstmt = con.prepareCall("{? = call XXWC_INV_BIN_MAINTENANCE_PKG.GET_BIN_VAL_LOC(?,?,?)}");
                cstmt.registerOutParameter(1, Types.NUMERIC);
                cstmt.setString(2, p_organization_id);
                cstmt.setString(3, p_locator);
                cstmt.setString(4, p_locator_prefix);
                cstmt.execute();
                x_locator_id = cstmt.getInt(1);
                cstmt.close();
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " x_locator_id " + x_locator_id);
                //if there isn't a count of a valid locator then raise message the locator entered doesn't exists
                if (x_locator_id == -1) {
                    gCallPoint = "x_return is -1";
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    int k = telnetsessionX.showPromptPage("Warning", "Locator " + pg.getmLocFldPreFix().getValue() + "-" + pg.getmLocFld().getValue() + " does not exist.  Would you like to create it and assign it to item " + pg.getmItemFld().getValue() + "?", new String [] {"Yes", "No"});
                    if (k == 0) {
                        gCallPoint = "Starting XXWC_INV_BIN_MAINTENANCE_PKG.CREATE_NEW_LOCATOR";
                        try {
                            con = ses.getConnection(); //added 5/12/2015
                            cstmt = con.prepareCall("{call XXWC_INV_BIN_MAINTENANCE_PKG.CREATE_NEW_LOCATOR(?,?,?,?,?,?)}");
                            cstmt.setString(1, p_organization_id);
                            cstmt.setString(2, p_subinventory_code);
                            cstmt.setString(3, p_locator);
                            cstmt.setString(4, p_locator_prefix);
                            cstmt.registerOutParameter(5, Types.NUMERIC);
                            cstmt.registerOutParameter(6, Types.VARCHAR);
                            cstmt.execute();
                            x_locator_id = cstmt.getInt(5);
                            x_message = cstmt.getString(6);
                            cstmt.close();
                            if (x_locator_id == -1) {
                                TelnetSession telnetsessionY = (TelnetSession)ses;
                                int l = telnetsessionY.showPromptPage("Error!", "Error creating locator " + p_locator_prefix + "-" + p_locator, dialogPageButtons);
                                if (l == 0) {
                                    clearFields(mwaevent, ses);
                                    return;
                                }
                            }
                            else {
                                pg.getmLocFld().setmLocationId(Integer.toString(x_locator_id));
                                pg.getmLocFld().setmWCPreFix(p_locator_prefix);
                                pg.getmLocFld().setmWCLocator(p_locator);
                                pg.getmLocFld().setmConcatedSegments(p_locator_prefix + "-" + p_locator);
                            }
                            
                        }
                        catch (Exception e) {
                            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " error getting XXWC_INV_BIN_MAINTENANCE_PKG.CREATE_NEW_LOCATOR " + e);
                            
                        }
                    } 
                    else if (k == 1){
                        clearFields(mwaevent, ses);
                        return;
                    }
                    else {
                        return;
                    }
                }
        
                pg.getmLocFld().setmLocationId(Integer.toString(x_locator_id));
                pg.getmLocFld().setmWCPreFix(p_locator_prefix);
                pg.getmLocFld().setmWCLocator(p_locator);
                pg.getmLocFld().setmConcatedSegments(p_locator_prefix + "-" + p_locator);
            }
            catch (Exception e) {
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " error " + e);
            }
        }
        
        //Resetting x_return and x_message values;
        x_return = 0;
        x_message = "";
        String p_locator_id = (String)pg.getmLocFld().getmLocationId();
                
        //if all test validate, then create item locator tie
        try {
            con = ses.getConnection(); //added 5/12/2015
            //                                                                                   1 2 3 4 5 6
            cstmt = con.prepareCall("{call XXWC_INV_BIN_MAINTENANCE_PKG.PROCESS_ITEM_LOCATOR_TIE(?,?,?,?,?,?)}");
            cstmt.setString(1, p_organization_id);
            cstmt.setString(2, p_inventory_item_id);
            cstmt.setString(3, p_subinventory_code);
            cstmt.setString(4, p_locator_id);
            cstmt.registerOutParameter(5, Types.NUMERIC);
            cstmt.registerOutParameter(6, Types.VARCHAR);
            cstmt.execute();
            x_return = cstmt.getInt(5);
            x_message = cstmt.getString(6);
            cstmt.close();
            if (x_return == 0) {
                ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_SUCCESS).append(" (").append(pg.getmItemFld().getValue() + " | " + pg.getmLocFld().getmConcatedSegments() + " " + x_message).append(")").toString());
                return;
            }
            if (x_return != 0) {
                TelnetSession telnetsessionX = (TelnetSession)ses;
                int k = telnetsessionX.showPromptPage("Error!", x_message, dialogPageButtons);
                if (k == 0) {
                    ses.setRefreshScreen(true);
                } else {
                    return;
                }
            }
        } catch (Exception e) {
            ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_ERROR).append(" (").append(e).append(")").toString());
            UtilFns.error((new StringBuilder()).append("Error in processItemLocatorTie: error PROCESS_ITEM_LOCATOR_TIE - ").append(e).toString());
        }
    }


    /*************************************************************************
    *   NAME:     public void clearFields(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/
    
    public void clearFields(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                   InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        String gMethod = "clearFields";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
        pg.mItemFld.setValue(null);
        pg.mItemDescFld.setValue(null);
        pg.mSubFld.setValue(null);
        pg.mSubFld.setHidden(true);
        pg.mLocFld.setValue(null);
        pg.mLocFld.setHidden(true);
        pg.mLocFldPreFix.setValue(null);
        pg.mLocFldPreFix.setHidden(true);
        pg.mDelete.setHidden(true);
        ses.setNextFieldName("XXWC.ITEM");
        pg.getmLocFld().clear();
        pg.getmLocFldPreFix().setValue(null);
    }
    
    

        /*************************************************************************
        *   NAME:     public void menuCancelPrompt(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                     InterruptedHandlerException,
                                                                                     DefaultOnlyHandlerException
        *
        *   PURPOSE:  Update mMenu label from Menu to Cancel.  
        *
        *   REVISIONS:
        *   Ver        Date        Author                     Description
        *   ---------  ----------  ---------------         -------------------------
             1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
        **************************************************************************/
   
         public void menuCancelPrompt(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
            String gMethod = "menuCancelPrompt";
            String gCallPoint = "Start";
            String s = UtilFns.fieldEnterSource(ses);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " current_field " + s);
            if (s.equals("XXWC.ITEM")) {
                pg.getmMenu().setPrompt("Menu");
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " set prompt to Menu");
                
            }
            else {
                pg.getmMenu().setPrompt("Cancel");
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " set prompt to Cancel");
            }
                                
        }
        
    /*************************************************************************
    *   NAME:     public void deleteItemLocatorTie(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                              TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/
    
    public void deleteItemLocatorTie(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        TelnetSession telnetsession = (TelnetSession)ses;
        Integer l =
            telnetsession.showPromptPage("Warning!", "Are you sure you want to delete bin assignment Item " + pg.getmItemFld().getValue() + " subinventory " + pg.getmSubFld().getValue() + " Locator " + pg.getmLocFld().getmConcatedSegments() + "?", (new String[] { "Delete Bin Assignment", "Back"}));

        
        if (l == 0) {
         
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            String p_organization_id = (String)ses.getObject("ORGID");
            String p_inventory_item_id = (String)pg.getmItemFld().getmInventoryItemId();
            String p_subinventory_code = (String)pg.getmSubFld().getValue();
            String p_locator_id = (String)pg.getmLocFld().getmLocationId();
            Integer x_return;
            String x_message;
            try {
                //                                                                                  1 2 3 4 5 6
                cstmt = con.prepareCall("{call XXWC_INV_BIN_MAINTENANCE_PKG.DELETE_ITEM_LOCATOR_TIE(?,?,?,?,?,?)}");
                cstmt.setString(1, p_organization_id);
                cstmt.setString(2, p_inventory_item_id);
                cstmt.setString(3, p_subinventory_code);
                cstmt.setString(4, p_locator_id);
                cstmt.registerOutParameter(5, Types.NUMERIC);
                cstmt.registerOutParameter(6, Types.VARCHAR);
                cstmt.execute();
                x_return = cstmt.getInt(5);
                x_message = cstmt.getString(6);
                cstmt.close();
                if (x_return == 0) {
                    ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_SUCCESS).append(" (").append(pg.getmItemFld().getValue() + " | " + pg.getmLocFld().getmConcatedSegments() + " " + x_message).append(")").toString());
                    cascadeBinAssignmentsDelete(mwaevent, ses, p_locator_id);
                    return;
                }
                if (x_return != 0) {
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    int k = telnetsessionX.showPromptPage("Error!", x_message, dialogPageButtons);
                    if (k == 0) {
                        ses.setRefreshScreen(true);
                    } else {
                        return;
                    }
                }
            } catch (Exception e) {
                ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_ERROR).append(" (").append(e).append(")").toString());
                UtilFns.error((new StringBuilder()).append("Error in deleteItemLocatorTie: error - ").append(e).toString());
            }
        }
        else if (l == 1) {
            clearFields(mwaevent, ses);
        }
        else {
        
            return;
        }
    }

    /*************************************************************************
    *   NAME:     public void cascadeBinAssignments(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                              TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/
    
    public void cascadeBinAssignmentsDelete(MWAEvent mwaevent, Session ses, String p_locator_id) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String gMethod = "cascadeBinAssignmentsDelete";
        String gCallPoint = "Start";
        CallableStatement cstmt = null;
        Connection con = ses.getConnection();
        String p_organization_id = (String)ses.getObject("ORGID");
        String p_inventory_item_id = (String)pg.getmItemFld().getmInventoryItemId();
        String p_subinventory_code = (String)pg.getmSubFld().getValue();
        Integer x_return;
        String x_message;
        try {
            //                                                                                         1 2 3 4 5 6 
            cstmt = con.prepareCall("{call XXWC_INV_BIN_MAINTENANCE_PKG.CASCADE_BIN_ASSIGNMENTS_DELETE(?,?,?,?,?,?)}");
            cstmt.setString(1, p_organization_id);
            cstmt.setString(2, p_inventory_item_id);
            cstmt.setString(3, p_subinventory_code);
            cstmt.setString(4, p_locator_id);
            cstmt.registerOutParameter(5, Types.NUMERIC);
            cstmt.registerOutParameter(6, Types.VARCHAR);
            cstmt.execute();
            x_return = cstmt.getInt(5);
            x_message = cstmt.getString(6);
            cstmt.close();
            if (x_return == 0) {
                ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_SUCCESS).append(" (").append(pg.getmSubFld().getValue() + " | " + pg.getmLocFldPreFix().getValue() + "-" + pg.getmLocFld().getValue() + " " + x_message).append(")").toString());            }
            if (x_return != 0) {
                TelnetSession telnetsessionX = (TelnetSession)ses;
                int k = telnetsessionX.showPromptPage("Error!", x_message, dialogPageButtons);
                if (k == 0) {
                    ses.setRefreshScreen(true);
                } else {
                    return;
                }
            }
        } catch (Exception e) {
            ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_ERROR).append(" (").append(e).append(")").toString());
            UtilFns.error((new StringBuilder()).append("Error in createLocator: error - ").append(e).toString());
        }
    }
    
   
}
