/*************************************************************************
   *   $Header XXWCItemLabelFListener.java $
   *   Module Name: XXWCItemLabelFListener
   *   
   *   Package: package xxwc.oracle.apps.inv.labels.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCItemLabelFListener Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00152 - RF Receiving
        1.1       02-MAR-2016   Lee Spitzer             TMS Ticket 20160309-00091 - RF Mass Bin Locating Screen   
 
**************************************************************************/
 package xxwc.oracle.apps.inv.labels.server;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.presentation.telnet.TelnetSession;
import oracle.jdbc.OraclePreparedStatement;
import oracle.sql.NUMBER;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.rcv.server.RcptGenPage;
import oracle.apps.inv.rcv.server.RcvTxnPage;

import xxwc.oracle.apps.inv.lov.server.*;


/*************************************************************************
 *   NAME: XXWCItemLabelFListener implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCItemLabelFListener
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00152 - RF Receiving
 **************************************************************************/


public class XXWCItemLabelFListener implements MWAFieldListener{
    XXWCItemLabelPage pg;
    Session ses;
    String dialogPageButtons[];
    Integer g_return = -1;
    String g_message = "";

    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.labels.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.labels.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCItemLabelFListener";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private static String gCallFrom = "XXWCItemLabelFListener";

    /*************************************************************************
     *   NAME: public XXWCItemLabelFListener()
     *
     *   PURPOSE:   public method XXWCItemLabelFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public XXWCItemLabelFListener() {
        $init$();
    }

    /*************************************************************************
     *   NAME:  public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException
     *
     *   PURPOSE:   field entered for XXWCItemLabelFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/
    
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        pg = (XXWCItemLabelPage)ses.getCurrentPage();
        String s = UtilFns.fieldEnterSource(ses);
        /*Shelf Life*/
        if (s.equals("XXWC.ITEM")) {
            enteredItem(mwaevent);
            return;
        }
        /*UBD*/
        if (s.equals("XXWC.BIN")) {
            enteredBins(mwaevent);
            return;
        }
        /*Copies*/
        if (s.equals("XXWC.PRICE")) {
            enteredPrice(mwaevent);
            return;
        }
        /*UBD Status*/
        if (s.equals("XXWC.ITEM_STATUS")) {
            enteredItemStatus(mwaevent);
            return;
        }
        /*UBD Status*/
        if (s.equals("XXWC.PRINT_LOCATION")) {
            enteredPrintLocation(mwaevent);
            return;
        }
        /*UBD Status*/
        if (s.equals("XXWC.SHELF_PRICE")) {
            enteredShelfPrice(mwaevent);
            return;
        }
        
    }

    /*************************************************************************
     *   NAME:      public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   field exited for XXWCItemLabelFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 - RF Receiving
            1.1       02-MAR-2016   Lee Spitzer             TMS Ticket 20160309-00091 - RF Mass Bin Locating Screen   
     
     **************************************************************************/

    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {
        String s = ((FieldBean)mwaevent.getSource()).getName();
        String vField = mwaevent.getAction();
        boolean flag = false;
        boolean flag1 = false;
        boolean flag3 = false;
        if (mwaevent.getAction().equals("MWA_SUBMIT")) {
            flag = true;
        } else if (mwaevent.getAction().equals("MWA_PREVIOUSFIELD")) {
            flag1 = true;
        } else if (mwaevent.getAction().equals("MWA_NEXTFIELD")) {
            flag3 = true;
        }
        /*Shelf Life*/
        if (s.equals("XXWC.ITEM")) {
            exitedItem(mwaevent, ses);
            return;
        }
        /*UBD*/
        if (s.equals("XXWC.BIN")) {
            exitedBin(mwaevent, ses); //Added TMS Ticket 20160309-00091
            return;
        }
        /*Shelf Life*/
        if (s.equals("XXWC.PRICE")) {
            return;
        }
        /*UBD*/
        if (s.equals("XXWC.ITEM_STATUS")) {
            exitedItemStatus(mwaevent, ses);
            return;
        }
        /*Shelf Life*/
        if (s.equals("XXWC.PRINT_LOCATION")) {
            return;
        }
        /*Shelf Life*/
        if (s.equals("XXWC.SHELF_PRICE")) {
            return;
        }
        /*Copies*/
        if (s.equals("XXWC.COPIES")) {
            exitedQty(mwaevent, ses);
            return;
        }
        /*Cancel*/
        if (flag && s.equals("INV.CANCEL")) {
            exitedCancel(mwaevent, ses);
            return;
        }
        /*SaveNext*/
        if (flag && s.equals("INV.SAVENEXT")) {
            execPrintItemLabel(mwaevent, ses);
            ses.setNextFieldName("XXWC.ITEM");
            return;
        }
        /*Done*/
        if (flag && s.equals("INV.DONE")) {
            execPrintItemLabel(mwaevent, ses);
            exitedDone(mwaevent, ses);
            return;
        }
        else {
            return;
        }
    }

    /*************************************************************************
     *   NAME:      private void $init$()
     *
     *   PURPOSE:   $init$ sets the diaglogButtons button for user prompt messages to OK
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private void $init$() {
        dialogPageButtons = (new String[] { "OK" });
    }


    
    /*************************************************************************
    *   NAME: public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

        
       try {
           XXWCItemLOV itemLOV = pg.getmItemFld();
           itemLOV.setValidateFromLOV(true);
           itemLOV.setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_WC_ITEM_LOV");
           //                      0    1    2    
           String paramType[] = { "C", "N", "S"};
           //                                     0       1       2      3              4                   5                      6                  7 
           String prompts[] = { "INVENTORY_ITEM_ID" , "ITEM", "UOM", "DESCRIPTION", "LOT_CONTROL_CODE", "SERIAL_CONTROL_CODE", "SHELF_LIFE_DAYS", "QTY"};
           //                    0     1     2     3     4     5       6    7     
           boolean flag[] = {false, true, true, true, false, false, true, true};
           //                        0        1                                                              2 
           String parameters[] = { " ", "ORGID", "xxwc.oracle.apps.inv.labels.server.XXWCItemLabelPage.XXWC.ITEM"};
           itemLOV.setInputParameterTypes(paramType);
           itemLOV.setInputParameters(parameters);
           itemLOV.setSubfieldPrompts(prompts);
           itemLOV.setSubfieldDisplays(flag);
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredItem " + e);
       }
    }


    /*************************************************************************
     *   NAME: public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
              1.0       21-OCT-2014   Lee Spitzer             Initial Version -
     **************************************************************************/

    public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        try {
            pg.getmItemFld().getValue().toUpperCase();
        } catch (Exception e) {
            UtilFns.error("Error in calling exitedItem" + e);
        }
    }
 

    /*************************************************************************
    *   NAME: public void enteredBins(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public void enteredBins(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

        
       try {
           ses.putSessionObject("sessInventoryItemId", pg.getmItemFld().getmInventoryItemId());
           ses.putSessionObject("sessRestrictLocator", 1);
           ses.putSessionObject("sessHidePrefix","N");
           XXWCBinsLOV binsLOV = pg.getmBinFld();
           //                      0    1    2    3    4    5    6    7    8
           String paramType[] = { "C", "N", "S", "N", "N", "S", "S", "S", "S" };
           String prompts[] = { "WC_LOCATOR", "PREFIX", "LOCATOR_ID", "LOCATOR", "DESCRIPTION", "SUBINVENTORY" };
           boolean flag[] = {false, false, false, true, false, false};
           //                        0        1   2                      3                      4                                                                 5   6   7                8 
           String parameters[] = { " ", "ORGID", "" ,"sessRestrictLocator", "sessInventoryItemId" , "xxwc.oracle.apps.inv.labels.server.XXWCItemLabelPage.XXWC.BIN", "", "", "sessHidePrefix" };
           //String parameters[] = { " ", "ORGID", "" ,"", "sessInventoryItemId" , "xxwc.oracle.apps.inv.lables.server.XXWCItemLabelPage.XXWC.BIN", "", "", "N" };
           binsLOV.setInputParameters(parameters);
           binsLOV.setlovStatement("XXWC_LABEL_PRINTERS_PKG.GET_BIN_LOC");
           binsLOV.setInputParameterTypes(paramType);
           binsLOV.setSubfieldPrompts(prompts);
           binsLOV.setSubfieldDisplays(flag);  
           
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredItem " + e);
       }
    }

    /*************************************************************************
     *   NAME: public void exitedBin(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.1       02-MAR-2016   Lee Spitzer             TMS Ticket 20160309-00091 - RF Mass Bin Locating Screen   
     **************************************************************************/

    public void exitedBin(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        try {
            pg.getmBinFld().getValue().toUpperCase();
        } catch (Exception e) {
            UtilFns.error("Error in calling exitedBin" + e);
        }
    }
    


    /*************************************************************************
    *   NAME: public void enteredPrice(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public void enteredPrice(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

        
       try {
           XXWCYesNoLOV yesNoLOV = pg.getmPriceFld();
           yesNoLOV.setValidateFromLOV(true);
           yesNoLOV.setlovStatement("XXWC_LABEL_PRINTERS_PKG.GET_YES_NO_LOV");
           
           String paramType[] = { "C", "S"};
           String prompts[] = { "LOOKUP_CODE" , "MEANING", "DESCRIPTION"};
           boolean flag[] = {false, false, true};
           String parameters[] = { " ", "xxwc.oracle.apps.inv.labels.server.XXWCItemLabelPage.XXWC.PRICE"};
           yesNoLOV.setInputParameters(parameters);
           yesNoLOV.setInputParameterTypes(paramType);
           yesNoLOV.setSubfieldPrompts(prompts);
           yesNoLOV.setSubfieldDisplays(flag);      

       } catch (Exception e) {
           UtilFns.error("Error in calling enteredItem " + e);
       }
    }
    


    /*************************************************************************
    *   NAME: public void enteredPrintLocation(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public void enteredPrintLocation(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

        
       try {
           XXWCYesNoLOV yesNoLOV = pg.getmPrintLocationFld();
           yesNoLOV.setValidateFromLOV(true);
           yesNoLOV.setlovStatement("XXWC_LABEL_PRINTERS_PKG.GET_YES_NO_LOV");
           
           String paramType[] = { "C", "S"};
           String prompts[] = { "LOOKUP_CODE" , "MEANING", "DESCRIPTION"};
           boolean flag[] = {false, false, true};
           String parameters[] = { " ", "xxwc.oracle.apps.inv.labels.server.XXWCItemLabelPage.XXWC.PRINT_LOCATION"};
           yesNoLOV.setInputParameters(parameters);
           yesNoLOV.setInputParameterTypes(paramType);
           yesNoLOV.setSubfieldPrompts(prompts);
           yesNoLOV.setSubfieldDisplays(flag);      

       } catch (Exception e) {
           UtilFns.error("Error in calling enteredPrintLocation " + e);
       }
    }
    

    /*************************************************************************
    *   NAME: public void enteredShelfPrice(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public void enteredShelfPrice(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

        
       try {
           XXWCYesNoLOV yesNoLOV = pg.getmShelfPricingFld();
           yesNoLOV.setValidateFromLOV(true);
           yesNoLOV.setlovStatement("XXWC_LABEL_PRINTERS_PKG.GET_YES_NO_LOV");
           
           String paramType[] = { "C", "S"};
           String prompts[] = { "LOOKUP_CODE" , "MEANING", "DESCRIPTION"};
           boolean flag[] = {false, false, true};
           String parameters[] = { " ", "xxwc.oracle.apps.inv.labels.server.XXWCItemLabelPage.XXWC.SHELF_PRICE"};
           yesNoLOV.setInputParameters(parameters);
           yesNoLOV.setInputParameterTypes(paramType);
           yesNoLOV.setSubfieldPrompts(prompts);
           yesNoLOV.setSubfieldDisplays(flag);      

       } catch (Exception e) {
           UtilFns.error("Error in calling enteredShelfPrice " + e);
       }
    }
    

    /*************************************************************************
    *   NAME: public void enteredPrinter(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Subinventory Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public void enteredItemStatus(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

         
       try {
           XXWCItemLabelStatusFlagLOV itemStatusLOV = pg.getmItemLabelStatusFlag();
           itemStatusLOV.setValidateFromLOV(true);
           itemStatusLOV.setlovStatement("XXWC_LABEL_PRINTERS_PKG.GET_ITEM_LBL_STATUS_LOV");
           String paramType[] = { "C", "S" };
           String parameters[] =
           { " ", "xxwc.oracle.apps.inv.labels.server.XXWCItemLabelPage.XXWC.ITEM_STATUS" };
           itemStatusLOV.setInputParameterTypes(paramType);
           itemStatusLOV.setInputParameters(parameters);
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredPrinter" + e);
       }
    }


    /*************************************************************************
    *   NAME: public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        try {
            XXWCItemLabelPage _tmp = pg;
            String sessPreviousPage = "";
            String sessPreviousField = "";
            try {
                    sessPreviousPage = (String)ses.getObject("sessPreviousPage").toString();
                    sessPreviousField = (String)ses.getObject("sessPreviousField").toString();
            }
            catch (Exception e) {
                    sessPreviousPage = "";
                    sessPreviousField = "";
            }
            
            if (sessPreviousPage.equals(null) || sessPreviousPage.equals("")){
                ses.setStatusMessage(pg.WMS_TXN_CANCEL);
                ses.clearAllApplicationScopeObjects();
                ses.setStatusMessage(XXWCItemLabelPage.WMS_TXN_CANCEL);
                pg.getmCancel().setNextPageName("|END_OF_TRANSACTION|");
            }
            else {
                ses.setStatusMessage(pg.WMS_TXN_CANCEL);
                ses.clearAllApplicationScopeObjects();
                ses.setStatusMessage(XXWCItemLabelPage.WMS_TXN_CANCEL);
                pg.getmCancel().setNextPageName(sessPreviousPage);
                ses.setNextFieldName(sessPreviousField); 
                //Added 5/2/2015
                ses.putSessionObject("sessPreviousPage", ""); //added 5/12/2015
                ses.putSessionObject("sessPreviousField", "");
                ses.putSessionObject("sessXXWC.ITEM", "");
                ses.putSessionObject("sessXXWC.BIN", "");
                ses.putSessionObject("sessXXWC.LOCATION_ID", "");
                ses.putSessionObject("sessXXWC.PRICE", "");
                ses.putSessionObject("sessXXWC.ITEM_STATUS", "");
                ses.putSessionObject("sessXXWC.PRINT_LOCATION", "");
                ses.putSessionObject("sessXXWC.SHELF_PRICE", "");
                ses.putSessionObject("sessXXWC.COPIES", "");
                
            }
        } catch (Exception e) {
            UtilFns.error("Error in calling exitedCancel " + e);
        }

    }
    
    
    /*************************************************************************
    *   NAME:     public void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving

    **************************************************************************/
    
    protected void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {
        try {
                XXWCItemLabelPage _tmp = pg;
                ses.setStatusMessage(pg.WMS_TXN_SUCCESS);
                pg.getmItemFld().setValue("");
                pg.getmBinFld().setValue("");
                pg.getmPriceFld().setValue("");
                pg.getmItemLabelStatusFlag().setValue("");
                pg.getmPrintLocationFld().setValue("");
                pg.getmShelfPricingFld().setValue("");
                pg.getmCopies().setValue("");
                String sessPreviousPage = "";
                String sessPreviousField = "";
                try {
                        sessPreviousPage = (String)ses.getObject("sessPreviousPage").toString();
                        sessPreviousField = (String)ses.getObject("sessPreviousField").toString();
                }
                catch (Exception e) {
                        sessPreviousPage = "";
                        sessPreviousField = "";
                }
                
                if (sessPreviousPage.equals(null) || sessPreviousPage.equals("")){
                    pg.getmDone().setNextPageName("|END_OF_TRANSACTION|");
                }
                else {
                    ses.clearAllApplicationScopeObjects();
                    pg.getmDone().setNextPageName(sessPreviousPage);
                    ses.setNextFieldName(sessPreviousField);
                    //Added 5/2/2015
                    ses.putSessionObject("sessPreviousPage", "");
                    ses.putSessionObject("sessPreviousField", "");
                    ses.putSessionObject("sessXXWC.ITEM", "");
                    ses.putSessionObject("sessXXWC.BIN", "");
                    ses.putSessionObject("sessXXWC.LOCATION_ID", "");
                    ses.putSessionObject("sessXXWC.PRICE", "");
                    ses.putSessionObject("sessXXWC.ITEM_STATUS", "");
                    ses.putSessionObject("sessXXWC.PRINT_LOCATION", "");
                    ses.putSessionObject("sessXXWC.SHELF_PRICE", "");
                    ses.putSessionObject("sessXXWC.COPIES", "");
                }
            } 
        catch (Exception e) {
            UtilFns.error("Error in calling exitedDone " + e);
        }
    
    }
    

    /*************************************************************************
        *   NAME: public void execPrintItemLabel(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                      InterruptedHandlerException,
                                                                      DefaultOnlyHandlerException 
    
         *   PURPOSE: Retrieves page values and executes the print material label API, xxwc_mwa_routines_pkg.material_label
         *    
         *   In: MWAEvent mwaevent
         *    
         *    
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
              1.0       06-FEB-2015   Lee Spitzer             Initial Version - 
    **************************************************************************/
      
    public String execPrintItemLabel(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                      InterruptedHandlerException,
                                                                      DefaultOnlyHandlerException 
    { 
        String gMethod = "execPrintItemLabel";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint );
        String p_organization_id = "";
        String p_inventory_item_id = "";
        String P_INV_ENABLE_BIN_LOC = "";
        String P_BIN_LOC_SELECTED = "";
        String P_ENABLE_BIN_LOC_RANGE = "";
        String P_BIN_LOC_LOW = "";
        String P_BIN_LOC_HIGH = "";
        String P_PRICE = "";
        String P_FLAG = "";
        String P_PRINT_LOCATORS = "";
        String P_SHELF_PRICING = "";
        String P_COPIES = "";
        Integer x_return = null;
        String x_message = "";
                
        gCallPoint = "Getting values from page";   
        
        p_organization_id = (String)ses.getObject("ORGID");
        p_inventory_item_id = pg.getmItemFld().getmInventoryItemId();
        P_INV_ENABLE_BIN_LOC = "Y";
        P_BIN_LOC_SELECTED = pg.getmBinFld().getmConcatedSegments();
        P_ENABLE_BIN_LOC_RANGE = "";
        P_BIN_LOC_LOW = "";
        P_BIN_LOC_HIGH = "";
        P_FLAG = pg.getmItemLabelStatusFlag().getmFlexValue();
        
        //if (pg.getmPriceFld().getmMeaning().substring(0, 1).toUpperCase().equals("") || pg.getmPriceFld().getmMeaning().substring(0, 1).toUpperCase().equals(null)){
        //}
        //else {
        
        //}
         
        //if (pg.getmPrintLocationFld().getmMeaning().substring(0, 1).toUpperCase().equals("") || pg.getmPrintLocationFld().getmMeaning().substring(0, 1).toUpperCase().equals(null)) {
        //    P_PRINT_LOCATORS = "";
        //} else {
        //    P_PRINT_LOCATORS = pg.getmPrintLocationFld().getmMeaning().substring(0, 1).toUpperCase();
        //}
        
        //if (pg.getmShelfPricingFld().getmMeaning().equals("") || pg.getmShelfPricingFld().getmMeaning().equals(null)){
        //    P_SHELF_PRICING = "";
        //}
        //else {
        //}
        
        P_PRICE = pg.getmPriceFld().getmMeaning();
        P_PRINT_LOCATORS = pg.getmPrintLocationFld().getmMeaning();
        P_SHELF_PRICING = pg.getmShelfPricingFld().getmMeaning();
        
        P_COPIES = pg.getmCopies().getValue();
        
        
        try {
            gCallPoint = "Calling XXWC_LABEL_PRINTERS_PKG.PROCESS_ITEM_LABEL_CCR";
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            //                                                                            1 2 3 4 5 6 7 8 9 0 1 2 3 4       
            cstmt = con.prepareCall("{call XXWC_LABEL_PRINTERS_PKG.PROCESS_ITEM_LABEL_CCR(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            cstmt.setString(1, p_organization_id); //p_organization_id
            cstmt.setString(2, p_inventory_item_id); //p_inventory_item_id
            cstmt.setString(3, P_INV_ENABLE_BIN_LOC); //P_INV_ENABLE_BIN_LOC
            cstmt.setString(4, P_BIN_LOC_SELECTED); //P_INV_ENABLE_BIN_LOC
            cstmt.setString(5, P_ENABLE_BIN_LOC_RANGE); //P_INV_ENABLE_BIN_LOC
            cstmt.setString(6, P_BIN_LOC_LOW); //P_INV_ENABLE_BIN_LOC
            cstmt.setString(7, P_BIN_LOC_HIGH); //P_INV_ENABLE_BIN_LOC
            cstmt.setString(8, P_PRICE); //P_INV_ENABLE_BIN_LOC
            cstmt.setString(9, P_FLAG); //P_INV_ENABLE_BIN_LOC
            cstmt.setString(10, P_PRINT_LOCATORS); //P_INV_ENABLE_BIN_LOC
            cstmt.setString(11, P_SHELF_PRICING); //P_INV_ENABLE_BIN_LOC
            cstmt.setString(12, P_COPIES); //P_INV_ENABLE_BIN_LOC
            cstmt.registerOutParameter(13, Types.INTEGER); //x_result
            cstmt.registerOutParameter(14, Types.VARCHAR); //x_message 
            cstmt.execute();
            x_return = cstmt.getInt(13);
            x_message = cstmt.getString(14);
            cstmt.close();
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_return " + x_return);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_message " + x_message);
            boolean showSubError = true;
                if (x_return == 0) {
                        ses.setStatusMessage(x_message);
                }
                if (x_return > 0) {
                    dialogPageButtons = new String[] { "OK" };
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    int k = telnetsessionX.showPromptPage("Error!", x_message, dialogPageButtons);
                    if (k == 0) {
                         ses.setNextFieldName("XXWC.COPIES");
                     } 
                }
        }
        catch (Exception e){
            UtilFns.error("Error in calling default execPrintTSLabel " + e);
        }
    
        return x_message;
    }
        
    /*************************************************************************
    *   NAME: public void exitedQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public void exitedQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
        try {
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            cstmt = con.prepareCall("{call XXWC_LABEL_PRINTERS_PKG.validate_lbl_qty_val(?,?,?)}");
            cstmt.setString(1, pg.getmCopies().getValue());
            cstmt.registerOutParameter(2, Types.INTEGER); //x_return
            cstmt.registerOutParameter(3, Types.VARCHAR); //x_message
            cstmt.execute();
            Integer x_return = cstmt.getInt(2);
            String  x_message = cstmt.getString(3);
            cstmt.close();
            boolean showSubError = true;
            if (x_return > 0) {
                TelnetSession telnetsessionX = (TelnetSession)ses;
                Integer k =
                    telnetsessionX.showPromptPage("Error!", x_message,
                                                  dialogPageButtons);
                if (k == 0) {
                    pg.getmCopies().setValue(null);
                    ses.setNextFieldName("XXWC.COPIES");
                } else {
                    return;
                }
            }
            
            Integer l_copies = Integer.parseInt(pg.getmCopies().getValue());
            Integer l_max_copies = 25;
            
            if (l_copies > l_max_copies){
                TelnetSession telnetsessionX = (TelnetSession)ses;
                Integer k =
                    telnetsessionX.showPromptPage("Error!", l_copies + " number of copies is greater than " + l_max_copies,
                                                  dialogPageButtons);
                if (k == 0) {
                    pg.getmCopies().setValue(null);
                    ses.setNextFieldName("XXWC.COPIES");
                } else {
                    return;
                }
            }
            //if (x_return == 0) {
            //    ses.setNextFieldName("XXWC.UBD_STATUS");
            //}
        
       } catch (Exception e) {
           UtilFns.error("Error in calling exitedCopies" + e);
       }
    }

    /*************************************************************************
    *   NAME: public void exitedQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

     public void exitedItemStatus(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                 DefaultOnlyHandlerException {
         try {
             if (pg.getmItemLabelStatusFlag().getValue().equals("") || pg.getmItemLabelStatusFlag().getValue().equals(null)) {
                 return;
             }
             else {
                 pg.getmItemLabelStatusFlag().getValue().toUpperCase();
             }
              
         }
         catch (Exception e) {
             UtilFns.error("Error in calling exitedItemStatus" + e);         
         }
    }
         

}
