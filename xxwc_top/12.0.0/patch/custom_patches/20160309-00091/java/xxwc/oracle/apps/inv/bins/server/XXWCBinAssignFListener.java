/*************************************************************************
   *   $Header XXWCBinAssignFListener.java $
   *   Module Name: XXWCBinAssignFListener
   *   
   *   Package: package xxwc.oracle.apps.inv.bins.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCBinAssignPage Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
        1.1       02-MAR-2016   Lee Spitzer             TMS Ticket ?20160309-00091 - RF Mass Bin Fixes
**************************************************************************/
package xxwc.oracle.apps.inv.bins.server;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.presentation.telnet.TelnetSession;
import oracle.jdbc.OraclePreparedStatement;
import oracle.sql.NUMBER;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.invinq.server.ItemOnhandQueryManager;

import xxwc.oracle.apps.inv.lov.server.XXWCBinsLOV;
import xxwc.oracle.apps.inv.lov.server.XXWCItemLOV;

/*************************************************************************
 *   NAME: XXWCBinAssignFListener implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCBinAssignFListener
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       25-JAN-2016   Lee Spitzer             Initial Version -
                                                          TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
 **************************************************************************/


public class XXWCBinAssignFListener implements MWAFieldListener{
    XXWCBinAssignPage pg;
    Session ses;
    String dialogPageButtons[];
    Integer g_return = 1; //Added 02/16/2016
    String g_message = ""; //Added 02/16/2016


    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.bins.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.bins.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCBinAssignFListener";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    private static String gCallFrom = "XXWCBinAssignFListener";

    /*************************************************************************
     *   NAME: public XXWCBinAssignFListener()
     *
     *   PURPOSE:   public method XXWCBinAssignFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    public XXWCBinAssignFListener() {
        $init$();
    }

    /*************************************************************************
     *   NAME:  public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException
     *
     *   PURPOSE:   field entered for XXWCBinAssignFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/
    
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        pg = (XXWCBinAssignPage)ses.getCurrentPage();
        String s = UtilFns.fieldEnterSource(ses);
        /*Item*/
        
        if (s.equals("XXWC.ITEM")) {
            menuCancelPrompt(mwaevent, ses);
            enteredItem(mwaevent);
            return;
        }
        /*Sub*/
        if (s.equals("XXWC.SUB")) {
            menuCancelPrompt(mwaevent, ses);
            enteredSub(mwaevent);
            return;
        }
        /*Loc*/
        if (s.equals("XXWC.LOC")) {
            menuCancelPrompt(mwaevent, ses);
            enteredLocator(mwaevent);
            return;
        }
        /*Loc*/
        if (s.equals("XXWC.WC_PREFIX")) {
            menuCancelPrompt(mwaevent, ses);
            enteredLocatorPrefix(mwaevent);
            return;
        }
        /*Print*/ //Added 02/16/2016
        if (s.equals("XXWC.PRINT")){ //Added 02/16/2016
            exitedLocatorPrefix(mwaevent, ses); //added 02/16/2016
            return; //Added 02/16/2016
        }
        /*Assign*/ //Added 02/16/2016
        if (s.equals("XXWC.ASSIGN")){ //Added 02/16/2016
            exitedLocatorPrefix(mwaevent, ses); //added 02/16/2016
            return; //Added 02/16/2016
        }
        
        
    }

    /*************************************************************************
     *   NAME:      public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   field exited for XXWCBinAssignFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {
        String s = ((FieldBean)mwaevent.getSource()).getName();
        String vField = mwaevent.getAction();
        boolean flag = false;
        boolean flag1 = false;
        boolean flag3 = false;
        if (mwaevent.getAction().equals("MWA_SUBMIT")) {
            flag = true;
        } else if (mwaevent.getAction().equals("MWA_PREVIOUSFIELD")) {
            flag1 = true;
        } else if (mwaevent.getAction().equals("MWA_NEXTFIELD")) {
            flag3 = true;
        }
        /*Item*/
        if ((flag && s.equals("XXWC.ITEM")) || (flag3 && s.equals("XXWC.ITEM"))) {
            exitedItem(mwaevent, ses);
            return;
        }
        /*Sub*/
        if ((flag && s.equals("XXWC.SUB")) || (flag3 && s.equals("XXWC.SUB"))) {
            exitedSub(mwaevent, ses);
            return;
        }
        /*Loc*/
        if ((flag && s.equals("XXWC.LOC")) || (flag3 && s.equals("XXWC.LOC"))) {
            exitedLocator(mwaevent, ses);
            return;
        }
        if ((flag && s.equals("XXWC.WC_PREFIX")) || (flag3 && s.equals("XXWC.WC_PREFIX"))){
            exitedLocatorPrefix(mwaevent, ses);
            return;
        }
        /*Cancel*/
        if (flag && s.equals("XXWC.CANCEL")) {
            exitedCancel(mwaevent, ses);
            return;
        }
        /*Print*/
        if (flag && s.equals("XXWC.PRINT")) {
            if (g_return==0) { //added 02/16/2016
                processItemLocatorTie(mwaevent, ses);
                clearFields(mwaevent, ses);
            } //added 02/16/2016
            else { //added 02/16/2016
                exitedLocatorPrefix(mwaevent,ses); //added 02/16/2016
            } //added 02/16/2016
            return;
        }
        /*Assign*/
        if (flag && s.equals("XXWC.ASSIGN")) {
            if (g_return==0) { //added 02/16/2016
                processItemLocatorTie(mwaevent, ses);
                clearFields(mwaevent, ses);
            } //added 02/16/2016
            else { //added 02/16/2016
                exitedLocatorPrefix(mwaevent,ses); //added 02/16/2016
            } //added 02/16/2016
            return;
        }
        else {
            return;
        }
    }

    /*************************************************************************
     *   NAME:      private void $init$()
     *
     *   PURPOSE:   $init$ sets the diaglogButtons button for user prompt messages to OK
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
         1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    private void $init$() {
        dialogPageButtons = (new String[] { "OK" });
    }


    /*************************************************************************
     *   NAME: public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Item Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/


    public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {

        try {
            pg.getmSubFld().setHidden(true);
            XXWCItemLOV itemlov = pg.getmItemFld();
            itemlov.setValidateFromLOV(true);
            itemlov.setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_WC_ITEM_LOV");
            String paramType[] = { "C", "N", "S" };
            String parameters[] =
            { " ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCBinAssignPage.XXWC.ITEM" };
            itemlov.setInputParameterTypes(paramType);
            itemlov.setInputParameters(parameters);
        } catch (Exception e) {
            UtilFns.error("Error in calling enteredItem" + e);
        }
    }

    /*************************************************************************
     *   NAME: public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:  Exited Item Field Listener to populate the Item Description Field
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     **************************************************************************/

    public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                  DefaultOnlyHandlerException {
        if (pg.getmItemFld().getValue().equals(null) || pg.getmItemFld().getValue().equals("")) {
            return;
        }
        else {
            try {
                pg.getmItemFld().getValue().toUpperCase();
                pg.getmItemDescFld().setValue(pg.getmItemFld().getmItemDescription());
                pg.getmSubFld().setHidden(false);
                pg.getmSubFld().setValue("General");
                exitedSub(mwaevent, ses);
            } catch (Exception e) {
                UtilFns.error("Error in calling exitedItem" + e);
            }
        }
    }

    /*************************************************************************
    *   NAME: public void enteredSub(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Subinventory Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/

    public void enteredSub(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

      
       
       try {
           pg.getmLocFld().setHidden(true);
           pg.getmLocFldPreFix().setHidden(true);
           SubinventoryLOV sublov = pg.getmSubFld();
           sublov.setValidateFromLOV(true);
           sublov.setlovStatement("INV_UI_ITEM_SUB_LOC_LOVS.GET_VALID_SUBS");
           String paramType[] = { "C", "N", "S" };
           String parameters[] =
           { " ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCBinAssignPage.XXWC.SUB" };
           sublov.setInputParameterTypes(paramType);
           sublov.setInputParameters(parameters);
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredSub" + e);
       }
    }

    /*************************************************************************
    *   NAME: public void exitedSub(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/

    public void exitedSub(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
       
        if (pg.getmSubFld().getValue().equals(null) || pg.getmSubFld().getValue().equals("")) {
            return;
        }
        else {
               try {
                   Integer i = getLocatorControl(mwaevent, ses);
                   if (i == 1) {
                       pg.getmLocFld().setHidden(true);
                       pg.getmLocFld().clear();
                       pg.getmLocFld().setRequired(false);
                       pg.getmLocFldPreFix().setHidden(true);
                       pg.getmLocFldPreFix().setValue(null);
                       pg.getmLocFldPreFix().setRequired(false);
                          
                   } else {
                       pg.getmLocFld().setHidden(false);
                       pg.getmLocFld().setRequired(true);
                       pg.getmLocFldPreFix().setHidden(false);
                       if (ses.getObject("TXN.MASS_ASSIGN").equals("N")) {
                            pg.getmLocFldPreFix().setRequired(true);
                            pg.getmLocFldPreFix().setEditable(true);
                       }
                       else if (ses.getObject("TXN.MASS_ASSIGN").equals("Y")) {
                            pg.getmLocFldPreFix().setRequired(false);
                            pg.getmLocFldPreFix().setEditable(false);
                       }
                       ses.setNextFieldName("XXWC.LOC");
                   }
               } catch (Exception e) {
                   UtilFns.error("Error in calling exitedSub" + e);
               }
        }
    }

    /*************************************************************************
    *   NAME: public void getLocatorControl(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Determines if Subinventory has Locator Control turned on or off
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
          1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/

    private int getLocatorControl(MWAEvent mwavevent, Session session) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
       int i = 1;
       int j = 1;
       int k = 1;
       int x_count = 0;
       try {
           Long orgid = Long.parseLong((String)session.getObject("ORGID"));
           FileLogger.getSystemLogger().trace("orgid  " + orgid);
           FileLogger.getSystemLogger().trace("i  " + i);
           OrgParameters orgparameters = OrgParameters.getOrgParameters(session.getConnection(), orgid);
           i = orgparameters.getLocatorControlCode();
           FileLogger.getSystemLogger().trace("i  " + i);
           FileLogger.getSystemLogger().trace("j " + j);
           SubinventoryLOV sublov = pg.getmSubFld();
           if (pg.getmSubFld().getValue() != null | pg.getmSubFld().getValue() != "") {
               j = Integer.parseInt(sublov.getLocatorType());
           }
           FileLogger.getSystemLogger().trace("j " + j);
           XXWCItemLOV itemlov = pg.getmItemFld();
           FileLogger.getSystemLogger().trace("k " + k);
           if (pg.getmItemFld().getValue() != null | pg.getmItemFld().getValue() != "") {
               k = Integer.parseInt("2");
           }
           FileLogger.getSystemLogger().trace("k " + k);
           if (UtilFns.isTraceOn) {
               UtilFns.trace((new StringBuilder()).append("RCV: getLocatorControl orgLocControl:").append(i).append(":subLocControl:").append(j).append(":itemLocControl:").append(k).toString());
           }
           
       } catch (Exception e) {
           FileLogger.getSystemLogger().trace("error getLocatorControl " + e);
       }
        //Added for WC Branches
        try {
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            String p_organization_id = (String)ses.getObject("ORGID");
            String p_subinventory_code = (String)pg.getmSubFld().getValue();
            cstmt = con.prepareCall("{? = call XXWC_INV_BIN_MAINTENANCE_PKG.WC_BRANCH_LOCATOR_CONTROL(?,?)}");
            cstmt.setString(2, p_organization_id);
            cstmt.setString(3, p_subinventory_code);
            cstmt.registerOutParameter(1, Types.NUMERIC);
            cstmt.execute();
            x_count = cstmt.getInt(1);
            cstmt.close();
            FileLogger.getSystemLogger().trace("x_count " + x_count);
        }
        catch (Exception e) {
            UtilFns.error("Error in calling getLocatorControl" + e);
        }
        
        if (x_count > 0) {
            return 0;
        }
        else {
            return 1;
        }
    }

    /*************************************************************************
    *   NAME: public void enteredLocator(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Locator Field Listener Process to set the List of Values for the Locator Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/

    public void enteredLocator(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {

        try {
            ses.putSessionObject("sessInventoryItemId", pg.getmItemFld().getmInventoryItemId().toString());
            ses.putSessionObject("sessRestrictLocator", 3);
            ses.putSessionObject("sessHidePrefix","N");
            XXWCBinsLOV binsLOV = pg.getmLocFld();
            //                      0    1    2    3    4    5    6    7    8
            String paramType[] = { "C", "N", "S", "N", "N", "S", "S", "S", "S" };
            String prompts[] = { "WC_LOCATOR", "PREFIX", "LOCATOR_ID", "LOCATOR", "DESCRIPTION", "SUBINVENTORY_CODE" };
            boolean flag[] = {true, true, false, true, false, false};
            //                        0        1                                                              2 
            String parameters[] = { " ", "ORGID", "xxwc.oracle.apps.inv.bins.server.XXWCBinAssignPage.XXWC.SUB" ,"sessRestrictLocator", "sessInventoryItemId" , "xxwc.oracle.apps.inv.bins.server.XXWCBinAssignPage.XXWC.LOC", "", "", "sessHidePrefix" };
            //String parameters[] = { " ", "ORGID", "" ,"", "sessInventoryItemId" , "xxwc.oracle.apps.inv.lables.server.XXWCItemLabelPage.XXWC.BIN", "", "", "N" };
            binsLOV.setInputParameters(parameters);
            binsLOV.setlovStatement("XXWC_INV_BIN_MAINTENANCE_PKG.GET_WC_BIN_LOC");
            binsLOV.setInputParameterTypes(paramType);
            binsLOV.setSubfieldPrompts(prompts);
            binsLOV.setSubfieldDisplays(flag);
            binsLOV.setValidateFromLOV(false);
            
        } catch (Exception e) {
            UtilFns.error("Error in calling enteredItem " + e);
        }
    }

    /*************************************************************************
    *   NAME: public void exitedLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
            1.1       02-MAR-2016   Lee Spitzer             TMS Ticket 20160309-00091 - RF Mass Bin Fixes
    **************************************************************************/

    public void exitedLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
       
       
        String gMethod = "exitedLocator";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
        gCallPoint = "Upper and Trim Locator Field"; //added TMS Ticket 20160309-00091
        pg.getmLocFld().setValue(pg.getmLocFld().getValue().toUpperCase().trim()); //added TMS Ticket 20160309-00091
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " locator field after triming and upper is " + pg.getmLocFld().getValue().toUpperCase().trim()); //added TMS Ticket 20160309-00091
          
       try {
           //If locator is null or blank then hide print and assign
           if ((pg.getmLocFld().getValue().equals(null)) || pg.getmLocFld().getValue().equals("")){
               gCallPoint = "Locator is null";
               FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
               pg.mPrint.setHidden(true);
               pg.mAssign.setHidden(true);
               pg.getmLocFldPreFix().setHidden(true);
               return;
           } 
           else {   CallableStatement cstmt = null;
                    Connection con = ses.getConnection();
                    //Need to validate the locator_id exists, if it was populated by the LOV it exists
                    String p_organization_id = (String)ses.getObject("ORGID");
                    String p_inventory_item_id = (String)pg.getmItemFld().getmInventoryItemId();
                    String p_subinventory_code = (String)pg.getmSubFld().getValue();
                    String p_locator  = (String)pg.getmLocFld().getValue();
                    String p_locator_id = (String)pg.getmLocFld().getmLocationId();
                    String p_locator_prefix = pg.getmLocFldPreFix().getValue();
                    Integer x_return;
                    String x_message;
                    //Check if locator is already assigned to item
                    try {
                        //                                                                                    1 2 3 4 5 6
                        cstmt = con.prepareCall("{call XXWC_INV_BIN_MAINTENANCE_PKG.CHECK_FOR_DUP_ASSIGNMENTS(?,?,?,?,?,?)}");
                        cstmt.setString(1, p_organization_id);
                        cstmt.setString(2, p_inventory_item_id);
                        cstmt.setString(3, p_subinventory_code);
                        cstmt.setString(4, p_locator);
                        cstmt.registerOutParameter(5, Types.NUMERIC);
                        cstmt.registerOutParameter(6, Types.VARCHAR);
                        cstmt.execute();
                        x_return = cstmt.getInt(5);
                        x_message = cstmt.getString(6);
                        cstmt.close();
                        //if the x_return is not 0, that means the locator is already assigned to the item
                        if (x_return != 0) {
                            TelnetSession telnetsessionX = (TelnetSession)ses;
                            int k = telnetsessionX.showPromptPage("Error!", x_message.replace("|",System.getProperty("line.separator")), dialogPageButtons);
                            if (k == 0) {
                                ses.setRefreshScreen(true);
                                pg.getmLocFld().setValue("");
                                pg.getmLocFldPreFix().setValue("");
                                pg.getmPrint().setHidden(true);
                                pg.getmAssign().setHidden(true);
                                ses.setNextFieldName("XXWC.LOC");
                                return;
                            } else {
                                return;
                            }
                        }
                    } catch (Exception e) {
                        ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_ERROR).append(" (").append(e).append(")").toString());
                        UtilFns.error((new StringBuilder()).append("Error in processItemLocatorTie: CHECK_FOR_DUP_ASSIGNMENTS error - ").append(e).toString());
                    }

               if (pg.getmLocFld().getmLocationId().equals(null) || pg.getmLocFld().getmLocationId().equals("")){
               
               //if locator is populated then validate locator is valid since lov validation is set to false
               gCallPoint = "Locator is not null";
               FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
               p_organization_id = (String)ses.getObject("ORGID");
               p_locator = (String)pg.getmLocFld().getValue();
               FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " p_organization_id " + p_organization_id );
               FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " p_locator " + p_locator);
               FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " prefix " + (String)pg.getmLocFld().getmWCPreFix());
               FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " location_id  " + (String)pg.getmLocFld().getmLocationId());
               x_return = 0;
               //Validate locator exists as a 1-, 2-, 3-
               gCallPoint = "About to call XXWC_INV_BIN_MAINTENANCE_PKG.VAL_LOC";
               try {
                   FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                   //                                                                      1 2 
                   con = ses.getConnection(); //added 5/12/2015
                   cstmt = con.prepareCall("{? = call XXWC_INV_BIN_MAINTENANCE_PKG.VAL_BIN(?,?)}");
                   cstmt.registerOutParameter(1, Types.NUMERIC);
                   cstmt.setString(2, p_organization_id);
                   cstmt.setString(3, p_locator);
                   cstmt.execute();
                   x_return = cstmt.getInt(1);
                   cstmt.close();
                   FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " x_return " + x_return);
                   if (x_return == 0) {
                       //Allow if coming from Bin Maintenance Form to add and create locators
                           gCallPoint = "x_return is 0 and Bin Maintenance" ;
                           FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                           createLocator(mwaevent, ses);
                           ses.setRefreshScreen(true);
                           pg.getmPrint().setHidden(false);
                           if (ses.getObject("TXN.MASS_ASSIGN").equals("N")) {
                               pg.getmAssign().setHidden(false);
                               ses.setNextFieldName("XXWC.WC_PREFIX"); //added 03/08/2016
                           }
                           else if (ses.getObject("TXN.MASS_ASSIGN").equals("Y")) {
                               pg.getmAssign().setHidden(true);
                               ses.setNextFieldName("XXWC.PRINT");    //added 03/08/2016
                               ses.putObject("MWA_AUTO_ENTER", Boolean.TRUE); //added 03/08/2016
                           }
                   }
                   else {
                       //The locator exists now get the next prefix
                       gCallPoint = "x_return is > 0";
                       p_inventory_item_id = pg.getmItemFld().getmInventoryItemId();
                       String x_next_bin = "0";
                       FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                       con = ses.getConnection(); //added 5/12/2015
                       cstmt = con.prepareCall("{? = call XXWC_INV_BIN_MAINTENANCE_PKG.NEXT_BIN_PREFIX(?,?)}");
                       cstmt.registerOutParameter(1, Types.VARCHAR);
                       cstmt.setString(2, p_organization_id);
                       cstmt.setString(3, p_inventory_item_id);
                       cstmt.execute();
                       x_next_bin = cstmt.getString(1);
                       cstmt.close();
                       FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " x_next_bin " + x_next_bin);
                       if (x_next_bin.equals(null) || x_next_bin.equals("")){
                           FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " next bin is null");
                           pg.getmLocFldPreFix().setValue("");
                           pg.mPrint.setHidden(true);
                           pg.mAssign.setHidden(true);
                           ses.setNextFieldName("XXWC.LOC");
                       }
                       else {
                           FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " next bin is not null");
                           pg.getmLocFldPreFix().setValue(x_next_bin);
                           pg.mPrint.setHidden(false);
                           if (ses.getObject("TXN.MASS_ASSIGN").equals("N")) {
                            pg.getmAssign().setHidden(false);
                            ses.setNextFieldName("XXWC.WC_PREFIX"); //added 03/08/2016
                            }
                           else if (ses.getObject("TXN.MASS_ASSIGN").equals("Y")) {
                            pg.getmAssign().setHidden(true);
                            ses.setNextFieldName("XXWC.PRINT");    //added 03/08/2016
                            ses.putObject("MWA_AUTO_ENTER", Boolean.TRUE); //added 03/08/2016
                           }
                          
                       }
                   }
               }
                catch (Exception e) {
                      FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " " + e);
                      UtilFns.error("Error in calling XXWC_INV_BIN_MAINTENANCE_PKG.VAL_LOC " + e);
                }
               
               }
               else {
                   gCallPoint = "Locator id populated from LOV";
                   FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                   pg.getmLocFldPreFix().setValue(pg.getmLocFld().getmWCPreFix());
                   pg.mPrint.setHidden(false);
                   if (ses.getObject("TXN.MASS_ASSIGN").equals("N")) {
                       pg.getmAssign().setHidden(false);
                       ses.setNextFieldName("XXWC.WC_PREFIX"); //added 03/08/2016
                    }
                   else if (ses.getObject("TXN.MASS_ASSIGN").equals("Y")) {
                        pg.getmAssign().setHidden(true);
                        ses.setNextFieldName("XXWC.PRINT");    //added 03/08/2016
                        ses.putObject("MWA_AUTO_ENTER", Boolean.TRUE); //added 03/08/2016
                   }
                }
            }
       }
        catch (Exception e) {
           UtilFns.error("Error in calling exitedLocator" + e);
       }
    }

    /*************************************************************************
    *   NAME: public void enteredLocatorPrefix(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Locator Field Listener Process to set the List of Values for the Locator Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/

    public void enteredLocatorPrefix(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {
        
        String gMethod = "enteredLocatorPrefix";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
        
    }

    /*************************************************************************
    *   NAME: public void exitedLocatorPrefix(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/

    public void exitedLocatorPrefix(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
       try {
           if (pg.getmLocFldPreFix().getValue().equals(null)){
               pg.mPrint.setHidden(true);
               pg.mAssign.setHidden(true);
               g_return = 1; //Added 02/16/2016
               g_message = "Unable to use Locator Prefix";
           } else {
               
               String gMethod = "exitedLocatorPrefix";
               String gCallPoint = "Start";
               FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
               
               pg.getmLocFldPreFix().setValue(pg.getmLocFldPreFix().getValue().trim());
               if (pg.getmLocFldPreFix().getValue().equals(null)) {
                   g_return = 1; //Added 02/16/2016
                   g_message = "Unable to use Locator Prefix";
                   return;
               } 
               else {
                   gCallPoint = "Calling XXWC_INV_BIN_MAINTENANCE_PKG.validate_loc_prefix_val ";
                   try {
                       CallableStatement cstmt = null;
                       Connection con = ses.getConnection();
                       cstmt = con.prepareCall("{call XXWC_INV_BIN_MAINTENANCE_PKG.validate_loc_prefix_val(?,?,?)}");
                       cstmt.setString(1, pg.getmLocFldPreFix().getValue());
                       cstmt.registerOutParameter(2, Types.INTEGER); //x_return
                       cstmt.registerOutParameter(3, Types.VARCHAR); //x_message
                       cstmt.execute();
                       Integer x_return = cstmt.getInt(2);
                       String  x_message = cstmt.getString(3);
                       cstmt.close();
                       boolean showSubError = true;
                       if (x_return > 0) {
                           TelnetSession telnetsessionX = (TelnetSession)ses;
                           Integer k =
                               telnetsessionX.showPromptPage("Error!", x_message,
                                                             dialogPageButtons);
                           if (k == 0) {
                               pg.getmLocFldPreFix().setValue(null);
                               ses.setNextFieldName("XXWC.WC_PREFIX");
                           } else {
                               return;
                           }
                           g_return = 1; //Added 02/16/2016
                           g_message = "Unable to use Locator Prefix"; //Added 02/16/2016
                       }
                       if (x_return == 0) {
                           pg.mPrint.setHidden(false);
                           if (ses.getObject("TXN.MASS_ASSIGN").equals("N")) {
                            pg.getmAssign().setHidden(false);
                           }
                           else if (ses.getObject("TXN.MASS_ASSIGN").equals("Y")) {
                            pg.getmAssign().setHidden(true);
                           }
                           ses.setNextFieldName("XXWC.PRINT");
                           g_return = 0; //Added 02/16/2016
                           g_message = "";
                       }
                   }
                   catch (Exception e) {
                       UtilFns.error("Error in validating Quantity" + e);
                   }
               }
           }
       } catch (Exception e) {
           UtilFns.error("Error in calling exitedLocatorPrefix" + e);
       }
    }

    /*************************************************************************
    *   NAME: public void exitedClose(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/

    public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        
        try {
            String gMethod = "exitedCancel";
            String gCallPoint = "Start";
            String l_prompt = "";
            l_prompt = pg.getmMenu().getPrompt();
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " l_prompt " + l_prompt);
            if (l_prompt.equals("Menu")){
                XXWCBinAssignPage _tmp = pg;
                ses.setStatusMessage(pg.WMS_TXN_CANCEL);
                ses.clearAllApplicationScopeObjects();
                ses.setStatusMessage(XXWCBinAssignPage.WMS_TXN_CANCEL);
                pg.getmMenu().setNextPageName("|END_OF_TRANSACTION|");
            }
            else if (l_prompt.equals("Cancel")) {
                clearFields(mwaevent, ses);
            }
            else {
                return;
            }
        } catch (Exception e) {
            UtilFns.error("Error in calling exitedCancel " + e);
        }

    }
    
    /*************************************************************************
    *   NAME:     public void processItemLocatorTie(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                              TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/
    
    public void processItemLocatorTie(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String gMethod = "processItemLocatorTie";
        String gCallPoint = "Start";
        CallableStatement cstmt = null;
        Connection con = ses.getConnection();
        //Need to validate the locator_id exists, if it was populated by the LOV it exists
        String p_organization_id = (String)ses.getObject("ORGID");
        String p_inventory_item_id = (String)pg.getmItemFld().getmInventoryItemId();
        String p_subinventory_code = (String)pg.getmSubFld().getValue();
        String p_locator  = (String)pg.getmLocFld().getValue();
        String p_locator_prefix = (String)pg.getmLocFldPreFix().getValue();
        Integer x_return;
        String x_message; 
        if (pg.getmLocFld().getmLocationId().equals(null) || pg.getmLocFld().getmLocationId().equals("")){
            //Need to get the locator id from the Locator Field and Locator Prefix; if that doesn't exist then need to pop up a message to create a new location
            try {
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                //                                                                              1 2 3
                Integer x_locator_id = -1;
                cstmt = con.prepareCall("{? = call XXWC_INV_BIN_MAINTENANCE_PKG.GET_BIN_VAL_LOC(?,?,?)}");
                cstmt.registerOutParameter(1, Types.NUMERIC);
                cstmt.setString(2, p_organization_id);
                cstmt.setString(3, p_locator);
                cstmt.setString(4, p_locator_prefix);
                cstmt.execute();
                x_locator_id = cstmt.getInt(1);
                cstmt.close();
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " x_locator_id " + x_locator_id);
                //if there isn't a count of a valid locator then raise message the locator entered doesn't exists
                if (x_locator_id == -1) {
                    gCallPoint = "x_return is -1";
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                    gCallPoint = "Starting XXWC_INV_BIN_MAINTENANCE_PKG.CREATE_NEW_LOCATOR";
                        try {
                            con = ses.getConnection(); //added 5/12/2015
                            cstmt = con.prepareCall("{call XXWC_INV_BIN_MAINTENANCE_PKG.CREATE_NEW_LOCATOR(?,?,?,?,?,?)}");
                            cstmt.setString(1, p_organization_id);
                            cstmt.setString(2, p_subinventory_code);
                            cstmt.setString(3, p_locator);
                            cstmt.setString(4, p_locator_prefix);
                            cstmt.registerOutParameter(5, Types.NUMERIC);
                            cstmt.registerOutParameter(6, Types.VARCHAR);
                            cstmt.execute();
                            x_locator_id = cstmt.getInt(5);
                            x_message = cstmt.getString(6);
                            cstmt.close();
                            if (x_locator_id == -1) {
                                TelnetSession telnetsessionY = (TelnetSession)ses;
                                int l = telnetsessionY.showPromptPage("Error!", "Error creating locator " + p_locator_prefix + "-" + p_locator, dialogPageButtons);
                                if (l == 0) {
                                    clearFields(mwaevent, ses);
                                    return;
                                }
                            }
                            else {
                                pg.getmLocFld().setmLocationId(Integer.toString(x_locator_id));
                                pg.getmLocFld().setmWCPreFix(p_locator_prefix);
                                pg.getmLocFld().setmWCLocator(p_locator);
                                pg.getmLocFld().setmConcatedSegments(p_locator_prefix + "-" + p_locator);
                            }
                            
                        }
                        catch (Exception e) {
                            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " error getting XXWC_INV_BIN_MAINTENANCE_PKG.CREATE_NEW_LOCATOR " + e);
                            
                        }
                }
                pg.getmLocFld().setmLocationId(Integer.toString(x_locator_id));
                pg.getmLocFld().setmWCPreFix(p_locator_prefix);
                pg.getmLocFld().setmWCLocator(p_locator);
                pg.getmLocFld().setmConcatedSegments(p_locator_prefix + "-" + p_locator);
            }
            catch (Exception e) {
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " error " + e);
            }
        }
        
        //Resetting x_return and x_message values;
        x_return = 0;
        x_message = "";
        String p_locator_id = (String)pg.getmLocFld().getmLocationId();
                
        //if all test validate, then create item locator tie
        try {
            con = ses.getConnection(); //added 5/12/2015
            //                                                                                   1 2 3 4 5 6
            cstmt = con.prepareCall("{call XXWC_INV_BIN_MAINTENANCE_PKG.PROCESS_ITEM_LOCATOR_TIE(?,?,?,?,?,?)}");
            cstmt.setString(1, p_organization_id);
            cstmt.setString(2, p_inventory_item_id);
            cstmt.setString(3, p_subinventory_code);
            cstmt.setString(4, p_locator_id);
            cstmt.registerOutParameter(5, Types.NUMERIC);
            cstmt.registerOutParameter(6, Types.VARCHAR);
            cstmt.execute();
            x_return = cstmt.getInt(5);
            x_message = cstmt.getString(6);
            cstmt.close();
            if (x_return == 0) {
                String x_print_message = "";
                String s = UtilFns.fieldEnterSource(ses);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " current_field " + s);
                if (s.equals("XXWC.PRINT")) {
                    x_print_message = execPrintItemLabel(mwaevent,ses);
                }
                ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_SUCCESS).append(" (").append(pg.getmItemFld().getValue() + " | " + pg.getmLocFld().getmConcatedSegments() + " " + x_message + " " + x_print_message).append(")").toString());
                
                //Cascade Locators
                cascadeBinAssignmentsAdd(mwaevent, ses, p_locator_id);
                
                return;
            }
            if (x_return != 0) {
                TelnetSession telnetsessionX = (TelnetSession)ses;
                int k = telnetsessionX.showPromptPage("Error!", x_message, dialogPageButtons);
                if (k == 0) {
                    ses.setRefreshScreen(true);
                } else {
                    return;
                }
            }
        } catch (Exception e) {
            ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_ERROR).append(" (").append(e).append(")").toString());
            UtilFns.error((new StringBuilder()).append("Error in processItemLocatorTie: error PROCESS_ITEM_LOCATOR_TIE - ").append(e).toString());
        }
    }


    /*************************************************************************
    *   NAME:     public void clearFields(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/
    
    public void clearFields(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                   InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        String gMethod = "clearFields";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
        pg.mItemFld.setValue(null);
        pg.mItemDescFld.setValue(null);
        pg.mSubFld.setValue(null);
        pg.mSubFld.setHidden(true);
        pg.mLocFld.setValue(null);
        pg.mLocFld.setHidden(true);
        pg.mLocFldPreFix.setValue(null);
        pg.mLocFldPreFix.setHidden(true);
        pg.mAssign.setHidden(true);
        pg.mPrint.setHidden(true);
        ses.setNextFieldName("XXWC.ITEM");
        pg.getmLocFld().clear();
        pg.getmLocFldPreFix().setValue(null);
    }
    
    
    /*************************************************************************
    *   NAME:     public void createLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                              TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/
    
    public void createLocator(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String gMethod = "createLocator";
        String gCallPoint = "Start";
        CallableStatement cstmt = null;
        Connection con = ses.getConnection();
        String p_organization_id = (String)ses.getObject("ORGID");
        String p_subinventory_code = (String)pg.getmSubFld().getValue();
        String p_locator = (String)pg.getmLocFld().getValue();
        Integer x_return;
        String x_message;
        try {
            //                                                                            1 2 3 4 5
            cstmt = con.prepareCall("{call XXWC_INV_BIN_MAINTENANCE_PKG.CREATE_WC_LOCATOR(?,?,?,?,?)}");
            cstmt.setString(1, p_organization_id);
            cstmt.setString(2, p_subinventory_code);
            cstmt.setString(3, p_locator);
            cstmt.registerOutParameter(4, Types.NUMERIC);
            cstmt.registerOutParameter(5, Types.VARCHAR);
            cstmt.execute();
            x_return = cstmt.getInt(4);
            x_message = cstmt.getString(5);
            cstmt.close();
            if (x_return == 0) {
                ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_SUCCESS).append(" (").append(pg.getmSubFld().getValue() + " | " + pg.getmLocFld().getValue() + " " + x_message).append(")").toString());
                //The locator exists now get the next prefix
                gCallPoint = "x_return is > 0";
                String p_inventory_item_id = "";
                p_inventory_item_id = pg.getmItemFld().getmInventoryItemId();
                String x_next_bin = "0";
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint);
                con = ses.getConnection();
                cstmt = con.prepareCall("{? = call XXWC_INV_BIN_MAINTENANCE_PKG.NEXT_BIN_PREFIX(?,?)}");
                cstmt.registerOutParameter(1, Types.VARCHAR);
                cstmt.setString(2, p_organization_id);
                cstmt.setString(3, p_inventory_item_id);
                cstmt.execute();
                x_next_bin = cstmt.getString(1);
                cstmt.close();
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " x_next_bin " + x_next_bin);
                if (x_next_bin.equals(null) || x_next_bin.equals("")){
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " next bin is null");
                    pg.getmLocFldPreFix().setValue("");
                }
                else {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " next bin is not null");
                    pg.getmLocFldPreFix().setValue(x_next_bin);
                }
            }
            if (x_return != 0) {
                TelnetSession telnetsessionX = (TelnetSession)ses;
                int k = telnetsessionX.showPromptPage("Error!", x_message, dialogPageButtons);
                if (k == 0) {
                    ses.setRefreshScreen(true);
                } else {
                    return;
                }
            }
        } catch (Exception e) {
            ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_ERROR).append(" (").append(e).append(")").toString());
            UtilFns.error((new StringBuilder()).append("Error in createLocator: error - ").append(e).toString());
        }
    }
    

        /*************************************************************************
        *   NAME:     public void menuCancelPrompt(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                     InterruptedHandlerException,
                                                                                     DefaultOnlyHandlerException
        *
        *   PURPOSE:  Update mMenu label from Menu to Cancel.  
        *
        *   REVISIONS:
        *   Ver        Date        Author                     Description
        *   ---------  ----------  ---------------         -------------------------
             1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
        **************************************************************************/
   
         public void menuCancelPrompt(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
            String gMethod = "menuCancelPrompt";
            String gCallPoint = "Start";
            String s = UtilFns.fieldEnterSource(ses);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " current_field " + s);
            if (s.equals("XXWC.ITEM")) {
                pg.getmMenu().setPrompt("Menu");
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " set prompt to Menu");
                
            }
            else {
                pg.getmMenu().setPrompt("Cancel");
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallPoint + " " + gMethod + " " + gCallPoint + " set prompt to Cancel");
            }
                                
        }
        
    /*************************************************************************
        *   NAME: public String execPrintItemLabel(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                      InterruptedHandlerException,
                                                                      DefaultOnlyHandlerException 
    
         *   PURPOSE: Executes the print material label API, xxwc_mwa_routines_pkg.material_label
         *    
         *   In: MWAEvent mwaevent
         *    
         *    
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
              1.0       27-JAN-2016   Lee Spitzer             Initial Version - 
    **************************************************************************/
      
    public String execPrintItemLabel(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                      InterruptedHandlerException,
                                                                      DefaultOnlyHandlerException 
    { 
        String gMethod = "execPrintItemLabel";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint );
        String p_organization_id = "";
        String p_inventory_item_id = "";
        String P_INV_ENABLE_BIN_LOC = "";
        String P_BIN_LOC_SELECTED = "";
        String P_ENABLE_BIN_LOC_RANGE = "";
        String P_BIN_LOC_LOW = "";
        String P_BIN_LOC_HIGH = "";
        String P_PRICE = "";
        String P_FLAG = "";
        String P_PRINT_LOCATORS = "";
        String P_SHELF_PRICING = "";
        String P_COPIES = "";
        Integer x_return = null;
        String x_message = "";
                
        gCallPoint = "Getting values from page";   
        
        p_organization_id = (String)ses.getObject("ORGID");
        p_inventory_item_id = pg.getmItemFld().getmInventoryItemId();
        P_INV_ENABLE_BIN_LOC = "Y";
        P_BIN_LOC_SELECTED = pg.getmLocFld().getmConcatedSegments();
        P_ENABLE_BIN_LOC_RANGE = "";
        P_BIN_LOC_LOW = "";
        P_BIN_LOC_HIGH = "";
        P_PRICE = "";
        P_SHELF_PRICING = "";
        
        //Getting Values From Profile Option
        P_FLAG = getDefaultPrinter("XXWC_LABEL_ITEM_SIZE");
        P_PRINT_LOCATORS = getDefaultPrinter("XXWC_LABEL_ITEM_LOCATION");
        P_COPIES = getDefaultPrinter("XXWC_LABEL_ITEM_COPIES");
        
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " P_FLAG " + P_FLAG); //added 02/16/2016
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " P_PRINT_LOCATORS " + P_PRINT_LOCATORS); //added 02/16/2016
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " P_COPIES " + P_COPIES); //added 02/16/2016
        
        gCallPoint = "Get Yes No Mean";
        
        try{ //added 02/16/2016
        //added 02/16/2016
        if (P_PRINT_LOCATORS.equals("") || P_PRINT_LOCATORS.equals(null)) {
            P_PRINT_LOCATORS = "2"; //set value to No
        }
        }
        catch (Exception e) { //added 02/16/2016
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + e); //added 02/16/2016
            P_PRINT_LOCATORS = "2";//added 02/16/2016
            
        }//added 02/16/2016
        
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " P_PRINT_LOCATORS " + P_PRINT_LOCATORS); //added 02/16/2016
        
        try{
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            cstmt = con.prepareCall("{? = call XXWC_LABEL_PRINTERS_PKG.GET_YES_NO_MEAN(?)}");
            cstmt.registerOutParameter(1, Types.VARCHAR);
            cstmt.setString(2, P_PRINT_LOCATORS);
            cstmt.execute();
            P_PRINT_LOCATORS = cstmt.getString(1);
            cstmt.close();
            if (P_PRINT_LOCATORS.equals(null) || P_PRINT_LOCATORS.equals("")) {
                P_PRINT_LOCATORS = "";
            }
        }
        catch(Exception e){
            UtilFns.error("Error in calling XXWC_LABEL_PRINTERS_PKG.GET_YES_NO_MEAN " + e);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + e);
            P_PRINT_LOCATORS = ""; //added 02/16/2016
        }
        
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " XXWC_LABEL_ITEM_SIZE " + P_PRINT_LOCATORS); //added 02/16/2016
        
        
        //If any of the values return blank, set these values
        if (P_FLAG.equals("")) {
            P_FLAG = "S";
        }
        
        if (P_PRINT_LOCATORS.equals("")){
            P_PRINT_LOCATORS = "No";
        }
        
        if (P_COPIES.equals("")){
            P_COPIES = "1";
        }
        
        String L_PRINT_LOCATORS = P_PRINT_LOCATORS.substring(0,1); //Added 02/16/2016
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " L_PRINT_LOCATORS " + L_PRINT_LOCATORS); //added 02/16/2016
        
        
        try {
            gCallPoint = "Calling XXWC_LABEL_PRINTERS_PKG.PROCESS_ITEM_LABEL_CCR";
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            //                                                                            1 2 3 4 5 6 7 8 9 0 1 2 3 4       
            cstmt = con.prepareCall("{call XXWC_LABEL_PRINTERS_PKG.PROCESS_ITEM_LABEL_CCR(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            cstmt.setString(1, p_organization_id); //p_organization_id
            cstmt.setString(2, p_inventory_item_id); //p_inventory_item_id
            cstmt.setString(3, P_INV_ENABLE_BIN_LOC); //P_INV_ENABLE_BIN_LOC
            cstmt.setString(4, P_BIN_LOC_SELECTED); //P_INV_ENABLE_BIN_LOC
            cstmt.setString(5, P_ENABLE_BIN_LOC_RANGE); //P_INV_ENABLE_BIN_LOC
            cstmt.setString(6, P_BIN_LOC_LOW); //P_INV_ENABLE_BIN_LOC
            cstmt.setString(7, P_BIN_LOC_HIGH); //P_INV_ENABLE_BIN_LOC
            cstmt.setString(8, P_PRICE); //P_INV_ENABLE_BIN_LOC
            cstmt.setString(9, P_FLAG); //P_INV_ENABLE_BIN_LOC
            //cstmt.setString(10, P_PRINT_LOCATORS); //P_INV_ENABLE_BIN_LOC //removed 2/16/2016
            cstmt.setString(10, L_PRINT_LOCATORS); //P_INV_ENABLE_BIN_LOC // added 2/16/2016
            cstmt.setString(11, P_SHELF_PRICING); //P_INV_ENABLE_BIN_LOC
            cstmt.setString(12, P_COPIES); //P_INV_ENABLE_BIN_LOC
            cstmt.registerOutParameter(13, Types.INTEGER); //x_result
            cstmt.registerOutParameter(14, Types.VARCHAR); //x_message 
            cstmt.execute();
            x_return = cstmt.getInt(13);
            x_message = cstmt.getString(14);
            cstmt.close();
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_return " + x_return);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_message " + x_message);
            boolean showSubError = true;
                if (x_return == 0) {
                        //ses.setStatusMessage(x_message);
                }
                if (x_return > 0) {
                    dialogPageButtons = new String[] { "OK" };
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    int k = telnetsessionX.showPromptPage("Error!", x_message, dialogPageButtons);
                    if (k == 0) {
                         ses.setNextFieldName("XXWC.COPIES");
                     } 
                }
        }
        catch (Exception e){
            UtilFns.error("Error in calling default execPrintTSLabel " + e);
        }
    
        return x_message;
    }
    

    /*************************************************************************
    *   NAME:     public void cascadeBinAssignments(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Execute the procedure XXWC_INV_PRIMARY_BIN_PKG.PROCESS_ITEM_LOCATOR_TIE
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                              TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
    **************************************************************************/
    
    public void cascadeBinAssignmentsAdd(MWAEvent mwaevent, Session ses, String p_locator_id) throws AbortHandlerException,
                                                                             InterruptedHandlerException,
                                                                             DefaultOnlyHandlerException {
        String gMethod = "cascadeBinAssignmentsAdd";
        String gCallPoint = "Start";
        CallableStatement cstmt = null;
        Connection con = ses.getConnection();
        String p_organization_id = (String)ses.getObject("ORGID");
        String p_inventory_item_id = (String)pg.getmItemFld().getmInventoryItemId();
        String p_subinventory_code = (String)pg.getmSubFld().getValue();
        Integer x_return;
        String x_message;
        try {
            //                                                                                      1 2 3 4 5 6
            cstmt = con.prepareCall("{call XXWC_INV_BIN_MAINTENANCE_PKG.CASCADE_BIN_ASSIGNMENTS_ADD(?,?,?,?,?,?)}");
            cstmt.setString(1, p_organization_id);
            cstmt.setString(2, p_inventory_item_id);
            cstmt.setString(3, p_subinventory_code);
            cstmt.setString(4, p_locator_id);
            cstmt.registerOutParameter(5, Types.NUMERIC);
            cstmt.registerOutParameter(6, Types.VARCHAR);
            cstmt.execute();
            x_return = cstmt.getInt(5);
            x_message = cstmt.getString(6);
            cstmt.close();
            if (x_return == 0) {
                ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_SUCCESS).append(" (").append(pg.getmSubFld().getValue() + " | " + pg.getmLocFldPreFix().getValue() + "-" + pg.getmLocFld().getValue() + " " + x_message).append(")").toString());
            }
            if (x_return != 0) {
                TelnetSession telnetsessionX = (TelnetSession)ses;
                int k = telnetsessionX.showPromptPage("Error!", x_message, dialogPageButtons);
                if (k == 0) {
                    ses.setRefreshScreen(true);
                } else {
                    return;
                }
            }
        } catch (Exception e) {
            ses.setStatusMessage((new StringBuilder()).append(pg.WMS_TXN_ERROR).append(" (").append(e).append(")").toString());
            UtilFns.error((new StringBuilder()).append("Error in createLocator: error - ").append(e).toString());
        }
    }
    
    /*************************************************************************
    *   NAME:     public void getDefaultPrinter(MWAEvent mwaevent) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Gets the default printer value for a user
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       25-JAN-2016   Lee Spitzer             Initial Version - 
                                                           TMS Ticket 20151103-00188 - RF Mass Bin Locating Screen   
     
    **************************************************************************/
    
    public String getDefaultPrinter(String p_profile_option_name) {
        String x_printer = "";
        String gMethod = "getDefaultPrinter";
        String gCallPoint = "Start";
        try{
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            cstmt = con.prepareCall("{? = call XXWC_LABEL_PRINTERS_PKG.get_default_printer(?)}");
            cstmt.registerOutParameter(1, Types.VARCHAR);
            cstmt.setString(2, p_profile_option_name);
            cstmt.execute();
            x_printer = cstmt.getString(1);
            cstmt.close();
            if (x_printer.equals(null) || x_printer.equals("")) {
                x_printer = "";
            }
        }
        catch (Exception e){
            UtilFns.error("Error in calling getDefaultPrinter " + e);
        }
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_printer " + x_printer); //added 02/16/2016
        return x_printer;
    }
   
}
