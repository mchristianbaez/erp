/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header APPS.XXWC_AIS_LOG_MV_N1 $
  Module Name: APPS.XXWC_AIS_LOG_MV_N1

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     14-JUN-2017   Pahwa, Nancy                Initially Created 
TMS# 20170613-00289
**************************************************************************/
create index APPS.XXWC_AIS_LOG_MV_N1 on XXWC_AIS_LOG_MV(creation_date);