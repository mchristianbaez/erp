/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header apps.view XXWC_AIS_LOG_MV $
  Module Name: XXWC_AIS_LOG_MV

  PURPOSE:Grant on apps.org_organization_definitions to EA_APEX

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     13-Jun-2017   Pahwa, Nancy                Initially Created 
TMS# 20170613-00289
**************************************************************************/
create materialized view APPS.XXWC_AIS_LOG_MV
REFRESH COMPLETE start with trunc(sysdate)+6/24
as 
select * from XXWC.XXWC_AIS_SEARCH_ARCH_TBL a
                         WHERE a.creation_date between
                              sysdate - 35 and
                               sysdate + 1;