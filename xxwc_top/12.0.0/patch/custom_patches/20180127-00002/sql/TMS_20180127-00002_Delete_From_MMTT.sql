/*
Task ID: 20180127-00002 Delete already processed but still stuck two records from pending trasactions form.
 */
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before delete');

   DELETE FROM apps.mtl_material_transactions_temp
   WHERE organization_id=528
     AND transaction_temp_id IN (698869328,698862989);
   
   DBMS_OUTPUT.put_line ('Records deleted-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to delete ' || SQLERRM);
END;
/