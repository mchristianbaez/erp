---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_PENDING_RTV_RPT_V $
  REVISIONS   :
  VERSION 		DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.1     	   12-Dec-2016        	Siva   		 TMS#20161219-00121   
  1.2		   17-Oct-2017			Siva		 TMS#20170927-00065 
**************************************************************************************************************/
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_PENDING_RTV_RPT_V
(
   vendor_num
  ,wc_return_number
  ,created_by
  ,supplier
  ,restocking_fee
  ,creation_date
  ,status
  ,notes
  ,return_reason
  ,return_number
  ,error_message
  ,employee_id
  ,buyer_id
  ,buyer_name
  ,org_code
  ,cdr_amt
  ,rtv_amt
  ,days_past_creation_date
  ,late_y_n
)
AS
     SELECT a.vendor_num
           ,a.wc_return_number
           ,a.created_by
           ,a.supplier
           ,a.restocking_fee
           ,a.creation_date
           ,a.status
           ,a.notes
           ,a.return_reason
           ,a.return_number
           ,a.error_message
           ,a.employee_id
           ,a.buyer_id
           ,a.buyer_name
           ,a.org_code
           ,to_number(a.cdr_amt) cdr_amt  --changed for version 1.1
           ,SUM (a.rtv_amt)
		   ,a.days_past_creation_date   --added for version 1.2
           ,(case when a.days_past_creation_date >14
                  then 'Yes'
                  else 'No'
                  end)late_y_n --added for version 1.2
       FROM (SELECT sup.segment1 vendor_num
                   ,rtv.wc_return_number wc_return_number
                   ,fu.description created_by
                   ,sup.vendor_name supplier
                   ,rtv.restocking_fee restocking_fee
                   ,TRUNC (rtv.creation_date) creation_date
                   ,rtv.rtv_status_code status
                   ,rtv.notes notes
                   ,rtvl.return_reason return_reason
                   ,rtv.return_number return_number
                   ,rtv.error_message error_message
                   ,fu.employee_id employee_id
                   ,rtv.buyer_id
                   ,fu1.description buyer_name
                   ,ood.organization_code org_code
                   ,DECODE (
                       rtv.rtv_status_code
                      ,'Pending', NULL
                      , (SELECT SUM (ail.amount)
                           FROM apps.ap_invoice_lines ail, apps.ap_invoices aia, mtl_material_transactions mmt
                          WHERE     1 = 1
                                AND aia.invoice_id = ail.invoice_id
                                AND ail.attribute1 = TO_CHAR (mmt.transaction_id)
                                AND aia.vendor_id = sup.vendor_id
                                AND mmt.inventory_item_id = rtvl.inventory_item_id
                                AND mmt.organization_id = ood.organization_id
                                AND aia.invoice_type_lookup_code IN ('CREDIT', 'DEBIT')))
                       cdr_amt  --changed for version 1.1
                   , (rtvl.return_quantity * rtvl.return_unit_price) rtv_amt
				   , (trunc(sysdate) - TRUNC(rtv.creation_date) )days_past_creation_date   --added for version 1.2
               FROM xxwc.xxwc_po_rtv_headers_tbl rtv
                   ,xxwc.xxwc_po_rtv_lines_tbl rtvl
                   ,org_organization_definitions ood
                   ,ap.ap_suppliers sup
                   ,apps.fnd_user fu
                   ,apps.fnd_user fu1
              WHERE     1 = 1
                    AND rtv.rtv_header_id = rtvl.rtv_header_id
                    AND ood.organization_id = rtv.ship_from_branch_id
                    AND rtv.supplier_id = sup.vendor_id
                    AND rtv.created_by = fu.user_id
                    AND fu1.user_id = rtv.buyer_id) a
   GROUP BY vendor_num
           ,wc_return_number
           ,created_by
           ,supplier
           ,restocking_fee
           ,creation_date
           ,status
           ,notes
           ,return_reason
           ,return_number
           ,error_message
           ,employee_id
           ,buyer_id
           ,buyer_name
           ,org_code
           ,to_number(A.cdr_amt) --changed for version 1.1
		   ,days_past_creation_date  --added for version 1.2
;
