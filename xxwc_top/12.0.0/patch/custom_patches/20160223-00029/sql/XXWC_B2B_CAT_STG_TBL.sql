/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC.XXWC_B2B_CAT_STG_TBL $
  Module Name: XXWC.XXWC_B2B_CAT_STG_TBL
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     14-FEB-2016   Pahwa, Nancy                Initially Created TMS# 20160223-00029 
**************************************************************************/
-- Create table
CREATE TABLE  "XXWC"."XXWC_B2B_CAT_STG_TBL" 
   (  "ID" NUMBER, 
  "ITEM_NUMBER" VARCHAR2(30), 
  "ICC" VARCHAR2(50), 
  "ATTR_GROUP_DISP_NAME" VARCHAR2(80), 
  "ATTR_DISPLAY_NAME" VARCHAR2(80), 
  "ATTRIBUTE_VALUE" VARCHAR2(3000), 
  "PROCESSED_DATE" DATE, 
  "PROCESS_STATUS" VARCHAR2(20), 
  "CREATED_BY" NUMBER, 
  "ERROR_MESSAGE" VARCHAR2(2000), 
  "CREATION_DATE" DATE, 
  "LAST_UPDATED_BY" NUMBER, 
  "LAST_UPDATED_DATE" DATE, 
  "BATCH_NUMBER" NUMBER, 
  "DESCRIPTION" VARCHAR2(2000), 
   CONSTRAINT "XXWC_B2B_CAT_STG_PK" PRIMARY KEY ("ID")
  USING INDEX  ENABLE
   );
/