/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC_B2B_CAT_ITEMS_MV$
  Module Name: XXWC_B2B_CAT_ITEMS_MV
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     14-FEB-2016   Pahwa, Nancy                Initially Created TMS# 20160223-00029 
**************************************************************************/
GRANT SELECT ON "APPS"."XXWC_B2B_CAT_ITEMS_MV" TO "EA_APEX";