/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC_B2B_CAT_UPLOAD_TBL $
  Module Name: XXWC_B2B_CAT_UPLOAD_TBL
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     14-FEB-2016   Pahwa, Nancy                Initially Created TMS# 20160223-00029 
**************************************************************************/
-- Create table
create table XXWC.XXWC_B2B_CAT_UPLOAD_TBL
(
  upload_id    NUMBER not null,
  doc_name     VARCHAR2(225),
  doc_size     NUMBER,
  blob_content BLOB,
  mime_type    VARCHAR2(225),
  created_by   VARCHAR2(50),
  created_on   DATE,
  updated_by   VARCHAR2(50),
  updated_on   DATE,
  description  VARCHAR2(225)
)
tablespace XXWC_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
/