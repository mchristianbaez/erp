/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header Alter XXWC.XXWC_B2B_CAT_ATTR_TBL $
  Module Name: XXWC.XXWC_B2B_CAT_ATTR_TBL
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     22-FEB-2016   Pahwa, Nancy                Initially Created TMS# 20160223-00029  
**************************************************************************/
alter table XXWC.XXWC_B2B_CAT_ATTR_TBL
  add constraint XXWC_B2B_CAT_ATTR_PK primary key (ID);