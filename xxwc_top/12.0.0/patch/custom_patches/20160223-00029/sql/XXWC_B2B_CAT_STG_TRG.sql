CREATE OR REPLACE TRIGGER  "XXWC"."XXWC_B2B_CAT_STG_TRG" 
  before insert on "XXWC"."XXWC_B2B_CAT_STG_TBL"               
  for each row  
/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC_B2B_CAT_STG_TRG $
  Module Name: XXWC_B2B_CAT_STG_TRG
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     14-FEB-2016   Pahwa, Nancy                Initially Created TMS# 20160223-00029 
**************************************************************************/
begin   
  if :NEW."ID" is null then 
    select "XXWC_B2B_CAT_STG_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end; 
/