--DROP MATERIALIZED VIEW APPS.XXWC_B2B_CAT_ITEMS_MV;
create materialized view APPS.XXWC_B2B_CAT_ITEMS_MV
refresh force on demand
start with to_date('02-06-2016', 'dd-mm-yyyy') next trunc(SYSDATE)+1  
as
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header APPS.XXWC_B2B_CAT_ITEMS_MV$
  Module Name: APPS.XXWC_B2B_CAT_ITEMS_MV

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-Mar-2016   Pahwa, Nancy                Initially Created 
TMS# 20160223-00029   
**************************************************************************/
select inventory_item_id, item_name, item_number,item_catalog_group_id from XXWC_B2B_CAT_ITEMS_VW;
/