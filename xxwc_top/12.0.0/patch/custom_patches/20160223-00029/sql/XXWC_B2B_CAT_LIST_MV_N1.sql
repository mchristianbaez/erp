/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC_B2B_CAT_LIST_MV_N1
  Module Name: XXWC_B2B_CAT_LIST_MV_N1
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     14-FEB-2016   Pahwa, Nancy                Initially Created TMS# 20160223-00029 
**************************************************************************/
-- Create/Recreate indexes 
CREATE INDEX "APPS"."XXWC_B2B_CAT_LIST_MV_N1" ON "APPS"."XXWC_B2B_CAT_LIST_MV" ("ITEM_CATALOG_GROUP_ID");