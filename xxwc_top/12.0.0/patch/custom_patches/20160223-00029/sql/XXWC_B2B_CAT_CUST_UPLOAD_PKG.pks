CREATE OR REPLACE PACKAGE APPS.XXWC_B2B_CAT_CUST_UPLOAD_PKG AS
  -- -----------------------------------------------------------------------------
  -- � Copyright 2008, Nancy Pahwa
  -- All Rights Reserved
  --
  -- Name           : insert_data
  -- Date Written   : 11-May-2016
  -- Author         : Nancy Pahwa
  --
  -- Modification History:
  --
  -- When         Who        Did what
  -- -----------  --------   -----------------------------------------------------
  -- 11-May-2016  nancypahwa   Initially Created TMS# 20160223-00029 
  ---------------------------------------------------------------------------------
  procedure customer_catalog_upload(p_file in number /*,
                                                                                                                                                                                                                                                                                                                                                                                                            p_customer_id       in number,
                                                                                                                                                                                                                                                                                                                                                                                                            p_hash_key          in number,
                                                                                                                                                                                                                                                                                                                                                                                                            p_cust_catalog_name varchar2*/);

  function hex_to_decimal(p_hex_str in varchar2) return number;
  FUNCTION get_customer_id(p_customer_name IN varchar2) return number;
  FUNCTION get_cat_group_id(p_item_number IN varchar2) return number;
  FUNCTION get_cat_inventory_item_id(p_item_number IN varchar2) return number;
  PROCEDURE XXWC_B2B_FILE_UPLOAD_PRC(p_filename     IN VARCHAR2,
                                     p_directory    IN VARCHAR2,
                                     p_location     IN VARCHAR2 DEFAULT NULL,
                                     p_new_filename OUT VARCHAR2);
end XXWC_B2B_CAT_CUST_UPLOAD_PKG;
/
