/*************************************************************************
  $Header Datafix_TMS_20161216_00197_Close_Order.sql $
  Module Name: TMS_20161216_00197


  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
   1.0       17-JAN-2016  Niraj K Ranjan        TMS#20161216_00197   ORDER 15389168 and 15595835
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20161216_00197    , Before Update');

   update apps.oe_order_lines_all
   set 
     INVOICE_INTERFACE_STATUS_CODE='NOT_ELIGIBLE'
    ,open_flag='N'
    ,flow_status_code='CLOSED'
   where line_id =48379947
   and headeR_id=29388409;

   DBMS_OUTPUT.put_line (
         'TMS: 20161216_00197  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20161216_00197    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161216_00197 , Errors : ' || SQLERRM);
END;
/
