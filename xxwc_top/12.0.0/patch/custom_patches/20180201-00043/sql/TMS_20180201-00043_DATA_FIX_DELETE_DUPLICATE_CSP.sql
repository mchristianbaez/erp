/******************************************************************************
   NAME:       TMS_20180201-00043_DATA_FIX_DELETE_DUPLICATE_CSP.sql
   PURPOSE:    Delete IN PROGRESS records of csp which are duplicate
   
   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        16/01/2018  Niraj K Ranjan   TMS#20180201-00043   Agreement 11407
******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20180201-00043   , Before Update');

   DELETE FROM apps.xxwc_om_csp_notifications_tbl WHERE ROWID IN
     (SELECT aa.rowid FROM 
      apps.xxwc_om_csp_notifications_tbl aa,
      (SELECT MAX(submitted_date) submitted_date,agreement_id,revision_number 
      FROM apps.xxwc_om_csp_notifications_tbl
      WHERE 1=1
	  AND  AGREEMENT_ID in (20039, 431535 ,11407,440531,24584) --127,1,62
      AND  agreement_status = 'IN PROGRESS'
      GROUP BY agreement_id,revision_number
      HAVING COUNT(1) > 1) bb
      WHERE aa.agreement_id = bb.agreement_id
      AND aa.revision_number = bb.revision_number
      AND aa.submitted_date <> bb.submitted_date);
   DBMS_OUTPUT.put_line ('TMS: 20180201-00043    , Total records deleted: '|| SQL%ROWCOUNT);
   UPDATE XXWC_OM_CSP_NOTIFICATIONS_TBL xocn
   SET  agreement_status = 'AWAITING_APPROVAL' 
   WHERE 1=1
   AND   xocn.agreement_id = 20039
   AND   xocn.revision_number = 127
   AND   agreement_status IN ('IN PROGRESS');
   DBMS_OUTPUT.put_line ('TMS: 20180201-00043    , Total records Updated for agreement id 20039: '|| SQL%ROWCOUNT);
   UPDATE XXWC_OM_CSP_NOTIFICATIONS_TBL xocn
   SET  agreement_status = 'AWAITING_APPROVAL' 
   WHERE 1=1
   AND   xocn.agreement_id = 431535
   AND   xocn.revision_number = 1
   AND   agreement_status IN ('IN PROGRESS');
   DBMS_OUTPUT.put_line ('TMS: 20180201-00043    , Total records Updated for agreement id 431535: '|| SQL%ROWCOUNT);
   UPDATE XXWC_OM_CSP_NOTIFICATIONS_TBL xocn
   SET  agreement_status = 'AWAITING_APPROVAL' 
   WHERE 1=1
   AND   xocn.agreement_id = 11407
   AND   xocn.revision_number = 62
   AND   agreement_status IN ('IN PROGRESS');
   DBMS_OUTPUT.put_line ('TMS: 20180201-00043    , Total records Updated for agreement id 11407: '|| SQL%ROWCOUNT);
   UPDATE XXWC_OM_CSP_NOTIFICATIONS_TBL xocn
   SET  agreement_status = 'AWAITING_APPROVAL' 
   WHERE 1=1
   AND   xocn.agreement_id = 440531
   AND   xocn.revision_number = 3
   AND   agreement_status IN ('IN PROGRESS');
   DBMS_OUTPUT.put_line ('TMS: 20180201-00043    , Total records Updated for agreement id 440531: '|| SQL%ROWCOUNT);
   UPDATE XXWC_OM_CSP_NOTIFICATIONS_TBL xocn
   SET  agreement_status = 'AWAITING_APPROVAL' 
   WHERE 1=1
   AND   xocn.agreement_id = 24584
   AND   xocn.revision_number = 12
   AND   agreement_status IN ('IN PROGRESS');
   DBMS_OUTPUT.put_line ('TMS: 20180201-00043    , Total records Updated for agreement id 24584: '|| SQL%ROWCOUNT);
   COMMIT;
   DBMS_OUTPUT.put_line ('TMS: 20180201-00043    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20180201-00043 , Errors : ' || SQLERRM);
END;
/
