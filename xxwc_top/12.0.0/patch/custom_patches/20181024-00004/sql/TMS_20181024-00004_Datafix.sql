/***********************************************************************************************************************************************
   NAME:       TMS_20181024-00004_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        10/24/2018  Rakesh Patel     TMS#20181024-00004_Datafix
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

CREATE TABLE xxwc.xla_events_bkp AS SELECT xe.*
       FROM xla_events xe, xla_accounting_errors xae
      WHERE xe.application_id = 707
        AND xae.application_id = 707
        AND xe.event_id = xae.event_id
        AND xe.upg_batch_id IS NULL
        AND xae.message_number = 95325
        AND xae.ledger_id = 2061
        AND xe.event_status_code = 'U'
        AND xe.process_status_code = 'I'
/

BEGIN
   DBMS_OUTPUT.put_line ('before update');
	
  UPDATE xla_events
     SET EVENT_DATE = TO_DATE ('9/24/2018', 'MM/DD/YYYY'),
         transaction_date =
          TO_DATE ('9/24/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'),
       REFERENCE_DATE_1 =
          TO_DATE ('9/24/2018 00:00:00', 'MM/DD/YYYY HH24:MI:SS'),
       LAST_UPDATE_DATE = SYSDATE,
       LAST_UPDATED_BY = 1290
   WHERE application_id = 707 AND Event_id IN (SELECT event_id FROM XXWC.xla_events_BKP );

   DBMS_OUTPUT.put_line ('Records Update -' || SQL%ROWCOUNT);
   
   COMMIT;
   
   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to insert record ' || SQLERRM);
	  ROLLBACK;
END;
/