/*
 TMS: 20160908-00190 / ESMS 474526
 Date: 09/08/2016
 Scope: @ GSC for i-Expense Mobile SSO issue - validate SSO credentials using LDAP API in Oracle OID and EBS
*/
set serveroutput on size 1000000;
declare
 --
     procedure print_log(p_message in varchar2) is
     begin
      if apps.fnd_global.conc_request_id >0 then
       apps.fnd_file.put_line(fnd_file.log, p_message);
      else
       dbms_output.put_line(p_message);
      end if;
     end;
 --
begin
     --
     print_log('');
     --      
            print_log(' Begin SQL -1');        
     begin 
           --
            if fnd_ldap_wrapper.validate_login ('MM027735','MikeTest1') then
               print_log ('VALID');
            else
                print_log ('Invalid password');
            end if;           
           --
     exception    
      when others then
       print_log('@ SQL 1, when-others, error ='||sqlerrm);       
     end;
            print_log(' End SQL -1');     
    --  
            print_log(' Begin SQL -2');        
     begin 
           --
                if fnd_user_pkg.validatelogin ('MM027735','MikeTest1') then
                  print_log ('VALID');
                else
                  print_log ('Invalid password');
                end if;       
           --
     exception    
      when others then
       print_log('@ SQL 2, when-others, error ='||sqlerrm);  
     end;
            print_log(' End SQL -2');     
    --    
            print_log(' Begin SQL -3');        
     begin 
           --
                if fnd_user_pkg.validatessologin ('MM027735','MikeTest1') then
                  print_log ('VALID');
                else
                  print_log ('Invalid password');
                end if;       
           --
     exception    
      when others then
       print_log('@ SQL 3, when-others, error ='||sqlerrm);  
     end;
            print_log(' End SQL -3');     
    --      
     print_log('');     
     --    
exception
 when others then
  rollback to square1;
  print_log('Outer block, message ='||sqlerrm);
end;
/