/* ************************************************************************
  $Header TMS_20170630-00118_header_close.sql $
  Module Name: TMS_20170630-00118 Data Fix script

  PURPOSE: Data Fix script 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        11-JUL-2017  Pattabhi Avula        TMS#20170630-00118

************************************************************************* */ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170630-00118   , Before Update');
   
UPDATE oe_order_headers_all
   SET open_flag = 'N',
       flow_status_code = 'CLOSED'
 WHERE header_id = 42644559; 

 DBMS_OUTPUT.put_line (
         'TMS: 20170630-00118 Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170630-00118   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170630-00118, Errors : ' || SQLERRM);
END;
/