/*
-- *********************************************************************************************************************************
-- $Header XXWC_INV_RTV_REPORT_DATA_TBL $
-- Module Name: XXWC_INV_RTV_REPORT_DATA_TBL

-- REVISIONS:
-- Ver        Date        Author           Description
-- ---------  ----------  ----------       ----------------
-- 1.0       04/13/2018  P.Vamshidhar      Initial Build - Task ID: 20170817-00112 - Receipt Traveler Enhancement Request
-- *********************************************************************************************************************************/
CREATE INDEX XXWC.XXWC_INV_RTV_REPORT_DATA_B_N1
   ON XXWC.XXWC_INV_RTV_REPORT_DATA_B (CONC_REQUEST_ID);


