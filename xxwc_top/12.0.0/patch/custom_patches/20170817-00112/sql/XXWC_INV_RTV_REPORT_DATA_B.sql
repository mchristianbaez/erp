/*
-- *********************************************************************************************************************************
-- $Header XXWC_INV_RTV_REPORT_DATA_TBL $
-- Module Name: XXWC_INV_RTV_REPORT_DATA_TBL

-- REVISIONS:
-- Ver        Date        Author           Description
-- ---------  ----------  ----------       ----------------
-- 1.0       03/07/2018  P.Vamshidhar      Initial Build - Task ID: 20170817-00112 - Receipt Traveler Enhancement Request
-- *********************************************************************************************************************************/
DROP TABLE XXWC.XXWC_INV_RTV_REPORT_DATA_B
/
CREATE TABLE XXWC.XXWC_INV_RTV_REPORT_DATA_B
(
  RECEIPT_NUM         VARCHAR2(30 BYTE),
  URL                 VARCHAR2(240 BYTE),
  SHIP_TO_ORG_ID      NUMBER,
  QTY_RECEIVED        NUMBER,
  RECEIVED_QTY        NUMBER,
  ORDER_QUANTITY_UOM  VARCHAR2(3 BYTE),
  ITEM_ID             NUMBER,
  RCT_QTY_RECEIVED    NUMBER,
  TRANSACTION_DATE    VARCHAR2(10 BYTE),
  PO_NUMBER           VARCHAR2(20 BYTE),
  CREATION_DATE       VARCHAR2(10 BYTE),
  RECEIVER            VARCHAR2(240 BYTE),
  BUYER               VARCHAR2(720 BYTE),
  EMAIL_ADDRESS       VARCHAR2(240 BYTE),
  SUPPLIER            VARCHAR2(240 BYTE),
  ORDER_NUMBER        NUMBER,
  ORGANIZATION_NAME   VARCHAR2(240 BYTE)        NOT NULL,
  ITEM                VARCHAR2(40 BYTE),
  DESCRIPTION         VARCHAR2(240 BYTE),
  SHIP_FROM_ORG_ID    NUMBER,
  SOLD_TO_ORG_ID      NUMBER,
  INVENTORY_ITEM_ID   NUMBER,
  ORGANIZATION_ID     NUMBER,
  ORDERED_QUANTITY    NUMBER,
  PO_LINE_ID          NUMBER,
  SHIPMENT_HEADER_ID  NUMBER                    NOT NULL,
  HEADER_ID           NUMBER,
  UOM_CODE            VARCHAR2(3 BYTE),
  GROUP_ID            NUMBER,
  ORG_ID              NUMBER,
  SHIPMENT_LINE_ID    NUMBER                    NOT NULL,
  REQ_DATE            VARCHAR2(11),
  LINE_ID             NUMBER,
  ADJ_QTY             NUMBER,
  CONC_REQUEST_ID     NUMBER
)
/