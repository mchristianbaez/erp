/***********************************************************************************************************************************************
   NAME:     TMS_20180413-00224_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        04/16/2018  Rakesh Patel     TMS#20180413-00224
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

   update xxwc.xxwc_sr_salesreps
      set prism_sr_number = '22709'
    where SALESREP_ID = 100152254;
 
   DBMS_OUTPUT.put_line ('Records Updated -' || SQL%ROWCOUNT);
 
   update xxwc.xxwc_sr_salesreps
   set prism_sr_number = '6763'
   where SALESREP_ID = 100000463; 
   
   DBMS_OUTPUT.put_line ('Records Updated -' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to Updated record ' || SQLERRM);
	  ROLLBACK;
END;
/