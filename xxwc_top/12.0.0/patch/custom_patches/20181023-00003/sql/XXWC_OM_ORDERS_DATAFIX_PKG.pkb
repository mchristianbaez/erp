CREATE OR REPLACE PACKAGE BODY xxwc_om_orders_datafix_pkg
AS
   /********************************************************************************************************************************
      $Header XXWC_OM_ORDERS_DATAFIX_PKG.PKG $
      Module Name: XXWC_OM_ORDERS_DATAFIX_PKG.PKG

      PURPOSE:   This package is used for data fix of order management

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------------------------------------------------------------
      1.0        22-Sep-2016 Niraj K Ranjan          Initial Version - TMS#20160911-00002   Data fix scripts into Concurrent program
	  
	  2.0        09-Aug-2017 Sundaramoorthy R        Intial Version -TMS #20170419-00233  Automate Frequently Running DataFix Scripts
	  3.0        26-Mar-2018 Pattabhi Avula          TMS#20171221-00087 -- New cursor added	
	  4.0        23-Oct-2018 Sundaramoorthy         TMS#20181023-00003  --New procedure for BPA Fix
   *********************************************************************************************************************************/

   --Email Defaults
   g_dflt_email   fnd_user.email_address%TYPE
                     := 'WC-ITDEVALERTS-U1@HDSupply.com';
   g_pkg_name     VARCHAR2 (50) := 'XXWC_OM_ORDERS_DATAFIX_PKG';

   /*************************************************************************
      PROCEDURE Name: Update

      PURPOSE:   Update line attributes to process stucked order line

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        18-May-2016    Niraj             Initial Version
   ****************************************************************************/
   PROCEDURE update_record_status ( p_order_header_id  IN NUMBER
                                   ,p_order_line_id    IN NUMBER
                                   ,p_request_id       IN NUMBER
								   ,p_data_fix_script  IN VARCHAR2
                                   ,p_status           IN VARCHAR2
								   ,p_errmsg           IN VARCHAR2
                                  )
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
	  l_errmsg VARCHAR2(2000);
   BEGIN
      fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG , 'Start of Procedure : update_record_status'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      UPDATE xxwc_om_orders_datafix_log_tbl
	  SET  STATUS           = p_status,
	       ERROR_MESSAGE    = p_errmsg,
		   LAST_UPDATE_DATE = SYSDATE,
		   LAST_UPDATED_BY  = FND_GLOBAL.USER_ID
	  WHERE ORDER_HEADER_ID = p_order_header_id
	  AND   ORDER_LINE_ID   = p_order_line_id
	  AND   REQUEST_ID      = p_request_id
	  AND   data_fix_script = p_data_fix_script;
      COMMIT;
	  fnd_file.put_line (fnd_file.LOG , 'End of Procedure : update_record_status'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
   EXCEPTION
      WHEN OTHERS THEN
	     ROLLBACK;
	     l_errmsg := SUBSTR(SQLERRM,1,2000);
	     fnd_file.put_line (fnd_file.LOG , 'When Others Error for header_id:'||p_order_header_id||' Line_id:'||p_order_line_id||'=>'||l_errmsg);
   END update_record_status;
    /*************************************************************************
      PROCEDURE Name: Correct_awaiting_Order

      PURPOSE:   Update line attributes to process stucked order line

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        18-May-2016    Niraj             Initial Version
   ****************************************************************************/
   PROCEDURE insert_record_log ( p_order_header_id IN  NUMBER
                                ,p_order_line_id   IN  NUMBER
                                ,p_request_id      IN  NUMBER
                                ,p_script_name     IN  VARCHAR2
                               )
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
	  l_errmsg VARCHAR2(2000);
   BEGIN
      fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG , 'Start of Procedure : insert_record_log'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      INSERT INTO xxwc_om_orders_datafix_log_tbl ( ORDER_HEADER_ID
                                         ,ORDER_LINE_ID
                                         ,REQUEST_ID
                                         ,DATA_FIX_SCRIPT
			                             ,STATUS
			                             ,ERROR_MESSAGE
			                             ,CREATION_DATE
			                             ,CREATED_BY
			                             ,LAST_UPDATE_DATE
			                             ,LAST_UPDATED_BY
			                            )
                                 VALUES (p_order_header_id,
                                         p_order_line_id,
                                         p_request_id,
									     p_script_name,
									     'Entered',
									     '',
                                         SYSDATE,
                                         FND_GLOBAL.USER_ID,
                                         SYSDATE,
                                         FND_GLOBAL.USER_ID);
	  COMMIT;
      fnd_file.put_line (fnd_file.LOG , 'End of Procedure : insert_record_log'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
   EXCEPTION
      WHEN OTHERS THEN
	     ROLLBACK;
	     l_errmsg := SUBSTR(SQLERRM,1,2000);
	     fnd_file.put_line (fnd_file.LOG , 'When Others Error for header_id:'||p_order_header_id||' Line_id:'||p_order_line_id||'=>'||l_errmsg);
   END insert_record_log;
   /*************************************************************************
      PROCEDURE Name: internal_order_stuck 

      PURPOSE:   Update line attributes to process stucked Internal Order Line

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      2.0        13-June-2017    Sundaramoorthy R  Initial Version for TMS #20170419-00233 
   ****************************************************************************/
   PROCEDURE internal_order_stuck ( p_exc_date  IN  VARCHAR2,
                                    p_errbuf    OUT VARCHAR2,
                                    p_retcode   OUT  VARCHAR2)
   IS
      e_user_exception          EXCEPTION;
      l_err_msg                 VARCHAR2(2000);
      l_sec                     VARCHAR2(150);
	  l_err_code                VARCHAR2(200);
	  l_line_id                 NUMBER;
	  l_ordered_quantity        NUMBER;
	  l_source_document_line_id NUMBER; 
	  l_transaction_quantity    NUMBER;
	  l_transaction_id          NUMBER;
	  l_request_id              NUMBER;
	  l_script_name             VARCHAR2(50);
	  l_order_details           VARCHAR2(150);	 
      l_internal_order          fnd_profile_option_values.profile_option_value%type :=fnd_profile.value('XXWC_INTERNAL_ORDER_LINE_TYPE');
	  l_locked_line             VARCHAR2(20);
	  l_updated_rowcount        NUMBER;
   
CURSOR cur_internal_order
	  IS
	  SELECT  oel.line_id,          
	          oel.ordered_quantity,
              ooh.header_id	,
              ooh.order_number,	
              oel.rowid line_rowid			  
	  FROM 
	   oe_order_lines_all oel,
	   oe_order_headers_all ooh  
	WHERE  1=1
          AND ooh.header_id=oel.header_id
          AND oel.flow_status_code='SHIPPED'
		  AND oel.open_flag ='Y'
		  AND ooh.flow_status_code NOT IN ('CANCELLED')   
          AND oel.line_type_id   = l_internal_order 
		  AND TRUNC(oel.last_update_date) = (TO_DATE(p_exc_date,'DD-MON-YY') -1 );
         		  
	BEGIN
	  fnd_file.put_line (fnd_file.LOG ,'=========================================================================================');
      fnd_file.put_line (fnd_file.LOG , 'Start of Procedure : internal_order_stuck ||Fix for Internal Order Stuck in Shipped Status'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      l_request_id := fnd_global.conc_request_id;
	  fnd_file.put_line (fnd_file.LOG , 'l_request_id:'||l_request_id);
	  fnd_file.put_line (fnd_file.LOG , 'p_exc_date:'||p_exc_date);     
      l_script_name := 'Internal Order Line stuck in Shipped Status';	 
	
	FOR rec_intrl IN cur_internal_order LOOP
	BEGIN
	    fnd_file.put_line (fnd_file.LOG , l_sec||'~'||'Order Number'||rec_intrl.order_number||'Line ID:'||rec_intrl.line_id);
	    l_order_details := 'Order Number'||rec_intrl.order_number||'~'||'Line ID:'||rec_intrl.line_id;
	    l_sec := l_script_name||' - '||'Check if record is locked';
		l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec_intrl.line_rowid, 'OE_ORDER_LINES_ALL'); 
       		
		IF l_locked_line = 'N' 
		THEN
			l_sec := 'Insert record for '||l_script_name;
		      insert_record_log( p_order_header_id   => rec_intrl.header_id
			                    ,p_order_line_id     => rec_intrl.line_id
			                    ,p_request_id        => l_request_id
			                    ,p_script_name       => l_script_name
			                   );	
			BEGIN 
		        l_sec :=l_script_name||'-'||'Checking Source Document ID';
		         SELECT  oel.source_document_line_id INTO l_source_document_line_id
                 FROM  oe_order_lines_all oel,
                       oe_order_headers_all oeh,
                       po_requisition_headers_all porh,
                       po_requisition_lines_all porl
                WHERE  oeh.header_id = oel.header_id
                AND    oel.source_document_id = porh.requisition_header_id
                AND    oel.source_document_line_id = porl.requisition_line_id
                AND    porh.requisition_header_id = porl.requisition_header_id
                AND    oel.line_id= rec_intrl.line_id
                AND    oel.org_id = porh.org_id;           		   
	         EXCEPTION
		      WHEN NO_DATA_FOUND THEN
		       	         l_err_msg := l_sec||' - '||'-Source Document ID does not exist'||rec_intrl.line_id;
		     	   	     RAISE e_user_exception;
		      WHEN OTHERS THEN
		                  l_err_msg := l_sec||'~'||l_order_details||'=>'||SUBSTR(SQLERRM,1,2000);
		     			 RAISE e_user_exception;
            END;
	 
	        BEGIN	
               l_sec :=l_script_name||'-'||'Checking the line is delivered from RECEIVING to INVENTORY';		  
                 SELECT transaction_id INTO l_transaction_id
	               FROM rcv_transactions 
	             WHERE 1=1
	               AND requisition_line_id=l_source_document_line_id 
	               AND transaction_type='DELIVER' 
	               AND destination_type_code='INVENTORY'; 
		      EXCEPTION
		      WHEN NO_DATA_FOUND THEN
		     	         l_err_msg := l_sec||' - '||'-line is not Delivered from “RECEIVING” to “INVENTORY” '||rec_intrl.line_id;
			      	     RAISE e_user_exception;
		      WHEN OTHERS THEN
		                l_err_msg := l_sec||'~'||l_order_details||'=>'||SUBSTR(SQLERRM,1,2000);
			   		    RAISE e_user_exception;
		    END;	 
	 
	    	BEGIN			
			  SELECT mmt.transaction_quantity into l_transaction_quantity
	           FROM mtl_material_transactions mmt 
	           WHERE rcv_transaction_id=l_transaction_id;
		        EXCEPTION
		        WHEN NO_DATA_FOUND THEN
		  	              l_err_msg := l_sec||' - '||'--line is not in MMT '||rec_intrl.line_id;
			        	     RAISE e_user_exception;
		        WHEN OTHERS THEN
		                  l_err_msg := l_sec||'~'||l_order_details||'=>'||SUBSTR(SQLERRM,1,2000);
					 fnd_file.put_line (fnd_file.LOG ,l_err_msg);   
					 RAISE e_user_exception;
	        END;
			
			   IF rec_intrl.ordered_quantity = l_transaction_quantity
			   THEN  	  	
			  	UPDATE apps.oe_order_lines_all
                   SET 
                     invoice_interface_status_code='NOT_ELIGIBLE',
                     open_flag='N',
                     flow_status_code='CLOSED'
                   WHERE line_id =rec_intrl.line_id
                   AND header_id=rec_intrl.header_id;
				   l_updated_rowcount := SQL%ROWCOUNT; 				   
				END IF;	
    fnd_file.put_line (fnd_file.LOG ,l_script_name||'~ Successfully Updated the Records for Header ID ~'||rec_intrl.header_id||'~Line ID ~'||rec_intrl.line_id);	
				IF l_updated_rowcount > 0 THEN
			       l_sec := l_script_name||' - '||'update_record_status - Success';
			       update_record_status( p_order_header_id => rec_intrl.header_id
			                            ,p_order_line_id   => rec_intrl.line_id
			                            ,p_request_id      => l_request_id
			     	 				     ,p_data_fix_script => l_script_name
			                            ,p_status          => 'Success'
			                            ,p_errmsg          => ''
			                           );
			    END IF;	 
			   COMMIT;
        END IF;		 
     EXCEPTION
	 WHEN e_user_exception THEN
		       ROLLBACK;
			   update_record_status( p_order_header_id => rec_intrl.header_id
			                        ,p_order_line_id   => rec_intrl.line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
        WHEN OTHERS THEN
		   ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   update_record_status( p_order_header_id => rec_intrl.header_id
			                        ,p_order_line_id   => rec_intrl.line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_script_name||' - '||l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';	
    END;
	END LOOP;	
        fnd_file.put_line (fnd_file.LOG , 'Completion of Procedure : internal_order_stuck'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
		fnd_file.put_line (fnd_file.LOG ,'=========================================================================================');		
	EXCEPTION 
	  WHEN OTHERS THEN
       p_errbuf := 'Error';
       p_retcode := '2';
	   l_err_code := SQLCODE;
       l_err_msg  := SUBSTR(SQLERRM,1,2000);
       fnd_file.put_line (fnd_file.LOG, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
       fnd_file.put_line (fnd_file.output, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
	   ROLLBACK;
       xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_ORDERS_DATAFIX_PKG.EXECUTE_DATA_FIXES'
           ,p_calling             => l_sec
           ,p_request_id          => l_request_id
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error while executing data fix'
           ,p_distribution_list   => g_dflt_email
           ,p_module              => 'XXWC');
		ROLLBACK;  
	 END internal_order_stuck;	
   
    /*************************************************************************
      PROCEDURE Name: Correct_awaiting_Order

      PURPOSE:   Update line attributes to process stucked order line

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        18-May-2016    Niraj             Initial Version	  
   ****************************************************************************/
   PROCEDURE execute_data_fixes ( p_errbuf         OUT VARCHAR2
                                 ,p_retcode        OUT NUMBER
                                 ,p_start_date     IN  VARCHAR2
                                 )
   IS
      e_user_exception         EXCEPTION;
      l_err_msg                VARCHAR2 (2000);
      l_err_code               VARCHAR2(200);
      l_sec                    VARCHAR2 (150);
	  l_request_id             NUMBER;
	  l_script_name            VARCHAR2(50);
	  l_execution_date         DATE;
	  l_delivery_detail_id     NUMBER;
	  l_updated_rowcount       NUMBER;
	  l_org_id                 NUMBER;
	  l_return_status          BOOLEAN;
	  l_locked_line            VARCHAR2(2);	  

	  --ITS_DELIVERY_CLOSED
	  CURSOR cr_its_del_closed(p_exc_date VARCHAR2)
	  IS
	     SELECT ool.header_id,ool.line_id,ool.flow_status_code,ool.rowid line_rowid
          FROM oe_order_lines ool
          WHERE  1=1
		  AND TRUNC(ool.last_update_date) = TO_DATE(p_exc_date,'DD-MON-YY')
          AND ool.ordered_quantity = 0
          AND ool.flow_status_code IN ('CANCELLED', 'AWAITING_SHIPPING','PICKED')
		  AND EXISTS(SELECT 1
                     FROM wsh_delivery_details wdd
                     WHERE wdd.source_line_id = ool.line_id
					 AND   wdd.released_status = 'C');

      --ITS_DELIVERY_CANCELED
      CURSOR cr_its_del_canceled(p_exc_date VARCHAR2)
	  IS
	     SELECT ool.header_id,ool.line_id,ool.flow_status_code,ool.rowid line_rowid
          FROM oe_order_lines ool
          WHERE  1=1
		  AND TRUNC(ool.last_update_date) = TO_DATE(p_exc_date,'DD-MON-YY')
          AND ool.ordered_quantity = 0
          AND ool.flow_status_code IN ('AWAITING_SHIPPING')
		  AND EXISTS(SELECT 1
                     FROM wsh_delivery_details wdd
                     WHERE wdd.source_line_id = ool.line_id
					 AND   wdd.released_status = 'D');

      --ITS_INTERFACED_FLAG
      CURSOR cr_interfaced_flag(p_exc_date VARCHAR2)
	  IS
	     SELECT ool.header_id,ool.line_id,ool.flow_status_code,ool.rowid line_rowid
         FROM  oe_order_lines ool
         WHERE 1=1
         AND   TRUNC(ool.last_update_date) = TO_DATE(p_exc_date,'DD-MON-YY')
         AND   ool.flow_status_code NOT IN ('AWAITING_SHIPPING',
                                            'BOOKED',
                                            'SHIPPED',
											'CANCELLED')
         AND   EXISTS(SELECT 1 FROM apps.wsh_delivery_Details wdd
                      WHERE wdd.source_line_id   = ool.line_id
                      AND wdd.oe_interfaced_flag = 'N'
                      AND wdd.released_status    = 'C'
                     );

	  --DROP_SHIP_LINE_FIX
      CURSOR cr_drop_ship_line_fix(p_exc_date VARCHAR2)
	  IS
	      SELECT ool.header_id,ool.line_id,ool.rowid line_rowid
          FROM oe_order_lines ool
          WHERE 1 = 1
          AND   TRUNC(ool.last_update_date) = TO_DATE(p_exc_date,'DD-MON-YY')
          AND   ool.flow_status_code = 'BOOKED'
          AND   EXISTS(SELECT 1
                    FROM rcv_transactions rcv,
                         po_line_locations poll,
                         oe_drop_ship_sources oed
                   WHERE    ool.source_type_code = 'EXTERNAL'
                         AND NVL (ool.shipped_quantity, 0) = 0
                         AND oed.line_id = ool.line_id
                         AND oed.line_location_id IS NOT NULL
                         AND poll.line_location_id = oed.line_location_id
                         AND rcv.po_line_location_id = poll.line_location_id
                         AND rcv.TRANSACTION_TYPE = 'DELIVER');

      CURSOR pending_receipts(p_order_line_id NUMBER)
      IS
         SELECT rcv.transaction_id
           FROM rcv_transactions rcv,
                po_line_locations poll,
                oe_drop_ship_sources oed,
                oe_order_lines oel
          WHERE     oel.source_type_code = 'EXTERNAL'
                AND NVL (oel.shipped_quantity, 0) = 0
                AND oed.line_id = oel.line_id
                AND oed.line_location_id IS NOT NULL
                AND poll.line_location_id = oed.line_location_id
                AND rcv.po_line_location_id = poll.line_location_id
                AND oel.line_id = p_order_line_id
                AND rcv.TRANSACTION_TYPE = 'DELIVER';

      --AWAITING_INVOICE_FIX
	  CURSOR cr_awaiting_invoice_fix(p_exc_date VARCHAR2)
      IS
	  SELECT ool.header_id,ool.line_id,ool.rowid line_rowid
      FROM   apps.oe_order_lines ool,apps.oe_order_headers ooh
      WHERE  1 = 1
      AND    ool.header_id = ooh.header_id
      AND   TRUNC(ool.last_update_date) >= TO_DATE(p_exc_date,'DD-MON-YY')
      AND   ool.flow_status_code = 'INVOICE_HOLD'
      AND   EXISTS(SELECT  /*+ INDEX(rctl XXWC_RA_CUST_TRX_LINES_N13)*/ 1
                   FROM apps.ra_customer_trx_lines rctl
                   WHERE rctl.interface_line_attribute6 = to_char(ool.line_id)
                   AND   rctl.interface_line_attribute1 = to_char(ooh.order_number)
                  );
	  /*SELECT ool.header_id,ool.line_id,ool.rowid line_rowid
      FROM   oe_order_lines_all ool
      WHERE  1 = 1
      AND   TRUNC(ool.last_update_date) = TO_DATE(p_exc_date,'DD-MON-YY')
      AND   ool.flow_status_code = 'INVOICE_HOLD'
      AND   EXISTS(SELECT  /*+ INDEX(rctl XXWC_RA_CUST_TRX_LINES_N14)*/ --1
                   /*FROM ra_customer_trx_lines_all rctl
                   WHERE rctl.interface_line_attribute6 = to_char(ool.line_id)                  );*/
	 
   BEGIN
      fnd_file.put_line (fnd_file.LOG ,'=========================================================================================');
      fnd_file.put_line (fnd_file.LOG , 'Start of Procedure : execute_data_fixes'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      l_request_id := fnd_global.conc_request_id;
	  fnd_file.put_line (fnd_file.LOG , 'l_request_id:'||l_request_id);
	  fnd_file.put_line (fnd_file.LOG , 'p_start_date:'||p_start_date);
	  l_execution_date := fnd_date.canonical_to_date(p_start_date);
	  fnd_file.put_line (fnd_file.LOG , 'l_execution_date:'||l_execution_date);

      l_sec := 'Set policy context - 162';
      apps.mo_global.set_policy_context('S',162);
	  
	  --Ver 1.0 for TMS #20170419-00233
	  -----------------------------------------------------------------------------------------------------------------------------
	  --Call for Procedure Procedure internal_order_stuck
	  -----------------------------------------------------------------------------------------------------------------------------
	  internal_order_stuck ( p_errbuf    => l_err_msg,
                             p_retcode   => l_err_code,
							 p_exc_date  => l_execution_date);
      p_errbuf  := l_err_msg;
	  p_retcode := l_err_code;
	  ---------------------------------------------------------------------------------------------------------------------------
	  --End call for Procedure internal_order_stuck
	  ---------------------------------------------------------------------------------------------------------------------------
	  ---------------------------------------------------------------------------------------------------------------------------
	  --Call for Procedure Procedure header_status_entered
	  ---------------------------------------------------------------------------------------------------------------------------
      header_status_entered (p_errbuf    => l_err_msg,
                             p_retcode   => l_err_code);
      p_errbuf  := l_err_msg;
	  p_retcode := l_err_code;
	  ---------------------------------------------------------------------------------------------------------------------------
	  --End call for Procedure internal_order_stuck
	  ---------------------------------------------------------------------------------------------------------------------------
	  ---------------------------------------------------------------------------------------------------------------------------
	  --Call for Procedure Procedure RENTAL_LINE_RECEIVED
	  -----------------------------------------------------------------------------------------------------------------------------
	  RETURN_LINE_RECEIVED ( p_errbuf    => l_err_msg,
                             p_retcode   => l_err_code);
      p_errbuf  := l_err_msg;
	  p_retcode := l_err_code;
	  ---------------------------------------------------------------------------------------------------------------------------
	  --End call for Procedure RENTAL_LINE_RECEIVED
	  ---------------------------------------------------------------------------------------------------------------------------
	  -- End of Ver 1.0 for TMS #20170419-00233
	  ---------------------------------------------------------------------------------------------------------------------------
	  --Start of ver 4.0 
	  ---------------------------------------------------------------------------------------------------------------------------
	  --Call for Procedure Procedure BPA_OPEN
	  -----------------------------------------------------------------------------------------------------------------------------
	  BPA_OPEN ( p_errbuf    => l_err_msg,
                 p_retcode   => l_err_code);
      p_errbuf  := l_err_msg;
	  p_retcode := l_err_code;
	  ---------------------------------------------------------------------------------------------------------------------------
	  --End call for Procedure BPA_OPEN
	  ---------------------------------------------------------------------------------------------------------------------------
	  -- End of Ver 4.0 for TMS #20181023-00003 
	  ---------------------------------------------------------------------------------------------------------------------------
	  
	  --1.1	Line has been cancelled, however the delivery has been created and trying to ship for ZERO quantity, line status in
	  --    Picked/Cancelled status (Interface trip stops completes warning) - ITS_DELIVERY_CLOSED
	  ---------------------------------------------------------------------------------------------------------------------------
	  l_script_name := 'ITS_DELIVERY_CLOSED';
	  l_sec := 'Start loop cr_its_del_closed';
	  fnd_file.put_line (fnd_file.LOG , 'Start of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
	  FOR rec_its_del_closed IN cr_its_del_closed(l_execution_date)
	  LOOP
	     BEGIN
            l_sec := l_script_name||' - '||'Check if record is locked';
		    l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec_its_del_closed.line_rowid, 'OE_ORDER_LINES_ALL');
			IF l_locked_line = 'N' THEN
		       l_sec := 'Insert record for '||l_script_name;
		       insert_record_log( p_order_header_id   => rec_its_del_closed.header_id
			                     ,p_order_line_id     => rec_its_del_closed.line_id
			                     ,p_request_id        => l_request_id
			                     ,p_script_name       => l_script_name
			                    );
	           BEGIN
			      l_sec := l_script_name||' - '||'Fetch delivery_detail_id';
	              SELECT delivery_detail_id INTO l_delivery_detail_id
                    FROM wsh_delivery_details wdd
                   WHERE wdd.source_line_id = rec_its_del_closed.line_id;
		       EXCEPTION
		          WHEN NO_DATA_FOUND THEN
		  	         l_err_msg := l_sec||' - '||'Delivery not found for line id:'||rec_its_del_closed.line_id;
			   	     RAISE e_user_exception;
				  WHEN TOO_MANY_ROWS THEN
				     l_err_msg := l_sec||' - '||'More than one delivery id found for line id:'||rec_its_del_closed.line_id;
			   	     RAISE e_user_exception;
		       END;
			   l_updated_rowcount := 0;
			   l_sec := 'UPDATE apps.wsh_delivery_details';
               UPDATE apps.wsh_delivery_details
                  SET released_status = 'D',
                      src_requested_quantity = 0,
                      requested_quantity = 0,
                      shipped_quantity = 0,
                      cycle_count_quantity = 0,
                      cancelled_quantity = 0,
                      subinventory = NULL,
                      locator_id = NULL,
                      lot_number = NULL,
                      revision = NULL,
                      inv_interfaced_flag = 'X',
                      oe_interfaced_flag = 'X'
                WHERE delivery_detail_id = l_delivery_detail_id;
			    l_updated_rowcount := SQL%ROWCOUNT;

			    IF l_updated_rowcount > 0 THEN
			       l_updated_rowcount := 0;
			       l_sec := 'UPDATE apps.wsh_delivery_assignments';
			       UPDATE apps.wsh_delivery_assignments
                     SET delivery_id = NULL, parent_delivery_detail_id = NULL
                   WHERE delivery_detail_id = l_delivery_detail_id;
			       l_updated_rowcount := SQL%ROWCOUNT;
                END IF;

                IF rec_its_del_closed.flow_status_code = 'AWAITING_SHIPPING' AND l_updated_rowcount > 0 THEN
                   l_sec := 'UPDATE apps.oe_order_lines_all';
                   UPDATE apps.oe_order_lines_all
                    SET flow_status_code = 'CANCELLED', cancelled_flag = 'Y'
                    WHERE line_id = rec_its_del_closed.line_id
			   	 AND header_id = rec_its_del_closed.header_id
                    AND ordered_quantity = 0
                    AND flow_status_code <> 'CANCELLED';
			   	   l_updated_rowcount := SQL%ROWCOUNT;
			    END IF;

			    IF l_updated_rowcount > 0 THEN
			       l_sec := l_script_name||' - '||'update_record_status - Success';
			       update_record_status( p_order_header_id => rec_its_del_closed.header_id
			                            ,p_order_line_id   => rec_its_del_closed.line_id
			                            ,p_request_id      => l_request_id
			     	 				     ,p_data_fix_script => l_script_name
			                            ,p_status          => 'Success'
			                            ,p_errmsg          => ''
			                           );
			    END IF;
			    COMMIT;
            END IF;
         EXCEPTION
		    WHEN e_user_exception THEN
		       ROLLBACK;
			   update_record_status( p_order_header_id => rec_its_del_closed.header_id
			                        ,p_order_line_id   => rec_its_del_closed.line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
		    WHEN OTHERS THEN
			   ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   update_record_status( p_order_header_id => rec_its_del_closed.header_id
			                        ,p_order_line_id   => rec_its_del_closed.line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_script_name||' - '||l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
		 END;
	  END LOOP;
      fnd_file.put_line (fnd_file.LOG , 'End of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

	  ---------------------------------------------------------------------------------------------------------------------------
	  --1.2	Line has been cancelled, however the delivery has been created and trying to ship for ZERO quantity, line status in
	  --    Picked/Cancelled status (Interface trip stops completes warning) - ITS_DELIVERY_CANCELED
	  ---------------------------------------------------------------------------------------------------------------------------
	  l_script_name := 'ITS_DELIVERY_CANCELED';
	  l_sec := 'Start loop cr_its_del_closed';
	  fnd_file.put_line (fnd_file.LOG , 'Start of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
	  FOR rec_its_del_canceled IN cr_its_del_canceled(l_execution_date)
	  LOOP
	     BEGIN
            l_sec := l_script_name||' - '||'Check if record is locked';
		    l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec_its_del_canceled.line_rowid, 'OE_ORDER_LINES_ALL');
			IF l_locked_line = 'N' THEN
		       l_sec := 'Insert record for '||l_script_name;
		       insert_record_log( p_order_header_id   => rec_its_del_canceled.header_id
			                     ,p_order_line_id     => rec_its_del_canceled.line_id
			                     ,p_request_id        => l_request_id
			                     ,p_script_name       => l_script_name
			                    );

               l_sec := l_script_name||' - '||'UPDATE apps.oe_order_lines_all';
               UPDATE apps.oe_order_lines_all
               SET flow_status_code = 'CANCELLED', cancelled_flag = 'Y'
               WHERE line_id = rec_its_del_canceled.line_id
               AND ordered_quantity = 0
               AND flow_status_code <> 'CANCELLED';
			   l_updated_rowcount := SQL%ROWCOUNT;

			   IF l_updated_rowcount > 0 THEN
			      l_sec := l_script_name||' - '||'update_record_status - Success';
			      update_record_status( p_order_header_id => rec_its_del_canceled.header_id
			                           ,p_order_line_id   => rec_its_del_canceled.line_id
			                           ,p_request_id      => l_request_id
			    	 				     ,p_data_fix_script => l_script_name
			                           ,p_status          => 'Success'
			                           ,p_errmsg          => ''
			                          );
			   END IF;
			   COMMIT;
            END IF;
         EXCEPTION
		    WHEN OTHERS THEN
			   ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   update_record_status( p_order_header_id => rec_its_del_canceled.header_id
			                        ,p_order_line_id   => rec_its_del_canceled.line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_script_name||' - '||l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
		 END;
	  END LOOP;
      fnd_file.put_line (fnd_file.LOG , 'End of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

	  ------------------------------------------------------------------------------------------------------------------------------
	  --2.	Line stuck in Picked status and the associated delivery detaild id has the OE_INTERFACED_FLAG set to N
	  --    (Interface trip stops completes warning) (ITS_INTERFACED_FLAG)
	  ------------------------------------------------------------------------------------------------------------------------------
	  l_script_name := 'ITS_INTERFACED_FLAG';
	  l_sec := 'Start loop cr_interfaced_flag';
	  fnd_file.put_line (fnd_file.LOG , 'Start of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
	  FOR rec_interfaced_flag IN cr_interfaced_flag(l_execution_date)
	  LOOP
	     BEGIN
            l_sec := l_script_name||' - '||'Check if record is locked';
		    l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec_interfaced_flag.line_rowid, 'OE_ORDER_LINES_ALL');
			IF l_locked_line = 'N' THEN
		       l_sec := 'Insert record for '||l_script_name;
		       insert_record_log( p_order_header_id   => rec_interfaced_flag.header_id
			                     ,p_order_line_id     => rec_interfaced_flag.line_id
			                     ,p_request_id        => l_request_id
			                     ,p_script_name       => l_script_name
			                    );
			   l_updated_rowcount := 0;
			   l_sec := l_script_name||' - '||'UPDATE apps.wsh_delivery_details';
               UPDATE apps.wsh_delivery_Details
               SET oe_interfaced_flag = 'Y'
               WHERE source_line_id     = rec_interfaced_flag.line_id
               AND oe_interfaced_flag = 'N'
               AND released_status    = 'C';
               l_updated_rowcount := SQL%ROWCOUNT;
			   IF l_updated_rowcount > 0 THEN
			      l_sec := l_script_name||' - '||'update_record_status - Success';
			      update_record_status( p_order_header_id => rec_interfaced_flag.header_id
			                           ,p_order_line_id   => rec_interfaced_flag.line_id
			                           ,p_request_id      => l_request_id
			      				        ,p_data_fix_script => l_script_name
			                           ,p_status          => 'Success'
			                           ,p_errmsg          => ''
			                          );
			   END IF;
			   COMMIT;
            END IF;
         EXCEPTION
		    WHEN OTHERS THEN
		       ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   update_record_status( p_order_header_id => rec_interfaced_flag.header_id
			                        ,p_order_line_id   => rec_interfaced_flag.line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_script_name||' - '||l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
		 END;
      END LOOP;
	  fnd_file.put_line (fnd_file.LOG , 'End of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

	  ---------------------------------------------------------------------------------------------------------------------------
       --3. Line has been received, however the associated sales order line has been stuck in BOOKED status - DROP_SHIP_LINE_FIX
      ---------------------------------------------------------------------------------------------------------------------------
	  l_script_name := 'DROP_SHIP_LINE_FIX';
	  l_sec := 'Start loop cr_drop_ship_line_fix';
	  fnd_file.put_line (fnd_file.LOG , 'Start of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
	  FOR rec_drop_ship_line_fix IN cr_drop_ship_line_fix(l_execution_date)
	  LOOP
	     BEGIN
            l_sec := l_script_name||' - '||'Check if record is locked';
		    l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec_drop_ship_line_fix.line_rowid, 'OE_ORDER_LINES_ALL');
			IF l_locked_line = 'N' THEN
		       l_sec := 'Insert record for '||l_script_name;
		       insert_record_log( p_order_header_id   => rec_drop_ship_line_fix.header_id
			                     ,p_order_line_id     => rec_drop_ship_line_fix.line_id
			                     ,p_request_id        => l_request_id
			                     ,p_script_name       => l_script_name
			                    );

			   l_updated_rowcount := 0;
			   l_sec := l_script_name||' - '||'UPDATE oe_order_lines_all';
               UPDATE oe_order_lines_all oeol
                  SET oeol.flow_status_code = 'AWAITING_RECEIPT'
                WHERE     oeol.line_id = rec_drop_ship_line_fix.line_id
                  AND oeol.flow_status_code <> 'AWAITING_RECEIPT';

			   l_updated_rowcount := SQL%ROWCOUNT;

			   IF l_updated_rowcount > 0 THEN
                  l_sec := l_script_name||' - '||'Fetch cursor pending_receipts';
			      FOR all_lines IN pending_receipts(rec_drop_ship_line_fix.line_id)
                  LOOP
                     l_return_status := OE_DS_PVT.DROPSHIPRECEIVE (all_lines.transaction_id, 'INV');
                  END LOOP;
			   END IF;
			   IF  l_return_status = TRUE THEN
			       l_sec := l_script_name||' - '||'update_record_status - Success';
			       update_record_status( p_order_header_id => rec_drop_ship_line_fix.header_id
			                            ,p_order_line_id   => rec_drop_ship_line_fix.line_id
			                            ,p_request_id      => l_request_id
			     	 				     ,p_data_fix_script => l_script_name
			                            ,p_status          => 'Success'
			                            ,p_errmsg          => ''
			                           );
               ELSE
			      l_sec := l_script_name||' - '||'update_record_status - Error';
			       update_record_status( p_order_header_id => rec_drop_ship_line_fix.header_id
			                            ,p_order_line_id   => rec_drop_ship_line_fix.line_id
			                            ,p_request_id      => l_request_id
			     	 				     ,p_data_fix_script => l_script_name
			                            ,p_status          => 'Error'
			                            ,p_errmsg          => 'OE_DS_PVT.DROPSHIPRECEIVE has not executed successfully'
			                           );
			   END IF;
               COMMIT;
            END IF;
         EXCEPTION
		    WHEN OTHERS THEN
		       ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   update_record_status( p_order_header_id => rec_drop_ship_line_fix.header_id
			                        ,p_order_line_id   => rec_drop_ship_line_fix.line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_script_name||' - '||l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
		 END;
      END LOOP;
	  fnd_file.put_line (fnd_file.LOG , 'End of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

	  ------------------------------------------------------------------------------------------------------------------------
	  --4.	Line is shipped, invoiced and the workflow is closed, however the line status is not closed - AWAITING_INVOICE_FIX
	  ------------------------------------------------------------------------------------------------------------------------
	  l_script_name := 'AWAITING_INVOICE_FIX';
	  l_sec := 'Start loop cr_awaiting_invoice_fix';
	  fnd_file.put_line (fnd_file.LOG , 'Start of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
	  FOR rec_awaiting_invoice_fix IN cr_awaiting_invoice_fix(l_execution_date)
	  LOOP
	     BEGIN
            l_sec := l_script_name||' - '||'Check if record is locked';
		    l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec_awaiting_invoice_fix.line_rowid, 'OE_ORDER_LINES_ALL');
			IF l_locked_line = 'N' THEN
		       l_sec := 'Insert record for '||l_script_name;
		       insert_record_log( p_order_header_id   => rec_awaiting_invoice_fix.header_id
			                     ,p_order_line_id     => rec_awaiting_invoice_fix.line_id
			                     ,p_request_id        => l_request_id
			                     ,p_script_name       => l_script_name
			                    );

			   l_updated_rowcount := 0;
			   l_sec := l_script_name||' - '||'UPDATE oe_order_lines_all';
               UPDATE apps.oe_order_lines_all ool
                  SET ool.invoice_interface_status_code = 'YES',
                      ool.open_flag = 'N',
                      ool.flow_status_code = 'CLOSED',
                      ool.invoiced_quantity = ool.shipped_quantity
                WHERE     line_id = rec_awaiting_invoice_fix.line_id
                AND ool.flow_status_code = 'INVOICE_HOLD';

			   l_updated_rowcount := SQL%ROWCOUNT;

		       IF l_updated_rowcount > 0 THEN
			       l_sec := l_script_name||' - '||'update_record_status - Success';
			       update_record_status( p_order_header_id => rec_awaiting_invoice_fix.header_id
			                            ,p_order_line_id   => rec_awaiting_invoice_fix.line_id
			                            ,p_request_id      => l_request_id
			     	 				     ,p_data_fix_script => l_script_name
			                            ,p_status          => 'Success'
			                            ,p_errmsg          => ''
			                           );
			   END IF;
			   COMMIT;
            END IF;
         EXCEPTION
		    WHEN OTHERS THEN
		       ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   update_record_status( p_order_header_id => rec_awaiting_invoice_fix.header_id
			                        ,p_order_line_id   => rec_awaiting_invoice_fix.line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_script_name||' - '||l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
		 END;
      END LOOP;
	 
   END execute_data_fixes;
    /*************************************************************************
      PROCEDURE Name: execute_rental_line_fixes

      PURPOSE:  DataFix Automation for Rental Line not splitting 

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      2.0        21-June-2017  Sundaramoorthy     Initial Version - TMS #20170419-00233 
	  2.1        04-Oct-2017   Sundaramoorthy     TMS #20171004-00085 - Adding Workflow debug 
	  
	  ************************************************************************/     
   PROCEDURE execute_rental_line_fixes(
                                         p_errbuf      OUT VARCHAR2 
                                        ,p_retcode     OUT NUMBER 
										,p_order_number IN NUMBER
                                        ,p_line_id     IN NUMBER 
                                        ,p_new_line_id IN NUMBER 
										,p_quantity    In NUMBER
								        )
       IS
           e_user_exception        EXCEPTION;
           l_err_msg               VARCHAR2 (2000);
		   l_old_header_id         oe_order_lines_all.header_id%type; 
		   l_old_ordered_qty       oe_order_lines_all.ordered_quantity%type;
		   l_new_unit_list_price   oe_order_lines_all.unit_list_price%type;
		   l_new_header_id         oe_order_lines_all.header_id%type; 
		   l_link_to_line_id       oe_order_lines_all.link_to_line_id%type;
		   l_return_attribute1     oe_order_lines_all.return_attribute1%type;
		   l_return_attribute2     oe_order_lines_all.return_attribute2%type;
		   l_old_line_number       oe_order_lines_all.line_number%type;
		   l_new_line_number       oe_order_lines_all.line_number%type;
		   l_order_number          oe_order_headers_all.order_number%type;
		   l_request_id            NUMBER;
	       l_script_name           VARCHAR2(50);
		   l_old_locked_line       VARCHAR2(20);
		   l_new_locked_line       VARCHAR2(20);
		   l_sec                   VARCHAR2(200);
		   l_log_line_id           NUMBER;
		   l_new_line_rowid        VARCHAR2(50);
		   l_old_line_rowid        VARCHAR2(50);
		   l_updated_rowcount      NUMBER;
		   l_result                VARCHAR2(30);    --ver 2.1
	  
	 BEGIN
	    fnd_file.put_line (fnd_file.LOG ,'=========================================================================================');
        fnd_file.put_line (fnd_file.LOG , 'Start of Program : XXWC OM Rental Line Split Data Fix Automation'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
        l_request_id := fnd_global.conc_request_id;
	    fnd_file.put_line (fnd_file.LOG , 'l_request_id:'||l_request_id);
		 l_script_name := 'Rental Line Not Splitting';
	    l_sec := 'Set policy context - 162';	   
        apps.mo_global.set_policy_context('S',162);
	        
			l_sec := 'Insert record for Old Line ID '||l_script_name;
		       insert_record_log( p_order_header_id   => NULL
			                     ,p_order_line_id     => p_line_id
			                     ,p_request_id        => l_request_id
			                     ,p_script_name       => l_script_name
			                    );
			
            l_sec := 'Insert record for New Line ID '||l_script_name;
		       insert_record_log( p_order_header_id   => NULL
			                     ,p_order_line_id     => p_new_line_id
			                     ,p_request_id        => l_request_id
			                     ,p_script_name       => l_script_name
			                    );								
	   
		        BEGIN
			     l_sec := l_script_name||' - '||'Fetch Header Details for Old Line Number';
				   l_log_line_id := p_line_id;
		           SELECT oola.header_id 
                       , oola.ordered_quantity 
                       , oola.ROWID
					   , oola.line_number
					   , oola.unit_list_price 
			           , oola.link_to_line_id 
                       , oola.return_attribute1 
                       , oola.return_attribute2
					   , oeh.order_number
                     INTO l_old_header_id
					     , l_old_ordered_qty
					     , l_old_line_rowid
						 , l_old_line_number
					     , l_new_unit_list_price
						 , l_link_to_line_id
						 , l_return_attribute1
						 , l_return_attribute2
						 , l_order_number
                     FROM  oe_order_lines_all oola ,
                           oe_order_headers_all oeh   
                     WHERE oola.header_id            = oeh.header_id
                     AND oola.line_id                = p_line_id
                     AND oola.open_flag              ='Y'
                     AND oola.flow_status_code       ='AWAITING_RETURN'
                     AND EXISTS (SELECT 1
                                     FROM oe_transaction_types_tl
                                     WHERE     language = 'US'
                                     AND name IN ('WC LONG TERM RENTAL', 'WC SHORT TERM RENTAL') 
                                     AND transaction_type_id = oeh.order_type_id) ; 
			      EXCEPTION
		        	 WHEN NO_DATA_FOUND THEN
		          	         l_err_msg := l_sec||' - '||'Header Details does not match required condition ~ Line Number'||l_old_line_number;
		        	   	     RAISE e_user_exception;
		             WHEN OTHERS THEN
		        		     l_err_msg := l_sec||'=>'||SUBSTR(SQLERRM,1,2000);
		     			     RAISE e_user_exception;
		        END;
		
		        BEGIN
			    l_sec := l_script_name||' - '||'Fetch Header Details for Newly created Line Number';
				    l_log_line_id := p_new_line_id;
		            SELECT oola.rowid ,oola.line_number,header_id
				    INTO l_new_line_rowid ,l_new_line_number, l_new_header_id
                    FROM oe_order_lines_all oola
                    WHERE line_id=p_new_line_id;
				    EXCEPTION
		    	      WHEN NO_DATA_FOUND THEN
		      	             l_err_msg := l_sec||' - '||'The New Line does not Exist:'||l_new_line_number;
		    	       	     RAISE e_user_exception;
		              WHEN OTHERS THEN
		    	    	     l_err_msg := l_sec||'=>'||SUBSTR(SQLERRM,1,2000);
		     			     RAISE e_user_exception;
		        END;
		
		IF l_old_header_id = l_new_header_id AND  p_quantity < l_old_ordered_qty
		THEN
	         l_sec := l_script_name||' - '||'Check if Line ID record is locked';
		     l_old_locked_line := xxwc_om_force_ship_pkg.is_row_locked (l_old_line_rowid, 'OE_ORDER_LINES_ALL');
			 l_new_locked_line := xxwc_om_force_ship_pkg.is_row_locked (l_new_line_rowid, 'OE_ORDER_LINES_ALL');
		
            IF l_old_locked_line ='N' AND l_new_locked_line ='N'
			THEN
      --ver 2.1	Start		
			OE_Standard_WF.OEOL_SELECTOR ( p_itemtype => 'OEOL' 
			                              ,p_itemkey => TO_CHAR(p_line_id) 
			                              ,p_actid => 12345 
										  ,p_funcmode => 'SET_CTX' 
			                              ,p_result => l_result );
            oe_debug_pub.add('Return value of OEOL_selector function is'||l_result);
	   --ver 2.1 End			
			fnd_file.put_line (fnd_file.LOG ,'Updating Order Number~'||l_order_number);
			    l_sec := l_script_name||' - '||'Update Order Quantity';
		        l_updated_rowcount :=0;
		        UPDATE oe_order_lines_all
                SET ordered_quantity = p_quantity 
                WHERE line_id        = p_line_id;
		        l_updated_rowcount := SQL%ROWCOUNT;
			        
		        IF l_updated_rowcount > 0 
				THEN
		             l_updated_rowcount := 0;
                     UPDATE oe_order_lines_all
                     SET shipped_quantity= ordered_quantity,
                         fulfilled_quantity= ordered_quantity,
                         flow_status_code  = 'AWAITING_FULFILLMENT',
                         last_update_date  = SYSDATE,
                         last_updated_by   =FND_GLOBAL.USER_ID
                     WHERE line_id       = p_line_id
                     AND flow_status_code='AWAITING_RETURN';
			        l_updated_rowcount := SQL%ROWCOUNT;
				END IF; 
            fnd_file.put_line (fnd_file.LOG ,'Successfully Updated the Values for Old line Number ~'||l_old_line_number||'~ Line ID ~'||p_line_id);
		--ver 2.1 Start		
			wf_engine.handleError('OEOL', to_char(p_line_id),'RMA_WAIT_FOR_RECEIVING','SKIP','COMPLETE');

            wf_engine.handleError('OEOL', to_char(p_line_id),'RMA_WAIT_FOR_INSPECTION','SKIP','COMPLETE');
			oe_debug_pub.debug_off;
		--ver 2.1 End		
	            IF l_updated_rowcount > 0  
				THEN									   
				    l_sec := l_script_name||' - '||'Update New Line ID Attributes';
                    UPDATE oe_order_lines_all
                    SET   unit_list_price          = l_new_unit_list_price,
                          unit_list_price_per_pqty = NULL,
                          link_to_line_id          = l_link_to_line_id,  
                          return_attribute1        = l_return_attribute1,
                          return_attribute2        = l_return_attribute2
                    WHERE line_id                  = p_new_line_id;   
			fnd_file.put_line (fnd_file.LOG ,'Successfully Updated the Values for New line Number ~'||l_new_line_number||'~ Line ID ~'||p_new_line_id);		
		        END IF;
                COMMIT;
		    ELSE
			fnd_file.put_line (fnd_file.LOG , l_script_name||' - '||'Line is Locked from Updating');
              p_errbuf := 'Warning';
              p_retcode := '1';	
		    END IF; --Lock Condition status IF
		ELSE
         fnd_file.put_line (fnd_file.LOG , l_script_name||' - '||'Qty given is more than ordered qty');
              p_errbuf := 'Warning';
              p_retcode := '1';		
		END IF; -- Primary Condition Checking IF
		EXCEPTION
		    WHEN e_user_exception THEN
		       ROLLBACK;
			   update_record_status( p_order_header_id => NULL
			                        ,p_order_line_id   => l_log_line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
				
              fnd_file.put_line (fnd_file.LOG , l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
		    WHEN OTHERS THEN
			   ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   update_record_status( p_order_header_id => NULL
			                        ,p_order_line_id   => l_log_line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_script_name||' - '||l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
			  
	  fnd_file.put_line (fnd_file.LOG , l_err_msg); 
	  fnd_file.put_line (fnd_file.LOG , 'End of Program :XXWC OM Rental Line Split Data Fix Automation'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (fnd_file.LOG ,'==========================================================================================');
	  xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_ORDERS_DATAFIX_PKG.EXECUTE_RENTAL_LINE_FIXES'
           ,p_calling             => l_sec
           ,p_request_id          => l_request_id
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error while executing data fix'
           ,p_distribution_list   => g_dflt_email
           ,p_module              => 'XXWC');
	END EXECUTE_RENTAL_LINE_FIXES;
	/*************************************************************************
      PROCEDURE Name: pending_prebill_error

      PURPOSE: Pending Prebill Error Datafix Automation

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      2.0        04-July-2017  Sundaramoorthy     Initial Version - TMS #20170419-00233 
	  
  ************************************************************************/     
   PROCEDURE pending_prebill_datafix(
                                         p_errbuf      OUT VARCHAR2 
                                        ,p_retcode     OUT NUMBER 
                                        ,p_header_id  IN NUMBER 
                                        ,p_line_id   IN NUMBER
										,p_shipment_date IN VARCHAR2
								        )
       IS
           e_user_exception   EXCEPTION;
           l_err_msg          VARCHAR2 (2000);
		   l_header_id        oe_order_lines_all.header_id%type; 
		   l_line_id          oe_order_lines_all.line_id%type; 
		   l_order_type       fnd_profile_option_values.profile_option_value%type :=fnd_profile.value('XXWC_STANDARD_ORDER_TYPE');
		   l_order_number     NUMBER;
		   l_line_number      NUMBER;
		   l_locked_line      VARCHAR2(20);  
		   l_rowid            VARCHAR2(50);
		   l_script_name      VARCHAR2(50);
		   l_sec              VARCHAR2(200);
		   l_updated_rowcount NUMBER;
		   l_request_id       NUMBER;
		   l_shipment_date    DATE;
		   
	 BEGIN
	    fnd_file.put_line (fnd_file.LOG ,'=========================================================================================');
        fnd_file.put_line (fnd_file.LOG , 'Start of Program : XXWC OM Pending Prebill Data fix Automation'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
        l_request_id := fnd_global.conc_request_id;
	    fnd_file.put_line (fnd_file.LOG , 'l_request_id:'||l_request_id);
		 l_script_name := 'Pending Prebill Error Datafix';
	    l_sec := 'Set policy context - 162';	   
        apps.mo_global.set_policy_context('S',162);
		l_shipment_date := fnd_date.canonical_to_date(p_shipment_date);
				
		l_sec := 'Insert record for Header ID '||l_script_name;
		       insert_record_log( p_order_header_id   => p_header_id
			                     ,p_order_line_id     => p_line_id
			                     ,p_request_id        => l_request_id
			                     ,p_script_name       => l_script_name
			                    );		
		         BEGIN
				 l_sec := l_script_name||' - '||'Check For Pre Billing Acceptance Line';
		            SELECT oel.ROWID,oeh.order_number,oel.line_number
					  INTO l_rowid,l_order_number,l_line_number
                      FROM oe_order_lines_all oel,
					       oe_order_headers_all oeh
                      WHERE oel.header_id = oeh.header_id
					  AND oel.line_id = p_line_id
                      AND oel.header_id = p_header_id
					  AND oeh.order_type_id = l_order_type
					  AND oeh.open_flag = 'Y'
					  AND oel.open_flag = 'Y'
					  AND oel.flow_status_code ='PRE-BILLING_ACCEPTANCE';
		           EXCEPTION
		              WHEN NO_DATA_FOUND THEN
		                l_err_msg := l_sec||' - '||'Pre Billing Acceptance does not exist for given Line';
		                RAISE e_user_exception;
		              WHEN OTHERS THEN
		                l_err_msg := l_sec||'=>'||SUBSTR(SQLERRM,1,2000);
		                RAISE e_user_exception; 
	              END;    
							
          l_sec := l_script_name||' - '||'Check if Line ID record is locked';	
          l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (l_rowid, 'OE_ORDER_LINES_ALL');	

        IF 	l_locked_line ='N'
        THEN
            l_sec := l_script_name||' - '||'Updating Quantites';
		    l_updated_rowcount :=0;			

          UPDATE apps.oe_order_lines_all
            SET 
            shipping_quantity = ordered_quantity,
                shipped_quantity = ordered_quantity,
                actual_shipment_date = l_shipment_date, 
                shipping_quantity_uom = order_quantity_uom,
                fulfilled_flag = 'Y',
                fulfillment_date = l_shipment_date,
                fulfilled_quantity = ordered_quantity,
                last_updated_by = FND_GLOBAL.USER_ID,
                last_update_date = SYSDATE
            WHERE line_id = p_line_id 
            AND header_id= p_header_id; 
		  
		  l_updated_rowcount := SQL%ROWCOUNT;

		    IF l_updated_rowcount > 0 THEN
			   l_sec := l_script_name||' - '||'update_record_status - Success';
			    update_record_status( p_order_header_id => p_header_id
			                         ,p_order_line_id   => p_line_id
			                         ,p_request_id      => l_request_id
			    				     ,p_data_fix_script => l_script_name
			                         ,p_status          => 'Success'
			                         ,p_errmsg          => ''
			                        );
		fnd_file.put_line (fnd_file.LOG ,'Successfully Updated details for ~Order Number~'||l_order_number||'~ and Line  Number~'||l_line_number);						
		fnd_file.put_line (fnd_file.LOG ,'Successfully Updated details for ~Header ID~'||p_header_id||'~ and Line ID~'||p_line_id);		
			END IF;
			COMMIT;
			ELSE 
			fnd_file.put_line (fnd_file.LOG ,'Record is Locked cannot update Lines');
		END IF;	
	EXCEPTION
		    WHEN e_user_exception THEN
		       ROLLBACK;
			   update_record_status( p_order_header_id => p_header_id
			                        ,p_order_line_id   => p_line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
				
              fnd_file.put_line (fnd_file.LOG , l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
		    WHEN OTHERS THEN
			   ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   update_record_status( p_order_header_id => p_header_id
			                        ,p_order_line_id   => p_line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_script_name||' - '||l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
			  
	  fnd_file.put_line (fnd_file.LOG , l_err_msg); 
	  fnd_file.put_line (fnd_file.LOG , 'End of Program :XXWC OM Pending Prebill Data fix Automation'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (fnd_file.LOG ,'==========================================================================================');
	  xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_ORDERS_DATAFIX_PKG.PENDING_PREBILL_DATAFIX'
           ,p_calling             => l_sec
           ,p_request_id          => l_request_id
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error while executing data fix'
           ,p_distribution_list   => g_dflt_email
           ,p_module              => 'XXWC');
	
   END PENDING_PREBILL_DATAFIX; 

/******************************************************************************
  $Header header_status_entered.prc $
  Module Name: 20170419-00233  Data Fix script automation

  PURPOSE: Data fix script automation for header status is in Entered,
           but line have been booked.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  2.0        03-JUL-2017  Pattabhi Avula        TMS#20170419-00233

*******************************************************************************/
PROCEDURE HEADER_STATUS_ENTERED(p_errbuf out varchar2, p_retcode out varchar2)
IS
v_package_name               VARCHAR2 (256) := 'xxwc_om_orders_datafix_pkg';
v_procedure_name             VARCHAR2 (256) := 'header_status_entered';
v_distro_list                VARCHAR2 (256) := 'HDSOracleDevelopers@hdsupply.com';
l_script_name                VARCHAR2 (256) := 'xxwc_om_orders_datafix_pkg.header_status_entered';
l_updated_rowcount           NUMBER (20);
l_sec                        VARCHAR2 (256);
l_request_id                 NUMBER(38):= fnd_global.conc_request_id;
l_locked_line                VARCHAR2(2);

CURSOR cur_header_enter
  IS
SELECT /*+ INDEX(oh XXWC_OE_ORDER_HEADERS_ALL_N9) */ oh.header_id
      ,oh.order_number
      ,rowid hdr_rowid
  FROM apps.oe_order_headers_all oh
WHERE oh.flow_status_code='ENTERED'
   AND oh.booked_flag='N'
   AND trunc(oh.creation_date) between TRUNC(sysdate) AND TRUNC(sysdate) - 10   
   AND OH.HEADER_ID IN (SELECT /*+ INDEX(oh OE_ORDER_LINES_N1) */ DISTINCT header_id 
                 FROM apps.oe_order_lines_all ol 
                WHERE 1=1 
                  AND ol.booked_flag='Y'
                  AND trunc(ol.last_update_date) between TRUNC(sysdate) AND TRUNC(sysdate) - 10
                  );
 
 BEGIN
  fnd_file.put_line (fnd_file.LOG ,'=========================================================================================');
  fnd_file.put_line (fnd_file.LOG , 'Start of Procedure :HEADER_STATUS_ENTERED ||Fix For:Sales Order Header in Entered Status and Sales Order Lines in Booked '||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
  fnd_file.put_line (fnd_file.LOG ,'Before for loop script execution');
 
 FOR I IN cur_header_enter 
   LOOP
   
   l_sec := l_script_name||' - '||'Check if record is locked';
		    l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (I.hdr_rowid, 'OE_ORDER_HEADERS_ALL');
			IF l_locked_line = 'N' THEN
		       l_sec := 'Insert record for '||l_script_name;
		      insert_record_log( p_order_header_id   => I.header_id
			                     ,p_order_line_id     => NULL
			                     ,p_request_id        => l_request_id
			                     ,p_script_name       => l_script_name
			                    );   
   UPDATE oe_order_headers_all 
      SET booked_flag = 'Y'
	     ,flow_status_code='BOOKED'
	WHERE header_id=I.header_id;	
	 fnd_file.put_line (fnd_file.LOG,'Updated the flow_status_code in oe_order_headers_all tab for order number '||I.order_number);
  END IF;
  END LOOP;
   l_updated_rowcount := SQL%ROWCOUNT;
   fnd_file.put_line (fnd_file.LOG,'No of rows updated is '||l_updated_rowcount);
  
  COMMIT;  
  fnd_file.put_line (fnd_file.LOG , 'End of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS')); 
  EXCEPTION
  WHEN OTHERS
  THEN
  ROLLBACK;
xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => v_package_name
               ,p_calling             => v_procedure_name
               ,p_request_id          => NULL
               ,p_ora_error_msg       => SUBSTR (p_errbuf, 1, 2000)
               ,p_error_desc          => 'Error running ' || v_package_name || '.' || v_procedure_name
               ,p_distribution_list   => v_distro_list
               ,p_module              => 'XXWC');

END HEADER_STATUS_ENTERED; 
/*************************************************************************
      PROCEDURE Name: RETURN_LINE_RECEIVED

      PURPOSE: Datafix Automation for Return Line Fully Received but shows Awaiting Return

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      2.0        13-July-2017  Sundaramoorthy     Initial Version - TMS #20170419-00233 
	  
  ************************************************************************/   
  PROCEDURE RETURN_LINE_RECEIVED  (  p_errbuf    OUT VARCHAR2,
                                     p_retcode   OUT  VARCHAR2)
   IS
        e_user_exception   EXCEPTION;
        l_err_msg          VARCHAR2 (2000);
		l_locked_line      VARCHAR2(20);  
		l_script_name      VARCHAR2(100);
		l_sec              VARCHAR2(200);
		l_updated_rowcount NUMBER;
		l_request_id       NUMBER;
		l_err_code         VARCHAR2(200);
		l_order_type        fnd_profile_option_values.profile_option_value%type :=fnd_profile.value('XXWC_RETURN_ORDER_TYPE');
   
   CURSOR cur_rental_line
      IS
        SELECT oel.line_id
              ,oel.header_id
			  ,/*+ INDEX(oeh XXWC_OE_ORDER_HEADERS_ALL_N9) */oeh.order_number
			  ,oel.line_number
			  ,oel.fulfilled_quantity
			  ,mmt.primary_quantity	
              ,oel.rowid			  
	    FROM 
		      oe_order_lines_all oel
			, oe_order_headers_all oeh
			, mtl_material_transactions mmt
			, rcv_transactions rt          
         WHERE oeh.org_id= fnd_profile.value('ORG_ID')
		 AND rt.oe_order_header_id = oeh.header_id 
         AND rt.oe_order_line_id = oel.line_id 
         AND mmt.rcv_transaction_id = rt.transaction_id
		 AND oel.inventory_item_id = mmt.inventory_item_id
       	 AND oeh.order_type_id = l_order_type
         AND oel.flow_status_code =  'AWAITING_RETURN'
		 AND oel.open_flag ='Y'
		 AND rt.source_document_code = 'RMA'
         AND oel.header_id = oeh.header_id         
		 AND NOT EXISTS (SELECT 1
                                     FROM oe_transaction_types_tl
                                     WHERE     language = 'US'
                                     AND name IN ('WC LONG TERM RENTAL', 'WC SHORT TERM RENTAL') 
                                     AND transaction_type_id = oeh.order_type_id) ;        
   
   BEGIN
	  fnd_file.put_line (fnd_file.LOG ,'=========================================================================================');
      fnd_file.put_line (fnd_file.LOG , 'Start of Procedure : RETURN_LINE_RECEIVED || Fix for: Return Line fully received but shows awaiting return'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      l_request_id := fnd_global.conc_request_id;
	  fnd_file.put_line (fnd_file.LOG , 'l_request_id:'||l_request_id);	    
      l_script_name := 'Rental Line Received Fully but shows Awaiting Return';	 
	
	FOR rec_rental IN cur_rental_line LOOP
	BEGIN
	    fnd_file.put_line (fnd_file.LOG ,'Order Number: '||rec_rental.order_number||' Line Number: '||rec_rental.line_number);
		l_sec :=NULL;
	    l_sec := l_script_name||' - '||'Check if record is locked';
		l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec_rental.rowid, 'OE_ORDER_LINES_ALL'); 
       		
		IF l_locked_line = 'N' 
		THEN
		    	l_sec := 'Insert record for '||l_script_name;
		      insert_record_log( p_order_header_id   => rec_rental.header_id
			                    ,p_order_line_id     => rec_rental.line_id
			                    ,p_request_id        => l_request_id
			                    ,p_script_name       => l_script_name
			                   );	
            
			IF rec_rental.fulfilled_quantity = rec_rental.primary_quantity
			THEN
			 l_sec := l_script_name||' - '||'Updating Line';
		     l_updated_rowcount :=0;	
            
            DELETE FROM oe_line_sets WHERE line_id = rec_rental.line_id;
            l_updated_rowcount := SQL%ROWCOUNT;	
			         
            IF l_updated_rowcount > 0
			THEN
			BEGIN
			fnd_file.put_line (fnd_file.LOG ,'Deleted the Record from oe_line_sets - Order Number: '||rec_rental.order_number||' and Line Number: '||rec_rental.line_number);
               UPDATE oe_order_lines_all
               SET    invoice_interface_status_code = NULL,
                      fulfilled_quantity            = ordered_quantity,
                      fulfilled_flag                = 'Y',
                      actual_fulfillment_date       = SYSDATE,
                      flow_status_code              = 'FULFILLED'
               WHERE  LINE_ID = rec_rental.line_id;
		  
		    l_updated_rowcount := SQL%ROWCOUNT;	
			EXCEPTION
			WHEN OTHERS THEN
			   ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   update_record_status( p_order_header_id => rec_rental.header_id
			                        ,p_order_line_id   => rec_rental.line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_script_name||' - '||l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
	        END;	
            ELSE
			fnd_file.put_line (fnd_file.LOG ,'Records not present in OE_LINE_SETS');
			END IF;
			
			IF l_updated_rowcount > 0 THEN
			    l_sec := l_script_name||' - '||'update_record_status - Success';
			    update_record_status( p_order_header_id => rec_rental.header_id
			                         ,p_order_line_id   => rec_rental.line_id
			                         ,p_request_id      => l_request_id
			    				     ,p_data_fix_script => l_script_name
			                         ,p_status          => 'Success'
			                         ,p_errmsg          => ''
			                        );
		fnd_file.put_line (fnd_file.LOG ,'Successfully Updated details for Order Number: '||rec_rental.order_number||' and Line Number: '||rec_rental.line_number);					
		fnd_file.put_line (fnd_file.LOG ,'Successfully Updated details for Header ID: '||rec_rental.header_id||' and Line ID: '||rec_rental.line_id);		
			END IF;
			COMMIT;
			ELSE
			fnd_file.put_line (fnd_file.LOG ,'Fulfilled Qty is not matching the Orimary qty in MMT');			
			END IF; --Condition IF
			
            ELSE
			fnd_file.put_line (fnd_file.LOG ,'Record is Locked for Order Number: '||rec_rental.order_number||' and Line Number: '||rec_rental.line_number);			
		END IF;	 --Lock Status Check
	EXCEPTION
		    WHEN OTHERS THEN
			   ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   update_record_status( p_order_header_id => rec_rental.header_id
			                        ,p_order_line_id   => rec_rental.line_id
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_script_name||' - '||l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
	END;
	END LOOP;	
    	
	    fnd_file.put_line (fnd_file.LOG , 'Completion of Procedure : RETURN_LINE_RECEIVED'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
		fnd_file.put_line (fnd_file.LOG ,'=========================================================================================');		
	EXCEPTION 
	  WHEN OTHERS THEN
       p_errbuf := 'Error';
       p_retcode := '2';
	   l_err_code := SQLCODE;
       l_err_msg  := SUBSTR(SQLERRM,1,2000);
       fnd_file.put_line (fnd_file.LOG, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
       fnd_file.put_line (fnd_file.output, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
	   ROLLBACK;
       xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_ORDERS_DATAFIX_PKG.RETURN_LINE_RECEIVED'
           ,p_calling             => l_sec
           ,p_request_id          => l_request_id
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error while executing data fix'
           ,p_distribution_list   => g_dflt_email
           ,p_module              => 'XXWC');
		ROLLBACK;  
	 END RETURN_LINE_RECEIVED;	
	 
/******************************************************************************
  $Header HEADER_NOT_CLOSED.prc $
  Module Name: 20171221-00087  Data Fix script automation

  PURPOSE: Data fix script automation for header not closed even all lines are,
           closed or cancelled. Handled only STANDARD,COUNTER and INTERNAL orders

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  3.0        26-Mar-2018  Pattabhi Avula      TMS#20171221-00087 -- New cursor added	

*******************************************************************************/
PROCEDURE HEADER_NOT_CLOSED(p_errbuf out varchar2, p_retcode out varchar2)
IS
v_package_name               VARCHAR2 (256) := 'xxwc_om_orders_datafix_pkg';
v_procedure_name             VARCHAR2 (256) := 'HEADER_NOT_CLOSED';
v_distro_list                VARCHAR2 (256) := 'HDSOracleDevelopers@hdsupply.com';
l_script_name                VARCHAR2 (256) := 'XXWC_OM_ORDERS_DATAFIX_PKG.HEADER_NOT_CLOSED';
l_updated_rowcount           NUMBER (20);
l_sec                        VARCHAR2 (256);
l_request_id                 NUMBER(38):= fnd_global.conc_request_id;
l_locked_line                VARCHAR2(2);
l_count                      NUMBER(10):=0;
l_max_date                   DATE;
l_curr_date                  DATE DEFAULT SYSDATE-7;
l_err_msg                    VARCHAR2 (2000);
l_err_code                   VARCHAR2(200);
e_user_exception             EXCEPTION;

 	  
-- Header Status in Booked, but all Lines are CLOSED or CANCELLED
-- Handled only STANDARD,COUNTER and INTERNAL orders
-- HEADER_NOT_CLOSED_EVEN_LINES_CLOSED
CURSOR cur_orders_booked
     IS   
      SELECT rowid hdr_rowid, oh.header_id
        FROM apps.oe_order_headers_all oh 
       WHERE oh.flow_status_code = 'BOOKED' 
      AND oh.order_type_id IN (1001,1011,1004)
      AND TRUNC(oh.last_update_date) <= TRUNC(SYSDATE-7) 
         AND EXISTS (SELECT 1
                       FROM apps.wf_items wi
                      WHERE wi.item_key(+) = oh.header_id
                        AND wi.item_type = 'OEOH'
                        AND  wi.end_date IS NULL)
         AND NOT EXISTS(SELECT 1
                          FROM apps.oe_order_lines_all ol   
                         WHERE ol.header_id=oh.header_id        
                           AND ol.flow_status_code NOT IN ('CLOSED','CANCELLED'))
      UNION
      SELECT rowid hdr_rowid, oh.header_id 
        FROM apps.oe_order_headers_all oh
       WHERE oh.flow_status_code = 'BOOKED'
      AND oh.order_type_id IN (1001,1011,1004)
         AND TRUNC(oh.last_update_date) <= TRUNC(SYSDATE-7)	
         AND NOT EXISTS (SELECT 1
                           FROM APPS.wf_items wi
                          WHERE WI.item_key(+) = oh.header_id
                            AND wi.item_type = 'OEOH')
         AND NOT EXISTS(SELECT 1
                          FROM  apps.oe_order_lines_all ol   
                         WHERE ol.header_id=oh.header_id        
                           AND ol.flow_status_code NOT IN ('CLOSED','CANCELLED'));  

   


 
 BEGIN
  fnd_file.put_line (fnd_file.LOG ,'=========================================================================================');
  fnd_file.put_line (fnd_file.LOG , 'Start of Procedure :HEADER_NOT_CLOSED ||Fix For:Sales Order Header not closed even all lines are closed or cancelled '||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
  fnd_file.put_line (fnd_file.LOG ,'Before for loop script execution');
 
   
   ---------------------------------------------------------------------------------------------------------------------------
   -- Order header status is in BOOKED status, but all order lines have been CLOSED OR CANCELLED status,Order supposed to
   -- closed in this case, it is not happening, we automated these kind of order issue, this script is only applicable
   -- STANDARD, COUNTER and  INTERNAL orders
   ---------------------------------------------------------------------------------------------------------------------------
   l_script_name := 'HEADER_NOT_CLOSED_EVEN_LINES_CLOSED';
   l_sec := 'Start loop cur_orders_booked';
   fnd_file.put_line (fnd_file.LOG , 'Start of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
   FOR rec_hdr_not_closed IN cur_orders_booked
   LOOP
	    BEGIN
            l_sec := l_script_name||' - '||'Check if record is locked';
		    l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec_hdr_not_closed.hdr_rowid, 'OE_ORDER_HEADERS_ALL');
			IF l_locked_line = 'N' THEN
		       l_sec := 'Insert record for '||l_script_name;
		       insert_record_log( p_order_header_id   => rec_hdr_not_closed.header_id
			                     ,p_order_line_id     => NULL
			                     ,p_request_id        => l_request_id
			                     ,p_script_name       => l_script_name
			                    );
	           	  -- Fetching the line level max last update date.
	      BEGIN
	       SELECT MAX(TRUNC(LAST_UPDATE_DATE))
	         INTO l_max_date
	         FROM apps.oe_order_lines_all 
	        WHERE header_id = rec_hdr_not_closed.header_id;
	      EXCEPTION
	       WHEN OTHERS THEN
	         l_max_date:=SYSDATE;
	      END;
	  
	      -- checking the max last update date with sysdate-7 value
	  
	     IF l_max_date <= TRUNC(l_curr_date) THEN 
	    
		  -- Updating the headers table
		  UPDATE oe_order_headers_all
		     SET flow_status_code = 'CLOSED'
                 ,open_flag = 'N'
		   WHERE header_id = rec_hdr_not_closed.header_id;
	     END IF;	  
	      l_count:=l_count+1;
	  
	      IF l_count > 0 THEN
		   l_sec := l_script_name||' - '||'update_record_status - Success';
		   update_record_status( p_order_header_id => rec_hdr_not_closed.header_id
		                        ,p_order_line_id   => NULL
		                        ,p_request_id      => l_request_id
		   				        ,p_data_fix_script => l_script_name
		                        ,p_status          => 'Success'
		                        ,p_errmsg          => ''
		                       );
	      END IF;
		  
		  COMMIT;
	  
	    END IF;  -- Lock checking end if   
      
       EXCEPTION
		    WHEN e_user_exception THEN
			l_err_msg:=SQLERRM;
		       ROLLBACK;
			   update_record_status( p_order_header_id => rec_hdr_not_closed.header_id
			                        ,p_order_line_id   => NULL
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
	        WHEN OTHERS THEN			  
			   ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   update_record_status( p_order_header_id => rec_hdr_not_closed.header_id
			                        ,p_order_line_id   => NULL
			                        ,p_request_id      => l_request_id
									,p_data_fix_script => l_script_name
			                        ,p_status          => 'Error'
			                        ,p_errmsg          => l_err_msg
			                       );
              fnd_file.put_line (fnd_file.LOG , l_script_name||' - '||l_err_msg);
              p_errbuf := 'Warning';
              p_retcode := '1';
	    END;
    END LOOP;
	
	  fnd_file.put_line (fnd_file.LOG , 'End of script: '||l_script_name||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
	  fnd_file.put_line (fnd_file.LOG , 'End of Procedure : HEADER_NOT_CLOSED'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (fnd_file.LOG ,'==========================================================================================');
   EXCEPTION
       WHEN OTHERS THEN
       p_errbuf := 'Error';
       p_retcode := '2';
	   l_err_code := SQLCODE;
       l_err_msg  := SUBSTR(SQLERRM,1,2000);
       fnd_file.put_line (fnd_file.LOG, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
       fnd_file.put_line (fnd_file.output, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
	   ROLLBACK;
       xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_ORDERS_DATAFIX_PKG.HEADER_NOT_CLOSED'
           ,p_calling             => l_sec
           ,p_request_id          => l_request_id
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error while executing data fix'
           ,p_distribution_list   => g_dflt_email
           ,p_module              => 'XXWC');

END HEADER_NOT_CLOSED; 

/******************************************************************************
  $Header BPA_OPEN.prc $
  Module Name: 20181023-00003   Unable to open BPA - datafix Automation

  PURPOSE: Data fix script automation for deleting the BPA from PO_DRAFT table

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  4.0       23-Oct-2018	 Sundaramoorthy         TMS#20181023-00003  --New procedure for BPA Fix	

*******************************************************************************/
 PROCEDURE BPA_OPEN (p_errbuf       OUT VARCHAR2, 
	                 p_retcode      OUT VARCHAR2
					 )
	IS  l_err_msg          VARCHAR2 (2000);
		l_locked_line      VARCHAR2(20);  
		l_script_name      VARCHAR2(100);
		l_sec              VARCHAR2(200);
		l_updated_rowcount NUMBER;
		l_request_id       NUMBER;
		LV_COUNT           NUMBER;
	
    BEGIN
	  fnd_file.put_line (fnd_file.LOG ,'=========================================================================================');
      fnd_file.put_line (fnd_file.LOG , 'Start of Procedure : BPA_OPEN || BPA unable to open'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      l_request_id := fnd_global.conc_request_id;
	  fnd_file.put_line (fnd_file.LOG , 'l_request_id:'||l_request_id);	    
      l_script_name := 'Unable to Open BPA';	 
	
	 SELECT count(1) INTO LV_COUNT
     FROM PO_DRAFTS      
      WHERE STATUS='PDOI ERROR';	     
	 
      IF LV_COUNT > 0 THEN 
       DELETE FROM PO_DRAFTS WHERE status='PDOI ERROR';  
        l_updated_rowcount := SQL%ROWCOUNT;
      	fnd_file.put_line (fnd_file.LOG ,'No.,of Rows Deleted '|| l_updated_rowcount);	      
	  ELSE
       fnd_file.put_line (fnd_file.LOG ,  'No Records found in the table PO_DRAFT');
       END IF;
	
	fnd_file.put_line (fnd_file.LOG , 'End of Procedure : BPA_OPEN || BPA unable to open');
	fnd_file.put_line (fnd_file.LOG ,'=========================================================================================');
	EXCEPTION
	WHEN OTHERS THEN
			   ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   p_errbuf := 'Warning';
               p_retcode := '1';
			  
	  fnd_file.put_line (fnd_file.LOG , l_err_msg); 
	  fnd_file.put_line (fnd_file.LOG , 'End of Program :XXWC OM Data Fix Automation'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (fnd_file.LOG ,'==========================================================================================');
	  xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_ORDERS_DATAFIX_PKG.BPA_OPEN'
           ,p_calling             => l_sec
           ,p_request_id          => l_request_id
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error while executing data fix'
           ,p_distribution_list   => g_dflt_email
           ,p_module              => 'XXWC');		
	    
    END BPA_OPEN;

END xxwc_om_orders_datafix_pkg;
/