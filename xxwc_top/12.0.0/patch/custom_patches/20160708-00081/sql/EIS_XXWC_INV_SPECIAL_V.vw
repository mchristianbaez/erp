---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_INV_SPECIAL_V $
  Module Name : Inventory
  PURPOSE	  : Specials Report
  REVISIONS   :
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     12-Apr-2016        Siva   		 TMS#20160407-00201, TMS#20160601-00142 Performance Tuning
  1.1	  14-Jun-2016		 Siva			 TMS#20160614-00010
  1.2	  03-Aug-2016		 Siva			 TMS#20160708-00081
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_INV_SPECIAL_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_INV_SPECIAL_V (ORGANIZATION_CODE, PART_NUMBER, DESCRIPTION, CAT, UOM, SELLING_PRICE, AVERAGECOST, GP, WEIGHT, ONHAND, VENDOR_NAME, VENDOR_NUMBER, BUYER_CODE, DATE_RECEIVED, OLDEST_BORN_DATE, OPEN_SALES_ORDERS, NO_OF_ITEM_PURCHASED, CREATION_DATE, INVENTORY_ITEM_ID, INV_ORGANIZATION_ID, ORG_ORGANIZATION_ID, AGENT_ID, MSI#HDS#LOB, MSI#HDS#DROP_SHIPMENT, MSI#HDS#INVOICE_UOM, MSI#HDS#PRODUCT_ID, MSI#HDS#VENDOR_PART_NUMBER, MSI#HDS#UNSPSC_CODE, MSI#HDS#UPC_PRIMARY, MSI#HDS#SKU_DESCRIPTION, MSI#WC#CA_PROP_65, MSI#WC#COUNTRY_OF_ORIGIN, MSI#WC#ORM_D_FLAG, MSI#WC#STORE_VELOCITY, MSI#WC#DC_VELOCITY, MSI#WC#YEARLY_STORE_VELOCITY, MSI#WC#YEARLY_DC_VELOCITY, MSI#WC#PRISM_PART_NUMBER, MSI#WC#HAZMAT_DESCRIPTION, MSI#WC#HAZMAT_CONTAINER, MSI#WC#GTP_INDICATOR, MSI#WC#LAST_LEAD_TIME, MSI#WC#AMU, MSI#WC#RESERVE_STOCK, MSI#WC#TAXWARE_CODE, MSI#WC#AVERAGE_UNITS, MSI#WC#PRODUCT_CODE, MSI#WC#IMPORT_DUTY_, MSI#WC#KEEP_ITEM_ACTIVE, MSI#WC#PESTICIDE_FLAG,
  MSI#WC#CALC_LEAD_TIME, MSI#WC#VOC_GL, MSI#WC#PESTICIDE_FLAG_STATE, MSI#WC#VOC_CATEGORY, MSI#WC#VOC_SUB_CATEGORY, MSI#WC#MSDS_#, MSI#WC#HAZMAT_PACKAGING_GROU)
AS
  SELECT /*+ INDEX(xosot XXWC_INV_SPCL_ONHAND_TBL_N2)*/ --added for version 1.2
    ood.organization_code ,
    msi.segment1 part_number ,
    msi.description ,
    (SELECT Mcv.concatenated_segments
    FROM Mtl_Categories_Kfv Mcv,
      MTL_CATEGORY_SETS MCS,
      Mtl_Item_Categories Mic
    WHERE Mcs.Category_Set_Name = 'Inventory Category'
    AND Mcs.Structure_Id        = Mcv.Structure_Id
    AND Mic.Inventory_Item_Id   = msi.inventory_item_id
    AND Mic.Organization_Id     = msi.organization_id
    AND MIC.CATEGORY_SET_ID     = MCS.CATEGORY_SET_ID
    AND Mic.Category_Id         = Mcv.Category_Id
    ) cat,
    msi.primary_uom_code uom ,
    --ol.unit_list_price selling_price,
    xxeis.eis_rs_xxwc_com_util_pkg_extn.get_list_price (msi.inventory_item_id, msi.organization_id) selling_price , --TMS#20160407-00201  by Pramod   on 04-12-2016
    xxeis.eis_rs_xxwc_com_util_pkg_extn.get_item_cost(1 ,msi.inventory_item_id ,msi.organization_id) averagecost,   --Changed for version 1.2
    --ROUND((((XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LIST_PRICE(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID))-(NVL(APPS.CST_COST_API.GET_ITEM_COST(1,MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID),0)))*100)/(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LIST_PRICE(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID)),2) GP,
    CASE
      WHEN NVL ( xxeis.eis_rs_xxwc_com_util_pkg_extn.get_list_price (msi.inventory_item_id ,msi.organization_id) ,0) = 0 --TMS#20160407-00201  by Pramod   on 04-12-2016
      THEN 0
      ELSE ROUND ( ( ( NVL ( xxeis.eis_rs_xxwc_com_util_pkg_extn.get_list_price (msi.inventory_item_id ,msi.organization_id) ,0) - (NVL ( xxeis.eis_rs_xxwc_com_util_pkg_extn.get_item_cost(1 ,msi.inventory_item_id ,msi.organization_id) ,0))) * 100) / (xxeis.eis_rs_xxwc_com_util_pkg_extn.get_list_price (msi.inventory_item_id ,msi.organization_id)) ,2) --TMS#20160407-00201  by Pramod   on 04-12-2016
    END gp ,
    msi.unit_weight weight ,
    -- xxeis.eis_po_xxwc_isr_util_qa_pkg.GET_PLANNING_QUANTITY(2,1,MSI.ORGANIZATION_ID,NULL,MSI.INVENTORY_ITEM_ID)QOH,
    xosot.onhand, --added for version 1.2
    --    xxeis.eis_po_xxwc_isr_util_qa_pkg.get_planning_quantity (2 ,1 ,msi.organization_id ,NULL ,msi.inventory_item_id) onhand , --Commented for version 1.2
    --xxeis.eis_rs_xxwc_com_util_pkg.get_onhand_inv(msi.inventory_item_id,msi.organization_id) ONHAND,
    /* (  ROUND (NVL (apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0)
    ,2)
    * (xxeis.eis_rs_xxwc_com_util_pkg.get_onhand_inv (msi.inventory_item_id, msi.organization_id)))
    extended_value ,*/
    --Commeneted by Mahender for TMS# 20141104-00095
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG_EXTN.GET_PO_VENDOR_DTLS(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID,'VENDORNAME') VENDOR_NAME,  --added for version 1.1
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG_EXTN.GET_PO_VENDOR_DTLS(msi.inventory_item_id,msi.organization_id,'VENDORNUM') VENDOR_NUMBER, --added for version 1.1
    papf.full_name buyer_code ,                                                                                                  --TMS#20160407-00201  by Pramod   on 04-12-2016
    (SELECT MAX(date_received)
    FROM mtl_onhand_quantities_detail moq
    WHERE Inventory_Item_Id =msi.inventory_item_id
    AND organization_id     =msi.organization_id
    ) date_received,--TMS#20160407-00201
    (SELECT to_date(TRUNC(MIN(orig_date_received)),'DD-MON-YYYY')
    FROM mtl_onhand_quantities_detail moq
    WHERE Inventory_Item_Id =msi.inventory_item_id
    AND organization_id     =msi.organization_id
    ) oldest_born_date ,--TMS#20160407-00201
    --gps.start_date,
    --gps.end_date,
    (
    SELECT COUNT (oh.order_number)
    FROM oe_order_headers oh,
      oe_order_lines ol
    WHERE oh.header_id           = ol.header_id
    AND ol.inventory_item_id     = msi.inventory_item_id
    AND ol.ship_from_org_id      = msi.organization_id
    AND ol.flow_status_code NOT IN ('CLOSED','CANCELLED')
    ) open_sales_orders, --TMS#20160407-00201
    (SELECT COUNT(DISTINCT poh.po_header_id)
    FROM po_headers poh,
      po_lines pol,
      PO_LINE_LOCATIONS POLL
      --      ,
      --      mtl_system_items_kfv msi   --commented for version 1.1
    WHERE poh.po_header_id    =pol.po_header_id
    AND pol.po_line_id        =poll.po_line_id
    AND msi.inventory_item_id =pol.item_id
    AND msi.organization_id   =poll.ship_to_organization_id
      --    AND msi.inventory_item_id     =msi.inventory_item_id --commented for version 1.1
      --    AND MSI.ORGANIZATION_ID       =msi.organization_id  --commented for version 1.1
    AND TRUNC(poh.creation_date) >= sysdate -365
    ) no_of_item_purchased, --TMS#20160407-00201
    TRUNC (msi.creation_date) creation_date ,
    --primary keys
    msi.inventory_item_id ,
    msi.organization_id inv_organization_id ,
    ood.organization_id org_organization_id ,
    --GPS.APPLICATION_ID,
    --GPS.SET_OF_BOOKS_ID,
    --GPS.PERIOD_NAME,
    poa.agent_id
    --descr#flexfield#start
    ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute1, NULL) msi#hds#lob ,
    DECODE (msi.attribute_category ,'HDS', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute10, 'F') ,NULL) msi#hds#drop_shipment ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute15, NULL) msi#hds#invoice_uom ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute2, NULL) msi#hds#product_id ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute3, NULL) msi#hds#vendor_part_number ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute4, NULL) msi#hds#unspsc_code ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute5, NULL) msi#hds#upc_primary ,
    DECODE (msi.attribute_category, 'HDS', msi.attribute7, NULL) msi#hds#sku_description ,
    DECODE (msi.attribute_category, 'WC', msi.attribute1, NULL) msi#wc#ca_prop_65 ,
    DECODE (msi.attribute_category, 'WC', msi.attribute10, NULL) msi#wc#country_of_origin ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute11, 'F') ,NULL) msi#wc#orm_d_flag ,
    DECODE (msi.attribute_category, 'WC', msi.attribute12, NULL) msi#wc#store_velocity ,
    DECODE (msi.attribute_category, 'WC', msi.attribute13, NULL) msi#wc#dc_velocity ,
    DECODE (msi.attribute_category, 'WC', msi.attribute14, NULL) msi#wc#yearly_store_velocity ,
    DECODE (msi.attribute_category, 'WC', msi.attribute15, NULL) msi#wc#yearly_dc_velocity ,
    DECODE (msi.attribute_category, 'WC', msi.attribute16, NULL) msi#wc#prism_part_number ,
    DECODE (msi.attribute_category, 'WC', msi.attribute17, NULL) msi#wc#hazmat_description ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC HAZMAT CONTAINER', msi.attribute18, 'I') ,NULL) msi#wc#hazmat_container ,
    DECODE (msi.attribute_category, 'WC', msi.attribute19, NULL) msi#wc#gtp_indicator ,
    DECODE (msi.attribute_category, 'WC', msi.attribute2, NULL) msi#wc#last_lead_time ,
    DECODE (msi.attribute_category, 'WC', msi.attribute20, NULL) msi#wc#amu ,
    DECODE (msi.attribute_category, 'WC', msi.attribute21, NULL) msi#wc#reserve_stock ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC TAXWARE CODE', msi.attribute22, 'I') ,NULL) msi#wc#taxware_code ,
    DECODE (msi.attribute_category, 'WC', msi.attribute25, NULL) msi#wc#average_units ,
    DECODE (msi.attribute_category, 'WC', msi.attribute26, NULL) msi#wc#product_code ,
    DECODE (msi.attribute_category, 'WC', msi.attribute27, NULL) msi#wc#import_duty_ ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute29, 'F') ,NULL) msi#wc#keep_item_active ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute3, 'F') ,NULL) msi#wc#pesticide_flag ,
    DECODE (msi.attribute_category, 'WC', msi.attribute30, NULL) msi#wc#calc_lead_time ,
    DECODE (msi.attribute_category, 'WC', msi.attribute4, NULL) msi#wc#voc_gl ,
    DECODE (msi.attribute_category, 'WC', msi.attribute5, NULL) msi#wc#pesticide_flag_state ,
    DECODE (msi.attribute_category, 'WC', msi.attribute6, NULL) msi#wc#voc_category ,
    DECODE (msi.attribute_category, 'WC', msi.attribute7, NULL) msi#wc#voc_sub_category ,
    DECODE (msi.attribute_category, 'WC', msi.attribute8, NULL) msi#wc#msds_# ,
    DECODE (msi.attribute_category ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC_HAZMAT_PACKAGE_GROUP', msi.attribute9, 'I') ,NULL) msi#wc#hazmat_packaging_grou
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM mtl_system_items_kfv msi,
    XXEIS.XXWC_INV_SPCL_ONHAND_TBL xosot, --added for version 1.2
    -- mtl_onhand_quantities_detail moqd,
    ORG_ORGANIZATION_DEFINITIONS OOD,
    -- po_agents_v poa   --TMS#20160407-00201  removed   on 04-12-2016
    APPS.PO_AGENTS POA,   --TMS#20160407-00201  added   on 04-12-2016
    PER_ALL_PEOPLE_F PAPF --TMS#20160407-00201  added   on 04-12-2016
    --gl_period_statuses gps
  WHERE msi.inventory_item_id= xosot.inventory_item_id  --added for version 1.2
  AND msi.organization_id    = XOSOT.organization_id	--added for version 1.2
  AND XOSOT.process_id       = xxeis.EIS_RS_XXWC_COM_UTIL_PKG_EXTN.get_process_id  --added for version 1.2
  AND msi.organization_id    = ood.organization_id
  AND MSI.BUYER_ID           = POA.AGENT_ID(+)
    --  AND MSI.ITEM_TYPE = 'SPECIAL' --Commented for version 1.2
  AND POA.AGENT_ID = PAPF.PERSON_ID(+)                                                   --TMS#20160407-00201  added   on 04-12-2016
  AND TRUNC(SYSDATE) BETWEEN PAPF.EFFECTIVE_START_DATE(+) AND PAPF.EFFECTIVE_END_DATE(+) --TMS#20160407-00201  added   on 04-12-2016
    --    and ood.organization_id=228
    --AND msi.inventory_item_id =moqd.inventory_item_id
    --AND msi.organization_id   =moqd.organization_id
    -- AND msi.segment1 LIKE 'SP%'
    --AND gps.application_id =101
    --AND gps.set_of_books_id=ood.set_of_books_id
    -- AND TRUNC(moqd.date_received) BETWEEN gps.start_date AND gps.end_date
    --AND MOQD.TRANSACTION_QUANTITY <> 0
  AND xosot.ONHAND!=0  --added for version 1.2
  AND EXISTS
    (SELECT 1
    FROM XXEIS.EIS_ORG_ACCESS_V
    WHERE organization_id = msi.organization_id
    )
/
