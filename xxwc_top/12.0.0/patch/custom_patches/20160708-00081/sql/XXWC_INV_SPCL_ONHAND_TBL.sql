--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: XXEIS.XXWC_INV_SPCL_ONHAND_TBL
  Description: This table is used to get data from XXEIS.EIS_RS_XXWC_COM_UTIL_PKG_EXTN Package.
				All rows for each process are deleted after each run. This table should normally have 0 rows.
  HISTORY
  ===============================================================================
  VERSION 		DATE            AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0	  	03-Aug-2016		 	Siva			 TMS#20160708-00081
********************************************************************************/
CREATE TABLE XXEIS.XXWC_INV_SPCL_ONHAND_TBL
  (
    PROCESS_ID        NUMBER,
    ONHAND            NUMBER,
    INVENTORY_ITEM_ID NUMBER,
    ORGANIZATION_ID   NUMBER
  );

COMMENT ON TABLE XXEIS.XXWC_INV_SPCL_ONHAND_TBL IS 'This table is used to get data from XXEIS.EIS_RS_XXWC_COM_UTIL_PKG_EXTN Package.
All rows for each process are deleted after each run. This table should normally have 0 rows.'  
/
