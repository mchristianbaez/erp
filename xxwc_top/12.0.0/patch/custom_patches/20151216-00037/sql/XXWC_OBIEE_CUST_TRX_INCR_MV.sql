DROP MATERIALIZED VIEW APPS.XXWC_OBIEE_CUST_TRX_INCR_MV;

CREATE MATERIALIZED VIEW APPS.XXWC_OBIEE_CUST_TRX_INCR_MV
   (
   C1_SALES_ORDLN_ID,
   C2_LEGAL_ENTITY_ID,
   C3_GL_ACCOUNT_ID,
   C4_INVENTORY_ORG_ID,
   C5_INVENTORY_PRODUCT_ID,
   C6_BRANCH_ID,
   C7_PLANT_LOC_ID,
   C8_PRODUCT_ID,
   C9_OPERATING_UNIT_ORG_ID,
   C10_SALES_REP_ID,
   C11_XACT_TYPE_ID,
   C12_LEDGER_ID,
   C13_CUSTOMER_ACCOUNT_ID,
   C14_XACT_SOURCE_ID,
   C15_CUST_PRI_BILL_TO_ID,
   C16_ACCOUNT_SALES_REP_ID,
   C17_VENDOR_SITE_ID,
   C18_VENDER_ID,
   C19_SHIP_METHOD,
   C20_SALES_REP_HIERARCHY_ID,
   C21_ACC_SALES_REP_HIERARCHY_ID,
   C22_INVOICE_HEADER_ID,
   C23_DOC_CURR_CODE,
   C24_TRANSACTION_DT,
   C25_INVOICE_POST_DT,
   C26_INVOICE_NUM,
   C27_EXTENDED_AMOUNT_SALES,
   C28_PRODUCT_UNIT_COST,
   C29_TRANSACTION_QTY,
   C30_PRICE_TYPE,
   C31_INV_ITEM_DESCRIPTION,
   C32_LINE_TYPE,
   C33_CUSTOMER_PO_NUMBER,
   C34_FREIGHT_AMOUNT,
   C35_ORDER_CREATE_BY_NTID,
   C36_INVOICE_CREATION_DATE,
   C37_INVOICE_DIRECT_FLG,
   C38_INV_DISCOUNT_AMT,
   C39_LINE_NUMBER,
   C40_LINE_PRICE_DATE,
   C41_MARKET_PRICE,
   C42_ORDER_CREATION_DATE,
   C43_ORDER_DATE,
   C44_ORDER_LAST_UPDATE_DATE,
   C45_ORDER_METHOD,
   C46_ORDER_NUMBER,
   C47_ORDER_QUANTITY,
   C48_PO_REPL_COST,
   C49_SALES_CHANNEL,
   C50_SHIP_DATE,
   C51_SOURCE_TYPE_CODE,
   C52_TAX_AMOUNT,
   C53_UNIT_OF_MEASURE,
   C54_UNIT_SELLING_PRICE,
   C55_INTER_COMP_TRANSACTION,
   C56_TAKEN_BY_NAME,
   C57_ORIGINAL_MODIFIER,
   C58_FINAL_MODIFIER,
   C59_SYSTEM_PRICE,
   C60_BRANCH_COST,
   C61_SALES_ORDER_NUMBER,
   C62_ORDER_TYPE,
   C63_ORDER_CREATE_BY_NAME,
   C64_SERIAL_NUMBER,
   C65_DELETE_FLG,
   C66_EXCHANGE_DT,
   C67_CREATED_BY_ID,
   C68_CHANGED_BY_ID,
   C69_CREATED_ON_DT,
   C70_CHANGED_ON_DT,
   C71_AUX1_CHANGED_ON_DT,
   C72_AUX2_CHANGED_ON_DT,
   C73_INTEGRATION_ID,
   C74_PAYMENT_DISCOUNT_PERCENT,
   C75_PAYMENT_TERM_DESCRIPTION,
   C76_LINE_SHIP_FROM_ORG_ID,
   C77_PRICE_BRANCH_ID,
   C78_BATCH_SOURCE_NAME,
   C79_CUST_TRX_LINE_ID,
   C80_ACTUAL_INVENTORY_ITEM_ID,
   C81_PRODUCT_CAT_CLASS,
   C82_VQN_UNIT_COST,
   C83_BILL_TO_SITE_USE_ID,
   C84_SHIP_TO_SITE_USE_ID,
   C85_SOLD_TO_SITE_USE_ID,
   C86_PAYING_SITE_USE_ID,
   C87_CUST_PROFILE_GROUP,
   C88_ORDER_SOURCE_ID,
   OL_LAST_UPDATE_DATE,
   CUSTOMER_TRX_ID,
   CUSTOMER_TRX_LINE_ID,
   C89_PRICE_SRC_TYPE -- ver 1.5 by Neha Saini
   )
   BUILD IMMEDIATE
   REFRESH COMPLETE
           ON DEMAND
           WITH PRIMARY KEY
AS
   WITH trx_lines
        /*  Rewrite of the Customer Transaction incremental extract for OBIEE.
            This MV is using the following seven sub-MV that can be refreshed in parallel
              APPS.XXWC_OBIEE_CT__PO_PRICE_MV
              APPS.XXWC_OBIEE_CT__PRC_ADJ_MV
              APPS.XXWC_OBIEE_CT__ORD_LN_COST_MV
              APPS.XXWC_OBIEE_CT__ORD_LN_MV
              APPS.XXWC_OBIEE_CT__MTL_ITEM_CAT_MV
              APPS.XXWC_OBIEE_CT__CTL_MV
              APPS.XXWC_OBIEE_CT__VNDR_QUOTE_MV <-- this sub-MV has date filter which should be
                                                    matching the main query
            Same set of MV without a date filter can be used for full historical refresh
       --
       -- 20141017  ver.1.0  Going live build w XXWC_OBIEE_CUST_TRX_INCR_MV name
       -- 20141020  ver.1.1  SYSDATE-7 filter
       -- 20141028  ver.1.2  correction for duplicate header price adjustments
       -- 20141106  ver.1.3  svc 265076: add ar.hz_cust_profile_classes attribute2
       -- 20141106  ver.1.4  svc 265076: change to ar.hz_cust_profile_classes attribute4
                             svc 262147: (Speedbuild) add ont.oe_order_headers.order_source_id
                                         as c88_order_source_id
       --20161602   ver 1.5  TMS 20151216-00037 adding new field C89_PRICE_SRC_TYPE
 */
        AS (SELECT /*+ qb_name(trx_lines) materialize */
                  ctla.created_by,
                   ctla.creation_date,
                   ctla.customer_trx_id,
                   ctla.customer_trx_line_id,
                   ctla.description,
                   ctla.extended_amount,
                   ctla.interface_line_attribute6,
                   ctla.interface_line_context,
                   ctla.actual_inventory_item_id,
                   ctla.last_update_date,
                   ctla.last_updated_by,
                   ctla.line_number,
                   ctla.line_type,
                   ctla.link_to_cust_trx_line_id,
                   ctla.org_id,
                   ctla.quantity_credited,
                   ctla.quantity_invoiced,
                   ctla.quantity_ordered,
                   ctla.sales_order,
                   ctla.sales_order_date,
                   ctla.unit_selling_price,
                   ctla.uom_code,
                   ctla.warehouse_id,
                   tax_ln.tax_amount
              FROM apps.xxwc_obiee_ct__ctl_mv ctla
                   LEFT OUTER JOIN apps.xxwc_obiee_ct__ctl_mv tax_ln
                      ON     ctla.customer_trx_id = tax_ln.customer_trx_id
                         AND ctla.customer_trx_line_id =
                                tax_ln.link_to_cust_trx_line_id
                         AND tax_ln.rn = 1
             WHERE     ctla.line_type = 'LINE'
                   AND ctla.last_update_date BETWEEN TRUNC (SYSDATE - 7)
                                                 AND SYSDATE),
        salesrep
        AS (SELECT /*+ qb_name(slsrep) materialize */
                  person_id,
                   salesrep_id,
                   org_id,
                   resource_id
              FROM jtf.jtf_rs_salesreps),
        inv_adj_amount
        AS (SELECT /*+ qb_name(ar_adj) materialize */
                   /* replacement for FUNCTION get_inv_adj_amount */
                   customer_trx_id,
                   customer_trx_line_id,
                   org_id,
                   SUM (
                      amount)
                   OVER (
                      PARTITION BY customer_trx_id,
                                   customer_trx_line_id,
                                   org_id)
                      AS inv_adj_amt
              FROM ar.ar_adjustments_all)
   SELECT /*+ qb_name(main) no_merge use_hash(u1 sr sra glo) */
         TO_CHAR (glo.sales_ordln_id) AS c1_sales_ordln_id,
          'COMPANY' || '~' || TO_CHAR (glo.legal_entity_id)
             AS c2_legal_entity_id,
          TO_CHAR (glo.gl_account_id) AS c3_gl_account_id,
          TO_CHAR (glo.warehouse_id) AS c4_inventory_org_id/* 10/04/2014 CG: Replaced to use glo.actual_inventory_item_id since it's seems to be always populated */
          ,
          TO_CHAR (glo.inventory_item_id || '~' || glo.warehouse_id) /* , TO_CHAR (glo.actual_inventory_item_id || '~' || glo.warehouse_id) */
             AS c5_inventory_product_id, /* AGH: compromise on using the function call from the original code for c6_branch_id/c77_price_branch_id
                                                 This will kick in only for the small number of cases where 1st choice is NULL
                                         */
          COALESCE (
             TO_CHAR (glo.ship_from_org_id),
             (SELECT TO_CHAR (mtl_parameters.organization_id)
                FROM inv.mtl_parameters mtl_parameters
               WHERE mtl_parameters.organization_code =
                        (apps.xxwc_mv_routines_pkg.get_branch_num_from_lob (
                            apps.xxwc_mv_routines_pkg.get_gl_code_segment (
                               glo.gl_account_id,
                               2)))),
             '227')
             AS c6_branch_id,
          'PLANT' || '~' || TO_CHAR (glo.warehouse_id) AS c7_plant_loc_id/* 10/04/2014 CG: Replaced to use glo.actual_inventory_item_id since it's seems to be always populated
                                                                          , TO_CHAR (glo.product_id) AS c8_product_id
                                                                         */
          ,
          TO_CHAR (glo.actual_inventory_item_id) AS c8_product_id,
          TO_CHAR (glo.org_id) AS c9_operating_unit_org_id,
          DECODE (sr.person_id,
                  NULL, 'RES~' || sr.resource_id,
                  'PER~' || sr.person_id)
             AS c10_sales_rep_id,
          'SALES_IVCLNS' || '~' || glo.xact_type_id AS c11_xact_type_id,
          glo.ledger_id AS c12_ledger_id,
          TO_CHAR (glo.customer_account_id) AS c13_customer_account_id,
          'SALES_IVCLNS' || '~' || glo.xact_source_id AS c14_xact_source_id,
          glo.cust_pri_bill_to_id AS c15_cust_pri_bill_to_id,
          DECODE (sra.person_id,
                  NULL, 'RES~' || sra.resource_id,
                  'PER~' || sra.person_id)
             AS c16_account_sales_rep_id,
          glo.poh_vendor_site_id AS c17_vendor_site_id,
          glo.poh_vendor_id AS c18_vender_id,
          glo.ship_method AS c19_ship_method,
          glo.sales_rep_id AS c20_sales_rep_hierarchy_id,
          glo.account_sales_rep_id AS c21_acc_sales_rep_hierarchy_id,
          TO_CHAR (glo.invoice_header_id) AS c22_invoice_header_id,
          glo.doc_curr_code AS c23_doc_curr_code,
          glo.trx_date AS c24_transaction_dt,
          glo.invoice_post_date AS c25_invoice_post_dt,
          glo.invoice_number AS c26_invoice_num,
          glo.extended_amount_sales AS c27_extended_amount_sales, /* COALESCE (apps.xxwc_mv_routines_pkg.get_order_line_cost ( ctl1.line_id), 0)*/
          COALESCE (glo.product_unit_cost, 0) AS c28_product_unit_cost,
          COALESCE (glo.quantity_invoiced,
                    glo.quantity_credited,
                    glo.quantity_ordered,
                    1)
             AS c29_transaction_qty, /* apps.xxwc_mv_routines_add_pkg.get_price_type (ctl1.line_header_id, ctl1.line_id)*/
          COALESCE (glo.price_type, 'SYSTEM') AS c30_price_type,
          glo.inv_item_description AS c31_inv_item_description,
          glo.line_type AS c32_line_type,
          glo.customer_po_number AS c33_customer_po_number,
          glo.freight_amount AS c34_freight_amount,
          glo.order_create_by_ntid AS c35_order_create_by_ntid,
          glo.invoice_creation_date AS c36_invoice_creation_date,
          (CASE
              WHEN glo.header_id IS NULL THEN 'N'
              ELSE glo.has_dropship_cnt
           END)
             AS c37_invoice_direct_flg,
          (CASE
              WHEN glo.customer_trx_id IS NULL AND glo.rcta_org_id IS NULL
              THEN
                 NULL
              ELSE
                 glo.inv_adj_amt
           END)
             AS c38_inv_discount_amt,
          glo.line_number AS c39_line_number,
          glo.line_price_date AS c40_line_price_date,
          glo.market_price AS c41_market_price,
          glo.order_creation_date AS c42_order_creation_date,
          glo.order_date AS c43_order_date,
          glo.order_last_update_date AS c44_order_last_update_date,
          glo.order_method AS c45_order_method,
          glo.order_number AS c46_order_number,
          glo.order_quantity AS c47_order_quantity,
          CASE
             WHEN     glo.rctla_org_id IS NULL
                  AND glo.warehouse_id IS NULL
                  AND glo.inventory_item_id IS NULL
             THEN
                NULL
             ELSE
                glo.last_po_price
          END
             AS c48_po_repl_cost,
          glo.sales_channel AS c49_sales_channel,
          glo.ship_date AS c50_ship_date,
          glo.source_type_code AS c51_source_type_code,
          (CASE
              WHEN     glo.customer_trx_id IS NULL
                   AND glo.customer_trx_line_id IS NULL
              THEN
                 0
              ELSE
                 glo.tax_amount
           END)
             AS c52_tax_amount,
          glo.unit_of_measure AS c53_unit_of_measure,
          glo.unit_selling_price AS c54_unit_selling_price,
          CASE
             WHEN    glo.cpc_name = 'WC BRANCHES'
                  OR glo.tt_name LIKE '%INTERNAL%'
             THEN
                'Y'
             ELSE
                'N'
          END
             AS c55_inter_comp_transaction,
          glo.taken_by_name AS c56_taken_by_name, /* apps.xxwc_mv_routines_add_pkg.get_original_modifier (ctl1.line_header_id, ctl1.line_id)*/
          NVL (glo.original_modifier, 'MARKET PRICE')
             AS c57_original_modifier, /* apps.xxwc_mv_routines_add_pkg.get_last_modifier (ctl1.line_header_id, ctl1.line_id)*/
          NVL (glo.last_modifier, 'MARKET PRICE') AS c58_final_modifier, /* apps.xxwc_mv_routines_add_pkg.get_overridden_sell_price (ctl1.line_header_id, ctl1.line_id)*/
          glo.overridden_sell_price AS c59_system_price,
          glo.branch_cost AS c60_branch_cost,
          glo.sales_order_number AS c61_sales_order_number,
          CASE
             WHEN glo.cpc_name = 'WC BRANCHES' THEN 'INTR'
             WHEN glo.tt_name LIKE '%STANDARD%' THEN 'STD'
             WHEN glo.tt_name LIKE '%COUNTER%' THEN 'CNTR'
             WHEN glo.tt_name LIKE '%INTERNAL%' THEN 'INTR'
             WHEN glo.tt_name LIKE '%LONG%RENTAL%' THEN 'RLLT'
             WHEN glo.tt_name LIKE '%SHORT%RENTAL%' THEN 'RLST'
             WHEN glo.tt_name LIKE '%REPAIR%' THEN 'REPR'
             WHEN glo.tt_name LIKE '%RETURN%' THEN 'RTRN'
             ELSE 'STD'
          END
             AS c62_order_type,
          u1.full_name AS c63_order_create_by_name,
          glo.serial_number AS c64_serial_number,
          glo.delete_flg AS c65_delete_flg,
          glo.exchange_date AS c66_exchange_dt,
          TO_CHAR (glo.created_by) AS c67_created_by_id,
          TO_CHAR (glo.last_updated_by) AS c68_changed_by_id,
          glo.creation_date AS c69_created_on_dt,
          glo.ctla_last_update_date AS c70_changed_on_dt,
          glo.cta_last_update_date AS c71_aux1_changed_on_dt,
          glo.ctt_last_update_date AS c72_aux2_changed_on_dt,
          glo.integration_id AS c73_integration_id,
          glo.payment_discount_percent AS c74_payment_discount_percent,
          glo.payment_term_description AS c75_payment_term_description,
          glo.line_ship_from_org_id AS c76_line_ship_from_org_id, /* AGH: compromise on using the function call from the original code for c6_branch_id/c77_price_branch_id
                                                                          This will kick in only for the small number of cases where 1st choice is NULL
                                                                  */
          TO_NUMBER (
             COALESCE (
                TO_CHAR (glo.price_branch),
                (SELECT TO_CHAR (mtl_parameters.organization_id)
                   FROM inv.mtl_parameters mtl_parameters
                  WHERE mtl_parameters.organization_code =
                           (apps.xxwc_mv_routines_pkg.get_branch_num_from_lob (
                               apps.xxwc_mv_routines_pkg.get_gl_code_segment (
                                  glo.gl_account_id,
                                  2)))),
                '227'))
             AS c77_price_branch_id,
          glo.batch_source_name AS c78_batch_source_name,
          glo.customer_trx_line_id AS c79_cust_trx_line_id,
          glo.actual_inventory_item_id AS c80_actual_inventory_item_id,
          glo.prod_cat_class AS c81_product_cat_class,
          glo.vqn_unit_cost AS c82_vqn_unit_cost,
          glo.bill_to_site_use_id AS c83_bill_to_site_use_id,
          glo.ship_to_site_use_id AS c84_ship_to_site_use_id,
          glo.sold_to_site_use_id AS c85_sold_to_site_use_id,
          glo.paying_site_use_id AS c86_paying_site_use_id, /* svc 265076: add ar.hz_cust_profile_classes attribute4*/
          glo.cpc_grp AS c87_cust_profile_group, /* svc 262147: (Speedbuild) add ont.oe_order_headers.order_source_id */
          glo.order_source_id AS c88_order_source_id/* The following 3 attributes are used for audit trail to the
                                                       original records in
                                                        ar.ra_customer_trx_all
                                                        ar.ra_customer_trx_lines_all
                                                        ont.oe_order_lines_all
                                                    */
          ,
          glo.ol_last_update_date,
          glo.customer_trx_id,
          glo.customer_trx_line_id,
          glo.PRICE_SRC_TYPE  AS C89_PRICE_SRC_TYPE                      --ver1.5 Added by Neha
     FROM (SELECT /*+ qb_name(glo) no_merge */
                 ctl1.sales_ordln_id,
                  ctl1.legal_entity_id,
                  ctl1.bill_to_site_use_id,
                  ctl1.paying_site_use_id,
                  ctl1.ship_to_site_use_id,
                  ctl1.sold_to_site_use_id,
                  ctl1.ship_from_org_id,
                  ctl1.gl_account_id,
                  ctl1.warehouse_id,
                  ctl1.inventory_item_id,
                  par.organization_id,
                  ctl1.product_id,
                  ctl1.org_id,
                  ctl1.sales_rep_id,
                  ctl1.xact_type_id,
                  ctl1.ledger_id,
                  ctl1.customer_account_id,
                  ctl1.xact_source_id,
                  ctl1.invoice_header_id,
                  ctl1.trx_date,
                  ctl1.doc_curr_code,
                  ctl1.invoice_post_date,
                  ctl1.invoice_number,
                  ctl1.extended_amount_sales,
                  ctl1.product_unit_cost,
                  ctl1.quantity_invoiced,
                  ctl1.quantity_credited,
                  ctl1.quantity_ordered,
                  ctl1.ship_method,
                  ctl1.price_type,
                  ctl1.delete_flg,
                  ctl1.exchange_date,
                  ctl1.created_by,
                  ctl1.last_updated_by,
                  ctl1.ctla_last_update_date,
                  ctl1.creation_date,
                  ctl1.ctt_last_update_date,
                  ctl1.cta_last_update_date,
                  ctl1.integration_id,
                  ctl1.inv_item_description,
                  ctl1.line_type,
                  ctl1.order_date,
                  (CASE
                      WHEN     ctl1.ooha_created_by IS NULL
                           AND ctl1.rcta_created_by IS NULL
                      THEN
                         NULL
                      ELSE
                         TO_CHAR (
                            COALESCE (ctl1.ooha_created_by,
                                      ctl1.rcta_created_by))
                   END)
                     AS order_create_by_ntid,
                  ctl1.customer_po_number,
                  ctl1.freight_amount,
                  ctl1.invoice_creation_date,
                  ctl1.header_id,
                  iaa.inv_adj_amt,
                  ctl1.rcta_org_id,
                  ctl1.customer_trx_id,
                  ctl1.line_number,
                  ctl1.line_price_date,
                  ctl1.market_price,
                  ctl1.order_creation_date,
                  ctl1.order_last_update_date,
                  ctl1.order_method,
                  ctl1.order_number,
                  ctl1.order_quantity,
                  ctl1.last_po_price,
                  ctl1.rctla_org_id,
                  ctl1.sales_channel,
                  ctl1.ship_date,
                  ctl1.source_type_code,
                  ctl1.tax_amount,
                  ctl1.unit_of_measure,
                  ctl1.unit_selling_price,
                  ctl1.branch_cost,
                  ctl1.last_modifier,
                  ctl1.original_modifier,
                  ctl1.sales_order_number,
                  ctl1.overridden_sell_price,
                  ctl1.taken_by_name,
                  ctl1.tt_name,
                  ctl1.cpc_name,
                  ctl1.cpc_grp,
                  cas1.account_sales_rep_id,
                  cas1.cust_pri_bill_to_id,
                  ctl1.poh_vendor_site_id,
                  ctl1.poh_vendor_id,
                  ctl1.has_dropship_cnt,
                  ctl1.serial_number,
                  ctl1.payment_discount_percent,
                  ctl1.payment_term_description,
                  ctl1.line_ship_from_org_id,
                  ctl1.price_branch,
                  ctl1.batch_source_name,
                  ctl1.ol_last_update_date,
                  ctl1.actual_inventory_item_id,
                  ctl1.prod_cat_class,
                  ctl1.customer_trx_line_id,
                  ctl1.vqn_unit_cost,
                  ctl1.order_source_id,
                  ctl1.PRICE_SRC_TYPE               --ver1.5 Added by Neha
             FROM (SELECT /*+ qb_name(ctl1) no_merge use_hash(ct ctl oh ol)  */
                         ctl.line_id AS sales_ordln_id,
                          ct.legal_entity_id,
                          ct.bill_to_site_use_id,
                          ct.paying_site_use_id,
                          ct.ship_to_site_use_id,
                          ct.sold_to_site_use_id,
                          (CASE
                              WHEN DECODE (
                                      REPLACE (
                                         TRANSLATE (
                                            ct.interface_header_attribute10,
                                            '1234567890.-',
                                            '############'),
                                         '#'),
                                      NULL, 1,
                                      0) = 1
                              THEN
                                 ct.interface_header_attribute10
                              ELSE
                                 NULL
                           END)
                             AS ship_from_org_id,
                          ct.code_combination_id AS gl_account_id,
                          ctl.warehouse_id,
                          ctl.line_type,
                          ctl.inventory_item_id AS product_id,
                          ct.org_id,
                          ct.primary_salesrep_id AS sales_rep_id,
                          ct.TYPE AS xact_type_id,
                          ct.set_of_books_id AS ledger_id,
                          ct.bill_to_customer_id AS customer_account_id,
                          ctl.interface_line_context AS xact_source_id,
                          ct.customer_trx_id AS invoice_header_id,
                          ct.trx_date,
                          COALESCE (ct.invoice_currency_code,
                                    '__UNASSIGNED__')
                             AS doc_curr_code,
                          ct.invoice_post_date,
                          ct.trx_number AS invoice_number,
                          ctl.extended_amount AS extended_amount_sales,
                          ctl.quantity_invoiced,
                          ctl.quantity_credited,
                          ctl.quantity_ordered,
                          ctl.shipping_method_code AS ship_method,
                          'N' AS delete_flg,
                          ct.exchange_date,
                          ctl.created_by,
                          ctl.last_updated_by,
                          ctl.ctla_last_update_date,
                          ctl.creation_date,
                          ct.ctt_last_update_date,
                          ct.cta_last_update_date,
                             TO_CHAR (ctl.sales_order)
                          || '~'
                          || ct.trx_number
                          || '~'
                          || TO_CHAR (ctl.line_number)
                          || '~'
                          || TO_CHAR (ct.customer_trx_id)
                          || '~'
                          || TO_CHAR (ctl.customer_trx_line_id)
                             AS integration_id/* 10/07/2014 CG: Updated to reflect rental item ids and desc
                                               , ctl.description AS inv_item_description
                                               , ctl.inventory_item_id
                                              */
                          ,
                          (CASE
                              WHEN     ctl.line_id IS NOT NULL
                                   AND ctl.actual_inventory_item_id = 2931193
                              THEN
                                 COALESCE (ctl.msib_item_description,
                                           ctl.description)
                              ELSE
                                 ctl.description
                           END)
                             AS inv_item_description,
                          (CASE
                              WHEN     ctl.line_id IS NOT NULL
                                   AND ctl.actual_inventory_item_id = 2931193
                              THEN
                                 COALESCE (ctl.msib_inventory_item_id,
                                           ctl.actual_inventory_item_id)
                              ELSE
                                 ctl.actual_inventory_item_id
                           END)
                             AS inventory_item_id,
                          ctl.line_number,
                          COALESCE (ctl.pricing_date, ctl.sales_order_date)
                             AS line_price_date,
                          COALESCE (ctl.actual_shipment_date, ct.trx_date)
                             AS ship_date,
                          COALESCE (ctl.cust_po_number, ct.purchase_order)
                             AS customer_po_number,
                          COALESCE (ctl.uom_code, 'EA') AS unit_of_measure,
                          ctl.unit_selling_price,
                          COALESCE (ctl.quantity_ordered,
                                    ctl.ordered_quantity)
                             AS order_quantity,
                          COALESCE (ctl.unit_list_price,
                                    ctl.unit_selling_price)
                             AS market_price,
                          COALESCE (TO_CHAR (oh.order_number),
                                    ct.interface_header_attribute1)
                             AS order_number,
                          ct.gl_date AS invoice_creation_date,
                          oh.creation_date AS order_creation_date,
                          oh.booked_date AS order_date,
                          oh.last_update_date AS order_last_update_date,
                          oh.attribute2 AS order_method,
                          ct.freight_original AS freight_amount,
                          oh.sales_channel_code AS sales_channel,
                          ctl.source_type_code,
                          oh.attribute7 AS taken_by_name,
                          ROUND (COALESCE (ctl.unit_cost, 0), 2)
                             AS branch_cost,
                          ctl.sales_order AS sales_order_number,
                          ctl.line_id,
                          oh.header_id,
                          ctl.customer_trx_line_id,
                          ct.org_id AS rcta_org_id,
                          ctl.org_id AS rctla_org_id/* 10/04/2014 CG: updated to uset the NTID versions
                                                    , ct.created_by AS rcta_created_by
                                                    , oh.created_by AS ooha_created_by
                                                    */
                          ,
                          ct.created_by_ntid AS rcta_created_by,
                          oh.created_by_ntid AS ooha_created_by,
                          ct.customer_trx_id,
                          oh.order_type_id,
                          oh.tt_name,
                          ct.cpc_name,
                          ct.cpc_grp,
                          ctl.attribute7 AS serial_number,
                          ct.payment_term_description
                             AS payment_term_description,
                          ct.perc_discount AS payment_discount_percent,
                          ctl.header_id AS line_header_id,
                          ctl.ship_from_org_id AS line_ship_from_org_id,
                          ctl.poh_vendor_site_id,
                          ctl.poh_vendor_id,
                          oh.ship_from_org_id AS price_branch,
                          ctl.has_dropship_cnt/* 10/02/14: CG: Updated to use batch_source_name from the lower level instead of defaulting to DEF
                                               , 'DEF' AS batch_source_name
                                               */
                          ,
                          ct.batch_source_name AS batch_source_name,
                          ctl.ol_last_update_date,
                          pol_up.last_po_price,
                          ctl.tax_amount,
                          ctl.product_unit_cost,
                          ctl.original_modifier,
                          ctl.last_modifier,
                          ctl.overridden_sell_price,
                          ctl.price_type,
                          ctl.actual_inventory_item_id,
                          ctl.prod_cat_class, /* apps.xxwc_mv_routines_pkg.get_vendor_quote_cost (sq_ra_cust_trx_lines_all.line_id) */
                          ctl.vqc_vendor_quote_cost AS vqn_unit_cost,
                          oh.order_source_id,
                          ctl.PRICE_SRC_TYPE        --ver1.5 Added by neha
                     FROM (SELECT /*+ qb_name(ct) no_merge use_hash(cta ctt cp arps ctlgl bs)   */
                                 cta.batch_source_id,
                                  cta.bill_to_customer_id,
                                  cta.bill_to_site_use_id,
                                  cta.created_by/* 10/04/2014 CG: Added to pull header Created by NTID
                                                */
                                  ,
                                  fu.user_name created_by_ntid,
                                  cta.creation_date,
                                  cta.cust_trx_type_id,
                                  cta.customer_trx_id,
                                  cta.exchange_date,
                                  cta.interface_header_attribute1,
                                  cta.interface_header_attribute10,
                                  cta.invoice_currency_code,
                                  cta.last_update_date
                                     AS cta_last_update_date,
                                  cta.legal_entity_id,
                                  cta.org_id,
                                  cta.paying_site_use_id,
                                  cta.primary_salesrep_id,
                                  cta.purchase_order,
                                  cta.set_of_books_id,
                                  cta.ship_to_site_use_id,
                                  cta.sold_to_site_use_id,
                                  cta.term_id,
                                  cta.trx_date,
                                  cta.trx_number,
                                  arpt.perc_discount,
                                  arpt.payment_term_description,
                                  cp.cpc_name,
                                  cp.cpc_grp,
                                  arps.freight_original,
                                  ctlgl.gl_date,
                                  ctlgl.code_combination_id,
                                  ctt.last_update_date
                                     AS ctt_last_update_date,
                                  ctt.TYPE/* 10/02/14: CG: added batch_source_name field, it was already lined to the table
                                          */
                                  ,
                                  bs.name batch_source_name,
                                  (CASE
                                      WHEN bs.name IN ('WC MANUAL',
                                                       'MANUAL-OTHER',
                                                       'REBILL',
                                                       'REBILL-CM')
                                      THEN
                                         cta.creation_date
                                      ELSE
                                         (cta.creation_date - 1)
                                   END)
                                     AS invoice_post_date
                             FROM ar.ra_customer_trx_all cta
                                  LEFT OUTER JOIN
                                  apps.xxwc_ar_payment_terms_vw arpt
                                     ON     cta.term_id = arpt.payterm_id
                                        AND cta.org_id =
                                               arpt.operating_unit_id
                                  LEFT OUTER JOIN
                                  (SELECT UPPER (cpc.name) AS cpc_name,
                                          site_use_id,
                                          cust_account_id,
                                          cpc.attribute4 AS cpc_grp
                                     FROM ar.hz_cust_profile_classes cpc
                                          INNER JOIN ar.hz_customer_profiles
                                             ON hz_customer_profiles.profile_class_id =
                                                   cpc.profile_class_id) cp
                                     ON     cta.bill_to_site_use_id =
                                               cp.site_use_id
                                        AND cta.bill_to_customer_id =
                                               cp.cust_account_id
                                  LEFT OUTER JOIN
                                  ar.ar_payment_schedules_all arps
                                     ON     cta.customer_trx_id =
                                               arps.customer_trx_id
                                        AND cta.org_id = arps.org_id
                                  LEFT OUTER JOIN
                                  ar.ra_cust_trx_line_gl_dist_all ctlgl
                                     ON     cta.customer_trx_id =
                                               ctlgl.customer_trx_id
                                        AND ctlgl.customer_trx_line_id
                                               IS NULL
                                  LEFT OUTER JOIN
                                  ar.ra_cust_trx_types_all ctt
                                     ON     cta.cust_trx_type_id =
                                               ctt.cust_trx_type_id
                                        AND cta.org_id = ctt.org_id
                                  LEFT OUTER JOIN ar.ra_batch_sources_all bs
                                     ON     cta.batch_source_id =
                                               bs.batch_source_id
                                        AND cta.org_id = bs.org_id
                                  /* 10/04/2014 CG: Added to pull header Created by NTID */
                                  LEFT OUTER JOIN applsys.fnd_user fu
                                     ON cta.created_by = fu.user_id
                            WHERE cta.last_update_date BETWEEN TRUNC (
                                                                  SYSDATE - 7)
                                                           AND SYSDATE) ct
                          INNER JOIN
                          (SELECT /*+ qb_name(ctl) no_merge use_hash(ctla ola) */
                                 ctla.created_by,
                                  ctla.creation_date,
                                  ctla.customer_trx_id,
                                  ctla.customer_trx_line_id,
                                  ctla.description,
                                  ctla.extended_amount,
                                  ctla.interface_line_attribute6,
                                  ctla.interface_line_context,
                                  ctla.last_update_date
                                     AS ctla_last_update_date,
                                  ctla.last_updated_by,
                                  ctla.line_number,
                                  ctla.line_type,
                                  ctla.org_id,
                                  ctla.quantity_credited,
                                  ctla.quantity_invoiced,
                                  ctla.quantity_ordered,
                                  ctla.sales_order,
                                  ctla.sales_order_date,
                                  ctla.unit_selling_price,
                                  ctla.uom_code,
                                  ctla.warehouse_id,
                                  ctla.actual_inventory_item_id,
                                  ola.line_id,
                                  ola.shipping_method_code,
                                  ola.pricing_date,
                                  ola.actual_shipment_date,
                                  ola.cust_po_number,
                                  ola.ordered_quantity,
                                  ola.unit_list_price,
                                  ola.source_type_code,
                                  ola.unit_cost,
                                  ola.attribute7,
                                  ola.header_id,
                                  ola.ship_from_org_id,
                                  ola.poh_vendor_site_id,
                                  ola.poh_vendor_id/* 10/06/14 CG */
                                  ,
                                  NVL (ola.has_dropship_cnt, 'N')
                                     has_dropship_cnt,
                                  ola.inventory_item_id,
                                  ola.attribute5,
                                  ola.line_category_code,
                                  ola.ol_last_update_date,
                                  NVL (ood1.org_id, 222) AS ood_org_id,
                                  ctla.tax_amount,
                                  ola.product_unit_cost,
                                  ola.original_modifier,
                                  ola.last_modifier,
                                  ola.overridden_sell_price,
                                  ola.price_type,
                                  ola.order_type_id,
                                  ola.msib_item_number,
                                  ola.msib_item_description,
                                  ola.msib_inventory_item_id, /* apps.xxwc_mv_routines_pkg.get_item_category call */
                                  mic.category_concat_segs AS prod_cat_class,
                                  ola.vqc_vendor_quote_cost,
                                  ola.PRICE_SRC_TYPE --ver1.5 Added by neha
                             FROM trx_lines ctla
                                  LEFT OUTER JOIN
                                  (SELECT /* replacement for apps.xxwc_mv_routines_pkg.get_mst_organization_id (sq_ra_cust_trx_lines_all.rctla_org_id) */
                                          DISTINCT
                                          (ood.organization_id) AS org_id,
                                          ood.operating_unit
                                     FROM apps.org_organization_definitions ood,
                                          inv.mtl_parameters mp
                                    WHERE ood.organization_id =
                                             mp.master_organization_id) ood1
                                     ON ood1.operating_unit = ctla.org_id
                                  LEFT OUTER JOIN
                                  (SELECT /*+ qb_name(ola) no_merge    */
                                         TO_CHAR (ol.line_id) AS line_id,
                                          ol.shipping_method_code,
                                          ol.pricing_date,
                                          ol.actual_shipment_date,
                                          ol.cust_po_number,
                                          ol.ordered_quantity,
                                          ol.unit_list_price,
                                          ol.source_type_code,
                                          ol.unit_cost,
                                          ol.attribute7,
                                          ol.header_id,
                                          ol.ship_from_org_id,
                                          pd.vendor_site_id
                                             AS poh_vendor_site_id,
                                          pd.vendor_id AS poh_vendor_id,
                                          ol.has_dropship_cnt,
                                          ol.inventory_item_id,
                                          ol.attribute5,
                                          ol.line_category_code,
                                          ol.last_update_date
                                             AS ol_last_update_date,
                                          COALESCE ( /* ola.mmt_order_line_cost*/
                                             (CASE
                                                 WHEN ol.line_category_code IN ('RETURN',
                                                                                'ORDER')
                                                 THEN
                                                    mmt1.order_line_cost
                                                 ELSE
                                                    NULL
                                              END),
                                             0)
                                             AS product_unit_cost,
                                          CASE
                                             WHEN    ( /* No automatic modifiers*/
                                                      pa  .count_auto_ln_modifiers =
                                                             0
                                                      AND pa.count_all_modifiers =
                                                             pa.count_manual_modifiers)
                                                  OR pa.adjustment_name
                                                        IS NULL
                                             THEN
                                                'MARKET PRICE'
                                             ELSE
                                                pa.first_modifier
                                          END
                                             AS original_modifier,
                                          pa.last_modifier,
                                          CASE /* ver.0.15 begin correction for C59_SYSTEM_PRICE */
                                             -- 10/06/14 CG: Updated to use new flag
                                             -- WHEN pa.price_typ_automatic_flag = 'N'
                                             WHEN NVL (
                                                     pa.aut_flag_for_selling_price,
                                                     'N') = 'N'
                                             THEN
                                                ol.unit_list_price
                                             /* ver.0.15 end correction for C59_SYSTEM_PRICE */
                                             WHEN pa.arithmetic_operator =
                                                     'NEWPRICE'
                                             THEN
                                                ROUND (pa.operand, 4)
                                             WHEN     pa.arithmetic_operator =
                                                         '%'
                                                  AND pa.list_line_type_code =
                                                         'DIS'
                                             THEN
                                                  ol.unit_list_price
                                                * (100 - pa.operand)
                                                / 100
                                             WHEN     pa.arithmetic_operator =
                                                         'AMT'
                                                  AND pa.list_line_type_code =
                                                         'DIS'
                                             THEN
                                                  ol.unit_list_price
                                                - pa.operand
                                             ELSE
                                                ol.unit_list_price
                                          END
                                             AS overridden_sell_price,
                                          CASE
                                             WHEN pa.price_typ_automatic_flag =
                                                     'N'
                                             THEN
                                                'MANUAL'
                                             WHEN    pa.lh_attribute10 =
                                                        'Contract Pricing'
                                                  OR pa.lh_name LIKE 'CSP%'
                                             THEN
                                                'CONTRACT'
                                             WHEN pa.lh_attribute10 =
                                                     'Vendor Quote'
                                             THEN
                                                'VQN'
                                             ELSE
                                                'SYSTEM'
                                          END
                                             AS price_type,
                                          vqc.vendor_quote_cost
                                             AS vqc_vendor_quote_cost/* following 4 used as replacement for apps.xxwc_mv_routines_pkg.get_om_rental_item */
                                          ,
                                          ol.order_type_id,
                                          ol.msib_item_number,
                                          ol.msib_item_description,
                                          ol.msib_inventory_item_id, /* ver.1.2  correction for duplicate header price adjustments */
                                          ROW_NUMBER ()
                                          OVER (
                                             PARTITION BY ol.header_id,
                                                          ol.line_id
                                             ORDER BY pa.line_id NULLS LAST)
                                             AS rn,
                                          pa.PRICE_SRC_TYPE --ver1.5 Added by neha
                                     FROM apps.xxwc_obiee_ct__ord_ln_mv ol
                                          LEFT OUTER JOIN
                                          (SELECT poh.vendor_site_id,
                                                  poh.vendor_id,
                                                  dss.line_id
                                             FROM po.po_headers_all poh
                                                  INNER JOIN
                                                  ont.oe_drop_ship_sources dss
                                                     ON dss.po_header_id =
                                                           poh.po_header_id)
                                          pd
                                             ON TO_CHAR (ol.line_id) =
                                                   TO_CHAR (pd.line_id)
                                          LEFT OUTER JOIN
                                          apps.xxwc_obiee_ct__vndr_quote_mv vqc
                                             ON TO_CHAR (vqc.order_line_id) =
                                                   TO_CHAR (ol.line_id)
                                          LEFT OUTER JOIN
                                          apps.xxwc_obiee_ct__ord_ln_cost_mv mmt1
                                             ON TO_CHAR (
                                                   mmt1.trx_source_line_id) =
                                                   TO_CHAR (ol.line_id)
                                          LEFT OUTER JOIN
                                          apps.xxwc_obiee_ct__prc_adj_mv pa
                                             ON     ol.header_id =
                                                       pa.header_id
                                                AND TO_CHAR (ol.line_id) =
                                                       TO_CHAR (
                                                          NVL (pa.line_id,
                                                               ol.line_id)))
                                  ola
                                     ON     TO_CHAR (
                                               ctla.interface_line_attribute6) =
                                               TO_CHAR (ola.line_id)
                                        AND ola.rn = 1
                                  LEFT OUTER JOIN /* replacement for apps.xxwc_mv_routines_pkg.get_item_category call */
                                  apps.xxwc_obiee_ct__mtl_item_cat_mv mic
                                     /* per CG: the msib_inventory_item_id holds the actual rental item and not the charge item,
                                                but will be blank for non OM invoices
                                                Changing the join from
                                                  mic.inventory_item_id = ctla.actual_inventory_item_id
                                                to
                                                  mic.inventory_item_id = NVL(ola.msib_inventory_item_id, ctla.actual_inventory_item_id)
                                     */
                                     ON     mic.inventory_item_id =
                                               NVL (
                                                  ola.msib_inventory_item_id,
                                                  ctla.actual_inventory_item_id)
                                        AND mic.organization_id =
                                               ctla.warehouse_id) ctl
                             ON ct.customer_trx_id = ctl.customer_trx_id
                          LEFT OUTER JOIN
                          apps.xxwc_obiee_ct__po_price_mv pol_up
                             ON /* 10/02/2014 CG: Updated to link with an inventory item id field always populated
                                   ctl.inventory_item_id = pol_up.item_id
                                   */
                               ctl  .actual_inventory_item_id =
                                       pol_up.item_id
                                AND COALESCE (ctl.warehouse_id,
                                              ctl.ood_org_id) =
                                       pol_up.to_organization_id
                          LEFT OUTER JOIN
                          (SELECT /*+ qb_name(oh) no_merge  */
                                 TO_CHAR (oha.order_number) AS order_number,
                                  oha.creation_date,
                                  oha.booked_date,
                                  oha.last_update_date,
                                  oha.attribute2,
                                  oha.sales_channel_code,
                                  oha.attribute7,
                                  oha.header_id,
                                  oha.created_by/* 10/04/2014 CG: Added to pull header Created by NTID*/
                                  ,
                                  fu.user_name created_by_ntid,
                                  oha.order_type_id,
                                  UPPER (tt.name) AS tt_name,
                                  oha.ship_from_org_id,
                                  oha.order_source_id
                             FROM ont.oe_order_headers_all oha
                                  LEFT OUTER JOIN
                                  ont.oe_transaction_types_tl tt
                                     ON oha.order_type_id =
                                           tt.transaction_type_id
                                  /* 10/04/2014 CG: Added to pull header Created by NTID */
                                  LEFT OUTER JOIN applsys.fnd_user fu
                                     ON oha.created_by = fu.user_id) oh
                             ON ct.interface_header_attribute1 =
                                   oh.order_number) ctl1
                  LEFT OUTER JOIN
                  (SELECT /*+ qb_name(cas1) no_merge */
                         cas.cust_account_id AS cust_account_id,
                          csu.primary_salesrep_id AS account_sales_rep_id,
                          csu.cust_acct_site_id AS cust_pri_bill_to_id,
                          ROW_NUMBER ()
                          OVER (PARTITION BY cas.cust_account_id
                                ORDER BY ROWNUM)
                             AS cas_rn
                     FROM ar.hz_cust_acct_sites_all cas
                          INNER JOIN ar.hz_cust_site_uses_all csu
                             ON     cas.cust_acct_site_id =
                                       csu.cust_acct_site_id
                                AND csu.site_use_code = 'BILL_TO'
                                AND csu.status = 'A'
                                AND csu.primary_flag = 'Y') cas1
                     ON     ctl1.customer_account_id = cas1.cust_account_id
                        AND cas_rn = 1
                  CROSS JOIN (SELECT organization_id
                                FROM inv.mtl_parameters
                               WHERE organization_code = 'WCC') par
                  LEFT OUTER JOIN inv_adj_amount iaa
                     ON     iaa.customer_trx_id = ctl1.customer_trx_id
                        AND iaa.customer_trx_line_id =
                               NVL (ctl1.customer_trx_line_id,
                                    iaa.customer_trx_line_id)
                        AND iaa.org_id = ctl1.rcta_org_id) glo
          LEFT OUTER JOIN salesrep sr /* 10/06/2014 CG: Updated to use the appropriate field in the initial query
                                      -- ON glo.account_sales_rep_id = sr.salesrep_id
                                      */
             ON glo.sales_rep_id = sr.salesrep_id AND glo.org_id = sr.org_id
          LEFT OUTER JOIN salesrep sra
             ON     glo.account_sales_rep_id = sra.salesrep_id
                AND glo.org_id = sra.org_id
          LEFT OUTER JOIN
          (SELECT /*+ qb_name(u) no_merge use_hash(u ppl) */
                 ppl.full_name,
                  u.user_name,
                  ppl.effective_start_date,
                  ppl.effective_end_date
             FROM hr.per_all_people_f ppl
                  INNER JOIN applsys.fnd_user u
                     ON     u.employee_id = ppl.person_id
                        AND ppl.person_type_id = 6) u1
             ON     glo.order_create_by_ntid = u1.user_name
                AND glo.order_creation_date >= u1.effective_start_date
                AND glo.order_creation_date <=
                       COALESCE (u1.effective_end_date, SYSDATE);


COMMENT ON MATERIALIZED VIEW APPS.XXWC_OBIEE_CUST_TRX_INCR_MV IS
   'snapshot table for snapshot APPS.XXWC_OBIEE_CUST_TRX_INCR_MV';