CREATE OR REPLACE PACKAGE APPS.XXWC_OM_PRC_CONSTRAINTS_PKG
AS
   /********************************************************************************
      FILE NAME: XXWC_OM_PRC_CONSTRAINTS_PKG.pkg

      PROGRAM TYPE: PL/SQL Package Body

      PURPOSE: Restrict sales order cancellation,updation using processing constraints

      HISTORY
      ===============================================================================
      VERSION DATE          AUTHOR(S)       DESCRIPTION
      ------- -----------   --------------- -----------------------------------------
      1.0     1/07/2014     Harsha          Initial creation of the procedure
      1.2     05/23/2017    Rakesh Patel    TMS# 20170323-00225 --Custom SOE entry - Phase2  
      1.3     7/20/2017     Neha Saini        TMSTask ID: 20170719-00212 added new procedure   
      ********************************************************************************/
   --This function is to check the end user profile organization.


    V_ERRBUF  VARCHAR2(3000);
   FUNCTION XXWC_GET_USER_PROF_VAL (V_PROFILE_USER VARCHAR2)
      RETURN NUMBER;

   --This Procedure is for restricting order line cancellation if enduser org id<> sales order line org id and order status is booked


   PROCEDURE CANCEL_LINE (
      p_application_id                 IN            NUMBER,
      p_entity_short_name              IN            VARCHAR2,
      p_validation_entity_short_name   IN            VARCHAR2,
      p_validation_tmplt_short_name    IN            VARCHAR2,
      p_record_set_short_name          IN            VARCHAR2,
      p_scope                          IN            VARCHAR2,
      x_result                            OUT NOCOPY NUMBER);

   --This procedure is to restrict order line quantity updation if enduser org id <> sales order line org id and order status is booked

   PROCEDURE update_LINE (
      p_application_id                 IN            NUMBER,
      p_entity_short_name              IN            VARCHAR2,
      p_validation_entity_short_name   IN            VARCHAR2,
      p_validation_tmplt_short_name    IN            VARCHAR2,
      p_record_set_short_name          IN            VARCHAR2,
      p_scope                          IN            VARCHAR2,
      x_result                            OUT NOCOPY NUMBER);

   --This Procedure is for restricting order header cancellation if enduser org id<> sales order line org id and order status is booked

   PROCEDURE CANCEL_HEADER (
      p_application_id                 IN            NUMBER,
      p_entity_short_name              IN            VARCHAR2,
      p_validation_entity_short_name   IN            VARCHAR2,
      p_validation_tmplt_short_name    IN            VARCHAR2,
      p_record_set_short_name          IN            VARCHAR2,
      p_scope                          IN            VARCHAR2,
      x_result                            OUT NOCOPY NUMBER);
      
   --Rev#1.2 < Start
   PROCEDURE CHANGE_LINE_WAREHOUSE (
      p_application_id                 IN            NUMBER,
      p_entity_short_name              IN            VARCHAR2,
      p_validation_entity_short_name   IN            VARCHAR2,
      p_validation_tmplt_short_name    IN            VARCHAR2,
      p_record_set_short_name          IN            VARCHAR2,
      p_scope                          IN            VARCHAR2,
      x_result                            OUT NOCOPY NUMBER);
   --Rev#1.2 > End  
   
   --ver1.3 starts
PROCEDURE CANCEL_LINE_QUANTITY (
   p_application_id                 IN            NUMBER,
   p_entity_short_name              IN            VARCHAR2,
   p_validation_entity_short_name   IN            VARCHAR2,
   p_validation_tmplt_short_name    IN            VARCHAR2,
   p_record_set_short_name          IN            VARCHAR2,
   p_scope                          IN            VARCHAR2,
   x_result                            OUT NOCOPY NUMBER);
   --ver1.3 ends
END XXWC_OM_PRC_CONSTRAINTS_PKG;
/