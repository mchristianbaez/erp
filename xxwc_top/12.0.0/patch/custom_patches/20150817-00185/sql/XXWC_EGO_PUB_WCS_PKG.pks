CREATE OR REPLACE PACKAGE XXWC_EGO_PUB_WCS_PKG
/*******************************************************************************************************
File Name: XXWC_EGO_PUB_WCS_PKG
PROGRAM TYPE: PL/SQL Package spec and body
PURPOSE: Procedures and functions for creating the WCSextracts
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     09/23/2012    Rajiv Rathod    Initial creation of the package
2.0     10-Jan-2014   Praveen Pawar   Added procedure sku_pub_hist_proc
3.0     16-Sep-2015   P.vamshidhar    TMS#20150817-00185 - PDH E-Comm catalog file changes
                                      Added new procedure to caputure category changes
********************************************************************************************************/
AS
   /********************************************************************************
   PROGRAM TYPE: PROCEDURE
   NAME: sku_pub_hist_proc
   PURPOSE: Procedure will maintain the Website Item SKUs with published value
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     10-Jan-2014   Praveen Pawar    Initial creation of the procedure
       ********************************************************************************/
   PROCEDURE sku_pub_hist_proc (errbuf       OUT NOCOPY VARCHAR2,
                                retcode      OUT NOCOPY VARCHAR2);

   /********************************************************************************
               PROGRAM TYPE: FUNCTION
   NAME: publish_ready
   PURPOSE: Function to determine the eligilibilty of publish
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/23/2012    Rajiv Rathod    Initial creation of the function
       ********************************************************************************/

   FUNCTION publish_ready (p_item_id IN NUMBER)
      RETURN NUMBER;

   /********************************************************************************
         PROGRAM TYPE: FUNCTION
      NAME: publish_ready_date
      PURPOSE: Function to determine the eligilibilty of publish based on date
      HISTORY
      ===============================================================================
      VERSION DATE          AUTHOR(S)       DESCRIPTION
      ------- -----------   --------------- -----------------------------------------
      1.0     09/23/2012    Rajiv Rathod    Initial creation of the function
       ********************************************************************************/

   FUNCTION publish_ready_date (p_item_id IN NUMBER, p_LAST_RUN_DATE DATE)
      RETURN NUMBER;

   /********************************************************************************
                     PROGRAM TYPE: FUNCTION
      NAME: allowed_values
      PURPOSE: Function to provide the values in value set
      HISTORY
      ===============================================================================
      VERSION DATE          AUTHOR(S)       DESCRIPTION
      ------- -----------   --------------- -----------------------------------------
      1.0     09/23/2012    Rajiv Rathod    Initial creation of the function
       ********************************************************************************/
   FUNCTION allowed_values (p_value_set_id IN NUMBER)
      RETURN VARCHAR2;

   /********************************************************************************
                     PROGRAM TYPE: FUNCTION
      NAME: get_flex_value
      PURPOSE: Function to value of value set based on code
      HISTORY
      ===============================================================================
      VERSION DATE          AUTHOR(S)       DESCRIPTION
      ------- -----------   --------------- -----------------------------------------
      1.0     09/23/2012    Rajiv Rathod    Initial creation of the function
       ********************************************************************************/
   FUNCTION get_flex_value (p_value_set_id      IN NUMBER,
                            p_validation_code   IN VARCHAR2,
                            p_value             IN VARCHAR2)
      RETURN VARCHAR2;

   /********************************************************************************
                     PROGRAM TYPE: PROCEDURE
      NAME: CREATE_ITEM_DATA
      PURPOSE: Procedure to generate the Item data
      HISTORY
      ===============================================================================
      VERSION DATE          AUTHOR(S)       DESCRIPTION
      ------- -----------   --------------- -----------------------------------------
      1.0     09/23/2012    Rajiv Rathod    Initial creation of the procedure
       ********************************************************************************/
   PROCEDURE CREATE_ITEM_DATA (P_TIMESTAMP IN VARCHAR2, L_LAST_RUN_DATE DATE);

   /********************************************************************************
                     PROGRAM TYPE: PROCEDURE
      NAME: CREATE_ITEM_ATTR_DATA
      PURPOSE: Procedure to generate the Item attribute data
      HISTORY
      ===============================================================================
      VERSION DATE          AUTHOR(S)       DESCRIPTION
      ------- -----------   --------------- -----------------------------------------
      1.0     09/23/2012    Rajiv Rathod    Initial creation of the procedure
       ********************************************************************************/

   PROCEDURE CREATE_ITEM_ATTR_DATA (P_TIMESTAMP       IN VARCHAR2,
                                    L_LAST_RUN_DATE      DATE);

   /********************************************************************************
                     PROGRAM TYPE: PROCEDURE
      NAME: CREATE_ITEM_DICTNRY_DATA
      PURPOSE: Procedure to generate the attribute dictionary data
      HISTORY
      ===============================================================================
      VERSION DATE          AUTHOR(S)       DESCRIPTION
      ------- -----------   --------------- -----------------------------------------
      1.0     09/23/2012    Rajiv Rathod    Initial creation of the procedure
       ********************************************************************************/

   PROCEDURE CREATE_ITEM_DICTNRY_DATA (P_TIMESTAMP       IN VARCHAR2,
                                       L_LAST_RUN_DATE      DATE);

   /********************************************************************************
                     PROGRAM TYPE: PROCEDURE
      NAME: CREATE_PROD_HIER_DATA
      PURPOSE: Procedure to generate the product hierarchy data
      HISTORY
      ===============================================================================
      VERSION DATE          AUTHOR(S)       DESCRIPTION
      ------- -----------   --------------- -----------------------------------------
      1.0     09/23/2012    Rajiv Rathod    Initial creation of the procedure
       ********************************************************************************/

   PROCEDURE CREATE_PROD_HIER_DATA (P_TIMESTAMP       IN VARCHAR2,
                                    L_LAST_RUN_DATE      DATE,
                                    FROM_DATE            VARCHAR2);

   PROCEDURE CREATE_PROD_RECL_DATA (P_TIMESTAMP       IN VARCHAR2,
                                    L_LAST_RUN_DATE      DATE);

   /********************************************************************************
               PROGRAM TYPE: PROCEDURE
      NAME: PUB_WCS_PROC
      PURPOSE: Main procedure to generate the extracts
      HISTORY
      ===============================================================================
      VERSION DATE          AUTHOR(S)       DESCRIPTION
      ------- -----------   --------------- -----------------------------------------
      1.0     09/23/2012    Rajiv Rathod    Initial creation of the procedure
       ********************************************************************************/
   PROCEDURE PUB_WCS_PROC (ERRBUF                        OUT NOCOPY VARCHAR2,
                           RETCODE                       OUT NOCOPY VARCHAR2,
                           ITEM_DATA                  IN            VARCHAR2,
                           ITEM_ATTRIBUTE             IN            VARCHAR2,
                           ATTR_DICTIONARY            IN            VARCHAR2,
                           PRODUCT_HIERARCHY          IN            VARCHAR2,
                           PRODUCT_RECLASSIFICATION   IN            VARCHAR2,
                           FROM_DATE                  IN            VARCHAR2);

   /********************************************************************************
               PROGRAM TYPE: FUNCTION
      NAME: PUB_WCS_PROC
      PURPOSE: Function to capture business events for WCS integration
      HISTORY
      ===============================================================================
      VERSION DATE          AUTHOR(S)       DESCRIPTION
      ------- -----------   --------------- -----------------------------------------
      1.0     09/23/2012    Rajiv Rathod    Initial creation of the function
       ********************************************************************************/
   FUNCTION process_pre_event (
      p_subscription_guid   IN            RAW,
      p_event               IN OUT NOCOPY WF_EVENT_T)
      RETURN VARCHAR2;

   /*************************************************************************
      *   Procedure : submit_job
      *
      *   PURPOSE:   This procedure is called by UC4 to initiate Publish CP: XXWC Publish data to WCS
      *   Parameter:
      *          IN
      *              p_user_name              -- User name to initilize the request
      *               p_responsibility_name    -- Responsibility name to initilize the request
      *           OUT:
      *              errbuf                 -- contains the error details
      *              retcode                -- 0 = Success, 1 = Warning, 2 = ERROR

       HISTORY
      ===============================================================================
       VERSION DATE          AUTHOR(S)       DESCRIPTION
       ------- -----------   --------------- -----------------------------------------
       1.0     05/29/2013    Rajasekar G    Initial creation of the procedure
      **********************************************************************************/
   PROCEDURE submit_job (errbuf           OUT NOCOPY VARCHAR2,
                         retcode          OUT NOCOPY VARCHAR2,
                         p_user_name   IN            VARCHAR2);


   -- Below procedure added in Revision 3.0
   /********************************************************************************************************
      *   Procedure : CATEGORY_DATA_EXTRACT
      *
      *   PURPOSE   : Procedure to track category changes for deletion file purpose.
      *               wc_cataloggroups_d.csv

      *   Parameter:
      *          IN
      *               p_category_set_id        -- Category set id
      *               p_structure_id           -- Structure id
      *               p_last_request_id        -- Prior request id
       HISTORY
      ===============================================================================
       VERSION DATE          AUTHOR(S)       DESCRIPTION
       ------- -----------   --------------- -----------------------------------------
       3.0     09/16/2015    P.vamshidhar    Initial creation of the procedure
                                             TMS#20150817-00185 - PDH E-Comm catalog file changes
      *****************************************************************************************************/
   PROCEDURE CATEGORY_DATA_EXTRACT (p_category_set_id   IN NUMBER,
                                    p_structure_id      IN NUMBER,
                                    p_last_request_id   IN NUMBER);
END XXWC_EGO_PUB_WCS_PKG;
/