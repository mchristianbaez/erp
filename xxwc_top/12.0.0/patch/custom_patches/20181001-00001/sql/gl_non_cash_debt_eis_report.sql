/* Following file contains below Objects Source/Objects Metadata/Reports Metadata/ LOVs: 
*/
/*Running the file from XXEIS works only the reports creation and views that owned by XXEIS.*/
--Report Name            : GL Non Cash Debt
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_2501584_NZGGZN_V
/* ************************************************************************
 HD Supply
 All rights reserved.
**************************************************************************
  PURPOSE: Treasury alerts for non cash debit transactions.
  REVISIONS:
   Ver          Date            Author                     Description
   ----------   ----------      ---------------------      -------------------------
   1.0          10/10/2018      Ashwin Sridhar             Initial Build-TMS#20181001-00001
**************************************************************************/
prompt Table type Objects cannot be imported
set scan off define off
prompt Creating Object APPS.XXEIS_2501584_NZGGZN_V
set scan off define off
DECLARE
mod_exist varchar2(1);
stmt CLOB;
v_objlength INTEGER := 0;
v_offset INTEGER := 1;
v_sql_long DBMS_SQL.varchar2s;
v_sql_long_count INTEGER := 1;
c NUMBER;
r NUMBER;
l_stmt varchar2(32000);
l_view_cur pls_integer;
l_view_rows NUMBER;
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
dbms_lob.createtemporary(stmt,TRUE);
dbms_lob.write( lob_loc   => stmt,amount    => length('create or replace view APPS.XXEIS_2501584_NZGGZN_V
 ("GL_SOURCE","GL_CATEGORY","JOURNAL_BATCH_NAME","DEBIT","CREDIT","LINE_DESCRIPTION","ACCOUNT_STRING","JE_BATCH_ID") as '),offset    => 1, BUFFER    => 'create or replace view APPS.XXEIS_2501584_NZGGZN_V
 ("GL_SOURCE","GL_CATEGORY","JOURNAL_BATCH_NAME","DEBIT","CREDIT","LINE_DESCRIPTION","ACCOUNT_STRING","JE_BATCH_ID") as ');
l_stmt :=  'SELECT glsrc.user_je_source_name gl_source
,      gls.user_je_category_name gl_category
,      jb.name  journal_batch_name
,      TO_CHAR(SUM(b.ENTERED_DR), ''FM$999,999,990.90'') Debit
,      TO_CHAR(SUM(b.ENTERED_CR), ''FM$999,999,990.90'') Credit
,      b.DESCRIPTION line_description
,      gcc.CONCATENATED_SEGMENTS Account_String
,      jb.je_batch_id je_batch_id
FROM   gl.gl_je_headers a
,      gl.gl_je_lines  b
,      gl.gl_je_categories_tl gls
,      gl.gl_je_batches jb
,      gl.gl_je_sources_tl glsrc
,      apps.gl_code_combinations_kfv gcc
WHERE 1 =1
AND gls.user_je_category_name =''Non Cash Debt''
AND gls.je_category_name      = a.je_category
AND jb.je_batch_id            = a.je_batch_id
AND glsrc.je_source_name      = a.je_source
AND a.je_header_id = b.je_header_id
AND b.code_combination_id = gcc.code_combination_id
AND NOT EXISTS
     (
       SELECT 1
       FROM XXCUS.XXCUS_GL_NON_CASH_DEBT_TBL
       WHERE 1 =1
       AND je_batch_id =jb.je_batch_id
     )
GROUP BY glsrc.user_je_source_name
,        gls.user_je_category_name
,        jb.name
,        jb.je_batch_id
,        b.DESCRIPTION 
,       gcc.CONCATENATED_SEGMENTS

';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
v_objlength := DBMS_LOB.getlength (stmt);
IF v_objlength <= 32000 THEN 
EXECUTE IMMEDIATE to_char(stmt);
ELSE
v_offset := 1; 
WHILE v_offset <= v_objlength
LOOP
-- each record is 256 bytes from the LOB
v_sql_long (v_sql_long_count) := DBMS_LOB.SUBSTR (stmt, 256, v_offset);
-- increment the record count
v_sql_long_count := v_sql_long_count + 1;
-- figure out the new offset
v_offset := v_offset + 256;
END LOOP;
BEGIN
  -- open cursor
 c := DBMS_SQL.open_cursor;
 -- parse VARCHAR2S table
 DBMS_SQL.parse (c, v_sql_long, 1, v_sql_long_count - 1, FALSE, 1);
 -- Execute the cursor
r := DBMS_SQL.EXECUTE (c);
-- Close the cursor
 DBMS_SQL.close_cursor (c);
 END;
END IF;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Object Data XXEIS_2501584_NZGGZN_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_2501584_NZGGZN_V
xxeis.eis_rsc_ins.v( 'XXEIS_2501584_NZGGZN_V',101,'Paste SQL View for GL Non Cash Debt','','','','AS066570','APPS','GL Non Cash Debt View','X_NV','','Y','VIEW','US','','','');
--Delete Object Columns for XXEIS_2501584_NZGGZN_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_2501584_NZGGZN_V',101,FALSE);
--Inserting Object Columns for XXEIS_2501584_NZGGZN_V
xxeis.eis_rsc_ins.vc( 'XXEIS_2501584_NZGGZN_V','GL_SOURCE',101,'Gl Source','','','','','AS066570','VARCHAR2','','','Gl Source','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2501584_NZGGZN_V','GL_CATEGORY',101,'Gl Category','','','','','AS066570','VARCHAR2','','','Gl Category','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2501584_NZGGZN_V','JE_BATCH_ID',101,'Je Batch Id','','','','','AS066570','NUMBER','','','Je Batch Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2501584_NZGGZN_V','LINE_DESCRIPTION',101,'Line Description','','','','','AS066570','VARCHAR2','','','Line Description','','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2501584_NZGGZN_V','ACCOUNT_STRING',101,'Account String','','','','','AS066570','VARCHAR2','','','Account String','','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2501584_NZGGZN_V','CREDIT',101,'Credit','','','','','AS066570','VARCHAR2','','','Credit','','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2501584_NZGGZN_V','DEBIT',101,'Debit','','','','','AS066570','VARCHAR2','','','Debit','','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_2501584_NZGGZN_V','JOURNAL_BATCH_NAME',101,'Journal Batch Name','','','','','AS066570','VARCHAR2','','','Journal Batch Name','','','','','');
--Inserting Object Components for XXEIS_2501584_NZGGZN_V
--Inserting Object Component Joins for XXEIS_2501584_NZGGZN_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report GL Non Cash Debt
prompt Creating Report Data for GL Non Cash Debt
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Deleting Report data - GL Non Cash Debt
xxeis.eis_rsc_utility.delete_report_rows( 'GL Non Cash Debt',101 );
--Inserting Report - GL Non Cash Debt
xxeis.eis_rsc_ins.r( 101,'GL Non Cash Debt','','This report extracts the journals information for Non-Cash debt','','','','AS066570','XXEIS_2501584_NZGGZN_V','Y','','SELECT glsrc.user_je_source_name gl_source
,      gls.user_je_category_name gl_category
,      jb.name  journal_batch_name
,      TO_CHAR(SUM(b.ENTERED_DR), ''FM$999,999,990.90'') Debit
,      TO_CHAR(SUM(b.ENTERED_CR), ''FM$999,999,990.90'') Credit
,      b.DESCRIPTION line_description
,      gcc.CONCATENATED_SEGMENTS Account_String
,      jb.je_batch_id je_batch_id
FROM   gl.gl_je_headers a
,      gl.gl_je_lines  b
,      gl.gl_je_categories_tl gls
,      gl.gl_je_batches jb
,      gl.gl_je_sources_tl glsrc
,      apps.gl_code_combinations_kfv gcc
WHERE 1 =1
AND gls.user_je_category_name =''Non Cash Debt''
AND gls.je_category_name      = a.je_category
AND jb.je_batch_id            = a.je_batch_id
AND glsrc.je_source_name      = a.je_source
AND a.je_header_id = b.je_header_id
AND b.code_combination_id = gcc.code_combination_id
AND NOT EXISTS
     (
       SELECT 1
       FROM XXCUS.XXCUS_GL_NON_CASH_DEBT_TBL
       WHERE 1 =1
       AND je_batch_id =jb.je_batch_id
     )
GROUP BY glsrc.user_je_source_name
,        gls.user_je_category_name
,        jb.name
,        jb.je_batch_id
,        b.DESCRIPTION 
,       gcc.CONCATENATED_SEGMENTS
','AS066570','','N','XXEIS Base Reports','','EXCEL,','N','','','','','','N','APPS','US','','','','','','','','GL Non Cash Debt Report','Please find the attached excel output on GL Non Cash Debt Report','Y','','URL,DashbLink,','N','','','');
--Inserting Report Columns - GL Non Cash Debt
xxeis.eis_rsc_ins.rc( 'GL Non Cash Debt',101,'GL_SOURCE','Gl Source','Gl Source','VARCHAR2','','default','','1','','Y','','','','','','','AS066570','N','N','','XXEIS_2501584_NZGGZN_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'GL Non Cash Debt',101,'GL_CATEGORY','Gl Category','Gl Category','VARCHAR2','','default','','2','','Y','','','','','','','AS066570','N','N','','XXEIS_2501584_NZGGZN_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'GL Non Cash Debt',101,'LINE_DESCRIPTION','Line Description','Line Description','VARCHAR2','','default','','7','','Y','','','','','','','AS066570','N','N','','XXEIS_2501584_NZGGZN_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'GL Non Cash Debt',101,'ACCOUNT_STRING','Account String','Account String','VARCHAR2','','','','4','','Y','','','','','','','AS066570','','','','XXEIS_2501584_NZGGZN_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'GL Non Cash Debt',101,'CREDIT','Credit','Credit','VARCHAR2','','','','6','','Y','','','','','','','AS066570','','','','XXEIS_2501584_NZGGZN_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'GL Non Cash Debt',101,'DEBIT','Debit','Debit','VARCHAR2','','','','5','','Y','','','','','','','AS066570','','','','XXEIS_2501584_NZGGZN_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'GL Non Cash Debt',101,'JOURNAL_BATCH_NAME','Journal Batch Name','Journal Batch Name','VARCHAR2','','','','3','','Y','','','','','','','AS066570','','','','XXEIS_2501584_NZGGZN_V','','','','US','','');
--Inserting Report Parameters - GL Non Cash Debt
--Inserting Dependent Parameters - GL Non Cash Debt
--Inserting Report Conditions - GL Non Cash Debt
--Inserting Report Sorts - GL Non Cash Debt
--Inserting Report Triggers - GL Non Cash Debt
xxeis.eis_rsc_ins.rt( 'GL Non Cash Debt',101,'BEGIN
XXCUS_GL_NONCASH_ALERT_PKG.non_cash_db;
END;','A','Y','AS066570','AQ');
--inserting report templates - GL Non Cash Debt
--Inserting Report Portals - GL Non Cash Debt
--inserting report dashboards - GL Non Cash Debt
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'GL Non Cash Debt','101','XXEIS_2501584_NZGGZN_V','XXEIS_2501584_NZGGZN_V','N','');
--inserting report security - GL Non Cash Debt
xxeis.eis_rsc_ins.rsec( 'GL Non Cash Debt','','BS006141','',101,'AS066570','','Y','');
xxeis.eis_rsc_ins.rsec( 'GL Non Cash Debt','','SA059956','',101,'AS066570','','N','');
xxeis.eis_rsc_ins.rsec( 'GL Non Cash Debt','101','','GENERAL_LEDGER_SUPER_USER',101,'AS066570','','','');
xxeis.eis_rsc_ins.rsec( 'GL Non Cash Debt','','VR073604','',101,'AS066570','','Y','');
--Inserting Report Pivots - GL Non Cash Debt
--Inserting Report Distribution Details 
EXECUTE IMMEDIATE 'DELETE FROM xxeis.eis_rs_report_distribution WHERE report_id = 
          (SELECT report_id FROM xxeis.eis_Rs_reports WHERE report_name = ''GL Non Cash Debt'' AND application_id = 101)';
xxeis.eis_rsc_ins.rdist( 'GL Non Cash Debt',101,'AS066570','','','ashwin.sridhar2@hdsupply.com','','','',NULL,'AS066570','User','','',-1,'','','','','','','','','','','');
xxeis.eis_rsc_ins.rdist( 'GL Non Cash Debt',101,'AO014801','','','Anthony.Oneal@hdsupply.com','','','',NULL,'AO014801','User','','',-1,'','','','','','','','','','','');
xxeis.eis_rsc_ins.rdist( 'GL Non Cash Debt',101,'VR073604','','','Vijayashanker.Ramaswamy@hdsupply.com','','','',NULL,'VR073604','User','','',-1,'','','','','','','','','','','');
xxeis.eis_rsc_ins.rdist( 'GL Non Cash Debt',101,'BS006141','','','balaguru.seshadri@hdsupply.com','','','',NULL,'BS006141','User','','',-1,'','','','','','','','','','','');
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- GL Non Cash Debt
xxeis.eis_rsc_ins.rv( 'GL Non Cash Debt','','GL Non Cash Debt','AS066570','10-OCT-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/

