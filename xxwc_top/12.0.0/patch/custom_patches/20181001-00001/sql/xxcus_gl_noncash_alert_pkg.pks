CREATE OR REPLACE PACKAGE XXCUS_GL_NONCASH_ALERT_PKG AS
/* ************************************************************************
 HD Supply
 All rights reserved.
**************************************************************************
  PURPOSE: Treasury alerts for non cash debit transactions.
  REVISIONS:
   Ver          Date            Author                     Description
   ----------   ----------      ---------------------      -------------------------
   1.0          09/27/2018      Ashwin Sridhar             Initial Build-TMS#20181001-00001
**************************************************************************/

PROCEDURE NON_CASH_DB;

END XXCUS_GL_NONCASH_ALERT_PKG;
/
