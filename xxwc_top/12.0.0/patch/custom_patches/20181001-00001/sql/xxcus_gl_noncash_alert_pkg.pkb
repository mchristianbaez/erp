CREATE OR REPLACE PACKAGE BODY XXCUS_GL_NONCASH_ALERT_PKG AS
/* ************************************************************************
 HD Supply
 All rights reserved.
**************************************************************************
  PURPOSE: Treasury alerts for non cash debit transactions.
  REVISIONS:
   Ver          Date            Author                     Description
   ----------   ----------      ---------------------      -------------------------
   1.0          09/27/2018      Ashwin Sridhar             Initial Build-TMS#20181001-00001
**************************************************************************/

PROCEDURE PRINT_LOG(p_message IN VARCHAR2) IS
BEGIN

 IF fnd_global.conc_request_id >0 THEN
 
   fnd_file.put_line(fnd_file.LOG, p_message);
  
 ELSE
 
   dbms_output.put_line(p_message);
  
 END IF;
 
EXCEPTION
WHEN others THEN

  fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||SQLERRM);
  
END PRINT_LOG;
  
PROCEDURE non_cash_db IS

CURSOR c_gl_batches IS
SELECT glsrc.user_je_source_name gl_source
,      gls.user_je_category_name gl_category
,      jb.name  jrnl_batch
,      TO_CHAR(SUM(a.running_total_dr), 'FM$999,999,990.90') total_dr
,      TO_CHAR(SUM(a.running_total_cr), 'FM$999,999,990.90') total_cr
,      jb.je_batch_id je_batch_id
FROM   gl.gl_je_headers a
,      gl.gl_je_categories_tl gls
,      gl.gl_je_batches jb
,      gl.gl_je_sources_tl glsrc
WHERE 1 =1
AND gls.user_je_category_name ='Non Cash Debt'
AND gls.je_category_name      = a.je_category
AND jb.je_batch_id            = a.je_batch_id
AND glsrc.je_source_name      = a.je_source
AND NOT EXISTS
(SELECT 1
 FROM  XXCUS.XXCUS_GL_NON_CASH_DEBT_TBL
 WHERE 1 =1
 AND   je_batch_id =jb.je_batch_id
)
GROUP BY glsrc.user_je_source_name
,        gls.user_je_category_name
,        jb.name
,        jb.je_batch_id ;    
 
TYPE c_gl_batches_type IS TABLE OF c_gl_batches%ROWTYPE INDEX BY BINARY_INTEGER;
c_gl_batches_rec c_gl_batches_type;

BEGIN
            
  OPEN  c_gl_batches;
  FETCH c_gl_batches BULK COLLECT INTO c_gl_batches_rec;
  CLOSE c_gl_batches;

  IF  c_gl_batches_rec.COUNT >0 THEN

    FOR idx IN 1 .. c_gl_batches_rec.COUNT LOOP

      BEGIN
      
        print_log(' ');
        print_log('JE Batch ID ='||c_gl_batches_rec(idx).je_batch_id);
             
        INSERT INTO XXCUS.XXCUS_GL_NON_CASH_DEBT_TBL values (c_gl_batches_rec(idx).je_batch_id); 

      EXCEPTION
      WHEN others THEN
                       
        NULL;
        
        print_log('@send_email_notif : je batch id =>'||c_gl_batches_rec(idx).je_batch_id);                       
        print_log('@send_email_notif : error msg =>'||SQLERRM); 

      END;          

    END LOOP;

  END IF;

  COMMIT;

EXCEPTION
WHEN others THEN
   
  print_log('Inside Main Exception '||SQLERRM);

END NON_CASH_DB;

END XXCUS_GL_NONCASH_ALERT_PKG;
/
