--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_FLEX_VAL_TAB
  Description: This table is used to get data from XXEIS.EIS_XXWC_INV_LIST_RPT_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     05/06/2016        Pramod   		--TMS#20160503-00085  Performance Tuning
********************************************************************************/
CREATE TABLE XXEIS.EIS_XXWC_FLEX_VAL_TAB 
   (	PROCESS_ID NUMBER, 
	FLEX_VALUE VARCHAR2(240 BYTE), 
	DESCRIPTION VARCHAR2(2000 BYTE)
)
/
