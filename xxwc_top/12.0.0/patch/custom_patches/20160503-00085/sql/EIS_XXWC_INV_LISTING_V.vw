---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_INV_LISTING_V $
  PURPOSE	  : Inventory Listing Report
  TMS Task Id : TMS#20160503-00085 
  REVISIONS   :
  VERSION 	DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     16-Apr-2016        Pramod   		 TMS#20160503-00085   Performance Tuning
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_INV_LISTING_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_INV_LISTING_V (PROCESS_ID, LOCATION, BRANCH_NUMBER, SEGMENT_LOCATION, LOCATION_STATE, PART_NUMBER, MFG_NUMBER, DESCRIPTION, CONSIGNED, CAT_CLASS, CAT_CLASS_DESCRIPTION, UOM, SELLING_PRICE, AVERAGECOST, WEIGHT, ONHAND, FINAL_END_QTY, VENDOR_NAME, SHELF_LIFE, CONTAINER_TYPE, UN_NUMBER, SALE_UNITS, SUBINVENTORY_CODE, BIN0, BIN1, BIN2, BIN3, TONHAND, ON_ORDER, ONHAND_TYPE, ORGANIZATION_ID, INVENTORY_ITEM_ID) AS 
  SELECT    /*+ ORDERED INDEX (MOQ, EIS_XXWC_SUBINV_ONHAND_N1) INDEX (FVCC EIS_XXWC_FLEX_VAL_TAB_N1) USE_NL (MOQ MOQT) USE_NL (MOQ MSI) USE_NL (MOQ tab) USE_NL (MSI GCC) */
    --  MOQ.ONHAND,
    MOQ.PROCESS_ID,
    SUBSTR(GCC.SEGMENT2,3) LOCATION,
    MP.ORGANIZATION_CODE BRANCH_NUMBER,
    GCC.SEGMENT2 SEGMENT_LOCATION,
    (SELECT LOC.REGION_2
    FROM HR_ORGANIZATION_UNITS HOU,
      HR_LOCATIONS_ALL LOC
    WHERE MP.ORGANIZATION_ID = HOU.ORGANIZATION_ID
    AND HOU.LOCATION_ID      = LOC.LOCATION_ID(+)
    ) LOCATION_STATE,
--      HOU.REGION_2 LOCATION_STATE,
    --  hLA.region_2 location_state,
    MSI.SEGMENT1 PART_NUMBER,
    xxeis.eis_xxwc_inv_list_rpt_pkg.get_lhqty(msi.inventory_item_id) mfg_number,
    --   cr.cross_reference mfg_number,
    --XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.Get_Item_Crossref(msi.inventory_item_id,msi.organization_id) mfg_number,
    MSI.DESCRIPTION,
    DECODE(moq.is_consigned,1,'Y',2,'N') CONSIGNED,
    tab.inv_cat_seg1
    ||'.'
    ||tab.cat cat_class,
    --xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class(msi.inventory_item_id,msi.organization_id) cat_class,
    fvcc.description CAT_CLASS_DESCRIPTION,
    --       xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class_desc(msi.inventory_item_id,msi.organization_id) CAT_CLASS_DESCRIPTION,
    msi.primary_uom_code uom,
    msi.list_price_per_unit selling_price,
    tab.aver_cost averagecost,
    --NVL(apps.cst_cost_api.get_item_cost(1,msi.inventory_item_id,msi.organization_id),0) averagecost,
    msi.unit_weight weight,
    NVL(MOQ.ONHAND,0) ONHAND,
    NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_FINAL_END_QTY(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID,xxeis.EIS_XXWC_INV_LIST_RPT_PKG.get_end_date),0) FINAL_END_QTY,
    -- **NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_FINAL_END_QTY(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID,GPS.END_DATE),0) FINAL_END_QTY,
    tab.vendor_name VENDOR_NAME,
    --XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_VENDOR_NAME(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID) VENDOR_NAME,
    MSI.SHELF_LIFE_DAYS SHELF_LIFE,
    MSI.CONTAINER_TYPE_CODE CONTAINER_TYPE,
    PUN.UN_NUMBER UN_NUMBER,
    --XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_MTD_SALES(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID,GPS.START_DATE,GPS.END_DATE) SALE_UNITS,
    TAB.ONE_STORE_SALE SALE_UNITS,
    moq.subinventory_code subinventory_code,
    (SELECT mil.Segment1
    FROM MTL_ITEM_LOCATIONS_KFV MIL,
      ------ diane added bin0 2/7/2014  --------
      MTL_SECONDARY_LOCATORS MSL
    WHERE MSL.INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
    AND MSL.ORGANIZATION_ID     = MSI.ORGANIZATION_ID
    AND MSL.SECONDARY_LOCATOR   = MIL.INVENTORY_LOCATION_ID
    AND mil.segment1 LIKE '0%'
    AND rownum = 1
    ) BIN0,
    (SELECT mil.Segment1
    FROM MTL_ITEM_LOCATIONS_KFV MIL,
      MTL_SECONDARY_LOCATORS MSL
    WHERE MSL.INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
    AND MSL.ORGANIZATION_ID     = MSI.ORGANIZATION_ID
    AND MSL.SECONDARY_LOCATOR   = MIL.INVENTORY_LOCATION_ID
    AND mil.segment1 LIKE '1%'
    AND rownum = 1
    ) BIN1,
    (SELECT mil.Segment1
    FROM MTL_ITEM_LOCATIONS_KFV MIL,
      MTL_SECONDARY_LOCATORS MSL
    WHERE MSL.INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
    AND MSL.ORGANIZATION_ID     = MSI.ORGANIZATION_ID
    AND MSL.SECONDARY_LOCATOR   = MIL.INVENTORY_LOCATION_ID
    AND mil.segment1 LIKE '2%'
    AND rownum = 1
    ) BIN2,
    (SELECT mil.Segment1
    FROM MTL_ITEM_LOCATIONS_KFV MIL,
      MTL_SECONDARY_LOCATORS MSL
    WHERE MSL.INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
    AND MSL.ORGANIZATION_ID     = MSI.ORGANIZATION_ID
    AND MSL.SECONDARY_LOCATOR   = MIL.INVENTORY_LOCATION_ID
    AND mil.segment1 LIKE '3%'
    AND rownum = 1
    ) BIN3,
    NVL(moqt.onhand,0) tonhand,
    tab.on_ord on_order,
    CASE
      WHEN NVL(moqt.onhand,0) > 0
      THEN 'On Hand'
      WHEN NVL(moqt.onhand,0) = 0
      THEN 'No on Hand'
      WHEN NVL(moqt.onhand,0) < 0
      THEN 'Negative on hand'
    END Onhand_Type,
    MSI.ORGANIZATION_ID,
    msi.inventory_item_id
  FROM XXEIS.EIS_XXWC_SUBINV_ONHAND_DTLS MOQ,
    XXEIS.EIS_XXWC_ONHAND_DTLS MOQT,
    MTL_SYSTEM_ITEMS_KFV MSI,
    XXEIS.EIS_XXWC_PO_ISR_TAB TAB,
    XXEIS.EIS_XXWC_FLEX_VAL_TAB FVCC,
    MTL_PARAMETERS MP,
--    hr_organization_units_v hou,
    GL_CODE_COMBINATIONS_KFV GCC,
    PO_UN_NUMBERS PUN
  WHERE MSI.ORGANIZATION_ID   = MOQ.ORGANIZATION_ID
  AND MSI.INVENTORY_ITEM_ID   = MOQ.INVENTORY_ITEM_ID
  AND MOQ.PROCESS_ID 		  = XXEIS.EIS_XXWC_INV_LIST_RPT_PKG.get_process_id
  AND MOQ.ORGANIZATION_ID     = MOQT.ORGANIZATION_ID
  AND MOQ.INVENTORY_ITEM_ID   = MOQT.INVENTORY_ITEM_ID
  AND MOQT.PROCESS_ID 		  = XXEIS.EIS_XXWC_INV_LIST_RPT_PKG.get_process_id
  AND MOQ.ORGANIZATION_ID     = TAB.ORGANIZATION_ID
  AND MOQ.INVENTORY_ITEM_ID   = TAB.INVENTORY_ITEM_ID
  AND TAB.CAT                 = FVCC.FLEX_VALUE
  AND FVCC.PROCESS_ID 		  = XXEIS.EIS_XXWC_INV_LIST_RPT_PKG.get_process_id
  AND MOQ.ORGANIZATION_ID     = MP.ORGANIZATION_ID
  AND GCC.CODE_COMBINATION_ID = MSI.COST_OF_SALES_ACCOUNT
  AND MSI.UN_NUMBER_ID        = PUN.UN_NUMBER_ID (+)
/
