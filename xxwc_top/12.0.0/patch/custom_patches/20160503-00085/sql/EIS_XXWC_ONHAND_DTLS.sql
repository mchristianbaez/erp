--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_ONHAND_DTLS
  Description: This table is used to get data from XXEIS.EIS_XXWC_INV_LIST_RPT_PKG Package.
  HISTORY
  ===============================================================================
  VERSION 		DATE            AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     	16-Apr-2016        Pramod   		TMS#20160503-00085  Performance Tuning
********************************************************************************/
CREATE TABLE XXEIS.EIS_XXWC_ONHAND_DTLS 
   (	PROCESS_ID NUMBER, 
	ONHAND NUMBER, 
	INVENTORY_ITEM_ID NUMBER, 
	ORGANIZATION_ID NUMBER
)
/
