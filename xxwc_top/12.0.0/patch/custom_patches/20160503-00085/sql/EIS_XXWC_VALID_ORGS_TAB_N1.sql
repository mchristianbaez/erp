-----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header EIS_XXWC_VALID_ORGS_TAB_N1
  File Name: EIS_XXWC_VALID_ORGS_TAB_N1.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        16-Apr-2016  Pramod        TMS#20160503-00085 Inventory Listing Report - Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX XXEIS.EIS_XXWC_VALID_ORGS_TAB_N1 ON XXEIS.EIS_XXWC_VALID_ORGS_TAB (ORGANIZATION_ID,PROCESS_ID) TABLESPACE APPS_TS_TX_DATA
/
