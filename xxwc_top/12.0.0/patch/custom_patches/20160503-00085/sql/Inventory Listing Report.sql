--Report Name            : Inventory Listing Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Inventory Listing Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_LISTING_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_LISTING_V',401,'','','','','PK059658','XXEIS','Eis Rs Xxwc Inv Listing V','EXILV','','');
--Delete View Columns for EIS_XXWC_INV_LISTING_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_LISTING_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_LISTING_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','SHELF_LIFE',401,'Shelf Life','SHELF_LIFE','','','','PK059658','NUMBER','','','Shelf Life','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','PK059658','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','ONHAND',401,'Onhand','ONHAND','','','','PK059658','NUMBER','','','Onhand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','SELLING_PRICE',401,'Selling Price','SELLING_PRICE','','','','PK059658','NUMBER','','','Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','UOM',401,'Uom','UOM','','','','PK059658','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','CAT_CLASS',401,'Cat Class','CAT_CLASS','','','','PK059658','VARCHAR2','','','Cat Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','PK059658','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','PART_NUMBER',401,'Part Number','PART_NUMBER','','','','PK059658','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','SEGMENT_LOCATION',401,'Segment Location','SEGMENT_LOCATION','','','','PK059658','VARCHAR2','','','Segment Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','SALE_UNITS',401,'Sale Units','SALE_UNITS','','','','PK059658','NUMBER','','','Sale Units','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','BRANCH_NUMBER',401,'Branch Number','BRANCH_NUMBER','','','','PK059658','VARCHAR2','','','Branch Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','CAT_CLASS_DESCRIPTION',401,'Cat Class Description','CAT_CLASS_DESCRIPTION','','','','PK059658','VARCHAR2','','','Cat Class Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','CONSIGNED',401,'Consigned','CONSIGNED','','','','PK059658','VARCHAR2','','','Consigned','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','AVERAGECOST',401,'Averagecost','AVERAGECOST','','~T~D~2','','PK059658','NUMBER','','','Averagecost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','SUBINVENTORY_CODE',401,'Subinventory Code','SUBINVENTORY_CODE','','','','PK059658','VARCHAR2','','','Subinventory Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','BIN1',401,'Bin1','BIN1','','','','PK059658','VARCHAR2','','','Bin1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','BIN2',401,'Bin2','BIN2','','','','PK059658','VARCHAR2','','','Bin2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','BIN3',401,'Bin3','BIN3','','','','PK059658','VARCHAR2','','','Bin3','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','BIN0',401,'Bin0','BIN0','','','','PK059658','VARCHAR2','','','Bin0','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','FINAL_END_QTY',401,'Final End Qty','FINAL_END_QTY','','','','PK059658','NUMBER','','','Final End Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','CONTAINER_TYPE',401,'Container Type','CONTAINER_TYPE','','','','PK059658','VARCHAR2','','','Container Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','LOCATION',401,'Location','LOCATION','','','','PK059658','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','UN_NUMBER',401,'Un Number','UN_NUMBER','','','','PK059658','VARCHAR2','','','Un Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','WEIGHT',401,'Weight','WEIGHT','','','','PK059658','NUMBER','','','Weight','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','MFG_NUMBER',401,'Mfg Number','MFG_NUMBER','','','','PK059658','NUMBER','','','Mfg Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','PK059658','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','ONHAND_TYPE',401,'Onhand Type','ONHAND_TYPE','','','','PK059658','VARCHAR2','','','Onhand Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','ON_ORDER',401,'On Order','ON_ORDER','','','','PK059658','NUMBER','','','On Order','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','ORGANIZATION_ID',401,'Organization Id','ORGANIZATION_ID','','','','PK059658','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','TONHAND',401,'Tonhand','TONHAND','','','','PK059658','NUMBER','','','Tonhand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','LOCATION_STATE',401,'Location State','LOCATION_STATE','','','','PK059658','VARCHAR2','','','Location State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','PROCESS_ID',401,'Process Id','PROCESS_ID','','','','PK059658','NUMBER','','','Process Id','','','');
--Inserting View Components for EIS_XXWC_INV_LISTING_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_LISTING_V','GL_CODE_COMBINATIONS_KFV',401,'GL_CODE_COMBINATIONS','GCC','GCC','PK059658','PK059658','-1','Account Combinations','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_LISTING_V','GL_PERIOD_STATUSES',401,'GL_PERIOD_STATUSES','GPS','GPS','PK059658','PK059658','-1','Calendar Period Statuses','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_LISTING_V','MTL_CATEGORIES',401,'MTL_CATEGORIES_B','MCV','MCV','PK059658','PK059658','-1','Categories','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_LISTING_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSI','MSI','PK059658','PK059658','-1','Items','','','','');
--Inserting View Component Joins for EIS_XXWC_INV_LISTING_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','GL_CODE_COMBINATIONS_KFV','GCC',401,'EXILV.CODE_COMBINATION_ID','=','GCC.CODE_COMBINATION_ID(+)','','','','Y','PK059658','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','GL_PERIOD_STATUSES','GPS',401,'EXILV.APPLICATION_ID','=','GPS.APPLICATION_ID(+)','','','','','PK059658','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','GL_PERIOD_STATUSES','GPS',401,'EXILV.SET_OF_BOOKS_ID','=','GPS.SET_OF_BOOKS_ID(+)','','','','','PK059658','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','GL_PERIOD_STATUSES','GPS',401,'EXILV.FISCAL_MONTH','=','GPS.PERIOD_NAME(+)','','','','','PK059658','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','MTL_CATEGORIES','MCV',401,'EXILV.CATEGORY_ID','=','MCV.CATEGORY_ID(+)','','','','','PK059658','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXILV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','PK059658','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXILV.INV_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','PK059658','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Inventory Listing Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Inventory Listing Report
xxeis.eis_rs_ins.lov( 401,'select SECONDARY_INVENTORY_NAME SUB_INVENTORY,HAOU.NAME ORGANIZATION_NAME
from MTL_SECONDARY_INVENTORIES MSI,
     HR_ALL_ORGANIZATION_UNITS HAOU
WHERE 1=1
AND MSI.ORGANIZATION_ID = HAOU.organization_id','','EIS_INV_SUBINVENTORY_LOV','List of All SubInventories.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT ''Negative Onhand Items'' onhand_type FROM dual
UNION
SELECT ''Positive Onhand Items'' onhand_type FROM dual
UNION
SELECT ''ALL Items'' onhand_type FROM dual','','INV ONHAND TYPE','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT ORGANIZATION_CODE WAREHOUSE,
  ORGANIZATION_NAME
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1)
AND EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
UNION
SELECT ''All'', ''All Organizations'' FROM Dual','','XXWC INV ORGANIZATIONS LOV1','List of All Inventory Orgs under a given operating unit.','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'','On Hand,No on Hand,ALL,Negative on hand','ON_HAND_TYPE_CSV','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Inventory Listing Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Inventory Listing Report
xxeis.eis_rs_utility.delete_report_rows( 'Inventory Listing Report' );
--Inserting Report - Inventory Listing Report
xxeis.eis_rs_ins.r( 401,'Inventory Listing Report','','Provide a listing of inventory related information ( Negative On Hand , HAZMAT list, Cat/Class list, Extended Description, Inventory Explosion, etc.)
Selection - Location, Prod, Ven, Cat/Class
Columns - stock level, negative avail, on hand, on order, turns, cat/class, descrip, extended description (Negative On Hand, HAZMAT list, Cat/Class list, Inventory Explosion  etc)
','','','','PK059658','EIS_XXWC_INV_LISTING_V','Y','','','PK059658','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Inventory Listing Report
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'BRANCH_NUMBER','Location','Branch Number','','','default','','1','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'SUBINVENTORY_CODE','Subinventory Code','Subinventory Code','','','default','','13','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'BIN1','Bin1','Bin1','','','default','','20','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'BIN2','Bin2','Bin2','','','default','','21','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'BIN3','Bin3','Bin3','','','default','','22','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'BIN0','Bin0','Bin0','','','','','19','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'CAT_CLASS','CatClass','Cat Class','','','default','','7','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'DESCRIPTION','Description','Description','','','default','','5','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'ONHAND','Qty on Hand','Onhand','','~~~','default','','14','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'PART_NUMBER','Part','Part Number','','','default','','4','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'SEGMENT_LOCATION','Segment Location','Segment Location','','','default','','2','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'SELLING_PRICE','Selling Price','Selling Price','','~T~D~2','default','','16','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'SHELF_LIFE','Shelf Life','Shelf Life','','~~~','default','','11','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'UOM','UOM','Uom','','','default','','12','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','10','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'LOCATION_STATE','Location State','Location State','','','default','','3','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'SALE_UNITS','Sale Units','Sale Units','','~~~','default','','15','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'CAT_CLASS_DESCRIPTION','Cat Class Description','Cat Class Description','','','default','','8','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'CONSIGNED','Consigned','Consigned','','','default','','6','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'FINAL END VALUE','Final End Value','Consigned','NUMBER','~T~D~2','default','','18','Y','','','','','','','(EXILV.AVERAGECOST*EXILV.ONHAND)','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'AVERAGECOST','Averagecost','Averagecost','','~T~D~2','default','','17','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'MFG_NUMBER','Mfg Number','Mfg Number','','','','','9','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'ON_ORDER','On Order','On Order','','','','','23','N','','','','','','','','PK059658','N','N','','EIS_XXWC_INV_LISTING_V','','');
--Inserting Report Parameters - Inventory Listing Report
xxeis.eis_rs_ins.rp( 'Inventory Listing Report',401,'Organization','Organization','BRANCH_NUMBER','IN','XXWC INV ORGANIZATIONS LOV1','','VARCHAR2','Y','Y','1','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Listing Report',401,'Include All Items with a Positive On Hand','Include All Items with a Positive On Hand','','IN','INV ONHAND TYPE','''ALL Items''','VARCHAR2','N','N','4','','N','CONSTANT','PK059658','N','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Listing Report',401,'Subinventory','Subinventory','SUBINVENTORY_CODE','IN','EIS_INV_SUBINVENTORY_LOV','','VARCHAR2','N','Y','3','','Y','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Listing Report',401,'Quantity on Hand','Quantity on Hand','','IN','ON_HAND_TYPE_CSV','','VARCHAR2','Y','Y','2','','N','CONSTANT','PK059658','Y','N','','','');
--Inserting Report Conditions - Inventory Listing Report
xxeis.eis_rs_ins.rcn( 'Inventory Listing Report',401,'','','','','and PROCESS_ID= :SYSTEM.PROCESS_ID','Y','1','','PK059658');
--Inserting Report Sorts - Inventory Listing Report
xxeis.eis_rs_ins.rs( 'Inventory Listing Report',401,'PART_NUMBER','ASC','PK059658','','');
--Inserting Report Triggers - Inventory Listing Report
xxeis.eis_rs_ins.rt( 'Inventory Listing Report',401,'begin
xxeis.eis_xxwc_inv_list_rpt_pkg.g_process_id := :SYSTEM.PROCESS_ID;
xxeis.eis_xxwc_inv_list_rpt_pkg.populate_Inv_List_Details(P_process_id    => :SYSTEM.PROCESS_ID,
                                    p_organization  => :Organization,
                                    p_qty_on_hand           => :Quantity on Hand,
                                    p_subinv  => :Subinventory);
end;','B','Y','PK059658');
xxeis.eis_rs_ins.rt( 'Inventory Listing Report',401,'begin
xxeis.eis_xxwc_inv_list_rpt_pkg.CLEAR_TEMP_TABLES(:SYSTEM.PROCESS_ID);
end;','A','Y','PK059658');
--Inserting Report Templates - Inventory Listing Report
--Inserting Report Portals - Inventory Listing Report
xxeis.eis_rs_ins.r_port( 'Inventory Listing Report','XXWC_PUR_TOP_RPTS','401','Inventory Listing','Negative On-Hand','OA.jsp?page=/eis/oracle/apps/xxeis/reporting/webui/EISLaunchPG&EisProduct=Reporting&Portal=Yes&mod=Inventory','','Pivot Excel,EXCEL,','CONC','N','MR020532');
--Inserting Report Dashboards - Inventory Listing Report
--Inserting Report Security - Inventory Listing Report
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50865',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50862',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50864',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50849',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','660','','50871',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50851',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50619',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50882',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50883',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50981',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50855',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50884',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','660','','50857',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50895',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','222','','51208',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','222','','50894',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50924',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','51052',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50879',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50852',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50821',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','20005','','50880',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','51029',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','660','','50886',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50867',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50868',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','20005','','50900',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','201','','50892',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50846',401,'MR020532','','');
--Inserting Report Pivots - Inventory Listing Report
xxeis.eis_rs_ins.rpivot( 'Inventory Listing Report',401,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Inventory Listing Report',401,'Pivot','CAT_CLASS','PAGE_FIELD','','','4','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Inventory Listing Report',401,'Pivot','DESCRIPTION','PAGE_FIELD','','','3','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Inventory Listing Report',401,'Pivot','ONHAND','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Inventory Listing Report',401,'Pivot','PART_NUMBER','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Inventory Listing Report',401,'Pivot','SEGMENT_LOCATION','PAGE_FIELD','','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Inventory Listing Report',401,'Pivot','VENDOR_NAME','PAGE_FIELD','','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Inventory Listing Report',401,'Pivot','BRANCH_NUMBER','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Inventory Listing Report',401,'Pivot','SUBINVENTORY_CODE','ROW_FIELD','','','1','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
