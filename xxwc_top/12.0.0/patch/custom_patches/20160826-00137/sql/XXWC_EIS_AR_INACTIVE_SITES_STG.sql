---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.XXWC_EIS_AR_INACTIVE_SITES_STG $
  Description	  : This table is used to get data from XXEIS.EIS_RS_XXWC_AR_UTIL_PKG Package.
  REVISIONS   :
  VERSION 		  DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.1     		24-Oct-2016       		Siva   		 TMS#20160826-00137-- changed the normal table into Global Temporary Table
**************************************************************************************************************/
DROP TABLE XXEIS.XXWC_EIS_AR_INACTIVE_SITES_STG;
CREATE GLOBAL TEMPORARY TABLE XXEIS.XXWC_EIS_AR_INACTIVE_SITES_STG
  (
    ACCOUNT_NUMBER       VARCHAR2(30 BYTE),
    ACCOUNT_NAME         VARCHAR2(240 BYTE),
    LOCATION_NUMBER      VARCHAR2(30 BYTE),
    LOCATION             VARCHAR2(40 BYTE),
    COLLECTOR            VARCHAR2(30 BYTE),
    PROFILE_CLASS        VARCHAR2(30 BYTE),
    SITE_CLASSIFICATION  VARCHAR2(150 BYTE),
    LAST_SALE_DATE       DATE,
    LAST_PAYMENT_DATE    DATE,
    ORDER_DATE           DATE,
    AMOUNT_DUE_REMAINING NUMBER
  )
ON COMMIT PRESERVE ROWS
/
