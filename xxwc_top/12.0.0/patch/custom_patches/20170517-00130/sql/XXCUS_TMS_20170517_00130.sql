/*
 TMS:  20170517-00130
 Date: 06/06/2017
 Notes:  Update target node for each rebate jobs to ebsprd6
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 v_db varchar2(30) :=Null;
 --
 cursor c_programs is
  select 
                 a.user_concurrent_program_name cp_name
                ,a.concurrent_program_id cp_id
                ,a.application_id app_id
                ,case
                   when c.db_name ='ebsdev' then 'ebsdev'
                   when c.db_name ='ebsqa' then 'ebsqa2'                   
                   when c.db_name ='ebssbx' then 'ebssbx'
                   when c.db_name ='ebsprd' then 'ebsprd6'
                   else null
                 end target_node
    from   apps.fnd_concurrent_programs_vl a
                  ,apps.fnd_conc_prog_onsite_info b
                  ,(select lower(name) db_name from v$database) c
    where 1  =1
           and a.application_id =b.program_application_id
           and a.concurrent_program_id =b.concurrent_program_id
           and b.connstr1 is not null
           and a.concurrent_program_id in
                    (
                         40849
                        ,85403
                        ,84401
                        ,55330
                        ,55331
                        ,170409
                        ,164411
                        ,163409
                        ,186409
                        ,167409
                        ,55332
                        ,55337
                        ,169409
                        ,147410
                        ,55334
                        ,119412
                        ,119411
                        ,55336
                        ,119409
                        ,119410
                        ,44836
                        ,44848        
                    )
    order by 1 asc, 2  
    ;    
 --
BEGIN --Main Processing...
   --
   n_loc :=101;
   --
   for rec in c_programs loop
               --
               begin
                --
                savepoint start1;
                --
                update apps.fnd_conc_prog_onsite_info
                set connstr1 =rec.target_node
                where 1 =1
                     and concurrent_program_id =rec.cp_id
                 ; 
                --
                dbms_output.put_line('Set up DB node affinity : Table fnd_conc_prog_onsite_info updated with target node '||rec.target_node||' for program name :'||rec.cp_name||', rows updated = '||sql%rowcount);
                --
                n_loc :=102;
                --
               exception
                when others then
                 --
                 n_loc :=103;
                 --
                 dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
                 --
                 rollback to start1;
                 --     
               end;
               --
   end loop;
   --
   commit;
   --
EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS:  20170517-00130, Errors =' || SQLERRM);
END;
/