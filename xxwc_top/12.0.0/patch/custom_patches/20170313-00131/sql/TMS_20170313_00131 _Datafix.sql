/***********************************************************************************************************************************************
   NAME:       TMS_20170309-00055_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        03/14/2017   P.Vamshidhar     TMS#20170313-00131 
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Insert');

INSERT INTO XXCUS.XXCUS_MONITOR_CP_B (CONCURRENT_PROGRAM_ID,
                                      EMAIL_WHEN_WARNING,
                                      EMAIL_WHEN_ERROR,
                                      USER_ID,
                                      CREATED_BY,
                                      CREATION_DATE,
                                      LAST_UPDATED_BY,
                                      LAST_UPDATE_DATE,
                                      WARNING_EMAIL_ADDRESS,
                                      ERROR_EMAIL_ADDRESS,
                                      OWNED_BY,
                                      APPLICATION_ID)
     VALUES (66442,
             'Y',
             'Y',
             15985,
             33710,
             SYSDATE,
             33710,
             SYSDATE,
             'WC-ITBSAAlerts-U1@HDSupply.com',
             'WC-ITBSAAlerts-U1@HDSupply.com',
             'SUPPLY CHAIN',
             20005);

   DBMS_OUTPUT.put_line ('Records Inserted -' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to insert record ' || SQLERRM);
	  ROLLBACK;
END;
/