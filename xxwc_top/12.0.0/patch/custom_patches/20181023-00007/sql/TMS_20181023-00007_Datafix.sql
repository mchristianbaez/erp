/***********************************************************************************************************************************************
   NAME:     TMS_20181023-00007_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        10/23/2018  Rakesh Patel     TMS#20181023-00007-Credit Limit Data Script - Jobs Only- Data fix script
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

DECLARE
  l_count              BINARY_INTEGER := 0;
  l_err_count          BINARY_INTEGER := 0;
  l_backupsuffix       VARCHAR2(10)   := TO_CHAR(SYSDATE,'ddhh24miss');
  l_cust_prof_amt_rec  hz_customer_profile_v2pub.cust_profile_amt_rec_type;
  l_version_num        NUMBER;
  l_return_status      VARCHAR(10);
  l_msg_count          NUMBER;    
  l_msg_data           VARCHAR(4000);
  
     CURSOR c_cust_sites 
	                    ( p_account_number    IN VARCHAR2
						 ,p_party_site_number IN VARCHAR2
						 ,p_cust_acct_site_id IN NUMBER
						 )
         IS
       SELECT 		 
            hca.account_number,
             hca.cust_account_id,
             hca.account_name,
             hps.party_site_number,
             hcsa.cust_acct_site_id,
             hcsa.party_site_id,
             hcsu.site_use_id,
             hcsu.site_use_code,
             hcpa.cust_acct_profile_amt_id,
             hcpa.created_by_module,
             hcpa.object_version_number
       FROM apps.hz_cust_accounts_all hca,
            apps.hz_party_sites hps,
            apps.hz_cust_acct_sites_All hcsa,
			apps.hz_cust_site_uses_all hcsu,
			apps.hz_cust_profile_amts hcpa
      WHERE  1 = 1
         AND hps.party_id           = hca.party_id
         AND hca.cust_account_id    = hcsa.cust_account_id
         AND hps.party_site_id      = hcsa.party_site_id --party site id
		 AND hcsu.cust_acct_site_id = hcsa.cust_acct_site_id
         AND hca.account_number     = p_account_number
         AND hps.party_site_number  = p_party_site_number  --party site number
         AND hcsa.cust_acct_site_id = p_cust_acct_site_id 
		 AND hcpa.cust_account_id   = hcsa.cust_account_id  --profile amt cust account
		 AND hcpa.site_use_id       = hcsu.site_use_id --profile amt cust account site
         AND hcsa.status            = 'A'
		 AND hcsu.status            = 'A'
		 AND NVL(hcpa.overall_credit_limit, 0) <> 15000;
BEGIN

  dbms_output.put_line( 'Begin Fix TMS_20181023-00007' );
  
  --Apps Initialization
  FND_GLOBAL.APPS_INITIALIZE(USER_ID      => 1290, --XXWC_INT_FINANCE
                             RESP_ID      => 51208,
                             RESP_APPL_ID => 222);
  MO_GLOBAL.INIT('AR');
  MO_GLOBAL.SET_POLICY_CONTEXT('S', 162);
  
  dbms_output.put( 'Begin Backup ... ' );
  EXECUTE IMMEDIATE 'CREATE TABLE xxwc.hz_cust_profile_amts' ||l_backupsuffix || ' AS SELECT * FROM AR.hz_cust_profile_amts WHERE cust_account_id IN (SELECT hca.cust_account_id 
                                                                                                                                                        FROM apps.hz_cust_accounts_all hca, 
                                                                                                                                                             xxwc.xxwc_cust_crlimit_upd_ext_tbl ext
                                                                                                                                                       WHERE hca.account_number = ext.oracle_account_number
                                                                                                                                                        )';
  EXECUTE IMMEDIATE 'GRANT ALL ON xxwc.hz_cust_profile_amts' ||l_backupsuffix || ' TO PUBLIC';
  dbms_output.put_line('Backup hz_cust_profile_amts Table - Done, xxwc.hz_cust_profile_amts'||l_backupsuffix);
  
  FOR cust_crlimit_upd_rec IN (SELECT oracle_account_number, oracle_site_number, cust_acct_site_id, overall_credit_limit
                                 FROM xxwc.xxwc_cust_crlimit_upd_ext_tbl
                                WHERE 1=1
                                  --AND CUST_ACCT_SITE_ID = 1764263
                                 ) LOOP
     FOR c_cust_sites_rec IN c_cust_sites (  p_account_number    => cust_crlimit_upd_rec.oracle_account_number
                                            ,p_party_site_number => cust_crlimit_upd_rec.oracle_site_number  
                                            ,p_cust_acct_site_id => cust_crlimit_upd_rec.cust_acct_site_id ) LOOP
        l_cust_prof_amt_rec.cust_acct_profile_amt_id := c_cust_sites_rec.cust_acct_profile_amt_id; 
        l_cust_prof_amt_rec.overall_credit_limit     := 15000; 
        l_version_num                                := c_cust_sites_rec.object_version_number; 
      
        hz_customer_profile_v2pub.update_cust_profile_amt ('T',l_cust_prof_amt_rec,l_version_num,l_return_status,l_msg_count,l_msg_data);
        
        IF l_return_status = 'S' THEN
           l_count := l_count+1;
        ELSE
            l_err_count := l_err_count+1;
            IF NVL(l_msg_count, 0) > 1 THEN
              FOR i IN 1 .. l_msg_count LOOP
                 dbms_output.put_line(' Error Status ' || l_return_status);
                 dbms_output.put_line(' Error message ' || l_msg_data);
              END LOOP;
            ELSE
               dbms_output.put_line(' Error message ' || l_msg_data);
            END IF;
        END IF; 
     END LOOP;      
  END LOOP;
  

  dbms_output.put_line('Records Updated:   ' || l_count);
  
  dbms_output.put_line('Records Error count :   ' || l_err_count);

  COMMIT;
  -- ROLLBACK;

  dbms_output.put_line( 'End Fix 20181023-00007' );
  
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('Unable to update records ' || SQLCODE || ': ' ||SQLERRM);
    ROLLBACK;
END;
/
