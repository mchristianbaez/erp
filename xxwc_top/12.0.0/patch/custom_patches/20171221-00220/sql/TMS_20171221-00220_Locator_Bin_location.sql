/*************************************************************************  
$Header TMS_20171221-00220_Locator_Bin_location.sql $
Module Name: 20171221-00220  locator Bin Location

PURPOSE: Locator Bin Location
REVISIONS:
Ver        Date         Author                Description
---------  -----------  ------------------    --------------------------
1.0        29-JAN-2018  Sundaramoorthy        TMS#20171221-00220 
**************************************************************************/

SET serveroutput ON SIZE 1000000
DECLARE
l_return_status VARCHAR2(10);
l_msg_count NUMBER;
L_MSG_DATA varchar2(240);
L_LOCATOR_ID NUMBER := 1815884;
l_org_id NUMBER := 367;
BEGIN
  inv_loc_wms_pub.delete_locator(
  x_return_status => l_return_status
, x_msg_count => l_msg_count
, X_MSG_DATA => L_MSG_DATA
, p_inventory_location_id => l_locator_id
, P_CONCATENATED_SEGMENTS => null
, p_organization_id => l_org_id
, p_organization_code => NULL
, p_validation_req_flag => 'N'
);

IF l_return_status = 'S' THEN
dbms_output.put_line('Failure ');
dbms_output.put_line('Reason ' || l_msg_data);
ELSE
dbms_output.put_line('Success ');
END IF;
EXCEPTION
WHEN OTHERS THEN
dbms_output.put_line('Error executing script '||SQLERRM);
END;
/
COMMIT;