CREATE OR REPLACE PACKAGE BODY XXWC_SMARTSHEET_EXTRACT_PKG 
AS
/*************************************************************************
     $Header XXWC_SMARTSHEET_EXTRACT_PKG  $
     Module Name: XXWC_SMARTSHEET_EXTRACT_PKG.pkb

     PURPOSE:   Mobile App    
                       
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        24/07/2018  Niraj K Ranjan          TMS#20180725-00025   SmartSheets Extract               
**************************************************************************/
   --Email Defaults
   g_dflt_email   fnd_user.email_address%TYPE := 'WC-ITDEVALERTS-U1@HDSupply.com';
   g_pkg_name     VARCHAR2 (50) := 'XXWC_SMARTSHEET_EXTRACT_PKG'; 
   
   /*************************************************************************
      Function Name: get_smartsheet_info

      PURPOSE:   This function will be called by mobile app to get smartsheet information

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        07/26/2017    Niraj K Ranjan     TMS#20180725-00025   SmartSheets Extract    
   ****************************************************************************/
    FUNCTION get_smartsheet_info 
	 return sys_refcursor IS
       v_ss_rc sys_refcursor;
       l_proc_name          VARCHAR2(100):= g_pkg_name||'.'||'GET_SMARTSHEET_INFO';
       l_user_id            NUMBER := FND_PROFILE.VALUE('USER_ID');
	   l_org_id             NUMBER := 163;
    BEGIN
       mo_global.set_policy_context('S',l_org_id);
       OPEN v_ss_rc FOR 'SELECT  project_code
              , project_description
              , SUM(invoice_amount) INVOICE_AMOUNT
              --, SUM(DECODE(inv_payment_status, ''Paid in Full'',invoice_amount,0)) Paid_In_Full_Amount
              --, SUM(DECODE(inv_payment_status, ''Pending Payment'',invoice_amount,0)) Pending_Payment_Amount
          FROM ( select DISTINCT i.invoice_num,
                                 i.invoice_amount
                                ,case i.payment_status_flag
                                     when ''P'' then ''Pending Payment'' --Partial Payment
                                     when ''N'' then ''Pending Payment''
                                     when ''Y'' then ''Paid in Full''
                                     else null
                                end inv_payment_status
                               ,cc.segment5 project_code
              ,(
                  select description
                  from apps.fnd_flex_values_vl
                  where     1 = 1
                       and flex_value_set_id = 1014551
                       and flex_value = cc.segment5) project_description
                  from apps.ap_invoices_all i,
                        apps.ap_supplier_sites_all vs,
                        apps.ap_invoice_distributions_all pd,
                        gl.gl_code_combinations cc,
                        apps.ap_invoice_lines_all apil,
                        xxcus.xxcus_location_code_tbl loc
                  where 1 =1
                        and i.org_id = :p_org_id
                        and i.invoice_id = pd.invoice_id
                        and i.invoice_id = apil.invoice_id(+)
                        and apil.line_number = pd.invoice_line_number(+)
                        and pd.dist_code_combination_id = cc.code_combination_id(+)
                        and i.vendor_site_id = vs.vendor_site_id(+)
                        and vs.pay_group_lookup_code != ''INVENTORY''
                        and loc.business_unit = ''WC1US''
                        and loc.entrp_loc = cc.segment2
                        and trunc(i.invoice_date) >= trunc(add_months(sysdate,-24))
                        and cc.segment5 <>''00000''
                        and apil.line_type_lookup_code <>''TAX''
                  ) GROUP BY project_code, project_description
                ORDER BY project_code'
                USING l_org_id;
        RETURN v_ss_rc; 
       
    EXCEPTION
       WHEN OTHERS THEN
          RETURN v_ss_rc; 
    END get_smartsheet_info;

END XXWC_SMARTSHEET_EXTRACT_PKG;
/
