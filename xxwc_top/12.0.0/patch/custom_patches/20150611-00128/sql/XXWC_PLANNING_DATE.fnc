/* Formatted on 5/4/2016 4:28:47 PM (QP5 v5.265.14096.38000) */
CREATE OR REPLACE FUNCTION APPS.XXWC_PLANNING_DATE/*******************************************************************************************
                                                  *   $Header XXWC_PLANNING_DATE $
                                                  *   Module Name: XXWC_PLANNING_DATE
                                                  *
                                                  *   PURPOSE:   PLT Need by date personalization
                                                  *
                                                  *   REVISIONS:
                                                  *   Ver        Date        Author                     Description
                                                  *   ---------  ----------  ---------------         -------------------------
                                                  *    1.0        10/16/2014  Lee Spitger             Initial Version
                                                       1.1        04/06/2016  P.Vamshidhar/Hari       TMS # 20150611-00128 -- PO - Fix issue with Need by date personalization.
                                                  *
                                                  * *****************************************************************************************/
(P_PO_HEADER_ID IN NUMBER)           ---Added by hari for TMS # 20150611-00128
   ---Commented below by hari for TMS # 20150611-00128
   /*p_number_of_days   IN NUMBER,
   p_date             IN DATE,
   p_calendar_code    IN VARCHAR2,
   p_exception_set_id IN NUMBER)*/
   ---Commented end by hari for TMS # 20150611-00128
   RETURN DATE
AS
   ---Added  below by hari for TMS # 20150611-00128
   CURSOR GET_PLT
   IS
        SELECT COUNT (*) cnt, NVL (msi.FULL_LEAD_TIME, 0) FULL_LEAD_TIME
          FROM PO_LINES_ALL PLA,
               PO_LINE_LOCATIONS_ALL PLL,
               MTL_SYSTEM_ITEMS_FVL MSI,
               PO_HEADERS_ALL PHA
         WHERE     PLA.PO_HEADER_ID = P_PO_HEADER_ID
               AND PLA.PO_HEADER_ID = PLL.PO_HEADER_ID
               AND pla.po_line_id = pll.po_line_id
               AND PLA.ORG_ID = PLL.ORG_ID
               AND PLA.ITEM_ID = MSI.INVENTORY_ITEM_ID
               AND MSI.ORGANIZATION_ID =
                      NVL (PLL.SHIP_TO_ORGANIZATION_ID,
                           (SELECT inventory_organization_id
                              FROM HR_LOCATIONS
                             WHERE LOCATION_ID = PHA.SHIP_TO_LOCATION_ID))
               AND MSI.FULL_LEAD_TIME IS NOT NULL
               AND PLA.PO_HEADER_ID = PHA.PO_HEADER_ID
               AND PHA.ORG_ID = PLA.ORG_ID
      GROUP BY MSI.FULL_LEAD_TIME
      ORDER BY CNT DESC;

   CURSOR GET_MAX_CNT_PLT
   IS
      SELECT MAX (DISTINCT cnt) m_cnt
        FROM (  SELECT COUNT (*) cnt,
                       NVL (msi.FULL_LEAD_TIME, 0) FULL_LEAD_TIME
                  FROM PO_LINES_ALL PLA,
                       PO_LINE_LOCATIONS_ALL PLL,
                       MTL_SYSTEM_ITEMS_FVL MSI,
                       PO_HEADERS_ALL PHA
                 WHERE     PLA.PO_HEADER_ID = P_PO_HEADER_ID
                       AND PLA.PO_HEADER_ID = PLL.PO_HEADER_ID
                       AND pla.po_line_id = pll.po_line_id
                       AND PLA.ORG_ID = PLL.ORG_ID
                       AND PLA.ITEM_ID = MSI.INVENTORY_ITEM_ID
                       AND MSI.ORGANIZATION_ID =
                              NVL (
                                 PLL.SHIP_TO_ORGANIZATION_ID,
                                 (SELECT inventory_organization_id
                                    FROM HR_LOCATIONS
                                   WHERE LOCATION_ID = PHA.SHIP_TO_LOCATION_ID))
                       AND MSI.FULL_LEAD_TIME IS NOT NULL
                       AND PLA.PO_HEADER_ID = PHA.PO_HEADER_ID
                       AND PHA.ORG_ID = PLA.ORG_ID
              GROUP BY MSI.FULL_LEAD_TIME
              ORDER BY CNT DESC);

   CURSOR CHK_CMN_PLT (
      cp_cnt   IN NUMBER)
   IS
      SELECT MIN (FULL_LEAD_TIME)
        FROM (  SELECT COUNT (*) cnt,
                       NVL (msi.FULL_LEAD_TIME, 0) FULL_LEAD_TIME
                  FROM PO_LINES_ALL PLA,
                       PO_LINE_LOCATIONS_ALL PLL,
                       MTL_SYSTEM_ITEMS_FVL MSI,
                       PO_HEADERS_ALL PHA
                 WHERE     PLA.PO_HEADER_ID = P_PO_HEADER_ID
                       AND PLA.PO_HEADER_ID = PLL.PO_HEADER_ID
                       AND pla.po_line_id = pll.po_line_id
                       AND PLA.ORG_ID = PLL.ORG_ID
                       AND PLA.ITEM_ID = MSI.INVENTORY_ITEM_ID
                       AND MSI.ORGANIZATION_ID =
                              NVL (
                                 PLL.SHIP_TO_ORGANIZATION_ID,
                                 (SELECT inventory_organization_id
                                    FROM HR_LOCATIONS
                                   WHERE LOCATION_ID = PHA.SHIP_TO_LOCATION_ID))
                       AND MSI.FULL_LEAD_TIME IS NOT NULL
                       AND PLA.PO_HEADER_ID = PHA.PO_HEADER_ID
                       AND PHA.ORG_ID = PLA.ORG_ID
              GROUP BY MSI.FULL_LEAD_TIME
              ORDER BY CNT DESC)
       WHERE CNT = CP_CNT;

   L_MCNT           NUMBER;
   L_PLT            NUMBER;
   L_MAX_CNT        NUMBER := 0;
   L_CNT            NUMBER;
   L_NEXT_SEQ_NUM   NUMBER;
   L_NEXT_DATE      DATE;
   L_PLT_TIME       NUMBER;
   L_DAY            VARCHAR2 (100);
   L_DAYS           NUMBER;
   L_NOW            NUMBER;
   L_NOH            NUMBER;
   L_SEC            VARCHAR2 (2000) := NULL;
   L_REQ_ID         NUMBER (10);
   G_DISTRO_LIST    VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
---
BEGIN
   l_next_date := NULL;
   L_DAYS := NULL;

   --comon plt
   OPEN GET_MAX_CNT_PLT;

   FETCH GET_MAX_CNT_PLT INTO L_MCNT;

   CLOSE GET_MAX_CNT_PLT;

   IF L_MCNT > 1
   THEN
      FOR REC IN GET_PLT
      LOOP
         IF L_MCNT = REC.CNT
         THEN
            L_MAX_CNT := L_MAX_CNT + 1;
            L_CNT := REC.CNT;
         END IF;
      END LOOP;

      IF L_MAX_CNT <> 0
      THEN
         OPEN CHK_CMN_PLT (L_CNT);

         FETCH CHK_CMN_PLT INTO L_PLT;

         CLOSE CHK_CMN_PLT;
      ELSE
         L_plt := 1;
      END IF;

      L_DAYS := L_PLT;

      BEGIN
         L_DAY := TO_CHAR (TRUNC (SYSDATE) + L_DAYS, 'D');
         L_NOW := 0;
         L_NOH := 0;
         RETURN APPS.XXWC_PO_WEEKDAY_FUNC (SYSDATE + L_DAYS);
      END;
   ELSE
      SELECT                                            --MAX (full_lead_time)
             MIN (full_lead_time)
        INTO L_DAYS
        FROM (  SELECT COUNT (*) cnt,
                       NVL (msi.FULL_LEAD_TIME, 0) FULL_LEAD_TIME
                  FROM PO_LINES_ALL PLA,
                       PO_LINE_LOCATIONS_ALL PLL,
                       MTL_SYSTEM_ITEMS_FVL MSI,
                       PO_HEADERS_ALL PHA
                 WHERE     PLA.PO_HEADER_ID = P_PO_HEADER_ID
                       AND PLA.PO_HEADER_ID = PLL.PO_HEADER_ID
                       AND pla.po_line_id = pll.po_line_id
                       AND PLA.ORG_ID = PLL.ORG_ID
                       AND PLA.ITEM_ID = MSI.INVENTORY_ITEM_ID
                       AND MSI.ORGANIZATION_ID =
                              NVL (
                                 PLL.SHIP_TO_ORGANIZATION_ID,
                                 (SELECT inventory_organization_id
                                    FROM HR_LOCATIONS
                                   WHERE LOCATION_ID = PHA.SHIP_TO_LOCATION_ID))
                       AND MSI.FULL_LEAD_TIME IS NOT NULL
                       AND PLA.PO_HEADER_ID = PHA.PO_HEADER_ID
                       AND PHA.ORG_ID = PLA.ORG_ID
              GROUP BY MSI.FULL_LEAD_TIME
              ORDER BY CNT DESC);

      RETURN APPS.XXWC_PO_WEEKDAY_FUNC (SYSDATE + NVL (L_DAYS, 0));
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'Error occured while deriving the PLT');
      l_sec := 'Error occured while deriving the PLT';
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'XXWC_PLANNING_DATE',
         p_calling             => l_sec,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (l_sec, 1, 2000),
         p_error_desc          => 'OTHERS Exception',
         p_distribution_list   => g_distro_list,
         p_module              => 'XXWC');
---Added end by hari for TMS # 20150611-00128
---Commented below by hari for TMS # 20150611-00128
/*BEGIN
SELECT next_seq_num
INTO   l_next_seq_num
FROM   bom_calendar_dates
where  calendar_code = p_calendar_code
AND    exception_set_id = p_exception_set_id
AND    trunc(calendar_date) = trunc(sysdate);
END;
BEGIN
SELECT next_date
INTO   l_next_date
from   bom_calendar_dates
where  calendar_code = p_calendar_code
AND    exception_set_id = p_exception_set_id
AND    seq_num = l_next_seq_num + p_number_of_days;
END;*/
--RETURN l_next_date;
---Commented end by hari for TMS # 20150611-00128
END;
/