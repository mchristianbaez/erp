CREATE OR REPLACE FUNCTION APPS.XXWC_PO_WEEKDAY_FUNC (P_DATE IN DATE)
   RETURN DATE
/****************************************************************************************************
Function: XXWC_PO_WEEKDAY_FUNC
Description: Function to get weekend if input date in weekend date;
HISTORY
=====================================================================================================
VERSION DATE               AUTHOR(S)       DESCRIPTION
------- -----------------  --------------- ----------------------------------------------------------
1.0     11-Feb-2016        P.Vamshidhar    Initial version TMS#20150611-00128
                                           PO - Fix issue with Need by date personalization
*****************************************************************************************************/
IS
   p_new_date   DATE;
BEGIN
   p_new_date := p_date;

   IF P_DATE IS NOT NULL AND TRUNC (p_date) <> TRUNC (SYSDATE)
   THEN
      IF TRIM (TO_CHAR (P_DATE, 'Day')) LIKE 'Sat%'
      THEN
         p_new_date := p_date - 1;
      ELSIF TRIM (TO_CHAR (P_DATE, 'Day')) LIKE 'Sun%'
      THEN
         p_new_date := p_date + 1 ;
      END IF;
   END IF;

   IF TRUNC (p_date) < TRUNC (SYSDATE)
   THEN
      p_new_date := SYSDATE;
   END IF;


   RETURN p_new_date;
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN p_new_date;
END;
/
