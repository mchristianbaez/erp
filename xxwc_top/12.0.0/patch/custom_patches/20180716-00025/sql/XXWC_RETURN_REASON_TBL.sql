   /*********************************************************************************************************************
   -- Table Name: XXWC_RETURN_REASON_TBL
   -- *******************************************************************************************************************
   --  To store Return reason codes.
   -- HISTORY
   -- ===================================================================================================================
   -- ===================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ------------------------------------------------------
   -- 1.0     16-Jul-2018   P.Vamshidhar    TMS#20180716-00025 - AHH Customers Interface    
   ***********************************************************************************************************************/

CREATE TABLE XXWC.XXWC_RETURN_REASON_TBL
(
  DESCRIPTION           VARCHAR2(100 BYTE),
  ORACLE_RETURN_REASON  VARCHAR2(100 BYTE)
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_RETURN_REASON_TBL TO EA_APEX;
/