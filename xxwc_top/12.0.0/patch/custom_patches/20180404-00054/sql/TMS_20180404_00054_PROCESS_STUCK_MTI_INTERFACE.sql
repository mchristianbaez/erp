/******************************************************************************
  $Header TMS_20180404_00054_Data_Fix.sql $
  Module Name:Data Fix script for 20180404_00054

  PURPOSE: Data fix script for 20180404_00054 Update MTL_TRANSACTION_INTERFACE table.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        04-APR-2018  Krishna Kumar         20180404_00054

*******************************************************************************/

ALTER SESSION SET CURRENT_SCHEMA=apps;
/
BEGIN
   mo_global.set_policy_context ('S', '162');
END;
/
CREATE TABLE XXWC.MTL_TXN_INTERFACE_04APR18
AS SELECT * 
FROM  MTL_TRANSACTIONS_INTERFACE
WHERE TRANSACTION_TYPE_ID   = 33
AND   LOCK_FLAG             =1 ; 
/
SET serveroutput ON SIZE 500000;
DECLARE

BEGIN

UPDATE  MTL_TRANSACTIONS_INTERFACE
SET     PROCESS_FLAG            = 1,
        LOCK_FLAG               = NULL ,
        TRANSACTION_MODE        = 3 ,
        ERROR_CODE              = NULL ,
        ERROR_EXPLANATION       = NULL
WHERE   TRANSACTION_TYPE_ID   = 33
AND     LOCK_FLAG             =1 ; 

dbms_output.put_line('Number of Rows Updated '||SQL%ROWCOUNT);

COMMIT;

EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      dbms_output.put_line(substr(sqlerrm, 1, 240));
END;
/

