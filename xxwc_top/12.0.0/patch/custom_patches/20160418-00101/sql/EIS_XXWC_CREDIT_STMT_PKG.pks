CREATE OR REPLACE PACKAGE    XXEIS.EIS_XXWC_CREDIT_STMT_PKG AS
 type CURSOR_TYPE4 is ref cursor; 
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  --TMS#20160418-00101  by Siva on 04-16-2016
--//
--// Object Usage 				:: This Object Referred by "White Cap EOM Credit Memo Statement Report"
--//
--// Object Name         		:: xxeis.EIS_XXWC_CREDIT_STMT_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     siva  	04/16/2016   Initial Build --TMS#20160418-00101  by Siva   	on 04-16-2016
--//============================================================================
PROCEDURE POPULATE_CREDIT_INVOICES(P_PERIOD_YEAR IN NUMBER,
													P_period_name in varchar2,
													P_PROCESS_ID IN NUMBER,
                          p_location in varchar2,
                          p_Origin_Date_From in date,
                          p_Origin_Date_To in date
                          );
--//============================================================================
--//
--// Object Name         :: POPULATE_CREDIT_INVOICES  
--//
--// Object Usage 		 :: This Object Referred by "White Cap EOM Credit Memo Statement Report"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_AR_CREDIT_MEMO_DTLS_C View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     siva  	04/16/2016   Initial Build --TMS#20160418-00101  by Siva   	on 04-16-2016
--//============================================================================
procedure clear_temp_tables ( p_process_id in number); 
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     siva  	04/16/2016   Initial Build --TMS#20160418-00101  by Siva   	on 04-16-2016
--//============================================================================
                            
End;
/
