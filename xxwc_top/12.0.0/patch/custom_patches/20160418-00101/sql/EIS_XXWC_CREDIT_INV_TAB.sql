--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_CREDIT_INV_TAB
  Description: This table is used to get data from XXEIS.EIS_XXWC_CREDIT_STMT_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     12-Apr-2016        Siva   	--TMS#20160418-00101  Performance Tuning
********************************************************************************/
CREATE TABLE XXEIS.EIS_XXWC_CREDIT_INV_TAB 
   (PROCESS_ID NUMBER, 
	FISCAL_YEAR NUMBER(15,0), 
	FISCAL_MONTH VARCHAR2(15 BYTE), 
	CUSTOMER_NUMBER VARCHAR2(30 BYTE), 
	ORIGIN_DATE DATE, 
	SHIP_NAME VARCHAR2(150 BYTE), 
	INVOICE_TYPE VARCHAR2(150 BYTE), 
	INVOICE_DATE DATE, 
	BUSINESS_DATE DATE, 
	TAX_RATE NUMBER, 
	FREIGHT_ORIGINAL NUMBER, 
	INVOICE_TOTAL NUMBER, 
	CREDIT VARCHAR2(100 BYTE), 
	GLNO VARCHAR2(100 BYTE), 
	AR_CUSTOMER_NUMBER VARCHAR2(150 BYTE), 
	RESTOCK_FEE_PERCENT VARCHAR2(150 BYTE), 
	LOCATION VARCHAR2(30 BYTE), 
	AMOUNT_PAID NUMBER, 
	TAX_AMT NUMBER, 
	INVOICE_NUMBER VARCHAR2(30 BYTE), 
	ORDER_NUMBER VARCHAR2(150 BYTE), 
	ORIGINAL_INVOICE_NUMBER VARCHAR2(150 BYTE), 
	PART_NUMBER VARCHAR2(150 BYTE), 
	CREDIT_REASON_SALE VARCHAR2(150 BYTE), 
	INVOICE_LINE_AMOUNT NUMBER, 
	QTY_SHIPPED NUMBER, 
	SALES_REP_NO VARCHAR2(30 BYTE), 
	CUSTOMER_TERRITORY VARCHAR2(30 BYTE), 
	CUSTOMER_NAME VARCHAR2(120 BYTE), 
	MASTER_NAME VARCHAR2(240 BYTE), 
	ORGINAL_ORDER_NUMBER VARCHAR2(150 BYTE), 
	TAKEN_BY VARCHAR2(100 BYTE)
)
/
