--Report Name            : White Cap EOM Credit Memo Statement Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for White Cap EOM Credit Memo Statement Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_CREDIT_MEMO_DTLS_C
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C',222,'','','','','SA059956','XXEIS','Eis Xxwc Ar Credit Memo Dtls C','EXACMDC','','');
--Delete View Columns for EIS_XXWC_AR_CREDIT_MEMO_DTLS_C
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_CREDIT_MEMO_DTLS_C',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_CREDIT_MEMO_DTLS_C
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','ORIGIN_DATE',222,'Origin Date','ORIGIN_DATE','','','','SA059956','DATE','','','Origin Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','TAKEN_BY',222,'Taken By','TAKEN_BY','','','','SA059956','VARCHAR2','','','Taken By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','MASTER_NAME',222,'Master Name','MASTER_NAME','','','','SA059956','VARCHAR2','','','Master Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','CUSTOMER_TERRITORY',222,'Customer Territory','CUSTOMER_TERRITORY','','','','SA059956','VARCHAR2','','','Customer Territory','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','SALES_REP_NO',222,'Sales Rep No','SALES_REP_NO','','','','SA059956','VARCHAR2','','','Sales Rep No','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','QTY_SHIPPED',222,'Qty Shipped','QTY_SHIPPED','','','','SA059956','NUMBER','','','Qty Shipped','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','INVOICE_LINE_AMOUNT',222,'Invoice Line Amount','INVOICE_LINE_AMOUNT','','','','SA059956','NUMBER','','','Invoice Line Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','CREDIT_REASON_SALE',222,'Credit Reason Sale','CREDIT_REASON_SALE','','','','SA059956','VARCHAR2','','','Credit Reason Sale','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','PART_NUMBER',222,'Part Number','PART_NUMBER','','','','SA059956','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','ORIGINAL_INVOICE_NUMBER',222,'Original Invoice Number','ORIGINAL_INVOICE_NUMBER','','','','SA059956','VARCHAR2','','','Original Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','INVOICE_NUMBER',222,'Invoice Number','INVOICE_NUMBER','','','','SA059956','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','TAX_AMT',222,'Tax Amt','TAX_AMT','','','','SA059956','NUMBER','','','Tax Amt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','AMOUNT_PAID',222,'Amount Paid','AMOUNT_PAID','','','','SA059956','NUMBER','','','Amount Paid','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','LOCATION',222,'Location','LOCATION','','','','SA059956','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','INVOICE_TOTAL',222,'Invoice Total','INVOICE_TOTAL','','','','SA059956','NUMBER','','','Invoice Total','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','FREIGHT_ORIGINAL',222,'Freight Original','FREIGHT_ORIGINAL','','','','SA059956','NUMBER','','','Freight Original','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','TAX_RATE',222,'Tax Rate','TAX_RATE','','','','SA059956','NUMBER','','','Tax Rate','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','BUSINESS_DATE',222,'Business Date','BUSINESS_DATE','','','','SA059956','DATE','','','Business Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','INVOICE_DATE',222,'Invoice Date','INVOICE_DATE','','','','SA059956','DATE','','','Invoice Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','INVOICE_TYPE',222,'Invoice Type','INVOICE_TYPE','','','','SA059956','VARCHAR2','','','Invoice Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','SHIP_NAME',222,'Ship Name','SHIP_NAME','','','','SA059956','VARCHAR2','','','Ship Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','AR_CUSTOMER_NUMBER',222,'Ar Customer Number','AR_CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Ar Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','FISCAL_MONTH',222,'Fiscal Month','FISCAL_MONTH','','','','SA059956','VARCHAR2','','','Fiscal Month','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','FISCAL_YEAR',222,'Fiscal Year','FISCAL_YEAR','','','','SA059956','NUMBER','','','Fiscal Year','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','ORDER_NUMBER',222,'Order Number','ORDER_NUMBER','','','','SA059956','VARCHAR2','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','ORGINAL_ORDER_NUMBER',222,'Orginal Order Number','ORGINAL_ORDER_NUMBER','','','','SA059956','VARCHAR2','','','Orginal Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','RESTOCK_FEE_PERCENT',222,'Restock Fee Percent','RESTOCK_FEE_PERCENT','','','','SA059956','VARCHAR2','','','Restock Fee Percent','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','CREDIT',222,'Credit','CREDIT','','','','SA059956','VARCHAR2','','','Credit','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','GLNO',222,'Glno','GLNO','','','','SA059956','VARCHAR2','','','Glno','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','PROCESS_ID',222,'Process Id','PROCESS_ID','','','','SA059956','NUMBER','','','Process Id','','','');
--Inserting View Components for EIS_XXWC_AR_CREDIT_MEMO_DTLS_C
--Inserting View Component Joins for EIS_XXWC_AR_CREDIT_MEMO_DTLS_C
END;
/
set scan on define on
prompt Creating Report LOV Data for White Cap EOM Credit Memo Statement Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - White Cap EOM Credit Memo Statement Report
xxeis.eis_rs_ins.lov( 222,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'SELECT DISTINCT GPS.PERIOD_NAME
FROM      HR_OPERATING_UNITS          HOU,
          GL_LEDGERS                  GL,
          gl_period_statuses          gps
WHERE
 GL.LEDGER_ID                      = HOU.SET_OF_BOOKS_ID
AND GL.LEDGER_ID                      = GPS.SET_OF_BOOKS_ID
AND GL.ACCOUNTED_PERIOD_TYPE          = GPS.PERIOD_TYPE
order by GPS.PERIOD_NAME','','Fiscal Month','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'SELECT DISTINCT GPS.PERIOD_YEAR
FROM      HR_OPERATING_UNITS          HOU,
          GL_LEDGERS                  GL,
          gl_period_statuses          gps
WHERE
 GL.LEDGER_ID                      = HOU.SET_OF_BOOKS_ID
AND GL.LEDGER_ID                      = GPS.SET_OF_BOOKS_ID
AND GL.ACCOUNTED_PERIOD_TYPE          = GPS.PERIOD_TYPE
order by GPS.PERIOD_YEAR
','','Fiscal Year','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct apply_date
    from ar_receivable_applications
    where application_type=''CM''','','Origin Date','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct apply_date
    from ar_receivable_applications
    where application_type=''CM''','','Origin Date','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for White Cap EOM Credit Memo Statement Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - White Cap EOM Credit Memo Statement Report
xxeis.eis_rs_utility.delete_report_rows( 'White Cap EOM Credit Memo Statement Report' );
--Inserting Report - White Cap EOM Credit Memo Statement Report
xxeis.eis_rs_ins.r( 222,'White Cap EOM Credit Memo Statement Report','','','','','','SA059956','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','Y','','','SA059956','','Y','White Cap Reports','','CSV,Pivot Excel,EXCEL,','');
--Inserting Report Columns - White Cap EOM Credit Memo Statement Report
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'INVOICE_TYPE','Invoice Type','Invoice Type','','','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'ORIGIN_DATE','Origin Date','Origin Date','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'SHIP_NAME','Ship Name','Ship Name','','','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'TAX_RATE','Tax Rate','Tax Rate','','~~~','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'AMOUNT_PAID','Amount Paid','Amount Paid','','~T~D~2','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'BUSINESS_DATE','Business Date','Business Date','','','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'AR_CUSTOMER_NUMBER','Ar Customer Number','Ar Customer Number','','','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'FISCAL_MONTH','Fiscal Month','Fiscal Month','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','17','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'INVOICE_TOTAL','Invoice Total','Invoice Total','','~~~','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'LOCATION','Location','Location','','','default','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'TAX_AMT','Tax Amt','Tax Amt','','~T~D~2','default','','16','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'CREDIT_REASON_SALE','Credit Reason Sale','Credit Reason Sale','','','default','','21','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','26','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'FISCAL_YEAR','Fiscal Year','Fiscal Year','','~~~','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'FREIGHT_ORIGINAL','Freight Original','Freight Original','','~T~D~2','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'CUSTOMER_TERRITORY','Customer Territory','Customer Territory','','','default','','25','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'TAKEN_BY','Taken By','Taken By','','','default','','29','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'INVOICE_LINE_AMOUNT','Invoice Line Amount','Invoice Line Amount','','~T~D~2','default','','22','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'MASTER_NAME','Master Name','Master Name','','','default','','27','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'ORIGINAL_INVOICE_NUMBER','Original Invoice Number','Original Invoice Number','','','default','','19','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'PART_NUMBER','Part Number','Part Number','','','default','','20','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'QTY_SHIPPED','Qty Shipped','Qty Shipped','','~~~','default','','23','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'SALES_REP_NO','Sales Rep No','Sales Rep No','','','default','','24','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'ORDER_NUMBER','Order Number','Order Number','','','','','18','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'ORGINAL_ORDER_NUMBER','Orginal Order Number','Orginal Order Number','','','','','28','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'RESTOCK_FEE_PERCENT','Restock Fee Percent','Restock Fee Percent','','','','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','');
--Inserting Report Parameters - White Cap EOM Credit Memo Statement Report
xxeis.eis_rs_ins.rp( 'White Cap EOM Credit Memo Statement Report',222,'Fiscal Year','Fiscal Year','FISCAL_YEAR','IN','Fiscal Year','SELECT distinct period_year FROM GL_PERIOD_STATUSES WHERE  APPLICATION_ID=101 AND CLOSING_STATUS=''O'' and sysdate between start_date and end_date','NUMERIC','Y','Y','1','','N','SQL','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap EOM Credit Memo Statement Report',222,'Fiscal Month','Fiscal Month','FISCAL_MONTH','IN','Fiscal Month','SELECT distinct period_name FROM GL_PERIOD_STATUSES WHERE  APPLICATION_ID=101 AND CLOSING_STATUS=''O'' and sysdate between start_date and end_date','VARCHAR2','Y','Y','2','','N','SQL','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap EOM Credit Memo Statement Report',222,'Location','Location','LOCATION','IN','AR Organizaion Code LOV','','VARCHAR2','N','Y','3','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap EOM Credit Memo Statement Report',222,'Origin Date From','Origin Date From','ORIGIN_DATE','>=','Origin Date','','DATE','N','Y','4','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap EOM Credit Memo Statement Report',222,'Origin Date To','Origin Date To','ORIGIN_DATE','<=','Origin Date','','DATE','N','Y','5','','N','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - White Cap EOM Credit Memo Statement Report
xxeis.eis_rs_ins.rcn( 'White Cap EOM Credit Memo Statement Report',222,'','','','','and process_id= :SYSTEM.PROCESS_ID','Y','1','','SA059956');
--Inserting Report Sorts - White Cap EOM Credit Memo Statement Report
--Inserting Report Triggers - White Cap EOM Credit Memo Statement Report
xxeis.eis_rs_ins.rt( 'White Cap EOM Credit Memo Statement Report',222,'BEGIN
XXEIS.eis_xxwc_credit_stmt_pkg.Populate_credit_invoices(
P_Period_Year => :Fiscal Year,
P_period_name => :Fiscal Month,
P_Process_id => :SYSTEM.PROCESS_ID,
p_location => :Location,
p_Origin_Date_From => :Origin Date From,
p_Origin_Date_To => :Origin Date To
);
END;','B','Y','SA059956');
xxeis.eis_rs_ins.rt( 'White Cap EOM Credit Memo Statement Report',222,'BEGIN
XXEIS.eis_xxwc_credit_stmt_pkg.CLEAR_TEMP_TABLES(P_Process_id => :SYSTEM.PROCESS_ID);
END;','A','Y','SA059956');
--Inserting Report Templates - White Cap EOM Credit Memo Statement Report
--Inserting Report Portals - White Cap EOM Credit Memo Statement Report
--Inserting Report Dashboards - White Cap EOM Credit Memo Statement Report
--Inserting Report Security - White Cap EOM Credit Memo Statement Report
xxeis.eis_rs_ins.rsec( 'White Cap EOM Credit Memo Statement Report','222','','50894',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'White Cap EOM Credit Memo Statement Report','','PP018915','',222,'SA059956','','');
--Inserting Report Pivots - White Cap EOM Credit Memo Statement Report
END;
/
set scan on define on
