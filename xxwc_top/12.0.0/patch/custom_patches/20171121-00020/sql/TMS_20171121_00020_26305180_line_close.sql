/*************************************************************************
  $Header TMS_20171121_00020_26305180_line_close.sql $
  Module Name: TMS_20171121-00020  


  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        12-Dec-2017  Krishna               TMS# 20171121-00020 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20171121-00020, Before Update');

UPDATE apps.oe_order_lines_all
   SET flow_status_code='CLOSED',
       open_flag='N'
 WHERE line_id=108665440
   AND header_id =65243763;

   DBMS_OUTPUT.put_line (
         'TMS: 20171121-00020  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20171121-00020    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20171121-00020 , Errors : ' || SQLERRM);
END;
/