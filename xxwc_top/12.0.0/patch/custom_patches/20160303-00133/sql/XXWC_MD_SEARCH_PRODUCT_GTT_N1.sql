  /*******************************************************************************
  Index: XXWC_MD_SEARCH_PRODUCT_GTT_N1
  Description: This index is used on XXWC_MD_SEARCH_PRODUCT_GTT_TBL table 
  and is used on AIS form
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     01-Mar-2016        Pahwa Nancy   TMS# 20160301-00328  Performance Tuning
  1.1     03-Mar-2016        Pahwa Nancy   TMS#	20160303-00133  Performance Tuning
  ********************************************************************************/
CREATE INDEX XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_N1 ON XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
(INVENTORY_ITEM_ID);
/