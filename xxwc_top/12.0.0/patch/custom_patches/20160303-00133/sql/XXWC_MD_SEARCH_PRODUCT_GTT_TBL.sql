  /*******************************************************************************
  Table: XXWC_MD_SEARCH_PRODUCT_GTT_TBL
  Description: This table is used to load items searched on AIS form
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     01-Mar-2016        Pahwa Nancy   TMS# 20160301-00328  Performance Tuning
  1.1     03-Mar-2016        Pahwa Nancy   TMS# 20160303-00133  Performance Tuning -Added new new modifier columns
  ********************************************************************************/
Drop table XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL CASCADE CONSTRAINTS;
CREATE GLOBAL TEMPORARY TABLE "XXWC"."XXWC_MD_SEARCH_PRODUCT_GTT_TBL" 
   (	"SCORE" NUMBER, 
	"INVENTORY_ITEM_ID" NUMBER, 
	"ORGANIZATION_ID" NUMBER, 
	"PARTNUMBER" VARCHAR2(40), 
	"TYPE" CHAR(7), 
	"MANUFACTURERPARTNUMBER" VARCHAR2(150), 
	"MANUFACTURER" VARCHAR2(150), 
	"SEQUENCE" NUMBER, 
	"SHORTDESCRIPTION" VARCHAR2(240), 
	"QUANTITYONHAND" NUMBER, 
	"LIST_PRICE" NUMBER, 
	"BUYABLE" CHAR(1), 
	"KEYWORD" VARCHAR2(1000), 
	"ITEM_TYPE" VARCHAR2(30), 
	"CROSS_REFERENCE" VARCHAR2(4000), 
	"PRIMARY_UOM_CODE" VARCHAR2(3), 
	"SELLING_PRICE" NUMBER, 
	"MODIFIER" VARCHAR2(240), 
	"MODIFIER_TYPE" VARCHAR2(200)
   ) 
ON COMMIT PRESERVE ROWS
RESULT_CACHE (MODE DEFAULT)
NOCACHE;
/