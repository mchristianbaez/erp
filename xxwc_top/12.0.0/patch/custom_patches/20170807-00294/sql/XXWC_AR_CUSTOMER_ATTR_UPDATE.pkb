CREATE OR REPLACE PACKAGE BODY APPS.xxwc_ar_customer_attr_update
IS
   l_running_messaget   CLOB;
   l_run_id             NUMBER;
   l_errbuf             VARCHAR2 (3024) := NULL;
   l_retcode            VARCHAR2 (1024) := '0';
   l_req_id             NUMBER := fnd_global.conc_request_id;

   /*************************************************************************************************************************************
     $Header XXWC_AR_CUSTOMER_ATTR_UPDATE $
     Module Name: XXWC_AR_CUSTOMER_ATTR_UPDATE.pkb

     PURPOSE:   Updating AR customer Attributes

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0
     1.1        05/05/2016  P.Vamshidhar            TMS# 20160407-00192
     1.2        6/22/2016   S.Neha                  TMS#20170530-00341 - Improving th eperformance for change collector and credit nalayst procedure  
  -- 1.3         8/23/2017   S.neha                 TMS 20170807-00294 --changes done for ver and TMS 20170807-00294
  --***********************************************************************************************************************************/

   PROCEDURE create_output_report (p_type VARCHAR2)
   IS
      v_report_lines   NUMBER;
      v_report_line    CLOB;
   BEGIN
      EXECUTE IMMEDIATE 'truncate table xxwc.xxwc_account_mass_report';

      IF p_type = 'error'
      THEN
         FOR r IN (SELECT *
                     FROM apps.xxwc_account_mass_update_list
                    WHERE error_message IS NOT NULL AND run_id = l_run_id)
         LOOP
            IF r.table_name = 'hz_customer_profiles'
            THEN
               INSERT INTO xxwc.xxwc_account_mass_report (
                              change_type,
                              previous_name,
                              new_name,
                              site_uses_rowid,
                              prof_rowid,
                              account_number,
                              account_name,
                              account_address,
                              site_address,
                              location,
                              collector_name,
                              credit_analyst_name,
                              salesreps_name,
                              error_message)
                  SELECT r.change_name,
                         r.old_name,
                         r.new_name,
                         site_uses_rowid,
                         prof_rowid,
                         account_number,
                         account_name,
                         account_address,
                         site_address,
                         location,
                         collector_name,
                         credit_analyst_name,
                         salesreps_name,
                         SUBSTR (r.error_message, 1, 1024)
                    FROM xxwc_customer_attr_update
                   WHERE level_flag = 'P' AND prof_rowid = r.table_rowid;
            END IF;

            IF r.table_name = 'hz_cust_site_uses_all'
            THEN
               INSERT INTO xxwc.xxwc_account_mass_report (
                              change_type,
                              previous_name,
                              new_name,
                              site_uses_rowid,
                              prof_rowid,
                              account_number,
                              account_name,
                              account_address,
                              site_address,
                              location,
                              collector_name,
                              credit_analyst_name,
                              salesreps_name,
                              error_message)
                  SELECT r.change_name,
                         r.old_name,
                         r.new_name,
                         site_uses_rowid,
                         prof_rowid,
                         account_number,
                         account_name,
                         account_address,
                         site_address,
                         location,
                         collector_name,
                         credit_analyst_name,
                         salesreps_name,
                         SUBSTR (r.error_message, 1, 1024)
                    FROM xxwc_customer_attr_update
                   WHERE level_flag = 'P' AND site_uses_rowid = r.table_rowid;
            END IF;
         END LOOP;
      END IF;

      IF p_type = 'success'
      THEN
         FOR r IN (SELECT *
                     FROM apps.xxwc_account_mass_update_list
                    WHERE error_message IS NULL AND run_id = l_run_id)
         LOOP
            IF r.table_name = 'hz_customer_profiles'
            THEN
               INSERT INTO xxwc.xxwc_account_mass_report (
                              change_type,
                              previous_name,
                              new_name,
                              site_uses_rowid,
                              prof_rowid,
                              account_number,
                              account_name,
                              account_address,
                              site_address,
                              location,
                              collector_name,
                              credit_analyst_name,
                              salesreps_name)
                  SELECT r.change_name,
                         r.old_name,
                         r.new_name,
                         site_uses_rowid,
                         prof_rowid,
                         account_number,
                         account_name,
                         account_address,
                         site_address,
                         location,
                         collector_name,
                         credit_analyst_name,
                         salesreps_name
                    FROM xxwc_customer_attr_update
                   WHERE level_flag = 'P' AND prof_rowid = r.table_rowid;
            END IF;

            IF r.table_name = 'hz_cust_site_uses_all'
            THEN
               INSERT INTO xxwc.xxwc_account_mass_report (
                              change_type,
                              previous_name,
                              new_name,
                              site_uses_rowid,
                              prof_rowid,
                              account_number,
                              account_name,
                              account_address,
                              site_address,
                              location,
                              collector_name,
                              credit_analyst_name,
                              salesreps_name)
                  SELECT r.change_name,
                         r.old_name,
                         r.new_name,
                         site_uses_rowid,
                         prof_rowid,
                         account_number,
                         account_name,
                         account_address,
                         site_address,
                         location,
                         collector_name,
                         credit_analyst_name,
                         salesreps_name
                    FROM xxwc_customer_attr_update
                   WHERE level_flag = 'P' AND site_uses_rowid = r.table_rowid;
            END IF;
         END LOOP;
      END IF;

      SELECT COUNT ('a')
        INTO v_report_lines
        FROM apps.xxwc_account_mass_report;

      IF p_type = 'error' AND v_report_lines > 0
      THEN
         fnd_file.put_line (fnd_file.output,
                            '                Error Report               ');
         --put headers

         v_report_line :=
               'change_type'
            || ','
            || 'previous_name'
            || ','
            || 'new_name'
            || ','
            || 'error_message'
            || ','
            || 'account_number'
            || ','
            || 'account_name'
            || ','
            || 'account_address'
            || ','
            || 'site_address'
            || ','
            || 'location'
            || ','
            || 'collector_name'
            || ','
            || 'credit_analyst_name'
            || ','
            || 'salesreps_name';
         fnd_file.put_line (fnd_file.output, v_report_line);

         --put lines
         FOR r IN (SELECT * FROM apps.xxwc_account_mass_report)
         LOOP
            v_report_line :=
                  r.change_type
               || ','
               || r.previous_name
               || ','
               || r.new_name
               || ','
               || r.error_message
               || ','
               || r.account_number
               || ','
               || r.account_name
               || ','
               || r.account_address
               || ','
               || r.site_address
               || ','
               || r.location
               || ','
               || r.collector_name
               || ','
               || r.credit_analyst_name
               || ','
               || r.salesreps_name;
            fnd_file.put_line (fnd_file.output, v_report_line);
         END LOOP;
      ELSIF p_type = 'success' AND v_report_lines > 0
      THEN
         fnd_file.put_line (fnd_file.output,
                            '                Success Report               ');

         --put headers
         v_report_line :=
               'change_type'
            || ','
            || 'previous_name'
            || ','
            || 'new_name'
            || ','
            || 'account_number'
            || ','
            || 'account_name'
            || ','
            || 'account_address'
            || ','
            || 'site_address'
            || ','
            || 'location'
            || ','
            || 'collector_name'
            || ','
            || 'credit_analyst_name'
            || ','
            || 'salesreps_name';
         fnd_file.put_line (fnd_file.output, v_report_line);

         --put lines
         FOR r IN (SELECT * FROM apps.xxwc_account_mass_report)
         LOOP
            v_report_line :=
                  r.change_type
               || ','
               || r.previous_name
               || ','
               || r.new_name
               || ','
               || r.account_number
               || ','
               || r.account_name
               || ','
               || r.account_address
               || ','
               || r.site_address
               || ','
               || r.location
               || ','
               || r.collector_name
               || ','
               || r.credit_analyst_name
               || ','
               || r.salesreps_name;
            fnd_file.put_line (fnd_file.output, v_report_line);
         END LOOP;
      END IF;
   END;

   --*******************************************************

   PROCEDURE keep_profile_amounts_asbefore (prof_rowid VARCHAR2)
   IS
      v_return_status      VARCHAR2 (1024);
      v_msg_count          NUMBER;
      v_msg_data           VARCHAR2 (1024);
      v_running_message1   VARCHAR2 (1024);
      v_msg_dummy          VARCHAR2 (1024);
   BEGIN
      FOR r IN (SELECT *
                  FROM apps.xxwc_customer_maint_amts
                 WHERE prof_rowid = prof_rowid)
      LOOP
         FOR rinner IN (SELECT *
                          FROM hz_cust_profile_amts
                         WHERE ROWID = r.amount_rowid)
         LOOP
            IF    NVL (rinner.overall_credit_limit, 9999999999) <>
                     NVL (r.overall_credit_limit, 9999999999)
               OR NVL (rinner.trx_credit_limit, 9999999999) <>
                     NVL (r.trx_credit_limit, 9999999999)
               OR NVL (rinner.min_statement_amount, 9999999999) <>
                     NVL (r.min_statement_amount, 9999999999)
               OR NVL (rinner.auto_rec_min_receipt_amount, 9999999999) <>
                     NVL (r.auto_rec_min_receipt_amount, 9999999999)
               OR NVL (rinner.interest_rate, 9999999999) <>
                     NVL (r.interest_rate, 9999999999)
            THEN
               UPDATE hz_cust_profile_amts
                  SET overall_credit_limit = r.overall_credit_limit,
                      trx_credit_limit = r.trx_credit_limit,
                      min_statement_amount = r.min_statement_amount,
                      auto_rec_min_receipt_amount =
                         r.auto_rec_min_receipt_amount,
                      interest_rate = r.interest_rate
                WHERE ROWID = r.amount_rowid;
            END IF;
         END LOOP;
      END LOOP;

      DELETE FROM xxwc_customer_maint_amts
            WHERE prof_rowid = prof_rowid;
   END;

   --**********************************************************
   --**********************************************************
   --*******************************************************

   FUNCTION get_sales_rep (p_org_id NUMBER, p_salesrep_id NUMBER)
      RETURN VARCHAR2
   IS
      v_salesrep      VARCHAR2 (164);
      v_resource_id   NUMBER;
   BEGIN
      BEGIN
         SELECT resource_id
           INTO v_resource_id
           FROM jtf_rs_salesreps jrs
          WHERE salesrep_id = p_salesrep_id AND org_id = p_org_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_resource_id := NULL;
      END;

      IF v_resource_id IS NOT NULL
      THEN
         FOR r
            IN (SELECT c.resource_id,
                       c.resource_name name,
                       c.source_id person_id,
                       b.role_code
                  FROM jtf_rs_role_relations a,
                       jtf_rs_roles_vl b,
                       jtf_rs_resource_extns_vl c
                 WHERE     a.role_resource_type = 'RS_INDIVIDUAL'
                       AND a.role_resource_id = c.resource_id
                       AND a.role_id = b.role_id
                       AND b.role_code = 'SALES_REP'
                       --  AND c.category = 'EMPLOYEE'
                       AND NVL (a.delete_flag, 'N') <> 'Y'
                       AND resource_id = v_resource_id)
         LOOP
            v_salesrep := r.name;
         END LOOP;
      END IF;

      RETURN v_salesrep;
   END;

   --**************************************************************
   --**************************************************************
   --**************************************************************

   FUNCTION credit_analyst (p_credit_analyst_id NUMBER)
      RETURN VARCHAR2
   IS
      v_credit_analyst_name   VARCHAR2 (124);
   BEGIN
      FOR r
         IN (SELECT c.resource_id,
                    c.resource_name name,
                    c.source_id person_id
               FROM jtf_rs_role_relations a,
                    jtf_rs_roles_vl b,
                    jtf_rs_resource_extns_vl c
              WHERE     a.role_resource_type = 'RS_INDIVIDUAL'
                    AND a.role_resource_id = c.resource_id
                    AND a.role_id = b.role_id
                    AND b.role_code = 'CREDIT_ANALYST'
                    AND c.category = 'EMPLOYEE'
                    AND NVL (a.delete_flag, 'N') <> 'Y'
                    AND c.resource_id = p_credit_analyst_id)
      LOOP
         v_credit_analyst_name := r.name;
      END LOOP;

      RETURN v_credit_analyst_name;
   END;

   --**************************************************************
   --**************************************************************
   --**************************************************************

   PROCEDURE xxwc_update_account (pr_cust_rowid             VARCHAR2,
                                  pr_primary_salesrep_id    NUMBER,
                                  p_last_updated_by         NUMBER,
                                  p_last_update_date        DATE,
                                  p_last_update_login       NUMBER,
                                  p_responsibility_id       NUMBER,
                                  p_org_id                  NUMBER)
   IS
      v_status                  VARCHAR2 (12);
      v_primary_salesrep_id     NUMBER;

      v_return_status           VARCHAR2 (1024);
      v_msg_count               NUMBER;
      v_msg_data                VARCHAR2 (1024);
      v_object_version_number   NUMBER := 1;
      v_cust_account_rec        hz_cust_account_v2pub.cust_account_rec_type;

      v_running_message1        VARCHAR2 (1024);
      l_msg_dummy               VARCHAR2 (1000);

      l_errbuf                  CLOB;
   BEGIN
      l_running_messaget := NULL;

      FOR r IN (SELECT *
                  FROM hz_cust_accounts
                 WHERE ROWID = pr_cust_rowid)
      LOOP
         v_cust_account_rec.cust_account_id := r.cust_account_id;
         v_cust_account_rec.account_number := r.account_number;
         v_cust_account_rec.attribute_category := r.attribute_category;
         v_cust_account_rec.attribute1 := r.attribute1;
         v_cust_account_rec.attribute2 := r.attribute2;
         v_cust_account_rec.attribute3 := r.attribute3;
         v_cust_account_rec.attribute4 := r.attribute4;
         v_cust_account_rec.attribute5 := r.attribute5;
         v_cust_account_rec.attribute6 := r.attribute6;
         v_cust_account_rec.attribute7 := r.attribute7;
         v_cust_account_rec.attribute8 := r.attribute8;
         v_cust_account_rec.attribute9 := r.attribute9;
         v_cust_account_rec.attribute10 := r.attribute10;
         v_cust_account_rec.attribute11 := r.attribute11;
         v_cust_account_rec.attribute12 := r.attribute12;
         v_cust_account_rec.attribute13 := r.attribute13;
         v_cust_account_rec.attribute14 := r.attribute14;
         v_cust_account_rec.attribute15 := r.attribute15;
         v_cust_account_rec.attribute16 := r.attribute16;
         v_cust_account_rec.attribute17 := r.attribute17;
         v_cust_account_rec.attribute18 := r.attribute18;
         v_cust_account_rec.attribute19 := r.attribute19;
         v_cust_account_rec.attribute20 := r.attribute20;
         v_cust_account_rec.global_attribute_category :=
            r.global_attribute_category;
         v_cust_account_rec.global_attribute1 := r.global_attribute1;
         v_cust_account_rec.global_attribute2 := r.global_attribute2;
         v_cust_account_rec.global_attribute3 := r.global_attribute3;
         v_cust_account_rec.global_attribute4 := r.global_attribute4;
         v_cust_account_rec.global_attribute5 := r.global_attribute5;
         v_cust_account_rec.global_attribute6 := r.global_attribute6;
         v_cust_account_rec.global_attribute7 := r.global_attribute7;
         v_cust_account_rec.global_attribute8 := r.global_attribute8;
         v_cust_account_rec.global_attribute9 := r.global_attribute9;
         v_cust_account_rec.global_attribute10 := r.global_attribute10;
         v_cust_account_rec.global_attribute11 := r.global_attribute11;
         v_cust_account_rec.global_attribute12 := r.global_attribute12;
         v_cust_account_rec.global_attribute13 := r.global_attribute13;
         v_cust_account_rec.global_attribute14 := r.global_attribute14;
         v_cust_account_rec.global_attribute15 := r.global_attribute15;
         v_cust_account_rec.global_attribute16 := r.global_attribute16;
         v_cust_account_rec.global_attribute17 := r.global_attribute17;
         v_cust_account_rec.global_attribute18 := r.global_attribute18;
         v_cust_account_rec.global_attribute19 := r.global_attribute19;
         v_cust_account_rec.global_attribute20 := r.global_attribute20;
         v_cust_account_rec.orig_system_reference := r.orig_system_reference;
         -- V_CUST_ACCOUNT_REC.ORIG_SYSTEM := R.ORIG_SYSTEM;
         v_cust_account_rec.status := r.status;
         v_cust_account_rec.customer_type := r.customer_type;
         v_cust_account_rec.customer_class_code := r.customer_class_code;
         v_cust_account_rec.primary_salesrep_id :=
            NVL (pr_primary_salesrep_id, r.primary_salesrep_id); --******************
         v_cust_account_rec.sales_channel_code := r.sales_channel_code;
         v_cust_account_rec.order_type_id := r.order_type_id;
         v_cust_account_rec.price_list_id := r.price_list_id;
         v_cust_account_rec.tax_code := r.tax_code;
         v_cust_account_rec.fob_point := r.fob_point;
         v_cust_account_rec.freight_term := r.freight_term;
         v_cust_account_rec.ship_partial := r.ship_partial;
         v_cust_account_rec.ship_via := r.ship_via;
         v_cust_account_rec.warehouse_id := r.warehouse_id;
         v_cust_account_rec.tax_header_level_flag := r.tax_header_level_flag;
         v_cust_account_rec.tax_rounding_rule := r.tax_rounding_rule;
         v_cust_account_rec.coterminate_day_month := r.coterminate_day_month;
         v_cust_account_rec.primary_specialist_id := r.primary_specialist_id;
         v_cust_account_rec.secondary_specialist_id :=
            r.secondary_specialist_id;
         v_cust_account_rec.account_liable_flag := r.account_liable_flag;
         v_cust_account_rec.current_balance := r.current_balance;
         v_cust_account_rec.account_established_date :=
            r.account_established_date;
         v_cust_account_rec.account_termination_date :=
            r.account_termination_date;
         v_cust_account_rec.account_activation_date :=
            r.account_activation_date;
         v_cust_account_rec.department := r.department;
         v_cust_account_rec.held_bill_expiration_date :=
            r.held_bill_expiration_date;
         v_cust_account_rec.hold_bill_flag := r.hold_bill_flag;
         v_cust_account_rec.realtime_rate_flag := r.realtime_rate_flag;
         v_cust_account_rec.acct_life_cycle_status := r.acct_life_cycle_status;
         v_cust_account_rec.account_name := r.account_name;
         v_cust_account_rec.deposit_refund_method := r.deposit_refund_method;
         v_cust_account_rec.dormant_account_flag := r.dormant_account_flag;
         v_cust_account_rec.npa_number := r.npa_number;
         v_cust_account_rec.suspension_date := r.suspension_date;
         v_cust_account_rec.source_code := r.source_code;
         v_cust_account_rec.comments := r.comments;
         v_cust_account_rec.dates_negative_tolerance :=
            r.dates_negative_tolerance;
         v_cust_account_rec.dates_positive_tolerance :=
            r.dates_positive_tolerance;
         v_cust_account_rec.date_type_preference := r.date_type_preference;
         v_cust_account_rec.over_shipment_tolerance :=
            r.over_shipment_tolerance;
         v_cust_account_rec.under_shipment_tolerance :=
            r.under_shipment_tolerance;
         v_cust_account_rec.over_return_tolerance := r.over_return_tolerance;
         v_cust_account_rec.under_return_tolerance := r.under_return_tolerance;
         v_cust_account_rec.item_cross_ref_pref := r.item_cross_ref_pref;
         v_cust_account_rec.ship_sets_include_lines_flag :=
            r.ship_sets_include_lines_flag;
         v_cust_account_rec.arrivalsets_include_lines_flag :=
            r.arrivalsets_include_lines_flag;
         v_cust_account_rec.sched_date_push_flag := r.sched_date_push_flag;
         v_cust_account_rec.invoice_quantity_rule := r.invoice_quantity_rule;
         v_cust_account_rec.pricing_event := r.pricing_event;
         v_cust_account_rec.status_update_date := r.status_update_date;
         v_cust_account_rec.autopay_flag := r.autopay_flag;
         v_cust_account_rec.notify_flag := r.notify_flag;
         v_cust_account_rec.last_batch_id := r.last_batch_id;
         v_cust_account_rec.selling_party_id := r.selling_party_id;
         v_cust_account_rec.created_by_module := r.created_by_module;
         v_cust_account_rec.application_id := r.application_id;
         v_object_version_number := r.object_version_number;

         IF     pr_primary_salesrep_id IS NOT NULL
            AND NVL (pr_primary_salesrep_id, 999999999999) <>
                   NVL (r.primary_salesrep_id, 999999999999)
         THEN
            -- DBMS_OUTPUT.put_line ('we are here');
            hz_cust_account_v2pub.update_cust_account (
               p_cust_account_rec        => v_cust_account_rec,
               p_object_version_number   => v_object_version_number,
               x_return_status           => v_return_status,
               x_msg_count               => v_msg_count,
               x_msg_data                => v_msg_data);
         END IF;

         -- DBMS_OUTPUT.put_line ('v_return_status ' || v_return_status);
         --  DBMS_OUTPUT.put_line ('v_msg_count ' || v_msg_count);
         --  DBMS_OUTPUT.put_line ('v_msg_data ' || v_msg_data);

         IF v_msg_count > 0                       --AND V_RETURN_STATUS <> 'S'
         THEN
            v_running_message1 := '';

            FOR i IN 1 .. v_msg_count
            LOOP
               fnd_msg_pub.get (i,
                                fnd_api.g_false,
                                v_msg_data,
                                l_msg_dummy);

               IF NVL (v_running_message1, 'null') <> v_msg_data
               THEN
                  --  DBMS_OUTPUT.put_line (SUBSTR ('Msg' || TO_CHAR (i) || ': ' || v_msg_data, 1, 255));
                  l_running_messaget :=
                        l_running_messaget
                     || ' '
                     || SUBSTR ('Msg' || TO_CHAR (i) || ': ' || v_msg_data,
                                1,
                                255);
               END IF;

               v_running_message1 := v_msg_data;
            END LOOP;
         END IF;
      END LOOP;
   --change the values
   EXCEPTION
      WHEN OTHERS
      THEN
         l_running_messaget := NULL;
         l_running_messaget :=
               'Error_Stack...XXWC_UPDATE_ACCOUNT'
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();

         fnd_file.put_line (fnd_file.LOG, 'error ' || l_running_messaget);
   END xxwc_update_account;

   --*****************************************************************************************
   --*****************************************************************************************
   --*****************************************************************************************

   PROCEDURE xxwc_update_customer_profile (p_prof_rowid           VARCHAR2,
                                           p_salesrep_id          NUMBER,
                                           p_collector_id         NUMBER,
                                           p_remmit_to_code       VARCHAR2,
                                           p_freight_term         VARCHAR2,
                                           p_analyst_id           NUMBER,
                                           p_credit_checking      VARCHAR2,
                                           p_credit_hold          VARCHAR2,
                                           p_payment_term_id      NUMBER,
                                           p_profile_class_id     NUMBER,
                                           p_account_status       VARCHAR2,
                                           p_credit_class_code    VARCHAR2,
                                           p_cust_rowid           VARCHAR2,
                                           p_site_uses_rowid      VARCHAR2,
                                           p_last_updated_by      NUMBER,
                                           p_last_update_date     DATE,
                                           p_last_update_login    NUMBER,
                                           p_responsibility_id    NUMBER,
                                           p_org_id               NUMBER,
                                           p_level_flag           VARCHAR2)
   IS
      v_cust_profile_rec          hz_customer_profile_v2pub.customer_profile_rec_type;
      v_object_version_number     NUMBER;
      v_return_status             VARCHAR2 (1024);
      v_msg_count                 NUMBER;
      v_msg_data                  VARCHAR2 (1024);
      v_running_message1          VARCHAR2 (1024);
      l_msg_dummy                 VARCHAR2 (1024);
      v_user_name                 VARCHAR2 (164);
      v_user_id                   NUMBER := p_last_updated_by;
      v_resp_id                   NUMBER;
      v_resp_appl_id              NUMBER;
      l_errbuf                    CLOB;
      v_appl_short_name           VARCHAR2 (164);
      v_org_id                    NUMBER;
      v_before_profile_class_id   NUMBER;
      v_after_profile_class_id    NUMBER;
      v_amt_row_inserted          NUMBER;
   BEGIN
      SELECT profile_class_id
        INTO v_before_profile_class_id
        FROM hz_customer_profiles
       WHERE ROWID = p_prof_rowid;

      l_running_messaget := NULL;

      --save profile amount values in temporary table
      BEGIN
         IF p_level_flag = 'P'
         THEN
            SELECT COUNT ('a')
              INTO v_amt_row_inserted
              FROM apps.xxwc_customer_maint_amts
             WHERE prof_rowid = p_prof_rowid;

            IF v_amt_row_inserted = 0
            THEN
               INSERT INTO xxwc.xxwc_customer_maint_amts
                  SELECT *
                    FROM xxwc_customer_maint_amount_v
                   WHERE prof_rowid = p_prof_rowid;
            END IF;
         END IF;
      END;

      --*****************************************************************
      l_running_messaget := NULL;

      IF p_level_flag = 'P'
      THEN
         FOR r IN (SELECT *
                     FROM hz_customer_profiles
                    WHERE ROWID = p_prof_rowid)
         LOOP
            v_cust_profile_rec.cust_account_profile_id :=
               r.cust_account_profile_id;
            v_cust_profile_rec.cust_account_id := r.cust_account_id;
            v_cust_profile_rec.status := r.status;
            v_cust_profile_rec.collector_id :=
               NVL (p_collector_id, r.collector_id);                 --*******
            v_cust_profile_rec.credit_analyst_id :=
               NVL (p_analyst_id, r.credit_analyst_id);              --*******
            v_cust_profile_rec.credit_checking :=
               NVL (p_credit_checking, r.credit_checking);           --*******
            v_cust_profile_rec.next_credit_review_date :=
               r.next_credit_review_date;
            v_cust_profile_rec.tolerance := r.tolerance;
            v_cust_profile_rec.discount_terms := r.discount_terms;
            v_cust_profile_rec.dunning_letters := r.dunning_letters;
            v_cust_profile_rec.interest_charges := r.interest_charges;
            v_cust_profile_rec.send_statements := r.send_statements;
            v_cust_profile_rec.credit_balance_statements :=
               r.credit_balance_statements;
            v_cust_profile_rec.credit_hold :=
               NVL (p_credit_hold, r.credit_hold);                   --*******
            v_cust_profile_rec.profile_class_id :=
               NVL (p_profile_class_id, r.profile_class_id);         --*******
            v_cust_profile_rec.site_use_id := r.site_use_id;
            v_cust_profile_rec.credit_rating := r.credit_rating;
            v_cust_profile_rec.risk_code := r.risk_code;
            v_cust_profile_rec.standard_terms :=
               NVL (p_payment_term_id, r.standard_terms);            --*******
            v_cust_profile_rec.override_terms := r.override_terms;
            v_cust_profile_rec.dunning_letter_set_id :=
               r.dunning_letter_set_id;
            v_cust_profile_rec.interest_period_days := r.interest_period_days;
            v_cust_profile_rec.payment_grace_days := r.payment_grace_days;
            v_cust_profile_rec.discount_grace_days := r.discount_grace_days;
            v_cust_profile_rec.statement_cycle_id := r.statement_cycle_id;
            v_cust_profile_rec.account_status :=
               NVL (p_account_status, r.account_status);             --*******
            v_cust_profile_rec.percent_collectable := r.percent_collectable;
            v_cust_profile_rec.autocash_hierarchy_id :=
               r.autocash_hierarchy_id;
            v_cust_profile_rec.attribute_category := r.attribute_category;
            v_cust_profile_rec.attribute1 := r.attribute1;
            v_cust_profile_rec.attribute2 :=
               NVL (p_remmit_to_code, r.attribute2);                 --*******
            v_cust_profile_rec.attribute3 := r.attribute3;
            v_cust_profile_rec.attribute4 := r.attribute4;
            v_cust_profile_rec.attribute5 := r.attribute5;
            v_cust_profile_rec.attribute6 := r.attribute6;
            v_cust_profile_rec.attribute7 := r.attribute7;
            v_cust_profile_rec.attribute8 := r.attribute8;
            v_cust_profile_rec.attribute9 := r.attribute9;
            v_cust_profile_rec.attribute10 := r.attribute10;
            v_cust_profile_rec.attribute11 := r.attribute11;
            v_cust_profile_rec.attribute12 := r.attribute12;
            v_cust_profile_rec.attribute13 := r.attribute13;
            v_cust_profile_rec.attribute14 := r.attribute14;
            v_cust_profile_rec.attribute15 := r.attribute15;
            v_cust_profile_rec.auto_rec_incl_disputed_flag :=
               r.auto_rec_incl_disputed_flag;
            v_cust_profile_rec.tax_printing_option := r.tax_printing_option;
            v_cust_profile_rec.charge_on_finance_charge_flag :=
               r.charge_on_finance_charge_flag;
            v_cust_profile_rec.grouping_rule_id := r.grouping_rule_id;
            v_cust_profile_rec.clearing_days := r.clearing_days;
            v_cust_profile_rec.jgzz_attribute_category :=
               r.jgzz_attribute_category;
            v_cust_profile_rec.jgzz_attribute1 := r.jgzz_attribute1;
            v_cust_profile_rec.jgzz_attribute2 := r.jgzz_attribute2;
            v_cust_profile_rec.jgzz_attribute3 := r.jgzz_attribute3;
            v_cust_profile_rec.jgzz_attribute4 := r.jgzz_attribute4;
            v_cust_profile_rec.jgzz_attribute5 := r.jgzz_attribute5;
            v_cust_profile_rec.jgzz_attribute6 := r.jgzz_attribute6;
            v_cust_profile_rec.jgzz_attribute7 := r.jgzz_attribute7;
            v_cust_profile_rec.jgzz_attribute8 := r.jgzz_attribute8;
            v_cust_profile_rec.jgzz_attribute9 := r.jgzz_attribute9;
            v_cust_profile_rec.jgzz_attribute10 := r.jgzz_attribute10;
            v_cust_profile_rec.jgzz_attribute11 := r.jgzz_attribute11;
            v_cust_profile_rec.jgzz_attribute12 := r.jgzz_attribute12;
            v_cust_profile_rec.jgzz_attribute13 := r.jgzz_attribute13;
            v_cust_profile_rec.jgzz_attribute14 := r.jgzz_attribute14;
            v_cust_profile_rec.jgzz_attribute15 := r.jgzz_attribute15;
            v_cust_profile_rec.global_attribute1 := r.global_attribute1;
            v_cust_profile_rec.global_attribute2 := r.global_attribute2;
            v_cust_profile_rec.global_attribute3 := r.global_attribute3;
            v_cust_profile_rec.global_attribute4 := r.global_attribute4;
            v_cust_profile_rec.global_attribute5 := r.global_attribute5;
            v_cust_profile_rec.global_attribute6 := r.global_attribute6;
            v_cust_profile_rec.global_attribute7 := r.global_attribute7;
            v_cust_profile_rec.global_attribute8 := r.global_attribute8;
            v_cust_profile_rec.global_attribute9 := r.global_attribute9;
            v_cust_profile_rec.global_attribute10 := r.global_attribute10;
            v_cust_profile_rec.global_attribute11 := r.global_attribute11;
            v_cust_profile_rec.global_attribute12 := r.global_attribute12;
            v_cust_profile_rec.global_attribute13 := r.global_attribute13;
            v_cust_profile_rec.global_attribute14 := r.global_attribute14;
            v_cust_profile_rec.global_attribute15 := r.global_attribute15;
            v_cust_profile_rec.global_attribute16 := r.global_attribute16;
            v_cust_profile_rec.global_attribute17 := r.global_attribute17;
            v_cust_profile_rec.global_attribute18 := r.global_attribute18;
            v_cust_profile_rec.global_attribute19 := r.global_attribute19;
            v_cust_profile_rec.global_attribute20 := r.global_attribute20;
            v_cust_profile_rec.global_attribute_category :=
               r.global_attribute_category;
            v_cust_profile_rec.cons_inv_flag := r.cons_inv_flag;
            v_cust_profile_rec.cons_inv_type := r.cons_inv_type;
            v_cust_profile_rec.autocash_hierarchy_id_for_adr :=
               r.autocash_hierarchy_id_for_adr;
            v_cust_profile_rec.lockbox_matching_option :=
               r.lockbox_matching_option;
            v_cust_profile_rec.created_by_module := r.created_by_module;
            v_cust_profile_rec.application_id := r.application_id;
            v_cust_profile_rec.review_cycle := r.review_cycle;
            v_cust_profile_rec.last_credit_review_date :=
               r.last_credit_review_date;
            v_cust_profile_rec.party_id := r.party_id;
            v_cust_profile_rec.credit_classification :=
               NVL (p_credit_class_code, r.credit_classification);   --*******
            v_cust_profile_rec.cons_bill_level := r.cons_bill_level;
            v_cust_profile_rec.late_charge_calculation_trx :=
               r.late_charge_calculation_trx;
            v_cust_profile_rec.credit_items_flag := r.credit_items_flag;
            v_cust_profile_rec.disputed_transactions_flag :=
               r.disputed_transactions_flag;
            v_cust_profile_rec.late_charge_type := r.late_charge_type;
            v_cust_profile_rec.late_charge_term_id := r.late_charge_term_id;
            v_cust_profile_rec.interest_calculation_period :=
               r.interest_calculation_period;
            v_cust_profile_rec.hold_charged_invoices_flag :=
               r.hold_charged_invoices_flag;
            v_cust_profile_rec.message_text_id := r.message_text_id;
            v_cust_profile_rec.multiple_interest_rates_flag :=
               r.multiple_interest_rates_flag;
            v_cust_profile_rec.charge_begin_date := r.charge_begin_date;
            v_cust_profile_rec.automatch_set_id := r.automatch_set_id;
            v_object_version_number := r.object_version_number;

            hz_customer_profile_v2pub.update_customer_profile (
               p_customer_profile_rec    => v_cust_profile_rec,
               p_object_version_number   => v_object_version_number,
               x_return_status           => v_return_status,
               x_msg_count               => v_msg_count,
               x_msg_data                => v_msg_data);
            --  DBMS_OUTPUT.put_line ('v_return_status ' || v_return_status);
            --  DBMS_OUTPUT.put_line ('v_msg_count ' || v_msg_count);
            --  DBMS_OUTPUT.put_line ('v_msg_data ' || v_msg_data);

            l_running_messaget := NULL;

            IF v_msg_count > 0 AND v_return_status <> 'S'
            THEN
               v_running_message1 := '';

               FOR i IN 1 .. v_msg_count
               LOOP
                  fnd_msg_pub.get (i,
                                   fnd_api.g_false,
                                   v_msg_data,
                                   l_msg_dummy);

                  IF NVL (v_running_message1, 'null') <> v_msg_data
                  THEN
                     --   DBMS_OUTPUT.put_line (SUBSTR ('Msg' || TO_CHAR (i) || ': ' || v_msg_data, 1, 255));
                     l_running_messaget :=
                           l_running_messaget
                        || ' '
                        || SUBSTR (
                              'Msg' || TO_CHAR (i) || ': ' || v_msg_data,
                              1,
                              255);
                  END IF;

                  v_running_message1 := v_msg_data;
               END LOOP;
            END IF;
         END LOOP;
      END IF;

      --********************
      BEGIN
         IF p_site_uses_rowid IS NULL AND p_level_flag = 'P'
         THEN
            SELECT profile_class_id
              INTO v_after_profile_class_id
              FROM xxwc_customer_attr_update
             WHERE prof_rowid = p_prof_rowid AND level_flag = 'P';
         ELSIF p_site_uses_rowid IS NOT NULL AND p_level_flag = 'P'
         THEN
            SELECT profile_class_id
              INTO v_after_profile_class_id
              FROM xxwc_customer_attr_update
             WHERE site_uses_rowid = p_site_uses_rowid AND level_flag = 'P';
         END IF;
      END;

      l_running_messaget := NULL;

      IF     v_after_profile_class_id = 0
         AND v_before_profile_class_id <> 0
         AND p_level_flag = 'P'
      THEN
         xxwc_update_prifile_class (
            px_prof_rowid          => p_prof_rowid,
            px_profile_class_id    => v_before_profile_class_id,
            px_cust_rowid          => NULL,
            px_site_uses_rowid     => p_site_uses_rowid,
            px_last_updated_by     => p_last_updated_by,
            px_last_update_date    => p_last_update_date,
            px_last_update_login   => p_last_update_login,
            px_responsibility_id   => p_responsibility_id,
            px_org_id              => p_org_id);
      END IF;

      keep_profile_amounts_asbefore (p_prof_rowid);

      IF l_running_messaget IS NOT NULL
      THEN
         UPDATE xxwc_account_mass_update_list
            SET error_message = l_running_messaget
          WHERE table_rowid = p_prof_rowid AND run_id = l_run_id;
      END IF;

      l_running_messaget := NULL;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_running_messaget := NULL;
         l_running_messaget :=
               'Error_Stack...XXWC_UPDATE_CUSTOMER_PROFILE'
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();

         IF l_running_messaget IS NOT NULL
         THEN
            UPDATE xxwc_account_mass_update_list
               SET error_message = l_running_messaget
             WHERE table_rowid = p_prof_rowid AND run_id = l_run_id;
         END IF;
   END xxwc_update_customer_profile;

   --*****************************************************************************************

   PROCEDURE xxwc_update_prifile_class (px_prof_rowid           VARCHAR2,
                                        px_profile_class_id     NUMBER,
                                        px_cust_rowid           VARCHAR2,
                                        px_site_uses_rowid      VARCHAR2,
                                        px_last_updated_by      NUMBER,
                                        px_last_update_date     DATE,
                                        px_last_update_login    NUMBER,
                                        px_responsibility_id    NUMBER,
                                        px_org_id               NUMBER)
   IS
      v_cust_profile_rec        hz_customer_profile_v2pub.customer_profile_rec_type;
      v_object_version_number   NUMBER;
      v_return_status           VARCHAR2 (1024);
      v_msg_count               NUMBER;
      v_msg_data                VARCHAR2 (1024);
      v_running_message1        VARCHAR2 (1024);
      l_msg_dummy               VARCHAR2 (1024);
   BEGIN
      l_running_messaget := NULL;

      FOR r IN (SELECT *
                  FROM hz_customer_profiles
                 WHERE ROWID = px_prof_rowid)
      LOOP
         v_cust_profile_rec.cust_account_profile_id :=
            r.cust_account_profile_id;
         v_cust_profile_rec.cust_account_id := r.cust_account_id;
         v_cust_profile_rec.status := r.status;
         v_cust_profile_rec.collector_id := r.collector_id;          --*******
         v_cust_profile_rec.credit_analyst_id := r.credit_analyst_id; --*******
         v_cust_profile_rec.credit_checking := r.credit_checking;    --*******
         v_cust_profile_rec.next_credit_review_date :=
            r.next_credit_review_date;
         v_cust_profile_rec.tolerance := r.tolerance;
         v_cust_profile_rec.discount_terms := r.discount_terms;
         v_cust_profile_rec.dunning_letters := r.dunning_letters;
         v_cust_profile_rec.interest_charges := r.interest_charges;
         v_cust_profile_rec.send_statements := r.send_statements;
         v_cust_profile_rec.credit_balance_statements :=
            r.credit_balance_statements;
         v_cust_profile_rec.credit_hold := r.credit_hold;            --*******
         v_cust_profile_rec.profile_class_id :=
            NVL (px_profile_class_id, r.profile_class_id);           --*******
         v_cust_profile_rec.site_use_id := r.site_use_id;
         v_cust_profile_rec.credit_rating := r.credit_rating;
         v_cust_profile_rec.risk_code := r.risk_code;
         v_cust_profile_rec.standard_terms := r.standard_terms;      --*******
         v_cust_profile_rec.override_terms := r.override_terms;
         v_cust_profile_rec.dunning_letter_set_id := r.dunning_letter_set_id;
         v_cust_profile_rec.interest_period_days := r.interest_period_days;
         v_cust_profile_rec.payment_grace_days := r.payment_grace_days;
         v_cust_profile_rec.discount_grace_days := r.discount_grace_days;
         v_cust_profile_rec.statement_cycle_id := r.statement_cycle_id;
         v_cust_profile_rec.account_status := r.account_status;      --*******
         v_cust_profile_rec.percent_collectable := r.percent_collectable;
         v_cust_profile_rec.autocash_hierarchy_id := r.autocash_hierarchy_id;
         v_cust_profile_rec.attribute_category := r.attribute_category;
         v_cust_profile_rec.attribute1 := r.attribute1;
         v_cust_profile_rec.attribute2 := r.attribute2;              --*******
         v_cust_profile_rec.attribute3 := r.attribute3;
         v_cust_profile_rec.attribute4 := r.attribute4;
         v_cust_profile_rec.attribute5 := r.attribute5;
         v_cust_profile_rec.attribute6 := r.attribute6;
         v_cust_profile_rec.attribute7 := r.attribute7;
         v_cust_profile_rec.attribute8 := r.attribute8;
         v_cust_profile_rec.attribute9 := r.attribute9;
         v_cust_profile_rec.attribute10 := r.attribute10;
         v_cust_profile_rec.attribute11 := r.attribute11;
         v_cust_profile_rec.attribute12 := r.attribute12;
         v_cust_profile_rec.attribute13 := r.attribute13;
         v_cust_profile_rec.attribute14 := r.attribute14;
         v_cust_profile_rec.attribute15 := r.attribute15;
         v_cust_profile_rec.auto_rec_incl_disputed_flag :=
            r.auto_rec_incl_disputed_flag;
         v_cust_profile_rec.tax_printing_option := r.tax_printing_option;
         v_cust_profile_rec.charge_on_finance_charge_flag :=
            r.charge_on_finance_charge_flag;
         v_cust_profile_rec.grouping_rule_id := r.grouping_rule_id;
         v_cust_profile_rec.clearing_days := r.clearing_days;
         v_cust_profile_rec.jgzz_attribute_category :=
            r.jgzz_attribute_category;
         v_cust_profile_rec.jgzz_attribute1 := r.jgzz_attribute1;
         v_cust_profile_rec.jgzz_attribute2 := r.jgzz_attribute2;
         v_cust_profile_rec.jgzz_attribute3 := r.jgzz_attribute3;
         v_cust_profile_rec.jgzz_attribute4 := r.jgzz_attribute4;
         v_cust_profile_rec.jgzz_attribute5 := r.jgzz_attribute5;
         v_cust_profile_rec.jgzz_attribute6 := r.jgzz_attribute6;
         v_cust_profile_rec.jgzz_attribute7 := r.jgzz_attribute7;
         v_cust_profile_rec.jgzz_attribute8 := r.jgzz_attribute8;
         v_cust_profile_rec.jgzz_attribute9 := r.jgzz_attribute9;
         v_cust_profile_rec.jgzz_attribute10 := r.jgzz_attribute10;
         v_cust_profile_rec.jgzz_attribute11 := r.jgzz_attribute11;
         v_cust_profile_rec.jgzz_attribute12 := r.jgzz_attribute12;
         v_cust_profile_rec.jgzz_attribute13 := r.jgzz_attribute13;
         v_cust_profile_rec.jgzz_attribute14 := r.jgzz_attribute14;
         v_cust_profile_rec.jgzz_attribute15 := r.jgzz_attribute15;
         v_cust_profile_rec.global_attribute1 := r.global_attribute1;
         v_cust_profile_rec.global_attribute2 := r.global_attribute2;
         v_cust_profile_rec.global_attribute3 := r.global_attribute3;
         v_cust_profile_rec.global_attribute4 := r.global_attribute4;
         v_cust_profile_rec.global_attribute5 := r.global_attribute5;
         v_cust_profile_rec.global_attribute6 := r.global_attribute6;
         v_cust_profile_rec.global_attribute7 := r.global_attribute7;
         v_cust_profile_rec.global_attribute8 := r.global_attribute8;
         v_cust_profile_rec.global_attribute9 := r.global_attribute9;
         v_cust_profile_rec.global_attribute10 := r.global_attribute10;
         v_cust_profile_rec.global_attribute11 := r.global_attribute11;
         v_cust_profile_rec.global_attribute12 := r.global_attribute12;
         v_cust_profile_rec.global_attribute13 := r.global_attribute13;
         v_cust_profile_rec.global_attribute14 := r.global_attribute14;
         v_cust_profile_rec.global_attribute15 := r.global_attribute15;
         v_cust_profile_rec.global_attribute16 := r.global_attribute16;
         v_cust_profile_rec.global_attribute17 := r.global_attribute17;
         v_cust_profile_rec.global_attribute18 := r.global_attribute18;
         v_cust_profile_rec.global_attribute19 := r.global_attribute19;
         v_cust_profile_rec.global_attribute20 := r.global_attribute20;
         v_cust_profile_rec.global_attribute_category :=
            r.global_attribute_category;
         v_cust_profile_rec.cons_inv_flag := r.cons_inv_flag;
         v_cust_profile_rec.cons_inv_type := r.cons_inv_type;
         v_cust_profile_rec.autocash_hierarchy_id_for_adr :=
            r.autocash_hierarchy_id_for_adr;
         v_cust_profile_rec.lockbox_matching_option :=
            r.lockbox_matching_option;
         v_cust_profile_rec.created_by_module := r.created_by_module;
         v_cust_profile_rec.application_id := r.application_id;
         v_cust_profile_rec.review_cycle := r.review_cycle;
         v_cust_profile_rec.last_credit_review_date :=
            r.last_credit_review_date;
         v_cust_profile_rec.party_id := r.party_id;
         v_cust_profile_rec.credit_classification := r.credit_classification; --*******
         v_cust_profile_rec.cons_bill_level := r.cons_bill_level;
         v_cust_profile_rec.late_charge_calculation_trx :=
            r.late_charge_calculation_trx;
         v_cust_profile_rec.credit_items_flag := r.credit_items_flag;
         v_cust_profile_rec.disputed_transactions_flag :=
            r.disputed_transactions_flag;
         v_cust_profile_rec.late_charge_type := r.late_charge_type;
         v_cust_profile_rec.late_charge_term_id := r.late_charge_term_id;
         v_cust_profile_rec.interest_calculation_period :=
            r.interest_calculation_period;
         v_cust_profile_rec.hold_charged_invoices_flag :=
            r.hold_charged_invoices_flag;
         v_cust_profile_rec.message_text_id := r.message_text_id;
         v_cust_profile_rec.multiple_interest_rates_flag :=
            r.multiple_interest_rates_flag;
         v_cust_profile_rec.charge_begin_date := r.charge_begin_date;
         v_cust_profile_rec.automatch_set_id := r.automatch_set_id;
         v_object_version_number := r.object_version_number;

         hz_customer_profile_v2pub.update_customer_profile (
            p_customer_profile_rec    => v_cust_profile_rec,
            p_object_version_number   => v_object_version_number,
            x_return_status           => v_return_status,
            x_msg_count               => v_msg_count,
            x_msg_data                => v_msg_data);
         DBMS_OUTPUT.put_line ('v_return_status ' || v_return_status);
         DBMS_OUTPUT.put_line ('v_msg_count ' || v_msg_count);
         DBMS_OUTPUT.put_line ('v_msg_data ' || v_msg_data);

         IF v_msg_count > 0 AND v_return_status <> 'S'
         THEN
            v_running_message1 := '';

            FOR i IN 1 .. v_msg_count
            LOOP
               fnd_msg_pub.get (i,
                                fnd_api.g_false,
                                v_msg_data,
                                l_msg_dummy);

               IF NVL (v_running_message1, 'null') <> v_msg_data
               THEN
                  DBMS_OUTPUT.put_line (
                     SUBSTR ('Msg' || TO_CHAR (i) || ': ' || v_msg_data,
                             1,
                             255));
                  l_running_messaget :=
                        l_running_messaget
                     || ' '
                     || SUBSTR ('Msg' || TO_CHAR (i) || ': ' || v_msg_data,
                                1,
                                255);
               END IF;

               v_running_message1 := v_msg_data;
            END LOOP;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_running_messaget := NULL;
         l_running_messaget :=
               'Error_Stack...update default profile class'
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();

         IF l_running_messaget IS NOT NULL
         THEN
            UPDATE xxwc_account_mass_update_list
               SET error_message = l_running_messaget
             WHERE table_rowid = px_prof_rowid AND run_id = l_run_id;
         END IF;
   END xxwc_update_prifile_class;

   --*****************************************************************************************

   --*****************************************************************************************

   --*****************************************************************************************
   --*****************************************************************************************

   PROCEDURE xxwc_update_cust_site_uses (p_site_rowid           VARCHAR2,
                                         p_salesrep_id          NUMBER,
                                         p_freight_term         VARCHAR2,
                                         p_last_updated_by      NUMBER,
                                         p_last_update_date     DATE,
                                         p_last_update_login    NUMBER,
                                         p_responsibility_id    NUMBER,
                                         p_org_id               NUMBER)
   IS
      v_cust_site_use_rec       hz_cust_account_site_v2pub.cust_site_use_rec_type;
      v_object_version_number   NUMBER;
      v_return_status           VARCHAR2 (1024);
      v_msg_count               NUMBER;
      v_msg_data                VARCHAR2 (1024);
      v_running_message1        VARCHAR2 (1024);
      l_msg_dummy               VARCHAR2 (1024);

      l_errbuf                  CLOB;
   BEGIN
      l_running_messaget := NULL;

      FOR r IN (SELECT *
                  FROM apps.hz_cust_site_uses
                 WHERE ROWID = p_site_rowid)
      LOOP
         v_cust_site_use_rec.site_use_id := r.site_use_id;
         v_cust_site_use_rec.cust_acct_site_id := r.cust_acct_site_id;
         v_cust_site_use_rec.site_use_code := r.site_use_code;
         v_cust_site_use_rec.primary_flag := r.primary_flag;
         v_cust_site_use_rec.status := r.status;
         v_cust_site_use_rec.location := r.location;
         v_cust_site_use_rec.contact_id := r.contact_id;
         v_cust_site_use_rec.bill_to_site_use_id := r.bill_to_site_use_id;
         v_cust_site_use_rec.orig_system_reference := r.orig_system_reference;
         v_cust_site_use_rec.orig_system := NULL;
         v_cust_site_use_rec.sic_code := r.sic_code;
         v_cust_site_use_rec.payment_term_id := r.payment_term_id;
         v_cust_site_use_rec.gsa_indicator := r.gsa_indicator;
         v_cust_site_use_rec.ship_partial := r.ship_partial;
         v_cust_site_use_rec.ship_via := r.ship_via;
         v_cust_site_use_rec.fob_point := r.fob_point;
         v_cust_site_use_rec.order_type_id := r.order_type_id;
         v_cust_site_use_rec.price_list_id := r.price_list_id;
         v_cust_site_use_rec.freight_term :=
            NVL (p_freight_term, r.freight_term);
         v_cust_site_use_rec.warehouse_id := r.warehouse_id;
         v_cust_site_use_rec.territory_id := r.territory_id;
         v_cust_site_use_rec.attribute_category := r.attribute_category;
         v_cust_site_use_rec.attribute1 := r.attribute1;
         v_cust_site_use_rec.attribute2 := r.attribute2;
         v_cust_site_use_rec.attribute3 := r.attribute3;
         v_cust_site_use_rec.attribute4 := r.attribute4;
         v_cust_site_use_rec.attribute5 := r.attribute5;
         v_cust_site_use_rec.attribute6 := r.attribute6;
         v_cust_site_use_rec.attribute7 := r.attribute7;
         v_cust_site_use_rec.attribute8 := r.attribute8;
         v_cust_site_use_rec.attribute9 := r.attribute9;
         v_cust_site_use_rec.attribute10 := r.attribute10;
         v_cust_site_use_rec.tax_reference := r.tax_reference;
         v_cust_site_use_rec.sort_priority := r.sort_priority;
         v_cust_site_use_rec.tax_code := r.tax_code;
         v_cust_site_use_rec.attribute11 := r.attribute11;
         v_cust_site_use_rec.attribute12 := r.attribute12;
         v_cust_site_use_rec.attribute13 := r.attribute13;
         v_cust_site_use_rec.attribute14 := r.attribute14;
         v_cust_site_use_rec.attribute15 := r.attribute15;
         v_cust_site_use_rec.attribute16 := r.attribute16;
         v_cust_site_use_rec.attribute17 := r.attribute17;
         v_cust_site_use_rec.attribute18 := r.attribute18;
         v_cust_site_use_rec.attribute19 := r.attribute19;
         v_cust_site_use_rec.attribute20 := r.attribute20;
         v_cust_site_use_rec.attribute21 := r.attribute21;
         v_cust_site_use_rec.attribute22 := r.attribute22;
         v_cust_site_use_rec.attribute23 := r.attribute23;
         v_cust_site_use_rec.attribute24 := r.attribute24;
         v_cust_site_use_rec.attribute25 := r.attribute25;
         v_cust_site_use_rec.demand_class_code := r.demand_class_code;
         v_cust_site_use_rec.tax_header_level_flag := r.tax_header_level_flag;
         v_cust_site_use_rec.tax_rounding_rule := r.tax_rounding_rule;
         v_cust_site_use_rec.global_attribute1 := r.global_attribute1;
         v_cust_site_use_rec.global_attribute2 := r.global_attribute2;
         v_cust_site_use_rec.global_attribute3 := r.global_attribute3;
         v_cust_site_use_rec.global_attribute4 := r.global_attribute4;
         v_cust_site_use_rec.global_attribute5 := r.global_attribute5;
         v_cust_site_use_rec.global_attribute6 := r.global_attribute6;
         v_cust_site_use_rec.global_attribute7 := r.global_attribute7;
         v_cust_site_use_rec.global_attribute8 := r.global_attribute8;
         v_cust_site_use_rec.global_attribute9 := r.global_attribute9;
         v_cust_site_use_rec.global_attribute10 := r.global_attribute10;
         v_cust_site_use_rec.global_attribute11 := r.global_attribute11;
         v_cust_site_use_rec.global_attribute12 := r.global_attribute12;
         v_cust_site_use_rec.global_attribute13 := r.global_attribute13;
         v_cust_site_use_rec.global_attribute14 := r.global_attribute14;
         v_cust_site_use_rec.global_attribute15 := r.global_attribute15;
         v_cust_site_use_rec.global_attribute16 := r.global_attribute16;
         v_cust_site_use_rec.global_attribute17 := r.global_attribute17;
         v_cust_site_use_rec.global_attribute18 := r.global_attribute18;
         v_cust_site_use_rec.global_attribute19 := r.global_attribute19;
         v_cust_site_use_rec.global_attribute20 := r.global_attribute20;
         v_cust_site_use_rec.global_attribute_category :=
            r.global_attribute_category;
         v_cust_site_use_rec.primary_salesrep_id :=
            NVL (p_salesrep_id, r.primary_salesrep_id);
         v_cust_site_use_rec.finchrg_receivables_trx_id :=
            r.finchrg_receivables_trx_id;
         v_cust_site_use_rec.dates_negative_tolerance :=
            r.dates_negative_tolerance;
         v_cust_site_use_rec.dates_positive_tolerance :=
            r.dates_positive_tolerance;
         v_cust_site_use_rec.date_type_preference := r.date_type_preference;
         v_cust_site_use_rec.over_shipment_tolerance :=
            r.over_shipment_tolerance;
         v_cust_site_use_rec.under_shipment_tolerance :=
            r.under_shipment_tolerance;
         v_cust_site_use_rec.item_cross_ref_pref := r.item_cross_ref_pref;
         v_cust_site_use_rec.over_return_tolerance := r.over_return_tolerance;
         v_cust_site_use_rec.under_return_tolerance :=
            r.under_return_tolerance;
         v_cust_site_use_rec.ship_sets_include_lines_flag :=
            r.ship_sets_include_lines_flag;
         v_cust_site_use_rec.arrivalsets_include_lines_flag :=
            r.arrivalsets_include_lines_flag;
         v_cust_site_use_rec.sched_date_push_flag := r.sched_date_push_flag;
         v_cust_site_use_rec.invoice_quantity_rule := r.invoice_quantity_rule;
         v_cust_site_use_rec.pricing_event := r.pricing_event;
         v_cust_site_use_rec.gl_id_rec := r.gl_id_rec;
         v_cust_site_use_rec.gl_id_rev := r.gl_id_rev;
         v_cust_site_use_rec.gl_id_tax := r.gl_id_tax;
         v_cust_site_use_rec.gl_id_freight := r.gl_id_freight;
         v_cust_site_use_rec.gl_id_clearing := r.gl_id_clearing;
         v_cust_site_use_rec.gl_id_unbilled := r.gl_id_unbilled;
         v_cust_site_use_rec.gl_id_unearned := r.gl_id_unearned;
         v_cust_site_use_rec.gl_id_unpaid_rec := r.gl_id_unpaid_rec;
         v_cust_site_use_rec.gl_id_remittance := r.gl_id_remittance;
         v_cust_site_use_rec.gl_id_factor := r.gl_id_factor;
         v_cust_site_use_rec.tax_classification := r.tax_classification;
         v_cust_site_use_rec.created_by_module := r.created_by_module;
         v_cust_site_use_rec.application_id := r.application_id;
         v_cust_site_use_rec.org_id := r.org_id;
         v_object_version_number := r.object_version_number;
      END LOOP;

      hz_cust_account_site_v2pub.update_cust_site_use (
         p_cust_site_use_rec       => v_cust_site_use_rec,
         p_object_version_number   => v_object_version_number,
         x_return_status           => v_return_status,
         x_msg_count               => v_msg_count,
         x_msg_data                => v_msg_data);

      -- DBMS_OUTPUT.put_line ('v_return_status ' || v_return_status);
      -- DBMS_OUTPUT.put_line ('v_msg_count ' || v_msg_count);
      --  DBMS_OUTPUT.put_line ('v_msg_data ' || v_msg_data);

      l_running_messaget := NULL;

      IF v_msg_count > 0 AND v_return_status <> 'S'
      THEN
         v_running_message1 := '';

         FOR i IN 1 .. v_msg_count
         LOOP
            fnd_msg_pub.get (i,
                             fnd_api.g_false,
                             v_msg_data,
                             l_msg_dummy);

            IF NVL (v_running_message1, 'null') <> v_msg_data
            THEN
               -- DBMS_OUTPUT.put_line (SUBSTR ('Msg' || TO_CHAR (i) || ': ' || v_msg_data, 1, 255));
               l_running_messaget :=
                     l_running_messaget
                  || ' '
                  || SUBSTR ('Msg' || TO_CHAR (i) || ': ' || v_msg_data,
                             1,
                             255);
            END IF;

            v_running_message1 := v_msg_data;
         END LOOP;
      END IF;

      IF l_running_messaget IS NOT NULL
      THEN
         UPDATE xxwc_account_mass_update_list
            SET error_message = l_running_messaget
          WHERE table_rowid = p_site_rowid AND run_id = l_run_id;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_running_messaget := NULL;
         l_running_messaget :=
               'Error_Stack...XXWC_UPDATE_CUST_SITE_USES'
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();

         IF l_running_messaget IS NOT NULL
         THEN
            UPDATE xxwc_account_mass_update_list
               SET error_message = l_running_messaget
             WHERE table_rowid = p_site_rowid AND run_id = l_run_id;
         END IF;
   END xxwc_update_cust_site_uses;

   --*****************************************************************************************
   --*****************************************************************************************
   --*****************************************************************************************
   --*****************************************************************************************
   --*****************************************************************************************
   --*****************************************************************************************

   --*****************************************************************************************
   --*****************************************************************************************

   PROCEDURE clean_log_table
   /*************************************************************************************************************************************
      Procedure: clean_log_table

      PURPOSE:   Cleanning/Updating Log tables

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0
      1.1        05/05/2016  P.Vamshidhar            TMS# 20160407-00192

     --***********************************************************************************************************************************/
   IS
   BEGIN
      -- Below code added in Rev 1.1 start
      apps.xxwc_mv_routines_add_pkg.enable_parallelism;
      -- Above code added in Rev 1.1 end.

      -- EXECUTE IMMEDIATE 'alter table xxwc.xxwc_account_mass_update_list nologging';
      -- EXECUTE IMMEDIATE 'truncate table xxwc.xxwc_account_mass_update_list';

      -- EXECUTE IMMEDIATE 'alter table xxwc.xxwc_account_mass_update_arch nologging';

      ---EXECUTE IMMEDIATE 'alter table xxwc.xxwc_account_mass_update_arch parallel(degree 8)';

      -- EXECUTE IMMEDIATE 'alter table xxwc.xxwc_account_mass_update_list parallel(degree 8)';

      INSERT INTO xxwc.xxwc_account_mass_update_arch
         SELECT * FROM apps.xxwc_account_mass_update_list;

      DELETE FROM xxwc_account_mass_update_arch
            WHERE insert_date < SYSDATE - 180;

      EXECUTE IMMEDIATE 'truncate table xxwc.xxwc_account_mass_update_list';

      -- Below code added in Rev 1.1 start
      apps.xxwc_mv_routines_add_pkg.disable_parallelism;
   -- Above code added in Rev 1.1 end.

   END;

   --*****************************************************************************************
   --*****************************************************************************************

   --*****************************************************************************************
   --*****************************************************************************************
   --*****************************************************************************************
  /*************************************************************************************************************************************

     procedure Name: change_collector_id

     PURPOSE:  change change_collector_id

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
    -- 1.2        6/22/2016   S.Neha                  TMS#20170530-00341 - Improving th eperformance for change collector and credit nalayst procedure
    -- 1.3         8/23/2017   S.neha                  TMS 20170807-00294 --changes done for ver and TMS 20170807-00294
   --*****************************************************************************************/

 PROCEDURE change_collector_id (
      p_errbuf                       OUT VARCHAR2,
      p_retcode                      OUT VARCHAR2,
      p_old_collector_id                 NUMBER,
      p_new_collector_id                 NUMBER,
      p_state                            VARCHAR2,
      p_include_nonactive_customer       VARCHAR2,
      p_account_status                   VARCHAR2)
   IS
      v_last_updated_by             NUMBER := fnd_profile.VALUE ('USER_ID');
      v_last_update_date            DATE := SYSDATE;

      v_responsibility_id           NUMBER := fnd_profile.VALUE ('RESP_ID');
      v_org_id                      NUMBER := fnd_profile.VALUE ('ORG_ID');
      v_last_update_login           NUMBER := fnd_profile.VALUE ('LOGIN_ID');
      v_old_collector_name          VARCHAR2 (164);
      v_new_collector_name          VARCHAR2 (164);
      v_message                     VARCHAR2 (564);

      v_error_count                 NUMBER := 0;
      v_all_count                   NUMBER := 0;
      v_status                      VARCHAR2 (1) := NULL;
      v_account_status              VARCHAR2 (12) := NULL;
      p_customer_profile_rec_type   HZ_CUSTOMER_PROFILE_V2PUB.CUSTOMER_PROFILE_REC_TYPE;
      p_cust_account_profile_id     NUMBER;
      p_object_version_number       NUMBER;
      v_count_rows_before           NUMBER;
      v_count_rows_after            NUMBER;
      x_return_status               VARCHAR2 (2000);
      x_msg_count                   NUMBER;
      x_msg_data                    VARCHAR2 (2000);
      l_counter                     NUMBER := 0;
      l_msg_index_out               NUMBER;
  BEGIN
         --changes starts for ver 1.2

      v_account_status := get_account_status (p_account_status);
      
      IF p_include_nonactive_customer = 'Active Sites Only'
      THEN
         INSERT INTO xxwc.xxwc_customer_attr_temp
            SELECT *
              FROM apps.xxwc_customer_attr_update
             WHERE     collector_id = p_old_collector_id
                   AND state = NVL (p_state, state)
                   AND (site_status IS NULL OR site_status = 'A')
                   AND profile_account_status =
                          NVL (v_account_status, profile_account_status);
      ELSIF p_include_nonactive_customer = 'Inactive Sites Only'
      THEN
         INSERT INTO xxwc.xxwc_customer_attr_temp
            SELECT *
              FROM apps.xxwc_customer_attr_update
             WHERE     collector_id = p_old_collector_id
                   AND state = NVL (p_state, state)
                   AND site_status = 'I'
                   AND profile_account_status =
                          NVL (v_account_status, profile_account_status);
      ELSIF p_include_nonactive_customer = 'All'
      THEN
         INSERT INTO xxwc.xxwc_customer_attr_temp
            SELECT *
              FROM apps.xxwc_customer_attr_update
             WHERE     collector_id = p_old_collector_id
                   AND state = NVL (p_state, state)
                   AND profile_account_status =
                          NVL (v_account_status, profile_account_status);

         fnd_file.put_line (
            fnd_file.LOG,
               'inserted rows - '
            || SQL%ROWCOUNT
            || ' - '
            || TO_CHAR (SYSDATE, 'mm/dd/yyyy hh:mm:ss'));
         COMMIT;
      END IF;

      l_errbuf := NULL;
      l_retcode := '0';


      FOR r IN (SELECT name old_collector_name
                  FROM ar_collectors
                 WHERE collector_id = p_old_collector_id)
      LOOP
         v_old_collector_name := r.old_collector_name;
      END LOOP;


      FOR r IN (SELECT name new_collector_name
                  FROM ar_collectors
                 WHERE collector_id = p_new_collector_id)
      LOOP
         v_new_collector_name := r.new_collector_name;
      END LOOP;



      --clean_log_table;

      v_message :=
            'updating : old collector name: '
         || v_old_collector_name
         || 'to new collector name: '
         || v_new_collector_name
         || ';status='
         || p_include_nonactive_customer
         || ';request_id ='
         || l_req_id
         || 'state='
         || p_state;
      l_run_id := xxwc.xxwc_account_maint_curr_s.NEXTVAL;
      
      fnd_file.put_line ( fnd_file.LOG,v_message);
            
      BEGIN
         SELECT COUNT (1)
           INTO v_count_rows_before
           FROM xxwc.xxwc_customer_attr_temp;

         fnd_file.put_line (
            fnd_file.LOG,
               'v_count_rows_before '
            || v_count_rows_before
            || ' '
            || TO_CHAR (SYSDATE, 'mm/dd/yyyy hh:mm:ss'));
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
                  'Error while fetching v_count_rows_before-'
               || TO_CHAR (SYSDATE, 'mm/dd/yyyy hh:mm:ss'));
            v_count_rows_before := 0;
      END;


      INSERT INTO xxwc.xxwc_account_mass_update_hist (change,
                                                      run_id,
                                                      change_name,
                                                      old_name,
                                                      new_name,
                                                      old_id,
                                                      new_id,
                                                      records_count_before,
                                                      records_count_after,
                                                      last_updated_by,
                                                      last_update_date,
                                                      responsibility_id,
                                                      org_id)
           VALUES (v_message,
                   l_run_id,
                   'update collector',
                   v_old_collector_name,
                   v_new_collector_name,
                   p_old_collector_id,
                   p_new_collector_id,
                   v_count_rows_before,
                   NULL,
                   v_last_updated_by,
                   v_last_update_date,
                   v_responsibility_id,
                   v_org_id);

      COMMIT;
     -- reset counters

      v_count_rows_after := 0;
      l_counter := 0;
      v_error_count :=0;
      --changes done for ver 1.3 and TMS 20170807-00294
      FOR rec_cust_prof
         IN (SELECT DISTINCT stg.cust_account_profile_id,
                    stg.collector_id,
                    stg.credit_analyst_id,
                    hcp.object_version_number
               FROM xxwc.xxwc_customer_attr_temp stg, --changes done for ver1.3 and TMS 20170807-00294
                    hz_customer_profiles hcp
              WHERE     1 = 1
                    AND stg.cust_account_profile_id =
                           hcp.cust_account_profile_id)
      LOOP
               
         l_counter := l_counter + 1;
         p_customer_profile_rec_type.cust_account_profile_id :=
            rec_cust_prof.cust_account_profile_id; -- Documentation on using TCA APIs - V2 Page 73
         p_customer_profile_rec_type.collector_id := p_new_collector_id;
         -- p_customer_profile_rec_type.credit_analyst_id       := rec_cust_prof.credit_analyst_id;
         p_object_version_number := rec_cust_prof.object_version_number;

         --API Call
         hz_customer_profile_v2pub.update_customer_profile (
            p_init_msg_list           => fnd_api.g_true,
            p_customer_profile_rec    => p_customer_profile_rec_type,
            p_object_version_number   => p_object_version_number,
            x_return_status           => x_return_status,
            x_msg_count               => x_msg_count,
            x_msg_data                => x_msg_data);



         IF x_return_status = FND_API.G_RET_STS_ERROR
         THEN
            v_error_count:=v_error_count+1;
            FOR I IN 1 .. x_msg_count
            LOOP
               fnd_file.put_line (
                  fnd_file.LOG,
                     'error '
                  || SUBSTR (FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE),
                             1,
                             255)
                  || x_msg_data);
            END LOOP;
         ELSIF x_return_status = FND_API.G_RET_STS_SUCCESS
         THEN
            v_count_rows_after := v_count_rows_after + 1;

         END IF;
      END LOOP;                                               -- rec_cust_prof

      fnd_file.put_line (
         fnd_file.LOG,
            'End of API- success records  '
         || v_count_rows_after
         || ' - l_counter records- '
         || l_counter);
         
         fnd_file.put_line (
         fnd_file.LOG,
            'End of API- error records  '
         || v_error_count);
         
      COMMIT;


      BEGIN
         UPDATE xxwc.xxwc_account_mass_update_hist
            SET records_count_after = v_count_rows_after
          WHERE run_id = l_run_id;

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
                  'Error while updating v_count_rows_after - '
               || v_count_rows_after
               || ' '
               || TO_CHAR (SYSDATE, 'mm/dd/yyyy hh:mm:ss'));
      END;

      p_errbuf := l_errbuf;
      p_retcode := l_retcode;

      COMMIT;
      --changes ends for ver 1.2
   EXCEPTION
      WHEN OTHERS
      THEN
         l_running_messaget := NULL;
         l_running_messaget :=
               'Error_Stack... '
            || v_message
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();
         l_errbuf := l_running_messaget;
         l_retcode := '2';
         p_errbuf := l_errbuf;
         p_retcode := l_retcode;
         fnd_file.put_line (fnd_file.LOG, 'error ' || l_running_messaget);
         clean_log_table;
         COMMIT;
   END;

   --*****************************************************************************************
   --*****************************************************************************************
   --*****************************************************************************************
   --*****************************************************************************************
   --*****************************************************************************************
   --*****************************************************************************************
  /*************************************************************************************************************************************

     procedure Name: change_credit_analyst_id

     PURPOSE:  change change_credit_analyst_id

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
    -- 1.2        6/22/2016   S.Neha                  TMS#20170530-00341 - Improving th eperformance for change collector and credit nalayst procedure
   -- 1.3         8/23/2017   S.neha                  TMS 20170807-00294 --changes done for ver and TMS 20170807-00294
   --*****************************************************************************************/
   PROCEDURE change_credit_analyst_id (
      p_errbuf                       OUT VARCHAR2,
      p_retcode                      OUT VARCHAR2,
      p_old_credit_analyst_id            NUMBER,
      p_new_credit_analyst_id            NUMBER,
      p_state                            VARCHAR2,
      p_include_nonactive_customer       VARCHAR2,
      p_account_status                   VARCHAR2)
   IS
      v_last_updated_by           NUMBER := fnd_profile.VALUE ('USER_ID');
      v_last_update_date          DATE := SYSDATE;

      v_responsibility_id         NUMBER := fnd_profile.VALUE ('RESP_ID');
      v_org_id                    NUMBER := fnd_profile.VALUE ('ORG_ID');
      v_last_update_login         NUMBER := fnd_profile.VALUE ('LOGIN_ID');
      v_old_credit_analyst_name   VARCHAR2 (164);
      v_new_credit_analyst_name   VARCHAR2 (164);
      v_message                   VARCHAR2 (564);
      v_records_count_before      NUMBER;
      v_records_count_after       NUMBER;
      v_error_count               NUMBER := 0;
      v_all_count                 NUMBER := 0;
      v_status                    VARCHAR2 (1) := NULL;
      v_records_count_old         NUMBER;
      v_records_count_new         NUMBER;
      v_account_status            VARCHAR2 (12) := NULL;
    --   v_message                     VARCHAR2 (564);

    --  v_error_count                 NUMBER := 0;
      v_all_count                   NUMBER := 0;
      v_status                      VARCHAR2 (1) := NULL;
     -- v_account_status              VARCHAR2 (12) := NULL;
      p_customer_profile_rec_type   HZ_CUSTOMER_PROFILE_V2PUB.CUSTOMER_PROFILE_REC_TYPE;
      p_cust_account_profile_id     NUMBER;
      p_object_version_number       NUMBER;
      v_count_rows_before           NUMBER;
      v_count_rows_after            NUMBER;
      x_return_status               VARCHAR2 (2000);
      x_msg_count                   NUMBER;
      x_msg_data                    VARCHAR2 (2000);
      l_counter                     NUMBER := 0;
      l_msg_index_out               NUMBER;
   BEGIN
      --changes starts for ver 1.2
      v_account_status := get_account_status (p_account_status);
      l_errbuf := NULL;
      l_retcode := '0';

      --insert into temporary table
      IF p_include_nonactive_customer = 'Active Sites Only'
      THEN
         INSERT INTO xxwc.xxwc_customer_attr_temp
            SELECT *
              FROM apps.xxwc_customer_attr_update
             WHERE     credit_analyst_id = p_old_credit_analyst_id
                   AND state = NVL (p_state, state)
                   AND (site_status IS NULL OR site_status = 'A')
                   AND profile_account_status =
                          NVL (v_account_status, profile_account_status);
      ELSIF p_include_nonactive_customer = 'Inactive Sites Only'
      THEN
         INSERT INTO xxwc.xxwc_customer_attr_temp
            SELECT *
              FROM apps.xxwc_customer_attr_update
             WHERE     credit_analyst_id = p_old_credit_analyst_id
                   AND state = NVL (p_state, state)
                   AND site_status = 'I'
                   AND profile_account_status =
                          NVL (v_account_status, profile_account_status);
      ELSIF p_include_nonactive_customer = 'All'
      THEN
         INSERT INTO xxwc.xxwc_customer_attr_temp
            SELECT *
              FROM apps.xxwc_customer_attr_update
             WHERE     credit_analyst_id = p_old_credit_analyst_id
                   AND state = NVL (p_state, state)
                   AND profile_account_status =
                          NVL (v_account_status, profile_account_status);
      END IF;

      l_run_id := xxwc.xxwc_account_maint_curr_s.NEXTVAL;
      v_old_credit_analyst_name := credit_analyst (p_old_credit_analyst_id);
      v_new_credit_analyst_name := credit_analyst (p_new_credit_analyst_id);

     -- clean_log_table;
      v_message :=
            'updating : old credit analyst name: '
         || v_old_credit_analyst_name
         || 'to new credit analyst name: '
         || v_new_credit_analyst_name
         || ';status='
         || p_include_nonactive_customer
         || ';request_id ='
         || l_req_id
         || 'state='
         || p_state;
      fnd_file.put_line ( fnd_file.LOG,v_message);
       BEGIN
         SELECT COUNT (1)
           INTO v_count_rows_before
           FROM xxwc.xxwc_customer_attr_temp;

         fnd_file.put_line (
            fnd_file.LOG,
               'v_count_rows_before '
            || v_count_rows_before
            || ' '
            || TO_CHAR (SYSDATE, 'mm/dd/yyyy hh:mm:ss'));
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
                  'Error while fetching v_count_rows_before-'
               || TO_CHAR (SYSDATE, 'mm/dd/yyyy hh:mm:ss'));
            v_count_rows_before := 0;
      END;

      INSERT INTO xxwc.xxwc_account_mass_update_hist (change,
                                                      run_id,
                                                      change_name,
                                                      old_name,
                                                      new_name,
                                                      old_id,
                                                      new_id,
                                                      records_count_before,
                                                      records_count_after,
                                                      last_updated_by,
                                                      last_update_date,
                                                      responsibility_id,
                                                      org_id)
           VALUES (v_message,
                   l_run_id,
                   'update credit_analyst',
                   v_old_credit_analyst_name,
                   v_new_credit_analyst_name,
                   p_old_credit_analyst_id,
                   p_new_credit_analyst_id,
                   v_count_rows_before,
                   NULL,
                   v_last_updated_by,
                   v_last_update_date,
                   v_responsibility_id,
                   v_org_id);
      -- reset counters

      v_count_rows_after := 0;
      l_counter := 0;
      v_error_count :=0;
      
      --changes done for ver1.3 and TMS 20170807-00294
       FOR rec_cust_prof
         IN (SELECT DISTINCT stg.cust_account_profile_id,
                    stg.collector_id,
                    stg.credit_analyst_id,
                    hcp.object_version_number
               FROM xxwc.xxwc_customer_attr_temp stg,  --changes done for ver1.3 and TMS 20170807-00294
                    apps.hz_customer_profiles hcp      --changes done for ver1.3 and TMS 20170807-00294
              WHERE     1 = 1
                    AND stg.cust_account_profile_id =
                           hcp.cust_account_profile_id)
     LOOP
               
         l_counter := l_counter + 1;
         p_customer_profile_rec_type.cust_account_profile_id := rec_cust_prof.cust_account_profile_id; -- Documentation on using TCA APIs - V2 Page 73
         p_customer_profile_rec_type.credit_analyst_id       := p_new_credit_analyst_id;  --changes done for ver1.3 and TMS 20170807-00294
         p_object_version_number := rec_cust_prof.object_version_number;

         --API Call
         hz_customer_profile_v2pub.update_customer_profile (
            p_init_msg_list           => fnd_api.g_true,
            p_customer_profile_rec    => p_customer_profile_rec_type,
            p_object_version_number   => p_object_version_number,
            x_return_status           => x_return_status,
            x_msg_count               => x_msg_count,
            x_msg_data                => x_msg_data);



         IF x_return_status = FND_API.G_RET_STS_ERROR
         THEN
            v_error_count:=v_error_count+1;
            FOR I IN 1 .. x_msg_count
            LOOP
               fnd_file.put_line (
                  fnd_file.LOG,
                     'error '
                  || SUBSTR (FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE),
                             1,
                             255)
                  || x_msg_data);
            END LOOP;
         ELSIF x_return_status = FND_API.G_RET_STS_SUCCESS
         THEN
            v_count_rows_after := v_count_rows_after + 1;

         END IF;
      END LOOP;      

      fnd_file.put_line (
         fnd_file.LOG,
            'End of API- success records  '
         || v_count_rows_after
         || ' - l_counter records- '
         || l_counter);
         
         fnd_file.put_line (
         fnd_file.LOG,
            'End of API- error records  '
         || v_error_count);
         
      COMMIT;


      BEGIN
         UPDATE xxwc.xxwc_account_mass_update_hist
            SET records_count_after = v_count_rows_after
          WHERE run_id = l_run_id;

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
                  'Error while updating v_count_rows_after - '
               || v_count_rows_after
               || ' '
               || TO_CHAR (SYSDATE, 'mm/dd/yyyy hh:mm:ss'));
      END;
      p_errbuf := l_errbuf;
      p_retcode := l_retcode;

      --changes ends for ver 1.2
      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_running_messaget := NULL;
         l_running_messaget :=
               'Error_Stack... '
            || v_message
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();
         l_errbuf := l_running_messaget;
         l_retcode := '2';
         p_errbuf := l_errbuf;
         p_retcode := l_retcode;
         DBMS_APPLICATION_INFO.set_module ('', '');
         fnd_file.put_line (fnd_file.LOG, 'error ' || l_running_messaget);
         clean_log_table;
         COMMIT;
   END;

   --*****************************************************************************************
   --*****************************************************************************************
   --*****************************************************************************************
   --*****************************************************************************************
   --*****************************************************************************************
   --*****************************************************************************************
   PROCEDURE change_sales_rep_id (
      p_errbuf                       OUT VARCHAR2,
      p_retcode                      OUT VARCHAR2,
      p_old_sales_rep_id                 NUMBER,
      p_new_sales_rep_id                 NUMBER,
      p_state                            VARCHAR2,
      p_include_nonactive_customer       VARCHAR2,
      p_account_status                   VARCHAR2)
   IS
      v_last_updated_by        NUMBER := fnd_profile.VALUE ('USER_ID');
      v_last_update_date       DATE := SYSDATE;

      v_responsibility_id      NUMBER := fnd_profile.VALUE ('RESP_ID');
      v_org_id                 NUMBER := fnd_profile.VALUE ('ORG_ID');
      v_last_update_login      NUMBER := fnd_profile.VALUE ('LOGIN_ID');
      v_old_sales_rep_name     VARCHAR2 (164);
      v_new_sales_rep_name     VARCHAR2 (164);
      v_message                VARCHAR2 (564);
      v_records_count_before   NUMBER;
      v_records_count_after    NUMBER;
      v_error_count            NUMBER := 0;
      v_all_count              NUMBER := 0;
      v_status                 VARCHAR2 (1) := NULL;
      v_account_status         VARCHAR2 (12) := NULL;
   BEGIN
      v_account_status := get_account_status (p_account_status);
      l_errbuf := NULL;
      l_retcode := '0';
      v_old_sales_rep_name := get_sales_rep (v_org_id, p_old_sales_rep_id);
      v_new_sales_rep_name := get_sales_rep (v_org_id, p_new_sales_rep_id);
      -- DBMS_OUTPUT.put_line ('v_old_sales_rep_name ' || v_old_sales_rep_name);
      -- DBMS_OUTPUT.put_line ('v_new_sales_rep_name ' || v_new_sales_rep_name);

      v_message :=
            'updating : old salesrep name: '
         || v_old_sales_rep_name
         || 'to new salesrep name: '
         || v_new_sales_rep_name
         || ';status='
         || p_include_nonactive_customer
         || ';request_id ='
         || l_req_id
         || 'state='
         || p_state;
      l_run_id := xxwc.xxwc_account_maint_curr_s.NEXTVAL;
      clean_log_table;

      IF p_include_nonactive_customer = 'Active Sites Only'
      THEN
         INSERT INTO xxwc.xxwc_customer_attr_temp
            SELECT *
              FROM apps.xxwc_customer_attr_update
             WHERE     primary_salesrep_id = p_old_sales_rep_id
                   AND state = NVL (p_state, state)
                   AND (site_status IS NULL OR site_status = 'A')
                   AND profile_account_status =
                          NVL (v_account_status, profile_account_status);
      ELSIF p_include_nonactive_customer = 'Inactive Sites Only'
      THEN
         INSERT INTO xxwc.xxwc_customer_attr_temp
            SELECT *
              FROM apps.xxwc_customer_attr_update
             WHERE     primary_salesrep_id = p_old_sales_rep_id
                   AND state = NVL (p_state, state)
                   AND site_status = 'I'
                   AND profile_account_status =
                          NVL (v_account_status, profile_account_status);
      ELSIF p_include_nonactive_customer = 'All'
      THEN
         INSERT INTO xxwc.xxwc_customer_attr_temp
            SELECT *
              FROM apps.xxwc_customer_attr_update
             WHERE     primary_salesrep_id = p_old_sales_rep_id
                   AND state = NVL (p_state, state)
                   AND profile_account_status =
                          NVL (v_account_status, profile_account_status);
      END IF;

      INSERT INTO xxwc.xxwc_account_mass_update_hist (change,
                                                      run_id,
                                                      change_name,
                                                      old_name,
                                                      new_name,
                                                      old_id,
                                                      new_id,
                                                      records_count_before,
                                                      records_count_after,
                                                      last_updated_by,
                                                      last_update_date,
                                                      responsibility_id,
                                                      org_id)
           VALUES (v_message,
                   l_run_id,
                   'update salesrep',
                   v_old_sales_rep_name,
                   v_new_sales_rep_name,
                   p_old_sales_rep_id,
                   p_new_sales_rep_id,
                   NULL,
                   NULL,
                   v_last_updated_by,
                   v_last_update_date,
                   v_responsibility_id,
                   v_org_id);

      INSERT INTO xxwc.xxwc_account_mass_update_list (table_rowid,
                                                      table_name,
                                                      run_id,
                                                      change_name,
                                                      old_name,
                                                      new_name,
                                                      old_id,
                                                      new_id,
                                                      last_updated_by,
                                                      last_update_date,
                                                      responsibility_id,
                                                      org_id,
                                                      insert_date)
         SELECT site_uses_rowid,
                'hz_cust_site_uses_all',
                l_run_id,
                'update salesrep',
                v_old_sales_rep_name,
                v_new_sales_rep_name,
                p_old_sales_rep_id,
                p_new_sales_rep_id,
                v_last_updated_by,
                v_last_update_date,
                v_responsibility_id,
                v_org_id,
                v_last_update_date
           FROM apps.xxwc_customer_attr_temp;

      FOR r
         IN (SELECT DISTINCT site_uses_rowid site_rowid
               FROM apps.xxwc_customer_attr_temp)
      LOOP
         xxwc_update_cust_site_uses (
            p_site_rowid          => r.site_rowid,
            p_salesrep_id         => p_new_sales_rep_id,
            p_freight_term        => NULL,
            p_last_updated_by     => v_last_updated_by,
            p_last_update_date    => v_last_update_date,
            p_last_update_login   => v_last_update_login,
            p_responsibility_id   => v_responsibility_id,
            p_org_id              => v_org_id);
      END LOOP;

      SELECT COUNT ('a')
        INTO v_error_count
        FROM apps.xxwc_account_mass_update_list
       WHERE error_message IS NOT NULL AND run_id = l_run_id;

      SELECT COUNT ('a')
        INTO v_all_count
        FROM apps.xxwc_account_mass_update_list
       WHERE run_id = l_run_id;

      IF v_error_count > 0
      THEN
         FOR r
            IN (SELECT    'rowid '
                       || table_rowid
                       || ' of table '
                       || table_name
                       || ' had error when '
                       || change_name
                       || ' from '
                       || old_name
                       || ' to '
                       || new_name
                       || ', error: '
                       || error_message
                       || ';'
                          err_message
                  FROM apps.xxwc_account_mass_update_list
                 WHERE error_message IS NOT NULL AND run_id = l_run_id)
         LOOP
            fnd_file.put_line (fnd_file.LOG, 'error ' || r.err_message);
         END LOOP;
      ELSE
         fnd_file.put_line (
            fnd_file.LOG,
            'all ' || v_all_count || ' sites records updated successfully;');
      END IF;

      create_output_report ('error');
      create_output_report ('success');
      p_errbuf := l_errbuf;
      p_retcode := l_retcode;
      clean_log_table;
      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_running_messaget := NULL;
         l_running_messaget :=
               'Error_Stack... '
            || v_message
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();
         l_errbuf := l_running_messaget;
         l_retcode := '2';
         p_errbuf := l_errbuf;
         p_retcode := l_retcode;
         fnd_file.put_line (fnd_file.LOG, 'error ' || l_running_messaget);
         clean_log_table;
         COMMIT;
   -- DBMS_OUTPUT.put_line ('error:' || l_running_messaget);
   END;

   --*************************************************
   FUNCTION get_account_status (p_in_account_status VARCHAR2)
      RETURN VARCHAR2
   IS
      v_return_status   VARCHAR2 (12) := NULL;
   BEGIN
      IF p_in_account_status = 'All'
      THEN
         v_return_status := NULL;
      ELSIF p_in_account_status = 'A'
      THEN
         v_return_status := 'A';
      ELSIF p_in_account_status = 'I'
      THEN
         v_return_status := 'I';
      ELSE
         v_return_status := NULL;
      END IF;

      RETURN v_return_status;
   END;
END;
--*****************************************************************************************
--*****************************************************************************************
--*****************************************************************************************
--*****************************************************************************************
/
