/***********************************************************************************************************************************************
   NAME:     TMS_20181018-00003_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        10/18/2018  Rakesh Patel     TMS#20181018-00003-Unable to open Blanket purchase agreements 
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Delete');

   delete from PO_DRAFTS where status='PDOI ERROR';

   DBMS_OUTPUT.put_line ('Records Delete -' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to delete records: ' || SQLERRM);
	  ROLLBACK;
END;
/