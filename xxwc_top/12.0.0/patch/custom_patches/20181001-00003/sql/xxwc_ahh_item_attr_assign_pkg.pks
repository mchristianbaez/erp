CREATE OR REPLACE PACKAGE APPS.xxwc_ahh_item_attr_assign_pkg IS
  /***************************************************************************
  *    Script Name: xxwc_ahh_item_attr_assign_pkg.pks
  *
  *    Interface / Conversion Name: Item conversion.
  *
  *    Functional Purpose: Convert Items using Interface for A.H Harris
  *
  *    History:
  *
  *    Version    Date             Author       Description
  ******************************************    ******************************
  *    1.0        10/01/2018       Rakesh P     TMS#20181001-00003 - AHH-Data fix program for updating attributes and other entities
  *****************************************************************************/

  -- Prints Debug/Log Message - Checks for Debug Flag.
  PROCEDURE print_debug(p_print_str IN VARCHAR2);

  -- Print Log Message - Ignores Debug Flag.
  PROCEDURE print_log(p_print_str IN VARCHAR2);

  -- Convert Varchar2 into Number function
  FUNCTION convert_number(pid     IN ROWID
                         ,p_col   IN VARCHAR2
                         ,p_value IN VARCHAR2) RETURN NUMBER;

   -- Main Item Conversion Program
   PROCEDURE main      (errbuf                OUT VARCHAR2
                        ,retcode               OUT NUMBER
                        ,p_item_creation_date  IN VARCHAR2
                        ,p_item1               IN VARCHAR2
                        ,p_item2               IN VARCHAR2
                        ,p_item3               IN VARCHAR
                        ,p_update_item_att     IN VARCHAR2
                        ,p_update_item_att_cat IN VARCHAR2
                        ,p_assign_source_rule  IN VARCHAR2
                        ,p_assign_Locators     IN VARCHAR2
   );

END xxwc_ahh_item_attr_assign_pkg;
/