CREATE OR REPLACE PACKAGE APPS.XXWC_CREDIT_COPY_TRX_PKG
IS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXXWC_CREDIT_COPY_TRX_PKG.pkb $
   *   Module Name: XXWC_CREDIT_COPY_TRX_PKG.pkb
   *
   *   PURPOSE:   This package is used by the XXWC Credit and Rebill UI to submit the conc program
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   * ***************************************************************************/

   --Define variables for logging debug messages
   g_level_unexpected CONSTANT   NUMBER := 6;
   g_level_error CONSTANT        NUMBER := 5;
   g_level_exception CONSTANT    NUMBER := 4;
   g_LEVEL_EVENT CONSTANT        NUMBER := 3;
   g_LEVEL_PROCEDURE CONSTANT    NUMBER := 2;
   g_LEVEL_STATEMENT CONSTANT    NUMBER := 1;

   --Global variable to store the org id
   g_org_id                      NUMBER;    --:= fnd_profile.VALUE ('ORG_ID');

   /*************************************************************************
   *   Procedure : LOG_MSG
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Insert Trigger
   *   Parameter:
   *          IN
   *              p_debug_level    -- Debug Level
   *              p_mod_name       -- Module Name
   *              p_debug_msg      -- Debug Message
   * ************************************************************************/

   PROCEDURE LOG_MSG (p_debug_level   IN NUMBER,
                      p_mod_name      IN VARCHAR2,
                      p_debug_msg     IN VARCHAR2);

   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXXWC_CREDIT_COPY_TRX_PKG.pkb $
   *   Module Name: XXWC_CREDIT_COPY_TRX_PKG.pkb
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Insert Trigger
   *              from XXWC Credit and Rebill UI
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   * ***************************************************************************/

   PROCEDURE Insert_Row (
      PX_CREDIT_COPY_TRX_ID          IN OUT NOCOPY NUMBER,
      P_CUSTOMER_TRX_ID                            NUMBER,
      P_REV_CUSTOMER_TRX_ID                        NUMBER,
      P_NEW_CUSTOMER_TRX_ID                        NUMBER,
      P_CUST_ACCOUNT_ID                            NUMBER,
      P_REBILL_BILL_TO_SITE_USE_ID                 NUMBER,
      P_REBILL_SHIP_TO_SITE_USE_ID                 NUMBER,
      P_REASON_CODE                                VARCHAR2,
      P_LAST_UPDATE_DATE                           DATE,
      P_LAST_UPDATED_BY                            NUMBER,
      P_CREATION_DATE                              DATE,
      P_CREATED_BY                                 NUMBER,
      P_LAST_UPDATE_LOGIN                          NUMBER,
      P_OBJECT_VERSION_NUMBER                      NUMBER,
      P_PROCESS_STATUS                             VARCHAR2,
      P_ERROR_MESSAGE                              VARCHAR2,
      P_SELECT_TRX                                 VARCHAR2
   );

   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXXWC_CREDIT_COPY_TRX_PKG.pkb $
   *   Module Name: XXWC_CREDIT_COPY_TRX_PKG.pkb
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Update Trigger
   *              from XXWC Credit and Rebill UI
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   * ***************************************************************************/

   PROCEDURE Update_Row (P_CREDIT_COPY_TRX_ID            NUMBER,
                         P_REBILL_BILL_TO_SITE_USE_ID    NUMBER,
                         P_REBILL_SHIP_TO_SITE_USE_ID    NUMBER,
                         P_REASON_CODE                   VARCHAR2,
                         P_LAST_UPDATE_DATE              DATE,
                         P_LAST_UPDATED_BY               NUMBER,
                         P_CREATION_DATE                 DATE,
                         P_CREATED_BY                    NUMBER,
                         P_LAST_UPDATE_LOGIN             NUMBER,
                         P_OBJECT_VERSION_NUMBER         NUMBER,
                         P_PROCESS_STATUS                VARCHAR2,
                         P_ERROR_MESSAGE                 VARCHAR2,
                         P_SELECT_TRX                    VARCHAR2);

   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXXWC_CREDIT_COPY_TRX_PKG.pkb $
   *   Module Name: XXWC_CREDIT_COPY_TRX_PKG.pkb
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Lock Trigger
   *              from XXWC Credit and Rebill UI
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   * ***************************************************************************/

   PROCEDURE Lock_Row (P_CREDIT_COPY_TRX_ID       NUMBER,
                       p_OBJECT_VERSION_NUMBER    NUMBER);

   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXXWC_CREDIT_COPY_TRX_PKG.pkb $
   *   Module Name: XXWC_CREDIT_COPY_TRX_PKG.pkb
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Delete Trigger
   *              from XXWC Credit and Rebill UI
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   * ***************************************************************************/

   PROCEDURE Delete_Row (P_CREDIT_COPY_TRX_ID NUMBER);

   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXXWC_CREDIT_COPY_TRX_PKG.pkb $
   *   Module Name: XXWC_CREDIT_COPY_TRX_PKG.pkb
   *
   *   PURPOSE:   This package is used by the XXWC Credit and Rebill UI to submit the conc program
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   * ***************************************************************************/
   PROCEDURE Submit (errbuf         OUT VARCHAR2,
                     retcode        OUT VARCHAR2,
                     p_user_id   IN     NUMBER);

/* **************************************************************************
   *   Procedure Name: cons_trx
   *
   *   PURPOSE:   This procedure called from a conc prog to consolidate the credit
   *              and rebill trx into a single transaction
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        04/21/2013  Shankar Hariharan             Initial Version
   *   1.10       03/14/2018  Nancy Pahwa              Task ID: 20180312-00253 Added an in parameter
   * ***************************************************************************/
   PROCEDURE cons_trx (errbuf         OUT VARCHAR2,
                     retcode        OUT VARCHAR2,
                     p_backup_interface IN varchar2); --Ver 1.10
END XXWC_CREDIT_COPY_TRX_PKG;
/