/********************************************************************************
   $Header ENG_ENGINEERING_CHANGES.sql $
   Module Name: ENG_ENGINEERING_CHANGES

   PURPOSE:  Synonym for eng_engineering_changes

   REVISIONS:
   Ver        Date        Author                  Description
   ---------  ----------  ---------------         -------------------------
   1.0        02/17/2016  Manjula Chellappan      TMS# 20160202-00288 - Create synonyms on EBS for OBIEE
********************************************************************************/
CREATE SYNONYM interface_obi.eng_engineering_changes FOR apps.eng_engineering_changes;