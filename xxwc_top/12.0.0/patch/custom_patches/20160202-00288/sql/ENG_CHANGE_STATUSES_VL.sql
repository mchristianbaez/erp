/********************************************************************************
   $Header ENG_CHANGE_STATUSES_VL.sql $
   Module Name: ENG_CHANGE_STATUSES_VL

   PURPOSE:  Synonym for ENG_CHANGE_STATUSES_VL

   REVISIONS:
   Ver        Date        Author                  Description
   ---------  ----------  ---------------         -------------------------
   1.0        02/17/2016  Manjula Chellappan      TMS# 20160202-00288 - Create synonyms on EBS for OBIEE
********************************************************************************/
CREATE SYNONYM interface_obi.ENG_CHANGE_STATUSES_VL FOR apps.ENG_CHANGE_STATUSES_VL;