/*
 TMS: 20160719-00167   
 Date: 7/22/2016
 */
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Update - Sales/Supply Chain alerts');

   UPDATE XXCUS.XXCUS_MONITOR_CP_B
      SET WARNING_EMAIL_ADDRESS = 'WC-ITBSAAlerts-U1@HDSupply.com',
          ERROR_EMAIL_ADDRESS = 'WC-ITBSAAlerts-U1@HDSupply.com',
          last_update_date = SYSDATE,
          last_updated_by = 16991
    WHERE OWNED_BY IN ('SALES AND FULFILLMENT', 'SUPPLY CHAIN');

   DBMS_OUTPUT.put_line (
         'After Update - Sales/Supply Chain alerts Update (Records) '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('Before Update - Finance ');

   UPDATE XXCUS.XXCUS_MONITOR_CP_B
      SET WARNING_EMAIL_ADDRESS = 'WC-ITBSAAlerts-U1@HDSupply.com',
          ERROR_EMAIL_ADDRESS = 'WC-ITBSAAlerts-U1@HDSupply.com',
          last_update_date = SYSDATE,
          last_updated_by = 16991
    WHERE     OWNED_BY = 'FINANCE'
          AND (   WARNING_EMAIL_ADDRESS = 'wcitfinance@hdsupply.com'
               OR ERROR_EMAIL_ADDRESS = 'wcitfinance@hdsupply.com');

   DBMS_OUTPUT.put_line ('After Update - Finance (Records) ' || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('Before Update - Dev Team Warning');

   UPDATE XXCUS.XXCUS_MONITOR_CP_B
      SET WARNING_EMAIL_ADDRESS = 'WC-ITDEVALERTS-U1@HDSupply.com',
          last_update_date = SYSDATE,
          last_updated_by = 16991
    WHERE WARNING_EMAIL_ADDRESS = 'HDSOracleDevelopers@hdsupply.com';

   DBMS_OUTPUT.put_line ('After Update - Dev Team Warning' || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('Before Update - Dev Team Error');

   UPDATE XXCUS.XXCUS_MONITOR_CP_B
      SET error_EMAIL_ADDRESS = 'WC-ITDEVALERTS-U1@HDSupply.com',
          last_update_date = SYSDATE,
          last_updated_by = 16991
    WHERE error_EMAIL_ADDRESS = 'HDSOracleDevelopers@hdsupply.com';

   DBMS_OUTPUT.put_line ('After Update - Dev Team error ' || SQL%ROWCOUNT);

   COMMIT;

EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.PUT_LINE ('Error updating table -' || SQLERRM);
END;
/