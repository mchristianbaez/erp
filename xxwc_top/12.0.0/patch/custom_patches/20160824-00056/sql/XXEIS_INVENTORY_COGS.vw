---------------------------------------------------------------------------------------------------------------

/**************************************************************************************************************
  $Header XXEIS.XXEIS_INVENTORY_COGS $
  Module Name : Inventory
  PURPOSE	  : Inventory - Shipped, No COS report
  REVISIONS   :
  VERSION 	DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.1     22-Sep-2016        	Siva   		 TMS#20160824-00056 --Changed the tables,columns,conditions orders 
															and added hints for fixing the for fixing the Performance issue
**************************************************************************************************************/
DROP VIEW xxeis.xxeis_inventory_cogs;

CREATE OR REPLACE VIEW xxeis.xxeis_inventory_cogs
(
   transaction_type_id
  ,acct_period_id
  ,subinventory_code
  ,actual_cost
  ,period_name
  ,msib_segment1
  ,invoice_enabled_flag
  ,organization_code
  ,transaction_type_name
  ,reference_line_id
  ,line_number
  ,shipment_number
  ,flow_status_code
  ,order_number
  ,mta_creation_date
  ,accounting_line_type
  ,primary_quantity
  ,sum_transaction_value
  ,concatenated_segments
  ,segment1
  ,segment2
  ,segment3
  ,segment4
  ,segment5
  ,segment6
  ,segment7
  ,meaning
  ,order_type
  ,trx_number
  ,creation_date
  ,gl_date
)
AS
     SELECT /*+ INDEX(oap XXWC_OBIEE_ORG_ACT_PERIODS) use_nl(mmt,mp) use_nl(ooha,ottt)*/
            --added for version 1.1
            mmt.transaction_type_id
           ,mmt.acct_period_id
           ,mmt.subinventory_code
           ,mmt.actual_cost
           ,oap.period_name
           ,msib.segment1 msib_segment1
           ,msib.invoice_enabled_flag
           ,mp.organization_code
           ,mtt.transaction_type_name
           ,oola.reference_line_id
           ,oola.line_number
           ,oola.shipment_number
           ,oola.flow_status_code
           ,ooha.order_number
           ,mta.creation_date mta_creation_date
           ,mta.accounting_line_type
           ,mta.primary_quantity
           ,SUM (mta.base_transaction_value) sum_transaction_value
           ,gcc.concatenated_segments
           ,gcc.segment1
           ,gcc.segment2
           ,gcc.segment3
           ,gcc.segment4
           ,gcc.segment5
           ,gcc.segment6
           ,gcc.segment7
           ,ml.meaning
           ,ottt.name order_type
           ,rcta.trx_number
           ,rcta.creation_date
           ,dist.gl_date
       FROM apps.mtl_material_transactions mmt
           ,inv.org_acct_periods oap
           ,inv.mtl_system_items_b msib
           ,inv.mtl_parameters mp
           ,inv.mtl_transaction_types mtt
           ,ont.oe_order_lines_all oola
           ,ont.oe_order_headers_all ooha
           ,inv.mtl_transaction_accounts mta
           ,apps.gl_code_combinations_kfv gcc
           ,-- ONT.OE_TRANSACTION_TYPES_ALL OTTA , --commented for version 1.1
            apps.mfg_lookups ml
           ,ont.oe_transaction_types_tl ottt
           ,ar.ra_customer_trx_lines_all rctla
           ,ar.ra_customer_trx_all rcta
           ,(  SELECT MAX (gl_date) gl_date, customer_trx_id
                 FROM ar.ra_cust_trx_line_gl_dist_all
             GROUP BY customer_trx_id) dist                                            --added for version 1.1
      --  (SELECT DISTINCT dist.gl_date,
      --    dist.customer_trx_id
      --  FROM ar.ra_cust_trx_line_gl_dist_all dist
      --  ) dist --commented for version 1.1
      WHERE     mmt.transaction_type_id = 33
            AND mmt.acct_period_id = oap.acct_period_id
            AND mmt.organization_id = oap.organization_id
            AND mmt.inventory_item_id = msib.inventory_item_id
            AND mmt.organization_id = msib.organization_id
            AND mmt.organization_id = mp.organization_id
            AND mmt.transaction_type_id = mtt.transaction_type_id
            AND mmt.trx_source_line_id = oola.line_id
            AND oola.header_id = ooha.header_id
            AND mmt.transaction_id = mta.transaction_id
            AND mta.accounting_line_type = 36                                    --Deferred Cost of Goods Sold
            AND mta.reference_account = gcc.code_combination_id
            --  AND oola.line_type_id       = otta.transaction_type_id --commented for version 1.1
            AND mta.accounting_line_type = ml.lookup_code(+)
            AND ml.lookup_type(+) = 'CST_ACCOUNTING_LINE_TYPE'
            AND ooha.order_type_id = ottt.transaction_type_id
            AND ottt.language = USERENV ('LANG')
            AND NOT EXISTS
                   (SELECT *
                      FROM apps.mtl_material_transactions mmt2
                     WHERE     mmt2.transaction_type_id = 10008
                           AND mmt2.trx_source_line_id = mmt.trx_source_line_id
                           AND mmt2.acct_period_id = mmt.acct_period_id)
            AND TO_CHAR (oola.line_id) = rctla.interface_line_attribute6(+)               --TMS#20140402-00058
            AND rctla.interface_line_context(+) = 'ORDER ENTRY'
            AND rctla.customer_trx_id = rcta.customer_trx_id(+)
            AND rcta.customer_trx_id = dist.customer_trx_id(+)
   GROUP BY mmt.transaction_type_id
           ,mmt.acct_period_id
           ,mmt.subinventory_code
           ,mmt.actual_cost
           ,oap.period_name
           ,msib.segment1
           ,msib.invoice_enabled_flag
           ,mp.organization_code
           ,mtt.transaction_type_name
           ,oola.reference_line_id
           ,oola.line_number
           ,oola.shipment_number
           ,oola.flow_status_code
           ,ooha.order_number
           ,mta.creation_date
           ,mta.accounting_line_type
           ,mta.primary_quantity
           ,gcc.concatenated_segments
           ,gcc.segment1
           ,gcc.segment2
           ,gcc.segment3
           ,gcc.segment4
           ,gcc.segment5
           ,gcc.segment6
           ,gcc.segment7
           ,ml.meaning
           ,ottt.name
           ,rcta.trx_number
           ,rcta.creation_date
           ,dist.gl_date
/