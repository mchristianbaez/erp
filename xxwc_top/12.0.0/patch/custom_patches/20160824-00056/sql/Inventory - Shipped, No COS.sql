--Report Name            : Inventory - Shipped, No COS
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Inventory - Shipped, No COS
set scan off define off
DECLARE
BEGIN 
--Inserting View XXEIS_INVENTORY_COGS
xxeis.eis_rs_ins.v( 'XXEIS_INVENTORY_COGS',401,'','','','','SA059956','XXEIS','Xxeis Inventory Cogs','XIC','','');
--Delete View Columns for XXEIS_INVENTORY_COGS
xxeis.eis_rs_utility.delete_view_rows('XXEIS_INVENTORY_COGS',401,FALSE);
--Inserting View Columns for XXEIS_INVENTORY_COGS
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','FLOW_STATUS_CODE',401,'Flow Status Code','FLOW_STATUS_CODE','','','','SA059956','VARCHAR2','','','Flow Status Code','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','ORDER_TYPE',401,'Order Type','ORDER_TYPE','','','','SA059956','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','GL_DATE',401,'Gl Date','GL_DATE','','','','SA059956','DATE','','','Gl Date','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','CREATION_DATE',401,'Creation Date','CREATION_DATE','','','','SA059956','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','TRX_NUMBER',401,'Trx Number','TRX_NUMBER','','','','SA059956','VARCHAR2','','','Trx Number','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','INVOICE_ENABLED_FLAG',401,'Invoice Enabled Flag','INVOICE_ENABLED_FLAG','','','','SA059956','VARCHAR2','','','Invoice Enabled Flag','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','PERIOD_NAME',401,'Period Name','PERIOD_NAME','','','','SA059956','VARCHAR2','','','Period Name','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','ACCT_PERIOD_ID',401,'Acct Period Id','ACCT_PERIOD_ID','','','','SA059956','NUMBER','','','Acct Period Id','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','SUM_TRANSACTION_VALUE',401,'Sum Transaction Value','SUM_TRANSACTION_VALUE','','','','SA059956','NUMBER','','','Sum Transaction Value','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','PRIMARY_QUANTITY',401,'Primary Quantity','PRIMARY_QUANTITY','','','','SA059956','NUMBER','','','Primary Quantity','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','MEANING',401,'Meaning','MEANING','','','','SA059956','VARCHAR2','','','Meaning','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','ACCOUNTING_LINE_TYPE',401,'Accounting Line Type','ACCOUNTING_LINE_TYPE','','','','SA059956','NUMBER','','','Accounting Line Type','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','MTA_CREATION_DATE',401,'Mta Creation Date','MTA_CREATION_DATE','','','','SA059956','DATE','','','Mta Creation Date','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','SHIPMENT_NUMBER',401,'Shipment Number','SHIPMENT_NUMBER','','','','SA059956','NUMBER','','','Shipment Number','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','LINE_NUMBER',401,'Line Number','LINE_NUMBER','','','','SA059956','NUMBER','','','Line Number','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','REFERENCE_LINE_ID',401,'Reference Line Id','REFERENCE_LINE_ID','','','','SA059956','NUMBER','','','Reference Line Id','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','ACTUAL_COST',401,'Actual Cost','ACTUAL_COST','','','','SA059956','NUMBER','','','Actual Cost','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','SUBINVENTORY_CODE',401,'Subinventory Code','SUBINVENTORY_CODE','','','','SA059956','VARCHAR2','','','Subinventory Code','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','SEGMENT7',401,'Segment7','SEGMENT7','','','','SA059956','VARCHAR2','','','Segment7','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','SEGMENT6',401,'Segment6','SEGMENT6','','','','SA059956','VARCHAR2','','','Segment6','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','SEGMENT5',401,'Segment5','SEGMENT5','','','','SA059956','VARCHAR2','','','Segment5','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','SEGMENT4',401,'Segment4','SEGMENT4','','','','SA059956','VARCHAR2','','','Segment4','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','SEGMENT3',401,'Segment3','SEGMENT3','','','','SA059956','VARCHAR2','','','Segment3','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','SEGMENT2',401,'Segment2','SEGMENT2','','','','SA059956','VARCHAR2','','','Segment2','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','SEGMENT1',401,'Segment1','SEGMENT1','','','','SA059956','VARCHAR2','','','Segment1','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','CONCATENATED_SEGMENTS',401,'Concatenated Segments','CONCATENATED_SEGMENTS','','','','SA059956','VARCHAR2','','','Concatenated Segments','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','TRANSACTION_TYPE_NAME',401,'Transaction Type Name','TRANSACTION_TYPE_NAME','','','','SA059956','VARCHAR2','','','Transaction Type Name','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','TRANSACTION_TYPE_ID',401,'Transaction Type Id','TRANSACTION_TYPE_ID','','','','SA059956','NUMBER','','','Transaction Type Id','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','ORDER_NUMBER',401,'Order Number','ORDER_NUMBER','','','','SA059956','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','MSIB_SEGMENT1',401,'Msib Segment1','MSIB_SEGMENT1','','','','SA059956','VARCHAR2','','','Msib Segment1','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_INVENTORY_COGS','ORGANIZATION_CODE',401,'Organization Code','ORGANIZATION_CODE','','','','SA059956','VARCHAR2','','','Organization Code','','','');
--Inserting View Components for XXEIS_INVENTORY_COGS
--Inserting View Component Joins for XXEIS_INVENTORY_COGS
END;
/
set scan on define on
prompt Creating Report LOV Data for Inventory - Shipped, No COS
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Inventory - Shipped, No COS
xxeis.eis_rs_ins.lov( 401,'select distinct organization_code
from apps.mtl_parameters','','XXWC Organization Code','Organization Code from the mtl_parameters table','ID020048',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select distinct period_name
from apps.ORG_ACCT_PERIODS','','XXWC PERIOD_NAME','PERIOD NAME FROM THE ORG_ACCT_PERIODS TABLE','ID020048',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Inventory - Shipped, No COS
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Inventory - Shipped, No COS
xxeis.eis_rs_utility.delete_report_rows( 'Inventory - Shipped, No COS' );
--Inserting Report - Inventory - Shipped, No COS
xxeis.eis_rs_ins.r( 401,'Inventory - Shipped, No COS','','Order that have been shipped but COGS Recognition transaction is not in the same accounting period','','','','SA059956','XXEIS_INVENTORY_COGS','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','');
--Inserting Report Columns - Inventory - Shipped, No COS
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'ACCOUNTING_LINE_TYPE','Accounting Line Type','Accounting Line Type','','~~~','default','','20','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'ACCT_PERIOD_ID','Acct Period Id','Acct Period Id','','~~~','default','','24','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'ACTUAL_COST','Actual Cost','Actual Cost','','~T~D~2','default','','15','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'CONCATENATED_SEGMENTS','Concatenated Segments','Concatenated Segments','','','default','','6','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'CREATION_DATE','Creation Date','Creation Date','','','default','','28','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'FLOW_STATUS_CODE','Flow Status Code','Flow Status Code','','','default','','31','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'GL_DATE','Gl Date','Gl Date','','','default','','29','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'INVOICE_ENABLED_FLAG','Invoice Enabled Flag','Invoice Enabled Flag','','','default','','26','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'LINE_NUMBER','Line Number','Line Number','','~~~','default','','17','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'MEANING','Meaning','Meaning','','','default','','21','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'MSIB_SEGMENT1','Msib Segment1','Msib Segment1','','','default','','2','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'MTA_CREATION_DATE','Mta Creation Date','Mta Creation Date','','','default','','19','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','3','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'ORDER_TYPE','Order Type','Order Type','','','default','','30','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'ORGANIZATION_CODE','Organization Code','Organization Code','','','default','','1','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'PERIOD_NAME','Period Name','Period Name','','','default','','25','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'PRIMARY_QUANTITY','Primary Quantity','Primary Quantity','','~~~','default','','22','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'REFERENCE_LINE_ID','Reference Line Id','Reference Line Id','','~~~','default','','16','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'SEGMENT1','Segment1','Segment1','','','default','','7','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'SEGMENT2','Segment2','Segment2','','','default','','8','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'SEGMENT3','Segment3','Segment3','','','default','','9','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'SEGMENT4','Segment4','Segment4','','','default','','10','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'SEGMENT5','Segment5','Segment5','','','default','','11','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'SEGMENT6','Segment6','Segment6','','','default','','12','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'SEGMENT7','Segment7','Segment7','','','default','','13','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'SHIPMENT_NUMBER','Shipment Number','Shipment Number','','~~~','default','','18','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'SUBINVENTORY_CODE','Subinventory Code','Subinventory Code','','','default','','14','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'SUM_TRANSACTION_VALUE','Sum Transaction Value','Sum Transaction Value','','~T~D~2','default','','23','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'TRANSACTION_TYPE_ID','Transaction Type Id','Transaction Type Id','','~~~','default','','4','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'TRANSACTION_TYPE_NAME','Transaction Type Name','Transaction Type Name','','','default','','5','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
xxeis.eis_rs_ins.rc( 'Inventory - Shipped, No COS',401,'TRX_NUMBER','Trx Number','Trx Number','','','default','','27','N','','','','','','','','SA059956','N','N','','XXEIS_INVENTORY_COGS','','');
--Inserting Report Parameters - Inventory - Shipped, No COS
xxeis.eis_rs_ins.rp( 'Inventory - Shipped, No COS',401,'Period_Name','','PERIOD_NAME','IN','XXWC PERIOD_NAME','','VARCHAR2','N','Y','1','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory - Shipped, No COS',401,'Organization Code','','ORGANIZATION_CODE','IN','XXWC Organization Code','','VARCHAR2','N','Y','2','','Y','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - Inventory - Shipped, No COS
xxeis.eis_rs_ins.rcn( 'Inventory - Shipped, No COS',401,'PERIOD_NAME','IN',':Period_Name','','','Y','1','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Inventory - Shipped, No COS',401,'ORGANIZATION_CODE','IN',':Organization Code','','','Y','2','Y','SA059956');
--Inserting Report Sorts - Inventory - Shipped, No COS
xxeis.eis_rs_ins.rs( 'Inventory - Shipped, No COS',401,'ORGANIZATION_CODE','ASC','SA059956','1','');
xxeis.eis_rs_ins.rs( 'Inventory - Shipped, No COS',401,'MSIB_SEGMENT1','ASC','SA059956','2','');
--Inserting Report Triggers - Inventory - Shipped, No COS
--Inserting Report Templates - Inventory - Shipped, No COS
--Inserting Report Portals - Inventory - Shipped, No COS
--Inserting Report Dashboards - Inventory - Shipped, No COS
--Inserting Report Security - Inventory - Shipped, No COS
xxeis.eis_rs_ins.rsec( 'Inventory - Shipped, No COS','707','','51104',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Inventory - Shipped, No COS','20005','','50900',401,'SA059956','','');
--Inserting Report Pivots - Inventory - Shipped, No COS
END;
/
set scan on define on
