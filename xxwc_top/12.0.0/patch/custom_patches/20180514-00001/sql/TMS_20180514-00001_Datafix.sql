/***********************************************************************************************************************************************
   NAME:       TMS_20180514-00001_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        05/22/2018  Rakesh Patel    TMS#20180514-00001 
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Records test Inserted -' || SQL%ROWCOUNT);
   INSERT INTO XXWC.XXWC_NEW_TMS_TST values('New TMS test msg');
   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to insert record ' || SQLERRM);
	  ROLLBACK;
END;
/