CREATE OR REPLACE PACKAGE APPS.XXWC_NEW_TMS_TST
AS
/*************************************************************************
     $Header XXWC_NEW_TMS_TST $
**************************************************************************/
   
   FUNCTION XXWC_NEW_TMS_TST(  p_cust_account_num        IN VARCHAR2
                                ,p_cust_name_search_string IN VARCHAR2
                                ,p_branch_code             IN VARCHAR2 
                              ) return sys_refcursor; 
                                
                        
END XXWC_NEW_TMS_TST;
/