set define off 

CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_DMS2_OB_PKG
AS
/*************************************************************************
     $Header XXWC_OM_DMS2_OB_PKG $
     Module Name: XXWC_OM_DMS2_OB_PKG.pkb

     PURPOSE:   DESCARTES Project outbound interface

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        09/05/2017  Rakesh Patel            TMS#20170811-00005-DMS Phase-2.0 Outbound Extract(Standard and Internal orders)
	 2.0        02/08/2018  Rakesh Patel            TMS#20180208-00103 Multiple minor fixes for the descartes errors
	 3.0        2/13/2018   Rakesh Patel            TMS# 20180213-00090-DMS 2.0 Pilot issues Alternate Delivery Address and Return Order CreditBill Only Lines
	 4.0        02/20/2018  Rakesh Patel            TMS#20180220-00071-DMS 2.0 Pilot enhancements based on branch feedback
	 5.0        03/22/2018  Rakesh Patel            TMS#20180321-00211-DMS 2.0 PO Outbound fix & Central Dispatching
	 6.0        04/26/2018  Rakesh Patel            TMS#20180404-00157-DMS 2.0 - Will Call Transfer Orders do not Display correct Pickup Address (Enhancement)
	 7.0        05/03/2018  Rakesh Patel            TMS#20180330-00089-DMS 2.0 Modification Request--Purchase orders outbound
**************************************************************************/

   --Email Defaults
   g_dflt_email                fnd_user.email_address%TYPE    := 'WC-ITDEVALERTS-U1@HDSupply.com';
   g_debug                     BOOLEAN := FALSE;
   g_retcode                   NUMBER;  
   g_err_msg                   VARCHAR2(3000);
   g_sqlcode                   NUMBER;   
   g_sqlerrm                   VARCHAR2(2000);
   g_message                   VARCHAR2(2000);
   g_package                   VARCHAR2(200) :=   'XXWC_OM_DMS2_OB_PKG';
   g_call_from                 VARCHAR2(200);
   g_module                    VARCHAR2 (80) := 'OM';
   
   g_wallet_path               VARCHAR2(100);
   g_dms2_username             VARCHAR2(100);
   g_dms2_passwordHash         VARCHAR2(100);
   g_dms2_organizationkey      VARCHAR2(100);
   g_dms2_shipmentsvc_url      VARCHAR2(2000);
   --g_request_id                NUMBER := fnd_global.conc_request_id;
   
   PROCEDURE write_output (p_message IN VARCHAR2)
   IS
   BEGIN
      --fnd_file.put_line (fnd_file.output, p_message);
      IF g_debug THEN
        DBMS_OUTPUT.PUT_LINE(p_message);
        fnd_file.put_line (fnd_file.log, p_message);
      END IF;  
   END;
   
   FUNCTION replace_special_characters( p_string in VARCHAR2 ) RETURN VARCHAR2 
   IS 
   BEGIN 
      RETURN 
        REPLACE(
           REPLACE(
                   REPLACE(
                           REPLACE(
                                   REPLACE(
                                           REGEXP_REPLACE( 
                                                          REGEXP_REPLACE(p_string
                                                                        ,'[[:punct:]]' --Rev# 7.0
                                                                        )
                                                          ,'[[:cntrl:]]'
                                                         )
                                           ,'&'
                                           ,'&amp;'
                                          )
                                  ,'<'
                                  ,'&lt;'
                                 ) 
                           ,'>'
                           ,'&gt;'
                          )              
                   ,''''
                   ,'&apos;'
                  )              
               ,'"'
               ,'&quot;'
              ); 
  END; 

  /*******************************************************************************
   Procedure Name  :   DEBUG_LOG
   Description     :   This function will be used for log messages 
   ******************************************************************************/
   PROCEDURE DEBUG_LOG (p_xxwc_log_rec IN OUT xxwc_log_rec )
   IS
       PRAGMA AUTONOMOUS_TRANSACTION;
       l_order_number XXWC.XXWC_DMS2_LOG_TBL.order_number%TYPE;
       l_po_number XXWC.XXWC_DMS2_LOG_TBL.po_number%TYPE;
       l_update BOOLEAN := FALSE;
       
       l_sec           VARCHAR2 (2000);
   BEGIN
      l_sec := 'Start DEBUG_LOG';
      g_call_from := 'DEBUG_LOG';
      
      IF p_xxwc_log_rec.order_number IS NOT NULL THEN
         BEGIN
           SELECT order_number
             INTO l_order_number 
             FROM xxwc.xxwc_dms2_log_tbl
            WHERE interface_type             = 'OUTBOUND'
              AND service_name	             = p_xxwc_log_rec.service_name
              AND order_number               = p_xxwc_log_rec.order_number
              AND NVL(order_type, 1)         = NVL(p_xxwc_log_rec.order_type, 1)
              AND NVL(delivery_id, 1)        = NVL(p_xxwc_log_rec.delivery_id, 1)
              AND NVL(line_id, 1)            = NVL(p_xxwc_log_rec.line_id, 1)
              AND NVL(rpt_con_request_id, 1) = NVL(p_xxwc_log_rec.rpt_con_request_id, 1)
              AND ROWNUM=1;
         
           l_update := TRUE;
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
           l_update := FALSE;
         WHEN OTHERS THEN
           l_update := FALSE;
         END;   
      ELSE
         BEGIN
            SELECT po_number
              INTO l_po_number
              FROM xxwc.xxwc_dms2_log_tbl
             WHERE interface_type  = 'OUTBOUND' 
               AND po_number       = p_xxwc_log_rec.po_number
               AND service_name	   = p_xxwc_log_rec.service_name
               AND schedule_delivery_date = p_xxwc_log_rec.schedule_delivery_date --Rev#4.0
               AND ROWNUM = 1;
    
          l_update := TRUE;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
           l_update := FALSE;
        WHEN OTHERS THEN
           l_update := FALSE;
        END;      
      END IF; --Update
      
      IF l_update = TRUE THEN
        UPDATE XXWC.XXWC_DMS2_LOG_TBL
           SET SERVICE_NAME            = p_xxwc_log_rec.service_name
              ,REQUEST_PAYLOAD         = p_xxwc_log_rec.request_payload
              ,RESPONSE_PAYLOAD        = p_xxwc_log_rec.response_payload
              ,WS_RESPONSECODE         = p_xxwc_log_rec.ws_responsecode
              ,WS_RESPONSEDESCRIPTION  = p_xxwc_log_rec.ws_responsedescription
              ,HTTP_STATUS_CODE        = p_xxwc_log_rec.http_status_code
              ,HTTP_REASON_PHRASE      = p_xxwc_log_rec.http_reason_phrase
              ,LAST_UPDATE_DATE        = SYSDATE
              ,LAST_UPDATED_BY         = FND_GLOBAL.user_id
              ,CONCURRENT_REQUEST_ID   = Fnd_global.conc_request_id 
              ,RPT_CON_REQUEST_ID      = p_xxwc_log_rec.rpt_con_request_id
         WHERE interface_type             = 'OUTBOUND'
           AND NVL(ORDER_NUMBER, -1)      = NVL( p_xxwc_log_rec.order_number, -1)
           AND NVL(PO_NUMBER, -1)         = NVL( p_xxwc_log_rec.PO_NUMBER, -1)
           AND service_name	              = p_xxwc_log_rec.service_name
           AND NVL(order_type, 1)         = NVL(p_xxwc_log_rec.order_type, 1)
           AND NVL(delivery_id, 1)        = NVL(p_xxwc_log_rec.delivery_id, 1)
           AND NVL(line_id, 1)            = NVL(p_xxwc_log_rec.line_id, 1)
           AND schedule_delivery_date     = p_xxwc_log_rec.schedule_delivery_date; --REv#4.0
      ELSE
         INSERT INTO XXWC.XXWC_DMS2_LOG_TBL
            (SEQUENCE_ID             ,
             SERVICE_NAME            ,
             INTERFACE_TYPE          ,
             ORDER_NUMBER            ,  
             ORDER_HEADER_ID         ,
             PO_NUMBER               , 
             PO_header_id            ,
             LINE_ID                 ,
             DELIVERY_ID             , 
             ORDER_TYPE              ,
             SCHEDULE_DELIVERY_DATE  ,
             RPT_CON_REQUEST_ID      ,
             REQUEST_PAYLOAD         ,
             RESPONSE_PAYLOAD        ,
             WS_RESPONSECODE         ,
             WS_RESPONSEDESCRIPTION  ,
             HTTP_STATUS_CODE        ,
             HTTP_REASON_PHRASE      ,
             CONCURRENT_REQUEST_ID   ,
             CREATION_DATE           ,
             CREATED_BY              ,
             LAST_UPDATE_DATE        ,
             LAST_UPDATED_BY         ,
             ATTRIBUTE1              ,
             ATTRIBUTE2              ,
             ATTRIBUTE3              ,
             ATTRIBUTE4              ,
             ATTRIBUTE5              
             )
         VALUES
             (
              XXWC.XXWC_DMS2_LOG_TBL_SEQ.NEXTVAL  ,
              p_xxwc_log_rec.service_name              ,
              'OUTBOUND'                               , 
              p_xxwc_log_rec.order_number              ,
              p_xxwc_log_rec.header_id                 ,
              p_xxwc_log_rec.po_number                 ,
              p_xxwc_log_rec.po_header_id              ,
              p_xxwc_log_rec.line_id                   ,
              p_xxwc_log_rec.delivery_id               ,
              p_xxwc_log_rec.order_type                ,
              p_xxwc_log_rec.schedule_delivery_date    ,
              p_xxwc_log_rec.rpt_con_request_id        ,
              p_xxwc_log_rec.request_payload           ,
              p_xxwc_log_rec.response_payload          ,
              p_xxwc_log_rec.ws_responsecode           ,
              p_xxwc_log_rec.ws_responsedescription    ,
              p_xxwc_log_rec.http_status_code          ,
              p_xxwc_log_rec.http_reason_phrase        ,
              Fnd_global.conc_request_id               ,
              SYSDATE                                  ,
              FND_GLOBAL.user_id                       ,
              SYSDATE                                  ,
              FND_GLOBAL.user_id                       ,
              NULL                                     ,   
              NULL                                     ,
              NULL                                     ,
              NULL                                     ,
              NULL                                   
             ) ;  
      END IF;
      
      COMMIT;
      l_sec := 'End DEBUG_LOG';
  EXCEPTION
    WHEN OTHERS THEN
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'Error in generating debug log ';
    
        xxcus_error_pkg.xxcus_error_main_api (  p_called_from         => g_package||'.'||g_call_from
                                              , p_calling             => l_sec
                                              , p_ora_error_msg       => SQLERRM
                                              , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                              , p_distribution_list   => g_dflt_email
                                              , p_module              => g_module);

   
   END DEBUG_LOG;
   
   /*******************************************************************************
   Function Name   :   IS_DMS_ELIGIBLE
   Description     :   This function will be used to check if the data should be send to descartes or not
   ******************************************************************************/
   FUNCTION IS_DMS_ELIGIBLE ( p_con_program_name     IN VARCHAR2
                            , p_default_shipping_org IN NUMBER 
                            , p_order_type_id        IN NUMBER
                            , p_header_id            IN NUMBER DEFAULT NULL----Rev#4.0
                            )
      RETURN VARCHAR2
   AS 
      l_count         NUMBER :=0;
      l_shp_org_count NUMBER :=0;    
      l_int_ord_count NUMBER :=0; --Rev#4.0    
   BEGIN
      IF p_order_type_id IS NOT NULL AND p_order_type_id = 1004 THEN
         RETURN 'N';
      ELSE
         SELECT count(1) 
           INTO l_shp_org_count 
           FROM apps.fnd_lookup_values flv,
                org_organization_definitions od
          WHERE 1=1
            AND flv.lookup_type    = 'XXWC_DMS2_BRANCHES_LOOKUP'
            AND flv.enabled_flag   = 'Y'
            AND flv.lookup_code    = od.organization_code
            AND od.organization_id = p_default_shipping_org
            AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1);
            
         IF l_shp_org_count >0 THEN
            SELECT count(1) 
              INTO l_count 
              FROM fnd_lookup_values flv
             WHERE 1=1
               AND flv.lookup_code  = p_con_program_name
               AND flv.lookup_type  = 'XXWC_DMS2_SCHEDULE_DEL_DATE'
               AND flv.enabled_flag = 'Y'
               AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1);
       
              IF l_count > 0 THEN
                 RETURN 'Y';
              --Rev#4.0 <Start   
              ELSIF p_order_type_id = 1011 AND p_con_program_name = 'XXWC_INT_ORD_PICK' THEN 
                 SELECT COUNT(1)
                   INTO l_int_ord_count
                   FROM apps.oe_order_headers_all ooh
                  WHERE 1 = 1
                    AND ooh.header_id = p_header_id
                    AND EXISTS( SELECT 1
                                  FROM fnd_lookup_values flv
                                 WHERE 1=1
                                  AND flv.meaning      = ooh.shipping_method_code
                                  AND flv.lookup_type  = 'XXWC_DMS2_INTERNAL_SHIP_METHOD'
                                  AND flv.enabled_flag = 'Y'
                                  AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                               );
                 IF l_int_ord_count > 0 THEN
                   RETURN 'Y';
                 ELSE 
                   RETURN 'N';
                 END IF;   
                 --Rev#4.0 <End
              ELSE 
                 RETURN 'N';
              END IF;
          ELSE
            RETURN 'N'; 
          END IF;    
       END IF;    
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
   END IS_DMS_ELIGIBLE;
   
   /*******************************************************************************
   Function Name   :   IS_DMS_SHIP_METHOD
   Description     :   This function will be used to check if the data should be send to descartes or not
   ******************************************************************************/
   FUNCTION IS_DMS_SHIP_METHOD ( p_ship_method IN VARCHAR2 )
      RETURN VARCHAR2
   AS 
      l_count NUMBER :=0;
   BEGIN
      SELECT count(1) 
        INTO l_count 
        FROM fnd_lookup_values flv
       WHERE 1=1
         AND flv.meaning  = p_ship_method
         AND flv.lookup_type  = 'XXWC_DMS2_SHIPPING_METHOD_CODE'
         AND flv.enabled_flag = 'Y'
         AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1);
       
        IF l_count > 0 THEN
           RETURN 'Y';
        ELSE
           RETURN 'N';
        END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
   END IS_DMS_SHIP_METHOD;
   
   --Rev#3.0 <Start
   /*******************************************************************************
   Function Name   :   IS_SO_ADA_EXISTS
   Description     :   This function will be used to check if Alternate Delivery address(ADA) exists for SO
   ******************************************************************************/
   FUNCTION IS_SO_ADA_EXISTS ( p_order_number IN NUMBER )
      RETURN VARCHAR2
   AS 
      l_count NUMBER :=0;
   BEGIN
      SELECT count(1) 
        INTO l_count 
        FROM apps.oe_order_headers_all ooh
       WHERE ooh.order_number  = p_order_number
         AND ooh.order_type_id = 1001 --Standard Order
         AND (   TRIM(attribute9) IS NOT NULL
              OR TRIM(ATTRIBUTE10) IS NOT NULL
              OR TRIM(ATTRIBUTE14) IS NOT NULL
              OR TRIM(ATTRIBUTE15) IS NOT NULL);

        IF l_count > 0 THEN
           RETURN 'Y';
        ELSE
           RETURN 'N';
        END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
   END IS_SO_ADA_EXISTS;
   --Rev#3.0 >End
   
   /*******************************************************************************
   Function Name   :   GET_DMS_ORG
   Description     :   This function will be used to get descarties depo
   ******************************************************************************/
   FUNCTION GET_DMS_ORG ( p_branch_code IN VARCHAR2 )
      RETURN VARCHAR2
   AS 
      l_dms_branch fnd_lookup_values.meaning%TYPE;
   BEGIN
      SELECT flv.meaning
        INTO l_dms_branch 
        FROM fnd_lookup_values flv
       WHERE 1=1
         AND flv.lookup_code  = p_branch_code
         AND flv.lookup_type  = 'XXWC_DMS2_BRANCHES_LOOKUP'
         AND flv.enabled_flag = 'Y'
         AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1);
       
      RETURN l_dms_branch;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'XXX';
   END GET_DMS_ORG;
   
   --Rev#5.0 <Start
   /*******************************************************************************
   Function Name   :   GET_DMS2_CENTRAL_DPATCHG_ORG
   Description     :   This function will be used to get central dispatching org
   ******************************************************************************/
   FUNCTION GET_DMS2_CENTRAL_DPATCHG_ORG ( p_branch_code IN VARCHAR2 )
      RETURN VARCHAR2
   AS 
      l_dms_branch fnd_lookup_values.meaning%TYPE;
   BEGIN
      SELECT flv.description
        INTO l_dms_branch 
        FROM fnd_lookup_values flv
       WHERE 1=1
         AND flv.lookup_code  = p_branch_code
         AND flv.lookup_type  = 'XXWC_DMS2_CENTRAL_DISPATCHING'
         AND flv.enabled_flag = 'Y'
         AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1);
       
      RETURN l_dms_branch;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END GET_DMS2_CENTRAL_DPATCHG_ORG;
   --Rev#5.0 >End

 --Rev#6.0 <Start
 FUNCTION  get_job_site_contact (p_ship_to_contact_id NUMBER ) RETURN VARCHAR2 AS
      l_rel_party_id   NUMBER;
      v_number         VARCHAR2 (100);
      l_phone_number   VARCHAR2 (20);
      l_area_code      VARCHAR2 (20);
   BEGIN
      IF p_ship_to_contact_id IS NOT NULL
      THEN
         -------------------------------------------------------------------
         -- Derive rel_party_id
         -------------------------------------------------------------------
         SELECT rel_party_id
           INTO l_rel_party_id
           FROM ar_contacts_v co
          WHERE 1 = 1 AND co.contact_id = p_ship_to_contact_id AND ROWNUM = 1;

         -------------------------------------------------------------------
         -- Derive Phone Number and Area Code
         -------------------------------------------------------------------
         BEGIN
            SELECT ph.area_code, ph.phone_number
              INTO l_area_code, l_phone_number
              FROM ar_phones_v ph
             WHERE     1 = 1
                   AND l_rel_party_id = ph.owner_table_id(+)
                   AND ph.owner_table_name(+) = 'HZ_PARTIES'
                   AND ph.status(+) = 'A'
                   AND ph.primary_flag(+) = 'Y'
                   AND (   ph.phone_type = 'GEN'
                        OR ph.phone_type = 'MOBILE'
                        OR ph.phone_type IS NULL)
                   AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         -------------------------------------------------------------------
         -- Format Phone Number
         -------------------------------------------------------------------
         IF l_phone_number IS NOT NULL
         THEN
            IF INSTR (l_phone_number, '-', 4) = 0
            THEN
               v_number :=
                  (   l_area_code
                   || '-'
                   || SUBSTR (l_phone_number, 1, 3)
                   || '-'
                   || SUBSTR (l_phone_number, 4, 7));
            ELSE
               v_number := (l_area_code || '-' || l_phone_number);
            END IF;
         ELSE
            v_number := NULL;
         END IF;                         -- IF l_phone_number IS NOT NULL THEN
      END IF;                       --IF p_ship_to_contact_id IS NOT NULL THEN
      RETURN TRIM (v_number);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN '';
   END;
  --Rev#6.0 > End

   PROCEDURE raise_so_cancel_line ( p_header_id IN NUMBER
                                   ,p_line_id  IN NUMBER
                                   ,p_error_msg  OUT VARCHAR2)

   IS PRAGMA AUTONOMOUS_TRANSACTION;

      l_header_id               NUMBER;
      l_line_id                 NUMBER;
      l_error_msg               VARCHAR2 (4000);
      l_parameter_list          wf_parameter_list_t;
      l_event_data              CLOB;
      l_flow_status_code        oe_order_headers_all.flow_status_code%TYPE;
      l_sec                     VARCHAR2 (100);
   BEGIN
      l_sec := 'Initialize Values';
      
      BEGIN
         SELECT ooh.flow_status_code
           INTO l_flow_status_code
           FROM oe_order_headers_all ooh
          WHERE 1 = 1
            AND ooh.header_id = p_header_id;
      EXCEPTION
      WHEN OTHERS THEN
        l_flow_status_code :=NULL;
      END;
      
      IF NVL(l_flow_status_code, 'XXX') <> 'CANCELLED' THEN
         l_header_id := p_header_id;
         l_line_id   := p_line_id;

         l_sec := 'Set values for Parameter List';
           l_parameter_list :=
               wf_parameter_list_t (
                  wf_parameter_t ('SEND_DATE',
                                  fnd_date.date_to_canonical (SYSDATE)),
                  wf_parameter_t ('P_REQUEST_TYPE', 'LINE'),                                  
                  wf_parameter_t ('P_HEADER_ID', l_header_id),
                  wf_parameter_t ('P_LINE_ID', l_line_id)
                            );

         l_sec := 'Raise business Event';
        
        wf_event.raise (
               p_event_name   => 'xxwc.oracle.apps.ont.order.cancel_hdr_line',
               p_event_key    => SYS_GUID (),
               p_event_data   => l_event_data,
               p_parameters   => l_parameter_list);

         COMMIT;
      END IF;  

   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_DMS2_OB_PKG.raise_so_cancel_line',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'OM');
            
         p_error_msg := 'Failed for cancel line '||SUBSTR (SQLERRM, 1, 240);
   END raise_so_cancel_line;
   
   PROCEDURE raise_so_cancel_order ( p_header_id IN NUMBER
                                    ,p_error_msg  OUT VARCHAR2)

   IS PRAGMA AUTONOMOUS_TRANSACTION;

      l_header_id               NUMBER;
      l_line_id                 NUMBER;
      l_error_msg               VARCHAR2 (4000);
      l_parameter_list          wf_parameter_list_t;
      l_event_data              CLOB;
      l_line_type_id            NUMBER;
      l_sec                     VARCHAR2 (100);
   BEGIN
      l_sec := 'Initialize Values';
      
      l_header_id := p_header_id;
      
      l_sec := 'Set values for Parameter List';
           l_parameter_list :=
               wf_parameter_list_t (
                  wf_parameter_t ('SEND_DATE',
                                  fnd_date.date_to_canonical (SYSDATE)),
                  wf_parameter_t ('P_REQUEST_TYPE', 'ORDER'),    
                  wf_parameter_t ('P_HEADER_ID', l_header_id)
                            );

      l_sec := 'Raise business Event';

      wf_event.raise (
               p_event_name   => 'xxwc.oracle.apps.ont.order.cancel_hdr_line',
               p_event_key    => SYS_GUID (),
               p_event_data   => l_event_data,
               p_parameters   => l_parameter_list);

      COMMIT;

   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_DMS2_OB_PKG.raise_so_cancel_order',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'OM');
            
         p_error_msg := 'Failed for cancel order '||SUBSTR (SQLERRM, 1, 240);
   END raise_so_cancel_order;
       
FUNCTION new_request(p_method        IN  VARCHAR2,
                     p_namespace     IN  VARCHAR2,
                     p_envelope_tag  IN  VARCHAR2 DEFAULT 'soapenv')
  RETURN t_request AS
-- ---------------------------------------------------------------------
  l_request  t_request;
BEGIN
  l_request.method       := p_method;
  l_request.namespace    := p_namespace;
  l_request.envelope_tag := p_envelope_tag;
  RETURN l_request;
END;

PROCEDURE add_parameter(p_request  IN OUT NOCOPY  t_request,
                        p_name     IN             VARCHAR2,
                        p_value    IN             VARCHAR2,
                        p_type     IN             VARCHAR2 := NULL) AS
-- ---------------------------------------------------------------------
BEGIN
  IF p_type IS NULL THEN
    p_request.body := p_request.body||'<'||p_name||'>'||p_value||'</'||p_name||'>';
  ELSE
    p_request.body := p_request.body||'<'||p_name||' xsi:type="'||p_type||'">'||p_value||'</'||p_name||'>';
  END IF;
END;

PROCEDURE add_complex_parameter(p_request  IN OUT NOCOPY  t_request,
                                p_xml      IN             VARCHAR2) AS
BEGIN
  p_request.body := p_request.body||p_xml;
END;

PROCEDURE add_toclob(p_footer_clob  IN OUT NOCOPY  CLOB,
                     p_name     IN             VARCHAR2,
                     p_value    IN             VARCHAR2) AS
BEGIN
   p_footer_clob := p_footer_clob||'<'||p_name||'>'||p_value||'</'||p_name||'>';
END;

PROCEDURE add_toclob_complex_parameter(p_footer_clob  IN OUT NOCOPY  CLOB,
                                       p_xml      IN             VARCHAR2) AS
BEGIN
  p_footer_clob := p_footer_clob||p_xml;
END;

PROCEDURE generate_envelope(p_request  IN OUT NOCOPY  t_request,
		                    p_env      IN OUT NOCOPY  VARCHAR2) AS
BEGIN
  p_env := '<'||p_request.envelope_tag||':Envelope xmlns:'||p_request.envelope_tag||'="http://schemas.xmlsoap.org/soap/envelope/" ' ||
               'xmlns:ship="http://shipment.services.operation.cuberoute.com">' ||
              '<'||p_request.envelope_tag||':Header/>' ||  
             '<'||p_request.envelope_tag||':Body>' ||
               '<'||p_request.method||'>'|| p_request.body ||
               '</'||p_request.method||'>' ||
             '</'||p_request.envelope_tag||':Body>' ||
           '</'||p_request.envelope_tag||':Envelope>';
END;

PROCEDURE show_envelope(p_env     IN  VARCHAR2,
                        p_heading IN  VARCHAR2 DEFAULT NULL) AS
  i      PLS_INTEGER;
  l_len  PLS_INTEGER;
BEGIN
  IF g_debug THEN
    IF p_heading IS NOT NULL THEN
      DBMS_OUTPUT.put_line('*****' || p_heading || '*****');
    END IF;

    i := 1; l_len := LENGTH(p_env);
    WHILE (i <= l_len) LOOP
      DBMS_OUTPUT.put_line(SUBSTR(p_env, i, 10000));
      i := i + 10000;
    END LOOP;
  END IF;
END;

PROCEDURE check_fault(p_response     IN OUT NOCOPY  t_response
                     ,p_xxwc_log_rec IN OUT xxwc_log_rec) 
AS
  l_fault_node    XMLTYPE;
  l_fault_code    VARCHAR2(256);
  l_fault_string  VARCHAR2(32767);
  l_error_code           NUMBER;
  p_error_msg            VARCHAR2(2000);
  l_sec                  VARCHAR2(2000);
BEGIN
  l_sec := 'Start check_fault';  
  l_fault_node := p_response.doc.extract('/'||p_response.envelope_tag||':Fault',
                                         'xmlns:'||p_response.envelope_tag||'="http://schemas.xmlsoap.org/soap/envelope/');

  IF (l_fault_node IS NOT NULL) THEN
    l_sec := 'Inside l_fault_node condition';  
    l_fault_code   := l_fault_node.extract('/'||p_response.envelope_tag||':Fault/faultcode/child::text()',
                                           'xmlns:'||p_response.envelope_tag||'="http://schemas.xmlsoap.org/soap/envelope/').getstringval();
    l_fault_string := l_fault_node.extract('/'||p_response.envelope_tag||':Fault/faultstring/child::text()',
                                           'xmlns:'||p_response.envelope_tag||'="http://schemas.xmlsoap.org/soap/envelope/').getstringval();
   
    fnd_file.put_line (fnd_file.log, 'l_fault_code : '|| l_fault_code);
    fnd_file.put_line (fnd_file.log, 'l_fault_string : '|| l_fault_string);

    p_xxwc_log_rec.ws_responsecode := l_fault_code;
    p_xxwc_log_rec.ws_responsedescription := SUBSTR(l_fault_string, 1, 4000);
  END IF;
  l_sec := 'End check_fault';  
EXCEPTION
WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the check_fault procedure'||SQLERRM;
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
      
      write_output ('Error in check_fault is '||l_error_code);
      fnd_file.put_line(fnd_file.log, 'Exception Block of check_fault Procedure');
      fnd_file.put_line(fnd_file.log,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log,'The Error Message Traced is : ' || p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.check_fault',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	 
END;

PROCEDURE check_success_response(p_response     IN OUT NOCOPY  t_response
                        ,p_xxwc_log_rec IN OUT xxwc_log_rec) 
AS
  l_success_node         XMLTYPE;
  l_success_code         VARCHAR2(256);
  l_success_description  VARCHAR2(32767);
  
  l_error_code           NUMBER;
  p_error_msg            VARCHAR2(2000);
  l_sec                  VARCHAR2(2000);
BEGIN
  l_sec := 'Start check_success_response';  
  l_success_node := p_response.doc.extract('/'||'ns2:'||'shipmentResponse',
                                         'xmlns:'||'ns2'||'="http://shipment.services.operation.cuberoute.com');

  IF (l_success_node IS NOT NULL) THEN
    l_sec := 'Inside l_success_node condition';
    l_success_code   := l_success_node.extract('/'||'ns2:'||'shipmentResponse/return/response/responseCode/child::text()',
                                           'xmlns:'||'ns2'||'="http://shipment.services.operation.cuberoute.com').getstringval();
    fnd_file.put_line (fnd_file.log, 'l_success_code : '|| l_success_code);
    p_xxwc_log_rec.ws_responsecode := l_success_code;   
    /**l_success_description  := l_success_node.extract('/'||'ns2:'||'shipmentResponse/return/response/responseDescription/child::text()',
                                           'xmlns:'||'ns2'||'="http://shipment.services.operation.cuberoute.com').getstringval();
                                                                                      
    write_output('l_success_description : '|| l_success_description);
    p_xxwc_log_rec.ws_responsedescription := SUBSTR(l_success_description, 1, 3500);**/
  END IF;
  l_sec := 'End check_success_response';                                  
EXCEPTION
WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the check_success_response procedure'||SQLERRM;
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
      
      write_output ('Error in check_success_response is '||l_error_code );
      fnd_file.put_line(fnd_file.log, 'Exception Block of check_success_response Procedure');
      fnd_file.put_line(fnd_file.log,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log,'The Error Message Traced is : ' || p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.check_success_response',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	 
END;

PROCEDURE invoke( p_request      IN OUT NOCOPY  t_request
                 ,p_url          IN             VARCHAR2
                 ,p_action       IN             VARCHAR2
                 ,p_xxwc_log_rec IN OUT xxwc_log_rec
                ) 
AS
  l_envelope       CLOB;
  l_http_request   UTL_HTTP.req;
  l_http_response  UTL_HTTP.resp;
  l_response       t_response;
  --lv_wallet_path      VARCHAR2(100) := 'file:/obase/ebsdev/admin/EBSDEV/wallet';
  -- lv_wallet_pwd       VARCHAR2(30) := 'Summer101';
  
  l_error_code               NUMBER;
  p_error_msg                VARCHAR2(4000);
  l_sec                      VARCHAR2 (4000);
  
  l_req_length      BINARY_INTEGER;
  l_amount          PLS_INTEGER := 2000; 
  l_offset          PLS_INTEGER := 1;
  l_buffer          VARCHAR2(4000);
  
  l_resp_length     BINARY_INTEGER;
  l_resp_envelope   CLOB;
BEGIN
  l_sec := 'Srart invoke';
  
  fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
  write_output('p_url : '|| p_url);

  generate_envelope(p_request, l_envelope);
  show_envelope(l_envelope, 'Request');
  p_xxwc_log_rec.request_payload := l_envelope;
  
  l_sec := 'Before set_wallet';
  --Setting the certificate by accessing the wallet
  UTL_HTTP.set_wallet(g_wallet_path);
  
  l_sec := 'Before begin_request';
  l_http_request := UTL_HTTP.begin_request(p_url, 'POST','HTTP/1.1');
  UTL_HTTP.set_header(l_http_request, 'Content-Type', 'text/xml');
  
  l_req_length := LENGTH(l_envelope); 
  write_output('l_req_length : '|| l_req_length);
  
  l_sec := 'Before write_text';
  --If message data is under 32kb limit
  IF l_req_length <= 32767
  THEN
     UTL_HTTP.set_header(l_http_request, 'Content-Length', LENGTH(l_envelope));
     UTL_HTTP.set_header(l_http_request, 'SOAPAction', p_action);
     UTL_HTTP.write_text(l_http_request, l_envelope);
  ELSE
     --If message data is more than 32kb limit
     UTL_HTTP.set_header(l_http_request, 'Transfer-Encoding', 'Chunked');
     
     WHILE(l_offset < l_req_length) 
     LOOP
        DBMS_LOB.read(l_envelope
                     ,l_amount
                     ,l_offset
                     ,l_buffer 
                     );
        UTL_HTTP.write_text(l_http_request, l_buffer);
        l_offset := l_offset + l_amount;    
     END LOOP;

  END IF;   
  
  l_sec := 'Before UTL_HTTP.get_response';  
  
  write_output('Before UTL_HTTP.get_response '); 
  l_http_response := UTL_HTTP.get_response(l_http_request);
  
  --Initialize the CLOB.
  DBMS_LOB.createtemporary(l_resp_envelope,TRUE);
  
  l_sec := 'Before loop to read get_response';  
  --Copy the response into the CLOB.
  BEGIN
    LOOP
      --Reading the text from the http_resp
      UTL_HTTP.read_text(l_http_response, l_buffer, 32767);
      DBMS_LOB.writeappend(l_resp_envelope, LENGTH(l_buffer), l_buffer);
    END LOOP;
  EXCEPTION
  WHEN UTL_HTTP.end_of_body THEN
    UTL_HTTP.end_response(l_http_response);
  END;
  
  l_sec := 'After loop to read get_response';
  
  --print the response 
  show_envelope(l_resp_envelope, 'Response');
  
  --assign to insert in the log table
  p_xxwc_log_rec.response_payload   := l_resp_envelope;
  p_xxwc_log_rec.http_status_code   := l_http_response.status_code;
  p_xxwc_log_rec.http_reason_phrase := l_http_response.reason_phrase;
  
  l_response.doc := XMLTYPE.createxml(l_resp_envelope);
  l_response.envelope_tag := p_request.envelope_tag;
  
  show_envelope(l_envelope, 'Request');
    
  -- Verify the response status and text
  write_output('Response Status: '||l_http_response.status_code||' ' || l_http_response.reason_phrase);
                   
  l_response.doc := l_response.doc.extract('/'||l_response.envelope_tag||':Envelope/'||l_response.envelope_tag||':Body/child::node()',
                                           'xmlns:'||l_response.envelope_tag||'="http://schemas.xmlsoap.org/soap/envelope/"');
  --print the response 
  l_sec := 'Before check_fault';  
  check_fault(l_response, p_xxwc_log_rec );
  
  l_sec := 'Before check_success_response';  
  check_success_response(l_response, p_xxwc_log_rec );
  
  DBMS_LOB.freetemporary(l_resp_envelope);
  l_sec := 'end invoke';
EXCEPTION
WHEN utl_http.request_failed THEN
   g_err_msg                             := SUBSTR(UTL_HTTP.GET_DETAILED_SQLERRM,1,255);
   p_xxwc_log_rec.ws_responsecode        := 'ERROR';
   p_xxwc_log_rec.ws_responsedescription := g_err_msg;
   write_output( 'Inside UTL_HTTP.request_failed' || g_err_msg);
WHEN UTL_HTTP.http_server_error THEN
   g_err_msg                             := SUBSTR(UTL_HTTP.GET_DETAILED_SQLERRM,1,255);
   p_xxwc_log_rec.ws_responsecode        := 'ERROR';
   p_xxwc_log_rec.ws_responsedescription := g_err_msg;
   write_output( 'Inside UTL_HTTP.http_server_error' || g_err_msg);
WHEN UTL_HTTP.http_client_error THEN
   g_err_msg                             := SUBSTR(UTL_HTTP.GET_DETAILED_SQLERRM,1,255);
   p_xxwc_log_rec.ws_responsecode        := 'ERROR';
   p_xxwc_log_rec.ws_responsedescription := g_err_msg;
   write_output( 'Inside UTL_HTTP.http_client_error' || g_err_msg);
WHEN UTL_HTTP.end_of_body THEN
  UTL_HTTP.END_RESPONSE(l_http_response);  
   g_err_msg                             := 'Inside UTL_HTTP.end_of_body';
   p_xxwc_log_rec.ws_responsecode        := 'ERROR';
   p_xxwc_log_rec.ws_responsedescription := g_err_msg;
   write_output(g_err_msg);
WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the invoke procedure'||SQLERRM;
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
      
      write_output ('Error in check_success_response is '||l_error_code );
      fnd_file.put_line(fnd_file.log, 'Exception Block of invoke Procedure');
      fnd_file.put_line(fnd_file.log,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log,'The Error Message Traced is : ' || p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.invoke',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	 
END;

FUNCTION get_return_value(p_response   IN OUT NOCOPY  t_response,
                          p_name       IN             VARCHAR2,
                          p_namespace  IN             VARCHAR2)
  RETURN VARCHAR2 AS
BEGIN
  RETURN p_response.doc.extract('//'||p_name||'/child::text()',p_namespace).getstringval();
END;

PROCEDURE debug_on AS
BEGIN
  g_debug := TRUE;
END;

PROCEDURE debug_off AS
BEGIN
  g_debug := FALSE;
END;

PROCEDURE ShipmentSvc_std_int_ord  (p_order_number      IN NUMBER,
                                    p_sch_delivery_date IN DATE,
                                    p_delivery_id       IN NUMBER,
                                    p_request_id        IN NUMBER,
                                    p_priority          IN VARCHAR2, --TMS# 20180213-00090
                                    p_xxwc_log_rec      IN OUT xxwc_log_rec )
AS
  l_request   XXWC_OM_DMS2_OB_PKG.t_request;
  l_response  XXWC_OM_DMS2_OB_PKG.t_response;
  l_return                   VARCHAR2(32767);
  l_error_code               NUMBER;
  p_error_msg                VARCHAR2(2000);
  l_sec                      VARCHAR2 (2000);
  
  l_url                      VARCHAR2(32767);
  l_namespace                VARCHAR2(32767);
  l_method                   VARCHAR2(32767);
  l_soap_action              VARCHAR2(32767);
  l_result_name              VARCHAR2(32767);
  l_add_header_info          BOOLEAN := TRUE;
  l_add_toclob_info          BOOLEAN := TRUE;
  l_print_invoice_tag        BOOLEAN := TRUE;
  
  l_order_number             NUMBER := -1;
  l_ordered_date             VARCHAR2(200);
  l_cust_po_number           VARCHAR2(200);
  
  l_invoice_close            VARCHAR2(32767); 
  l_footer_clob              CLOB; 
  
  l_data_found               BOOLEAN := FALSE; -- to check if the data exists
  l_dms_branch_city          fnd_lookup_values.meaning%TYPE;
  l_ada_flag                 VARCHAR2(1);--Rev#3.0
  l_cd_dms_branch_city       fnd_lookup_values.description%TYPE;--Rev#5.0
  CURSOR c_order_info ( p_order_number IN NUMBER
                       ,p_delivery_id  IN NUMBER )
   IS
   SELECT SUBSTR(hl.location_code, 1, INSTR(hl.location_code, ' ') - 1) branch_code
      ,ooh.order_number ORDER_NUMBER
      ,wsh_stg.delivery_id
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (party.party_name) CUSTOMER_NAME
      ,cust_acct.account_number CUSTOMER_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_su.location) JOB_SITE_NAME
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address1) JOB_SITE_ADD
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address2) JOB_SITE_ADD_CROSS_STREET1
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address3) JOB_SITE_ADD_CROSS_STREET2
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.city) JOB_SITE_ADD_CITY
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.state) JOB_SITE_ADD_STATE
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.postal_code) JOB_SITE_ADD_ZIP
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (XXWC_OM_DMS2_OB_PKG.get_job_site_contact (p_ship_to_contact_id => ooh.ship_to_contact_id)) JOB_SITE_PHONE_NUMBER --Rev#6.0
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters ( ship_party.person_title || '  '|| ship_party.PERSON_FIRST_NAME || '  '|| ship_party.PERSON_LAST_NAME) JOB_SITE_CONTACT
      ,TO_CHAR(ooh.ordered_date,'YYYY-MM-DD')||'T'||to_char(ooh.ordered_date,'HH24:MI:SS')||'Z' ORDERED_DATE
      ,TO_CHAR(p_sch_delivery_date,'YYYY-MM-DD')||'T'||to_char(p_sch_delivery_date,'HH24:MI:SS')||'Z' SCHEDULED_DELIVERY_DATE
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ooh.cust_po_number) CUST_PO_NUMBER
      ,(ool.line_number||'.'||ool.shipment_number) line_number
      --ool.line_number
      ,ool.ordered_item ITEM_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (msib.description)  ITEM_DESCRIPTION
      ,ool.order_quantity_uom
      --,msib.unit_weight ITEM_UNIT_WEIGHT
      ,NVL(wsh_stg.transaction_qty, 0) * NVL(msib.unit_weight, 0) ITEM_UNIT_WEIGHT
      ,NVL(wsh_stg.transaction_qty, 0) SHIPMENT_QUANTITY
      ,ool.order_quantity_uom ITEM_UOM
      ,TRIM(REGEXP_REPLACE(ASCIISTR(XXWC_OM_DMS2_OB_PKG.replace_special_characters (ooh.shipping_instructions)),'\\[[:xdigit:]]{4}','')) SHIPPING_INSTRUCTIONS
      ,ot.name order_type
      ,ooh.attribute9 ADA_AddressLine1 --Rev#3.0
      ,ooh.attribute10 ADA_City --Rev#3.0
      ,ooh.attribute14 ADA_State --Rev#3.0
      ,ooh.attribute15 ADA_Zip  --Rev#3.0
     FROM oe_order_headers_all ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , oe_order_lines_all ool
       , mtl_system_items_b msib
       , xxwc_wsh_shipping_stg wsh_stg
      WHERE ooh.header_id                   = ool.header_id
        AND msib.inventory_item_id          = ool.inventory_item_id
        AND msib.organization_id            = ool.ship_from_org_id
        AND wsh_stg.header_id               = ooh.header_id
        AND wsh_stg.line_id                 = ool.line_id 
        AND ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND ool.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = 'CONTACT'
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ool.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND ooh.order_number                = p_order_number
        AND wsh_stg.delivery_id             = p_delivery_id
        AND ool.user_item_description       = 'OUT_FOR_DELIVERY'
        AND ool.source_type_code            = 'INTERNAL'
        --AND wsh_stg.delivery_id             = NVL(p_delivery_id, wsh_stg.delivery_id)
        AND EXISTS                          ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS2_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                             )
        ORDER BY ooh.order_number, wsh_stg.delivery_id;
BEGIN
  l_sec := 'Srart ShipmentSvc_std_int_ord p_order_number: ' ||p_order_number || ' p_delivery_id: '||p_delivery_id;
  l_url         := g_dms2_shipmentsvc_url;--'https://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl';
  l_namespace   := NULL;--'xmlns="http://shipment.services.operation.cuberoute.com"';
  l_method      := 'ship:shipment';
  --l_soap_action := 'http://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl/ship';
  l_soap_action := g_dms2_shipmentsvc_url||'/ship';
  l_result_name := 'return';
  l_ada_flag := 'N';--Rev#3.0
  
  mo_global.set_policy_context('S',162);
  
  l_request := XXWC_OM_DMS2_OB_PKG.new_request( p_method       => l_method,
                                                     p_namespace    => l_namespace);

  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<request>');
  
  l_sec := 'Before ShipmentSvc_std_int_ord';                                                  
  FOR r_order_info IN c_order_info ( p_order_number => p_order_number
                                    ,p_delivery_id  => p_delivery_id)
  LOOP    
     l_data_found := TRUE; -- set to true if to generate xml file
     l_sec := 'Inside r_order_info p_order_number : '|| r_order_info.order_number|| ' Delivery_id : '||r_order_info.Delivery_id ||' LINE_NUMBER '|| r_order_info.LINE_NUMBER || ' Item Number : '|| r_order_info.Item_number;
     write_output (l_sec);
     
     l_ordered_date    := r_order_info.ordered_date;
     l_cust_po_number  := r_order_info.cust_po_number; 
     IF l_add_header_info = TRUE THEN
        l_add_header_info := FALSE;
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'operationCode',
                         p_value   => 'Edit');
 
        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                       p_xml    => '<address>');
        
        --Rev#3.0 <Start
        IF IS_SO_ADA_EXISTS ( p_order_number => r_order_info.order_number) = 'Y' THEN 
           l_ada_flag := 'Y'; 
        
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'addressLine1',
                            p_value   => r_order_info.ADA_AddressLine1);
                            
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'city',
                            p_value   => r_order_info.ADA_CITY);
                            
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'postalCode',
                            p_value   => r_order_info.ADA_Zip);
 
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'stateCode',
                            p_value   => r_order_info.ADA_State);                                                        
        ELSE --Rev#3.0 >End
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'addressLine1',
                            p_value   => r_order_info.JOB_SITE_ADD);
        
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'city',
                            p_value   => r_order_info.JOB_SITE_ADD_CITY);
  
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'phoneNumber1',
                            p_value   => r_order_info.JOB_SITE_PHONE_NUMBER);

           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'postalCode',
                            p_value   => r_order_info.JOB_SITE_ADD_ZIP);
 
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'stateCode',
                            p_value   => r_order_info.JOB_SITE_ADD_STATE);
                                                                       
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'crossStreet1',
                            p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET1);
                         
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'crossStreet2',
                            p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET2);
        END IF; --Rev#3.0                    
                                                         
        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</address>');
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerName2',
                         p_value   => r_order_info.JOB_SITE_NAME);    
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerCompanyName',
                         p_value   => r_order_info.CUSTOMER_NAME);   
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerID',
                         p_value   => r_order_info.CUSTOMER_NUMBER);     

     END IF;                                                   

     IF l_order_number <> r_order_info.ORDER_NUMBER THEN
        --Check if delivery is different and it is not a first record, then close the invoice element    
        IF l_order_number <> -1 THEN
          --start close the invoices element        
          XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</invoices>');
                                                   
          --end close the invoices element   
        END IF;
        
        l_order_number := r_order_info.ORDER_NUMBER;
        l_print_invoice_tag := TRUE;
             
        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<invoices>'); 
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ORDER_NUMBER);   
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => r_order_info.JOB_SITE_CONTACT); 
    
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'comment',
                         p_value   => r_order_info.SHIPPING_INSTRUCTIONS);
     END IF;                  

     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<items>'); 
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ITEM_NUMBER);   
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom2',
                         p_value   => r_order_info.LINE_NUMBER); 
                             
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'description',
                         p_value   => r_order_info.ITEM_DESCRIPTION);

     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'pieceCount',
                         p_value   => r_order_info.SHIPMENT_QUANTITY); 
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'weight',
                         p_value   => r_order_info.ITEM_UNIT_WEIGHT);  
                                                                      
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => r_order_info.item_uom);   
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'UOM');
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');
       
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</items>'); 
     
     IF l_print_invoice_tag = TRUE THEN
        l_print_invoice_tag := FALSE;
        
        l_invoice_close := NULL;
        
        --start close the invoices element        
        --delivery_id as attribute value -- invoices -->attributes
        XXWC_OM_DMS2_OB_PKG.add_toclob_complex_parameter(p_footer_clob => l_invoice_close ,
                                                                   p_xml    => '</invoices>');
        --end close the invoices element      
     END IF;

     IF l_add_toclob_info = TRUE THEN
        l_add_toclob_info := FALSE;
        
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'shipmentType',
                         p_value   => 'CustomerDelivery');  
        
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'internalOrderId',
                         p_value   => r_order_info.delivery_id);  
        
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'scheduledDeliveryDate',
                         p_value   => r_order_info.SCHEDULED_DELIVERY_DATE);   
        
        fnd_file.put_line(fnd_file.log, 'branch code : '|| r_order_info.branch_code);
       
        l_dms_branch_city := XXWC_OM_DMS2_OB_PKG.GET_DMS_ORG( p_branch_code => r_order_info.branch_code);
       
        fnd_file.put_line(fnd_file.log, 'l_dms_branch_city : '|| l_dms_branch_city);
        
        --Rev#5.0 <Start
        l_cd_dms_branch_city := XXWC_OM_DMS2_OB_PKG.GET_DMS2_CENTRAL_DPATCHG_ORG( p_branch_code => r_order_info.branch_code);
        
        fnd_file.put_line(fnd_file.log, 'l_cd_dms_branch_city : '|| l_cd_dms_branch_city);
        
        IF l_cd_dms_branch_city IS NOT NULL THEN
           XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'sellingStoreNumber',
                         p_value   => l_cd_dms_branch_city);
        ELSE--Rev#5.0 >End
           XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'sellingStoreNumber',
                         p_value   => l_dms_branch_city);
        END IF;--Rev#5.0                 
                         
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'fulfillingStoreNumber',
                         p_value   => l_dms_branch_city);                                                                                                                                   
     END IF ;  
     l_sec := 'End Loop r_order_info';                                                                                   
  END LOOP;
  
  l_sec := 'Afer Loop r_order_info';
  
  l_request.body := l_request.body||l_invoice_close;
  
  l_request.body := l_request.body||l_footer_clob;
  
  --orderd date as attribute value -- request -->attributes
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_ordered_date);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Ordered Date');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                                                   
  --cust_po_number as attribute value-- request -->attributes
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_cust_po_number);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Cust PO #');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
  --Order Type                              
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_xxwc_log_rec.ORDER_TYPE);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Order type');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');                                                  
  --Request id                              
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_request_id);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Request id');
                                                  
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');    
                                                                      
  --Rev#3.0 <Start 
  --ADA                              
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_ada_flag);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'ADA');
                         
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');   

  --Priority                              
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  
  IF p_priority = 'Y' THEN
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'value',
                            p_value   => 'Y');
  ELSE
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'value',
                            p_value   => 'N');
  END IF;                          
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Priority');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                             p_xml    => '</attributes>'); 
                                                   
  --Rev#3.0 > End
  
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'username',
                         p_value   => g_dms2_username);                       
        
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'passwordHash',
                         p_value   => g_dms2_passwordHash); 
                         
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'organizationKey',
                         p_value   => g_dms2_organizationkey);   
                         
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</request>');

  --Set the body xml to NULL if there is no data found
  IF l_data_found = FALSE THEN
     l_request.body := NULL; 
     p_xxwc_log_rec.ws_responsecode := 'NO_DATA_FOUND';   
     fnd_file.put_line (fnd_file.log, 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called');

     g_retcode := 1;
     g_err_msg := 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called';
  ELSE   
     XXWC_OM_DMS2_OB_PKG.invoke( p_request => l_request
                                     ,p_url     => l_url
                                     ,p_action  => l_soap_action
                                     ,p_xxwc_log_rec => p_xxwc_log_rec);
  END IF;                                   
  l_sec := 'End procedure ShipmentSvc_std_int_ord';  
   
EXCEPTION
WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the ShipmentSvc_std_int_ord procedure'||SQLERRM;
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
      
      write_output ('Error in ShipmentSvc_std_int_ord is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
      fnd_file.put_line(fnd_file.log, 'Exception Block of ShipmentSvc_std_int_ord Procedure');
      fnd_file.put_line(fnd_file.log,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log,'The Error Message Traced is : ' || p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.ShipmentSvc_std_int_ord',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  
  END ShipmentSvc_std_int_ord;

PROCEDURE ShipmentSvc_rental_ord  ( p_order_number      IN NUMBER
                                   ,p_shipment_type     IN VARCHAR2 
                                   ,p_line_type_id      IN NUMBER  
                                   ,p_flow_status_code  IN VARCHAR2     
                                   ,p_sch_delivery_date IN DATE
                                   ,p_rental_return     IN NUMBER DEFAULT NULL
                                   ,p_request_id        IN NUMBER
                                   ,p_xxwc_log_rec      IN OUT xxwc_log_rec
                                   )
AS
  l_request   XXWC_OM_DMS2_OB_PKG.t_request;
  l_response  XXWC_OM_DMS2_OB_PKG.t_response;
  l_return                   VARCHAR2(32767);
  l_error_code    NUMBER;
  p_error_msg     VARCHAR2(2000);
  l_sec           VARCHAR2 (2000);
	
  l_url                      VARCHAR2(32767);
  l_namespace                VARCHAR2(32767);
  l_method                   VARCHAR2(32767);
  l_soap_action              VARCHAR2(32767);
  l_result_name              VARCHAR2(32767);
  l_add_header_info          BOOLEAN := TRUE;
  l_add_toclob_info          BOOLEAN := TRUE;
  
  l_ordered_date             VARCHAR2(200);
  l_cust_po_number           VARCHAR2(200);
  
  l_footer_clob              CLOB; 
  l_data_found               BOOLEAN := FALSE; -- to check if the data exists
  l_dms_branch_city          fnd_lookup_values.meaning%TYPE;
  l_cd_dms_branch_city       fnd_lookup_values.description%TYPE;--Rev#5.0
  
  CURSOR c_order_info ( p_order_number  IN NUMBER 
                       ,p_line_type_id  IN NUMBER
                       ,p_flow_status_code  IN VARCHAR2 
                       ,p_rental_return IN NUMBER
                      )
   IS
   SELECT SUBSTR(hl.location_code, 1, INSTR(hl.location_code, ' ') - 1) branch_code
      ,ooh.order_number ORDER_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (party.party_name) CUSTOMER_NAME
      ,cust_acct.account_number CUSTOMER_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_su.location) JOB_SITE_NAME
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address1) JOB_SITE_ADD
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address2) JOB_SITE_ADD_CROSS_STREET1
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address3) JOB_SITE_ADD_CROSS_STREET2
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.city) JOB_SITE_ADD_CITY
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.state) JOB_SITE_ADD_STATE
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.postal_code) JOB_SITE_ADD_ZIP
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters ( XXWC_OM_DMS2_OB_PKG.get_job_site_contact (p_ship_to_contact_id => ooh.ship_to_contact_id)) JOB_SITE_PHONE_NUMBER --Rev#6.0
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters ( ship_party.person_title || '  '|| ship_party.PERSON_FIRST_NAME || '  '|| ship_party.PERSON_LAST_NAME) JOB_SITE_CONTACT
      ,TO_CHAR(ooh.ordered_date,'YYYY-MM-DD')||'T'||to_char(ooh.ordered_date,'HH24:MI:SS')||'Z' ORDERED_DATE
      ,TO_CHAR(p_sch_delivery_date,'YYYY-MM-DD')||'T'||to_char(p_sch_delivery_date,'HH24:MI:SS')||'Z' SCHEDULED_DELIVERY_DATE
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ooh.cust_po_number) CUST_PO_NUMBER
      --,ool.line_number
      ,(ool.line_number||'.'||ool.shipment_number) line_number
      ,ool.ordered_item ITEM_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (msib.description)  ITEM_DESCRIPTION
      ,ool.order_quantity_uom
      ,(NVL(ool.ordered_quantity, 0) * NVL(msib.unit_weight, 0)) ITEM_UNIT_WEIGHT
      ,NVL(ool.ordered_quantity, 0) SHIPMENT_QUANTITY
      ,ool.order_quantity_uom ITEM_UOM
      ,TRIM(REGEXP_REPLACE(ASCIISTR(XXWC_OM_DMS2_OB_PKG.replace_special_characters (ooh.shipping_instructions)),'\\[[:xdigit:]]{4}','')) SHIPPING_INSTRUCTIONS
      ,ot.name order_type
   FROM oe_order_headers_all ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , oe_order_lines_all ool
       , mtl_system_items_b msib       
      WHERE ooh.header_id                   = ool.header_id
        AND msib.inventory_item_id          = ool.inventory_item_id
        AND msib.organization_id            = ool.ship_from_org_id
        AND ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND ool.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = 'CONTACT'
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ool.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND ool.source_type_code            = 'INTERNAL'
        AND ooh.order_number                = p_order_number
        AND ool.line_type_id                = p_line_type_id   
        AND ool.flow_status_code            = NVL(p_flow_status_code , ool.flow_status_code) 
        AND EXISTS                          ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS2_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                             )
    UNION
      SELECT SUBSTR(hl.location_code, 1, INSTR(hl.location_code, ' ') - 1) branch_code
      ,ooh.order_number ORDER_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (party.party_name) CUSTOMER_NAME
      ,cust_acct.account_number CUSTOMER_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_su.location) JOB_SITE_NAME
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address1) JOB_SITE_ADD
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address2) JOB_SITE_ADD_CROSS_STREET1
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address3) JOB_SITE_ADD_CROSS_STREET2
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.city) JOB_SITE_ADD_CITY
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.state) JOB_SITE_ADD_STATE
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.postal_code) JOB_SITE_ADD_ZIP
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters ( XXWC_OM_DMS2_OB_PKG.get_job_site_contact (p_ship_to_contact_id => ooh.ship_to_contact_id)) JOB_SITE_PHONE_NUMBER --Rev#6.0
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters ( ship_party.person_title || '  '|| ship_party.PERSON_FIRST_NAME || '  '|| ship_party.PERSON_LAST_NAME) JOB_SITE_CONTACT
      ,TO_CHAR(ooh.ordered_date,'YYYY-MM-DD')||'T'||to_char(ooh.ordered_date,'HH24:MI:SS')||'Z' ORDERED_DATE
      ,TO_CHAR(p_sch_delivery_date,'YYYY-MM-DD')||'T'||to_char(p_sch_delivery_date,'HH24:MI:SS')||'Z' SCHEDULED_DELIVERY_DATE
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ooh.cust_po_number) CUST_PO_NUMBER
      --,ool.line_number
      ,(ool.line_number||'.'||ool.shipment_number) line_number
      ,ool.ordered_item ITEM_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (msib.description)  ITEM_DESCRIPTION
      ,ool.order_quantity_uom
      ,(NVL(ool.ordered_quantity, 0) * NVL(msib.unit_weight, 0)) ITEM_UNIT_WEIGHT
      ,NVL(ool.ordered_quantity, 0) SHIPMENT_QUANTITY
      ,ool.order_quantity_uom ITEM_UOM
      ,TRIM(REGEXP_REPLACE(ASCIISTR(XXWC_OM_DMS2_OB_PKG.replace_special_characters (ooh.shipping_instructions)),'\\[[:xdigit:]]{4}','')) SHIPPING_INSTRUCTIONS
      ,ot.name order_type
   FROM oe_order_headers_all ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , oe_order_lines_all ool
       , mtl_system_items_b msib  
       , fnd_lookup_values flv     
      WHERE ooh.header_id                   = ool.header_id
        AND msib.inventory_item_id          = ool.inventory_item_id
        AND msib.organization_id            = ool.ship_from_org_id
        AND ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND ool.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = 'CONTACT'
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ool.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND ooh.order_number                = p_order_number
        --AND ool.line_type_id                = p_line_type_id  
        AND flv.meaning                     = msib.segment1
        AND flv.lookup_type                 = 'XXWC_DMS2_RENTAL_ITEMS'
        AND ool.flow_status_code            <> 'CLOSED'
        AND flv.enabled_flag                = 'Y'
        AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
        AND 1                               =  p_rental_return
        AND EXISTS                          ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS2_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                             );        
BEGIN
  l_sec         := 'Srart ShipmentSvc_rental_ord p_order_number: ' ||p_order_number;
  l_url         := g_dms2_shipmentsvc_url;--'https://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl';
  l_namespace   := NULL;--'xmlns="http://shipment.services.operation.cuberoute.com"';
  l_method      := 'ship:shipment';
  --l_soap_action := 'http://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl/ship';
  l_soap_action := g_dms2_shipmentsvc_url||'/ship';
  l_result_name := 'return';

  mo_global.set_policy_context('S',162);
  
  l_request := XXWC_OM_DMS2_OB_PKG.new_request( p_method       => l_method,
                                                     p_namespace    => l_namespace);

  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<request>');
  l_sec := 'Before r_order_info';        
          
  FOR r_order_info IN c_order_info ( p_order_number     => p_order_number
                                    ,p_line_type_id     => p_line_type_id
                                    ,p_flow_status_code => p_flow_status_code
                                    ,p_rental_return    => p_rental_return
                                   )
  LOOP  
     l_data_found := TRUE; -- set to true if to generate xml file
     l_sec := 'Inside r_order_info p_order_number : '|| r_order_info.order_number|| ' LINE_NUMBER '|| r_order_info.LINE_NUMBER || ' Item Number : '|| r_order_info.Item_number;
     write_output (l_sec);

     l_ordered_date    := r_order_info.ordered_date;
     l_cust_po_number  := r_order_info.cust_po_number; 
     IF l_add_header_info = TRUE THEN
        l_add_header_info := FALSE;
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'operationCode',
                         p_value   => 'Edit');
 
        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                       p_xml    => '<address>');
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'addressLine1',
                            p_value   => r_order_info.JOB_SITE_ADD);
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'city',
                            p_value   => r_order_info.JOB_SITE_ADD_CITY);
  
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'phoneNumber1',
                            p_value   => r_order_info.JOB_SITE_PHONE_NUMBER);

        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'postalCode',
                            p_value   => r_order_info.JOB_SITE_ADD_ZIP);
 
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'stateCode',
                            p_value   => r_order_info.JOB_SITE_ADD_STATE);
                                                                       
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'crossStreet1',
                            p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET1);
                         
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'crossStreet2',
                            p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET2);
                                                         
        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</address>');
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerName2',
                         p_value   => r_order_info.JOB_SITE_NAME);    
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerCompanyName',
                         p_value   => r_order_info.CUSTOMER_NAME);   
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerID',
                         p_value   => r_order_info.CUSTOMER_NUMBER);     

        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<invoices>'); 
        --Order number, job site contact and comment should be printed only one time
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ORDER_NUMBER);   
                         
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                          p_name    => 'name',
                         p_value   => r_order_info.JOB_SITE_CONTACT); 
    
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'comment',
                         p_value   => r_order_info.SHIPPING_INSTRUCTIONS);   
    END IF;                                                   

     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<items>'); 
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ITEM_NUMBER); 
                           
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom2',
                         p_value   => r_order_info.LINE_NUMBER);                          
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'description',
                         p_value   => r_order_info.ITEM_DESCRIPTION);

     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'pieceCount',
                         p_value   => r_order_info.SHIPMENT_QUANTITY); 
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'weight',
                         p_value   => r_order_info.ITEM_UNIT_WEIGHT);  
                                                                      
     --item_uom as attribute value -- items -->item_uom    
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => r_order_info.item_uom);   
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'UOM');
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');
       
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</items>'); 
     
     IF l_add_toclob_info = TRUE THEN
        l_add_toclob_info := FALSE;
        XXWC_OM_DMS2_OB_PKG.add_toclob_complex_parameter(p_footer_clob => l_footer_clob ,
                                                   p_xml    => '</invoices>');      
        
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'shipmentType',
                         p_value   => p_shipment_type);  
        
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'scheduledDeliveryDate',
                         p_value   => r_order_info.SCHEDULED_DELIVERY_DATE);   
        
       fnd_file.put_line(fnd_file.log, 'branch code : '|| r_order_info.branch_code);
       
       l_dms_branch_city := XXWC_OM_DMS2_OB_PKG.GET_DMS_ORG( p_branch_code => r_order_info.branch_code);
       
       fnd_file.put_line(fnd_file.log, 'l_dms_branch_city : '|| l_dms_branch_city);

        --Rev#5.0 <Start
       l_cd_dms_branch_city := XXWC_OM_DMS2_OB_PKG.GET_DMS2_CENTRAL_DPATCHG_ORG( p_branch_code => r_order_info.branch_code);
        
       fnd_file.put_line(fnd_file.log, 'l_cd_dms_branch_city : '|| l_cd_dms_branch_city);
        
       IF l_cd_dms_branch_city IS NOT NULL THEN
          XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                        p_name    => 'sellingStoreNumber',
                        p_value   => l_cd_dms_branch_city);
       ELSE--Rev#5.0 >End
          XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                        p_name    => 'sellingStoreNumber',
                        p_value   => l_dms_branch_city);
       END IF;--Rev#5.0  
                         
       XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'fulfillingStoreNumber',
                         p_value   => l_dms_branch_city);                                                                                                                                     
     END IF ;  
     l_sec := 'End loop r_order_info';                                                                                   
  END LOOP;
  
  l_sec := 'After loop r_order_info';
  
  l_request.body := l_request.body||l_footer_clob;
  
  --orderd date as attribute value -- request -->attributes
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_ordered_date);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Ordered Date');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                                                   
  --cust_po_number as attribute value-- request -->attributes
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_cust_po_number);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Cust PO #');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
  
  --Order Type                              
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_xxwc_log_rec.ORDER_TYPE);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Order type');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');   
                                                                      
  --Request id                              
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_request_id);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Request id');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                                                   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'username',
                         p_value   => g_dms2_username);                       
        
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'passwordHash',
                         p_value   => g_dms2_passwordHash); 
                         
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'organizationKey',
                         p_value   => g_dms2_organizationkey);   
                         
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</request>');

  --Set the body xml to NULL if there is no data found
  IF l_data_found = FALSE THEN
     l_request.body := NULL; 
     p_xxwc_log_rec.ws_responsecode := 'NO_DATA_FOUND';   
     fnd_file.put_line (fnd_file.log, 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called');
     g_retcode := 1;
     g_err_msg := 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called';
  ELSE   
     XXWC_OM_DMS2_OB_PKG.invoke( p_request => l_request
                                     ,p_url     => l_url
                                     ,p_action  => l_soap_action
                                     ,p_xxwc_log_rec => p_xxwc_log_rec);
  END IF;  

  l_sec := 'End ShipmentSvc_rental_ord';                                  
EXCEPTION
WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the ShipmentSvc_rental_ord procedure'||SQLERRM;
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
      
      write_output ('Error in ShipmentSvc_rental_ord is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
      fnd_file.put_line(fnd_file.log, 'Exception Block of ShipmentSvc_rental_ord Procedure');
      fnd_file.put_line(fnd_file.log,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log,'The Error Message Traced is : ' || p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.ShipmentSvc_rental_ord',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  
END ShipmentSvc_rental_ord;

PROCEDURE ShipmentSvc_return_ord  ( p_order_number      IN NUMBER
                                   ,p_shipment_type     IN VARCHAR2  
                                   ,p_sch_delivery_date IN DATE
                                   ,p_request_id        IN NUMBER
                                   ,p_xxwc_log_rec      IN OUT xxwc_log_rec
                                   )
AS
  l_request   XXWC_OM_DMS2_OB_PKG.t_request;
  l_response  XXWC_OM_DMS2_OB_PKG.t_response;
  l_return                   VARCHAR2(32767);
  l_error_code    NUMBER;
  p_error_msg     VARCHAR2(2000);
  l_sec           VARCHAR2 (2000);
	
  l_url                      VARCHAR2(32767);
  l_namespace                VARCHAR2(32767);
  l_method                   VARCHAR2(32767);
  l_soap_action              VARCHAR2(32767);
  l_result_name              VARCHAR2(32767);
  l_add_header_info          BOOLEAN := TRUE;
  l_add_toclob_info          BOOLEAN := TRUE;
  
  l_ordered_date             VARCHAR2(200);
  l_cust_po_number           VARCHAR2(200);
  
  l_footer_clob              CLOB; 
  l_data_found               BOOLEAN := FALSE; -- to check if the data exists
  l_dms_branch_city          fnd_lookup_values.meaning%TYPE;
  l_cd_dms_branch_city       fnd_lookup_values.description%TYPE;--Rev#5.0
  
  CURSOR c_order_info ( p_order_number IN NUMBER )
   IS
    SELECT SUBSTR(hl.location_code, 1, INSTR(hl.location_code, ' ') - 1) branch_code
      ,ooh.order_number ORDER_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (party.party_name) CUSTOMER_NAME
      ,cust_acct.account_number CUSTOMER_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_su.location) JOB_SITE_NAME
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address1) JOB_SITE_ADD
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address2) JOB_SITE_ADD_CROSS_STREET1
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address3) JOB_SITE_ADD_CROSS_STREET2
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.city) JOB_SITE_ADD_CITY
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.state) JOB_SITE_ADD_STATE
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.postal_code) JOB_SITE_ADD_ZIP
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters ( XXWC_OM_DMS2_OB_PKG.get_job_site_contact (p_ship_to_contact_id => ooh.ship_to_contact_id)) JOB_SITE_PHONE_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters ( ship_party.person_title || '  '|| ship_party.PERSON_FIRST_NAME || '  '|| ship_party.PERSON_LAST_NAME) JOB_SITE_CONTACT
      ,TO_CHAR(ooh.ordered_date,'YYYY-MM-DD')||'T'||to_char(ooh.ordered_date,'HH24:MI:SS')||'Z' ORDERED_DATE
      ,TO_CHAR(p_sch_delivery_date,'YYYY-MM-DD')||'T'||to_char(p_sch_delivery_date,'HH24:MI:SS')||'Z' SCHEDULED_DELIVERY_DATE
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ooh.cust_po_number) CUST_PO_NUMBER
      --,ool.line_number
      ,(ool.line_number||'.'||ool.shipment_number) line_number
      ,ool.ordered_item ITEM_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (msib.description)  ITEM_DESCRIPTION
      ,ool.order_quantity_uom
      ,(NVL(ool.ordered_quantity, 0) * NVL(msib.unit_weight, 0)) ITEM_UNIT_WEIGHT
      ,NVL(ool.ordered_quantity, 0) SHIPMENT_QUANTITY
      ,ool.order_quantity_uom ITEM_UOM
      ,TRIM(REGEXP_REPLACE(ASCIISTR(XXWC_OM_DMS2_OB_PKG.replace_special_characters (ooh.shipping_instructions)),'\\[[:xdigit:]]{4}','')) SHIPPING_INSTRUCTIONS
      ,ot.name order_type
  FROM oe_order_headers_all ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , oe_order_lines_all ool
       , mtl_system_items_b msib       
      WHERE ooh.header_id                   = ool.header_id
        AND msib.inventory_item_id          = ool.inventory_item_id
        AND msib.organization_id            = ool.ship_from_org_id
        AND ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND ool.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = 'CONTACT'
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ool.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND ool.source_type_code            = 'INTERNAL'
        AND ooh.order_number                = p_order_number
        AND ool.flow_status_code            NOT IN ( 'CANCELLED', 'CLOSED') 
        AND ool.line_type_id                = 1007 --RETURN WITH RECEIPT (1007) --Rev#3.0
        AND EXISTS                          ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS2_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                             );
BEGIN
  l_sec := 'Srart ShipmentSvc_return_ord p_order_number: ' ||p_order_number;

  l_url         := g_dms2_shipmentsvc_url;--'https://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl';
  l_namespace   := NULL;--'xmlns="http://shipment.services.operation.cuberoute.com"';
  l_method      := 'ship:shipment';
  --l_soap_action := 'http://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl/ship';
  l_soap_action := g_dms2_shipmentsvc_url||'/ship';
  l_result_name := 'return';
 
  mo_global.set_policy_context('S',162);
  
  l_request := XXWC_OM_DMS2_OB_PKG.new_request( p_method       => l_method,
                                                     p_namespace    => l_namespace);

  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<request>');
  
  l_sec := 'Before r_order_info';                                                 
  FOR r_order_info IN c_order_info ( p_order_number => p_order_number
                                   )
  LOOP    
    l_data_found := TRUE; -- set to true if to generate xml file
    l_sec := 'Inside r_order_info p_order_number : '|| r_order_info.order_number|| ' LINE_NUMBER '|| r_order_info.LINE_NUMBER || ' Item Number : '|| r_order_info.Item_number;
    write_output (l_sec);
     l_ordered_date    := r_order_info.ordered_date;
     l_cust_po_number  := r_order_info.cust_po_number; 
     IF l_add_header_info = TRUE THEN
        l_add_header_info := FALSE;
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'operationCode',
                         p_value   => 'Edit');
 
        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                       p_xml    => '<address>');
       
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'addressLine1',
                            p_value   => r_order_info.JOB_SITE_ADD);
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'city',
                            p_value   => r_order_info.JOB_SITE_ADD_CITY);
  
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'phoneNumber1',
                            p_value   => r_order_info.JOB_SITE_PHONE_NUMBER);

        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'postalCode',
                            p_value   => r_order_info.JOB_SITE_ADD_ZIP);
 
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'stateCode',
                            p_value   => r_order_info.JOB_SITE_ADD_STATE);
                                                                       
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'crossStreet1',
                            p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET1);
                         
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'crossStreet2',
                            p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET2);

        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</address>');
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerName2',
                         p_value   => r_order_info.JOB_SITE_NAME);    
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerCompanyName',
                         p_value   => r_order_info.CUSTOMER_NAME);   
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerID',
                         p_value   => r_order_info.CUSTOMER_NUMBER);     

        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<invoices>'); 
        --Order number, job site contact and comment should be printed only one time
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ORDER_NUMBER);   
                         
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                          p_name    => 'name',
                         p_value   => r_order_info.JOB_SITE_CONTACT); 
    
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'comment',
                         p_value   => r_order_info.SHIPPING_INSTRUCTIONS);   
    END IF;                                                   

     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<items>'); 
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ITEM_NUMBER); 
                           
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom2',
                         p_value   => r_order_info.LINE_NUMBER);                          
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'description',
                         p_value   => r_order_info.ITEM_DESCRIPTION);

     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'pieceCount',
                         p_value   => r_order_info.SHIPMENT_QUANTITY); 
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'weight',
                         p_value   => r_order_info.ITEM_UNIT_WEIGHT);  
                                                                      
     --item_uom as attribute value -- items -->item_uom    
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => r_order_info.item_uom);   
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'UOM');
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');
       
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</items>'); 
     
     IF l_add_toclob_info = TRUE THEN
        l_add_toclob_info := FALSE;
        XXWC_OM_DMS2_OB_PKG.add_toclob_complex_parameter(p_footer_clob => l_footer_clob ,
                                                   p_xml    => '</invoices>');      
        
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'shipmentType',
                         p_value   => p_shipment_type);  
        
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'scheduledDeliveryDate',
                         p_value   => r_order_info.SCHEDULED_DELIVERY_DATE);   
                         
       fnd_file.put_line(fnd_file.log, 'branch code : '|| r_order_info.branch_code);
       
       l_dms_branch_city := XXWC_OM_DMS2_OB_PKG.GET_DMS_ORG( p_branch_code => r_order_info.branch_code);
       
       fnd_file.put_line(fnd_file.log, 'l_dms_branch_city : '|| l_dms_branch_city);

       --Rev#5.0 <Start
       l_cd_dms_branch_city := XXWC_OM_DMS2_OB_PKG.GET_DMS2_CENTRAL_DPATCHG_ORG( p_branch_code => r_order_info.branch_code);
        
       fnd_file.put_line(fnd_file.log, 'l_cd_dms_branch_city : '|| l_cd_dms_branch_city);
        
       IF l_cd_dms_branch_city IS NOT NULL THEN
          XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'sellingStoreNumber',
                         p_value   => l_cd_dms_branch_city);
       ELSE--Rev#5.0 >End
          XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'sellingStoreNumber',
                         p_value   => l_dms_branch_city);
       END IF;--Rev#5.0  
                         
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'fulfillingStoreNumber',
                         p_value   => l_dms_branch_city);                                                                                                                                       
     END IF ;  
     l_sec := 'End loop r_order_info';                                                                                     
  END LOOP;
  
  l_request.body := l_request.body||l_footer_clob;
  
  --orderd date as attribute value -- request -->attributes
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_ordered_date);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Ordered Date');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                                                   
  --cust_po_number as attribute value-- request -->attributes
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_cust_po_number);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Cust PO #');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                     
  --Order Type                              
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_xxwc_log_rec.ORDER_TYPE);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Order type');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');   
  --Request id                              
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_request_id);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Request id');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                                                                                                    
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'username',
                         p_value   => g_dms2_username);                       
        
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'passwordHash',
                         p_value   => g_dms2_passwordHash); 
                         
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'organizationKey',
                         p_value   => g_dms2_organizationkey);   
                         
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</request>');

  --Set the body xml to NULL if there is no data found
  IF l_data_found = FALSE THEN
     l_request.body := NULL; 
     p_xxwc_log_rec.ws_responsecode := 'NO_DATA_FOUND';   
     fnd_file.put_line (fnd_file.log, 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called');
     g_retcode := 1;
     g_err_msg := 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called';
  ELSE   
     XXWC_OM_DMS2_OB_PKG.invoke( p_request => l_request
                                     ,p_url     => l_url
                                     ,p_action  => l_soap_action
                                     ,p_xxwc_log_rec => p_xxwc_log_rec);
  END IF;  
 
  l_sec := 'End ShipmentSvc_return_ord';                        
EXCEPTION
WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the ShipmentSvc_retrun_ord procedure'||SQLERRM;
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
      
      write_output ('Error in ShipmentSvc_return_ord is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
      
      fnd_file.put_line(fnd_file.log, 'Exception Block of ShipmentSvc_return_ord Procedure');
      fnd_file.put_line(fnd_file.log,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log,'The Error Message Traced is : ' || p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.ShipmentSvc_return_ord',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  
END ShipmentSvc_return_ord;

--Rev4.0 <Start
PROCEDURE ShipmentSvc_int_ord_pickup(p_order_number      IN NUMBER,
                                     p_shipment_type     IN VARCHAR2,
                                     p_sch_delivery_date IN DATE,
                                     p_request_id        IN NUMBER,
                                     p_xxwc_log_rec      IN OUT xxwc_log_rec )
AS
  l_request   XXWC_OM_DMS2_OB_PKG.t_request;
  l_response  XXWC_OM_DMS2_OB_PKG.t_response;
  l_return                   VARCHAR2(32767);
  l_error_code               NUMBER;
  p_error_msg                VARCHAR2(2000);
  l_sec                      VARCHAR2 (2000);
  
  l_url                      VARCHAR2(32767);
  l_namespace                VARCHAR2(32767);
  l_method                   VARCHAR2(32767);
  l_soap_action              VARCHAR2(32767);
  l_result_name              VARCHAR2(32767);
  l_add_header_info          BOOLEAN := TRUE;
  l_add_toclob_info          BOOLEAN := TRUE;
  l_print_invoice_tag        BOOLEAN := TRUE;
  
  l_order_number             NUMBER := -1;
  l_ordered_date             VARCHAR2(200);
  l_cust_po_number           VARCHAR2(200);
  
  l_invoice_close            VARCHAR2(32767); 
  l_footer_clob              CLOB; 
  
  l_data_found               BOOLEAN := FALSE; -- to check if the data exists
  l_dms_branch_city          fnd_lookup_values.meaning%TYPE;
  l_cd_dms_branch_city       fnd_lookup_values.description%TYPE;--Rev#5.0
  
  CURSOR c_order_info ( p_order_number IN NUMBER )
   IS
   SELECT LPAD(SUBSTR(party.party_name, 1, INSTR(party.party_name, ' ') - 1),3,'0') branch_code
      ,ooh.order_number ORDER_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (party.party_name) CUSTOMER_NAME
      ,cust_acct.account_number CUSTOMER_NUMBER
      /**,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_su.location) JOB_SITE_NAME
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address1) JOB_SITE_ADD
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address2) JOB_SITE_ADD_CROSS_STREET1
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address3) JOB_SITE_ADD_CROSS_STREET2
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.city) JOB_SITE_ADD_CITY
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.state) JOB_SITE_ADD_STATE
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.postal_code) JOB_SITE_ADD_ZIP
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters ( XXWC_OM_DMS2_OB_PKG.get_job_site_contact (p_ship_to_contact_id => ooh.ship_to_contact_id)) JOB_SITE_PHONE_NUMBER --Rev#6.0
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters ( ship_party.person_title || '  '|| ship_party.PERSON_FIRST_NAME || '  '|| ship_party.PERSON_LAST_NAME) JOB_SITE_CONTACT**/--Rev#6.0
      --Rev#6.0 <Start
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (branch_loc.location_code) JOB_SITE_NAME
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (branch_loc.address_line_1) JOB_SITE_ADD
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (branch_loc.address_line_2) JOB_SITE_ADD_CROSS_STREET1
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (branch_loc.address_line_3) JOB_SITE_ADD_CROSS_STREET2
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (branch_loc.town_or_city) JOB_SITE_ADD_CITY
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (branch_loc.region_2) JOB_SITE_ADD_STATE
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (branch_loc.postal_code) JOB_SITE_ADD_ZIP
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (branch_loc.telephone_number_1) JOB_SITE_PHONE_NUMBER
      --Rev#6.0 > End
      ,TO_CHAR(ooh.ordered_date,'YYYY-MM-DD')||'T'||to_char(ooh.ordered_date,'HH24:MI:SS')||'Z' ORDERED_DATE
      ,TO_CHAR(p_sch_delivery_date,'YYYY-MM-DD')||'T'||to_char(p_sch_delivery_date,'HH24:MI:SS')||'Z' SCHEDULED_DELIVERY_DATE
      ,TO_CHAR(ooh.request_date,'YYYY-MM-DD')||'T'||to_char(ooh.request_date,'HH24:MI:SS')||'Z' request_date
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ooh.cust_po_number) CUST_PO_NUMBER
      ,(ool.line_number||'.'||ool.shipment_number) line_number
      ,ool.ordered_item ITEM_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (msib.description)  ITEM_DESCRIPTION
      ,ool.order_quantity_uom
      ,NVL(ool.ordered_quantity, 0) * NVL(msib.unit_weight, 0) ITEM_UNIT_WEIGHT
      ,NVL(ool.ordered_quantity, 0) SHIPMENT_QUANTITY
      ,ool.order_quantity_uom ITEM_UOM
      ,TRIM(REGEXP_REPLACE(ASCIISTR(XXWC_OM_DMS2_OB_PKG.replace_special_characters (ooh.shipping_instructions)),'\\[[:xdigit:]]{4}','')) SHIPPING_INSTRUCTIONS
      ,ot.name order_type
     FROM oe_order_headers_all ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       /**, hz_cust_site_uses_all ship_su 
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps--Rev#6.0
       , hz_locations ship_loc--Rev#6.0
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct**/ --Rev# 6.0 
       , oe_order_lines_all ool
       , mtl_system_items_b msib
       , apps.hr_all_organization_units haou --Rev#6.0
       , apps.hr_locations_all branch_loc--Rev#6.0
       WHERE ooh.header_id                   = ool.header_id
        AND msib.inventory_item_id          = ool.inventory_item_id
        AND msib.organization_id            = ool.ship_from_org_id
        AND ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        /**AND ool.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = 'CONTACT'
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)**/--Rev#6.0
        AND ool.ship_from_org_id            = haou.organization_id(+)--Rev#6.0
        AND haou.location_id                = branch_loc.location_id --Rev#6.0  
        AND ooh.order_number                = p_order_number
        AND ool.source_type_code            = 'INTERNAL'
        AND EXISTS                          ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS2_INTERNAL_SHIP_METHOD'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                             )
        ORDER BY ooh.order_number;
BEGIN
  l_sec := 'Srart ShipmentSvc_int_ord_pickup p_order_number: ' ||p_order_number;
  l_url         := g_dms2_shipmentsvc_url;--'https://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl';
  l_namespace   := NULL;--'xmlns="http://shipment.services.operation.cuberoute.com"';
  l_method      := 'ship:shipment';
  --l_soap_action := 'http://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl/ship';
  l_soap_action := g_dms2_shipmentsvc_url||'/ship';
  l_result_name := 'return';

  mo_global.set_policy_context('S',162);
  
  l_request := XXWC_OM_DMS2_OB_PKG.new_request( p_method       => l_method,
                                                     p_namespace    => l_namespace);

  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<request>');
  
  l_sec := 'Before ShipmentSvc_int_ord_pickup';                                                  
  FOR r_order_info IN c_order_info ( p_order_number => p_order_number )
  LOOP    
     l_data_found := TRUE; -- set to true if to generate xml file
     l_sec := 'Inside r_order_info p_order_number : '|| r_order_info.order_number|| ' LINE_NUMBER '|| r_order_info.LINE_NUMBER || ' Item Number : '|| r_order_info.Item_number;
     write_output (l_sec);
     
     l_ordered_date    := r_order_info.ordered_date;
     l_cust_po_number  := r_order_info.cust_po_number; 
     IF l_add_header_info = TRUE THEN
        l_add_header_info := FALSE;
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'operationCode',
                         p_value   => 'Edit');
 
        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                       p_xml    => '<address>');
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'addressLine1',
                            p_value   => r_order_info.JOB_SITE_ADD);
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'city',
                            p_value   => r_order_info.JOB_SITE_ADD_CITY);
  
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'phoneNumber1',
                            p_value   => r_order_info.JOB_SITE_PHONE_NUMBER);

        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'postalCode',
                            p_value   => r_order_info.JOB_SITE_ADD_ZIP);
 
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'stateCode',
                            p_value   => r_order_info.JOB_SITE_ADD_STATE);
                                                                       
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'crossStreet1',
                            p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET1);
                         
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'crossStreet2',
                            p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET2);
                                                         
        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</address>');
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerName2',
                         p_value   => r_order_info.JOB_SITE_NAME);    
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerCompanyName',
                         p_value   => r_order_info.CUSTOMER_NAME);   
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerID',
                         p_value   => r_order_info.CUSTOMER_NUMBER);     

     END IF;                                                   

     IF l_order_number <> r_order_info.ORDER_NUMBER THEN
        --Check if delivery is different and it is not a first record, then close the invoice element    
        IF l_order_number <> -1 THEN
          --start close the invoices element        
          XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</invoices>');
                                                   
          --end close the invoices element   
        END IF;
        
        l_order_number := r_order_info.ORDER_NUMBER;
        l_print_invoice_tag := TRUE;
             
        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<invoices>'); 
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ORDER_NUMBER);   
        
        /**XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => r_order_info.JOB_SITE_CONTACT); **/--Rev 6.0
    
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'comment',
                         p_value   => r_order_info.SHIPPING_INSTRUCTIONS);
     END IF;                  

     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<items>'); 
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ITEM_NUMBER);   
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom2',
                         p_value   => r_order_info.LINE_NUMBER); 
                             
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'description',
                         p_value   => r_order_info.ITEM_DESCRIPTION);

     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'pieceCount',
                         p_value   => r_order_info.SHIPMENT_QUANTITY); 
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'weight',
                         p_value   => r_order_info.ITEM_UNIT_WEIGHT);  
                                                                      
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => r_order_info.item_uom);   
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'UOM');
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');
       
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</items>'); 
     
     IF l_print_invoice_tag = TRUE THEN
        l_print_invoice_tag := FALSE;
        
        l_invoice_close := NULL;
        
        --start close the invoices element        
        --delivery_id as attribute value -- invoices -->attributes
        XXWC_OM_DMS2_OB_PKG.add_toclob_complex_parameter(p_footer_clob => l_invoice_close ,
                                                                   p_xml    => '</invoices>');
        --end close the invoices element      
     END IF;

     IF l_add_toclob_info = TRUE THEN
        l_add_toclob_info := FALSE;
        
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'shipmentType',
                         p_value   => p_shipment_type);  
        
        IF r_order_info.SCHEDULED_DELIVERY_DATE IS NULL THEN
           XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                            p_name    => 'scheduledDeliveryDate',
                            p_value   => r_order_info.REQUEST_DATE);   
        ELSE
           XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                            p_name    => 'scheduledDeliveryDate',
                            p_value   => r_order_info.SCHEDULED_DELIVERY_DATE);
        END IF;                      
        
        fnd_file.put_line(fnd_file.log, 'branch code : '|| r_order_info.branch_code);
       
        l_dms_branch_city := XXWC_OM_DMS2_OB_PKG.GET_DMS_ORG( p_branch_code => r_order_info.branch_code);
       
        fnd_file.put_line(fnd_file.log, 'l_dms_branch_city : '|| l_dms_branch_city);

        --Rev#5.0 <Start
        l_cd_dms_branch_city := XXWC_OM_DMS2_OB_PKG.GET_DMS2_CENTRAL_DPATCHG_ORG( p_branch_code => r_order_info.branch_code);
        
        fnd_file.put_line(fnd_file.log, 'l_cd_dms_branch_city : '|| l_cd_dms_branch_city);
        
        IF l_cd_dms_branch_city IS NOT NULL THEN
           XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'sellingStoreNumber',
                         p_value   => l_cd_dms_branch_city);
        ELSE--Rev#5.0 >End
           XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'sellingStoreNumber',
                         p_value   => l_dms_branch_city);
        END IF;--Rev#5.0  
                          
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'fulfillingStoreNumber',
                         p_value   => l_dms_branch_city);                                                                                                                                   
     END IF ;  
     l_sec := 'End Loop r_order_info';                                                                                   
  END LOOP;
  
  l_sec := 'Afer Loop r_order_info';
  
  l_request.body := l_request.body||l_invoice_close;
  
  l_request.body := l_request.body||l_footer_clob;
  
  --orderd date as attribute value -- request -->attributes
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_ordered_date);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Ordered Date');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                                                   
  --cust_po_number as attribute value-- request -->attributes
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_cust_po_number);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Cust PO #');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
  --Order Type                              
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_xxwc_log_rec.ORDER_TYPE);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Order type');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');                                                  
  --Request id                              
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_request_id);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Request id');
                                                  
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');    
                                                                      
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'username',
                         p_value   => g_dms2_username);                       
        
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'passwordHash',
                         p_value   => g_dms2_passwordHash); 
                         
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'organizationKey',
                         p_value   => g_dms2_organizationkey);   
                         
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</request>');

  --Set the body xml to NULL if there is no data found
  IF l_data_found = FALSE THEN
     l_request.body := NULL; 
     p_xxwc_log_rec.ws_responsecode := 'NO_DATA_FOUND';   
     fnd_file.put_line (fnd_file.log, 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called');

     g_retcode := 1;
     g_err_msg := 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called';
  ELSE   
     XXWC_OM_DMS2_OB_PKG.invoke( p_request => l_request
                                     ,p_url     => l_url
                                     ,p_action  => l_soap_action
                                     ,p_xxwc_log_rec => p_xxwc_log_rec);
  END IF;                                   
  l_sec := 'End procedure ShipmentSvc_int_ord_pickup';  
   
EXCEPTION
WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the ShipmentSvc_int_ord_pickup procedure'||SQLERRM;
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
      
      write_output ('Error in ShipmentSvc_int_ord_pickup is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
      fnd_file.put_line(fnd_file.log, 'Exception Block of ShipmentSvc_int_ord_pickup Procedure');
      fnd_file.put_line(fnd_file.log,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log,'The Error Message Traced is : ' || p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.ShipmentSvc_int_ord_pickup',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  
  END ShipmentSvc_int_ord_pickup;
  --Rev4.0 >End   
    
  /********************************************************************************
  ProcedureName : Send_to_DMS
  Purpose       : This will be called by printer form
  ----------------------------------------------------------------------------------*/
  PROCEDURE Send_to_DMS(   p_conc_program_name IN VARCHAR2
                          ,p_order_header_id   IN NUMBER
                          ,p_delivery_id       IN NUMBER
                          ,p_sch_delivery_date IN DATE
                          ,p_request_id        IN NUMBER
                          ,p_priority          IN VARCHAR2 --TMS# 20180213-00090
                          )
  AS
    
    l_error_code         NUMBER;
    p_error_msg          VARCHAR2(2000);
	l_sec                VARCHAR2 (150);
	l_sch_delivery_date  VARCHAR2(11);
    l_Shipment_Type      VARCHAR2 (150);
    l_request_id         NUMBER := 0;
    l_line_count         NUMBER := 0;
    l_order_type_id      NUMBER := 0;
  BEGIN
     IF FND_PROFILE.VALUE ('XXWC_OM_DMS2_DEBUG_FLAG') = 'Y' THEN
       XXWC_OM_DMS2_OB_PKG.debug_on;
     END IF;
    
     l_sec := 'Start of Procedure : Send_to_DMS';
     
     l_sch_delivery_date := TO_CHAR (p_sch_delivery_date, 'DD-MON-YYYY');
     
     BEGIN
        SELECT ooh.order_type_id
          INTO l_order_type_id
          FROM oe_order_headers_all ooh
         WHERE 1 = 1
           AND ooh.header_id = p_order_header_id;
      EXCEPTION
      WHEN OTHERS THEN
        l_order_type_id :=0;
      END;
    
     IF p_conc_program_name IN ( 'XXWC_OM_PACK_SLIP' , 'XXWC_INT_ORD_PACK' )
     THEN
	    SELECT COUNT(1)
		  INTO l_line_count
		 FROM oe_order_headers_all ooh
             ,oe_order_lines_all ool
        WHERE ooh.header_id                   = ool.header_id
          AND ooh.header_id                   = p_order_header_id
          AND ool.user_item_description       = 'OUT_FOR_DELIVERY'
          AND EXISTS                          ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS2_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                                  );
		 
        fnd_global.apps_initialize (fnd_global.user_id,
                                      fnd_global.resp_id,
                                      fnd_global.resp_appl_id
                                      );  
                                 
		IF l_line_count > 0 THEN
	        l_request_id :=  fnd_request.submit_request (application      => 'XXWC',
	                                                   program          => 'XXWC_OM_DMS2_OB',
	                                                   description      => NULL,
	                                                   start_time       => SYSDATE,
	                                                   sub_request      => FALSE,
	                                                   argument1        => p_order_header_id,
	                                                   argument2        => l_sch_delivery_date,
	                                                   argument3        => p_delivery_id,
	                                                   argument4        => 'CustomerDelivery',
	                                                   argument5        => 'SEND_TO_DMS_SO_STD',
  	                                                   argument6        => FND_PROFILE.VALUE ('XXWC_OM_DMS2_DEBUG_FLAG'),
  	                                                   argument7        => p_request_id,
  	                                                   argument8        => p_priority --TMS# 20180213-00090 
	                                                 );	
          --Commit Updates 	
          xxwc_ascp_scwb_pkg.proccommit;
       END IF;
	 END IF; --'XXWC_OM_PACK_SLIP'
	
	 IF p_conc_program_name IN ('XXWC_OM_SRECEIPT', 'XXWC_OM_SRECEIPT_CUSTOMER') 
	 AND l_order_type_id = FND_PROFILE.VALUE ('XXWC_RETURN_ORDER_TYPE') 
	 THEN
   		 SELECT COUNT(1)
		   INTO l_line_count
		   FROM oe_order_headers_all ooh
               ,oe_order_lines_all ool
          WHERE ooh.header_id                   = ool.header_id
            AND ooh.order_type_id               = l_order_type_id
            AND ooh.header_id                   = p_order_header_id
            AND ool.line_type_id                = 1007 --RETURN WITH RECEIPT (1007) --Rev#4.0
            AND EXISTS                      ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS2_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                                );
		   IF l_line_count > 0 THEN
  	          l_request_id :=  fnd_request.submit_request (application      => 'XXWC',
	                                                   program          => 'XXWC_OM_DMS2_OB',
	                                                   description      => NULL,
	                                                   start_time       => SYSDATE,
	                                                   sub_request      => FALSE,
	                                                   argument1        => p_order_header_id,
	                                                   argument2        => l_sch_delivery_date,
	                                                   argument3        => p_delivery_id,
	                                                   argument4        => 'CustomerPickup',
	                                                   argument5        => 'SEND_TO_DMS_SO_RETURN',
  	                                                   argument6        =>  FND_PROFILE.VALUE ('XXWC_OM_DMS2_DEBUG_FLAG'),
  	                                                   argument7        => p_request_id,
  	                                                   argument8        => p_priority --TMS# 20180213-00090
	                                                 );	
            --Commit Updates 	
            xxwc_ascp_scwb_pkg.proccommit;
          END IF;
  	END IF; --Retrun Order condition
  	                                     
    IF p_conc_program_name IN ('XXWC_OM_SHORTRENT_ACKL', 'XXWCRRWSHEET_ITEM', 'XXWC_OM_RENTAL_PS_RCT1') 
	 AND l_order_type_id IN (1013, 1014)--FND_PROFILE.VALUE ('XXWC_WC_SHORT_TERM_RENTAL_TYPE') 
	 THEN
	     IF p_conc_program_name IN ('XXWC_OM_SHORTRENT_ACKL', 'XXWC_OM_RENTAL_PS_RCT1')
	     THEN
	        l_Shipment_Type := 'CustomerDelivery';
	     ELSIF  p_conc_program_name = 'XXWCRRWSHEET_ITEM'   
	     THEN   
	        l_Shipment_Type := 'CustomerPickup';
	     END IF;
	     
   		 SELECT COUNT(1)
		   INTO l_line_count
		   FROM oe_order_headers_all ooh
               ,oe_order_lines_all ool
          WHERE ooh.header_id                   = ool.header_id
            AND ooh.order_type_id               = l_order_type_id
            AND ooh.header_id                   = p_order_header_id
            AND EXISTS                      ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS2_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                                );
		   --insert into XXWC_CLOB_TEMP(step) values ('l_line_count : '||l_line_count );
		   
		   IF l_line_count > 0 THEN
  	          l_request_id :=  fnd_request.submit_request (application      => 'XXWC',
	                                                   program          => 'XXWC_OM_DMS2_OB',
	                                                   description      => NULL,
	                                                   start_time       => SYSDATE,
	                                                   sub_request      => FALSE,
	                                                   argument1        => p_order_header_id,
	                                                   argument2        => l_sch_delivery_date,
	                                                   argument3        => p_delivery_id,
	                                                   argument4        => l_Shipment_Type,
	                                                   argument5        => 'SEND_TO_DMS_SO_RENTAL',
  	                                                   argument6        =>  FND_PROFILE.VALUE ('XXWC_OM_DMS2_DEBUG_FLAG'),
  	                                                   argument7        => p_request_id,
  	                                                   argument8        => p_priority --TMS# 20180213-00090
	                                                 );	
            --Commit Updates 	
            xxwc_ascp_scwb_pkg.proccommit;
          END IF;
  	END IF; --Retrun Order condition    
  	
  	--Rev#4.0 Start<
  	IF p_conc_program_name = 'XXWC_INT_ORD_PICK' AND l_order_type_id = 1011 THEN
	   XXWC_OM_DMS2_OB_PKG.Send_to_DMS_INT_PICKUP( p_order_header_id   => p_order_header_id
                                                  ,p_sch_delivery_date => p_sch_delivery_date
                                                  ,p_request_id        => p_request_id
                                                 );
	END IF;
	--Rev#4.0 >End
	 
    l_sec := 'End of Procedure : Send_to_DMS';
    
  EXCEPTION
    WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the Send_to_DMS procedure'||SQLERRM;
    
      write_output ('Error in XXWC_OM_DMS2_OB_PKG.Send_to_DMS is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
    
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.Send_to_DMS',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  						
  END Send_to_DMS;
    
  /********************************************************************************
  ProcedureName : Send_to_DMS_Clear_Delivery
  Purpose       : This will be called by printer form
  ----------------------------------------------------------------------------------*/
  PROCEDURE Send_to_DMS_Clear_Delivery( p_order_header_id   IN NUMBER
                                       ,p_delivery_id       IN NUMBER
                                       ,p_sch_delivery_date IN DATE DEFAULT SYSDATE
                                       ) AS
    
    l_error_code         NUMBER;
    p_error_msg          VARCHAR2(2000);
	l_sec                VARCHAR2 (150);
	l_sch_delivery_date  VARCHAR2(11);
    l_Shipment_Type      VARCHAR2 (150);
    l_request_id         NUMBER := 0;
    l_line_count         NUMBER := 0;
BEGIN
     IF FND_PROFILE.VALUE ('XXWC_OM_DMS2_DEBUG_FLAG') = 'Y' THEN
       XXWC_OM_DMS2_OB_PKG.debug_on;
     END IF;
     l_sec := 'Start of Procedure : Send_to_DMS_Clear_Delivery';
     
     l_sch_delivery_date := TO_CHAR (p_sch_delivery_date, 'DD-MON-YYYY');
     
     SELECT COUNT(1)
	   INTO l_line_count
	   FROM oe_order_headers_all ooh
           ,oe_order_lines_all ool
      WHERE ooh.header_id                   = ool.header_id
        AND ooh.header_id                   = p_order_header_id
        AND EXISTS                      (     SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS2_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                                );

		IF l_line_count > 0 THEN
           l_request_id :=  fnd_request.submit_request (    application      => 'XXWC',
	                                                   program          => 'XXWC_OM_DMS2_OB',
	                                                   description      => NULL,
	                                                   start_time       => SYSDATE,
	                                                   sub_request      => FALSE,
	                                                   argument1        => p_order_header_id,
	                                                   argument2        => l_sch_delivery_date,
	                                                   argument3        => p_delivery_id,
	                                                   argument4        => 'CustomerDelivery',
	                                                   argument5        => 'SEND_TO_DMS_SO_STD_CLR_DEL',
  	                                                   argument6        =>  FND_PROFILE.VALUE ('XXWC_OM_DMS2_DEBUG_FLAG'),
  	                                                   argument7        => NULL,
  	                                                   argument8        => 'N' --TMS# 20180213-00090
	                                                 );	
           --Commit Updates 	
           xxwc_ascp_scwb_pkg.proccommit;
       END IF;
 	
     l_sec := 'End of Procedure : Send_to_DMS_Clear_Delivery';
  EXCEPTION
    WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the Send_to_DMS_Clear_Delivery procedure'||SQLERRM;
    
      write_output ('Error in XXWC_OM_DMS2_OB_PKG.Send_to_DMS_Clear_Delivery is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
    
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.Send_to_DMS_Clear_Delivery',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  						
  END Send_to_DMS_Clear_Delivery;
  
  PROCEDURE ShipmentSvc_delete_hdr_line (p_header_id           IN NUMBER,
                                         p_line_id             IN NUMBER,
                                         p_delivery_id         IN NUMBER,
                                         p_sch_delivery_date   IN DATE,
                                         p_hdr_flow_status_code IN VARCHAR2,
                                         p_ln_flow_status_code IN VARCHAR2,
                                         p_xxwc_log_rec        IN OUT xxwc_log_rec )
  AS
  l_request   XXWC_OM_DMS2_OB_PKG.t_request;
  l_response  XXWC_OM_DMS2_OB_PKG.t_response;
  l_return                   VARCHAR2(32767);
  l_error_code               NUMBER;
  p_error_msg                VARCHAR2(2000);
  l_sec                      VARCHAR2 (2000);
  
  l_url                      VARCHAR2(32767);
  l_namespace                VARCHAR2(32767);
  l_method                   VARCHAR2(32767);
  l_soap_action              VARCHAR2(32767);
  l_result_name              VARCHAR2(32767);
  l_add_header_info          BOOLEAN := TRUE;
  l_add_toclob_info          BOOLEAN := TRUE;
  l_print_invoice_tag        BOOLEAN := TRUE;
  
  l_order_number             NUMBER := -1;
  l_ordered_date             VARCHAR2(200);
  l_cust_po_number           VARCHAR2(200);
  
  l_invoice_close            VARCHAR2(32767); 
  l_footer_clob              CLOB; 
  
  l_data_found               BOOLEAN := FALSE; -- to check if the data exists
  l_dms_branch_city          fnd_lookup_values.meaning%TYPE;
  l_cd_dms_branch_city       fnd_lookup_values.description%TYPE;--Rev#5.0
  
  CURSOR c_order_info ( p_header_id IN NUMBER
                       ,p_line_id  IN NUMBER )
   IS
   SELECT SUBSTR(hl.location_code, 1, INSTR(hl.location_code, ' ') - 1) branch_code
      ,ooh.order_number ORDER_NUMBER
      ,wsh_stg.delivery_id DELIVERY_ID
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (party.party_name) CUSTOMER_NAME
      ,cust_acct.account_number CUSTOMER_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_su.location) JOB_SITE_NAME
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address1) JOB_SITE_ADD
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address2) JOB_SITE_ADD_CROSS_STREET1
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address3) JOB_SITE_ADD_CROSS_STREET2
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.city) JOB_SITE_ADD_CITY
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.state) JOB_SITE_ADD_STATE
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.postal_code) JOB_SITE_ADD_ZIP
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (XXWC_OM_DMS2_OB_PKG.get_job_site_contact (p_ship_to_contact_id => ooh.ship_to_contact_id)) JOB_SITE_PHONE_NUMBER --Rev#6.0
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters ( ship_party.person_title || '  '|| ship_party.PERSON_FIRST_NAME || '  '|| ship_party.PERSON_LAST_NAME) JOB_SITE_CONTACT
      ,TO_CHAR(ooh.ordered_date,'YYYY-MM-DD')||'T'||to_char(ooh.ordered_date,'HH24:MI:SS')||'Z' ORDERED_DATE
      ,TO_CHAR(p_sch_delivery_date,'YYYY-MM-DD')||'T'||to_char(p_sch_delivery_date,'HH24:MI:SS')||'Z' SCHEDULED_DELIVERY_DATE
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ooh.cust_po_number) CUST_PO_NUMBER
      ,(ool.line_number||'.'||ool.shipment_number) line_number
      --,ool.line_number
      ,ool.ordered_item ITEM_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (msib.description)  ITEM_DESCRIPTION
      ,ool.order_quantity_uom
      ,(NVL(ool.ordered_quantity, 0) * NVL(msib.unit_weight, 0)) ITEM_UNIT_WEIGHT
      ,NVL(ool.ordered_quantity, 0) SHIPMENT_QUANTITY
      ,ool.order_quantity_uom ITEM_UOM
      ,TRIM(REGEXP_REPLACE(ASCIISTR(XXWC_OM_DMS2_OB_PKG.replace_special_characters (ooh.shipping_instructions)),'\\[[:xdigit:]]{4}','')) SHIPPING_INSTRUCTIONS
      ,ot.name order_type
     FROM oe_order_headers ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , oe_order_lines ool
       , mtl_system_items_b msib
       , xxwc_wsh_shipping_stg wsh_stg
      WHERE ooh.header_id                   = ool.header_id
        AND msib.inventory_item_id          = ool.inventory_item_id
        AND msib.organization_id            = ool.ship_from_org_id
        AND wsh_stg.header_id               = ooh.header_id
        AND wsh_stg.line_id                 = ool.line_id 
        AND ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND ool.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = 'CONTACT'
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ool.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND ooh.header_id                   = p_header_id
        AND ool.line_id                     = NVL(p_line_id, ool.line_id)
        AND wsh_stg.delivery_id             =  p_delivery_id
        /**AND (wsh_stg.status                  = p_ln_flow_status_code --'CANCELLED'/'OUT_FOR_DELIVERY'
              OR ool.flow_status_code        = p_ln_flow_status_code
              OR ooh.flow_status_code        = p_hdr_flow_status_code)**/ -- 'CANCELLED' - For header level cancellation for standard and internal orders.
        AND ool.source_type_code            = 'INTERNAL'
        AND EXISTS                          ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS2_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                             )
   --     ORDER BY ooh.order_number, wsh_stg.delivery_id
  UNION
     -- This sql will retrun result for only rental and return order
     SELECT SUBSTR(hl.location_code, 1, INSTR(hl.location_code, ' ') - 1) branch_code
      ,ooh.order_number ORDER_NUMBER
      ,NULL DELIVERY_ID
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (party.party_name) CUSTOMER_NAME
      ,cust_acct.account_number CUSTOMER_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_su.location) JOB_SITE_NAME
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address1) JOB_SITE_ADD
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address2) JOB_SITE_ADD_CROSS_STREET1
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address3) JOB_SITE_ADD_CROSS_STREET2
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.city) JOB_SITE_ADD_CITY
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.state) JOB_SITE_ADD_STATE
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.postal_code) JOB_SITE_ADD_ZIP
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters ( XXWC_OM_DMS2_OB_PKG.get_job_site_contact (p_ship_to_contact_id => ooh.ship_to_contact_id)) JOB_SITE_PHONE_NUMBER --Rev#6.0
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters ( ship_party.person_title || '  '|| ship_party.PERSON_FIRST_NAME || '  '|| ship_party.PERSON_LAST_NAME) JOB_SITE_CONTACT
      ,TO_CHAR(ooh.ordered_date,'YYYY-MM-DD')||'T'||to_char(ooh.ordered_date,'HH24:MI:SS')||'Z' ORDERED_DATE
      ,TO_CHAR(p_sch_delivery_date,'YYYY-MM-DD')||'T'||to_char(p_sch_delivery_date,'HH24:MI:SS')||'Z' SCHEDULED_DELIVERY_DATE
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ooh.cust_po_number) CUST_PO_NUMBER
      ,(ool.line_number||'.'||ool.shipment_number) line_number
      --,ool.line_number
      ,ool.ordered_item ITEM_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (msib.description)  ITEM_DESCRIPTION
      ,ool.order_quantity_uom
      ,(NVL(ool.ordered_quantity, 0) * NVL(msib.unit_weight, 0)) ITEM_UNIT_WEIGHT
      ,NVL(ool.ordered_quantity, 0) SHIPMENT_QUANTITY
      ,ool.order_quantity_uom ITEM_UOM
      ,TRIM(REGEXP_REPLACE(ASCIISTR(XXWC_OM_DMS2_OB_PKG.replace_special_characters (ooh.shipping_instructions)),'\\[[:xdigit:]]{4}','')) SHIPPING_INSTRUCTIONS
      ,ot.name order_type
     FROM oe_order_headers ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , oe_order_lines ool
       , mtl_system_items_b msib
      WHERE ooh.header_id                   = ool.header_id
        AND msib.inventory_item_id          = ool.inventory_item_id
        AND msib.organization_id            = ool.ship_from_org_id
        AND ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND ool.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = 'CONTACT'
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ool.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND ooh.header_id                   = p_header_id
        AND ool.line_id                     = NVL(p_line_id, ool.line_id)
        /**AND (
               ooh.flow_status_code         = NVL(p_hdr_flow_status_code, ooh.flow_status_code)
            OR ool.flow_status_code         = NVL(p_ln_flow_status_code, ool.flow_status_code)
             )**/
        AND ooh.order_type_id               IN (1013, 1014, 1006) --WC LONG TERM RENTAL(1013) /WC SHORT TERM RENTAL(1014)/ RETURN ORDER (1006)
        AND ool.line_type_id                IN (1003, 1015, 1007, 1008) --BILL ONLY (1003)/Rental(1015)/RETURN WITH RECEIPT (1007)/Credit(1008)
        AND ool.source_type_code            = 'INTERNAL';
        /**AND EXISTS                          ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS2_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                             );**/
BEGIN
  l_sec := 'Srart ShipmentSvc_delete_line p_header_id: ' ||p_header_id || ' p_line_id: '||p_line_id;
  l_url         := g_dms2_shipmentsvc_url;--'https://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl';
  l_namespace   := NULL;--'xmlns="http://shipment.services.operation.cuberoute.com"';
  l_method      := 'ship:shipment';
  --l_soap_action := 'http://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl/ship';
  l_soap_action := g_dms2_shipmentsvc_url||'/ship';
  l_result_name := 'return';
  
  mo_global.set_policy_context('S',162);
  
  l_request := XXWC_OM_DMS2_OB_PKG.new_request( p_method       => l_method,
                                                     p_namespace    => l_namespace);

  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<request>');
  
  l_sec := 'Before ShipmentSvc_delete_line';                                                  
  FOR r_order_info IN c_order_info ( p_header_id    => p_header_id
                                    ,p_line_id      => p_line_id)
  LOOP    
     l_data_found := TRUE; -- set to true if to generate xml file
     p_xxwc_log_rec.delivery_id    := r_order_info.delivery_id;
     l_sec := 'Inside r_order_info p_order_number : '|| r_order_info.order_number|| ' Delivery_id : '||r_order_info.Delivery_id ||' LINE_NUMBER '|| r_order_info.LINE_NUMBER || ' Item Number : '|| r_order_info.Item_number;
     write_output (l_sec);
     
     l_ordered_date    := r_order_info.ordered_date;
     l_cust_po_number  := r_order_info.cust_po_number; 
     IF l_add_header_info = TRUE THEN
        l_add_header_info := FALSE;
        
        IF p_line_id IS NULL THEN --IF header is cancelled or ship method is changed to will call at header level.
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                                  p_name    => 'operationCode',
                                                  p_value   => 'Delete');
        ELSE
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                                  p_name    => 'operationCode',
                                                  p_value   => 'DeleteItem');
        END IF;                                        
 
        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                       p_xml    => '<address>');
  
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'addressLine1',
                         p_value   => r_order_info.JOB_SITE_ADD);
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'city',
                         p_value   => r_order_info.JOB_SITE_ADD_CITY);
  
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'phoneNumber1',
                         p_value   => r_order_info.JOB_SITE_PHONE_NUMBER);

        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'postalCode',
                         p_value   => r_order_info.JOB_SITE_ADD_ZIP);
 
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'stateCode',
                         p_value   => r_order_info.JOB_SITE_ADD_STATE);
                                                                       
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'crossStreet1',
                         p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET1);
                         
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'crossStreet2',
                         p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET2);
                                                         
        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</address>');
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerName2',
                         p_value   => r_order_info.JOB_SITE_NAME);    
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerCompanyName',
                         p_value   => r_order_info.CUSTOMER_NAME);   
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerID',
                         p_value   => r_order_info.CUSTOMER_NUMBER);     

     END IF;                                                   

     IF l_order_number <> r_order_info.ORDER_NUMBER THEN
        --Check if delivery is different and it is not a first record, then close the invoice element    
        IF l_order_number <> -1 THEN
          --start close the invoices element        
          XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</invoices>');
                                                   
          --end close the invoices element   
        END IF;
        
        l_order_number := r_order_info.ORDER_NUMBER;
        l_print_invoice_tag := TRUE;
             
        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<invoices>'); 
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ORDER_NUMBER);   
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => r_order_info.JOB_SITE_CONTACT); 
    
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'comment',
                         p_value   => r_order_info.SHIPPING_INSTRUCTIONS);
     END IF;                  

     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<items>'); 
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ITEM_NUMBER);   
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom2',
                         p_value   => r_order_info.LINE_NUMBER); 
                             
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'description',
                         p_value   => r_order_info.ITEM_DESCRIPTION);

     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'pieceCount',
                         p_value   => r_order_info.SHIPMENT_QUANTITY); 
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'weight',
                         p_value   => r_order_info.ITEM_UNIT_WEIGHT);  
                                                                      
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => r_order_info.item_uom);   
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'UOM');
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');
       
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</items>'); 
     
     IF l_print_invoice_tag = TRUE THEN
        l_print_invoice_tag := FALSE;
        
        l_invoice_close := NULL;
        
        --start close the invoices element        
        --delivery_id as attribute value -- invoices -->attributes
        XXWC_OM_DMS2_OB_PKG.add_toclob_complex_parameter(p_footer_clob => l_invoice_close ,
                                                                   p_xml    => '</invoices>');
        --end close the invoices element      
     END IF;

     IF l_add_toclob_info = TRUE THEN
        l_add_toclob_info := FALSE;
        p_xxwc_log_rec.order_type := r_order_info.order_type;   
        IF r_order_info.order_type = 'RETURN ORDER' THEN
           XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                            p_name    => 'shipmentType',
                            p_value   => 'CustomerPickup');
        ELSE
           XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                            p_name    => 'shipmentType',
                            p_value   => 'CustomerDelivery');
        END IF;                      
        
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'internalOrderId',
                         p_value   => r_order_info.delivery_id);  
        
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'scheduledDeliveryDate',
                         p_value   => r_order_info.SCHEDULED_DELIVERY_DATE);   
                         
        fnd_file.put_line(fnd_file.log, 'branch code : '|| r_order_info.branch_code);
       
        l_dms_branch_city := XXWC_OM_DMS2_OB_PKG.GET_DMS_ORG( p_branch_code => r_order_info.branch_code);
       
        fnd_file.put_line(fnd_file.log, 'l_dms_branch_city : '|| l_dms_branch_city);

        --Rev#5.0 <Start
        l_cd_dms_branch_city := XXWC_OM_DMS2_OB_PKG.GET_DMS2_CENTRAL_DPATCHG_ORG( p_branch_code => r_order_info.branch_code);
        
        fnd_file.put_line(fnd_file.log, 'l_cd_dms_branch_city : '|| l_cd_dms_branch_city);
        
        IF l_cd_dms_branch_city IS NOT NULL THEN
           XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'sellingStoreNumber',
                         p_value   => l_cd_dms_branch_city);
        ELSE--Rev#5.0 >End
           XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'sellingStoreNumber',
                         p_value   => l_dms_branch_city);
        END IF;--Rev#5.0  
                         
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'fulfillingStoreNumber',
                         p_value   => l_dms_branch_city);                                                                                                                                        
     END IF ;  
     l_sec := 'End Loop r_order_info';                                                                                   
  END LOOP;
  
  l_sec := 'Afer Loop r_order_info';
  
  l_request.body := l_request.body||l_invoice_close;
  
  l_request.body := l_request.body||l_footer_clob;
  
  --orderd date as attribute value -- request -->attributes
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_ordered_date);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Ordered Date');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                                                   
  --cust_po_number as attribute value-- request -->attributes
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_cust_po_number);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Cust PO #');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                     
  --Order Type                              
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_xxwc_log_rec.ORDER_TYPE);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Order type');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');   
                                                   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'username',
                         p_value   => g_dms2_username);                       
        
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'passwordHash',
                         p_value   => g_dms2_passwordHash); 
                         
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'organizationKey',
                         p_value   => g_dms2_organizationkey);   
                         
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</request>');

  --Set the body xml to NULL if there is no data found
  IF l_data_found = FALSE THEN
     l_request.body := NULL; 
     p_xxwc_log_rec.ws_responsecode := 'NO_DATA_FOUND';   
     fnd_file.put_line (fnd_file.log, 'No Data Found for the Order Number : '||p_header_id || ' Shipment service will not be called');
     g_retcode := 1;
     g_err_msg := 'No Data Found for the Order Number : '||p_header_id || ' Shipment service will not be called';
  ELSE   
     XXWC_OM_DMS2_OB_PKG.invoke( p_request => l_request
                                     ,p_url     => l_url
                                     ,p_action  => l_soap_action
                                     ,p_xxwc_log_rec => p_xxwc_log_rec);
  END IF;                                   
  l_sec := 'End ShipmentSvc_delete_line'; 
EXCEPTION
WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the ShipmentSvc_delete_line procedure'||SQLERRM;
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
      
      write_output ('Error in ShipmentSvc_delete_line is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
      fnd_file.put_line(fnd_file.log, 'Exception Block of ShipmentSvc_delete_line Procedure');
      fnd_file.put_line(fnd_file.log,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log,'The Error Message Traced is : ' || p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.ShipmentSvc_delete_line',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  
   END ShipmentSvc_delete_hdr_line;
  
   FUNCTION Delete_SO_hdr_Line (p_subscription_guid   IN     RAW,
                                p_event               IN OUT wf_event_t)
      RETURN VARCHAR2
   IS
      l_plist         wf_parameter_list_t := p_event.getparameterlist ();
      l_error_msg     VARCHAR2 (500);
      l_request_type  VARCHAR2 (50);
      l_so_header_id  oe_order_headers_all.header_id%TYPE;
      l_so_line_id    oe_order_lines_all.line_id%TYPE;
      l_order_number  oe_order_headers_all.order_number%TYPE;
      l_order_type    oe_transaction_types_tl.name%TYPE;
      l_sec           VARCHAR2 (100);
      l_xxwc_log_rec  xxwc_log_rec;
      
      CURSOR c_order_delivery_info ( p_header_id     IN NUMBER
                                    ,p_line_id       IN NUMBER
                                 )
      IS
         SELECT DISTINCT wsh_stg.delivery_id, ooh.order_number
           FROM oe_order_headers_all ooh
               ,xxwc_wsh_shipping_stg wsh_stg
          WHERE wsh_stg.header_id   = ooh.header_id
            AND ooh.header_id       = p_header_id
            AND wsh_stg.line_id     = NVL(p_line_id, wsh_stg.line_id)
          ORDER BY wsh_stg.delivery_id;
   BEGIN
      l_sec := 'Initialize values';
      
      l_request_type := wf_event.getvalueforparameter ('P_REQUEST_TYPE', l_plist);
      l_so_header_id := wf_event.getvalueforparameter ('P_HEADER_ID', l_plist);
 
      IF l_request_type = 'LINE' THEN
         l_so_line_id   := wf_event.getvalueforparameter ('P_LINE_ID', l_plist);
         l_xxwc_log_rec.line_id      := l_so_line_id;
         l_xxwc_log_rec.service_name := 'DELETE_SO_LINE';
      ELSE
         l_xxwc_log_rec.service_name := 'DELETE_SO';   
         l_so_line_id                := NULL; 
      END IF;

      l_xxwc_log_rec.header_id    := l_so_header_id;
    
      BEGIN   
         SELECT ooh.order_number
               ,ott.name
           INTO l_order_number 
               ,l_order_type
           FROM oe_order_headers_all ooh
               ,oe_transaction_types_tl ott
          WHERE ooh.order_type_id = ott.transaction_type_id 
            AND ooh.header_id     = l_so_header_id;
      EXCEPTION   
      WHEN OTHERS THEN
          l_order_number := NULL;
          l_order_type   := NULL;
      END;
      
      l_xxwc_log_rec.order_number := l_order_number;
      l_xxwc_log_rec.order_type   := l_order_type;
             
      g_wallet_path                   := FND_PROFILE.VALUE ('XXWC_OM_DMS2_WALLET_PATH');
      write_output ('Profile XXWC_OM_DMS2_WALLET_PATH value : ' || g_wallet_path);
    
      g_dms2_username            := FND_PROFILE.VALUE ('XXWC_OM_DMS2_USERNAME');
      write_output ('Profile XXWC_OM_DMS2_USERNAME value : ' || g_dms2_username);
      
      g_dms2_passwordHash        := FND_PROFILE.VALUE ('XXWC_OM_DMS2_PASSWORDHASH');
      g_dms2_organizationkey     := FND_PROFILE.VALUE ('XXWC_OM_DMS2_ORGANIZATIONKEY');
    
      g_dms2_shipmentsvc_url     := FND_PROFILE.VALUE ('XXWC_OM_DMS2_SHIPMENTSERVICE_URL');
      write_output ('Profile XXWC_OM_DMS2_SHIPMENTSERVICE_URL value : ' || g_dms2_shipmentsvc_url);
      
      apps.mo_global.set_policy_context('S',162);
      
      IF l_order_type IN  ('STANDARD ORDER', 'INTERNAL ORDER') THEN 
          FOR r_order_info IN c_order_delivery_info ( p_header_id     => l_so_header_id
                                                     ,p_line_id       => l_so_line_id
                                                     )
          LOOP 
             XXWC_OM_DMS2_OB_PKG.ShipmentSvc_delete_hdr_line( 
                                                              p_header_id            => l_so_header_id
                                                            , p_line_id              => l_so_line_id
                                                            , p_delivery_id          => r_order_info.delivery_id
                                                            , p_sch_delivery_date    => SYSDATE
                                                            , p_hdr_flow_status_code => 'CANCELLED'
                                                            , p_ln_flow_status_code  => 'CANCELLED'
                                                            , p_xxwc_log_rec         => l_xxwc_log_rec
                                                          );
             DEBUG_LOG (p_xxwc_log_rec => l_xxwc_log_rec ); 
         END LOOP; 
      ELSE
         XXWC_OM_DMS2_OB_PKG.ShipmentSvc_delete_hdr_line( 
                                                           p_header_id            => l_so_header_id
                                                         , p_line_id              => l_so_line_id
                                                         , p_delivery_id          => NULL
                                                         , p_sch_delivery_date    => SYSDATE
                                                         , p_hdr_flow_status_code => 'CANCELLED'
                                                         , p_ln_flow_status_code  => 'CANCELLED'
                                                         , p_xxwc_log_rec         => l_xxwc_log_rec
                                                       );
     
         DEBUG_LOG (p_xxwc_log_rec => l_xxwc_log_rec );
      END IF;   

      RETURN ('SUCCESS');
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg := SUBSTR (SQLERRM, 1, 240);

         wf_core.context ('XXWC_OM_DMS2_OB_PKG',
                          'Delete_SO_Hdr_Line',
                          p_event.geteventname (),
                          p_subscription_guid);
         wf_event.seterrorinfo (p_event, SUBSTR (SQLERRM, 1, 240));
         RETURN 'ERROR';

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_DMS2_OB_PKG.Delete_SO_Hdr_Line',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'OM');
   END Delete_SO_Hdr_Line;
  
/**PROCEDURE Delete_SO_Lines (
      p_application_id                 IN            NUMBER,
      p_entity_short_name              IN            VARCHAR2,
      p_validation_entity_short_name   IN            VARCHAR2,
      p_validation_tmplt_short_name    IN            VARCHAR2,
      p_record_set_short_name          IN            VARCHAR2,
      p_scope                          IN            VARCHAR2,
      x_result                            OUT NOCOPY NUMBER)
IS
      l_line_id                 NUMBER;
      l_error_msg               VARCHAR2 (240);
      l_line_type_id            NUMBER;
      l_shipping_method_code   oe_order_lines_all.shipping_method_code%TYPE; 
      l_sec                     VARCHAR2 (100);
      l_shp_m_exist_count       NUMBER;
      l_xxwc_log_rec  xxwc_log_rec;
      l_ln_flow_status_code     oe_order_lines_all.flow_status_code%TYPE; 
    
   CURSOR cur_order_line IS
   SELECT ooh.header_id
         ,ool.shipping_method_code
         ,ooh.order_number
    FROM oe_order_lines ool
        ,oe_order_headers ooh
     WHERE ooh.header_id = ool.header_id 
       AND line_id       = l_line_id
       AND EXISTS ( SELECT 1
                      FROM fnd_lookup_values flv
                     WHERE 1=1
                       AND flv.meaning      = ool.shipping_method_code
                       AND flv.lookup_type  = 'XXWC_DMS2_SHIPPING_METHOD_CODE'
                       AND flv.enabled_flag = 'Y'
                       AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                  );

   BEGIN
      l_sec := 'Initialize Values';

      l_line_id              := oe_line_security.g_record.line_id;
      l_line_type_id         := oe_line_security.g_record.line_type_id;
      l_shipping_method_code := oe_line_security.g_record.shipping_method_code;

      FOR rec_order_line IN cur_order_line
      LOOP
         IF l_line_type_id IN (fnd_profile.value('XXWC_STANDARD_LINE_TYPE'), fnd_profile.value('XXWC_INTERNAL_ORDER_LINE_TYPE')
                              ,1003, 1015, 1007, 1008)
         THEN
            SELECT count(1)
              INTO l_shp_m_exist_count
              FROM fnd_lookup_values flv
             WHERE 1=1
               AND flv.meaning      = l_shipping_method_code
               AND flv.lookup_type  = 'XXWC_DMS2_SHIPPING_METHOD_CODE'
               AND flv.enabled_flag = 'Y'
               AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1);
            
            IF l_shp_m_exist_count =0 THEN
               l_xxwc_log_rec.service_name := 'DELETE_SO_LINE';
               l_xxwc_log_rec.header_id    := rec_order_line.header_id;
               l_xxwc_log_rec.line_id      := l_line_id;
      
               l_xxwc_log_rec.order_number := rec_order_line.order_number;
             
               g_wallet_path                   := FND_PROFILE.VALUE ('XXWC_OM_DMS2_WALLET_PATH');
               write_output ('Profile XXWC_OM_DMS2_WALLET_PATH value : ' || g_wallet_path);
    
               g_dms2_username            := FND_PROFILE.VALUE ('XXWC_OM_DMS2_USERNAME');
               write_output ('Profile XXWC_OM_DMS2_USERNAME value : ' || g_dms2_username);
      
               g_dms2_passwordHash        := FND_PROFILE.VALUE ('XXWC_OM_DMS2_PASSWORDHASH');
               g_dms2_organizationkey     := FND_PROFILE.VALUE ('XXWC_OM_DMS2_ORGANIZATIONKEY');
    
               g_dms2_shipmentsvc_url     := FND_PROFILE.VALUE ('XXWC_OM_DMS2_SHIPMENTSERVICE_URL');
               write_output ('Profile XXWC_OM_DMS2_SHIPMENTSERVICE_URL value : ' || g_dms2_shipmentsvc_url);
    
               IF l_line_type_id IN (fnd_profile.value('XXWC_STANDARD_LINE_TYPE'), fnd_profile.value('XXWC_INTERNAL_ORDER_LINE_TYPE')) 
               THEN-- STANDARD ORDER / INTERNAL ORDER
                  l_ln_flow_status_code := 'OUT_FOR_DELIVERY';
               ELSE
                  l_ln_flow_status_code := NULL;
               END IF;
                   
               XXWC_OM_DMS2_OB_PKG.ShipmentSvc_delete_hdr_line( 
                                                                    p_header_id            => rec_order_line.header_id
                                                                  , p_line_id              => l_line_id
                                                                  , p_sch_delivery_date    => SYSDATE
                                                                  , p_hdr_flow_status_code => NULL
                                                                  , p_ln_flow_status_code  => l_ln_flow_status_code
                                                                  , p_xxwc_log_rec         => l_xxwc_log_rec
                                                              );
     
               DEBUG_LOG (p_xxwc_log_rec => l_xxwc_log_rec );
            END IF;   
         END IF;
    END LOOP;

    x_result := 0;

   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_DMS2_OB_PKG.Delete_SO_Lines',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'OM');

         x_result := 1;

   END Delete_SO_Lines;**/
   
PROCEDURE CANCEL_SO_OR_LINE (
      p_application_id                 IN            NUMBER,
      p_entity_short_name              IN            VARCHAR2,
      p_validation_entity_short_name   IN            VARCHAR2,
      p_validation_tmplt_short_name    IN            VARCHAR2,
      p_record_set_short_name          IN            VARCHAR2,
      p_scope                          IN            VARCHAR2,
      x_result                            OUT NOCOPY NUMBER)
IS
      l_header_id               oe_order_headers_all.header_id%TYPE;
      l_line_id                 oe_order_lines_all.line_id%TYPE;
      l_error_msg               VARCHAR2 (240);
      l_order_type_id           NUMBER;
      l_line_type_id           NUMBER;
      l_shipping_method_code    oe_order_headers_all.shipping_method_code%TYPE; 
      l_ln_flow_status_code     oe_order_lines_all.flow_status_code%TYPE; 
      l_sec                     VARCHAR2 (100);
      l_shp_m_exist_count       NUMBER;
      l_xxwc_log_rec  xxwc_log_rec;
    
   CURSOR cur_order_hdr IS
   SELECT ooh.header_id
         ,ooh.shipping_method_code
         ,ooh.order_number
    FROM oe_order_headers ooh
     WHERE ooh.header_id = l_header_id
       AND ooh.order_type_id IN (1013, 1014, 1006, 1001 , 1011) --WC LONG TERM RENTAL(1013) /WC SHORT TERM RENTAL(1014)/ RETURN ORDER (1006)/ STANDARD ORDER / INTERNAL ORDER
       AND EXISTS ( SELECT 1
                      FROM fnd_lookup_values flv
                     WHERE 1=1
                       AND flv.meaning      = ooh.shipping_method_code
                       AND flv.lookup_type  = 'XXWC_DMS2_SHIPPING_METHOD_CODE'
                       AND flv.enabled_flag = 'Y'
                       AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                  );

   CURSOR c_order_delivery_info ( p_header_id     IN NUMBER
                                 ,p_line_id       IN NUMBER
                                 )
      IS
         SELECT DISTINCT wsh_stg.delivery_id, ooh.order_number
           FROM oe_order_headers_all ooh
               ,xxwc_wsh_shipping_stg wsh_stg
          WHERE wsh_stg.header_id   = ooh.header_id
            AND ooh.header_id       = p_header_id
            AND wsh_stg.line_id     = NVL(p_line_id, wsh_stg.line_id)
          ORDER BY wsh_stg.delivery_id;
   BEGIN
      l_sec := 'Initialize Values';

      l_header_id            := oe_header_security.g_record.header_id;
      l_order_type_id        := oe_header_security.g_record.order_type_id;  
  	  
      IF p_validation_tmplt_short_name = 'WC_CNHD' THEN -- Cancel Header--to delete the order from descarties system
      
         --WC LONG TERM RENTAL(1013) /WC SHORT TERM RENTAL(1014)/ RETURN ORDER (1006)/STANDARD ORDER / INTERNAL ORDER
         --insert into XXWC_CLOB_TEMP(step) values ('1p_validation_tmplt_short_name : '||p_validation_tmplt_short_name ||'l_line_id : '||l_line_id ||'l_header_id :'||l_header_id );
         IF l_order_type_id IN (1013, 1014, 1006, 1001, 1011) THEN
            XXWC_OM_DMS2_OB_PKG.raise_so_cancel_order(l_header_id,l_error_msg);
         END IF;
      ELSIF p_validation_tmplt_short_name = 'WC_CNLI' THEN -- Cancel Line --to delete the order from descarties system for rental and return
         l_header_id          := oe_line_security.g_record.header_id;
         l_line_id            := oe_line_security.g_record.line_id;
         l_line_type_id       := oe_line_security.g_record.line_type_id;
      
         --insert into XXWC_CLOB_TEMP(step) values ('1p_validation_tmplt_short_name : '||p_validation_tmplt_short_name ||'l_line_id : '||l_line_id ||'l_header_id :'||l_header_id );
         --insert into XXWC_CLOB_TEMP(step) values ('l_line_type_id : '||l_line_type_id);
               
         IF l_line_type_id IN (1003, 1015, 1007, 1008) AND oe_line_security.g_record.ordered_quantity = 0--BILL ONLY (1003)/Rental(1015)/RETURN WITH RECEIPT (1007)/Credit(1008)
         THEN 
            XXWC_OM_DMS2_OB_PKG.raise_so_cancel_line(l_header_id, l_line_id, l_error_msg);
         END IF;
         
      ELSE --WC_CNLI
         -- this logic will delete the order from descartes when the ship_method is changed to will call. 
         l_shipping_method_code := oe_header_security.g_record.shipping_method_code;                                         
         FOR rec_order_hdr IN cur_order_hdr
         LOOP
            IF l_order_type_id IN (1013, 1014, 1006, 1001, 1011) -- WC LONG TERM RENTAL(1013) /WC SHORT TERM RENTAL(1014) /RETURN ORDER (1006) / STANDARD ORDER / INTERNAL ORDER
            THEN
               SELECT count(1)
                 INTO l_shp_m_exist_count
                 FROM fnd_lookup_values flv
                WHERE 1=1
                  AND flv.meaning      = l_shipping_method_code
                  AND flv.lookup_type  = 'XXWC_DMS2_SHIPPING_METHOD_CODE'
                  AND flv.enabled_flag = 'Y'
                  AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1);
                
                IF l_shp_m_exist_count =0 THEN
                   l_xxwc_log_rec.service_name := 'DELETE_SO';
                   l_xxwc_log_rec.header_id    := rec_order_hdr.header_id;
                   --l_xxwc_log_rec.line_id      := l_line_id;
      
                   l_xxwc_log_rec.order_number := rec_order_hdr.order_number;
             
                   g_wallet_path                   := FND_PROFILE.VALUE ('XXWC_OM_DMS2_WALLET_PATH');
                   write_output ('Profile XXWC_OM_DMS2_WALLET_PATH value : ' || g_wallet_path);
    
                   g_dms2_username            := FND_PROFILE.VALUE ('XXWC_OM_DMS2_USERNAME');
                   write_output ('Profile XXWC_OM_DMS2_USERNAME value : ' || g_dms2_username);
      
                   g_dms2_passwordHash        := FND_PROFILE.VALUE ('XXWC_OM_DMS2_PASSWORDHASH');
                   g_dms2_organizationkey     := FND_PROFILE.VALUE ('XXWC_OM_DMS2_ORGANIZATIONKEY');
    
                   g_dms2_shipmentsvc_url     := FND_PROFILE.VALUE ('XXWC_OM_DMS2_SHIPMENTSERVICE_URL');
                   write_output ('Profile XXWC_OM_DMS2_SHIPMENTSERVICE_URL value : ' || g_dms2_shipmentsvc_url);
                   
                   /**IF l_order_type_id IN (1001, 1011) THEN-- STANDARD ORDER / INTERNAL ORDER
                      l_ln_flow_status_code := 'OUT_FOR_DELIVERY';
                   ELSE
                      l_ln_flow_status_code := NULL;
                   END IF;**/
                   
                   apps.mo_global.set_policy_context('S',162);
                   
                   IF l_order_type_id IN (1001, 1011) THEN -- STANDARD ORDER / INTERNAL ORDER
                      FOR r_order_info IN c_order_delivery_info ( p_header_id     => rec_order_hdr.header_id
                                                                 ,p_line_id       => NULL
                                                                )
                      LOOP 
                         XXWC_OM_DMS2_OB_PKG.ShipmentSvc_delete_hdr_line(  p_header_id            => rec_order_hdr.header_id
                                                                              , p_line_id              => NULL -- to include all lines--Truck Delivery --> will call
                                                                              , p_delivery_id          => r_order_info.delivery_id
                                                                              , p_sch_delivery_date    => SYSDATE
                                                                              , p_hdr_flow_status_code => NULL 
                                                                              , p_ln_flow_status_code  => 'OUT_FOR_DELIVERY'--l_ln_flow_status_code
                                                                              , p_xxwc_log_rec         => l_xxwc_log_rec
                                                                            );
     
                        DEBUG_LOG (p_xxwc_log_rec => l_xxwc_log_rec );
                      END LOOP;
                   ELSE
                      XXWC_OM_DMS2_OB_PKG.ShipmentSvc_delete_hdr_line(  p_header_id            => rec_order_hdr.header_id
                                                                           , p_line_id              => NULL -- to include all lines--Truck Delivery --> will call
                                                                           , p_delivery_id          => NULL  
                                                                           , p_sch_delivery_date    => SYSDATE
                                                                           , p_hdr_flow_status_code => NULL 
                                                                           , p_ln_flow_status_code  => NULL--l_ln_flow_status_code
                                                                           , p_xxwc_log_rec         => l_xxwc_log_rec
                                                                      );
     
                     DEBUG_LOG (p_xxwc_log_rec => l_xxwc_log_rec );
                  END IF;    
                END IF;   
            END IF;
         END LOOP;
      END IF; 
      x_result := 0;

   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_DMS2_OB_PKG.CANCEL_SO_OR_LINE',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'OM');

         x_result := 1;

   END CANCEL_SO_OR_LINE;

  PROCEDURE ShipmentSvc_clear_delivery  (p_order_number      IN NUMBER,
                                         p_sch_delivery_date IN DATE,
                                         p_delivery_id       IN NUMBER,
                                         p_xxwc_log_rec      IN OUT xxwc_log_rec )
  AS
  l_request   XXWC_OM_DMS2_OB_PKG.t_request;
  l_response  XXWC_OM_DMS2_OB_PKG.t_response;
  l_return                   VARCHAR2(32767);
  l_error_code               NUMBER;
  p_error_msg                VARCHAR2(2000);
  l_sec                      VARCHAR2 (2000);
  
  l_url                      VARCHAR2(32767);
  l_namespace                VARCHAR2(32767);
  l_method                   VARCHAR2(32767);
  l_soap_action              VARCHAR2(32767);
  l_result_name              VARCHAR2(32767);
  l_add_header_info          BOOLEAN := TRUE;
  l_add_toclob_info          BOOLEAN := TRUE;
  l_print_invoice_tag        BOOLEAN := TRUE;
  
  l_order_number             NUMBER := -1;
  l_ordered_date             VARCHAR2(200);
  l_cust_po_number           VARCHAR2(200);
  
  l_invoice_close            VARCHAR2(32767); 
  l_footer_clob              CLOB; 
  
  l_data_found               BOOLEAN := FALSE; -- to check if the data exists
  l_dms_branch_city          fnd_lookup_values.meaning%TYPE;
  l_cd_dms_branch_city       fnd_lookup_values.description%TYPE;--Rev#5.0
  
  CURSOR c_order_info ( p_order_number IN NUMBER
                       ,p_delivery_id  IN NUMBER )
   IS
   SELECT SUBSTR(hl.location_code, 1, INSTR(hl.location_code, ' ') - 1) branch_code
      ,ooh.order_number ORDER_NUMBER
      ,dms_stg.delivery_id
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (party.party_name) CUSTOMER_NAME
      ,cust_acct.account_number CUSTOMER_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_su.location) JOB_SITE_NAME
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address1) JOB_SITE_ADD
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address2) JOB_SITE_ADD_CROSS_STREET1
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address3) JOB_SITE_ADD_CROSS_STREET2
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.city) JOB_SITE_ADD_CITY
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.state) JOB_SITE_ADD_STATE
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.postal_code) JOB_SITE_ADD_ZIP
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters ( XXWC_OM_DMS2_OB_PKG.get_job_site_contact (p_ship_to_contact_id => ooh.ship_to_contact_id)) JOB_SITE_PHONE_NUMBER --Rev#6.0
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters ( ship_party.person_title || '  '|| ship_party.PERSON_FIRST_NAME || '  '|| ship_party.PERSON_LAST_NAME) JOB_SITE_CONTACT
      ,TO_CHAR(ooh.ordered_date,'YYYY-MM-DD')||'T'||to_char(ooh.ordered_date,'HH24:MI:SS')||'Z' ORDERED_DATE
      ,TO_CHAR(p_sch_delivery_date,'YYYY-MM-DD')||'T'||to_char(p_sch_delivery_date,'HH24:MI:SS')||'Z' SCHEDULED_DELIVERY_DATE
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ooh.cust_po_number) CUST_PO_NUMBER
      ,(ool.line_number||'.'||ool.shipment_number) line_number
      --,ool.line_number
      ,ool.ordered_item ITEM_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (msib.description)  ITEM_DESCRIPTION
      ,ool.order_quantity_uom
      ,(NVL(ool.ordered_quantity, 0) * NVL(msib.unit_weight, 0)) ITEM_UNIT_WEIGHT
      ,NVL(ool.ordered_quantity, 0) SHIPMENT_QUANTITY
      ,ool.order_quantity_uom ITEM_UOM
      ,TRIM(REGEXP_REPLACE(ASCIISTR(XXWC_OM_DMS2_OB_PKG.replace_special_characters (ooh.shipping_instructions)),'\\[[:xdigit:]]{4}','')) SHIPPING_INSTRUCTIONS
      ,ot.name order_type
     FROM oe_order_headers_all ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , oe_order_lines_all ool
       , mtl_system_items_b msib
       , xxwc_om_dms_change_tbl dms_stg --xxwc_wsh_shipping_stg wsh_stg
      WHERE ooh.header_id                   = ool.header_id
        AND msib.inventory_item_id          = ool.inventory_item_id
        AND msib.organization_id            = ool.ship_from_org_id
        AND dms_stg.header_id               = ooh.header_id
        AND dms_stg.line_id                 = ool.line_id 
        AND ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND ool.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = 'CONTACT'
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ool.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND ooh.order_number                = p_order_number
        AND dms_stg.delivery_id             = p_delivery_id
        AND dms_stg.change_type             = 'D'
        AND dms_stg.status                  = 'OUT_FOR_DELIVERY'
        AND ool.source_type_code            = 'INTERNAL'
        AND EXISTS                          ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS2_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                             )
        ORDER BY ooh.order_number, dms_stg.delivery_id;
BEGIN
  l_sec := 'Srart ShipmentSvc_clear_delivery p_order_number: ' ||p_order_number || ' p_delivery_id: '||p_delivery_id;
  l_url         := g_dms2_shipmentsvc_url;--'https://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl';
  l_namespace   := NULL;--'xmlns="http://shipment.services.operation.cuberoute.com"';
  l_method      := 'ship:shipment';
  --l_soap_action := 'http://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl/ship';
  l_soap_action := g_dms2_shipmentsvc_url||'/ship';
  l_result_name := 'return';
  
  mo_global.set_policy_context('S',162);
  
  l_request := XXWC_OM_DMS2_OB_PKG.new_request( p_method       => l_method,
                                                     p_namespace    => l_namespace);

  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<request>');
  
  l_sec := 'Before ShipmentSvc_clear_delivery';                                                  
  FOR r_order_info IN c_order_info ( p_order_number => p_order_number
                                    ,p_delivery_id  => p_delivery_id)
  LOOP    
     l_data_found := TRUE; -- set to true if to generate xml file
     l_sec := 'Inside r_order_info p_order_number : '|| r_order_info.order_number|| ' Delivery_id : '||r_order_info.Delivery_id ||' LINE_NUMBER '|| r_order_info.LINE_NUMBER || ' Item Number : '|| r_order_info.Item_number;
     write_output (l_sec);
     
     l_ordered_date    := r_order_info.ordered_date;
     l_cust_po_number  := r_order_info.cust_po_number; 
     IF l_add_header_info = TRUE THEN
        l_add_header_info := FALSE;
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                               p_name    => 'operationCode',
                                               p_value   => 'Delete');
 
        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                       p_xml    => '<address>');
  
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'addressLine1',
                         p_value   => r_order_info.JOB_SITE_ADD);
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'city',
                         p_value   => r_order_info.JOB_SITE_ADD_CITY);
  
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'phoneNumber1',
                         p_value   => r_order_info.JOB_SITE_PHONE_NUMBER);

        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'postalCode',
                         p_value   => r_order_info.JOB_SITE_ADD_ZIP);
 
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'stateCode',
                         p_value   => r_order_info.JOB_SITE_ADD_STATE);
                                                                       
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'crossStreet1',
                         p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET1);
                         
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'crossStreet2',
                         p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET2);
                                                         
        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</address>');
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerName2',
                         p_value   => r_order_info.JOB_SITE_NAME);    
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerCompanyName',
                         p_value   => r_order_info.CUSTOMER_NAME);   
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerID',
                         p_value   => r_order_info.CUSTOMER_NUMBER);     

     END IF;                                                   

     IF l_order_number <> r_order_info.ORDER_NUMBER THEN
        --Check if delivery is different and it is not a first record, then close the invoice element    
        IF l_order_number <> -1 THEN
          --start close the invoices element        
          XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</invoices>');
                                                   
          --end close the invoices element   
        END IF;
        
        l_order_number := r_order_info.ORDER_NUMBER;
        l_print_invoice_tag := TRUE;
             
        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<invoices>'); 
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ORDER_NUMBER);   
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => r_order_info.JOB_SITE_CONTACT); 
    
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'comment',
                         p_value   => r_order_info.SHIPPING_INSTRUCTIONS);
     END IF;                  

     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<items>'); 
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ITEM_NUMBER);   
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom2',
                         p_value   => r_order_info.LINE_NUMBER); 
                             
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'description',
                         p_value   => r_order_info.ITEM_DESCRIPTION);

     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'pieceCount',
                         p_value   => r_order_info.SHIPMENT_QUANTITY); 
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'weight',
                         p_value   => r_order_info.ITEM_UNIT_WEIGHT);  
                                                                      
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => r_order_info.item_uom);   
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'UOM');
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');
       
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</items>'); 
     
     IF l_print_invoice_tag = TRUE THEN
        l_print_invoice_tag := FALSE;
        
        l_invoice_close := NULL;
        
        --start close the invoices element        
        --delivery_id as attribute value -- invoices -->attributes
        XXWC_OM_DMS2_OB_PKG.add_toclob_complex_parameter(p_footer_clob => l_invoice_close ,
                                                                   p_xml    => '</invoices>');
        --end close the invoices element      
     END IF;

     IF l_add_toclob_info = TRUE THEN
        l_add_toclob_info := FALSE;
        
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'shipmentType',
                         p_value   => 'CustomerDelivery');  
        
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'internalOrderId',
                         p_value   => r_order_info.delivery_id);  
        
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'scheduledDeliveryDate',
                         p_value   => r_order_info.SCHEDULED_DELIVERY_DATE);   
                         
        fnd_file.put_line(fnd_file.log, 'branch code : '|| r_order_info.branch_code);
       
        l_dms_branch_city := XXWC_OM_DMS2_OB_PKG.GET_DMS_ORG( p_branch_code => r_order_info.branch_code);
       
        fnd_file.put_line(fnd_file.log, 'l_dms_branch_city : '|| l_dms_branch_city);

        --Rev#5.0 <Start
        l_cd_dms_branch_city := XXWC_OM_DMS2_OB_PKG.GET_DMS2_CENTRAL_DPATCHG_ORG( p_branch_code => r_order_info.branch_code);
        
        fnd_file.put_line(fnd_file.log, 'l_cd_dms_branch_city : '|| l_cd_dms_branch_city);
        
        IF l_cd_dms_branch_city IS NOT NULL THEN
           XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'sellingStoreNumber',
                         p_value   => l_cd_dms_branch_city);
        ELSE--Rev#5.0 >End
           XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'sellingStoreNumber',
                         p_value   => l_dms_branch_city);
        END IF;--Rev#5.0  
                         
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'fulfillingStoreNumber',
                         p_value   => l_dms_branch_city);                                                                                                                                        
     END IF ;  
     l_sec := 'End Loop r_order_info';                                                                                   
  END LOOP;
  
  l_sec := 'Afer Loop r_order_info';
  
  l_request.body := l_request.body||l_invoice_close;
  
  l_request.body := l_request.body||l_footer_clob;
  
  --orderd date as attribute value -- request -->attributes
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_ordered_date);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Ordered Date');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                                                   
  --cust_po_number as attribute value-- request -->attributes
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_cust_po_number);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Cust PO #');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                     
  --Order Type                              
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_xxwc_log_rec.ORDER_TYPE);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Order type');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');   
                                                   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'username',
                         p_value   => g_dms2_username);                       
        
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'passwordHash',
                         p_value   => g_dms2_passwordHash); 
                         
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'organizationKey',
                         p_value   => g_dms2_organizationkey);   
                         
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</request>');

  --Set the body xml to NULL if there is no data found
  IF l_data_found = FALSE THEN
     l_request.body := NULL; 
     p_xxwc_log_rec.ws_responsecode := 'NO_DATA_FOUND';   
     fnd_file.put_line (fnd_file.log, 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called');
     g_retcode := 1;
     g_err_msg := 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called';
  ELSE   
     XXWC_OM_DMS2_OB_PKG.invoke( p_request => l_request
                                     ,p_url     => l_url
                                     ,p_action  => l_soap_action
                                     ,p_xxwc_log_rec => p_xxwc_log_rec);
  END IF;                                   
  l_sec := 'End ShipmentSvc_clear_delivery'; 
EXCEPTION
WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the ShipmentSvc_clear_delivery procedure'||SQLERRM;
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
      
      write_output ('Error in ShipmentSvc_clear_delivery is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
      fnd_file.put_line(fnd_file.log, 'Exception Block of ShipmentSvc_clear_delivery Procedure');
      fnd_file.put_line(fnd_file.log,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log,'The Error Message Traced is : ' || p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.ShipmentSvc_clear_delivery',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  
  END ShipmentSvc_clear_delivery;
  
  PROCEDURE ShipmentSvc_WCD(p_order_number      IN NUMBER,
                            p_sch_delivery_date IN DATE,
                            p_delivery_id       IN NUMBER,
                            p_priority          IN VARCHAR2, --TMS# 20180213-00090
                            p_xxwc_log_rec      IN OUT xxwc_log_rec )
AS
  l_request   XXWC_OM_DMS2_OB_PKG.t_request;
  l_response  XXWC_OM_DMS2_OB_PKG.t_response;
  l_return                   VARCHAR2(32767);
  l_error_code               NUMBER;
  p_error_msg                VARCHAR2(2000);
  l_sec                      VARCHAR2 (2000);
  
  l_url                      VARCHAR2(32767);
  l_namespace                VARCHAR2(32767);
  l_method                   VARCHAR2(32767);
  l_soap_action              VARCHAR2(32767);
  l_result_name              VARCHAR2(32767);
  l_add_header_info          BOOLEAN := TRUE;
  l_add_toclob_info          BOOLEAN := TRUE;
  l_print_invoice_tag        BOOLEAN := TRUE;
  
  l_order_number             NUMBER := -1;
  l_ordered_date             VARCHAR2(200);
  l_cust_po_number           VARCHAR2(200);
  
  l_invoice_close            VARCHAR2(32767); 
  l_footer_clob              CLOB; 
  
  l_data_found               BOOLEAN := FALSE; -- to check if the data exists
  l_dms_branch_city          fnd_lookup_values.meaning%TYPE;
  l_user_def_org_code        mtl_parameters.organization_code%TYPE;
  l_ada_flag                 VARCHAR2(1);--Rev#3.0
  l_cd_dms_branch_city       fnd_lookup_values.description%TYPE;--Rev#5.0
  
  CURSOR c_order_info ( p_order_number IN NUMBER
                       ,p_delivery_id  IN NUMBER )
   IS
   SELECT 
   -- SUBSTR(hl.location_code, 1, INSTR(hl.location_code, ' ') - 1) branch_code
      ooh.order_number ORDER_NUMBER
      ,wsh_stg.delivery_id
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (party.party_name) CUSTOMER_NAME
      ,cust_acct.account_number CUSTOMER_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_su.location) JOB_SITE_NAME
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address1) JOB_SITE_ADD
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address2) JOB_SITE_ADD_CROSS_STREET1
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.address3) JOB_SITE_ADD_CROSS_STREET2
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.city) JOB_SITE_ADD_CITY
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.state) JOB_SITE_ADD_STATE
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ship_loc.postal_code) JOB_SITE_ADD_ZIP
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters ( XXWC_OM_DMS2_OB_PKG.get_job_site_contact (p_ship_to_contact_id => ooh.ship_to_contact_id)) JOB_SITE_PHONE_NUMBER --Rev#6.0
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters ( ship_party.person_title || '  '|| ship_party.PERSON_FIRST_NAME || '  '|| ship_party.PERSON_LAST_NAME) JOB_SITE_CONTACT
      ,TO_CHAR(ooh.ordered_date,'YYYY-MM-DD')||'T'||to_char(ooh.ordered_date,'HH24:MI:SS')||'Z' ORDERED_DATE
      ,TO_CHAR(p_sch_delivery_date,'YYYY-MM-DD')||'T'||to_char(p_sch_delivery_date,'HH24:MI:SS')||'Z' SCHEDULED_DELIVERY_DATE
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (ooh.cust_po_number) CUST_PO_NUMBER
      --,ool.line_number
      ,(ool.line_number||'.'||ool.shipment_number) line_number
      ,ool.ordered_item ITEM_NUMBER
      ,XXWC_OM_DMS2_OB_PKG.replace_special_characters (msib.description)  ITEM_DESCRIPTION
      ,ool.order_quantity_uom
      ,NVL(wsh_stg.transaction_qty, 0) * NVL(msib.unit_weight, 0) ITEM_UNIT_WEIGHT
      ,NVL(wsh_stg.transaction_qty, 0) SHIPMENT_QUANTITY
      ,ool.order_quantity_uom ITEM_UOM
      ,TRIM(REGEXP_REPLACE(ASCIISTR(XXWC_OM_DMS2_OB_PKG.replace_special_characters (ooh.shipping_instructions)),'\\[[:xdigit:]]{4}','')) SHIPPING_INSTRUCTIONS
      ,ot.name order_type
      ,ooh.attribute9 ADA_AddressLine1 --Rev#3.0
      ,ooh.attribute10 ADA_City --Rev#3.0
      ,ooh.attribute14 ADA_State --Rev#3.0
      ,ooh.attribute15 ADA_Zip  --Rev#3.0
     FROM oe_order_headers_all ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , oe_order_lines_all ool
       , mtl_system_items_b msib
       , xxwc_wsh_shipping_stg wsh_stg
      WHERE ooh.header_id                   = ool.header_id
        AND msib.inventory_item_id          = ool.inventory_item_id
        AND msib.organization_id            = ool.ship_from_org_id
        AND wsh_stg.header_id               = ooh.header_id
        AND wsh_stg.line_id                 = ool.line_id 
        AND ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND ool.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = 'CONTACT'
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ool.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND ooh.order_number                = p_order_number
        AND wsh_stg.delivery_id             = p_delivery_id
        AND wsh_stg.status                  = 'DELIVERED'
        AND ool.source_type_code            = 'INTERNAL'
        --AND wsh_stg.delivery_id             = NVL(p_delivery_id, wsh_stg.delivery_id)
        /**AND EXISTS                          ( SELECT 1
                                                FROM fnd_lookup_values flv
                                               WHERE 1=1
                                                 AND flv.meaning      = ool.shipping_method_code
                                                 AND flv.lookup_type  = 'XXWC_DMS2_SHIPPING_METHOD_CODE'
                                                 AND flv.enabled_flag = 'Y'
                                                 AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                                             )**/ -- need to send all types of shipping method
        ORDER BY ooh.order_number, wsh_stg.delivery_id;
BEGIN
  l_sec := 'Srart ShipmentSvc_WCD p_order_number : ' ||p_order_number || ' p_delivery_id: '||p_delivery_id;
  l_url         := g_dms2_shipmentsvc_url;--'https://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl';
  l_namespace   := NULL;--'xmlns="http://shipment.services.operation.cuberoute.com"';
  l_method      := 'ship:shipment';
  --l_soap_action := 'http://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl/ship';
  l_soap_action := g_dms2_shipmentsvc_url||'/ship';
  l_result_name := 'return';
  l_ada_flag := 'N';--Rev#3.0
  
  mo_global.set_policy_context('S',162);
  
  l_request := XXWC_OM_DMS2_OB_PKG.new_request( p_method       => l_method,
                                                     p_namespace    => l_namespace);

  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<request>');
  
  l_sec := 'Before ShipmentSvc_std_int_ord';
  
  FOR r_order_info IN c_order_info ( p_order_number => p_order_number
                                    ,p_delivery_id  => p_delivery_id)
  LOOP    
     l_data_found := TRUE; -- set to true if to generate xml file
     
     l_sec := 'Inside r_order_info p_order_number : '|| r_order_info.order_number|| ' Delivery_id : '||r_order_info.Delivery_id ||' LINE_NUMBER '|| r_order_info.LINE_NUMBER || ' Item Number : '|| r_order_info.Item_number;
     write_output (l_sec);
          
     l_ordered_date    := r_order_info.ordered_date;
     l_cust_po_number  := r_order_info.cust_po_number; 
     IF l_add_header_info = TRUE THEN
        l_add_header_info := FALSE;
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'operationCode',
                         p_value   => 'Edit');
 
        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                       p_xml    => '<address>');
  
        --Rev#3.0 <Start
        IF IS_SO_ADA_EXISTS ( p_order_number => r_order_info.order_number) = 'Y' THEN 
           l_ada_flag := 'Y'; 
        
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'addressLine1',
                            p_value   => r_order_info.ADA_AddressLine1);
                            
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'city',
                            p_value   => r_order_info.ADA_CITY);
                            
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'postalCode',
                            p_value   => r_order_info.ADA_Zip);
 
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'stateCode',
                            p_value   => r_order_info.ADA_State);                                                        
        ELSE --Rev#3.0 >End
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'addressLine1',
                            p_value   => r_order_info.JOB_SITE_ADD);
                         
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'city',
                            p_value   => r_order_info.JOB_SITE_ADD_CITY);
  
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'phoneNumber1',
                            p_value   => r_order_info.JOB_SITE_PHONE_NUMBER);

           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'postalCode',
                            p_value   => r_order_info.JOB_SITE_ADD_ZIP);
 
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'stateCode',
                            p_value   => r_order_info.JOB_SITE_ADD_STATE);
        
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'crossStreet1',
                            p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET1);
                                                                                        
           XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'crossStreet2',
                            p_value   => r_order_info.JOB_SITE_ADD_CROSS_STREET2);
        END IF;--Rev#3.0 
                                                         
        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</address>');
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerName2',
                         p_value   => r_order_info.JOB_SITE_NAME);    
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerCompanyName',
                         p_value   => r_order_info.CUSTOMER_NAME);   
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerID',
                         p_value   => r_order_info.CUSTOMER_NUMBER);     

     END IF;                                                   

     IF l_order_number <> r_order_info.ORDER_NUMBER THEN
        --Check if delivery is different and it is not a first record, then close the invoice element    
        IF l_order_number <> -1 THEN
          --start close the invoices element        
          XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</invoices>');
                                                   
          --end close the invoices element   
        END IF;
        
        l_order_number := r_order_info.ORDER_NUMBER;
        l_print_invoice_tag := TRUE;
             
        XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<invoices>'); 
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ORDER_NUMBER);   
        
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => r_order_info.JOB_SITE_CONTACT); 
    
        XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'comment',
                         p_value   => r_order_info.SHIPPING_INSTRUCTIONS);
     END IF;                  

     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<items>'); 
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom',
                         p_value   => r_order_info.ITEM_NUMBER);   
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'custom2',
                         p_value   => r_order_info.LINE_NUMBER); 
                             
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'description',
                         p_value   => r_order_info.ITEM_DESCRIPTION);

     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'pieceCount',
                         p_value   => r_order_info.SHIPMENT_QUANTITY); 
     
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'weight',
                         p_value   => r_order_info.ITEM_UNIT_WEIGHT);  
                                                                      
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => r_order_info.item_uom);   
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'UOM');
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');
       
     XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</items>'); 
     
     IF l_print_invoice_tag = TRUE THEN
        l_print_invoice_tag := FALSE;
        
        l_invoice_close := NULL;
        
        --start close the invoices element        
        --delivery_id as attribute value -- invoices -->attributes
        XXWC_OM_DMS2_OB_PKG.add_toclob_complex_parameter(p_footer_clob => l_invoice_close ,
                                                                   p_xml    => '</invoices>');
        --end close the invoices element      
     END IF;

     IF l_add_toclob_info = TRUE THEN
        l_add_toclob_info := FALSE;
        
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'shipmentType',
                         p_value   => 'CustomerDelivery');  
        
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'internalOrderId',
                         p_value   => r_order_info.delivery_id);  
        
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'scheduledDeliveryDate',
                         p_value   => r_order_info.SCHEDULED_DELIVERY_DATE);   
        BEGIN
          SELECT ORGANIZATION_CODE	
            INTO l_user_def_org_code
            FROM mtl_parameters mp
           WHERE mp.organization_id = fnd_profile.VALUE('XXWC_OM_DEFAULT_SHIPPING_ORG'); --Rev#5.0
        EXCEPTION
        WHEN OTHERS THEN
           l_user_def_org_code := NULL;
        END;   
       
        l_dms_branch_city := XXWC_OM_DMS2_OB_PKG.GET_DMS_ORG( p_branch_code => l_user_def_org_code);
       
        fnd_file.put_line(fnd_file.log, 'l_dms_branch_city : '|| l_dms_branch_city);

        --Rev#5.0 <Start
        l_cd_dms_branch_city := XXWC_OM_DMS2_OB_PKG.GET_DMS2_CENTRAL_DPATCHG_ORG( p_branch_code => l_user_def_org_code);
        
        fnd_file.put_line(fnd_file.log, 'l_cd_dms_branch_city : '|| l_cd_dms_branch_city);
        
        IF l_cd_dms_branch_city IS NOT NULL THEN
          XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'sellingStoreNumber',
                         p_value   => l_cd_dms_branch_city);
        ELSE--Rev#5.0 >End
          XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'sellingStoreNumber',
                         p_value   => l_dms_branch_city);
        END IF;--Rev#5.0  
                         
        XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                         p_name    => 'fulfillingStoreNumber',
                         p_value   => l_dms_branch_city);                                                                                                                                  
     END IF ;  
     l_sec := 'End Loop r_order_info';                                                                                   
  END LOOP;
  
  l_sec := 'Afer Loop r_order_info';
  
  l_request.body := l_request.body||l_invoice_close;
  
  l_request.body := l_request.body||l_footer_clob;
  
  --orderd date as attribute value -- request -->attributes
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_ordered_date);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Ordered Date');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
                                                   
  --cust_po_number as attribute value-- request -->attributes
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_cust_po_number);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Cust PO #');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>'); 
  
  --Order Type                              
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => p_xxwc_log_rec.ORDER_TYPE || '-' ||'WCD');   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Order type');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');   
 
  --Rev#3.0 <Start 
  --ADA                              
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'value',
                         p_value   => l_ada_flag);   
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'ADA');
                         
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</attributes>');   

  --Priority                              
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '<attributes>'); 
  
  IF p_priority = 'Y' THEN
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'value',
                            p_value   => 'Y');
  ELSE
     XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                            p_name    => 'value',
                            p_value   => 'N');
  END IF;                          
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'name',
                         p_value   => 'Priority');
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                             p_xml    => '</attributes>'); 
  --Rev#3.0 > End
                                                                       
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'username',
                         p_value   => g_dms2_username);                       
        
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'passwordHash',
                         p_value   => g_dms2_passwordHash); 
                         
  XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'organizationKey',
                         p_value   => g_dms2_organizationkey);   
                         
  XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml    => '</request>');

  --Set the body xml to NULL if there is no data found
  IF l_data_found = FALSE THEN
     l_request.body := NULL; 
     p_xxwc_log_rec.ws_responsecode := 'NO_DATA_FOUND';   
     fnd_file.put_line (fnd_file.log, 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called');

     g_retcode := 1;
     g_err_msg := 'No Data Found for the Order Number : '||p_order_number || ' Shipment service will not be called';
  ELSE   
     XXWC_OM_DMS2_OB_PKG.invoke( p_request => l_request
                                     ,p_url     => l_url
                                     ,p_action  => l_soap_action
                                     ,p_xxwc_log_rec => p_xxwc_log_rec);
  END IF;                                   
  
  l_sec := 'End Procedure ShipmentSvc_WCD';
  g_retcode:=0;
  fnd_file.put_line (fnd_file.log, 'End Procedure ShipmentSvc_WCD');  
EXCEPTION
WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the ShipmentSvc_WCD procedure'||SQLERRM;
    
      g_retcode := 2;
      g_err_msg := p_error_msg;
      
      write_output ('Error in ShipmentSvc_WCD is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
      fnd_file.put_line(fnd_file.log, 'Exception Block of ShipmentSvc_WCD Procedure');
      fnd_file.put_line(fnd_file.log,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log,'The Error Message Traced is : ' || p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.ShipmentSvc_WCD',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  
  END ShipmentSvc_WCD;
  
  /********************************************************************************
  ProcedureName : Send_to_DMS_WCD
  Purpose       : This will be called by printer form
  ----------------------------------------------------------------------------------*/
  PROCEDURE Send_to_DMS_WCD( p_order_header_id   IN NUMBER
                            ,p_delivery_id       IN NUMBER
                            ,p_sch_delivery_date IN DATE DEFAULT SYSDATE
                            ,p_priority          IN VARCHAR2 --TMS# 20180213-00090
                           ) AS
    
    l_error_code         NUMBER;
    p_error_msg          VARCHAR2(2000);
	l_sec                VARCHAR2 (150);
	l_sch_delivery_date  VARCHAR2(11);
    l_Shipment_Type      VARCHAR2 (150);
    l_request_id         NUMBER := 0;
    l_line_count         NUMBER := 0;
    l_order_type_id      NUMBER := 0;
  
  BEGIN
     IF FND_PROFILE.VALUE ('XXWC_OM_DMS2_DEBUG_FLAG') = 'Y' THEN
        XXWC_OM_DMS2_OB_PKG.debug_on;
     END IF;
       
     l_sec := 'Start of Procedure : Send_to_DMS_WCD';
     
     l_sch_delivery_date := TO_CHAR (p_sch_delivery_date, 'DD-MON-YYYY');
     write_output ('Start of Procedure Send_to_DMS_WCD p_order_header_id: '||p_order_header_id|| ' p_delivery_id : '||p_delivery_id);
          
     l_request_id :=  fnd_request.submit_request  (    application      => 'XXWC',
	                                                   program          => 'XXWC_OM_DMS2_OB',
	                                                   description      => NULL,
	                                                   start_time       => SYSDATE,
	                                                   sub_request      => FALSE,
	                                                   argument1        => p_order_header_id, --Sales Order / PO Header Id
	                                                   argument2        => l_sch_delivery_date, --Sch delivery date(DD-MON-YYYY)
	                                                   argument3        => p_delivery_id, --Delivery id
	                                                   argument4        => 'CustomerDelivery', --Shipment Type
	                                                   argument5        => 'SEND_TO_DMS_SO_WCD', --Interface Type
  	                                                   argument6        =>  FND_PROFILE.VALUE ('XXWC_OM_DMS2_DEBUG_FLAG'),--Debug
  	                                                   argument7        => NULL, --Request id
  	                                                   argument8        => p_priority --TMS# 20180213-00090
	                                                 );	
     --Commit Updates 	
     xxwc_ascp_scwb_pkg.proccommit;
     
     write_output ('After submitting concurrent request l_request_id: '||l_request_id);

     l_sec := 'End of Procedure : Send_to_DMS_WCD';
  EXCEPTION
    WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the Send_to_DMS_WCD procedure'||SQLERRM;
    
      write_output ('Error in XXWC_OM_DMS2_OB_PKG.Send_to_DMS_WCD is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
    
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.Send_to_DMS_WCD',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  						
  END Send_to_DMS_WCD;
  
  --Rev#4.0 <Start
  /********************************************************************************
  ProcedureName : Send_to_DMS_INT_PICKUP
  Purpose       : This will be called by xxwc_om_int_order_pkg.submit_internal_order_pick_rpt 
                  package to create a pickup for the destination branch 
  ----------------------------------------------------------------------------------*/
  PROCEDURE Send_to_DMS_INT_PICKUP( p_order_header_id   IN NUMBER
                                   ,p_sch_delivery_date IN DATE 
                                   ,p_request_id        IN NUMBER
                                   ) AS
    
    l_error_code         NUMBER;
    p_error_msg          VARCHAR2(2000);
	l_sec                VARCHAR2 (150);
	l_sch_delivery_date  VARCHAR2(11);
    l_request_id         NUMBER := 0;
  BEGIN
     IF FND_PROFILE.VALUE ('XXWC_OM_DMS2_DEBUG_FLAG') = 'Y' THEN
        XXWC_OM_DMS2_OB_PKG.debug_on;
     END IF;
       
     l_sec := 'Start of Procedure : Send_to_DMS_INT_PICKUP';
     
     l_sch_delivery_date := TO_CHAR (p_sch_delivery_date, 'DD-MON-YYYY');
     write_output ('Start of Procedure Send_to_DMS_INT_PICKUP p_order_header_id: '||p_order_header_id);
          
     l_request_id :=  fnd_request.submit_request  (    application      => 'XXWC',
	                                                   program          => 'XXWC_OM_DMS2_OB',
	                                                   description      => NULL,
	                                                   start_time       => SYSDATE,
	                                                   sub_request      => FALSE,
	                                                   argument1        => p_order_header_id, --Sales Order / PO Header Id
	                                                   argument2        => l_sch_delivery_date, --Sch delivery date(DD-MON-YYYY)
	                                                   argument3        => NULL, --Delivery id
	                                                   argument4        => 'CustomerPickup', --Shipment Type
	                                                   argument5        => 'SEND_TO_DMS_SO_INT_PICKUP', --Interface Type
  	                                                   argument6        => FND_PROFILE.VALUE ('XXWC_OM_DMS2_DEBUG_FLAG'),--Debug
  	                                                   argument7        => p_request_id, --Request id
  	                                                   argument8        => 'N' --Priority 
	                                                 );	
     --Commit Updates 	
     xxwc_ascp_scwb_pkg.proccommit;
     
     write_output ('After submitting concurrent request l_request_id: '||l_request_id);

     l_sec := 'End of Procedure : Send_to_DMS_INT_PICKUP';
  EXCEPTION
    WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the Send_to_DMS_INT_PICKUP procedure'||SQLERRM;
    
      write_output ('Error in XXWC_OM_DMS2_OB_PKG.Send_to_DMS_INT_PICKUP is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
    
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.Send_to_DMS_INT_PICKUP',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  						
  END Send_to_DMS_INT_PICKUP;
  --Rev#4.0 > End
  
  PROCEDURE ShipmentSvc_po(p_header_id         IN NUMBER,
                          p_xxwc_log_rec IN OUT xxwc_log_rec,
                          p_promised_date     IN DATE, --Rev#4.0
                          p_operationCode     IN VARCHAR2) AS
   l_request    XXWC_OM_DMS2_OB_PKG.t_request;
   l_error_code NUMBER;
   p_error_msg  VARCHAR2(2000);
   l_sec        VARCHAR2(2000);
 
   l_url               VARCHAR2(32767);
   l_namespace         VARCHAR2(32767);
   l_method            VARCHAR2(32767);
   l_soap_action       VARCHAR2(32767);
   l_result_name       VARCHAR2(32767);
   l_add_header_info   BOOLEAN := TRUE;
   l_add_toclob_info   BOOLEAN := TRUE;
   l_print_invoice_tag BOOLEAN := TRUE;
 
   l_order_number   NUMBER := -1;
   l_ordered_date   VARCHAR2(200);
   l_cust_po_number VARCHAR2(200);
 
   l_invoice_close VARCHAR2(32767);
   l_footer_clob   CLOB;
   l_address1      po_headers_XML.CP_VC_ADDRESS_LINE1%type;
   l_address2      po_headers_XML.CP_VC_ADDRESS_LINE2%type;
   l_address3      po_headers_XML.CP_VC_ADDRESS_LINE3%type;
   l_city          po_headers_XML.CP_VC_TOWN_CITY%type;
   l_state         po_headers_XML.CP_VC_STATE_PROVICE%type;
   l_zip           po_headers_XML.CP_VC_POSTAL_CODE%type;
   l_phone_number  po_headers_XML.CP_VC_PHONE%type;
 
   l_data_found BOOLEAN := FALSE; -- to check if the data exists
   l_dms_branch_city          fnd_lookup_values.meaning%TYPE;
   l_cd_dms_branch_city       fnd_lookup_values.description%TYPE;--Rev#5.0
   l_quantity_received        rcv_shipment_lines.quantity_received%TYPE;--Rev#7.0 
   l_open_quantity            rcv_shipment_lines.quantity_received%TYPE;--Rev#7.0
   
   CURSOR c_order_info(p_header_id IN NUMBER ) IS
     select SUBSTR(ph.ship_to_location, 1, INSTR(ph.ship_to_location, ' ') - 1) branch_code,
            ph.segment1 po_number,
            XXWC_OM_DMS2_OB_PKG.replace_special_characters(pv.VENDOR_NAME) supplier_name,
            pv.SEGMENT1 supplier_number,
            XXWC_OM_DMS2_OB_PKG.replace_special_characters(NVL(ph_xml.CP_VC_ADDRESS_LINE1, ph_xml.VENDOR_ADDRESS_LINE1)) address1,--#Rev2.0
            XXWC_OM_DMS2_OB_PKG.replace_special_characters(ph_xml.CP_VC_ADDRESS_LINE2) address2,
            XXWC_OM_DMS2_OB_PKG.replace_special_characters(ph_xml.CP_VC_ADDRESS_LINE3) address3,
            XXWC_OM_DMS2_OB_PKG.replace_special_characters(NVL(ph_xml.CP_VC_TOWN_CITY, ph_xml.VENDOR_CITY)) city,--#Rev2.0
            XXWC_OM_DMS2_OB_PKG.replace_special_characters(NVL(ph_xml.CP_VC_STATE_PROVICE, ph_xml.VENDOR_STATE)) state,--#Rev2.0
            XXWC_OM_DMS2_OB_PKG.replace_special_characters(NVL(ph_xml.CP_VC_POSTAL_CODE, ph_xml.VENDOR_POSTAL_CODE))  zip,--#Rev2.0
            XXWC_OM_DMS2_OB_PKG.replace_special_characters(NVL(ph_xml.CP_VC_PHONE, ph_xml.VENDOR_PHONE)) phone,--#Rev2.0
            XXWC_OM_DMS2_OB_PKG.replace_special_characters(ph.VENDOR_CONTACT) vendor_contact,
            XXWC_OM_DMS2_OB_PKG.replace_special_characters(pl.item_number)  item_number,
            XXWC_OM_DMS2_OB_PKG.replace_special_characters(pl.item_description)  item_description,
            pl.line_num line_number,
            pl.quantity SHIPMENT_QUANTITY,
            pl.unit_meas_lookup_code item_uom,
            null ITEM_UNIT_WEIGHT,
            ph.note_to_vendor supplier_header_notes,
            pl.note_to_vendor supplier_line_notes,
            ph.agent_name buyer, 
            ph.approved_date,
            TO_CHAR(p_promised_date,'YYYY-MM-DD')||'T'||to_char(p_promised_date,'HH24:MI:SS')||'Z' SCHEDULED_DELIVERY_DATE,--Rev#4.0
            ph.po_header_id,--Rev#7.0
            pl.po_line_id--Rev#7.0
       from PO_HEADERS_V     ph,
            PO_LINES_V       pl,
            po_vendors       pv,
            po_headers_XML   ph_xml,
            po_line_locations_all poll--Rev#4.0
      WHERE ph.po_header_id = pl.po_header_id
        and pv.vendor_id = ph.vendor_id
        and ph.po_header_id = p_header_id
        and (NVL(pl.cancel_flag, 'N') != 'Y' or NVL(ph.cancel_flag, 'N') = 'Y') --Rev#5.0
        and ph_xml.PO_HEADER_ID = ph.po_header_id
        and ph.po_header_id = poll.po_header_id --Rev#4.0 <Start
        and pl.po_line_id = poll.po_line_id
        and (
             TO_CHAR(TRUNC(poll.promised_date), 'MM-DD-YYYY') = TO_CHAR(TRUNC(p_promised_date), 'MM-DD-YYYY')
            OR ( poll.promised_date IS NULL 
             AND  TO_CHAR(TRUNC(TO_DATE(NVL(ph.attribute6, ph.attribute1), 'YYYY/MM/DD HH24:MI:SS')), 'MM-DD-YYYY') 
                                              = TO_CHAR(TRUNC(p_promised_date), 'MM-DD-YYYY')
              )
           
         );--Rev#4.0 >End
 BEGIN 
   l_sec         := 'Start ShipmentSvc_po p_header_id: ' || p_header_id;
   fnd_file.put_line(fnd_file.log,l_sec ||' p_promised_date: '||p_promised_date);--Rev#4.0 
   
   l_url         := g_dms2_shipmentsvc_url; --'https://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl';
   l_namespace   := NULL; --'xmlns="http://shipment.services.operation.cuberoute.com"';
   l_method      := 'ship:shipment';
   --l_soap_action := 'http://ondemand.qa.descartes.com/axis2/services/ShipmentService?wsdl/ship';
   l_soap_action := g_dms2_shipmentsvc_url||'/ship';
   l_result_name := 'return';
 
   mo_global.set_policy_context('S', 162);
 
   l_request := XXWC_OM_DMS2_OB_PKG.new_request(p_method    => l_method,
                                                        p_namespace => l_namespace);
 
   XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                     p_xml     => '<request>');
 
   l_sec := 'Before c_order_info';
   FOR r_order_info IN c_order_info(p_header_id => p_header_id) LOOP
      
      --Rev#7.0 <Start 
      BEGIN
         SELECT SUM(rsl.quantity_received) 
           INTO l_quantity_received 
           FROM rcv_transactions rct
                 ,rcv_shipment_lines rsl
          WHERE rct.transaction_type = 'RECEIVE'
            AND rct.shipment_line_id = rsl.shipment_line_id
            AND rct.po_header_id	 = r_order_info.po_header_id
            AND rct.po_line_id       = r_order_info.po_line_id;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         l_quantity_received := 0;
      WHEN OTHERS THEN
         l_quantity_received := 0;
      END;   
      
      l_open_quantity := r_order_info.shipment_quantity - NVL(l_quantity_received, 0);
      
      IF l_open_quantity > 0 THEN
      --Rev#7.0 >End
         
         p_xxwc_log_rec.po_number := r_order_info.po_number;
     
         p_xxwc_log_rec.schedule_delivery_date := p_promised_date;--Rev#4.0 
     
         IF r_order_info.vendor_contact LIKE '1SEE NOTES%' then
            SELECT XXWC_OM_DMS2_OB_PKG.replace_special_characters(ADDRESS_LINE1) ADDRESS_LINE1,
                   XXWC_OM_DMS2_OB_PKG.replace_special_characters(ADDRESS_LINE2) ADDRESS_LINE2,
                   XXWC_OM_DMS2_OB_PKG.replace_special_characters(ADDRESS_LINE3) ADDRESS_LINE3,
                   XXWC_OM_DMS2_OB_PKG.replace_special_characters(CITY) city,
                   XXWC_OM_DMS2_OB_PKG.replace_special_characters(STATE) state,
                   XXWC_OM_DMS2_OB_PKG.replace_special_characters(ZIP) zip,
                   PHONE_NUMBER
              INTO l_address1,
                   l_address2,
                   l_address3,
                   l_city,
                   l_state,
                   l_zip,
                   l_phone_number
              FROM XXWC.XXWC_PO_ADDITIONAL_ATTRS
             WHERE po_header_id = p_header_id;
         END IF;
     
         l_data_found := TRUE; -- set to true if to generate xml file
         l_ordered_date   := r_order_info.approved_date;
         l_cust_po_number := r_order_info.supplier_number;
     
         IF l_add_header_info = TRUE THEN
            l_add_header_info := FALSE;
     
            XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                              p_name    => 'operationCode',
                                              p_value   => p_operationCode);
     
            XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                      p_xml     => '<address>');
                                                      
            IF r_order_info.vendor_contact LIKE '1SEE NOTES%' then
               XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                                 p_name    => 'addressLine1',
                                                 p_value   => l_address1); 
            ELSE
               XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                                 p_name    => 'addressLine1',
                                                 p_value   => r_order_info.address1); 
            END IF;
            
            IF r_order_info.vendor_contact LIKE '1SEE NOTES%' then
               XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                                 p_name    => 'city',
                                                 p_value   => l_city);
            ELSE
               XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                                 p_name    => 'city',
                                                 p_value   => r_order_info.city);
            END IF;
            
            IF r_order_info.vendor_contact LIKE '1SEE NOTES%' then
               XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                                 p_name    => 'phoneNumber1',
                                                 p_value   => l_phone_number);
            ELSE
               XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                                 p_name    => 'phoneNumber1',
                                                 p_value   => r_order_info.phone);
            END IF;
       
            IF r_order_info.vendor_contact LIKE '1SEE NOTES%' then
               XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                                 p_name    => 'postalCode',
                                                 p_value   => l_zip);
            ELSE
               XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                                 p_name    => 'postalCode',
                                                 p_value   => r_order_info.zip);
            END IF;
            
            IF r_order_info.vendor_contact LIKE '1SEE NOTES%' then
               XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                                 p_name    => 'stateCode',
                                                 p_value   => l_state);--
            ELSE
               XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                                 p_name    => 'stateCode',
                                                 p_value   => r_order_info.state);
            END IF;
      
            XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                      p_xml     => '</address>');
            XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                         p_name    => 'customerCompanyName',
                         p_value   => r_order_info.supplier_name);   
     
            XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                                 p_name    => 'customerID',
                                                 p_value   => r_order_info.supplier_number);
     
         END IF;
   
         IF l_order_number <> r_order_info.po_number THEN
            --Check if delivery is different and it is not a first record, then close the invoice element    
            IF l_order_number <> -1 THEN
               --start close the invoices element        
               XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                        p_xml     => '</invoices>');
       
               --end close the invoices element   
            END IF;
     
            l_order_number      := r_order_info.po_number;
            l_print_invoice_tag := TRUE;
     
            XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                      p_xml     => '<invoices>');
     
            XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                              p_name    => 'custom',
                                              p_value   => r_order_info.po_number); 
     
            XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                              p_name    => 'comment',
                                              p_value   => r_order_info.supplier_header_notes);
         END IF;
   
         XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml     => '<items>');
   
         XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                           p_name    => 'custom',
                                           p_value   => r_order_info.ITEM_NUMBER);
  
         XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                           p_name    => 'description',
                                           p_value   => r_order_info.ITEM_DESCRIPTION);
   
         XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                           p_name    => 'pieceCount',
                                           p_value   => l_open_quantity --r_order_info.SHIPMENT_QUANTITY Rev#7.0
                                           );
   
         XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                           p_name    => 'weight',
                                           p_value   => r_order_info.ITEM_UNIT_WEIGHT);
   
         XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml     => '<attributes>');
         
         XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                           p_name    => 'value',
                                           p_value   => r_order_info.item_uom);
         
         XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                           p_name    => 'name',
                                           p_value   => 'UOM');
         
         XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml     => '</attributes>');
                                                   
         XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml     => '<attributes>');  
     
         XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                            p_name    => 'value',
                                            p_value   => r_order_info.buyer);
         
         XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                           p_name    => 'name',
                                           p_value   => 'BUYER_NAME');
      
         XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                   p_xml     => '</attributes>');
   
         XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                    p_xml     => '</items>');
   
         IF l_print_invoice_tag = TRUE THEN
            l_print_invoice_tag := FALSE;
     
            l_invoice_close := NULL;
     
            --start close the invoices element        
            --delivery_id as attribute value -- invoices -->attributes
            XXWC_OM_DMS2_OB_PKG.add_toclob_complex_parameter(p_footer_clob => l_invoice_close,
                                                                p_xml         => '</invoices>');
            --end close the invoices element      
         END IF;
   
         IF l_add_toclob_info = TRUE THEN
            l_add_toclob_info := FALSE;
     
            XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                                           p_name        => 'shipmentType',
                                           p_value       => 'CustomerPickup');
     
            XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                                           p_name        => 'scheduledDeliveryDate',
                                           p_value       =>  r_order_info.SCHEDULED_DELIVERY_DATE );
                                       
            fnd_file.put_line(fnd_file.log, 'branch code : '|| r_order_info.branch_code);
       
            l_dms_branch_city := XXWC_OM_DMS2_OB_PKG.GET_DMS_ORG( p_branch_code => r_order_info.branch_code);
       
            fnd_file.put_line(fnd_file.log, 'l_dms_branch_city : '|| l_dms_branch_city);

            --Rev#5.0 <Start
            l_cd_dms_branch_city := XXWC_OM_DMS2_OB_PKG.GET_DMS2_CENTRAL_DPATCHG_ORG( p_branch_code => r_order_info.branch_code);
        
            fnd_file.put_line(fnd_file.log, 'l_cd_dms_branch_city : '|| l_cd_dms_branch_city);
         
            IF l_cd_dms_branch_city IS NOT NULL THEN
               XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                                                  p_name    => 'sellingStoreNumber',
                                                  p_value   => l_cd_dms_branch_city);
            ELSE--Rev#5.0 >End
               XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                                              p_name        => 'sellingStoreNumber',
                                              p_value       => l_dms_branch_city);
            END IF;--Rev#5.0  
                         
            XXWC_OM_DMS2_OB_PKG.add_toclob(p_footer_clob => l_footer_clob,
                                           p_name        => 'fulfillingStoreNumber',
                                           p_value       => l_dms_branch_city);  
                         
         END IF;
         l_sec := 'End Loop r_order_info';
         
      END IF;--Rev#7.0    
   END LOOP;
 
   l_sec := 'Afer Loop r_order_info';
 
   l_request.body := l_request.body || l_invoice_close;
 
   l_request.body := l_request.body || l_footer_clob;
 
   --Purchase Order                        
   XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                     p_xml     => '<attributes>');
   XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                             p_name    => 'value',
                                             p_value   => 'Purchase Order');
   XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                             p_name    => 'name',
                                             p_value   => 'Order type');
   XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                     p_xml     => '</attributes>');
 
   XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                             p_name    => 'username',
                                             p_value   => g_dms2_username);
 
   XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                             p_name    => 'passwordHash',
                                             p_value   => g_dms2_passwordHash);
 
   XXWC_OM_DMS2_OB_PKG.add_parameter(p_request => l_request,
                                             p_name    => 'organizationKey',
                                             p_value   => g_dms2_organizationkey);
 
   XXWC_OM_DMS2_OB_PKG.add_complex_parameter(p_request => l_request,
                                                     p_xml     => '</request>');
 
   --Set the body xml to NULL if there is no data found
   IF l_data_found = FALSE THEN
     l_request.body                 := NULL;
     p_xxwc_log_rec.ws_responsecode := 'NO_DATA_FOUND';
     fnd_file.put_line(fnd_file.log,
                       'No Data Found for the PO Number : ' ||
                       p_header_id ||
                       ' Shipment service will not be called');
     g_retcode := 1;
     g_err_msg := 'No Data Found for the Order Number : ' || p_header_id ||
                  ' Shipment service will not be called';
   ELSE
     XXWC_OM_DMS2_OB_PKG.invoke(p_request      => l_request,
                                        p_url          => l_url,
                                        p_action       => l_soap_action,
                                        p_xxwc_log_rec => p_xxwc_log_rec);
   END IF;
   l_sec := 'End ShipmentSvc_po';
 EXCEPTION
   WHEN OTHERS THEN
     l_error_code := SQLCODE;
     p_error_msg  := 'Error in the ShipmentSvc_po procedure' ||
                     SQLERRM;
   
     g_retcode := 2;
     g_err_msg := p_error_msg;
   
     write_output('Error in ShipmentSvc_po is SQLERROR CODE' ||
                  l_error_code || 'SQLERRM : ' || SQLERRM);
     fnd_file.put_line(fnd_file.log,
                       'Exception Block of ShipmentSvc_po Procedure');
     fnd_file.put_line(fnd_file.log,
                       'The Error Code Traced is : ' || l_error_code);
     fnd_file.put_line(fnd_file.log,
                       'The Error Message Traced is : ' || p_error_msg);
     xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_package ||
                                                                 '.ShipmentSvc_po',
                                          p_calling           => l_sec,
                                          p_request_id        => fnd_global.conc_request_id,
                                          p_ora_error_msg     => SUBSTR(' Error_Stack...' ||
                                                                        DBMS_UTILITY.format_error_stack() ||
                                                                        ' Error_Backtrace...' ||
                                                                        DBMS_UTILITY.format_error_backtrace(),
                                                                        1,
                                                                        2000),
                                          p_error_desc        => SUBSTR(p_error_msg,
                                                                        1,
                                                                        240),
                                          p_distribution_list => g_dflt_email,
                                          p_module            => g_module);
 END ShipmentSvc_po;
 
  /********************************************************************************
  ProcedureName : MAIN
  Purpose       : This is mail program and will be called by concurrent program to 
                  shipment webservice
  
  HISTORY
   ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/20/2017    Rakesh Patel    Initial version.
  ********************************************************************************/
  ----------------------------------------------------------------------------------
  -- Main Procedure
  ----------------------------------------------------------------------------------
  PROCEDURE main(errbuf              OUT VARCHAR2
                ,retcode             OUT NUMBER
                ,p_header_id         IN NUMBER
                ,p_sch_delivery_date IN VARCHAR2
                ,p_delivery_id       IN NUMBER
                ,p_shipment_type     IN VARCHAR2
                ,p_interface_type    IN VARCHAR2
                ,p_debug_flag        IN VARCHAR2
                ,p_request_id        IN NUMBER
                ,p_priority          IN VARCHAR2 --TMS# 20180213-00090
                 ) AS
    
    l_error_code         NUMBER;
    p_error_msg          VARCHAR2(2000);
	l_sec                VARCHAR2 (100);
	l_sch_delivery_date  DATE;
    l_xxwc_log_rec       xxwc_log_rec;
    l_order_type         OE_TRANSACTION_TYPES_TL.NAME%TYPE;
    l_order_number       oe_order_headers_all.order_number%TYPE;
    l_line_type_id       NUMBER;
    l_order_found        BOOLEAN := FALSE;
    
    CURSOR c_order_delivery_info ( p_header_id     IN NUMBER
                                  ,p_delivery_id   IN NUMBER
                                 )
   IS
      SELECT DISTINCT wsh_stg.delivery_id, ooh.order_number, ott.name order_type
        FROM oe_order_headers_all ooh
            ,xxwc_wsh_shipping_stg wsh_stg
            ,oe_transaction_types_tl ott
       WHERE ooh.order_type_id   = ott.transaction_type_id 
         AND wsh_stg.header_id   = ooh.header_id
         AND ooh.header_id       = p_header_id
         AND wsh_stg.delivery_id = NVL(p_delivery_id, wsh_stg.delivery_id)
      ORDER BY wsh_stg.delivery_id;
    
   --Rev#4.0
   CURSOR c_promised_date ( p_header_id     IN NUMBER )
   IS 
       SELECT NVL(TRUNC(poll.promised_date),TO_DATE(NVL(poh.attribute6, poh.attribute1), 'YYYY/MM/DD HH24:MI:SS')) promised_date
       FROM po_line_locations_all poll, po_headers_all poh, po_lines_all pol
      where poh.po_header_id = p_header_id
        AND poh.po_header_id = pol.po_header_id
        AND poh.po_header_id = poll.po_header_id
        AND pol.po_line_id = poll.po_line_id
      GROUP BY NVL(TRUNC(poll.promised_date),TO_DATE(NVL(poh.attribute6, poh.attribute1), 'YYYY/MM/DD HH24:MI:SS'));
    --Rev#4.0  
  BEGIN
    l_sec := 'Start of Procedure : MAIN';
    g_retcode := 0;
        
    IF p_debug_flag = 'Y' THEN
       XXWC_OM_DMS2_OB_PKG.debug_on;
    ELSE
       XXWC_OM_DMS2_OB_PKG.debug_off;
    END IF;
    
    apps.mo_global.set_policy_context('S',162); 
  
    fnd_file.put_line(fnd_file.log, '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log, 'Start of Procedure : MAIN');
    fnd_file.put_line(fnd_file.log, '----------------------------------------------------------------------------------');
	  
    fnd_file.put_line(fnd_file.log, 'Input Parameters     :');
    fnd_file.put_line(fnd_file.log,'Order Header id        : ' || p_header_id);
    fnd_file.put_line(fnd_file.log,'Schedule delivery date : ' || p_sch_delivery_date);
    fnd_file.put_line(fnd_file.log,'Delivery id            : ' || p_delivery_id);
    fnd_file.put_line(fnd_file.log,'Shipment Type          : ' || p_shipment_type);
    fnd_file.put_line(fnd_file.log,'Interface Type         : ' || p_Interface_type);
    fnd_file.put_line(fnd_file.log,'Debug Flag             : ' || p_debug_flag);
    fnd_file.put_line(fnd_file.log,'p_request_id           : ' || p_request_id);
    fnd_file.put_line(fnd_file.log,'p_priority             : ' || p_priority);
    fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
    
    IF p_sch_delivery_date IS NOT NULL THEN
       l_sch_delivery_date := TO_DATE (p_sch_delivery_date, 'DD-MON-YYYY');
    ELSE
       l_sch_delivery_date := SYSDATE;
    END IF; 
    
    l_xxwc_log_rec.schedule_delivery_date := l_sch_delivery_date;
    
    write_output ('l_sch_delivery_date : '|| l_sch_delivery_date);
    
    l_xxwc_log_rec.rpt_con_request_id := p_request_id;
    
    write_output ('l_xxwc_log_rec.rpt_con_request_id : '|| l_xxwc_log_rec.rpt_con_request_id);
    
    g_wallet_path                   := FND_PROFILE.VALUE ('XXWC_OM_DMS2_WALLET_PATH');
    write_output ('Profile XXWC_OM_DMS2_WALLET_PATH value : ' || g_wallet_path);
    IF g_wallet_path IS NULL THEN
       retcode := 2;
       errbuf  := 'Profile XXWC_OM_DMS2_WALLET_PATH value not defined';
       write_output ('Profile XXWC_OM_DMS2_WALLET_PATH value not defined');
       RETURN;
    END IF;
    
    g_dms2_username            := FND_PROFILE.VALUE ('XXWC_OM_DMS2_USERNAME');
    write_output ('Profile XXWC_OM_DMS2_USERNAME value : ' || g_dms2_username);
    IF g_dms2_username IS NULL THEN
       retcode := 2;
       errbuf  := 'Profile XXWC_OM_DMS2_USERNAME value not defined';
       write_output ('Profile XXWC_OM_DMS2_USERNAME value not defined');
       RETURN;
    END IF;
    
    g_dms2_passwordHash        := FND_PROFILE.VALUE ('XXWC_OM_DMS2_PASSWORDHASH');
    IF g_dms2_passwordHash IS NULL THEN
       retcode := 2;
       errbuf  := 'Profile XXWC_OM_DMS2_PASSWORDHASH value not defined';
       write_output ('Profile XXWC_OM_DMS2_PASSWORDHASH value not defined');
       RETURN;
    END IF;
    
    g_dms2_organizationkey     := FND_PROFILE.VALUE ('XXWC_OM_DMS2_ORGANIZATIONKEY');
    IF g_dms2_organizationkey IS NULL THEN
       retcode := 2;
       errbuf  := 'Profile XXWC_OM_DMS2_ORGANIZATIONKEY value not defined';
       write_output ('Profile XXWC_OM_DMS2_ORGANIZATIONKEY value not defined');
       RETURN;
    END IF;
    
    g_dms2_shipmentsvc_url     := FND_PROFILE.VALUE ('XXWC_OM_DMS2_SHIPMENTSERVICE_URL');
    write_output ('Profile XXWC_OM_DMS2_SHIPMENTSERVICE_URL value : ' || g_dms2_shipmentsvc_url);
    IF g_dms2_shipmentsvc_url IS NULL THEN
       retcode := 2;
       errbuf  := 'Profile XXWC_OM_DMS2_SHIPMENTSERVICE_URL value not defined';
       write_output ('Profile XXWC_OM_DMS2_SHIPMENTSERVICE_URL value not defined');
       RETURN;
    END IF;
    fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
    
    IF p_interface_type = 'SEND_TO_DMS_SO_STD_CLR_DEL' THEN
       l_sec := ' call ShipmentSvc_clear_delivery';
	   fnd_file.put_line(fnd_file.log,'Before call to ShipmentSvc_clear_delivery');
    
       l_xxwc_log_rec.service_name := 'STD_INT_ORDER_SVC_CLEAR_DELIVERY';
       
       BEGIN   
          SELECT ooh.order_number
                ,ott.name   
            INTO l_order_number
                ,l_order_type
            FROM oe_order_headers ooh
                ,oe_transaction_types_tl ott
           WHERE ooh.order_type_id = ott.transaction_type_id 
             AND ooh.header_id     = p_header_id;
       EXCEPTION   
       WHEN OTHERS THEN
          l_order_number := NULL;
          retcode        := 1;
          errbuf         := 'No Data Found for the order please provide a valid order';
          fnd_file.put_line (fnd_file.log, 'No Data Found for the order please provide a valid order');
          RETURN;
       END;
       
       l_xxwc_log_rec.header_id    := p_header_id;
       l_xxwc_log_rec.order_number := l_order_number;
       l_xxwc_log_rec.order_type   := l_order_type;
       l_xxwc_log_rec.delivery_id  := p_delivery_id;
          
       XXWC_OM_DMS2_OB_PKG.ShipmentSvc_clear_delivery( p_order_number      => l_xxwc_log_rec.order_number
                                                           ,p_sch_delivery_date => l_sch_delivery_date
                                                           ,p_delivery_id       => p_delivery_id
                                                           ,p_xxwc_log_rec      => l_xxwc_log_rec );
    ELSIF p_interface_type = 'SEND_TO_DMS_SO_WCD' THEN --'WC Direct'
       l_sec := ' call ShipmentSvc_WCD';
	   fnd_file.put_line(fnd_file.log,'Before call to ShipmentSvc_WCD');
    
       l_xxwc_log_rec.service_name := 'STD_INT_ORDER_WCD';
       
       BEGIN   
          SELECT ooh.order_number
                ,ott.name   
            INTO l_order_number
                ,l_order_type
            FROM oe_order_headers ooh
                ,oe_transaction_types_tl ott
           WHERE ooh.order_type_id = ott.transaction_type_id 
             AND ooh.header_id     = p_header_id;
       EXCEPTION   
       WHEN OTHERS THEN
          l_order_number := NULL;
          retcode        := 1;
          errbuf         := 'No Data Found for the order please provide a valid order';
          fnd_file.put_line (fnd_file.log, 'No Data Found for the order please provide a valid order');
          RETURN;
       END;
       
       l_xxwc_log_rec.header_id    := p_header_id;
       l_xxwc_log_rec.order_number := l_order_number;
       l_xxwc_log_rec.order_type   := l_order_type;
       l_xxwc_log_rec.delivery_id  := p_delivery_id;
          
       XXWC_OM_DMS2_OB_PKG.ShipmentSvc_WCD( p_order_number      => l_xxwc_log_rec.order_number
                                                ,p_sch_delivery_date => l_sch_delivery_date
                                                ,p_delivery_id       => p_delivery_id
                                                ,p_priority        => p_priority --TMS# 20180213-00090
                                                ,p_xxwc_log_rec      => l_xxwc_log_rec );
                                                
    ELSIF p_interface_type = 'SEND_TO_DMS_SO_STD' THEN --'Standard/Internal Orders'
       l_sec := ' call ShipmentSvc_std_int_ord';
	   fnd_file.put_line(fnd_file.log,'Before call to ShipmentSvc_std_int_ord');
	   
       l_order_found := FALSE;
	   
	   l_xxwc_log_rec.service_name := 'STD_INT_ORDER_SVC';
          
       FOR r_order_info IN c_order_delivery_info ( p_header_id     => p_header_id
                                                  ,p_delivery_id   => p_delivery_id
                                                  )
       LOOP 
          l_xxwc_log_rec.order_number           := r_order_info.order_number;
          l_xxwc_log_rec.header_id              := p_header_id;
          l_xxwc_log_rec.order_type             := r_order_info.order_type;
          
          l_order_found                  := TRUE;
          write_output ('Inside r_order_info delivery_id : '|| r_order_info.delivery_id);
          l_sec := 'Inside r_order_info delivery_id : '|| r_order_info.delivery_id;
          
          l_xxwc_log_rec.delivery_id := r_order_info.delivery_id;
          
          ----------------------------------------------------------------------------------
          -- Calling the standard / internal order ShipmentService procedure to call shipment web service
          ----------------------------------------------------------------------------------
       
          XXWC_OM_DMS2_OB_PKG.ShipmentSvc_std_int_ord( p_order_number      => l_xxwc_log_rec.order_number
                                                           ,p_sch_delivery_date => l_sch_delivery_date
                                                           ,p_delivery_id       => r_order_info.delivery_id
                                                           ,p_xxwc_log_rec      => l_xxwc_log_rec
                                                           ,p_request_id        => p_request_id
                                                           ,p_priority        => p_priority --TMS# 20180213-00090
                                                          );
       END LOOP;      
          
       IF l_order_found = FALSE THEN
          l_order_number := NULL;
          retcode        := 1;
          errbuf         := 'No Data Found for the order please provide a valid order';
          fnd_file.put_line (fnd_file.log, 'No Data Found for the order please provide a valid order');
          RETURN;
       END IF;
    
       l_sec := 'After call to ShipmentSvc_std_int_ord';
       fnd_file.put_line(fnd_file.log,'After call to ShipmentSvc_std_int_ord');
    
    ELSIF p_interface_type = 'SEND_TO_DMS_SO_RENTAL' THEN   --'WC SHORT TERM RENTAL', 'WC LONG TERM RENTAL'
       l_sec := 'Before call ShipmentSvc_rental_ord';
       fnd_file.put_line(fnd_file.log,'Before call ShipmentSvc_rental_ord');
       
       l_xxwc_log_rec.service_name := 'RENTAL_ORDER_SVC';
       
       BEGIN   
          SELECT ooh.order_number
                ,ott.name   
            INTO l_order_number
                ,l_order_type
            FROM oe_order_headers ooh
                ,oe_transaction_types_tl ott
           WHERE ooh.order_type_id = ott.transaction_type_id 
             AND ooh.header_id     = p_header_id;
       EXCEPTION   
       WHEN OTHERS THEN
          l_order_number := NULL;
          retcode        := 1;
          errbuf         := 'No Data Found for the order please provide a valid order';
          fnd_file.put_line (fnd_file.log, 'No Data Found for the order please provide a valid order');
          RETURN;
       END;
       
       l_xxwc_log_rec.header_id    := p_header_id;
       l_xxwc_log_rec.order_number := l_order_number;
       l_xxwc_log_rec.order_type   := l_order_type;
          
       ----------------------------------------------------------------------------------
       -- Calling the Rental Order ShipmentService procedure to call shipment web service
       ----------------------------------------------------------------------------------
       
       IF p_shipment_type = 'CustomerDelivery' THEN
          l_line_type_id := 1015;
          
          XXWC_OM_DMS2_OB_PKG.ShipmentSvc_rental_ord( p_order_number      => l_order_number
                                                          ,p_shipment_type     => p_shipment_type
                                                          ,p_line_type_id      => l_line_type_id
                                                          ,p_flow_status_code  => 'AWAITING_SHIPPING'
                                                          ,p_sch_delivery_date => l_sch_delivery_date 
                                                          ,p_rental_return     => 1 -- to include the lookup items
                                                          ,p_request_id        => p_request_id  
                                                          ,p_xxwc_log_rec      => l_xxwc_log_rec
                                                         );
       ELSIF p_shipment_type = 'CustomerPickup' THEN
          l_line_type_id := 1007;
         
          XXWC_OM_DMS2_OB_PKG.ShipmentSvc_rental_ord( p_order_number      => l_order_number
                                                          ,p_shipment_type     => p_shipment_type
                                                          ,p_line_type_id      => l_line_type_id
                                                          ,p_flow_status_code  => 'AWAITING_RETURN' -- to include all flow status code
                                                          ,p_sch_delivery_date => l_sch_delivery_date 
                                                          ,p_rental_return     => 0 -- to exclude the lookup items
                                                          ,p_request_id        => p_request_id  
                                                          ,p_xxwc_log_rec      => l_xxwc_log_rec
                                                         );
       END IF;
    
       l_sec := 'After call to ShipmentSvc_rental_ord';
       fnd_file.put_line(fnd_file.log,'After call to ShipmentSvc_rental_ord');

    ELSIF p_interface_type = 'SEND_TO_DMS_SO_RETURN' THEN   --'WC SHORT TERM RENTAL', 'WC LONG TERM RENTAL'
       fnd_file.put_line(fnd_file.log,'Before call ShipmentSvc_return_ord');
       
       l_xxwc_log_rec.service_name := 'RETURN_ORDER_SVC';
       
       BEGIN   
          SELECT ooh.order_number
                ,ott.name   
            INTO l_order_number
                ,l_order_type
            FROM oe_order_headers ooh
                ,oe_transaction_types_tl ott
           WHERE ooh.order_type_id = ott.transaction_type_id 
             AND ooh.header_id     = p_header_id;
       EXCEPTION   
       WHEN OTHERS THEN
          l_order_number := NULL;
          retcode        := 1;
          errbuf         := 'No Data Found for the order please provide a valid order';
          fnd_file.put_line (fnd_file.log, 'No Data Found for the order please provide a valid order');
          RETURN;
       END;
       
       l_xxwc_log_rec.header_id    := p_header_id;
       l_xxwc_log_rec.order_number := l_order_number;
       l_xxwc_log_rec.order_type   := l_order_type;
       
       ----------------------------------------------------------------------------------
       -- Calling the Return ShipmentService procedure to call shipment web service
       ----------------------------------------------------------------------------------
       IF l_order_number IS NOT NULL THEN
          XXWC_OM_DMS2_OB_PKG.ShipmentSvc_return_ord( p_order_number      => l_order_number
                                                     ,p_shipment_type     => p_shipment_type
                                                     ,p_sch_delivery_date => l_sch_delivery_date 
                                                     ,p_request_id        => p_request_id  
                                                     ,p_xxwc_log_rec      => l_xxwc_log_rec
                                                     );
       END IF;                                                  
    
       fnd_file.put_line(fnd_file.log,'After call to ShipmentSvc_return_ord');
    --Rev#4.0 <Start   
    ELSIF p_interface_type = 'SEND_TO_DMS_SO_INT_PICKUP' THEN   --'Internal order for pickup
       fnd_file.put_line(fnd_file.log,'Before call ShipmentSvc_int_ord_pickup');
       
       l_xxwc_log_rec.service_name := 'INT_ORDER_PICKUP_SVC';
       
       BEGIN   
          SELECT ooh.order_number
                ,ott.name   
            INTO l_order_number
                ,l_order_type
            FROM oe_order_headers ooh
                ,oe_transaction_types_tl ott
           WHERE ooh.order_type_id = ott.transaction_type_id 
             AND ooh.header_id     = p_header_id;
       EXCEPTION   
       WHEN OTHERS THEN
          l_order_number := NULL;
          retcode        := 1;
          errbuf         := 'No Data Found for the order please provide a valid order';
          fnd_file.put_line (fnd_file.log, 'No Data Found for the order please provide a valid order');
          RETURN;
       END;
       
       l_xxwc_log_rec.header_id    := p_header_id;
       l_xxwc_log_rec.order_number := l_order_number;
       l_xxwc_log_rec.order_type   := l_order_type;
       
       ----------------------------------------------------------------------------------
       -- Calling the Return ShipmentService procedure to call shipment web service
       ----------------------------------------------------------------------------------
       IF l_order_number IS NOT NULL THEN
          XXWC_OM_DMS2_OB_PKG.ShipmentSvc_int_ord_pickup(  p_order_number      => l_order_number
                                                          ,p_shipment_type     => p_shipment_type
                                                          ,p_sch_delivery_date => l_sch_delivery_date 
                                                          ,p_request_id        => p_request_id  
                                                          ,p_xxwc_log_rec      => l_xxwc_log_rec
                                                         );
       END IF;                                                  
    
       fnd_file.put_line(fnd_file.log,'After call to ShipmentSvc_int_ord_pickup'); 
    --Rev#4.0 >End
    ELSIF p_interface_type = 'SEND_TO_DMS_PO' THEN --PO
      
       --Rev#4.0 <Start   
       FOR r_promised_date IN c_promised_date ( p_header_id => p_header_id )
       LOOP  --Rev#4.0 >End   
          l_sec := ' call SEND_TO_DMS_PO';
          fnd_file.put_line(fnd_file.log, 'Before SEND_TO_DMS_PO');
          fnd_file.put_line(fnd_file.log, 'r_promised_date.promised_date : '||r_promised_date.promised_date); --Rev#4.0
          l_xxwc_log_rec.service_name := 'SEND_TO_DMS_PO';
          l_xxwc_log_rec.po_header_id := p_header_id;
          l_xxwc_log_rec.order_type := l_order_type;
      
          XXWC_OM_DMS2_OB_PKG.ShipmentSvc_po(p_header_id         => p_header_id,
                                             p_xxwc_log_rec      => l_xxwc_log_rec,
                                             p_promised_date     => r_promised_date.promised_date, --Rev#4.0
                                             p_operationCode     => 'Edit');
          
          DEBUG_LOG (p_xxwc_log_rec => l_xxwc_log_rec );--Rev#4.0
                                             
       END LOOP; --Rev#4.0                                    
                                                 
    ELSIF p_interface_type = 'SEND_TO_DELETE_DMS_PO' THEN --Delete PO
      --Rev#4.0 <Start   
      FOR r_promised_date IN c_promised_date ( p_header_id => p_header_id )
      LOOP  --Rev#4.0 >End   
         l_sec := ' call SEND_TO_DELETE_DMS_PO';
         fnd_file.put_line(fnd_file.log, 'Before SEND_TO_DELETE_DMS_PO');
         l_xxwc_log_rec.service_name := 'SEND_TO_DELETE_DMS_PO';
         l_xxwc_log_rec.header_id := p_header_id;
         l_xxwc_log_rec.order_type := l_order_type;
      
         XXWC_OM_DMS2_OB_PKG.ShipmentSvc_po(p_header_id         => p_header_id,
                                            p_xxwc_log_rec      => l_xxwc_log_rec,
                                            p_promised_date     => r_promised_date.promised_date, --Rev#4.0
                                            p_operationCode     => 'Delete');
         
         DEBUG_LOG (p_xxwc_log_rec => l_xxwc_log_rec );--Rev#4.0
                                            
      END LOOP; --Rev#4.0                                                          
    END IF; --p_interface_type
         
	retcode := g_retcode;
    errbuf  := g_err_msg;
  
    --Rev#4.0 <Start   
    IF p_interface_type NOT IN ( 'SEND_TO_DMS_PO', 'SEND_TO_DELETE_DMS_PO') THEN
       DEBUG_LOG (p_xxwc_log_rec => l_xxwc_log_rec );
    END IF;
    --Rev#4.0 >End          
    
    fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log, 'End of Procedure : MAIN');
    fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
    
    l_sec := 'End of Procedure : MAIN';
    
  EXCEPTION
    WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      p_error_msg := 'Error in the main procedure'||SQLERRM;
    
      retcode := 2;
      errbuf  := p_error_msg;
      write_output ('Error in XXWC_OM_DMS2_OB_PKG.main is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM);
    
      fnd_file.put_line(fnd_file.log, 'Exception Block of Main Procedure');
      fnd_file.put_line(fnd_file.log
                       ,'The Error Code Traced is : ' || l_error_code);
      fnd_file.put_line(fnd_file.log
                       ,'The Error Message Traced is : ' ||
                        p_error_msg);
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.main',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  						
  END main;
  
  /**FUNCTION GET_DMS_PO(p_header_id IN NUMBER) RETURN number AS
  v_cancel_flag        varchar2(1);
  v_cancel_flag1       varchar2(1);
  l_revision_num       number := null;
  l_revision_num1      number := null;
  l_rownum             number;
  l_rownum1            number;
  v_ship_via_code      varchar2(25);
  v_ship_via_code1     varchar2(25);
  v_freight_lookup     po_headers_all.freight_terms_lookup_code%type;
  v_freight_lookup1    po_headers_all.freight_terms_lookup_code%type;
  v_vendor_contact_id  po_headers_all.vendor_contact_id%type;
  v_vendor_contact_id1 po_headers_all.vendor_contact_id%type;
  v_quantity           po_lines_all.quantity%type;
  v_quantity1          po_lines_all.quantity%type;
  v_promised_date      po_line_locations_all.promised_date%type;
  v_promised_date1     po_line_locations_all.promised_date%type;

BEGIN
  SELECT poh.vendor_contact_id,
         poh.freight_terms_lookup_code,
         poh.ship_via_lookup_code,
         poh.revision_num
    into v_vendor_contact_id,
         v_freight_lookup,
         v_ship_via_code,
         l_revision_num
    from po_headers_all poh
   where poh.po_header_id = p_header_id;
  select poha.vendor_contact_id,
         poha.freight_terms_lookup_code,
         poha.ship_via_lookup_code
    into v_vendor_contact_id1, v_freight_lookup1, v_ship_via_code1
    from po_headers_archive_all poha
   where poha.po_header_id = p_header_id
     and poha.revision_num in
         (select max(poha.revision_num) - 1
            from po.po_headers_archive_all poha
           where poha.po_header_id = p_header_id);
  for l in (select distinct polaa.po_line_id, polaa.po_header_id
              from po_lines_archive_all polaa
             where polaa.po_header_id = p_header_id
            /* order  by polaa.revision_num desc*/
         /**   ) loop
  
    begin
    
      SELECT a.Quantity,
             d.promised_date,
             nvl(a.cancel_flag, 'N') cancel_flag
        into v_quantity, v_promised_date, v_cancel_flag
        FROM po_lines_all A
       inner join po_line_locations_all d
          on a.po_header_id = d.po_header_id
         and d.po_line_id = a.po_line_id
       where a.po_header_id = l.po_header_id
         and a.po_line_id = l.po_line_id
         and rownum = 1;
      select revision_num
        into l_revision_num1
        from (select rownum row_num, REVISION_NUM
                from apps.po_lines_archive_all
               where po_header_id = l.po_header_id
                 and po_line_id = l.po_line_id
               order by revision_num desc)
       where row_num = 2;
      SELECT a.Quantity,
             d.promised_date,
             nvl(a.cancel_flag, 'N') cancel_flag
        into v_quantity1, v_promised_date1, v_cancel_flag1
        FROM po_lines_archive_all A
       inner join PO_LINE_LOCATIONS_ARCHIVE_ALL d
          on a.po_header_id = d.po_header_id
         and d.po_line_id = a.po_line_id
         and a.revision_num = d.revision_num
       where a.po_header_id = l.po_header_id
         and a.po_line_id = l.po_line_id
         and a.revision_num = l_revision_num1;
    exception
      when others then
        dbms_output.put_line('exception: ' || substr(SQLERRM, 1, 250));
    end;
    if v_ship_via_code != v_ship_via_code1 or
       v_vendor_contact_id != v_vendor_contact_id1 or
       v_freight_lookup != v_freight_lookup1 then
      return 1;
    end if;
    if l_revision_num != l_revision_num1 then
      if (v_quantity != v_quantity1 or v_promised_date != v_promised_date1) and
         v_cancel_flag = v_cancel_flag1 then
        return 1;
      end if;
      if v_cancel_flag != v_cancel_flag1 then
        if v_cancel_flag = 'Y' then
          return 1;
        end if;
      end if;
    end if;
  end loop;
  -- return 0;
  -- end if;
EXCEPTION
  WHEN OTHERS THEN
    RETURN 0;
end;
  **/
  /********************************************************************************
  ProcedureName : Send_to_DMS2_PO
  Purpose       : This will be called when PO is approved
  ----------------------------------------------------------------------------------*/
  PROCEDURE Send_to_DMS2_PO(p_header_id IN NUMBER) AS
  
    l_error_code NUMBER;
    p_error_msg  VARCHAR2(2000);
    l_sec        VARCHAR2(2000);--#Rev2.0
    l_Shipment_Type             VARCHAR2(150);
    l_request_id                NUMBER := 0;
    l_line_count                NUMBER := 0;
    l_rev_count                 NUMBER := 0;
    l_order_type_code           po_headers_all.type_lookup_code%type;
    l_cancel_flag               po_headers_archive_all.cancel_flag%type;
    v_ship_via_code1            varchar2(25);
    v_freight_lookup1           po_headers_all.freight_terms_lookup_code%type;
    l_ship_via_lookup_code      varchar2(25);
    l_freight_terms_lookup_code po_headers_all.freight_terms_lookup_code%type;
    l_cancel_flag1              po_headers_all.cancel_flag%type;
  
  BEGIN
     IF FND_PROFILE.VALUE ('XXWC_OM_DMS2_DEBUG_FLAG') = 'Y' THEN
       XXWC_OM_DMS2_OB_PKG.debug_on;
     END IF;
     
     fnd_global.apps_initialize(fnd_global.user_id,
                                fnd_global.resp_id,
                                fnd_global.resp_appl_id);
  
     l_sec := 'Start of Procedure : Send_to_DMS2_PO';
 
     BEGIN
        SELECT ooh.type_lookup_code
          INTO l_order_type_code
          FROM po_headers_all ooh
         WHERE 1 = 1
           AND ooh.po_header_id = p_header_id;
     EXCEPTION
     WHEN OTHERS THEN
        l_order_type_code := null;
     END;

     l_sec := 'before checking PO Type'; --#Rev2.0
     -- check if it is standard order then look if other conditions are met to move further
     IF l_order_type_code = 'STANDARD' THEN
       SELECT COUNT(1)
         INTO l_line_count
         FROM po.PO_HEADERS_ALL t
        WHERE /**t.freight_terms_lookup_code = 'WILL CALL'
          AND t.ship_via_lookup_code in
               ('OUR TRUCK',
                'Our Truck',
                'WCD',
                'BRANCH_LOOP_TRUCK',
                'DC_LOOP_TRUCK')**/--Rev#3.0    
          EXISTS ( SELECT 1
                     FROM apps.fnd_lookup_values flv
                     WHERE 1=1
                       AND flv.lookup_type    = 'XXWC_DMS2_PUR_SHIP_METHOD'
                       AND flv.enabled_flag   = 'Y'
                       AND UPPER(flv.meaning) = UPPER(t.ship_via_lookup_code)
                       AND UPPER(flv.tag)     = UPPER(t.freight_terms_lookup_code)
                       AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
                  ) --Rev#3.0     
          AND t.authorization_status = 'APPROVED'
          AND t.po_header_id = p_header_id
          AND NVL(t.cancel_flag, 'N') != 'Y';

      l_sec := 'before checking l_line_count condition'; --#Rev2.0
      --- if all first conditions are met then check if po has revisions in archive table
      IF l_line_count > 0 THEN
        BEGIN
          select count(*)
            into l_rev_count
            from po_headers_archive_all a
           where a.po_header_id = p_header_id;
        END;
        l_sec := 'After checking l_rev_count'||l_rev_count; --#Rev2.0
        --- if no revisions then call concurrent programs to generate xml
        IF l_rev_count = 1 THEN
          l_sec := 'Before submiting concurrent prog with SEND_TO_DMS_PO'; --#Rev2.0
          l_request_id := fnd_request.submit_request(application => 'XXWC',
                                                     program     => 'XXWC_OM_DMS2_OB',
                                                     description => NULL,
                                                     start_time  => SYSDATE,
                                                     sub_request => FALSE,
                                                     argument1   => p_header_id,--Sales Order / PO Header Id
                                                     argument2   => null,--Sch delivery date(DD-MON-YYYY)
                                                     argument3   => null,--Delivery id
                                                     argument4   => 'CustomerPickup',--Shipment Type
                                                     argument5   => 'SEND_TO_DMS_PO', --Interface Type
                                                     argument6   => FND_PROFILE.VALUE('XXWC_OM_DMS2_DEBUG_FLAG'),--Debug
                                                     argument7   => NULL, --Request id
                                                     argument8   => 'N'--TMS# 20180213-00090
                                                     ); 
          COMMIT;
        END IF;
        
        -- if revisions exists that means changes were made in PO and then send for reapproval
        -- now pick the latest revision and check if any change in certain field to the actual one
        -- if yes then trigger concurrent program. 
        IF l_rev_count > 1 THEN
          BEGIN
            SELECT NVL(a.cancel_flag, 'N')
              INTO l_cancel_flag
              FROM po_headers_archive_all a
             WHERE po_header_id = p_header_id
               AND revision_num IN
                   (SELECT MAX(revision_num)
                      FROM po_headers_archive_all a
                     WHERE po_header_id = p_header_id);
          EXCEPTION --#Rev2.0
          WHEN OTHERS THEN --#Rev2.0
             l_cancel_flag := 'N'; --#Rev2.0
          END;

          IF l_cancel_flag = 'Y' THEN
            l_sec := 'Before submiting concurrent prog with SEND_TO_DELETE_DMS_PO'; --#Rev2.0
            l_request_id := fnd_request.submit_request(application => 'XXWC',
                                                       program     => 'XXWC_OM_DMS2_OB',
                                                       description => NULL,
                                                       start_time  => SYSDATE,
                                                       sub_request => FALSE,
                                                       argument1   => p_header_id,--Sales Order / PO Header Id
                                                       argument2   => null,--Sch delivery date(DD-MON-YYYY)
                                                       argument3   => null,--Delivery id
                                                       argument4   => 'CustomerPickup',--Shipment Type
                                                       argument5   => 'SEND_TO_DELETE_DMS_PO',--Interface Type
                                                       argument6   => FND_PROFILE.VALUE('XXWC_OM_DMS2_DEBUG_FLAG'),--Debug
                                                       argument7   => NULL, --Request id
                                                       argument8   => 'N'--TMS# 20180213-00090
                                                       );
            COMMIT;
          ELSE --IF GET_DMS_PO(p_header_id) = 1 THEN
            l_sec := 'Before submiting concurrent prog with SEND_TO_DMS_PO'; --#Rev2.0
            l_request_id := fnd_request.submit_request(application => 'XXWC',
                                                       program     => 'XXWC_OM_DMS2_OB',
                                                       description => NULL,
                                                       start_time  => SYSDATE,
                                                       sub_request => FALSE,
                                                       argument1   => p_header_id,--Sales Order / PO Header Id
                                                       argument2   => null,--Sch delivery date(DD-MON-YYYY)
                                                       argument3   => null,--Delivery id
                                                       argument4   => 'CustomerPickup',--Shipment Type
                                                       argument5   => 'SEND_TO_DMS_PO',--Interface Type
                                                       argument6   => FND_PROFILE.VALUE('XXWC_OM_DMS2_DEBUG_FLAG'),--Debug
                                                       argument7   => NULL, --Request id
                                                       argument8   => 'N'--TMS# 20180213-00090
                                                       );
            COMMIT;
          /** ELSIF GET_DMS_PO(p_header_id) = 2 THEN
              l_request_id := fnd_request.submit_request(application => 'XXWC',
                                                       program     => 'XXWC_OM_DMS2_OB',
                                                       description => NULL,
                                                       start_time  => SYSDATE,
                                                       sub_request => FALSE,
                                                       argument1   => p_header_id,--Sales Order / PO Header Id
                                                       argument2   => null,--Sch delivery date(DD-MON-YYYY)
                                                       argument3   => null,--Delivery id
                                                       argument4   => 'CustomerPickup',--Shipment Type
                                                       argument5   => 'SEND_TO_DELETE_DMS_PO',--Interface Type
                                                       argument6   => FND_PROFILE.VALUE('XXWC_OM_DMS2_DEBUG_FLAG'),--Debug
                                                       argument7   => NULL --Request id
                                                       );
              COMMIT;**/
          END IF;
        END IF;
      ELSE
        BEGIN
          SELECT t.freight_terms_lookup_code, t.ship_via_lookup_code, t.cancel_flag
            INTO l_freight_terms_lookup_code, l_ship_via_lookup_code, l_cancel_flag1
            FROM po.PO_HEADERS_ALL t
           WHERE t.authorization_status = 'APPROVED'
             AND t.po_header_id = p_header_id;
             
          SELECT poha.freight_terms_lookup_code, poha.ship_via_lookup_code
            INTO v_freight_lookup1, v_ship_via_code1
            FROM po_headers_archive_all poha
           WHERE poha.po_header_id = p_header_id
             AND poha.revision_num IN
                 (SELECT MAX(poha.revision_num)-1
                    FROM po.po_headers_archive_all poha
                   WHERE poha.po_header_id = p_header_id);
         EXCEPTION --#Rev2.0
         WHEN OTHERS THEN --#Rev2.0
           v_freight_lookup1 := null; --#Rev2.0
           v_ship_via_code1 := null; --#Rev2.0
        END;
        IF l_cancel_flag1 = 'Y' THEN 
          l_sec := 'Before submiting concurrent prog with SEND_TO_DELETE_DMS_PO l_cancel_flag1'||l_cancel_flag1; --#Rev2.0
          l_request_id := fnd_request.submit_request(application => 'XXWC',
                                                     program     => 'XXWC_OM_DMS2_OB',
                                                     description => NULL,
                                                     start_time  => SYSDATE,
                                                     sub_request => FALSE,
                                                     argument1   => p_header_id,--Sales Order / PO Header Id
                                                     argument2   => null,--Sch delivery date(DD-MON-YYYY)
                                                     argument3   => null,--Delivery id
                                                     argument4   => 'CustomerPickup',--Shipment Type
                                                     argument5   => 'SEND_TO_DELETE_DMS_PO',--Interface Type
                                                     argument6   => FND_PROFILE.VALUE('XXWC_OM_DMS2_DEBUG_FLAG'),--Debug
                                                     argument7   => NULL, --Request id
                                                     argument8   => 'N'--TMS# 20180213-00090
                                                     );
          COMMIT;
        END IF;
         
        IF (v_freight_lookup1 = 'WILL CALL' AND
           l_freight_terms_lookup_code != v_freight_lookup1) OR
           (v_ship_via_code1 in ('OUR TRUCK',
                                 'Our Truck',
                                 'WCD',
                                 'BRANCH_LOOP_TRUCK',
                                 'DC_LOOP_TRUCK') AND
           l_ship_via_lookup_code != v_ship_via_code1) THEN
           
           l_sec := 'Before submiting concurrent prog with SEND_TO_DELETE_DMS_PO l_freight_terms_lookup_code'||l_freight_terms_lookup_code; --#Rev2.0
           l_request_id := fnd_request.submit_request(application => 'XXWC',
                                                     program     => 'XXWC_OM_DMS2_OB',
                                                     description => NULL,
                                                     start_time  => SYSDATE,
                                                     sub_request => FALSE,
                                                     argument1   => p_header_id,
                                                     argument2   => null,--Sch delivery date(DD-MON-YYYY)
                                                     argument3   => null,--Delivery id
                                                     argument4   => 'CustomerPickup',--Shipment Type
                                                     argument5   => 'SEND_TO_DELETE_DMS_PO',--Interface Type
                                                     argument6   => FND_PROFILE.VALUE('XXWC_OM_DMS2_DEBUG_FLAG'),
                                                     argument7   => NULL, --Request id
                                                     argument8   => 'N'--TMS# 20180213-00090
                                                     );
           COMMIT;
        END IF;
      END IF;
    END IF; 
    
    COMMIT;
  
    l_sec := 'End of Procedure : Send_to_DMS2';
  
  EXCEPTION
    WHEN OTHERS THEN
      l_error_code := SQLCODE;
      p_error_msg  := 'Error in the Send_to_DMS2 procedure' || SQLERRM;
    
      write_output('Error in XXWC_OM_DMS2_OB_PKG.Send_to_DMS2 is SQLERROR CODE' ||
                   l_error_code || 'SQLERRM : ' || SQLERRM);
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_package ||
                                                                  '.Send_to_DMS2',
                                           p_calling           => l_sec,
                                           p_request_id        => fnd_global.conc_request_id,
                                           p_ora_error_msg     => SUBSTR(' Error_Stack...' ||
                                                                         DBMS_UTILITY.format_error_stack() ||
                                                                         ' Error_Backtrace...' ||
                                                                         DBMS_UTILITY.format_error_backtrace(),
                                                                         1,
                                                                         2000),
                                           p_error_desc        => SUBSTR(p_error_msg,
                                                                         1,
                                                                         240),
                                           p_distribution_list => g_dflt_email,
                                           p_module            => g_module);
   END Send_to_DMS2_PO;
  
   PROCEDURE raise_po_Send_To_DMS2 (p_po_header_id  IN NUMBER,
                                    p_error_msg  OUT VARCHAR2)
   IS PRAGMA AUTONOMOUS_TRANSACTION;

      l_error_msg               VARCHAR2 (4000);
      l_parameter_list          wf_parameter_list_t;
      l_event_data              CLOB;
      l_sec                     VARCHAR2 (100);
      l_ship_location           NUMBER:=0; 

   BEGIN
      l_sec := 'Set values for Parameter List';
      
      mo_global.set_policy_context('S',162);
      
      SELECT count(1)
        INTO l_ship_location
        FROM apps.fnd_lookup_values flv,
             po_headers_v     ph
       WHERE 1=1
         AND flv.lookup_type  = 'XXWC_DMS2_BRANCHES_LOOKUP'
         AND flv.enabled_flag = 'Y'
         AND flv.lookup_code  = SUBSTR(ph.SHIP_TO_LOCATION, 1, INSTR(ph.SHIP_TO_LOCATION, ' ') - 1) 
         AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1)
         AND ph.po_header_id = p_po_header_id;
      
      IF l_ship_location >0 THEN   
         l_parameter_list :=
                  wf_parameter_list_t (
                  wf_parameter_t ('SEND_DATE',      fnd_date.date_to_canonical (SYSDATE)),
                  wf_parameter_t ('P_PO_HEADER_ID', p_po_header_id)
                            );
 
         l_sec := 'Raise business Event';

         wf_event.raise (
            p_event_name   => 'xxwc.oracle.apps.po.sendtodms2',
            p_event_key    => SYS_GUID (),
            p_event_data   => l_event_data,
            p_parameters   => l_parameter_list);
         COMMIT;
      END IF;   

   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_DMS2_OB_PKG.raise_po_send_to_dms2',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'PO');

        p_error_msg := 'Failed for raise_po_send_to_dms2 '||SUBSTR (SQLERRM, 1, 240);
   END raise_po_send_to_dms2;

   FUNCTION PO_Send_To_DMS2 (p_subscription_guid   IN     RAW,
                             p_event               IN OUT wf_event_t)
      RETURN VARCHAR2
   IS
      l_plist         wf_parameter_list_t := p_event.getparameterlist ();
      l_error_msg     VARCHAR2 (500);
      l_po_header_id  po_headers_archive_all.po_header_id%TYPE;
      l_sec           VARCHAR2 (100);
   BEGIN
      l_sec := 'Initialize values';

      l_po_header_id := wf_event.getvalueforparameter ('P_PO_HEADER_ID', l_plist);

      XXWC_OM_DMS2_OB_PKG.Send_to_DMS2_PO(p_header_id  => l_po_header_id);  

      COMMIT;

      RETURN ('SUCCESS');
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg := SUBSTR (SQLERRM, 1, 240);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_DMS2_OB_PKG.PO_Send_To_DMS2',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'PO');

   END PO_Send_To_DMS2;
  
END XXWC_OM_DMS2_OB_PKG;
/