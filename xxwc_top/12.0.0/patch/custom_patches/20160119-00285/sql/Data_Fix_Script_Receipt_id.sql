/* Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        27-Jan-2015  Neha Saini       TMS#20160119-00285   IT - update Status on Receipt
                                            Initial Version   */

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;
BEGIN
UPDATE xxwc.xxwcar_cash_rcpts_tbl
SET    status    = 'PROCESSED'
WHERE  receipt_id = 5677506;
--1 row expected to be updated
COMMIT;
EXCEPTION
  WHEN others THEN
    dbms_output.put_line('Error occured while updating STATUS: '||SQLERRM);
END;
/