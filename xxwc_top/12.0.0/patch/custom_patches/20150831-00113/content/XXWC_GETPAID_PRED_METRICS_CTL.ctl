--* *************************************************************************************************************************
--*File Name: XXWC_GETPAID_PRED_METRICS_CTL.ctl           																	*
--*Author: Gopi Damuluri                                  																	*
--*Creation Date: 11-Nov-2013                             																	*
--*Last Updated Date: 11-Nov-2013                         																	*
--*Description: This is the control file. SQL Loader will 																	*
--*   use to load the GetPaid Predictive Metrics data                                                                       *
--*  Ver          Date            Author                                 Description     								    *
--*   ---------   ----------     -------------------------------         ----------------									*
--*    1.0        11-Nov-2013   Gopi Damuluri                         Initial Version                                       *					
-- *   2.0        14-Sep-2015   Hari Prasad M                         TMS# 20150831-00113  Added for default org id.        *
--***************************************************************************************************************************


LOAD DATA
REPLACE INTO TABLE XXWC.XXWC_GETPAID_PRED_METRICS_TBL
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(CUSTOMER_NUMBER        "TRIM(:CUSTOMER_NUMBER)"
, RAW_SCORE             "TRIM(:RAW_SCORE)"
, CREATED_BY            CONSTANT "-1"
, CREATION_DATE         SYSDATE
, LAST_UPDATED_BY       CONSTANT "-1"
, LAST_UPDATE_DATE      SYSDATE
, STATUS                CONSTANT "N"
, ORG_ID                CONSTANT "162"  ---Added By Hari for TMS# 20150831-00113
)
