
--***************************************************************************************************************************
--*File Name: XXWC_UPDATE_CUST_PROFILE_TBL.ctl                                         										*
--*Author: Gopi Damuluri                                                               										*
--*Creation Date: 08-Oct-2012                                                          										*
--*Last Updated Date: 08-Oct-2012                                                      										* 
--*Data Source: PRISM                                                                  										*
--*Description: This is the control file. SQL Loader will                              										*
--*             use to load the CustProfile Staging data                               										*
--*    Ver        Date          Author                                 Description     										*
--*   ---------  ----------     -------------------------------       ----------------										*
--*    1.0        08-Oct-2012   Gopi Damuluri                         Initial Version                                       *					
-- *   2.0        14-Sep-2015   Hari Prasad M                         TMS# 20150831-00113  Added for default org id.        *
--***************************************************************************************************************************

LOAD DATA
REPLACE INTO TABLE XXWC.XXWC_UPDATE_CUST_PROFILE_TBL
FIELDS TERMINATED BY '|'
TRAILING NULLCOLS
( CUST_ACCOUNT_PROFILE_ID          "TRIM(:CUST_ACCOUNT_PROFILE_ID)"
, COLLECTOR_ID                     "TRIM(:COLLECTOR_ID)"
, CREDIT_ANALYST_ID                "TRIM(:CREDIT_ANALYST_ID)"
, STATUS                           CONSTANT "N"
, ORG_ID                           CONSTANT "162"  ---Added By Hari for TMS# 20150831-00113 
 )
 