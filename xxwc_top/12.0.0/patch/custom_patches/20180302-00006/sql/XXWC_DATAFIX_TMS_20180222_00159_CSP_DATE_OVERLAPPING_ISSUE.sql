CREATE OR REPLACE FUNCTION xxwc_csp_overlap_dates_check (P_agreement_id in number, 
                                                  P_start_date in date, 
												  P_end_date in date, 
												  P_product_attribute in varchar2, 
												  P_product_value in varchar2, 
												  P_rev_num in number)
RETURN VARCHAR2												  
IS
l_count number;
BEGIN
  select nvl(count(1),0)
    into l_count
    from xxwc_om_contract_pricing_lines xocp
   where agreement_id = P_agreement_id
     and product_attribute=P_product_attribute
     and product_value=P_product_value
     and revision_number<>nvl(P_rev_num,-1)
     and (        (P_start_date >= START_DATE and P_start_date <= NVL(END_DATE,P_end_date))
               or (P_end_date   >= START_DATE and P_end_date   <= NVL(END_DATE,P_end_date))
               or (P_start_date <= start_DATE and P_end_date   >= NVL(end_DATE, P_end_date+1))
               or (P_start_date between start_date and end_date)
               or (P_end_date between start_date and end_date)
               or (P_start_date >= start_date and end_date is null and P_end_date is null)
               or (P_start_date <= start_date and P_end_date is null)
               or (P_end_date<= end_date and P_start_date is null and start_date is null)
               or (P_end_date is null and end_date is null and P_start_date is null and start_date is null)
               or (end_date is null and start_date is null)
               or (P_end_date is null and P_start_date is null) 
               or (P_start_date is null and P_end_date >=start_date)
               ); 
  
  IF l_count > 0 THEN
    RETURN 'Y';
  ELSE
     RETURN 'N';
  END IF;	
END xxwc_csp_overlap_dates_check;
/
CREATE OR REPLACE PROCEDURE xxwc_update_modifier_lines (p_agreement_id IN NUMBER,
                                    p_agreement_line_id NUMBER,
									p_error_msg OUT VARCHAR2)
  IS 
    l_control_rec QP_GLOBALS.Control_Rec_Type;
    l_return_status VARCHAR2(1);
    x_msg_count number;
    x_msg_data Varchar2(2000);
    x_msg_index number;
    l_index       NUMBER :=1; --DEBUG
    l_MODIFIER_LIST_rec QP_Modifiers_PUB.Modifier_List_Rec_Type;
    l_MODIFIER_LIST_val_rec QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
    l_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
    l_MODIFIERS_val_tbl QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
    l_QUALIFIERS_tbl QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
    l_QUALIFIERS_val_tbl QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
    l_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
    l_PRICING_ATTR_val_tbl QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
    
    l_x_MODIFIER_LIST_rec QP_Modifiers_PUB.Modifier_List_Rec_Type;
    l_x_MODIFIER_LIST_val_rec QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
    l_x_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
    l_x_MODIFIERS_val_tbl QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
    l_x_QUALIFIERS_tbl QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
    l_x_QUALIFIERS_val_tbl QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
    l_x_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
    l_x_PRICING_ATTR_val_tbl QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
    
    --CSP Header Level Variables
    v_agreement_id NUMBER := p_agreement_id;
    v_agreement_type VARCHAR2(30) ;
    v_vendor_quote_number NUMBER ;
    v_price_type VARCHAR2(30);
    v_customer_id NUMBER ;
    v_customer_site_id NUMBER ;
    v_agreement_status VARCHAR2(30) ;
    v_revision_number NUMBER ;
    v_incompatability_group VARCHAR2(30);
    v_incomp_group VARCHAR2(30);
    v_attribute_10 VARCHAR2(25) :='Contract Pricing'; --Variable to populate DFF Attribute 10 on Modifier Header (Contract Pricing or Vendor Quote)
    v_attribute_11 Number := NULL ;
    v_attribute_12 VARCHAR2(25) := NULL ; --Vendor Name for VQN
    v_attribute_13 VARCHAR2(25) := NULL ; --VEndor Number for VQN
    v_attribute_14 Number ; --Mapped to Vendor Agreement Number DFF on Modifier Header
    v_attribute_15 Number ; --Mapped to Agreement Revision Number DFF on Modifier Header
    v1_price_type VARCHAR2(30);
    v_customer_name VARCHAR2(50) ;
    v_party_id Number ;
    v_party_name VARCHAR2(360) ;
    v_party_site_number Number ;
    v_qualifier_value Number ;
    v_agreeid Number ;
    v_qual_context VARCHAR2(360);
    v_excluder_flag VARCHAR2(1) ;
    v_operator_code VARCHAR(360) ;
    v_delimitier VARCHAR2(1) ;
    v_qualifier_grouping_no Number ;
    v_qualifier_precedence Number ; 
    v_operation VARCHAR2(30) ; 
    v_active_flag VARCHAR2(30) ;
    v_list_header_id NUMBER ; --v_list_header_id
    v_list_line_id NUMBER ; 
    --START XXWC_CSP_LINES VARIABLES
    v_atrribute_3 NUMBER ;
    v_ln_special_cost NUMBER ;
    v_ln_id NUMBER ; --Line Level Variable for xxwc.agreement_line_id
    v_ln_agreement_type VARCHAR2(30) ; --Line Level Variable for xxwc.agreement_type
    v_ln_agree_type VARCHAR2(30) ; --Set CSP or VQN Agreement Type Variable passed to API
    v_ln_vendor_quote_number VARCHAR2(30) ;  --Line Level Variable for XXWC.VENDOR_QUOTE_NUMBER
    v_ln_start_date DATE ; --Line Level Variable for XXWC.START_DATE
    v_ln_end_date DATE;  --Line Level Variable for XXWC.END_DATE
    v_ln_mod_type  VARCHAR2(30) ; --Line Level Variable for Modifier Type
    v_arithmetic_operator VARCHAR2(30) ; --Line Level Variable for Modifier Type API Value
    v_ln_mod_value NUMBER ; --Line Level Variable for Modifier Value
    v_ln_mod_val NUMBER ; --Line Level Modifer Value passed to API
    v_ln_vendor_id NUMBER ;--Line LEvel Variable for Vendor ID
    v_product_attribute VARCHAR2(50) ;  --Line Level Variable for Product Attribute (Item/Cat Class)
    v_product_value VARCHAR2(50) ;--Line Level Variable for Product Value (SKU Number)
    v_product_attribute_api VARCHAR2(50) ; --Line Level Variable to set in API
    v_product_attribute_context VARCHAR2(30) := 'ITEM' ; --Variable to set product_attribute_context in Pricing Attr API
    v_product_attr_value VARCHAR2(50) ;
    v_generate_using_formula_id NUMBER ;
    V9 NUMBER ; 
    i NUMBER := 1;
    j NUMBER ;
    L_MSG VARCHAR2(2000) ; 
    l_formula_id NUMBER ; 
	e_biz_error EXCEPTION;
    l_user_id NUMBER := 0; --sysadmin
    l_resp_id NUMBER := 50891; --HDS Pricing Manager - WC
    l_resp_appl_id NUMBER  := 661; --Advanced Pricing 	
    /*CURSOR LINEIDUPDATE 
    IS select agreement_line_id 
    from xxwc.xxwc_om_contract_pricing_lines 
    where agreement_id = v_agreement_id 
    and revision_number != 0
    and interfaced_flag = 'N' ;*/
        
    Begin
	   --dbms_output.put_line('Start xxwc_update_modifier_lines'); 
       p_error_msg := NULL;	   
	   mo_global.SET_POLICY_CONTEXT('S',162);
	   fnd_global.APPS_INITIALIZE(l_user_id,l_resp_id,l_resp_appl_id);
      
      /*  Initial select to set line level variables */    
        BEGIN
          SELECT 
          start_date,
          end_date
          INTO
          v_ln_start_date, --Line Level Variable for start date
          v_ln_end_date --Line Level Variable for end date
          FROM xxwc.xxwc_om_contract_pricing_lines
          WHERE agreement_id = p_agreement_id
		  AND agreement_line_id = p_agreement_line_id; --c3_rec.agreement_line_id ;           
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('Initial select failed, check modifier line' || SQLCODE || SQLERRM,1,240) ;
			  RAISE e_biz_error;
          END; 
      /*  Initial select to set line level variables */            
        BEGIN
          select list_line_id
          into v_list_line_id
          from qp_list_lines
          where attribute2 = p_agreement_line_id;--c3_rec.agreement_line_id ;
		  --FND_FILE.Put_Line(FND_FILE.Log,'v_list_line_id = '||v_list_line_id);
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('No modifier list header id exists' || SQLCODE || SQLERRM,1,240) ;
			  RAISE e_biz_error;
          END; 
          
          /*  Populate API Variables for Modifier Line Level Updates */
            l_MODIFIERS_tbl(i).list_line_id := v_list_line_id ;
            l_MODIFIERS_tbl(i).start_date_active := v_ln_start_date ;
            l_MODIFIERS_tbl(i).end_date_active := v_ln_end_date ;
            l_MODIFIERS_tbl(i).operation := QP_GLOBALS.G_OPR_UPDATE; 
          
        --dbms_output.put_line('CALL QP_Modifiers_PUB.Process_Modifiers');
      /*  API Call */
        QP_Modifiers_PUB.Process_Modifiers
        ( p_api_version_number => 1.0
        , p_init_msg_list => FND_API.G_FALSE
        , p_return_values => FND_API.G_FALSE
        , p_commit => FND_API.G_FALSE
        , x_return_status => l_return_status
        , x_msg_count =>x_msg_count
        , x_msg_data =>x_msg_data
        ,p_MODIFIER_LIST_rec => l_MODIFIER_LIST_rec
        ,p_MODIFIERS_tbl => l_MODIFIERS_tbl
        ,p_QUALIFIERS_tbl => l_QUALIFIERS_tbl
        ,p_PRICING_ATTR_tbl => l_PRICING_ATTR_tbl
        ,x_MODIFIER_LIST_rec => l_x_MODIFIER_LIST_rec         
        ,x_MODIFIER_LIST_val_rec => l_x_MODIFIER_LIST_val_rec 
        ,x_MODIFIERS_tbl => l_x_MODIFIERS_tbl                 
        ,x_MODIFIERS_val_tbl => l_x_MODIFIERS_val_tbl         
        ,x_QUALIFIERS_tbl => l_x_QUALIFIERS_tbl               
        ,x_QUALIFIERS_val_tbl => l_x_QUALIFIERS_val_tbl       
        ,x_PRICING_ATTR_tbl => l_x_PRICING_ATTR_tbl           
        ,x_PRICING_ATTR_val_tbl => l_x_PRICING_ATTR_val_tbl   
        );
		--dbms_output.put_line('l_return_status:'||l_return_status);
		 IF l_return_status != 'S'
         THEN
            IF (x_msg_count > 0)
            THEN
               FOR l_lcv IN 1 .. x_msg_count
               LOOP
                  x_msg_data := oe_msg_pub.get(p_msg_index => l_index, p_encoded => 'F');
				  --dbms_output.put_line('Error:'||x_msg_data);
				  l_index := l_index+1;
				  p_error_msg := SUBSTR(p_error_msg||' '||l_index||')'||x_msg_data,1,2000);
               END LOOP;
            END IF;
         END IF;
    --dbms_output.put_line('End CALL QP_Modifiers_PUB.Process_Modifiers ');
  /*Delete all line level exclusions associated with CSP Agreement */ 
    BEGIN
       select list_header_id
       into v_list_header_id
       from qp_list_headers
       where attribute14 = v_agreement_id ; 
	EXCEPTION
	   WHEN OTHERS THEN
	      v_list_header_id := NULL;
	END;
    --dbms_output.put_line('v_list_header_id: '||v_list_header_id);
	IF v_list_header_id IS NOT NULL THEN
       delete from
       qp_pricing_attributes
       where list_header_id = v_list_header_id
       and excluder_flag = 'Y' ; 
	END IF;
    --dbms_output.put_line('End xxwc_update_modifier_lines');
  EXCEPTION
    WHEN e_biz_error THEN
       --dbms_output.put_line(L_MSG);
	   p_error_msg := L_MSG;
	WHEN OTHERS THEN
	   --dbms_output.put_line(SQLERRM);
	   p_error_msg := SUBSTR(SQLERRM,1,2000);
  END xxwc_update_modifier_lines;
 /
 
 CREATE TABLE XXWC.XXWC_OM_CSP_LINE_TAB_BKP AS
(SELECT rowid line_rowid,xocp.* 
      FROM xxwc_om_contract_pricing_lines xocp
      WHERE 1=1
      AND xxwc_csp_overlap_dates_check(xocp.agreement_id,
                                      xocp.start_date,
                                      xocp.end_date,
                                      xocp.product_attribute,
                                      xocp.product_value,
                                      xocp.revision_number) = 'Y');
							  
set serveroutput on SIZE 1000000;
DECLARE
   l_loop_exit_flag VARCHAR2(1);
   l_sec varchar2(100);
   l_error_msg varchar2(2000);
   CURSOR cr_main_data
   IS
      SELECT rowid main_rowid,xocp.agreement_id,xocp.agreement_line_id,xocp.revision_number,xocp.product_value,
	         TRUNC(xocp.START_DATE) START_DATE,TRUNC(xocp.END_DATE) END_DATE,xocp.product_attribute
      FROM xxwc_om_contract_pricing_lines xocp
      WHERE 1=1 
      --AND AGREEMENT_ID = 6610
      AND xxwc_csp_overlap_dates_check(xocp.agreement_id,
                                      xocp.start_date,
                                      xocp.end_date,
                                      xocp.product_attribute,
                                      xocp.product_value,
                                      xocp.revision_number) = 'Y'
      ORDER BY AGREEMENT_ID,PRODUCT_VALUE,START_DATE;
	  
   CURSOR cr_sub_data(p_agreement_id  xxwc_om_contract_pricing_lines.agreement_id%type,
                      p_product_value xxwc_om_contract_pricing_lines.product_value%type,
					  p_revision_num    xxwc_om_contract_pricing_lines.revision_number%type,
					  p_product_attribute xxwc_om_contract_pricing_lines.product_attribute%type)
   IS
      SELECT rowid sub_rowid,xocp2.agreement_id,xocp2.agreement_line_id,xocp2.product_value,xocp2.revision_number,xocp2.product_attribute,
	         TRUNC(xocp2.START_DATE) START_DATE,TRUNC(xocp2.END_DATE) END_DATE
      FROM xxwc_om_contract_pricing_lines xocp2
      WHERE 1=1 
      AND xocp2.AGREEMENT_ID = p_agreement_id
	  AND xocp2.PRODUCT_VALUE = p_product_value
	  AND xocp2.product_attribute = p_product_attribute
	  AND xocp2.revision_number <> p_revision_num
      ORDER BY AGREEMENT_ID,PRODUCT_VALUE,START_DATE;
BEGIN
   dbms_output.put_line('Start Update Script');
   l_sec := 'Start main loop';
   FOR rec_main_data IN cr_main_data
   LOOP
      --dbms_output.put_line('Start Processing '||' Agreement_id:'||rec_main_data.agreement_id||' Agreement_line_id:'||rec_main_data.agreement_line_id||' Product:'||rec_main_data.product_value);
      l_loop_exit_flag := 'N';
	  l_sec := 'Start subsidiary loop for agreement_id:'||rec_main_data.agreement_id||' agreement_line_id:'||rec_main_data.agreement_line_id;
      FOR rec_sub_data IN cr_sub_data(rec_main_data.agreement_id,
	                                  rec_main_data.product_value,
					                  rec_main_data.revision_number,
					                  rec_main_data.product_attribute)
      LOOP
         IF (rec_main_data.START_DATE BETWEEN rec_sub_data.START_DATE AND rec_sub_data.END_DATE) AND (rec_main_data.END_DATE IS NOT NULL)
		 THEN
		    l_sec := 'Update#1';
			IF  (rec_main_data.START_DATE - 1) > rec_sub_data.START_DATE 
			THEN
			   l_sec := 'Update#1.1';
		       UPDATE xxwc_om_contract_pricing_lines xocp3
			   SET xocp3.END_DATE = rec_main_data.START_DATE - 1
			   WHERE rowid = rec_sub_data.sub_rowid;
			   
			   xxwc_update_modifier_lines(p_agreement_id => rec_sub_data.agreement_id,
                                        p_agreement_line_id => rec_sub_data.agreement_line_id,
										p_error_msg => l_error_msg);
			   l_loop_exit_flag := 'Y';
			ELSIF (rec_sub_data.END_DATE + 1) < rec_main_data.END_DATE 
			THEN
			   l_sec := 'Update#1.2';
			   UPDATE xxwc_om_contract_pricing_lines xocp3
			   SET xocp3.START_DATE = rec_sub_data.END_DATE + 1
			   WHERE rowid = rec_main_data.main_rowid;
			   
			   xxwc_update_modifier_lines(p_agreement_id => rec_main_data.agreement_id,
                                        p_agreement_line_id => rec_main_data.agreement_line_id,
										p_error_msg => l_error_msg);
			   l_loop_exit_flag := 'Y';
			ELSIF rec_sub_data.START_DATE <  rec_sub_data.END_DATE AND rec_main_data.REVISION_NUMBER > rec_sub_data.REVISION_NUMBER
			THEN
			   l_sec := 'Update#1.3';
			   UPDATE xxwc_om_contract_pricing_lines xocp3
			   SET xocp3.START_DATE = rec_sub_data.END_DATE + 1
			      ,xocp3.END_DATE = rec_sub_data.END_DATE + 2
			   WHERE rowid = rec_main_data.main_rowid;
			   
			   xxwc_update_modifier_lines(p_agreement_id => rec_main_data.agreement_id,
                                        p_agreement_line_id => rec_main_data.agreement_line_id,
										p_error_msg => l_error_msg);
			   l_loop_exit_flag := 'Y';
			ELSIF rec_sub_data.START_DATE <  rec_sub_data.END_DATE AND rec_main_data.START_DATE <  rec_main_data.END_DATE AND rec_sub_data.START_DATE < rec_main_data.START_DATE AND rec_sub_data.START_DATE < rec_main_data.END_DATE AND
			rec_main_data.REVISION_NUMBER < rec_sub_data.REVISION_NUMBER --Ex agreement id: 57361
			THEN
			   l_sec := 'Update#1.4';
			   UPDATE xxwc_om_contract_pricing_lines xocp3
			   SET xocp3.START_DATE = rec_sub_data.START_DATE
			      ,xocp3.END_DATE = rec_main_data.START_DATE
			   WHERE rowid = rec_main_data.main_rowid;
			   
			   xxwc_update_modifier_lines(p_agreement_id => rec_main_data.agreement_id,
                                        p_agreement_line_id => rec_main_data.agreement_line_id,
										p_error_msg => l_error_msg);
               UPDATE xxwc_om_contract_pricing_lines xocp3
			   SET xocp3.START_DATE = rec_main_data.END_DATE
			   WHERE rowid = rec_sub_data.sub_rowid;
			   
			   xxwc_update_modifier_lines(p_agreement_id => rec_sub_data.agreement_id,
                                        p_agreement_line_id => rec_sub_data.agreement_line_id,
										p_error_msg => l_error_msg);
			   l_loop_exit_flag := 'Y';
			END IF;
         ELSIF (rec_main_data.END_DATE BETWEEN rec_sub_data.START_DATE AND rec_sub_data.END_DATE) AND 
		    (rec_sub_data.END_DATE IS NOT NULL) AND (rec_main_data.END_DATE IS NOT NULL)
		 THEN
		    l_sec := 'Update#2';
		    UPDATE xxwc_om_contract_pricing_lines xocp3
			   SET xocp3.END_DATE = rec_main_data.START_DATE + 1
			   WHERE rowid = rec_main_data.main_rowid;
			   
			   xxwc_update_modifier_lines(p_agreement_id => rec_main_data.agreement_id,
                                        p_agreement_line_id => rec_main_data.agreement_line_id,
										p_error_msg => l_error_msg);
			   l_loop_exit_flag := 'Y';
         ELSIF rec_main_data.START_DATE <= rec_sub_data.END_DATE AND rec_main_data.END_DATE IS NULL
		 THEN
		    l_sec := 'Update#3';
			IF (rec_main_data.START_DATE - 1) > rec_sub_data.START_DATE THEN
			   UPDATE xxwc_om_contract_pricing_lines xocp3
			   SET xocp3.END_DATE = rec_main_data.START_DATE -1
			   WHERE rowid = rec_sub_data.sub_rowid;
			   
			   xxwc_update_modifier_lines(p_agreement_id => rec_sub_data.agreement_id,
                                        p_agreement_line_id => rec_sub_data.agreement_line_id,
										p_error_msg => l_error_msg);
			   l_loop_exit_flag := 'Y';
		    END IF;
		 ELSIF rec_main_data.START_DATE >= rec_sub_data.START_DATE AND rec_sub_data.END_DATE IS NULL AND rec_main_data.END_DATE IS NOT NULL
		 THEN
		    l_sec := 'Update#4';
			UPDATE xxwc_om_contract_pricing_lines xocp3
			SET xocp3.START_DATE = (select max(end_date) + 1
			                        from  xxwc_om_contract_pricing_lines xocp4
			                        where xocp4.agreement_id = rec_main_data.agreement_id
	                                AND   xocp4.product_value = rec_main_data.product_value)
			WHERE rowid = rec_sub_data.sub_rowid;
			
			xxwc_update_modifier_lines(p_agreement_id => rec_sub_data.agreement_id,
                                       p_agreement_line_id => rec_sub_data.agreement_line_id,
									   p_error_msg => l_error_msg
									  );
			l_loop_exit_flag := 'Y';
         ELSIF rec_main_data.START_DATE <= rec_sub_data.START_DATE AND rec_main_data.END_DATE IS NULL AND 
		       rec_sub_data.END_DATE IS NULL
		 THEN
		    l_sec := 'Update#5';
			IF (rec_sub_data.START_DATE - 1) > rec_main_data.START_DATE THEN
			   l_sec := 'Update#5.1';
			   UPDATE xxwc_om_contract_pricing_lines xocp4
			   SET xocp4.END_DATE = rec_sub_data.START_DATE - 1
			   WHERE rowid = rec_main_data.main_rowid;
			   xxwc_update_modifier_lines(p_agreement_id => rec_main_data.agreement_id,
                                       p_agreement_line_id => rec_main_data.agreement_line_id,
									   p_error_msg => l_error_msg
									  );
			   l_loop_exit_flag := 'Y';
			ELSE
			   l_sec := 'Update#5.2';
			   UPDATE xxwc_om_contract_pricing_lines xocp4
			   SET xocp4.END_DATE = rec_main_data.START_DATE + 1
			   WHERE rowid = rec_main_data.main_rowid;
			   xxwc_update_modifier_lines(p_agreement_id => rec_main_data.agreement_id,
                                       p_agreement_line_id => rec_main_data.agreement_line_id,
									   p_error_msg => l_error_msg
									  );
			   UPDATE xxwc_om_contract_pricing_lines xocp4
			   SET xocp4.START_DATE = rec_main_data.START_DATE + 2
			   WHERE rowid = rec_sub_data.sub_rowid;
			   xxwc_update_modifier_lines(p_agreement_id => rec_sub_data.agreement_id,
                                       p_agreement_line_id => rec_sub_data.agreement_line_id,
									   p_error_msg => l_error_msg
									  );
			   l_loop_exit_flag := 'Y';
			END IF;
         ELSIF rec_sub_data.START_DATE > rec_sub_data.END_DATE AND rec_main_data.START_DATE = rec_sub_data.START_DATE AND 
		       (rec_main_data.END_DATE - rec_main_data.START_DATE) > 1  AND rec_main_data.REVISION_NUMBER > rec_sub_data.REVISION_NUMBER
		 THEN
		    l_sec := 'Update#6';
			UPDATE xxwc_om_contract_pricing_lines xocp4
			   SET xocp4.START_DATE = rec_sub_data.END_DATE
			      ,xocp4.END_DATE   = rec_sub_data.START_DATE
			   WHERE rowid = rec_sub_data.sub_rowid;
			   xxwc_update_modifier_lines(p_agreement_id => rec_sub_data.agreement_id,
                                       p_agreement_line_id => rec_sub_data.agreement_line_id,
									   p_error_msg => l_error_msg
									  );
			UPDATE xxwc_om_contract_pricing_lines xocp4
			   SET xocp4.START_DATE = rec_main_data.START_DATE + 1
			   WHERE rowid = rec_main_data.main_rowid;
			   xxwc_update_modifier_lines(p_agreement_id => rec_main_data.agreement_id,
                                       p_agreement_line_id => rec_main_data.agreement_line_id,
									   p_error_msg => l_error_msg
									  );
			   l_loop_exit_flag := 'Y';
         ELSIF rec_main_data.agreement_id = 6629 AND rec_main_data.product_value = '437WS1118' AND rec_sub_data.START_DATE = TO_DATE('27-OCT-2104','DD-MON-YYYY')
		 THEN
		    l_sec := 'Update#7';
			UPDATE xxwc_om_contract_pricing_lines xocp4
			   SET xocp4.START_DATE = TO_DATE('27-OCT-2014 00:00:00','DD-MON-YYYY HH24:MI:SS')
			   WHERE rowid = rec_sub_data.sub_rowid;
			   xxwc_update_modifier_lines(p_agreement_id => rec_sub_data.agreement_id,
                                       p_agreement_line_id => rec_sub_data.agreement_line_id,
									   p_error_msg => l_error_msg
									  );
			   l_loop_exit_flag := 'Y';
		 END IF;
		 IF l_loop_exit_flag = 'Y' THEN
		    IF l_error_msg IS NULL THEN
		       COMMIT;
			   dbms_output.put_line('SUCCESS '||l_sec||' Agreement_id:'||rec_sub_data.agreement_id||' Agreement_line_id:'||rec_sub_data.agreement_line_id||' Product:'||rec_sub_data.product_value);
			ELSE
			   dbms_output.put_line('ERROR '||l_sec||' Agreement_id:'||rec_sub_data.agreement_id||' Agreement_line_id:'||rec_sub_data.agreement_line_id||' Product:'||rec_sub_data.product_value||'=>'||l_error_msg);
			   ROLLBACK;
			END IF;
		    EXIT;
         END IF;
	  END LOOP;
	  dbms_output.put_line('End Processing '||' Agreement_id:'||rec_main_data.agreement_id||' Agreement_line_id:'||rec_main_data.agreement_line_id);
   END LOOP;
   dbms_output.put_line('End Update Script');
EXCEPTION
   when others then
      dbms_output.put_line(l_sec||'=>'||sqlerrm);
END;
/
Drop Procedure xxwc_update_modifier_lines
/
