/*
 TMS: 20150804-00228 [Parent]
 TMS: 20151112-00097 [Child]
 Date: 11/12/2015
*/
DROP TABLE XXCUS.XXCUS_TMP_TMS_2015080400228;
CREATE TABLE XXCUS.XXCUS_TMP_TMS_2015080400228 
(
   dctm_url               varchar2(240)
  ,apex_url                varchar2(240)
  ,location_id               NUMBER
  ,lease_detail_id        NUMBER
  ,lease_id                     NUMBER
  ,loc_url_status         VARCHAR2(20) DEFAULT 'NEW'
  ,rer_url_status         VARCHAR2(20)  DEFAULT 'NEW'     
  ,error_msg                VARCHAR2(2000)                                   
);
GRANT ALL ON XXCUS.XXCUS_TMP_TMS_2015080400228 TO BS006141 WITH GRANT OPTION;
CREATE INDEX  XXCUS.XXCUS_TMP_TMS_2015080400228_N1 ON  XXCUS.XXCUS_TMP_TMS_2015080400228 (DCTM_URL);
