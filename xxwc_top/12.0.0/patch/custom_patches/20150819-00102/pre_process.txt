echo XPatch 20150819-00102 created by Rivas, Andre A on 8/19/2015 9:50:51 AM
echo include any Linux commands to be executed before the XPatch is applied.
echo Beginning XPatch execution for 20150819-00102
echo 'Num Unk Permissions Num User Group Num Month Day YearOrTime FilePath Type Analysis' > sox_audit_full_list.txt
find $APPL_TOP/ -type f -name '*.fmx' -mtime -75 -ls >> sox_audit_full_list.txt
find $APPL_TOP/ -type f -name '*.plx' -mtime -75 -ls >> sox_audit_full_list.txt
find $APPL_TOP/ -type f -name '*.rdf' -mtime -75 -ls >> sox_audit_full_list.txt
find $APPL_TOP/ -type f -name '*.sql' -mtime -75 -ls >> sox_audit_full_list.txt
find $APPL_TOP/ -type f -name '*.pls' -mtime -75 -ls >> sox_audit_full_list.txt
find $APPL_TOP/ -type f -name '*.ldt' -mtime -75 -ls >> sox_audit_full_list.txt
find $JAVA_TOP/ -type f -name '*.java' -mtime -75 -ls >> sox_audit_full_list.txt
find $JAVA_TOP/ -type f -name '*.class' -mtime -75 -ls >> sox_audit_full_list.txt
find $JAVA_TOP/ -type f -name '*.xml' -mtime -75 -ls >> sox_audit_full_list.txt
