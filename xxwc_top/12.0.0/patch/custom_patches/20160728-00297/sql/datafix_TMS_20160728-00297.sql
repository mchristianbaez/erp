/*************************************************************************
  $Header TMS_20160728-00297_SHIPPED_TO_CLOSE.sql $
  Module Name: TMS_20160728-00297


  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
   1.0        25-AUG-2016  Niraj K Ranjan   TMS#20160728-00297 for order number 20752886
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160728-00297    , Before Update');

update apps.oe_order_lines_all
set 
  INVOICE_INTERFACE_STATUS_CODE='NOT_ELIGIBLE'
 ,open_flag='N'
 ,flow_status_code='CLOSED'
  --,INVOICED_QUANTITY=1
where line_id  in (select line_id from apps.oe_order_lines_all where header_id= 43803437 
                   and flow_status_Code='INVOICE_HOLD')
and headeR_id=43803437;

   DBMS_OUTPUT.put_line (
         'TMS: 20160728-00297  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160728-00297    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160728-00297 , Errors : ' || SQLERRM);
END;
/
