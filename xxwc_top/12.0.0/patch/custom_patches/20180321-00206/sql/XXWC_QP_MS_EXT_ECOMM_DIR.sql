/*******************************************************************************
  * Directory creation:   XXWC_QP_MS_EXT_ECOMM_DIR
  * Description: XXWC_QP_MS_EXT_ECOMM_DIR and XXWC_QP_MS_EXT_ECOMM_ARC_DIR 
                 directories created for price lists for micro sites.  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     04/04/2018    Pattabhi Avula  TMS#20180321-00206 Initial Version
  
********************************************************************************/

CREATE OR REPLACE DIRECTORY XXWC_QP_MS_EXT_ECOMM_DIR AS '/xx_iface/ebsprd/inbound/wcs/microsites'
/
CREATE OR REPLACE DIRECTORY XXWC_QP_MS_EXT_ECOMM_ARC_DIR AS '/xx_iface/ebsprd/inbound/wcs/microsites/archive'
/