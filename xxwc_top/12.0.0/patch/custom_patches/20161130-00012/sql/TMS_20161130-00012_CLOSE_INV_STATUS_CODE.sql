/*************************************************************************
  $Header TMS_20161130-00012_CLOSE_INV_STATUS_CODE.sql $
  Module Name: 20161130-00012  Data Fix script for 20161130-00012

  PURPOSE: Data fix script for 20161130-00012--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        21-DEC-2016  Ashwin Sridhar        TMS#20161130-00012

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   
  DBMS_OUTPUT.put_line ('TMS: 20161130-00012    , Before Update');

  UPDATE apps.oe_order_lines_all
  SET    INVOICE_INTERFACE_STATUS_CODE='NOT_ELIGIBLE'
  ,      open_flag ='N'
  ,      flow_status_code='CLOSED'
  WHERE  line_id   = 80530763
  AND    header_id = 49289179;

   DBMS_OUTPUT.put_line (
         'TMS: 20161130-00012  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

  COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20161130-00012    , End Update');
   
EXCEPTION
WHEN others THEN
     
  ROLLBACK;
     
  DBMS_OUTPUT.put_line ('TMS: 20161130-00012 , Errors : ' || SQLERRM);

END;
/