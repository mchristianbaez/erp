/* Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        13-Jan-2016  Kishorebabu V    TMS#20160111-00251 data fix script to process month end transactions for delivery id # 5126764
                                            Initial Version   */

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

UPDATE apps.wsh_delivery_details
SET    released_status          = 'D'
,      src_requested_quantity   = 0
,      requested_quantity       = 0
,      shipped_quantity         = 0
,      cycle_count_quantity     = 0
,      cancelled_quantity       = 0
,      subinventory             = NULL
,      locator_id               = NULL
,      lot_number               = NULL
,      revision                 = NULL
,      inv_interfaced_flag      = 'X'
,      oe_interfaced_flag       = 'X'
WHERE  delivery_detail_id       = 13364090;

--1 row expected to be updated

UPDATE apps.wsh_delivery_assignments
SET    delivery_id                = NULL
,      parent_delivery_detail_id  = NULL
WHERE  delivery_detail_id         = 13364090;

--1 row expected to be updated

COMMIT;
/