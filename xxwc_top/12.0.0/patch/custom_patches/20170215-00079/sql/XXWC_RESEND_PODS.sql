/*
 TMS: 20170215-00079 
 Date: 03/16/2017
 Notes: Missing OSU - PODs 
*/

SET serveroutput ON

SET SERVEROUTPUT ON SIZE 1000000

DECLARE
   l_request_id_count NUMBER;
   l_argument1 fnd_concurrent_requests.argument1%TYPE;
   l_argument2 fnd_concurrent_requests.argument1%TYPE; 
   l_argument3 fnd_concurrent_requests.argument1%TYPE; 
   l_argument4 fnd_concurrent_requests.argument1%TYPE; 
   l_argument5 fnd_concurrent_requests.argument1%TYPE; 
   l_argument6 fnd_concurrent_requests.argument1%TYPE; 
   l_argument7 fnd_concurrent_requests.argument1%TYPE; 
   l_argument8 fnd_concurrent_requests.argument1%TYPE; 
   l_argument9 fnd_concurrent_requests.argument1%TYPE; 
   l_argument10 fnd_concurrent_requests.argument1%TYPE; 
   l_argument11 fnd_concurrent_requests.argument1%TYPE;
   
   TYPE xxwc_header_ids_tbl_rec IS RECORD (
         header_id        xxwc.xxwc_signature_capture_tbl.id%TYPE,
		 DELIVERY_ID      xxwc.xxwc_signature_capture_tbl.DELIVERY_ID%TYPE
      );
   
   TYPE header_ids_tbl IS TABLE OF  xxwc_header_ids_tbl_rec INDEX BY PLS_INTEGER;
   l_header_ids_tbl   header_ids_tbl;
   
   TYPE request_ids_tbl IS TABLE OF NUMBER INDEX BY PLS_INTEGER;
   l_request_ids_tbl   request_ids_tbl;
   
   l_error_buff                VARCHAR2 (4000);
   l_error_code                VARCHAR2 (1);
   l_return_status             VARCHAR2 (4000);
   l_req_id                    NUMBER;
   l_set_mode			       BOOLEAN;
   l_set_print_options         BOOLEAN;
   l_layout                    BOOLEAN;
      
   
   PROCEDURE write_log (p_message IN VARCHAR2)
   IS
   BEGIN
     DBMS_OUTPUT.PUT_LINE(p_message);
   END;
   
   PROCEDURE wait_for_running_request ( p_request_id IN NUMBER )
   IS
      CURSOR cu_running_requests ( p_request_id IN NUMBER)
      IS
         SELECT /*+ Index(fnd_concurrent_requests_N7) */ request_id
           FROM apps.fnd_concurrent_requests fcr
          WHERE fcr.concurrent_program_id IN ( SELECT CONCURRENT_PROGRAM_ID 
                                                 FROM FND_CONCURRENT_PROGRAMS_VL 
                                                WHERE CONCURRENT_PROGRAM_NAME ='XXWC_OM_PACK_SLIP_DATAFIX'
                                                AND ENABLED_FLAG = 'Y' ) 
            AND (status_code = 'R' OR status_code = 'I')
            AND (phase_code = 'R' OR phase_code ='P')
            AND request_id = p_request_id;

      l_request_id    NUMBER;
      lc_phase        VARCHAR2 (100);
      lc_status       VARCHAR2 (100);
      lc_dev_phase    VARCHAR2 (100);
      lc_dev_status   VARCHAR2 (100);
      lc_message      VARCHAR2 (100);
      lc_waited BOOLEAN;
   BEGIN
      LOOP
         OPEN cu_running_requests( p_request_id );

         FETCH cu_running_requests
          INTO l_request_id;
         
         IF cu_running_requests%NOTFOUND
         THEN
            CLOSE cu_running_requests;

            EXIT;
         END IF;

         CLOSE cu_running_requests;

         IF l_request_id > 0
         THEN
            lc_waited:=fnd_concurrent.wait_for_request (
                                             request_id      => l_request_id,
                                             INTERVAL        => 2,
                                             max_wait        => 180,
                                             phase           => lc_phase,
                                             status          => lc_status,
                                             dev_phase       => lc_dev_phase,
                                             dev_status      => lc_dev_status,
                                             MESSAGE         => lc_message
                                            );
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log ('Error in wait_for_running_request order header id ' || p_request_id);
         write_log ('Error: ' || SQLERRM);
         RAISE;
   END wait_for_running_request;
   
   BEGIN
	  FND_GLOBAL.APPS_INITIALIZE (15986, 50880, 222);
    
      mo_global.set_policy_context('S',162);
    
	   SELECT id, delivery_id
       BULK COLLECT INTO l_header_ids_tbl
       FROM xxwc.xxwc_signature_capture_tbl scap
      WHERE 1=1
	    AND trunc(scap.creation_date) BETWEEN TO_DATE('09/02/2017', 'DD/MM/YYYY') AND TO_DATE('11/02/2017', 'DD/MM/YYYY')
        AND NVL (scap.SIGNATURE_NAME, '*&*^&*') != '***CANCEL***'
		AND scap.delivery_id IS NOT NULL
	    AND EXISTS ( SELECT 1
	                  FROM xxwc.xxwc_print_log_tbl XPLT
                     WHERE XPLT.header_id             = scap.id
				       AND XPLT.ARGUMENT10            = scap.delivery_id
				       AND XPLT.CONCURRENT_PROGRAM_ID = 70401
  				       AND XPLT.signature_name IS NOT NULL
		           )
		AND NOT EXISTS ( SELECT 1 
	                       FROM APPS.FND_CONCURRENT_REQUESTS fcr
			   		      WHERE fcr.argument2 = scap.id
						    AND fcr.concurrent_program_id IN ( SELECT CONCURRENT_PROGRAM_ID 
                                                                 FROM FND_CONCURRENT_PROGRAMS_VL 
                                                                WHERE CONCURRENT_PROGRAM_NAME ='XXWC_OM_PACK_SLIP_DATAFIX'
                                                                  AND ENABLED_FLAG = 'Y' ) 
			  	        )
     UNION
       SELECT id, delivery_id
       FROM xxwc.xxwc_signature_capture_arc_tbl scap
       WHERE 1=1
	   AND trunc(scap.creation_date) BETWEEN TO_DATE('09/02/2017', 'DD/MM/YYYY') AND TO_DATE('11/02/2017', 'DD/MM/YYYY')
       AND NVL (scap.SIGNATURE_NAME, '*&*^&*') != '***CANCEL***'
	   AND scap.delivery_id IS NOT NULL
	   AND EXISTS ( SELECT 1
	                  FROM xxwc.xxwc_print_log_tbl XPLT
                     WHERE XPLT.header_id             = scap.id
				       AND XPLT.ARGUMENT10            = TO_CHAR(scap.delivery_id)
				       AND XPLT.CONCURRENT_PROGRAM_ID = 70401
  				       AND XPLT.signature_name IS NOT NULL
		           )
	   AND NOT EXISTS (SELECT 1 
	                     FROM APPS.FND_CONCURRENT_REQUESTS fcr
				        WHERE fcr.argument2 =  TO_CHAR(scap.id)
						  AND fcr.concurrent_program_id IN ( SELECT CONCURRENT_PROGRAM_ID 
                                                               FROM FND_CONCURRENT_PROGRAMS_VL 
                                                              WHERE CONCURRENT_PROGRAM_NAME ='XXWC_OM_PACK_SLIP_DATAFIX'
                                                                AND ENABLED_FLAG = 'Y' ) 
			  	  );

      write_log ( 'PL/SQL table records count: ' || l_header_ids_tbl.COUNT );
	  l_request_id_count := 0;

      IF l_header_ids_tbl.COUNT > 0
      THEN
         FOR i IN l_header_ids_tbl.FIRST .. l_header_ids_tbl.LAST
         LOOP
            BEGIN
               SELECT ARGUMENT1, ARGUMENT2, ARGUMENT3, ARGUMENT4, ARGUMENT5, ARGUMENT6, ARGUMENT7, ARGUMENT8, ARGUMENT9, ARGUMENT10, ARGUMENT11
                 INTO l_argument1, l_argument2, l_argument3, l_argument4, l_argument5, l_argument6, l_argument7, l_argument8, l_argument9, l_argument10, l_argument11
                 FROM xxwc.xxwc_print_log_tbl XPLT
                WHERE header_id = l_header_ids_tbl(i).header_id
				  AND XPLT.ARGUMENT10 = l_header_ids_tbl(i).DELIVERY_ID
				  AND CONCURRENT_PROGRAM_ID = 70401
				  AND signature_name is not null
				  AND rownum=1;

               write_log ('Fetched l_argument1: '||l_argument1||' l_argument2: '|| l_argument2||' l_argument3: '|| l_argument3||' l_argument4: '|| l_argument4||' l_argument5: '||l_argument5||' l_argument6: '|| l_argument6||' l_argument7: '|| l_argument7||' l_argument8: '|| l_argument8||' l_argument9: '|| l_argument9||' l_argument10: '|| l_argument10||' l_argument11: '|| l_argument11);
              
             l_layout :=
                 fnd_request.add_layout (template_appl_name      => 'XXWC',
                                         template_code           => 'XXWC_OM_PACK_SLIP',
                                         template_language       => 'ENG',
                                         template_territory      => NULL,    --   'US'
                                         output_format           => 'PDF' --'RTF'
                                        );
                                            
              l_set_print_options := FND_Request.set_print_options(printer=>'noprint',style=>NULL,copies=>0,save_output =>TRUE,print_together =>'N',validate_printer =>'RESOLVE');
 
              l_set_mode := FND_Request.Set_Mode(TRUE);
       
			  --------------------------------------------------------------------------
	          -- Submit "XXWC Create Outbound File Common Program"
	          --------------------------------------------------------------------------
	          l_req_id := fnd_request.submit_request(application => 'XXWC'
	                                       , program     => 'XXWC_OM_PACK_SLIP_DATAFIX'
	                                       , description => NULL
	                                       , start_time  => SYSDATE
	                                       , sub_request => FALSE
	                                       , argument1   => l_argument1 --Organization
	                                       , argument2   => l_argument2 --Order Number
	                                       , argument3   => l_argument3 --Print Pricing
										   , argument4   => 'Y'--l_argument4 --Reprint
	                                       , argument5   => l_argument5 --Print Kit Details
	                                       , argument6   => l_argument6 --Send to Rightfax Yes or No?
	                                       , argument7   => l_argument7 --Fax Number
										   , argument8   => l_argument8 --Rightfax Comment
	                                       , argument9   => l_argument9 --Print Hazmat
	                                       , argument10  => l_argument10 --Delivery ID
										   , argument11  => l_argument11 --Print Allocations
	                                       );
	
	         COMMIT;

             IF NVL (l_req_id, 0) > 0
             THEN
			    l_request_id_count := l_request_id_count+1;
			    l_request_ids_tbl (l_request_id_count) := l_req_id;
				
  			    l_return_status := 'Concurrent request '  || l_req_id || ' has been submitted to header_id ' || l_header_ids_tbl(i).header_id|| 'Delivery id: '||l_header_ids_tbl(i).DELIVERY_ID;
				write_log  (l_return_status); 
             ELSE
               l_return_status := 'Could not submit request to pick order ' ||l_header_ids_tbl(i).header_id|| 'Delivery id: '||l_header_ids_tbl(i).DELIVERY_ID;
			   write_log  (l_return_status); 
             END IF;
			 
			 IF l_request_ids_tbl.COUNT >= 50 THEN
			    FOR i IN l_request_ids_tbl.FIRST .. l_request_ids_tbl.LAST
			    LOOP
			       wait_for_running_request ( p_request_id => l_request_ids_tbl(i) );
				END LOOP;  
				l_request_ids_tbl.delete;
				l_request_id_count := 0;
			 END IF;
               
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log('NO record found  in with xxwc_print_log_tbl table for header_id: '||l_header_ids_tbl(i).header_id || 'Delivery id: '||l_header_ids_tbl(i).DELIVERY_ID);
            END;
         END LOOP; 
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log (   'XXWC_RESEND_PODS- When Others Error: '|| SQLERRM);
END;
/
