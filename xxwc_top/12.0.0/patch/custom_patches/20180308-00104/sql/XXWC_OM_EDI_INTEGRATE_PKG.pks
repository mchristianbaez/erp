CREATE OR REPLACE PACKAGE APPS.XXWC_OM_EDI_INTEGRATE_PKG
AS
/******************************************************************************
      $Header XXWC_OM_EDI_INTEGRATE_PKG.pks $
      Module Name: XXWC_OM_EDI_INTEGRATE_PKG.pks

      PURPOSE:   This XXWC_OM_EDI_INTEGRATE_PKG package is used to automate
	             the EDI process through UC4. Customer Integration - Order 
				 Import Modifications for ecomm

      REVISIONS:
      Ver        Date         Author              Description 
      ---------  ----------   ---------------     --------------------------
      1.0        12-04-2018  Pattabhi Avula       TMS#20180308-00104, 
	                                              TMS#20180308-00274
	                                              Initial version
*******************************************************************************/


PROCEDURE PROG_850_IB_UC4_WRAPPER(ERRBUFF OUT VARCHAR2, RETCODE OUT VARCHAR2, P_PATH IN VARCHAR2,P_EMAIL1 IN VARCHAR2,P_EMAIL2 IN VARCHAR2);

PROCEDURE PROG_855_OB_UC4_WRAPPER(ERRBUFF OUT VARCHAR2, RETCODE OUT VARCHAR2,P_DAYS IN NUMBER,P_PATH IN VARCHAR2);

PROCEDURE PROG_810_OB_UC4_WRAPPER(ERRBUFF OUT VARCHAR2, RETCODE OUT VARCHAR2,P_DAYS IN NUMBER, P_PATH IN VARCHAR2);

END XXWC_OM_EDI_INTEGRATE_PKG;
/