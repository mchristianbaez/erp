CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_EDI_INTEGRATE_PKG
AS
/******************************************************************************
      $Header XXWC_OM_EDI_INTEGRATE_PKG.pkb $
      Module Name: XXWC_OM_EDI_INTEGRATE_PKG.pkb

      PURPOSE:   This XXWC_OM_EDI_INTEGRATE_PKG package is used to automate
	             the EDI process through UC4. Customer Integration - Order 
				 Import Modifications for ecomm

      REVISIONS:
      Ver        Date         Author              Description 
      ---------  ----------   ---------------     --------------------------
      1.0        12-04-2018  Pattabhi Avula       TMS#20180308-00104, 
	                                              TMS#20180308-00274
	                                              Initial version
*******************************************************************************/

/******************************************************************************
      $Header PROG_850_IB_UC4_WRAPPER.prc $
      Module Name: PROG_850_IB_UC4_WRAPPER.prc
      PURPOSE:   This XXWC_OM_EDI_INTEGRATE_PKG package is used to automate
	             the EDI process through UC4. Customer Integration - Order 
				 Import Modifications for ecomm

      REVISIONS:
      Ver        Date         Author              Description 
      ---------  ----------   ---------------     --------------------------
      1.0        12-04-2018  Pattabhi Avula       TMS#20180308-00104,
	                                              TMS#20180308-00274
	                                              Initial version
*******************************************************************************/

PROCEDURE PROG_850_IB_UC4_WRAPPER(ERRBUFF  OUT VARCHAR2,
                                  RETCODE  OUT VARCHAR2, 
								  P_PATH   IN  VARCHAR2,
								  P_EMAIL1 IN  VARCHAR2,
								  P_EMAIL2 IN  VARCHAR2)
IS

  
  --------------------------------------------------------------
  -- Email Variables
  --------------------------------------------------------------
  l_host                         VARCHAR2 (256)
                                    := 'mailoutrelay.hdsupply.net';
  l_hostport                     VARCHAR2 (20) := '25';
                                 
  l_sender                       VARCHAR2 (100) := 'no-reply@whitecap.net';
                                 
  l_subject                      VARCHAR2 (32767) DEFAULT NULL;
  l_body                         VARCHAR2 (32767) DEFAULT NULL;
  l_body_header                  VARCHAR2 (32767) DEFAULT NULL;
  l_body_detail                  VARCHAR2 (32767) DEFAULT NULL;
  l_body_footer                  VARCHAR2 (32767) DEFAULT NULL;
  l_instance                     VARCHAR2 (50);
  l_def_notif_email              VARCHAR2 (250); 
  
  --------------------------------------------------------------
  -- Local Variables
  --------------------------------------------------------------
  
  v_rec_cnt                      NUMBER;
  v_filename                     VARCHAR2(100);
  l_user_id                      FND_USER.USER_ID%TYPE; --:=30437; --44422;
  l_responsibility_id            FND_RESPONSIBILITY_VL.RESPONSIBILITY_ID%TYPE; --:=50922;
  l_resp_application_id          FND_RESPONSIBILITY_VL.APPLICATION_ID%TYPE; --:=175;
  l_org_id                       NUMBER:=162;
  l_req_id                       NUMBER NULL;
  lc_phase                       VARCHAR2(50);
  lc_status                      VARCHAR2(50);
  lc_dev_phase                   VARCHAR2(50);
  lc_dev_status                  VARCHAR2(50);
  lc_message                     VARCHAR2(50);
  l_req_return_status            BOOLEAN;
  l_exeception                   EXCEPTION;
  lv_cust_po_num                 VARCHAR2(50)DEFAULT NULL;
  lv_cust_num                    VARCHAR2(50)DEFAULT NULL;
  ln_cust_acct_id                NUMBER:=0;
  lv_ship_to_contact_first_name  VARCHAR2(100)DEFAULT NULL;
  lv_ship_to_contact_last_name   VARCHAR2(100)DEFAULT NULL;
  ln_inv_to_contact_id           NUMBER:=0;
  lv_orig_sys_document_ref       VARCHAR2(50);
  l_import_id                    NUMBER:=0;
  l_ord_req_return_status        BOOLEAN;
  lv_phase                       VARCHAR2(50);
  lv_status                      VARCHAR2(50);
  lv_dev_phase                   VARCHAR2(50);
  lv_dev_status                  VARCHAR2(50);
  lv_message                     VARCHAR2(50);
  l_ord_num                      NUMBER:=0;
  l_ord_err_flag                 VARCHAR2(1);
  ln_party_id                    NUMBER:=0;
  l_cust_name                    VARCHAR2(240);
  
BEGIN

      ----------------------------------------------------------------------------------
      -- Derive Oracle Instance
      ----------------------------------------------------------------------------------
      BEGIN
         SELECT SUBSTR (UPPER (instance_name), 1, 7)
           INTO l_instance
           FROM v$instance;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_instance := NULL;
      END;  

	
	  ----------------------------------------------------------------------------------
      -- Derive FND_GLOBAL.APPS_INITIALIZE Variables
      ----------------------------------------------------------------------------------
	  -- User details             
	                  
	  BEGIN           
         SELECT user_id		        
           INTO l_user_id		        
           FROM fnd_user
		  WHERE user_name='XXWC_INT_SALESFULFILLMENT';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_user_id := NULL;
      END;
	  
	  -- Responsibility_id and application_id
	  
	  BEGIN
         SELECT responsibility_id,
		        application_id
           INTO l_responsibility_id,
		        l_resp_application_id
           FROM FND_RESPONSIBILITY_VL 
		  WHERE RESPONSIBILITY_NAME='HDS EDI Super User - WC';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_responsibility_id := NULL;
			l_resp_application_id:=NULL;
      END;
	  
	DELETE FROM XXWC.XXWC_EDI_IB_FILES_DTLS_TBL;
	
	COMMIT;
	
	INSERT INTO XXWC.XXWC_EDI_IB_FILES_DTLS_TBL(FILE_NAME)
	 SELECT FILE_NAME 
	   FROM XXWC.XXWC_EDI_IB_FILES_EXTRNL_TBL
	  WHERE FILE_NAME LIKE '850%.dat';

	COMMIT;
	
	  fnd_global.apps_initialize (l_user_id,
                                  l_responsibility_id,
                                  l_resp_application_id);
								  
      mo_global.set_policy_context ('S', l_org_id);
      fnd_request.set_org_id (l_org_id);

  SELECT COUNT(*)
    INTO v_rec_cnt
    FROM xxwc.XXWC_EDI_IB_FILES_DTLS_TBL
   WHERE file_name LIKE '850%.dat';
   
   dbms_output.put_line('File count - '||v_rec_cnt);

  IF v_rec_cnt > 0
  THEN

    BEGIN
      FOR c_file IN (SELECT DISTINCT file_name
                       FROM xxwc.XXWC_EDI_IB_FILES_DTLS_TBL
                      WHERE file_name LIKE '850%.dat')
      LOOP
	  BEGIN
        v_filename := c_file.file_name;
		lv_cust_po_num:=SUBSTR(v_filename,(INSTR(v_filename,'_')+1),(INSTR(v_filename,'.')-2)-INSTR(v_filename,'_')+1);
         fnd_file.put_line(fnd_file.log,'v_file_name - ' || v_filename);
		 dbms_output.put_line('v_file_name - ' || v_filename);
		 dbms_output.put_line('PO# - ' || lv_cust_po_num);
   
   l_req_id := fnd_request.submit_request (
                  application   => 'EC',
                  program       => 'ECEPOI',
                  description   => NULL,
                  start_time    => SYSDATE,
                  sub_request   => FALSE,				 
                  argument1     => P_PATH,
                  argument2     => v_filename,
				  argument3     => 0,
				  argument4     => 'N',
				  argument5     => 4,
				  argument6     => 'POI',
				  argument7     => 357,
				  argument8     => 'UTF8');
	  COMMIT;
	  IF l_req_id > 0 THEN
	   dbms_output.put_line('Request_id is '||l_req_id);
       LOOP
	  
	    l_req_return_status :=
            fnd_concurrent.wait_for_request (request_id      => l_req_id
                                            ,interval        => 5 --interval Number of seconds to wait between checks
                                            ,max_wait        => 5 --Maximum number of seconds to wait for the request completion
                                            ,phase           => lc_phase
                                            ,status          => lc_status
                                            ,dev_phase       => lc_dev_phase
                                            ,dev_status      => lc_dev_status
                                            ,message         => lc_message
                                            );						
          EXIT
        WHEN UPPER (lc_phase) = 'COMPLETED' OR UPPER (lc_status) IN ('CANCELLED', 'ERROR', 'TERMINATED');		
      END LOOP;
       
        --
        IF UPPER (lc_phase) = 'COMPLETED' AND UPPER (lc_status) IN ('ERROR','WARNING') THEN		
		
    l_subject := 'Error importing Customer PO#'||lv_cust_po_num;
        
     l_body_header :=
        '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
     l_body_detail :=
           '<BR> <B> '
        || l_instance                                                  
        || '</B> </BR>'
        || '<BR> </BR> <BR> 850 EDI  Error processing Customer PO#'
        || lv_cust_po_num
        || '. Please go to Staged Documents folder to address the issues. </BR>';
     l_body_footer :=
        '<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport for any questions. </BR>';
     l_body :=
           l_body_header
        || l_body_detail
        || CHR (10)
        || l_body_footer;

     xxcus_misc_pkg.html_email (
        p_to              => P_EMAIL1, -- 'HDSWCITECGAlerts@HDSupply.com',  
        p_from            => l_sender,
        p_text            => 'EDI 850 IB Error',
        p_subject         => l_subject,
        p_html            => l_body,
        p_smtp_hostname   => l_host,
        p_smtp_portnum    => l_hostport);
		
           fnd_file.put_line(fnd_file.log,'IN: Purchase Orders (850/ORDERS) Program completed in error. Oracle request id: '||l_req_id ||' '||SQLERRM);
		RETCODE:='2';
		dbms_output.put_line('Returncode '||RETCODE);
        ELSIF UPPER (lc_phase) = 'COMPLETED' AND UPPER (lc_status) = 'NORMAL' THEN
		
       BEGIN
	   SELECT customer_number
	         ,orig_sys_document_ref
	         ,ship_to_contact_first_name
             ,ship_to_contact_last_name
	     INTO lv_cust_num
		     ,lv_orig_sys_document_ref
		     ,lv_ship_to_contact_first_name
			 ,lv_ship_to_contact_last_name			 
		 FROM apps.oe_headers_iface_all
		WHERE 1 = 1
		  AND order_source_id = 6
		  AND customer_po_number = lv_cust_po_num
		  AND ROWNUM =1;
	   EXCEPTION
	    WHEN OTHERS THEN
		 dbms_output.put_line('No data found in OE_HEADERS_IFACE_ALL table for customer PO# :'||lv_cust_po_num);
	  END;
	  
	   -- Fetchcing cutomer account id
         BEGIN		
		  SELECT cust_account_id
                 ,party_id		  
		    INTO ln_cust_acct_id
                ,ln_party_id			
		    FROM apps.hz_cust_accounts 
		   WHERE account_number = lv_cust_num;
         EXCEPTION
		  WHEN OTHERS THEN
		  NULL;
		 END;
		 
		 -- Deriving customer name
		 BEGIN
		 SELECT party_name
           INTO l_cust_name
           FROM apps.hz_parties 
          WHERE party_id=ln_party_id;
		EXCEPTION
		  WHEN OTHERS THEN
		  NULL;
		 END;
		  
		 BEGIN
		  SELECT ACCT_ROLE.CUST_ACCOUNT_ROLE_ID
            INTO ln_inv_to_contact_id
            FROM hz_cust_account_roles acct_role,                       
                 hz_parties party,
                 hz_parties rel_party,
                 hz_relationships rel,
                 hz_org_contacts org_cont,
                 hz_cust_accounts role_acct
           WHERE acct_role.party_id = rel.party_id                       
             AND NOT EXISTS(SELECT '1'
                              FROM hz_role_responsibility rr
                             WHERE rr.cust_account_role_id = acct_role.cust_account_role_id)
             AND acct_role.role_type = 'CONTACT'
             AND org_cont.party_relationship_id = rel.relationship_id
             AND rel.subject_id = party.party_id
             AND rel_party.party_id = rel.party_id
             AND acct_role.cust_account_id = role_acct.cust_account_id
             AND role_acct.party_id = rel.object_id
             AND rel.subject_table_name = 'HZ_PARTIES'
             AND REL.OBJECT_TABLE_NAME = 'HZ_PARTIES'
             AND UPPER(PARTY.PERSON_FIRST_NAME) = UPPER(lv_ship_to_contact_first_name)
             AND UPPER (PARTY.PERSON_LAST_NAME) = UPPER(lv_ship_to_contact_last_name)
             AND (org_cont.job_title IS NULL AND org_cont.job_title_code IS NULL)
             AND party.status = 'A'
             AND rel_party.status = 'A'
             AND rel.status = 'A'
             AND ACCT_ROLE.STATUS = 'A'
             AND (ROLE_ACCT.ACCOUNT_NUMBER = lv_cust_num)
             AND ROWNUM = 1;
		 EXCEPTION
		   WHEN OTHERS THEN
		 ln_inv_to_contact_id:=NULL;
		  dbms_output.put_line('Contact Id not found for customer PO#'||lv_cust_po_num);
		  -- Update interface table to prevent this record from import program
		  UPDATE ont.oe_headers_iface_all
           SET error_flag = 'Y'
         WHERE 1 = 1
		   AND order_source_id = 6
           AND customer_po_number = lv_cust_po_num
		   AND customer_number = lv_cust_num;		   
		   COMMIT;
		   RAISE l_exeception;            
	     END;
		  
		
		UPDATE ont.oe_headers_iface_all
           SET invoice_to_contact_id = ln_inv_to_contact_id
         WHERE 1 = 1
		   AND order_source_id = 6
           AND customer_po_number = lv_cust_po_num
		   AND customer_number = lv_cust_num;
		
		COMMIT;
	   --<End> new logic		
          fnd_file.put_line(fnd_file.log, 'The Program request successful for request id: ' || l_req_id);
	    END IF;	
		
		-- NEED TO CALL ORDER IMPORT PROGRAM		

		  l_import_id := fnd_request.submit_request (
            application => 'ONT',
            program     => 'OEOIMP',
	        argument1   => NULL, -- --Operating_unit_id  bug6918092
            argument2   => '6',	-- Order_Source_Id =6 for EDI
            argument3   => lv_orig_sys_document_ref,	-- Order Ref = all
            argument4   => 'INSERT',-- Operation code = INSERT
            argument5   => 'N',	-- Validate_Only = 'N'
            argument6   => '1',	-- Debug Level = 1
            argument7   => 4 -- No. of Instance to run for OIMP
			);
	COMMIT;		
	IF l_import_id > 0 THEN
	   dbms_output.put_line('Import orders Request_id is '||l_import_id);
       LOOP
	  
	    l_ord_req_return_status :=
            fnd_concurrent.wait_for_request (request_id      => l_import_id
                                            ,interval        => 5 --interval Number of seconds to wait between checks
                                            ,max_wait        => 5 --Maximum number of seconds to wait for the request completion
                                            ,phase           => lv_phase
                                            ,status          => lv_status
                                            ,dev_phase       => lv_dev_phase
                                            ,dev_status      => lv_dev_status
                                            ,message         => lv_message
                                            );						
          EXIT
        WHEN UPPER (lv_phase) = 'COMPLETED' OR UPPER (lv_status) IN ('CANCELLED', 'ERROR', 'TERMINATED');		
      END LOOP;
       
        --
       IF UPPER (lv_phase) = 'COMPLETED' AND UPPER (lv_status) IN ('ERROR','WARNING') THEN
	   
	     l_subject := 'Error importing Customer  PO#'||lv_cust_po_num;
        
          l_body_header :=
             '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
          l_body_detail :=
                '<BR> <B> '
             || l_instance
             || ' </B> </BR> <BR> </BR> <BR> Customer Name : <B>'        
             || l_cust_name
             || ' </B> </BR> <BR> </BR> <BR> Customer Number: <B>'       
             || lv_cust_num                                         
             || '</B> </BR>'
             || ' <BR> </BR> <BR> Error importing CustomerPO#'
             || lv_cust_po_num
             || ' Please go to Import Orders Corrections folder to address the issues. </BR>';
          l_body_footer :=
             '<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport for any questions. </BR>';
          l_body :=
                l_body_header
             || l_body_detail
             || CHR (10)
             || l_body_footer;
          
          xxcus_misc_pkg.html_email (
             p_to              => P_EMAIL1, -- 'HDSWCITECGAlerts@HDSupply.com',   
             p_from            => l_sender,
             p_text            => 'Order Error',
             p_subject         => l_subject,
             p_html            => l_body,
             p_smtp_hostname   => l_host,
             p_smtp_portnum    => l_hostport);
	  	   
              dbms_output.put_line('Order import failed');
              fnd_file.put_line(fnd_file.log,'Order Import failed RETURN REQ_ID - '||l_import_id);	
		
		  RETCODE:='2';
		  dbms_output.put_line('Orders Import Returncode '||RETCODE);		
          fnd_file.put_line(fnd_file.log,'Orders Import Program completed in error. Oracle request id: '||l_import_id ||' '||SQLERRM);
       ELSIF UPPER (lv_phase) = 'COMPLETED' AND UPPER (lv_status) = 'NORMAL' THEN	   
	   
	 -------------------------------------------------------------
     -- Send Email Notification for Order Success scenario
     -------------------------------------------------------------
	 BEGIN
	  SELECT order_number
	    INTO l_ord_num
	    FROM apps.oe_order_headers_all
	   WHERE cust_po_number = lv_cust_po_num
	     AND sold_to_org_id = ln_cust_acct_id;
     EXCEPTION
	   WHEN OTHERS THEN
	    l_ord_num:=NULL;
     END;
	 
	IF l_ord_num IS NOT NULL
	  THEN
     l_subject := 'e-Commerce Gateway Order#'||l_ord_num;
        
     l_body_header :=
        '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
     l_body_detail :=
           '<BR> <B> '
        || l_instance
        || ' </B> </BR> <BR> </BR> <BR> Customer Name : <B>'        
        || l_cust_name
        || ' </B> </BR> <BR> </BR> <BR> Customer Number: <B>'       
        || lv_cust_num                                         
        || '</B> </BR>'
        || ' <BR> </BR> <BR> 850 EDI Sales Order# '
        || l_ord_num
        || ' for CustomerPO# '
        || lv_cust_po_num
        || ' has been created in Oracle. </BR>';
     l_body_footer :=
        '<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport for any questions. </BR>';
     l_body :=
           l_body_header
        || l_body_detail
        || CHR (10)
        || l_body_footer;

     xxcus_misc_pkg.html_email (
        p_to              => P_EMAIL2,  -- 'WC-B2Bprogram-U1@HDSupply.com',   
        p_from            => l_sender,
        p_text            => 'Order Created',
        p_subject         => l_subject,
        p_html            => l_body,
        p_smtp_hostname   => l_host,
        p_smtp_portnum    => l_hostport);
		
         dbms_output.put_line('Import order program completed successfully');
         fnd_file.put_line(fnd_file.log,'Orders Import RETURN REQ_ID - '||l_import_id);		 
	ELSE 
		 -- Sending Email Notification for failed orders
	 SELECT error_flag
	   INTO l_ord_err_flag
	   FROM apps.oe_headers_iface_all
	  WHERE  1 = 1
		AND order_source_id = 6
		AND customer_po_number = lv_cust_po_num
		AND customer_number = lv_cust_num
		AND rownum = 1;
		
	    IF l_ord_num IS NULL AND l_ord_err_flag = 'Y' THEN
		  
	 l_subject := 'Error importing Customer  PO#'||lv_cust_po_num;
        
     l_body_header :=
        '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
     l_body_detail :=
           '<BR> <B> '
        || l_instance
        || ' </B> </BR> <BR> </BR> <BR> Customer Name : <B>'        
        || l_cust_name
        || ' </B> </BR> <BR> </BR> <BR> Customer Number: <B>'       
        || lv_cust_num                                         
        || '</B> </BR>'
        || ' <BR> </BR> <BR> Error importing CustomerPO#'
        || lv_cust_po_num
        || ' Please go to Import Orders Corrections folder to address the issues. </BR>';
     l_body_footer :=
        '<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport for any questions. </BR>';
     l_body :=
           l_body_header
        || l_body_detail
        || CHR (10)
        || l_body_footer;

     xxcus_misc_pkg.html_email (
        p_to              => P_EMAIL1,  -- 'HDSWCITECGAlerts@HDSupply.com',   
        p_from            => l_sender,
        p_text            => 'Order Error',
        p_subject         => l_subject,
        p_html            => l_body,
        p_smtp_hostname   => l_host,
        p_smtp_portnum    => l_hostport);
		
         dbms_output.put_line('Order import failed');
         fnd_file.put_line(fnd_file.log,'Order Import failed RETURN REQ_ID - '||l_import_id);		
		END IF;
	 END IF;
	   END IF;   
	END IF;  -- Import program
	
   END IF;	
    EXCEPTION
	 WHEN l_exeception THEN
	 -------------------------------------------------------------
            -- Send Email Notification for Contact Id not found scenario
            -------------------------------------------------------------
            l_subject := 'Error importing for CustomerPO#'||lv_cust_po_num;
               
            l_body_header :=
               '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
            l_body_detail :=
                  '<BR> <B> '
               || l_instance
               || ' </B> </BR> <BR> </BR> <BR> Customer Name : <B>'        
               || l_cust_name
               || ' </B> </BR> <BR> </BR> <BR> Customer Number: <B>'       
               || lv_cust_num                                         
               || '</B> </BR>'
               || ' <BR> </BR> <BR> 850 EDI Sales Order#  ' 
               || 'CustomerPO# '
               || lv_cust_po_num
               || ' Please check the archive folder for the failed Purchase Order and check Contacts.'
	       	   || '</B> </BR>'
	       	   || 'Clear the failed order in the Order Import Corrections folder. </BR>';
            l_body_footer :=
               '<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport for any questions. </BR>';
            l_body :=
                  l_body_header
               || l_body_detail
               || CHR (10)
               || l_body_footer;
           
            xxcus_misc_pkg.html_email (
               p_to              => P_EMAIL1,  -- 'HDSWCITECGAlerts@HDSupply.com',   
               p_from            => l_sender,
               p_text            => 'Order Contact Not exists Error',
               p_subject         => l_subject,
               p_html            => l_body,
               p_smtp_hostname   => l_host,
               p_smtp_portnum    => l_hostport);
		WHEN OTHERS THEN
		NULL;
	    END;
		
    END LOOP;

	   fnd_file.put_line(fnd_file.log,'Orders Import Return Req_id - ' || l_import_id);
    END;
  ELSE
   v_rec_cnt := 0;
       dbms_output.put_line('No file to process');
       fnd_file.put_line(fnd_file.log,'Orders Import RETURN REQ_ID - '||l_req_id);
	   RETCODE:='1';
	   dbms_output.put_line('Returncode '||RETCODE);
  END IF;
 EXCEPTION  
  WHEN OTHERS THEN
  RETCODE:='2';
   dbms_output.put_line('Returncode '||RETCODE);
END PROG_850_IB_UC4_WRAPPER;


/******************************************************************************
      $Header PROG_855_OB_UC4_WRAPPER.prc $
      Module Name: PROG_855_OB_UC4_WRAPPER.prc
      PURPOSE:   This XXWC_OM_EDI_INTEGRATE_PKG package is used to automate
	             the EDI process through UC4. Customer Integration - Order 
				 Import Modifications for ecomm

      REVISIONS:
      Ver        Date         Author              Description 
      ---------  ----------   ---------------     --------------------------
      1.0        12-04-2018  Pattabhi Avula       TMS#20180308-00274
	                                              Initial version
*******************************************************************************/
PROCEDURE PROG_855_OB_UC4_WRAPPER(ERRBUFF OUT VARCHAR2, RETCODE OUT VARCHAR2,P_DAYS NUMBER, P_PATH VARCHAR2)
 IS
  v_rec_cnt             NUMBER;
  v_filename            VARCHAR2(100);
  l_user_id             FND_USER.USER_ID%TYPE; --:=30437; --44422;
  l_responsibility_id   FND_RESPONSIBILITY_VL.RESPONSIBILITY_ID%TYPE; --:=50922;
  l_resp_application_id FND_RESPONSIBILITY_VL.APPLICATION_ID%TYPE; --:=175;
  l_org_id              NUMBER:=162;
  l_req_id              NUMBER NULL;
  lc_phase              VARCHAR2(50);
  lc_status             VARCHAR2(50);
  lc_dev_phase          VARCHAR2(50);
  lc_dev_status         VARCHAR2(50);
  lc_message            VARCHAR2(50);
  l_req_return_status   BOOLEAN; 
  -- v_file_path           VARCHAR2(500):=FND_PROFILE.VALUE('ECE_OUT_FILE_PATH');
  
  CURSOR CUR_855_POS 
    IS
     SELECT akh.order_number, akh.cust_po_number 
       FROM apps.oe_header_acks akh
      WHERE akh.acknowledgment_flag IS NULL 
        AND TRUNC(akh.last_update_date) >= TRUNC(SYSDATE)- NVL(P_DAYS,1);

BEGIN

    v_filename  := NULL;	
	
	  ----------------------------------------------------------------------------------
      -- Derive FND_GLOBAL.APPS_INITIALIZE Variables
      ----------------------------------------------------------------------------------
	  -- User details             
	                  
	  BEGIN           
         SELECT user_id		        
           INTO l_user_id		        
           FROM fnd_user
		  WHERE user_name='XXWC_INT_SALESFULFILLMENT';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_user_id := NULL;
      END;
	  
	  -- Responsibility_id and application_id
	  
	  BEGIN
         SELECT responsibility_id,
		        application_id
           INTO l_responsibility_id,
		        l_resp_application_id
           FROM FND_RESPONSIBILITY_VL 
		  WHERE RESPONSIBILITY_NAME='HDS EDI Super User - WC';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_responsibility_id := NULL;
			l_resp_application_id:=NULL;
      END;
	  
	  fnd_global.apps_initialize (l_user_id,
                                  l_responsibility_id,
                                  l_resp_application_id);
								  
      mo_global.set_policy_context ('S', l_org_id);
      fnd_request.set_org_id (l_org_id);


    FOR I IN CUR_855_POS
	 LOOP
        v_filename :='855_'||I.cust_po_number||'.dat';
         fnd_file.put_line(fnd_file.log,'v_file_name - ' || v_filename);
         dbms_output.put_line('File name is '||  v_filename);
   l_req_id := fnd_request.submit_request (
                  application    => 'EC',
                  program        => 'ECEPOAO',
                  description    => NULL,
                  start_time     => SYSDATE,
                  sub_request    => FALSE,				 
                  argument1      => P_PATH,
                  argument2      => v_filename,
				  argument3      => 'POAO',
				  argument4      => 0,
				  argument5      => NULL,
				  argument6      => NULL,
				  argument7      => NULL,
				  argument8      => NULL,
				  argument9      => I.order_number,
				  argument10     => I.order_number,
				  argument11     => NULL,
				  argument12     => NULL,
				  argument13     => NULL,
				  argument14     => NULL);
	  COMMIT;
	  IF l_req_id > 0 THEN
	   dbms_output.put_line('Request_id - '||l_req_id);
       LOOP
	  
	           l_req_return_status :=
            fnd_concurrent.wait_for_request (request_id      => l_req_id
                                            ,interval        => 5 --interval Number of seconds to wait between checks
                                            ,max_wait        => 5 --Maximum number of seconds to wait for the request completion
                                            ,phase           => lc_phase
                                            ,status          => lc_status
                                            ,dev_phase       => lc_dev_phase
                                            ,dev_status      => lc_dev_status
                                            ,message         => lc_message
                                            );						
          EXIT
        WHEN UPPER (lc_phase) = 'COMPLETED' OR UPPER (lc_status) IN ('CANCELLED', 'ERROR', 'TERMINATED');
		
      END LOOP;
        --
        --
        IF UPPER (lc_phase) = 'COMPLETED' AND UPPER (lc_status)  IN ( 'ERROR','WARNING') THEN
		RETCODE:='2';
		dbms_output.put_line('Returncode '||RETCODE);
           fnd_file.put_line(fnd_file.log,'The Program completed in error. Oracle request id: '||l_req_id ||' '||SQLERRM);
        ELSIF UPPER (lc_phase) = 'COMPLETED' AND UPPER (lc_status) = 'NORMAL' THEN
		
           fnd_file.put_line(fnd_file.log, 'The Program request successful for request id: ' || l_req_id);
	    END IF;	

   END IF;		
    END LOOP;

	   fnd_file.put_line(fnd_file.log,'Return Req_id - ' || l_req_id);
	   
 EXCEPTION
  WHEN OTHERS THEN
  RETCODE:='2';
   dbms_output.put_line('Returncode '||RETCODE);
END PROG_855_OB_UC4_WRAPPER;

/******************************************************************************
      $Header PROG_810_OB_UC4_WRAPPER.prc $
      Module Name: PROG_810_OB_UC4_WRAPPER.prc
      PURPOSE:   This XXWC_OM_EDI_INTEGRATE_PKG package is used to automate
	             the EDI process through UC4. Customer Integration - Order 
				 Import Modifications for ecomm

      REVISIONS:
      Ver        Date         Author              Description 
      ---------  ----------   ---------------     --------------------------
      1.0        12-04-2018  Pattabhi Avula       TMS#20180308-00274
	                                              Initial version
*******************************************************************************/
PROCEDURE PROG_810_OB_UC4_WRAPPER(ERRBUFF OUT VARCHAR2, RETCODE OUT VARCHAR2,P_DAYS NUMBER, P_PATH VARCHAR2)
 IS
  v_rec_cnt             NUMBER;
  v_filename            VARCHAR2(100);
  l_user_id             FND_USER.USER_ID%TYPE; --:=30437; --44422;
  l_responsibility_id   FND_RESPONSIBILITY_VL.RESPONSIBILITY_ID%TYPE; --:=50922;
  l_resp_application_id FND_RESPONSIBILITY_VL.APPLICATION_ID%TYPE; --:=175;
  l_org_id              NUMBER:=162;
  l_req_id              NUMBER NULL;
  lc_phase              VARCHAR2(50);
  lc_status             VARCHAR2(50);
  lc_dev_phase          VARCHAR2(50);
  lc_dev_status         VARCHAR2(50);
  lc_message            VARCHAR2(50);
  l_req_return_status   BOOLEAN; 
  -- v_file_path           VARCHAR2(500):=FND_PROFILE.VALUE('ECE_OUT_FILE_PATH');
  
  CURSOR CUR_810_INVS 
    IS
     SELECT eih.transaction_number,
            eih.invoice_name,  -- invoice
            rcta.trx_date,
            eih.transaction_date
       FROM apps.ece_ino_header_v eih,
            apps.ra_customer_trx_all rcta
      WHERE 1 = 1
        AND eih.transaction_number = rcta.trx_number
        AND NVL(rcta.edi_processed_flag,'N') = 'N'
        AND rcta.edi_processed_status IS NULL
        AND eih.communication_method = 'EDI'
        AND eih.tp_document_id = 'INO'
        AND eih.document_type = 'INV'
        AND TRUNC(rcta.trx_date)>=TRUNC(SYSDATE)-NVL(P_DAYS,1);

BEGIN

    v_filename  := NULL;	
	
	  ----------------------------------------------------------------------------------
      -- Derive FND_GLOBAL.APPS_INITIALIZE Variables
      ----------------------------------------------------------------------------------
	  -- User details             
	                  
	  BEGIN           
         SELECT user_id		        
           INTO l_user_id		        
           FROM fnd_user
		  WHERE user_name='XXWC_INT_SALESFULFILLMENT';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_user_id := NULL;
      END;
	  
	  -- Responsibility_id and application_id
	  
	  BEGIN
         SELECT responsibility_id,
		        application_id
           INTO l_responsibility_id,
		        l_resp_application_id
           FROM FND_RESPONSIBILITY_VL 
		  WHERE RESPONSIBILITY_NAME='HDS EDI Super User - WC';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_responsibility_id := NULL;
			l_resp_application_id:=NULL;
      END;
	  
	  fnd_global.apps_initialize (l_user_id,
                                  l_responsibility_id,
                                  l_resp_application_id);
								  
      mo_global.set_policy_context ('S', l_org_id);
      fnd_request.set_org_id (l_org_id);


    FOR I IN CUR_810_INVS
	 LOOP
        v_filename :='810_'||I.transaction_number||'.dat';
         fnd_file.put_line(fnd_file.log,'v_file_name - ' || v_filename);
         dbms_output.put_line('File name is '||  v_filename);
   l_req_id := fnd_request.submit_request (
                  application    => 'EC',
                  program        => 'ECEINO',
                  description    => NULL,
                  start_time     => SYSDATE,
                  sub_request    => FALSE,				 
                  argument1      => P_PATH,                 -- Output File Path
                  argument2      => v_filename,             -- Output File Name
				  argument3      => NULL,                   -- Creation Date From
				  argument4      => NULL,                   -- Creation Date To
				  argument5      => NULL,                   -- Customer Name
				  argument6      => NULL,                   -- Customer Site Name
				  argument7      => 'INV',                  -- Document Type
				  argument8      => I.transaction_number,   -- Transaction Number
				  argument9      => 0);                     -- Debug Mode for INO 				  
	  COMMIT;
	  IF l_req_id > 0 THEN
	   dbms_output.put_line('Request_id - '||l_req_id);
       LOOP
	  
	           l_req_return_status :=
            fnd_concurrent.wait_for_request (request_id      => l_req_id
                                            ,interval        => 3 --interval Number of seconds to wait between checks
                                            ,max_wait        => 3 --Maximum number of seconds to wait for the request completion
                                            ,phase           => lc_phase
                                            ,status          => lc_status
                                            ,dev_phase       => lc_dev_phase
                                            ,dev_status      => lc_dev_status
                                            ,message         => lc_message
                                            );						
          EXIT
        WHEN UPPER (lc_phase) = 'COMPLETED' OR UPPER (lc_status) IN ('CANCELLED', 'ERROR', 'TERMINATED');
		
      END LOOP;
        --
        --
        IF UPPER (lc_phase) = 'COMPLETED' AND UPPER (lc_status)  IN ( 'ERROR','WARNING') THEN
		RETCODE:='2';
		dbms_output.put_line('Returncode '||RETCODE);
           fnd_file.put_line(fnd_file.log,'The Program completed in error. Oracle request id: '||l_req_id ||' '||SQLERRM);
        ELSIF UPPER (lc_phase) = 'COMPLETED' AND UPPER (lc_status) = 'NORMAL' THEN
		
           fnd_file.put_line(fnd_file.log, 'The Program request successful for request id: ' || l_req_id);
	    END IF;	

   END IF;		
    END LOOP;

	   fnd_file.put_line(fnd_file.log,'Return Req_id - ' || l_req_id);
	   
 EXCEPTION
  WHEN OTHERS THEN
  RETCODE:='2';
   dbms_output.put_line('Returncode '||RETCODE);
END PROG_810_OB_UC4_WRAPPER;


END XXWC_OM_EDI_INTEGRATE_PKG; 
/