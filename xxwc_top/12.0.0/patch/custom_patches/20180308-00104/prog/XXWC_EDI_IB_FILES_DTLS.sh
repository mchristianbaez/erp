# ------------------------------------------------------------------------
# ------------------------------------------------------------------------
#!/bin/sh
# ------------------------------------------------------------------------
# copyright 2012, All Rights Reserved, 
# Version 1.0
#
# Name        : XXWC_EDI_IB_FILES_DTLS.sh 
# Date Written: 
# Author      : Pattabhi Avula
#
# Description : To a load XXWC_EDI_IB_FILES_DTLS_TBL External table
#
# Usasage     : ./XXWC_EDI_IB_FILES_DTLS.sh
#
# Modification History:
#
# When         Who        Did what
# --------------------------------------------------------------------------
# 12-Apr-2018  Pattabhi    Created for TMS#20180308-00104
# --------------------------------------------------------------------------
cd /xx_iface/ebsprd/inbound/EDI
/bin/ls -l 850*.dat /xx_iface/ebsprd/inbound/EDI

# This file is called by the XXWC_EDI_IB_FILES_EXTRNL_TBL external table to
# show the files in the directory above.

# -- Create table
# TMS#20180308-00104 - Customer Integration - Order Import Modifications
# CREATE TABLE XXWC.XXWC_EDI_IB_FILES_EXTRNL_TBL
#(
 # FILE_PERMISSIONS  VARCHAR2(50 BYTE),
  #FILE_TYPE         VARCHAR2(64 BYTE),
  #FILE_OWNER        VARCHAR2(64 BYTE),
  #FILE_GROUP        VARCHAR2(64 BYTE),
  #FILE_SIZE         VARCHAR2(64 BYTE),
  #FILE_MONTH        VARCHAR2(64 BYTE),
  #FILE_DAY          VARCHAR2(64 BYTE),
  #FILE_TIME         VARCHAR2(64 BYTE),
  #FILE_NAME         VARCHAR2(612 BYTE)
#)
#ORGANIZATION EXTERNAL
#( TYPE ORACLE_LOADER
  #DEFAULT DIRECTORY EDI_INBOUND
  #ACCESS PARAMETERS 
  #( 
    #RECORDS DELIMITED BY NEWLINE
    #LOAD WHEN file_permissions != 'total'
    #PREPROCESSOR EDI_INBOUND: 'list_files.sh'
    #NOBADFILE
    #NODISCARDFILE
    #NOLOGFILE
    #FIELDS TERMINATED BY WHITESPACE
  #)
  #  LOCATION (EDI_INBOUND:'list_files_dummy_source.txt')
  #)
  #REJECT LIMIT UNLIMITED;