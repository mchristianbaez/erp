CREATE OR REPLACE PACKAGE BODY APPS.XXWC_ISR_FLIP_DATE_ADI_PKG
/*************************************************************************
  $Header XXWC_ISR_FLIP_DATE_ADI_PKG.pkb $
  Module Name: XXWC_ISR_FLIP_DATE_ADI_PKG

  PURPOSE: To Update the Flip Date in ISR Table through WEB ADI

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        18-Jan-2016  Manjula Chellappan    Initial Version TMS # 20160104-00014

**************************************************************************/
IS
   g_dflt_email   fnd_user.email_address%TYPE
                     := 'HDSOracleDevelopers@hdsupply.com';

   PROCEDURE Load_flip_date (p_org         IN VARCHAR2,
                             p_item_number IN VARCHAR2,
							 p_flip_date   IN VARCHAR2)
/*************************************************************************
  $Header XXWC_ISR_FLIP_DATE_ADI_PKG.pkb $
  Module Name: XXWC_ISR_FLIP_DATE_ADI_PKG.Load_flip_date

  PURPOSE: To Load the Flip Date to the staging Table through WEB ADI

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        18-Jan-2016  Manjula Chellappan    Initial Version TMS # 20160104-00014

**************************************************************************/
   IS
      l_sec                     VARCHAR2 (100);
      l_exception               EXCEPTION;      
      l_org                     VARCHAR2(3);
	  l_organization_id         NUMBER;
	  l_inventory_item_id       NUMBER;
	  l_flip_date               DATE;
	  l_error_msg               VARCHAR2(2000);
      	  	  
   BEGIN  
   
      fnd_message.clear;             
		 
		 l_sec := 'Validate Branch';
		 
		 BEGIN
		 
             SELECT organization_code, organization_id
			   INTO l_org , l_organization_id
			   FROM mtl_parameters 
			  WHERE organization_code = LPAD(p_org,3,'0') ;
 
		 EXCEPTION WHEN OTHERS THEN
		 
			l_org := NULL;
			
			l_error_msg :=
                  l_error_msg || 'Invalid Org ' || CHR (10) || SQLERRM || CHR (10);
							 
		 END ; 
		 		 
		 l_sec := 'Validate Item';
		 
		 BEGIN
		 
             SELECT inventory_item_id			 
			   INTO l_inventory_item_id
			   FROM mtl_system_items_b
			  WHERE organization_id = l_organization_id
			    AND segment1 = p_item_number ;
 
		 EXCEPTION WHEN OTHERS THEN
		 
			l_org := NULL;
			l_error_msg := l_error_msg||' Invalid Item - '||SQLERRM;
							 
		 END ;

        l_sec := 'Validate Date';
		
		IF p_flip_date = 'UPN' THEN
		
		l_flip_date := NULL;
		
		ELSIF p_flip_date IS NULL THEN
		l_error_msg := l_error_msg||' Flip Date Cannot be NULL ';
		
        ELSE 
        l_flip_date := to_date(p_flip_date, 'MM/DD/YY') ;
		
		END IF;
		
		
		IF l_error_msg IS NOT NULL THEN
		
		RAISE l_exception;
		
		ELSE
				


         BEGIN
		 l_sec :=
            'Delete Record from XXWC.XXWC_ISR_FLIP_DATE_STG_TBL';
			
			DELETE FROM XXWC.XXWC_ISR_FLIP_DATE_STG_TBL
			WHERE inventory_item_id = l_inventory_item_id
			AND organization_id = l_organization_id;
		    
         l_sec :=
            'Load data into staging table XXWC.XXWC_ISR_FLIP_DATE_STG_TBL';
			
            INSERT INTO XXWC.XXWC_ISR_FLIP_DATE_STG_TBL    (organization_id,
                                                            inventory_item_id,
                                                            org,
                                                            item_number,
                                                            flip_date,
                                                            creation_date)
                 VALUES (l_organization_id,
                         l_inventory_item_id,
                         l_org,
                         p_item_number,
                         l_flip_date,
                         sysdate);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_error_msg :=
                  l_error_msg || 'Failed to Insert ' || CHR (10) || SQLERRM || CHR (10);

               RAISE l_exception;
         END;

         COMMIT;
		 
		 END IF;
		 

   EXCEPTION
      WHEN l_exception
      THEN
         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_error_msg);
		 
      WHEN OTHERS
      THEN
	  
         l_error_msg :=
            l_error_msg || l_sec || CHR (10) || SQLERRM || CHR (10);
         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_error_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_ISR_FLIP_DATE_ADI_PKG.Load_flip_date',
            p_calling             => l_sec,
            p_request_id          => NULL,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'INV');			
   
   END Load_flip_date;
   

 PROCEDURE Update_flip_date 
/*************************************************************************
  $Header XXWC_ISR_FLIP_DATE_ADI_PKG.pkb $
  Module Name: XXWC_ISR_FLIP_DATE_ADI_PKG.Update_flip_date

  PURPOSE: To Update the Flip Date in the ISR Table through the WEB ADI

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        18-Jan-2016  Manjula Chellappan    Initial Version TMS # 20160104-00014

**************************************************************************/								
	IS
l_sec VARCHAR2(100);

	BEGIN
	
	l_sec := 'Update Flip Date';
	
	 UPDATE XXWC.XXWC_ISR_DETAILS_ALL isr 
      SET flip_date =
	      (SELECT flip_date 
		     FROM XXWC.XXWC_ISR_FLIP_DATE_STG_TBL
			WHERE organization_id= isr.organization_id
              AND inventory_item_id = isr.inventory_item_id)
	WHERE EXISTS
	      (SELECT 1
		     FROM XXWC.XXWC_ISR_FLIP_DATE_STG_TBL
			WHERE organization_id= isr.organization_id
              AND inventory_item_id = isr.inventory_item_id);

	COMMIT;
	
		l_sec := 'Truncate Table XXWC.XXWC_ISR_FLIP_DATE_STG_TBL ';
		 
		 EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_ISR_FLIP_DATE_STG_TBL ';
		 		
 EXCEPTION WHEN OTHERS
      THEN
	     fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_sec ||CHR (10)||SUBSTR(SQLERRM,1,240));
		 
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_ISR_FLIP_DATE_ADI_PKG.Update_flip_date',
            p_calling             => l_sec,
            p_request_id          => NULL,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => 'OTHERS Exception',
            p_distribution_list   => g_dflt_email,
            p_module              => 'INV');	
			
	END Update_flip_date;
 								
END XXWC_ISR_FLIP_DATE_ADI_PKG;
/
