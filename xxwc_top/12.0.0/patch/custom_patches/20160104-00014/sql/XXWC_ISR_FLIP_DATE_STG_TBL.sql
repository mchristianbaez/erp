/*************************************************************************
  $Header XXWC_ISR_FLIP_DATE_STG_TBL.sql $

  PURPOSE: Staging table to store the Flip dates to be be updated through WEB ADI

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        18-Jan-2016  Manjula Chellappan    Initial Version TMS # 20160104-00014

**************************************************************************/
 CREATE TABLE XXWC.XXWC_ISR_FLIP_DATE_STG_TBL
  ( ORGANIZATION_ID     NUMBER NOT NULL, 
    INVENTORY_ITEM_ID   NUMBER NOT NULL,
    ORG         	    VARCHAR2(10) NOT NULL,	
    ITEM_NUMBER       	VARCHAR2(30) NOT NULL,
	FLIP_DATE           DATE ,
    CREATION_DATE       DATE NOT NULL    
  );  
	
  CREATE UNIQUE INDEX XXWC.XXWC_ISR_FLIP_DATE_STG_TBL_U1 ON XXWC.XXWC_ISR_FLIP_DATE_STG_TBL(organization_id,inventory_item_id);
       	