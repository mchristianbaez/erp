CREATE OR REPLACE PACKAGE APPS.XXWC_ISR_FLIP_DATE_ADI_PKG
/*************************************************************************
  $Header XXWC_ISR_FLIP_DATE_ADI_PKG.pks $
  Module Name: XXWC_ISR_FLIP_DATE_ADI_PKG

  PURPOSE: To Update the Flip Date in ISR Table through WEB ADI

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        18-Jan-2016  Manjula Chellappan    Initial Version TMS # 20160104-00014

**************************************************************************/
AS
   PROCEDURE Load_flip_date (p_org           IN VARCHAR2,
                             p_item_number   IN VARCHAR2,
                             p_flip_date     IN VARCHAR2);

   PROCEDURE Update_flip_date;
END XXWC_ISR_FLIP_DATE_ADI_PKG;
/