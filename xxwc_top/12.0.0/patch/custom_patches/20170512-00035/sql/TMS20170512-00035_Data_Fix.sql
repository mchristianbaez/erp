 /****************************************************************************************************
  Table: TMS20170512-00035_Data_Fix.sql     
  Description: batch to insert  category_ids to XXWC.XXWC_MTL_INV_CATE_UPDATE_LOG load 3
  HISTORY
  ======================================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ---------------------------------------------------------------------------
  1.0     17-May-2017        P.Vamshidhar    Initial version TMS#20170512-00035  
                                             batch to insert  category_ids to XXWC.XXWC_MTL_INV_CATE_UPDATE_LOG load 3
  ***********************************************************************************************************************/
SET SERVEROUT ON
  
BEGIN
   INSERT INTO XXWC.XXWC_MTL_INV_CATE_UPDATE_LOG (CATEGORY_ID,
                                                  EXPN_ACCT,
                                                  PROCESS_STATUS,
                                                  CREATION_DATE)
      SELECT DISTINCT mic.category_id,
                      mcv.attribute8,
                      'NEW',
                      SYSDATE
        FROM apps.MTL_SYSTEM_ITEMS_B MIB,
             apps.mtl_parameters orgs,
             apps.MTL_ITEM_CATEGORIES_V mic,
             apps.FND_USER FUSE,
             apps.MTL_CATEGORIES_V mcv,
             apps.gl_code_combinations_kfv COGS,
             apps.gl_code_combinations_kfv SALES,
             apps.gl_code_combinations_kfv EXPENSE
       WHERE     mib.organization_id = orgs.organization_id
             AND mib.inventory_item_id = mic.inventory_item_id
             AND mib.organization_id = mic.organization_id
             AND mib.created_by = fuse.user_id(+)
             -- AND mib.item_type = 'INTANGIBLE'
             AND mic.category_concat_segs = MCV.CATEGORY_CONCAT_SEGS
             AND mcv.structure_name = 'Item Categories'
             AND mic.category_set_name = 'Inventory Category'
             AND orgs.master_organization_id = 222
             AND orgs.organization_code NOT LIKE ('9%')
             AND mib.COST_OF_SALES_ACCOUNT = cogs.code_combination_id
             AND mib.SALES_ACCOUNT = sales.code_combination_id
             AND mib.EXPENSE_ACCOUNT = expense.code_combination_id
             AND mib.inventory_item_status_code <> 'Inactive'
             AND Expense.segment4 <> mcv.attribute8
             AND mcv.category_id IN (4141,
                                     3685,
                                     3722,
                                     3796,
                                     3806,
                                     3715,
                                     2638,
                                     4082,
                                     3541,
                                     2883,
                                     2476,
                                     23707,
                                     4129,
                                     2759,
                                     3723,
                                     2479,
                                     2687,
                                     2796,
                                     2921,
                                     2808,
                                     4021,
                                     3598,
                                     3826,
                                     3790,
                                     4058,
                                     2527,
                                     4171,
                                     4152,
                                     46705,
                                     4182,
                                     3828,
                                     3810,
                                     3515,
                                     3760,
                                     2689,
                                     2511,
                                     4036,
                                     2751,
                                     3753,
                                     3720,
                                     3452,
                                     2766,
                                     3824,
                                     2503,
                                     2478,
                                     2747,
                                     2933,
                                     23763,
                                     4131,
                                     3904,
                                     3713,
                                     3589,
                                     2676,
                                     3675,
                                     4149,
                                     2636,
                                     4132,
                                     3893,
                                     3581,
                                     2497,
                                     4054,
                                     46704,
                                     2752,
                                     3580,
                                     4088,
                                     23722,
                                     2811,
                                     3762,
                                     2558,
                                     4218,
                                     3748,
                                     18699,
                                     4160,
                                     4022,
                                     2839,
                                     4140,
                                     3939,
                                     3553,
                                     2515,
                                     3630,
                                     2455,
                                     3996,
                                     4137,
                                     3747,
                                     2864,
                                     4056,
                                     3582,
                                     3769,
                                     4055,
                                     4279,
                                     2908,
                                     2507,
                                     2539,
                                     2516,
                                     53689,
                                     2494,
                                     4217,
                                     4169,
                                     2532,
                                     4142,
                                     3809,
                                     3689,
                                     2899);

   DBMS_OUTPUT.PUT_LINE ('Number of rows inserted ' || SQL%ROWCOUNT);

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error occured ' || SUBSTR (SQLERRM, 1, 250));
      ROLLBACK;
END;
/