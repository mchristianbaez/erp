/* Formatted on 04/20/2018 7:42:49 PM (QP5 v5.265.14096.38000) */
/*
 TMS: 20180420-00040 Data fix 
 Date: 04/20/2018
 */
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.PUT_LINE ('Before Update');

  UPDATE MTL_CC_ENTRIES_INTERFACE
   SET ACTION_CODE          = 14,
       LAST_UPDATE_DATE     = SYSDATE,
       LAST_UPDATED_BY      = 28474,
       CREATED_BY           = 28474,
       employee_id          = 28474,
       COUNT_DATE           = SYSDATE,
       COUNT_UOM            = 'EA',
       PRIMARY_UOM_QUANTITY = 1,
       COUNT_QUANTITY       = 1,
       LOCK_FLAG            = 2,
       PROCESS_FLAG         = 1,
       PROCESS_MODE         = 3,
       VALID_FLAG           = 1,
       status_flag          = 4,
       DELETE_FLAG          = 2
 WHERE CC_ENTRY_INTERFACE_ID IN (9467907, 9467908);

   DBMS_OUTPUT.PUT_LINE ('Number Rows Updated ' || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.PUT_LINE ('After Update');
EXCEPTION
WHEN OTHERS THEN
DBMS_OUTPUT.PUT_LINE('Error occurred '||substr(sqlerrm,1,250));
END;
/