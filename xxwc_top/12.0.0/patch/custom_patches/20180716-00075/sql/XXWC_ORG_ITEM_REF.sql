  /********************************************************************************
  FILE NAME: XXWC_ORG_ITEM_REF.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: Inventory Org Item Reference table
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)        DESCRIPTION
  ------- -----------   ---------------- ----------------------------------------------------
  1.0     07/05/2018    Naveen Kalidindi Initial Version.20180716-00075
  *******************************************************************************************/
WHENEVER SQLERROR CONTINUE
-- DROP TABLE
DROP TABLE XXWC.XXWC_ORG_ITEM_REF;

-- CREATE TABLE
CREATE TABLE XXWC.XXWC_ORG_ITEM_REF
(
  AHH_ATTRIBUTE  VARCHAR2(100),
  AHH_ATTR_VALUE VARCHAR2(100),
  EBS_VALUE      VARCHAR2(100)
);
-- CREATE/RECREATE INDEXES 
CREATE INDEX XXWC.XXWC_ORG_ITEM_REF_N1 ON XXWC.XXWC_ORG_ITEM_REF (AHH_ATTRIBUTE, AHH_ATTR_VALUE);