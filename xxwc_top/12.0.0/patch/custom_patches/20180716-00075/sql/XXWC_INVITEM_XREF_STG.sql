  /********************************************************************************
  FILE NAME: XXWC_INVITEM_XREF_STG.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: INVITEM XREF Staging Reference table
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)        DESCRIPTION
  ------- -----------   ---------------- ----------------------------------------------------
  1.0     07/05/2018    Naveen Kalidindi Initial Version. TMS# 20180716-00075.
  *******************************************************************************************/
WHENEVER SQLERROR CONTINUE
-- DROP TABLE
DROP TABLE XXWC.XXWC_INVITEM_XREF_STG;


-- CREATE TABLE
CREATE TABLE XXWC.XXWC_INVITEM_XREF_STG
(
  PARTNUMBER        VARCHAR2(30),
  XREF_PART         VARCHAR2(200),
  UPC_FLAG          VARCHAR2(200),
  ERROR_MESSAGE     VARCHAR2(4000),
  STATUS            VARCHAR2(1),
  CROSS_REF_STATUS  VARCHAR2(1),
  CROSS_REF_MESSAGE VARCHAR2(2000)
);
-- Create/Recreate indexes 
CREATE INDEX XXWC.XXWC_INVITEM_XREF_STG_N1 ON XXWC.XXWC_INVITEM_XREF_STG (PARTNUMBER, XREF_PART);