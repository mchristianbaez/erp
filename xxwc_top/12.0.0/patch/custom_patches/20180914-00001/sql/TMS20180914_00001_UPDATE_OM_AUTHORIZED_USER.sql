/*************************************************************************
   *   $Header TMS20180914_00001_UPDATE_OM_AUTHORIZED_USER.sql $
   *   Module Name: TMS20180914_00001_UPDATE_OM_AUTHORIZED_USER.sql
   *
   *   PURPOSE:   This script is used to update profile option XXWC_OM_USER_AUTHORIZATION at user level to make the user as authorized user
   *
   *   REVISIONS:
   * Created By           TMS                 Date          Version  Comments
   * ===========         ===============      ===========    =======  ===========
   * Niraj K Ranjan      TMS#20180914-00001   18-SEP-2018    1.0      Initial
 *******************************************************************************/
 set serveroutput on size 1000000
Declare
   l_Result Boolean;
   l_user_id NUMBER; 
   le_exception EXCEPTION;
   l_err_msg VARCHAR2(2000);
   CURSOR cur_user
   IS
     SELECT NTID 
	 FROM XXWC.XXWC_OM_AUTHORIZED_USER_TBL;
BEGIN
    DBMS_OUTPUT.put_line ('Start Script for TMS#20180914-00001');
    FOR rec_user IN cur_user
	LOOP
	   BEGIN
	      l_user_id := null;
	      l_err_msg := NULL;
	       BEGIN
	          SELECT USER_ID 
		      INTO l_user_id
		      FROM FND_USER
		      WHERE USER_NAME = upper(rec_user.NTID);
	       EXCEPTION
	          WHEN NO_DATA_FOUND THEN
		         l_err_msg := 'User '||rec_user.NTID||' not found';
		         RAISE le_exception;
	       END;
          l_Result := fnd_profile.SAVE('XXWC_OM_USER_AUTHORIZATION', 'Y', 'USER',l_user_id);
          IF l_Result
          THEN
		     COMMIT;
			 DBMS_OUTPUT.put_line ('User Authorization is set to Y for user '||rec_user.NTID);
		  ELSE
             l_err_msg := 'Profile Not Updated for user '||rec_user.NTID;
			 RAISE le_exception;
          END IF;
	   EXCEPTION
	      WHEN le_exception THEN
		     DBMS_OUTPUT.put_line ('Error: '||l_err_msg);
			 ROLLBACK;
	   END;
	END LOOP;
	DBMS_OUTPUT.put_line ('End Script for TMS#20180914-00001');
EXCEPTION
   WHEN OTHERS THEN
      l_err_msg := SUBSTR(SQLERRM,1,2000);
	  DBMS_OUTPUT.put_line ('Error: '||l_err_msg);
	  ROLLBACK;
END;
/