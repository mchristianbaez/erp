CREATE OR REPLACE PACKAGE BODY APPS.XXWC_CREDIT_COPY_TRX_PKG
IS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXXWC_CREDIT_COPY_TRX_PKG.pkb $
   *   Module Name: XXWC_CREDIT_COPY_TRX_PKG.pkb
   *
   *   PURPOSE:   This package is used by the XXWC Credit and Rebill UI to submit the conc program
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman           Initial Version
   *   1.1        02/23/2012  Shankar Hariharan      Fix for issue 45. Pass the line number for tax differently
   *   1.2        04/21/2013  Shankar Hariharan       Added new procedure cons_trx
   *   1.3        01/08/2014  Maharajan Shunmugam     TMS# 20130909-00743
   *   1.4        04/21/2014  Maharajan Shunmugam     TMS# 20140402-00009 Copy attirbutes to Rebill source and Rebill CM
   *   1.5        06/06/2014  Maharajan Shunmugam     TMS# 20140124-00031 Update attributes in interface line and Fix for split invoice
   *   1.6        08/25/2014  Maharajan Shunmugam     TMS# 20140826-00040 Consolidatin program change for split invoice grouping
   *   1.7        09/17/2014  Maharajan Shunmugam     TMS# 20141001-00153 Canada Multi Org changes
   *   1.8        07/29/2015  Maharajan Shunmugam     TMS# 20150123-00197 AR - Customer Flag as indicator to auto apply credits
   *   1.9        02/12/2016  Neha Saini              TMS# 20151022-00140 AR - modify HDS Credit Rebill functionality
   * **********************************************************************************************************************************/

   /*************************************************************************
   *   Procedure : LOG_MSG
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Insert Trigger
   *   Parameter:
   *          IN
   *              p_debug_level    -- Debug Level
   *              p_mod_name       -- Module Name
   *              p_debug_msg      -- Debug Message
   * ************************************************************************/

   -- add debug message to log table and concurrent log file
   PROCEDURE LOG_MSG (p_debug_level   IN NUMBER,
                      p_mod_name      IN VARCHAR2,
                      p_debug_msg     IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      --IF (p_debug_level >= FND_LOG.G_CURRENT_RUNTIME_LEVEL)
      --THEN
      -- Call Oracle FND_LOG API to insert the debug message
      FND_LOG.STRING (p_debug_level, p_mod_name, P_DEBUG_MSG);
      -- END IF;

      fnd_file.put_line (fnd_file.LOG, P_DEBUG_MSG);
      COMMIT;
   END LOG_MSG;

   /*************************************************************************
   *   Procedure : Write_output
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Insert Trigger
   *   Parameter:
   *          IN
   *              p_debug_msg      -- Debug Message
   * ************************************************************************/

   --add message to concurrent output file
   PROCEDURE Write_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.output, p_debug_msg);
   END Write_output;

   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXXWC_CREDIT_COPY_TRX_PKG.pkb $
   *   Module Name: XXWC_CREDIT_COPY_TRX_PKG.pkb
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Insert Trigger
   *              from XXWC Credit and Rebill UI
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   * ***************************************************************************/
   PROCEDURE Insert_Row (
      PX_CREDIT_COPY_TRX_ID          IN OUT NOCOPY NUMBER,
      P_CUSTOMER_TRX_ID                            NUMBER,
      P_REV_CUSTOMER_TRX_ID                        NUMBER,
      P_NEW_CUSTOMER_TRX_ID                        NUMBER,
      P_CUST_ACCOUNT_ID                            NUMBER,
      P_REBILL_BILL_TO_SITE_USE_ID                 NUMBER,
      P_REBILL_SHIP_TO_SITE_USE_ID                 NUMBER,
      P_REASON_CODE                                VARCHAR2,
      P_LAST_UPDATE_DATE                           DATE,
      P_LAST_UPDATED_BY                            NUMBER,
      P_CREATION_DATE                              DATE,
      P_CREATED_BY                                 NUMBER,
      P_LAST_UPDATE_LOGIN                          NUMBER,
      P_OBJECT_VERSION_NUMBER                      NUMBER,
      P_PROCESS_STATUS                             VARCHAR2,
      P_ERROR_MESSAGE                              VARCHAR2,
      P_SELECT_TRX                                 VARCHAR2
   )
   IS
      l_mod_name   VARCHAR2 (100) := 'xxwc_credit_copy_trx_pkg.Insert_Row';

      CURSOR C1
      IS
         SELECT   XXWC_AR_CREDIT_COPY_TRX_S.NEXTVAL FROM sys.DUAL;
   BEGIN
      IF (PX_CREDIT_COPY_TRX_ID IS NULL)
         OR (PX_CREDIT_COPY_TRX_ID = FND_API.G_MISS_NUM)
      THEN
         OPEN C1;

         FETCH C1 INTO   PX_CREDIT_COPY_TRX_ID;

         CLOSE C1;
      END IF;

      LOG_MSG (g_LEVEL_STATEMENT,
               l_mod_name,
               'px_credit_copy_trx_id=' || px_credit_copy_trx_id);

      INSERT INTO XXWC_AR_CREDIT_COPY_TRX (CREDIT_COPY_TRX_ID,
                                           CUSTOMER_TRX_ID,
                                           REV_CUSTOMER_TRX_ID,
                                           NEW_CUSTOMER_TRX_ID,
                                           CUST_ACCOUNT_ID,
                                           REBILL_BILL_TO_SITE_USE_ID,
                                           REBILL_SHIP_TO_SITE_USE_ID,
                                           REASON_CODE,
                                           LAST_UPDATE_DATE,
                                           LAST_UPDATED_BY,
                                           CREATION_DATE,
                                           CREATED_BY,
                                           LAST_UPDATE_LOGIN,
                                           OBJECT_VERSION_NUMBER,
                                           PROCESS_STATUS,
                                           ERROR_MESSAGE,
                                           SELECT_TRX)
        VALUES   (PX_CREDIT_COPY_TRX_ID,
                  P_CUSTOMER_TRX_ID,
                  P_REV_CUSTOMER_TRX_ID,
                  P_NEW_CUSTOMER_TRX_ID,
                  P_CUST_ACCOUNT_ID,
                  P_REBILL_BILL_TO_SITE_USE_ID,
                  P_REBILL_SHIP_TO_SITE_USE_ID,
                  P_REASON_CODE,
                  P_LAST_UPDATE_DATE,
                  P_LAST_UPDATED_BY,
                  P_CREATION_DATE,
                  P_CREATED_BY,
                  P_LAST_UPDATE_LOGIN,
                  P_OBJECT_VERSION_NUMBER,
                  P_PROCESS_STATUS,
                  P_ERROR_MESSAGE,
                  P_SELECT_TRX);

      LOG_MSG (
         g_LEVEL_STATEMENT,
         l_mod_name,
         'sucessfully inserted for the px_credit_copy_trx_id='
         || px_credit_copy_trx_id
      );
   END Insert_Row;


   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXXWC_CREDIT_COPY_TRX_PKG.pkb $
   *   Module Name: XXWC_CREDIT_COPY_TRX_PKG.pkb
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Update Trigger
   *              from XXWC Credit and Rebill UI
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   * ***************************************************************************/
   PROCEDURE Update_Row (P_CREDIT_COPY_TRX_ID            NUMBER,
                         P_REBILL_BILL_TO_SITE_USE_ID    NUMBER,
                         P_REBILL_SHIP_TO_SITE_USE_ID    NUMBER,
                         P_REASON_CODE                   VARCHAR2,
                         P_LAST_UPDATE_DATE              DATE,
                         P_LAST_UPDATED_BY               NUMBER,
                         P_CREATION_DATE                 DATE,
                         P_CREATED_BY                    NUMBER,
                         P_LAST_UPDATE_LOGIN             NUMBER,
                         P_OBJECT_VERSION_NUMBER         NUMBER,
                         P_PROCESS_STATUS                VARCHAR2,
                         P_ERROR_MESSAGE                 VARCHAR2,
                         P_SELECT_TRX                    VARCHAR2)
   IS
      l_mod_name   VARCHAR2 (100) := 'xxwc_credit_copy_trx_pkg.Update_Row';
   BEGIN
      LOG_MSG (g_LEVEL_STATEMENT,
               l_mod_name,
               ' p_credit_copy_trx_id=' || p_credit_copy_trx_id);

      UPDATE   XXWC_AR_CREDIT_COPY_TRX
         SET   REBILL_BILL_TO_SITE_USE_ID =
                  DECODE (P_REBILL_BILL_TO_SITE_USE_ID,
                          FND_API.G_MISS_NUM, REBILL_BILL_TO_SITE_USE_ID,
                          P_REBILL_BILL_TO_SITE_USE_ID),
               REBILL_SHIP_TO_SITE_USE_ID =
                  DECODE (P_REBILL_SHIP_TO_SITE_USE_ID,
                          FND_API.G_MISS_NUM, REBILL_SHIP_TO_SITE_USE_ID,
                          P_REBILL_SHIP_TO_SITE_USE_ID),
               REASON_CODE =
                  DECODE (P_REASON_CODE,
                          FND_API.G_MISS_CHAR, REASON_CODE,
                          P_REASON_CODE),
               LAST_UPDATE_DATE =
                  DECODE (P_LAST_UPDATE_DATE,
                          FND_API.G_MISS_DATE, LAST_UPDATE_DATE,
                          P_LAST_UPDATE_DATE),
               LAST_UPDATED_BY =
                  DECODE (P_LAST_UPDATED_BY,
                          FND_API.G_MISS_NUM, LAST_UPDATED_BY,
                          P_LAST_UPDATED_BY),
               CREATION_DATE =
                  DECODE (P_CREATION_DATE,
                          FND_API.G_MISS_DATE, CREATION_DATE,
                          P_CREATION_DATE),
               CREATED_BY =
                  DECODE (P_CREATED_BY,
                          FND_API.G_MISS_NUM, CREATED_BY,
                          P_CREATED_BY),
               LAST_UPDATE_LOGIN =
                  DECODE (P_LAST_UPDATE_LOGIN,
                          FND_API.G_MISS_NUM, LAST_UPDATE_LOGIN,
                          P_LAST_UPDATE_LOGIN),
               OBJECT_VERSION_NUMBER =
                  DECODE (P_OBJECT_VERSION_NUMBER,
                          FND_API.G_MISS_NUM, OBJECT_VERSION_NUMBER,
                          P_OBJECT_VERSION_NUMBER),
               PROCESS_STATUS =
                  DECODE (P_PROCESS_STATUS,
                          FND_API.G_MISS_CHAR, PROCESS_STATUS,
                          P_PROCESS_STATUS),
               ERROR_MESSAGE =
                  DECODE (P_ERROR_MESSAGE,
                          FND_API.G_MISS_CHAR, ERROR_MESSAGE,
                          P_ERROR_MESSAGE),
               SELECT_TRX =
                  DECODE (P_SELECT_TRX,
                          FND_API.G_MISS_CHAR, SELECT_TRX,
                          P_SELECT_TRX)
       WHERE   CREDIT_COPY_TRX_ID = P_CREDIT_COPY_TRX_ID;

      LOG_MSG (
         g_LEVEL_STATEMENT,
         l_mod_name,
         'sucessfully updated for the p_credit_copy_trx_id='
         || p_credit_copy_trx_id
      );
   EXCEPTION
      WHEN OTHERS
      THEN
         LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, 'error msg=' || SQLERRM);
   END Update_Row;

   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXXWC_CREDIT_COPY_TRX_PKG.pkb $
   *   Module Name: XXWC_CREDIT_COPY_TRX_PKG.pkb
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Lock Trigger
   *              from XXWC Credit and Rebill UI
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   * ***************************************************************************/

   PROCEDURE Lock_Row (P_CREDIT_COPY_TRX_ID       NUMBER,
                       p_OBJECT_VERSION_NUMBER    NUMBER)
   IS
      CURSOR c
      IS
             SELECT   OBJECT_VERSION_NUMBER
               FROM   apps.XXWC_AR_CREDIT_COPY_TRX
              WHERE   CREDIT_COPY_TRX_ID = P_CREDIT_COPY_TRX_ID
         FOR UPDATE   OF CREDIT_COPY_TRX_ID NOWAIT;

      recinfo   c%ROWTYPE;
   BEGIN
      OPEN c;

      FETCH c INTO   recinfo;

      IF (c%NOTFOUND)
      THEN
         CLOSE c;

         fnd_message.set_name ('FND', 'FORM_RECORD_DELETED');
         app_exception.raise_exception;
      END IF;

      CLOSE c;

      IF (recinfo.OBJECT_VERSION_NUMBER = P_OBJECT_VERSION_NUMBER)
      THEN
         NULL;
      ELSE
         fnd_message.set_name ('FND', 'FORM_RECORD_CHANGED');
         app_exception.raise_exception;
      END IF;
   END Lock_Row;

   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXXWC_CREDIT_COPY_TRX_PKG.pkb $
   *   Module Name: XXWC_CREDIT_COPY_TRX_PKG.pkb
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Delete Trigger
   *              from XXWC Credit and Rebill UI
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   *   1.1        09/17/2014  Maharajan Shunmugam     TMS# 20141001-00153 Canada Multi Org changes
   * ***************************************************************************/


   PROCEDURE Delete_Row (P_CREDIT_COPY_TRX_ID NUMBER)
   IS
   BEGIN
      DELETE FROM   XXWC_AR_CREDIT_COPY_TRX
            WHERE   CREDIT_COPY_TRX_ID = P_CREDIT_COPY_TRX_ID;
   END Delete_Row;


   PROCEDURE reverse_trx (p_customer_trx_id       IN            NUMBER,
                          x_rev_customer_trx_id      OUT NOCOPY NUMBER,
                          x_return_status            OUT NOCOPY VARCHAR2,
                          x_error_msg                OUT NOCOPY VARCHAR2,
                          x_error_count              OUT NOCOPY NUMBER)
   IS
      l_mod_name                  VARCHAR2 (100) := 'xxwc_credit_copy_trx_pkg.reverse_trx :';

      l_dummy                     VARCHAR2 (240);
      l_line_amount               NUMBER;
      l_freight_amount            NUMBER := 0;
      l_amt_original              NUMBER;
      l_cm_lines_tbl              arw_cmreq_cover.cm_line_tbl_type_cover;
      l_cm_reason_code            VARCHAR2 (150);
      l_comments                  VARCHAR2 (150);
      l_request_id                NUMBER;
      l_batch_source_id           NUMBER;
      l_batch_source_name         VARCHAR2 (150) DEFAULT NULL ;
      l_line_cnt                  NUMBER;
      cm_trx_id                   NUMBER;
      l_frt_amount                NUMBER;
      --l_line_amount         NUMBER;
      l_tax_amount                NUMBER;
      l_credit_memo_type_id       NUMBER;
      l_sold_to_customer_id       NUMBER;
      l_paying_customer_id        NUMBER;
      l_paying_site_use_id        NUMBER;
      l_ccid                      NUMBER;
      l_rec_ccid                  NUMBER;
      l_gl_date                   DATE;
      l_attribute7                VARCHAR2(150);
      l_extended_amount           NUMBER;
      l_taxable_amount            NUMBER;
      l_auto_trx_numbering_flag   ra_batch_sources.auto_trx_numbering_flag%TYPE;
      l_trx_number                ra_customer_trx.trx_number%TYPE;
      l_orig_trx_number           ra_customer_trx.trx_number%TYPE;
      l_customer_trx_id           ra_customer_trx.customer_trx_id%TYPE;
      l_line_credits_flag         ra_cm_requests.line_credits_flag%TYPE;
      --Added below by Maha for ver 1.4
      l_interface_hdr_context     ra_customer_trx.interface_header_context%TYPE;
      l_interface_hdr_attribute7  ra_customer_trx.interface_header_attribute7%TYPE;
      l_interface_hdr_attribute8  ra_customer_trx.interface_header_attribute8%TYPE;
      l_interface_hdr_attribute9  ra_customer_trx.interface_header_attribute9%TYPE;
      l_interface_hdr_attribute10 ra_customer_trx.interface_header_attribute10%TYPE;
      l_interface_hdr_attribute11 ra_customer_trx.interface_header_attribute11%TYPE;
      l_interface_hdr_attribute12 ra_customer_trx.interface_header_attribute12%TYPE;
      l_interface_line_attribute6 ra_customer_trx_lines.interface_line_attribute6%TYPE;

      CURSOR C1
      IS
         SELECT   *
           FROM   apps.ra_customer_trx_lines
          WHERE   customer_trx_id = x_rev_customer_trx_id;

      --User defined exception
      VALIDATION_ERROR EXCEPTION;
      API_FAILURE EXCEPTION;
      SKIP_RECORD EXCEPTION;
   BEGIN
   
   fnd_file.put_line (fnd_file.LOG, 'Reverse trx prc for PRISM invoices');
      LOG_MSG (g_level_statement,
               l_mod_name,
               'p_customer_trx_id =>' || p_customer_trx_id);

      -- provide customer_trx_id of the invoice to credit
      l_customer_trx_id := p_customer_trx_id;

      --Get the commments for the credit transaction
      l_comments :=
         NVL (fnd_profile.VALUE ('XXWC_CREDIT_REBILL_COMMENTS'),
              'Ship To Adjustment');

      --Get the batch_source_id from original customer_trx_id
      l_batch_source_id :=
         FND_PROFILE.VALUE ('XXWC_CREDIT_REBILL_CM_BATCH_SOURCE');

      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_batch_source_id =>' || l_batch_source_id);

      BEGIN
         SELECT   name
           INTO   l_batch_source_name
           FROM   apps.ra_batch_sources src
          WHERE   batch_source_id = l_batch_source_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            LOG_MSG (
               g_level_statement,
               l_mod_name,
               'Error while getting the batch source name =>'
               || l_batch_source_id
            );
            RAISE VALIDATION_ERROR;
      END;

      /*
               xxwc_credit_copy_trx_util_pkg.get_batch_source_name (
                  p_customer_trx_id
               );
      */

      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_batch_source_name =>' || l_batch_source_name);

      IF l_batch_source_name IS NULL
      THEN
         FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_AR_TRX_SOURCE_MISSING');
         FND_MSG_PUB.ADD;
         RAISE VALIDATION_ERROR;
      END IF;

      --Get the credit memo reason
      l_cm_reason_code := FND_PROFILE.VALUE ('XXWC_CREDIT_REBILL_CM_REASON');

      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_cm_reason_code =>' || l_cm_reason_code);

      --Raise Error Message if reason code is null
      IF l_cm_reason_code IS NULL
      THEN
         FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_AR_CM_REASON_MISSING');
         FND_MSG_PUB.ADD;
         RAISE VALIDATION_ERROR;
      END IF;



      BEGIN
         SELECT   sold_to_customer_id,
                  paying_customer_id,
                  paying_site_use_id,
                  auto_trx_numbering_flag,
                  trx_number,
                  trx.attribute7,
                  --Added below by Maha for ver 1.4
                  interface_header_context,
                  interface_header_attribute7,
                  interface_header_attribute8,
                  interface_header_attribute9,
                  interface_header_attribute10,
                  interface_header_attribute11,
                  interface_header_attribute12
           INTO   l_sold_to_customer_id,
                  l_paying_customer_id,
                  l_paying_site_use_id,
                  l_auto_trx_numbering_flag,
                  l_trx_number,
                  l_attribute7,
                  --Added below by Maha for ver 1.4
                  l_interface_hdr_context,
                  l_interface_hdr_attribute7,
                  l_interface_hdr_attribute8,
                  l_interface_hdr_attribute9,
                  l_interface_hdr_attribute10,
                  l_interface_hdr_attribute11,
                  l_interface_hdr_attribute12
           FROM   apps.ra_customer_trx trx, apps.ra_batch_sources trx_src
          WHERE   trx.batch_source_id = trx_src.batch_source_id
                  AND trx.customer_trx_id = p_customer_trx_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_AR_INVOICE_MISSING');
            FND_MSG_PUB.ADD;
            RAISE VALIDATION_ERROR;
      END;

      /*
            IF l_auto_trx_numbering_flag = 'N'
            THEN
               l_trx_number := l_trx_number || '-CONV';
            ELSE
               l_trx_number := NULL;
            END IF;
      */

      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_sold_to_customer_id =>' || l_sold_to_customer_id);

      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_paying_customer_id =>' || l_paying_customer_id);

      --get the total amount remaining,tax remaining,freight remaining
      SELECT   SUM (NVL (amount_line_items_remaining, 0)),
               SUM (NVL (tax_remaining, 0)),
               SUM (NVL (freight_remaining, 0)),
               SUM(NVL(amount_due_original,0))
        INTO   l_line_amount, l_tax_amount, l_frt_amount,l_amt_original
        FROM   apps.ar_payment_schedules ct
       WHERE   ct.customer_trx_id = p_customer_trx_id;
       
      -- <Start added by Maha for ver 1.3
       FND_FILE.PUT_LINE(FND_FILE.LOG,'Before tax calculation');
       FND_FILE.PUT_LINE(FND_FILE.LOG,'Line Amount: '||l_line_amount);
       FND_FILE.PUT_LINE(FND_FILE.LOG,'Tax Amount: '||l_tax_amount);
       FND_FILE.PUT_LINE(FND_FILE.LOG,'Freight Amount: '||l_frt_amount);
       FND_FILE.PUT_LINE(FND_FILE.LOG,'Total Amount: '||l_amt_original);

      l_tax_amount := l_amt_original-(l_line_amount+l_frt_amount);
      
       FND_FILE.PUT_LINE(FND_FILE.LOG,'After tax calculation');
       FND_FILE.PUT_LINE(FND_FILE.LOG,'Line Amount: '||l_line_amount);
       FND_FILE.PUT_LINE(FND_FILE.LOG,'Tax Amount: '||l_tax_amount);
       FND_FILE.PUT_LINE(FND_FILE.LOG,'Freight Amount: '||l_frt_amount);
       FND_FILE.PUT_LINE(FND_FILE.LOG,'Total Amount: '||l_amt_original);
       -- >End
      
      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_line_amount =>' || l_line_amount);
      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_tax_amount =>' || l_tax_amount);
      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_frt_amount =>' || l_frt_amount);

      LOG_MSG (g_level_statement,
               l_mod_name,
               'Calling ar_credit_memo_api_pub.create_request ');
      ar_credit_memo_api_pub.create_request (
         p_api_version                  => 1.0,
         p_init_msg_list                => fnd_api.g_true,
         p_commit                       => fnd_api.g_false,
         p_validation_level             => fnd_api.g_valid_level_full,
         p_customer_trx_id              => l_customer_trx_id,
         p_freight_amount               => (-1 * l_frt_amount),
         p_line_amount                  => (-1 * l_line_amount),
         p_tax_amount                   => (-1 * l_tax_amount),
         p_line_credit_flag             => 'N',
         p_cm_reason_code               => l_cm_reason_code,
         p_comments                     => l_comments,
         p_cm_line_tbl                  => l_cm_lines_tbl,
         --p_orig_trx_number              => l_orig_trx_number,
         p_skip_workflow_flag           => 'Y',
         p_credit_method_installments   => NULL,
         p_credit_method_rules          => NULL,
         p_batch_source_name            => l_batch_source_name,
         p_trx_number                   => l_trx_number,
         p_org_id                       => g_org_id,
         x_request_id                   => l_request_id,
         x_return_status                => x_return_status,
         x_msg_count                    => x_error_count,
         x_msg_data                     => x_error_msg
      );

      LOG_MSG (
         g_level_statement,
         l_mod_name,
         'Return Status (ar_credit_memo_api_pub.create_request)=> '
         || x_return_status
      );

      IF x_return_status <> 'S'
      THEN
         LOG_MSG (g_level_statement,
                  l_mod_name,
                  'x_error_count => ' || x_error_count);
         LOG_MSG (g_level_statement,
                  l_mod_name,
                  'x_error_msg => ' || x_error_msg);

         -- Display messages from the message stack
         FOR I IN 1 .. x_error_count
         LOOP
            x_error_msg :=
               (SUBSTR (FND_MSG_PUB.GET (p_msg_index => i, p_encoded => 'F'),
                        1,
                        254));
            LOG_MSG (g_level_statement, l_mod_name, x_error_msg);
         END LOOP;

         RAISE API_FAILURE;
      ELSE
         BEGIN
            SELECT   cm_customer_trx_id
              INTO   x_rev_customer_trx_id
              FROM   apps.ra_cm_requests
             WHERE   request_id = l_request_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;
         
         
         --Addded by Maha for ver 1.3 to handle tax issue <Start
         BEGIN
         SELECT SUM(extended_amount)* (-1)
               INTO l_extended_amount
          FROM apps.ra_customer_trx_lines
          WHERE customer_trx_id = x_rev_customer_trx_id
           AND line_type = 'TAX';
         EXCEPTION
         WHEN OTHERS THEN
           l_extended_amount := 0;
         END;
         
       /*  BEGIN
         SELECT DISTINCT(TAXABLE_AMOUNT)
          INTO l_taxable_amount
          FROM ra_customer_trx_lines_all
          WHERE customer_trx_id = x_rev_customer_trx_id
          AND line_type = 'TAX'
          AND extended_amount != 0;
          EXCEPTION
          WHEN OTHERS THEN
           l_taxable_amount := 0;
         END;   */

        FND_FILE.PUT_LINE(FND_FILE.LOG,'Tax created for CM :'||l_extended_amount);
        FND_FILE.PUT_LINE(FND_FILE.LOG,'Original Tax for INV :'||l_tax_amount);
 
       
      IF l_tax_amount != l_extended_amount THEN
      FND_FILE.PUT_LINE(FND_FILE.LOG,'CM Tax not matching Original Tax ');
         
         FOR I in ( SELECT customer_trx_line_id,
                           extended_amount,
                           taxable_amount,
                           tax_rate
                     FROM apps.ra_customer_trx_lines
                     WHERE customer_trx_id =  p_customer_trx_id
                       AND line_type = 'TAX')
         LOOP                
       
        
          UPDATE ra_customer_trx_lines_all
           SET extended_amount = i.extended_amount *(-1),
               taxable_amount = i.taxable_amount,
               tax_rate = i.tax_rate
           WHERE customer_trx_id = x_rev_customer_trx_id
           AND previous_customer_trx_line_id = i.customer_trx_line_id
           AND line_type = 'TAX';
--           AND extended_amount = 0
 --          AND rownum = 1;             
        END LOOP;          
        
                 
       END IF;
       -- > End
       
         --Update the sold_to_customer_id and paying_customer_id
         UPDATE   ra_customer_trx_all
            SET   sold_to_customer_id           = l_sold_to_customer_id,
                  paying_customer_id            = l_paying_customer_id,
                  paying_site_use_id            = l_paying_site_use_id,
                  attribute7                    = l_attribute7,
                  --Added below by Maha for ver 1.4
                  interface_header_context      = l_interface_hdr_context,
                  interface_header_attribute7   = l_interface_hdr_attribute7,
                  interface_header_attribute8   = l_interface_hdr_attribute8,
                  interface_header_attribute9   = l_interface_hdr_attribute9,
                  interface_header_attribute10  = l_interface_hdr_attribute10,
                  interface_header_attribute11  = l_interface_hdr_attribute11,
                  interface_header_attribute12  = l_interface_hdr_attribute12
          WHERE   customer_trx_id               = x_rev_customer_trx_id;

         LOG_MSG (g_level_statement,
                  l_mod_name,
                  'Num of Rows Updated = ' || sql%ROWCOUNT);

         LOG_MSG (g_level_statement,
                  l_mod_name,
                  'SUCESS CM Customer_trx_id = ' || x_rev_customer_trx_id);

         LOG_MSG (g_level_statement,
                  l_mod_name,
                  'Updating the CCID for Credit Memo');

         FOR i IN C1
         LOOP
            BEGIN
               BEGIN
                  SELECT   code_combination_id, gl_date
                    INTO   l_ccid, l_gl_date
                    FROM   apps.ra_cust_trx_line_gl_dist
                   WHERE   customer_trx_line_id =
                              i.previous_customer_trx_line_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     LOG_MSG (g_level_statement,
                              l_mod_name,
                              'Error :' || SQLERRM);
                     RAISE SKIP_RECORD;
               END;

               LOG_MSG (g_level_statement, l_mod_name, 'l_ccid :' || l_ccid);
               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'l_gl_date :' || l_gl_date);

               IF i.customer_trx_line_id IS NOT NULL
               THEN
                  UPDATE   ra_cust_trx_line_gl_dist_all
                     SET   code_combination_id = l_ccid --, gl_date = l_gl_date 
                     --Commented by Shankar 14-Mar-2012 per Bob's email
                   WHERE   customer_trx_line_id = i.customer_trx_line_id;

                  LOG_MSG (g_level_statement,
                           l_mod_name,
                           'Num of Rows updated with CCID :' || sql%ROWCOUNT);
               END IF;
            EXCEPTION
               WHEN SKIP_RECORD
               THEN
                  NULL;
            END;
         END LOOP;

         BEGIN
            SELECT   code_combination_id, gl_date
              INTO   l_rec_ccid, l_gl_date
              FROM   apps.ra_cust_trx_line_gl_dist
             WHERE       customer_trx_id = p_customer_trx_id
                     AND account_class = 'REC'
                     AND customer_trx_line_id IS NULL;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         LOG_MSG (g_level_statement, l_mod_name, 'l_rec_ccid:' || l_rec_ccid);

         UPDATE   ra_cust_trx_line_gl_dist_all
            SET   code_combination_id = l_rec_ccid--, gl_date = l_gl_date
            --Commented by Shankar 14-Mar-2012 per Bob's email
          WHERE       customer_trx_id = x_rev_customer_trx_id
                  AND account_class = 'REC'
                  AND customer_trx_line_id IS NULL;

         LOG_MSG (
            g_level_statement,
            l_mod_name,
            'Num of Rows updated with CCID for Receivable account:'
            || sql%ROWCOUNT
         );

         UPDATE   ra_customer_trx_all
            SET   trx_date =
                     (SELECT   trx_date
                        FROM   apps.ra_customer_trx
                       WHERE   customer_trx_id = p_customer_trx_id)
          WHERE   customer_trx_id = x_rev_customer_trx_id;

         LOG_MSG (
            g_level_statement,
            l_mod_name,
            'Num of Rows updated with trx date on header records'
            || sql%ROWCOUNT
         );
      END IF;

      --Added below by Maha for ver 1.4  <<Start

        FOR I in ( SELECT customer_trx_line_id
              ,interface_line_context
             ,interface_line_attribute1
             ,interface_line_attribute2
             ,interface_line_attribute3
             ,interface_line_attribute4
             ,interface_line_attribute5
             ,interface_line_attribute6
             ,interface_line_attribute7
             ,interface_line_attribute8
             ,interface_line_attribute9
             ,interface_line_attribute10
             ,interface_line_attribute11
             ,interface_line_attribute12
             ,interface_line_attribute13
             ,interface_line_attribute14
             ,interface_line_attribute15
             ,tax_exempt_flag
                     FROM apps.ra_customer_trx_lines
                     WHERE customer_trx_id =  p_customer_trx_id
                       AND line_type = 'LINE')
         LOOP             
        --Added below IF clause for ver 1.5
          IF i.interface_line_context = 'ORDER ENTRY' THEN
             l_interface_line_attribute6 := i.interface_line_attribute6||'00';
          ELSE
             l_interface_line_attribute6 := i.interface_line_attribute6||'_REBILL';

          END IF;

          UPDATE ra_customer_trx_lines_all
           SET          interface_line_context          = i.interface_line_context  
             ,interface_line_attribute1      = i.interface_line_attribute1
             ,interface_line_attribute2    = i.interface_line_attribute2
             ,interface_line_attribute3    = i.interface_line_attribute3
             ,interface_line_attribute4    = i.interface_line_attribute4
             ,interface_line_attribute5    = i.interface_line_attribute5
             ,interface_line_attribute6    = l_interface_line_attribute6
             ,interface_line_attribute7    = i.interface_line_attribute7
             ,interface_line_attribute8    = i.interface_line_attribute8
             ,interface_line_attribute9    = i.interface_line_attribute9
             ,interface_line_attribute10    = i.interface_line_attribute10
             ,interface_line_attribute11    = i.interface_line_attribute11
             ,interface_line_attribute12    = i.interface_line_attribute12
             ,interface_line_attribute13    = i.interface_line_attribute13
             ,interface_line_attribute14    = i.interface_line_attribute14
             ,interface_line_attribute15    = i.interface_line_attribute5
             ,tax_exempt_flag        = i.tax_exempt_flag
           WHERE customer_trx_id = x_rev_customer_trx_id
           AND previous_customer_trx_line_id = i.customer_trx_line_id
           AND line_type = 'LINE';
        END LOOP;          
-->> End

   EXCEPTION
      WHEN VALIDATION_ERROR
      THEN
         x_return_status := 'E';
      WHEN API_FAILURE
      THEN
         x_return_status := 'E';
         x_error_msg := SQLERRM;
         x_error_count := 1;
      WHEN OTHERS
      THEN
         x_return_status := 'E';
         x_error_msg := SQLERRM;
         x_error_count := 1;
   END reverse_trx;

   PROCEDURE create_new_trx (p_customer_trx_id       IN            NUMBER,
                             p_bill_to_site_use_id   IN            NUMBER,
                             p_ship_to_site_use_id   IN            NUMBER,
                             p_reason                IN            VARCHAR2,
                             x_new_customer_trx_id      OUT NOCOPY NUMBER,
                             x_return_status            OUT NOCOPY VARCHAR2,
                             x_error_msg                OUT NOCOPY VARCHAR2,
                             x_error_count              OUT NOCOPY NUMBER)
   IS
      l_mod_name VARCHAR2 (100)
            := 'xxwc_credit_copy_trx_pkg.create_new_trx :' ;

      l_batch_source_rec          ar_invoice_api_pub.batch_source_rec_type;
      l_trx_header_tbl            ar_invoice_api_pub.trx_header_tbl_type;
      l_trx_lines_tbl             ar_invoice_api_pub.trx_line_tbl_type;
      l_trx_dist_tbl              ar_invoice_api_pub.trx_dist_tbl_type;
      l_trx_salescredits_tbl      ar_invoice_api_pub.trx_salescredits_tbl_type;

      l_tax_rate_id               NUMBER;
      l_tax_jurisdiction_code     zx_lines_v.tax_jurisdiction_code%TYPE;
      l_tax_rate_code             zx_lines_v.tax_rate_code%TYPE;
      l_tax_status_code           zx_lines_v.tax_status_code%TYPE;
      l_tax_regime_code           zx_lines_v.tax_regime_code%TYPE;
      l_tax                       zx_lines_v.tax%TYPE;

      p_count                     NUMBER := 0;
      l_trx_header_id             NUMBER;
      l_trx_line_id               NUMBER;
      l_trx_dist_id               NUMBER;
      l_rebill_batch_source_id    NUMBER;
      l_hdr_cnt                   NUMBER;
      l_line_cnt                  NUMBER;
      l_dist_cnt                  NUMBER;
      l_cnt                       NUMBER;
      l_tax_trx_line_id           NUMBER;
      l_tax_trx_dist_id           NUMBER;
      l_frt_trx_line_id           NUMBER;
      l_first_trx_line_id         NUMBER;
      l_gl_date                   DATE;
      l_rec_trx_dist_id           NUMBER;
      l_trx_rec_ccid_id           NUMBER;
      l_term_due_date             DATE;
      l_comments                  VARCHAR2 (100);
      l_related_customer_trx_id   NUMBER;
      l_note_id                   NUMBER;
      l_total_lines               NUMBER; -- Added by Shankar 23-Feb-2012

      l_ship_to_customer_id       NUMBER;
      l_ship_to_site_use_id       NUMBER;
      l_ship_to_contact_id        NUMBER;

      l_ct_reference              ra_customer_trx_all.ct_reference%TYPE;
      l_customer_reference        ra_customer_trx_all.customer_reference%TYPE;
      l_customer_reference_date   ra_customer_trx_all.customer_reference_date%TYPE;
      l_purchase_order            ra_customer_trx_all.purchase_order%TYPE;
      l_purchase_order_date       ra_customer_trx_all.purchase_order_date%TYPE;
      l_purchase_order_revision   ra_customer_trx_all.purchase_order_revision%TYPE;
      l_doc_sequence_value        ra_customer_trx_all.doc_sequence_value%TYPE;
      --l_ship_via                  ra_customer_trx_all.ship_via%TYPE;
      l_fob_point                 ra_customer_trx_all.fob_point%TYPE;
      --l_ship_date_actual          ra_customer_trx_all.ship_date_actual%TYPE;
      --l_waybill_number            ra_customer_trx_all.waybill_number%TYPE;
     -- <Start added by Maha for ver 1.3
      l_iface_hdr_attribute1      ra_customer_trx_all.interface_header_attribute1%type;
      l_iface_hdr_attribute2      ra_customer_trx_all.interface_header_attribute2%type;
      l_iface_hdr_attribute3      ra_customer_trx_all.interface_header_attribute3%type;
      l_iface_hdr_attribute4      ra_customer_trx_all.interface_header_attribute4%type;
      l_iface_hdr_attribute5      ra_customer_trx_all.interface_header_attribute5%type;
      l_iface_hdr_attribute6      ra_customer_trx_all.interface_header_attribute6%type;
      l_iface_hdr_attribute7      ra_customer_trx_all.interface_header_attribute7%type;
      l_iface_hdr_attribute8      ra_customer_trx_all.interface_header_attribute8%type;
      l_iface_hdr_attribute9      ra_customer_trx_all.interface_header_attribute9%type;
      l_iface_hdr_attribute10     ra_customer_trx_all.interface_header_attribute10%type;
      l_iface_hdr_attribute11     ra_customer_trx_all.interface_header_attribute11%type;
      l_iface_hdr_attribute12     ra_customer_trx_all.interface_header_attribute12%type;
      l_iface_hdr_attribute13     ra_customer_trx_all.interface_header_attribute13%type;
      l_iface_hdr_attribute14     ra_customer_trx_all.interface_header_attribute14%type;
      l_iface_hdr_attribute15     ra_customer_trx_all.interface_header_attribute15%type;
      -->End


      --User defined exception
      API_FAILURE EXCEPTION;
      VALIDATION_ERROR EXCEPTION;


      CURSOR cur_header
      IS
         SELECT   *
           FROM   apps.ra_customer_trx
          WHERE   customer_trx_id = p_customer_trx_id;

      CURSOR cur_lines
      IS
         SELECT   *
           FROM   apps.ra_customer_trx_lines
          WHERE   customer_trx_id = p_customer_trx_id AND line_type = 'LINE';

      CURSOR list_errors
      IS
         SELECT   trx_header_id,
                  trx_line_id,
                  trx_salescredit_id,
                  trx_dist_id,
                  trx_contingency_id,
                  error_message,
                  invalid_value
           FROM   apps.ar_trx_errors_gt;

      CURSOR cur_frt_hdr
      IS
         SELECT   *
           FROM   apps.ra_customer_trx_lines
          WHERE       customer_trx_id = p_customer_trx_id
                  AND line_type = 'FREIGHT'
                  AND link_to_cust_trx_line_id IS NULL;

      CURSOR cur_tax_lines (
         p_customer_trx_line_id   IN            NUMBER
      )
      IS
         SELECT   *
           FROM   apps.ra_customer_trx_lines
          WHERE   link_to_cust_trx_line_id = p_customer_trx_line_id
                  AND line_type IN ('TAX');

      CURSOR cur_dist (p_customer_trx_line_id IN NUMBER)
      IS
         SELECT   *
           FROM   apps.ra_cust_trx_line_gl_dist
          WHERE   customer_trx_line_id = p_customer_trx_line_id;

      CURSOR cur_rec_dist
      IS
         SELECT   *
           FROM   apps.ra_cust_trx_line_gl_dist
          WHERE       customer_trx_id = p_customer_trx_id
                  AND account_class = 'REC'
                  AND customer_trx_line_id IS NULL;

      CURSOR cur_notes
      IS
         SELECT   *
           FROM   apps.ar_notes
          WHERE   customer_trx_id = p_customer_trx_id;
   BEGIN
      --Get the Rebill batch source from profile option
      l_rebill_batch_source_id :=
         FND_PROFILE.VALUE ('XXWC_CREDIT_REBILL_BATCH_SOURCE');

      --Raise error message if the batch source not found
      IF l_rebill_batch_source_id IS NULL
      THEN
         FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_AR_TRX_SOURCE_MISSING');
         FND_MSG_PUB.ADD;
         RAISE VALIDATION_ERROR;
      END IF;

      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_rebill_batch_source_id =>' || l_rebill_batch_source_id);

      -- Populate batch source information.
      l_batch_source_rec.batch_source_id := l_rebill_batch_source_id;


      --Get the commments for the credit transaction
      l_comments :=
         NVL (fnd_profile.VALUE ('XXWC_CREDIT_REBILL_COMMENTS'),
              'Ship To Adjustment');

      --delete the plsql table for the
      --header,lines,distributions
      l_trx_header_tbl.delete;
      l_trx_lines_tbl.delete;
      l_trx_dist_tbl.delete;

      l_hdr_cnt := 1;

      FOR i IN cur_header
      LOOP
         SELECT   RA_CUSTOMER_TRX_S.NEXTVAL INTO l_trx_header_id FROM DUAL;
         
         l_gl_date := sysdate;
           -- xxwc_credit_copy_trx_util_pkg.get_gl_date (p_customer_trx_id);
            --Commented by Shankar 14-Mar-2012 per Bob's email

         LOG_MSG (g_level_statement, l_mod_name, 'l_gl_date =>' || l_gl_date);

         -- Populate header information.
         l_trx_header_tbl (l_hdr_cnt).trx_header_id := l_trx_header_id;
         l_trx_header_tbl (l_hdr_cnt).trx_number := i.trx_number;
         l_trx_header_tbl (l_hdr_cnt).trx_date := i.trx_date;
         l_trx_header_tbl (l_hdr_cnt).gl_date := l_gl_date;
         l_trx_header_tbl (l_hdr_cnt).cust_trx_type_id := i.cust_trx_type_id;
         l_trx_header_tbl (l_hdr_cnt).bill_to_customer_id :=
            i.bill_to_customer_id;
         l_trx_header_tbl (l_hdr_cnt).bill_to_site_use_id :=
            p_bill_to_site_use_id;
         l_trx_header_tbl (l_hdr_cnt).bill_to_contact_id :=
            i.bill_to_contact_id;

         l_trx_header_tbl (l_hdr_cnt).ship_to_customer_id :=
            NVL (i.ship_to_customer_id, i.bill_to_customer_id);
         l_trx_header_tbl (l_hdr_cnt).ship_to_site_use_id :=
            p_ship_to_site_use_id;
         l_trx_header_tbl (l_hdr_cnt).ship_to_contact_id :=
            i.ship_to_contact_id;
         l_trx_header_tbl (1).primary_salesrep_id := i.primary_salesrep_id;
         l_trx_header_tbl (l_hdr_cnt).ship_via := i.ship_via;
         l_trx_header_tbl (l_hdr_cnt).ship_date_actual := i.ship_date_actual;
         l_trx_header_tbl (l_hdr_cnt).waybill_number := i.waybill_number;
         l_fob_point := i.fob_point;

         l_trx_header_tbl (1).paying_customer_id := i.paying_customer_id;
         l_trx_header_tbl (1).paying_site_use_id := i.paying_site_use_id;
         l_trx_header_tbl (1).sold_to_customer_id := i.sold_to_customer_id;
         --l_trx_header_tbl (1).sold_to_site_use_id  := i.sold_to_site_use_id;
         l_trx_header_tbl (1).remit_to_address_id := i.remit_to_address_id;
         l_trx_header_tbl (1).term_id := i.term_id;
         l_trx_header_tbl (1).comments := l_comments;
         l_trx_header_tbl (1).internal_notes := i.internal_notes;
         l_trx_header_tbl (1).receipt_method_id := i.receipt_method_id;
         l_customer_reference := i.customer_reference;
         l_customer_reference_date := i.customer_reference_date;
         l_purchase_order := i.purchase_order;
         l_purchase_order_date := i.purchase_order_date;
         l_purchase_order_revision := i.purchase_order_revision;
         l_term_due_date := i.term_due_date;
         l_ct_reference := i.ct_reference;
         l_doc_sequence_value := i.doc_sequence_value;
         l_related_customer_trx_id := i.related_customer_trx_id;
    --< Start Added by Maha for ver 1.3
         l_trx_header_tbl (1).attribute_category := i.attribute_category;
         l_trx_header_tbl (1).attribute1 := i.attribute1;
         l_trx_header_tbl (1).attribute2 := i.attribute2;
         l_trx_header_tbl (1).attribute3 := i.attribute3;
         l_trx_header_tbl (1).attribute4 := i.attribute4;
         l_trx_header_tbl (1).attribute5 := i.attribute5;
         l_trx_header_tbl (1).attribute6 := i.attribute6;
         l_trx_header_tbl (1).attribute7 := i.attribute7;
         l_trx_header_tbl (1).attribute8 := i.attribute8;
         l_trx_header_tbl (1).attribute9 := i.attribute9;
         l_trx_header_tbl (1).attribute10 := i.attribute10;
         l_trx_header_tbl (1).attribute11 := i.attribute11;
         l_trx_header_tbl (1).attribute12 := null;--i.attribute12; --changed for ver 1.9 by Neha
         l_trx_header_tbl (1).attribute13 := i.attribute13;
         l_trx_header_tbl (1).attribute14 := i.attribute14;
         l_trx_header_tbl (1).attribute15 := i.attribute15;
        l_trx_header_tbl (1).interface_header_context := i.interface_header_context;
        --> End
         

         l_hdr_cnt := l_hdr_cnt + 1;
      END LOOP;                                              -- end cur_header

      l_line_cnt := 1;
      l_dist_cnt := 1;

      l_first_trx_line_id := NULL;
      
      -- Added by Shankar for issue 45 23-Feb-2012
         SELECT   count(*)
           INTO   l_total_lines
           FROM   apps.ra_customer_trx_lines
          WHERE   customer_trx_id = p_customer_trx_id 
            AND line_type = 'LINE';

      FOR k IN cur_lines
      LOOP
         SELECT   ra_customer_trx_lines_s.NEXTVAL INTO l_trx_line_id FROM DUAL;

         IF l_first_trx_line_id IS NULL
         THEN
            l_first_trx_line_id := l_trx_line_id;
         END IF;

         -- Populate lines information.
         l_trx_lines_tbl (l_line_cnt).trx_header_id := l_trx_header_id;
         l_trx_lines_tbl (l_line_cnt).trx_line_id := l_trx_line_id;
         l_trx_lines_tbl (l_line_cnt).line_number := k.line_number;
         l_trx_lines_tbl (l_line_cnt).inventory_item_id := k.inventory_item_id;
         l_trx_lines_tbl (l_line_cnt).description := k.description;
         l_trx_lines_tbl (l_line_cnt).quantity_ordered := k.quantity_ordered;
         l_trx_lines_tbl (l_line_cnt).quantity_invoiced := k.quantity_invoiced;
         l_trx_lines_tbl (l_line_cnt).unit_selling_price :=
            k.unit_selling_price;
         l_trx_lines_tbl (l_line_cnt).line_type := k.line_type;
         l_trx_lines_tbl (l_line_cnt).uom_code := k.uom_code;
         l_trx_lines_tbl (l_line_cnt).amount_includes_tax_flag :=
            NVL (k.amount_includes_tax_flag, 'N');
         l_trx_lines_tbl (l_line_cnt).taxable_flag := 'N';
         l_trx_lines_tbl (l_line_cnt).attribute1 := k.attribute1;
         l_trx_lines_tbl (l_line_cnt).attribute2 := k.attribute2;
         l_trx_lines_tbl (l_line_cnt).attribute3 := k.attribute3;
         l_trx_lines_tbl (l_line_cnt).attribute4 := k.attribute4;
         l_trx_lines_tbl (l_line_cnt).attribute5 := k.attribute5;
         l_trx_lines_tbl (l_line_cnt).attribute6 := k.translated_description;
         l_trx_lines_tbl (l_line_cnt).attribute7 := k.ship_to_contact_id;
         l_trx_lines_tbl (l_line_cnt).attribute8 := k.ship_to_site_use_id;
         l_trx_lines_tbl (l_line_cnt).attribute9 := k.ship_to_customer_id;
         l_trx_lines_tbl (l_line_cnt).attribute10 := k.sales_order_source;
         l_trx_lines_tbl (l_line_cnt).attribute11 := k.reason_code;
         l_trx_lines_tbl (l_line_cnt).attribute12 := k.sales_order_date;
         l_trx_lines_tbl (l_line_cnt).attribute13 := k.sales_order_line;
         l_trx_lines_tbl (l_line_cnt).attribute14 := k.sales_order_revision;
         l_trx_lines_tbl (l_line_cnt).attribute15 := k.sales_order;
         l_trx_lines_tbl (l_line_cnt).warehouse_id := k.warehouse_id;

         -- < start Added below by Maha for ver 1.3                                                 
           l_trx_lines_tbl (l_line_cnt).tax_classification_code   := k.tax_classification_code;
          l_trx_lines_tbl (l_line_cnt).unit_standard_price          := k.unit_standard_price;
          l_trx_lines_tbl (l_line_cnt).interface_line_context       := k.interface_line_context;
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute1    := trim(k.interface_line_attribute1);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute2    := trim(k.interface_line_attribute2);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute3    := trim(k.interface_line_attribute3);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute4    := trim(k.interface_line_attribute4);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute5    := trim(k.interface_line_attribute5);
          
          IF k.interface_line_context = 'ORDER ENTRY'
          THEN
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute6    := trim(k.interface_line_attribute6)||'00';
          ELSE          
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute6    := trim(k.interface_line_attribute6)||'_REBILL';
           END IF;
           
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute7    := trim(k.interface_line_attribute7);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute8    := trim(k.interface_line_attribute8);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute9    := trim(k.interface_line_attribute9);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute10   := trim(k.interface_line_attribute10);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute11   := trim(k.interface_line_attribute11);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute12   := trim(k.interface_line_attribute12);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute13   := trim(k.interface_line_attribute13);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute14   := trim(k.interface_line_attribute14);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute15   := trim(k.interface_line_attribute15);
           -- > End
       
         --Populate the distribution lines for LINE types
         FOR l IN cur_dist (k.customer_trx_line_id)
         LOOP
            SELECT   ra_cust_trx_line_gl_dist_s.NEXTVAL
              INTO   l_trx_dist_id
              FROM   DUAL;

            -- Populate Distribution Information
            l_trx_dist_tbl (l_dist_cnt).trx_dist_id := l_trx_dist_id;
            l_trx_dist_tbl (l_dist_cnt).trx_header_id := l_trx_header_id;
            l_trx_dist_tbl (l_dist_cnt).trx_line_id := l_trx_line_id;
            l_trx_dist_tbl (l_dist_cnt).account_class := l.account_class;
            l_trx_dist_tbl (l_dist_cnt).amount := l.amount;
            l_trx_dist_tbl (l_dist_cnt).code_combination_id :=
               l.code_combination_id;
            l_trx_dist_tbl (l_dist_cnt).attribute15 := l.code_combination_id;

            l_dist_cnt := l_dist_cnt + 1;
         END LOOP;

         -- Populate the tax lines

         FOR m IN cur_tax_lines (k.customer_trx_line_id)
         LOOP
            SELECT   ra_customer_trx_lines_s.NEXTVAL
              INTO   l_tax_trx_line_id
              FROM   DUAL;

            l_line_cnt := l_line_cnt + 1;

            BEGIN
               SELECT   tax,
                        tax_rate_id,
                        tax_jurisdiction_code,
                        tax_rate_code,
                        tax_status_code,
                        tax_regime_code
                 INTO   l_tax,
                        l_tax_rate_id,
                        l_tax_jurisdiction_code,
                        l_tax_rate_code,
                        l_tax_status_code,
                        l_tax_regime_code
                 FROM   apps.zx_lines_v
                WHERE   tax_line_id = m.tax_line_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_tax_rate_id := NULL;
                  l_tax_jurisdiction_code := NULL;
                  l_tax_rate_code := NULL;
                  l_tax_status_code := NULL;
                  l_tax_regime_code := NULL;
            END;
            l_total_lines := l_total_lines + 1;  -- Shankar
            -- Populate tax  lines information.
            l_trx_lines_tbl (l_line_cnt).trx_header_id := l_trx_header_id;
            l_trx_lines_tbl (l_line_cnt).trx_line_id := l_tax_trx_line_id;
            l_trx_lines_tbl (l_line_cnt).link_to_trx_line_id := l_trx_line_id;
            l_trx_lines_tbl (l_line_cnt).line_number := l_total_lines; --k.line_number; Shankar
            l_trx_lines_tbl (l_line_cnt).line_type := m.line_type;
            l_trx_lines_tbl (l_line_cnt).amount := m.extended_amount;
            l_trx_lines_tbl (l_line_cnt).tax := l_tax;
            l_trx_lines_tbl (l_line_cnt).vat_tax_id := m.vat_tax_id;
            l_trx_lines_tbl (l_line_cnt).tax_rate := m.tax_rate;
            l_trx_lines_tbl (l_line_cnt).tax_jurisdiction_code :=
               l_tax_jurisdiction_code;
            l_trx_lines_tbl (l_line_cnt).tax_rate_code := l_tax_rate_code;
            l_trx_lines_tbl (l_line_cnt).tax_status_code := l_tax_status_code;
            l_trx_lines_tbl (l_line_cnt).tax_regime_code := l_tax_regime_code;
            l_trx_lines_tbl (l_line_cnt).amount_includes_tax_flag := NULL;
            l_trx_lines_tbl (l_line_cnt).taxable_flag := NULL;

            LOG_MSG (
               g_level_statement,
               l_mod_name,
               'l_trx_lines_tbl (l_line_cnt).tax_rate =>'
               || l_trx_lines_tbl (l_line_cnt).tax_rate
            );
            LOG_MSG (
               g_level_statement,
               l_mod_name,
               'l_trx_lines_tbl (l_line_cnt).tax_jurisdiction_code =>'
               || l_trx_lines_tbl (l_line_cnt).tax_jurisdiction_code
            );
            LOG_MSG (
               g_level_statement,
               l_mod_name,
               'l_trx_lines_tbl (l_line_cnt).tax_rate_code =>'
               || l_trx_lines_tbl (l_line_cnt).tax_rate_code
            );
            LOG_MSG (
               g_level_statement,
               l_mod_name,
               'l_trx_lines_tbl (l_line_cnt).tax_status_code =>'
               || l_trx_lines_tbl (l_line_cnt).tax_status_code
            );
            LOG_MSG (
               g_level_statement,
               l_mod_name,
               'l_trx_lines_tbl (l_line_cnt).tax_regime_code =>'
               || l_trx_lines_tbl (l_line_cnt).tax_regime_code
            );

            -- Populate Distribution lines for TAX line types
            FOR n IN cur_dist (m.customer_trx_line_id)
            LOOP
               SELECT   ra_cust_trx_line_gl_dist_s.NEXTVAL
                 INTO   l_tax_trx_dist_id
                 FROM   DUAL;

               -- Populate Distribution lines for TAX and FREIGHT line types
               l_trx_dist_tbl (l_dist_cnt).trx_dist_id := l_tax_trx_dist_id;
               l_trx_dist_tbl (l_dist_cnt).trx_header_id := l_trx_header_id;
               l_trx_dist_tbl (l_dist_cnt).trx_line_id := l_tax_trx_line_id;
               l_trx_dist_tbl (l_dist_cnt).account_class := n.account_class;
               l_trx_dist_tbl (l_dist_cnt).amount := n.amount;
               l_trx_dist_tbl (l_dist_cnt).code_combination_id :=
                  n.code_combination_id;
               l_trx_dist_tbl (l_dist_cnt).attribute15 :=
                  n.code_combination_id;
               l_dist_cnt := l_dist_cnt + 1;
            END LOOP;
         END LOOP;
         
         l_line_cnt := l_line_cnt + 1;
      END LOOP;

      /*
            FOR n IN cur_frt_hdr
            LOOP
               SELECT   ra_customer_trx_lines_s.NEXTVAL
                 INTO   l_frt_trx_line_id
                 FROM   DUAL;

               --populate the freight lines for the header level
               l_trx_lines_tbl (l_line_cnt).trx_header_id := l_trx_header_id;
               l_trx_lines_tbl (l_line_cnt).trx_line_id := l_frt_trx_line_id;
               --l_trx_lines_tbl (l_line_cnt).link_to_trx_line_id := l_first_trx_line_id;
               l_trx_lines_tbl (l_line_cnt).line_number := n.line_number;
               l_trx_lines_tbl (l_line_cnt).line_type := 'FREIGHT';
               l_trx_lines_tbl (l_line_cnt).amount := n.extended_amount;
               l_line_cnt := l_line_cnt + 1;

               FOR p IN cur_dist (n.customer_trx_line_id)
               LOOP
                  SELECT   ra_cust_trx_line_gl_dist_s.NEXTVAL
                    INTO   l_tax_trx_dist_id
                    FROM   DUAL;

                  -- Populate Distribution lines for FREIGHT at header level
                  l_trx_dist_tbl (l_dist_cnt).trx_dist_id := l_tax_trx_dist_id;
                  l_trx_dist_tbl (l_dist_cnt).trx_header_id := l_trx_header_id;
                  l_trx_dist_tbl (l_dist_cnt).trx_line_id := l_frt_trx_line_id;
                  l_trx_dist_tbl (l_dist_cnt).account_class := p.account_class;
                  l_trx_dist_tbl (l_dist_cnt).amount := p.amount;
                  l_trx_dist_tbl (l_dist_cnt).code_combination_id :=
                     p.code_combination_id;
                  l_dist_cnt := l_dist_cnt + 1;
               END LOOP;
            END LOOP;
      */

      -- Populate Distribution lines for receivables type
      FOR rec IN cur_rec_dist
      LOOP
         l_trx_rec_ccid_id := rec.code_combination_id;
      END LOOP;

      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_trx_rec_ccid_id =>' || l_trx_rec_ccid_id);

      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_trx_header_tbl.count =>' || l_trx_header_tbl.COUNT);
      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_trx_lines_tbl.count =>' || l_trx_lines_tbl.COUNT);
      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_trx_dist_tbl.count => ' || l_trx_dist_tbl.COUNT);

      /*
            FOR p IN l_trx_lines_tbl.FIRST .. l_trx_lines_tbl.LAST
            LOOP
               LOG_MSG (g_level_statement,
                        l_mod_name,
                        '******************************************');
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  'l_trx_lines_tbl.trx_line_id =>'
                  || l_trx_lines_tbl (p).trx_line_id
               );
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  'l_trx_lines_tbl.line_type =>' || l_trx_lines_tbl (p).line_type
               );
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  'l_trx_lines_tbl.line_number =>'
                  || l_trx_lines_tbl (p).line_number
               );
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  'l_trx_lines_tbl.link_to_trx_line_id =>'
                  || l_trx_lines_tbl (p).link_to_trx_line_id
               );
            END LOOP;
      */

      -- call the api
      ar_invoice_api_pub.create_single_invoice (
         p_api_version            => 1.0,
         p_batch_source_rec       => l_batch_source_rec,
         p_trx_header_tbl         => l_trx_header_tbl,
         p_trx_lines_tbl          => l_trx_lines_tbl,
         p_trx_dist_tbl           => l_trx_dist_tbl,
         p_trx_salescredits_tbl   => l_trx_salescredits_tbl,
         x_customer_trx_id        => x_new_customer_trx_id,
         x_return_status          => x_return_status,
         x_msg_count              => x_error_count,
         x_msg_data               => x_error_msg
      );

      LOG_MSG (g_level_statement,
               l_mod_name,
               'x_return_status(create_single_invoice)=' || x_return_status||'-'||x_new_customer_Trx_id);

      IF x_return_status <> 'S'
      THEN
         LOG_MSG (g_level_statement, l_mod_name, 'unexpected errors found!');

         IF x_error_count = 1
         THEN
            LOG_MSG (g_level_statement,
                     l_mod_name,
                     'x_error_msg ' || x_error_msg);
         ELSIF x_error_count > 1
         THEN
            LOOP
               p_count := p_count + 1;
               x_error_msg :=
                  FND_MSG_PUB.Get (FND_MSG_PUB.G_NEXT, FND_API.G_FALSE);

               IF x_error_msg IS NULL
               THEN
                  EXIT;
               END IF;

               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'Message' || p_count || '.' || x_error_msg);
            END LOOP;
         END IF;
      ELSE
         SELECT   COUNT ( * ) INTO l_cnt FROM ar_trx_errors_gt;

         IF l_cnt = 0 
         THEN
            LOG_MSG (
               g_level_statement,
               l_mod_name,
               'SUCCESS: Created customer_trx_id = ' || x_new_customer_trx_id
            );


            FOR p_notes_rec IN cur_notes
            LOOP
               /*------------------------------+
                |  get the unique id            |
                +------------------------------*/

               SELECT   ar_notes_s.NEXTVAL INTO l_note_id FROM DUAL;

               /*------------------------------+
             |  insert the record            |
             +------------------------------*/
               INSERT INTO ar_notes (note_id,
                                     note_type,
                                     text,
                                     customer_trx_id,
                                     customer_call_id,
                                     customer_call_topic_id,
                                     call_action_id,
                                     last_updated_by,
                                     last_update_date,
                                     last_update_login,
                                     created_by,
                                     creation_date)
                 VALUES   (l_note_id,
                           p_notes_rec.note_type,
                           p_notes_rec.text,
                           x_new_customer_trx_id,
                           p_notes_rec.customer_call_id,
                           p_notes_rec.customer_call_topic_id,
                           p_notes_rec.call_action_id,
                           fnd_global.user_id,
                           SYSDATE,
                           fnd_global.login_id,
                           fnd_global.user_id,
                           SYSDATE);
            END LOOP;
            
-- < Start Added below block by Maha for ver 1.3
            
BEGIN
   SELECT interface_header_attribute1,
         interface_header_attribute2,
         interface_header_attribute3, 
         interface_header_attribute4,
         interface_header_attribute5,
          interface_header_attribute6,
          interface_header_attribute7,
          interface_header_attribute8,
          interface_header_attribute9,
          interface_header_attribute10,
          interface_header_attribute11,
          interface_header_attribute12,
          interface_header_attribute13,
          interface_header_attribute14,
          interface_header_attribute15
     INTO l_iface_hdr_attribute1,
          l_iface_hdr_attribute2,
           l_iface_hdr_attribute3,
          l_iface_hdr_attribute4,
           l_iface_hdr_attribute5,
          l_iface_hdr_attribute6,
          l_iface_hdr_attribute7,
          l_iface_hdr_attribute8,
          l_iface_hdr_attribute9,
          l_iface_hdr_attribute10,
          l_iface_hdr_attribute11,
          l_iface_hdr_attribute12,
          l_iface_hdr_attribute13,
          l_iface_hdr_attribute14,
          l_iface_hdr_attribute15
     FROM apps.ra_customer_trx
    WHERE customer_trx_id = p_customer_trx_id;
EXCEPTION
   WHEN OTHERS
   THEN
   NULL;
END;

--> End            


            UPDATE   ra_customer_trx_all
               SET   customer_reference = l_customer_reference,
                     customer_reference_date = l_customer_reference_date,
                     purchase_order = l_purchase_order,
                     purchase_order_date = l_purchase_order_date,
                     purchase_order_revision = l_purchase_order_revision,
                     term_due_date = l_term_due_date,
                     ct_reference = l_ct_reference,
                     doc_sequence_value = l_doc_sequence_value,
                     related_customer_trx_id = l_related_customer_trx_id, --ship_via = l_ship_via
                     fob_point = l_fob_point,                    
                     /* Formatted on 1/27/2014 12:04:26 PM (QP5 v5.254.13202.43190) */
-- < Start Added below by Maha for ver 1.3
                     interface_header_attribute1 = l_iface_hdr_attribute1,
                     interface_header_attribute2 = l_iface_hdr_attribute2,
                     interface_header_attribute3 = l_iface_hdr_attribute3,
                     interface_header_attribute4 = l_iface_hdr_attribute4,
                     interface_header_attribute5 = l_iface_hdr_attribute5,
                     interface_header_attribute6 = l_iface_hdr_attribute6,
                     interface_header_attribute7 = l_iface_hdr_attribute7,
                     interface_header_attribute8 = l_iface_hdr_attribute8,
                     interface_header_attribute9 = l_iface_hdr_attribute9,
                     interface_header_attribute10 = l_iface_hdr_attribute10,
                     interface_header_attribute11 = l_iface_hdr_attribute11,
                     interface_header_attribute12 = l_iface_hdr_attribute12,
                     interface_header_attribute13 = l_iface_hdr_attribute13,
                     interface_header_attribute14 = l_iface_hdr_attribute14,
                     interface_header_attribute15 = l_iface_hdr_attribute15
                     --> End
             --ship_date_actual = l_ship_date_actual,
             --waybill_number   = l_waybill_number
             WHERE   customer_trx_id = x_new_customer_trx_id;

            UPDATE   ra_customer_trx_lines_all
               SET   sales_order = attribute15,
                     translated_description = attribute6,
                     ship_to_contact_id = attribute7,
                     ship_to_site_use_id = attribute8,
                     ship_to_customer_id = attribute9,
                     sales_order_source = attribute10,
                     reason_code = attribute11,
                     sales_order_date = attribute12,
                     sales_order_line = attribute13,
                     sales_order_revision = attribute14
             WHERE   customer_trx_id = x_new_customer_trx_id;


            --update the receivale account after creating
            UPDATE   ra_cust_trx_line_gl_dist_all
               SET   code_combination_id = l_trx_rec_ccid_id
             WHERE       customer_trx_id = x_new_customer_trx_id
                     AND account_class = 'REC'
                     AND customer_trx_line_id IS NULL;

            LOG_MSG (
               g_level_statement,
               l_mod_name,
               'Num of Rows Updated with CCID for REC  =' || sql%ROWCOUNT
            );

            --update the receivale account after creating
            UPDATE   ra_cust_trx_line_gl_dist_all
               SET   code_combination_id = attribute15
             WHERE       customer_trx_id = x_new_customer_trx_id
                     AND account_class <> 'REC'
                     AND customer_trx_line_id IS NOT NULL;

            LOG_MSG (
               g_level_statement,
               l_mod_name,
               'Num of Rows Updated with CCID for others=' || sql%ROWCOUNT
            );            
           
         ELSE
            x_return_status := 'E';
            -- k. List errors
            LOG_MSG (g_level_statement,
                     l_mod_name,
                     'FAILURE: Errors encountered, see list below:');

            FOR i IN list_errors
            LOOP
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  '----------------------------------------------------'
               );
               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'Header ID       = ' || TO_CHAR (i.trx_header_id));
               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'Line ID         = ' || TO_CHAR (i.trx_line_id));
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  'Sales Credit ID = ' || TO_CHAR (i.trx_salescredit_id)
               );
               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'Dist Id         = ' || TO_CHAR (i.trx_dist_id));
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  'Contingency ID  = ' || TO_CHAR (i.trx_contingency_id)
               );
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  'Message         = ' || SUBSTR (i.error_message, 1, 80)
               );
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  'Invalid Value   = ' || SUBSTR (i.invalid_value, 1, 80)
               );
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  '----------------------------------------------------'
               );
            END LOOP;
         END IF;
      END IF;
   EXCEPTION
      WHEN API_FAILURE
      THEN
         x_return_status := 'E';
      WHEN VALIDATION_ERROR
      THEN
         x_return_status := 'E';
      WHEN OTHERS
      THEN
         x_return_status := 'E';
         x_error_msg := SQLERRM;
         x_error_count := 1;
   END create_new_trx;

   PROCEDURE apply_credit_memo (
      p_cm_customer_trx_id   IN            NUMBER,
      p_dm_customer_trx_id   IN            NUMBER,
      x_return_status           OUT NOCOPY VARCHAR2,
      x_error_msg               OUT NOCOPY VARCHAR2,
      x_error_count             OUT NOCOPY NUMBER
   )
   IS
      k_api_version CONSTANT        NUMBER := 1;
      k_init_msg_list CONSTANT      VARCHAR2 (1) := FND_API.g_false;
      k_comments                    ar_receivable_applications.comments%TYPE;
      k_commit CONSTANT             VARCHAR2 (1) := FND_API.g_false;

      l_acctd_amount_applied_from   ar_receivable_applications_all.acctd_amount_applied_from%TYPE;
      l_acctd_amount_applied_to     ar_receivable_applications_all.acctd_amount_applied_to%TYPE;
      l_cm_app_rec                  AR_CM_API_PUB.cm_app_rec_type;
      --l_msg_count                   NUMBER;
      --l_msg_data                    VARCHAR2 (255);
      l_out_rec_application_id      NUMBER;
      --l_return_status               VARCHAR2 (1);
      l_amount_due_remaining        NUMBER;
      l_mod_name VARCHAR2 (100)
            := 'xxwc_credit_copy_trx_pkg.apply_credit_memo :' ;

      API_FAILURE EXCEPTION;
      VALIDATION_ERROR EXCEPTION;
   BEGIN
      log_msg (g_level_statement, l_mod_name, 'At Begin');

      k_comments :=
         NVL (fnd_profile.VALUE ('XXWC_CREDIT_REBILL_COMMENTS'),
              'Ship To Adjustment');

      --get the total amount remaining,tax remaining,freight remaining
      SELECT   SUM (amount_due_remaining)
        INTO   l_amount_due_remaining
        FROM   apps.ar_payment_schedules ct
       WHERE   ct.customer_trx_id = p_dm_customer_trx_id;

      log_msg (g_level_statement,
               l_mod_name,
               'l_amount_due_remaining =>' || l_amount_due_remaining);

      l_cm_app_rec.cm_customer_trx_id := p_cm_customer_trx_id;
      l_cm_app_rec.cm_trx_number := NULL;                -- Credit Memo Number
      l_cm_app_rec.inv_customer_trx_id := p_dm_customer_trx_id;
      l_cm_app_rec.inv_trx_number := NULL;                   -- Invoice Number
      l_cm_app_rec.installment := NULL;
      l_cm_app_rec.applied_payment_schedule_id := NULL;
      l_cm_app_rec.amount_applied := l_amount_due_remaining;
      l_cm_app_rec.apply_date := TRUNC (SYSDATE);
      l_cm_app_rec.gl_date := TRUNC (SYSDATE);
      l_cm_app_rec.inv_customer_trx_line_id := NULL;
      l_cm_app_rec.inv_line_number := NULL;
      l_cm_app_rec.show_closed_invoices := NULL;
      l_cm_app_rec.ussgl_transaction_code := NULL;
      l_cm_app_rec.attribute_category := NULL;
      l_cm_app_rec.attribute1 := NULL;
      l_cm_app_rec.attribute2 := NULL;
      l_cm_app_rec.attribute3 := NULL;
      l_cm_app_rec.attribute4 := NULL;
      l_cm_app_rec.attribute5 := NULL;
      l_cm_app_rec.attribute6 := NULL;
      l_cm_app_rec.attribute7 := NULL;
      l_cm_app_rec.attribute8 := NULL;
      l_cm_app_rec.attribute9 := NULL;
      l_cm_app_rec.attribute10 := NULL;
      l_cm_app_rec.attribute11 := NULL;
      l_cm_app_rec.attribute12 := NULL;
      l_cm_app_rec.attribute13 := NULL;
      l_cm_app_rec.attribute14 := NULL;
      l_cm_app_rec.attribute15 := NULL;
      l_cm_app_rec.global_attribute_category := NULL;
      l_cm_app_rec.global_attribute1 := NULL;
      l_cm_app_rec.global_attribute2 := NULL;
      l_cm_app_rec.global_attribute3 := NULL;
      l_cm_app_rec.global_attribute4 := NULL;
      l_cm_app_rec.global_attribute5 := NULL;
      l_cm_app_rec.global_attribute6 := NULL;
      l_cm_app_rec.global_attribute7 := NULL;
      l_cm_app_rec.global_attribute8 := NULL;
      l_cm_app_rec.global_attribute9 := NULL;
      l_cm_app_rec.global_attribute10 := NULL;
      l_cm_app_rec.global_attribute11 := NULL;
      l_cm_app_rec.global_attribute12 := NULL;
      l_cm_app_rec.global_attribute12 := NULL;
      l_cm_app_rec.global_attribute14 := NULL;
      l_cm_app_rec.global_attribute15 := NULL;
      l_cm_app_rec.global_attribute16 := NULL;
      l_cm_app_rec.global_attribute17 := NULL;
      l_cm_app_rec.global_attribute18 := NULL;
      l_cm_app_rec.global_attribute19 := NULL;
      l_cm_app_rec.global_attribute20 := NULL;
      l_cm_app_rec.comments := K_comments;
      l_cm_app_rec.called_from := NULL;

      log_msg (g_level_statement,
               l_mod_name,
               'ar_cm_api_pub.apply_on_account');
      ar_cm_api_pub.apply_on_account (
         p_api_version                 => K_api_version,
         p_init_msg_list               => K_init_msg_list,
         p_commit                      => K_commit,
         p_cm_app_rec                  => l_cm_app_rec,
         x_return_status               => x_return_status,
         x_msg_count                   => x_error_count,
         x_msg_data                    => x_error_msg,
         x_out_rec_application_id      => l_out_rec_application_id,
         x_acctd_amount_applied_from   => l_acctd_amount_applied_from,
         x_acctd_amount_applied_to     => l_acctd_amount_applied_to
      );

      log_msg (
         g_level_statement,
         l_mod_name,
         'x_return_status: (ar_cm_api_pub.apply_on_account) =>'
         || x_return_status
      );
      log_msg (g_level_statement,
               l_mod_name,
               'x_error_count: ' || x_error_count);
      log_msg (g_level_statement,
               l_mod_name,
               'out_rec_application_id: ' || l_out_rec_application_id);
      log_msg (g_level_statement,
               l_mod_name,
               'acctd_amount_applied_from: ' || l_acctd_amount_applied_from);
      log_msg (g_level_statement,
               l_mod_name,
               'acctd_amount_applied_to: ' || l_acctd_amount_applied_to);


      IF x_error_count = 1
      THEN
         log_msg (g_level_statement, l_mod_name, x_error_msg);
      ELSIF x_error_count > 1
      THEN
         FOR I IN 1 .. x_error_count
         LOOP
            log_msg (
               g_level_statement,
               l_mod_name,
               I || '. '
               || SUBSTR (FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE),
                          1,
                          255)
            );
         END LOOP;
      END IF;
   EXCEPTION
      WHEN API_FAILURE
      THEN
         NULL;
      WHEN VALIDATION_ERROR
      THEN
         x_return_status := 'E';
      WHEN OTHERS
      THEN
         x_return_status := 'E';
         x_error_msg := SQLERRM;
         x_error_count := 1;
   END apply_credit_memo;

   PROCEDURE create_cm_trx (p_customer_trx_id       IN            NUMBER,
                            p_bill_to_site_use_id   IN            NUMBER,
                            p_ship_to_site_use_id   IN            NUMBER,
                            p_reason                IN            VARCHAR2,
                            x_new_customer_trx_id      OUT NOCOPY NUMBER,
                            x_return_status            OUT NOCOPY VARCHAR2,
                            x_error_msg                OUT NOCOPY VARCHAR2,
                            x_error_count              OUT NOCOPY NUMBER)
   IS
      l_mod_name VARCHAR2 (100)
            := 'xxwc_credit_copy_trx_pkg.create_cm_trx :' ;

      l_batch_source_rec          ar_invoice_api_pub.batch_source_rec_type;
      l_trx_header_tbl            ar_invoice_api_pub.trx_header_tbl_type;
      l_trx_lines_tbl             ar_invoice_api_pub.trx_line_tbl_type;
      l_trx_dist_tbl              ar_invoice_api_pub.trx_dist_tbl_type;
      l_trx_salescredits_tbl      ar_invoice_api_pub.trx_salescredits_tbl_type;
      --l_customer_trx_id        NUMBER;
      p_count                     NUMBER := 0;
      l_trx_header_id             NUMBER;
      l_trx_line_id               NUMBER;
      l_trx_dist_id               NUMBER;
      l_rebill_batch_source_id    NUMBER;
      l_hdr_cnt                   NUMBER;
      l_line_cnt                  NUMBER;
      l_dist_cnt                  NUMBER;
      l_cnt                       NUMBER;
      l_tax_trx_line_id           NUMBER;
      l_tax_trx_dist_id           NUMBER;
      l_gl_date                   DATE;
      l_frt_trx_line_id           NUMBER;
      l_first_trx_line_id         NUMBER;
      l_tax_rate_id               NUMBER;
      l_comments                  VARCHAR2 (100);
      l_trx_rec_ccid_id           NUMBER;
      l_total_lines               NUMBER; -- Added by Shankar 23-Feb-2012

      l_tax_jurisdiction_code     zx_lines_v.tax_jurisdiction_code%TYPE;
      l_tax_rate_code             zx_lines_v.tax_rate_code%TYPE;
      l_tax_status_code           zx_lines_v.tax_status_code%TYPE;
      l_tax_regime_code           zx_lines_v.tax_regime_code%TYPE;
      l_tax                       zx_lines_v.tax%TYPE;
      l_reason_code               ra_customer_trx_all.reason_code%TYPE;
      l_customer_reference        ra_customer_trx_all.customer_reference%TYPE;
      l_customer_reference_date   ra_customer_trx_all.customer_reference_date%TYPE;

      l_purchase_order            ra_customer_trx_all.purchase_order%TYPE;
      l_purchase_order_date       ra_customer_trx_all.purchase_order_date%TYPE;
      l_purchase_order_revision   ra_customer_trx_all.purchase_order_revision%TYPE;
      l_fob_point                 ra_customer_trx_all.fob_point%TYPE;

      l_ct_reference              ra_customer_trx_all.ct_reference%TYPE;
      l_doc_sequence_value        ra_customer_trx_all.doc_sequence_value%TYPE;
      l_term_due_date             DATE;
      l_related_customer_trx_id   NUMBER;
      l_note_id                   NUMBER;

      API_FAILURE EXCEPTION;
      VALIDATION_ERROR EXCEPTION;
      --Added below by Maha for ver 1.3 < Start
l_iface_hdr_attribute1      ra_customer_trx_all.interface_header_attribute1%type;
l_iface_hdr_attribute2      ra_customer_trx_all.interface_header_attribute2%type;
l_iface_hdr_attribute3      ra_customer_trx_all.interface_header_attribute3%type;
l_iface_hdr_attribute4      ra_customer_trx_all.interface_header_attribute4%type;
l_iface_hdr_attribute5      ra_customer_trx_all.interface_header_attribute5%type;
l_iface_hdr_attribute6      ra_customer_trx_all.interface_header_attribute6%type;
l_iface_hdr_attribute7      ra_customer_trx_all.interface_header_attribute7%type;
l_iface_hdr_attribute8      ra_customer_trx_all.interface_header_attribute8%type;
l_iface_hdr_attribute9      ra_customer_trx_all.interface_header_attribute9%type;
l_iface_hdr_attribute10      ra_customer_trx_all.interface_header_attribute10%type;
l_iface_hdr_attribute11      ra_customer_trx_all.interface_header_attribute11%type;
l_iface_hdr_attribute12      ra_customer_trx_all.interface_header_attribute12%type;
l_iface_hdr_attribute13      ra_customer_trx_all.interface_header_attribute13%type;
l_iface_hdr_attribute14      ra_customer_trx_all.interface_header_attribute14%type;
l_iface_hdr_attribute15     ra_customer_trx_all.interface_header_attribute15%type;
-- End >
      CURSOR cur_header
      IS
         SELECT   *
           FROM   apps.ra_customer_trx
          WHERE   customer_trx_id = p_customer_trx_id;

      CURSOR cur_lines
      IS
         SELECT   *
           FROM   apps.ra_customer_trx_lines
          WHERE       customer_trx_id = p_customer_trx_id
                  AND line_type = 'LINE'
                  AND link_to_cust_trx_line_id IS NULL;


      CURSOR cur_tax_lines (
         p_customer_trx_line_id   IN            NUMBER
      )
      IS
         SELECT   *
           FROM   apps.ra_customer_trx_lines
          WHERE   link_to_cust_trx_line_id = p_customer_trx_line_id
                  AND line_type IN ('TAX');

      CURSOR list_errors
      IS
         SELECT   trx_header_id,
                  trx_line_id,
                  trx_salescredit_id,
                  trx_dist_id,
                  trx_contingency_id,
                  error_message,
                  invalid_value
           FROM   apps.ar_trx_errors_gt;


      CURSOR cur_frt_hdr
      IS
         SELECT   *
           FROM   apps.ra_customer_trx_lines
          WHERE       customer_trx_id = p_customer_trx_id
                  AND line_type = 'FREIGHT'
                  AND link_to_cust_trx_line_id IS NULL;


      CURSOR cur_dist (p_customer_trx_line_id IN NUMBER)
      IS
         SELECT   *
           FROM   apps.ra_cust_trx_line_gl_dist
          WHERE   customer_trx_line_id = p_customer_trx_line_id;


      CURSOR cur_rec_dist
      IS
         SELECT   *
           FROM   apps.ra_cust_trx_line_gl_dist
          WHERE       customer_trx_id = p_customer_trx_id
                  AND account_class = 'REC'
                  AND customer_trx_line_id IS NULL;

      CURSOR cur_notes
      IS
         SELECT   *
           FROM   apps.ar_notes
          WHERE   customer_trx_id = p_customer_trx_id;
   BEGIN
   
      fnd_file.put_line (fnd_file.LOG, 'Create CM trx prc for PRISM invoices');

      --Get the Rebill batch source
      l_rebill_batch_source_id :=
         fnd_profile.VALUE ('XXWC_CREDIT_REBILL_BATCH_SOURCE');

      --Raise error message if the batch source not found
      IF l_rebill_batch_source_id IS NULL
      THEN
         FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_AR_TRX_SOURCE_MISSING');
         FND_MSG_PUB.ADD;
         RAISE VALIDATION_ERROR;
      END IF;

      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_rebill_batch_source_id =>' || l_rebill_batch_source_id);

      -- Populate batch source information.
      l_batch_source_rec.batch_source_id := l_rebill_batch_source_id;

      l_hdr_cnt := 1;

      --Get the commments for the credit transaction
      l_comments :=
         NVL (fnd_profile.VALUE ('XXWC_CREDIT_REBILL_COMMENTS'),
              'Ship To Adjustment');

      --Delete the plsql table for headers,lines,distributions
      l_trx_header_tbl.delete;
      l_trx_lines_tbl.delete;
      l_trx_dist_tbl.delete;

      FOR i IN cur_header
      LOOP
         SELECT   RA_CUSTOMER_TRX_S.NEXTVAL INTO l_trx_header_id FROM DUAL;

         l_gl_date := sysdate;
           -- XXWC_CREDIT_COPY_TRX_UTIL_PKG.get_gl_date (p_customer_trx_id);
           --Commented by Shankar 14-Mar-2012 per Bob's email
         LOG_MSG (g_level_statement, l_mod_name, 'l_gl_date =>' || l_gl_date);

         -- Populate header information.
         l_trx_header_tbl (l_hdr_cnt).trx_header_id := l_trx_header_id;
         l_trx_header_tbl (l_hdr_cnt).trx_number := i.trx_number;
         l_trx_header_tbl (l_hdr_cnt).trx_date := i.trx_date;
         l_trx_header_tbl (l_hdr_cnt).gl_date := l_gl_date;
         l_trx_header_tbl (l_hdr_cnt).cust_trx_type_id := i.cust_trx_type_id;
         l_trx_header_tbl (l_hdr_cnt).bill_to_customer_id :=
            i.bill_to_customer_id;
         l_trx_header_tbl (l_hdr_cnt).bill_to_site_use_id :=
            p_bill_to_site_use_id;
         l_trx_header_tbl (l_hdr_cnt).bill_to_contact_id :=
            i.bill_to_contact_id;

         l_trx_header_tbl (l_hdr_cnt).ship_to_customer_id :=
            NVL (i.ship_to_customer_id, i.bill_to_customer_id);
         l_trx_header_tbl (l_hdr_cnt).ship_to_site_use_id :=
            p_ship_to_site_use_id;
         l_trx_header_tbl (l_hdr_cnt).ship_to_contact_id :=
            i.ship_to_contact_id;
         l_trx_header_tbl (1).primary_salesrep_id := i.primary_salesrep_id;

         l_trx_header_tbl (1).paying_customer_id := i.paying_customer_id;
         l_trx_header_tbl (1).paying_site_use_id := i.paying_site_use_id;
         l_trx_header_tbl (1).sold_to_customer_id := i.sold_to_customer_id;
         l_trx_header_tbl (1).remit_to_address_id := i.remit_to_address_id;
         l_trx_header_tbl (1).term_id := i.term_id;
         l_trx_header_tbl (1).comments := l_comments;
         l_trx_header_tbl (1).internal_notes := i.internal_notes;
         l_trx_header_tbl (1).receipt_method_id := i.receipt_method_id;

         l_trx_header_tbl (1).ship_via := i.ship_via;
         l_trx_header_tbl (1).ship_date_actual := i.ship_date_actual;
         l_trx_header_tbl (1).waybill_number := i.waybill_number;
         l_fob_point := i.fob_point;

         l_reason_code := i.reason_code;
         l_customer_reference := i.customer_reference;
         l_customer_reference_date := i.customer_reference_date;
         l_purchase_order := i.purchase_order;
         l_purchase_order_date := i.purchase_order_date;
         l_purchase_order_revision := i.purchase_order_revision;

         --l_term_due_date := i.term_due_date;
         l_ct_reference := i.ct_reference;
         l_doc_sequence_value := i.doc_sequence_value;
         l_related_customer_trx_id := i.related_customer_trx_id;
         --Added by Maha for ver 1.3 < Start
         l_trx_header_tbl (1).attribute_category := i.attribute_category;
         l_trx_header_tbl (1).attribute1 := i.attribute1;
         l_trx_header_tbl (1).attribute2 := i.attribute2;
         l_trx_header_tbl (1).attribute3 := i.attribute3;
         l_trx_header_tbl (1).attribute4 := i.attribute4;
         l_trx_header_tbl (1).attribute5 := i.attribute5;
         l_trx_header_tbl (1).attribute6 := i.attribute6;
         l_trx_header_tbl (1).attribute7 := i.attribute7;
         l_trx_header_tbl (1).attribute8 := i.attribute8;
         l_trx_header_tbl (1).attribute9 := i.attribute9;
         l_trx_header_tbl (1).attribute10 := i.attribute10;
         l_trx_header_tbl (1).attribute11 := i.attribute11;
         l_trx_header_tbl (1).attribute12 := null;--i.attribute12;--changed for ver 1.9 by Neha
         l_trx_header_tbl (1).attribute13 := i.attribute13;
         l_trx_header_tbl (1).attribute14 := i.attribute14;
         l_trx_header_tbl (1).attribute15 := i.attribute15;
         
         l_trx_header_tbl (1).interface_header_context := i.interface_header_context;
         -- End >
         
         l_hdr_cnt := l_hdr_cnt + 1;
      END LOOP;

      l_line_cnt := 1;
      l_dist_cnt := 1;

      l_first_trx_line_id := NULL;

-- Added by Shankar for issue 45 23-Feb-2011
         SELECT   count(*)
           INTO   l_total_lines
           FROM   apps.ra_customer_trx_lines
          WHERE   customer_trx_id = p_customer_trx_id 
            AND line_type = 'LINE';
            
--Added below block by Maha for ver 1.3        < Start
BEGIN
   SELECT interface_header_attribute1,
         interface_header_attribute2,
         interface_header_attribute3, 
         interface_header_attribute4,
         interface_header_attribute5,
          interface_header_attribute6,
          interface_header_attribute7,
          interface_header_attribute8,
          interface_header_attribute9,
          interface_header_attribute10,
          interface_header_attribute11,
          interface_header_attribute12,
          interface_header_attribute13,
          interface_header_attribute14,
          interface_header_attribute15
     INTO l_iface_hdr_attribute1,
          l_iface_hdr_attribute2,
           l_iface_hdr_attribute3,
          l_iface_hdr_attribute4,
           l_iface_hdr_attribute5,
          l_iface_hdr_attribute6,
          l_iface_hdr_attribute7,
          l_iface_hdr_attribute8,
          l_iface_hdr_attribute9,
          l_iface_hdr_attribute10,
          l_iface_hdr_attribute11,
          l_iface_hdr_attribute12,
          l_iface_hdr_attribute13,
          l_iface_hdr_attribute14,
          l_iface_hdr_attribute15
     FROM apps.ra_customer_trx
    WHERE customer_trx_id = p_customer_trx_id;
EXCEPTION
   WHEN OTHERS
   THEN
   NULL;
END;
-- End>

      FOR k IN cur_lines
      LOOP
         SELECT   RA_CUSTOMER_TRX_LINES_S.NEXTVAL INTO l_trx_line_id FROM DUAL;

         IF l_first_trx_line_id IS NULL
         THEN
            l_first_trx_line_id := l_trx_line_id;
         END IF;

         -- Populate lines information.
         l_trx_lines_tbl (l_line_cnt).trx_header_id := l_trx_header_id;
         l_trx_lines_tbl (l_line_cnt).trx_line_id := l_trx_line_id;
         l_trx_lines_tbl (l_line_cnt).line_number := k.line_number;
         l_trx_lines_tbl (l_line_cnt).inventory_item_id := k.inventory_item_id;
         l_trx_lines_tbl (l_line_cnt).description := k.description;
         l_trx_lines_tbl (l_line_cnt).reason_code := k.reason_code;
         --l_trx_lines_tbl (l_line_cnt).quantity_credited := k.quantity_credited;
         l_trx_lines_tbl (l_line_cnt).quantity_ordered := k.quantity_ordered;
         --l_trx_lines_tbl (l_line_cnt).unit_selling_price := k.unit_selling_price;
         l_trx_lines_tbl (l_line_cnt).amount := k.extended_amount;
         l_trx_lines_tbl (l_line_cnt).line_type := k.line_type;
         --l_trx_lines_tbl (l_line_cnt).uom_code := k.uom_code;
         l_trx_lines_tbl (l_line_cnt).amount_includes_tax_flag := 'N';
         l_trx_lines_tbl (l_line_cnt).taxable_flag := 'N';

         l_trx_lines_tbl (l_line_cnt).attribute1 := k.attribute1;
         l_trx_lines_tbl (l_line_cnt).attribute2 := k.attribute2;
         l_trx_lines_tbl (l_line_cnt).attribute3 := k.translated_description;
         l_trx_lines_tbl (l_line_cnt).attribute4 := k.ship_to_contact_id;
         l_trx_lines_tbl (l_line_cnt).attribute5 := k.ship_to_site_use_id;
         l_trx_lines_tbl (l_line_cnt).attribute6 := k.ship_to_customer_id;
         l_trx_lines_tbl (l_line_cnt).attribute7 := k.sales_order_source;
         l_trx_lines_tbl (l_line_cnt).attribute8 := k.reason_code;
         l_trx_lines_tbl (l_line_cnt).attribute9 := k.sales_order_date;
         l_trx_lines_tbl (l_line_cnt).attribute10 := k.sales_order_line;

         l_trx_lines_tbl (l_line_cnt).attribute11 := k.sales_order_revision;
         l_trx_lines_tbl (l_line_cnt).attribute12 := k.sales_order;
         l_trx_lines_tbl (l_line_cnt).attribute13 := k.unit_selling_price;
         l_trx_lines_tbl (l_line_cnt).attribute14 := k.quantity_credited;
         l_trx_lines_tbl (l_line_cnt).attribute15 := k.uom_code;


         l_trx_lines_tbl (l_line_cnt).warehouse_id := k.warehouse_id;
         
          --Added below by Maha for ver 1.3 < Start
          l_trx_lines_tbl (l_line_cnt).tax_classification_code   := k.tax_classification_code;
          l_trx_lines_tbl (l_line_cnt).unit_standard_price          := k.unit_standard_price;
          l_trx_lines_tbl (l_line_cnt).interface_line_context       := k.interface_line_context;
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute1    := trim(k.interface_line_attribute1);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute2    := trim(k.interface_line_attribute2);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute3    := trim(k.interface_line_attribute3);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute4    := trim(k.interface_line_attribute4);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute5    := trim(k.interface_line_attribute5);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute6    := trim(k.interface_line_attribute6)||'00'; --Appended 99 to handle DFF validation
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute7    := trim(k.interface_line_attribute7);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute8    := trim(k.interface_line_attribute8);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute9    := trim(k.interface_line_attribute9);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute10   := trim(k.interface_line_attribute10);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute11   := trim(k.interface_line_attribute11);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute12   := trim(k.interface_line_attribute12);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute13   := trim(k.interface_line_attribute13);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute14   := trim(k.interface_line_attribute14);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute15   := trim(k.interface_line_attribute15);
          l_trx_lines_tbl (l_line_cnt).tax_exempt_flag              := trim(k.tax_exempt_flag);
 --End>

         FOR l IN cur_dist (k.customer_trx_line_id)
         LOOP
            SELECT   ra_cust_trx_line_gl_dist_s.NEXTVAL
              INTO   l_trx_dist_id
              FROM   DUAL;

            -- Populate Distribution Information
            l_trx_dist_tbl (l_dist_cnt).trx_dist_id := l_trx_dist_id;
            l_trx_dist_tbl (l_dist_cnt).trx_header_id := l_trx_header_id;
            l_trx_dist_tbl (l_dist_cnt).trx_line_id := l_trx_line_id;
            l_trx_dist_tbl (l_dist_cnt).account_class := l.account_class;
            l_trx_dist_tbl (l_dist_cnt).amount := l.amount;
            l_trx_dist_tbl (l_dist_cnt).code_combination_id :=
               l.code_combination_id;
            l_dist_cnt := l_dist_cnt + 1;
         END LOOP;

         FOR m IN cur_tax_lines (k.customer_trx_line_id)
         LOOP
            SELECT   RA_CUSTOMER_TRX_LINES_S.NEXTVAL
              INTO   l_tax_trx_line_id
              FROM   DUAL;

            l_line_cnt := l_line_cnt + 1;

            BEGIN
               SELECT   tax,
                        tax_rate_id,
                        tax_jurisdiction_code,
                        tax_rate_code,
                        tax_status_code,
                        tax_regime_code
                 INTO   l_tax,
                        l_tax_rate_id,
                        l_tax_jurisdiction_code,
                        l_tax_rate_code,
                        l_tax_status_code,
                        l_tax_regime_code
                 FROM   apps.zx_lines_v
                WHERE   tax_line_id = m.tax_line_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_tax_rate_id := NULL;
                  l_tax_jurisdiction_code := NULL;
                  l_tax_rate_code := NULL;
                  l_tax_status_code := NULL;
                  l_tax_regime_code := NULL;
            END;
            l_total_lines := l_total_lines + 1;
            -- Populate tax lines information.
            l_trx_lines_tbl (l_line_cnt).trx_header_id := l_trx_header_id;
            l_trx_lines_tbl (l_line_cnt).trx_line_id := l_tax_trx_line_id;
            l_trx_lines_tbl (l_line_cnt).link_to_trx_line_id := l_trx_line_id;
            l_trx_lines_tbl (l_line_cnt).line_number := l_total_lines; --k.line_number; Shankar
            l_trx_lines_tbl (l_line_cnt).line_type := m.line_type;
            l_trx_lines_tbl (l_line_cnt).amount := m.extended_amount;
            l_trx_lines_tbl (l_line_cnt).tax := l_tax;
            l_trx_lines_tbl (l_line_cnt).vat_tax_id := m.vat_tax_id;
            l_trx_lines_tbl (l_line_cnt).tax_rate := m.tax_rate;
            l_trx_lines_tbl (l_line_cnt).tax_jurisdiction_code :=
               l_tax_jurisdiction_code;
            l_trx_lines_tbl (l_line_cnt).tax_rate_code := l_tax_rate_code;
            l_trx_lines_tbl (l_line_cnt).tax_status_code := l_tax_status_code;
            l_trx_lines_tbl (l_line_cnt).tax_regime_code := l_tax_regime_code;
            l_trx_lines_tbl (l_line_cnt).amount_includes_tax_flag := NULL;
            l_trx_lines_tbl (l_line_cnt).taxable_flag := NULL;

            FOR n IN cur_dist (m.customer_trx_line_id)
            LOOP
               SELECT   ra_cust_trx_line_gl_dist_s.NEXTVAL
                 INTO   l_tax_trx_dist_id
                 FROM   DUAL;

               -- Populate Distribution Information
               l_trx_dist_tbl (l_dist_cnt).trx_dist_id := l_tax_trx_dist_id;
               l_trx_dist_tbl (l_dist_cnt).trx_header_id := l_trx_header_id;
               l_trx_dist_tbl (l_dist_cnt).trx_line_id := l_tax_trx_line_id;
               l_trx_dist_tbl (l_dist_cnt).account_class := n.account_class;
               l_trx_dist_tbl (l_dist_cnt).amount := n.amount;
               l_trx_dist_tbl (l_dist_cnt).code_combination_id :=
                  n.code_combination_id;
               l_dist_cnt := l_dist_cnt + 1;
            END LOOP;
         END LOOP;                                            --Tax Line Types

         l_line_cnt := l_line_cnt + 1;
      END LOOP;                                                  --Lines Types

      /*
            FOR n IN cur_frt_hdr
            LOOP
               SELECT   ra_customer_trx_lines_s.NEXTVAL
                 INTO   l_frt_trx_line_id
                 FROM   DUAL;

               --populate the freight lines
               l_trx_lines_tbl (l_line_cnt).trx_header_id := l_trx_header_id;
               l_trx_lines_tbl (l_line_cnt).trx_line_id := l_frt_trx_line_id;
               --l_trx_lines_tbl (l_line_cnt).link_to_trx_line_id := l_first_trx_line_id;
               l_trx_lines_tbl (l_line_cnt).line_number := n.line_number;
               l_trx_lines_tbl (l_line_cnt).line_type := 'FREIGHT';
               l_trx_lines_tbl (l_line_cnt).amount := n.extended_amount;
               l_line_cnt := l_line_cnt + 1;

               FOR p IN cur_dist (n.customer_trx_line_id)
               LOOP
                  SELECT   ra_cust_trx_line_gl_dist_s.NEXTVAL
                    INTO   l_tax_trx_dist_id
                    FROM   DUAL;

                  -- Populate Distribution Information
                  l_trx_dist_tbl (l_dist_cnt).trx_dist_id := l_tax_trx_dist_id;
                  l_trx_dist_tbl (l_dist_cnt).trx_header_id := l_trx_header_id;
                  l_trx_dist_tbl (l_dist_cnt).trx_line_id := l_frt_trx_line_id;
                  l_trx_dist_tbl (l_dist_cnt).account_class := p.account_class;
                  l_trx_dist_tbl (l_dist_cnt).amount := p.amount;
                  l_trx_dist_tbl (l_dist_cnt).code_combination_id :=
                     p.code_combination_id;
                  l_dist_cnt := l_dist_cnt + 1;
               END LOOP;
            END LOOP;
      */
      -- Populate Distribution lines for receivables type
      FOR rec IN cur_rec_dist
      LOOP
         l_trx_rec_ccid_id := rec.code_combination_id;
      END LOOP;



      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_trx_header_tbl.count =>' || l_trx_header_tbl.COUNT);
      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_trx_lines_tbl.count  =>' || l_trx_lines_tbl.COUNT);
      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_trx_dist_tbl.count   => ' || l_trx_dist_tbl.COUNT);

      FOR p IN l_trx_lines_tbl.FIRST .. l_trx_lines_tbl.LAST
      LOOP
         LOG_MSG (g_level_statement,
                  l_mod_name,
                  '******************************************');
         LOG_MSG (
            g_level_statement,
            l_mod_name,
            'l_trx_lines_tbl.trx_line_id =>'
            || l_trx_lines_tbl (p).trx_line_id
         );
         LOG_MSG (
            g_level_statement,
            l_mod_name,
            'l_trx_lines_tbl.line_type =>' || l_trx_lines_tbl (p).line_type
         );
         LOG_MSG (
            g_level_statement,
            l_mod_name,
            'l_trx_lines_tbl.line_number =>'
            || l_trx_lines_tbl (p).line_number
         );
         LOG_MSG (
            g_level_statement,
            l_mod_name,
            'l_trx_lines_tbl.link_to_trx_line_id =>'
            || l_trx_lines_tbl (p).link_to_trx_line_id
         );
      END LOOP;

      -- call the api
      ar_invoice_api_pub.create_single_invoice (
         p_api_version            => 1.0,
         p_batch_source_rec       => l_batch_source_rec,
         p_trx_header_tbl         => l_trx_header_tbl,
         p_trx_lines_tbl          => l_trx_lines_tbl,
         p_trx_dist_tbl           => l_trx_dist_tbl,
         p_trx_salescredits_tbl   => l_trx_salescredits_tbl,
         x_customer_trx_id        => x_new_customer_trx_id,
         x_return_status          => x_return_status,
         x_msg_count              => x_error_count,
         x_msg_data               => x_error_msg
      );

      LOG_MSG (g_level_statement,
               l_mod_name,
               'x_return_status(create_single_invoice)=' || x_return_status);

      IF x_return_status <> 'S'
      THEN
         LOG_MSG (g_level_statement, l_mod_name, 'unexpected errors found!');

         IF x_error_count = 1
         THEN
            LOG_MSG (g_level_statement,
                     l_mod_name,
                     'x_error_msg ' || x_error_msg);
         ELSIF x_error_count > 1
         THEN
            LOOP
               p_count := p_count + 1;
               x_error_msg :=
                  FND_MSG_PUB.Get (FND_MSG_PUB.G_NEXT, FND_API.G_FALSE);

               IF x_error_msg IS NULL
               THEN
                  EXIT;
               END IF;

               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'Message' || p_count || '.' || x_error_msg);
            END LOOP;
         END IF;
      ELSE
         SELECT   COUNT ( * ) INTO l_cnt FROM ar_trx_errors_gt;

         IF l_cnt = 0
         THEN
            LOG_MSG (
               g_level_statement,
               l_mod_name,
               'SUCCESS: Created customer_trx_id = ' || x_new_customer_trx_id
            );
            LOG_MSG (g_level_statement,
                     l_mod_name,
                     'l_reason_code => ' || l_reason_code);

            FOR p_notes_rec IN cur_notes
            LOOP
               /*------------------------------+
                |  get the unique id            |
                +------------------------------*/

               SELECT   ar_notes_s.NEXTVAL INTO l_note_id FROM DUAL;

               /*------------------------------+
             |  insert the record            |
             +------------------------------*/
               INSERT INTO ar_notes (note_id,
                                     note_type,
                                     text,
                                     customer_trx_id,
                                     customer_call_id,
                                     customer_call_topic_id,
                                     call_action_id,
                                     last_updated_by,
                                     last_update_date,
                                     last_update_login,
                                     created_by,
                                     creation_date)
                 VALUES   (l_note_id,
                           p_notes_rec.note_type,
                           p_notes_rec.text,
                           x_new_customer_trx_id,
                           p_notes_rec.customer_call_id,
                           p_notes_rec.customer_call_topic_id,
                           p_notes_rec.call_action_id,
                           fnd_global.user_id,
                           SYSDATE,
                           fnd_global.login_id,
                           fnd_global.user_id,
                           SYSDATE);

               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'Num of Rows Inserted (Notes) = ' || sql%ROWCOUNT);
            END LOOP;

            --update the receivale account after creating
            UPDATE   ra_cust_trx_line_gl_dist_all
               SET   code_combination_id = l_trx_rec_ccid_id
             WHERE       customer_trx_id = x_new_customer_trx_id
                     AND account_class = 'REC'
                     AND customer_trx_line_id IS NULL;

            LOG_MSG (g_level_statement,
                     l_mod_name,
                     'Num of Rows Updated(Lines) = ' || sql%ROWCOUNT);


            UPDATE   ra_customer_trx_lines_all
               SET   unit_selling_price =
                        DECODE (attribute15, NULL, NULL, attribute13), --unit_selling_price
                     quantity_credited =
                        DECODE (attribute15, NULL, NULL, attribute14), --quantity_credited
                     uom_code = attribute15,                        --uom_code
                     translated_description = attribute3,
                     ship_to_contact_id = attribute4,
                     ship_to_site_use_id = attribute5,
                     ship_to_customer_id = attribute6,
                     sales_order_source = attribute7,
                     reason_code = attribute8,
                     sales_order_date = attribute9,
                     sales_order_line = attribute10,
                     sales_order_revision = attribute11,
                     sales_order = attribute12
             WHERE   customer_trx_id = x_new_customer_trx_id;

            LOG_MSG (g_level_statement,
                     l_mod_name,
                     'Num of Rows Updated(Lines) = ' || sql%ROWCOUNT);

            UPDATE   ra_customer_trx_all
               SET   reason_code = l_reason_code,
                     customer_reference = l_customer_reference,
                     customer_reference_date = l_customer_reference_date,
                     purchase_order = l_purchase_order,
                     purchase_order_date = l_purchase_order_date,
                     purchase_order_revision = l_purchase_order_revision,
                     ct_reference = l_ct_reference,
                     doc_sequence_value = l_doc_sequence_value,
                     related_customer_trx_id = l_related_customer_trx_id,
                     fob_point = l_fob_point,
                     --Added by Maha for ver 1.3 start>
                     interface_header_attribute1 = l_iface_hdr_attribute1,
                     interface_header_attribute2 = l_iface_hdr_attribute2,
                     interface_header_attribute3 = l_iface_hdr_attribute3,
                     interface_header_attribute4 = l_iface_hdr_attribute4,
                     interface_header_attribute5 = l_iface_hdr_attribute5,
                     interface_header_attribute6 = l_iface_hdr_attribute6,
                     interface_header_attribute7 = l_iface_hdr_attribute7,
                     interface_header_attribute8 = l_iface_hdr_attribute8,
                     interface_header_attribute9 = l_iface_hdr_attribute9,
                     interface_header_attribute10 = l_iface_hdr_attribute10,
                     interface_header_attribute11 = l_iface_hdr_attribute11,
                     interface_header_attribute12 = l_iface_hdr_attribute12,
                     interface_header_attribute13 = l_iface_hdr_attribute13,
                     interface_header_attribute14 = l_iface_hdr_attribute14,
                     interface_header_attribute15 = l_iface_hdr_attribute15
                     --End >
             WHERE   customer_trx_id = x_new_customer_trx_id;

            LOG_MSG (g_level_statement,
                     l_mod_name,
                     'Num of Rows Updated(Header) = ' || sql%ROWCOUNT);
         ELSE
            x_return_status := 'E';
            -- k. List errors
            LOG_MSG (g_level_statement,
                     l_mod_name,
                     'FAILURE: Errors encountered, see list below:');

            FOR i IN list_errors
            LOOP
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  '----------------------------------------------------'
               );
               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'Header ID       = ' || TO_CHAR (i.trx_header_id));
               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'Line ID         = ' || TO_CHAR (i.trx_line_id));
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  'Sales Credit ID = ' || TO_CHAR (i.trx_salescredit_id)
               );
               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'Dist Id         = ' || TO_CHAR (i.trx_dist_id));
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  'Contingency ID  = ' || TO_CHAR (i.trx_contingency_id)
               );
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  'Message         = ' || SUBSTR (i.error_message, 1, 80)
               );
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  'Invalid Value   = ' || SUBSTR (i.invalid_value, 1, 80)
               );
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  '----------------------------------------------------'
               );
            END LOOP;
         END IF;
      END IF;
   EXCEPTION
      WHEN API_FAILURE
      THEN
         NULL;
      WHEN VALIDATION_ERROR
      THEN
         x_return_status := 'E';
      WHEN OTHERS
      THEN
         x_return_status := 'E';
         x_error_msg := SQLERRM;
         x_error_count := 1;
   END create_cm_trx;

   PROCEDURE reverse_cm_trx (p_customer_trx_id       IN            NUMBER,
                             p_bill_to_site_use_id   IN            NUMBER,
                             p_ship_to_site_use_id   IN            NUMBER,
                             p_reason                IN            VARCHAR2,
                             x_new_customer_trx_id      OUT NOCOPY NUMBER,
                             x_return_status            OUT NOCOPY VARCHAR2,
                             x_error_msg                OUT NOCOPY VARCHAR2,
                             x_error_count              OUT NOCOPY NUMBER)
   IS
      l_mod_name VARCHAR2 (100)
            := 'xxwc_credit_copy_trx_pkg.reverse_cm_trx :' ;

      l_batch_source_rec          ar_invoice_api_pub.batch_source_rec_type;
      l_trx_header_tbl            ar_invoice_api_pub.trx_header_tbl_type;
      l_trx_lines_tbl             ar_invoice_api_pub.trx_line_tbl_type;
      l_trx_dist_tbl              ar_invoice_api_pub.trx_dist_tbl_type;
      l_trx_salescredits_tbl      ar_invoice_api_pub.trx_salescredits_tbl_type;

      p_count                     NUMBER := 0;
      l_trx_header_id             NUMBER;
      l_trx_line_id               NUMBER;
      l_trx_dist_id               NUMBER;
      l_batch_source_id           NUMBER;
      l_hdr_cnt                   NUMBER;
      l_line_cnt                  NUMBER;
      l_dist_cnt                  NUMBER;
      l_cnt                       NUMBER;
      l_tax_trx_line_id           NUMBER;
      l_tax_trx_dist_id           NUMBER;
      l_dm_cust_trx_type_id       NUMBER;
      l_frt_trx_line_id           NUMBER;
      l_first_trx_line_id         NUMBER;
      l_comments                  VARCHAR2 (100);
      l_trx_rec_ccid_id           NUMBER;
      l_total_lines               NUMBER; -- Added by Shankar 23-Feb-2012

      l_trx_number                ra_customer_trx_all.trx_number%TYPE;
      l_tax_rate_id               NUMBER;
      l_tax_jurisdiction_code     zx_lines_v.tax_jurisdiction_code%TYPE;
      l_tax_rate_code             zx_lines_v.tax_rate_code%TYPE;
      l_tax_status_code           zx_lines_v.tax_status_code%TYPE;
      l_tax_regime_code           zx_lines_v.tax_regime_code%TYPE;
      l_tax                       zx_lines_v.tax%TYPE;

      l_auto_trx_numbering_flag   ra_batch_sources.auto_trx_numbering_flag%TYPE;
      --Added below by Maha for ver 1.4
      l_interface_hdr_context     ra_customer_trx.interface_header_context%TYPE;
      l_interface_hdr_attribute7  ra_customer_trx.interface_header_attribute7%TYPE;
      l_interface_hdr_attribute8  ra_customer_trx.interface_header_attribute8%TYPE;
      l_interface_hdr_attribute9  ra_customer_trx.interface_header_attribute9%TYPE;
      l_interface_hdr_attribute10 ra_customer_trx.interface_header_attribute10%TYPE;
      l_interface_hdr_attribute11 ra_customer_trx.interface_header_attribute11%TYPE;
      l_interface_hdr_attribute12 ra_customer_trx.interface_header_attribute12%TYPE;

      API_FAILURE EXCEPTION;
      VALIDATION_ERROR EXCEPTION;


      CURSOR cur_header
      IS
         SELECT   *
           FROM   apps.ra_customer_trx
          WHERE   customer_trx_id = p_customer_trx_id;

      CURSOR cur_lines
      IS
         SELECT   *
           FROM   apps.ra_customer_trx_lines
          WHERE   customer_trx_id = p_customer_trx_id AND line_type = 'LINE';

      CURSOR list_errors
      IS
         SELECT   trx_header_id,
                  trx_line_id,
                  trx_salescredit_id,
                  trx_dist_id,
                  trx_contingency_id,
                  error_message,
                  invalid_value
           FROM   apps.ar_trx_errors_gt;

      CURSOR cur_tax_lines (
         p_customer_trx_line_id   IN            NUMBER
      )
      IS
         SELECT   *
           FROM   apps.ra_customer_trx_lines
          WHERE   link_to_cust_trx_line_id = p_customer_trx_line_id
                  AND line_type IN ('TAX', 'FREIGHT');

      CURSOR cur_dist (p_customer_trx_line_id IN NUMBER)
      IS
         SELECT   *
           FROM   apps.ra_cust_trx_line_gl_dist
          WHERE   customer_trx_line_id = p_customer_trx_line_id;

      CURSOR cur_frt_hdr
      IS
         SELECT   *
           FROM   apps.ra_customer_trx_lines
          WHERE       customer_trx_id = p_customer_trx_id
                  AND line_type = 'FREIGHT'
                  AND link_to_cust_trx_line_id IS NULL;

      CURSOR cur_rec_dist
      IS
         SELECT   *
           FROM   apps.ra_cust_trx_line_gl_dist
          WHERE       customer_trx_id = p_customer_trx_id
                  AND account_class = 'REC'
                  AND customer_trx_line_id IS NULL;
   BEGIN
   
    fnd_file.put_line (fnd_file.LOG, 'Create Reverse CM trx prc for PRISM invoices');
      --Get the Debit memo transaction type
      l_dm_cust_trx_type_id :=
         fnd_profile.VALUE ('XXWC_CREDIT_REBILL_DM_TRX_TYPE');

      IF l_dm_cust_trx_type_id IS NULL
      THEN
         FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_AR_DM_TRX_TYPE_MISSING');
         FND_MSG_PUB.ADD;
         RAISE VALIDATION_ERROR;
      END IF;

      --Get the commments for the credit transaction
      l_comments :=
         NVL (fnd_profile.VALUE ('XXWC_CREDIT_REBILL_COMMENTS'),
              'Ship To Adjustment');

      --Get the batch source
      l_batch_source_id :=
         FND_PROFILE.VALUE ('XXWC_CREDIT_REBILL_CM_BATCH_SOURCE');

      /*
               xxwc_credit_copy_trx_util_pkg.get_batch_source_id (
                  p_customer_trx_id
               );
      */

      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_batch_source_id =>' || l_batch_source_id);

      --Raise error message if the batch source not found
      IF l_batch_source_id IS NULL
      THEN
         FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_AR_TRX_SOURCE_MISSING');
         FND_MSG_PUB.ADD;
         RAISE VALIDATION_ERROR;
      END IF;

      -- Populate batch source information.
      l_batch_source_rec.batch_source_id := l_batch_source_id;
      
      --Added below by Maha for ver 1.4 <Start
       BEGIN
         SELECT   interface_header_context,
                  interface_header_attribute7,
                  interface_header_attribute8,
                  interface_header_attribute9,
                  interface_header_attribute10,
                  interface_header_attribute11,
                  interface_header_attribute12
           INTO   l_interface_hdr_context,
                  l_interface_hdr_attribute7,
                  l_interface_hdr_attribute8,
                  l_interface_hdr_attribute9,
                  l_interface_hdr_attribute10,
                  l_interface_hdr_attribute11,
                  l_interface_hdr_attribute12
           FROM   apps.ra_customer_trx trx
           WHERE trx.customer_trx_id = p_customer_trx_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_AR_INVOICE_MISSING');
            FND_MSG_PUB.ADD;
            RAISE VALIDATION_ERROR;
      END; --<<End


      /*

            BEGIN
               SELECT   auto_trx_numbering_flag
                 INTO   l_auto_trx_numbering_flag
                 FROM   ra_batch_sources
                WHERE   batch_source_id = l_batch_source_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_AR_TRX_SOURCE_MISSING');
                  FND_MSG_PUB.ADD;
                  RAISE VALIDATION_ERROR;
            END;
      */
      l_hdr_cnt := 1;

      --Delete the plsql table for the headers,lines,distributions
      l_trx_header_tbl.delete;
      l_trx_lines_tbl.delete;
      l_trx_dist_tbl.delete;

      FOR i IN cur_header
      LOOP
         SELECT   RA_CUSTOMER_TRX_S.NEXTVAL INTO l_trx_header_id FROM DUAL;

         -- Populate header information.
         l_trx_header_tbl (l_hdr_cnt).trx_header_id := l_trx_header_id;

         /*
                  IF l_auto_trx_numbering_flag = 'N'
                  THEN
                     l_trx_header_tbl (l_hdr_cnt).trx_number := i.trx_number || '-CONV';
                  ELSE
                     l_trx_header_tbl (l_hdr_cnt).trx_number := NULL;
                  END IF;
         */

         l_trx_header_tbl (l_hdr_cnt).trx_number := i.trx_number;
         l_trx_header_tbl (l_hdr_cnt).trx_date := i.trx_date;
         l_trx_header_tbl (l_hdr_cnt).cust_trx_type_id := l_dm_cust_trx_type_id;
         l_trx_header_tbl (l_hdr_cnt).bill_to_customer_id := i.bill_to_customer_id;
         l_trx_header_tbl (l_hdr_cnt).bill_to_site_use_id := i.bill_to_site_use_id;
         --NVL (p_bill_to_site_use_id, i.bill_to_site_use_id);commented by Shankar 14-Mar-2012
         l_trx_header_tbl (l_hdr_cnt).bill_to_contact_id :=i.bill_to_contact_id;
         l_trx_header_tbl (l_hdr_cnt).ship_to_customer_id :=NVL (i.ship_to_customer_id, i.bill_to_customer_id);
         l_trx_header_tbl (l_hdr_cnt).ship_to_site_use_id := i.ship_to_site_use_id; 
            --NVL (p_ship_to_site_use_id, i.ship_to_site_use_id); --commented by Shankar 14-Mar-2012
         l_trx_header_tbl (l_hdr_cnt).ship_to_contact_id := i.ship_to_contact_id;

         l_trx_header_tbl (1).primary_salesrep_id := i.primary_salesrep_id;

         l_trx_header_tbl (1).paying_customer_id := i.paying_customer_id;
         l_trx_header_tbl (1).paying_site_use_id := i.paying_site_use_id;
         l_trx_header_tbl (1).sold_to_customer_id := i.sold_to_customer_id;
         l_trx_header_tbl (1).remit_to_address_id := i.remit_to_address_id;
         l_trx_header_tbl (1).term_id := i.term_id;
         l_trx_header_tbl (1).comments := l_comments;
         l_trx_header_tbl (1).internal_notes := i.internal_notes;
         l_trx_header_tbl (1).receipt_method_id := i.receipt_method_id;
         --Added by Maha for ver 1.3 Start >
          l_trx_header_tbl (1).attribute_category := i.attribute_category;
         l_trx_header_tbl (1).attribute1 := i.attribute1;
         l_trx_header_tbl (1).attribute2 := i.attribute2;
         l_trx_header_tbl (1).attribute3 := i.attribute3;
         l_trx_header_tbl (1).attribute4 := i.attribute4;
         l_trx_header_tbl (1).attribute5 := i.attribute5;
         l_trx_header_tbl (1).attribute6 := i.attribute6;
         l_trx_header_tbl (1).attribute7 := i.attribute7;
         l_trx_header_tbl (1).attribute8 := i.attribute8;
         l_trx_header_tbl (1).attribute9 := i.attribute9;
         l_trx_header_tbl (1).attribute10 := i.attribute10;
         l_trx_header_tbl (1).attribute11 := i.attribute11;
         l_trx_header_tbl (1).attribute12 := null;--i.attribute12;--changed for ver 1.9 by Neha
         l_trx_header_tbl (1).attribute13 := i.attribute13;
         l_trx_header_tbl (1).attribute14 := i.attribute14;
         l_trx_header_tbl (1).attribute15 := i.attribute15;
         --End >
         --l_trx_header_tbl (1).reference_number := i.ct_reference;
         l_hdr_cnt := l_hdr_cnt + 1;
      END LOOP;

      l_line_cnt := 1;
      l_dist_cnt := 1;
      l_first_trx_line_id := NULL;


-- Added by Shankar for issue 45 23-Feb-2011
         SELECT   count(*)
           INTO   l_total_lines
           FROM   apps.ra_customer_trx_lines
          WHERE   customer_trx_id = p_customer_trx_id 
            AND line_type = 'LINE';

      FOR k IN cur_lines
      LOOP
         SELECT   RA_CUSTOMER_TRX_LINES_S.NEXTVAL INTO l_trx_line_id FROM DUAL;

         IF l_first_trx_line_id IS NULL
         THEN
            l_first_trx_line_id := l_trx_line_id;
         END IF;

         -- Populate lines information.
         l_trx_lines_tbl (l_line_cnt).trx_header_id := l_trx_header_id;
         l_trx_lines_tbl (l_line_cnt).trx_line_id := l_trx_line_id;
         l_trx_lines_tbl (l_line_cnt).line_number := k.line_number;
         l_trx_lines_tbl (l_line_cnt).inventory_item_id := k.inventory_item_id;
         l_trx_lines_tbl (l_line_cnt).description := k.description;
       --Added by Maha for ver 1.4 <,Start  
          l_trx_lines_tbl (l_line_cnt).interface_line_context       := k.interface_line_context;
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute1    := trim(k.interface_line_attribute1);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute2    := trim(k.interface_line_attribute2);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute3    := trim(k.interface_line_attribute3);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute4    := trim(k.interface_line_attribute4);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute5    := trim(k.interface_line_attribute5);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute6    := trim(k.interface_line_attribute6)||'99'; --Appended 99 to handle DFF validation
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute7    := trim(k.interface_line_attribute7);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute8    := trim(k.interface_line_attribute8);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute9    := trim(k.interface_line_attribute9);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute10   := trim(k.interface_line_attribute10);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute11   := trim(k.interface_line_attribute11);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute12   := trim(k.interface_line_attribute12);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute13   := trim(k.interface_line_attribute13);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute14   := trim(k.interface_line_attribute14);
          l_trx_lines_tbl (l_line_cnt).interface_line_attribute15   := trim(k.interface_line_attribute15);
          l_trx_lines_tbl (l_line_cnt).tax_exempt_flag              := trim(k.tax_exempt_flag);

 --End>

         IF k.quantity_credited IS NULL
         THEN
            l_trx_lines_tbl (l_line_cnt).quantity_invoiced := 1;
         -- Shankar 23-OCT-2012
         ELSIF SIGN (k.unit_selling_price) = 1 then
            l_trx_lines_tbl (l_line_cnt).quantity_invoiced := k.quantity_credited * -1; 
         ELSE
            l_trx_lines_tbl (l_line_cnt).quantity_invoiced := ABS (k.quantity_credited);
         END IF;

         IF k.unit_selling_price IS NULL
         THEN
            l_trx_lines_tbl (l_line_cnt).unit_selling_price :=
               ABS (k.extended_amount);
         ELSE
            IF SIGN (k.unit_selling_price) = -1
            THEN
               l_trx_lines_tbl (l_line_cnt).unit_selling_price := k.unit_selling_price * -1; 
                  --ABS (k.unit_selling_price); shankar
            ELSIF SIGN (k.unit_selling_price) = 1
            THEN
               l_trx_lines_tbl (l_line_cnt).unit_selling_price := 
                  k.unit_selling_price;
            ELSE
               l_trx_lines_tbl (l_line_cnt).unit_selling_price :=
                  k.unit_selling_price;
            END IF;
         END IF;
--         l_trx_lines_tbl (l_line_cnt).unit_selling_price := null; -- Shankar
         IF  l_trx_lines_tbl (l_line_cnt).unit_selling_price is null or 
             l_trx_lines_tbl (l_line_cnt).quantity_invoiced is null then        
         l_trx_lines_tbl (l_line_cnt).amount := k.extended_amount * -1; --ABS (k.extended_amount); shankar
         END IF;
         l_trx_lines_tbl (l_line_cnt).line_type := k.line_type;
         l_trx_lines_tbl (l_line_cnt).uom_code := k.uom_code;
         l_trx_lines_tbl (l_line_cnt).amount_includes_tax_flag := 'N';
         l_trx_lines_tbl (l_line_cnt).taxable_flag := 'N';
      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_line.extended_amt   => ' || l_trx_lines_tbl (l_line_cnt).amount);

      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_line.USP   => ' || l_trx_lines_tbl (l_line_cnt).unit_selling_price);

      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_line.qty   => ' || l_trx_lines_tbl (l_line_cnt).quantity_invoiced);
               

         --comment the distribution lines
         FOR l IN cur_dist (k.customer_trx_line_id)
         LOOP
            SELECT   RA_CUST_TRX_LINE_GL_DIST_S.NEXTVAL
              INTO   l_trx_dist_id
              FROM   DUAL;

            -- Populate Distribution Information
            l_trx_dist_tbl (l_dist_cnt).trx_dist_id := l_trx_dist_id;
            l_trx_dist_tbl (l_dist_cnt).trx_header_id := l_trx_header_id;
            l_trx_dist_tbl (l_dist_cnt).trx_line_id := l_trx_line_id;
            l_trx_dist_tbl (l_dist_cnt).account_class := l.account_class;
            l_trx_dist_tbl (l_dist_cnt).amount := l.amount*-1; -- shankar ABS (l.amount);
            l_trx_dist_tbl (l_dist_cnt).code_combination_id :=
               l.code_combination_id;
            l_dist_cnt := l_dist_cnt + 1;
         END LOOP;

         -- populate the tax lines
         FOR m IN cur_tax_lines (k.customer_trx_line_id)
         LOOP
            SELECT   RA_CUSTOMER_TRX_LINES_S.NEXTVAL
              INTO   l_tax_trx_line_id
              FROM   DUAL;

            l_line_cnt := l_line_cnt + 1;

            BEGIN
               SELECT   tax,
                        tax_rate_id,
                        tax_jurisdiction_code,
                        tax_rate_code,
                        tax_status_code,
                        tax_regime_code
                 INTO   l_tax,
                        l_tax_rate_id,
                        l_tax_jurisdiction_code,
                        l_tax_rate_code,
                        l_tax_status_code,
                        l_tax_regime_code
                 FROM   apps.zx_lines_v
                WHERE   tax_line_id = m.tax_line_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_tax_rate_id := NULL;
                  l_tax_jurisdiction_code := NULL;
                  l_tax_rate_code := NULL;
                  l_tax_status_code := NULL;
                  l_tax_regime_code := NULL;
            END;
            l_total_lines := l_total_lines + 1; -- Shankar
            -- Populate tax lines information.
            l_trx_lines_tbl (l_line_cnt).trx_header_id := l_trx_header_id;
            l_trx_lines_tbl (l_line_cnt).trx_line_id := l_tax_trx_line_id;
            l_trx_lines_tbl (l_line_cnt).link_to_trx_line_id := l_trx_line_id;
            l_trx_lines_tbl (l_line_cnt).line_number := l_total_lines; --k.line_number; Shankar
            l_trx_lines_tbl (l_line_cnt).line_type := m.line_type;
            l_trx_lines_tbl (l_line_cnt).amount := m.extended_amount * -1 ; -- shankar ABS (m.extended_amount);
            l_trx_lines_tbl (l_line_cnt).tax := l_tax;
            l_trx_lines_tbl (l_line_cnt).vat_tax_id := m.vat_tax_id;
            l_trx_lines_tbl (l_line_cnt).tax_rate := m.tax_rate;
            l_trx_lines_tbl (l_line_cnt).tax_jurisdiction_code :=
               l_tax_jurisdiction_code;
            l_trx_lines_tbl (l_line_cnt).tax_rate_code := l_tax_rate_code;
            l_trx_lines_tbl (l_line_cnt).tax_status_code := l_tax_status_code;
            l_trx_lines_tbl (l_line_cnt).tax_regime_code := l_tax_regime_code;
            l_trx_lines_tbl (l_line_cnt).amount_includes_tax_flag := NULL;
            l_trx_lines_tbl (l_line_cnt).taxable_flag := NULL;

            FOR n IN cur_dist (m.customer_trx_line_id)
            LOOP
               SELECT   ra_cust_trx_line_gl_dist_s.NEXTVAL
                 INTO   l_tax_trx_dist_id
                 FROM   DUAL;

               -- Populate Distribution Information
               l_trx_dist_tbl (l_dist_cnt).trx_dist_id := l_tax_trx_dist_id;
               l_trx_dist_tbl (l_dist_cnt).trx_header_id := l_trx_header_id;
               l_trx_dist_tbl (l_dist_cnt).trx_line_id := l_tax_trx_line_id;
               l_trx_dist_tbl (l_dist_cnt).account_class := n.account_class;
               l_trx_dist_tbl (l_dist_cnt).amount := n.amount * -1; --ABS (n.amount);shankar
               l_trx_dist_tbl (l_dist_cnt).code_combination_id :=
                  n.code_combination_id;
               l_dist_cnt := l_dist_cnt + 1;
            END LOOP;
            
         END LOOP;
         l_line_cnt := l_line_cnt + 1;
      END LOOP;

      /*
            FOR n IN cur_frt_hdr
            LOOP
               SELECT   ra_customer_trx_lines_s.NEXTVAL
                 INTO   l_frt_trx_line_id
                 FROM   DUAL;

               --populate the freight lines
               l_trx_lines_tbl (l_line_cnt).trx_header_id := l_trx_header_id;
               l_trx_lines_tbl (l_line_cnt).trx_line_id := l_frt_trx_line_id;
               --l_trx_lines_tbl (l_line_cnt).link_to_trx_line_id := l_first_trx_line_id;
               l_trx_lines_tbl (l_line_cnt).line_number := n.line_number;
               l_trx_lines_tbl (l_line_cnt).line_type := 'FREIGHT';
               l_trx_lines_tbl (l_line_cnt).amount := -1 * n.extended_amount;
               l_line_cnt := l_line_cnt + 1;

               FOR p IN cur_dist (n.customer_trx_line_id)
               LOOP
                  SELECT   ra_cust_trx_line_gl_dist_s.NEXTVAL
                    INTO   l_tax_trx_dist_id
                    FROM   DUAL;

                  -- Populate Distribution Information
                  l_trx_dist_tbl (l_dist_cnt).trx_dist_id := l_tax_trx_dist_id;
                  l_trx_dist_tbl (l_dist_cnt).trx_header_id := l_trx_header_id;
                  l_trx_dist_tbl (l_dist_cnt).trx_line_id := l_frt_trx_line_id;
                  l_trx_dist_tbl (l_dist_cnt).account_class := p.account_class;
                  l_trx_dist_tbl (l_dist_cnt).amount := -1 * p.amount;
                  l_trx_dist_tbl (l_dist_cnt).code_combination_id :=
                     p.code_combination_id;
                  l_dist_cnt := l_dist_cnt + 1;
               END LOOP;
            END LOOP;
      */

      -- Populate Distribution lines for receivables type
      FOR rec IN cur_rec_dist
      LOOP
         l_trx_rec_ccid_id := rec.code_combination_id;
      END LOOP;


      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_trx_header_tbl.count =>' || l_trx_header_tbl.COUNT);
      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_trx_lines_tbl.count =>' || l_trx_lines_tbl.COUNT);
      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_trx_dist_tbl.count => ' || l_trx_dist_tbl.COUNT);

      -- CAll the api
      ar_invoice_api_pub.create_single_invoice (
         p_api_version            => 1.0,
         p_batch_source_rec       => l_batch_source_rec,
         p_trx_header_tbl         => l_trx_header_tbl,
         p_trx_lines_tbl          => l_trx_lines_tbl,
         p_trx_dist_tbl           => l_trx_dist_tbl,
         p_trx_salescredits_tbl   => l_trx_salescredits_tbl,
         x_customer_trx_id        => x_new_customer_trx_id,
         x_return_status          => x_return_status,
         x_msg_count              => x_error_count,
         x_msg_data               => x_error_msg
      );

      LOG_MSG (g_level_statement,
               l_mod_name,
               'x_return_status(create_single_invoice)=' || x_return_status);

      IF x_return_status <> 'S'
      THEN
         LOG_MSG (g_level_statement, l_mod_name, 'unexpected errors found!');

         IF x_error_count = 1
         THEN
            LOG_MSG (g_level_statement,
                     l_mod_name,
                     'x_error_msg ' || x_error_msg);
         ELSIF x_error_count > 1
         THEN
            LOOP
               p_count := p_count + 1;
               x_error_msg :=
                  FND_MSG_PUB.Get (FND_MSG_PUB.G_NEXT, FND_API.G_FALSE);

               IF x_error_msg IS NULL
               THEN
                  EXIT;
               END IF;

               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'Message' || p_count || '.' || x_error_msg);
            END LOOP;
         END IF;
      ELSE
         SELECT   COUNT ( * ) INTO l_cnt FROM ar_trx_errors_gt;

         IF l_cnt = 0
         THEN
            LOG_MSG (
               g_level_statement,
               l_mod_name,
               'SUCCESS: Created customer_trx_id = ' || x_new_customer_trx_id
            );

            --update the receivale account after creating
            UPDATE   ra_cust_trx_line_gl_dist_all
               SET   code_combination_id = l_trx_rec_ccid_id
             WHERE       customer_trx_id = x_new_customer_trx_id
                     AND account_class = 'REC'
                     AND customer_trx_line_id IS NULL;
            --Added below by Maha for ver 1.4         
            UPDATE   ra_customer_trx_all
            SET   interface_header_context      = l_interface_hdr_context,
                  interface_header_attribute7   = l_interface_hdr_attribute7,
                  interface_header_attribute8   = l_interface_hdr_attribute8,
                  interface_header_attribute9   = l_interface_hdr_attribute9,
                  interface_header_attribute10  = l_interface_hdr_attribute10,
                  interface_header_attribute11  = l_interface_hdr_attribute11,
                  interface_header_attribute12  = l_interface_hdr_attribute12
          WHERE   customer_trx_id               = x_new_customer_trx_id;         
                     
         ELSE
            x_return_status := 'E';
            -- k. List errors
            LOG_MSG (g_level_statement,
                     l_mod_name,
                     'FAILURE: Errors encountered, see list below:');

            FOR i IN list_errors
            LOOP
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  '----------------------------------------------------'
               );
               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'Header ID       = ' || TO_CHAR (i.trx_header_id));
               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'Line ID         = ' || TO_CHAR (i.trx_line_id));
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  'Sales Credit ID = ' || TO_CHAR (i.trx_salescredit_id)
               );
               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'Dist Id         = ' || TO_CHAR (i.trx_dist_id));
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  'Contingency ID  = ' || TO_CHAR (i.trx_contingency_id)
               );
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  'Message         = ' || SUBSTR (i.error_message, 1, 80)
               );
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  'Invalid Value   = ' || SUBSTR (i.invalid_value, 1, 80)
               );
               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  '----------------------------------------------------'
               );
            END LOOP;
         END IF;
      END IF;
   EXCEPTION
      WHEN API_FAILURE
      THEN
         NULL;
      WHEN VALIDATION_ERROR
      THEN
         x_return_status := 'E';
      WHEN OTHERS
      THEN
         x_return_status := 'E';
         x_error_msg := SQLERRM;
         x_error_count := 1;
   END reverse_cm_trx;

   PROCEDURE process_invoice (p_customer_trx_id       IN            NUMBER,
                              p_bill_to_site_use_id   IN            NUMBER,
                              p_ship_to_site_use_id   IN            NUMBER,
                              p_reason                IN            VARCHAR2,
                              x_new_customer_trx_id      OUT NOCOPY NUMBER,
                              x_rev_customer_trx_id      OUT NOCOPY NUMBER,
                              x_return_status            OUT NOCOPY VARCHAR2,
                              x_error_msg                OUT NOCOPY VARCHAR2,
                              x_error_count              OUT NOCOPY NUMBER)
   IS
      l_mod_name VARCHAR2 (100)
            := 'xxwc_credit_copy_trx_pkg.process_invoice :' ;
      api_failure exception;
   BEGIN
      SAVEPOINT process_invoice;

      LOG_MSG (g_level_statement, l_mod_name, 'calling create_new_trx');
      create_new_trx (p_customer_trx_id       => p_customer_trx_id,
                      p_bill_to_site_use_id   => p_bill_to_site_use_id,
                      p_ship_to_site_use_id   => p_ship_to_site_use_id,
                      p_reason                => p_reason,
                      x_new_customer_trx_id   => x_new_customer_trx_id,
                      x_return_status         => x_return_status,
                      x_error_msg             => x_error_msg,
                      x_error_count           => x_error_count);

      LOG_MSG (g_level_statement,
               l_mod_name,
               'x_return_status (create_new_trx) =' || x_return_status);

      IF x_return_status <> 'S'
      THEN
         RAISE api_failure;
      END IF;

      LOG_MSG (g_level_statement, l_mod_name, 'calling reverse_trx');
      reverse_trx (p_customer_trx_id       => p_customer_trx_id,
                   x_rev_customer_trx_id   => x_rev_customer_trx_id,
                   x_return_status         => x_return_status,
                   x_error_msg             => x_error_msg,
                   x_error_count           => x_error_count);

      LOG_MSG (g_level_statement,
               l_mod_name,
               'x_return_status (reverse_trx) =' || x_return_status);

      IF x_return_status <> 'S'
      THEN
         RAISE api_failure;
      END IF;
   EXCEPTION
      WHEN api_failure
      THEN
         ROLLBACK TO process_invoice;
         FND_MSG_PUB.count_and_get (p_encoded   => FND_API.G_FALSE,
                                    p_count     => x_error_count,
                                    p_data      => x_error_msg);
      WHEN OTHERS
      THEN
         ROLLBACK TO process_invoice;
         x_return_status := 'E';
         x_error_msg := SQLERRM;
         x_error_count := 1;
   END process_invoice;


   PROCEDURE process_dm (p_customer_trx_id       IN            NUMBER,
                         p_bill_to_site_use_id   IN            NUMBER,
                         p_ship_to_site_use_id   IN            NUMBER,
                         p_reason                IN            VARCHAR2,
                         x_new_customer_trx_id      OUT NOCOPY NUMBER,
                         x_rev_customer_trx_id      OUT NOCOPY NUMBER,
                         x_return_status            OUT NOCOPY VARCHAR2,
                         x_error_msg                OUT NOCOPY VARCHAR2,
                         x_error_count              OUT NOCOPY NUMBER)
   IS
      l_mod_name   VARCHAR2 (100) := 'xxwc_credit_copy_trx_pkg.process_dm :';
      api_failure EXCEPTION;
   BEGIN
      SAVEPOINT process_dm;

      LOG_MSG (g_level_statement, l_mod_name, 'calling create_new_trx');
      create_new_trx (p_customer_trx_id       => p_customer_trx_id,
                      p_bill_to_site_use_id   => p_bill_to_site_use_id,
                      p_ship_to_site_use_id   => p_ship_to_site_use_id,
                      p_reason                => p_reason,
                      x_new_customer_trx_id   => x_new_customer_trx_id,
                      x_return_status         => x_return_status,
                      x_error_msg             => x_error_msg,
                      x_error_count           => x_error_count);

      LOG_MSG (g_level_statement,
               l_mod_name,
               'x_return_status (create_new_trx) =' || x_return_status);

      IF x_return_status <> 'S'
      THEN
         RAISE api_failure;
      END IF;

      LOG_MSG (g_level_statement, l_mod_name, 'calling reverse_trx');
      reverse_trx (p_customer_trx_id       => p_customer_trx_id,
                   x_rev_customer_trx_id   => x_rev_customer_trx_id,
                   x_return_status         => x_return_status,
                   x_error_msg             => x_error_msg,
                   x_error_count           => x_error_count);

      LOG_MSG (g_level_statement,
               l_mod_name,
               'x_return_status (reverse_trx) =' || x_return_status);

      IF x_return_status <> 'S'
      THEN
         RAISE api_failure;
      END IF;
   EXCEPTION
      WHEN api_failure
      THEN
         ROLLBACK TO process_dm;
         FND_MSG_PUB.count_and_get (p_encoded   => FND_API.G_FALSE,
                                    p_count     => x_error_count,
                                    p_data      => x_error_msg);
      WHEN OTHERS
      THEN
         ROLLBACK TO process_dm;
         x_return_status := 'E';
         x_error_msg := SQLERRM;
         x_error_count := 1;
   END process_dm;

   PROCEDURE process_cm (p_customer_trx_id       IN            NUMBER,
                         p_bill_to_site_use_id   IN            NUMBER,
                         p_ship_to_site_use_id   IN            NUMBER,
                         p_reason                IN            VARCHAR2,
                         x_new_customer_trx_id      OUT NOCOPY NUMBER,
                         x_rev_customer_trx_id      OUT NOCOPY NUMBER,
                         x_return_status            OUT NOCOPY VARCHAR2,
                         x_error_msg                OUT NOCOPY VARCHAR2,
                         x_error_count              OUT NOCOPY NUMBER)
   IS
      l_mod_name   VARCHAR2 (100) := 'xxwc_credit_copy_trx_pkg.process_cm :';
      api_failure EXCEPTION;
   BEGIN
      SAVEPOINT process_cm;

      LOG_MSG (g_level_statement, l_mod_name, 'calling create_cm_trx');
      create_cm_trx (p_customer_trx_id       => p_customer_trx_id,
                     p_bill_to_site_use_id   => p_bill_to_site_use_id,
                     p_ship_to_site_use_id   => p_ship_to_site_use_id,
                     p_reason                => p_reason,
                     x_new_customer_trx_id   => x_new_customer_trx_id,
                     x_return_status         => x_return_status,
                     x_error_msg             => x_error_msg,
                     x_error_count           => x_error_count);

      LOG_MSG (g_level_statement,
               l_mod_name,
               'x_return_status (create_cm_trx) =' || x_return_status);

      IF x_return_status <> 'S'
      THEN
         RAISE api_failure;
      END IF;

      LOG_MSG (g_level_statement, l_mod_name, 'calling reverse_cm_trx');
      reverse_cm_trx (p_customer_trx_id       => p_customer_trx_id,
                      p_bill_to_site_use_id   => p_bill_to_site_use_id,
                      p_ship_to_site_use_id   => p_ship_to_site_use_id,
                      p_reason                => p_reason,
                      x_new_customer_trx_id   => x_rev_customer_trx_id,
                      x_return_status         => x_return_status,
                      x_error_msg             => x_error_msg,
                      x_error_count           => x_error_count);

      LOG_MSG (g_level_statement,
               l_mod_name,
               'x_return_status (reverse_cm_trx) =' || x_return_status);

      IF x_return_status <> 'S'
      THEN
         RAISE api_failure;
      END IF;

      LOG_MSG (g_level_statement, l_mod_name, 'calling apply_credit_memo');
      apply_credit_memo (p_cm_customer_trx_id   => p_customer_trx_id,
                         p_dm_customer_trx_id   => x_rev_customer_trx_id,
                         x_return_status        => x_return_status,
                         x_error_msg            => x_error_msg,
                         x_error_count          => x_error_count);

      LOG_MSG (g_level_statement,
               l_mod_name,
               'x_return_status (apply_credit_memo) =' || x_return_status);

      IF x_return_status <> 'S'
      THEN
         RAISE api_failure;
      END IF;
   EXCEPTION
      WHEN api_failure
      THEN
         ROLLBACK TO process_cm;
         FND_MSG_PUB.count_and_get (p_encoded   => FND_API.G_FALSE,
                                    p_count     => x_error_count,
                                    p_data      => x_error_msg);
      WHEN OTHERS
      THEN
         ROLLBACK TO process_cm;
         x_return_status := 'E';
         x_error_msg := SQLERRM;
         x_error_count := 1;
   END process_cm;

   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXXWC_CREDIT_COPY_TRX_PKG.pkb $
   *   Module Name: XXWC_CREDIT_COPY_TRX_PKG.pkb
   *
   *   PURPOSE:   This package is used by the XXWC Credit and Rebill UI to submit the conc program
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   * ***************************************************************************/

   PROCEDURE Submit (errbuf         OUT VARCHAR2,
                     retcode        OUT VARCHAR2,
                     p_user_id   IN     NUMBER)
   IS
      l_mod_name                 VARCHAR2 (100) := 'xxwc_credit_copy_trx_pkg.submit :';

      x_return_status            VARCHAR2 (10);
      x_error_msg                VARCHAR2 (1000);
      x_error_count              NUMBER;
      lx_new_customer_trx_id     NUMBER;
      lx_rev_customer_trx_id     NUMBER;
      l_dup_count                NUMBER;
      l_process_status_meaning   VARCHAR2 (100);
      l_rebill_batch_source_id   NUMBER;
      l_request_id               NUMBER;
      p_count                    NUMBER := 0;

      validation_error EXCEPTION;
      api_failure EXCEPTION;

      CURSOR C1
      IS
         SELECT   xacc.credit_copy_trx_id,
                  xacc.customer_trx_id,
                  xacc.reason_code,
                  rct.bill_to_site_use_id,
                  rct.ship_to_site_use_id,
                  rct.trx_number,
                  rctt.TYPE trx_type_name,
                  NVL (xacc.rebill_bill_to_site_use_id,
                       rct.bill_to_site_use_id)
                     rebill_bill_to_site_use_id,
                  NVL (xacc.rebill_ship_to_site_use_id,
                       rct.ship_to_site_use_id)
                     rebill_ship_to_site_use_id
           FROM   apps.xxwc_ar_credit_copy_trx xacc,
                  apps.ra_customer_trx rct,
                  apps.ra_cust_trx_types rctt
          WHERE       xacc.customer_trx_id = rct.customer_trx_id
                  AND rct.cust_trx_type_id = rctt.cust_trx_type_id
                  AND rct.batch_source_id <> l_rebill_batch_source_id
                  AND xacc.process_status <> 'P'
                  AND xacc.select_trx = 'Y'
                  AND xacc.created_by = p_user_id;

      CURSOR C2
      IS
           SELECT   COUNT ( * ) COUNT, process_status
             FROM   apps.xxwc_ar_credit_copy_trx xacc
            WHERE   xacc.created_by = p_user_id
                    AND xacc.request_id = l_request_id
         GROUP BY   process_status;

      CURSOR C3
      IS
         SELECT   trx.trx_number,
                  new_trx.trx_number new_trx_number,
                  rev_trx.trx_number rev_trx_number,
                  process_status,
                  error_message
           FROM   apps.xxwc_ar_credit_copy_trx xacc,
                  apps.ra_customer_trx trx,
                  apps.ra_customer_trx new_trx,
                  apps.ra_customer_trx rev_trx
          WHERE       xacc.customer_trx_id = trx.customer_trx_id
                  AND xacc.new_customer_trx_id = new_trx.customer_trx_id(+)
                  AND xacc.rev_customer_trx_id = rev_trx.customer_trx_id(+)
                  AND xacc.request_id = l_request_id
                  AND xacc.created_by = p_user_id;
   BEGIN
      LOG_MSG (
         g_level_statement,
         l_mod_name,
         '+---------------------------------------------------------------------------------+'
      );
      LOG_MSG (g_level_statement, l_mod_name, 'p_user_id =>' || p_user_id);

      --Initialize the out paramete for the conc program
      errbuf := NULL;
      retcode := '0';

      --Initialize the API out parameter
      x_return_status := 'S';
      x_error_msg := NULL;
      x_error_count := 0;

      l_request_id := fnd_global.conc_request_id;

      SELECT   mo_global.get_current_org_id () INTO g_org_id FROM DUAL;

      LOG_MSG (g_level_statement,
               l_mod_name,
               'Current MO Org_id=' || g_org_id);

      --Initialize the apps
      --mo_global.init ('AR');
      mo_global.set_policy_context ('S', g_org_id);
      fnd_global.apps_initialize (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id,
                                  0);

      --arp_global.init_global;

      l_rebill_batch_source_id :=
         FND_PROFILE.VALUE ('XXWC_CREDIT_REBILL_BATCH_SOURCE');
      LOG_MSG (g_level_statement,
               l_mod_name,
               'l_rebill_batch_source_id =>' || l_rebill_batch_source_id);

      IF l_rebill_batch_source_id IS NULL
      THEN
         FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_AR_TRX_SOURCE_MISSING');
         FND_MSG_PUB.ADD;
         RAISE VALIDATION_ERROR;
      END IF;

      FOR i IN C1
      LOOP
         BEGIN
            --Initialize the message stack
            fnd_msg_pub.Initialize;
            LOG_MSG (
               g_level_statement,
               l_mod_name,
               '+------------------------------------------------------------------------------+'
            );
            LOG_MSG (g_level_statement,
                     l_mod_name,
                     'i.customer_trx_id       =' || i.customer_trx_id);
            LOG_MSG (g_level_statement,
                     l_mod_name,
                     'i.trx_type_name         =' || i.trx_type_name);
            LOG_MSG (
               g_level_statement,
               l_mod_name,
               'i.rebill_ship_to_site_use_id ='
               || i.rebill_ship_to_site_use_id
            );
            LOG_MSG (
               g_level_statement,
               l_mod_name,
               'i.rebill_bill_to_site_use_id ='
               || i.rebill_bill_to_site_use_id
            );
            LOG_MSG (g_level_statement,
                     l_mod_name,
                     'i.bill_to_site_use_id   =' || i.bill_to_site_use_id);
            LOG_MSG (g_level_statement,
                     l_mod_name,
                     'i.ship_to_site_use_id   =' || i.ship_to_site_use_id);

            IF (i.bill_to_site_use_id = i.rebill_bill_to_site_use_id)
               AND (NVL (i.ship_to_site_use_id, 999) =
                       NVL (i.rebill_ship_to_site_use_id, 999))
            THEN
               FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_AR_REBILL_SITE_NOT_DIFF');
               FND_MSG_PUB.ADD;
               RAISE VALIDATION_ERROR;
            END IF;

            IF i.rebill_bill_to_site_use_id IS NULL
            THEN
               FND_MESSAGE.SET_NAME ('XXWC',
                                     'XXWC_AR_REBILL_BILLTO_SITE_MIS');
               FND_MSG_PUB.ADD;
               RAISE VALIDATION_ERROR;
            END IF;

            BEGIN
               SELECT   COUNT ( * )
                 INTO   l_dup_count
                 FROM   ra_customer_trx rct
                WHERE   rct.batch_source_id = l_rebill_batch_source_id
                        AND rct.trx_number = i.trx_number;
            EXCEPTION
               WHEN OTHERS
               THEN
                  x_error_msg := SQLERRM;
                  RAISE VALIDATION_ERROR;
            END;

            LOG_MSG (g_level_statement,
                     l_mod_name,
                     'l_dup_count   =' || l_dup_count);

            IF l_dup_count > 0
            THEN
               FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_AR_TRX_NUMBER_DUP');
               FND_MESSAGE.SET_TOKEN ('TRX_NUMBER', i.trx_number);
               FND_MSG_PUB.ADD;
               RAISE VALIDATION_ERROR;
            END IF;

            IF i.trx_type_name = 'INV'
            THEN
               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'Calling process_invoice');
               process_invoice (
                  p_customer_trx_id       => i.customer_trx_id,
                  p_bill_to_site_use_id   => i.rebill_bill_to_site_use_id,
                  p_ship_to_site_use_id   => i.rebill_ship_to_site_use_id,
                  p_reason                => i.reason_code,
                  x_new_customer_trx_id   => lx_new_customer_trx_id,
                  x_rev_customer_trx_id   => lx_rev_customer_trx_id,
                  x_return_status         => x_return_status,
                  x_error_msg             => x_error_msg,
                  x_error_count           => x_error_count
               );

               LOG_MSG (
                  g_level_statement,
                  l_mod_name,
                  'x_return_status(process_invoice) =' || x_return_status
               );

               IF x_return_status <> 'S'
               THEN
                  RAISE api_failure;
               END IF;
            ELSIF i.trx_type_name = 'DM'
            THEN
               LOG_MSG (g_level_statement, l_mod_name, 'Calling Process_dm');
               process_dm (
                  p_customer_trx_id       => i.customer_trx_id,
                  p_bill_to_site_use_id   => i.rebill_bill_to_site_use_id,
                  p_ship_to_site_use_id   => i.rebill_ship_to_site_use_id,
                  p_reason                => i.reason_code,
                  x_new_customer_trx_id   => lx_new_customer_trx_id,
                  x_rev_customer_trx_id   => lx_rev_customer_trx_id,
                  x_return_status         => x_return_status,
                  x_error_msg             => x_error_msg,
                  x_error_count           => x_error_count
               );

               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'x_return_status(process_dm) =' || x_return_status);

               IF x_return_status <> 'S'
               THEN
                  RAISE api_failure;
               END IF;
            ELSIF i.trx_type_name = 'CM'
            THEN
               LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, 'Calling Process_cm');
               process_cm (
                  p_customer_trx_id       => i.customer_trx_id,
                  p_bill_to_site_use_id   => i.rebill_bill_to_site_use_id,
                  p_ship_to_site_use_id   => i.rebill_ship_to_site_use_id,
                  p_reason                => i.reason_code,
                  x_new_customer_trx_id   => lx_new_customer_trx_id,
                  x_rev_customer_trx_id   => lx_rev_customer_trx_id,
                  x_return_status         => x_return_status,
                  x_error_msg             => x_error_msg,
                  x_error_count           => x_error_count
               );

               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'x_return_status(process_cm) =' || x_return_status);

               IF x_return_status <> 'S'
               THEN
                  RAISE api_failure;
               END IF;
            ELSE
               x_error_msg :=
                  i.trx_type_name
                  || ' -Invalid Trx Type. The Program supports only INV,DM,CM ';
               x_error_count := 1;
               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'x_error_msg =' || x_error_msg);
               RAISE VALIDATION_ERROR;
            END IF;

            --Update the table with the process status as success and error message
            UPDATE   xxwc_ar_credit_copy_trx
               SET   process_status = 'P',
                     error_message = NULL,
                     new_customer_trx_id = lx_new_customer_trx_id,
                     rev_customer_trx_id = lx_rev_customer_trx_id,
                     object_version_number = object_version_number + 1,
                     last_update_date = SYSDATE,
                     last_update_login = fnd_global.login_id,
                     last_updated_by = p_user_id,
                     request_id = l_request_id
             WHERE   credit_copy_trx_id = i.credit_copy_trx_id;
         EXCEPTION
            WHEN API_FAILURE
            THEN
               --Update the table with the process status as error and error message
               UPDATE   xxwc_ar_credit_copy_trx
                  SET   process_status = 'E',
                        error_message = x_error_msg,
                        object_version_number = object_version_number + 1,
                        last_update_date = SYSDATE,
                        last_update_login = fnd_global.login_id,
                        last_updated_by = p_user_id,
                        request_id = l_request_id
                WHERE   credit_copy_trx_id = i.credit_copy_trx_id;
            WHEN VALIDATION_ERROR
            THEN
               x_error_msg :=
                  FND_MSG_PUB.Get (FND_MSG_PUB.G_NEXT, FND_API.G_FALSE);
               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'Message :' || x_error_msg);

               --Update the table with the process status as error and error message
               UPDATE   xxwc_ar_credit_copy_trx
                  SET   process_status = 'E',
                        error_message = x_error_msg,
                        object_version_number = object_version_number + 1,
                        last_update_date = SYSDATE,
                        last_update_login = fnd_global.login_id,
                        last_updated_by = p_user_id,
                        request_id = l_request_id
                WHERE   credit_copy_trx_id = i.credit_copy_trx_id;
            WHEN OTHERS
            THEN
               x_error_msg := SQLERRM;
               LOG_MSG (g_level_statement,
                        l_mod_name,
                        'x_error_msg =' || x_error_msg);

               --Update the table with the process status as error and error message
               UPDATE   xxwc_ar_credit_copy_trx
                  SET   process_status = 'E',
                        error_message = x_error_msg,
                        object_version_number = object_version_number + 1,
                        last_update_date = SYSDATE,
                        last_update_login = fnd_global.login_id,
                        last_updated_by = p_user_id,
                        request_id = l_request_id
                WHERE   credit_copy_trx_id = i.credit_copy_trx_id;
         END;
      END LOOP;


      --Write the output message
      write_output ('  ');
      write_output ('  ');
      write_output('+--------------------------------------------------------------------------------+');
      write_output('********************Record Count by Process Status******************************');
      write_output('+--------------------------------------------------------------------------------+');
      write_output (RPAD ('Status', 25, ' ') || RPAD ('Count', 10, ' '));
      write_output('+--------------------------------------------------------------------------------+');

      FOR k IN C2
      LOOP
         SELECT   DECODE (k.process_status,
                          'R', 'Ready to Process      ',
                          'E', 'Error                 ',
                          'P', 'Processed Successfully',
                          '                      ')
           INTO   l_process_status_meaning
           FROM   DUAL;

         write_output (RPAD (l_process_status_meaning, 25, '.') || k.COUNT);
      END LOOP;

      write_output('+--------------------------------------------------------------------------------+');
      write_output ('  ');
      write_output ('  ');
      write_output ('  ');
      write_output('+--------------------------------------------------------------------------------+');
      write_output('***********************Transaction Processed by the Program***********************');
      write_output('+--------------------------------------------------------------------------------+');
      write_output(   RPAD ('Trx_Number', 15, ' ')
                   || RPAD ('New_Trx_Number', 15, ' ')
                   || RPAD ('Rev_Trx_Number', 20, ' ')
                   || RPAD ('Status', 15, ' ')
                   || RPAD ('Error_Message', 100, ' '));
      write_output('+--------------------------------------------------------------------------------+');

      FOR m IN C3
      LOOP
         SELECT   DECODE (m.process_status,
                          'R', 'Ready to Process      ',
                          'E', 'Error                 ',
                          'P', 'Processed Successfully',
                          '                      ')
           INTO   l_process_status_meaning
           FROM   DUAL;

         write_output(   RPAD (m.trx_number, 15, ' ')
                      || RPAD (m.new_trx_number, 15, ' ')
                      || RPAD (m.rev_trx_number, 20, ' ')
                      || RPAD (m.process_status, 15, ' ')
                      || RPAD (SUBSTR (m.error_message, 100), 100, ''));
      END LOOP;

      write_output('+--------------------------------------------------------------------------------+');

      LOG_MSG (g_level_statement,
               l_mod_name,
               'Deleting the records from xxwc_ar_credit_copy_trx');

      DELETE FROM   xxwc_ar_credit_copy_trx
            WHERE   created_by = p_user_id AND process_status <> 'P';

      LOG_MSG (
         g_level_statement,
         l_mod_name,
         'Num of Rows Deleted (xxwc_ar_credit_copy_trx) =' || sql%ROWCOUNT
      );

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := SQLERRM;
         retcode := '2';
   END Submit;

/* **************************************************************************
   *   Procedure Name: cons_trx
   *
   *   PURPOSE:   This procedure called from a conc prog to consolidate the credit
   *              and rebill trx into a single transaction
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        04/21/2013  Shankar Hariharan       Initial Version
   *   1.1        01/15/2014  Maharajan Shunmugam     TMS# 20130909-00743
   *   1.5        06/06/2014  Maharajan Shunmugam     TMS# 20140124-00031 Update attributes in interface line and Fix for split invoice
   *   1.6        08/25/2014  Maharajan Shunmugam     TMS# 20140826-00040 Consolidatin program change for split invoice grouping
   *   1.7        09/17/2014  Maharajan Shunmugam     TMS# 20141001-00153 Canada Multi Org changes       
   *   1.8        07/29/2015  Maharajan Shunmugam     TMS# 20150123-00197 AR - Customer Flag as indicator to auto apply credits
   * ***************************************************************************/
PROCEDURE cons_trx (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
IS
   l_reason_code        VARCHAR2 (100);
   l_ship_via           VARCHAR2 (100);
   l_waybill_num        VARCHAR2 (100);
   l_term_id            NUMBER;
   l_trx_number         VARCHAR2 (100);
   l_bc_id              NUMBER;
   l_sc_id              NUMBER;
   --Added below for ver 1.5
   l_ship_cust_id    NUMBER;
   l_bill_cust_id    NUMBER;
   l_ship_add_id     NUMBER;
   l_bill_add_id     NUMBER;
   l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   l_sec             VARCHAR2(240);
   --Added by Maha for ver 1.1
   l_trx_num            VARCHAR2 (20);
   l_cust_trx_id        NUMBER;
   l_cust_trx_line_id   NUMBER;
   l_auto_apply         VARCHAR2(3);          --Added for ver 1.8
   l_remove_ref         VARCHAR2(3) := 'Y';          --Added for ver 1.8

   CURSOR c1
   IS
        SELECT a.interface_line_context,
               a.interface_line_attribute1,
               SUM (amount) total_amount,
               COUNT (1)
          FROM apps.ra_interface_lines a,
               apps.oe_order_headers b,
               apps.oe_order_lines c
         WHERE     a.interface_line_context = 'ORDER ENTRY'
               AND a.interface_line_attribute2 = 'RETURN ORDER'
               AND a.interface_line_attribute6 = c.line_id
               AND a.interface_line_attribute1 = b.order_number
               AND b.header_id = c.header_id
               --AND c.line_type_id = 1003                            --commented and added below profile for ver 1.7
               AND c.line_type_id = fnd_profile.value('XXWC_BILL_ONLY_LINE_TYPE')              
            AND a.customer_trx_id IS NULL
               AND EXISTS
                      (SELECT 1
                         FROM apps.ra_interface_lines x,
                              apps.oe_order_headers y,
                              apps.oe_order_lines z
                        WHERE     x.interface_line_context = 'ORDER ENTRY'
                              AND x.interface_line_attribute2 = 'RETURN ORDER'
                              AND x.interface_line_attribute1 = b.order_number
                              AND x.interface_line_attribute1 = y.order_number
                              AND x.interface_line_attribute6 = z.line_id
                              AND y.header_id = z.header_id
                              --AND z.line_type_id = 1008                     --commented and added below profile for ver 1.7
                  AND z.line_type_id = fnd_profile.value('XXWC_CREDIT_ONLY_LINE_TYPE')   
                              AND x.customer_trx_id IS NULL)
      GROUP BY a.interface_line_context, a.interface_line_attribute1;

   --Added by Maha for ver 1.1 Start >

   CURSOR c2
   IS
      SELECT a.reference_line_id iface_reference_line_id,
             --c.source_document_line_id
             c.reference_line_id || '00' so_reference_line_id
        FROM apps.ra_interface_lines a,
             apps.oe_order_headers b,
             apps.oe_order_lines c
       WHERE     a.interface_line_context = 'ORDER ENTRY'
             AND a.interface_line_attribute2 = 'RETURN ORDER'
             AND a.interface_line_attribute6 = c.line_id
             AND a.interface_line_attribute1 = b.order_number
             AND b.header_id = c.header_id
            -- AND c.line_type_id IN (1007, 1008) --RETURN WITH RECEIPT and  CREDIT ONLY                    --commented and added below profile for ver 1.7
         AND c.line_type_id IN  (fnd_profile.value('XXWC_RETURN_WITH_RECEIPT_LINE_TYPE'), fnd_profile.value('XXWC_CREDIT_ONLY_LINE_TYPE'))
             AND a.customer_trx_id IS NULL
             AND NOT EXISTS
                        (SELECT 1
                           FROM apps.ra_interface_lines x,
                                apps.oe_order_headers y,
                                apps.oe_order_lines z
                          WHERE     x.interface_line_context = 'ORDER ENTRY'
                                AND x.interface_line_attribute2 =
                                       'RETURN ORDER'
                                AND x.interface_line_attribute6 = z.line_id
                                AND x.interface_line_attribute1 =
                                       y.order_number
                                AND x.interface_line_attribute1 =
                                       b.order_number
                                AND y.header_id = z.header_id
                               -- AND z.line_type_id = 1003                                                  --commented and added below profile for ver 1.7
                AND z.line_type_id = fnd_profile.value('XXWC_BILL_ONLY_LINE_TYPE')
                                AND x.customer_trx_id IS NULL);
--End>

--Added below cursor c3 for ver 1.5 
  CURSOR c3 IS
     SELECT   interface_line_attribute1                          --Changed attribute2 to attribute1 by Maha for ver 1.6
         ,orig_system_ship_address_id
         ,cust_trx_type_id
         ,sales_order
         ,ship_date_actual
         ,orig_system_bill_contact_id
         ,orig_system_ship_contact_id 
         ,purchase_order
         ,interface_line_attribute10
         ,warehouse_id
         ,min(trx_number) trx_number 
     FROM APPS.XXWC_TAXW_INVOICE_NUM_LOG 
      WHERE TRUNC(creation_date) = TRUNC(sysdate)        --Added by Maha for ver 1.6
     GROUP BY interface_line_attribute1
             ,orig_system_ship_address_id
             ,cust_trx_type_id
             ,sales_order
             ,ship_date_actual
             ,orig_system_bill_contact_id
             ,orig_system_ship_contact_id 
             ,purchase_order
             ,interface_line_attribute10
             ,warehouse_id;
--Added below cursor for ver#1.8
CURSOR C4 IS
 SELECT        a.interface_line_context,
               a.interface_line_attribute1,
               MAX(b.sold_to_org_id) sold_to_org_id
          FROM apps.ra_interface_lines a,
               apps.oe_order_headers b,
               apps.oe_order_lines c
         WHERE     a.interface_line_context = 'ORDER ENTRY'
               AND a.interface_line_attribute2 = 'RETURN ORDER'
               AND a.interface_line_attribute6 = c.line_id
               AND a.interface_line_attribute1 = b.order_number
               AND b.header_id = c.header_id
            AND a.customer_trx_id IS NULL
      GROUP BY a.interface_line_context, a.interface_line_attribute1;    


BEGIN

l_sec := 'Processing consolidation on invoice interface lines';
   --Added by Maha for ver 1.3 Start >
   FOR c2_rec IN c2
   LOOP
      BEGIN
         SELECT rct.trx_number
           INTO l_trx_num
           FROM apps.ra_customer_trx rct, apps.ra_customer_trx_lines rcta
          WHERE     rct.customer_trx_id = rcta.customer_trx_id
                AND rcta.customer_trx_line_id =
                       c2_rec.iface_reference_line_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_trx_num := NULL;
      END;

      BEGIN
         SELECT customer_trx_id
           INTO l_cust_trx_id
           FROM apps.ra_customer_trx
          WHERE batch_source_id = 1006 AND trx_number = l_trx_num;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_cust_trx_id := NULL;
      END;


      IF l_cust_trx_id IS NOT NULL
      THEN
         BEGIN
            SELECT customer_trx_line_id
              INTO l_cust_trx_line_id
              FROM apps.ra_customer_trx_lines
             WHERE     customer_trx_id = l_cust_trx_id
                   AND interface_line_attribute6 =
                          c2_rec.so_reference_line_id; -- c2_rec.source_document_line_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_cust_trx_line_id := NULL;
         END;

         IF l_cust_trx_line_id IS NOT NULL
         THEN
            UPDATE ra_interface_lines_all
               SET reference_line_id = l_cust_trx_line_id
             WHERE reference_line_id = c2_rec.iface_reference_line_id;
         END IF;
      END IF;
   END LOOP;
--End >

--Added for ver# 1.8 <<START

FOR c4_rec in c4
LOOP 
fnd_file.put_line (fnd_file.LOG, 'Processing auto apply for Interface line for SO '||c4_rec.interface_line_attribute1); 
 l_auto_apply := NULL;
BEGIN
  SELECT hca.attribute11
    INTO l_auto_apply
  FROM hz_cust_accounts hca
   WHERE hca.cust_account_id = c4_rec.sold_to_org_id;
EXCEPTION
WHEN OTHERS THEN
 l_auto_apply := 'Y';
END;

fnd_file.put_line (fnd_file.LOG, 'Auto apply flag for cust account id '||c4_rec.sold_to_org_id||'--'||l_auto_apply); 

     IF l_auto_apply = 'N' 
     THEN
          fnd_file.put_line (fnd_file.LOG, 'Auto apply is OFF, so removing reference line id for '||c4_rec.interface_line_attribute1); 
     BEGIN
      update ra_interface_lines_all 
         set reference_line_id=null
       where interface_line_context='ORDER ENTRY'
         and interface_line_attribute1=c4_rec.interface_line_attribute1
         and customer_Trx_id is null
         and reference_line_id is not null
         and cust_trx_type_id=2;
      EXCEPTION
      WHEN OTHERS THEN
      NULL;
      END;
      END IF;
END LOOP;
--<<END  --Ver 1.8
     
FOR c1_rec in c1
LOOP 
      BEGIN  
 --Added below for Ver#1.8 <<Start
             l_reason_code := NULL; 
             l_trx_number := NULL; 
             l_bc_id := NULL; 
             l_sc_id := NULL; 
             l_bill_cust_id := NULL; 
             l_ship_cust_id := NULL; 
             l_bill_add_id := NULL; 
             l_ship_add_id  := NULL;  
--Added below for Ver#1.8 <<End

      BEGIN --Ver1.8
      select reason_code, 
             trx_number, 
             orig_system_bill_contact_id,
             orig_system_ship_contact_id,
             orig_system_bill_customer_id,                          --Added below four fields for ver 1.5
             orig_system_ship_customer_id,
             orig_system_bill_address_id,
             orig_system_ship_address_id
        into l_reason_code, 
             l_trx_number, 
             l_bc_id, 
             l_sc_id,
             l_bill_cust_id,
             l_ship_cust_id,
             l_bill_add_id,
             l_ship_add_id             
        from apps.ra_interface_lines x
       where cust_trx_type_id=2
         and interface_line_context='ORDER ENTRY'
         and interface_line_attribute1=c1_rec.interface_line_attribute1
         and customer_Trx_id is null
         and exists (select 1 
                       from apps.oe_order_lines y
                      where y.line_id=x.interface_line_attribute6
                        --and y.line_type_id=1008)                                                          --commented and added below profile for ver 1.7
              and y.line_type_id=fnd_profile.value('XXWC_CREDIT_ONLY_LINE_TYPE')) 
         and rownum=1;
       EXCEPTION      --Added Exception for ver# 1.8
       WHEN OTHERS THEN
             l_reason_code := NULL; 
             l_trx_number := NULL; 
             l_bc_id := NULL; 
             l_sc_id := NULL; 
             l_bill_cust_id := NULL; 
             l_ship_cust_id := NULL; 
             l_bill_add_id := NULL; 
             l_ship_add_id  := NULL;        
      END;
         
IF l_reason_code IS NOT NULL  --Added IF clause for ver#1.8
 THEN 
      BEGIN 
      update ra_interface_lines_all x
         set cust_trx_type_id=2
           , term_id=null
           , reason_code=l_reason_code
           , ship_via = null
           , waybill_number = null
           , orig_system_bill_contact_id    = l_bc_id
           , orig_system_ship_contact_id    = l_sc_id
           , orig_system_bill_customer_id   = l_bill_cust_id      --Added below four fields for ver 1.5
           , orig_system_ship_customer_id   = l_ship_cust_id
           , orig_system_bill_address_id    = l_bill_add_id
           , orig_system_ship_address_id   =  l_ship_add_id
       where cust_trx_type_id in (1,2)
         and interface_line_context='ORDER ENTRY'
         and interface_line_attribute1=c1_rec.interface_line_attribute1
         and customer_Trx_id is null
         and exists (select 1 
                       from apps.oe_order_lines y
                      where y.line_id=x.interface_line_attribute6
                        --and y.line_type_id IN (1003,1008));                                             --commented and added below profile for ver 1.7
                          and y.line_type_id IN (fnd_profile.value('XXWC_BILL_ONLY_LINE_TYPE'),fnd_profile.value('XXWC_CREDIT_ONLY_LINE_TYPE')));
       EXCEPTION --Added for ver#1.8
       WHEN OTHERS THEN
       NULL;
       END;

     fnd_file.put_line (fnd_file.LOG, 'Updated reason code '); --Ver1.8
     BEGIN --VER1.8    
      update ra_interface_lines_all x
         set trx_number=l_trx_number
       where interface_line_context='ORDER ENTRY'
         and interface_line_attribute1=c1_rec.interface_line_attribute1
         and customer_Trx_id is null
         and exists (select 1 
                       from apps.oe_order_lines y
                      where y.line_id=x.interface_line_attribute6
                        --and y.line_type_id in (1003,1008));                                              --commented and added below profile for ver 1.7
                          and y.line_type_id IN (fnd_profile.value('XXWC_BILL_ONLY_LINE_TYPE'),fnd_profile.value('XXWC_CREDIT_ONLY_LINE_TYPE'))); 
    EXCEPTION --VER1.8
    WHEN OTHERS THEN
    NULL;
    END;

     fnd_file.put_line (fnd_file.LOG, 'Updated trx num ');  --Ver1.8

    END IF;  --Added for ver#1.8

      
      update ra_interface_lines_all x
               set reference_line_id=null
             where interface_line_context='ORDER ENTRY'
               and interface_line_attribute1=c1_rec.interface_line_attribute1
               and customer_Trx_id is null
               and reference_line_id is not null
               and cust_trx_type_id=2
               and exists (select 1 
                             from apps.oe_order_lines y
                            where y.line_id=x.interface_line_attribute6
                          and y.line_type_id IN (fnd_profile.value('XXWC_BILL_ONLY_LINE_TYPE'),fnd_profile.value('XXWC_CREDIT_ONLY_LINE_TYPE')));
  
      
     EXCEPTION
        when others then 
   fnd_file.put_line (fnd_file.LOG, 'When others exception in updating reason code, reference line id '||c1_rec.interface_line_attribute1); --Ver1.8
      null;
     END;
    end loop;
--Added below for ver 1.5 <Start
   FOR c3_rec IN c3
   LOOP
   BEGIN
   FOR I IN (SELECT trx_number
              FROM APPS.XXWC_TAXW_INVOICE_NUM_LOG 
             WHERE interface_line_attribute1       =    c3_rec.interface_line_attribute1       --Changed attribute2 to attribute1 by Maha for ver 1.6
                   AND orig_system_ship_address_id =    c3_rec.orig_system_ship_address_id
                   AND orig_system_bill_contact_id =    c3_rec.orig_system_bill_contact_id
                   AND orig_system_ship_contact_id =    c3_rec.orig_system_ship_contact_id 
                   AND purchase_order              =    c3_rec.purchase_order  
                   AND interface_line_attribute10  =    c3_rec.interface_line_attribute10 
                   AND warehouse_id                =    c3_rec.warehouse_id
                   AND sales_order                 =    c3_rec.sales_order
                   AND ship_date_actual            =    c3_rec.ship_date_actual 
                   AND cust_trx_type_id            =    c3_rec.cust_trx_type_id
                   AND TRUNC(creation_date)        =    TRUNC(sysdate) )                       --Added by Maha for ver 1.6
    LOOP
    IF c3_rec.trx_number != i.trx_number THEN
    
    UPDATE XXWC.XXWC_TAXW_INVOICE_NUM_LOG 
      SET trx_number = c3_rec.trx_number
    WHERE interface_line_attribute1       =    c3_rec.interface_line_attribute1               --Changed attribute2 to attribute1 by Maha for ver 1.6
      AND orig_system_ship_address_id =    c3_rec.orig_system_ship_address_id
      AND orig_system_bill_contact_id =    c3_rec.orig_system_bill_contact_id
      AND orig_system_ship_contact_id =    c3_rec.orig_system_ship_contact_id 
      AND purchase_order              =    c3_rec.purchase_order  
      AND interface_line_attribute10  =    c3_rec.interface_line_attribute10 
      AND warehouse_id                =    c3_rec.warehouse_id
      AND sales_order                 =    c3_rec.sales_order
      AND ship_date_actual            =    c3_rec.ship_date_actual 
      AND cust_trx_type_id            =    c3_rec.cust_trx_type_id
      AND trx_number                  =    i.trx_number;
      
      
    UPDATE ra_interface_lines_all
     SET  trx_number = c3_rec.trx_number
    WHERE trx_number = i.trx_number
     AND sales_order = c3_rec.sales_order;
     
    END IF;
  END LOOP;
  END;
END LOOP;
 --<End
EXCEPTION
 WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log,'Error Message = '|| SQLERRM);
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWC_CREDIT_COPY_TRX_PKG.CONS_TRX'
                                            ,p_calling           => l_sec
                                            ,p_request_id        => NULL
                                            ,p_ora_error_msg     => substr(SQLERRM
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => 'Error in XXWC_CREDIT_COPY_TRX_PKG.CONS_TRX procedure with OTHERS Exception'
                                            ,p_distribution_list => l_distro_list 
                                            ,p_module            => 'AR');


END cons_trx;

END XXWC_CREDIT_COPY_TRX_PKG;
/