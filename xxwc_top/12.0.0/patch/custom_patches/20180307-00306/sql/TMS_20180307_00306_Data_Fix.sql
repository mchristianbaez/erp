/*
 TMS#20180307-00306 - Duplicate cat-class assignment for item #121AMF99 in branch #853
Created by Vamshi on 07-Mar-2018
*/
SET SERVEROUT ON

CREATE TABLE XXWC.XXWC_20180307_00306_BKP
AS
   SELECT *
  FROM apps.mtl_item_categories
 WHERE inventory_item_id = 11784979 AND organization_id = 369;               
/
DELETE FROM mtl_item_categories
      WHERE     inventory_item_id = 11784979
            AND organization_id = 369
            AND CATEGORY_ID = 4430;

COMMIT;
/