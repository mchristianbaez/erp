/*************************************************************************
-- $Header APPS.XXEIS_GL_AR_180_DREAM_RPT_V
-- Module Name: XXEIS 

-- REVISIONS:
-- Ver        Date        Author             Description
-- ---------  ----------  ----------         ----------------
-- 1.0       07/20/2018  Missing in SVN      This view was probably created in EIS front end directly as a paste SQL.  
-- 1.1       07/20/2018  Balaguru Seshadri   Update to EiS View_Report for Subledger Recon - HARRIS (TMS# 20180720-00013)
**********/
CREATE OR REPLACE FORCE VIEW APPS.XXEIS_GL_AR_180_DREAM_RPT_V
(
   SOURCE,
   JE_CATEGORY,
   ACC_DATE,
   ENTRY,
   LINE_DESCR,
   LINE_ACCTD_DR,
   LINE_ACCTD_CR,
   LINE_ENT_DR,
   LINE_ENT_CR,
   DTL_ID,
   TRANSACTION_NUMBER,
   CUSTOMER_NUMBER,
   CUSTOMER_OR_VENDOR,
   DTL_ENTITY_ID,
   RECEIPT_CLASS,
   PAYMENT_METHOD,
   RECEIPT_NUMBER,
   RECEIPT_DATE,
   RECEIPT_ACTIVITY_DATE,
   RECEIPT_STATUS,
   RECEIPT_STATE,
   RECEIPT_COMMENTS,
   CUSTOMER_RECEIPT_REF,
   REMIT_BANK_ACCOUNT,
   REVERSAL_CATEGORY,
   REVERSAL_COMMENTS,
   REVERSAL_DATE,
   REVERSAL_REASON,
   REVERSAL_REASON_CODE,
   ADJUSMENT_NAME,
   ADJUSTMENT_NUMBER,
   ENTERED_DR,
   ENTERED_CR,
   ACCOUNTED_DR,
   ACCOUNTED_CR,
   SLA_LINE_ACCOUNTED_DR,
   SLA_LINE_ACCOUNTED_CR,
   SLA_LINE_ACCOUNTED_NET,
   SLA_LINE_ENTERED_DR,
   SLA_LINE_ENTERED_CR,
   SLA_LINE_ENTERED_NET,
   SLA_DIST_ENTERED_CR,
   SLA_DIST_ENTERED_DR,
   SLA_DIST_ENTERED_NET,
   SLA_DIST_ACCOUNTED_CR,
   SLA_DIST_ACCOUNTED_DR,
   SLA_DIST_ACCOUNTED_NET,
   GL_ACCOUNT_STRING,
   PERIOD_NAME,
   TYPE,
   NAME,
   BATCH_NAME,
   GCC#50328#ACCOUNT,
   GCC#50328#COST_CENTER,
   GCC#50328#FURTURE_USE,
   GCC#50328#FUTURE_USE_2,
   GCC#50328#LOCATION,
   GCC#50328#PRODUCT,
   GCC#50328#PROJECT_CODE,
   GCC#50368#ACCOUNT,
   GCC#50368#DEPARTMENT,
   GCC#50368#DIVISION,
   GCC#50368#FUTURE_USE,
   GCC#50368#PRODUCT,
   GCC#50368#SUBACCOUNT,
   AR_TRX_SOURCE -- VER 1.1
)
AS
   SELECT jh.je_source SOURCE,
          jh.je_category,
          jl.effective_date acc_date,
          jh.NAME entry,
          SUBSTR (jl.description, 1, 240) line_descr,
          jl.accounted_dr line_acctd_dr,
          jl.accounted_cr line_acctd_cr,
          jl.entered_dr line_ent_dr,
          jl.entered_cr line_ent_cr,
          ar_dtls."DTL_ID",
          ar_dtls."TRANSACTION_NUMBER",
          ar_dtls."CUSTOMER_NUMBER",
          ar_dtls."CUSTOMER_OR_VENDOR",
          ar_dtls."DTL_ENTITY_ID",
          ar_dtls."RECEIPT_CLASS",
          ar_dtls."PAYMENT_METHOD",
          ar_dtls."RECEIPT_NUMBER",
          ar_dtls."RECEIPT_DATE",
          ar_dtls."RECEIPT_ACTIVITY_DATE",
          ar_dtls."RECEIPT_STATUS",
          ar_dtls."RECEIPT_STATE",
          ar_dtls."RECEIPT_COMMENTS",
          ar_dtls."CUSTOMER_RECEIPT_REF",
          ar_dtls."REMIT_BANK_ACCOUNT",
          ar_dtls."REVERSAL_CATEGORY",
          ar_dtls."REVERSAL_COMMENTS",
          ar_dtls."REVERSAL_DATE",
          ar_dtls."REVERSAL_REASON",
          ar_dtls."REVERSAL_REASON_CODE",
          ar_dtls."ADJUSMENT_NAME",
          ar_dtls."ADJUSTMENT_NUMBER", 
          DECODE (
             ra_dist.cust_trx_line_gl_dist_id,
             NULL, dist.AMOUNT_DR,
             TO_NUMBER (
                DECODE (
                   ra_dist.account_class,
                   'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                  -1, NULL,
                                  NVL (ra_dist.amount, 0)),
                   DECODE (SIGN (NVL (ra_dist.amount, 0)),
                           -1, -NVL (ra_dist.amount, 0),
                           NULL))))
             Entered_DR,
          DECODE (
             ra_dist.cust_trx_line_gl_dist_id,
             NULL, dist.AMOUNT_cR,
             TO_NUMBER (
                DECODE (
                   ra_dist.account_class,
                   'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                  -1, -NVL (ra_dist.amount, 0),
                                  NULL),
                   DECODE (SIGN (NVL (ra_dist.amount, 0)),
                           -1, NULL,
                           NVL (ra_dist.amount, 0)))))
             Entered_CR,
          DECODE (
             ra_dist.cust_trx_line_gl_dist_id,
             NULL, dist.ACCTD_AMOUNT_DR,
             TO_NUMBER (
                DECODE (
                   ra_dist.account_class,
                   'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                  -1, NULL,
                                  NVL (ra_dist.acctd_amount, 0)),
                   DECODE (SIGN (NVL (ra_dist.amount, 0)),
                           -1, -NVL (ra_dist.acctd_amount, 0),
                           NULL))))
             Accounted_DR,
          DECODE (
             ra_dist.cust_trx_line_gl_dist_id,
             NULL, dist.ACCTD_AMOUNT_CR,
             TO_NUMBER (
                DECODE (
                   ra_dist.account_class,
                   'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                  -1, -NVL (ra_dist.acctd_amount, 0),
                                  NULL),
                   DECODE (SIGN (NVL (ra_dist.amount, 0)),
                           -1, NULL,
                           NVL (ra_DIST.acctd_amount, 0)))))
             Accounted_CR,
          ael.unrounded_accounted_dr sla_line_accounted_dr,
          ael.unrounded_accounted_cr sla_line_accounted_cr,
            NVL (ael.unrounded_accounted_dr, 0)
          - NVL (ael.unrounded_accounted_cr, 0)
             sla_line_accounted_net,
          ael.unrounded_entered_dr sla_line_entered_dr,
          ael.unrounded_entered_cr sla_line_entered_cr,
            NVL (ael.unrounded_entered_dr, 0)
          - NVL (ael.unrounded_entered_cr, 0)
             sla_line_entered_net,
          xdl.unrounded_entered_cr sla_dist_entered_cr,
          xdl.unrounded_entered_dr sla_dist_entered_dr,
            NVL (xdl.unrounded_entered_cr, 0)
          - NVL (xdl.unrounded_entered_dr, 0)
             sla_dist_entered_net,
          xdl.unrounded_accounted_cr sla_dist_accounted_cr,
          xdl.unrounded_accounted_dr sla_dist_accounted_dr,
            NVL (xdl.unrounded_accounted_cr, 0)
          - NVL (xdl.unrounded_accounted_dr, 0)
             sla_dist_accounted_net,
          gcc.CONCATENATED_SEGMENTS gl_account_string,
          jh.period_name,
          NVL (dist.source_type, NULL) TYPE,
          gle.name,
          gb.name batch_name,
          GCC.SEGMENT4 GCC#50328#ACCOUNT,
          GCC.SEGMENT3 GCC#50328#COST_CENTER,
          GCC.SEGMENT6 GCC#50328#FURTURE_USE,
          GCC.SEGMENT7 GCC#50328#FUTURE_USE_2,
          GCC.SEGMENT2 GCC#50328#LOCATION,
          GCC.SEGMENT1 GCC#50328#PRODUCT,
          GCC.SEGMENT5 GCC#50328#PROJECT_CODE,
          GCC.SEGMENT4 GCC#50368#ACCOUNT,
          GCC.SEGMENT3 GCC#50368#DEPARTMENT,
          GCC.SEGMENT2 GCC#50368#DIVISION,
          GCC.SEGMENT6 GCC#50368#FUTURE_USE,
          GCC.SEGMENT1 GCC#50368#PRODUCT,
          GCC.SEGMENT5 GCC#50368#SUBACCOUNT,
          AR_DTLS.AR_SOURCE AR_TRX_SOURCE  -- VER 1.1
     FROM gl_ledgers gle,
          gl_je_headers jh,
          gl_je_lines jl,
          gl_code_combinations_kfv gcc,
          gl_import_references jir,
          xla_distribution_links xdl,
          xla_ae_lines ael,
          xla_ae_headers aeh,
          ar_distributions_all dist,
          ra_cust_trx_line_gl_dist_all ra_dist,
          gl_period_statuses gps,
          gl_je_batches gb,
          xla_events xlae,
          (SELECT trx.customer_trx_id dtl_id,
                  trx.trx_number transaction_number,
                  hzca.account_number customer_number,
                  hzp.party_name customer_or_vendor,
                  xlate.entity_id dtl_entity_id,
                  TO_CHAR (NULL) receipt_class,
                  TO_CHAR (NULL) payment_method,
                  TO_CHAR (NULL) receipt_number,
                  TO_DATE (NULL) receipt_date,
                  TO_DATE (NULL) receipt_activity_date,
                  TO_CHAR (NULL) receipt_status,
                  TO_CHAR (NULL) receipt_state,
                  TO_CHAR (NULL) receipt_comments,
                  TO_CHAR (NULL) customer_receipt_ref,
                  TO_CHAR (NULL) remit_bank_account,
                  TO_CHAR (NULL) reversal_category,
                  TO_CHAR (NULL) reversal_comments,
                  TO_DATE (NULL) reversal_date,
                  TO_CHAR (NULL) reversal_reason,
                  TO_CHAR (NULL) reversal_reason_code,
                  TO_CHAR (NULL) adjusment_name,
                  TO_CHAR (NULL) adjustment_number,
                  -- BEGIN -- VER 1.1
                  (SELECT /*+ RESULT_CACHE */
                         NAME AR_SOURCE
                     FROM RA_BATCH_SOURCES_ALL
                    WHERE 1 = 1 AND BATCH_SOURCE_ID = TRX.BATCH_SOURCE_ID)
                     AR_SOURCE
             -- END VER 1.1
             FROM ra_customer_trx_all trx,
                  hz_cust_accounts hzca,
                  hz_parties hzp,
                  xla_transaction_entities xlate,
                  fnd_application fndapp
            WHERE     1 = 1
                  AND fndapp.application_short_name = 'AR'
                  AND xlate.application_id = fndapp.application_id
                  AND xlate.entity_code = 'TRANSACTIONS'
                  AND xlate.source_id_int_1 = trx.customer_trx_id
                  AND hzca.cust_account_id = trx.bill_to_customer_id
                  AND hzp.party_id = hzca.party_id
           UNION
           SELECT crv.cash_receipt_id dtl_id,
                  TO_CHAR (NULL) transaction_number,
                  crv.customer_number customer_number,
                  crv.customer_name customer_or_vendor,
                  xlate.entity_id dtl_entity_id,
                  receipt_class_dsp receipt_class,
                  crv.payment_method_dsp payment_method,
                  crv.receipt_number receipt_number,
                  crv.receipt_date receipt_date,
                  crh.trx_date receipt_activity_date,
                  crv.receipt_status_dsp receipt_status,
                  crh.status receipt_state,
                  crv.comments receipt_comments,
                  crv.customer_receipt_reference customer_receipt_ref,
                  crv.remit_bank_account remit_bank_account,
                  crv.reversal_category reversal_category,
                  crv.reversal_comments reversal_comments,
                  NVL (crv.reversal_date, TO_DATE (NULL)) reversal_date,
                  crv.reversal_reason reversal_reason,
                  crv.reversal_reason_code reversal_reason_code,
                  TO_CHAR (NULL) adjusment_name,
                  TO_CHAR (NULL) adjustment_number,
                  TO_CHAR (NULL) AR_SOURCE -- VER 1.1
             FROM ar_cash_receipts_v crv,
                  ar_cash_receipt_history crh,
                  xla_transaction_entities xlate,
                  fnd_application fndapp
            WHERE     1 = 1
                  AND fndapp.application_short_name = 'AR'
                  AND xlate.application_id = fndapp.application_id
                  AND xlate.entity_code = 'RECEIPTS'
                  AND xlate.source_id_int_1 = crv.cash_receipt_id
                  AND crh.cash_receipt_id = crv.cash_receipt_id
                  AND crh.current_record_flag = 'Y'
           UNION
           SELECT adj.adjustment_id dtl_id,
                  adj.trx_number transaction_number,
                  adj.customer_number customer_number,
                  adj.customer_name customer_or_vendor,
                  xlate.entity_id dtl_entity_id,
                  TO_CHAR (NULL) receipt_class,
                  TO_CHAR (NULL) payment_method,
                  TO_CHAR (NULL) receipt_number,
                  TO_DATE (NULL) receipt_date,
                  TO_DATE (NULL) receipt_activity_date,
                  TO_CHAR (NULL) receipt_status,
                  TO_CHAR (NULL) receipt_state,
                  TO_CHAR (NULL) receipt_comments,
                  TO_CHAR (NULL) customer_receipt_ref,
                  TO_CHAR (NULL) remit_bank_account,
                  TO_CHAR (NULL) reversal_category,
                  TO_CHAR (NULL) reversal_comments,
                  TO_DATE (NULL) reversal_date,
                  TO_CHAR (NULL) reversal_reason,
                  TO_CHAR (NULL) reversal_reason_code,
                  adj.activity_name adjusment_name,
                  adj.adjustment_number adjustment_number,
                  -- BEGIN VER 1.1
                  (SELECT /*+ RESULT_CACHE */
                         A.NAME AR_SOURCE
                     FROM RA_BATCH_SOURCES_ALL A, RA_CUSTOMER_TRX_ALL B
                    WHERE     1 = 1
                          AND B.CUSTOMER_TRX_ID = ADJ.CUSTOMER_TRX_ID
                          AND A.BATCH_SOURCE_ID = B.BATCH_SOURCE_ID)
                     AR_SOURCE
             -- END VER 1.1
             FROM ar_adjustments_v adj,
                  xla_transaction_entities xlate,
                  fnd_application fndapp
            WHERE     1 = 1
                  AND fndapp.application_short_name = 'AR'
                  AND xlate.application_id = fndapp.application_id
                  AND xlate.entity_code = 'ADJUSTMENTS'
                  AND xlate.source_id_int_1 = adj.adjustment_id) ar_dtls
    WHERE     jh.je_header_id = jl.je_header_id
          AND jl.code_combination_id = gcc.code_combination_id
          AND jh.je_header_id = jir.je_header_id
          AND jl.je_line_num = jir.je_line_num
          AND jh.ledger_id = gle.ledger_id
          AND jh.je_source = 'Receivables'
          AND jir.gl_sl_link_id = ael.gl_sl_link_id(+)
          AND jir.gl_sl_link_table = ael.gl_sl_link_table(+)
          AND ael.ae_line_num = xdl.ae_line_num(+)
          AND ael.ae_header_id = xdl.ae_header_id(+)
          AND ael.ae_header_id = aeh.ae_header_id(+)
          AND xlae.application_id(+) = aeh.application_id
          AND xlae.event_id(+) = aeh.event_id
          AND ar_dtls.dtl_entity_id(+) = xlae.entity_id
          AND dist.line_id(+) =
                 DECODE (
                    xdl.source_distribution_type,
                    'AR_DISTRIBUTIONS_ALL', xdl.source_distribution_id_num_1,
                    NULL)
          AND ra_dist.cust_trx_line_gl_dist_id(+) =
                 DECODE (xdl.source_distribution_type,
                         'AR_DISTRIBUTIONS_ALL', NULL,
                         xdl.source_distribution_id_num_1)
          AND gps.application_id = 101
          AND gps.period_name = jh.period_name
          AND gps.ledger_id = gle.ledger_id
          AND jh.je_batch_id = gb.je_batch_id;
