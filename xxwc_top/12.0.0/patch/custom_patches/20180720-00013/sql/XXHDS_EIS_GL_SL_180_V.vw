CREATE OR REPLACE VIEW XXEIS.XXHDS_EIS_GL_SL_180_V 
/*************************************************************************
-- $Header XXHDS_EIS_GL_SL_180_V $
-- Module Name: XXEIS 

-- REVISIONS:
-- Ver        Date        Author             Description
-- ---------  ----------  ----------         ----------------
-- 1.1       03/18/2015  Balaguru Seshadri   TMS# 20150105-00229 / ESMS# 281615  
-- 1.2       03/19/2015  Balaguru Seshadri   TMS# 20150105-00229 / ESMS# 281615 
--                                           Add rownum clause to fetch only one record
-- 1.3       09/15/2015 Maharajan Shunmugam  TMS#20150602-00007 Freight - Including a SO/PO number from TMS in Oracle    
-- 1.4       12/21/2015 Neha Saini           TMS#20151027-00222 Freight - removing condition to pull only US Ledger detail we want CANADA details too.                                   
-- 1.5  	 04/18/2016 Siva Amineni       TMS#20160426-00098 Performance Tuning
-- 1.6       05/12/2016 Siva Amineni       TMS#20160503-00101 Performance Tuning
-- 1.7		 10/07/2016 Siva Amineni		 TMS#20160930-00078
-- 1.8		 22/06/2017 Siva Amineni		 TMS#20170613-00012  
-- 1.9		 03/08/2017 Siva Amineni		 TMS#20170508-00286
-- 2.0       07/20/2018 Balaguru Seshadri    Update to EiS View_Report for Subledger Recon - HARRIS (TMS# 20180720-00013)
-- **************************************************************************/
(SOURCE, JE_CATEGORY, JE_HEADER_ID, JE_LINE_NUM, ACC_DATE, ENTRY, BATCH, LINE_DESCR, LSEQUENCE, HNUMBER, LLINE, LINE_ACCTD_DR, LINE_ACCTD_CR, LINE_ENT_DR, LINE_ENT_CR, SEQ_ID, SEQ_NUM, H_SEQ_ID, GL_SL_LINK_ID, CUSTOMER_OR_VENDOR, CUSTOMER_OR_VENDOR_NUMBER, ENTERED_DR, ENTERED_CR, ACCOUNTED_DR, ACCOUNTED_CR, SLA_LINE_ACCOUNTED_DR, SLA_LINE_ACCOUNTED_CR, SLA_LINE_ACCOUNTED_NET, SLA_LINE_ENTERED_DR, SLA_LINE_ENTERED_CR, SLA_LINE_ENTERED_NET, SLA_DIST_ENTERED_CR, SLA_DIST_ENTERED_DR, SLA_DIST_ENTERED_NET, SLA_DIST_ACCOUNTED_CR, SLA_DIST_ACCOUNTED_DR, SLA_DIST_ACCOUNTED_NET, ASSOCIATE_NUM, TRANSACTION_NUM, SALES_ORDER, GL_ACCOUNT_STRING, CURRENCY_CODE, PERIOD_NAME, TYPE, EFFECTIVE_PERIOD_NUM, NAME, BATCH_NAME, PO_NUMBER, BOL_NUMBER, AP_INV_SOURCE, SLA_EVENT_TYPE, PART_NUMBER, PART_DESCRIPTION, ITEM_UNIT_WT, ITEM_TXN_QTY, ITEM_MATERIAL_COST, ITEM_OVRHD_COST, XXCUS_LINE_DESC, XXCUS_PO_CREATED_BY, XXCUS_IMAGE_LINK, CODE_COMBINATION_ID, GCC50328PRODUCT, GCC50328PRODUCTDESCR, GCC50328LOCATION, GCC50328LOCATIONDESCR, GCC50328COST_CENTER, GCC50328COST_CENTERDESCR, GCC50328ACCOUNT, GCC50328ACCOUNTDESCR, GCC50328PROJECT_CODE, GCC50328PROJECT_CODEDESCR, GCC50328FURTURE_USE, GCC50328FURTURE_USEDESCR, GCC50328FUTURE_USE_2, GCC50328FUTURE_USE_2DESCR, GCC50368PRODUCT, GCC50368PRODUCTDESCR, GCC50368DIVISION, GCC50368DIVISIONDESCR, GCC50368DEPARTMENT, GCC50368DEPARTMENTDESCR, GCC50368ACCOUNT, GCC50368ACCOUNTDESCR, GCC50368SUBACCOUNT, GCC50368SUBACCOUNTDESCR, GCC50368FUTURE_USE, GCC50368FUTURE_USEDESCR, SOURCE_DIST_TYPE, SOURCE_DIST_ID_NUM_1, APPLIED_TO_SOURCE_ID_NUM_1, FETCH_SEQ, AP_INV_DATE, WAYBILL, CATEGORY_NAME, CATEGORY_DESCRIPTION, CATMGT_CATEGORY, CATMGT_CATEGORY_DESC, PO_ATTRIBUTE2, ORG_CODE, SHIP_TO, TRANSACTION_DR, TRANSACTION_CR, AR_TRX_SOURCE, AR_TRX_CREATE_DATE) --Ver 2.0
AS 
  (                                                                        --
    -- begin of source other than ('Payables', 'Receivables', 'Cost Management') -Part 1
    --
    SELECT /*+ INDEX(gcc GL_CODE_COMBINATIONS_U1)*/  --TMS# 20160426-00098
           jes.user_je_source_name SOURCE,
           jh.je_category,
           jl.je_header_id je_header_id,
           jl.je_line_num je_line_num,
           jl.effective_date acc_date,
           jh.NAME entry,
           jh.je_batch_id batch,
           SUBSTR (jl.description, 1, 240) line_descr,
           NULL LSequence,
           jh.doc_sequence_value hnumber,
           jl.je_line_num lline,
           jl.accounted_dr line_acctd_dr,
           jl.accounted_cr line_acctd_cr,
           jl.entered_dr line_ent_dr,
           jl.entered_cr line_ent_cr,
           NULL seq_id,
           NULL seq_num,
           jh.doc_sequence_id h_seq_id,
           NULL gl_sl_link_id,
           TO_CHAR (NULL) customer_or_vendor,
           TO_CHAR (NULL) customer_or_vendor_number,
           jl.entered_dr ENTERED_DR,
           jl.entered_cr ENTERED_CR,
           jl.accounted_cr ACCOUNTED_DR,
           jl.accounted_dr ACCOUNTED_CR,
           jl.accounted_dr sla_line_accounted_dr,
           jl.accounted_cr sla_line_accounted_cr,
           NVL (jl.accounted_dr, 0) - NVL (jl.accounted_cr, 0)
              sla_line_accounted_net,
           jl.entered_dr sla_line_entered_dr,
           jl.entered_cr sla_line_entered_cr,
           NVL (jl.entered_dr, 0) - NVL (jl.entered_cr, 0)
              sla_line_entered_net,
           jl.entered_cr sla_dist_entered_cr,
           jl.entered_dr sla_dist_entered_dr,
           NVL (jl.entered_cr, 0) - NVL (jl.entered_dr, 0)
              sla_dist_entered_net,
           jl.accounted_cr sla_dist_accounted_cr,
           jl.accounted_dr sla_dist_accounted_dr,
           --NVL (jl.accounted_cr, 0) - NVL (jl.accounted_dr, 0)
           -- sla_dist_accounted_net,
           NVL (jl.accounted_dr, 0) - NVL (jl.accounted_cr, 0)
              sla_dist_accounted_net,
           TO_CHAR (NULL) associate_num,
           TO_CHAR (NULL) transaction_num,
           TO_CHAR (NULL) SALES_ORDER,
           gcc.CONCATENATED_SEGMENTS gl_account_string,
           jh.currency_code,
           jh.period_name,
           NULL TYPE,
           gps.effective_period_num,
           gle.name,
           gb.name batch_name,
           TO_CHAR (NULL) PO_NUMBER,
           TO_CHAR (NULL) BOL_NUMBER,
           TO_CHAR (NULL) AP_INV_SOURCE,
           NULL SLA_EVENT_TYPE,
           TO_CHAR (NULL) PART_NUMBER, --TMS# 20150105-00229
           TO_CHAR (NULL) PART_DESCRIPTION, --TMS# 20150105-00229
           TO_NUMBER (NULL) ITEM_UNIT_WT, --TMS# 20150105-00229
           TO_NUMBER (NULL) ITEM_TXN_QTY, --TMS# 20150105-00229
           TO_NUMBER (NULL) ITEM_TOTAL_TXN_COST, -- / ESMS# 281615
           TO_NUMBER (NULL) ITEM_OVRHD_CALC_COST, --ESMS# 281615
           SUBSTR (jl.description, 1, 240) XXCUS_GSC_ENH_LINE_DESC, --ESMS# 281615
           TO_CHAR (NULL) XXCUS_GSC_PO_CREATED_BY, --TMS# 20150105-00229 / ESMS# 281615
           TO_CHAR (NULL) XXCUS_GSC_IMAGE_LINK,
           gcc.code_combination_id,                    
           GCC.SEGMENT1 GCC50328PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_PRODUCT')
              GCC50328PRODUCTDESCR,
           GCC.SEGMENT2 GCC50328LOCATION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_LOCATION')
              GCC50328LOCATIONDESCR,
           GCC.SEGMENT3 GCC50328COST_CENTER,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                 'XXCUS_GL_COSTCENTER')
              GCC50328COST_CENTERDESCR,
           GCC.SEGMENT4 GCC50328ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ACCOUNT')
              GCC50328ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50328PROJECT_CODE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                 'XXCUS_GL_PROJECT')
              GCC50328PROJECT_CODEDESCR,
           GCC.SEGMENT6 GCC50328FURTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_FUTURE_USE1')
              GCC50328FURTURE_USEDESCR,
           GCC.SEGMENT7 GCC50328FUTURE_USE_2,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7,
                                                 'XXCUS_GL_FUTURE_USE_2')
              GCC50328FUTURE_USE_2DESCR,
           GCC.SEGMENT1 GCC50368PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_LTMR_PRODUCT')
              GCC50368PRODUCTDESCR,
           GCC.SEGMENT2 GCC50368DIVISION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_ LTMR _DIVISION')
              GCC50368DIVISIONDESCR,
           GCC.SEGMENT3 GCC50368DEPARTMENT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT3,
              'XXCUS_GL_ LTMR _DEPARTMENT')
              GCC50368DEPARTMENTDESCR,
           GCC.SEGMENT4 GCC50368ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ LTMR _ACCOUNT')
              GCC50368ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50368SUBACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT5,
              'XXCUS_GL_ LTMR _SUBACCOUNT')
              GCC50368SUBACCOUNTDESCR,
           GCC.SEGMENT6 GCC50368FUTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_ LTMR _FUTUREUSE')
              GCC50368FUTURE_USEDESCR,
           TO_CHAR (NULL), --start 1.1
           TO_NUMBER (NULL),
           TO_NUMBER (NULL),
           1 FETCH_SEQ,
           TO_DATE (NULL) ap_inv_date, --end 1.1
		   NULL WAYBILL  --added for version 1.7
           ,TO_CHAR (NULL) CATEGORY_NAME, -- added for version 1.8
           TO_CHAR (NULL) CATEGORY_DESCRIPTION, -- added for version 1.8
           NULL CATMGT_CATEGORY, -- added for version 1.8
           NULL CATMGT_CATEGORY_DESC, -- added for version 1.8
		   to_char(NULL) po_attribute2, --added for version 1.9
           to_char(NULL) org_code, --added for version 1.9
           to_char(NULL) ship_to,  --added for version 1.9
           jl.accounted_dr transaction_dr, --added for version 1.9
           jl.accounted_cr transaction_cr --added for version 1.9
		  , TO_CHAR(NULL) AR_TRX_SOURCE --Ver 2.0
		  , TO_DATE(NULL) AR_TRX_CREATE_DATE --Ver 2.0
      FROM gl_je_lines jl,
           gl_je_headers jh,
           gl_code_combinations_kfv gcc,
           gl_ledgers gle,
           gl_period_statuses gps,
           gl_je_batches gb,
           gl_je_sources jes
     WHERE     jl.status = 'P'
           AND jh.status = 'P'
           AND jh.actual_flag = 'A'
           AND jh.je_header_id = jl.je_header_id
           AND jes.user_je_source_name NOT IN
                  ('Payables', 'Receivables', 'Cost Management') --bcoz these three sources are fetched down below separately
           and JL.CODE_COMBINATION_ID = GCC.CODE_COMBINATION_ID 
           -- AND gle.name = 'HD Supply USD' commented by Neha for TMS#20151027-00222 
           and JH.LEDGER_ID  =   GLE.LEDGER_ID   --TMS# 20160426-00098 added  4/18/16
           -- AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id  --TMS# 20160426-00098 removed 4/18/16
           and GPS.APPLICATION_ID = 101
           and JH.PERIOD_NAME = GPS.PERIOD_NAME 
           AND gle.ledger_id  = gps.ledger_id 
           and JH.JE_BATCH_ID = GB.JE_BATCH_ID
           AND jh.je_source   = jes.je_source_name 
    --
    -- end of source other than ('Payables', 'Receivables', 'Cost Management') -Part 1
    --
    UNION ALL
    --
    -- begin of JE source 'Cost Management' and JE Category "Inventory" -Part 2
    --
   SELECT /*+ INDEX(gcc GL_CODE_COMBINATIONS_U1) INDEX(gle GL_LEDGERS_U2)*/  --TMS# 20160426-00098
  jes.user_je_source_name SOURCE,
  JH.JE_CATEGORY,
  jl.je_header_id je_header_id,
  jl.je_line_num je_line_num,
  jl.effective_date acc_date,
  jh.NAME entry,
  jh.je_batch_id batch,
  SUBSTR (jl.description, 1, 240) line_descr,
  NULL LSequence,
  jh.doc_sequence_value hnumber,
  jl.je_line_num lline,
  jl.accounted_dr line_acctd_dr,
  jl.accounted_cr line_acctd_cr,
  jl.entered_dr line_ent_dr,
  jl.entered_cr line_ent_cr,
  ir.subledger_doc_sequence_id seq_id,
  ir.subledger_doc_sequence_value seq_num,
  jh.doc_sequence_id h_seq_id,
  ir.gl_sl_link_id gl_sl_link_id,
  --
  CASE
      --
    WHEN xeh.event_type_code IN ('PO_DEL_INV', 'PO_DEL_ADJ', 'RET_RI_INV')
    THEN
      (
        SELECT /*+ RESULT_CACHE */
          aps.vendor_name
        FROM
          po_headers poh,
          ap_suppliers aps
        WHERE
          1                  = 1
        AND poh.po_header_id = mta.transaction_source_id --from main SQL
        AND aps.vendor_id    = poh.vendor_id
      )
      --
    WHEN xeh.event_type_code = 'FOB_RCPT_SENDER_RCPT_NO_TP'
    THEN
      (
        SELECT  /*+ RESULT_CACHE */
          hzp.party_name
        FROM
          mtl_material_transactions mmt,
          rcv_transactions rcv,
          oe_order_lines_all oeol,
          hz_cust_accounts hzca,
          hz_parties hzp
        WHERE
          1                         = 1
        AND mmt.transaction_id      = mta.transaction_id --from main SQL
        and RCV.TRANSACTION_ID      = MMT.RCV_TRANSACTION_ID
        and OEOL.SOURCE_DOCUMENT_ID = MTA.TRANSACTION_SOURCE_ID --internal requisition header id
        AND oeol.source_document_line_id = rcv.requisition_line_id --internal requisition line id
        AND hzca.cust_account_id = oeol.sold_to_org_id
        AND hzp.party_id         = hzca.party_id
        AND ROWNUM               <2 --Ver 1.2
      )
      --
    WHEN xeh.event_type_code = 'AVG_COST_UPD'
    THEN TO_CHAR (NULL)
      --
    ELSE TO_CHAR (NULL)
      --
  END customer_or_vendor, --TMS# 20150105-00229
  --
  CASE
      --
    WHEN xeh.event_type_code IN ('PO_DEL_INV', 'PO_DEL_ADJ', 'RET_RI_INV')
    THEN
      (
        SELECT  /*+ RESULT_CACHE */
          aps.segment1
        FROM
          po_headers poh,
          ap_suppliers aps
        WHERE
          1                  = 1
        AND poh.po_header_id = mta.transaction_source_id --from main SQL
        AND aps.vendor_id    = poh.vendor_id
      )
      --
    WHEN xeh.event_type_code = 'FOB_RCPT_SENDER_RCPT_NO_TP'
    THEN
      (
        SELECT /*+ RESULT_CACHE */
          hzca.account_number
        FROM
          mtl_material_transactions mmt,
          rcv_transactions rcv,
          oe_order_lines_all oeol,
          hz_cust_accounts hzca
        WHERE
          1                         = 1
        AND mmt.transaction_id      = mta.transaction_id --from main SQL
        AND rcv.transaction_id      = mmt.rcv_transaction_id
        AND oeol.source_document_id = mta.transaction_source_id --internal requisition header id
        AND oeol.source_document_line_id = rcv.requisition_line_id --internal requisition line id
        AND hzca.cust_account_id = oeol.sold_to_org_id
        AND ROWNUM               <2 --Ver 1.2
      )
      --
    WHEN xeh.event_type_code = 'AVG_COST_UPD'
    THEN TO_CHAR (NULL)
      --
    ELSE TO_CHAR (NULL)
      --
  END customer_or_vendor_number, --TMS# 20150105-00229
  --
  jl.entered_dr ENTERED_DR,
  jl.entered_cr ENTERED_CR,
  jl.accounted_cr ACCOUNTED_DR,
  jl.accounted_dr ACCOUNTED_CR,
  xel.unrounded_accounted_dr sla_line_accounted_dr,
  xel.unrounded_accounted_cr sla_line_accounted_cr,
  NVL (xel.unrounded_accounted_dr, 0) - NVL (xel.unrounded_accounted_cr, 0)
  sla_line_accounted_net,
  xel.unrounded_entered_dr sla_line_entered_dr,
  xel.unrounded_entered_cr sla_line_entered_cr,
  NVL (xel.unrounded_entered_dr, 0) - NVL (xel.unrounded_entered_cr, 0)
  sla_line_entered_net,
  xdl.unrounded_entered_cr sla_dist_entered_cr,
  xdl.unrounded_entered_dr sla_dist_entered_dr,
  NVL (xdl.unrounded_entered_cr, 0) - NVL (xdl.unrounded_entered_dr, 0)
  sla_dist_entered_net,
  xdl.unrounded_accounted_cr sla_dist_accounted_cr,
  xdl.unrounded_accounted_dr sla_dist_accounted_dr,
  NVL (xdl.unrounded_accounted_dr, 0) - NVL (xdl.unrounded_accounted_cr, 0)
  sla_dist_accounted_net,
  TO_CHAR (NULL) associate_num,
  --
  CASE 
    WHEN xeh.event_type_code = 'AVG_COST_UPD'
    THEN TO_CHAR (NULL) 
    ELSE TO_CHAR (NULL) 
  END transaction_num,
  --
  CASE
      --
    WHEN xeh.event_type_code = 'FOB_RCPT_SENDER_RCPT_NO_TP'
    THEN
      (
        SELECT  /*+ RESULT_CACHE */
          TO_CHAR (oeoh.order_number)
        FROM
          mtl_material_transactions mmt,
          rcv_transactions rcv,
          oe_order_lines oeol,
          oe_order_headers oeoh
        WHERE
          1                         = 1
        AND mmt.transaction_id      = mta.transaction_id --from main SQL
        and RCV.TRANSACTION_ID      = MMT.RCV_TRANSACTION_ID
        AND oeol.source_document_id = mta.transaction_source_id --internal requisition header id
        AND oeol.source_document_line_id = rcv.requisition_line_id --internal requisition line id
        AND oeoh.header_id = oeol.header_id
        AND ROWNUM         <2 --Ver 1.2
      )
      --
    WHEN xeh.event_type_code = 'AVG_COST_UPD'
    THEN TO_CHAR (NULL)
      --
     WHEN xeh.event_type_code IN ('PO_DEL_INV', 'PO_DEL_ADJ', 'RET_RI_INV')
    THEN
      to_char(nvl((
        SELECT oh.order_number
		FROM oe_order_headers oh,
			 oe_drop_ship_sources odss
		WHERE oh.header_id   =odss.header_id
		AND odss.po_header_id=mta.transaction_source_id
		AND rownum           =1
      ),(SELECT trim(SUBSTR(PRL.NOTE_TO_RECEIVER,instr(PRL.NOTE_TO_RECEIVER,':',1)+1,instr(PRL.NOTE_TO_RECEIVER,',',1)-instr(PRL.NOTE_TO_RECEIVER,':',1)-1))so
      FROM po_distributions_all pod,
        po_req_distributions_all prd,
        po_requisition_lines_all prl,
        po_requisition_headers_all prh
      WHERE 
          pod.po_header_id        = mta.transaction_source_id
      AND pod.req_distribution_id = prd.distribution_id
      AND prd.requisition_line_id = prl.requisition_line_id
      and prl.requisition_header_id = prh.requisition_header_id
      and prh.interface_source_code='CTO'
      AND ROWNUM                  =1
      )))  --added for version 1.9	
    ELSE TO_CHAR (NULL)
      --
  END sales_order, --TMS# 20150105-00229
  --
  gcc.CONCATENATED_SEGMENTS gl_account_string,
  jh.currency_code,
  jh.period_name,
  xel.accounting_class_code TYPE,
  gps.effective_period_num,
  gle.name,
  gb.name batch_name,
  --
  CASE
    WHEN xeh.event_type_code IN ('PO_DEL_INV', 'PO_DEL_ADJ', 'RET_RI_INV')
    THEN
      (
        SELECT  /*+ RESULT_CACHE */
          poh.segment1
        FROM
          po_headers poh
        WHERE
          1                  = 1
        AND poh.po_header_id = mta.transaction_source_id
      )
      --
    WHEN xeh.event_type_code = 'FOB_RCPT_SENDER_RCPT_NO_TP'
    THEN TO_CHAR (NULL)
      --
    WHEN xeh.event_type_code = 'AVG_COST_UPD'
    THEN TO_CHAR (NULL)
      --
    ELSE TO_CHAR (NULL)
      --
  END PO_NUMBER, --TMS# 20150105-00229
  --
  TO_CHAR (NULL) BOL_NUMBER,    --TMS# 20150105-00229
  TO_CHAR (NULL) AP_INV_SOURCE, --TMS# 20150105-00229
  --
  xeh.event_type_code SLA_EVENT_TYPE,
  --
  msi.segment1 PART_NUMBER,         --TMS# 20150105-00229
  msi.description PART_DESCRIPTION, --TMS# 20150105-00229
  --
  msi.unit_weight ITEM_UNIT_WT,            --TMS# 20150105-00229
  --ABS (mta.primary_quantity) ITEM_TXN_QTY, --Commented for version 1.9	
  decode(xeh.event_type_code,'PO_DEL_INV',abs(mta.primary_quantity),decode((decode(xdl.ae_header_id,null,XEL.unrounded_accounted_cr,xdl.unrounded_accounted_cr)),null,-1*(abs(mta.primary_quantity)),abs(mta.primary_quantity))) ITEM_TXN_QTY, --added for version 1.9	
  (
    SELECT
      transaction_cost
    FROM
      mtl_material_transactions
    WHERE
      1                = 1
    AND transaction_id = mta.transaction_id
  )
  ITEM_TOTAL_TXN_COST,                     --ESMS# 281615
  mta.rate_or_amount ITEM_OVRHD_CALC_COST, --TMS# 20150105-00229
  --
  TO_CHAR (NULL) XXCUS_GSC_ENH_LINE_DESC, --ESMS# 281615
  TO_CHAR (NULL) XXCUS_GSC_PO_CREATED_BY, --ESMS# 281615
  TO_CHAR (NULL) XXCUS_GSC_IMAGE_LINK,    --ESMS# 281615
  --
  gcc.code_combination_id,
  GCC.SEGMENT1 GCC50328PRODUCT,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1, 'XXCUS_GL_PRODUCT')
  GCC50328PRODUCTDESCR,
  GCC.SEGMENT2 GCC50328LOCATION,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2, 'XXCUS_GL_LOCATION')
  GCC50328LOCATIONDESCR,
  GCC.SEGMENT3 GCC50328COST_CENTER,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3, 'XXCUS_GL_COSTCENTER')
  GCC50328COST_CENTERDESCR,
  GCC.SEGMENT4 GCC50328ACCOUNT,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4, 'XXCUS_GL_ACCOUNT')
  GCC50328ACCOUNTDESCR,
  GCC.SEGMENT5 GCC50328PROJECT_CODE,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5, 'XXCUS_GL_PROJECT')
  GCC50328PROJECT_CODEDESCR,
  GCC.SEGMENT6 GCC50328FURTURE_USE,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6, 'XXCUS_GL_FUTURE_USE1')
  GCC50328FURTURE_USEDESCR,
  GCC.SEGMENT7 GCC50328FUTURE_USE_2,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7, 'XXCUS_GL_FUTURE_USE_2')
  GCC50328FUTURE_USE_2DESCR,
  GCC.SEGMENT1 GCC50368PRODUCT,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1, 'XXCUS_GL_LTMR_PRODUCT')
  GCC50368PRODUCTDESCR,
  GCC.SEGMENT2 GCC50368DIVISION,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2, 'XXCUS_GL_ LTMR _DIVISION') GCC50368DIVISIONDESCR,
  GCC.SEGMENT3 GCC50368DEPARTMENT,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3, 'XXCUS_GL_ LTMR _DEPARTMENT') GCC50368DEPARTMENTDESCR,
  GCC.SEGMENT4 GCC50368ACCOUNT,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4, 'XXCUS_GL_ LTMR _ACCOUNT' ) GCC50368ACCOUNTDESCR,
  GCC.SEGMENT5 GCC50368SUBACCOUNT,
  xxeis.eis_rs_fin_utility.decode_vset ( GCC.SEGMENT5, 'XXCUS_GL_ LTMR _SUBACCOUNT') GCC50368SUBACCOUNTDESCR,
  GCC.SEGMENT6 GCC50368FUTURE_USE,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6, 'XXCUS_GL_ LTMR _FUTUREUSE') GCC50368FUTURE_USEDESCR,
  xdl.source_distribution_type,     --TMS# 20150105-00229
  xdl.source_distribution_id_num_1, --TMS# 20150105-00229
  xdl.applied_to_source_id_num_1,   --TMS# 20150105-00229
  2 FETCH_SEQ,                      --TMS# 20150105-00229
  TO_DATE (NULL) ap_inv_date,        --TMS# 20150105-00229
  NULL WAYBILL  --added for version 1.7
  ,(SELECT /*+ RESULT_CACHE */
      MC.CONCATENATED_SEGMENTS
    FROM MTL_SYSTEM_ITEMS_B MSIB,
         MTL_ITEM_CATEGORIES MIT,
         MTL_CATEGORIES_B_KFV MC
    WHERE MSIB.INVENTORY_ITEM_ID = MIT.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID     = MIT.ORGANIZATION_ID
      AND MIT.CATEGORY_ID         = MC.CATEGORY_ID
      AND MIT.CATEGORY_SET_ID     = 1100000062
      AND MSIB.INVENTORY_ITEM_ID   = MSI.INVENTORY_ITEM_ID 
      and MSIB.ORGANIZATION_ID     = MSI.ORGANIZATION_ID) CATEGORY_NAME, -- added for version 1.8
  (SELECT /*+ RESULT_CACHE */
      MCT.description
    FROM MTL_SYSTEM_ITEMS_B MSIB,
         MTL_ITEM_CATEGORIES MIT,
         MTL_CATEGORIES_TL MCT
    WHERE MSIB.INVENTORY_ITEM_ID = MIT.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID     = MIT.ORGANIZATION_ID
      AND MIT.CATEGORY_ID         = MCT.CATEGORY_ID
      AND MIT.CATEGORY_SET_ID     = 1100000062
      AND MSIB.INVENTORY_ITEM_ID   = MSI.INVENTORY_ITEM_ID 
      and MSIB.ORGANIZATION_ID     = MSI.ORGANIZATION_ID) CATEGORY_DESCRIPTION, -- added for version 1.8
 (SELECT /*+ RESULT_CACHE */
		mc.ATTRIBUTE5 CATMGT_CATEGORY
    FROM MTL_SYSTEM_ITEMS_B MSIB,
         MTL_ITEM_CATEGORIES MIT,
         MTL_CATEGORIES_B_KFV MC,
	   APPS.FND_FLEX_VALUES B_CAT,
    APPS.FND_FLEX_VALUE_SETS V_CAT
    WHERE MSIB.INVENTORY_ITEM_ID   		= MIT.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID     		= MIT.ORGANIZATION_ID
      AND MIT.CATEGORY_ID          		= MC.CATEGORY_ID
      AND MIT.CATEGORY_SET_ID      		= 1100000062
	  AND mc.STRUCTURE_ID  		   		= 101
	  and mc.attribute5            		= b_cat.flex_value
      AND B_CAT.FLEX_VALUE_SET_ID    	= V_CAT.FLEX_VALUE_SET_ID
      AND v_cat.flex_value_set_name  	='XXWC_CATMGT_CATEGORIES'
      AND MSIB.INVENTORY_ITEM_ID   		= MSI.INVENTORY_ITEM_ID 
      and MSIB.ORGANIZATION_ID     		= MSI.ORGANIZATION_ID) CATMGT_CATEGORY, -- added for version 1.8
	(SELECT /*+ RESULT_CACHE */
		T_CAT.DESCRIPTION CATMGT_CATEGORY_DESC
    FROM MTL_SYSTEM_ITEMS_B MSIB,
         MTL_ITEM_CATEGORIES MIT,
         MTL_CATEGORIES_B_KFV MC,
	APPS.FND_FLEX_VALUES B_CAT,
	APPS.FND_FLEX_VALUES_TL T_CAT,
    APPS.FND_FLEX_VALUE_SETS V_CAT
    WHERE MSIB.INVENTORY_ITEM_ID   		= MIT.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID     		= MIT.ORGANIZATION_ID
      AND MIT.CATEGORY_ID          		= MC.CATEGORY_ID
      AND MIT.CATEGORY_SET_ID      		= 1100000062
	  AND mc.STRUCTURE_ID  		   		= 101
	  and mc.attribute5            		= b_cat.flex_value
	  AND b_cat.flex_value_id           = t_cat.flex_value_id
	  AND t_cat.language                = userenv('LANG')
      AND B_CAT.FLEX_VALUE_SET_ID    	= V_CAT.FLEX_VALUE_SET_ID
      AND v_cat.flex_value_set_name  	='XXWC_CATMGT_CATEGORIES'
      AND MSIB.INVENTORY_ITEM_ID   		= MSI.INVENTORY_ITEM_ID 
      and MSIB.ORGANIZATION_ID     		= MSI.ORGANIZATION_ID) CATMGT_CATEGORY_DESC, -- added for version 1.8
	(SELECT pha.attribute2
		FROM po_headers_all pha
	 WHERE pha.po_header_id=mta.transaction_source_id
	)po_attribute2, --added for version 1.9
	(SELECT mtlp.organization_code
		FROM mtl_parameters mtlp
	 WHERE mtlp.organization_id= msi.organization_id
	) org_code, --added for version 1.9
    DECODE ( NVL ((SELECT pha.attribute2
		FROM po_headers_all pha
		WHERE pha.po_header_id=mta.transaction_source_id), 'STANDARD') ,'STANDARD',
    (SELECT SUBSTR (hrl.location_code, 1, 3)
    FROM mtl_material_transactions mmt,
      rcv_transactions rcv,
      po_line_locations_all poll,
      hr_locations hrl
    WHERE mmt.transaction_id     = mta.transaction_id
    AND mmt.rcv_transaction_id   = rcv.transaction_id
    AND rcv.po_line_location_id  = poll.line_location_id
    AND poll.ship_to_location_id = hrl.location_id
    AND rownum                   =1
    ),
    (SELECT location
    FROM hz_cust_site_uses_all hcsu
    WHERE hcsu.site_use_id =
      (SELECT ooh.ship_to_org_id
      FROM oe_drop_ship_sources ods ,
        oe_order_headers_all ooh
      WHERE ods.po_header_id=mta.transaction_source_id
      AND ooh.header_id     = ods.header_id
      AND rownum            =1
      )
    )) ship_to, --added for version 1.9
  decode(xdl.ae_header_id,null,XEL.unrounded_accounted_dr,xdl.unrounded_accounted_dr)transaction_dr, --added for version 1.9	
  decode(xdl.ae_header_id,null,XEL.unrounded_accounted_cr,xdl.unrounded_accounted_cr)transaction_cr --added for version 1.9	
 ,TO_CHAR(NULL) AR_TRX_SOURCE --Ver 2.0
 ,TO_DATE(NULL) AR_TRX_CREATE_DATE --Ver 2.0
FROM
  gl_je_headers jh,
  GL_JE_LINES JL,
  gl_je_sources jes,
  GL_JE_CATEGORIES JEC,
  GL_CODE_COMBINATIONS_KFV GCC,
  gl_je_batches gb,
  gl_ledgers gle,
  gl_period_statuses gps,
  gl_import_references ir,
  XLA_AE_LINES XEL,
  xla_ae_headers xeh,
  xla_distribution_links xdl,
  --rcv_receiving_sub_ledger rsl,
  mtl_transaction_accounts mta,
  mtl_system_items msi
WHERE
  -- AND gle.name = 'HD Supply USD' commented by Neha for TMS#20151027-00222
  JH.JE_HEADER_ID           = JL.JE_HEADER_ID
AND JES.USER_JE_SOURCE_NAME = 'Cost Management'
AND JH.JE_SOURCE            = JES.JE_SOURCE_NAME
AND jec.je_category_name    = 'Inventory'
AND JH.JE_CATEGORY          = JEC.JE_CATEGORY_NAME
AND JL.CODE_COMBINATION_ID  = GCC.CODE_COMBINATION_ID
AND JH.JE_BATCH_ID          = GB.JE_BATCH_ID
AND jh.ledger_id            = gle.ledger_id
AND GPS.APPLICATION_ID      = 101
AND GLE.LEDGER_ID           = GPS.LEDGER_ID
AND jh.period_name          = gps.period_name
AND JL.JE_HEADER_ID         = IR.JE_HEADER_ID
AND JL.JE_LINE_NUM          = IR.JE_LINE_NUM
--  and GLE.CHART_OF_ACCOUNTS_ID = GCC.CHART_OF_ACCOUNTS_ID
AND ir.gl_sl_link_id                 = xel.gl_sl_link_id(+)
AND IR.GL_SL_LINK_TABLE              = XEL.GL_SL_LINK_TABLE(+)
AND xel.application_id(+)            = 707
AND XEL.AE_HEADER_ID                 = XEH.AE_HEADER_ID(+)
AND XEL.application_id               = XEH.application_id(+) --TMS# 20160426-00098 Added 4/18/16
AND xel.ae_header_id                 = xdl.ae_header_id(+)
AND XEL.AE_LINE_NUM                  = XDL.AE_LINE_NUM(+)
AND XEL.APPLICATION_ID               = XDL.APPLICATION_ID(+) --TMS# 20160426-00098 Added 4/18/16
AND XDL.SOURCE_DISTRIBUTION_ID_NUM_1 = MTA.INV_SUB_LEDGER_ID(+)
AND xdl.source_distribution_type(+)  = 'MTL_TRANSACTION_ACCOUNTS'
AND msi.inventory_item_id(+)         = mta.inventory_item_id
AND MSI.ORGANIZATION_ID(+)           = MTA.ORGANIZATION_ID
    -- end of JE source 'Cost Management' and JE Category "Inventory" -Part 2
    --
    --
    -- begin of JE source 'Cost Management' and JE Category other than "Inventory" -Part 3
    --
UNION ALL
  SELECT  /*+ INDEX(gcc GL_CODE_COMBINATIONS_U1) INDEX(gle GL_LEDGERS_U2)*/  --TMS#20160503-00101 
  jes.user_je_source_name SOURCE,
  jh.je_category,
  jl.je_header_id je_header_id,
  jl.je_line_num je_line_num,
  jl.effective_date acc_date,
  jh.NAME entry,
  jh.je_batch_id batch,
  SUBSTR (jl.description, 1, 240) line_descr,
  NULL LSequence,
  jh.doc_sequence_value hnumber,
  jl.je_line_num lline,
  jl.accounted_dr line_acctd_dr,
  jl.accounted_cr line_acctd_cr,
  jl.entered_dr line_ent_dr,
  jl.entered_cr line_ent_cr,
  ir.subledger_doc_sequence_id seq_id,
  ir.subledger_doc_sequence_value seq_num,
  jh.doc_sequence_id h_seq_id,
  ir.gl_sl_link_id gl_sl_link_id,
  --
  CASE
    WHEN xeh.event_type_code = 'PERIOD_END_ACCRUAL'
    THEN
      (
        SELECT /*+ RESULT_CACHE */
          aps.vendor_name
        FROM
          po_headers poh,
          ap_suppliers aps
        WHERE
          1                  = 1
        AND poh.po_header_id = TO_NUMBER (rsl.reference2)
        AND aps.vendor_id    = poh.vendor_id
      )
    ELSE pov.vendor_name
  END customer_or_vendor, --TMS# 20150105-00229 / ESMS# 281615
  --
  CASE
    WHEN xeh.event_type_code = 'PERIOD_END_ACCRUAL'
    THEN
      (
        SELECT   /*+ RESULT_CACHE */
          aps.segment1
        FROM
          po_headers poh,
          ap_suppliers aps
        WHERE
          1                  = 1
        AND poh.po_header_id = TO_NUMBER (rsl.reference2)
        AND aps.vendor_id    = poh.vendor_id
      )
    ELSE pov.segment1
  END customer_or_vendor_number, --TMS# 20150105-00229 / ESMS# 281615
  --
  jl.entered_dr ENTERED_DR,
  jl.entered_cr ENTERED_CR,
  jl.accounted_cr ACCOUNTED_DR,
  jl.accounted_dr ACCOUNTED_CR,
  xel.unrounded_accounted_dr sla_line_accounted_dr,
  xel.unrounded_accounted_cr sla_line_accounted_cr,
  NVL (xel.unrounded_accounted_dr, 0) - NVL (xel.unrounded_accounted_cr, 0)
  sla_line_accounted_net,
  xel.unrounded_entered_dr sla_line_entered_dr,
  xel.unrounded_entered_cr sla_line_entered_cr,
  NVL (xel.unrounded_entered_dr, 0) - NVL (xel.unrounded_entered_cr, 0)
  sla_line_entered_net,
  xdl.unrounded_entered_cr sla_dist_entered_cr,
  xdl.unrounded_entered_dr sla_dist_entered_dr,
  NVL (xdl.unrounded_entered_cr, 0) - NVL (xdl.unrounded_entered_dr, 0)
  sla_dist_entered_net,
  xdl.unrounded_accounted_cr sla_dist_accounted_cr,
  xdl.unrounded_accounted_dr sla_dist_accounted_dr,
  NVL (xdl.unrounded_accounted_dr, 0) - NVL (xdl.unrounded_accounted_cr, 0)
  sla_dist_accounted_net,
  TO_CHAR (NULL) associate_num,
  XXEIS.EIS_XXWC_INV_UTIL_PKG.GET_INVOICE_URL(TO_NUMBER (rsl.reference3),JH.period_name,'INVOICENUM') transaction_num, --added for version 1.7
  -- TO_CHAR (NULL) transaction_num, --commented for version 1.7
  --TO_CHAR (NULL) sales_order, --Commented for version 1.9	
  (CASE
    WHEN xeh.event_type_code = 'PERIOD_END_ACCRUAL'
    THEN
     to_char( nvl((SELECT oh.order_number
      FROM oe_order_headers oh,
        oe_drop_ship_sources odss
      WHERE oh.header_id    =odss.header_id
      AND odss.po_header_id = to_number (rsl.reference2)
      AND rownum            =1
      ),(SELECT trim(SUBSTR(PRL.NOTE_TO_RECEIVER,instr(PRL.NOTE_TO_RECEIVER,'-',1)+1,instr(PRL.NOTE_TO_RECEIVER,',',1)-instr(PRL.NOTE_TO_RECEIVER,'-',1)-1))so
      FROM po_distributions_all pod,
        po_req_distributions_all prd,
        po_requisition_lines_all prl,
        po_requisition_headers_all prh
      WHERE 1                     =1
      AND pod.po_header_id        = to_number (rsl.reference2)
      AND pod.req_distribution_id = prd.distribution_id
      AND prd.requisition_line_id = prl.requisition_line_id
      and prl.requisition_header_id = prh.requisition_header_id
      and prh.interface_source_code='CTO'
      AND ROWNUM                  =1
      )))
    ELSE
      to_char(nvl((SELECT oh.order_number
      FROM oe_order_headers oh,
        oe_drop_ship_sources odss
      WHERE oh.header_id    =odss.header_id
      AND odss.po_header_id = poh.po_header_id
      AND rownum            =1
      ),(SELECT trim(SUBSTR(PRL.NOTE_TO_RECEIVER,instr(PRL.NOTE_TO_RECEIVER,':',1)+1,instr(PRL.NOTE_TO_RECEIVER,',',1)-instr(PRL.NOTE_TO_RECEIVER,':',1)-1))so
      FROM po_distributions_all pod,
        po_req_distributions_all prd,
        po_requisition_lines_all prl,
        po_requisition_headers_all prh
      WHERE 1                     =1
      AND pod.po_header_id        = poh.po_header_id
      AND pod.req_distribution_id = prd.distribution_id
      AND prd.requisition_line_id = prl.requisition_line_id
      and prl.requisition_header_id = prh.requisition_header_id
      and prh.interface_source_code='CTO'
      AND ROWNUM                  =1
      )))
  END) sales_order, --added for version 1.9	
  gcc.CONCATENATED_SEGMENTS gl_account_string,
  jh.currency_code,
  jh.period_name,
  xel.accounting_class_code TYPE,
  gps.effective_period_num,
  gle.name,
  gb.name batch_name,
  --
  CASE
    WHEN xeh.event_type_code = 'PERIOD_END_ACCRUAL'
    THEN rsl.reference4
    ELSE poh.segment1
  END PO_NUMBER, --TMS# 20150105-00229
  --
  TO_CHAR (NULL) BOL_NUMBER,    --TMS# 20150105-00229
  TO_CHAR (NULL) AP_INV_SOURCE, --TMS# 20150105-00229
  --
  xeh.event_type_code SLA_EVENT_TYPE,
  --
  CASE
    WHEN xeh.event_type_code = 'PERIOD_END_ACCRUAL'
    THEN
      (
        SELECT /*+ RESULT_CACHE */
          mtlsi.segment1
        FROM
          po_distributions pod,
          po_lines pol,
          mtl_system_items mtlsi
        WHERE
          1                         = 1
        AND pod.po_distribution_id  = TO_NUMBER (rsl.reference3)
        AND pol.po_line_id          = pod.po_line_id
        AND mtlsi.inventory_item_id = pol.item_id
        AND mtlsi.organization_id   = 222
      )
    ELSE msi.segment1 --comes from main SQL
  END PART_NUMBER,    --TMS# 20150105-00229
  --
  CASE
    WHEN xeh.event_type_code = 'PERIOD_END_ACCRUAL'
    THEN
      (
        SELECT /*+ RESULT_CACHE */
          pol.item_description
        FROM
          po_distributions pod,
          po_lines pol
        WHERE
          1                        = 1
        AND pod.po_distribution_id = TO_NUMBER (rsl.reference3)
        AND pol.po_line_id         = pod.po_line_id
      )
    ELSE msi.description --comes from main SQL
  END PART_DESCRIPTION,  --TMS# 20150105-00229
  --
  msi.unit_weight ITEM_UNIT_WT,               --TMS# 20150105-00229
  --ABS (rsl.source_doc_quantity) ITEM_TXN_QTY, --Commented for version 1.9	
  decode(xeh.event_type_code,'PO_DEL_INV',abs(rsl.source_doc_quantity),decode((decode(xdl.ae_header_id,null,XEL.unrounded_accounted_cr,xdl.unrounded_accounted_cr)),null,-1*(abs(rsl.source_doc_quantity)),abs(rsl.source_doc_quantity))) ITEM_TXN_QTY, --added for version 1.9	
  TO_NUMBER (NULL) ITEM_TOTAL_TXN_COST,       --TMS# 20150105-00229
  TO_NUMBER (NULL) ITEM_OVRHD_CALC_COST,      --TMS# 20150105-00229
  --
  CASE
    WHEN xeh.event_type_code = 'PERIOD_END_ACCRUAL'
    THEN rsl.reference4
      ||
      (
        SELECT /*+ RESULT_CACHE */
          pol.item_description
        FROM
          po_distributions pod,
          po_lines pol
        WHERE
          1                        = 1
        AND pod.po_distribution_id = TO_NUMBER (rsl.reference3)
        AND pol.po_line_id         = pod.po_line_id
      )
    ELSE
      NVL (msi.description, rl.item_description)
  END XXCUS_GSC_ENH_LINE_DESC, --ESMS# 281615
  --
  CASE
    WHEN xeh.event_type_code = 'PERIOD_END_ACCRUAL'
    THEN
      (
        SELECT
          D.DESCRIPTION --User who created the Requisition
        FROM
          FND_USER D,
          PO_HEADERS_ALL A,
          PO_DISTRIBUTIONS_ALL C,
          PO_REQ_DISTRIBUTIONS_ALL B
        WHERE
          1                   = 1
        AND A.SEGMENT1        = rsl.reference4
        AND D.USER_ID         = B.CREATED_BY
        AND B.DISTRIBUTION_ID = C.REQ_DISTRIBUTION_ID
        AND C.PO_HEADER_ID    = A.po_header_id
        AND ROWNUM            = 1
      )
    ELSE
      (
        SELECT
          D.DESCRIPTION --User who created the Requisition
        FROM
          FND_USER D,
          PO_DISTRIBUTIONS_ALL C,
          PO_REQ_DISTRIBUTIONS_ALL B
        WHERE
          1                   = 1
        AND D.USER_ID         = B.CREATED_BY
        AND B.DISTRIBUTION_ID = C.REQ_DISTRIBUTION_ID
        AND C.PO_HEADER_ID    = poh.po_header_id
        AND ROWNUM            = 1
      )
  END XXCUS_GSC_PO_CREATED_BY, --ESMS# 281615
  --
  /*CASE
    WHEN
      (
        xeh.event_type_code = 'PERIOD_END_ACCRUAL'
      AND poh.org_id       IN (163, 167)
      )
    THEN
      (
        SELECT
          ''''
          || a.url
        FROM
          fnd_attached_docs_form_vl a,
          ap_invoice_distributions b,
          po_distributions_all c
        WHERE
          1                        = 1
        AND c.po_distribution_id   = TO_NUMBER (rsl.reference3)
        AND b.po_distribution_id   = c.po_distribution_id
        AND a.entity_name          = 'AP_INVOICES'
        AND a.pk1_value            = TO_CHAR (b.invoice_id)
        AND a.datatype_name        = 'Web Page'
        AND a.category_description = 'Invoice Internal'
        AND a.document_description = 'Documentum Image URL'
        AND a.function_name        = 'APXINWKB'
        AND ROWNUM                 < 2
      )
    WHEN
      (
        xeh.event_type_code != 'PERIOD_END_ACCRUAL'
      AND poh.org_id        IN (163, 167)
      )
    THEN
      (
        SELECT
          ''''
          || a.url
        FROM
          fnd_attached_docs_form_vl a,
          ap_invoices_all b
        WHERE
          1                        = 1
        AND b.quick_po_header_id   = poh.po_header_id
        AND a.entity_name          = 'AP_INVOICES'
        AND a.pk1_value            = TO_CHAR (b.invoice_id)
        AND a.datatype_name        = 'Web Page'
        AND a.category_description = 'Invoice Internal'
        AND a.document_description = 'Documentum Image URL'
        AND a.function_name        = 'APXINWKB'
        AND ROWNUM                 < 2
      )
    ELSE TO_CHAR (NULL)
  END XXCUS_GSC_IMAGE_LINK, */--ESMS# 281615  --commented for version 1.7
  --
  XXEIS.EIS_XXWC_INV_UTIL_PKG.GET_INVOICE_URL(TO_NUMBER (rsl.reference3),JH.period_name,'URL') XXCUS_GSC_IMAGE_LINK, --added for version 1.7
  gcc.code_combination_id,
  GCC.SEGMENT1 GCC50328PRODUCT,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1, 'XXCUS_GL_PRODUCT')
  GCC50328PRODUCTDESCR,
  GCC.SEGMENT2 GCC50328LOCATION,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2, 'XXCUS_GL_LOCATION')
  GCC50328LOCATIONDESCR,
  GCC.SEGMENT3 GCC50328COST_CENTER,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3, 'XXCUS_GL_COSTCENTER')
  GCC50328COST_CENTERDESCR,
  GCC.SEGMENT4 GCC50328ACCOUNT,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4, 'XXCUS_GL_ACCOUNT')
  GCC50328ACCOUNTDESCR,
  GCC.SEGMENT5 GCC50328PROJECT_CODE,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5, 'XXCUS_GL_PROJECT')
  GCC50328PROJECT_CODEDESCR,
  GCC.SEGMENT6 GCC50328FURTURE_USE,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6, 'XXCUS_GL_FUTURE_USE1')
  GCC50328FURTURE_USEDESCR,
  GCC.SEGMENT7 GCC50328FUTURE_USE_2,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7, 'XXCUS_GL_FUTURE_USE_2')
  GCC50328FUTURE_USE_2DESCR,
  GCC.SEGMENT1 GCC50368PRODUCT,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1, 'XXCUS_GL_LTMR_PRODUCT')
  GCC50368PRODUCTDESCR,
  GCC.SEGMENT2 GCC50368DIVISION,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2, 'XXCUS_GL_ LTMR _DIVISION') GCC50368DIVISIONDESCR,
  GCC.SEGMENT3 GCC50368DEPARTMENT,
  xxeis.eis_rs_fin_utility.decode_vset ( GCC.SEGMENT3, 'XXCUS_GL_ LTMR _DEPARTMENT') GCC50368DEPARTMENTDESCR,
  GCC.SEGMENT4 GCC50368ACCOUNT,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4, 'XXCUS_GL_ LTMR _ACCOUNT') GCC50368ACCOUNTDESCR,
  GCC.SEGMENT5 GCC50368SUBACCOUNT,
  xxeis.eis_rs_fin_utility.decode_vset ( GCC.SEGMENT5, 'XXCUS_GL_ LTMR _SUBACCOUNT') GCC50368SUBACCOUNTDESCR,
  GCC.SEGMENT6 GCC50368FUTURE_USE,
  xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6, 'XXCUS_GL_ LTMR _FUTUREUSE') GCC50368FUTURE_USEDESCR,
  xdl.source_distribution_type,     --TMS# 20150105-00229
  xdl.source_distribution_id_num_1, --TMS# 20150105-00229
  xdl.applied_to_source_id_num_1,   --TMS# 20150105-00229
  3 FETCH_SEQ,                      --TMS# 20150105-00229
  (
    SELECT
      TRUNC (invoice_date)
    FROM
      ap_invoices
    WHERE
      1                    = 1
    AND quick_po_header_id = poh.po_header_id
    AND ROWNUM             < 2
  )
  ap_inv_date, --ESMS 281615
  rh.WAYBILL_AIRBILL_NUM WAYBILL --added for version 1.7
  ,CASE
    WHEN xeh.event_type_code = 'PERIOD_END_ACCRUAL'
    THEN
	( SELECT /*+ RESULT_CACHE */
          MC.CONCATENATED_SEGMENTS
        FROM
          PO_DISTRIBUTIONS POD,
          PO_LINES POL,
          mtl_categories_b_kfv mc
        WHERE POD.PO_DISTRIBUTION_ID = TO_NUMBER (RSL.REFERENCE3)
        and POL.PO_LINE_ID         = POD.PO_LINE_ID
        and POL.CATEGORY_ID        = MC.CATEGORY_ID) 
	ELSE	
		(SELECT /*+ RESULT_CACHE */
          MC.CONCATENATED_SEGMENTS
        FROM
          PO_LINES POL,
          mtl_categories_b_kfv mc
        WHERE 
			POL.PO_LINE_ID         = RL.PO_LINE_ID
        and POL.CATEGORY_ID        = MC.CATEGORY_ID
		) 
	END	CATEGORY_NAME, -- added for version 1.8
  CASE
    WHEN xeh.event_type_code = 'PERIOD_END_ACCRUAL'
    THEN	
  (SELECT /*+ RESULT_CACHE */
          MCT.description
        FROM
          PO_DISTRIBUTIONS POD,
          PO_LINES POL,
          MTL_CATEGORIES_TL MCT
        WHERE POD.PO_DISTRIBUTION_ID = TO_NUMBER (RSL.REFERENCE3)
        and POL.PO_LINE_ID         = POD.PO_LINE_ID
        AND POL.CATEGORY_ID        = MCT.CATEGORY_ID)
	ELSE	
		(SELECT /*+ RESULT_CACHE */
          MCT.description
        FROM
          PO_LINES POL,
          MTL_CATEGORIES_TL MCT
        WHERE 
			POL.PO_LINE_ID         = RL.PO_LINE_ID
        and POL.CATEGORY_ID        = MCT.CATEGORY_ID
		) 
	END CATEGORY_DESCRIPTION, -- added for version 1.8
  CASE
    WHEN xeh.event_type_code = 'PERIOD_END_ACCRUAL'
    THEN
	( SELECT /*+ RESULT_CACHE */
          mc.ATTRIBUTE5 CATMGT_CATEGORY
        FROM
          PO_DISTRIBUTIONS POD,
          PO_LINES POL,
          mtl_categories_b_kfv mc,
		  apps.fnd_flex_values b_cat,
		  APPS.FND_FLEX_VALUE_SETS V_CAT
        WHERE POD.PO_DISTRIBUTION_ID  = TO_NUMBER (RSL.REFERENCE3)
        and POL.PO_LINE_ID         	  = POD.PO_LINE_ID
        and POL.CATEGORY_ID        	  = MC.CATEGORY_ID
		AND mc.STRUCTURE_ID  		  = 101
		and mc.attribute5             = b_cat.flex_value
		AND B_CAT.FLEX_VALUE_SET_ID   = V_CAT.FLEX_VALUE_SET_ID
		AND v_cat.flex_value_set_name = 'XXWC_CATMGT_CATEGORIES')
	ELSE	
		(SELECT /*+ RESULT_CACHE */
          mc.ATTRIBUTE5 CATMGT_CATEGORY
        FROM
          PO_LINES POL,
          mtl_categories_b_kfv mc,
		  apps.fnd_flex_values b_cat,
		  APPS.FND_FLEX_VALUE_SETS V_CAT
        WHERE 
			POL.PO_LINE_ID            = RL.PO_LINE_ID
        and POL.CATEGORY_ID           = MC.CATEGORY_ID
		AND mc.STRUCTURE_ID  		  = 101
		and mc.attribute5             = b_cat.flex_value
		AND B_CAT.FLEX_VALUE_SET_ID   = V_CAT.FLEX_VALUE_SET_ID
		AND v_cat.flex_value_set_name = 'XXWC_CATMGT_CATEGORIES'
		) 
	END	CATMGT_CATEGORY, -- added for version 1.8
 CASE
    WHEN xeh.event_type_code = 'PERIOD_END_ACCRUAL'
    THEN
	( SELECT /*+ RESULT_CACHE */
          T_CAT.DESCRIPTION CATMGT_CATEGORY_DESC
        FROM
          PO_DISTRIBUTIONS POD,
          PO_LINES POL,
          mtl_categories_b_kfv mc,
		  apps.fnd_flex_values b_cat,
		  APPS.FND_FLEX_VALUES_TL T_CAT,
		  APPS.FND_FLEX_VALUE_SETS V_CAT
        WHERE POD.PO_DISTRIBUTION_ID  = TO_NUMBER (RSL.REFERENCE3)
        and POL.PO_LINE_ID         	  = POD.PO_LINE_ID
        and POL.CATEGORY_ID        	  = MC.CATEGORY_ID
		AND mc.STRUCTURE_ID  		  = 101
		and mc.attribute5             = b_cat.flex_value
		AND b_cat.flex_value_id       = t_cat.flex_value_id
		AND t_cat.language            = userenv('LANG')
		AND B_CAT.FLEX_VALUE_SET_ID   = V_CAT.FLEX_VALUE_SET_ID
		AND v_cat.flex_value_set_name = 'XXWC_CATMGT_CATEGORIES')
	ELSE	
		(SELECT /*+ RESULT_CACHE */
          T_CAT.DESCRIPTION CATMGT_CATEGORY_DESC
        FROM
          PO_LINES POL,
          mtl_categories_b_kfv mc,
		  apps.fnd_flex_values b_cat,
		  APPS.FND_FLEX_VALUES_TL T_CAT,
		  APPS.FND_FLEX_VALUE_SETS V_CAT
        WHERE 
			POL.PO_LINE_ID            = RL.PO_LINE_ID
        and POL.CATEGORY_ID           = MC.CATEGORY_ID
		AND mc.STRUCTURE_ID  		  = 101
		and mc.attribute5             = b_cat.flex_value
		AND b_cat.flex_value_id       = t_cat.flex_value_id
		AND t_cat.language            = userenv('LANG')
		AND B_CAT.FLEX_VALUE_SET_ID   = V_CAT.FLEX_VALUE_SET_ID
		AND v_cat.flex_value_set_name = 'XXWC_CATMGT_CATEGORIES'
		) 
	END CATMGT_CATEGORY_DESC, -- added for version 1.8 
  POH.attribute2 po_attribute2, --added for version 1.9
	(SELECT mtlp.organization_code
		FROM mtl_parameters mtlp
	 WHERE mtlp.organization_id= msi.organization_id
	 ) org_code, --added for version 1.9
    DECODE ( NVL (POH.attribute2, 'STANDARD') ,'STANDARD',
    (SELECT SUBSTR (hrl.location_code, 1, 3)
    FROM 
      po_line_locations_all poll,
      hr_locations hrl
    WHERE 
        poll.line_location_id = rt.po_line_location_id  
    AND poll.ship_to_location_id = hrl.location_id
    AND rownum                   =1
    ),
    (SELECT location
    FROM hz_cust_site_uses_all hcsu
    WHERE hcsu.site_use_id =
      (SELECT ooh.ship_to_org_id
      FROM oe_drop_ship_sources ods ,
        oe_order_headers_all ooh
      WHERE ods.po_header_id=POH.po_header_id
      AND ooh.header_id     = ods.header_id
      AND rownum            =1
      )
    )) ship_to, --added for version 1.9
  decode(xdl.ae_header_id,null,xel.unrounded_accounted_dr,xdl.unrounded_accounted_dr)transaction_dr, --added for version 1.9	
  decode(xdl.ae_header_id,null,xel.unrounded_accounted_cr,xdl.unrounded_accounted_cr)transaction_cr --added for version 1.9
  ,TO_CHAR(NULL) AR_TRX_SOURCE --Ver 2.0
  ,TO_DATE(NULL) AR_TRX_CREATE_DATE --Ver 2.0
FROM
  APPS.GL_JE_HEADERS JH,
  apps.gl_je_lines jl,
  apps.gl_ledgers gle,
  apps.gl_je_batches gb,
  APPS.GL_JE_SOURCES JES,
  apps.gl_je_categories jec,
  gl_period_statuses gps,
  apps.gl_code_combinations_kfv gcc,
  apps.gl_import_references ir,
  apps.xla_ae_lines xel,
  apps.xla_ae_headers xeh,
  apps.xla_distribution_links xdl,
  apps.rcv_receiving_sub_ledger rsl,
  apps.rcv_transactions rt,
  APPS.RCV_SHIPMENT_LINES RL,
  APPS.RCV_SHIPMENT_HEADERS RH, --added 9/26
  APPS.PO_HEADERS_ALL POH,       --added 9/26
  apps.po_vendors pov,          --added 9/26
  APPS.PO_VENDOR_SITES POVS,    --added 9/26
  apps.mtl_system_items msi    -- added 9/7
WHERE
  JH.JE_HEADER_ID = JL.JE_HEADER_ID
  --AND gle.name = 'HD Supply USD' commented by Neha for TMS#20151027-00222
AND JH.LEDGER_ID       = GLE.LEDGER_ID
AND jh.je_batch_id     = gb.je_batch_id
AND jh.je_source       = 'Cost Management'
AND JH.JE_SOURCE       = JES.JE_SOURCE_NAME
AND jh.je_category    <> 'Inventory'
AND jh.je_category     = jec.je_category_name
AND gps.application_id = 101
AND gps.period_name    = jh.period_name
AND gps.ledger_id      = gle.ledger_id
-- AND gle.chart_of_Accounts_id = gcc.chart_of_accounts_id   --TMS# 20160426-00098 Removed 4/18/16
AND jl.code_combination_id           = gcc.code_combination_id
AND jl.je_header_id                  = ir.je_header_id
AND jl.je_line_num                   = ir.je_line_num
AND ir.gl_sl_link_id                 = xel.gl_sl_link_id(+)
AND IR.GL_SL_LINK_TABLE              = XEL.GL_SL_LINK_TABLE(+)
AND xel.application_id(+)            = 707 --added 4/18/16
AND XEL.AE_HEADER_ID                 = XEH.AE_HEADER_ID(+)
AND XEL.application_id               = XEH.application_id(+) --TMS# 20160426-00098 Added 4/18/16
AND xel.ae_header_id                 = xdl.ae_header_id(+)
AND XEL.AE_LINE_NUM                  = XDL.AE_LINE_NUM(+)
AND XEL.application_id               = XDL.application_id(+) --TMS# 20160426-00098 Added 4/18/16
AND xdl.source_distribution_id_num_1 = rsl.rcv_sub_ledger_id(+)
AND xdl.source_distribution_type(+)  = 'RCV_RECEIVING_SUB_LEDGER'
AND rsl.rcv_transaction_id           = rt.transaction_id(+)
AND RT.SHIPMENT_LINE_ID              = RL.SHIPMENT_LINE_ID(+) --added 2/13/2013
AND rl.shipment_header_id            = rh.shipment_header_id(+)
AND rl.po_header_id                  = poh.po_header_id(+)      --added 2/13/2013
AND rh.vendor_id                     = pov.vendor_id(+)         --added 2/13/2013
AND rh.vendor_site_id                = povs.vendor_site_id(+)   --added 2/13/2013
AND RL.ITEM_ID                       = MSI.INVENTORY_ITEM_ID(+) --added 2/13/2013
AND rl.to_organization_id            = msi.organization_id(+) --added 2/13/2013
    --
    -- end of JE source 'Cost Management' and JE Category other than "Inventory" -Part 3
    --
UNION ALL
    --
    -- begin of JE source "Receivables" -Part 4
    --
    SELECT /*+ INDEX(gcc GL_CODE_COMBINATIONS_U1) INDEX(gle GL_LEDGERS_U2)*/  --TMS# 20160426-00098
            jes.user_je_source_name SOURCE,
           jh.je_category,
           jir.je_header_id je_header_id,
           jir.je_line_num je_line_num,
           jl.effective_date acc_date,
           jh.NAME entry,
           jh.je_batch_id batch,
           SUBSTR (jl.description, 1, 240) line_descr,
           NULL LSequence,
           jh.doc_sequence_value hnumber,
           jl.je_line_num lline,
           jl.accounted_dr line_acctd_dr,
           jl.accounted_cr line_acctd_cr,
           jl.entered_dr line_ent_dr,
           jl.entered_cr line_ent_cr,
           jir.subledger_doc_sequence_id seq_id,
           jir.subledger_doc_sequence_value seq_num,
           jh.doc_sequence_id h_seq_id,
           jir.gl_sl_link_id gl_sl_link_id,
           --
           (CASE
               WHEN (source_distribution_type = 'AR_DISTRIBUTIONS_ALL')
               THEN
                  (SELECT /*+ RESULT_CACHE */
                         hzp.party_name
                     FROM hz_cust_accounts hzca, hz_parties hzp
                    WHERE     hzca.cust_account_id = dist.third_party_id
                          AND hzp.party_id = hzca.party_id)
               WHEN (source_distribution_type =
                        'RA_CUST_TRX_LINE_GL_DIST_ALL')
               THEN
                  (SELECT /*+ RESULT_CACHE */
                         hzp.party_name
                     FROM hz_cust_accounts hzca,
                          hz_parties hzp,
                          ra_customer_trx trx
                    WHERE     1 = 1
                          AND trx.customer_trx_id = ra_dist.customer_trx_id
                          AND hzca.cust_account_id = trx.bill_to_customer_id
                          AND hzp.party_id = hzca.party_id)
               ELSE
                  TO_CHAR (NULL)
            END)
              CUSTOMER_OR_VENDOR,
           --
           (CASE
               WHEN (source_distribution_type = 'AR_DISTRIBUTIONS_ALL')
               THEN
                  (SELECT /*+ RESULT_CACHE */
                         hzca.account_number
                     FROM hz_cust_accounts hzca
                    WHERE hzca.cust_account_id = dist.third_party_id)
               WHEN (source_distribution_type =
                        'RA_CUST_TRX_LINE_GL_DIST_ALL')
               THEN
                  (SELECT /*+ RESULT_CACHE */
                         hzca.account_number
                     FROM hz_cust_accounts hzca, ra_customer_trx trx
                    WHERE     1 = 1
                          AND trx.customer_trx_id = ra_dist.customer_trx_id
                          AND hzca.cust_account_id = trx.bill_to_customer_id)
               ELSE
                  TO_CHAR (NULL)
            END)
              CUSTOMER_OR_VENDOR_NUMBER, --TMS# 20150105-00229
           --
           DECODE (
              ra_dist.cust_trx_line_gl_dist_id,
              NULL, dist.AMOUNT_DR,
              TO_NUMBER (
                 DECODE (
                    ra_dist.account_class,
                    'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                   -1, NULL,
                                   NVL (ra_dist.amount, 0)),
                    DECODE (SIGN (NVL (ra_dist.amount, 0)),
                            -1, -NVL (ra_dist.amount, 0),
                            NULL))))
              Entered_DR,
           DECODE (
              ra_dist.cust_trx_line_gl_dist_id,
              NULL, dist.AMOUNT_cR,
              TO_NUMBER (
                 DECODE (
                    ra_dist.account_class,
                    'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                   -1, -NVL (ra_dist.amount, 0),
                                   NULL),
                    DECODE (SIGN (NVL (ra_dist.amount, 0)),
                            -1, NULL,
                            NVL (ra_dist.amount, 0)))))
              Entered_CR,
           DECODE (
              ra_dist.cust_trx_line_gl_dist_id,
              NULL, dist.ACCTD_AMOUNT_DR,
              TO_NUMBER (
                 DECODE (
                    ra_dist.account_class,
                    'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                   -1, NULL,
                                   NVL (ra_dist.acctd_amount, 0)),
                    DECODE (SIGN (NVL (ra_dist.amount, 0)),
                            -1, -NVL (ra_dist.acctd_amount, 0),
                            NULL))))
              Accounted_DR,
           DECODE (
              ra_dist.cust_trx_line_gl_dist_id,
              NULL, dist.ACCTD_AMOUNT_CR,
              TO_NUMBER (
                 DECODE (
                    ra_dist.account_class,
                    'REC', DECODE (SIGN (NVL (ra_dist.amount, 0)),
                                   -1, -NVL (ra_dist.acctd_amount, 0),
                                   NULL),
                    DECODE (SIGN (NVL (ra_dist.amount, 0)),
                            -1, NULL,
                            NVL (ra_DIST.acctd_amount, 0)))))
              Accounted_CR,
           ael.unrounded_accounted_dr sla_line_accounted_dr,
           ael.unrounded_accounted_cr sla_line_accounted_cr,
             NVL (ael.unrounded_accounted_dr, 0)
           - NVL (ael.unrounded_accounted_cr, 0)
              sla_line_accounted_net,
           ael.unrounded_entered_dr sla_line_entered_dr,
           ael.unrounded_entered_cr sla_line_entered_cr,
             NVL (ael.unrounded_entered_dr, 0)
           - NVL (ael.unrounded_entered_cr, 0)
              sla_line_entered_net,
           xdl.unrounded_entered_cr sla_dist_entered_cr,
           xdl.unrounded_entered_dr sla_dist_entered_dr,
             NVL (xdl.unrounded_entered_cr, 0)
           - NVL (xdl.unrounded_entered_dr, 0)
              sla_dist_entered_net,
           xdl.unrounded_accounted_cr sla_dist_accounted_cr,
           xdl.unrounded_accounted_dr sla_dist_accounted_dr,
             --             NVL (xdl.unrounded_accounted_cr, 0)
             --           - NVL (xdl.unrounded_accounted_dr, 0)
             --              sla_dist_accounted_net,
             NVL (xdl.unrounded_accounted_dr, 0)
           - NVL (xdl.unrounded_accounted_cr, 0)
              sla_dist_accounted_net,
           TO_CHAR (NULL) associate_num,
           (CASE
               WHEN (    xdl.source_distribution_type =
                            'AR_DISTRIBUTIONS_ALL'
                     AND dist.source_table = 'CRH'
                     AND dist.source_type = 'CASH')
               THEN
                  (SELECT /*+ RESULT_CACHE */
                         cr.receipt_number
                     FROM ar_cash_receipt_history_all crh,
                          ar_cash_receipts_all cr
                    WHERE     1 = 1
                          AND crh.cash_receipt_history_id = dist.source_id
                          AND cr.cash_receipt_id = crh.cash_receipt_id)
               WHEN (    xdl.source_distribution_type =
                            'AR_DISTRIBUTIONS_ALL'
                     AND dist.source_table = 'RA'
                     AND dist.source_type IN ('EDISC', 'REC'))
               THEN
                  (SELECT /*+ RESULT_CACHE */
                         trx_number
                     FROM ra_customer_trx trx, ra_customer_trx_lines trxlines
                    WHERE     1 = 1
                          AND trxlines.customer_trx_line_id =
                                 dist.ref_customer_trx_line_id
                          AND trx.customer_trx_id = trxlines.customer_trx_id)
               WHEN (xdl.source_distribution_type =
                        'RA_CUST_TRX_LINE_GL_DIST_ALL')
               THEN
                  (SELECT /*+ RESULT_CACHE */
                         trx_number
                     FROM ra_customer_trx
                    WHERE 1 = 1 AND customer_trx_id = ra_dist.customer_trx_id)
               ELSE
                  TO_CHAR (NULL)
            END)
              TRANSACTION_NUM, --TMS# 20150105-00229
           --
           (CASE
               WHEN (    xdl.source_distribution_type =
                            'AR_DISTRIBUTIONS_ALL'
                     AND dist.source_table = 'CRH'
                     AND dist.source_type = 'CASH')
               THEN
                  (TO_CHAR (NULL))
               WHEN (    xdl.source_distribution_type =
                            'AR_DISTRIBUTIONS_ALL'
                     AND dist.source_table = 'RA'
                     AND dist.source_type IN ('EDISC', 'REC'))
               THEN
                  (SELECT /*+ RESULT_CACHE */
                         ct_reference
                     FROM ra_customer_trx trx, ra_customer_trx_lines trxlines
                    WHERE     1 = 1
                          AND trxlines.customer_trx_line_id =
                                 dist.ref_customer_trx_line_id
                          AND trx.customer_trx_id = trxlines.customer_trx_id)
               WHEN (xdl.source_distribution_type =
                        'RA_CUST_TRX_LINE_GL_DIST_ALL')
               THEN
                  (SELECT /*+ RESULT_CACHE */
                         ct_reference
                     FROM ra_customer_trx
                    WHERE 1 = 1 AND customer_trx_id = ra_dist.customer_trx_id)
               ELSE
                  TO_CHAR (NULL)
            END)
              SALES_ORDER, --TMS# 20150105-00229
           -- 
           gcc.CONCATENATED_SEGMENTS gl_account_string,
           jh.currency_code,
           jh.period_name,
           NVL (dist.source_type, NULL) TYPE,
           gps.effective_period_num,
           gle.name,
           gb.name batch_name,
           TO_CHAR (NULL) PO_NUMBER, --TMS# 20150105-00229
           TO_CHAR (NULL) BOL_NUMBER, --TMS# 20150105-00229
           TO_CHAR (NULL) AP_INV_SOURCE, --TMS# 20150105-00229
           xlae.event_type_code SLA_EVENT_TYPE,
           --
           CASE
              WHEN (    xdl.source_distribution_type = 'AR_DISTRIBUTIONS_ALL'
                    AND dist.source_table = 'CRH'
                    AND dist.source_type = 'CASH')
              THEN
                 (TO_CHAR (NULL))
              WHEN (    xdl.source_distribution_type = 'AR_DISTRIBUTIONS_ALL'
                    AND dist.source_table = 'RA'
                    AND dist.source_type IN ('EDISC', 'REC'))
              THEN
                 (SELECT /*+ RESULT_CACHE */
                        mtlsi.segment1
                    FROM ra_customer_trx_lines trxlines,
                         mtl_system_items mtlsi
                   WHERE     1 = 1
                         AND trxlines.customer_trx_line_id =
                                dist.ref_customer_trx_line_id
                         AND mtlsi.inventory_item_id =
                                trxlines.inventory_item_id
                         AND mtlsi.organization_id = 222)
              WHEN (xdl.source_distribution_type =
                       'RA_CUST_TRX_LINE_GL_DIST_ALL')
              THEN
                 (SELECT /*+ RESULT_CACHE */
                        mtlsi.segment1
                    FROM ra_customer_trx_lines trxlines,
                         mtl_system_items mtlsi
                   WHERE     1 = 1
                         AND trxlines.customer_trx_line_id =
                                ra_dist.customer_trx_line_id
                         AND mtlsi.inventory_item_id =
                                trxlines.inventory_item_id
                         AND mtlsi.organization_id = 222)
              ELSE
                 TO_CHAR (NULL)
           END
              PART_NUMBER,  --TMS# 20150105-00229
           --
           CASE
              WHEN (    xdl.source_distribution_type = 'AR_DISTRIBUTIONS_ALL'
                    AND dist.source_table = 'CRH'
                    AND dist.source_type = 'CASH')
              THEN
                 (TO_CHAR (NULL))
              WHEN (    xdl.source_distribution_type = 'AR_DISTRIBUTIONS_ALL'
                    AND dist.source_table = 'RA'
                    AND dist.source_type IN ('EDISC', 'REC'))
              THEN
                 (SELECT /*+ RESULT_CACHE */
                        mtlsi.description
                    FROM ra_customer_trx_lines trxlines,
                         mtl_system_items mtlsi
                   WHERE     1 = 1
                         AND trxlines.customer_trx_line_id =
                                dist.ref_customer_trx_line_id
                         AND mtlsi.inventory_item_id =
                                trxlines.inventory_item_id
                         AND mtlsi.organization_id = 222)
              WHEN (source_distribution_type = 'RA_CUST_TRX_LINE_GL_DIST_ALL')
              THEN
                 (SELECT /*+ RESULT_CACHE */
                        mtlsi.description
                    FROM ra_customer_trx_lines trxlines,
                         mtl_system_items mtlsi
                   WHERE     1 = 1
                         AND trxlines.customer_trx_line_id =
                                ra_dist.customer_trx_line_id
                         AND mtlsi.inventory_item_id =
                                trxlines.inventory_item_id
                         AND mtlsi.organization_id = 222)
              ELSE
                 TO_CHAR (NULL)
           END
              PART_DESCRIPTION, --TMS# 20150105-00229
           --
           TO_NUMBER (NULL) ITEM_UNIT_WT, --TMS# 20150105-00229
           TO_NUMBER (NULL) ITEM_TXN_QTY, --TMS# 20150105-00229
           TO_NUMBER (NULL) ITEM_TOTAL_TXN_COST, --TMS# 20150105-00229
           TO_NUMBER (NULL) ITEM_OVRHD_CALC_COST, --TMS# 20150105-00229
           --
           SUBSTR (jl.description, 1, 240) XXCUS_GSC_ENH_LINE_DESC, --ESMS# 281615
           TO_CHAR (NULL) XXCUS_GSC_PO_CREATED_BY, --ESMS# 281615
           TO_CHAR (NULL) XXCUS_GSC_IMAGE_LINK, --ESMS# 281615
           --
           gcc.code_combination_id,                     
           GCC.SEGMENT1 GCC50328PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_PRODUCT')
              GCC50328PRODUCTDESCR,
           GCC.SEGMENT2 GCC50328LOCATION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_LOCATION')
              GCC50328LOCATIONDESCR,
           GCC.SEGMENT3 GCC50328COST_CENTER,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                 'XXCUS_GL_COSTCENTER')
              GCC50328COST_CENTERDESCR,
           GCC.SEGMENT4 GCC50328ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ACCOUNT')
              GCC50328ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50328PROJECT_CODE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                 'XXCUS_GL_PROJECT')
              GCC50328PROJECT_CODEDESCR,
           GCC.SEGMENT6 GCC50328FURTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_FUTURE_USE1')
              GCC50328FURTURE_USEDESCR,
           GCC.SEGMENT7 GCC50328FUTURE_USE_2,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7,
                                                 'XXCUS_GL_FUTURE_USE_2')
              GCC50328FUTURE_USE_2DESCR,
           GCC.SEGMENT1 GCC50368PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_LTMR_PRODUCT')
              GCC50368PRODUCTDESCR,
           GCC.SEGMENT2 GCC50368DIVISION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_ LTMR _DIVISION')
              GCC50368DIVISIONDESCR,
           GCC.SEGMENT3 GCC50368DEPARTMENT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT3,
              'XXCUS_GL_ LTMR _DEPARTMENT')
              GCC50368DEPARTMENTDESCR,
           GCC.SEGMENT4 GCC50368ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ LTMR _ACCOUNT')
              GCC50368ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50368SUBACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT5,
              'XXCUS_GL_ LTMR _SUBACCOUNT')
              GCC50368SUBACCOUNTDESCR,
           GCC.SEGMENT6 GCC50368FUTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_ LTMR _FUTUREUSE')
              GCC50368FUTURE_USEDESCR,
           xdl.source_distribution_type, --TMS# 20150105-00229
           xdl.source_distribution_id_num_1, --TMS# 20150105-00229
           xdl.applied_to_source_id_num_1, --TMS# 20150105-00229
           4 FETCH_SEQ, --TMS# 20150105-00229
           TO_DATE (NULL) ap_inv_date, --ESMS 281615  
		   NULL WAYBILL  --added for version 1.7
		   ,(CASE
    WHEN ( xdl.source_distribution_type = 'AR_DISTRIBUTIONS_ALL'
    AND dist.source_table               = 'CRH'
    AND dist.source_type                = 'CASH')
    THEN (TO_CHAR (NULL))
    WHEN ( xdl.source_distribution_type = 'AR_DISTRIBUTIONS_ALL'
    AND dist.source_table               = 'RA'
    AND dist.source_type               IN ('EDISC', 'REC'))
    THEN
      (SELECT /*+ RESULT_CACHE */
        MC.CONCATENATED_SEGMENTS
      FROM RA_CUSTOMER_TRX_LINES TRXLINES,
        MTL_SYSTEM_ITEMS_B MSIB,
        MTL_ITEM_CATEGORIES MIT,
        MTL_CATEGORIES_B_KFV MC
      WHERE trxlines.customer_trx_line_id = dist.ref_customer_trx_line_id
      AND TRXLINES.INVENTORY_ITEM_ID      = MSIB.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID            = 222
      AND MSIB.INVENTORY_ITEM_ID          = MIT.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID            = MIT.ORGANIZATION_ID
      AND MIT.CATEGORY_ID                 = MC.CATEGORY_ID
      AND MIT.CATEGORY_SET_ID             = 1100000062
      AND rownum                          =1
      )
    WHEN (xdl.source_distribution_type = 'RA_CUST_TRX_LINE_GL_DIST_ALL')
    THEN
      (SELECT /*+ RESULT_CACHE */
        MC.CONCATENATED_SEGMENTS
      FROM RA_CUSTOMER_TRX_LINES TRXLINES,
        MTL_SYSTEM_ITEMS_B MSIB,
        MTL_ITEM_CATEGORIES MIT,
        MTL_CATEGORIES_B_KFV MC
      WHERE trxlines.customer_trx_line_id = ra_dist.customer_trx_line_id
      AND TRXLINES.INVENTORY_ITEM_ID      = MSIB.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID            = 222
      AND MSIB.INVENTORY_ITEM_ID          = MIT.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID            = MIT.ORGANIZATION_ID
      AND MIT.CATEGORY_ID                 = MC.CATEGORY_ID
      AND MIT.CATEGORY_SET_ID             = 1100000062
      AND rownum                          =1
      )
    ELSE TO_CHAR (NULL)
  END) CATEGORY_NAME, -- added for version 1.8
		  (CASE
    WHEN ( xdl.source_distribution_type = 'AR_DISTRIBUTIONS_ALL'
    AND dist.source_table               = 'CRH'
    AND dist.source_type                = 'CASH')
    THEN (TO_CHAR (NULL))
    WHEN ( xdl.source_distribution_type = 'AR_DISTRIBUTIONS_ALL'
    AND dist.source_table               = 'RA'
    AND dist.source_type               IN ('EDISC', 'REC'))
    THEN
      (SELECT /*+ RESULT_CACHE */
        MC.description
      FROM RA_CUSTOMER_TRX_LINES TRXLINES,
        MTL_SYSTEM_ITEMS_B MSIB,
        MTL_ITEM_CATEGORIES MIT,
        MTL_CATEGORIES_TL MC
      WHERE trxlines.customer_trx_line_id = dist.ref_customer_trx_line_id
      AND TRXLINES.INVENTORY_ITEM_ID      = MSIB.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID            = 222
      AND MSIB.INVENTORY_ITEM_ID          = MIT.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID            = MIT.ORGANIZATION_ID
      AND MIT.CATEGORY_ID                 = MC.CATEGORY_ID
      AND MIT.CATEGORY_SET_ID             = 1100000062
      AND rownum                          =1
      )
    WHEN (xdl.source_distribution_type = 'RA_CUST_TRX_LINE_GL_DIST_ALL')
    THEN
      (SELECT /*+ RESULT_CACHE */
        MC.description
      FROM RA_CUSTOMER_TRX_LINES TRXLINES,
        MTL_SYSTEM_ITEMS_B MSIB,
        MTL_ITEM_CATEGORIES MIT,
        MTL_CATEGORIES_TL MC
      WHERE trxlines.customer_trx_line_id = ra_dist.customer_trx_line_id
      AND TRXLINES.INVENTORY_ITEM_ID      = MSIB.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID            = 222
      AND MSIB.INVENTORY_ITEM_ID          = MIT.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID            = MIT.ORGANIZATION_ID
      AND MIT.CATEGORY_ID                 = MC.CATEGORY_ID
      AND MIT.CATEGORY_SET_ID             = 1100000062
      AND rownum                          =1
      )
    ELSE TO_CHAR (NULL)
  END) CATEGORY_DESCRIPTION, -- added for version 1.8
  (CASE
    WHEN ( xdl.source_distribution_type = 'AR_DISTRIBUTIONS_ALL'
    AND dist.source_table               = 'CRH'
    AND dist.source_type                = 'CASH')
    THEN (TO_CHAR (NULL))
    WHEN ( xdl.source_distribution_type = 'AR_DISTRIBUTIONS_ALL'
    AND dist.source_table               = 'RA'
    AND dist.source_type               IN ('EDISC', 'REC'))
    THEN
      (SELECT /*+ RESULT_CACHE */
         mc.ATTRIBUTE5 CATMGT_CATEGORY
      FROM RA_CUSTOMER_TRX_LINES TRXLINES,
        MTL_SYSTEM_ITEMS_B MSIB,
        MTL_ITEM_CATEGORIES MIT,
        MTL_CATEGORIES_B_KFV MC,
		apps.fnd_flex_values b_cat,
		APPS.FND_FLEX_VALUE_SETS V_CAT
      WHERE trxlines.customer_trx_line_id = dist.ref_customer_trx_line_id
      AND TRXLINES.INVENTORY_ITEM_ID      = MSIB.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID            = 222
      AND MSIB.INVENTORY_ITEM_ID          = MIT.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID            = MIT.ORGANIZATION_ID
      AND MIT.CATEGORY_ID                 = MC.CATEGORY_ID
      AND MIT.CATEGORY_SET_ID             = 1100000062
	    AND mc.STRUCTURE_ID  				        = 101
	    and mc.attribute5                   = b_cat.flex_value
      AND B_CAT.FLEX_VALUE_SET_ID         = V_CAT.FLEX_VALUE_SET_ID
      AND v_cat.flex_value_set_name       = 'XXWC_CATMGT_CATEGORIES'
      AND rownum                          = 1
      )
    WHEN (xdl.source_distribution_type = 'RA_CUST_TRX_LINE_GL_DIST_ALL')
    THEN
      (SELECT /*+ RESULT_CACHE */
         mc.ATTRIBUTE5 CATMGT_CATEGORY
      FROM RA_CUSTOMER_TRX_LINES TRXLINES,
        MTL_SYSTEM_ITEMS_B MSIB,
        MTL_ITEM_CATEGORIES MIT,
        MTL_CATEGORIES_B_KFV MC,
		    apps.fnd_flex_values b_cat,
        APPS.FND_FLEX_VALUE_SETS V_CAT
      WHERE trxlines.customer_trx_line_id = ra_dist.customer_trx_line_id
      AND TRXLINES.INVENTORY_ITEM_ID      = MSIB.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID            = 222
      AND MSIB.INVENTORY_ITEM_ID          = MIT.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID            = MIT.ORGANIZATION_ID
      AND MIT.CATEGORY_ID                 = MC.CATEGORY_ID
      AND MIT.CATEGORY_SET_ID             = 1100000062
	    AND mc.STRUCTURE_ID  				        = 101
	    and mc.attribute5                   = b_cat.flex_value
      AND B_CAT.FLEX_VALUE_SET_ID         = V_CAT.FLEX_VALUE_SET_ID
      AND v_cat.flex_value_set_name       IN ('XXWC_CATMGT_CATEGORIES')
      AND rownum                          =1
      )
    ELSE TO_CHAR (NULL)
  END) CATMGT_CATEGORY, -- added for version 1.8
 (CASE
    WHEN ( xdl.source_distribution_type = 'AR_DISTRIBUTIONS_ALL'
    AND dist.source_table               = 'CRH'
    AND dist.source_type                = 'CASH')
    THEN (TO_CHAR (NULL))
    WHEN ( xdl.source_distribution_type = 'AR_DISTRIBUTIONS_ALL'
    AND dist.source_table               = 'RA'
    AND dist.source_type               IN ('EDISC', 'REC'))
    THEN
      (SELECT /*+ RESULT_CACHE */
         T_CAT.DESCRIPTION CATMGT_CATEGORY_DESC
      FROM RA_CUSTOMER_TRX_LINES TRXLINES,
        MTL_SYSTEM_ITEMS_B MSIB,
        MTL_ITEM_CATEGORIES MIT,
        MTL_CATEGORIES_B_KFV MC,
		apps.fnd_flex_values b_cat,
		APPS.FND_FLEX_VALUES_TL T_CAT,
		APPS.FND_FLEX_VALUE_SETS V_CAT
      WHERE trxlines.customer_trx_line_id = dist.ref_customer_trx_line_id
      AND TRXLINES.INVENTORY_ITEM_ID      = MSIB.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID            = 222
      AND MSIB.INVENTORY_ITEM_ID          = MIT.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID            = MIT.ORGANIZATION_ID
      AND MIT.CATEGORY_ID                 = MC.CATEGORY_ID
      AND MIT.CATEGORY_SET_ID             = 1100000062
	  AND mc.STRUCTURE_ID  				  = 101
	  and mc.attribute5                   = b_cat.flex_value
	  AND b_cat.flex_value_id             = t_cat.flex_value_id
	  AND t_cat.language                  = userenv('LANG')
      AND B_CAT.FLEX_VALUE_SET_ID         = V_CAT.FLEX_VALUE_SET_ID
      AND v_cat.flex_value_set_name       = 'XXWC_CATMGT_CATEGORIES'
      AND rownum                          = 1
      )
    WHEN (xdl.source_distribution_type = 'RA_CUST_TRX_LINE_GL_DIST_ALL')
    THEN
      (SELECT /*+ RESULT_CACHE */
         T_CAT.DESCRIPTION CATMGT_CATEGORY_DESC
      FROM RA_CUSTOMER_TRX_LINES TRXLINES,
        MTL_SYSTEM_ITEMS_B MSIB,
        MTL_ITEM_CATEGORIES MIT,
        MTL_CATEGORIES_B_KFV MC,
		apps.fnd_flex_values b_cat,
		APPS.FND_FLEX_VALUES_TL T_CAT,
        APPS.FND_FLEX_VALUE_SETS V_CAT
      WHERE trxlines.customer_trx_line_id = ra_dist.customer_trx_line_id
      AND TRXLINES.INVENTORY_ITEM_ID      = MSIB.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID            = 222
      AND MSIB.INVENTORY_ITEM_ID          = MIT.INVENTORY_ITEM_ID
      AND MSIB.ORGANIZATION_ID            = MIT.ORGANIZATION_ID
      AND MIT.CATEGORY_ID                 = MC.CATEGORY_ID
      AND MIT.CATEGORY_SET_ID             = 1100000062
	  AND mc.STRUCTURE_ID  				  = 101
	  and mc.attribute5                   = b_cat.flex_value
	  AND b_cat.flex_value_id             = t_cat.flex_value_id
	  AND t_cat.language                  = userenv('LANG')
      AND B_CAT.FLEX_VALUE_SET_ID         = V_CAT.FLEX_VALUE_SET_ID
      AND v_cat.flex_value_set_name       IN ('XXWC_CATMGT_CATEGORIES')
      AND rownum                          =1
      )
    ELSE TO_CHAR (NULL)
  END) CATMGT_CATEGORY_DESC, -- added for version 1.8 
  to_char(NULL) po_attribute2, --added for version 1.9
  to_char(NULL) org_code, --added for version 1.9
  to_char(NULL) ship_to,  --added for version 1.9
    decode(xdl.ae_header_id,null,ael.unrounded_accounted_dr,xdl.unrounded_accounted_dr)transaction_dr, --added for version 1.9	
    decode(xdl.ae_header_id,null,ael.unrounded_accounted_cr,xdl.unrounded_accounted_cr)transaction_cr --added for version 1.9
  ,( --Ver 2.0
	SELECT /*+ RESULT_CACHE */ A.NAME  --Ver 2.0
	FROM RA_BATCH_SOURCES_ALL A, RA_CUSTOMER_TRX_ALL B --Ver 2.0
	WHERE     1 = 1 --Ver 2.0
	  AND B.CUSTOMER_TRX_ID = RA_DIST.CUSTOMER_TRX_ID --Ver 2.0
	  AND A.BATCH_SOURCE_ID = B.BATCH_SOURCE_ID --Ver 2.0
   ) AR_TRX_SOURCE --Ver 2.0
  ,TRUNC(RA_DIST.CREATION_DATE) AR_TRX_CREATE_DATE --Ver 2.0
 FROM
  gl_je_headers jh,
  GL_JE_LINES JL,
  GL_LEDGERS GLE,
  GL_JE_BATCHES GB,
  GL_JE_SOURCES JES,
  GL_PERIOD_STATUSES GPS,
  gl_code_combinations_kfv gcc,
  GL_IMPORT_REFERENCES JIR,
  xla_ae_lines ael,
  xla_ae_headers aeh,
  xla_distribution_links xdl,
  xla_events xlae,
  ar_distributions_all dist,
  RA_CUST_TRX_LINE_GL_DIST_ALL RA_DIST
where
    jh.je_header_id        = jl.je_header_id
and JH.LEDGER_ID           = GLE.LEDGER_ID
and JH.JE_BATCH_ID         = GB.JE_BATCH_ID
AND jes.user_je_source_name = 'Receivables'
AND jh.je_source           = jes.je_source_name 
AND gps.application_id = 101
and GPS.PERIOD_NAME    = JH.PERIOD_NAME
AND gps.ledger_id      = gle.ledger_id
and JL.CODE_COMBINATION_ID = GCC.CODE_COMBINATION_ID
AND jl.je_header_id        = jir.je_header_id
AND jl.je_line_num         = jir.je_line_num
--AND gle.name = 'HD Supply USD' commented by Neha for TMS#20151027-00222
AND jir.gl_sl_link_id      = ael.gl_sl_link_id(+)
AND jir.gl_sl_link_table   = ael.gl_sl_link_table(+)
and ael.application_id(+)  = 222				--TMS# 20160426-00098		
and AEL.AE_HEADER_ID       = AEH.AE_HEADER_ID(+)
AND AEL.APPLICATION_ID     = AEH.APPLICATION_ID(+) --TMS# 20160426-00098 added on 4/18/16
AND AEL.AE_HEADER_ID       = XDL.AE_HEADER_ID(+)
AND ael.ae_line_num        = xdl.ae_line_num(+)
AND AEL.APPLICATION_ID     = XDL.APPLICATION_ID(+) --TMS# 20160426-00098 added on 4/18/16
AND aeh.application_id     = xlae.application_id(+)     --sbala 10/16/12
AND aeh.event_id           = xlae.event_id(+)             -- sbala 10/16/12
AND dist.line_id(+)        = DECODE ( xdl.source_distribution_type,
  'AR_DISTRIBUTIONS_ALL', xdl.source_distribution_id_num_1, NULL)
AND ra_dist.cust_trx_line_gl_dist_id(+) = DECODE (xdl.source_distribution_type,
  'AR_DISTRIBUTIONS_ALL', NULL, xdl.source_distribution_id_num_1)
    --
    -- end of JE source "Receivables" -Part 4
    --
UNION ALL
    --
    -- begin of JE source "Payables" and xla_distribution_links source_distribution_type "AP_INV_DIST" -Part 5
    --
SELECT /*+ INDEX(gcc GL_CODE_COMBINATIONS_U1)*/    --TMS# 20160426-00098
           jes.user_je_source_name SOURCE,
           jh.je_category,
           gir.je_header_id je_header_id,
           gir.je_line_num je_line_num,
           jl.effective_date acc_date,
           jh.NAME entry,
           jh.je_batch_id batch,
           SUBSTR (jl.description, 1, 240) line_descr,
           ds.name LSequence,
           jh.doc_sequence_value hnumber,
           jl.je_line_num lline,
           jl.accounted_dr line_acctd_dr,
           jl.accounted_cr line_acctd_cr,
           jl.entered_dr ent_dr,
           jl.entered_cr ent_cr,
           gir.subledger_doc_sequence_id seq_id,
           gir.subledger_doc_sequence_value seq_num,
           jh.doc_sequence_id h_seq_id,
           gir.gl_sl_link_id gl_sl_link_id,
           --
           (CASE
               WHEN SIGN (ai.vendor_id) = -1
               THEN
                  'No Supplier'
               ELSE
                  (SELECT vendor_name
                     FROM po_vendors
                    WHERE 1 = 1 AND vendor_id = ai.vendor_id)
            END)
              customer_or_vendor, --TMS# 20150105-00229
           --
           (CASE
               WHEN adl.applied_to_source_id_num_1 IS NOT NULL
               THEN
                  (SELECT /*+ RESULT_CACHE */
                         aps.segment1
                     FROM ap_invoices_all apia, ap_suppliers aps
                    WHERE     1 = 1
                          AND apia.invoice_id =
                                 adl.applied_to_source_id_num_1
                          AND aps.vendor_id = apia.vendor_id)
               ELSE
                  NULL
            END)
              customer_or_vendor_number, --TMS# 20150105-00229
           --
           jl.entered_dr ENTERED_DR,
           jl.entered_cr ENTERED_CR,
           jl.accounted_cr ACCOUNTED_DR,
           jl.accounted_dr ACCOUNTED_CR,
           ael.unrounded_accounted_dr sla_line_accounted_dr,
           ael.unrounded_accounted_cr sla_line_accounted_cr,
             NVL (ael.unrounded_accounted_dr, 0)
           - NVL (ael.unrounded_accounted_cr, 0)
              sla_line_accounted_net,
           ael.unrounded_entered_dr sla_line_entered_dr,
           ael.unrounded_entered_cr sla_line_entered_cr,
             NVL (ael.unrounded_entered_dr, 0)
           - NVL (ael.unrounded_entered_cr, 0)
              sla_line_entered_net,
           adl.unrounded_entered_cr sla_dist_entered_cr,
           adl.unrounded_entered_dr sla_dist_entered_dr,
             NVL (adl.unrounded_entered_cr, 0)
           - NVL (adl.unrounded_entered_dr, 0)
              sla_dist_entered_net,
           adl.unrounded_accounted_cr sla_dist_accounted_cr,
           adl.unrounded_accounted_dr sla_dist_accounted_dr,
             --             NVL (adl.unrounded_accounted_cr, 0)
             --           - NVL (adl.unrounded_accounted_dr, 0)
             --              sla_dist_accounted_net,
             NVL (adl.unrounded_accounted_dr, 0)
           - NVL (adl.unrounded_accounted_cr, 0)
              sla_dist_accounted_net,
           '' associate_num,
           AI.INVOICE_NUM transaction_num,  --TMS# 20150105-00229
         --  TO_CHAR (NULL) SALES_ORDER, --TMS# 20150105-00229             --commented TMS#20150602-00007 
         --  ai.attribute3    SALES_ORDER, --Commented for version 1.9	                                  --Added for TMS#20150602-00007
           nvl(ai.attribute3,to_char(NVL((SELECT oh.order_number
          FROM oe_order_headers oh,
               oe_drop_ship_sources odss
          WHERE oh.header_id    =odss.header_id
            AND odss.po_header_id = poh.po_header_id
            AND rownum            =1
            ),(SELECT trim(SUBSTR(PRL.NOTE_TO_RECEIVER,instr(PRL.NOTE_TO_RECEIVER,':',1)+1,instr(PRL.NOTE_TO_RECEIVER,',',1)-instr(PRL.NOTE_TO_RECEIVER,':',1)-1))so
                FROM po_distributions_all pod,
                     po_req_distributions_all prd,
                     po_requisition_lines_all prl,
                     po_requisition_headers_all prh
                WHERE pod.po_header_id        = poh.po_header_id
                  AND pod.req_distribution_id   = prd.distribution_id
                  AND prd.requisition_line_id   = prl.requisition_line_id
                  AND prl.requisition_header_id = prh.requisition_header_id
                  AND prh.interface_source_code ='CTO'
                  AND ROWNUM                    =1
                  )))) SALES_ORDER,  --added for version 1.9	
           gcc.CONCATENATED_SEGMENTS gl_account_string,
           jh.currency_code,
           jh.period_name,
           ael.accounting_class_code TYPE,
           gps.effective_period_num,
           le.name,
           gb.name batch_name,
          -- poh.segment1 PO_NUMBER, --TMS# 20150105-00229                 --commented TMS#20150602-00007
          CASE                                   --Added for TMS#20150602-00007 
             WHEN aid.po_distribution_id IS NULL THEN ai.attribute4
          ELSE poh.segment1 END PO_NUMBER, 
           AI.ATTRIBUTE6 BOL_NUMBER, --TMS# 20150105-00229
           AI.SOURCE AP_INV_SOURCE, --TMS# 20150105-00229
           xlae.event_type_code SLA_EVENT_TYPE,
           --
           (CASE
               WHEN adl.applied_to_dist_id_num_1 IS NOT NULL
               THEN
                  (SELECT /*+ RESULT_CACHE */
                         mtlsi.segment1
                     FROM ap_invoice_distributions_all apid,
                          ap_invoice_lines_all apil,
                          mtl_system_items mtlsi
                    WHERE     1 = 1
                          AND apid.invoice_distribution_id =
                                 adl.applied_to_dist_id_num_1
                          AND apil.invoice_id = apid.invoice_id
                          AND apil.line_number = apid.invoice_line_number
                          AND mtlsi.inventory_item_id =
                                 apil.inventory_item_id
                          AND mtlsi.organization_id = 222)
               ELSE
                  NULL
            END)
              PART_NUMBER, --TMS# 20150105-00229
           --
           (CASE
               WHEN adl.applied_to_dist_id_num_1 IS NOT NULL
               THEN
                  (SELECT /*+ RESULT_CACHE */
                         mtlsi.description
                     FROM ap_invoice_distributions_all apid,
                          ap_invoice_lines_all apil,
                          mtl_system_items mtlsi
                    WHERE     1 = 1
                          AND apid.invoice_distribution_id =
                                 adl.applied_to_dist_id_num_1
                          AND apil.invoice_id = apid.invoice_id
                          AND apil.line_number = apid.invoice_line_number
                          AND mtlsi.inventory_item_id =
                                 apil.inventory_item_id
                          AND mtlsi.organization_id = 222)
               ELSE
                  NULL
            END)
              PART_DESCRIPTION, --TMS# 20150105-00229
           --
           (SELECT msi.unit_weight
              FROM po_lines pol, mtl_system_items msi
             WHERE     1 = 1
                   AND pol.po_line_id = pod.po_line_id
                   AND msi.inventory_item_id = pol.item_id
                   AND msi.organization_id = pod.destination_organization_id)
              ITEM_UNIT_WT, --TMS# 20150105-00229
          -- aid.quantity_invoiced ITEM_TXN_QTY, --Commented for version 1.9	
          decode(xlae.event_type_code,'PO_DEL_INV',abs(aid.quantity_invoiced),decode((decode(adl.ae_header_id,null,ael.unrounded_accounted_cr,adl.unrounded_accounted_cr)),null,-1*(abs(aid.quantity_invoiced)),abs(aid.quantity_invoiced))) ITEM_TXN_QTY, --added for version 1.9	
           TO_NUMBER (NULL) ITEM_TOTAL_TXN_COST, --TMS# 20150105-00229
           TO_NUMBER (NULL) ITEM_OVRHD_CALC_COST, --TMS# 20150105-00229
           --
           --           AI.INVOICE_NUM||':'||
           (SELECT vendor_name
              FROM po_vendors
             WHERE     1 = 1
                   AND SIGN (ai.vendor_id) <> -1
                   AND vendor_id = ai.vendor_id)
              XXCUS_GSC_ENH_LINE_DESC, --ESMS# 281615
           (SELECT D.DESCRIPTION            --User who created the Requisition
              FROM FND_USER D, PO_REQ_DISTRIBUTIONS_ALL B
             WHERE     1 = 1
                   AND B.DISTRIBUTION_ID = pod.REQ_DISTRIBUTION_ID
                   AND D.USER_ID = B.CREATED_BY
                   AND ROWNUM = 1)
              XXCUS_GSC_PO_CREATED_BY, --ESMS# 281615
           (SELECT '''' || url image_link
              FROM fnd_attached_docs_form_vl
             WHERE     1 = 1
                   and ai.org_id IN (163, 167)
                   AND entity_name = 'AP_INVOICES'
                   AND pk1_value = TO_CHAR (ai.invoice_id)
                   AND datatype_name = 'Web Page'
                   AND category_description = 'Invoice Internal'
                   AND document_description = 'Documentum Image URL'
                   AND function_name = 'APXINWKB'
                   AND ROWNUM < 2)
              XXCUS_GSC_IMAGE_LINK, --ESMS# 281615
           --
           gcc.code_combination_id,                     
           GCC.SEGMENT1 GCC50328PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_PRODUCT')
              GCC50328PRODUCTDESCR,
           GCC.SEGMENT2 GCC50328LOCATION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_LOCATION')
              GCC50328LOCATIONDESCR,
           GCC.SEGMENT3 GCC50328COST_CENTER,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                 'XXCUS_GL_COSTCENTER')
              GCC50328COST_CENTERDESCR,
           GCC.SEGMENT4 GCC50328ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ACCOUNT')
              GCC50328ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50328PROJECT_CODE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                 'XXCUS_GL_PROJECT')
              GCC50328PROJECT_CODEDESCR,
           GCC.SEGMENT6 GCC50328FURTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_FUTURE_USE1')
              GCC50328FURTURE_USEDESCR,
           GCC.SEGMENT7 GCC50328FUTURE_USE_2,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7,
                                                 'XXCUS_GL_FUTURE_USE_2')
              GCC50328FUTURE_USE_2DESCR,
           GCC.SEGMENT1 GCC50368PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_LTMR_PRODUCT')
              GCC50368PRODUCTDESCR,
           GCC.SEGMENT2 GCC50368DIVISION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_ LTMR _DIVISION')
              GCC50368DIVISIONDESCR,
           GCC.SEGMENT3 GCC50368DEPARTMENT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT3,
              'XXCUS_GL_ LTMR _DEPARTMENT')
              GCC50368DEPARTMENTDESCR,
           GCC.SEGMENT4 GCC50368ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ LTMR _ACCOUNT')
              GCC50368ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50368SUBACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT5,
              'XXCUS_GL_ LTMR _SUBACCOUNT')
              GCC50368SUBACCOUNTDESCR,
           GCC.SEGMENT6 GCC50368FUTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_ LTMR _FUTUREUSE')
              GCC50368FUTURE_USEDESCR,
           adl.source_distribution_type, --TMS# 20150105-00229
           adl.source_distribution_id_num_1, --TMS# 20150105-00229
           adl.applied_to_source_id_num_1, --TMS# 20150105-00229
           5 FETCH_SEQ, --TMS# 20150105-00229
           TRUNC (ai.invoice_date) ap_inv_date, --ESMS 281615
    	   NULL WAYBILL  --added for version 1.7
          ,(CASE
    		WHEN adl.applied_to_dist_id_num_1 IS NOT NULL
    		THEN
      			(SELECT /*+ RESULT_CACHE */
            		MC.CONCATENATED_SEGMENTS
      			FROM ap_invoice_distributions_all apid,
        			 AP_INVOICE_LINES_ALL APIL,
        			 PO_LINES_ALL pol,
        			 mtl_categories_b_kfv mc
      			WHERE apid.invoice_distribution_id = adl.applied_to_dist_id_num_1
      			AND apil.invoice_id              = apid.invoice_id
      			AND APIL.LINE_NUMBER             = APID.INVOICE_LINE_NUMBER
      			AND APIL.PO_LINE_ID              = POL.PO_LINE_ID
      			and POL.CATEGORY_ID              = MC.CATEGORY_ID      
      			)
    	 	ELSE NULL
  		 END)  CATEGORY_NAME, -- added for version 1.8
  		(CASE
    		WHEN adl.applied_to_dist_id_num_1 IS NOT NULL
    	THEN
      	(SELECT /*+ RESULT_CACHE */
            MCT.description
      	FROM ap_invoice_distributions_all apid,
        	 AP_INVOICE_LINES_ALL APIL,
        	 PO_LINES_ALL pol,
        	 MTL_CATEGORIES_TL MCT
      	WHERE apid.invoice_distribution_id = adl.applied_to_dist_id_num_1
      	  AND apil.invoice_id              = apid.invoice_id
      	  AND APIL.LINE_NUMBER             = APID.INVOICE_LINE_NUMBER
      	  AND APIL.PO_LINE_ID              = POL.PO_LINE_ID
      	  and POL.CATEGORY_ID              = MCT.CATEGORY_ID 
      	 )
    	ELSE NULL
  	  END)  CATEGORY_DESCRIPTION, -- added for version 1.8
      (CASE
    		WHEN adl.applied_to_dist_id_num_1 IS NOT NULL
    		THEN
      			(SELECT /*+ RESULT_CACHE */
            		mc.ATTRIBUTE5 CATMGT_CATEGORY
      			FROM ap_invoice_distributions_all apid,
        			 AP_INVOICE_LINES_ALL APIL,
        			 PO_LINES_ALL pol,
        			 mtl_categories_b_kfv mc,
					 apps.fnd_flex_values b_cat,
					 APPS.FND_FLEX_VALUE_SETS V_CAT
      			WHERE apid.invoice_distribution_id = adl.applied_to_dist_id_num_1
      			AND apil.invoice_id              = apid.invoice_id
      			AND APIL.LINE_NUMBER             = APID.INVOICE_LINE_NUMBER
      			AND APIL.PO_LINE_ID              = POL.PO_LINE_ID
      			and POL.CATEGORY_ID              = MC.CATEGORY_ID 
				AND mc.STRUCTURE_ID  			 = 101
				and mc.attribute5                = b_cat.flex_value
				AND B_CAT.FLEX_VALUE_SET_ID      = V_CAT.FLEX_VALUE_SET_ID
				AND v_cat.flex_value_set_name    ='XXWC_CATMGT_CATEGORIES'
      			)
    	 	ELSE NULL
  		 END)  CATMGT_CATEGORY, -- added for version 1.8
  		(CASE
    		WHEN adl.applied_to_dist_id_num_1 IS NOT NULL
    	THEN
      	   (SELECT /*+ RESULT_CACHE */
			   T_CAT.DESCRIPTION CATMGT_CATEGORY_DESC
				FROM ap_invoice_distributions_all apid,
        			 AP_INVOICE_LINES_ALL APIL,
        			 PO_LINES_ALL pol,
        			 mtl_categories_b_kfv mc,
					 apps.fnd_flex_values b_cat,
					 APPS.FND_FLEX_VALUES_TL T_CAT,
					 APPS.FND_FLEX_VALUE_SETS V_CAT
      			WHERE apid.invoice_distribution_id = adl.applied_to_dist_id_num_1
      			AND apil.invoice_id              = apid.invoice_id
      			AND APIL.LINE_NUMBER             = APID.INVOICE_LINE_NUMBER
      			AND APIL.PO_LINE_ID              = POL.PO_LINE_ID
      			and POL.CATEGORY_ID              = MC.CATEGORY_ID 
				AND mc.STRUCTURE_ID  			 = 101
				and mc.attribute5                = b_cat.flex_value
				AND b_cat.flex_value_id          = t_cat.flex_value_id
				AND t_cat.language               = userenv('LANG')
				AND B_CAT.FLEX_VALUE_SET_ID      = V_CAT.FLEX_VALUE_SET_ID
				AND v_cat.flex_value_set_name    ='XXWC_CATMGT_CATEGORIES'
      	  )
    	ELSE NULL
  	  END)  CATMGT_CATEGORY_DESC, -- added for version 1.8   
     POH.attribute2 po_attribute2, --added for version 1.9
	  (SELECT mtlp.organization_code
		 FROM 
     mtl_parameters mtlp
	  WHERE mtlp.organization_id= pod.destination_organization_id
	   ) org_code, --added for version 1.9
    DECODE ( NVL (POH.attribute2, 'STANDARD') ,'STANDARD',
    (SELECT SUBSTR (hrl.location_code, 1, 3)
    FROM 
      po_line_locations_all poll,
      hr_locations hrl
    WHERE 
        poll.line_location_id = pod.line_location_id  
    AND poll.ship_to_location_id = hrl.location_id
    AND rownum                   =1
    ),
    (SELECT location
    FROM hz_cust_site_uses_all hcsu
    WHERE hcsu.site_use_id =
      (SELECT ooh.ship_to_org_id
      FROM oe_drop_ship_sources ods ,
        oe_order_headers_all ooh
      WHERE ods.po_header_id=POH.po_header_id
      AND ooh.header_id     = ods.header_id
      AND rownum            =1
      )
    )) ship_to, --added for version 1.9
      decode(adl.ae_header_id,null,ael.unrounded_accounted_dr,adl.unrounded_accounted_dr)transaction_dr, --added for version 1.9	
      decode(adl.ae_header_id,null,ael.unrounded_accounted_cr,adl.unrounded_accounted_cr)transaction_cr --added for version 1.9
	   ,TO_CHAR(NULL) AR_TRX_SOURCE --Ver 2.0
       ,TO_DATE(NULL) AR_TRX_CREATE_DATE --Ver 2.0
FROM
  gl_je_headers jh,
  gl_je_lines jl,
  gl_je_batches gb,
  gl_je_sources jes,
  GL_LEDGERS LE,
  GL_PERIOD_STATUSES GPS,
  GL_CODE_COMBINATIONS_KFV GCC,
  gl_import_references gir,
  xla_ae_lines ael,
  gl_code_combinations_kfv gcc1,
  xla_ae_headers aeh,
  xla_events xlae,
  xla_distribution_links adl,
  AP_INVOICE_DISTRIBUTIONS_ALL AID,
  AP_INVOICES_ALL AI,
  --po_vendors pov,
  FND_DOCUMENT_SEQUENCES ds,
  po_distributions_all pod, --added 9/27
  po_headers_all poh        --added 9/27
WHERE
  jh.je_header_id                    = jl.je_header_id
AND JH.JE_BATCH_ID                   = GB.JE_BATCH_ID
AND jh.je_source                     = jes.je_source_name
AND JES.USER_JE_SOURCE_NAME          = 'Payables'
AND JH.LEDGER_ID                     = LE.LEDGER_ID
AND le.object_type_code              = 'L'
AND GPS.APPLICATION_ID               = 101
AND GPS.LEDGER_ID                    = LE.LEDGER_ID
AND gps.period_name                  = jh.period_name
AND jl.code_combination_id           = gcc.code_combination_id
AND jh.je_header_id                  = gir.je_header_id
AND jl.je_line_num                   = gir.je_line_num
AND gir.gl_sl_link_id                = ael.gl_sl_link_id(+)
AND gir.gl_sl_link_table             = ael.gl_sl_link_table(+)
AND AEL.APPLICATION_ID(+)            = 200					--TMS# 20160426-00098
AND ael.code_combination_id          = gcc1.code_combination_id(+)
AND AEL.AE_HEADER_ID                 = AEH.AE_HEADER_ID(+)
AND AEL.application_id               = aeh.application_id(+)    --TMS# 20160426-00098
AND aeh.application_id               = xlae.application_id(+) --sbala 10/16/12
AND aeh.event_id                     = xlae.event_id(+)       -- sbala 10/16/12
AND AEL.AE_HEADER_ID                 = ADL.AE_HEADER_ID(+)
AND ael.ae_line_num                  = adl.ae_line_num(+)
AND AEL.application_id               = adl.application_id(+)   --TMS# 20160426-00098
AND adl.SOURCE_DISTRIBUTION_TYPE     = 'AP_INV_DIST'
and ADL.SOURCE_DISTRIBUTION_ID_NUM_1 = AID.INVOICE_DISTRIBUTION_ID
AND AID.INVOICE_ID                   = AI.INVOICE_ID
  -- AND aip.invoice_id(+)=AID.INVOICE_DISTRIBUTION_ID
  -- AND AIP.CHECK_ID=AC.CHECK_ID(+)
  --AND POV.VENDOR_ID = AI.VENDOR_ID
  --AND le.name = 'HD Supply USD' commented by Neha for TMS#20151027-00222
AND gir.subledger_doc_sequence_id = ds.doc_sequence_id(+)
AND aid.po_distribution_id        = pod.po_distribution_id(+) --added 9/27
AND pod.po_header_id              = poh.po_header_id(+)       --added 9/27
    --
    -- end of JE source "Payables" and xla_distribution_links source_distribution_type "AP_INV_DIST" -Part 5
    --
UNION ALL
    --
    -- begin of JE source "Payables" and xla_distribution_links source_distribution_type "AP_PMT_DIST" -Part 6
    --
select /*+ INDEX(gcc GL_CODE_COMBINATIONS_U1)*/   --TMS# 20160426-00098
            jes.user_je_source_name SOURCE,
           jh.je_category,
           gir.je_header_id je_header_id,
           gir.je_line_num je_line_num,
           jl.effective_date acc_date,
           jh.NAME entry,
           jh.je_batch_id batch,
           SUBSTR (jl.description, 1, 240) line_descr,
           ds.name LSequence,
           jh.doc_sequence_value hnumber,
           jl.je_line_num lline,
           jl.accounted_dr line_acctd_dr,
           jl.accounted_cr line_acctd_cr,
           jl.entered_dr ent_dr,
           jl.entered_cr ent_cr,
           gir.subledger_doc_sequence_id seq_id,
           gir.subledger_doc_sequence_value seq_num,
           jh.doc_sequence_id h_seq_id,
           gir.gl_sl_link_id gl_sl_link_id,
           (CASE
               WHEN SIGN (ai.vendor_id) = -1
               THEN
                  'No Supplier'
               ELSE
                  (SELECT vendor_name
                     FROM po_vendors
                    WHERE 1 = 1 AND vendor_id = ai.vendor_id)
            END)
              customer_or_vendor,
           TO_CHAR (NULL) customer_or_vendor_number,
           jl.entered_dr ENTERED_DR,
           jl.entered_cr ENTERED_CR,
           jl.accounted_cr ACCOUNTED_DR,
           jl.accounted_dr ACCOUNTED_CR,
           ael.unrounded_accounted_dr sla_line_accounted_dr,
           ael.unrounded_accounted_cr sla_line_accounted_cr,
             NVL (ael.unrounded_accounted_dr, 0)
           - NVL (ael.unrounded_accounted_cr, 0)
              sla_line_accounted_net,
           ael.unrounded_entered_dr sla_line_entered_dr,
           ael.unrounded_entered_cr sla_line_entered_cr,
             NVL (ael.unrounded_entered_dr, 0)
           - NVL (ael.unrounded_entered_cr, 0)
              sla_line_entered_net,
           adl.unrounded_entered_cr sla_dist_entered_cr,
           adl.unrounded_entered_dr sla_dist_entered_dr,
             NVL (adl.unrounded_entered_cr, 0)
           - NVL (adl.unrounded_entered_dr, 0)
              sla_dist_entered_net,
           adl.unrounded_accounted_cr sla_dist_accounted_cr,
           ADL.UNROUNDED_ACCOUNTED_DR SLA_DIST_ACCOUNTED_DR,
             --             NVL (adl.unrounded_accounted_cr, 0)
             --           - NVL (adl.unrounded_accounted_dr, 0)
             --              sla_dist_accounted_net,
             NVL (adl.unrounded_accounted_dr, 0)
           - NVL (adl.unrounded_accounted_cr, 0)
              sla_dist_accounted_net,
           TO_CHAR (
              DECODE (adl.SOURCE_DISTRIBUTION_TYPE,
                      'AP_PMT_DIST', AC.CHECK_NUMBER,
                      ''))
              associate_num,
           AI.INVOICE_NUM transaction_num, --TMS# 20150105-00229
           --TO_CHAR (NULL) SALES_ORDER, --TMS# 20150105-00229   --commented for TMS#20150602-00007 
           --ai.attribute3 SALES_ORDER ,       --Commented for version 1.9	
          nvl(ai.attribute3,to_char(NVL((SELECT oh.order_number
              FROM oe_order_headers oh,
                oe_drop_ship_sources odss
              WHERE oh.header_id    =odss.header_id
                AND odss.po_header_id = poh.po_header_id
                AND ROWNUM            =1
             ),(SELECT trim(SUBSTR(PRL.NOTE_TO_RECEIVER,instr(PRL.NOTE_TO_RECEIVER,':',1)+1,instr(PRL.NOTE_TO_RECEIVER,',',1)-instr(PRL.NOTE_TO_RECEIVER,':',1)-1))so
                FROM po_distributions_all pod,
                     po_req_distributions_all prd,
                     po_requisition_lines_all prl,
                     po_requisition_headers_all prh
                WHERE pod.po_header_id        = poh.po_header_id
                  AND pod.req_distribution_id   = prd.distribution_id
                  AND prd.requisition_line_id   = prl.requisition_line_id
                  AND prl.requisition_header_id = prh.requisition_header_id
                  AND prh.interface_source_code ='CTO'
                  AND ROWNUM                    =1
                  )))) SALES_ORDER, --added for version 1.9	
           gcc.CONCATENATED_SEGMENTS gl_account_string,
           jh.currency_code,
           jh.period_name,
           ael.accounting_class_code TYPE,
           gps.effective_period_num,
           le.name,
           gb.name batch_name,
          -- poh.segment1 PO_NUMBER, --TMS# 20150105-00229                 --commented for TMS#20150602-00007 
          CASE                                   --Added for TMS#20150602-00007 
             WHEN aid.po_distribution_id IS NULL THEN ai.attribute4
          ELSE poh.segment1 END PO_NUMBER,
           AI.ATTRIBUTE6 BOL_NUMBER, --TMS# 20150105-00229
           AI.SOURCE AP_INV_SOURCE, --TMS# 20150105-00229
           xlae.event_type_code SLA_EVENT_TYPE,
           --
           TO_CHAR (NULL) PART_NUMBER, --TMS# 20150105-00229
           TO_CHAR (NULL) PART_DESCRIPTION,--TMS# 20150105-00229
           --
           TO_NUMBER (NULL) ITEM_UNIT_WT,--TMS# 20150105-00229
           TO_NUMBER (NULL) ITEM_TXN_QTY, --TMS# 20150105-00229
           TO_NUMBER (NULL) ITEM_TOTAL_TXN_COST, --TMS# 20150105-00229
           TO_NUMBER (NULL) ITEM_OVRHD_CALC_COST, --TMS# 20150105-00229
           --
           --           AI.INVOICE_NUM||':'||
           (SELECT vendor_name
              FROM po_vendors
             WHERE     1 = 1
                   AND SIGN (ai.vendor_id) <> -1
                   AND vendor_id = ai.vendor_id)
              XXCUS_GSC_ENH_LINE_DESC, --ESMS# 281615
           (SELECT D.DESCRIPTION            --User who created the Requisition
              FROM FND_USER D, PO_REQ_DISTRIBUTIONS_ALL B
             WHERE     1 = 1
                   AND B.DISTRIBUTION_ID = pod.REQ_DISTRIBUTION_ID
                   AND D.USER_ID = B.CREATED_BY
                   AND ROWNUM = 1)
              XXCUS_GSC_PO_CREATED_BY, --ESMS# 281615
           (SELECT '''' || url image_link
              FROM fnd_attached_docs_form_vl
             WHERE     1 = 1
                   and ai.org_id IN (163, 167)
                   AND entity_name = 'AP_INVOICES'
                   AND pk1_value = TO_CHAR (ai.invoice_id)
                   AND datatype_name = 'Web Page'
                   AND category_description = 'Invoice Internal'
                   AND document_description = 'Documentum Image URL'
                   AND function_name = 'APXINWKB'
                   AND ROWNUM <2 --Ver 1.2
                   )
              XXCUS_GSC_IMAGE_LINK, --ESMS# 281615
           --
           gcc.code_combination_id,                     
           GCC.SEGMENT1 GCC50328PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_PRODUCT')
              GCC50328PRODUCTDESCR,
           GCC.SEGMENT2 GCC50328LOCATION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_LOCATION')
              GCC50328LOCATIONDESCR,
           GCC.SEGMENT3 GCC50328COST_CENTER,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                 'XXCUS_GL_COSTCENTER')
              GCC50328COST_CENTERDESCR,
           GCC.SEGMENT4 GCC50328ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ACCOUNT')
              GCC50328ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50328PROJECT_CODE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                 'XXCUS_GL_PROJECT')
              GCC50328PROJECT_CODEDESCR,
           GCC.SEGMENT6 GCC50328FURTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_FUTURE_USE1')
              GCC50328FURTURE_USEDESCR,
           GCC.SEGMENT7 GCC50328FUTURE_USE_2,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7,
                                                 'XXCUS_GL_FUTURE_USE_2')
              GCC50328FUTURE_USE_2DESCR,
           GCC.SEGMENT1 GCC50368PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_LTMR_PRODUCT')
              GCC50368PRODUCTDESCR,
           GCC.SEGMENT2 GCC50368DIVISION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_ LTMR _DIVISION')
              GCC50368DIVISIONDESCR,
           GCC.SEGMENT3 GCC50368DEPARTMENT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT3,
              'XXCUS_GL_ LTMR _DEPARTMENT')
              GCC50368DEPARTMENTDESCR,
           GCC.SEGMENT4 GCC50368ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ LTMR _ACCOUNT')
              GCC50368ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50368SUBACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT5,
              'XXCUS_GL_ LTMR _SUBACCOUNT')
              GCC50368SUBACCOUNTDESCR,
           GCC.SEGMENT6 GCC50368FUTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_ LTMR _FUTUREUSE')
              GCC50368FUTURE_USEDESCR,
           adl.source_distribution_type, --TMS# 20150105-00229
           adl.source_distribution_id_num_1, --TMS# 20150105-00229
           adl.applied_to_source_id_num_1, --TMS# 20150105-00229
           6 FETCH_SEQ, --TMS# 20150105-00229
           TRUNC (ai.invoice_date) ap_inv_date, --ESMS 281615
  		   NULL WAYBILL  --added for version 1.7
		   ,TO_CHAR (NULL) CATEGORY_NAME, -- added for version 1.8
		   TO_CHAR (NULL) CATEGORY_DESCRIPTION, -- added for version 1.8
      	   NULL CATMGT_CATEGORY, -- added for version 1.8
           NULL CATMGT_CATEGORY_DESC, -- added for version 1.8       
     POH.attribute2 po_attribute2, --added for version 1.9
	  (SELECT mtlp.organization_code
		 FROM 
     mtl_parameters mtlp
	  WHERE mtlp.organization_id= pod.destination_organization_id
	   ) org_code, --added for version 1.9
    DECODE ( NVL (POH.attribute2, 'STANDARD') ,'STANDARD',
    (SELECT SUBSTR (hrl.location_code, 1, 3)
    FROM 
      po_line_locations_all poll,
      hr_locations hrl
    WHERE 
        poll.line_location_id = pod.line_location_id  
    AND poll.ship_to_location_id = hrl.location_id
    AND rownum                   =1
    ),
    (SELECT location
    FROM hz_cust_site_uses_all hcsu
    WHERE hcsu.site_use_id =
      (SELECT ooh.ship_to_org_id
      FROM oe_drop_ship_sources ods ,
        oe_order_headers_all ooh
      WHERE ods.po_header_id=POH.po_header_id
      AND ooh.header_id     = ods.header_id
      AND rownum            =1
      )
    )) ship_to, --added for version 1.9
      decode(adl.ae_header_id,null,ael.unrounded_accounted_dr,adl.unrounded_accounted_dr)transaction_dr, --added for version 1.9	
      decode(adl.ae_header_id,null,ael.unrounded_accounted_cr,adl.unrounded_accounted_cr)transaction_cr --added for version 1.9
	  	   ,TO_CHAR(NULL) AR_TRX_SOURCE --Ver 2.0
       ,TO_DATE(NULL) AR_TRX_CREATE_DATE --Ver 2.0
FROM
  gl_je_headers jh,
  gl_je_lines jl,
  GL_JE_SOURCES JES,
  gl_je_batches gb,
  GL_LEDGERS LE,
  gl_period_statuses gps,
  gl_code_combinations_kfv gcc,
  gl_import_references gir,
  xla_ae_lines ael,
  gl_code_combinations_kfv gcc1,
  xla_ae_headers aeh,
  xla_events xlae,
  xla_distribution_links adl,
  ap_payment_hist_dists APHD,
  AP_INVOICE_PAYMENTS_ALL AIP,
  AP_CHECKS_ALL AC,
  AP_INVOICE_DISTRIBUTIONS_ALL AID,
  AP_INVOICES_ALL AI, 
  --po_vendors pov,
  FND_DOCUMENT_SEQUENCES ds,
  po_distributions_all pod, --added 9/27
  po_headers_all poh        --added 9/27
WHERE
  JH.JE_HEADER_ID                    = JL.JE_HEADER_ID
AND JH.JE_SOURCE                     = JES.JE_SOURCE_NAME
AND jes.user_je_source_name          = 'Payables'
AND jh.je_batch_id                   = gb.je_batch_id
AND jh.ledger_id                     = le.ledger_id
AND LE.OBJECT_TYPE_CODE              = 'L'
AND gps.application_id               = 101
AND GPS.PERIOD_NAME                  = JH.PERIOD_NAME
AND GPS.LEDGER_ID                    = LE.LEDGER_ID
AND jl.code_combination_id           = gcc.code_combination_id
AND jh.je_header_id                  = gir.je_header_id
AND jl.je_line_num                   = gir.je_line_num
AND gir.gl_sl_link_id                = ael.gl_sl_link_id(+)
AND GIR.GL_SL_LINK_TABLE             = AEL.GL_SL_LINK_TABLE(+)
AND ael.application_id(+)            = 200				--TMS# 20160426-00098
AND AEL.AE_HEADER_ID                 = AEH.AE_HEADER_ID(+)
AND ael.application_id               = aeh.application_id (+)  --TMS# 20160426-00098
AND aeh.application_id               = xlae.application_id(+) --sbala 10/16/12
AND aeh.event_id                     = xlae.event_id(+)       -- sbala 10/16/12
AND ael.code_combination_id          = gcc1.code_combination_id(+)
AND AEL.AE_HEADER_ID                 = ADL.AE_HEADER_ID(+)
AND ael.ae_line_num                  = adl.ae_line_num(+)
AND ael.application_id               = adl.application_id (+)   --TMS# 20160426-00098
AND adl.SOURCE_DISTRIBUTION_TYPE     = 'AP_PMT_DIST'
AND adl.SOURCE_DISTRIBUTION_ID_NUM_1 = APHD.PAYMENT_HIST_DIST_ID
AND APHD.INVOICE_PAYMENT_ID          = AIP.INVOICE_PAYMENT_ID
AND AIP.CHECK_ID                     = AC.CHECK_ID(+)
AND APHD.INVOICE_DISTRIBUTION_ID     = AID.INVOICE_DISTRIBUTION_ID
AND AID.INVOICE_ID                   = AI.INVOICE_ID
  --AND POV.VENDOR_ID = AI.VENDOR_ID
  --AND jh.je_source = 'Payables'
  --AND le.name = 'HD Supply USD' commented by Neha for TMS#20151027-00222
AND gir.subledger_doc_sequence_id = ds.doc_sequence_id(+)
AND aid.po_distribution_id        = pod.po_distribution_id(+) --added 9/27
AND POD.PO_HEADER_ID              = POH.PO_HEADER_ID(+)       --added 9/27
    --
    -- end of JE source "Payables" and xla_distribution_links source_distribution_type "AP_PMT_DIST" -Part 6
    --
UNION ALL
    --
    -- begin of JE source "Payables" and xla_distribution_links source_distribution_type "AP_PREPAY" -Part 7
    --
SELECT /*+ INDEX(apad AP_PREPAY_APP_DISTS_U1)*/   --TMS# 20160426-00098
          jes.user_je_source_name SOURCE,
           jh.je_category,
           gir.je_header_id je_header_id,
           gir.je_line_num je_line_num,
           jl.effective_date acc_date,
           jh.NAME entry,
           jh.je_batch_id batch,
           SUBSTR (jl.description, 1, 240) line_descr,
           ds.name LSequence,
           jh.doc_sequence_value hnumber,
           jl.je_line_num lline,
           jl.accounted_dr line_acctd_dr,
           jl.accounted_cr line_acctd_cr,
           jl.entered_dr ent_dr,
           jl.entered_cr ent_cr,
           gir.subledger_doc_sequence_id seq_id,
           gir.subledger_doc_sequence_value seq_num,
           jh.doc_sequence_id h_seq_id,
           gir.gl_sl_link_id gl_sl_link_id,
           (CASE
               WHEN SIGN (ai.vendor_id) = -1
               THEN
                  'No Supplier'
               ELSE
                  (SELECT vendor_name
                     FROM po_vendors
                    WHERE 1 = 1 AND vendor_id = ai.vendor_id)
            END)
              customer_or_vendor, --TMS# 20150105-00229
           TO_CHAR (NULL) customer_or_vendor_number, --TMS# 20150105-00229
           jl.entered_dr ENTERED_DR,
           jl.entered_cr ENTERED_CR,
           jl.accounted_cr ACCOUNTED_DR,
           jl.accounted_dr ACCOUNTED_CR,
           ael.unrounded_accounted_dr sla_line_accounted_dr,
           ael.unrounded_accounted_cr sla_line_accounted_cr,
             NVL (ael.unrounded_accounted_dr, 0)
           - NVL (ael.unrounded_accounted_cr, 0)
              sla_line_accounted_net,
           ael.unrounded_entered_dr sla_line_entered_dr,
           ael.unrounded_entered_cr sla_line_entered_cr,
             NVL (ael.unrounded_entered_dr, 0)
           - NVL (ael.unrounded_entered_cr, 0)
              sla_line_entered_net,
           adl.unrounded_entered_cr sla_dist_entered_cr,
           adl.unrounded_entered_dr sla_dist_entered_dr,
             NVL (adl.unrounded_entered_cr, 0)
           - NVL (adl.unrounded_entered_dr, 0)
              sla_dist_entered_net,
           adl.unrounded_accounted_cr sla_dist_accounted_cr,
           adl.unrounded_accounted_dr sla_dist_accounted_dr,
             --             NVL (adl.unrounded_accounted_cr, 0)
             --           - NVL (adl.unrounded_accounted_dr, 0)
             --              sla_dist_accounted_net,
             NVL (adl.unrounded_accounted_dr, 0)
           - NVL (adl.unrounded_accounted_cr, 0)
              sla_dist_accounted_net,
           '' associate_num,
           AI.INVOICE_NUM transaction_num, --TMS# 20150105-00229
           --TO_CHAR (NULL) SALES_ORDER, --TMS# 20150105-00229      --commented for TMS#20150602-00007 
           -- ai.attribute3 SALES_ORDER, --Commented for version 1.9	                               --Added for TMS#20150602-00007 
            nvl(ai.attribute3,to_char(NVL((SELECT oh.order_number
              FROM oe_order_headers oh,
                oe_drop_ship_sources odss
              WHERE oh.header_id    =odss.header_id
                AND odss.po_header_id = poh.po_header_id
                AND ROWNUM            =1
             ),(SELECT trim(SUBSTR(PRL.NOTE_TO_RECEIVER,instr(PRL.NOTE_TO_RECEIVER,':',1)+1,instr(PRL.NOTE_TO_RECEIVER,',',1)-instr(PRL.NOTE_TO_RECEIVER,':',1)-1))so
                FROM po_distributions_all pod,
                     po_req_distributions_all prd,
                     po_requisition_lines_all prl,
                     po_requisition_headers_all prh
                WHERE pod.po_header_id        = poh.po_header_id
                  AND pod.req_distribution_id   = prd.distribution_id
                  AND prd.requisition_line_id   = prl.requisition_line_id
                  AND prl.requisition_header_id = prh.requisition_header_id
                  AND prh.interface_source_code ='CTO'
                  AND ROWNUM                    =1
                  )))) SALES_ORDER, --added for version 1.9	
           gcc.CONCATENATED_SEGMENTS gl_account_string,
           jh.currency_code,
           jh.period_name,
           ael.accounting_class_code TYPE,
           gps.effective_period_num,
           le.name,
           gb.name batch_name,
          -- poh.segment1 PO_NUMBER, --TMS# 20150105-00229                 --commented for TMS#20150602-00007 
          CASE                                   --Added for TMS#20150602-00007 
             WHEN aid.po_distribution_id IS NULL THEN ai.attribute4
          ELSE poh.segment1 END PO_NUMBER,
           AI.ATTRIBUTE6 BOL_NUMBER, --TMS# 20150105-00229
           AI.SOURCE AP_INV_SOURCE, --TMS# 20150105-00229
           xlae.event_type_code SLA_EVENT_TYPE,
           --
           TO_CHAR (NULL) PART_NUMBER,  --TMS# 20150105-00229
           TO_CHAR (NULL) PART_DESCRIPTION,  --TMS# 20150105-00229
           --
           TO_NUMBER (NULL) ITEM_UNIT_WT, --TMS# 20150105-00229
           TO_NUMBER (NULL) ITEM_TXN_QTY, --TMS# 20150105-00229
           TO_NUMBER (NULL) ITEM_TOTAL_TXN_COST, --TMS# 20150105-00229
           TO_NUMBER (NULL) ITEM_OVRHD_CALC_COST, --TMS# 20150105-00229
           --
           --           AI.INVOICE_NUM||':'||
           (SELECT vendor_name
              FROM po_vendors
             WHERE     1 = 1
                   AND SIGN (ai.vendor_id) <> -1
                   AND vendor_id = ai.vendor_id)
              XXCUS_GSC_ENH_LINE_DESC, --ESMS# 281615
           (SELECT D.DESCRIPTION            --User who created the Requisition
              FROM FND_USER D, PO_REQ_DISTRIBUTIONS_ALL B
             WHERE     1 = 1
                   AND B.DISTRIBUTION_ID = pod.REQ_DISTRIBUTION_ID
                   AND D.USER_ID = B.CREATED_BY
                   AND ROWNUM = 1)
              XXCUS_GSC_PO_CREATED_BY, --ESMS# 281615
           (SELECT '''' || url image_link
              FROM fnd_attached_docs_form_vl
             WHERE     1 = 1
                   and ai.org_id IN (163, 167)
                   AND entity_name = 'AP_INVOICES'
                   AND pk1_value = TO_CHAR (ai.invoice_id)
                   AND datatype_name = 'Web Page'
                   AND category_description = 'Invoice Internal'
                   AND document_description = 'Documentum Image URL'
                   AND function_name = 'APXINWKB'
                   AND ROWNUM <2 --Ver 1.2
                   )
              XXCUS_GSC_IMAGE_LINK, --ESMS# 281615
           --
           gcc.code_combination_id,                      
           GCC.SEGMENT1 GCC50328PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_PRODUCT')
              GCC50328PRODUCTDESCR,
           GCC.SEGMENT2 GCC50328LOCATION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_LOCATION')
              GCC50328LOCATIONDESCR,
           GCC.SEGMENT3 GCC50328COST_CENTER,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT3,
                                                 'XXCUS_GL_COSTCENTER')
              GCC50328COST_CENTERDESCR,
           GCC.SEGMENT4 GCC50328ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ACCOUNT')
              GCC50328ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50328PROJECT_CODE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT5,
                                                 'XXCUS_GL_PROJECT')
              GCC50328PROJECT_CODEDESCR,
           GCC.SEGMENT6 GCC50328FURTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_FUTURE_USE1')
              GCC50328FURTURE_USEDESCR,
           GCC.SEGMENT7 GCC50328FUTURE_USE_2,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT7,
                                                 'XXCUS_GL_FUTURE_USE_2')
              GCC50328FUTURE_USE_2DESCR,
           GCC.SEGMENT1 GCC50368PRODUCT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT1,
                                                 'XXCUS_GL_LTMR_PRODUCT')
              GCC50368PRODUCTDESCR,
           GCC.SEGMENT2 GCC50368DIVISION,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT2,
                                                 'XXCUS_GL_ LTMR _DIVISION')
              GCC50368DIVISIONDESCR,
           GCC.SEGMENT3 GCC50368DEPARTMENT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT3,
              'XXCUS_GL_ LTMR _DEPARTMENT')
              GCC50368DEPARTMENTDESCR,
           GCC.SEGMENT4 GCC50368ACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT4,
                                                 'XXCUS_GL_ LTMR _ACCOUNT')
              GCC50368ACCOUNTDESCR,
           GCC.SEGMENT5 GCC50368SUBACCOUNT,
           xxeis.eis_rs_fin_utility.decode_vset (
              GCC.SEGMENT5,
              'XXCUS_GL_ LTMR _SUBACCOUNT')
              GCC50368SUBACCOUNTDESCR,
           GCC.SEGMENT6 GCC50368FUTURE_USE,
           xxeis.eis_rs_fin_utility.decode_vset (GCC.SEGMENT6,
                                                 'XXCUS_GL_ LTMR _FUTUREUSE')
              GCC50368FUTURE_USEDESCR,
           adl.source_distribution_type, --TMS# 20150105-00229
           adl.source_distribution_id_num_1, --TMS# 20150105-00229
           adl.applied_to_source_id_num_1, --TMS# 20150105-00229
           7 FETCH_SEQ, --TMS# 20150105-00229
           TRUNC (ai.invoice_date) ap_inv_date, --ESMS 281615
		   NULL WAYBILL  --added for version 1.7
		   ,TO_CHAR (NULL) CATEGORY_NAME, -- added for version 1.8
		   TO_CHAR (NULL) CATEGORY_DESCRIPTION, -- added for version 1.8
      	   NULL CATMGT_CATEGORY, -- added for version 1.8
      	   NULL CATMGT_CATEGORY_DESC, -- added for version 1.8       
          to_char(NULL) po_attribute2, --added for version 1.9
          to_char(NULL) org_code, --added for version 1.9
          to_char(NULL) ship_to,  --added for version 1.9
           decode(adl.ae_header_id,null,ael.unrounded_accounted_dr,adl.unrounded_accounted_dr)transaction_dr, --added for version 1.9	
           decode(adl.ae_header_id,null,ael.unrounded_accounted_cr,adl.unrounded_accounted_cr)transaction_cr  --added for version 1.9	
		   	   ,TO_CHAR(NULL) AR_TRX_SOURCE --Ver 2.0
       ,TO_DATE(NULL) AR_TRX_CREATE_DATE --Ver 2.0
     FROM
  gl_je_headers jh,
  gl_je_lines jl,
  GL_JE_SOURCES JES,
  gl_je_batches gb,
  gl_ledgers le,
  gl_period_statuses gps,
  gl_import_references gir,
  GL_CODE_COMBINATIONS_KFV GCC,
  xla_ae_lines ael,
  gl_code_combinations_kfv gcc1,
  xla_ae_headers aeh,
  xla_events xlae,
  xla_distribution_links adl,
  ap_prepay_app_dists apad,
  AP_INVOICE_DISTRIBUTIONS_ALL AID,
  AP_INVOICES_ALL AI,
  --po_vendors pov,
  FND_DOCUMENT_SEQUENCES DS,
  po_distributions_all pod, --added 9/27
  po_headers_all poh        --added 9/27
WHERE
  JH.JE_HEADER_ID                    = JL.JE_HEADER_ID
AND jes.user_je_source_name          = 'Payables'
AND JH.JE_SOURCE                     = JES.JE_SOURCE_NAME
AND JH.JE_BATCH_ID                   = GB.JE_BATCH_ID
AND JH.LEDGER_ID                     = LE.LEDGER_ID
AND le.object_type_code              = 'L'
AND GPS.APPLICATION_ID               = 101
AND GPS.PERIOD_NAME                  = JH.PERIOD_NAME
AND gps.ledger_id                    = le.ledger_id
AND jl.code_combination_id           = gcc.code_combination_id
AND jh.je_header_id                  = gir.je_header_id
AND JL.JE_LINE_NUM                   = GIR.JE_LINE_NUM
AND gir.gl_sl_link_id                = ael.gl_sl_link_id(+)
AND GIR.GL_SL_LINK_TABLE             = AEL.GL_SL_LINK_TABLE(+)
AND AEL.APPLICATION_ID(+)            = 200   				--TMS# 20160426-00098
AND ael.code_combination_id          = gcc1.code_combination_id(+)
AND AEL.AE_HEADER_ID                 = AEH.AE_HEADER_ID(+)
AND ael.application_id               = aeh.application_id (+)  --TMS# 20160426-00098
AND aeh.application_id               = xlae.application_id(+) --sbala 10/16/12
AND aeh.event_id                     = xlae.event_id(+)       -- sbala 10/16/12
AND ael.ae_header_id                 = adl.ae_header_id(+)
AND ael.ae_line_num                  = adl.ae_line_num(+)
AND ael.application_id               = adl.application_id (+)   --TMS# 20160426-00098
AND adl.SOURCE_DISTRIBUTION_TYPE     = 'AP_PREPAY'
AND adl.SOURCE_DISTRIBUTION_ID_NUM_1 = apad.prepay_app_dist_id
AND apad.invoice_distribution_id     = aid.invoice_distribution_id
AND AID.INVOICE_ID                   = AI.INVOICE_ID
  --AND POV.VENDOR_ID = AI.VENDOR_ID
  --AND jh.je_source = 'Payables'
  --AND le.name = 'HD Supply USD' commented by Neha for TMS#20151027-00222
AND gir.subledger_doc_sequence_id = ds.doc_sequence_id(+)
AND AID.PO_DISTRIBUTION_ID        = POD.PO_DISTRIBUTION_ID(+) --added 9/27
AND pod.po_header_id              = poh.po_header_id(+)       --added 9/27
--
-- end of JE source "Payables" and xla_distribution_links source_distribution_type "AP_PREPAY" -Part 7
--
)
/
