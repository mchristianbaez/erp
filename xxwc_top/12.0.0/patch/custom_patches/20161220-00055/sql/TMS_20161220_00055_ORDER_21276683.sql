/*************************************************************************
  $Header TMS_20161220_00055_ORDER_21276683.sql $
  Module Name: TMS_20161220_00055  Data Fix script for 21276683

  PURPOSE: Data fix script for 21276683--No permanent fix in process 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        01-FEB-2017  Niraj Kumar Ranjan     TMS#20160825-00139 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20161220-00055    , Before Update');

   update apps.oe_order_lines_all
   set FLOW_STATUS_CODE='CANCELLED',
       CANCELLED_FLAG='Y'
   where line_id = 75008826
   and header_id= 45844404;
   DBMS_OUTPUT.put_line (
         'TMS: 20161220-00055 Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20161220-00055    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161220-00055 , Errors : ' || SQLERRM);
END;
/
