/***********************************************************************************************************************************************
   NAME:     TMS_20180802-00077_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        08/02/2018  Rakesh Patel     TMS#20180802-00077-update attribute15 in ra_customer_trx_all
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before update');

  UPDATE apps.ra_customer_trx_all      rcta
     SET attribute15 = '12/31/2020'
   WHERE 1=1
     AND batch_source_id IN (1012, 1013)
     AND attribute15 IS NULL;
              
   DBMS_OUTPUT.put_line ('Records Updated -' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
	  ROLLBACK;
END;
/