/*************************************************************************
  $Header TMS_20160503-00058 _SHIPPED_TO_CLOSE.sql $
  Module Name: TMS_20160503-00058  Data Fix script for I667946 

  PURPOSE: Data Fix script for I667946

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        06-JUN-2016  Raghav Velichetti         TMS#20160503-00058 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160503-00058    , Before Update');

update apps.oe_order_lines_all
set 
  INVOICE_INTERFACE_STATUS_CODE='NOT_ELIGIBLE'
,open_flag='N'
,flow_status_code='CLOSED'
--,INVOICED_QUANTITY=1
where line_id =64614072
and headeR_id=39453195;

   DBMS_OUTPUT.put_line (
         'TMS: 20160503-00058  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160503-00058    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160503-00058 , Errors : ' || SQLERRM);
END;
/