 /********************************************************************************
  FILE NAME: xxwc.XXWC_OM_SO_LINE_PICK_AUD_TBL
  PROGRAM TYPE: Alter Table script

  PURPOSE: To fulfill PQC changes requirement

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     04/18/2018    Pattabhi Avula  TMS#20180416-00056 - PQC Post Golive issue
                                        fixes - revision 2  -- Initial Version
  ********************************************************************************/
-- Added column 
ALTER TABLE XXWC.XXWC_OM_SO_LINE_PICK_AUD_TBL  ADD(AVAILABLE_QTY      NUMBER)
/