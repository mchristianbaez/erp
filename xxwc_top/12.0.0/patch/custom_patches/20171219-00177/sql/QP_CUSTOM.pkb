CREATE OR REPLACE PACKAGE BODY APPS.QP_CUSTOM AS

  /*********************************************************************************
  -- Package QP_CUSTOM
  -- *******************************************************************************
  --
  -- PURPOSE: Package is used for Customizing Pricing 
  -- ===========================================================================
  -- HISTORY
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.0     02-Sep-2015   Gopi Damuluri   TMS# 20150831-00107
                                           Resolve issue with Default pricing on 
                                           AIS Lite Form.
  -- 2.0     27-03-2018    Rakesh Patel    TMS# 20180321-00207-Replacement Cost CSP Form Changes 										   
  *******************************************************************************/



  /********************************************************************************
  -- PROCEDURE: Get_Custom_Price
  --
  -- PURPOSE: Procedure to derive Custom Price
  -- ===========================================================================
  -- HISTORY
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.0     02-Sep-2015   Gopi Damuluri   TMS# 20150831-00107
                                           Resolve issue with Default pricing on 
                                           AIS Lite Form.
  -- 2.0     27-03-2018    Rakesh Patel    TMS# 20180321-00207-Replacement Cost CSP Form Changes 
  *******************************************************************************/
  FUNCTION Get_Custom_Price (p_price_formula_id     IN NUMBER,
                           p_list_price           IN NUMBER,
                           p_price_effective_date IN DATE,
                           p_req_line_attrs_tbl   IN QP_FORMULA_PRICE_CALC_PVT.REQ_LINE_ATTRS_TBL)
RETURN NUMBER AS
l_cost NUMBER ;
l_Price NUMBER ;
l_ship_org NUMBER;
l_item_id NUMBER;
l_line_id NUMBER;
l_Mod_Name   Varchar2(240) := 'QP_CUSTOM.GET_CUSTOM_PRICE' ;

-- Define Cursor to Check Pricing Formula is  VENDOR_ITEM_COST  : Satish U: 10-APR-2012

    C_FORMULA_VENDOR_ITEM_COST    CONSTANT VARCHAR2(30) :=  'WC_VENDOR_ITEM_COST';
    l_Vendor_Item_Cost    Number ;
    l_Organization_Id     Number ;
    l_Inventory_Item_id   Number ;
    l_SO_Line_ID          Number ;
    l_sell_price          Number ; 
    l_modifier_line       Number ; 
    l_operand             Number ; 
    i Number := 1                ; /*  TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, added to query modifier line operand during GCP call */
    l_rounding_factor Number ; /*  TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, added to query modifier line operand during GCP call */
    l_item_type VARCHAR2(30) ; /*  TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, added to query modifier line operand during GCP call */
    l_formula_id  NUMBER;--Added by Ram Talluri for TMS 20140407-00224 on 4/7/2014
    l_replacement_cost    NUMBER; --Rev#2.0 

    Cursor Get_Formula_Cur (P_Price_Formula_ID Number ) Is
        Select Price_Formula_Id
        From   apps.QP_PRICE_FORMULAS_VL
        Where  PRICE_FORMULA_ID = P_PRICE_FORMULA_ID
        AND    NAME             = C_FORMULA_VENDOR_ITEM_COST;
BEGIN
    XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '101 : Beging of API =' );
    oe_debug_pub.add('<-----In Get_Custom_Price Function----->');
    -- Satish U: 10-APR-2012 :  Vendor Quote : SSD Pricing Formula Variables
    l_Organization_Id := OE_ORDER_PUB.G_LINE.SHIP_FROM_ORG_ID ;
    l_Inventory_Item_id := OE_ORDER_PUB.G_LINE.INVENTORY_ITEM_ID ;
    l_SO_Line_ID := OE_ORDER_PUB.G_LINE.LINE_ID ;

    FOR i IN 1..p_req_line_attrs_tbl.count LOOP
       -- Assign Inventory Item ID
       If (p_req_line_attrs_tbl(i).attribute_type = 'PRODUCT') and
               (p_req_line_attrs_tbl(i).context = 'ITEM') and
               (p_req_line_attrs_tbl(i).attribute = 'PRICING_ATTRIBUTE1')
       Then
          l_item_id := p_req_line_attrs_tbl(i).value;
          XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '101.01 : Inventory Item ID ='  || p_req_line_attrs_tbl(i).value );
       End If;

       --XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '101.10 : Attribute Type ='  || p_req_line_attrs_tbl(i).attribute_type );
       --XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '101.11 : Attribute Context ='  || p_req_line_attrs_tbl(i).Context );
       --XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '101.12 : Attribute Type ='  || p_req_line_attrs_tbl(i).attribute );
       --XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '101.13 : Attributge Value ='  || p_req_line_attrs_tbl(i).value );
    END LOOP;

   --l_ship_org := OE_ORDER_PUB.G_LINE.SHIP_FROM_ORG_ID ;
   --l_item_id := OE_ORDER_PUB.G_LINE.INVENTORY_ITEM_ID ;
   -- If p_Price_Formula_ID = 8027 THEN (ORIGINAL )


     BEGIN
        -- SatishU : 06/10/2012 : Made changes to following code as per suggestion from Metalink
        SELECT line_id
        INTO l_line_id
        FROM qp_preq_lines_tmp
        WHERE line_index = p_req_line_attrs_tbl(1).line_index;

        --WHERE line_index = 1
        --AND line_type_code = 'LINE';

        XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '101.100 : = Line ID'  || l_line_id );
     EXCEPTION
        when others then
           SELECT line_id
           INTO l_line_id
           FROM apps.qp_preq_lines_tmp
           WHERE line_index = 2 AND line_type_code = 'LINE';
           XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '101.101 : = Line ID ' || l_line_id );
     END;

     If l_line_id Is Not Null Then
         Begin
             select ship_From_org_id
             --, inventory_item_id
             into l_ship_org
             --, l_Item_id
             from apps.oe_order_lines_all
             where line_id = l_line_id
             AND  Inventory_Item_ID = l_Item_ID ;

              XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '101.102 : = Ship From Org ' || l_ship_org );
              XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '101.103 : = Ship From Org ' || l_Item_id );
         Exception
            When others Then
               l_Line_Id := NULL ;
               Begin
                   Select  Organization_ID
                   Into l_Ship_Org
                   From XXWC_PA_WAREHOUSE_GT 
                   where rownum = 1              -- Version# 1.0
                   ;
                   XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '101.104 : = Ship From Org ' || l_ship_org );
               Exception
                  When Others Then
                     l_Ship_Org := Null;
               End;

         End ;
         
     -- 06/12/2013 CG TMS 20130605-00943: Change for Quote Form to be able to price lines where cost based formula is used.
     ELSE
        l_Line_Id := NULL ;
           Begin
               Select  Organization_ID
               Into l_Ship_Org
               From XXWC_PA_WAREHOUSE_GT 
               where rownum = 1;
               XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '101.104 : = Ship From Org ' || l_ship_org );
           Exception
              When Others Then
                 l_Ship_Org := Null;
           End;
     End If;

     --- Inncluded more Formula IDs : 08-JUN-2012
     -- Satish U: Added Formula IDs for
     --1."Commodity Cost Escalator 20%"
     --2."Commodity Cost Escalator 25%"
     --3."Commodity Cost Escalator 30%"
     --4."Commodity Cost Escalator 35%"
     --5."Commodity Cost Escalator 40%"
     --6."Commodity Cost Escalator 45%"
     --7."Branch Cost Modifier"
     --8."Cost Escalator"
     --9."Contingency Pricing Formula"
     -- ('7029', '13105','8029','13091','13092','13093','13094','13095','13096')
    XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '101.105 : SHip From Org ID =' || l_ship_org  );
    XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '101.106 : Item ID =' || l_item_id  );
    XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '101.107 : Price_Formula_ID =' || p_Price_Formula_ID  );

     IF --p_Price_Formula_ID in ('7030','7037', '7049','7050','7051','7053','7054','7055','7056')--Commented by Ram Talluri for TMS 20140407-00224 on 4/7/2014
        p_Price_Formula_ID in ('7030','7049','7050','7051','7053','7054','7055','7056')--Added by Ram Talluri for TMS 20140407-00224 on 4/7/2014
        AND l_Ship_Org is Not Null and l_Item_Id is Not Null THEN
       --If p_Price_Formula_ID in ('8027','7029','8029') AND l_Ship_Org is Not Null and l_Item_Id is Not Null THEN
        BEGIN
            --l_ship_org := OE_ORDER_PUB.G_LINE.SHIP_FROM_ORG_ID ;
            --l_item_id := OE_ORDER_PUB.G_LINE.INVENTORY_ITEM_ID ;
            XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '102 : SHip From Org ID =' || l_ship_org  );
            XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '103 : Item ID =' || l_item_id  );
            oe_debug_pub.add('102 : SHip From Org ID =' || l_ship_org);
            oe_debug_pub.add('103 : Item ID =' || l_item_id);

          BEGIN
            SELECT item_cost
            INTO l_cost
            FROM apps.CST_ITEM_COST_TYPE_V
            WHERE inventory_item_id = l_item_id
            AND organization_id = l_ship_org
            AND cost_type_id = 2  ;
            XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '104 : Item Cost  =' || l_cost  );
            oe_debug_pub.add('104 : Item Cost  =' || l_cost);
          EXCEPTION
           WHEN OTHERS THEN
               XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_ERROR, l_mod_name, '105 :  When Others Exception Item Cost is ZERO ' );
               oe_debug_pub.add('105 :  When Others Exception Item Cost is ZERO');
              END ; 

          --Rev#2.0 < Start 
          l_replacement_cost :=  XXWC_OM_SO_CREATE.Get_replacement_cost ( p_ship_from_org_id    => l_ship_org
                                                                         ,p_inventory_item_id   => l_item_id
                                                                        );
          XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '104.1 : Item Replacement Cost  =' || l_replacement_cost  );
          oe_debug_pub.add('104.1 : Item Replacement Cost  =' || l_replacement_cost);

          IF NVL(l_replacement_cost, 0) <> 0 THEN    
             l_cost := l_replacement_cost;
             XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '104.1 : Item Cost  =' || l_cost  );
             oe_debug_pub.add('104.1 : Item Cost  =' || l_cost);
          END IF;           
          --Rev#2.0 > End
    
               BEGIN
            select CREATED_FROM_LIST_LINE_ID--, operand_value
            into l_modifier_line--, l_operand
            from apps.qp_npreq_ldets_tmp
            where line_index =p_req_line_attrs_tbl(i).line_index
            and pricing_status_code in ('N','X','UPDATED')
            and applied_flag = 'Y'
            --and price_formula_id in ('7030','7037', '7049','7050','7051','7053','7054','7055','7056') ; --Commented by Ram Talluri for TMS 20140407-00224 on 4/7/2014
            and price_formula_id in ('7030','7049','7050','7051','7053','7054','7055','7056') ;--Added by Ram Talluri for TMS 20140407-00224 on 4/7/2014
             EXCEPTION
                         WHEN OTHERS THEN
                            XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_ERROR, l_mod_name, '105 :  No Modifier Line ID Found' );
          END ; 
       
       
        /*  TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, added to query modifier line operand during GCP call */
           Begin
               select operand
               into l_operand
               from apps.qp_list_lines
               where list_line_id = l_modifier_line ; 
                EXCEPTION
                         WHEN OTHERS THEN
                             XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_ERROR, l_mod_name, '105 :  No operand found ' );
                             oe_debug_pub.add('105 :  No operand found');
               END ; 
              
              
               Begin
                     select item_type
                     into l_item_type
                     from apps.mtl_system_items_b
                     WHERE inventory_item_id = l_item_id
                     AND organization_id = l_ship_org ; 
                     EXCEPTION
                         WHEN OTHERS THEN
                             l_item_type := NULL ;
                    END ; 
                        
                     
                      IF l_item_type in ('SPECIAL','REPAIR','K')
                      THEN l_rounding_factor := 5 ; /*TMS For Rounding Re-implementation 20140414-00019) */
                      ELSE
                          BEGIN 
                           select N_EXT_ATTR1 
                           into l_rounding_factor
                           from apps.EGO_MTL_SY_ITEMS_EXT_B 
                           where N_EXT_ATTR1 is not null 
                           and attr_group_id = 861
                           and inventory_item_id = l_item_id
                          AND organization_id = 222 ;
                          EXCEPTION
                                 WHEN OTHERS THEN
                                     l_rounding_factor := 2 ;
                                     END ; 
                      END IF ; 
                          
              l_sell_price := ROUND((l_cost /(1 - l_operand)),l_rounding_factor) ;
              
              RETURN l_sell_price ; 
               
               
               
               
               
        END ;
        
        
        
        
     End If;
     /**********************************************
     -- Satish U: 27-MAR-2012 : Vendor QUote  Formula :
     -- Vendor Item Cost :  Derive it using Seeded API.
     If P_PRICE_FORMULA_ID IS NOT NULL  Then
        For Formula_Rec In Get_Formula_Cur(p_price_formula_id )   Loop
           l_Vendor_Item_Cost := OZF_QP_QUAL_PVT.get_item_cost (OE_ORDER_PUB.G_LINE.line_id,OZF_ORDER_PRICE_PVT.G_RESALE_LINE_TBL) ;

           XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_statement, l_mod_name, '120 :  Calculating Item Cost using OZF_QP_QUAL_PVT.Get_Item_Cost API' );
           Return l_Vendor_Item_Cost ;
        ENd Loop;
     End IF;

     -- Satish U: 27-MAR-2012 : Vendor QUote  Formula :
     -- Vendor Item Cost :
     If P_PRICE_FORMULA_ID IS NOT NULL
        AND l_SO_Line_ID IS NOT NULL
        AND l_Inventory_Item_ID IS NOT NULL
        AND l_Organization_ID  IS NOT NULL Then
        For Formula_Rec In Get_Formula_Cur(p_price_formula_id )   Loop
           OZF_QP_QUAL_PVT.get_item_cost (OE_ORDER_PUB.G_LINE.line_id,OZF_ORDER_PRICE_PVT.G_RESALE_LINE_TBL)
            l_Vendor_Item_Cost := XXWC_OZF_VENDOR_QUOTE_PKG.Get_Vendor_Item_Cost(
                p_Inventory_Item_ID => l_Inventory_Item_ID ,
                P_SO_Line_ID        => l_SO_Line_ID  ,
                P_Organization_ID   => l_Organization_ID )  ;
            XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_statement, l_mod_name, '120 :  Calculating Vendor Item Cost' );
            Return l_Vendor_Item_Cost ;
        ENd Loop;
     End IF;
     ***************************/
     ---Added by Ram Talluri for TMS 20140407-00224 on 4/7/2014
   BEGIN
   
       BEGIN
        l_formula_id:=NULL;
        SELECT Price_Formula_Id
          INTO l_formula_id
          FROM apps.QP_PRICE_FORMULAS
         WHERE UPPER(NAME) = 'PERCENT DISCOUNT' AND ROWNUM = 1;
       EXCEPTION
        WHEN OTHERS THEN
        l_formula_id := NULL;
       END;         
          /* New Logic Added to return rounding factor for Rounding TMS, Percent Discount */
    
     IF p_Price_Formula_ID = l_formula_id
        AND l_Ship_Org is Not Null and l_Item_Id is Not Null THEN
        BEGIN
            --l_ship_org := OE_ORDER_PUB.G_LINE.SHIP_FROM_ORG_ID ;
            --l_item_id := OE_ORDER_PUB.G_LINE.INVENTORY_ITEM_ID ;
            XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '102 : SHip From Org ID =' || l_ship_org  );
            XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '103 : Item ID =' || l_item_id  );
            oe_debug_pub.add('102 : SHip From Org ID =' || l_ship_org);
            oe_debug_pub.add('103 : Item ID =' || l_item_id);

                  Begin
                     select item_type
                     into l_item_type
                     from apps.mtl_system_items_b
                     WHERE inventory_item_id = l_item_id
                     AND organization_id = l_ship_org ; 
                     EXCEPTION
                         WHEN OTHERS THEN
                             l_item_type := NULL ;
                    END ; 
                        
                     
                     IF l_item_type in ('SPECIAL','REPAIR','K')
                     THEN l_rounding_factor := 5 ; /*TMS For Rounding Re-Implementation 20140414-00019) */
                     ELSE
                          BEGIN 
                           select N_EXT_ATTR1 
                           into l_rounding_factor
                           from apps.EGO_MTL_SY_ITEMS_EXT_B 
                           where N_EXT_ATTR1 is not null 
                           and attr_group_id = 861
                           and inventory_item_id = l_item_id
                          AND organization_id = 222 ;
                          EXCEPTION
                                 WHEN OTHERS THEN
                                     l_rounding_factor := 2 ;
                                     END ; 
                      END IF ; 
                      
                
                 RETURN l_rounding_factor ; 
          END ; 
     END IF ; 
   --Added by Ram Talluri for TMS 20140407-00224 on 4/7/2014
   EXCEPTION
    WHEN OTHERS THEN
    l_rounding_factor := 2 ;
    RETURN l_rounding_factor ; 
   END;
     
     --Below logic for Contingency Price formula is added by Ram Talluri for TMS 20140407-00224 on 4/7/2014
   BEGIN
   
    BEGIN
        l_formula_id:=NULL;
        SELECT Price_Formula_Id
          INTO l_formula_id
          FROM apps.QP_PRICE_FORMULAS
         WHERE UPPER(NAME) = 'CONTINGENCY PRICING FORMULA' AND ROWNUM = 1;
    EXCEPTION
        WHEN OTHERS THEN
        l_formula_id := NULL;
    END;
    
     IF p_Price_Formula_ID = l_formula_id
        AND l_Ship_Org is Not Null and l_Item_Id is Not Null THEN
        BEGIN
            XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '102 : SHip From Org ID =' || l_ship_org  );
            XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '103 : Item ID =' || l_item_id  );
            oe_debug_pub.add('102 : SHip From Org ID =' || l_ship_org);
            oe_debug_pub.add('103 : Item ID =' || l_item_id);

            SELECT item_cost
            INTO l_cost
            FROM apps.CST_ITEM_COST_TYPE_V
            WHERE inventory_item_id = l_item_id
            AND organization_id = l_ship_org
            AND cost_type_id = 2  ;
            XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_STATEMENT, l_mod_name, '104 : Item Cost  =' || l_cost  );
            oe_debug_pub.add('104 : Item Cost  =' || l_cost);
            
            IF l_cost=0 THEN 
            l_cost := NULL;
            END IF;
            
            Return l_cost ;
        EXCEPTION
           WHEN OTHERS THEN
               XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_ERROR, l_mod_name, '105 :  When Others Exception Item Cost is ZERO ' );
               oe_debug_pub.add('105 :  When Others Exception Item Cost is ZERO');
               Return 0;
        END ;
     END IF;
   EXCEPTION
           WHEN OTHERS THEN
               XXWC_GEN_ROUTINES_PKG.LOG_MSG (g_LEVEL_ERROR, l_mod_name, '106 :  When Others Exception for Contingency Price formula ' );
               oe_debug_pub.add('106 :  When Others Exception for Contingency Price formula');
               Return 0;
   END;
     --Changes end by Ram Talluri for TMS 20140407-00224 on 4/7/2014     

End Get_Custom_Price;
END QP_CUSTOM ;
/