

/* Formatted on 11/13/2013 3:27:49 PM (QP5 v5.256.13226.35510) */
CREATE OR REPLACE FORCE VIEW APPS.XXWC_CUSTOMER_MAINT_FORM
/*************************************************************************
     $Header    : XXWC_CUSTOMER_MAINT_FORM
     Module Name: AR
      
     REVISIONS:

     Ver        Date        Author              Description
     ---------  ----------  ----------          ----------------
     1.0                                        Initial version
     2.0        03/30/2015  Maharajan Shunmugam TMS#20140516-00043 Additional fields to Customer Maintenance Form  
     3.0        06/23/2015  Maharajan Shunmugam TMS#20150604-00146 Billtrust - Automatic Update of Credit Balance Statement Field
 **************************************************************************/
(
   PROF_ROWID,
   CUST_ROWID,
   SITE_USES_ROWID,
   CUST_ACCT_SITE_ROWID,
   CUST_ACCOUNT_ID,
   CUST_ACCOUNT_PROFILE_ID,
   CUST_ACCT_SITE_ID,
   SITE_USE_ID,
   PARTY_SITE_PARTY_ID,
   LV,
   ACCOUNT_NUMBER,
   PROFILE_PARTY_ID,
   ACCOUNT_NAME,
   ACCOUNT_ADDRESS,
   SITE_ADDRESS,
   PARTY_SITE_NUMBER,
   ACCOUNT_STATUS,
   PROFILE_ACCOUNT_STATUS,
   ACCOUNT_FREIGHT_TERM,
   COLLECTOR_ID,
   COLLECTOR_NAME,
   CREDIT_ANALYST_ID,
   CREDIT_ANALYST_NAME,
   CREDIT_CHECKING,
   CREDIT_HOLD,
   REMMIT_TO_CODE_ATTRIBUTE2,
   ACCOUNT_PAYMENT_TERMS,
   STANDARD_TERMS_NAME,
   STANDARD_TERMS_DESCRIPTION,
   CREDIT_CLASSIFICATION,
   PROFILE_CLASS_ID,
   PROFILE_CLASS_NAME,
   CREDIT_CLASSIFICATION_MEANING,
   ACCOUNT_STATUS_MEANING,
   PURPOSES,
   SITE_USE_CODE,
   PRIMARY_FLAG,
   ACCT_SITE_BILL_TO_FLAG,
   ACCT_SITE_SHIP_TO_FLAG,
   SITE_STATUS,
   LOCATION,
   ORG_ID,
   PRIMARY_SALESREP_ID,
   SALESREPS_NAME,
   SITE_FREIGHT_TERMS,
   ACCT_SITE_PARTY_SITE_ID,
   ACCT_SITE_STATUS,
   SITE_USES_CREATION_DATE,
   LEVEL_FLAG,
   PRINT_PRICE,
   MAND_PO,
   JOINT_CHECK,
   CATEGORY,
 --Added for ver 2.0 <<Start
   PREDOMINANT_TRADE,
   LEGAL_COLLECTION_INDICATOR,
   LEGAL_PLACEMENT,
   TAX_EXEMPT,
   TAX_EXEMPT_TYPE,
   CLASSIFICATION,
   SEND_STATEMENT,
   MANDATORY_NOTE,
   MANDATORY_NOTE1,
   MANDATORY_NOTE2,
   MANDATORY_NOTE3,
   MANDATORY_NOTE4,
   MANDATORY_NOTE5,
   ORDERBY,
 --Added for ver 2.0 <<End
   SEND_CREDIT_BALANCE               --Added by Maha for ver# 3.0
)
AS
   SELECT prof.ROWID prof_rowid,
          cust.ROWID cust_rowid,
          site_uses.ROWID site_uses_rowid,
          acct_site.ROWID cust_acct_site_rowid,
          cust.cust_account_id,
          prof.cust_account_profile_id,
          acct_site.cust_acct_site_id,
          site_uses.site_use_id site_use_id,
          party_site.party_id party_site_party_id,
          NVL2 (TO_CHAR (site_uses.site_use_id), 'Site', 'Account') lv, -- ,NVL2 (TO_CHAR (prof.site_use_id), 'Site', 'Account') lv,
          cust.account_number,
          prof.party_id profile_party_id,
          cust.account_name,
          arh_addr_pkg.format_address (NULL,
                                       party.address1,
                                       party.address2,
                                       party.address3,
                                       party.address4,
                                       party.city,
                                       party.county,
                                       party.state,
                                       party.province,
                                       party.postal_code,
                                       NULL)
             account_address,
          NVL (arh_addr_pkg.format_address (loc.address_style,
                                            loc.address1,
                                            loc.address2,
                                            loc.address3,
                                            loc.address4,
                                            loc.city,
                                            loc.county,
                                            loc.state,
                                            loc.province,
                                            loc.postal_code,
                                            NULL),
               arh_addr_pkg.format_address (NULL,
                                            party.address1,
                                            party.address2,
                                            party.address3,
                                            party.address4,
                                            party.city,
                                            party.county,
                                            party.state,
                                            party.province,
                                            party.postal_code,
                                            NULL))
             site_address,
          party_site.party_site_number,
          prof.account_status,
          prof.account_status profile_account_status,
          cust.freight_term account_freight_term,
          prof.collector_id,
          col.name collector_name,
          prof.credit_analyst_id,
          xxwc_ar_customer_maint_form.credit_analyst (prof.credit_analyst_id)
             credit_analyst_name,
          prof.credit_checking,
          prof.credit_hold,
          prof.attribute2 remmit_to_code_attribute2,
          prof.standard_terms account_payment_terms,
          term.name standard_terms_name,
          term.description standard_terms_description,
          prof.credit_classification,
          pc.profile_class_id,
          pc.name profile_class_name,
          arpt_sql_func_util.get_lookup_meaning (
             'AR_CMGT_CREDIT_CLASSIFICATION',
             prof.credit_classification)
             credit_classification_meaning,
          arpt_sql_func_util.get_lookup_meaning ('ACCOUNT_STATUS',
                                                 prof.account_status)
             account_status_meaning,
          NVL (
             xxwc_ar_customer_maint_form.get_site_purposes (
                site_uses.site_use_id),
             'Account')
             purposes,
          site_uses.site_use_code,
          site_uses.primary_flag,
          acct_site.bill_to_flag acct_site_bill_to_flag,
          acct_site.ship_to_flag acct_site_ship_to_flag,
          site_uses.status site_status,
          site_uses.location,
          NVL (site_uses.org_id, acct_site.org_id) org_id,
          site_uses.primary_salesrep_id,
          xxwc_ar_customer_maint_form.get_sales_rep (
             site_uses.org_id,
             site_uses.primary_salesrep_id)
             salesreps_name,
          site_uses.freight_term site_freight_terms,
          acct_site.party_site_id acct_site_party_site_id,
          acct_site.status acct_site_status,
          site_uses.creation_date site_uses_creation_date,
          'S' level_flag,
          acct_site.attribute1 PRINT_PRICE,
          acct_site.attribute3 MAND_PO,
          acct_site.attribute12 JOINT_CHECK,
          acct_site.CUSTOMER_CATEGORY_CODE CATEGORY,
 --Added for ver 2.0 <<Start
          cust.attribute9 PREDOMINANT_TRADE,
          cust.attribute5 LEGAL_COLLECTION_INDICATOR,
          cust.attribute10 LEGAL_PLACEMENT,
          acct_site.attribute16 TAX_EXEMPT,
          acct_site.attribute15 TAX_EXEMPT_TYPE,
          arpt_sql_func_util.get_lookup_meaning('CUSTOMER CLASS',cust.customer_class_code) CLASSIFICATION,
          prof.send_statements SEND_STATEMENT,
          cust.attribute_category MANDATORY_NOTE,
          cust.attribute17 MANDATORY_NOTE1,
          cust.attribute18 MANDATORY_NOTE2,
          cust.attribute19 MANDATORY_NOTE3,
          cust.attribute20 MANDATORY_NOTE4,
          cust.attribute16 MANDATORY_NOTE5,
          DECODE(NVL (
             xxwc_ar_customer_maint_form.get_site_purposes (
                site_uses.site_use_id),
             'Account'),'Account',1,'PBillTo',2,'PShipTo',3) ORDERBY,
 --Added for ver 2.0 <<End
          prof.credit_balance_statements SEND_CREDIT_BALANCE      --Added by Maha for ver# 3.0
     FROM hz_cust_accounts cust,
          hz_customer_profiles prof,
          hz_cust_profile_classes pc,
          (SELECT *
             FROM apps.hz_cust_site_uses s
			--  WHERE s.status = 'A' AND s.org_id = 162) site_uses,   -- 29/09/2014 Org_id removed by pattabhi for Canada & US OU Testing 
            WHERE s.status = 'A') site_uses,
          apps.hz_cust_acct_sites acct_site,
          hz_party_sites party_site,
          hz_locations loc,
          hz_parties party,
          ar_collectors col,
          ra_terms term
    WHERE     cust.cust_account_id = prof.cust_account_id     --**************
          AND prof.profile_class_id = pc.profile_class_id
          AND prof.site_use_id IS NULL
          AND acct_site.cust_account_id = cust.cust_account_id
          -- AND prof.site_use_id = site_uses.site_use_id(+)
          AND acct_site.cust_acct_site_id = site_uses.cust_acct_site_id
          AND loc.location_id(+) = party_site.location_id
          AND acct_site.party_site_id = party_site.party_site_id(+)
          AND cust.party_id = party.party_id                  --**************
          AND prof.collector_id = col.collector_id(+)
          AND prof.standard_terms = term.term_id(+)
          AND NOT EXISTS
                     (SELECT 'X'
                        FROM hz_customer_profiles rt
                       WHERE     rt.site_use_id = site_uses.site_use_id
                             AND rt.cust_account_id = prof.cust_account_id)
   UNION
   SELECT prof.ROWID prof_rowid,
          cust.ROWID cust_rowid,
          site_uses.ROWID site_uses_rowid,
          acct_site.ROWID cust_acct_site_rowid,
          cust.cust_account_id,
          prof.cust_account_profile_id,
          acct_site.cust_acct_site_id,
          site_uses.site_use_id site_use_id,
          party_site.party_id party_site_party_id,
          NVL2 (TO_CHAR (prof.site_use_id), 'Site', 'Account') lv,
          cust.account_number,
          prof.party_id profile_party_id,
          cust.account_name,
          arh_addr_pkg.format_address (NULL,
                                       party.address1,
                                       party.address2,
                                       party.address3,
                                       party.address4,
                                       party.city,
                                       party.county,
                                       party.state,
                                       party.province,
                                       party.postal_code,
                                       NULL)
             account_address,
          NVL (arh_addr_pkg.format_address (loc.address_style,
                                            loc.address1,
                                            loc.address2,
                                            loc.address3,
                                            loc.address4,
                                            loc.city,
                                            loc.county,
                                            loc.state,
                                            loc.province,
                                            loc.postal_code,
                                            NULL),
               arh_addr_pkg.format_address (NULL,
                                            party.address1,
                                            party.address2,
                                            party.address3,
                                            party.address4,
                                            party.city,
                                            party.county,
                                            party.state,
                                            party.province,
                                            party.postal_code,
                                            NULL))
             site_address,
          party_site.party_site_number,
          prof.account_status,
          prof.account_status profile_account_status,
          cust.freight_term account_freight_term,
          prof.collector_id,
          col.name collector_name,
          prof.credit_analyst_id,
          xxwc_ar_customer_maint_form.credit_analyst (prof.credit_analyst_id)
             credit_analyst_name,
          prof.credit_checking,
          prof.credit_hold,
          prof.attribute2 remmit_to_code_attribute2,
          prof.standard_terms account_payment_terms,
          term.name standard_terms_name,
          term.description standard_terms_description,
          prof.credit_classification,
          pc.profile_class_id,
          pc.name profile_class_name,
          arpt_sql_func_util.get_lookup_meaning (
             'AR_CMGT_CREDIT_CLASSIFICATION',
             prof.credit_classification)
             credit_classification_meaning,
          arpt_sql_func_util.get_lookup_meaning ('ACCOUNT_STATUS',
                                                 prof.account_status)
             account_status_meaning,
          NVL (
             xxwc_ar_customer_maint_form.get_site_purposes (
                site_uses.site_use_id),
             'Account')
             purposes,
          site_uses.site_use_code,
          site_uses.primary_flag,
          acct_site.bill_to_flag acct_site_bill_to_flag,
          acct_site.ship_to_flag acct_site_ship_to_flag,
          site_uses.status site_status,
          site_uses.location,
          NVL (site_uses.org_id, acct_site.org_id) org_id,
          site_uses.primary_salesrep_id,
          xxwc_ar_customer_maint_form.get_sales_rep (
             site_uses.org_id,
             site_uses.primary_salesrep_id)
             salesreps_name,
          site_uses.freight_term site_freight_terms,
          acct_site.party_site_id acct_site_party_site_id,
          acct_site.status acct_site_status,
          site_uses.creation_date site_uses_creation_date,
          'P' level_flag,
          acct_site.attribute1 PRINT_PRICE,
          acct_site.attribute3 MAND_PO,
          acct_site.attribute12 JOINT_CHECK,
          acct_site.CUSTOMER_CATEGORY_CODE CATEGORY,
 --Added for ver 2.0 <<Start
          cust.attribute9 PREDOMINANT_TRADE,
          cust.attribute5 LEGAL_COLLECTION_INDICATOR,
          cust.attribute10 LEGAL_PLACEMENT,
          acct_site.attribute16 TAX_EXEMPT,
          acct_site.attribute15 TAX_EXEMPT_TYPE,
          arpt_sql_func_util.get_lookup_meaning('CUSTOMER CLASS',cust.customer_class_code) CLASSIFICATION,
          prof.send_statements SEND_STATEMENT,
	  cust.attribute_category MANDATORY_NOTE,
          cust.attribute17 MANDATORY_NOTE1,
          cust.attribute18 MANDATORY_NOTE2,
          cust.attribute19 MANDATORY_NOTE3,
          cust.attribute20 MANDATORY_NOTE4,
          cust.attribute16 MANDATORY_NOTE5,
          DECODE(NVL (
             xxwc_ar_customer_maint_form.get_site_purposes (
                site_uses.site_use_id),
             'Account'),'Account',1,'PBillTo',2,'PShipTo',3) ORDERBY,
 --Added for ver 2.0 <<End
          prof.credit_balance_statements SEND_CREDIT_BALANCE      --Added by Maha for ver# 3.0
     FROM hz_cust_accounts cust,
          hz_customer_profiles prof,
          hz_cust_profile_classes pc,
          (SELECT *
             FROM apps.hz_cust_site_uses s
            WHERE s.status = 'A') site_uses,
          apps.hz_cust_acct_sites acct_site,
          hz_party_sites party_site,
          hz_locations loc,
          hz_parties party,
          ar_collectors col,
          ra_terms term
    WHERE     cust.cust_account_id = prof.cust_account_id
          AND prof.profile_class_id = pc.profile_class_id
          AND prof.site_use_id(+) = site_uses.site_use_id
          AND acct_site.cust_acct_site_id(+) = site_uses.cust_acct_site_id
          AND loc.location_id(+) = party_site.location_id
          AND acct_site.party_site_id = party_site.party_site_id(+)
          AND cust.party_id = party.party_id
          AND prof.collector_id = col.collector_id(+)
          AND prof.standard_terms = term.term_id(+)
UNION
 --Added below SELECT query for ver 2.0
SELECT prof.ROWID prof_rowid,
       cust.ROWID cust_rowid,
       NULL site_uses_rowid,
       NULL cust_acct_site_rowid,
       cust.cust_account_id,
       prof.cust_account_profile_id,
       NULL cust_acct_site_id,
       NULL site_use_id,
       NULL party_site_party_id,
       NVL2 (TO_CHAR (prof.site_use_id), 'Site', 'Account') lv,
       cust.account_number,
       prof.party_id profile_party_id,
       cust.account_name,
       arh_addr_pkg.format_address (NULL,
                                    party.address1,
                                    party.address2,
                                    party.address3,
                                    party.address4,
                                    party.city,
                                    party.county,
                                    party.state,
                                    party.province,
                                    party.postal_code,
                                    NULL)
          account_address,
       NULL site_address,
       NULL party_site_number,
       prof.account_status,
       prof.account_status profile_account_status,
       cust.freight_term account_freight_term,
       prof.collector_id,
       col.name collector_name,
       prof.credit_analyst_id,
       xxwc_ar_customer_maint_form.credit_analyst (prof.credit_analyst_id)
          credit_analyst_name,
       prof.credit_checking,
       prof.credit_hold,
       prof.attribute2 remmit_to_code_attribute2,
       prof.standard_terms account_payment_terms,
       term.name standard_terms_name,
       term.description standard_terms_description,
       prof.credit_classification,
       pc.profile_class_id,
       pc.name profile_class_name,
       arpt_sql_func_util.get_lookup_meaning (
          'AR_CMGT_CREDIT_CLASSIFICATION',
          prof.credit_classification)
          credit_classification_meaning,
       arpt_sql_func_util.get_lookup_meaning ('ACCOUNT_STATUS',
                                              prof.account_status)
          account_status_meaning,
       NVL (xxwc_ar_customer_maint_form.get_site_purposes (NULL), 'Account')
          purposes,
       NULL site_use_code,
       NULL primary_flag,
       NULL acct_site_bill_to_flag,
       NULL acct_site_ship_to_flag,
       NULL site_status,
       NULL location,
       NULL org_id,
       NULL primary_salesrep_id,
       NULL salesreps_name,
       NULL site_freight_terms,
       NULL acct_site_party_site_id,
       NULL acct_site_status,
       NULL site_uses_creation_date,
       'P' level_flag,
       NULL PRINT_PRICE,
       NULL MAND_PO,
       NULL JOINT_CHECK,
       NULL CATEGORY,
       cust.attribute9 PREDOMINANT_TRADE,
       cust.attribute5 LEGAL_COLLECTION_INDICATOR,
       cust.attribute10 LEGAL_PLACEMENT,
       NULL TAX_EXEMPT,
       NULL TAX_EXEMPT_TYPE,
       arpt_sql_func_util.get_lookup_meaning('CUSTOMER CLASS',cust.customer_class_code) CLASSIFICATION,
       prof.send_statements SEND_STATEMENT,
       cust.attribute_category MANDATORY_NOTE,
       cust.attribute17 MANDATORY_NOTE1,
       cust.attribute18 MANDATORY_NOTE2,
       cust.attribute19 MANDATORY_NOTE3,
       cust.attribute20 MANDATORY_NOTE4,
       cust.attribute16 MANDATORY_NOTE5,
       1 ORDERBY,
       prof.credit_balance_statements SEND_CREDIT_BALANCE      --Added by Maha for ver# 3.0
  FROM hz_cust_accounts cust,
       hz_customer_profiles prof,
       hz_cust_profile_classes pc,
       hz_parties party,
       ar_collectors col,
       ra_terms term
 WHERE     1 = 1
       AND prof.site_use_id IS NULL
       AND cust.cust_account_id = prof.cust_account_id
       AND prof.profile_class_id = pc.profile_class_id
       AND cust.party_id = party.party_id       
       AND prof.collector_id = col.collector_id(+)
       AND prof.standard_terms = term.term_id(+);
