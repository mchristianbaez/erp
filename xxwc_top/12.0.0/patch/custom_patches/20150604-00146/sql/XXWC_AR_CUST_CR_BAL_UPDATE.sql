/*
 TMS: 20150604-00146
 Date: 07/08/2015
 Notes: One time script to update credit balance statements
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
      l_customer_profile_rec          HZ_CUSTOMER_PROFILE_V2PUB.customer_profile_rec_type;
      l_return_status                 VARCHAR2 (2000);
      l_msg_count                     NUMBER;
      l_msg_data                      VARCHAR2 (2000);
      l_obj_version_number            NUMBER;
      l_org_id                        NUMBER;
      l_sec          		      VARCHAR2 (200);
      l_msg               	      VARCHAR2 (150);

   BEGIN


 FOR I IN (SELECT hcp.cust_account_profile_id, 
       		  hcp.object_Version_number
 	   FROM apps.hz_cust_accounts_all hca, 
                apps.hz_customer_profiles hcp
           WHERE hcp.cust_account_id = hca.cust_account_id
             AND hcp.site_use_id IS NULL
             AND hcp.send_statements = 'N'
             AND hcp.credit_balance_statements = 'Y'       
         UNION
	  SELECT hcp.cust_account_profile_id,
       		 hcp.object_Version_number
  	  FROM apps.hz_parties hp,
       	       apps.hz_party_sites hps,
       	       apps.hz_cust_accounts_all hca,
       	       apps.hz_cust_acct_sites_all hcsa,
       	       apps.hz_cust_site_uses_all hcsu,
       	       apps.hz_customer_profiles hcp
	 WHERE hp.party_id = hps.party_id
       	   AND hp.party_id = hca.party_id
       	   AND hcsa.party_site_id = hps.party_site_id
       	   AND hcsu.cust_acct_site_id = hcsa.cust_acct_site_id
       	   AND hca.cust_account_id = hcsa.cust_account_id
       	   AND hcp.cust_account_id = hca.cust_account_id
       	   AND hcp.site_use_id = hcsu.site_use_id
           AND hcp.site_use_id IS NOT NULL
           AND hcp.send_statements = 'N'
           AND hcp.credit_balance_statements = 'Y')
      LOOP
        
        l_customer_profile_rec.cust_account_profile_id := i.cust_account_profile_id;
        l_customer_profile_rec.credit_balance_statements := 'N'; 
        l_obj_version_number := i.object_version_number;
    
        BEGIN
         HZ_CUSTOMER_PROFILE_V2PUB.update_customer_profile (
            p_init_msg_list           => 'T',
            p_customer_profile_rec    => l_customer_profile_rec,
            p_object_version_number   => l_obj_version_number,
            x_return_status           => l_return_status,
            x_msg_count               => l_msg_count,
            x_msg_data                => l_msg_data);
        EXCEPTION
        WHEN OTHERS THEN
           DBMS_OUTPUT.PUT_LINE('Return status is -'||l_return_status||' and error message is '||l_msg_data);
        END;

         IF l_return_status <> 'S'
         THEN
            DBMS_OUTPUT.PUT_LINE ('Issue updating credit balance statement for cust_account_profile_id '
               || i.cust_account_profile_id||'-'|| l_msg_data);
         END IF;
     END LOOP;  
   DBMS_OUTPUT.put_line ('TMS: 20150604-00146 , rows modified: '||sql%rowcount);
   COMMIT;
  END;
/