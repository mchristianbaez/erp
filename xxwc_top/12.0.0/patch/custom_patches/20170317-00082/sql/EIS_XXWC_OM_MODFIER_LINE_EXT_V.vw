------------------------------------------------------------------------------------
/******************************************************************************
   NAME       :  XXEIS.EIS_XXWC_OM_MODFIER_LINE_EXT_V
   REVISIONS:
	Ver         Date            Author             Description
   ---------  ----------    ---------------  ------------------------------------
    1.0       	29-May-2017     Siva 	            Initial Version --TMS#20170317-00082 		  		 
******************************************************************************/
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_OM_MODFIER_LINE_EXT_V (MODIFIER_NAME, LIST_LINE_ID, START_DATE, END_DATE, START_DATE_ACTIVE, END_DATE_ACTIVE, PRODUCT_ATTRIBUTE_DESC, PRODUCT_ATTRIBUTE_VALUE, APPLICATION_METHOD, LINE_VALUE, INCOMPATIBILITY, PRICE_BREAK_TYPE, PRICING_BREAK_VALUE_FROM, PRICING_BREAK_VALUE_TO, MODIFIER_TYPE, CUSTOMER_NAME, CUSTOMER_NUMBER, PARTY_SITE_NAME, PARTY_SITE_NUMBER, SALESREP_NAME, SALESREP_DM, REGION)
AS
  SELECT qlh.name modifier_name,
    qll.list_line_id ,
    TO_CHAR(qll.start_date_active,'DD-Mon-YYYY') start_date ,
    TO_CHAR(qll.end_date_active,'DD-Mon-YYYY') end_date,
    qll.start_date_active,
    qll.end_date_active,
    (
    CASE
      WHEN qpa.product_attribute_context = 'ITEM'
      AND qpa.product_attribute          = 'PRICING_ATTRIBUTE1'
      THEN 'Item Number'
      WHEN qpa.product_attribute_context = 'ITEM'
      AND qpa.product_attribute          = 'PRICING_ATTRIBUTE2'
      THEN 'Item Category'
      WHEN qpa.product_attribute_context = 'ITEM'
      AND qpa.product_attribute          = 'PRICING_ATTRIBUTE27'
      THEN 'Pricing Category'
      ELSE 'Unknown Product Attribute'
    END) product_attribute_desc ,
    concat(chr(39),(
    CASE
      WHEN qpa.product_attribute_context = 'ITEM'
      AND qpa.product_attribute          = 'PRICING_ATTRIBUTE1'
      THEN apps.xxwc_qp_market_price_util_pkg.get_item_number (qpa.product_attr_value, 'ITEM_NUM')
      WHEN qpa.product_attribute_context = 'ITEM'
      AND qpa.product_attribute          = 'PRICING_ATTRIBUTE2'
      THEN apps.xxwc_qp_market_price_util_pkg.get_category_desc (qpa.product_attr_value, 'Inventory Category', 0)
      WHEN qpa.product_attribute_context = 'ITEM'
      AND qpa.product_attribute          = 'PRICING_ATTRIBUTE27'
      THEN apps.xxwc_qp_market_price_util_pkg.get_category_desc (qpa.product_attr_value, 'Pricing Category', 0)
      ELSE 'Unknown Product Attribute Value'
    END)) product_attribute_value ,
    (
    CASE
      WHEN qll.price_by_formula_id = 7030
      THEN 'Cost Plus'
      WHEN qll.price_by_formula_id = 52060
      THEN 'Percent'
      ELSE flv.meaning
    END) application_method ,
    (
    CASE
      WHEN qll.price_by_formula_id = 7030
      THEN (qll.operand*100)
      WHEN qll.price_by_formula_id = 52060
      THEN ((1-qll.operand)*100)
      ELSE qll.operand
    END) line_value ,
    (
    CASE
      WHEN qll.incompatibility_grp_code = 'EXCL'
      THEN 'Exclusive'
      WHEN qll.incompatibility_grp_code = 'LVL 1'
      THEN 'Best Price Wins'
      ELSE 'Best Price Wins'
    END) incompatibility,
    qll.price_break_type_code price_break_type ,
    qpa.pricing_attr_value_from_number pricing_break_value_from,
    qpa.pricing_attr_value_to_number pricing_break_value_to,
    qlh.attribute10 modifier_type,
    xxeis.EIS_XXWC_INV_UTIL_PKG.get_qp_customer_info(qlh.list_header_id,'CUST_NAME') CUSTOMER_NAME,
    xxeis.EIS_XXWC_INV_UTIL_PKG.get_qp_customer_info(qlh.list_header_id,'CUST_NUM') CUSTOMER_NUMBER,
    xxeis.EIS_XXWC_INV_UTIL_PKG.get_qp_customer_info(qlh.list_header_id,'SITE_NAME') PARTY_SITE_NAME,
    xxeis.EIS_XXWC_INV_UTIL_PKG.get_qp_customer_info(qlh.list_header_id,'SITE_NUM') PARTY_SITE_NUMBER ,
    xxeis.EIS_XXWC_INV_UTIL_PKG.get_qp_customer_info(qlh.list_header_id,'SALESREP') SALESREP_NAME,
    xxeis.eis_xxwc_inv_util_pkg.get_qp_customer_info(qlh.list_header_id,'SALESREP_DM') salesrep_dm,
    (SELECT mp.attribute9
    FROM qp_qualifiers qql,
      mtl_parameters mp
    WHERE qql.list_header_id     =qlh.list_header_id
    AND qql.qualifier_context    = 'ORDER'
    AND qql.qualifier_attribute  ='QUALIFIER_ATTRIBUTE18'
    AND qql.qualifier_attr_value = TO_CHAR (mp.organization_id)
    AND rownum                   =1
    ) region
  FROM apps.qp_list_headers qlh ,
    apps.qp_list_lines qll ,
    apps.qp_pricing_attributes qpa ,
    apps.qp_price_formulas_tl qpft ,
    apps.fnd_lookup_values flv
  WHERE qlh.list_type_code                        = 'DLT'
  AND NVL(qlh.start_date_active, TRUNC(sysdate)) <= TRUNC(sysdate)
  AND NVL(qlh.end_date_active, TRUNC(sysdate+1))  > TRUNC(sysdate)
  AND qlh.list_header_id                          = qll.list_header_id
  AND qll.list_line_type_code                     = 'DIS'
  AND NVL(qll.start_date_active, TRUNC(sysdate)) <= TRUNC(sysdate)
  AND NVL(qll.end_date_active, TRUNC(sysdate+1))  > TRUNC(sysdate)
  AND qll.list_header_id                          = qpa.list_header_id
  AND qll.qualification_ind                       = qpa.qualification_ind
  AND qll.pricing_phase_id                        = qpa.pricing_phase_id
  AND qll.list_line_id                            = qpa.list_line_id
  AND nvl(qpa.excluder_flag,'N')                 != 'Y'
  AND qll.price_by_formula_id                     = qpft.price_formula_id(+)
  AND qpft.language(+)                            = userenv('LANG')
  AND flv.lookup_type                             = 'ARITHMETIC_OPERATOR'
  AND flv.view_application_id                     = 661 -- QP
  AND qll.arithmetic_operator                     = flv.lookup_code
  AND flv.security_group_id                       = 0
  AND FLV.LANGUAGE                                = USERENV('LANG')
/
