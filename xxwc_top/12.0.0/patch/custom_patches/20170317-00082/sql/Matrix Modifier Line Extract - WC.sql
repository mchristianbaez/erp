--Report Name            : Matrix Modifier Line Extract - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_OM_MODFIER_LINE_EXT_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_MODFIER_LINE_EXT_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V',660,'','','','','SA059956','XXEIS','Eis Xxwc Om Modfier Line Ext V','EXOMLEV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_OM_MODFIER_LINE_EXT_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_MODFIER_LINE_EXT_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_MODFIER_LINE_EXT_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','SALESREP_DM',660,'SalesRep DM','SALESREP_DM','','','','SA059956','VARCHAR2','','','Salesrep Dm','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','SALESREP_NAME',660,'Master Account SalesRep (or Ship To Account SalesRep if Ship To Account Name is not NULL)','SALESREP_NAME','','','','SA059956','VARCHAR2','','','Salesrep Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','PARTY_SITE_NUMBER',660,'Ship To Account Number','PARTY_SITE_NUMBER','','','','SA059956','VARCHAR2','','','Party Site Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','PARTY_SITE_NAME',660,'Ship To Account Name','PARTY_SITE_NAME','','','','SA059956','VARCHAR2','','','Party Site Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','CUSTOMER_NUMBER',660,'Master Account Number','CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Customer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','CUSTOMER_NAME',660,'Master Account Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','MODIFIER_TYPE',660,'Modifier Type','MODIFIER_TYPE','','','','SA059956','VARCHAR2','','','Modifier Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','PRICING_BREAK_VALUE_TO',660,'Pricing Break Value To','PRICING_BREAK_VALUE_TO','','','','SA059956','NUMBER','','','Pricing Break Value To','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','PRICING_BREAK_VALUE_FROM',660,'Pricing Break Value From','PRICING_BREAK_VALUE_FROM','','','','SA059956','NUMBER','','','Pricing Break Value From','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','PRICE_BREAK_TYPE',660,'Price Break Type','PRICE_BREAK_TYPE','','','','SA059956','VARCHAR2','','','Price Break Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','INCOMPATIBILITY',660,'Incompatibility','INCOMPATIBILITY','','','','SA059956','VARCHAR2','','','Incompatibility','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','LINE_VALUE',660,'Line Value','LINE_VALUE','','','','SA059956','NUMBER','','','Line Value','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','APPLICATION_METHOD',660,'Application Method','APPLICATION_METHOD','','','','SA059956','VARCHAR2','','','Application Method','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','PRODUCT_ATTRIBUTE_VALUE',660,'Product Attribute Value','PRODUCT_ATTRIBUTE_VALUE','','','','SA059956','VARCHAR2','','','Product Attribute Value','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','PRODUCT_ATTRIBUTE_DESC',660,'Product Attribute Desc','PRODUCT_ATTRIBUTE_DESC','','','','SA059956','VARCHAR2','','','Product Attribute Desc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','LIST_LINE_ID',660,'List Line Id','LIST_LINE_ID','','','','SA059956','NUMBER','','','List Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','MODIFIER_NAME',660,'Modifier Name','MODIFIER_NAME','','','','SA059956','VARCHAR2','','','Modifier Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','REGION',660,'Region','REGION','','','','SA059956','VARCHAR2','','','Region','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','END_DATE',660,'End Date','END_DATE','','','','SA059956','VARCHAR2','','','End Date','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','END_DATE_ACTIVE',660,'End Date Active','END_DATE_ACTIVE','','','','SA059956','DATE','','','End Date Active','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','START_DATE',660,'Start Date','START_DATE','','','','SA059956','VARCHAR2','','','Start Date','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_MODFIER_LINE_EXT_V','START_DATE_ACTIVE',660,'Start Date Active','START_DATE_ACTIVE','','','','SA059956','DATE','','','Start Date Active','','','','');
--Inserting Object Components for EIS_XXWC_OM_MODFIER_LINE_EXT_V
--Inserting Object Component Joins for EIS_XXWC_OM_MODFIER_LINE_EXT_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Matrix Modifier Line Extract - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Matrix Modifier Line Extract - WC
xxeis.eis_rsc_ins.lov( 660,'select  RS.Name,SALESREP_ID  from  RA_SALESREPS RS
WHERE  RS.NAME is not null','','OM SALES REP','This gives the sales representative name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct attribute10 Modifier_type from qp_list_headers_vl','','XXWC Modifier Type','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT DISTINCT mp.attribute9 region
FROM mtl_parameters mp
WHERE mp.attribute9 IS NOT NULL
ORDER BY 1','','EIS XXWC Region LOV','This LOV Lists all region details.','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT distinct ppf.full_name district_manager
FROM 
  APPS.RA_SALESREPS RASA,
  PER_ALL_ASSIGNMENTS_F PAAF,
  Per_All_People_F Ppf
WHERE  RASA.PERSON_ID           = PAAF.PERSON_ID
AND PAAF.PRIMARY_FLAG (+)  =''Y''
AND paaf.Assignment_Type(+)=''E''
AND PAAF.SUPERVISOR_ID       = PPF.PERSON_ID
order by 1','','XXWC Salesrep District Mgr LOV','','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Matrix Modifier Line Extract - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Matrix Modifier Line Extract - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Matrix Modifier Line Extract - WC' );
--Inserting Report - Matrix Modifier Line Extract - WC
xxeis.eis_rsc_ins.r( 660,'Matrix Modifier Line Extract - WC','','Extract of matrix discounts by customer & salesrep','','','','SA059956','EIS_XXWC_OM_MODFIER_LINE_EXT_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - Matrix Modifier Line Extract - WC
xxeis.eis_rsc_ins.rc( 'Matrix Modifier Line Extract - WC',660,'APPLICATION_METHOD','Application Method','Application Method','','','default','','13','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Matrix Modifier Line Extract - WC',660,'CUSTOMER_NAME','Customer Name','Master Account Name','','','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Matrix Modifier Line Extract - WC',660,'CUSTOMER_NUMBER','Customer Number','Master Account Number','','','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Matrix Modifier Line Extract - WC',660,'INCOMPATIBILITY','Incompatibility','Incompatibility','','','default','','15','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Matrix Modifier Line Extract - WC',660,'LINE_VALUE','Line Value','Line Value','','~~~','default','','14','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Matrix Modifier Line Extract - WC',660,'MODIFIER_NAME','Modifier Name','Modifier Name','','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Matrix Modifier Line Extract - WC',660,'PARTY_SITE_NAME','Party Site Name','Ship To Account Name','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Matrix Modifier Line Extract - WC',660,'PARTY_SITE_NUMBER','Party Site Number','Ship To Account Number','','','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Matrix Modifier Line Extract - WC',660,'PRICE_BREAK_TYPE','Price Break Type','Price Break Type','','','default','','16','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Matrix Modifier Line Extract - WC',660,'PRICING_BREAK_VALUE_FROM','Pricing Break Value From','Pricing Break Value From','','~~~','default','','17','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Matrix Modifier Line Extract - WC',660,'PRICING_BREAK_VALUE_TO','Pricing Break Value To','Pricing Break Value To','','~~~','default','','18','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Matrix Modifier Line Extract - WC',660,'PRODUCT_ATTRIBUTE_DESC','Product Attribute','Product Attribute Desc','','','default','','11','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Matrix Modifier Line Extract - WC',660,'PRODUCT_ATTRIBUTE_VALUE','Product Value','Product Attribute Value','','','default','','12','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Matrix Modifier Line Extract - WC',660,'SALESREP_DM','DM','SalesRep DM','','','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Matrix Modifier Line Extract - WC',660,'SALESREP_NAME','Salesrep','Master Account SalesRep (or Ship To Account SalesRep if Ship To Account Name is not NULL)','','','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Matrix Modifier Line Extract - WC',660,'LIST_LINE_ID','List Line Id','List Line Id','','~~~','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Matrix Modifier Line Extract - WC',660,'END_DATE','End Date','End Date','','','','','10','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Matrix Modifier Line Extract - WC',660,'START_DATE','Start Date','Start Date','','','','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','US','');
--Inserting Report Parameters - Matrix Modifier Line Extract - WC
xxeis.eis_rsc_ins.rp( 'Matrix Modifier Line Extract - WC',660,'Modifier Type','Modifier Type','MODIFIER_TYPE','IN','XXWC Modifier Type','''Segmented Price''','VARCHAR2','Y','Y','4','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Matrix Modifier Line Extract - WC',660,'Distric Mgr','Salesrep Dm','SALESREP_DM','IN','XXWC Salesrep District Mgr LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Matrix Modifier Line Extract - WC',660,'Salesrep','Salesrep Name','SALESREP_NAME','IN','OM SALES REP','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Matrix Modifier Line Extract - WC',660,'Region','Region','REGION','IN','EIS XXWC Region LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','US','');
--Inserting Dependent Parameters - Matrix Modifier Line Extract - WC
--Inserting Report Conditions - Matrix Modifier Line Extract - WC
xxeis.eis_rsc_ins.rcnh( 'Matrix Modifier Line Extract - WC',660,'EXOMLEV.MODIFIER_TYPE IN Modifier Type','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','MODIFIER_TYPE','','Modifier Type','','','','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','','','IN','Y','Y','','','','','1',660,'Matrix Modifier Line Extract - WC','EXOMLEV.MODIFIER_TYPE IN Modifier Type');
xxeis.eis_rsc_ins.rcnh( 'Matrix Modifier Line Extract - WC',660,'EXOMLEV.SALESREP_DM IN Salesrep Dm','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SALESREP_DM','','Distric Mgr','','','','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','','','IN','Y','Y','','','','','1',660,'Matrix Modifier Line Extract - WC','EXOMLEV.SALESREP_DM IN Salesrep Dm');
xxeis.eis_rsc_ins.rcnh( 'Matrix Modifier Line Extract - WC',660,'EXOMLEV.SALESREP_NAME IN Salesrep Name','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SALESREP_NAME','','Salesrep','','','','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','','','IN','Y','Y','','','','','1',660,'Matrix Modifier Line Extract - WC','EXOMLEV.SALESREP_NAME IN Salesrep Name');
xxeis.eis_rsc_ins.rcnh( 'Matrix Modifier Line Extract - WC',660,'EXOMLEV.REGION IN Region','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','REGION','','Region','','','','','EIS_XXWC_OM_MODFIER_LINE_EXT_V','','','','','','IN','Y','Y','','','','','1',660,'Matrix Modifier Line Extract - WC','EXOMLEV.REGION IN Region');
--Inserting Report Sorts - Matrix Modifier Line Extract - WC
--Inserting Report Triggers - Matrix Modifier Line Extract - WC
--inserting report templates - Matrix Modifier Line Extract - WC
--Inserting Report Portals - Matrix Modifier Line Extract - WC
--inserting report dashboards - Matrix Modifier Line Extract - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Matrix Modifier Line Extract - WC','660','EIS_XXWC_OM_MODFIER_LINE_EXT_V','EIS_XXWC_OM_MODFIER_LINE_EXT_V','N','');
--inserting report security - Matrix Modifier Line Extract - WC
xxeis.eis_rsc_ins.rsec( 'Matrix Modifier Line Extract - WC','661','','QP_SSA_PRICING_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Matrix Modifier Line Extract - WC','661','','QP_PRICING_MANAGER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Matrix Modifier Line Extract - WC','661','','QP_SSA_PRICING_ADMINISTRATOR',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Matrix Modifier Line Extract - WC','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Matrix Modifier Line Extract - WC','661','','XXWC_PRICING_MANAGER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Matrix Modifier Line Extract - WC','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Matrix Modifier Line Extract - WC','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Matrix Modifier Line Extract - WC','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Matrix Modifier Line Extract - WC','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'SA059956','','','');
--Inserting Report Pivots - Matrix Modifier Line Extract - WC
--Inserting Report   Version details- Matrix Modifier Line Extract - WC
xxeis.eis_rsc_ins.rv( 'Matrix Modifier Line Extract - WC','','Matrix Modifier Line Extract - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
