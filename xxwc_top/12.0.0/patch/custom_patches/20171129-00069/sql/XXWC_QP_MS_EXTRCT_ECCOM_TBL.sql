/*******************************************************************************
  * Table:   XXWC_QP_MS_EXTRCT_ECCOM_TBL
  * Description: Data will load into this table through external table 
                 (xxwc.xxwc_qp_ms_ext_eccom), this table will be used in 
				 XXWC_QP_MS_EXTRCT_ECCOM_TBL package to generate the price lists
				 for micro sites.  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     11/21/2017    Pattabhi Avula  TMS#20171129-00069 Initial Version
  
********************************************************************************/
CREATE TABLE XXWC.XXWC_QP_MS_EXTRCT_ECCOM_TBL(
account_number            VARCHAR2(30),
primary_branch            VARCHAR2(5),
product_sku               VARCHAR2(40),
INVENTORY_ITEM_ID         NUMBER,
description               VARCHAR2(240),
start_date_active         VARCHAR2(30),
end_date_active           VARCHAR2(30),
pricing_attr_value_from   VARCHAR2(240), 
pricing_attr_value_to     VARCHAR2(240),
selling_price             NUMBER(10,2),
list_line_id              NUMBER,
modifier                  VARCHAR2(240), 
modifier_type             VARCHAR2(200),
Price_list_type           VARCHAR2(10),
name                      VARCHAR2(240),
product_precedence        NUMBER,
currency_code             VARCHAR2(3),
created_by                NUMBER,
creation_date             DATE DEFAULT SYSDATE,
last_update_date          DATE DEFAULT SYSDATE,
last_updated_by           NUMBER,
last_update_login         NUMBER,
status_flag               VARCHAR2(1) DEFAULT 'N',
error_message             VARCHAR2(500))
/
CREATE OR REPLACE directory XXWC_PRICLITS_EXCT_ECOM_MS_DIR as '/xx_iface/ebsprd/outbound/wcs/microsites'
/