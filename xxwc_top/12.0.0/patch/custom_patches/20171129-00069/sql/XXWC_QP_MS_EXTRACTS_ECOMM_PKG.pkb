create or replace 
PACKAGE BODY      XXWC_QP_MS_EXTRACTS_ECOMM_PKG
AS
/*******************************************************************************
  * Procedure:   Price_list
  * Description: This procedure to fetch the microsites price lists
                 for loaded customers in XXWC.xxwc_qp_ms_extrct_eccom_tbl table
                 
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     11/21/2017    Pattabhi Avula  TMS#20171129-00069 Initial Version
  
********************************************************************************/

   xxwc_error              EXCEPTION;

   -- global error handling variables
   g_err_callfrom          VARCHAR2 (75) := 'XXWC_QP_MS_EXTRACTS_ECOMM_PKG.main';
   g_err_callpoint         VARCHAR2 (75) := 'START';
   g_distro_list           VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   g_message               VARCHAR2 (2000);

   -- global file name variables
   l_directory             VARCHAR2 (80) := 'XXWC_PRICLITS_EXCT_ECOM_MS_DIR';
   l_pll_outfile           UTL_FILE.file_type;
   l_pll_file_name         VARCHAR2 (80) := 'ms_outbound_customerprice.csv';  

PROCEDURE price_list(retcode out VARCHAR2, errbuff out VARCHAR2)
 IS
 
-- Local Variables
 p_line_tbl                 qp_preq_grp.line_tbl_type;
   p_qual_tbl                 qp_preq_grp.qual_tbl_type;
   p_line_attr_tbl            qp_preq_grp.line_attr_tbl_type;
   p_line_detail_tbl          qp_preq_grp.line_detail_tbl_type;
   p_line_detail_qual_tbl     qp_preq_grp.line_detail_qual_tbl_type;
   p_line_detail_attr_tbl     qp_preq_grp.line_detail_attr_tbl_type;
   p_related_lines_tbl        qp_preq_grp.related_lines_tbl_type;
   p_control_rec              qp_preq_grp.control_record_type;
   x_line_tbl                 qp_preq_grp.line_tbl_type;
   x_line_qual                qp_preq_grp.qual_tbl_type;
   x_line_attr_tbl            qp_preq_grp.line_attr_tbl_type;
   x_line_detail_tbl          qp_preq_grp.line_detail_tbl_type;
   x_line_detail_qual_tbl     qp_preq_grp.line_detail_qual_tbl_type;
   x_line_detail_attr_tbl     qp_preq_grp.line_detail_attr_tbl_type;
   x_related_lines_tbl        qp_preq_grp.related_lines_tbl_type;
   x_return_status            VARCHAR2 (240);
   x_return_status_text       VARCHAR2 (240);
   qual_rec                   qp_preq_grp.qual_rec_type;
   line_attr_rec              qp_preq_grp.line_attr_rec_type;
   line_rec                   qp_preq_grp.line_rec_type;
   detail_rec                 qp_preq_grp.line_detail_rec_type;
   ldet_rec                   qp_preq_grp.line_detail_rec_type;
   rltd_rec                   qp_preq_grp.related_lines_rec_type;
   l_pricing_contexts_tbl     qp_attr_mapping_pub.contexts_result_tbl_type;
   l_qualifier_contexts_tbl   qp_attr_mapping_pub.contexts_result_tbl_type;
   v_line_tbl_cnt             INTEGER;

   i                          BINARY_INTEGER;
   j                          BINARY_INTEGER;
   k                          BINARY_INTEGER;
   
   l_version                  VARCHAR2 (240);
   l_file_val                 VARCHAR2 (60);
   l_modifier_name            VARCHAR2 (240);
   l_list_header_id           NUMBER;
   l_list_line_id             NUMBER;
   l_incomp_code              VARCHAR2 (30);
   l_modifier_type            VARCHAR2 (30);
   l_line_modifier_type       VARCHAR2 (30);
   l_item_category            NUMBER;
   l_gm_selling_price         NUMBER(10,2);
   l_message_level            NUMBER;
   l_currency_code            VARCHAR2 (3);  
   l_org_id                   NUMBER := fnd_profile.value('ORG_ID');
   l_list_line_type_code      VARCHAR2 (30); 
   
   l_rec_cntr                 NUMBER;
   l_line_attr_tbl_cntr       NUMBER;
   l_qual_tbl_cntr            NUMBER;
   l_inventory_item_id        NUMBER;
   l_cust_acct_id             NUMBER;
   l_organization_id          NUMBER;
   l_item_id                  NUMBER;
   l_primary_uom_code         VARCHAR2(3);
   l_description              VARCHAR2(240);
   l_start_date_active        VARCHAR2(30);
   l_end_date_active          VARCHAR2(30);
   l_PRICING_ATTR_VALUE_FROM  VARCHAR2(240);
   l_PRICING_ATTR_VALUE_TO    VARCHAR2(240);
   l_product_precedence       NUMBER;
   l_name                     VARCHAR2(240);
   l_error_message            VARCHAR2(500);
   l_err_status               VARCHAR2(5);
   l_mod_type                 VARCHAR2(10);
   l_exception                EXCEPTION;
   
   CURSOR cur_cust_inv_dtls 
    IS
     SELECT account_number,
            primary_branch,
            product_sku
       FROM xxwc.xxwc_qp_ms_extrct_eccom_tbl
      WHERE status_flag = 'N'
      GROUP BY account_number,
               primary_branch,
               product_sku;
BEGIN
  -- Truncating the table before loading the data
  Execute Immediate 'truncate table  xxwc.xxwc_qp_ms_extrct_eccom_tbl';
  l_rec_cntr           := 0;
  l_line_attr_tbl_cntr := 0;
  l_qual_tbl_cntr      := 0;
  
  -- Inserting the file data into custom table from external table
  fnd_file.put_line (fnd_file.LOG,'Before data loading');
  
  BEGIN
  fnd_file.put_line (fnd_file.LOG,'Inside the insert statement');
  insert into  xxwc.xxwc_qp_ms_extrct_eccom_tbl
    (account_number,primary_branch,product_sku)
      select * from xxwc.xxwc_qp_ms_extrnl_eccom_tbl;
  EXCEPTION
  WHEN OTHERS THEN
  fnd_file.put_line (fnd_file.LOG,'Exception in insert');
  g_message:=('Error while data inserting into table, error details are :'||sqlerrm);
  END;
  COMMIT;
  
-- Loop starting to fetch customer_id,organization_id and Item_id
fnd_file.put_line (fnd_file.LOG,'before loop');
  FOR rec_lsts IN cur_cust_inv_dtls
   LOOP
   BEGIN
fnd_file.put_line (fnd_file.LOG,'Inside loop');
    l_rec_cntr           := l_rec_cntr + 1;
    l_line_attr_tbl_cntr := l_line_attr_tbl_cntr + 1;
    l_qual_tbl_cntr      := l_qual_tbl_cntr + 1; 
    l_gm_selling_price   := NULL;
    l_item_category      := NULL;
    
    -- Fetching cust_account_id
    
    fnd_file.put_line (fnd_file.LOG,'customer acct number'||rec_lsts.account_number);

      BEGIN
       SELECT  cust_account_id
         INTO  l_cust_acct_id
         FROM  apps.hz_cust_accounts
        where  account_number = rec_lsts.account_number;
      EXCEPTION
         WHEN OTHERS
         THEN
          l_err_status:='E';
          l_error_message:='Customer not exists in oracle '||rec_lsts.account_number;
          RAISE l_exception;
      END;     
      -- Fetching organization_id

      BEGIN
       SELECT organization_id
         INTO l_organization_id
         FROM apps.org_organization_definitions
        WHERE organization_code = rec_lsts.primary_branch;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_err_status:='E';
            l_error_message:='Branch not exists in oracle '||rec_lsts.primary_branch;
          RAISE l_exception;
      END;

      -- Fetching inventory_item_id and primary_uom_code
      BEGIN   
       SELECT inventory_item_id,primary_uom_code,DECODE (INSTR (description, '"'),
                        0, description,
                        '"' || REPLACE (description, '"', '""') || '"') description
         INTO l_item_id, l_primary_uom_code,l_description
         FROM apps.mtl_system_items_b
        WHERE segment1 = rec_lsts.product_sku
          AND organization_id = l_organization_id;
      EXCEPTION
         WHEN OTHERS
         THEN
             l_err_status:='E';
             l_error_message:='Product_sku not exists in oracle '||rec_lsts.product_sku;
          RAISE l_exception;
      END;

     -- Fetching currency Code
     SELECT gl.currency_code
       INTO l_currency_code
       FROM hr_operating_units hou, gl_ledgers gl
      WHERE hou.organization_id = l_org_id
        AND gl.ledger_id = hou.set_of_books_id;

      BEGIN
         SELECT mic.category_id
           INTO l_item_category
           FROM mtl_item_categories_v mic
          WHERE mic.category_set_name = 'Inventory Category'
            AND mic.inventory_item_id = l_item_id
            AND mic.organization_id = l_organization_id
            AND ROWNUM = 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_item_category := NULL;
      END;

     --fnd_message.debug('2');
      qp_attr_mapping_pub.build_contexts (
         p_request_type_code           => 'ONT',
         p_pricing_type                => 'L',
         x_price_contexts_result_tbl   => l_pricing_contexts_tbl,
         x_qual_contexts_result_tbl    => l_qualifier_contexts_tbl);
         

      ---- Control Record
      p_control_rec.pricing_event := 'LINE';             -- 'BATCH';
      p_control_rec.calculate_flag := 'Y';               --QP_PREQ_GRP.G_SEARCH_N_CALCULATE;
      p_control_rec.simulation_flag := 'Y';
      p_control_rec.rounding_flag := 'Q';
      p_control_rec.manual_discount_flag := 'Y';
      p_control_rec.request_type_code := 'ONT';
      p_control_rec.temp_table_insert_flag := 'Y';


      ---- Line Records ---------
      line_rec.request_type_code := 'ONT';
      line_rec.line_id := -1;                              -- Order Line Id. This can be any thing for this script
      line_rec.line_index := l_rec_cntr;                   -- Request Line Index
      line_rec.line_type_code := 'LINE';                   -- LINE or ORDER(Summary Line)
      line_rec.pricing_effective_date := SYSDATE;          -- Pricing as of what date ?
      line_rec.active_date_first := SYSDATE;               -- Can be Ordered Date or Ship Date
      line_rec.active_date_second := SYSDATE;              -- Can be Ordered Date or Ship Date
      line_rec.active_date_first_type := 'NO TYPE';        -- ORD/SHIP
      line_rec.active_date_second_type := 'NO TYPE';       -- ORD/SHIP
      line_rec.line_quantity := 1;                         -- Ordered Quantity

       line_rec.line_uom_code := l_primary_uom_code;
      -- 20160307-00117 < End

      line_rec.currency_code := l_currency_code; 

      line_rec.price_flag := 'Y';                          -- Price Flag can have 'Y' , 'N'(No pricing) , 'P'(Phase)
      p_line_tbl (l_rec_cntr) := line_rec;

      ---- Line Attribute Record
      line_attr_rec.line_index := l_rec_cntr;
      line_attr_rec.pricing_context := 'ITEM';
      line_attr_rec.pricing_attribute := 'PRICING_ATTRIBUTE1';
      line_attr_rec.pricing_attr_value_from := TO_CHAR (l_item_id); -- INVENTORY ITEM ID
      line_attr_rec.validated_flag := 'N';
      p_line_attr_tbl (l_line_attr_tbl_cntr) := line_attr_rec;

      IF l_item_category IS NOT NULL
      THEN

         l_line_attr_tbl_cntr     := l_line_attr_tbl_cntr + 1;
         line_attr_rec.line_index := l_rec_cntr;
         line_attr_rec.pricing_context := 'ITEM';                           --
         line_attr_rec.pricing_attribute := 'PRICING_ATTRIBUTE2';
         line_attr_rec.pricing_attr_value_from := l_item_category; -- Category ID
         line_attr_rec.validated_flag := 'N';
         p_line_attr_tbl (l_line_attr_tbl_cntr) := line_attr_rec;
      END IF;

      qual_rec.line_index := l_rec_cntr;
      qual_rec.qualifier_context := 'ORDER';
      qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE18';
      qual_rec.qualifier_attr_value_from := l_organization_id;         -- SHIP_FROM_ORG_ID;
      qual_rec.comparison_operator_code := '=';
      qual_rec.validated_flag := 'Y';
      p_qual_tbl (l_qual_tbl_cntr) := qual_rec;

      IF l_cust_acct_id is not null then
      
      l_qual_tbl_cntr := l_qual_tbl_cntr + 1;
      qual_rec.line_index := l_rec_cntr;
      qual_rec.qualifier_context := 'CUSTOMER';
      qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE32';
      qual_rec.qualifier_attr_value_from := l_cust_acct_id;                         -- CUSTOMER ID;
      qual_rec.comparison_operator_code := '=';
      qual_rec.validated_flag := 'Y';
      p_qual_tbl (l_qual_tbl_cntr) := qual_rec;
      END IF;

      l_qual_tbl_cntr := l_qual_tbl_cntr + 1;
      
      qual_rec.line_index := l_rec_cntr;
      qual_rec.qualifier_context := 'ITEM_NUMBER';
      qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE31';
      qual_rec.qualifier_attr_value_from := TO_CHAR (l_item_id);       -- ItemId
      qual_rec.comparison_operator_code := 'NOT=';
      qual_rec.validated_flag := 'Y';
      p_qual_tbl (l_qual_tbl_cntr) := qual_rec;
    


    qp_preq_pub.price_request (p_line_tbl,
                                 p_qual_tbl,
                                 p_line_attr_tbl,
                                 p_line_detail_tbl,
                                 p_line_detail_qual_tbl,
                                 p_line_detail_attr_tbl,
                                 p_related_lines_tbl,
                                 p_control_rec,
                                 x_line_tbl,
                                 x_line_qual,
                                 x_line_attr_tbl,
                                 x_line_detail_tbl,
                                 x_line_detail_qual_tbl,
                                 x_line_detail_attr_tbl,
                                 x_related_lines_tbl,
                                 x_return_status,
                                 x_return_status_text);
                                 
    fnd_file.put_line (fnd_file.LOG,'x_return_status '||x_return_status);
    fnd_file.put_line (fnd_file.LOG,'x_return_status_text '||x_return_status_text);
    fnd_file.put_line (fnd_file.LOG,'Customer Number '||rec_lsts.account_number);
    fnd_file.put_line (fnd_file.LOG,'Organization Code '||rec_lsts.primary_branch);
    fnd_file.put_line (fnd_file.LOG,'Product '||rec_lsts.product_sku);


    l_err_status:=x_return_status;
    l_error_message:=x_return_status_text;
    i := x_line_detail_tbl.FIRST;
    
    IF l_err_status = 'E' THEN
      RAISE l_exception;
    END IF;
        
      IF i IS NOT NULL 
       THEN
         LOOP            
            --fnd_message.debug('7');
            IF x_line_detail_tbl (i).automatic_flag = 'Y'
            THEN
               l_modifier_name      := NULL;
               l_list_header_id     := NULL;
               l_list_line_id       := NULL;
               l_incomp_code        := NULL;
               l_modifier_type      := NULL;
               l_list_line_type_code:= NULL;
               l_line_modifier_type := NULL;

               BEGIN
                  SELECT qlh.name, qlh.attribute10,ffv.description
                    INTO l_modifier_name, l_modifier_type, l_mod_type
                    FROM qp_list_headers_vl qlh,
                         fnd_flex_values_vl ffv
                   WHERE ffv.flex_value = qlh.attribute10
                     AND ffv.flex_value_set_id = 1015252
                     AND qlh.list_header_id = x_line_detail_tbl (i).list_header_id;
                EXCEPTION
                WHEN OTHERS THEN
                dbms_output.put_line('Error is :'||SQLERRM);
                END;
                  fnd_file.put_line (fnd_file.LOG,'l_modifier_name '||l_modifier_name);
                  fnd_file.put_line (fnd_file.LOG,'l_modifier_type '||l_modifier_type);
                  --fnd_message.debug('7');
                  l_list_header_id := x_line_detail_tbl (i).list_header_id;
                  l_list_line_id := x_line_detail_tbl (i).list_line_id;
                  
                  fnd_file.put_line (fnd_file.LOG,'l_list_header_id '||l_list_header_id);
                  
                  fnd_file.put_line (fnd_file.LOG,'l_list_line_id '||l_list_line_id);

                  --fnd_message.debug(l_list_line_id);
                  IF l_list_line_id IS NOT NULL THEN
                     SELECT incompatibility_grp_code,
                            list_line_type_code,
                            attribute5
                       INTO l_incomp_code,
                            l_list_line_type_code,
                            l_line_modifier_type
                       FROM qp_list_lines
                      WHERE list_line_id = l_list_line_id;

                     IF l_modifier_type = 'Contract Pricing' THEN
                        l_modifier_type := NVL (l_line_modifier_type, l_modifier_type);
                     END IF;

                     IF l_modifier_type IS NOT NULL THEN
                        BEGIN
                           SELECT description
                             INTO l_modifier_Type
                             FROM fnd_flex_values_vl
                            WHERE flex_value_set_id = 1015252
                              AND flex_value = l_modifier_Type;
                        EXCEPTION
                           WHEN OTHERS THEN
                              NULL;
                        END;
                     END IF;

                     IF l_list_line_type_code = 'PLL' THEN
                        l_incomp_code := 'PLL';
                     END IF;
                   END IF;             
         -- Getting new selling price

                  l_gm_selling_price := NULL;
                  j := x_line_tbl.FIRST;
                  IF j IS NOT NULL THEN
                    LOOP
                      IF x_line_tbl (j).LINE_INDEX = x_line_detail_tbl (i).LINE_INDEX THEN
                          l_gm_selling_price := x_line_tbl (j).adjusted_unit_price;                          
                      END IF; 
                      EXIT WHEN l_gm_selling_price IS NOT NULL;
                      --EXIT WHEN j = x_line_tbl.LAST;
                      j          := x_line_tbl.NEXT (j);
                    END LOOP;
                  END IF;
                       
            IF l_gm_selling_price = 0 THEN
              l_gm_selling_price := NULL;
            END IF;
            
            BEGIN
              SELECT TO_CHAR (
                       DECODE (
                          qll.start_date_active,
                          NULL, qlh.start_date_active,
                        DECODE (
                         qlh.start_date_active,
                         NULL, qll.start_date_active,
                         GREATEST (qll.start_date_active,
                                   qlh.start_date_active))),
                   'YYYY-MM-DD HH24:MI:SS')
                   start_date_active,
                    TO_CHAR (
                       DECODE (
                          qll.end_date_active,
                          NULL, qlh.end_date_active,
                          DECODE (
                             qlh.end_date_active,
                             NULL, qll.end_date_active,
                             LEAST (qll.end_date_active, qlh.end_date_active))),
                       'YYYY-MM-DD HH24:MI:SS')
                       end_date_active, 
                    qll.pricing_attr_value_from,            -- price break qty low
                    qll.pricing_attr_value_to,             -- price break qty high
                    qll.product_precedence,
                    qlh.name
              INTO l_start_date_active,
                   l_end_date_active, 
                   l_PRICING_ATTR_VALUE_FROM, 
                   l_PRICING_ATTR_VALUE_TO,
                   l_product_precedence,
                   l_name
              FROM qp_secu_list_headers_vl qlh,
                   qp_modifier_summary_v qll
             WHERE qlh.LIST_HEADER_ID =qll.LIST_HEADER_ID
               AND qll.LIST_HEADER_ID=l_list_header_id
               AND qll.LIST_LINE_ID =l_list_line_id;
              EXCEPTION
               WHEN OTHERS THEN
                   l_start_date_active      := NULL;
                   l_end_date_active        := NULL;
                   l_PRICING_ATTR_VALUE_FROM:= NULL; 
                   l_PRICING_ATTR_VALUE_TO  := NULL;
              END;
             
             UPDATE XXWC.xxwc_qp_ms_extrct_eccom_tbl
               SET selling_price            = l_gm_selling_price
                 , modifier                 = l_modifier_name
                 , modifier_type            = l_modifier_type
                 , price_list_type          = l_mod_type
                 , description              = l_description      
                 , start_date_active        = l_start_date_active   
                 , end_date_active          = l_end_date_active  
                 , PRICING_ATTR_VALUE_FROM  = l_PRICING_ATTR_VALUE_FROM
                 , PRICING_ATTR_VALUE_TO    = l_PRICING_ATTR_VALUE_TO
                 , name                     = l_name
                 , currency_code            = l_currency_code
                 , list_line_id             = l_list_line_id                 
                 , STATUS_FLAG              = l_err_status
                 , error_message            = l_error_message                  
                 , inventory_item_id        = l_item_id
             WHERE account_number           = rec_lsts.account_number
               AND PRIMARY_BRANCH           = rec_lsts.PRIMARY_BRANCH
               AND PRODUCT_SKU              = rec_lsts.PRODUCT_SKU;

            END IF; -- IF x_line_detail_tbl (i).automatic_flag = 'Y'

            EXIT WHEN i = x_line_detail_tbl.LAST;
            i := x_line_detail_tbl.NEXT (i);
        -- end if;
         END LOOP;
      END IF;
   EXCEPTION
   WHEN l_exception THEN 
   UPDATE XXWC.xxwc_qp_ms_extrct_eccom_tbl
               SET STATUS_FLAG              = l_err_status
                 , error_message            = l_error_message                  
             WHERE account_number = rec_lsts.account_number
               AND PRIMARY_BRANCH = rec_lsts.PRIMARY_BRANCH
               AND PRODUCT_SKU    = rec_lsts.PRODUCT_SKU;

    
    WHEN OTHERS THEN
    l_error_message:=SQLERRM;
     UPDATE XXWC.xxwc_qp_ms_extrct_eccom_tbl
               SET STATUS_FLAG              = 'E'
                 , error_message            = l_error_message                  
             WHERE account_number = rec_lsts.account_number
               AND PRIMARY_BRANCH = rec_lsts.PRIMARY_BRANCH
               AND PRODUCT_SKU    = rec_lsts.PRODUCT_SKU;
    COMMIT;
   END;           
    END LOOP;
    COMMIT;
EXCEPTION
WHEN OTHERS THEN
fnd_file.put_line (fnd_file.LOG,'Error is :' ||sqlerrm);    
END price_list;


PROCEDURE main(retcode OUT VARCHAR2, errbuff OUT VARCHAR2)
IS 

CURSOR cur_qpms_lns    
IS
SELECT ACCOUNT_NUMBER
      ,INVENTORY_ITEM_ID
	  ,PRODUCT_SKU
	  ,SELLING_PRICE
	  ,PRICE_LIST_TYPE
	  ,PRIMARY_BRANCH  
  FROM xxwc.xxwc_qp_ms_extrct_eccom_tbl
 WHERE status_flag = 'S'
 GROUP BY ACCOUNT_NUMBER
         ,INVENTORY_ITEM_ID
	     ,PRODUCT_SKU
	     ,SELLING_PRICE
	     ,PRICE_LIST_TYPE
	     ,PRIMARY_BRANCH;
    
l_errbuff  VARCHAR2(2000);
l_retcode  VARCHAR2(100);

BEGIN

fnd_file.put_line (fnd_file.LOG,
                         'Executing the price lists procedure ');
  BEGIN
  price_list(l_retcode,l_errbuff);
  EXCEPTION
  WHEN OTHERS THEN
  g_message:=SQLERRM;
  END;

      l_pll_outfile := UTL_FILE.fopen (l_directory, l_pll_file_name, 'w'); -- write new offers interface file

      UTL_FILE.put_line (
         l_pll_outfile,
            'CUSTOMERACCT#|PRODUCTID|PRODUCTSKU|CUSTOMERPRICE|PRICELISTTYPE|BRANCHNUMBER');

  FOR j IN cur_qpms_lns
    LOOP
    
 UTL_FILE.put_line (
               l_pll_outfile,
                  j.ACCOUNT_NUMBER                       -- CUSTOMERACCT#
               || '|'
               ||                                         
                  j.INVENTORY_ITEM_ID                    -- PRODUCTID
               || '|'
               ||                                         
                  j.PRODUCT_SKU                          -- PRODUCT_SKU
               || '|'
               ||                                         
                  j.SELLING_PRICE                        -- CUSTOMERPRICE
               || '|'
               ||                                         
                  j.PRICE_LIST_TYPE                        -- PRICELISTTYPE
			   || '|'
               || j.PRIMARY_BRANCH                       -- BRANCHNUMBER
                  );
        END LOOP;
        UTL_FILE.fclose (l_pll_outfile);

    UPDATE xxwc.xxwc_qp_ms_extrct_eccom_tbl
       SET status_flag = 'X'
     WHERE status_flag = 'S';
    COMMIT;    
 EXCEPTION
 WHEN OTHERS
      THEN
         errbuff := 'Error';
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         IF UTL_FILE.is_open (l_pll_outfile)
         THEN
            UTL_FILE.fclose (l_pll_outfile);
         END IF;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom,
            p_calling             => g_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
   END;
END; -- PACKAGE
/