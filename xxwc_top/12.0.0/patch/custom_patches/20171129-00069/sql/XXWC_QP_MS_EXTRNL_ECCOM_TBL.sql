/*******************************************************************************
  * External Table:   xxwc.XXWC_QP_MS_EXTRNL_ECCOM_TBL
  * Description: Data will load into this table through external file
                 this table will be used in XXWC_QP_MS_EXTRCT_ECCOM package to 
				 generate the price lists for micro sites.  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     11/21/2017    Pattabhi Avula  TMS#20171129-00069 Initial Version
  
********************************************************************************/
CREATE TABLE XXWC.XXWC_QP_MS_EXTRNL_ECCOM_TBL(
account_number     VARCHAR2(30),
organization_code  VARCHAR2(5),
Item_number        VARCHAR2(40)
)
ORGANIZATION EXTERNAL(
TYPE ORACLE_LOADER
DEFAULT DIRECTORY XXWC_QP_MS_EXT_ECOMM_DIR
ACCESS PARAMETERS(
RECORDS DELIMITED BY NEWLINE SKIP 1
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
)
LOCATION('skus.csv')
)
REJECT LIMIT UNLIMITED
/
CREATE OR REPLACE DIRECTORY XXWC_QP_MS_EXT_ECOMM_DIR AS '/xx_iface/ebsprd/inbound/wcs'
/