/*************************************************************************
  $Header TMS_20160512_00054_MV_ROUTINES_ERROR_ALERT_STOP.sql $
  Module Name: TMS_20160512-00054 MV_ROUTINES Data Fix script

  PURPOSE: Data Fix script to stop the error alerts for MV_ROUTINES
           package Errors

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        12-MAY-2016  Pattabhi Avula        TMS#20160512-00054 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160512-00054    , Before Update');

UPDATE xxcus.xxcus_errors_tbl
SET email_flag1='Y'
WHERE called_from='XXWC_MV_ROUTINES_ADD_PKG'
AND calling='DISABLE_PARALLELISM'
AND email_flag1='N';

   DBMS_OUTPUT.put_line (
         'TMS: 20160512-00054  EMAIL_FLAG1 flag changed record count: '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160512-00054     , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160512-00054 , Errors : ' || SQLERRM);
END;
/