/*
 TMS: 20151021-00033
 Date: 10/21/2015
*/
set serveroutput on size 1000000;
declare
 --
 n_count number :=0;
 v_db_name varchar2(20) :=Null;
 --
     procedure print_log(p_message in varchar2) is
     begin
      if apps.fnd_global.conc_request_id >0 then
       apps.fnd_file.put_line(fnd_file.log, p_message);
      else
       dbms_output.put_line(p_message);
      end if;
     end;
 --
begin
     --
     print_log('');     
     --   
     begin 
            select   name
            into      v_db_name
            from    v$database;
            --
     exception   
      when others then
        v_db_name :='NA'; 
        print_log('When-others-1, error ='||sqlerrm);       
     end;
    --           
     begin 
            select   count(1)
            into      n_count
            from    pn_locations_all
            where 1 =1
                 and org_id =122;
            --
     exception
      when no_data_found then
        n_count :=0;        
      when others then
        n_count :=0; 
        print_log('When-others-2, error ='||sqlerrm);       
     end;
    --
    print_log('Before update, total records with org_id 122 ='||n_count);
    -- 
    if  (n_count >0)  then
      begin 
       update pn_locations_all
              set  org_id =163 --Update all wrong locations org_id of 122 with 163.
       where 1 =1
             and org_id =122; --Pull all locations where we have a wrong org_id of 122
        --
        if (sql%rowcount >0) then
          --
          print_log('Total records updated in pn_locations_all with US org_id 163 ='||sql%rowcount);
          --         
         print_log('Committed changes in '||v_db_name);
         --                 
        end if;
        --
         commit;
         --        
      exception
       when others then
        rollback to start_here;
        print_log('Error in updating pn_locations_all with org_id 163, error ='||sqlerrm);
      end;
    else
     print_log('No opn locations record found with org_id 122.');
    end if;
    --    
exception
 when others then
  rollback to square1;
  print_log('Outer block, message ='||sqlerrm);
end;
/