/*
-- 
-- --------------------------------------------------------------------------------------------------------------------------------
TICKET#                            DATE              AUTHOR                           NOTES  
-- --------------------------------------------------------------------------------------------------------------------------------
TMS 20171013-00238 / ESMS 538724   07/05/2017       BALAGURU SESHADRI                 NEW EBS PAM CUSTOM TABLES
--
*/
--
DROP TABLE XXCUS.XXCUS_PAM_YTD_INCOME_TBL CASCADE CONSTRAINTS; 
--
CREATE TABLE XXCUS.XXCUS_PAM_YTD_INCOME_TBL
(
  ID                 NUMBER,
  MVID               VARCHAR2(30 BYTE),
  LOB_ID             NUMBER,
  BU_ID              NUMBER,
  PLAN_ID            NUMBER,
  PERIOD_TYPE        VARCHAR2(20 BYTE),
  REBATE_TYPE        VARCHAR2(30 BYTE),
  PAM_YEAR               NUMBER,
  SYS_YEAR                 NUMBER,
  ACCRUED_AMT        NUMBER,
  CUST_ID            NUMBER,
  SOURCE             VARCHAR2(20 BYTE),
  EBS_CREATED_BY     NUMBER,
  EBS_CREATION_DATE  DATE,
  REQUEST_ID         NUMBER
)
;
--
CREATE UNIQUE INDEX XXCUS.XXCUS_PAM_YTD_INCOME_TBL_U1 ON XXCUS.XXCUS_PAM_YTD_INCOME_TBL (ID);
--
COMMENT ON TABLE XXCUS.XXCUS_PAM_YTD_INCOME_TBL IS 'TMS 20170202-00054 / ESMS 538724';
--