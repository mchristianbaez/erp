/*
-- 
-- --------------------------------------------------------------------------------------------------------------------------------
TICKET#                            DATE              AUTHOR                           NOTES  
-- --------------------------------------------------------------------------------------------------------------------------------
TMS 20171013-00238 / ESMS 538724   07/05/2017       BALAGURU SESHADRI                 NEW EBS PAM CUSTOM TABLES
--
*/
--
DROP TABLE XXCUS.XXCUS_PAM_VEND_LOV_TBL CASCADE CONSTRAINTS;
--
CREATE TABLE XXCUS.XXCUS_PAM_VEND_LOV_TBL
(
  ID                 NUMBER,
  MVID               VARCHAR2(30 BYTE),
  MV_NAME            VARCHAR2(255 BYTE),
  CUST_ID            NUMBER,
  SOURCE             VARCHAR2(20 BYTE),
  EBS_CREATED_BY     NUMBER,
  EBS_CREATION_DATE  DATE,
  REQUEST_ID         NUMBER
)
;
--
CREATE UNIQUE INDEX XXCUS.XXCUS_PAM_VEND_LOV_TBL_U1 ON XXCUS.XXCUS_PAM_VEND_LOV_TBL (ID);
--
COMMENT ON TABLE XXCUS.XXCUS_PAM_VEND_LOV_TBL IS 'TMS 20170202-00054 / ESMS 538724';
--