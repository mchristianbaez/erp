/*
-- 
-- --------------------------------------------------------------------------------------------------------------------------------
TICKET#                                     DATE             AUTHOR                            NOTES  
-- --------------------------------------------------------------------------------------------------------------------------------
TMS 20171013-00238 / ESMS 538724            07/05/2017       BALAGURU SESHADRI                 NEW EBS PAM CUSTOM TABLES
--
*/
--
DROP TABLE XXCUS.PAM_PROGRAM_LIST_TBL_TMP;
--
CREATE TABLE XXCUS.PAM_PROGRAM_LIST_TBL_TMP
(
  ID                            NUMBER,
  MVID                          VARCHAR2(30 BYTE),
  MV_NAME                       VARCHAR2(150 BYTE),
  AGREEMENT_YR                  VARCHAR2(30 BYTE),
  AGREEMENT_STATUS              VARCHAR2(30 BYTE),
  PROGRAM_NAME                  VARCHAR2(255 BYTE),
  PROGRAM_DESCRIPTION           VARCHAR2(4000 BYTE),
  REBATE_TYPE                   VARCHAR2(30 BYTE),
  AGREEMENT_NAME                VARCHAR2(255 BYTE),
  AGREEMENT_DESCRIPTION         VARCHAR2(4000 BYTE),
  PAYMENT_METHOD                VARCHAR2(30 BYTE),
  PAYMENT_FREQ                  VARCHAR2(30 BYTE),
  AUTO_RENEWAL                  VARCHAR2(1 BYTE),
  UNTIL_YEAR                    VARCHAR2(30 BYTE),
  ACTIVITY_TYPE                 VARCHAR2(30 BYTE),
  AUTOPAY_ENABLED_FLAG          VARCHAR2(1 BYTE),
  GLOBAL_FLAG                   VARCHAR2(1 BYTE),
  BACK_TO_OR_MIN_GUARANTEE_AMT  VARCHAR2(30 BYTE),
  CUST_ID                       NUMBER,
  SOURCE                        VARCHAR2(20 CHAR),
  CONTRACT_NBR                  NVARCHAR2(30),
  PLAN_ID                       NUMBER,
  EBS_CREATED_BY                NUMBER,
  EBS_CREATION_DATE             DATE,
  REQUEST_ID                    NUMBER
)
;
--
COMMENT ON TABLE XXCUS.PAM_PROGRAM_LIST_TBL_TMP IS 'TMS 20171013-00238';
--