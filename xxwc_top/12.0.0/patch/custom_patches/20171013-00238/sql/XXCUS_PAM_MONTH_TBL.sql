/*
-- 
-- --------------------------------------------------------------------------------------------------------------------------------
TICKET#                            DATE              AUTHOR                           NOTES  
-- --------------------------------------------------------------------------------------------------------------------------------
TMS 20171013-00238 / ESMS 538724   07/05/2017       BALAGURU SESHADRI                 NEW EBS PAM CUSTOM TABLES
--
*/
--
DROP TABLE XXCUS.XXCUS_PAM_MONTH_TBL;
--
CREATE TABLE XXCUS.XXCUS_PAM_MONTH_TBL
(
  ID           NUMBER,
  MONTH_ID     NUMBER,
  MONTH_NAME   VARCHAR2(30 BYTE),
  PERIOD_TYPE  VARCHAR2(10 BYTE)
);
--
COMMENT ON TABLE XXCUS.XXCUS_PAM_MONTH_TBL IS 'TMS 20171013-00238';
--