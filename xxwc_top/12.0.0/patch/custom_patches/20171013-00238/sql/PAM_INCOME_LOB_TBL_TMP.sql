/*
-- 
-- --------------------------------------------------------------------------------------------------------------------------------
TICKET#                                     DATE             AUTHOR                            NOTES  
-- --------------------------------------------------------------------------------------------------------------------------------
TMS 20171013-00238 / ESMS 538724            07/05/2017       BALAGURU SESHADRI                 NEW EBS PAM CUSTOM TABLES
--
*/
--
DROP TABLE XXCUS.PAM_INCOME_LOB_TBL_TMP;
--
CREATE TABLE XXCUS.PAM_INCOME_LOB_TBL_TMP
(
  ID                 NUMBER,
  MVID               VARCHAR2(30 BYTE),
  MV_NAME            VARCHAR2(255 BYTE),
  LOB_ID             NUMBER,
  BU_ID              NUMBER,
  PLAN_ID            NUMBER,
  YEAR               NUMBER,
  PERIOD_TYPE        VARCHAR2(20 BYTE),
  REBATE_TYPE        VARCHAR2(30 BYTE),
  MONTH_ID           NUMBER,
  MONTH_NAME         VARCHAR2(20 BYTE),
  AMOUNT             NUMBER,
  AGREEMENT_YEAR     VARCHAR2(30 BYTE),
  SOURCE             VARCHAR2(20 BYTE),
  CUST_ID            NUMBER,
  EBS_CREATED_BY     NUMBER,
  EBS_CREATION_DATE  DATE,
  REQUEST_ID         NUMBER,
  SYS_YEAR           NUMBER
)
;
--
COMMENT ON TABLE XXCUS.PAM_INCOME_LOB_TBL_TMP IS 'TMS 20171013-00238';
--