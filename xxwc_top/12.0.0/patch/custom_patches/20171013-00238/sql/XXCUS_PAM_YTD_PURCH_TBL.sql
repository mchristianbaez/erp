/*
-- 
-- --------------------------------------------------------------------------------------------------------------------------------
TICKET#                            DATE              AUTHOR                           NOTES  
-- --------------------------------------------------------------------------------------------------------------------------------
TMS 20171013-00238 / ESMS 538724   07/05/2017       BALAGURU SESHADRI                 NEW EBS PAM CUSTOM TABLES
--
*/
--
drop table xxcus.xxcus_pam_ytd_purch_tbl cascade constraints;
--
create table xxcus.xxcus_pam_ytd_purch_tbl
(
  id                 number,
  mvid               varchar2(30 byte),
  lob_id             number,
  bu_id              number,
  period_type varchar2(20),    
  pam_year               number,
  sys_year number,  
  total_purchases    number,
  cust_id            number,
  source             varchar2(20 byte),
  ebs_created_by     number,
  ebs_creation_date  date,
  request_id         number
)
;
--
create unique index xxcus.xxcus_pam_ytd_purch_tbl_u1 on xxcus.xxcus_pam_ytd_purch_tbl (id);
--
comment on table xxcus.xxcus_pam_ytd_purch_tbl is 'TMS 20170202-00054 / ESMS 538724';
--