/*
-- 
-- --------------------------------------------------------------------------------------------------------------------------------
TICKET#                            DATE              AUTHOR                           NOTES  
-- --------------------------------------------------------------------------------------------------------------------------------
TMS 20171013-00238 / ESMS 538724   07/05/2017       BALAGURU SESHADRI                 NEW EBS PAM CUSTOM TABLES
--
*/
--
DROP TABLE XXCUS.XXCUS_PAM_PAYMENT_DET_TBL CASCADE CONSTRAINTS;
--
CREATE TABLE XXCUS.XXCUS_PAM_PAYMENT_DET_TBL
(
  ID                    NUMBER,
  CUST_ID               NUMBER,
  MVID                  VARCHAR2(30 BYTE),
  MV_NAME               VARCHAR2(300 BYTE),
  LOB_ID                NUMBER,
  BU_ID                 NUMBER,
  PLAN_ID               NUMBER,
  PERIOD_TYPE           VARCHAR2(20 BYTE),
  REBATE_TYPE           VARCHAR2(20 BYTE),
  PAYMENT_DATE          DATE,
  CREATION_DATE         DATE,
  RECEIPT_TIMEFRAME     VARCHAR2(30 BYTE),
  PAYMENT_AMOUNT        NUMBER,
  CURRENCY_CODE         VARCHAR2(30 BYTE),
  PAYMENT_METHOD        VARCHAR2(30 BYTE),
  RECEIPT_REFERENCE     VARCHAR2(30 BYTE),
  PAYMENT_NUMBER        VARCHAR2(55 BYTE),
  CASH_RECEIPT_ID       NUMBER,
  PAYMENT_SCHEDULE_ID   NUMBER,
  IMAGE_URL             VARCHAR2(255 BYTE),
  AMOUNT_DUE_REMAINING  NUMBER,
  SOURCE                VARCHAR2(20 BYTE),
  GL_DATE               DATE,
  AGREEMENT_YEAR        NVARCHAR2(10),
  AGREEMENT_CODE        NVARCHAR2(30),
  AGREEMENT_NAME        NVARCHAR2(120),
  EBS_CREATED_BY        NUMBER,
  EBS_CREATION_DATE     DATE,
  REQUEST_ID            NUMBER
)
;
--
CREATE UNIQUE INDEX XXCUS.XXCUS_PAM_PAYMENT_DET_TBL_U1 ON XXCUS.XXCUS_PAM_PAYMENT_DET_TBL (ID);
--
COMMENT ON TABLE XXCUS.XXCUS_PAM_PAYMENT_DET_TBL IS 'TMS 20170202-00054 / ESMS 538724';
--