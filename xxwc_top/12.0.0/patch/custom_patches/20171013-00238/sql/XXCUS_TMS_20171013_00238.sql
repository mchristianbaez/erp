/*
   Ticket#                                       Date                 Author                         Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS: 20171013-00238      12/10/2017   Balaguru Seshadri    Recreate APXPRD_LNK pointing to eaapxprd instead of xpeprd
*/
declare
 l_sql varchar2(4000) :=Null;
 l_db_name varchar2(20) :=Null;
begin
  --
  select UPPER(name) into l_db_name from v$database;
  --
  l_sql :='DROP DATABASE LINK APXPRD_LNK';
  --
  execute immediate l_sql;
  --
  dbms_output.put_line('@TMS: 20171013-00238, Dropped existing DB link APXPRD_LNK - Message =>'||sqlerrm);
  --
  l_sql :=Null;
  --
  if l_db_name <>'EBSPRD' then -- Non PROD like EBSDEV OR EBSQA
      --
      l_sql :='CREATE DATABASE LINK APXPRD_LNK
                                              CONNECT TO INTERFACE_ORACLER12 IDENTIFIED BY pa$$w0rd
                                              USING ''(DESCRIPTION =    (ADDRESS = (PROTOCOL = TCP)(HOST = eaapxqa.hdsupply.net)(PORT = 1521))
                                                      (CONNECT_DATA =      (SERVER = DEDICATED)      (SERVICE_NAME = eaapxqa)))''';  
      --
      execute immediate l_sql;
      --
      dbms_output.put_line('@TMS: 20171013-00238, Database :'||l_db_name||'Created DB link APXPRD_LNK - Message =>'||sqlerrm);
      --     
  else -- EBSPRD...
      --
      l_sql :='CREATE DATABASE LINK APXPRD_LNK
                                              CONNECT TO INTERFACE_ORACLER12 IDENTIFIED BY pa$$w0rd
                                              USING ''(DESCRIPTION =    (ADDRESS = (PROTOCOL = TCP)(HOST = eaapxprd.hdsupply.net)(PORT = 1521))
                                                      (CONNECT_DATA =      (SERVER = DEDICATED)      (SERVICE_NAME = eaapxprd)))''';  
      --
      execute immediate l_sql;
      --
      dbms_output.put_line('@TMS: 20171013-00238, Database :'||l_db_name||'Created DB link APXPRD_LNK - Message =>'||sqlerrm);
      --   
  end if;
  -- 
exception
 when others then
  dbms_output.put_line('@TMS: 20171013-00238, Outer Block - Message =>'||sqlerrm);
end;
/