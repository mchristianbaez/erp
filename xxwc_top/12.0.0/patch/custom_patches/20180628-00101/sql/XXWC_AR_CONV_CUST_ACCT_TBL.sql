   /******************************************************************************************************************************************************
        $Header XXWC_AR_CONV_CUST_ACCT_TBL $
        Module Name: XXWC_AR_CONV_CUST_ACCT_TBL.pkb

        PURPOSE:   AHH Customer Conversion

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        06/06/2018  P.Vamshidhar     TMS#20180308-00290 -AH HARRIS Customer Conversion
   ******************************************************************************************************************************************************/

DROP TABLE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
(
  RECORD_ID                 VARCHAR2(240 BYTE),
  SALES_FORCE_ID            VARCHAR2(240 BYTE),
  CUSTOMER_NUMBER           VARCHAR2(240 BYTE),
  CUSTOMER_SITE_NUMBER      VARCHAR2(240 BYTE),
  BUSINESS_NAME             VARCHAR2(240 BYTE),
  BUSINESS_PHONE            VARCHAR2(240 BYTE),
  BUSINESS_FAX              VARCHAR2(240 BYTE),
  EMAIL_ADDRESS             VARCHAR2(240 BYTE),
  OUTSIDE_SALES_REP_ID      VARCHAR2(240 BYTE),
  INSIDE_SALES_REP_ID       VARCHAR2(240 BYTE),
  TYPE_OF_BUSINESS          VARCHAR2(240 BYTE),
  PROFILE_CLASS             VARCHAR2(240 BYTE),
  CREDIT_MANAGER            VARCHAR2(240 BYTE),
  CREDIT_CLASSIFICATION     VARCHAR2(240 BYTE),
  CREDIT_HOLD               VARCHAR2(3 BYTE),
  ACCOUNT_STATUS            VARCHAR2(240 BYTE),
  TERMS                     VARCHAR2(240 BYTE),
  PREDOMINANT_TRADE         VARCHAR2(240 BYTE),
  STATUS                    VARCHAR2(240 BYTE),
  BILLING_ADDRESS1          VARCHAR2(240 BYTE),
  BILLING_ADDRESS2          VARCHAR2(240 BYTE),
  BILLING_CITY              VARCHAR2(240 BYTE),
  BILLING_COUNTY            VARCHAR2(240 BYTE),
  BILLING_STATE             VARCHAR2(240 BYTE),
  BILLING_COUNTRY           VARCHAR2(240 BYTE),
  BILLING_ZIP_CODE          VARCHAR2(240 BYTE),
  SHIPPING_ADDRESS1         VARCHAR2(240 BYTE),
  SHIPPING_ADDRESS2         VARCHAR2(240 BYTE),
  SHIPPING_CITY             VARCHAR2(240 BYTE),
  SHIPPING_COUNTY           VARCHAR2(240 BYTE),
  SHIPPING_STATE            VARCHAR2(240 BYTE),
  TAXING_STATE              VARCHAR2(240 BYTE),
  SHIPPING_COUNTRY          VARCHAR2(240 BYTE),
  SHIPPING_ZIP_CODE         VARCHAR2(240 BYTE),
  CUST_SITE_CLASSIFICATION  VARCHAR2(240 BYTE),
  CREDIT_LIMIT              VARCHAR2(240 BYTE),
  DEFAULT_JOB_CREDIT_LIMIT  VARCHAR2(240 BYTE),
  CREDIT_DECISIONING        VARCHAR2(240 BYTE),
  AP_CONTACT_FIRST_NAME     VARCHAR2(240 BYTE),
  AP_CONTACT_LAST_NAME      VARCHAR2(240 BYTE),
  AP_PHONE                  VARCHAR2(240 BYTE),
  AP_EMAIL                  VARCHAR2(240 BYTE),
  INVOICE_EMAIL_ADDRESS     VARCHAR2(240 BYTE),
  POD_EMAIL_ADDRESS         VARCHAR2(240 BYTE),
  SOA_EMAIL_ADDRESS         VARCHAR2(240 BYTE),
  PURCHASE_ORDER_REQUIRED   VARCHAR2(240 BYTE),
  DUNSNUMBER                VARCHAR2(100 BYTE),
  CREATION_DATE             VARCHAR2(240 BYTE),
  CREATED_BY                VARCHAR2(240 BYTE),
  SEND_STATEMENT            VARCHAR2(10 BYTE),
  CUSTOMER_SOURCE           VARCHAR2(30 BYTE),
  PRINT_PRICES_ON_ORDER     VARCHAR2(150 BYTE),
  JOB_SITE_NAME             VARCHAR2(100 BYTE),
  JOB_DESC                  VARCHAR2(240 BYTE),
  MANDATORY_NOTES           VARCHAR2(100 BYTE),
  MANDATORY_NOTES1          VARCHAR2(240 BYTE),
  MANDATORY_NOTES2          VARCHAR2(240 BYTE),
  MANDATORY_NOTES3          VARCHAR2(240 BYTE),
  MANDATORY_NOTES4          VARCHAR2(240 BYTE),
  MANDATORY_NOTES5          VARCHAR2(240 BYTE),
  REMIT_TO_ADDRESS          VARCHAR2(150 BYTE),
  TAX_EXEMPTION             VARCHAR2(150 BYTE),
  COLLECTION_NOTES          VARCHAR2(240 BYTE),
  SHIP_METHOD               VARCHAR2(100 BYTE),
  TAX_CERTIFICATE           VARCHAR2(100 BYTE),
  NON_TAX_REASON            VARCHAR2(100 BYTE),
  AUTO_APPLY                VARCHAR2(100 BYTE),
  BILL_TO_FLAG              VARCHAR2(1 BYTE),
  SHIP_TO_FLAG              VARCHAR2(1 BYTE),
  PROCESS_FLAG              VARCHAR2(1 BYTE),
  PROCESS_MESSAGE           VARCHAR2(4000 BYTE),
  ORACLE_CUSTOMER_NUMBER    VARCHAR2(40 BYTE),
  REQUEST_ID                VARCHAR2(40 BYTE),
  FSD                       VARCHAR2(30 BYTE)
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AR_CONV_CUST_ACCT_TBL TO EA_APEX;

GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AR_CONV_CUST_ACCT_TBL TO VP038429
/