--******************************************************************************
--*File Name: XXWC_CONV_CUST_DATA.ctl                                          * 
--*Author: P.Vamshidhar                                                        *
--*Creation Date: 21-May-2018 - Vamshi                                         *
--*Data Source: AH Harries                                                     *
--*Description: This is the control file. SQL Loader will                      *
--*           use to load the Customers.                                       *
--* Vamshi  : 24-May-2012 : Added Options Parameter                            *
--*           TMS#20180308-00290 -AH HARRIS Customer Conversion                *
--******************************************************************************
OPTIONS(SKIP=0,DIRECT=FALSE,PARALLEL=TRUE,ROWS=30000,BINDSIZE=20970000)
LOAD DATA
BADFILE 'XXWC_CONV_CUST_DATA.bad'
DISCARDFILE 'XXWC_CONV_CUST_DATA.dsc'
REPLACE INTO TABLE XXWC.XXWC_AR_CONV_CUST_ACCT_TBL
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(RECORD_ID                      "trim(:RECORD_ID)"  
,SALES_FORCE_ID                 "trim(:SALES_FORCE_ID)"  
,CUSTOMER_NUMBER                "trim(:CUSTOMER_NUMBER)"
,CUSTOMER_SITE_NUMBER           "trim(:CUSTOMER_SITE_NUMBER)" 
,BUSINESS_NAME                  "trim(:BUSINESS_NAME)"
,BUSINESS_PHONE                 "trim(:BUSINESS_PHONE)"
,BUSINESS_FAX                   "trim(:BUSINESS_FAX)"
,EMAIL_ADDRESS                  "trim(:EMAIL_ADDRESS)"
,OUTSIDE_SALES_REP_ID           "trim(:OUTSIDE_SALES_REP_ID)"
,INSIDE_SALES_REP_ID            "trim(:INSIDE_SALES_REP_ID)"
,TYPE_OF_BUSINESS               "trim(:TYPE_OF_BUSINESS)"
,PROFILE_CLASS                  "trim(:PROFILE_CLASS)"
,CREDIT_MANAGER                 "trim(:CREDIT_MANAGER)"
,CREDIT_CLASSIFICATION          "trim(:CREDIT_CLASSIFICATION)"
,CREDIT_HOLD                    "trim(:CREDIT_HOLD)"
,ACCOUNT_STATUS                 "trim(:ACCOUNT_STATUS)"
,TERMS                          "trim(:TERMS)" 
,PREDOMINANT_TRADE              "trim(:PREDOMINANT_TRADE)"
,STATUS                         "trim(:STATUS)"
,BILLING_ADDRESS1               "trim(:BILLING_ADDRESS1)"
,BILLING_ADDRESS2               "trim(:BILLING_ADDRESS2)"
,BILLING_CITY                   "trim(:BILLING_CITY)"
,BILLING_COUNTY                 "trim(:BILLING_COUNTY)"
,BILLING_STATE                  "trim(:BILLING_STATE)"
,BILLING_COUNTRY                "trim(:BILLING_COUNTRY)"
,BILLING_ZIP_CODE               "trim(:BILLING_ZIP_CODE)"
,SHIPPING_ADDRESS1              "trim(:SHIPPING_ADDRESS1)"
,SHIPPING_ADDRESS2              "trim(:SHIPPING_ADDRESS2)"
,SHIPPING_CITY                  "trim(:SHIPPING_CITY)"
,SHIPPING_COUNTY                "trim(:SHIPPING_COUNTY)"
,SHIPPING_STATE                 "trim(:SHIPPING_STATE)"
,TAXING_STATE                   "trim(:TAXING_STATE)"
,SHIPPING_COUNTRY               "trim(:SHIPPING_COUNTRY)"
,SHIPPING_ZIP_CODE              "trim(:SHIPPING_ZIP_CODE)"
,CUST_SITE_CLASSIFICATION       "trim(:CUST_SITE_CLASSIFICATION)"
,CREDIT_LIMIT                   "trim(:CREDIT_LIMIT)"
,DEFAULT_JOB_CREDIT_LIMIT       "trim(:DEFAULT_JOB_CREDIT_LIMIT)"
,CREDIT_DECISIONING             "trim(:CREDIT_DECISIONING)"
,AP_CONTACT_FIRST_NAME          "trim(:AP_CONTACT_FIRST_NAME)"
,AP_CONTACT_LAST_NAME           "trim(:AP_CONTACT_LAST_NAME)"
,AP_PHONE                       "trim(:AP_PHONE)"
,AP_EMAIL                       "trim(:AP_EMAIL)"
,INVOICE_EMAIL_ADDRESS          "trim(:INVOICE_EMAIL_ADDRESS)"
,POD_EMAIL_ADDRESS              "trim(:POD_EMAIL_ADDRESS)"
,SOA_EMAIL_ADDRESS              "trim(:SOA_EMAIL_ADDRESS)"
,PURCHASE_ORDER_REQUIRED        "trim(:PURCHASE_ORDER_REQUIRED)"
,DUNSNUMBER                     "trim(:DUNSNUMBER)"
,CREATION_DATE                  "trim(UPPER(:CREATION_DATE))"
,CREATED_BY                     "trim(:CREATED_BY)"
,SEND_STATEMENT                 "trim(:SEND_STATEMENT)"
,CUSTOMER_SOURCE                "trim(:CUSTOMER_SOURCE)"
,PRINT_PRICES_ON_ORDER          "trim(:PRINT_PRICES_ON_ORDER)"
,JOB_SITE_NAME                  "trim(:JOB_SITE_NAME)"
,JOB_DESC                       "trim(:JOB_DESC)"
,MANDATORY_NOTES                "trim(:MANDATORY_NOTES)"
,MANDATORY_NOTES1               "trim(:MANDATORY_NOTES1)"
,MANDATORY_NOTES2               "trim(:MANDATORY_NOTES2)"
,MANDATORY_NOTES3               "trim(:MANDATORY_NOTES3)"
,MANDATORY_NOTES4               "trim(:MANDATORY_NOTES4)"
,MANDATORY_NOTES5               "trim(:MANDATORY_NOTES5)"
,REMIT_TO_ADDRESS               "trim(:REMIT_TO_ADDRESS)"
,TAX_EXEMPTION                  "trim(:TAX_EXEMPTION)"
,COLLECTION_NOTES               "trim(:COLLECTION_NOTES)"
,SHIP_METHOD                    "trim(:SHIP_METHOD)"
,TAX_CERTIFICATE                "trim(:TAX_CERTIFICATE)"
,NON_TAX_REASON                 "trim(:NON_TAX_REASON)"
,AUTO_APPLY                     "TRIM(:AUTO_APPLY)"
,FSD                            "REPLACE(REPLACE(TRIM(:FSD),CHR(13),''),CHR(10),'')"
)