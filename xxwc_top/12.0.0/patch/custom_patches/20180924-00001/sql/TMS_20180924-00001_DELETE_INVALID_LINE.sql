/******************************************************************************
  $Header TMS_20180924-00001_DELETE_INVALID_LINE.sql $
  Module Name:Data Fix script for 20180924-00001

  PURPOSE: delete the record from the table "xxwc.XXWC_OE_OPEN_ORDER_LINES"

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        24-SEP-2018  Pattabhi Avula        TMS#20180924-00001 - delete the 
                                                record from the table "xxwc.XXWC_OE_OPEN_ORDER_LINES" 

*******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Delete');

   DELETE 
     FROM xxwc.xxwc_oe_open_order_lines
    WHERE inventory_item_id=2930339
      AND organization_id=282;

   DBMS_OUTPUT.put_line ('Records Deleted-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to Delete record ' || SQLERRM);
END;
/