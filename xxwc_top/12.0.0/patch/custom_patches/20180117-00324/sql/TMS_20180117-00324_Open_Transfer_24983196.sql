  set serveroutput on;
 /*************************************************************************
      PURPOSE:  24983196 Open transfer

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        06-FEB-2018  Sundaramoorthy     Initial Version - TMS #20180117-00324
	************************************************************************/ 
 BEGIN
 dbms_output.put_line ('Start Update ');

 UPDATE oe_order_headers_all
  SET  flow_status_code ='CLOSED'
  , open_flag ='N'
   WHERE header_id =60590442;  
  
  dbms_output.put_line ('Update count '||SQL%ROWCOUNT);
  COMMIT;
  
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
	dbms_output.put_line ('Inside Exception '|| SUBSTR(SQLERRM,1,2000));	
END;
/	