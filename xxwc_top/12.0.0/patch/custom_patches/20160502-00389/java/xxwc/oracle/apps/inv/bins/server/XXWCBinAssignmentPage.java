/*************************************************************************
   *   $Header XXWCBinAssignmentPage.java $
   *   Module Name: XXWCBinAssignmentPage
   *   
   *   Package: package xxwc.oracle.apps.inv.bins.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCBinAssignmentPage Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00164 - RF - Bin maintenance form   
        1.1       03-MAY-2016   Lee Spitzer             TMS Ticket 20160502-00389 Menu to Cancel Button
**************************************************************************/

package xxwc.oracle.apps.inv.bins.server;

import java.sql.SQLException;

import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.fnd.flexj.FlexException;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.inv.lov.server.ItemLOV;
import oracle.apps.inv.lov.server.LocatorKFF;
import oracle.apps.inv.lov.server.SubinventoryLOV;
import xxwc.oracle.apps.inv.lov.server.*;

/*************************************************************************
 *   NAME: XXWCBinAssignmentPage extends PageBean
 *
 *   PURPOSE:   Main class for XXWCBinAssignmentPage
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00164 - RF - Bin maintenance form   
 **************************************************************************/


public class XXWCBinAssignmentPage extends PageBean{
    /*Define objects in the class*/
    public static String WMS_TXN_SUCCESS = "Txn Successful";
    public static String WMS_TXN_ERROR = "Txn Failed. Please check log";
    public static String WMS_TXN_CANCEL = "Txn Cancelled";
    public static String WMS_LOT_INS_FAIL = "Lot Insert Failed";
    public static String WMS_SER_INS_FAIL = "Serial Insert Failed";
    public static String savenextPrompt = "";
    public static String donePrompt = "";
    public static String cancelPrompt = "";
    private boolean areThePromptsSet;
    protected XXWCItemLOV mItemFld;
    TextFieldBean mItemDescFld;
    protected SubinventoryLOV mSubFld;
    protected XXWCBinsLOV mLocFld;
    protected XXWCBinsLOV mLocFldPreFix;
    protected ButtonFieldBean mCancel;
    protected ButtonFieldBean mSaveNext;
    protected ButtonFieldBean mDone;
    protected String m_userid;


    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.bins.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.bins.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCBinAssignmentPage";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class name CustomListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    private static String gCallFrom = "XXWCBinAssignmentPage";



    /*************************************************************************
     *   NAME: public XXWCBinAssignmentPage(Session session)
     *
     *   PURPOSE:   Inherits session from XXWCPrimaryBinAssignmentPage
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public XXWCBinAssignmentPage(Session session) {
        initLayout(session);
    }

    
    /*************************************************************************
     *   NAME: private void initLayout(Session session)
     *
     *   PURPOSE:   Sets the initial layout of the page
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00164 - RF - Bin maintenance form   
          1.1       03-MAY-2016   Lee Spitzer             TMS Ticket 20160502-00389 Menu to Cancel Button
     **************************************************************************/

    private void initLayout(Session session) {
        m_userid = "";
        WMS_TXN_SUCCESS = UtilFns.getMessage(session, "WMS", "WMS_TXN_SUCCESS");
        WMS_TXN_ERROR = UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR");
        WMS_TXN_CANCEL = UtilFns.getMessage(session, "WMS", "WMS_TXN_CANCEL");
        WMS_LOT_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_LOT_INS_FAIL");
        WMS_SER_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_SER_INS_FAIL");
        /*Item Field*/
        // mItemFld = new LOVFieldBean();
        mItemFld = new XXWCItemLOV("ALL");
        mItemFld.setName("XXWC.ITEM");
        mItemFld.setRequired(true);
        /*Item Description*/
        mItemDescFld = new TextFieldBean();
        mItemDescFld.setName("XXWC.DESC");
        mItemDescFld.setPrompt("Item Desc");
        mItemDescFld.setEditable(false);
        /*Subinventory*/
        mSubFld = new SubinventoryLOV("INVINQ");
        mSubFld.setName("XXWC.SUB");
        mSubFld.setRequired(false);
        /*Bin*/
        try {
            mLocFld = new XXWCBinsLOV();
            mLocFld.setName("XXWC.LOC");
            mLocFld.setRequired(false);
            mLocFld.setHidden(true);
            
            mLocFldPreFix = new XXWCBinsLOV();
            mLocFldPreFix.setName("XXWC.WC_PREFIX");
            mLocFldPreFix.setRequired(false);
            mLocFldPreFix.setHidden(true);
            mLocFldPreFix.setEditable(false);
            
        } catch (Exception e) {
            UtilFns.trace("Error instantiating rcvtxn locator kff");
        }
        /*Save Properties*/
        mSaveNext = new ButtonFieldBean();
        mSaveNext.setName("XXWC.SAVENEXT");
        mSaveNext.setPrompt("Save/Next");
        mSaveNext.setEnableAcceleratorKey(true);
        mSaveNext.setHidden(true);
        /*Save Properties*/
        mDone = new ButtonFieldBean();
        mDone.setName("XXWC.DONE");
        mDone.setPrompt("Done");
        mDone.setEnableAcceleratorKey(true);
        mDone.setHidden(true);
        /*Cancel Properties*/
        mCancel = new ButtonFieldBean();
        mCancel.setName("XXWC.CANCEL");
        //mCancel.setPrompt("Menu"); Removed 05/03/2016 TMS Ticket 20160502-00389
        mCancel.setEnableAcceleratorKey(true);
        mCancel.setHidden(false);
        /*set the properties for each to the addFieldBean*/
        addFieldBean(mItemFld);
        addFieldBean(mItemDescFld);
        addFieldBean(mSubFld);
        addFieldBean(mLocFld);
        addFieldBean(mLocFldPreFix);
        addFieldBean(mSaveNext);
        addFieldBean(mDone);
        addFieldBean(mCancel);
        /*Point Page to the XXWCPrimaryBinAssignmentFListener Listener*/
        XXWCBinAssignmentFListener fieldListener = new XXWCBinAssignmentFListener();
        /*Add Listener to each property*/
        mItemFld.addListener(fieldListener);
        mItemDescFld.addListener(fieldListener);
        mSubFld.addListener(fieldListener);
        mLocFld.addListener(fieldListener);
        mLocFldPreFix.addListener(fieldListener);
        mSaveNext.addListener(fieldListener);
        mDone.addListener(fieldListener);
        mCancel.addListener(fieldListener);
        try {
            setPrompts(session);
        } catch (Exception exception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_PROMPT_NOTSET"));
        }
    }
    
    /*************************************************************************
     *   NAME: private void initPrompts(Session session) throws SQLException
     *
     *   PURPOSE:   Sets the initial prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    private void initPrompts(Session session) throws SQLException {
        try {
            String gMethod = "initPrompts";
            String gCallPoint = "Start";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Bin Assignment (").append(s).append(")").toString());

            mItemFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable",
                                                  "INV_ITEM_PROMPT"));
            mItemFld.retrieveAttributes("INV_ITEM_PROMPT");
            mItemDescFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable",
                                                      "INV_DESCRIPTION_PROMPT"));
            mItemDescFld.retrieveAttributes("INV_DESCRIPTION_PROMPT");
            mSubFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable",
                                                 "INV_SUB_PROMPT"));
            mSubFld.retrieveAttributes("INV_SUB_PROMPT");
            mLocFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable",
                                                 "INV_LOCATOR_PROMPT"));
            mLocFld.retrieveAttributes("INV_LOCATOR_PROMPT");
            mLocFldPreFix.setPrompt("Loc PreFix");
            savenextPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_SAVE_NEXT_PROMPT");
            donePrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_DONE_PROMPT");
            cancelPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_CANCEL_PROMPT");
            areThePromptsSet = true;
            gCallPoint = "End";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
        } catch (SQLException sqlexception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR"));
        }
    }

    /*************************************************************************
     *   NAME: private void setPrompts(Session session)
     *
     *   PURPOSE:   Sets the set prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00164 - RF - Bin maintenance form   
          1.1       03-MAY-2016   Lee Spitzer             TMS Ticket 20160502-00389 Menu to Cancel Button
     **************************************************************************/

    private void setPrompts(Session session) {
            String gMethod = "setPrompts";
            String gCallPoint = "Start";
            if (!areThePromptsSet) {
                try {
                    initPrompts(session);
                } catch (SQLException sqlexception) {
                }
            }
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Bin Assignment (").append(s).append(")").toString());

            mSaveNext.setPrompt(savenextPrompt);
            mDone.setPrompt(donePrompt);
            mCancel.setPrompt(cancelPrompt);
            mSaveNext.retrieveAttributes("INVE_SAVE_NEXT_PROMPT");
            mDone.retrieveAttributes("INV_DONE_PROMPT");
            mCancel.retrieveAttributes("INV_CANCEL_PROMPT");
            //mCancel.setPrompt("Menu"); //Removed 05/03/2016 TMS Ticket 20160502-00389
            gCallPoint = "End Prompts";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            gCallPoint = "setting field values";
            setFieldValues(session);
    }


    /*************************************************************************
     *   NAME: public ItemLOV  getmItemFld()
     *
     *   PURPOSE:   return the value of mItemFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
      **************************************************************************/

    public XXWCItemLOV getmItemFld()
        //public LOVFieldBean getmItemFld()
    {
        return mItemFld;
    }

    /*************************************************************************
     *   NAME: public TextFieldBean getmItemDescFld()
     *
     *   PURPOSE:   return the value of mItemDescFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public TextFieldBean getmItemDescFld() {
        return mItemDescFld;
    }

    /*************************************************************************
     *   NAME: public SubinventoryLOV getmSubFld()
     *
     *   PURPOSE:   return the value of mSubFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public SubinventoryLOV getmSubFld() {
        return mSubFld;
    }

    /*************************************************************************
     *   NAME: public XXWCBinsLOV getmLocFld()
     *
     *   PURPOSE:   return the value of mLocFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public XXWCBinsLOV getmLocFld() {
        return mLocFld;
    }

    /*************************************************************************
     *   NAME: public XXWCBinsLOV getmLocFldPreFix
     *
     *   PURPOSE:   return the value of mLocFldPreFix
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public XXWCBinsLOV getmLocFldPreFix() {
         return mLocFldPreFix;
     }

     /*************************************************************************
      *   NAME: public ButtonFieldBean getmCancel
      *
      *   PURPOSE:   return the value of mCancel
      *
      *   REVISIONS:
      *   Ver        Date        Author                     Description
      *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00164 - RF - Bin maintenance form   
      **************************************************************************/

    public ButtonFieldBean getmCancel() {
        return mCancel;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmSaveNext()
     *
     *   PURPOSE:   return the value of mSaveNext
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public ButtonFieldBean getmSaveNext() {
        return mSaveNext;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmDone()
     *
     *   PURPOSE:   return the value of mDone
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public ButtonFieldBean getmDone() {
        return mDone;
    }


    /*************************************************************************
     *   NAME: public String getUserId()
     *
     *   PURPOSE:   return the value of m_userid
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public String getUserId() {
        return m_userid;
    }

    /*************************************************************************
     *   NAME: void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageEntered method
     *
     *   Parameters: MWAEvent mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
        Session session = e.getSession();
    }

    /*************************************************************************
     *   NAME: void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageExited method
     *
     *
     *   Parameters: MWAEvent   mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   

     **************************************************************************/

    public void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                              DefaultOnlyHandlerException {
        Session session = e.getSession();
    }


    public void setFieldValues(Session session){
        String gMethod = "sesFieldValues";
        String gCallPoint = "Start";
        session.setRefreshScreen(true);
        String sessPreviousPage = "";
            try {
                sessPreviousPage = (String)session.getObject("sessPreviousPage").toString();
            }
            catch (Exception e) {
                sessPreviousPage = "";
                session.putSessionObject("sessXXWC.ITEM", "");
                session.putSessionObject("sessInventoryItemId", "");
                session.putSessionObject("sessXXWC.DESCRIPTION", "");
                session.putSessionObject("sessXXWC.SUBINVENTORY", "");
            }
        
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + "sessPreviousPage " + sessPreviousPage);
       
        if (!sessPreviousPage.equals(null) || !sessPreviousPage.equals("")){
            gCallPoint = "if !sessPreviousPage";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + "sessXXWC.ITEM " + session.getObject("sessXXWC.ITEM".toString()));
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + "sessInventoryItemId " + session.getObject(("sessInventoryItemId")));
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + "sessXXWC.DESCRIPTION " + session.getObject("sessXXWC.DESCRIPTION".toString()));
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + "sessPreviousPage " + session.getObject("sessXXWC.SUBINVENTORY".toString()));
            getmItemFld().setValue(session.getObject("sessXXWC.ITEM").toString());
            getmItemFld().setmItemNumber(session.getObject("sessXXWC.ITEM").toString());
            getmItemFld().setmInventoryItemId(session.getObject("sessInventoryItemId").toString());
            getmItemDescFld().setValue(session.getObject("sessXXWC.DESCRIPTION").toString());
            getmSubFld().setValue(session.getObject("sessXXWC.SUBINVENTORY").toString());
            getmLocFld().setHidden(false);
            getmLocFldPreFix().setHidden(false);
        }
        else {
            gCallPoint = "else sessPreviousPage";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + "sessPreviousPage " + sessPreviousPage);
            mItemFld.setValue("");
            session.putSessionObject("sessInventoryItemId", "");
            mItemDescFld.setValue("");
            mSubFld.setValue("");
            getmLocFld().setHidden(true);
            getmLocFldPreFix().setHidden(true);
        }
        
        session.putSessionObject("sessXXWC.ITEM", "");
        session.putSessionObject("sessInventoryItemId", "");
        session.putSessionObject("sessXXWC.DESCRIPTION", "");
        session.putSessionObject("sessXXWC.SUBINVENTORY","");
    }
   
}
