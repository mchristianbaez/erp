DROP MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__ORD_LN_MV;
CREATE MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__ORD_LN_MV (ACTUAL_SHIPMENT_DATE,ATTRIBUTE5,ATTRIBUTE7,CUST_PO_NUMBER,HEADER_ID,INVENTORY_ITEM_ID,LAST_UPDATE_DATE,LINE_CATEGORY_CODE,LINE_ID,LINK_TO_LINE_ID,REFERENCE_LINE_ID,ORDERED_QUANTITY,PRICING_DATE,SHIP_FROM_ORG_ID,SHIPPING_METHOD_CODE,SOURCE_TYPE_CODE,UNIT_COST,UNIT_LIST_PRICE,HAS_DROPSHIP_CNT,ORDER_TYPE_ID,MSIB_ITEM_NUMBER,MSIB_ITEM_DESCRIPTION,MSIB_INVENTORY_ITEM_ID)
BUILD IMMEDIATE
REFRESH COMPLETE ON DEMAND
WITH PRIMARY KEY
AS 
WITH order_lines
     AS /* sub-MV used by APPS.XXWC_OBIEE_CUST_TRANS_INCR_MV
   --
   -- This MV will be refreshed before the main query in
   -- XXWC_OBIEE_CUST_TRANS_INCR_MV
   --
   -- v0.2  -- join w ont.oe_order_headers_all for order_type_id
      deriving rental orders info for lookups to
      apps.xxwc_mv_routines_pkg.get_om_rental_item
      -- C37_INVOICE_DIRECT_FLG change has_dropship_cnt from 1/0 to Y/N
      v0.3 CG Updated to correct issue of looking at cancelled lines
      v0.4 CG Previous version worked only for for regular rental return
              and shipment. Added the second oe_order_lines_all join in the
              with clause to be able to retrieve the rental return or credit line
   */
       (SELECT /*+ qb_name(ord_ln) materialize */
              l1.actual_shipment_date
              ,l1.attribute5
              ,l1.attribute7
              ,l1.cust_po_number
              ,l1.header_id
              ,l1.inventory_item_id
              ,l1.last_update_date
              ,l1.line_category_code
              ,l1.line_id
              ,NVL (l1.link_to_line_id, l2.link_to_line_id) link_to_line_id
              ,l1.reference_line_id
              ,l1.ordered_quantity
              ,l1.org_id
              ,l1.pricing_date
              ,l1.ship_from_org_id
              ,l1.shipping_method_code
              ,l1.source_type_code
              ,l1.unit_cost
              ,l1.unit_list_price
              ,CASE
                  WHEN NVL (l1.booked_flag, 'N') = 'Y' -- 10/02/14: CG Updated to correct issue of looking at cancelled lines
                                                       -- AND NVL (cancelled_flag, 'N') = 'Y'
                       AND NVL (l1.cancelled_flag, 'N') = 'N' AND l1.source_type_code = 'EXTERNAL' THEN 'Y'
                  ELSE 'N'
               END
                  has_dropship_cnt
          FROM ont.oe_order_lines_all l1
               LEFT OUTER JOIN ont.oe_order_lines_all l2                                 -- For rental returns
                                                        ON l1.reference_line_id = l2.line_id)
SELECT l.actual_shipment_date
      ,l.attribute5
      ,l.attribute7
      ,l.cust_po_number
      ,l.header_id
      ,l.inventory_item_id
      ,l.last_update_date
      ,l.line_category_code
      ,l.line_id
      ,l.link_to_line_id
      ,l.reference_line_id
      ,l.ordered_quantity
      ,l.pricing_date
      ,l.ship_from_org_id
      ,l.shipping_method_code
      ,l.source_type_code
      ,l.unit_cost
      ,l.unit_list_price
      ,l.has_dropship_cnt
      ,h.order_type_id
      ,msib.segment1 AS msib_item_number
      ,msib.description AS msib_item_description
      ,msib.inventory_item_id AS msib_inventory_item_id
  FROM order_lines l
       INNER JOIN ont.oe_order_headers_all h ON l.header_id = h.header_id AND l.org_id = h.org_id
       LEFT OUTER JOIN order_lines rma_ln
          ON l.link_to_line_id = rma_ln.line_id AND rma_ln.line_category_code = 'RETURN'
       LEFT OUTER JOIN order_lines shp_ln
          ON rma_ln.link_to_line_id = shp_ln.line_id AND shp_ln.line_category_code = 'ORDER'
       LEFT OUTER JOIN inv.mtl_system_items_b msib
          ON     shp_ln.inventory_item_id = msib.inventory_item_id
             AND shp_ln.ship_from_org_id = msib.organization_id;


COMMENT ON MATERIALIZED VIEW APPS.XXWC_OBIEE_CT__ORD_LN_MV IS 'snapshot table for snapshot APPS.XXWC_OBIEE_CT__ORD_LN_MV';
