/*************************************************************************
  $Header TMS_20161006-00018_stuck_Order_issue.sql $
  Module Name: TMS#20161006-00018 - STUCK ORDER 22165574 

  PURPOSE: Data fix to update the stuck order status

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        10-OCT-2016 Pattabhi Avula         TMS#20161006-00018 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20161006-00018    , Before Update');

update apps.oe_order_lines_all
set FLOW_STATUS_CODE='CANCELLED',
CANCELLED_FLAG='Y'
where line_id = 80829817
and header_id= 49473488;

   DBMS_OUTPUT.put_line (
         'TMS: 20161006-00018  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20161006-00018    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161006-00018 , Errors : ' || SQLERRM);
END;
/