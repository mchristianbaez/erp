/*************************************************************************
  $Header TMS_20160215_00025_Datafix.sql $
  Module Name: TMS#20160215-00025 - Update flip date to null for non stock items(with flip date value prior to 5-Jun-2015).

  PURPOSE: Data fix to update flip date as null for non-stock flag 'N'

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        2/15/2016  Ram Talluri         TMS#20160215-00025

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160215-00025 - Before Update');

   UPDATE xxwc.xxwc_isr_details_all A
      SET A.FLIP_DATE = NULL
    WHERE     NVL (A.STK_FLAG, 'N') = 'N'
          AND A.FLIP_DATE <TO_DATE('05-JUN-2015 00:00:00', 'DD-MON-YYYY HH24:MI:SS');

   DBMS_OUTPUT.put_line (
         'TMS: 20160215-00025 ISR Data fix - Number of records updated: '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160215-00025 -  End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160215-00025 -  Errors : ' || SQLERRM);
END;
/