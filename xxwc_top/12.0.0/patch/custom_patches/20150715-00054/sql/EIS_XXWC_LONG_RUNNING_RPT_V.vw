CREATE OR REPLACE FORCE VIEW "XXEIS"."EIS_XXWC_LONG_RUNNING_RPT_V" ("REQUEST_ID", "REQUEST_NAME", "REQUESTOR", "DURATION_MINS", "DATE_STARTED", "DATE_COMPLETED", "PARAMETERS", "COMPLETION_TEXT", "ACTUAL_COMPLETION_DATE", "ACTUAL_START_DATE", "REQUEST_DATE") AS 
  SELECT request_id 
,      program request_name
,      requestor ||' - '|| (select description
                               from apps.fnd_user
                               where user_name = requestor) requestor
,      ROUND(((ACTUAL_COMPLETION_DATE-ACTUAL_START_DATE)*24*60*60/60)) DURATION_MINS
,      ACTUAL_START_DATE DATE_STARTED
,      actual_completion_date DATE_COMPLETED
,      argument_text PARAMETERS
,      COMPLETION_TEXT
,ACTUAL_COMPLETION_DATE
,ACTUAL_START_DATE
,request_date
from   APPS.FND_CONC_REQ_SUMMARY_V
where  1=1;
/