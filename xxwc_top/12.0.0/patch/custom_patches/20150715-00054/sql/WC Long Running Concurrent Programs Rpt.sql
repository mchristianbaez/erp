--Report Name            : WC Long Running Concurrent Programs Rpt
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for WC Long Running Concurrent Programs Rpt
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_LONG_RUNNING_RPT_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_LONG_RUNNING_RPT_V',85000,'','','','','MR020532','XXEIS','Eis Xxwc Long Running Rpt V','EXLRRV','','');
--Delete View Columns for EIS_XXWC_LONG_RUNNING_RPT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_LONG_RUNNING_RPT_V',85000,FALSE);
--Inserting View Columns for EIS_XXWC_LONG_RUNNING_RPT_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','COMPLETION_TEXT',85000,'Completion Text','COMPLETION_TEXT','','','','MR020532','VARCHAR2','','','Completion Text','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','PARAMETERS',85000,'Parameters','PARAMETERS','','','','MR020532','VARCHAR2','','','Parameters','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','DATE_COMPLETED',85000,'Date Completed','DATE_COMPLETED','','','','MR020532','DATE','','','Date Completed','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','DATE_STARTED',85000,'Date Started','DATE_STARTED','','','','MR020532','DATE','','','Date Started','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','DURATION_MINS',85000,'Duration Mins','DURATION_MINS','','','','MR020532','NUMBER','','','Duration Mins','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','REQUESTOR',85000,'Requestor','REQUESTOR','','','','MR020532','VARCHAR2','','','Requestor','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','REQUEST_NAME',85000,'Request Name','REQUEST_NAME','','','','MR020532','VARCHAR2','','','Request Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','REQUEST_ID',85000,'Request Id','REQUEST_ID','','','','MR020532','NUMBER','','','Request Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','ACTUAL_COMPLETION_DATE',85000,'Actual Completion Date','ACTUAL_COMPLETION_DATE','','','','MR020532','DATE','','','Actual Completion Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','ACTUAL_START_DATE',85000,'Actual Start Date','ACTUAL_START_DATE','','','','MR020532','DATE','','','Actual Start Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LONG_RUNNING_RPT_V','REQUEST_DATE',85000,'Request Date','REQUEST_DATE','','','','MR020532','DATE','','','Request Date','','','');
--Inserting View Components for EIS_XXWC_LONG_RUNNING_RPT_V
--Inserting View Component Joins for EIS_XXWC_LONG_RUNNING_RPT_V
END;
/
set scan on define on
prompt Creating Report Data for WC Long Running Concurrent Programs Rpt
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - WC Long Running Concurrent Programs Rpt
xxeis.eis_rs_utility.delete_report_rows( 'WC Long Running Concurrent Programs Rpt' );
--Inserting Report - WC Long Running Concurrent Programs Rpt
xxeis.eis_rs_ins.r( 85000,'WC Long Running Concurrent Programs Rpt','','once every morning to publish or distribute an Excel report of all concurrent programs running over a certain run time threshold say "X" minutes. The "X" can be a parameter on the report defaulted to 120 mins (2 hours). Users should be able to run the report ad-hoc by providing the Date range, and "Run time threshold".','','','','MR020532','EIS_XXWC_LONG_RUNNING_RPT_V','Y','','','MR020532','','N','WC Knowledge Management Reports','PDF,','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - WC Long Running Concurrent Programs Rpt
xxeis.eis_rs_ins.rc( 'WC Long Running Concurrent Programs Rpt',85000,'COMPLETION_TEXT','Completion Text','Completion Text','','','','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_LONG_RUNNING_RPT_V','','');
xxeis.eis_rs_ins.rc( 'WC Long Running Concurrent Programs Rpt',85000,'DATE_COMPLETED','Date Completed','Date Completed','','','','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_LONG_RUNNING_RPT_V','','');
xxeis.eis_rs_ins.rc( 'WC Long Running Concurrent Programs Rpt',85000,'DATE_STARTED','Date Started','Date Started','','','','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_LONG_RUNNING_RPT_V','','');
xxeis.eis_rs_ins.rc( 'WC Long Running Concurrent Programs Rpt',85000,'DURATION_MINS','Duration Mins','Duration Mins','','','','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_LONG_RUNNING_RPT_V','','');
xxeis.eis_rs_ins.rc( 'WC Long Running Concurrent Programs Rpt',85000,'PARAMETERS','Parameters','Parameters','','','','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_LONG_RUNNING_RPT_V','','');
xxeis.eis_rs_ins.rc( 'WC Long Running Concurrent Programs Rpt',85000,'REQUESTOR','Requestor','Requestor','','','','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_LONG_RUNNING_RPT_V','','');
xxeis.eis_rs_ins.rc( 'WC Long Running Concurrent Programs Rpt',85000,'REQUEST_ID','Request Id','Request Id','','','','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_LONG_RUNNING_RPT_V','','');
xxeis.eis_rs_ins.rc( 'WC Long Running Concurrent Programs Rpt',85000,'REQUEST_NAME','Request Name','Request Name','','','','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_LONG_RUNNING_RPT_V','','');
--Inserting Report Parameters - WC Long Running Concurrent Programs Rpt
xxeis.eis_rs_ins.rp( 'WC Long Running Concurrent Programs Rpt',85000,'Time Duration','Time Duration','DURATION_MINS','>=','','120','NUMERIC','N','Y','3','','N','CONSTANT','MR020532','Y','','','','');
xxeis.eis_rs_ins.rp( 'WC Long Running Concurrent Programs Rpt',85000,'Date From','Date From','DATE_STARTED','>=','','select sysdate -1 from dual','DATE','N','Y','1','','Y','SQL','MR020532','Y','','','','');
xxeis.eis_rs_ins.rp( 'WC Long Running Concurrent Programs Rpt',85000,'Date To','Date To','DATE_STARTED','<=','','','DATE','N','Y','2','','N','CURRENT_DATE','MR020532','Y','','','','');
--Inserting Report Conditions - WC Long Running Concurrent Programs Rpt
xxeis.eis_rs_ins.rcn( 'WC Long Running Concurrent Programs Rpt',85000,'','','','','AND ((to_date(to_char(request_date,''DD-MM-YY HH24:MI:SS''),''DD-MM-YY HH24:MI:SS'')
     between to_date(:Date From||'' 06:00:00'',''DD-MM-YY HH24:MI:SS'') AND to_date(:Date To|| '' 07:00:00'',''DD-MM-YY HH24:MI:SS''))
     or (TO_DATE(TO_CHAR(ACTUAL_START_DATE,''DD-MM-YY HH24:MI:SS''),''DD-MM-YY HH24:MI:SS'')
     between TO_DATE(:Date From||'' 06:00:00'',''DD-MM-YY HH24:MI:SS'') and TO_DATE(:Date To||'' 07:00:00'',''DD-MM-YY HH24:MI:SS'')))','Y','0','','MR020532');
xxeis.eis_rs_ins.rcn( 'WC Long Running Concurrent Programs Rpt',85000,'DURATION_MINS','>=',':Time Duration','','','Y','4','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'WC Long Running Concurrent Programs Rpt',85000,'DATE_STARTED','>=',':Date From','','','Y','1','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'WC Long Running Concurrent Programs Rpt',85000,'DATE_STARTED','<=',':Date To','','','N','3','N','MR020532');
--Inserting Report Sorts - WC Long Running Concurrent Programs Rpt
--Inserting Report Triggers - WC Long Running Concurrent Programs Rpt
--Inserting Report Templates - WC Long Running Concurrent Programs Rpt
--Inserting Report Portals - WC Long Running Concurrent Programs Rpt
--Inserting Report Dashboards - WC Long Running Concurrent Programs Rpt
--Inserting Report Security - WC Long Running Concurrent Programs Rpt
xxeis.eis_rs_ins.rsec( 'WC Long Running Concurrent Programs Rpt','20005','','50900',85000,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'WC Long Running Concurrent Programs Rpt','20005','','51207',85000,'MR020532','','');
--Inserting Report Pivots - WC Long Running Concurrent Programs Rpt
END;
/
set scan on define on
