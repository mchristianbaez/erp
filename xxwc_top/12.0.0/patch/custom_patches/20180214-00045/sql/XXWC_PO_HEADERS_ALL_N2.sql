---------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_PO_HEADERS_ALL_N2
  File Name: XXWC_PO_HEADERS_ALL_N2.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------      -----------------------------------------------------------------------------------
     1.0        14-Feb-2018  P.Vamshidhar  TMS# 20180214-00045 - ISR Running longer at XXWC.XXWC_ISR_BPA_STG## table load stage 
****************************************************************************************************************************/
CREATE INDEX PO.XXWC_PO_HEADERS_ALL_N2
   ON PO.PO_HEADERS_ALL (TYPE_LOOKUP_CODE,
                         PO_HEADER_ID,
                         NVL ("CANCEL_FLAG", 'N'),
                         NVL ("CLOSED_CODE", 'X'),
                         NVL ("AUTHORIZATION_STATUS", 'X'))
   TABLESPACE APPS_TS_TX_DATA
   ONLINE
   PARALLEL 4;
 
ALTER INDEX PO.XXWC_PO_HEADERS_ALL_N2 NOPARALLEL;
