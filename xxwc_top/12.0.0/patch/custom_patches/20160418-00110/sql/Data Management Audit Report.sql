--Report Name            : Data Management Audit Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Data Management Audit Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_DATA_MGMT_AUDIT_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_DATA_MGMT_AUDIT_V',401,'','','','','SA059956','XXEIS','Eis Xxwc Data Mgmt Audit V','EXDMAV','','');
--Delete View Columns for EIS_XXWC_DATA_MGMT_AUDIT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_DATA_MGMT_AUDIT_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_DATA_MGMT_AUDIT_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','EXPENSE_ACCOUNT',401,'Expense Account','EXPENSE_ACCOUNT','','','','SA059956','VARCHAR2','','','Expense Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','SALES_ACCOUNT',401,'Sales Account','SALES_ACCOUNT','','','','SA059956','VARCHAR2','','','Sales Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','COGS_ACCOUNT',401,'Cogs Account','COGS_ACCOUNT','','','','SA059956','VARCHAR2','','','Cogs Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','ITEM_CREATION_DATE',401,'Item Creation Date','ITEM_CREATION_DATE','','','','SA059956','DATE','','','Item Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','WEB_LONG_DESCRIPTION',401,'Web Long Description','WEB_LONG_DESCRIPTION','','','','SA059956','VARCHAR2','','','Web Long Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','WEB_KEYWORDS',401,'Web Keywords','WEB_KEYWORDS','','','','SA059956','VARCHAR2','','','Web Keywords','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','WEB_SHORT_DESCRIPTION',401,'Web Short Description','WEB_SHORT_DESCRIPTION','','','','SA059956','VARCHAR2','','','Web Short Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','WEB_SEQUENCE',401,'Web Sequence','WEB_SEQUENCE','','','','SA059956','NUMBER','','','Web Sequence','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','THUMBNAIL_IMAGE_1',401,'Thumbnail Image 1','THUMBNAIL_IMAGE_1','','','','SA059956','VARCHAR2','','','Thumbnail Image 1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','FULLSIZE_IMAGE_1',401,'Fullsize Image 1','FULLSIZE_IMAGE_1','','','','SA059956','VARCHAR2','','','Fullsize Image 1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','CYBERSOURCE_TAX_CODE',401,'Cybersource Tax Code','CYBERSOURCE_TAX_CODE','','','','SA059956','VARCHAR2','','','Cybersource Tax Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','BULKY_ITEM_FLAG',401,'Bulky Item Flag','BULKY_ITEM_FLAG','','','','SA059956','VARCHAR2','','','Bulky Item Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','HAZMAT_FLAG',401,'Hazmat Flag','HAZMAT_FLAG','','','','SA059956','VARCHAR2','','','Hazmat Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','WEB_MFG_PART_NUMBER',401,'Web Mfg Part Number','WEB_MFG_PART_NUMBER','','','','SA059956','VARCHAR2','','','Web Mfg Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','BRAND',401,'Brand','BRAND','','','','SA059956','VARCHAR2','','','Brand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','WC_WEB_HIERARCHY',401,'Wc Web Hierarchy','WC_WEB_HIERARCHY','','','','SA059956','VARCHAR2','','','Wc Web Hierarchy','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','WEBSITE_ITEM',401,'Website Item','WEBSITE_ITEM','','','','SA059956','VARCHAR2','','','Website Item','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','INVENTORY_CATEGORY_DESCRIPTION',401,'Inventory Category Description','INVENTORY_CATEGORY_DESCRIPTION','','','','SA059956','VARCHAR2','','','Inventory Category Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','INVENTORY_CATEGORY',401,'Inventory Category','INVENTORY_CATEGORY','','','','SA059956','VARCHAR2','','','Inventory Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','BPA_NUMBER',401,'Bpa Number','BPA_NUMBER','','','','SA059956','VARCHAR2','','','Bpa Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','BPA_COST',401,'Bpa Cost','BPA_COST','','','','SA059956','NUMBER','','','Bpa Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','AVERAGE_COST',401,'Average Cost','AVERAGE_COST','','','','SA059956','NUMBER','','','Average Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','LIST_PRICE_PER_UNIT',401,'List Price Per Unit','LIST_PRICE_PER_UNIT','','','','SA059956','NUMBER','','','List Price Per Unit','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','FIXED_LOT_MULTIPLIER',401,'Fixed Lot Multiplier','FIXED_LOT_MULTIPLIER','','','','SA059956','NUMBER','','','Fixed Lot Multiplier','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','MFG_PART_NUMBER',401,'Mfg Part Number','MFG_PART_NUMBER','','','','SA059956','VARCHAR2','','','Mfg Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','REGION',401,'Region','REGION','','','','SA059956','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','DISTRICT',401,'District','DISTRICT','','','','SA059956','VARCHAR2','','','District','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','ORG_DESCRIPTION',401,'Org Description','ORG_DESCRIPTION','','','','SA059956','VARCHAR2','','','Org Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','ENGINEERING_ITEM_FLAG',401,'Engineering Item Flag','ENGINEERING_ITEM_FLAG','','','','SA059956','VARCHAR2','','','Engineering Item Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','USER_ITEM_TYPE',401,'User Item Type','USER_ITEM_TYPE','','','','SA059956','VARCHAR2','','','User Item Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','ITEM_STATUS',401,'Item Status','ITEM_STATUS','','','','SA059956','VARCHAR2','','','Item Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','ORG',401,'Org','ORG','','','','SA059956','VARCHAR2','','','Org','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','SOURCING_VENDOR_NAME',401,'Sourcing Vendor Name','SOURCING_VENDOR_NAME','','','','SA059956','VARCHAR2','','','Sourcing Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','SOURCING_VENDOR_NUMBER',401,'Sourcing Vendor Number','SOURCING_VENDOR_NUMBER','','','','SA059956','VARCHAR2','','','Sourcing Vendor Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','SOURCE',401,'Source','SOURCE','','','','SA059956','VARCHAR2','','','Source','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','MASTER_VENDOR_NUMBER',401,'Master Vendor Number','MASTER_VENDOR_NUMBER','','','','SA059956','VARCHAR2','','','Master Vendor Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','COUNTRY_OF_ORIGIN',401,'Country Of Origin','COUNTRY_OF_ORIGIN','','','','SA059956','VARCHAR2','','','Country Of Origin','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','SHELF_LIFE_DAYS',401,'Shelf Life Days','SHELF_LIFE_DAYS','','','','SA059956','NUMBER','','','Shelf Life Days','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','WEIGHT_UOM_CODE',401,'Weight Uom Code','WEIGHT_UOM_CODE','','','','SA059956','VARCHAR2','','','Weight Uom Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','WEIGHT',401,'Weight','WEIGHT','','','','SA059956','NUMBER','','','Weight','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','UOM',401,'Uom','UOM','','','','SA059956','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','LONG_DESCRIPTION',401,'Long Description','LONG_DESCRIPTION','','','','SA059956','VARCHAR2','','','Long Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','SHORT_DESCRIPTION',401,'Short Description','SHORT_DESCRIPTION','','','','SA059956','VARCHAR2','','','Short Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','ITEM',401,'Item','ITEM','','','','SA059956','VARCHAR2','','','Item','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','PRODUCT_LINE',401,'Product Line','PRODUCT_LINE','','','','SA059956','VARCHAR2','','','Product Line','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','AVP_CODE',401,'Avp Code','AVP_CODE','','','','SA059956','VARCHAR2','','','Avp Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','CONFLICT_MINERALS',401,'Conflict Minerals','CONFLICT_MINERALS','','','','SA059956','VARCHAR2','','','Conflict Minerals','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','FULFILLMENT',401,'Fulfillment','FULFILLMENT','','','','SA059956','VARCHAR2','','','Fulfillment','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','ITEM_LEVEL',401,'Item Level','ITEM_LEVEL','','','','SA059956','VARCHAR2','','','Item Level','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','RETAIL_SELLING_UOM',401,'Retail Selling Uom','RETAIL_SELLING_UOM','','','','SA059956','VARCHAR2','','','Retail Selling Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','XPAR',401,'Xpar','XPAR','','','','SA059956','VARCHAR2','','','Xpar','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','CREATED_BY',401,'Created By','CREATED_BY','','','','SA059956','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','SA059956','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','PROCESS_ID',401,'Process Id','PROCESS_ID','','','','SA059956','NUMBER','','','Process Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','ENABLED_FLAG',401,'Enabled Flag','ENABLED_FLAG','','','','SA059956','VARCHAR2','','','Enabled Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','END_DATE_ACTIVE',401,'End Date Active','END_DATE_ACTIVE','','','','SA059956','DATE','','','End Date Active','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','START_DATE_ACTIVE',401,'Start Date Active','START_DATE_ACTIVE','','','','SA059956','DATE','','','Start Date Active','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DATA_MGMT_AUDIT_V','SUMMARY_FLAG',401,'Summary Flag','SUMMARY_FLAG','','','','SA059956','VARCHAR2','','','Summary Flag','','','');
--Inserting View Components for EIS_XXWC_DATA_MGMT_AUDIT_V
--Inserting View Component Joins for EIS_XXWC_DATA_MGMT_AUDIT_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Data Management Audit Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Data Management Audit Report
xxeis.eis_rs_ins.lov( 401,'SELECT DISTINCT concatenated_segments item, description FROM mtl_system_items_kfv msi,
                org_organization_definitions ood
          WHERE msi.organization_id = ood.organization_id
            AND ood.operating_unit = fnd_profile.VALUE (''ORG_ID'')
       ORDER BY concatenated_segments','','EIS_INV_ITEM_LOV','List of all inventory items.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select INVENTORY_ITEM_STATUS_CODE
from mtl_item_status','','EIS_INV_ITEM_STATUS_LOV','Lists  the item status','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 401,'select distinct fcl.meaning
from fnd_common_lookups fcl
where fcl.lookup_type = ''ITEM_TYPE''
','','EIS_INV_USERITEM_TYPE_LOV','List values for User Item Type','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 401,'select vendor_name
from po_vendors
order by  vendor_name
','','EIS_INV_VENDOR_NAME_LOV','List all the Vendors','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 401,'select segment1 vendor_number from po_vendors','','EIS_INV_VENDOR_NUM_LOV','List the Vendor Numbers','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 401,'SELECT organization_code code,organization_name name
    FROM  ORG_ORGANIZATION_DEFINITIONS OOD,
      HR_OPERATING_UNITS HOU
    WHERE  OOD.OPERATING_UNIT = HOU.ORGANIZATION_ID
      AND HOU.ORGANIZATION_ID  = FND_PROFILE.VALUE(''ORG_ID'')
      ORDER BY organization_code','','XXWC Inventory Org List','List of All Inventory Orgs under a given operating unit.','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Supplier'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Supplier List','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT DISTINCT  description Short_Description
             ,CONCATENATED_SEGMENTS ITEM
             FROM MTL_SYSTEM_ITEMS_KFV MSI,
                  ORG_ORGANIZATION_DEFINITIONS OOD,
                  HR_OPERATING_UNITS HOU
           WHERE msi.organization_id = ood.organization_id
             AND OOD.OPERATING_UNIT = HOU.ORGANIZATION_ID
            AND HOU.ORGANIZATION_ID  = FND_PROFILE.VALUE(''ORG_ID'')
             ORDER BY concatenated_segments','','EIS_INV_ITEM_DESC_LOV','EIS_INV_ITEM_DESC_LOV','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Source'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Source List','XXWC Source List','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select distinct source
       from xxeis.eis_xxwc_po_isr_tab expi,
           org_organization_definitions ood
          WHERE expi.organization_id = ood.organization_id
            and ood.operating_unit = fnd_profile.value (''ORG_ID'')','','EIS_SOURCE_LOV','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT DISTINCT DECODE(MCV.SEGMENT2,NULL,MCV.SEGMENT1,MCV.SEGMENT1
      ||''.''
      ||MCV.SEGMENT2) Website_Item
    FROM mtl_item_categories mic,
      mtl_categories_vl mcv,
      mtl_category_sets mcs
    WHERE 1=1
    AND mic.category_id         = mcv.category_id
    AND mic.category_set_id     = mcs.category_set_id
    AND mcs.structure_id        = mcv.structure_id
    AND mcs.category_set_name   = ''Website Item''','','EIS_INV_WEB_ITEM_CAT_LOV','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Data Management Audit Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Data Management Audit Report
xxeis.eis_rs_utility.delete_report_rows( 'Data Management Audit Report' );
--Inserting Report - Data Management Audit Report
xxeis.eis_rs_ins.r( 401,'Data Management Audit Report','','Provides a comprehensive report of master, organization, classification, and website attributes.','','','','SA059956','EIS_XXWC_DATA_MGMT_AUDIT_V','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Data Management Audit Report
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'BULKY_ITEM_FLAG','Bulky Item Flag','Bulky Item Flag','','','default','','39','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'COGS_ACCOUNT','Cogs Account','Cogs Account','','','default','','49','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'COUNTRY_OF_ORIGIN','Country Of Origin','Country Of Origin','','','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'CYBERSOURCE_TAX_CODE','Cybersource Tax Code','Cybersource Tax Code','','','default','','40','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'DISTRICT','District','District','','','default','','21','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'ENGINEERING_ITEM_FLAG','Engineering Item Flag','Engineering Item Flag','','','default','','19','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'EXPENSE_ACCOUNT','Expense Account','Expense Account','','','default','','51','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'FIXED_LOT_MULTIPLIER','Fixed Lot Multiplier','Fixed Lot Multiplier','','~~~','default','','24','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'FULLSIZE_IMAGE_1','Fullsize Image 1','Fullsize Image 1','','','default','','41','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'HAZMAT_FLAG','Hazmat Flag','Hazmat Flag','','','default','','38','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'INVENTORY_CATEGORY','Inventory Category','Inventory Category','','','default','','31','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'INVENTORY_CATEGORY_DESCRIPTION','Inventory Category Description','Inventory Category Description','','','default','','32','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'ITEM','Item','Item','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'ITEM_CREATION_DATE','Item Creation Date','Item Creation Date','','','default','','47','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'ITEM_STATUS','Item Status','Item Status','','','default','','16','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'LIST_PRICE_PER_UNIT','List Price','List Price Per Unit','','~,~.~4','default','','25','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'LONG_DESCRIPTION','Long Description','Long Description','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'MASTER_VENDOR_NUMBER','Master Vendor Number','Master Vendor Number','','','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'MFG_PART_NUMBER','Mfg Part Number','Mfg Part Number','','','default','','23','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'ORG','Org','Org','','','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'ORG_DESCRIPTION','Org Description','Org Description','','','default','','20','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'REGION','Region','Region','','','default','','22','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'SALES_ACCOUNT','Sales Account','Sales Account','','','default','','50','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'SHELF_LIFE_DAYS','Shelf Life Days','Shelf Life Days','','~~~','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'SHORT_DESCRIPTION','Short Description','Short Description','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'SOURCE','Source','Source','','','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'SOURCING_VENDOR_NAME','Sourcing Vendor Name','Sourcing Vendor Name','','','default','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'SOURCING_VENDOR_NUMBER','Sourcing Vendor Number','Sourcing Vendor Number','','','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'THUMBNAIL_IMAGE_1','Thumbnail Image 1','Thumbnail Image 1','','','default','','42','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'UOM','Uom','Uom','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'USER_ITEM_TYPE','User Item Type','User Item Type','','','default','','17','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'WC_WEB_HIERARCHY','Wc Web Hierarchy','Wc Web Hierarchy','','','default','','34','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'WEBSITE_ITEM','Website Item','Website Item','','','default','','33','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'WEB_KEYWORDS','Web Keywords','Web Keywords','','','default','','43','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'WEB_LONG_DESCRIPTION','Web Long Description','Web Long Description','','','default','','46','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'WEB_MFG_PART_NUMBER','Web Mfg Part Number','Web Mfg Part Number','','','default','','37','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'WEB_SEQUENCE','Web Sequence','Web Sequence','','~~~','default','','44','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'WEB_SHORT_DESCRIPTION','Web Short Description','Web Short Description','','','default','','45','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'WEIGHT','Weight','Weight','','~,~.~2','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'WEIGHT_UOM_CODE','Weight Uom Code','Weight Uom Code','','','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'PRODUCT_LINE','Product Line','Product Line','','','default','','36','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'AVP_CODE','Avp Code','Avp Code','','','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'CONFLICT_MINERALS','Conflict Minerals','Conflict Minerals','','','default','','52','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'FULFILLMENT','Fulfillment','Fulfillment','','','default','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'ITEM_LEVEL','Item Level','Item Level','','','default','','18','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'RETAIL_SELLING_UOM','Retail Selling Uom','Retail Selling Uom','','','default','','29','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'XPAR','Ext Prod Attr Grp','Xpar','','','default','','30','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'CREATED_BY','Created By','Created By','','','','','48','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'AVERAGE_COST','Average Cost','Average Cost','','~,~.~4','default','','26','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'BPA_COST','Bpa Cost','Bpa Cost','','~,~.~4','default','','27','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'BPA_NUMBER','Bpa Number','Bpa Number','','','default','','28','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'BRAND','Brand','Brand','','','default','','35','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'ENABLED_FLAG','Enabled Flag','Enabled Flag','','','default','','53','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'END_DATE_ACTIVE','End Date Active','End Date Active','','','default','','56','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'START_DATE_ACTIVE','Start Date Active','Start Date Active','','','default','','55','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
xxeis.eis_rs_ins.rc( 'Data Management Audit Report',401,'SUMMARY_FLAG','Summary Flag','Summary Flag','','','default','','54','N','','','','','','','','SA059956','N','N','','EIS_XXWC_DATA_MGMT_AUDIT_V','','');
--Inserting Report Parameters - Data Management Audit Report
xxeis.eis_rs_ins.rp( 'Data Management Audit Report',401,'Item Number','Item','ITEM','IN','EIS_INV_ITEM_LOV','','VARCHAR2','N','Y','1','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Data Management Audit Report',401,'Item Creation Date','Item Creation Date','','IN','','','DATE','N','Y','13','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Data Management Audit Report',401,'Item Status','Item Status','ITEM_STATUS','IN','EIS_INV_ITEM_STATUS_LOV','','VARCHAR2','N','Y','10','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Data Management Audit Report',401,'Organization','Org','ORG','IN','XXWC Inventory Org List','','VARCHAR2','N','Y','8','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Data Management Audit Report',401,'Source','Source','SOURCE','IN','EIS_SOURCE_LOV','','VARCHAR2','N','Y','6','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Data Management Audit Report',401,'User Item Type','User Item Type','USER_ITEM_TYPE','IN','EIS_INV_USERITEM_TYPE_LOV','','VARCHAR2','N','Y','11','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Data Management Audit Report',401,'Website Item','Website Item','WEBSITE_ITEM','IN','EIS_INV_WEB_ITEM_CAT_LOV','','VARCHAR2','N','Y','12','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Data Management Audit Report',401,'Item List','Item List','','IN','XXWC Item List','','VARCHAR2','N','Y','3','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Data Management Audit Report',401,'Source List','Source List','','IN','XXWC Source List','','VARCHAR2','N','Y','7','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Data Management Audit Report',401,'Master Vendor List','Master Vendor List','','IN','XXWC Supplier List','','VARCHAR2','N','Y','5','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Data Management Audit Report',401,'Organization List','Organization List','','IN','XXWC Org List','','VARCHAR2','N','Y','9','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Data Management Audit Report',401,'Master Vendor Number','Master Vendor Number','MASTER_VENDOR_NUMBER','IN','EIS_INV_VENDOR_NUM_LOV','','VARCHAR2','N','Y','4','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Data Management Audit Report',401,'Master Vendor Name','Master Vendor Name','SOURCING_VENDOR_NAME','IN','EIS_INV_VENDOR_NAME_LOV','','VARCHAR2','N','Y','14','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Data Management Audit Report',401,'Short Description','Short Description','SHORT_DESCRIPTION','LIKE','EIS_INV_ITEM_DESC_LOV','','VARCHAR2','N','Y','2','','N','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - Data Management Audit Report
xxeis.eis_rs_ins.rcn( 'Data Management Audit Report',401,'','','','','AND PROCESS_ID    = :SYSTEM.PROCESS_ID','Y','1','','SA059956');
--Inserting Report Sorts - Data Management Audit Report
xxeis.eis_rs_ins.rs( 'Data Management Audit Report',401,'ITEM','ASC','SA059956','','1');
xxeis.eis_rs_ins.rs( 'Data Management Audit Report',401,'ORG','ASC','SA059956','','2');
--Inserting Report Triggers - Data Management Audit Report
xxeis.eis_rs_ins.rt( 'Data Management Audit Report',401,'BEGIN

XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.SET_DATE_FROM(:Item Creation Date);

XXEIS.EIS_XXWC_DATA_MANG_RPT_PKG.GET_DATA_MANG_DTLS(
P_PROCESS_ID    => (:SYSTEM.PROCESS_ID),
                              P_ITEM_LIST   => :Item List,
                              P_MASTER_VENDOR_LIST    => :Master Vendor List,
                              P_ORGANIZATION_LIST => :Organization List,
                              P_SOURCE_LIST =>:Source List,
                              P_SHORT_DESC =>:Short Description,
                              P_Item_Number          =>:Item Number,
                              P_Master_Vendor_Number =>:Master Vendor Number,
                              P_Source 	      =>:Source,
                              P_Organization	=>:Organization,
                              P_Item_Status 	=>:Item Status,
                              P_User_Item_Type   =>:User Item Type, 
                              P_Website_Item         =>:Website Item,
                              P_Master_Vendor_Name  =>:Master Vendor Name
);

END;','B','Y','SA059956');
xxeis.eis_rs_ins.rt( 'Data Management Audit Report',401,'begin
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.parse_cleanup_table(:SYSTEM.PROCESS_ID);
XXEIS.EIS_XXWC_DATA_MANG_RPT_PKG.clear_temp_tables(:SYSTEM.PROCESS_ID);
end;','A','Y','SA059956');
--Inserting Report Templates - Data Management Audit Report
--Inserting Report Portals - Data Management Audit Report
--Inserting Report Dashboards - Data Management Audit Report
--Inserting Report Security - Data Management Audit Report
xxeis.eis_rs_ins.rsec( 'Data Management Audit Report','401','','50990',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Data Management Audit Report','201','','50983',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Data Management Audit Report','','SG019472','',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Data Management Audit Report','20005','','51207',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Data Management Audit Report','20005','','50900',401,'SA059956','','');
--Inserting Report Pivots - Data Management Audit Report
END;
/
set scan on define on
