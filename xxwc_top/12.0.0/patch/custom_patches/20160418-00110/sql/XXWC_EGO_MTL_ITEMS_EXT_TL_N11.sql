-----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_EGO_MTL_ITEMS_EXT_TL_N11
  File Name: XXWC_EGO_MTL_ITEMS_EXT_TL_N11.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        12-Apr-2016  Pramod        --TMS#20160418-00110 Data Management Audit Report - Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX APPS.XXWC_EGO_MTL_ITEMS_EXT_TL_N11 ON APPS.EGO_MTL_SY_ITEMS_EXT_TL 
(INVENTORY_ITEM_ID,ORGANIZATION_ID,ATTR_GROUP_ID,DATA_LEVEL_ID) TABLESPACE APPS_TS_TX_DATA
/
