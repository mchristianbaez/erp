--/**************************************************************************
--$FIle Name: XXWC_INV_PUBD_ONHAND_CTL.sql  $
--PURPOSE:   This is the control file. SQL Loader will
--           use to load the Item On Hand  information.
--REVISIONS:
-- Ver        Date         Author                  Description
-- ---------  -----------  ---------------         -------------------------
-- 1.0        28-Mar-2016  Rakesh Patel            TMS 20160107-00119-PUBD - On-Hand Conversion
--**************************************************************************/
LOAD DATA
APPEND INTO TABLE XXWC.XXWC_PUBD_ONHAND_CNV
FIELDS TERMINATED BY '|'
TRAILING NULLCOLS
( 
   ORGANIZATION_CODE                "LPAD(TRIM(:ORGANIZATION_CODE),3,'0')"
 , ITEM_SEGMENT                     "TRIM(:ITEM_SEGMENT)"
 , DESCRIPTION                      "TRIM(:DESCRIPTION)"    
 , GEN_QTY_OH                       "TRIM(:GEN_QTY_OH)"
 , BIN_1                            "TRIM(:BIN_1)"
 , BIN_2                            "TRIM(:BIN_2)"
 , BIN_3                            "TRIM(:BIN_3)"
 , GEN_QTY_STAY_GEN                 "TRIM(:GEN_QTY_STAY_GEN)"
 , GEN_QTY_MOVE_PUBD                "TRIM(:GEN_QTY_MOVE_PUBD)"
 , GEN_QTY_HAZWASTE                 "TRIM(:GEN_QTY_HAZWASTE)"
 , EXPIRED_PUBD                     "TRIM(:EXPIRED_PUBD)"
 , EXPIRED_HAZWASTE                 "TRIM(:EXPIRED_HAZWASTE)"
 , CREATED_BY                       CONSTANT "-1"
 , CREATION_DATE                    SYSDATE
 , LAST_UPDATED_BY                  CONSTANT "-1"
 , LAST_UPDATE_DATE                 SYSDATE
 )