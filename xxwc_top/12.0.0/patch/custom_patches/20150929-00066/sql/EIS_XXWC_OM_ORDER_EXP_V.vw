CREATE OR REPLACE FORCE VIEW "XXEIS"."EIS_XXWC_OM_ORDER_EXP_V" ("LOC", "EXCEPTION_RESAON", "ORDER_NUMBER", "ORDER_DATE", "EXCEPTION_DATE", "CUSTOMER_NAME", "ORDER_TYPE", "EXT_ORDER_TOTAL", "QTY", "EXCEPTION_DESCRIPTION", "SALES_PERSON_NAME", "CREATED_BY", "SHIP_METHOD")
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_OM_ORDER_EXP_V.vw $
  Module Name: Order Management
  PURPOSE: Orders Exception Report
  TMS Task Id :
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        NA    			NA		            Initial Version
  1.1  		11/05/2015	Mahender Reddy			TMS#20150929-00066
  **************************************************************************************************************/
 AS 
  SELECT /*+ INDEX(oh XXWC_OE_ORDER_HDR_AL_N2) index(ol XXWC_OE_ORDER_LN_AL_N3) */
         mp.organization_code loc
         ,'Order On Hold' exception_resaon
         ,oh.order_number order_number
         ,TRUNC (oh.ordered_date) order_date
         ,                                                                 --OHd.CREATION_DATE EXCEPTION_DATE,
          ooh.creation_date exception_date
         ,NVL (hca.account_name, hp.party_name) customer_name
         ,ottl.name order_type
         ,CASE
             WHEN xxeis.eis_rs_xxwc_com_util_pkg.get_line_id (ol.line_id) = 1
             THEN
                ROUND (
                   (  ROUND (
                           DECODE (ol.line_category_code, 'RETURN', -1, 1)
                         * NVL (ol.ordered_quantity, 0)
                         * NVL (ol.unit_selling_price, 0)
                        ,2)
                    + NVL (ol.tax_value, 0))
                  ,2)
             ELSE
                NULL
          END
             ext_order_total
         ,DECODE (ol.line_category_code, 'RETURN', -1, 1) * NVL (ol.ordered_quantity, 0) qty
         ,ohd.description exception_description
         ,rep.name sales_person_name
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_empolyee_name (fu.user_id, oh.creation_date) created_by
         ,NVL (flv.meaning, ol.shipping_method_code) ship_method
     FROM oe_order_headers oh
         ,oe_order_lines ol
         ,                                                                -- ORG_ORGANIZATION_DEFINITIONS OOD,
          mtl_parameters mp
         ,hz_cust_accounts hca
         ,hz_parties hp
         ,oe_transaction_types_vl ottl
         ,apps.oe_order_holds_all ooh
         ,oe_hold_sources ohs
         ,oe_hold_definitions ohd
         ,ra_salesreps rep
         ,                                                                               --  PER_PEOPLE_F PPF,
          fnd_user fu
         ,fnd_lookup_values flv
    WHERE     oh.header_id = ol.header_id
          AND ol.ship_from_org_id = mp.organization_id
          AND oh.order_type_id = ottl.transaction_type_id
          AND oh.salesrep_id = rep.salesrep_id(+)
          AND oh.org_id = rep.org_id(+)
          AND oh.sold_to_org_id = hca.cust_account_id
          AND ol.flow_status_code NOT IN ('CLOSED', 'CANCELLED')
          AND ohd.name NOT IN ('Pricing Guardrail Hold'
                              ,'Pricing Guardrail Hold - Limited User'
                              ,'XXWC_PRICE_CHANGE_HOLD')
          AND hca.party_id = hp.party_id
          AND ooh.header_id = oh.header_id
          AND ol.line_id = ooh.line_id(+)
          AND ohs.hold_source_id = ooh.hold_source_id
          AND ooh.hold_release_id IS NULL
          AND ohs.hold_id = ohd.hold_id
          AND fu.user_id = oh.created_by
          AND flv.lookup_type(+) = 'SHIP_METHOD'
          AND flv.lookup_code(+) = ol.shipping_method_code
   UNION ALL
   SELECT /*+ INDEX(oh XXWC_OE_ORDER_HDR_AL_N2) index(ol XXWC_OE_ORDER_LN_AL_N3) */
         mp.organization_code loc
         ,'Order On Hold' exception_resaon
         ,oh.order_number order_number
         ,TRUNC (oh.ordered_date) order_date
         ,                                                                 --OHd.CREATION_DATE EXCEPTION_DATE,
          ooh.creation_date exception_date
         ,NVL (hca.account_name, hp.party_name) customer_name
         ,ottl.name order_type
         ,CASE
             WHEN xxeis.eis_rs_xxwc_com_util_pkg.get_line_id (ol.line_id) = 1
             THEN
                ROUND (
                   (  ROUND (
                           DECODE (ol.line_category_code, 'RETURN', -1, 1)
                         * NVL (ol.ordered_quantity, 0)
                         * NVL (ol.unit_selling_price, 0)
                        ,2)
                    + NVL (ol.tax_value, 0))
                  ,2)
             ELSE
                NULL
          END
             ext_order_total
         ,DECODE (ol.line_category_code, 'RETURN', -1, 1) * NVL (ol.ordered_quantity, 0) qty
         ,ohd.description exception_description
         ,rep.name sales_person_name
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_empolyee_name (fu.user_id, oh.creation_date) created_by
         ,NVL (flv.meaning, ol.shipping_method_code) ship_method
     FROM oe_order_headers oh
         ,oe_order_lines ol
         ,                                                                -- ORG_ORGANIZATION_DEFINITIONS OOD,
          mtl_parameters mp
         ,hz_cust_accounts hca
         ,hz_parties hp
         ,oe_transaction_types_vl ottl
         ,apps.oe_order_holds_all ooh
         ,oe_hold_sources ohs
         ,oe_hold_definitions ohd
         ,ra_salesreps rep
         ,                                                                               --  PER_PEOPLE_F PPF,
          fnd_user fu
         ,fnd_lookup_values flv
    WHERE     oh.header_id = ol.header_id
          AND ol.ship_from_org_id = mp.organization_id
          AND oh.order_type_id = ottl.transaction_type_id
          AND oh.salesrep_id = rep.salesrep_id(+)
          AND oh.org_id = rep.org_id(+)
          AND oh.sold_to_org_id = hca.cust_account_id
          AND ol.flow_status_code NOT IN ('CLOSED', 'CANCELLED')
          AND ohd.name NOT IN ('Pricing Guardrail Hold'
                              ,'Pricing Guardrail Hold - Limited User'
                              ,'XXWC_PRICE_CHANGE_HOLD')
          AND hca.party_id = hp.party_id
          AND ooh.header_id = oh.header_id
          AND ooh.line_id IS NULL
          AND ohs.hold_source_id = ooh.hold_source_id
          AND ooh.hold_release_id IS NULL
          AND ohs.hold_id = ohd.hold_id
          AND fu.user_id = oh.created_by
          AND flv.lookup_type(+) = 'SHIP_METHOD'
          AND flv.lookup_code(+) = ol.shipping_method_code
   UNION ALL
   SELECT /*+ INDEX(ol XXWC_OE_ORDER_LN_AL_N3) */
         mp.organization_code loc
         ,CASE
             WHEN ol.flow_status_code = 'INVOICE_HOLD'               /* Invoice on hold added on 03-oct-2013*/
             THEN
                'Invoice on Hold'
             WHEN ottl.name = 'STANDARD ORDER' AND ol.flow_status_code = 'AWAITING_RECEIPT'
             THEN
                'Open Direct'
             WHEN ottl.name = 'RETURN ORDER' AND ol.flow_status_code = 'AWAITING_RETURN'
             THEN
                'Open RMA'
             WHEN ol.flow_status_code = 'PRE-BILLING_ACCEPTANCE'
             THEN
                'Pending Pre Billing'
             WHEN (ottl.name = 'COUNTER ORDER' AND ol.flow_status_code <> 'ENTERED') -- THEN 'Booked Not Closed'
             THEN
                'Counter Order > 1- Day'
             WHEN (ottl.name = 'COUNTER ORDER' AND ol.flow_status_code = 'ENTERED')
             THEN
                'Entered Not Booked'
             ELSE
                NULL
          END
             exception_resaon
         ,oh.order_number order_number
         ,TRUNC (oh.ordered_date) order_date
         ,CASE
             WHEN ol.flow_status_code = 'INVOICE_HOLD'
             /* Invoice on hold added on 03-oct-2013*/
             THEN
                TRUNC (oh.ordered_date)
             WHEN ottl.name = 'STANDARD ORDER' AND ol.flow_status_code = 'AWAITING_RECEIPT'
             THEN
                TRUNC (oh.ordered_date)
             WHEN ottl.name = 'RETURN ORDER' AND ol.flow_status_code = 'AWAITING_RETURN'
             THEN
                TRUNC (oh.ordered_date)
             WHEN ol.flow_status_code = 'PRE-BILLING_ACCEPTANCE'
             THEN
                TRUNC (ol.actual_shipment_date)
             WHEN (ottl.name = 'COUNTER ORDER' AND ol.flow_status_code <> 'ENTERED')
             THEN
                TRUNC (oh.ordered_date) + 1
             WHEN (ottl.name = 'COUNTER ORDER' AND ol.flow_status_code = 'ENTERED')
             THEN
                TRUNC (oh.ordered_date)
             ELSE
                NULL
          END
             exception_date
         ,NVL (hca.account_name, hp.party_name) customer_name
         ,ottl.name order_type
         ,ROUND (
             (  ROUND (
                     DECODE (ol.line_category_code, 'RETURN', -1, 1)
                   * NVL (ol.ordered_quantity, 0)
                   * NVL (ol.unit_selling_price, 0)
                  ,2)
              + NVL (ol.tax_value, 0))
            ,2)
             ext_order_total
         ,DECODE (ol.line_category_code, 'RETURN', -1, 1) * NVL (ol.ordered_quantity, 0) qty
         ,CASE
             WHEN ol.flow_status_code = 'INVOICE_HOLD'
             /* Invoice on hold added on 03-oct-2013*/
             THEN
                'Invoice Hold Applied - Review for Release'
             WHEN ottl.name = 'STANDARD ORDER' AND ol.flow_status_code = 'AWAITING_RECEIPT'
             THEN
                'Direct Ship waiting to be Received'
             WHEN ottl.name = 'RETURN ORDER' AND ol.flow_status_code = 'AWAITING_RETURN'
             THEN
                'RMA waiting to be Received'
             WHEN ol.flow_status_code = 'PRE-BILLING_ACCEPTANCE'
             THEN
                'Item waiting on Fulfillment Acceptance'
             WHEN (ottl.name = 'COUNTER ORDER' AND ol.flow_status_code <> 'ENTERED')
             THEN
                'Counter Order Needs Review'
             WHEN (ottl.name = 'COUNTER ORDER' AND ol.flow_status_code = 'ENTERED')
             THEN
                'Needs to be Reviewed and Booked'
             ELSE
                NULL
          END
             exception_description
         ,rep.name sales_person_name
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_empolyee_name (fu.user_id, oh.creation_date) created_by
         ,NVL (flv.meaning, ol.shipping_method_code) ship_method
     FROM oe_order_headers oh
         ,oe_order_lines ol
         ,mtl_parameters mp
         ,hz_cust_accounts hca
         ,hz_parties hp
         ,oe_transaction_types_vl ottl
         ,ra_salesreps rep
         ,                                                                                -- PER_PEOPLE_F PPF,
          fnd_user fu
         ,fnd_lookup_values flv
    WHERE     oh.header_id = ol.header_id
          AND ol.ship_from_org_id = mp.organization_id
          AND oh.order_type_id = ottl.transaction_type_id
          AND oh.salesrep_id = rep.salesrep_id                                                           --(+)
          AND oh.org_id = rep.org_id                                                                     --(+)
          AND oh.sold_to_org_id = hca.cust_account_id
          AND hca.party_id = hp.party_id
          AND fu.user_id = oh.created_by
          AND flv.lookup_type(+) = 'SHIP_METHOD'
          AND flv.lookup_code(+) = ol.shipping_method_code
          --and fu.employee_id      = ppf.person_id(+)
          -- AND TRUNC (oh.creation_date) BETWEEN NVL (ppf.effective_start_date, TRUNC (oh.creation_date) ) AND NVL (ppf.effective_end_date, TRUNC (oh.creation_date) )
          AND ol.flow_status_code NOT IN ('CLOSED', 'CANCELLED')
          AND ottl.name IN ('STANDARD ORDER'
                           ,'COUNTER ORDER'
                           ,'RETURN ORDER'
                           ,'REPAIR ORDER')
          AND (   (    ottl.name != 'COUNTER ORDER'
                   AND ol.flow_status_code IN ('AWAITING_RECEIPT'
                                              ,'AWAITING_RETURN'
                                              ,'PRE-BILLING_ACCEPTANCE'
                                              ,'INVOICE_HOLD'))          ----added 'INVOICE_HOLD' on 9/26/2013
               OR (ottl.name = 'COUNTER ORDER' AND TRUNC (SYSDATE) > TRUNC (ordered_date)))
          /* � Counter Orders that are not closed, due to a Hold, should only display once under Order On Hold*/
          AND NOT EXISTS
                     (SELECT DISTINCT ooh.header_id
                        FROM oe_order_headers ohe
                            ,oe_order_lines ole
                            ,apps.oe_order_holds_all ooh
                            ,oe_hold_sources ohs
                            ,oe_hold_definitions ohd
                            ,oe_transaction_types_vl ottl
                       WHERE     ohe.header_id = ole.header_id
                             AND ohe.header_id = oh.header_id
                             AND ole.line_id = ol.line_id
                             AND ooh.header_id = ohe.header_id
                             --and OLE.LINE_ID           = OOH.LINE_ID(+)
                             AND ohs.hold_source_id = ooh.hold_source_id
                             AND ooh.hold_release_id IS NULL
                             AND ohs.hold_id = ohd.hold_id
                             AND ohe.order_type_id = ottl.transaction_type_id
                             AND ottl.name = 'COUNTER ORDER')
								--Start Ver 1.1
            AND NOT EXISTS
                     (SELECT DISTINCT ooh.header_id
                        FROM oe_order_headers ohe
                            ,oe_order_lines ole
                            ,apps.oe_order_holds_all ooh
                            ,oe_hold_sources ohs
                            ,OE_HOLD_DEFINITIONS OHD
                       WHERE     OHE.HEADER_ID = OLE.HEADER_ID
                             AND OHE.HEADER_ID = oh.header_id
                             AND ole.line_id = ol.line_id
                             AND ooh.header_id = ohe.header_id
                             AND ohs.hold_source_id = ooh.hold_source_id
                             AND ooh.hold_release_id IS NULL
                             AND ohs.hold_id = ohd.hold_id
                             AND ohd.name IN ('Pricing Guardrail Hold'
                              ,'Pricing Guardrail Hold - Limited User'
                              ,'XXWC_PRICE_CHANGE_HOLD')
                              ) 
								--End Ver 1.1							 
   UNION ALL
   SELECT /*+ INDEX(oh XXWC_OE_ORDER_HDR_AL_N2) index(ol XXWC_OE_ORDER_LN_AL_N3) */
         mp.organization_code loc
         ,'Out for Delivery' exception_resaon
         ,oh.order_number order_number
         ,TRUNC (oh.ordered_date) order_date
         ,                                                  --xxeis.eis_rs_xxwc_com_util_pkg.yy_get_doc_date (
          -- oh.header_id,
          --  ol.line_id,
          ---  ol.inventory_item_id,
          --  ol.ship_from_org_id,
          --  oh.ship_from_org_id)
          (SELECT creation_date
             FROM apps.xxwc_wsh_shipping_stg
            WHERE header_id = oh.header_id AND line_id = ol.line_id)
             exception_date                                     --TMS#20140428-00163 by Mahender on 07-11-2014
         --wdd.creation_date exception_date
         , -- XXEIS.eis_rs_xxwc_com_util_pkg.get_doc_date_modified ( oh.header_id, ol.line_id, ol.inventory_item_id, ol.ship_from_org_id, oh.ship_from_org_id) exception_date,
          NVL (hca.account_name, hp.party_name) customer_name
         ,ottl.name order_type
         ,ROUND (
             (  ROUND (
                     DECODE (ol.line_category_code, 'RETURN', -1, 1)
                   * NVL (ol.ordered_quantity, 0)
                   * NVL (ol.unit_selling_price, 0)
                  ,2)
              + NVL (ol.tax_value, 0))
            ,2)
             ext_order_total
         ,DECODE (ol.line_category_code, 'RETURN', -1, 1) * NVL (ol.ordered_quantity, 0) qty
         ,'Item Shipped but Not Confirmed' exception_description
         ,rep.name sales_person_name
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_empolyee_name (fu.user_id, oh.creation_date) created_by
         ,NVL (flv.meaning, ol.shipping_method_code) ship_method
     FROM oe_order_headers oh
         ,oe_order_lines ol
         ,mtl_parameters mp
         ,hz_cust_accounts hca
         ,hz_parties hp
         ,oe_transaction_types_vl ottl
         ,ra_salesreps rep
         ,                                                                                -- PER_PEOPLE_F PPF,
          fnd_user fu
         ,fnd_lookup_values flv
         ,wsh_delivery_details wdd
         ,wsh_delivery_assignments wda
    WHERE     oh.header_id = ol.header_id
          AND ol.ship_from_org_id = mp.organization_id
          AND oh.order_type_id = ottl.transaction_type_id
          AND oh.salesrep_id = rep.salesrep_id(+)
          AND oh.org_id = rep.org_id
          AND oh.sold_to_org_id = hca.cust_account_id
          AND hca.party_id = hp.party_id
          AND fu.user_id = oh.created_by
          AND flv.lookup_type(+) = 'SHIP_METHOD'
          AND flv.lookup_code(+) = ol.shipping_method_code
          AND (ol.split_from_line_id IS NULL AND user_item_description != 'BACKORDERED')
          AND ol.flow_status_code NOT IN ('CLOSED', 'CANCELLED')
          AND ottl.name IN ('STANDARD ORDER', 'RETURN ORDER', 'REPAIR ORDER')
          -- AND ol.flow_status_code      = 'AWAITING_SHIPPING'
          AND ol.user_item_description = 'OUT_FOR_DELIVERY'
          AND wdd.source_header_id = ol.header_id
          AND wdd.source_line_id = ol.line_id
          AND wdd.inventory_item_id = ol.inventory_item_id
          AND wdd.organization_id = ol.ship_from_org_id
          AND wda.delivery_detail_id = wdd.delivery_detail_id
          --and oh.ORDER_NUMBER = 11826733
          AND ( (SYSDATE - wdd.creation_date) > 1)
/ 