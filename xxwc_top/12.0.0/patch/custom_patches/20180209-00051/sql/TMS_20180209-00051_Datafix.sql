/***********************************************************************************************************************************************
   NAME:       TMS_20180209-00051_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        02/09/2018  Rakesh Patel     TMS#20180209-00051
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Delete');
   
   DELETE FROM xxwc.XXWC_OM_DMS2_SIGN_CAPTURE_TBL
    WHERE ORDER_NUMBER IN ( '26084794', '26084771', '26084789', '26085684', '26092384', '26092334');

   DBMS_OUTPUT.put_line ('Records deleted -' || SQL%ROWCOUNT);
   COMMIT;
   
  EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to detele record ' || SQLERRM);
	  ROLLBACK;
END;
/