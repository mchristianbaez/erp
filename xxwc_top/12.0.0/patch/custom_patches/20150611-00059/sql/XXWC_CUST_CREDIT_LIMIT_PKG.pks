--
-- XXWC_CUST_CREDIT_LIMIT_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.XXWC_CUST_CREDIT_LIMIT_PKG
AS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_CUST_CREDIT_LIMIT_PKG.pks $
   *   Module Name: XXWC_CUST_CREDIT_LIMIT_PKG.pks
   *
   *   PURPOSE:   This package is used by the workflow to create profile class and amount
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   *   1.1        09/04/2015  Manjula Chellappan        TMS# 20150611-00059 - Customer - Profile and Profile Amount tab not populating timely
   * ************************************************************************/

   --Define variables for logging debug messages
   g_level_unexpected   CONSTANT NUMBER := 6;
   g_level_error        CONSTANT NUMBER := 5;
   g_level_exception    CONSTANT NUMBER := 4;
   g_LEVEL_EVENT        CONSTANT NUMBER := 3;
   g_LEVEL_PROCEDURE    CONSTANT NUMBER := 2;
   g_LEVEL_STATEMENT    CONSTANT NUMBER := 1;

   /*************************************************************************
   *   Procedure : LOG_MSG
   *
   *   PURPOSE:   This procedure is the table handler called by the On-Insert Trigger
   *   Parameter:
   *          IN
   *              p_debug_level    -- Debug Level
   *              p_mod_name       -- Module Name
   *              p_debug_msg      -- Debug Message
   * ************************************************************************/
   PROCEDURE LOG_MSG (p_debug_level   IN NUMBER
                     ,p_mod_name      IN VARCHAR2
                     ,p_debug_msg     IN VARCHAR2);



   /*************************************************************************
   *   Procedure : CREATE_PROFILE
   *
   *   PURPOSE:   This procedure is called from the CREATE_PROFILE_WF procedure
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   * ************************************************************************/
   PROCEDURE CREATE_PROFILE (p_site_use_id         IN            NUMBER
                            ,p_cust_acct_site_id   IN            NUMBER
                            ,p_user_id             IN            NUMBER
                            ,x_return_status          OUT NOCOPY VARCHAR2
                            ,x_msg_count              OUT NOCOPY NUMBER
                            ,x_msg_data               OUT NOCOPY VARCHAR2);

   /*************************************************************************
   *   Procedure : CREATE_PROFILE_WF
   *
   *   PURPOSE:   This procedure is called from the workflow from the function node
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   * ************************************************************************/
   PROCEDURE CREATE_PROFILE_WF (itemtype    IN            VARCHAR2
                               ,itemkey     IN            VARCHAR2
                               ,actid       IN            NUMBER
                               ,funcmode    IN            VARCHAR2
                               ,resultout   IN OUT NOCOPY VARCHAR2);

   /*************************************************************************
   *   Procedure : SET_ATTR
   *
   *   PURPOSE:   This procedure is called from the workflow to set the attributes
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   * ************************************************************************/
   PROCEDURE SET_ATTR (itemtype    IN            VARCHAR2
                      ,itemkey     IN            VARCHAR2
                      ,actid       IN            NUMBER
                      ,funcmode    IN            VARCHAR2
                      ,resultout   IN OUT NOCOPY VARCHAR2);



   /*************************************************************************
   *   Procedure : ASSIGN_CREDIT_USAGE
   *
   *   PURPOSE:   This procedure is called from the CREATE_PROFILE_WF procedure
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   * ************************************************************************/
   PROCEDURE ASSIGN_CREDIT_USAGE (
      p_cust_account_id            IN            NUMBER
     ,p_cust_acct_profile_amt_id   IN            NUMBER
     ,x_return_status                 OUT NOCOPY VARCHAR2
     ,x_msg_count                     OUT NOCOPY NUMBER
     ,x_msg_data                      OUT NOCOPY VARCHAR2);

   /*************************************************************************
   *   Procedure : ASSIGN_CREDIT_USAGE_WF
   *
   *   PURPOSE:   This procedure is called from the workflow from the function node
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   * ************************************************************************/
   PROCEDURE ASSIGN_CREDIT_USAGE_WF (itemtype    IN            VARCHAR2
                                    ,itemkey     IN            VARCHAR2
                                    ,actid       IN            NUMBER
                                    ,funcmode    IN            VARCHAR2
                                    ,resultout   IN OUT NOCOPY VARCHAR2);

   /*************************************************************************
   *   Procedure : SET_ATTRIBUTES
   *
   *   PURPOSE:   This procedure is called from the workflow to set the attributes
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   * ************************************************************************/
   PROCEDURE SET_ATTRIBUTES (itemtype    IN            VARCHAR2
                            ,itemkey     IN            VARCHAR2
                            ,actid       IN            NUMBER
                            ,funcmode    IN            VARCHAR2
                            ,resultout   IN OUT NOCOPY VARCHAR2);

   /*************************************************************************
   *   Procedure : GET_SALESREP_ID
   *
   *   PURPOSE:   This function is  get the primary salesrep id from site Use Id
   *   Parameter:
   *          IN
   *              p_cust_acct_site_id    -- Site Use Id
   * ************************************************************************/
   FUNCTION GET_SALESREP_ID (p_cust_account_id IN NUMBER)
      RETURN NUMBER;

   /*************************************************************************
   *   Procedure : GET_FREIGHT_TERM
   *
   *   PURPOSE:   This function is  get the freight term from site Use Id
   *   Parameter:
   *          IN
   *              p_cust_acct_site_id    -- Site Use Id
   * ************************************************************************/
   FUNCTION GET_FREIGHT_TERM (p_cust_acct_site_id IN NUMBER)
      RETURN VARCHAR2;

   /*************************************************************************
   *   Procedure : CHECK_SITE_IS_BILLTO
   *
   *   PURPOSE:   This procedure is called from the workflow to check if the site is Bill To
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   * ************************************************************************/
   PROCEDURE CHECK_SITE_IS_BILLTO (itemtype    IN            VARCHAR2
                                  ,itemkey     IN            VARCHAR2
                                  ,actid       IN            NUMBER
                                  ,funcmode    IN            VARCHAR2
                                  ,resultout   IN OUT NOCOPY VARCHAR2);

   /*************************************************************************
   *   Procedure : CHECK_SITE_IS_JOB_SITE
   *
   *   PURPOSE:   This procedure is called from the workflow to check if the site is Job Site
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   * ************************************************************************/
   PROCEDURE CHECK_SITE_IS_JOB_SITE (itemtype    IN            VARCHAR2
                                    ,itemkey     IN            VARCHAR2
                                    ,actid       IN            NUMBER
                                    ,funcmode    IN            VARCHAR2
                                    ,resultout   IN OUT NOCOPY VARCHAR2);

   /*************************************************************************
   *   Procedure : UPDATE_GSA_FLAG
   *
   *   PURPOSE:   This procedure is called from the workflow to update the GSA Flag
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   * ************************************************************************/
   PROCEDURE UPDATE_GSA_FLAG (itemtype    IN            VARCHAR2
                             ,itemkey     IN            VARCHAR2
                             ,actid       IN            NUMBER
                             ,funcmode    IN            VARCHAR2
                             ,resultout   IN OUT NOCOPY VARCHAR2);

   /*************************************************************************
   *   Procedure : UPDATE_SHIP_T0_SITE
   *
   *   PURPOSE:   This procedure is called from the workflow to update the ship to site
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   * ************************************************************************/
   PROCEDURE UPDATE_SHIP_TO_SITE (itemtype    IN            VARCHAR2
                                 ,itemkey     IN            VARCHAR2
                                 ,actid       IN            NUMBER
                                 ,funcmode    IN            VARCHAR2
                                 ,resultout   IN OUT NOCOPY VARCHAR2);

--Added for Revision 1.1								 
   /*************************************************************************
   *   Procedure : SUBMIT_WF_PROCESS
   *
   *   PURPOSE:   This procedure is called from Trigger XXWC_HZ_CUST_SITE_USES_BIR_TRG
   *   Parameter: None
   * ************************************************************************/
   PROCEDURE SUBMIT_WF_PROCESS;
   
END XXWC_CUST_CREDIT_LIMIT_PKG;
/

