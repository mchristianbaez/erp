--
-- XXWC_CUST_CREDIT_LIMIT_PKG  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY APPS.XXWC_CUST_CREDIT_LIMIT_PKG
AS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_CUST_CREDIT_LIMIT_PKG.pkb $
   *   Module Name: XXWC_CUST_CREDIT_LIMIT_PKG.pkb
   *
   *   PURPOSE:   This package is used by the workflow to create profile class and amount
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        09/01/2011  Vivek Lakaman             Initial Version
   *   1.1		  24/09/2014  Veera C					TMS# 20141001-00163 Modified code as per the Canada OU Test
   *   1.2        09/04/2015  Manjula Chellappan        TMS# 20150611-00059 - Customer - Profile and Profile Amount tab not populating timely   
   * ************************************************************************/

   /*************************************************************************
    *   Procedure Name: LOG_MSG
    *
    *************************************************************************
    *   Purpose       : To write the debug message to fnd debug table
    *   Parameter:
    *       IN
    *            p_debug_level    -Debug Level
    *            p_mod_name       -Module Name
    *            p_debug_msg      -Debug Message
    * ***********************************************************************/

   PROCEDURE LOG_MSG (p_debug_level   IN NUMBER
                     ,p_mod_name      IN VARCHAR2
                     ,p_debug_msg     IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      --IF (p_debug_level >= FND_LOG.G_CURRENT_RUNTIME_LEVEL)
      --THEN
      FND_LOG.STRING (p_debug_level, p_mod_name, p_debug_msg);
      --END IF;

      COMMIT;
   END LOG_MSG;

   /*************************************************************************
    *   Procedure Name: init_apps
    *
    *************************************************************************
    *   Purpose       : To Initialize the application
    *   Parameter:
    *       IN
    *            p_user_id        -User Id
    * ***********************************************************************/

   PROCEDURE INIT_APPS (p_user_id IN NUMBER)
   IS
      l_org_id   NUMBER := fnd_profile.VALUE ('ORG_ID');
   BEGIN
      fnd_global.apps_initialize (user_id             => p_user_id
                                 ,resp_id             => fnd_global.resp_id
                                 ,resp_appl_id        => fnd_global.resp_appl_id
                                 ,security_group_id   => 0);
      mo_global.set_policy_context ('S', l_org_id);
   --arp_global.init_global;
   --mo_global.init ('AR');
   END INIT_APPS;


   /*************************************************************************
   *   Procedure : CREATE_PROFILE
   *
   *   PURPOSE:   This procedure is called from the CREATE_PROFILE_WF procedure
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   *    SATISH U :  19-APR-2012 :  Default Credit Limit do not assign 0 value when Credit Limit is NULL
   * ************************************************************************/
   PROCEDURE CREATE_PROFILE (p_site_use_id         IN            NUMBER
                            ,p_cust_acct_site_id   IN            NUMBER
                            ,p_user_id             IN            NUMBER
                            ,x_return_status          OUT NOCOPY VARCHAR2
                            ,x_msg_count              OUT NOCOPY NUMBER
                            ,x_msg_data               OUT NOCOPY VARCHAR2)
   IS
      l_customer_profile_rec         hz_customer_profile_v2pub.customer_profile_rec_type;
      v_customer_profile_amt         hz_customer_profile_v2pub.cust_profile_amt_rec_type;

      l_cust_created_by_module       hz_cust_accounts_all.created_by_module%TYPE;

      lv_profile_class_amount_id     NUMBER;
      lv_cust_profile_id             NUMBER;
      lv_profile_class_id            NUMBER;

      l_cust_account_id              NUMBER;
      x_cust_site_profile_id         NUMBER;
      l_error_msg                    VARCHAR2 (2000);
      --l_profile_class_id             NUMBER;
      v_cust_act_prof_amt_id         NUMBER;
      l_cust_account_profile_id      NUMBER;
      l_default_credit_limit         NUMBER;
      l_trx_credit_limit             NUMBER;
      l_min_dunning_amount           NUMBER;
      l_min_dunning_invoice_amount   NUMBER;
      l_min_statement_amount         NUMBER;
      l_min_receipt_amount           NUMBER;
      l_credit_hold                  VARCHAR2 (1);
      l_credit_class                 FND_LOOKUP_VALUES_VL.meaning%TYPE;
      l_profile_name                 VARCHAR2 (30);
      l_risk_matrix_meaning          VARCHAR2 (100);
      l_mod_name                     VARCHAR2 (200)
         := 'xxwc_cust_credit_limit_pkg.create_profile :';


      CURSOR Cur_cust_prof (p_cust_account_id IN NUMBER)
      IS
         SELECT *
           FROM hz_customer_profiles
          WHERE cust_account_id = p_cust_account_id AND site_use_id IS NULL;

      l_cust_acct_profile            hz_customer_profiles%ROWTYPE;

      l_StartTime                    NUMBER;
      l_EndTime                      NUMBER;


      --Define user defined exception
      API_ERROR                      EXCEPTION;
      VALIDATION_ERROR               EXCEPTION;
   BEGIN
      --set the context
      --LOG_MSG (g_level_STATEMENT, l_mod_name,'Initializing application');
      --init_apps (p_user_id);

      LOG_MSG (g_level_STATEMENT
              ,l_mod_name
              ,'p_site_use_id =' || p_site_use_id);
      LOG_MSG (g_level_STATEMENT
              ,l_mod_name
              ,'p_cust_acct_site_id =' || p_cust_acct_site_id);

      l_StartTime := DBMS_UTILITY.get_time;

      BEGIN
         SELECT hcas.cust_account_id
           INTO l_cust_account_id
           FROM hz_cust_acct_sites_all hcas
          WHERE cust_acct_site_id = p_cust_acct_site_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_cust_account_id := NULL;
            x_msg_data := 'Error while getting cust_acct_id :' || SQLERRM;
            LOG_MSG (g_level_error, l_mod_name, x_msg_data);
            RAISE VALIDATION_ERROR;
      END;

      LOG_MSG (g_level_STATEMENT
              ,l_mod_name
              ,'l_cust_account_id =' || l_cust_account_id);

      OPEN Cur_cust_prof (l_cust_account_id);

      FETCH Cur_cust_prof INTO l_cust_acct_profile;

      IF Cur_cust_prof%NOTFOUND
      THEN
         x_msg_data := 'Error while getting cust_acct_id :' || SQLERRM;
         LOG_MSG (g_level_error, l_mod_name, x_msg_data);
         RAISE VALIDATION_ERROR;
      END IF;

      IF Cur_cust_prof%ISOPEN
      THEN
         CLOSE Cur_cust_prof;
      END IF;

      LOG_MSG (
         g_level_error
        ,l_mod_name
        ,   'l_cust_acct_profile.profile_class_id ='
         || l_cust_acct_profile.profile_class_id);

      BEGIN
         SELECT UPPER (name)
           INTO l_profile_name
           FROM hz_cust_profile_classes
          WHERE profile_class_id = l_cust_acct_profile.profile_class_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            x_msg_data :=
               'Error while getting Profile Class Name :' || SQLERRM;
            LOG_MSG (g_level_error, l_mod_name, x_msg_data);
            RAISE VALIDATION_ERROR;
      END;


      BEGIN
         SELECT created_by_module
           INTO l_cust_created_by_module
           FROM hz_cust_accounts_all  
		   WHERE cust_account_id = l_cust_account_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            x_msg_data := 'Error while getting created_by_module :' || SQLERRM;
            LOG_MSG (g_level_error, l_mod_name, x_msg_data);
            RAISE VALIDATION_ERROR;
      END;

      --Put the customer site on credit hold
      --if profile class is DEFAULT and not created from
      --ONT_UI_ADD_CUSTOMER UI
      IF l_profile_name = 'DEFAULT'
      THEN
         IF l_cust_created_by_module = 'ONT_UI_ADD_CUSTOMER'
         THEN
            l_credit_hold := 'N';
         ELSE
            l_credit_hold := 'Y';
         END IF;
      ELSE
         l_credit_hold := 'N';
      END IF;

      l_customer_profile_rec.cust_account_id := l_cust_account_id;
      l_customer_profile_rec.site_use_id := p_site_use_id;
      l_customer_profile_rec.created_by_module := 'ONT_UI_ADD_CUSTOMER';
      l_customer_profile_rec.account_status :=
         l_cust_acct_profile.account_status;
      l_customer_profile_rec.profile_class_id :=
         l_cust_acct_profile.profile_class_id;
      l_customer_profile_rec.collector_id := l_cust_acct_profile.collector_id;
      l_customer_profile_rec.credit_analyst_id :=
         l_cust_acct_profile.credit_analyst_id;
      l_customer_profile_rec.credit_checking :=
         l_cust_acct_profile.credit_checking;
      l_customer_profile_rec.next_credit_review_date :=
         l_cust_acct_profile.next_credit_review_date;
      l_customer_profile_rec.tolerance := l_cust_acct_profile.tolerance;
      l_customer_profile_rec.discount_terms :=
         l_cust_acct_profile.discount_terms;
      l_customer_profile_rec.next_credit_review_date :=
         l_cust_acct_profile.next_credit_review_date;
      l_customer_profile_rec.dunning_letters :=
         l_cust_acct_profile.dunning_letters;
      l_customer_profile_rec.interest_charges :=
         l_cust_acct_profile.interest_charges;
      l_customer_profile_rec.send_statements :=
         l_cust_acct_profile.send_statements;
      l_customer_profile_rec.credit_balance_statements :=
         l_cust_acct_profile.credit_balance_statements;
      l_customer_profile_rec.credit_hold := l_credit_hold;
      l_customer_profile_rec.credit_rating :=
         l_cust_acct_profile.credit_rating;
      l_customer_profile_rec.risk_code := l_cust_acct_profile.risk_code;
      l_customer_profile_rec.standard_terms :=
         l_cust_acct_profile.standard_terms;
      l_customer_profile_rec.override_terms :=
         l_cust_acct_profile.override_terms;
      l_customer_profile_rec.dunning_letter_set_id :=
         l_cust_acct_profile.dunning_letter_set_id;
      l_customer_profile_rec.interest_period_days :=
         l_cust_acct_profile.interest_period_days;
      l_customer_profile_rec.payment_grace_days :=
         l_cust_acct_profile.payment_grace_days;
      l_customer_profile_rec.discount_grace_days :=
         l_cust_acct_profile.discount_grace_days;
      l_customer_profile_rec.statement_cycle_id :=
         l_cust_acct_profile.statement_cycle_id;
      l_customer_profile_rec.percent_collectable :=
         l_cust_acct_profile.percent_collectable;
      l_customer_profile_rec.autocash_hierarchy_id :=
         l_cust_acct_profile.autocash_hierarchy_id;
      l_customer_profile_rec.auto_rec_incl_disputed_flag :=
         l_cust_acct_profile.auto_rec_incl_disputed_flag;
      l_customer_profile_rec.tax_printing_option :=
         l_cust_acct_profile.tax_printing_option;
      l_customer_profile_rec.charge_on_finance_charge_flag :=
         l_cust_acct_profile.charge_on_finance_charge_flag;
      l_customer_profile_rec.grouping_rule_id :=
         l_cust_acct_profile.grouping_rule_id;
      l_customer_profile_rec.clearing_days :=
         l_cust_acct_profile.clearing_days;
      l_customer_profile_rec.cons_inv_flag :=
         l_cust_acct_profile.cons_inv_flag;
      l_customer_profile_rec.cons_inv_type :=
         l_cust_acct_profile.cons_inv_type;
      l_customer_profile_rec.autocash_hierarchy_id_for_adr :=
         l_cust_acct_profile.autocash_hierarchy_id_for_adr;
      l_customer_profile_rec.lockbox_matching_option :=
         l_cust_acct_profile.lockbox_matching_option;
      l_customer_profile_rec.review_cycle := l_cust_acct_profile.review_cycle;
      l_customer_profile_rec.party_id := l_cust_acct_profile.party_id;
      l_customer_profile_rec.late_charge_calculation_trx :=
         l_cust_acct_profile.late_charge_calculation_trx;
      l_customer_profile_rec.credit_classification :=
         l_cust_acct_profile.credit_classification;
      l_customer_profile_rec.cons_bill_level :=
         l_cust_acct_profile.cons_bill_level;
      l_customer_profile_rec.late_charge_calculation_trx :=
         l_cust_acct_profile.late_charge_calculation_trx;
      l_customer_profile_rec.credit_items_flag :=
         l_cust_acct_profile.credit_items_flag;
      l_customer_profile_rec.disputed_transactions_flag :=
         l_cust_acct_profile.disputed_transactions_flag;
      l_customer_profile_rec.late_charge_type :=
         l_cust_acct_profile.late_charge_type;
      l_customer_profile_rec.late_charge_term_id :=
         l_cust_acct_profile.late_charge_term_id;
      l_customer_profile_rec.interest_calculation_period :=
         l_cust_acct_profile.interest_calculation_period;
      l_customer_profile_rec.hold_charged_invoices_flag :=
         l_cust_acct_profile.hold_charged_invoices_flag;
      l_customer_profile_rec.message_text_id :=
         l_cust_acct_profile.message_text_id;
      l_customer_profile_rec.multiple_interest_rates_flag :=
         l_cust_acct_profile.multiple_interest_rates_flag;
      l_customer_profile_rec.charge_begin_date :=
         l_cust_acct_profile.charge_begin_date;
      l_customer_profile_rec.automatch_set_id :=
         l_cust_acct_profile.automatch_set_id;

      l_customer_profile_rec.attribute2 := l_cust_acct_profile.attribute2; --Invoice Remit To Addre

      l_customer_profile_rec.attribute3 := l_cust_acct_profile.attribute3; --Statement by Job

      LOG_MSG (g_level_STATEMENT
              ,l_mod_name
              ,' Calling create_customer_profile API');
      hz_customer_profile_v2pub.create_customer_profile (
         p_init_msg_list             => FND_API.G_TRUE
        ,p_customer_profile_rec      => l_customer_profile_rec
        ,p_create_profile_amt        => FND_API.G_FALSE
        ,x_cust_account_profile_id   => x_cust_site_profile_id
        ,x_return_status             => x_return_status
        ,x_msg_count                 => x_msg_count
        ,x_msg_data                  => x_msg_data);


      LOG_MSG (
         g_level_STATEMENT
        ,l_mod_name
        ,' x_return_status(create_customer_profile)=' || x_return_status);
      LOG_MSG (g_level_STATEMENT
              ,l_mod_name
              ,' x_cust_site_profile_id=' || x_cust_site_profile_id);

      IF x_return_status <> fnd_api.g_ret_sts_success
      THEN
         LOG_MSG (g_level_error
                 ,l_mod_name
                 ,' create_customer_profile API Failed');

         FOR i IN 1 .. x_msg_count
         LOOP
            l_error_msg := fnd_msg_pub.get (p_msg_index => i, p_encoded => 'F');
            LOG_MSG (
               g_level_error
              ,l_mod_name
              ,' Error from create_customer_profile API=' || l_error_msg);
            x_msg_data := x_msg_data || l_error_msg;
         END LOOP;

         RAISE API_ERROR;
      END IF;

      BEGIN
         SELECT TO_NUMBER (attribute1)
               ,trx_credit_limit
               ,min_dunning_amount
               ,min_dunning_invoice_amount
               ,min_statement_amount
               ,auto_rec_min_receipt_amount
           INTO l_default_credit_limit
               ,l_trx_credit_limit
               ,l_min_dunning_amount
               ,l_min_dunning_invoice_amount
               ,l_min_statement_amount
               ,l_min_receipt_amount
           FROM hz_cust_profile_amts
          WHERE     cust_account_profile_id =
                       l_cust_acct_profile.cust_account_profile_id
                AND cust_account_id = l_cust_account_id
                AND currency_code = 'USD';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_default_credit_limit := NULL;
            x_msg_data :=
                  'Error while getting default credit limit for the cust account:'
               || SQLERRM;
            LOG_MSG (g_level_error, l_mod_name, x_msg_data);
            RAISE VALIDATION_ERROR;
      END;

      LOG_MSG (
         g_level_statement
        ,l_mod_name
        ,'l_default_credit_limit (From DFF)=' || l_default_credit_limit);

      IF l_default_credit_limit IS NULL
      THEN
         LOG_MSG (
            g_level_statement
           ,l_mod_name
           ,   'l_cust_acct_profile.credit_classification ='
            || l_cust_acct_profile.credit_classification);

         BEGIN
            SELECT LTRIM (UPPER (meaning))
              INTO l_credit_class
              FROM FND_LOOKUP_VALUES_VL
             WHERE     lookup_type = 'AR_CMGT_CREDIT_CLASSIFICATION'
                   AND lookup_code =
                          l_cust_acct_profile.credit_classification;
         EXCEPTION
            WHEN OTHERS
            THEN
               x_msg_data :=
                  'Error while getting Credit Classification :' || SQLERRM;
               LOG_MSG (g_level_error, l_mod_name, x_msg_data);
               RAISE VALIDATION_ERROR;
         END;

         LOG_MSG (g_level_statement
                 ,l_mod_name
                 ,'l_credit_class =' || l_credit_class);

         l_risk_matrix_meaning := l_profile_name || '-' || l_credit_class;

         LOG_MSG (g_level_statement
                 ,l_mod_name
                 ,'l_risk_matrix_meaning =' || l_risk_matrix_meaning);

         BEGIN
            -- Satish U: 19-APR-2012 : DO not assign 0 for Credit limit
            --SELECT   NVL(TO_NUMBER (tag),0)
            SELECT TO_NUMBER (tag)
              INTO l_default_credit_limit
              FROM fnd_lookup_values_vl
             WHERE     lookup_type = 'XXWC_RISK_CREDIT_MATRIX'
                   AND UPPER (meaning) = UPPER (l_risk_matrix_meaning);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               -- Satish U: 19-APR-2012 : DO not assign 0 for Credit limit
               -- l_default_credit_limit := 0;
               l_default_credit_limit := NULL;
            WHEN OTHERS
            THEN
               x_msg_data :=
                     'Error while getting Default Credit Limit from Risk Matrix :'
                  || SQLERRM;
               LOG_MSG (g_level_error, l_mod_name, x_msg_data);
               RAISE VALIDATION_ERROR;
         END;


         LOG_MSG (
            g_level_error
           ,l_mod_name
           ,   'l_default_credit_limit (After Risk Credit matrix)='
            || l_default_credit_limit);
      END IF;


      IF l_default_credit_limit IS NULL
      THEN
         x_msg_data := 'Default Credit Limit Cannot be derived';
         LOG_MSG (g_level_error, l_mod_name, x_msg_data);
      -- Satsih U: 19-APR-2012  Do not Raise Exception
      -- RAISE VALIDATION_ERROR;
      END IF;

      -- Create Customer Profile Amount Record
      --v_customer_profile_amt := NULL;
      v_customer_profile_amt.cust_account_profile_id := x_cust_site_profile_id;
      v_customer_profile_amt.cust_account_id := l_cust_account_id;
      v_customer_profile_amt.currency_code := 'USD';
      v_customer_profile_amt.attribute1 := l_default_credit_limit;
      v_customer_profile_amt.overall_credit_limit := l_default_credit_limit;
      v_customer_profile_amt.auto_rec_min_receipt_amount :=
         l_min_receipt_amount;
      v_customer_profile_amt.min_dunning_invoice_amount :=
         l_min_dunning_invoice_amount;
      v_customer_profile_amt.trx_credit_limit := l_trx_credit_limit;
      v_customer_profile_amt.min_dunning_amount := l_min_dunning_amount;
      v_customer_profile_amt.min_statement_amount := l_min_statement_amount;
      v_customer_profile_amt.site_use_id := p_site_use_id;
      v_customer_profile_amt.created_by_module := 'ONT_UI_ADD_CUSTOMER';


      LOG_MSG (g_level_STATEMENT
              ,l_mod_name
              ,' Calling create_cust_profile_amt API');

      HZ_CUSTOMER_PROFILE_V2PUB.create_cust_profile_amt (
         p_init_msg_list              => 'T'
        ,p_check_foreign_key          => FND_API.G_TRUE
        ,p_cust_profile_amt_rec       => v_customer_profile_amt
        ,x_cust_acct_profile_amt_id   => v_cust_act_prof_amt_id
        ,x_return_status              => x_return_status
        ,x_msg_count                  => x_msg_count
        ,x_msg_data                   => x_msg_data);

      LOG_MSG (
         g_level_STATEMENT
        ,l_mod_name
        ,' x_return_status(create_cust_profile_amt)=' || x_return_status);

      IF x_return_status <> fnd_api.g_ret_sts_success
      THEN
         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,' create_cust_profile_amt Failed');

         FOR i IN 1 .. x_msg_count
         LOOP
            l_error_msg := fnd_msg_pub.get (p_msg_index => i, p_encoded => 'F');
            x_msg_data := x_msg_data || l_error_msg;
            LOG_MSG (
               g_level_error
              ,l_mod_name
              ,' Error from create_cust_profile_amt API=' || l_error_msg);
         END LOOP;

         RAISE API_ERROR;
      END IF;

      LOG_MSG (
         g_level_error
        ,l_mod_name
        ,   'Getting profile_class_amount_id for the profile_class_id ='
         || l_cust_acct_profile.profile_class_id);

      BEGIN
         SELECT profile_class_amount_id
           INTO lv_profile_class_amount_id
           FROM hz_cust_prof_class_amts
          WHERE profile_class_id = l_cust_acct_profile.profile_class_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            x_msg_data :=
               'Error while getting Profile Class Amount Id :' || SQLERRM;
            LOG_MSG (g_level_error, l_mod_name, x_msg_data);
            RAISE VALIDATION_ERROR;
      END;


      LOG_MSG (g_level_STATEMENT
              ,l_mod_name
              ,'calling cascade_credit_usage_rules');
      LOG_MSG (g_level_STATEMENT
              ,l_mod_name
              ,'v_cust_act_prof_amt_id =' || v_cust_act_prof_amt_id);
      LOG_MSG (g_level_STATEMENT
              ,l_mod_name
              ,'lv_profile_class_amount_id=' || lv_profile_class_amount_id);

      hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
         p_cust_acct_profile_amt_id   => v_cust_act_prof_amt_id
        ,p_cust_profile_id            => lv_cust_profile_id
        ,p_profile_class_amt_id       => lv_profile_class_amount_id
        ,p_profile_class_id           => lv_profile_class_id
        ,x_return_status              => x_return_status
        ,x_msg_count                  => x_msg_count
        ,x_msg_data                   => x_msg_data);

      LOG_MSG (
         g_level_STATEMENT
        ,l_mod_name
        ,'x_return_status (cascade_credit_usage_rules)=' || x_return_status);

      l_EndTime := DBMS_UTILITY.get_time;
      LOG_MSG (
         g_level_STATEMENT
        ,l_mod_name
        ,'Time Elapsed (In Secs) =>' || (l_EndTime - l_StartTime) / 100);
   EXCEPTION
      WHEN VALIDATION_ERROR
      THEN
         --ROLLBACK;
         IF Cur_cust_prof%ISOPEN
         THEN
            CLOSE Cur_cust_prof;
         END IF;

         x_return_status := 'E';
      WHEN API_ERROR
      THEN
         --ROLLBACK;
         x_return_status := 'E';
      WHEN OTHERS
      THEN
         --ROLLBACK;
         x_return_status := 'E';
         x_msg_data := SQLERRM;
         x_msg_count := 1;
   END CREATE_PROFILE;

   /*************************************************************************
   *   Procedure : CREATE_PROFILE_WF
   *
   *   PURPOSE:   This procedure is called from the workflow from the function node
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   * ************************************************************************/

   PROCEDURE CREATE_PROFILE_WF (itemtype    IN            VARCHAR2
                               ,itemkey     IN            VARCHAR2
                               ,actid       IN            NUMBER
                               ,funcmode    IN            VARCHAR2
                               ,resultout   IN OUT NOCOPY VARCHAR2)
   IS
      l_site_use_id         NUMBER;
      l_cust_acct_site_id   NUMBER;
      l_user_id             NUMBER;
      lx_return_status      VARCHAR2 (10);
      lx_msg_count          NUMBER;
      l_credit_analyst      NUMBER;
      lx_msg_data           VARCHAR2 (2000);
      l_mod_name            VARCHAR2 (200)
         := 'xxwc_cust_credit_limit_pkg.create_profile_wf :';

      API_ERROR             EXCEPTION;
   BEGIN
      -- start data fix project
      OE_STANDARD_WF.Set_Msg_Context (actid);

      IF (funcmode = 'RUN')
      THEN
         l_site_use_id :=
            TO_NUMBER (
               wf_engine.getitemattrtext (itemtype   => itemtype
                                         ,itemkey    => itemkey
                                         ,aname      => 'XXWC_SITE_USE_ID'));

         l_cust_acct_site_id :=
            TO_NUMBER (
               wf_engine.getitemattrtext (
                  itemtype   => itemtype
                 ,itemkey    => itemkey
                 ,aname      => 'XXWC_CUST_ACCT_SITE_ID'));

         l_user_id :=
            TO_NUMBER (
               wf_engine.getitemattrtext (itemtype   => itemtype
                                         ,itemkey    => itemkey
                                         ,aname      => 'XXWC_USER_ID'));


         LOG_MSG (g_level_STATEMENT, l_mod_name, ' l_user_id=' || l_user_id);
         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,' l_cust_acct_site_id=' || l_cust_acct_site_id);
         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,' l_site_use_id=' || l_site_use_id);
         LOG_MSG (g_level_STATEMENT, l_mod_name, ' calling CREATE_PROFILE');
         CREATE_PROFILE (p_site_use_id         => l_site_use_id
                        ,p_cust_acct_site_id   => l_cust_acct_site_id
                        ,p_user_id             => l_user_id
                        ,x_return_status       => lx_return_status
                        ,x_msg_count           => lx_msg_count
                        ,x_msg_data            => lx_msg_data);


         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,' lx_return_status (create_profile )=' || lx_return_status);

         IF lx_return_status <> fnd_api.g_ret_sts_success
         THEN
            LOG_MSG (g_level_error, l_mod_name, ' CREATE_PROFILE Failed');
            LOG_MSG (g_level_error
                    ,l_mod_name
                    ,' lx_msg_data =' || lx_msg_data);
            -- API failed with error message
            resultout := 'COMPLETE:Y';
            RAISE API_ERROR;
         ELSE
            -- API completed successfully
            resultout := 'COMPLETE:N';
            RETURN;
         END IF;
      END IF;

      IF (funcmode = 'CANCEL')
      THEN
         -- no result needed
         resultout := 'COMPLETE';
         RETURN;
      END IF;
   EXCEPTION
      WHEN API_ERROR
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('XXWC_CUST_CREDIT_LIMIT_PKG'
                         ,'CREATE_PROFILE_WF'
                         ,itemtype
                         ,itemkey
                         ,TO_CHAR (actid)
                         ,funcmode);
         -- start data fix project
         OE_STANDARD_WF.Add_Error_Activity_Msg (p_actid      => actid
                                               ,p_itemtype   => itemtype
                                               ,p_itemkey    => itemkey);
         OE_STANDARD_WF.Save_Messages;
         OE_STANDARD_WF.Clear_Msg_Context;
      WHEN OTHERS
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('XXWC_CUST_CREDIT_LIMIT_PKG'
                         ,'CREATE_PROFILE_WF'
                         ,itemtype
                         ,itemkey
                         ,TO_CHAR (actid)
                         ,funcmode);
         -- start data fix project
         OE_STANDARD_WF.Add_Error_Activity_Msg (p_actid      => actid
                                               ,p_itemtype   => itemtype
                                               ,p_itemkey    => itemkey);
         OE_STANDARD_WF.Save_Messages;
         OE_STANDARD_WF.Clear_Msg_Context;
   END CREATE_PROFILE_WF;

   /*************************************************************************
   *   Procedure : SET_ATTR
   *
   *   PURPOSE:   This procedure is called from the workflow to set the attributes
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   * ************************************************************************/
   PROCEDURE SET_ATTR (itemtype    IN            VARCHAR2
                      ,itemkey     IN            VARCHAR2
                      ,actid       IN            NUMBER
                      ,funcmode    IN            VARCHAR2
                      ,resultout   IN OUT NOCOPY VARCHAR2)
   IS
      l_cust_acct_site_id   NUMBER;
      l_user_name           JTF_RS_DEFRESOURCES_V.user_name%TYPE;
      l_cust_account_num    hz_cust_accounts.account_number%TYPE;

      l_cust_account_id     NUMBER;
      l_credit_analyst_id   NUMBER;
      l_party_site_id       hz_cust_acct_sites_all.party_site_id%TYPE;
      l_party_site_number   hz_party_sites.party_site_number%TYPE;
      l_error_msg           VARCHAR2 (1000);
      l_mod_name            VARCHAR2 (200)
                               := 'xxwc_cust_credit_limit_pkg.set_attr :';


      VALIDATION_ERROR      EXCEPTION;
   BEGIN
      -- start data fix project
      OE_STANDARD_WF.Set_Msg_Context (actid);

      IF (funcmode = 'RUN')
      THEN
         l_cust_acct_site_id :=
            TO_NUMBER (
               wf_engine.getitemattrtext (
                  itemtype   => itemtype
                 ,itemkey    => itemkey
                 ,aname      => 'XXWC_CUST_ACCT_SITE_ID'));

         BEGIN
            SELECT hcas.cust_account_id
                  ,hcas.party_site_id
                  ,hca.account_number
              INTO l_cust_account_id, l_party_site_id, l_cust_account_num
              FROM hz_cust_acct_sites_all hcas, hz_cust_accounts hca
             WHERE     cust_acct_site_id = l_cust_acct_site_id
                   AND hcas.cust_account_id = hca.cust_account_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_error_msg := 'Error while getting cust_acct_id :' || SQLERRM;
               LOG_MSG (g_level_error, l_mod_name, l_error_msg);
               RAISE VALIDATION_ERROR;
         END;


         BEGIN
            SELECT party_site_number
              INTO l_party_site_number
              FROM hz_party_sites
             WHERE party_site_id = l_party_site_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_error_msg :=
                  'Error while getting party_site_number :' || SQLERRM;
               LOG_MSG (g_level_error, l_mod_name, l_error_msg);
               RAISE VALIDATION_ERROR;
         END;


         BEGIN
            SELECT credit_analyst_id
              INTO l_credit_analyst_id
              FROM hz_customer_profiles
             WHERE     cust_account_id = l_cust_account_id
                   AND site_use_id IS NULL;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_error_msg :=
                  'Error while getting credit_analyst_id :' || SQLERRM;
               LOG_MSG (g_level_error, l_mod_name, l_error_msg);
               RAISE VALIDATION_ERROR;
         END;

         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,' l_credit_analyst_id = ' || l_credit_analyst_id);

         BEGIN
            SELECT user_name
              INTO l_user_name
              FROM JTF_RS_DEFRESOURCES_V
             WHERE resource_id = l_credit_analyst_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_user_name := 'SYSADMIN';
         END;

         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,' l_user_name = ' || l_user_name);
         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,' l_cust_account_num = ' || l_cust_account_num);
         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,' l_party_site_number = ' || l_party_site_number);

         wf_engine.setitemattrtext (itemtype   => itemtype
                                   ,itemkey    => itemkey
                                   ,aname      => 'XXWC_NTF_PERFORMER'
                                   ,avalue     => l_user_name);
         wf_engine.setitemattrtext (itemtype   => itemtype
                                   ,itemkey    => itemkey
                                   ,aname      => 'XXWC_CUST_ACCT_NUM'
                                   ,avalue     => l_cust_account_num);
         wf_engine.setitemattrtext (itemtype   => itemtype
                                   ,itemkey    => itemkey
                                   ,aname      => 'XXWC_PARTY_SITE_NUM'
                                   ,avalue     => l_party_site_number);

         -- no result needed
         resultout := 'COMPLETE';
         RETURN;
      END IF;

      IF (funcmode = 'CANCEL')
      THEN
         -- no result needed
         resultout := 'COMPLETE';
         RETURN;
      END IF;
   EXCEPTION
      WHEN VALIDATION_ERROR
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('xxwc_cust_credit_limit_pkg'
                         ,'set_attr'
                         ,itemtype
                         ,itemkey
                         ,TO_CHAR (actid)
                         ,funcmode);
         -- start data fix project
         OE_STANDARD_WF.Add_Error_Activity_Msg (p_actid      => actid
                                               ,p_itemtype   => itemtype
                                               ,p_itemkey    => itemkey);
         OE_STANDARD_WF.Save_Messages;
         OE_STANDARD_WF.Clear_Msg_Context;
      WHEN OTHERS
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('xxwc_cust_credit_limit_pkg'
                         ,'set_attr'
                         ,itemtype
                         ,itemkey
                         ,TO_CHAR (actid)
                         ,funcmode);
         -- start data fix project
         OE_STANDARD_WF.Add_Error_Activity_Msg (p_actid      => actid
                                               ,p_itemtype   => itemtype
                                               ,p_itemkey    => itemkey);
         OE_STANDARD_WF.Save_Messages;
         OE_STANDARD_WF.Clear_Msg_Context;
   END SET_ATTR;

   /*************************************************************************
   *   Procedure : SET_ATTRIBUTES
   *
   *   PURPOSE:   This procedure is called from the workflow to set the attributes
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   * ************************************************************************/
   PROCEDURE SET_ATTRIBUTES (itemtype    IN            VARCHAR2
                            ,itemkey     IN            VARCHAR2
                            ,actid       IN            NUMBER
                            ,funcmode    IN            VARCHAR2
                            ,resultout   IN OUT NOCOPY VARCHAR2)
   IS
      l_user_name           JTF_RS_DEFRESOURCES_V.user_name%TYPE;
      l_cust_account_num    hz_cust_accounts.account_number%TYPE;

      l_cust_account_id     NUMBER;
      l_credit_analyst_id   NUMBER;
      --l_party_site_id       hz_cust_acct_sites_all.party_site_id%TYPE;
      l_party_site_number   hz_party_sites.party_site_number%TYPE;
      l_site_use_id         NUMBER;
      l_error_msg           VARCHAR2 (1000);
      l_mod_name            VARCHAR2 (200)
         := 'xxwc_cust_credit_limit_pkg.set_attributes :';

      VALIDATION_ERROR      EXCEPTION;
   BEGIN
      -- start data fix project
      OE_STANDARD_WF.Set_Msg_Context (actid);

      IF (funcmode = 'RUN')
      THEN
         l_cust_account_id :=
            TO_NUMBER (
               wf_engine.getitemattrtext (
                  itemtype   => itemtype
                 ,itemkey    => itemkey
                 ,aname      => 'XXWC_CUST_ACCOUNT_ID'));

         l_site_use_id :=
            TO_NUMBER (
               wf_engine.getitemattrtext (itemtype   => itemtype
                                         ,itemkey    => itemkey
                                         ,aname      => 'XXWC_SITE_USE_ID'));

         LOG_MSG (g_level_error
                 ,l_mod_name
                 ,'l_cust_account_id=' || l_cust_account_id);
         LOG_MSG (g_level_error
                 ,l_mod_name
                 ,'l_site_use_id    =' || l_site_use_id);

         BEGIN
            SELECT account_number
              INTO l_cust_account_num
              FROM hz_cust_accounts
             WHERE cust_account_id = l_cust_account_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_error_msg :=
                  'Error while getting credit_analyst_id :' || SQLERRM;
               LOG_MSG (g_level_error, l_mod_name, l_error_msg);
               RAISE VALIDATION_ERROR;
         END;

         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,'l_cust_account_num = ' || l_cust_account_num);

         BEGIN
            SELECT credit_analyst_id
              INTO l_credit_analyst_id
              FROM hz_customer_profiles
             WHERE     cust_account_id = l_cust_account_id
                   AND site_use_id IS NULL;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_error_msg :=
                  'Error while getting credit_analyst_id :' || SQLERRM;
               LOG_MSG (g_level_error, l_mod_name, l_error_msg);
               RAISE VALIDATION_ERROR;
         END;

         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,'l_credit_analyst_id = ' || l_credit_analyst_id);

         BEGIN
            SELECT user_name
              INTO l_user_name
              FROM JTF_RS_DEFRESOURCES_V
             WHERE resource_id = l_credit_analyst_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_user_name := 'SYSADMIN';
         END;


         BEGIN
            SELECT hps.party_site_number
              INTO l_party_site_number
              FROM hz_cust_site_uses_all su
                  ,hz_cust_acct_sites_all site
                  ,hz_party_sites hps
             WHERE     su.cust_acct_site_id = site.cust_acct_site_id
                   AND site.party_site_id = hps.party_site_id
                   AND su.site_use_id = l_site_use_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_error_msg :=
                  'Error while getting Party Site Number =' || SQLERRM;
               LOG_MSG (g_level_error, l_mod_name, l_error_msg);
               RAISE VALIDATION_ERROR;
         END;

         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,'l_user_name        = ' || l_user_name);
         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,'l_cust_account_num = ' || l_cust_account_num);
         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,'l_party_site_number = ' || l_party_site_number);

         wf_engine.setitemattrtext (itemtype   => itemtype
                                   ,itemkey    => itemkey
                                   ,aname      => 'XXWC_NTF_PERFORMER'
                                   ,avalue     => l_user_name);
         wf_engine.setitemattrtext (itemtype   => itemtype
                                   ,itemkey    => itemkey
                                   ,aname      => 'XXWC_CUST_ACCT_NUM'
                                   ,avalue     => l_cust_account_num);
         wf_engine.setitemattrtext (itemtype   => itemtype
                                   ,itemkey    => itemkey
                                   ,aname      => 'XXWC_PARTY_SITE_NUM'
                                   ,avalue     => l_party_site_number);
         -- no result needed
         resultout := 'COMPLETE';
         RETURN;
      END IF;

      IF (funcmode = 'CANCEL')
      THEN
         -- no result needed
         resultout := 'COMPLETE';
         RETURN;
      END IF;
   EXCEPTION
      WHEN VALIDATION_ERROR
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('xxwc_cust_credit_limit_pkg'
                         ,'set_attributes'
                         ,itemtype
                         ,itemkey
                         ,TO_CHAR (actid)
                         ,funcmode);
         -- start data fix project
         OE_STANDARD_WF.Add_Error_Activity_Msg (p_actid      => actid
                                               ,p_itemtype   => itemtype
                                               ,p_itemkey    => itemkey);
         OE_STANDARD_WF.Save_Messages;
         OE_STANDARD_WF.Clear_Msg_Context;
      WHEN OTHERS
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('xxwc_cust_credit_limit_pkg'
                         ,'set_attributes'
                         ,itemtype
                         ,itemkey
                         ,TO_CHAR (actid)
                         ,funcmode);
         -- start data fix project
         OE_STANDARD_WF.Add_Error_Activity_Msg (p_actid      => actid
                                               ,p_itemtype   => itemtype
                                               ,p_itemkey    => itemkey);
         OE_STANDARD_WF.Save_Messages;
         OE_STANDARD_WF.Clear_Msg_Context;
   END SET_ATTRIBUTES;

   /*************************************************************************
   *   Procedure : ASSIGN_CREDIT_USAGE
   *
   *   PURPOSE:   This procedure is called from the CREATE_PROFILE_WF procedure
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   * ************************************************************************/
   PROCEDURE ASSIGN_CREDIT_USAGE (
      p_cust_account_id            IN            NUMBER
     ,p_cust_acct_profile_amt_id   IN            NUMBER
     ,x_return_status                 OUT NOCOPY VARCHAR2
     ,x_msg_count                     OUT NOCOPY NUMBER
     ,x_msg_data                      OUT NOCOPY VARCHAR2)
   IS
      l_profile_class_id           NUMBER;
      lv_profile_class_amount_id   NUMBER;
      lv_cust_profile_id           NUMBER;
      p_count                      NUMBER := 0;
      x_error_msg                  VARCHAR2 (1000);
      VALIDATION_ERROR             EXCEPTION;
      API_FAILURE                  EXCEPTION;

      l_mod_name                   VARCHAR2 (200)
         := 'xxwc_cust_credit_limit_pkg.assign_credit_usage';
   BEGIN
      --set the context
      --LOG_MSG (g_level_STATEMENT, l_mod_name,'Initializing application');
      INIT_APPS (fnd_global.user_id);

      LOG_MSG (g_level_statement, l_mod_name, 'At Begin');
      LOG_MSG (g_level_statement
              ,l_mod_name
              ,'p_cust_account_id =' || p_cust_account_id);
      LOG_MSG (g_level_statement
              ,l_mod_name
              ,'p_cust_acct_profile_amt_id =' || p_cust_acct_profile_amt_id);

      BEGIN
         SELECT profile_class_id
           INTO l_profile_class_id
           FROM hz_customer_profiles
          WHERE cust_account_id = p_cust_account_id AND site_use_id IS NULL;
      EXCEPTION
         WHEN OTHERS
         THEN
            x_msg_data := 'Error while getting Profile Class Id :' || SQLERRM;
            LOG_MSG (g_level_statement, l_mod_name, x_msg_data);
            RAISE VALIDATION_ERROR;
      END;

      LOG_MSG (g_level_statement
              ,l_mod_name
              ,'l_profile_class_id =' || l_profile_class_id);

      BEGIN
         SELECT profile_class_amount_id
           INTO lv_profile_class_amount_id
           FROM hz_cust_prof_class_amts
          WHERE profile_class_id = l_profile_class_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            x_msg_data :=
               'Error while getting Profile Class Amount Id :' || SQLERRM;
            LOG_MSG (g_level_statement, l_mod_name, x_msg_data);
            RAISE VALIDATION_ERROR;
      END;

      LOG_MSG (g_level_statement
              ,l_mod_name
              ,'calling cascade_credit_usage_rules');
      hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
         p_cust_acct_profile_amt_id   => p_cust_acct_profile_amt_id
        ,p_cust_profile_id            => lv_cust_profile_id
        ,p_profile_class_amt_id       => lv_profile_class_amount_id
        ,p_profile_class_id           => l_profile_class_id
        ,x_return_status              => x_return_status
        ,x_msg_count                  => x_msg_count
        ,x_msg_data                   => x_msg_data);

      LOG_MSG (
         g_level_statement
        ,l_mod_name
        ,'x_return_status (cascade_credit_usage_rules)=' || x_return_status);

      IF x_return_status <> 'S'
      THEN
         xxwc_cust_credit_limit_pkg.log_msg (g_level_statement
                                            ,l_mod_name
                                            ,'unexpected errors found!');

         IF x_msg_count = 1
         THEN
            xxwc_cust_credit_limit_pkg.log_msg (
               g_level_statement
              ,l_mod_name
              ,'x_msg_data = ' || x_msg_data);
         ELSIF x_msg_count > 1
         THEN
            LOOP
               p_count := p_count + 1;
               x_error_msg :=
                  FND_MSG_PUB.Get (FND_MSG_PUB.G_NEXT, FND_API.G_FALSE);

               IF x_error_msg IS NULL
               THEN
                  EXIT;
               END IF;

               xxwc_cust_credit_limit_pkg.log_msg (
                  g_level_statement
                 ,l_mod_name
                 ,'Message' || p_count || '.' || x_error_msg);
            END LOOP;
         END IF;
      END IF;
   END ASSIGN_CREDIT_USAGE;

   /*************************************************************************
   *   Procedure : ASSIGN_CREDIT_USAGE_WF
   *
   *   PURPOSE:   This procedure is called from the workflow from the function node
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   * ************************************************************************/
   PROCEDURE ASSIGN_CREDIT_USAGE_WF (itemtype    IN            VARCHAR2
                                    ,itemkey     IN            VARCHAR2
                                    ,actid       IN            NUMBER
                                    ,funcmode    IN            VARCHAR2
                                    ,resultout   IN OUT NOCOPY VARCHAR2)
   IS
      lx_return_status          VARCHAR2 (10);
      lx_msg_count              NUMBER;
      lx_msg_data               VARCHAR2 (2000);

      l_cust_account_id         NUMBER;
      l_cust_acct_prof_amt_id   NUMBER;

      l_mod_name                VARCHAR2 (200)
         := 'xxwc_cust_credit_limit_pkg.assign_credit_usage_wf:';

      API_ERROR                 EXCEPTION;
   BEGIN
      -- start data fix project
      OE_STANDARD_WF.Set_Msg_Context (actid);

      LOG_MSG (g_level_STATEMENT, l_mod_name, 'At Begin ');

      IF (funcmode = 'RUN')
      THEN
         l_cust_account_id :=
            TO_NUMBER (
               wf_engine.getitemattrtext (
                  itemtype   => itemtype
                 ,itemkey    => itemkey
                 ,aname      => 'XXWC_CUST_ACCOUNT_ID'));

         l_cust_acct_prof_amt_id :=
            TO_NUMBER (
               wf_engine.getitemattrtext (
                  itemtype   => itemtype
                 ,itemkey    => itemkey
                 ,aname      => 'XXWC_CUST_ACCT_PROF_AMT_ID'));

         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,' l_cust_account_id      =' || l_cust_account_id);
         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,' l_cust_acct_prof_amt_id=' || l_cust_acct_prof_amt_id);
         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,' calling assign_credit_usage');

         ASSIGN_CREDIT_USAGE (
            p_cust_account_id            => l_cust_account_id
           ,p_cust_acct_profile_amt_id   => l_cust_acct_prof_amt_id
           ,x_return_status              => lx_return_status
           ,x_msg_count                  => lx_msg_count
           ,x_msg_data                   => lx_msg_data);

         LOG_MSG (
            g_level_STATEMENT
           ,l_mod_name
           ,' lx_return_status (assign_credit_usage )=' || lx_return_status);

         IF lx_return_status <> fnd_api.g_ret_sts_success
         THEN
            LOG_MSG (g_level_error
                    ,l_mod_name
                    ,' assign_credit_usage Failed');
            LOG_MSG (g_level_error
                    ,l_mod_name
                    ,' lx_msg_data =' || lx_msg_data);
            -- API failed with error message
            resultout := 'COMPLETE:Y';
            RAISE API_ERROR;
         ELSE
            -- API completed successfully
            resultout := 'COMPLETE:N';
            RETURN;
         END IF;
      END IF;

      IF (funcmode = 'CANCEL')
      THEN
         -- no result needed
         resultout := 'COMPLETE';
         RETURN;
      END IF;
   EXCEPTION
      WHEN API_ERROR
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('XXWC_CUST_CREDIT_LIMIT_PKG'
                         ,'ASSIGN_CREDIT_USAGE_WF'
                         ,itemtype
                         ,itemkey
                         ,TO_CHAR (actid)
                         ,funcmode);
         -- start data fix project
         OE_STANDARD_WF.Add_Error_Activity_Msg (p_actid      => actid
                                               ,p_itemtype   => itemtype
                                               ,p_itemkey    => itemkey);
         OE_STANDARD_WF.Save_Messages;
         OE_STANDARD_WF.Clear_Msg_Context;
      WHEN OTHERS
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('XXWC_CUST_CREDIT_LIMIT_PKG'
                         ,'ASSIGN_CREDIT_USAGE_WF'
                         ,itemtype
                         ,itemkey
                         ,TO_CHAR (actid)
                         ,funcmode);
         -- start data fix project
         OE_STANDARD_WF.Add_Error_Activity_Msg (p_actid      => actid
                                               ,p_itemtype   => itemtype
                                               ,p_itemkey    => itemkey);
         OE_STANDARD_WF.Save_Messages;
         OE_STANDARD_WF.Clear_Msg_Context;
   END ASSIGN_CREDIT_USAGE_WF;

   /*************************************************************************
   *   Procedure : GET_SALESREP_ID
   *
   *   PURPOSE:   This function is  get the primary salesrep id from site Use Id
   *   Parameter:
   *          IN
   *              p_site_use_id    -- Site Use Id
   * ************************************************************************/
   FUNCTION GET_SALESREP_ID (p_cust_account_id IN NUMBER)
      RETURN NUMBER
   IS
      --PRAGMA AUTONOMOUS_TRANSACTION;

      l_primary_salesrep_id   NUMBER;
      l_mod_name              VARCHAR2 (200)
         := 'xxwc_cust_credit_limit_pkg.get_salesrep_id:';
   BEGIN
      LOG_MSG (g_level_STATEMENT
              ,l_mod_name
              ,' p_cust_account_id=' || p_cust_account_id);

      BEGIN
         SELECT su.primary_salesrep_id
           INTO l_primary_salesrep_id
           FROM hz_cust_site_uses su, hz_cust_acct_sites cas
          WHERE     su.cust_acct_site_id = cas.cust_acct_site_id
                AND su.site_use_code = 'BILL_TO'
                AND su.primary_flag = 'Y'
                AND cas.cust_account_id = p_cust_account_id
                AND su.status = 'A';


         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,' l_primary_salesrep_id=' || l_primary_salesrep_id);
      EXCEPTION
         WHEN OTHERS
         THEN
            l_primary_salesrep_id := NULL;
            LOG_MSG (g_level_STATEMENT, l_mod_name, ' Error =' || SQLERRM);
      END;

      RETURN l_primary_salesrep_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END GET_SALESREP_ID;

   /*************************************************************************
   *   Procedure : GET_FREIGHT_TERM
   *
   *   PURPOSE:   This function is  get the freight term from site Use Id
   *   Parameter:
   *          IN
   *              p_site_use_id    -- Site Use Id
   * ************************************************************************/
   FUNCTION GET_FREIGHT_TERM (p_cust_acct_site_id IN NUMBER)
      RETURN VARCHAR2
   IS
      --PRAGMA AUTONOMOUS_TRANSACTION;

      l_cust_account_id   NUMBER;
      l_freight_term      hz_cust_accounts.freight_term%TYPE;
      l_mod_name          VARCHAR2 (200)
         := 'xxwc_cust_credit_limit_pkg.get_freight_term:';
   BEGIN
      LOG_MSG (g_level_STATEMENT
              ,l_mod_name
              ,' p_cust_acct_site_id=' || p_cust_acct_site_id);

      BEGIN
         SELECT cust_account_id
           INTO l_cust_account_id
           FROM hz_cust_acct_sites_all cas
          WHERE cust_acct_site_id = p_cust_acct_site_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_cust_account_id := NULL;
      END;

      LOG_MSG (g_level_STATEMENT
              ,l_mod_name
              ,' l_cust_account_id=' || l_cust_account_id);

      BEGIN
         SELECT freight_term
           INTO l_freight_term
           FROM hz_cust_accounts hca
          WHERE cust_account_id = l_cust_account_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_freight_term := NULL;
      END;

      LOG_MSG (g_level_STATEMENT
              ,l_mod_name
              ,' l_freight_term=' || l_freight_term);
      RETURN l_freight_term;
   EXCEPTION
      WHEN OTHERS
      THEN
         LOG_MSG (g_level_STATEMENT, l_mod_name, ' error =' || SQLERRM);
         RETURN NULL;
   END GET_FREIGHT_TERM;

   /*************************************************************************
   *   Procedure : GET_BILL_TO_SITE_USE_ID
   *
   *   PURPOSE:   This function is  get the Bill To Site Use Id from the Site Id
   *   Parameter:
   *          IN
   *              p_cust_acct_site_id    -- Site Use Id
   * ************************************************************************/
   FUNCTION GET_BILL_TO_SITE_USE_ID (p_cust_acct_site_id IN NUMBER)
      RETURN NUMBER
   IS
      l_bill_to_site_use_id   NUMBER;
      l_mod_name              VARCHAR2 (200)
         := 'xxwc_cust_credit_limit_pkg.get_bill_to_site_use_id:';
   BEGIN
      BEGIN
         SELECT site_use_id
           INTO l_bill_to_site_use_id
           FROM hz_cust_site_uses_all su
          WHERE     su.cust_acct_site_id = p_cust_acct_site_id
                AND site_use_code = 'BILL_TO'
                AND status = 'A';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_bill_to_site_use_id := NULL;
      END;

      RETURN l_bill_to_site_use_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         LOG_MSG (g_level_STATEMENT, l_mod_name, ' error =' || SQLERRM);
         RETURN NULL;
   END GET_BILL_TO_SITE_USE_ID;

   /*************************************************************************
   *   Procedure : GET_BILL_TO_SITE_USE_ID
   *
   *   PURPOSE:   This function is  get the Bill To Site Use Id from the Site Id
   *   Parameter:
   *          IN
   *              p_cust_acct_site_id    -- Site Use Id
   * ************************************************************************/
   FUNCTION GET_GSA_FLAG (p_cust_account_id IN NUMBER)
      RETURN VARCHAR2
   IS
      l_gsa_flag   VARCHAR2 (10);
      l_mod_name   VARCHAR2 (200)
                      := 'xxwc_cust_credit_limit_pkg.get_gsa_flag:';
   BEGIN
      BEGIN
         SELECT su.gsa_indicator
           INTO l_gsa_flag
           FROM hz_cust_site_uses_all su, hz_cust_acct_sites_all hcas
          WHERE     su.cust_acct_site_id = hcas.cust_acct_site_id
                AND su.site_use_code = 'BILL_TO'
                AND su.status = 'A'
                AND hcas.cust_account_id = p_cust_account_id
                AND su.primary_flag = 'Y';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_gsa_flag := NULL;
      END;

      RETURN l_gsa_flag;
   EXCEPTION
      WHEN OTHERS
      THEN
         LOG_MSG (g_level_STATEMENT, l_mod_name, ' error =' || SQLERRM);
         RETURN NULL;
   END GET_GSA_FLAG;

   /*************************************************************************
   *   Procedure : CHECK_SITE_IS_JOB_SITE
   *
   *   PURPOSE:   This procedure is called from the workflow to check if the site is Job Site
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   * ************************************************************************/
   PROCEDURE CHECK_SITE_IS_JOB_SITE (itemtype    IN            VARCHAR2
                                    ,itemkey     IN            VARCHAR2
                                    ,actid       IN            NUMBER
                                    ,funcmode    IN            VARCHAR2
                                    ,resultout   IN OUT NOCOPY VARCHAR2)
   IS
      l_job_site            VARCHAR2 (100);
      l_created_by_module   VARCHAR2 (100);
      l_mod_name            VARCHAR2 (200)
         := 'xxwc_cust_credit_limit_pkg.check_site_is_job_site';
   BEGIN
      -- start data fix project
      OE_STANDARD_WF.Set_Msg_Context (actid);

      LOG_MSG (g_level_STATEMENT, l_mod_name, 'At Begin ');

      IF (funcmode = 'RUN')
      THEN
         l_job_site :=
            wf_engine.getitemattrtext (itemtype   => itemtype
                                      ,itemkey    => itemkey
                                      ,aname      => 'XXWC_ATTRIBUTE1');

         l_created_by_module :=
            wf_engine.getitemattrtext (itemtype   => itemtype
                                      ,itemkey    => itemkey
                                      ,aname      => 'XXWC_CREATED_BY_MODULE');

         IF     l_job_site = 'JOB'
            AND l_created_by_module = 'ONT_UI_ADD_CUSTOMER'
         THEN
            -- API failed with error message
            resultout := 'COMPLETE:Y';
            RETURN;
         ELSE
            -- API failed with error message
            resultout := 'COMPLETE:N';
            RETURN;
         END IF;
      END IF;

      IF (funcmode = 'CANCEL')
      THEN
         -- no result needed
         resultout := 'COMPLETE:N';
         RETURN;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('XXWC_CUST_CREDIT_LIMIT_PKG'
                         ,'CHECK_SITE_IS_JOB_SITE'
                         ,itemtype
                         ,itemkey
                         ,TO_CHAR (actid)
                         ,funcmode);
         -- start data fix project
         OE_STANDARD_WF.Add_Error_Activity_Msg (p_actid      => actid
                                               ,p_itemtype   => itemtype
                                               ,p_itemkey    => itemkey);
         OE_STANDARD_WF.Save_Messages;
         OE_STANDARD_WF.Clear_Msg_Context;
   END CHECK_SITE_IS_JOB_SITE;

   /*************************************************************************
   *   Procedure : CHECK_SITE_IS_BILLTO
   *
   *   PURPOSE:   This procedure is called from the workflow to check if the site is Bill To
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   *        Satish U: 19-MAR-2012 : Add Sales rep to Bill To Site Too ( As per Bob's Conversion )
   * ************************************************************************/
   PROCEDURE CHECK_SITE_IS_BILLTO (itemtype    IN            VARCHAR2
                                  ,itemkey     IN            VARCHAR2
                                  ,actid       IN            NUMBER
                                  ,funcmode    IN            VARCHAR2
                                  ,resultout   IN OUT NOCOPY VARCHAR2)
   IS
      l_site_use_code         VARCHAR2 (100);
      l_mod_name              VARCHAR2 (200)
         := 'XXWC_CUST_CREDIT_LIMIT_PKG.CHECK_SITE_IS_BILLTO';
      l_site_use_id           NUMBER;
      l_primary_salesrep_id   NUMBER;
      l_cust_account_id       NUMBER;
      l_cust_acct_site_id     NUMBER;
   BEGIN
      -- start data fix project
      OE_STANDARD_WF.Set_Msg_Context (actid);

      LOG_MSG (g_level_STATEMENT, l_mod_name, 'At Begin ');

      IF (funcmode = 'RUN')
      THEN
         l_site_use_code :=
            wf_engine.getitemattrtext (itemtype   => itemtype
                                      ,itemkey    => itemkey
                                      ,aname      => 'XXWC_SITE_USE_CODE');

         l_cust_acct_site_id :=
            TO_NUMBER (
               wf_engine.getitemattrtext (
                  itemtype   => itemtype
                 ,itemkey    => itemkey
                 ,aname      => 'XXWC_CUST_ACCT_SITE_ID'));

         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,'l_site_use_code = ' || l_site_use_code);
         -- Satish 19-MAR-2012 :
         l_site_use_id :=
            TO_NUMBER (
               wf_engine.getitemattrtext (itemtype   => itemtype
                                         ,itemkey    => itemkey
                                         ,aname      => 'XXWC_SITE_USE_ID'));


         BEGIN
            SELECT cust_account_id
              INTO l_cust_account_id
              FROM hz_cust_acct_sites_all cas
             WHERE cust_acct_site_id = l_cust_acct_site_id;

            LOG_MSG (g_level_STATEMENT
                    ,l_mod_name
                    ,'l_cust_account_id=' || l_cust_account_id);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_cust_account_id := NULL;
               LOG_MSG (g_level_STATEMENT, l_mod_name, 'Error =' || SQLERRM);
         END;

         -- Get Primary Salesrep ID
         l_primary_salesrep_id :=
            xxwc_cust_credit_limit_pkg.get_salesrep_id (l_cust_account_id);
         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,'l_primary_salesrep_id =' || l_primary_salesrep_id);


         --Update site use with the bill_to_site_use_id
         UPDATE hz_cust_site_uses_all
            SET primary_salesrep_id =
                   NVL (l_primary_salesrep_id, primary_salesrep_id)
          WHERE site_use_id = l_site_use_id;

         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,'Num of Rows Updated =' || SQL%ROWCOUNT);


         IF l_site_use_code = 'BILL_TO'
         THEN
            -- API failed with error message
            resultout := 'COMPLETE:Y';
            RETURN;
         ELSE
            -- API failed with error message
            resultout := 'COMPLETE:N';
            RETURN;
         END IF;
      END IF;

      IF (funcmode = 'CANCEL')
      THEN
         -- no result needed
         resultout := 'COMPLETE:N';
         RETURN;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('XXWC_CUST_CREDIT_LIMIT_PKG'
                         ,'CHECK_SITE_IS_BILLTO'
                         ,itemtype
                         ,itemkey
                         ,TO_CHAR (actid)
                         ,funcmode);
         -- start data fix project
         OE_STANDARD_WF.Add_Error_Activity_Msg (p_actid      => actid
                                               ,p_itemtype   => itemtype
                                               ,p_itemkey    => itemkey);
         OE_STANDARD_WF.Save_Messages;
         OE_STANDARD_WF.Clear_Msg_Context;
   END CHECK_SITE_IS_BILLTO;

   /*************************************************************************
   *   Procedure : UPDATE_GSA_FLAG
   *
   *   PURPOSE:   This procedure is called from the workflow to update the GSA Flag
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   * ************************************************************************/
   PROCEDURE UPDATE_GSA_FLAG (itemtype    IN            VARCHAR2
                             ,itemkey     IN            VARCHAR2
                             ,actid       IN            NUMBER
                             ,funcmode    IN            VARCHAR2
                             ,resultout   IN OUT NOCOPY VARCHAR2)
   IS
      l_mod_name            VARCHAR2 (200)
                               := 'xxwc_cust_credit_limit_pkg.update_gsa_flag';
      l_cust_account_id     NUMBER;
      l_cust_acct_site_id   NUMBER;
      l_site_use_id         NUMBER;
      l_gsa_flag            VARCHAR2 (10);
   BEGIN
      -- start data fix project
      OE_STANDARD_WF.Set_Msg_Context (actid);

      LOG_MSG (g_level_STATEMENT, l_mod_name, 'At Begin ');

      IF (funcmode = 'RUN')
      THEN
         l_cust_acct_site_id :=
            TO_NUMBER (
               wf_engine.getitemattrtext (
                  itemtype   => itemtype
                 ,itemkey    => itemkey
                 ,aname      => 'XXWC_CUST_ACCT_SITE_ID'));

         l_site_use_id :=
            TO_NUMBER (
               wf_engine.getitemattrtext (itemtype   => itemtype
                                         ,itemkey    => itemkey
                                         ,aname      => 'XXWC_SITE_USE_ID'));

         log_msg (g_level_statement
                 ,l_mod_name
                 ,'l_cust_acct_site_id =' || l_cust_acct_site_id);
         log_msg (g_level_statement
                 ,l_mod_name
                 ,'l_site_use_id =' || l_site_use_id);

         BEGIN
            SELECT cust_account_id
              INTO l_cust_account_id
              FROM hz_cust_acct_sites_all
             WHERE cust_acct_site_id = l_cust_acct_site_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               log_msg (g_level_statement, l_mod_name, 'Error  =' || SQLERRM);
               l_cust_account_id := NULL;
         END;

         l_gsa_flag := get_gsa_flag (l_cust_account_id);

         --Update site use with the bill_to_site_use_id
         UPDATE hz_cust_site_uses_all
            SET gsa_indicator = l_gsa_flag
          WHERE site_use_id = l_site_use_id;

         -- API failed with error message
         resultout := 'COMPLETE';
         RETURN;
      END IF;

      IF (funcmode = 'CANCEL')
      THEN
         -- no result needed
         resultout := 'COMPLETE';
         RETURN;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('XXWC_CUST_CREDIT_LIMIT_PKG'
                         ,'UPDATE_GSA_FLAG'
                         ,itemtype
                         ,itemkey
                         ,TO_CHAR (actid)
                         ,funcmode);
         -- start data fix project
         OE_STANDARD_WF.Add_Error_Activity_Msg (p_actid      => actid
                                               ,p_itemtype   => itemtype
                                               ,p_itemkey    => itemkey);
         OE_STANDARD_WF.Save_Messages;
         OE_STANDARD_WF.Clear_Msg_Context;
   END UPDATE_GSA_FLAG;

   /*************************************************************************
   *   Procedure : UPDATE_SHIP_T0_SITE
   *
   *   PURPOSE:   This procedure is called from the workflow to update the ship to site
   *   Parameter:
   *          IN
   *              itemtype    -- Item Type
   *              itemkey     -- Itemm Key
   *              actid       -- Activity Id
   *              funcmode    -- Function mode
   *              resultout   -- Result
   * ************************************************************************/

   PROCEDURE UPDATE_SHIP_TO_SITE (itemtype    IN            VARCHAR2
                                 ,itemkey     IN            VARCHAR2
                                 ,actid       IN            NUMBER
                                 ,funcmode    IN            VARCHAR2
                                 ,resultout   IN OUT NOCOPY VARCHAR2)
   IS
      l_cust_acct_site_id     NUMBER;
      l_cust_account_id       NUMBER;
      l_primary_salesrep_id   NUMBER;
      l_site_use_id           NUMBER;
      l_bill_to_site_use_id   NUMBER;
      l_freight_term          VARCHAR2 (30);
      l_primary_flag          VARCHAR2 (10);
      l_mod_name              VARCHAR2 (200)
         := 'xxwc_cust_credit_limit_pkg.update_ship_to_site';
   BEGIN
      -- start data fix project
      OE_STANDARD_WF.Set_Msg_Context (actid);

      LOG_MSG (g_level_STATEMENT, l_mod_name, 'At Begin ');

      IF (funcmode = 'RUN')
      THEN
         l_cust_acct_site_id :=
            TO_NUMBER (
               wf_engine.getitemattrtext (
                  itemtype   => itemtype
                 ,itemkey    => itemkey
                 ,aname      => 'XXWC_CUST_ACCT_SITE_ID'));

         l_primary_flag :=
            wf_engine.getitemattrtext (itemtype   => itemtype
                                      ,itemkey    => itemkey
                                      ,aname      => 'XXWC_PRIMARY_FLAG');

         l_site_use_id :=
            TO_NUMBER (
               wf_engine.getitemattrtext (itemtype   => itemtype
                                         ,itemkey    => itemkey
                                         ,aname      => 'XXWC_SITE_USE_ID'));

         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,'l_cust_acct_site_id=' || l_cust_acct_site_id);
         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,'l_primary_flag     =' || l_primary_flag);
         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,'l_site_use_id      =' || l_site_use_id);

         l_bill_to_site_use_id :=
            get_bill_to_site_use_id (l_cust_acct_site_id);
         LOG_MSG (g_level_STATEMENT
                 ,l_mod_name
                 ,'l_bill_to_site_use_id =' || l_bill_to_site_use_id);

         BEGIN
            SELECT cust_account_id
              INTO l_cust_account_id
              FROM apps.hz_cust_acct_sites cas  
			  WHERE cust_acct_site_id = l_cust_acct_site_id;

            LOG_MSG (g_level_STATEMENT
                    ,l_mod_name
                    ,'l_cust_account_id=' || l_cust_account_id);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_cust_account_id := NULL;
               LOG_MSG (g_level_STATEMENT, l_mod_name, 'Error =' || SQLERRM);
         END;


         l_freight_term :=
            xxwc_cust_credit_limit_pkg.get_freight_term (l_cust_acct_site_id);
         LOG_MSG (g_level_statement
                 ,l_mod_name
                 ,'l_freight_term =' || l_freight_term);

         --Default Salesperson only if the site is not a primary
         IF l_primary_flag <> 'Y'
         THEN
            l_primary_salesrep_id :=
               xxwc_cust_credit_limit_pkg.get_salesrep_id (l_cust_account_id);
            LOG_MSG (g_level_STATEMENT
                    ,l_mod_name
                    ,'l_primary_salesrep_id =' || l_primary_salesrep_id);

            --Update site use with the bill_to_site_use_id
            UPDATE hz_cust_site_uses_all
               SET bill_to_site_use_id = l_bill_to_site_use_id
                  ,freight_term = NVL (l_freight_term, freight_term)
                  ,primary_salesrep_id =
                      NVL (l_primary_salesrep_id, primary_salesrep_id)
             WHERE site_use_id = l_site_use_id;

            LOG_MSG (g_level_STATEMENT
                    ,l_mod_name
                    ,'Num of Rows Updated =' || SQL%ROWCOUNT);
         ELSE
            --Update site use with the bill_to_site_use_id
            UPDATE  apps.hz_cust_site_uses			        
               SET bill_to_site_use_id = l_bill_to_site_use_id
                  ,freight_term = NVL (l_freight_term, freight_term)
             WHERE site_use_id = l_site_use_id;

            LOG_MSG (g_level_STATEMENT
                    ,l_mod_name
                    ,'Num of Rows Updated =' || SQL%ROWCOUNT);
         END IF;

         -- API failed with error message
         resultout := 'COMPLETE';
         RETURN;
      END IF;

      IF (funcmode = 'CANCEL')
      THEN
         -- no result needed
         resultout := 'COMPLETE';
         RETURN;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('XXWC_CUST_CREDIT_LIMIT_PKG'
                         ,'UPDATE_SHIP_T0_SITE'
                         ,itemtype
                         ,itemkey
                         ,TO_CHAR (actid)
                         ,funcmode);
         -- start data fix project
         OE_STANDARD_WF.Add_Error_Activity_Msg (p_actid      => actid
                                               ,p_itemtype   => itemtype
                                               ,p_itemkey    => itemkey);
         OE_STANDARD_WF.Save_Messages;
         OE_STANDARD_WF.Clear_Msg_Context;
   END UPDATE_SHIP_TO_SITE;
   
--Added for Revision 1.2								 
   /*************************************************************************
   *   Procedure : SUBMIT_WF_PROCESS
   *
   *   PURPOSE:   This procedure is called from Trigger XXWC_HZ_CUST_SITE_USES_BIR_TRG
   *   Parameter: None
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------   
   *   1.2        09/04/2015  Manjula Chellappan        TMS# 20150611-00059 - Customer - Profile and Profile Amount tab not populating timely   
   * ************************************************************************/
   PROCEDURE SUBMIT_WF_PROCESS IS 
   PRAGMA AUTONOMOUS_TRANSACTION;
   							   
   l_request_id NUMBER; 
   l_program_name VARCHAR2(20) := 'FNDWFBG';
   l_sec VARCHAR2(100);
   l_dflt_email fnd_user.email_address%TYPE
                      := 'HDSOracleDevelopers@hdsupply.com';

   BEGIN
   
		l_sec := 'Apps Initialize';
   		
		fnd_global.apps_initialize (fnd_global.user_id,fnd_global.resp_id,fnd_global.resp_appl_id);

		l_sec := 'Submit workflow Background Process';
		
		l_request_id :=
            fnd_request.submit_request ('FND',
                                        l_program_name,
                                        NULL,
                                        TO_CHAR(sysdate+ 1/(24*60*12),'DD-MON-YYYY HH24:MI:SS'),
                                        FALSE,
                                        'XXWCHZAP',
                                        NULL,
										NULL,
										'Y',
										'Y',
										'Y'
										);	
   
   IF l_request_id >0 THEN
	COMMIT;
   
   ELSE
	xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_CUST_CREDIT_LIMIT_PKG.SUBMIT_WF_PROCESS',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR ( DBMS_UTILITY.format_error_stack ()                                       
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => 'Failed to Submit the Program',
            p_distribution_list   => l_dflt_email,
            p_module              => 'PO');
			commit;
   END IF; 
   
   EXCEPTION WHEN OTHERS THEN
   
    xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_CUST_CREDIT_LIMIT_PKG.SUBMIT_WF_PROCESS',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR ( DBMS_UTILITY.format_error_stack ()                                       
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => 'OTHERS EXCEPTION',
            p_distribution_list   => l_dflt_email,
            p_module              => 'PO');
			commit;
   END SUBMIT_WF_PROCESS;
   
END XXWC_CUST_CREDIT_LIMIT_PKG;
/


