/***********************************************************************************************************************************************
   NAME:       MS#20180523-00113_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        06/05/2018  Rakesh Patel     TMS#20180523-00113
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before update');
   
   UPDATE xxwc.XXWC_OM_QUOTE_HEADERS xoqh1
	  SET xoqh1.valid_until = '01-JAN-2099'
	WHERE EXISTS (SELECT 1
 	                FROM xxwc.XXWC_OM_QUOTE_HEADERS xoqh2
				   WHERE (
				         TO_CHAR(xoqh2.valid_until,'DD-MON-YYYY') = '30-JUN-2018'
				      OR TO_CHAR(xoqh2.valid_until,'DD-MON-YYYY') = '28-JUN-2018' 
					  )
					  AND xoqh2.quote_number = xoqh1.quote_number
				  );

   DBMS_OUTPUT.put_line ('Records Updated -' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
	  ROLLBACK;
END;
/