-----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header AR.XXWC_HZ_RELATIONSHIPS_N4
  File Name: AR.XXWC_HZ_RELATIONSHIPS_N4.sql
  PURPOSE:   
  REVISIONS:
     Ver          Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        06-Jul-2018    Nancy Pahwa        TMS#20180706-00040   
****************************************************************************************************************************/
create index AR.XXWC_HZ_RELATIONSHIPS_N4 ON AR.HZ_RELATIONSHIPS(OBJECT_ID,OBJECT_TYPE,OBJECT_TABLE_NAME,DIRECTION_CODE,RELATIONSHIP_TYPE,START_DATE,END_DATE,STATUS)
/
