CREATE OR REPLACE PACKAGE BODY XXEIS.EIS_RS_XXWC_AR_UTIL_PKG
AS
   /****************************************************************************************************
    *
    * FUNCTION | PROCEDURE | CURSOR
    *  EIS_RS_XXWC_AR_UTIL_PKG
    *
    * DESCRIPTION
    *  EIS report to show account sites that haven't had activity within the last XX month
    *
    * PARAMETERS
    * ==========
    * NAME              TYPE     DESCRIPTION
   .* ----------------- -------- ------------------------------------------------------------------------
    * P_CLASS          <IN>       Class name
    * P_MONTH          <IN>       No of months
    * P_AS_OF_DATE     <IN>       Report run date
    *
    *
    * CALLED BY
    *   Which program, if any, calls this one
    *
    * HISTORY
    * =======
    *
    * VERSION DATE        AUTHOR(S)       DESCRIPTION
    * ------- ----------- --------------- ----------------------------------------------------------------
    * 1.0    07/18/2014   Maharajan S     Creation TMS# 20140220-00170 
    * 1.1    08/25/2015   Maharajan SShunmugam    TMS#20150825-00160 IT - need a view created for the Account Inactivation EiS report
    ******************************************************************************************************/

   PROCEDURE inactive_site_pre_trig (p_class   IN     VARCHAR2,
                                     p_month   IN     NUMBER,
                                     p_date    IN     DATE)
   IS
      l_return_status    VARCHAR2 (1);
      l_msg_count        NUMBER;
      l_msg_data         VARCHAR2 (2000);
      l_error_message    VARCHAR2 (2000);
      n_req_id           NUMBER := 0;
      l_as_of_date       DATE;
      l_class            VARCHAR2 (30);
      l_msg                    VARCHAR2 (150);
      l_user_id                NUMBER        := fnd_global.user_id;
      l_distro_list            VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_sec                    VARCHAR2(200);      
      l_bill_to_site           NUMBER;
      l_bill_sale_date         DATE;
      l_ship_to_site           NUMBER;
      l_ship_sale_date         DATE;
      l_cust_site_use          NUMBER;
      l_payment_date           DATE ;
      l_account_bal            NUMBER;
      l_invoice_site_use       NUMBER;
      l_invoice_order_date     DATE;
      l_ship_site_use          NUMBER;
      l_ship_order_date        DATE;
     
      CURSOR xxwc_inactive_site_cur (l_class_name    VARCHAR2)
      IS
     SELECT /*+leading(hca)*/ cust.account_number,
         cust.account_name,
         party_site.party_site_number,
         site_uses.site_use_id,
         site_uses.location,
         coll.name collector,
         pc.name profile_class,
         site_uses.attribute1 site_classification
    FROM AR.hz_cust_accounts cust,
         AR.hz_parties party,
         AR.hz_party_sites party_site,
         AR.hz_cust_acct_sites_all acct_site,          
         AR.hz_cust_site_uses_all site_uses,       
         AR.hz_locations loc,
         AR.hz_customer_profiles prof,
         AR.hz_cust_profile_classes pc,    
         AR.ar_collectors coll
   WHERE cust.party_id                         = party.party_id
    AND  party.party_id                        = party_site.party_id
    AND  cust.cust_account_id                  = acct_site.cust_account_id
    AND  acct_site.party_site_id               = party_site.party_site_id
    AND acct_site.cust_acct_site_id            = site_uses.cust_acct_site_id
    AND loc.location_id                        = party_site.location_id
    AND prof.site_use_id                       = site_uses.site_use_id   
    AND prof.profile_class_id                  = pc.profile_class_id
    AND prof.collector_id                      = coll.collector_id(+)
    AND site_uses.org_id                       = acct_site.org_id
    AND site_uses.org_id = 162
    AND NVL (acct_site.status, 'A')         = 'A'
    AND NVL (site_uses.status, 'A')         = 'A'
    AND NVL (cust.status, 'A')              = 'A'
    AND pc.name NOT IN ('DEFAULT', 
                         'Intercompany Customers', 
                         'WC Branches')
    AND site_uses.attribute1 IS NOT NULL
    AND site_uses.attribute1                    = NVL (l_class_name, site_uses.attribute1)
    AND site_uses.primary_flag = 'N';
   BEGIN

      l_sec := 'Delete existing records from XXEIS.XXWC_EIS_AR_INACTIVE_SITES_STG';

    
   EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.XXWC_EIS_AR_INACTIVE_SITES_STG';


      ----------------------------------------------------------------
      -- Validation for Class parameter
      ----------------------------------------------------------------

      l_sec := 'Validating Class parameter';

      IF p_class = 'ALL'
      THEN
         l_class := NULL;
      ELSE
         l_class := p_class;
      END IF;

    fnd_file.put_line (fnd_file.LOG, 'L_CLASS: '||l_class);

     l_sec := 'Opening Cursor';
    
        FOR inactive_site_rec IN xxwc_inactive_site_cur (l_class)
        LOOP

          
        l_sec := 'Get last bill to sale date';


	BEGIN
     	SELECT bill_to_site_use_id, 
            MAX (trx_date)
       	INTO l_bill_to_site, 
            l_bill_sale_date
        FROM ar.ra_customer_trx_all
      	WHERE bill_to_site_use_id = inactive_site_rec.site_use_id
   	GROUP BY bill_to_site_use_id;
	EXCEPTION
   	WHEN OTHERS
   	THEN
      	l_bill_to_site := NULL;
      	l_bill_sale_date := NULL;
	END;

	l_sec := 'Get last ship to sale date';         

	BEGIN
            SELECT   ship_to_site_use_id,
                     MAX (trx_date)
                INTO l_ship_to_site,
                     l_ship_sale_date
            FROM ar.ra_customer_trx_all
            where ship_to_site_use_id = inactive_site_rec.site_use_id
            GROUP BY ship_to_site_use_id;
        EXCEPTION
        WHEN OTHERS THEN
          l_ship_to_site := NULL;
          l_ship_sale_date := NULL;
        END;
       
	l_sec := 'Get last payment date'; 
        
	BEGIN
        SELECT   customer_site_use_id,
                 MAX (TRUNC (last_update_date)),
                 SUM (amount_due_remaining) 
            INTO l_cust_site_use,
                 l_payment_date,
                 l_account_bal
         FROM ar.ar_payment_schedules_all
         WHERE customer_site_use_id = inactive_site_rec.site_use_id
         GROUP BY customer_site_use_id;
         EXCEPTION
         WHEN OTHERS THEN
         l_cust_site_use := NULL;
          l_payment_date     := NULL;
         l_account_bal := -1;
        END;
        
        l_sec := 'Check if any open SO for bill to'; 
       
        BEGIN
        SELECT oeh.invoice_to_org_id,
              MAX(oeh.ordered_date)
         INTO l_invoice_site_use,
              l_invoice_order_date 
        FROM ont.oe_order_headers_all oeh
        WHERE oeh.invoice_to_org_id = inactive_site_rec.site_use_id
         AND NOT EXISTS ( SELECT 1 FROM oe_order_headers_all ooh WHERE 
        ooh.invoice_to_org_id = oeh.invoice_to_org_id
        AND ooh.flow_status_code IN ('ENTERED','BOOKED','DRAFT','PENDING_CUSTOMER_ACCEPTANCE'))
        GROUP BY oeh.invoice_to_org_id;
        EXCEPTION
        WHEN OTHERS THEN
           l_invoice_site_use := NULL;
           l_invoice_order_date := NULL;
        END;

       l_sec := 'Check if any open SO for ship to';         

        BEGIN
        SELECT oeh.ship_to_org_id,
              MAX(oeh.ordered_date)
         INTO l_ship_site_use,
              l_ship_order_date 
        FROM ont.oe_order_headers_all oeh
        WHERE oeh.ship_to_org_id = inactive_site_rec.site_use_id
         AND NOT EXISTS ( SELECT 1 FROM oe_order_headers_all ooh WHERE 
        ooh.ship_to_org_id = oeh.ship_to_org_id
        AND ooh.flow_status_code IN ('ENTERED','BOOKED','DRAFT','PENDING_CUSTOMER_ACCEPTANCE'))
        GROUP BY oeh.invoice_to_org_id;
        EXCEPTION
        WHEN OTHERS THEN
           l_ship_site_use := NULL;
           l_ship_order_date := NULL;
        END;

       l_sec := 'Check if any activity'; 

        IF  l_payment_date  < ADD_MONTHS (
                   NVL (
                      P_DATE,
                      TO_DATE (TO_CHAR (SYSDATE, 'DD-MON-YYYY'),
                               'DD-MON-YYYY')),
                   (-1) * p_month) 
        AND l_account_bal = 0 
        AND NVL(l_bill_sale_date,l_ship_sale_date)  < ADD_MONTHS (
                   NVL (
                      P_DATE,
                      TO_DATE (TO_CHAR (SYSDATE, 'DD-MON-YYYY'),
                               'DD-MON-YYYY')),
                   (-1) * p_month)
         AND (l_ship_order_date IS NOT NULL OR l_invoice_order_date IS NOT NULL) THEN

    
       l_sec := 'insert into XXEIS.XXWC_EIS_AR_INACTIVE_SITES_STG'; 

          INSERT INTO XXEIS.XXWC_EIS_AR_INACTIVE_SITES_STG
	  	VALUES( inactive_site_rec.account_number,
			 inactive_site_rec.account_name,
			 inactive_site_rec.party_site_number,
			 inactive_site_rec.location,
			 inactive_site_rec.collector,
			 inactive_site_rec.profile_class,
			 inactive_site_rec.site_classification,
			 NVL(l_ship_sale_date,l_bill_sale_date),
			 l_payment_date,
			 NVL(l_invoice_order_date,l_ship_order_date),
			l_account_bal);                  
                  END IF;
            END LOOP;

COMMIT;

   EXCEPTION
      WHEN OTHERS
      THEN
          l_msg := 'Error: ' || SQLERRM;
         fnd_file.put_line (FND_FILE.LOG,'Error in XXEIS.EIS_RS_XXWC_AR_UTIL_PKG');
         apps.xxcus_error_pkg.xxcus_error_main_api
         (p_called_from            => 'XXEIS.EIS_RS_XXWC_AR_UTIL_PKG',
          p_calling                => l_sec,
          p_request_id             => NULL,
          p_ora_error_msg          => l_msg,
          p_error_desc             => 'Error running XXEIS.EIS_RS_XXWC_AR_UTIL_PKG with PROGRAM ERROR',
          p_distribution_list      => l_distro_list,
          p_module                 => 'AR'
         );
   END;

   /****************************************************************************************************
    *
    * HISTORY
    * =======
    *
    * VERSION DATE        AUTHOR(S)       DESCRIPTION
    * ------- ----------- --------------- ----------------------------------------------------------------
    * 1.0    08/25/2015   Maharajan Shunmugam    TMS#20150825-00160 IT - need a view created for the Account Inactivation EiS report
    *
    ******************************************************************************************************/


   PROCEDURE inactive_acc_pre_trig ( p_month       IN  NUMBER,
                                     p_date        IN  DATE)
   IS
      
      l_sec                              VARCHAR2(200);      
      l_bill_to_customer              NUMBER;
      l_bill_sale_date                DATE;
      l_ship_to_customer              NUMBER;
      l_ship_sale_date                DATE;
      l_customer_id                   NUMBER;
      l_payment_date                  DATE ;
      l_account_bal                   NUMBER;
      l_sold_to_org_id                NUMBER;
      l_order_date                    DATE;
      l_account_name                  VARCHAR2 (2000);
      l_distro_list                   VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_msg                           VARCHAR2 (150);    
     
   BEGIN

   l_sec := 'Delete existing records from XXEIS.EIS_XXWC_AR_INACTIVE_ACC_STG';

    
   EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_AR_INACTIVE_ACC_STG';


      ----------------------------------------------------------------
      -- Open cursor for writing into output file
      ----------------------------------------------------------------

 l_sec := ' Opening cursor';

 FOR I IN (SELECT /*+leading(hca)*/
      cust.account_number,
       cust.account_name,
       cust.cust_account_id,
       party.party_number,
       party.party_name,
       party.party_id,
       coll.name collector,
       pc.name profile_class
  FROM AR.hz_cust_accounts cust,
       AR.hz_parties party,
       AR.hz_customer_profiles prof,
       AR.hz_cust_profile_classes pc,
       AR.ar_collectors coll
 WHERE     cust.party_id = party.party_id
       AND cust.cust_account_id = prof.cust_account_id
       AND prof.profile_class_id = pc.profile_class_id
       AND prof.collector_id = coll.collector_id(+)
       AND NVL (cust.status, 'A') = 'A'
       AND NVL (party.status, 'A') = 'A'
       AND prof.site_use_id IS NULL
       AND pc.name NOT IN ('DEFAULT',
                           'Intercompany Customers',
                           'WC Branches',
                           'Branch Cash Account')
       AND NOT EXISTS
              (SELECT 1
                 FROM hz_party_sites hps,
                      hz_cust_acct_sites hcsa,
                      hz_cust_site_uses hcsu
                WHERE hps.party_id = party.party_id
                 AND  hps.party_site_id = hcsa.party_site_id
                 AND  hcsa.cust_acct_site_id = hcsu.cust_acct_site_id 
                 AND  hps.status = 'A'
                 AND  hcsu.site_use_code != 'BILL_TO')
       AND cust.creation_date < ADD_MONTHS (
                   NVL (
                      P_DATE,
                      TO_DATE (TO_CHAR (SYSDATE, 'DD-MON-YYYY'),
                               'DD-MON-YYYY')),
                   (-1) * p_month))
        LOOP

    l_sec := 'Get last bill to sale date'; 
           
    BEGIN
         SELECT bill_to_customer_id, 
            MAX (trx_date)
           INTO l_bill_to_customer, 
            l_bill_sale_date
        FROM apps.ra_customer_trx       
          WHERE bill_to_customer_id = i.cust_account_id
       GROUP BY bill_to_customer_id;
    EXCEPTION
       WHEN OTHERS
       THEN
          l_bill_to_customer := NULL;
          l_bill_sale_date := NULL;
    END;

    l_sec := 'Get last ship to sale date';         

    BEGIN
            SELECT   ship_to_customer_id,
                     MAX (trx_date)
                INTO l_ship_to_customer,
                     l_ship_sale_date
            FROM apps.ra_customer_trx   
            where ship_to_customer_id = i.cust_account_id
            GROUP BY ship_to_customer_id;
        EXCEPTION
        WHEN OTHERS THEN
          l_ship_to_customer := NULL;
          l_ship_sale_date := NULL;
        END;
       
    l_sec := 'Get last payment date'; 
        
    BEGIN
        SELECT   customer_id,
                 MAX (TRUNC (last_update_date)),
                 SUM (amount_due_remaining) 
            INTO l_customer_id,
                 l_payment_date,
                 l_account_bal
         FROM apps.ar_payment_schedules     
         WHERE customer_id= i.cust_account_id
         GROUP BY customer_id;
         EXCEPTION
         WHEN OTHERS THEN
          l_customer_id:= NULL;
          l_payment_date     := NULL;
          l_account_bal := 0;
        END;
        
        l_sec := 'Check if any open SO '; 
       
        BEGIN
        SELECT oeh.sold_to_org_id,
              MAX(oeh.ordered_date)
         INTO l_sold_to_org_id,
              l_order_date 
        FROM apps.oe_order_headers oeh     
        WHERE oeh.sold_to_org_id = i.cust_account_id
         AND EXISTS ( SELECT 1 FROM oe_order_headers ooh 
                           WHERE ooh.sold_to_org_id = oeh.sold_to_org_id
                            AND ooh.flow_status_code IN ('ENTERED','BOOKED','DRAFT','PENDING_CUSTOMER_ACCEPTANCE'))
        GROUP BY oeh.sold_to_org_id;
        EXCEPTION
        WHEN OTHERS THEN
           l_sold_to_org_id := NULL;
           l_order_date := NULL;
        END;
        

       l_sec := 'Checking if any activity within N period of time'; 

  IF  (l_payment_date  IS NULL OR l_payment_date  < ADD_MONTHS (
                   NVL (
                      P_DATE,
                      TO_DATE (TO_CHAR (SYSDATE, 'DD-MON-YYYY'),
                               'DD-MON-YYYY')),
                   (-1) * p_month))
        AND l_account_bal = 0
        AND (l_bill_sale_date IS NULL OR l_ship_sale_date IS NULL OR NVL(l_bill_sale_date,l_ship_sale_date)  < ADD_MONTHS (
                   NVL (
                      P_DATE,
                      TO_DATE (TO_CHAR (SYSDATE, 'DD-MON-YYYY'),
                               'DD-MON-YYYY')),
                   (-1) * p_month))
         AND l_order_date IS NULL THEN

       l_sec := 'insert into XXEIS.EIS_XXWC_AR_INACTIVE_ACC_STG'; 


          INSERT INTO XXEIS.EIS_XXWC_AR_INACTIVE_ACC_STG
	  	VALUES( i.account_number,
			i.account_name,
			i.collector,
			i.profile_class,
			 NVL(l_ship_sale_date,l_bill_sale_date),
			 l_payment_date,
			 l_order_date,
			l_account_bal);  
                  END IF;
            END LOOP;

COMMIT;
                

   EXCEPTION
      WHEN OTHERS
      THEN
          l_msg := 'Error: ' || SQLERRM;
         fnd_file.put_line (FND_FILE.LOG,'Error in XXWC_AR_CUST_INACTIVATE_PKG');
         apps.xxcus_error_pkg.xxcus_error_main_api
         (p_called_from            => 'XXEIS.EIS_RS_XXWC_AR_UTIL_PKG.INACTIVE_ACC_PRE_TRIG',
          p_calling                => l_sec,
          p_request_id             => NULL,
          p_ora_error_msg          =>  SUBSTR(DBMS_UTILITY.format_error_stack ()|| DBMS_UTILITY.format_error_backtrace (), 1,2000),
          p_error_desc             => 'WHEN OTHERS EXCEPTION in XXEIS.EIS_RS_XXWC_AR_UTIL_PKG',
          p_distribution_list      => l_distro_list,
          p_module                 => 'AR'
         );
   END;

END;
/
--//Below GRANTS need to be executed from XXEIS
GRANT EXECUTE ON XXEIS.EIS_RS_XXWC_AR_UTIL_PKG TO XXEIS
/
GRANT EXECUTE ON XXEIS.EIS_RS_XXWC_AR_UTIL_PKG TO APPS
/