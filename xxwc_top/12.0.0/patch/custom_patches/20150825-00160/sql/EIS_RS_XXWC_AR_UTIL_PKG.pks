CREATE OR REPLACE PACKAGE XXEIS.EIS_RS_XXWC_AR_UTIL_PKG
AS
   /****************************************************************************************************
    *
    * FUNCTION | PROCEDURE | CURSOR
    *  EIS_RS_XXWC_AR_UTIL_PKG
    *
    * DESCRIPTION
    *  EIS report to show account sites that haven't had activity within the last XX month
    *
    * PARAMETERS
    * ==========
    * NAME              TYPE     DESCRIPTION
   .* ----------------- -------- ------------------------------------------------------------------------
    * P_CLASS          <IN>       Class name
    * P_MONTH          <IN>       No of months
    * P_AS_OF_DATE     <IN>       Report run date
    *
    *
    * CALLED BY
    *   Which program, if any, calls this one
    *
    * HISTORY
    * =======
    *
    * VERSION DATE        AUTHOR(S)       DESCRIPTION
    * ------- ----------- --------------- ----------------------------------------------------------------
    * 1.0    07/18/2014   Maharajan S     Creation TMS# 20140220-00170 
    * 1.1    08/25/2015   Maharajan SShunmugam    TMS#20150825-00160 IT - need a view created for the Account Inactivation EiS report
    ******************************************************************************************************/

   PROCEDURE inactive_site_pre_trig (p_class   IN     VARCHAR2,
                                     p_month   IN     NUMBER,
                                     p_date    IN     DATE);
   PROCEDURE inactive_acc_pre_trig  (p_month   IN     NUMBER,               --Added procedure for ver1.1
                                     p_date    IN     DATE);
 END;
/
