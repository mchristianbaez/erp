-----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header EIS_XXWC_CNTR_ORD_TAB_N1
  File Name: EIS_XXWC_CNTR_ORD_TAB_N1.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        06-MAY-2016  SIVA        TMS#20160503-00089 Orders Exception Report - Program Running for longer time
	 1.1       15-Mar-2017   Siva   	 TMS#20170321-00004
****************************************************************************************************************************/
CREATE INDEX XXEIS.EIS_XXWC_CNTR_ORD_TAB_N1 ON XXEIS.EIS_XXWC_CNTR_ORD_TAB (PROCESS_ID,HEADER_ID) 
--TABLESPACE APPS_TS_TX_DATA --Commented for version 1.1
/
