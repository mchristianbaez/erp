---------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_GTT_OE_ORDER_LINES_N2
  File Name: XXWC_GTT_OE_ORDER_LINES_N2.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        20-Jun-16    Siva        TMS# 20160617-00018  --performance Tuning
	 1.1        15-Mar-2017    Siva   	   TMS#20170321-00004
****************************************************************************************************************************/
CREATE INDEX XXEIS.XXWC_GTT_OE_ORDER_LINES_N2 ON XXEIS.XXWC_GTT_OE_ORDER_LINES_TBL
  (
    PROCESS_ID,
    HEADER_ID,
    FLOW_STATUS_CODE
  )
 -- TABLESPACE APPS_TS_TX_DATA  --Commented for version 1.1
/

