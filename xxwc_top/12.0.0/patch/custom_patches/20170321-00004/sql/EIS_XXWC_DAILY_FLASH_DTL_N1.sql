-----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header EIS_XXWC_DAILY_FLASH_DTL_N1
  File Name: EIS_XXWC_DAILY_FLASH_DTL_N1.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        20-Apr-2016  PRAMOD        TMS#20160429-00034 Daily Flash Delivery Report - Program Running for longer time
	 1.1     	15-Mar-2017  Siva   	   TMS#20170321-00004 
****************************************************************************************************************************/
CREATE INDEX XXEIS.EIS_XXWC_DAILY_FLASH_DTL_N1 ON XXEIS.EIS_XXWC_DAILY_FLASH_DTL_TAB (PROCESS_ID)
-- TABLESPACE APPS_TS_TX_DATA  --Commented for version 1.1
/
