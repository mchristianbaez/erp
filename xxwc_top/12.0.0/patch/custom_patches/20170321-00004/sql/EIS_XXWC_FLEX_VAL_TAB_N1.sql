----------------------------------------------------------------------------------------------
/***********************************************************************************************
     $Header XXEIS.EIS_XXWC_FLEX_VAL_TAB_N1
     PURPOSE:   

     REVISIONS:
  VERSION 	DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     05/06/2016          	Pramod   		--TMS#20160503-00085 -- Performance Tuning
  1.1     15-Mar-2017      		Siva   			TMS#20170321-00004
***********************************************************************************************/
CREATE INDEX XXEIS.EIS_XXWC_FLEX_VAL_TAB_N1
 ON XXEIS.EIS_XXWC_FLEX_VAL_TAB (FLEX_VALUE,PROCESS_ID)
-- TABLESPACE APPS_TS_TX_DATA   --Commented for version 1.1
/
