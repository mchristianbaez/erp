----------------------------------------------------------------------------------------------
/***********************************************************************************************
     $Header XXEIS.EIS_DATA_MANAGMNT_DTLS_N1
     PURPOSE:   

     REVISIONS:
  VERSION 	DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     09/01/2016         P.Vamshidhar    TMS#20160824-00263 - Webber File
  1.1     15-Mar-2017    	  Siva   		 TMS#20170321-00004 
***********************************************************************************************/
CREATE INDEX XXEIS.EIS_DATA_MANAGMNT_DTLS_N1
 ON XXEIS.EIS_DATA_MANAGMNT_DTLS (ITEM) --removed semicolon for version 1.1
/
