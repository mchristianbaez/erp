---------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header EIS_XXWC_VALID_BIN_ORGS_N1
  File Name: EIS_XXWC_VALID_BIN_ORGS_N1.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        16-May-2016  Siva        --TMS#20160429-00037 Bin Location Program Running for longer time
	 1.1        15-Mar-2017  Siva   	   TMS#20170321-00004
****************************************************************************************************************************/
CREATE INDEX XXEIS.EIS_XXWC_VALID_BIN_ORGS_N1 ON XXEIS.EIS_XXWC_VALID_BIN_ORGS
  (
    ORGANIZATION_ID,
    PROCESS_ID
  )
  --TABLESPACE APPS_TS_TX_DATA  --Commented for version 1.1
/

