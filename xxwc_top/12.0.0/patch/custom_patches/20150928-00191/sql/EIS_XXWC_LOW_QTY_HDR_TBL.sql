--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_LOW_QTY_HDR_TBL
  Description: This table is used to load data from XXEIS.EIS_RS_XXWC_LOW_QTY_PRI_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     12-Apr-2016        Siva       --TMS#20150928-00191   Performance Tuning
********************************************************************************/
DROP TABLE XXEIS.EIS_XXWC_LOW_QTY_HDR_TBL CASCADE CONSTRAINTS;

CREATE TABLE XXEIS.EIS_XXWC_LOW_QTY_HDR_TBL 
   (	process_id number, 
	header_id number
)
/
