------------------------------------------------------------------------------------
/******************************************************************************
   NAME       :  XXEIS.EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V
   PURPOSE    :  Replaced the old view definition with new view.This view is created based on 
				 custom table XXEIS.XXWC_LOW_QTY_MAN_PRIC_TBL and this custom table data
				 gets populated by XXEIS.EIS_RS_XXWC_LOW_QTY_PRI_PKG package.
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0		 04/12/2016   Siva  			--TMS#20150928-00191 --Performance Tuning                                           
******************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V (PROCESS_ID, BRANCH_NAME, BRANCH, ACCOUNT_MANAGER, CREATED_BY, PRICING_RESPONSIBILITY, HOLD_NAME, APPROVED_BY,
 REL_RES_CODE, REL_RES_COMM, CUSTOMER_NAME, CUSTOMER_NUMBER, ORDER_NUMBER, ORDERED_DATE, INVOICE_NUMBER, INVOICE_DATE, ORDER_TYPE, SHIP_METHOD, VENDOR_NAME, VENDOR_NUMBER,
 ITEM_NUMBER, ITEM_DESCRIPTION, SALES_QTY, BASE_PRICE, ORIGINAL_UNIT_SELLING_PRICE, ORGINAL_EXTEND_PRICE, DOLLAR_LOST, MODIFER_NUMBER, FINAL_SELLING_PRICE, FINAL_EXTENDED_PRICE,
 FINAL_GM, ORGINIAL_GM, DISTRICT, REGION, INVENTORY_ITEM_ID, SHIP_FROM_ORG_ID, ORDERED_QUANTITY, QTY_TYPE, UNIT_COST, LHQTY)
AS
  SELECT process_id, 
    BRANCH_NAME,
    BRANCH,
    ACCOUNT_MANAGER,
    CREATED_BY,
    PRICING_RESPONSIBILITY,
    HOLD_NAME,
    APPROVED_BY,
    REL_RES_CODE,
    REL_RES_COMM,
    CUSTOMER_NAME,
    CUSTOMER_NUMBER,
    ORDER_NUMBER,
    ORDERED_DATE,
    INVOICE_NUMBER,
    INVOICE_DATE,
    ORDER_TYPE,
    SHIP_METHOD,
    VENDOR_NAME,
    VENDOR_NUMBER,
    ITEM_NUMBER,
    ITEM_DESCRIPTION,
    SALES_QTY,
    BASE_PRICE,
    ORIGINAL_UNIT_SELLING_PRICE,
    ORGINAL_EXTEND_PRICE,
    DOLLAR_LOST,
    MODIFER_NUMBER,
    FINAL_SELLING_PRICE,
    FINAL_EXTENDED_PRICE,
    FINAL_GM,
    ORGINIAL_GM,
    DISTRICT,
    REGION,
    INVENTORY_ITEM_ID,
    SHIP_FROM_ORG_ID,
    ORDERED_QUANTITY,
    QTY_TYPE,
    UNIT_COST,
    LHQTY
  FROM XXEIS.XXWC_LOW_QTY_MAN_PRIC_TBL
/
