CREATE OR REPLACE VIEW APPS.XXWC_PO_VENDOR_CONTACTS_VW
  /**************************************************************************************************************
  $Header XXWC_PO_VENDOR_CONTACTS_VW.vw $
  Module Name: XXWC_PO_VENDOR_CONTACTS_VW
  PURPOSE: View for Supplier Contact Inquiry Form
  TMS Task Id : 20150915-00073
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        09/15/2015  Damuluri Gopi         Initial Version
  
  **************************************************************************************************************/
AS
  SELECT PVC.VENDOR_CONTACT_ID VENDOR_CONTACT_ID ,
    APS.VENDOR_SITE_ID VENDOR_SITE_ID ,
    APS.VENDOR_ID ,
    SUBSTR(HP.PERSON_FIRST_NAME,1,15) FIRST_NAME ,
    SUBSTR(HP.PERSON_LAST_NAME,1,15) LAST_NAME ,
    (SELECT HP2.PRIMARY_PHONE_NUMBER
    FROM hz_parties hp2
    WHERE PVC.REL_PARTY_ID         =hp2.party_id
    AND HP2.primary_phone_line_type='GEN'
    ) PHONE ,
    HP2.EMAIL_ADDRESS EMAIL_ADDRESS ,
    (SELECT HCP6.PHONE_NUMBER
    FROM HZ_CONTACT_POINTS HCP6
    WHERE HCP6.OWNER_TABLE_NAME = 'HZ_PARTIES'
    AND PVC.REL_PARTY_ID        = HCP6.OWNER_TABLE_ID
    AND HCP6.CONTACT_POINT_TYPE = 'PHONE'
    AND HCP6.PHONE_LINE_TYPE    = 'FAX'
    AND HCP6.STATUS             ='A'
    AND ROWNUM                  < 2
    ) FAX
  FROM AP_SUPPLIER_CONTACTS PVC ,
    HZ_PARTIES HP ,
    HZ_PARTIES HP2 ,
    AP_SUPPLIER_SITES_ALL APS
  WHERE PVC.PER_PARTY_ID                     = HP.PARTY_ID
  AND PVC.REL_PARTY_ID                       = HP2.PARTY_ID
  AND PVC.ORG_PARTY_SITE_ID                  = APS.PARTY_SITE_ID
  AND NVL(pvc.inactive_date,TRUNC(SYSDATE)) >= TRUNC(SYSDATE)
/