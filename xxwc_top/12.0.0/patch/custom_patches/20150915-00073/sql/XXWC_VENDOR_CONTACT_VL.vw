CREATE OR REPLACE FORCE VIEW APPS.XXWC_VENDOR_CONTACT_VL
  /**************************************************************************************************************
  $Header XXWC_VENDOR_CONTACT_VL.vw $
  Module Name: XXWC_VENDOR_CONTACT_VL
  PURPOSE: View for Supplier Contact Inquiry Form
  ESMS Task Id :
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.1        06/27/2014  Manjula Chellappan   TMS # 20140616-00188 - Drive the search based on Contacts
                                              Removed the inline queries to xxwc_po_vendor_minimum and linked the table to Main query
                                              Removed the function calls
  1.2        10/01/2014   Pattabhi Avula      TMS# 20141001-00057 Multi Org Changes done
  1.3        02/24/2015   P. Vamshidhar       TMS#20141013-00109
                                              Added condition to where clause to show active contacts only
  1.4        09/14/2015   Gopi Damuluri       Replaced the po_vendor_contacts view with XXWC_PO_VENDOR_CONTACTS_VW
                                              for TMS#20150915-00073
  **************************************************************************************************************/
  ( SUPPLIER_NAME, VENDOR_ID, ADDRESS, REMIT_ADD, SITE_NAME, SUPPLIER_NUM, VENDOR_SITE_ID, VENDOR_CONTACT_ID, EMAIL, CONTACT_NAME, FAX, PHONE, VENDOR_MIN_DOLLAR, FREIGHT_MIN_DOLLAR, FREIGHT_MIN_UOM, SUPPLIER_STATUS, EDI_FLAG, PPD_FREIGHT_UNITS, NOTES, DEFAULT_FLAG )
AS
  SELECT DISTINCT asp.vendor_name supplier_name,
    asp.vendor_id --Changed to pick the contact address
    ,
    DECODE ( pscv.vendor_contact_id, NULL, ( ass.address_line1
    || DECODE (ass.address_line2, NULL, NULL, ',')
    || ass.address_line2
    || DECODE (ass.address_line3, NULL, NULL, ',')
    || ass.address_line3
    || DECODE (ass.address_line4, NULL, NULL, ',')
    || ass.address_line4
    || DECODE (ass.city, NULL, NULL, ',')
    || ass.city
    || DECODE (ass.state, NULL, NULL, ',')
    || ass.state
    || DECODE (ass.zip, NULL, NULL, ',')
    || ass.zip
    || DECODE (ass.country, NULL, NULL, ',')
    || ass.country), ( assa.address_line1
    || DECODE (assa.address_line2, NULL, NULL, ',')
    || assa.address_line2
    || DECODE (assa.address_line3, NULL, NULL, ',')
    || assa.address_line3
    || DECODE (assa.address_line4, NULL, NULL, ',')
    || assa.address_line4
    || DECODE (assa.city, NULL, NULL, ',')
    || assa.city
    || DECODE (assa.state, NULL, NULL, ',')
    || assa.state
    || DECODE (assa.zip, NULL, NULL, ',')
    || assa.zip
    || DECODE (assa.country, NULL, NULL, ',')
    || assa.country)) address,
    (SELECT location_code
    FROM hr_locations
    WHERE location_id = ass.bill_to_location_id
    ) remit_add,
    ass.vendor_site_code site_name,
    asp.segment1 supplier_num,
    --Modififed by Manjula for Ver 1.1 Begin
    ass.vendor_site_id,
    /*(SELECT   email_address
    FROM   po_vendor_contacts
    WHERE   vendor_site_id = ass.vendor_site_id)*/
    --assa.email_address
    pscv.vendor_contact_id,
    DECODE (pscv.vendor_contact_id, NULL, ass.email_address, pscv.email_address) email,
    /*(SELECT   first_name || middle_name || last_name
    FROM   po_vendor_contacts
    WHERE   vendor_site_id = ass.vendor_site_id)*/
    --assa.vendor_site_code
    pscv.contact contact_name,
    -- 12/11/2013 CG: TMS 20131211-00143:Updated to use site phone/fax instead of contact phone/fax
    /*(SELECT area_code || fax
    FROM po_vendor_contacts
    WHERE vendor_site_id = ass.vendor_site_id)
    fax,
    (SELECT area_code || phone
    FROM po_vendor_contacts
    WHERE vendor_site_id = ass.vendor_site_id)
    phone,*/
    --(assa.fax_area_code || ' ' || assa.fax)
    DECODE (pscv.vendor_contact_id, NULL, ass.area_code
    || ' '
    || ass.fax, pscv.fax) fax,
    --(assa.area_code || ' ' || assa.phone)
    DECODE (pscv.vendor_contact_id, NULL, ass.area_code
    || ' '
    || ass.phone, pscv.phone) phone,
    -- 12/11/2013 CG: TMS 20131211-00143: Updated to use site phone/fax instead of contact phone/fax
    /*(SELECT SUM (vendor_min_dollar)
    FROM xxwc_po_vendor_minimum xpom
    WHERE     xpom.vendor_id = asp.vendor_id
    AND xpom.vendor_id = ass.vendor_id
    AND xpom.vendor_site_id = ass.vendor_site_id
    -- 01/08/2014 CG: TMS 20130904-00654: Added to restrict vendor min data pull by org
    AND xpom.organization_id =
    TO_NUMBER (
    fnd_profile.VALUE ('XXWC_SUPP_CONT_VM_ORG_ID')))*/
    NVL (xpv.vendor_min_dollar, xpv1.vendor_min_dollar) vendor_min_dollar,
    /*(SELECT SUM (freight_min_dollar)
    FROM xxwc_po_vendor_minimum xpom
    WHERE     xpom.vendor_id = asp.vendor_id
    AND xpom.vendor_id = ass.vendor_id
    AND xpom.vendor_site_id = ass.vendor_site_id
    -- 01/08/2014 CG: TMS 20130904-00654: Added to restrict vendor min data pull by org
    AND xpom.organization_id =
    TO_NUMBER (
    fnd_profile.VALUE ('XXWC_SUPP_CONT_VM_ORG_ID')))*/
    NVL (xpv.freight_min_dollar, xpv1.freight_min_dollar) freight_min_dollar,
    /*(SELECT SUM (freight_min_uom)
    FROM xxwc_po_vendor_minimum xpom
    WHERE     xpom.vendor_id = asp.vendor_id
    AND xpom.vendor_id = ass.vendor_id
    AND xpom.vendor_site_id = ass.vendor_site_id
    -- 01/08/2014 CG: TMS 20130904-00654: Added to restrict vendor min data pull by org
    AND xpom.organization_id =
    TO_NUMBER (
    fnd_profile.VALUE ('XXWC_SUPP_CONT_VM_ORG_ID')))*/
    NVL (xpv.freight_min_uom, xpv1.freight_min_uom) freight_min_uom,
    DECODE (SIGN (SYSDATE - NVL (end_date_active, SYSDATE)), -1, 'In Active', 'Active') supplier_status,
    xxwc_vendor_cont_frm_pkg.xxwc_edi_doctype (ass.tp_header_id) ediflag
    /*(
    SELECT DISTINCT edi_flag
    FROM ece_tp_details etd, ece_tp_headers eth
    WHERE     etd.TP_header_ID = eth.tp_header_id
    AND etd.tp_header_id = ass.tp_header_id)
    EDI_FLAG*/
    -- 01/08/14 CG: TMS 20131212-00176: Added new Vendor Min fields
    ,
    /*
    (SELECT SUM (ppd_freight_units)
    FROM xxwc_po_vendor_minimum xpom
    WHERE     xpom.vendor_id = asp.vendor_id
    AND xpom.vendor_id = ass.vendor_id
    AND xpom.vendor_site_id = ass.vendor_site_id
    -- 01/08/2014 CG: TMS 20130904-00654: Added to restrict vendor min data pull by org
    AND xpom.organization_id =
    TO_NUMBER (
    fnd_profile.VALUE ('XXWC_SUPP_CONT_VM_ORG_ID')))*/
    NVL (xpv.ppd_freight_units, xpv1.ppd_freight_units) ppd_freight_units,
    /* SUBSTR (
    (xxwc_ascp_scwb_pkg.get_vendor_minimum_notes (
    asp.vendor_id,
    ass.vendor_site_id,
    TO_NUMBER (fnd_profile.VALUE ('XXWC_SUPP_CONT_VM_ORG_ID')))),
    1,
    4000)*/
    SUBSTR (NVL (xpv.notes, xpv1.notes), 1, 4000) notes,
    NVL (NVL (xpv.default_flag, xpv1.default_flag), 'N') default_flag
    --Modififed by Manjula for Ver 1.1 End
    -- 01/08/14 CG: TMS 20131212-00176: Added new Vendor Min fields
  FROM ap_suppliers asp,
    apps.ap_supplier_sites ass --Modififed by Manjula for Ver 1.1 Begin
    ,
    (SELECT vendor_id,
      vendor_contact_id,
      vendor_site_id,
      (DECODE (pvc.first_name, NULL, pvc.last_name, pvc.last_name
      || ', '
      || pvc.first_name)) contact,
      pvc.phone,
      pvc.fax,
      pvc.email_address
    FROM xxwc_po_vendor_contacts_vw pvc -- Ver# 1.4 -- WHERE pvc.inactive_date >= TRUNC(SYSDATE) -- Added by Vamshi on 24-Feb-2015 for TMS#20141013-00109  to show active contacts - 1.3
    ) pscv,
    apps.ap_supplier_sites assa,
    (SELECT *
    FROM apps.xxwc_po_vendor_minimum
    WHERE organization_id = TO_NUMBER ( fnd_profile.VALUE ('XXWC_SUPP_CONT_VM_ORG_ID'))
    ) xpv,
    (SELECT *
    FROM apps.xxwc_po_vendor_minimum
    WHERE organization_id  = TO_NUMBER ( fnd_profile.VALUE ('XXWC_SUPP_CONT_VM_ORG_ID'))
    AND vendor_contact_id IS NULL
    ) xpv1
    --Modififed by Manjula for Ver 1.1 End
  WHERE asp.vendor_id = ass.vendor_id
    --Modififed by Manjula for Ver 1.1 Begin
  AND TRUNC (NVL (ass.inactive_date, SYSDATE + 1)) > TRUNC (SYSDATE)
  AND ass.vendor_site_id                           = pscv.vendor_site_id(+)
  AND pscv.contact                                 = assa.vendor_site_code(+)
  AND asp.vendor_id                                = NVL (assa.vendor_id, asp.vendor_id)
  AND ass.purchasing_site_flag                     = 'Y'
  AND pscv.vendor_contact_id                       = xpv.vendor_contact_id(+)
  AND pscv.vendor_id                               = xpv.vendor_id(+)
  AND pscv.vendor_site_id                          = xpv.vendor_site_id(+)
  AND ass.vendor_id                                = xpv1.vendor_id(+)
  AND ass.vendor_site_id                           = xpv1.vendor_site_id(+)
    --   AND ass.org_id = fnd_profile.VALUE ('org_id')  -- Commented by pattabhi on 10/15/2014 for TMS# 20141001-00057 Multi org Change
  ORDER BY 1, 5, 10
    --Modififed by Manjula for Ver 1.1 End
/