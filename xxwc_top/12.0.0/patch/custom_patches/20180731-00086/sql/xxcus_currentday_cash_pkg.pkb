CREATE OR REPLACE PACKAGE BODY APPS.xxcus_currentday_cash_pkg as
   /*
      ************************************************************************
    HD Supply
    All rights reserved.
   **************************************************************************
     PURPOSE:   GSC Current day cash positioning BAI file process

     REVISIONS:
     Ver        Date                Author                           Description
     ---------  ----------         ---------------                  -------------------------------------------------------------------------------------------------------------
     1.0       09/21/2016  Balaguru Seshadri     TMS:  20161026-00104 / ESMS 322411 - GSC Current day cash positioning BAI file process
     1.1       12/15/2016  Balaguru Seshadri     TMS:   20161215-00113 / ESMS 501131 - Remove zip file logic and send files individually to Treasury
     1.2       08/15/2018  Vamshi Singirikonda   TMS:  20180815-00062 / HDS Treasury: Remove OEBS uc4 job item and replace with SQL wrapper
     1.3       08/30/2018  Balaguru Seshadri      TMS:  20180731-00086 / HDS Treasury: Fix to remove the payroll wires from WellsFargo File     
   *************************************************************************
  */
    --
    -- Begin global variables
    --
     g_header_found varchar2(1) :='N';
     g_field_delimiter varchar2(2) :=',';
     g_user number :=fnd_global.user_id;
     --
    --
    g_notif_email_from varchar2(240) :='OracleEBS.Treasury.CashPositioning@hdsupply.com';
    g_notif_email_to varchar2(240) :='treasury@hdsupply.com';
    g_send_email_attach_to varchar2(240) :='treasury_inbox@share.hdsupply.com';
    g_notif_email_cc varchar2(240) :='HDS.OracleGLSupport@hdsupply.com';
    --
    g_text1 varchar2(2000) :=Null;
    g_carriage_return varchar2(1) :='
';
    g_notif_email_subject varchar2(240) :=Null;
    g_notif_email_body varchar2(4000) :=Null;
    g_email_mime_type  varchar2(150) :='text; charset=us-ascii';
    --
     g_command varchar2(2000) :=Null;
     g_out_file_zip varchar2(30) :='.zip';
     --
     g_out_file_prefix varchar2(150) :=Null;
     g_out_file_ext varchar2(150) :='.csv';
     g_out_file_name_temp varchar2(150) :=Null;
     --
     g_all_codes_file varchar2(150) :=Null;
     g_reqd_codes_file varchar2(150) :=Null;
     g_error_codes_file varchar2(150) :=Null;
     --
     g_cash_posn_attr_cat varchar2(30) :='Cash Positioning';
     g_cash_posn_attribute2 varchar2(10) :='yes';
     g_all_03_lines varchar2(240) :=Null;
     g_org_id number :=0;
     --
     g_ce_bank_name apps.ce_bank_branches_v.bank_name%type :=Null;
     g_ce_bank_br_name apps.ce_bank_branches_v.bank_branch_name%type :=Null;
     g_bank_account_name apps.ce_bank_accounts.bank_account_name%type :=Null;
     --
     g_bank_file_create_date date;
     g_bank_account_num apps.ce_bank_accounts.bank_account_num%type :=Null;
     g_currency varchar2(60) :=Null;
     --
     g_bank_name varchar2(30) :=Null;
     g_request_id number :=fnd_global.conc_request_id;
     g_proceed boolean;
     g_program apps.fnd_concurrent_programs.concurrent_program_name%type :=Null;
      --
      g_file_name xxcus.xxcus_ce_current_day_files_b.file_name%type;
      g_creation_date xxcus.xxcus_ce_current_day_files_b.creation_date%type;
      g_imported_flag  xxcus.xxcus_ce_current_day_files_b.file_imported_flag%type;
     --
     g_new varchar2(15) :='NEW';
     g_inprocess varchar2(15) :='IN PROCESS';
     g_error varchar2(15) :='ERROR';
     g_complete varchar2(15) :='COMPLETE';
     --
     g_prereq_error varchar2(240) :='Exceeded maximum files allowed per day.';
     g_bank_file_copied varchar2(240) :='Bank file backed up.';
     g_bank_file_imported varchar2(240) :='Bank file imported.';
     g_bai_codes_complete varchar2(240) :='BAI codes extracted.';
     g_control_file varchar2(150) :=Null;
     g_data_file varchar2(240) :=Null;
     --
     g_WELLSFARGO_prefix varchar2(20) :='D';
     g_BOFA_US_prefix varchar2(20) :='hd_supply_us_cd';
     g_BOFA_CA_prefix varchar2(20) :='hd_supply_canada_cd';
     g_final_product_file varchar2(60) :='HDS_CURRDAY_BAI_RESULTS.zip';
     --
     g_bank_WF VARCHAR2(20) :='WELLSFARGO';
     g_bank_BOA_US VARCHAR2(20) :='BOA_US';
     g_bank_BOA_CA VARCHAR2(20) :='BOA_CA';
     g_WF_File varchar2(150) :=Null; -- Ver 1.3
     --
     g_WF_bai2_sql_ldr_job             apps.fnd_concurrent_programs.concurrent_program_name%type := 'XXCUS_WELLSFARGO_BAI2';
     g_BOA_US_bai2_sql_ldr_job   apps.fnd_concurrent_programs.concurrent_program_name%type := 'XXCUS_BOFA_US_BAI2';
     g_BOA_CA_bai2_sql_ldr_job   apps.fnd_concurrent_programs.concurrent_program_name%type := 'XXCUS_BOFA_CA_BAI2';
     --
     -- rec id defaults
     --
     g_rec_id_01 varchar2(2) :='01';
     g_rec_id_02 varchar2(2) :='02';
     g_rec_id_03 varchar2(2) :='03';
     g_rec_id_16 varchar2(2) :='16';
     g_rec_id_88 varchar2(2) :='88';
     g_rec_id_49 varchar2(2) :='49';
     g_rec_id_98 varchar2(2) :='98';
     g_rec_id_99 varchar2(2) :='99';
     --
     -- begin rec id 01 fields
     g_rec_type varchar2(2);
     g_sender_id varchar2(60);
     g_receiver_id varchar2(60);
     g_file_creation_date varchar2(10);
     g_file_creation_time varchar2(10);
     g_file_id_number varchar2(3);
     g_physical_rec_length varchar2(3);
     g_block_size varchar2(10);
     g_version_number varchar2(3);
     -- end rec id 01 fields
     --
     -- End global variables
     --
  procedure print_log(p_message in varchar2) as
  begin
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  end print_log;
  --
  procedure print_out(p_message in varchar2) as
  begin
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.output, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  end print_out;
  --
 procedure send_email_notif
   (
        p_email_from in varchar2,
        p_email_to in varchar2,
        p_email_cc in varchar2,
        p_email_subject in varchar2,
        p_email_body in varchar2,
        p_mime_type in varchar2
   ) is
  begin
      --
      UTL_MAIL.send
          (
            sender          =>p_email_from,
            recipients    =>p_email_to,
            cc                  =>p_email_cc,
            subject        =>p_email_subject,
            message     =>p_email_body,
            mime_type =>p_mime_type
         );
      --
  exception
   when others then
    print_log('Issue in sending email notification : message =>'||sqlerrm);
  end send_email_notif;
  --
  function get_03lines_excl_499899_boa_us (p_from_line in number, p_to_line in number, p_last_rec in varchar2) return number is
    --
    n_count number :=0;
    --
    cursor get_linestoscan_for_last_03 is
      select count(c.rec_no)
      from xxcus.xxcus_ce_current_day_files_b a
               ,xxcus.xxcus_ce_current_day_records_b b
               ,xxcus.xxcus_ce_stmt_int_tmp2_boa_us c
      where 1 =1
           and b.bank_name =g_bank_name
           and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
           and trim(b.file_record) is not null
           and (
                      (p_last_rec ='N' and (c.rec_no between p_from_line and p_to_line-1 and c.rec_id_no <>g_rec_id_49))
                    OR
                      (p_last_rec ='Y' and c.rec_no >=p_from_line and c.rec_id_no NOT IN (g_rec_id_49,g_rec_id_98, g_rec_id_99) )
                   )
           and a.bank_name =b.bank_name
           and c.bank_name =a.bank_name
           and a.file_name =b.file_name
           and c.file_name =a.file_name
           and b.status_code ='STAGE-4'
           and a.status_code =b.status_code
           and c.status_code =a.status_code
           and trunc(b.creation_date) =trunc(sysdate)
           and trunc(a.creation_date) =trunc(b.creation_date)
           and trunc(c.creation_date) =trunc(c.creation_date)
           and a.file_imported_flag ='Y'
           and c.file_imported_flag =a.file_imported_flag
           and b.line_number =c.rec_no
           and a.creation_date =
                     (
                        select max(d.creation_date)
                        from  xxcus.xxcus_ce_current_day_files_b d
                        where 1 =1
                             and  d.bank_name =a.bank_name
                             and  d.file_name =a.file_name
                             and  d.status_code =a.status_code
                             and  d.file_imported_flag =a.file_imported_flag
                             and  d.request_id =a.request_id
                     )
            ;
    --
  begin
      --
      open get_linestoscan_for_last_03;
      fetch get_linestoscan_for_last_03 into n_count;
      close get_linestoscan_for_last_03;
      --
      return n_count;
      --
  exception
    when others then
     print_log ('@ get_03lines_excl_499899_boa_us, error : '||sqlerrm);
     return n_count;
     raise program_error;
  end get_03lines_excl_499899_boa_us;
  --
  function get_03lines_excl_499899_wf (p_from_line in number, p_to_line in number, p_last_rec in varchar2) return number is
    --
    n_count number :=0;
    --
    cursor get_linestoscan_for_last_03 is
      select count(c.rec_no)
      from xxcus.xxcus_ce_current_day_files_b a
               ,xxcus.xxcus_ce_current_day_records_b b
               ,xxcus.xxcus_ce_stmt_int_tmp2_wf c
      where 1 =1
           and b.bank_name =g_bank_name
           and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
           and trim(b.file_record) is not null
           and (
                      (p_last_rec ='N' and (c.rec_no between p_from_line and p_to_line-1 and c.rec_id_no <>g_rec_id_49))
                    OR
                      (p_last_rec ='Y' and c.rec_no >=p_from_line and c.rec_id_no NOT IN (g_rec_id_49,g_rec_id_98, g_rec_id_99) )
                   )
           and a.bank_name =b.bank_name
           and c.bank_name =a.bank_name
           and a.file_name =b.file_name
           and c.file_name =a.file_name
           and b.status_code ='STAGE-4'
           and a.status_code =b.status_code
           and c.status_code =a.status_code
           and trunc(b.creation_date) =trunc(sysdate)
           and trunc(a.creation_date) =trunc(b.creation_date)
           and trunc(c.creation_date) =trunc(c.creation_date)
           and a.file_imported_flag ='Y'
           and c.file_imported_flag =a.file_imported_flag
           and b.line_number =c.rec_no
           and a.creation_date =
                     (
                        select max(d.creation_date)
                        from  xxcus.xxcus_ce_current_day_files_b d
                        where 1 =1
                             and  d.bank_name =a.bank_name
                             and  d.file_name =a.file_name
                             and  d.status_code =a.status_code
                             and  d.file_imported_flag =a.file_imported_flag
                             and  d.request_id =a.request_id
                     )
            ;
    --
  begin
      --
      open get_linestoscan_for_last_03;
      fetch get_linestoscan_for_last_03 into n_count;
      close get_linestoscan_for_last_03;
      --
      return n_count;
      --
  exception
    when others then
     print_log ('@ get_03lines_excl_499899_wf, error : '||sqlerrm);
     return n_count;
     raise program_error;
  end get_03lines_excl_499899_wf;
  --
  function get_03lines_excl_499899_boa_ca (p_from_line in number, p_to_line in number, p_last_rec in varchar2) return number is
    --
    n_count number :=0;
    --
    cursor get_linestoscan_for_last_03 is
      select count(c.rec_no)
      from xxcus.xxcus_ce_current_day_files_b a
               ,xxcus.xxcus_ce_current_day_records_b b
               ,xxcus.xxcus_ce_stmt_int_tmp2_boa_ca c
      where 1 =1
           and b.bank_name =g_bank_name
           and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
           and trim(b.file_record) is not null
           and (
                      (p_last_rec ='N' and (c.rec_no between p_from_line and p_to_line-1 and c.rec_id_no <>g_rec_id_49))
                    OR
                      (p_last_rec ='Y' and c.rec_no >=p_from_line and c.rec_id_no NOT IN (g_rec_id_49,g_rec_id_98, g_rec_id_99) )
                   )
           and a.bank_name =b.bank_name
           and c.bank_name =a.bank_name
           and a.file_name =b.file_name
           and c.file_name =a.file_name
           and b.status_code ='STAGE-4'
           and a.status_code =b.status_code
           and c.status_code =a.status_code
           and trunc(b.creation_date) =trunc(sysdate)
           and trunc(a.creation_date) =trunc(b.creation_date)
           and trunc(c.creation_date) =trunc(c.creation_date)
           and a.file_imported_flag ='Y'
           and c.file_imported_flag =a.file_imported_flag
           and b.line_number =c.rec_no
           and a.creation_date =
                     (
                        select max(d.creation_date)
                        from  xxcus.xxcus_ce_current_day_files_b d
                        where 1 =1
                             and  d.bank_name =a.bank_name
                             and  d.file_name =a.file_name
                             and  d.status_code =a.status_code
                             and  d.file_imported_flag =a.file_imported_flag
                             and  d.request_id =a.request_id
                     )
            ;
    --
  begin
      --
      open get_linestoscan_for_last_03;
      fetch get_linestoscan_for_last_03 into n_count;
      close get_linestoscan_for_last_03;
      --
      return n_count;
      --
  exception
    when others then
     print_log ('@ get_03lines_excl_499899_boa_ca, error : '||sqlerrm);
     return n_count;
     raise program_error;
  end get_03lines_excl_499899_boa_ca;
  --
  procedure get_all_03_linenums_boa_us is
    --
    cursor get_all_03_lines is
      select
                  listagg(c.rec_no,',') within group (order by c.rec_no)  rec_nos
      from xxcus.xxcus_ce_current_day_files_b a
               ,xxcus.xxcus_ce_current_day_records_b b
               ,xxcus.xxcus_ce_stmt_int_tmp2_boa_us c
      where 1 =1
           and b.bank_name =g_bank_name
          and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
           and c.rec_id_no =g_rec_id_03
           and trim(b.file_record) is not null
           and a.bank_name =b.bank_name
           and c.bank_name =a.bank_name
           and a.file_name =b.file_name
           and c.file_name =a.file_name
           and b.status_code ='STAGE-4'
           and a.status_code =b.status_code
           and c.status_code =a.status_code
           and trunc(b.creation_date) =trunc(sysdate)
           and trunc(a.creation_date) =trunc(b.creation_date)
           and trunc(c.creation_date) =trunc(c.creation_date)
           and a.file_imported_flag ='Y'
           and c.file_imported_flag =a.file_imported_flag
           and b.line_number =c.rec_no
           and a.creation_date =
                     (
                        select max(d.creation_date)
                        from  xxcus.xxcus_ce_current_day_files_b d
                        where 1 =1
                             and  d.bank_name =a.bank_name
                             and  d.file_name =a.file_name
                             and  d.status_code =a.status_code
                             and  d.file_imported_flag =a.file_imported_flag
                             and  d.request_id =a.request_id
                     )
    order by b.line_number asc;
    --
  begin
      --
      open get_all_03_lines;
      fetch get_all_03_lines into g_all_03_lines;
      close get_all_03_lines;
      --
      g_all_03_lines :=nvl(g_all_03_lines, 'NA');
      --
  exception
    when no_data_found then
     print_log ('@ get_all_03_linenums_boa_us, error : '||sqlerrm);
     g_all_03_lines :='NA';
    when others then
     print_log ('@ get_all_03_linenums_boa_us, error : '||sqlerrm);
     g_all_03_lines :='NA';
     raise program_error;
  end get_all_03_linenums_boa_us;
   --
  procedure get_all_03_linenums_wf is
    --
    cursor get_all_03_lines is
      select
                  listagg(c.rec_no,',') within group (order by c.rec_no)  rec_nos
      from xxcus.xxcus_ce_current_day_files_b a
               ,xxcus.xxcus_ce_current_day_records_b b
               ,xxcus.xxcus_ce_stmt_int_tmp2_wf c
      where 1 =1
           and b.bank_name =g_bank_name
           and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
           and c.rec_id_no =g_rec_id_03
           and trim(b.file_record) is not null
           and a.bank_name =b.bank_name
           and c.bank_name =a.bank_name
           and a.file_name =b.file_name
           and c.file_name =a.file_name
           and b.status_code ='STAGE-4'
           and a.status_code =b.status_code
           and c.status_code =a.status_code
           and trunc(b.creation_date) =trunc(sysdate)
           and trunc(a.creation_date) =trunc(b.creation_date)
           and trunc(c.creation_date) =trunc(c.creation_date)
           and a.file_imported_flag ='Y'
           and c.file_imported_flag =a.file_imported_flag
           and b.line_number =c.rec_no
           and a.creation_date =
                     (
                        select max(d.creation_date)
                        from  xxcus.xxcus_ce_current_day_files_b d
                        where 1 =1
                             and  d.bank_name =a.bank_name
                             and  d.file_name =a.file_name
                             and  d.status_code =a.status_code
                             and  d.file_imported_flag =a.file_imported_flag
                             and  d.request_id =a.request_id
                     )
    order by b.line_number asc;
    --
  begin
      --
      open get_all_03_lines;
      fetch get_all_03_lines into g_all_03_lines;
      close get_all_03_lines;
      --
      g_all_03_lines :=nvl(g_all_03_lines, 'NA');
      --
  exception
    when others then
     print_log ('@ get_all_03_linenums_wf, error : '||sqlerrm);
     g_all_03_lines :='NA';
     raise program_error;
  end get_all_03_linenums_wf;
   --
  procedure get_all_03_linenums_boa_ca is
    --
    cursor get_all_03_lines is
      select
                  listagg(c.rec_no,',') within group (order by c.rec_no)  rec_nos
      from xxcus.xxcus_ce_current_day_files_b a
               ,xxcus.xxcus_ce_current_day_records_b b
               ,xxcus.xxcus_ce_stmt_int_tmp2_boa_ca c
      where 1 =1
           and b.bank_name =g_bank_name
           and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
           and c.rec_id_no =g_rec_id_03
           and trim(b.file_record) is not null
           and a.bank_name =b.bank_name
           and c.bank_name =a.bank_name
           and a.file_name =b.file_name
           and c.file_name =a.file_name
           and b.status_code ='STAGE-4'
           and a.status_code =b.status_code
           and c.status_code =a.status_code
           and trunc(b.creation_date) =trunc(sysdate)
           and trunc(a.creation_date) =trunc(b.creation_date)
           and trunc(c.creation_date) =trunc(c.creation_date)
           and a.file_imported_flag ='Y'
           and c.file_imported_flag =a.file_imported_flag
           and b.line_number =c.rec_no
           and a.creation_date =
                     (
                        select max(d.creation_date)
                        from  xxcus.xxcus_ce_current_day_files_b d
                        where 1 =1
                             and  d.bank_name =a.bank_name
                             and  d.file_name =a.file_name
                             and  d.status_code =a.status_code
                             and  d.file_imported_flag =a.file_imported_flag
                             and  d.request_id =a.request_id
                     )
    order by b.line_number asc;
    --
  begin
      --
      open get_all_03_lines;
      fetch get_all_03_lines into g_all_03_lines;
      close get_all_03_lines;
      --
      g_all_03_lines :=nvl(g_all_03_lines, 'NA');
      --
  exception
    when no_data_found then
     print_log ('@ get_all_03_linenums_boa_ca, error : '||sqlerrm);
     g_all_03_lines :='NA';
    when others then
     print_log ('@ get_all_03_linenums_boa_ca, error : '||sqlerrm);
     g_all_03_lines :='NA';
     raise program_error;
  end get_all_03_linenums_boa_ca;
   --
   -- Begin Ver 1.3
  function get_database_name  return varchar2 is
    v_db varchar2(240) :=Null;
  begin 
      --
    select lower(name)
    into    v_db
    from  v$database
          ;
      --
       return v_db;
      --
  exception
    when no_data_found then
     print_log ('get_database_name, no data error : '||sqlerrm);
     return null;
     raise program_error;      
    when others then
     print_log ('get_database_name, other error : '||sqlerrm);
     return null;
     raise program_error;
  end get_database_name;
   --    
  function get_wellsfargo_file  return varchar2 is
    v_file varchar2(240) :=Null;
  begin 
      --
    select file_name
    into    v_file
    from  xxcus.xxcus_ce_current_day_files_b A1
    where 1 =1
         and bank_name =g_bank_WF
         and A1.status_code ='STAGE-3'
         and A1.file_imported_flag ='N'
         and A1.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
         and A1.creation_date =
                 (
                    select max(A2.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b A2
                    where 1 =1
                         and  A2.bank_name =A1.bank_name
                         and  A2.file_name =A1.file_name
                         and  A2.status_code =A1.status_code
                         and  A2.file_imported_flag =A1.file_imported_flag
                 )
          ;
      --
      print_log('Today''s WellsFargo File : '||v_file);
      --
       return v_file;
      --
  exception
    when no_data_found then
     print_log ('get_wellsfargo_file, no data error : '||sqlerrm);
     return null;
     raise program_error;    
    when too_many_rows then
     print_log ('get_wellsfargo_file, too many rows error : '||sqlerrm);
     return null;
     raise program_error;      
    when others then
     print_log ('get_wellsfargo_file, other error : '||sqlerrm);
     return null;
     raise program_error;
  end get_wellsfargo_file;
   --  
   -- End Ver 1.3
   --    
  function get_location (v_operation in varchar2, v_type  in varchar2) return varchar2 is
    v_loc varchar2(240) :=Null;
  begin
      --
      if v_operation ='DBA_DIRECTORY' then 
       --
           if v_type = 'INBOUND' then
               --
               v_loc := 'XXCUS_CE_CURR_DAY_INBOUND_LOC';
               --
           elsif v_type = 'OUTBOUND' then
               --
               v_loc := 'XXCUS_CE_CURR_DAY_OUTBOUND_LOC';
               --
           elsif v_type = 'INBOUND_ARCHIVE' then
               --
               v_loc := 'XXCUS_CE_CURR_DAY_INB_ARCH';
               --
           elsif v_type = 'OUTBOUND_ARCHIVE' then
               --
               v_loc := 'XXCUS_CE_CURR_DAY_OUTB_ARCH';
               --                
           else
               --
               v_loc := Null;
               --
           end if;
       --
      elsif v_operation ='ABSOLUTE_PATH' then 
       --
           if v_type = 'INBOUND' then
               --
               select '/xx_iface/'||lower(name)||'/inbound/uc4/current_day_cash'
               into    v_loc
               from  v$database;
               --
           elsif v_type = 'OUTBOUND' then
               --
               select '/xx_iface/'||lower(name)||'/outbound/uc4/current_day_cash'
               into    v_loc
               from  v$database;
               --
           elsif v_type = 'INBOUND_ARCHIVE' then
               --
               select '/xx_iface/'||lower(name)||'/inbound/uc4/current_day_cash/archive'
               into    v_loc
               from  v$database;
               --
           elsif v_type = 'OUTBOUND_ARCHIVE' then
               --
               select '/xx_iface/'||lower(name)||'/outbound/uc4/current_day_cash/archive'
               into    v_loc
               from  v$database;
               --               
           else
               --
               v_loc := Null;
               --
           end if;
       --
      else
               --
               v_loc := Null;
               --
      end if;
      --
      print_log('File location details: Operation :'||v_operation||', Type :'||v_type||', directory =>'||v_loc);
      --
       return v_loc;
      --
  exception
    when others then
     print_log ('get_location, error : '||sqlerrm);
     return null;
     raise program_error;
  end get_location;
   --
  function get_field (v_delimiter in varchar2, n_field_no in number ,v_line_read in varchar2, p_which_line in number ) return varchar2 is
       n_start_field_pos number;
       n_end_field_pos   number;
       v_get_field       varchar2(2000);
  begin
       if n_field_no = 1 then
          n_start_field_pos := 1;
       else
          n_start_field_pos := instr(v_line_read,v_delimiter,1,n_field_no-1)+1;
       end if;

       n_end_field_pos   := instr(v_line_read,v_delimiter,1,n_field_no) -1;
       if n_end_field_pos > 0 then
          v_get_field := substr(v_line_read,n_start_field_pos,(n_end_field_pos - n_start_field_pos)+1);
       else
          v_get_field := substr(v_line_read,n_start_field_pos);
       end if;

       return v_get_field;

  exception
    when others then
     print_log ('Line# ='||p_which_line);
     print_log ('Line ='||v_line_read);
     print_log ('get field: '||sqlerrm);
  end get_field;
  --
  procedure backup_stmt_int_tmp
   (
     p_bank_name in varchar2
    ,p_file_name in varchar2
    ,p_creation_date in date
    ,p_file_imported_flag in varchar2
    ,p_request_id in number
    ,p_status_code in varchar2
   ) is
   begin
    --
    --
        if p_bank_name =g_bank_WF then
                 delete xxcus.xxcus_ce_stmt_int_tmp2_wf;
        elsif p_bank_name =g_bank_BOA_US then
                 delete xxcus.xxcus_ce_stmt_int_tmp2_boa_us;
        elsif p_bank_name =g_bank_BOA_CA then
                 delete xxcus.xxcus_ce_stmt_int_tmp2_boa_ca;
        else
                 Null;
        end if;
    --
        if p_bank_name =g_bank_WF then
                --
                insert into xxcus.xxcus_ce_stmt_int_tmp2_wf
                 (
                     bank_name
                    ,file_name
                    ,creation_date
                    ,file_imported_flag
                    ,request_id
                    ,status_code
                    ,rec_no
                    ,rec_id_no
                    ,column1
                    ,column2
                    ,column3
                    ,column4
                    ,column5
                    ,column6
                    ,column7
                    ,column8
                    ,column9
                    ,column10
                    ,column11
                    ,column12
                    ,column13
                    ,column14
                    ,column15
                    ,column16
                    ,column17
                    ,column18
                    ,column19
                    ,column20
                    ,column21
                    ,column22
                    ,column23
                    ,column24
                    ,column25
                    ,column26
                    ,column27
                    ,column28
                    ,column29
                    ,column30
                    ,column31
                    ,column32
                    ,column33
                    ,column34
                    ,column35
                 )
                (
                  select
                     p_bank_name
                    ,p_file_name
                    ,p_creation_date
                    ,p_file_imported_flag
                    ,p_request_id
                    ,p_status_code
                    ,rec_no
                    ,rec_id_no
                    ,column1
                    ,column2
                    ,column3
                    ,column4
                    ,column5
                    ,column6
                    ,column7
                    ,column8
                    ,column9
                    ,column10
                    ,column11
                    ,column12
                    ,column13
                    ,column14
                    ,column15
                    ,column16
                    ,column17
                    ,column18
                    ,column19
                    ,column20
                    ,column21
                    ,column22
                    ,column23
                    ,column24
                    ,column25
                    ,column26
                    ,column27
                    ,column28
                    ,column29
                    ,column30
                    ,column31
                    ,column32
                    ,column33
                    ,column34
                    ,column35
                  from xxcus.xxcus_ce_stmt_int_tmp_wf
                );
                --
        elsif p_bank_name =g_bank_BOA_US then
                --
                insert into xxcus.xxcus_ce_stmt_int_tmp2_boa_us
                 (
                     bank_name
                    ,file_name
                    ,creation_date
                    ,file_imported_flag
                    ,request_id
                    ,status_code
                    ,rec_no
                    ,rec_id_no
                    ,column1
                    ,column2
                    ,column3
                    ,column4
                    ,column5
                    ,column6
                    ,column7
                    ,column8
                    ,column9
                    ,column10
                    ,column11
                    ,column12
                    ,column13
                    ,column14
                    ,column15
                    ,column16
                    ,column17
                    ,column18
                    ,column19
                    ,column20
                    ,column21
                    ,column22
                    ,column23
                    ,column24
                    ,column25
                    ,column26
                    ,column27
                    ,column28
                    ,column29
                    ,column30
                    ,column31
                    ,column32
                    ,column33
                    ,column34
                    ,column35
                 )
                (
                  select
                     p_bank_name
                    ,p_file_name
                    ,p_creation_date
                    ,p_file_imported_flag
                    ,p_request_id
                    ,p_status_code
                    ,rec_no
                    ,rec_id_no
                    ,column1
                    ,column2
                    ,column3
                    ,column4
                    ,column5
                    ,column6
                    ,column7
                    ,column8
                    ,column9
                    ,column10
                    ,column11
                    ,column12
                    ,column13
                    ,column14
                    ,column15
                    ,column16
                    ,column17
                    ,column18
                    ,column19
                    ,column20
                    ,column21
                    ,column22
                    ,column23
                    ,column24
                    ,column25
                    ,column26
                    ,column27
                    ,column28
                    ,column29
                    ,column30
                    ,column31
                    ,column32
                    ,column33
                    ,column34
                    ,column35
                  from xxcus.xxcus_ce_stmt_int_tmp_boa_us
                );
                --
        elsif p_bank_name =g_bank_BOA_CA then
                --
                insert into xxcus.xxcus_ce_stmt_int_tmp2_boa_ca
                 (
                     bank_name
                    ,file_name
                    ,creation_date
                    ,file_imported_flag
                    ,request_id
                    ,status_code
                    ,rec_no
                    ,rec_id_no
                    ,column1
                    ,column2
                    ,column3
                    ,column4
                    ,column5
                    ,column6
                    ,column7
                    ,column8
                    ,column9
                    ,column10
                    ,column11
                    ,column12
                    ,column13
                    ,column14
                    ,column15
                    ,column16
                    ,column17
                    ,column18
                    ,column19
                    ,column20
                    ,column21
                    ,column22
                    ,column23
                    ,column24
                    ,column25
                    ,column26
                    ,column27
                    ,column28
                    ,column29
                    ,column30
                    ,column31
                    ,column32
                    ,column33
                    ,column34
                    ,column35
                 )
                (
                  select
                     p_bank_name
                    ,p_file_name
                    ,p_creation_date
                    ,p_file_imported_flag
                    ,p_request_id
                    ,p_status_code
                    ,rec_no
                    ,rec_id_no
                    ,column1
                    ,column2
                    ,column3
                    ,column4
                    ,column5
                    ,column6
                    ,column7
                    ,column8
                    ,column9
                    ,column10
                    ,column11
                    ,column12
                    ,column13
                    ,column14
                    ,column15
                    ,column16
                    ,column17
                    ,column18
                    ,column19
                    ,column20
                    ,column21
                    ,column22
                    ,column23
                    ,column24
                    ,column25
                    ,column26
                    ,column27
                    ,column28
                    ,column29
                    ,column30
                    ,column31
                    ,column32
                    ,column33
                    ,column34
                    ,column35
                  from xxcus.xxcus_ce_stmt_int_tmp_boa_ca
                );
                --
        else
                 --
                 Null;
                 --
        end if;
    --
        if p_bank_name =g_bank_WF then
            print_log('Inserted '||sql%rowcount||' records into xxcus.xxcus_ce_stmt_int_tmp2_wf for bank :'||p_bank_name);
        elsif p_bank_name =g_bank_BOA_US then
            print_log('Inserted '||sql%rowcount||' records into xxcus.xxcus_ce_stmt_int_tmp2_boa_us for bank :'||p_bank_name);
        elsif p_bank_name =g_bank_BOA_CA then
            print_log('Inserted '||sql%rowcount||' records into xxcus.xxcus_ce_stmt_int_tmp2_boa_ca for bank :'||p_bank_name);
                Null;
        else
                 Null;
        end if;
    --
   exception
    when others then
        if p_bank_name =g_bank_WF then
            print_log('Failed to insert into xxcus.xxcus_ce_stmt_int_tmp2_wf for bank :'||p_bank_name||', msg ='||sqlerrm);
        elsif p_bank_name =g_bank_BOA_US then
            print_log('Failed to insert into xxcus.xxcus_ce_stmt_int_tmp2_boa_us for bank :'||p_bank_name||', msg ='||sqlerrm);
        elsif p_bank_name =g_bank_BOA_CA then
            print_log('Failed to insert into xxcus.xxcus_ce_stmt_int_tmp2_boa_ca for bank :'||p_bank_name||', msg ='||sqlerrm);
                Null;
        else
                 Null;
        end if;
   end backup_stmt_int_tmp;
   --
  procedure setup_prereqs
   (
     retcode out varchar2
    ,errbuf  out varchar2
    ,p_max_files_per_day in number
    ,p_email in varchar2
    ,p_cc_email in varchar2
   ) is
  --
  l_inbound_loc varchar2(150) :=Null;
  l_result varchar2(2000) :=Null;
  --
  l_input_file_id   UTL_FILE.FILE_TYPE;
  l_line_read       VARCHAR2(2000):= NULL;
  l_line_count      NUMBER :=0;
  l_lines_processed NUMBER :=0;
  --
  l_bank xxcus.xxcus_ce_current_day_files_b.bank_name%type :=Null;
  l_file     xxcus.xxcus_ce_current_day_files_b.file_name%type :=Null;
  --
  l_BOA_CA_file   xxcus.xxcus_ce_current_day_files_b.file_name%type :=Null;
  l_BOA_US_file   xxcus.xxcus_ce_current_day_files_b.file_name%type :=Null;
  l_WF_file             xxcus.xxcus_ce_current_day_files_b.file_name%type :=Null;
  --
  l_BOA_CA_count Number :=0;
  l_BOA_US_count Number :=0;
  l_WF_count Number :=0;
  l_current_db_name varchar2(20) :=Null;
  l_prod_db_name varchar2(10) :='EBSPRD';
  --
  b_more_BOA_US_files_found boolean :=FALSE;
  b_more_BOA_CA_files_found boolean :=FALSE;
  b_more_WF_files_found boolean :=FALSE;
  --
  b_BOA_CA_file_OK boolean :=FALSE;
  b_BOA_US_file_OK boolean :=FALSE;
  b_WF_file_OK boolean :=FALSE;
  --
  cursor c_prereqs is
  select A1.* from xxcus.xxcus_ce_current_day_files_b A1
  where 1 =1
       and A1.status_code ='STAGE-1'
       and trunc(A1.creation_date) =trunc(sysdate)
       and A1.file_imported_flag ='N'
       and A1.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and A1.creation_date =
                 (
                    select max(A2.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b A2
                    where 1 =1
                         and  A2.file_name =A1.file_name
                         and  A2.status_code =A1.status_code
                         and  A2.file_imported_flag =A1.file_imported_flag
                         and  A2.request_id =A1.request_id
                 )
   ;
  --
begin
          --
       print_log(' Pass1');
          l_inbound_loc :=get_location ( v_operation =>'ABSOLUTE_PATH', v_type  =>'INBOUND');
          --
       print_log(' Pass2');
          begin
            --
            select upper(name) into l_current_db_name from v$database;
            --
             if  (p_email is not null and p_cc_email is not null) then
                  --
                   g_notif_email_to :=p_email;
                   g_notif_email_cc :=p_cc_email;
                  --
             else
              --
                 if l_current_db_name <>l_prod_db_name then -- for all non production instances
                      --
                       g_notif_email_to :=p_email;
                       g_notif_email_cc :=p_email;
                      --
                 else --for production instance
                      --
                       g_notif_email_to :=p_email;
                       g_notif_email_cc :=g_notif_email_cc;
                      --
                 end if;
              --
             end if;
             --
            --
          exception
           when others then
            print_log('Failed to fetch database instance name, message =>'||sqlerrm);
          end;
          --
       print_log(' Pass3');
         begin
          -- $$$$
                 --
                 for rec in c_prereqs loop
                  --
                             --
                             savepoint sqr1;
                             --
                           print_log('Before update of bank name.');
                           --
                           update xxcus.xxcus_ce_current_day_files_b
                               set bank_name =
                                      case
                                         when substr (file_name, 1, 15) =g_BOFA_US_prefix then g_bank_BOA_US
                                         when substr (file_name, 1, 19) =g_BOFA_CA_prefix then g_bank_BOA_CA
                                         else g_bank_WF
                                      end
                                     ,status_code ='STAGE-2'
                                     ,last_update_date =sysdate
                                     ,last_updated_by =g_user
                             where 1 =1
                                  and file_name =rec.file_name
                                  and status_code = 'STAGE-1'
                                  and trunc(creation_date) =trunc(sysdate)
                                  and file_imported_flag ='N'
                                  and request_id =rec.request_id
                              returning bank_name, file_name into l_bank, l_file--10/27/2016
                              ;
                             --
                             print_log (' ');
                             --
                             print_log('After update of bank name, total rows updated : '||sql%rowcount);
                             --
                             -- begin capture the rows updated for each bank
                             if (l_bank =g_bank_BOA_CA) then
                               l_BOA_CA_count :=sql%rowcount;
                               l_BOA_CA_file :=rec.file_name;
                             elsif (l_bank =g_bank_BOA_US) then
                               l_BOA_US_count :=sql%rowcount;
                               l_BOA_US_file :=rec.file_name;
                             elsif (l_bank =g_bank_WF) then
                               l_WF_count :=sql%rowcount;
                               l_WF_file :=rec.file_name;
                             else
                               Null;
                             end if;
                             -- end capture the rows updated for each bank
                             --
                             if (sql%rowcount >1) then
                                --
                                rollback to sqr1;
                                --
                               print_log('More than 1 bank file found.');
                               print_log ('');
                               print_log (' ***** Current list of files found in folder :');
                               print_log ('');
                                --
                                for c_files in (select * from xxcus.xxcus_ce_current_day_files_b where status_code ='STAGE-1' and file_name =rec.file_name and request_id =rec.request_id ) loop
                                           --
                                           print_log ('File name :'||c_files.file_name);
                                           --
                                end loop;
                                --
                                print_log ('');
                                --
                                begin
                                   update xxcus.xxcus_ce_current_day_files_b
                                       set status_code ='ERROR', comments =g_prereq_error
                                   where 1 =1
                                        and status_code = 'STAGE-1'
                                        and file_name =rec.file_name
                                        and request_id =rec.request_id
                                   ;
                                     commit;
                                exception
                                 when others then
                                  print_log ('Error in updating current day file status from STAGE-1 TO ERROR, message => '||sqlerrm);
                                  raise program_error;
                                end;
                                --
                                --raise program_error; --we will comment it for now as the below boolean variables will let us keep track of what went wrong with each bank
                                --
                                if (NOT b_more_WF_files_found) then
                                     --
                                     if (l_bank =g_bank_WF) then
                                       b_more_WF_files_found :=TRUE;
                                     else
                                        Null;
                                     end if;
                                     --
                                else
                                  Null;
                                end if;
                                --
                                if (NOT b_more_BOA_US_files_found) then
                                     --
                                     if (l_bank =g_bank_BOA_US) then
                                       b_more_BOA_US_files_found :=TRUE;
                                     else
                                        Null;
                                     end if;
                                     --
                                else
                                  Null;
                                end if;
                                --
                                if (NOT b_more_BOA_CA_files_found) then
                                     --
                                     if (l_bank =g_bank_BOA_CA) then
                                       b_more_BOA_CA_files_found :=TRUE;
                                     else
                                        Null;
                                     end if;
                                     --
                                else
                                  Null;
                                end if;
                                --
                             elsif (sql%rowcount =1) then --we were able to find out which files we are going to process
                                 --
                                 --
                                 if (l_bank =g_bank_BOA_CA) then
                                   b_BOA_CA_file_OK :=TRUE;
                                 elsif (l_bank =g_bank_BOA_US) then
                                   b_BOA_US_file_OK :=TRUE;
                                 elsif (l_bank =g_bank_WF) then
                                   b_WF_file_OK :=TRUE;
                                 else
                                   Null;
                                 end if;
                                 --
                                commit;
                                --
                                begin
                                                 g_command := 'chmod 777 '
                                                             ||l_inbound_loc
                                                             ||'/'
                                                             ||rec.file_name
                                                             ;
                                                 --
                                                 print_log('Chmod command:');
                                                 print_log('===============');
                                                 print_log(g_command);
                                                 --
                                                 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
                                                 into   l_result
                                                 from   dual;
                                                 print_log('v_result '||l_result);
                                exception
                                 when others then
                                    print_log ('Failed to change permission of file '||rec.file_name||' to 777, error = '||l_result);
                                    raise program_error;
                                end;
                                --
                             else --sql%rowcount =0
                              print_log ('@Setup Pre-requisites, Alert: No bank files found, @file '||rec.file_name);
                              raise program_error;
                             end if;
                             --
                 --
                 end loop;  --end for rec in c_prereqs loop
                 --
          -- $$$$
         exception
          when no_data_found then
             --
             -- Set email subject
             g_notif_email_subject :='HDS Treasury: Current Day Cash Positioning - Critical Alert ['||l_current_db_name||']';
             --
              g_text1 :='No file found for BOFA US/CA /WellsFargo bank';
             --
             -- Set email body and fire the email api
             begin
                    --
                    g_notif_email_body :=
                                                   '**** Current Day BAI Bank Files Processing - No File Found **** '
                                               ||g_carriage_return
                                               ||' '
                                               ||g_text1||'. Please check with the bank and manually send the remaining files over to HDS Oracle GL Support @ HDS.OracleGLSupport@HDSupply.com.'
                                               ||g_carriage_return
                                               ||' '
                                               ||g_carriage_return
                                               ||'This is an automated email. Please DO NOT reply to this message.'
                                               ||g_carriage_return
                                               ||' '
                                               ||g_carriage_return
                                               ||'Thanks.'
                                               ||g_carriage_return
                                               ||' '
                                               ||g_carriage_return
                                               ||'HDS GL Support'
                                              ;
                    --
                        send_email_notif
                           (
                                p_email_from      =>g_notif_email_from,
                                p_email_to           =>g_notif_email_to,
                                p_email_cc           =>g_notif_email_cc,
                                p_email_subject =>g_notif_email_subject,
                                p_email_body     =>g_notif_email_body,
                                p_mime_type      =>g_email_mime_type
                           );
                    --
             exception
              when others then
               print_log('Other errors : Unable to alert when no file found, message =>'||sqlerrm);
               raise program_error;
             end;
             --
           print_log('No file found to process, unable to setup pre-requisites, message =>'||sqlerrm);
           raise program_error;
          when others then
           print_log('Other errors, unable to setup pre-requisites, message =>'||sqlerrm);
           raise program_error;
         end;
       --
       -- fire an alert if we have found more than 1 file per bank
       --
       print_log(' Pass4');
        if (b_more_BOA_CA_files_found OR b_more_BOA_US_files_found OR b_more_WF_files_found) then
             --
             -- Set email subject
             g_notif_email_subject :='HDS Treasury: Current Day Cash Positioning - Critical Alert ['||l_current_db_name||']';
             --
             if (b_more_BOA_US_files_found and b_more_BOA_CA_files_found and b_more_WF_files_found) then
              g_text1 :='We have found more than one file for all banks';
             elsif (b_more_BOA_US_files_found and (NOT b_more_BOA_CA_files_found and NOT b_more_WF_files_found)) then
              g_text1 :='We have found more than one file for Bank of America US';
             elsif (b_more_BOA_CA_files_found and (NOT b_more_BOA_US_files_found and NOT b_more_WF_files_found)) then
              g_text1 :='We have found more than one file for Bank of America CA';
             elsif (b_more_WF_files_found and (NOT b_more_BOA_US_files_found and NOT b_more_BOA_CA_files_found)) then
              g_text1 :='We have found more than one file for WellsFargo';
             elsif (b_more_BOA_US_files_found and b_more_BOA_CA_files_found and (NOT b_more_WF_files_found)) then
              g_text1 :='We have found more than one file for Bank of America [US and CA]';
             elsif (b_more_BOA_US_files_found and b_more_WF_files_found  and (NOT b_more_BOA_CA_files_found)) then
              g_text1 :='We have found more than one file for Bank of America US and WellsFargo';
             elsif (b_more_BOA_CA_files_found and b_more_WF_files_found  and (NOT b_more_BOA_US_files_found)) then
              g_text1 :='We have found more than one file for Bank of America CA and WellsFargo';
             else
              g_text1 :=Null;
             end if;

             -- Set email body and fire the email api
             begin
                    --
                    g_notif_email_body :=
                                                   '**** Current Day BAI Bank Files Processing - More than requested files found. **** '
                                               ||g_carriage_return
                                               ||' '
                                               ||g_carriage_return
                                               ||g_text1||'. Please check with the bank and manually send the remaining files over to HDS Oracle GL Support @ HDS.OracleGLSupport@HDSupply.com.'
                                               ||' '
                                               ||g_carriage_return
                                               ||'WellsFargo File : '||l_WF_file||', total files :'||l_WF_count
                                               ||' '
                                               ||g_carriage_return
                                               ||'Bank of America US File : '||l_BOA_US_file||', total files :'||l_BOA_US_count
                                               ||' '
                                               ||g_carriage_return
                                               ||'Bank of America CA File : '||l_BOA_CA_file||', total files :'||l_BOA_CA_count
                                               ||' '
                                               ||g_carriage_return
                                               ||'This is an automated email. Please DO NOT reply to this message.'
                                               ||g_carriage_return
                                               ||' '
                                               ||g_carriage_return
                                               ||'Thanks.'
                                               ||g_carriage_return
                                               ||' '
                                               ||g_carriage_return
                                               ||'HDS GL Support'
                                              ;
                    --
                        send_email_notif
                           (
                                p_email_from      =>g_notif_email_from,
                                p_email_to           =>g_notif_email_to,
                                p_email_cc           =>g_notif_email_cc,
                                p_email_subject =>g_notif_email_subject,
                                p_email_body     =>g_notif_email_body,
                                p_mime_type      =>g_email_mime_type
                           );
                    --
             exception
              when others then
               Null;
             end;
             --
             raise program_error;
        end if;
       --
       -- fire an alert if we have found atleast 1 file for any of the three banks [ WellsFargo , BOFA US or BOFA CA ]
       --
       print_log(' Pass5');
        if (b_BOA_CA_file_OK OR b_BOA_US_file_OK OR b_WF_file_OK) then
             --
             -- Set email subject
             g_notif_email_subject :='HDS Treasury: Current Day Cash Positioning - Files Processed Alert ['||l_current_db_name||']';
             print_log(' Pass 5.1');
             --
             if (b_BOA_CA_file_OK AND b_BOA_US_file_OK AND b_WF_file_OK) then
              g_text1 :='We found one file for WellsFargo, Bank of America US and Canada. All set to go.';
             elsif (b_BOA_US_file_OK and (NOT b_BOA_CA_file_OK and NOT b_WF_file_OK)) then
              g_text1 :='We have found one file for Bank of America US only'||'. Please check with the other banks and manually send the remaining files over to HDS Oracle GL Support @ HDS.OracleGLSupport@HDSupply.com.';
             elsif (b_BOA_CA_file_OK and (NOT b_BOA_US_file_OK and NOT b_WF_file_OK)) then
              g_text1 :='We have found one file for Bank of America CA only'||'. Please check with the other banks and manually send the remaining files over to HDS Oracle GL Support @ HDS.OracleGLSupport@HDSupply.com.';
             elsif (b_WF_file_OK and (NOT b_BOA_CA_file_OK and NOT b_BOA_US_file_OK)) then
              g_text1 :='We have found one file for WellsFargo only'||'. Please check with the other banks and manually send the remaining files over to HDS Oracle GL Support @ HDS.OracleGLSupport@HDSupply.com.';
             elsif (b_BOA_US_file_OK and b_BOA_CA_file_OK and (NOT b_WF_file_OK)) then
              g_text1 :='We have found one file for Bank of America [US and CA] only'||'. Please check with the other bank and manually send the remaining files over to HDS Oracle GL Support @ HDS.OracleGLSupport@HDSupply.com.';
             elsif (b_BOA_US_file_OK and b_WF_file_OK  and (NOT b_BOA_CA_file_OK)) then
              g_text1 :='We have found one file for Bank of America US and WellsFargo only'||'. Please check with the other bank and manually send the remaining files over to HDS Oracle GL Support @ HDS.OracleGLSupport@HDSupply.com.';
             elsif (b_BOA_CA_file_OK and b_WF_file_OK  and (NOT b_BOA_US_file_OK)) then
              g_text1 :='We have found one file for Bank of America CA and WellsFargo only'||'. Please check with the other bank and manually send the remaining files over to HDS Oracle GL Support @ HDS.OracleGLSupport@HDSupply.com.';
             else
              g_text1 :=Null;
             end if;
             --
             print_log(' Pass 5.2');
             -- Set email body and fire the email api
             begin
                    --
                    g_notif_email_body :=
                                                   '**** Current Day BAI Bank Files Processed  **** '
                                               ||g_carriage_return
                                               ||' '
                                               ||g_carriage_return
                                               ||g_text1
                                               ||g_carriage_return
                                               ||' '
                                               ||g_carriage_return
                                               ||'   1) WellsFargo File : '||l_WF_file||', Total files :'||l_WF_count
                                               ||g_carriage_return
                                               ||' '
                                               ||g_carriage_return
                                               ||'   2) Bank of America US File : '||l_BOA_US_file||', Total files :'||l_BOA_US_count
                                               ||g_carriage_return
                                               ||' '
                                               ||g_carriage_return
                                               ||'   3) Bank of America CA File : '||l_BOA_CA_file||', Total files :'||l_BOA_CA_count
                                               ||g_carriage_return
                                               ||' '
                                               ||g_carriage_return
                                               ||'This is an automated email. Please DO NOT reply to this message.'
                                               ||g_carriage_return
                                               ||' '
                                               ||g_carriage_return
                                               ||'Thanks.'
                                               ||g_carriage_return
                                               ||' '
                                               ||g_carriage_return
                                               ||'HDS GL Support'
                                              ;
                    --
             print_log(' Pass 5.3');
                    --
                        send_email_notif
                           (
                                p_email_from      =>g_notif_email_from,
                                p_email_to           =>g_notif_email_to,
                                p_email_cc           =>g_notif_email_cc,
                                p_email_subject =>g_notif_email_subject,
                                p_email_body     =>g_notif_email_body,
                                p_mime_type      =>g_email_mime_type
                           );
             print_log(' Pass 5.4');
                    --
             exception
              when others then
               Null;
                print_log(' Pass 5.5');
                 print_log('@ Pass 5.5, Failed to update bank name, errror message :'||sqlerrm);
             end;
             --
             print_log(' Pass 5.6');
        end if;
         --
       print_log(' Pass6');
   exception
   when others then
       print_log(' Pass7');
    print_log('Failed to update bank name, errror message :'||sqlerrm);
    raise program_error;
   end setup_prereqs;
   --
 procedure backup_bankfile
   (
     retcode out varchar2
    ,errbuf  out varchar2
--    ,p_bank_name in varchar2
   ) is
  -- =================================================================================
  -- local variables
  -- =================================================================================
  l_input_file_id   UTL_FILE.FILE_TYPE;
  l_line_read       VARCHAR2(2000):= NULL;
  l_line_count      NUMBER :=0;
  l_lines_processed NUMBER :=0;
  l_org_id NUMBER :=163;
  --
  l_print_count     NUMBER :=0;
  b_move_fwd      BOOLEAN;
  v_unprocessed_flag VARCHAR2(1) :='Z';
  --
  o_return_status           VARCHAR2 (2000);
  o_msg_count                 NUMBER;
  o_msg_data                   VARCHAR2 (2000);
  --
  v_api_message             VARCHAR2 (4000);
  n_count                           NUMBER :=0;
  n_seq                               NUMBER :=0;
  --
  l_inbound_loc VARCHAR2 (150) :=Null;
  l_outbound_loc  VARCHAR2 (150) :=Null;
  --
  cursor c_bank_files is
  select A1.* from xxcus.xxcus_ce_current_day_files_b A1
  where 1 =1
       and A1.status_code ='STAGE-2'
       and trunc(A1.creation_date) =trunc(sysdate)
       and A1.file_imported_flag ='N'
       and A1.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and A1.creation_date =
                 (
                    select max(A2.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b A2
                    where 1 =1
                         and  A2.bank_name =A1.bank_name
                         and  A2.file_name =A1.file_name
                         and  A2.status_code =A1.status_code
                         and  A2.file_imported_flag =A1.file_imported_flag
                 )
   ;
  --
  --
begin
    --
    --
    l_inbound_loc :=get_location ( v_operation =>'DBA_DIRECTORY', v_type  =>'INBOUND');
    --
    l_outbound_loc :=get_location ( v_operation =>'DBA_DIRECTORY', v_type  =>'OUTBOUND');
    --
    v_api_message :='truncate table xxcus.xxcus_ce_current_day_records_b';
    --
    execute immediate v_api_message;
    --
    v_api_message :=Null;
    --
    begin
     --
       for rec in c_bank_files loop
           -- @@@@
           --
           l_line_count :=0;
           --
           begin
            -- ##### begin get file handle and read thru the file
                    print_log ('@ File: '||rec.file_name||', before get file handle.');
                    l_input_file_id  := utl_file.fopen(l_inbound_loc, rec.file_name, 'R');
                    print_log ('@ File: '||rec.file_name||', after get file handle.');
                   --
                  loop
                       begin
                          --
                          utl_file.get_line(l_input_file_id, l_line_read);
                          --
                          l_line_count:= l_line_count+1; --number of lines read
                          --
                            --
                            -- Insert the file content as such into the custom table for current day transactions.
                            --
                             begin
                              --
                               Insert Into xxcus.xxcus_ce_current_day_records_b
                                (
                                 file_name
                                ,bank_name
                                ,status_code
                                ,line_number
                                ,file_record
                                ,created_by
                                ,creation_date
                               )
                              Values
                               (
                                 rec.file_name
                                ,rec.bank_name
                                ,'STAGE-3'
                                ,l_line_count
                                ,l_line_read
                                ,g_user
                                ,sysdate
                               );
                              --
                              --
                             exception
                              when others then
                               print_log(' Error in insert of file content into table xxcus.xxcus_ce_current_day_records_b, error =>'||sqlerrm);
                               rollback;
                               raise program_error;
                             end;
                          --
                       exception
                             when no_data_found then
                               exit;
                             when others then
                               print_log('Line# '||l_line_count||', Record: '||l_line_read);
                               print_log('@ Inside file loop operation, error : '||sqlerrm);
                               exit;
                       end;
                  end loop;

                    utl_file.fclose(l_input_file_id);
                    print_log ('After file close.');
                  -- @@@
            -- ##### end get file handle and close the file handle
           exception
                 when utl_file.invalid_path then
                  raise_application_error(-20010,'File: '||rec.file_name||' Invalid Path: '||sqlerrm);
                 when utl_file.invalid_mode then
                  raise_application_error(-20010,'File: '||rec.file_name||' Invalid Mode: '||sqlerrm);
                 when utl_file.invalid_operation then
                  raise_application_error(-20010,'File: '||rec.file_name||' Invalid Operation: '||sqlerrm);
                 when utl_file.invalid_filehandle then
                  raise_application_error(-20010,'File: '||rec.file_name||' Invalid File Handle: '||sqlerrm);
                 when utl_file.read_error then
                  raise_application_error(-20010,'File: '||rec.file_name||' File Read Error: '||sqlerrm);
                 when utl_file.internal_error then
                  raise_application_error(-20010,'File: '||rec.file_name||' File Internal Error: '||sqlerrm);
                 when others then
                   print_log ('Error in caller apps.xxcus_currentday_cash_pkg.backup_bankfile, file : =>'||rec.file_name);
                   print_log ('Error in caller apps.xxcus_currentday_cash_pkg.backup_bankfile, error =>'||sqlerrm);
           end;
          --
          begin
           update xxcus.xxcus_ce_current_day_files_b
               set status_code ='STAGE-3'
                    ,comments =g_bank_file_copied
                   ,last_update_date =sysdate
                   ,last_updated_by =g_user
             where 1 =1
                   and bank_name =rec.bank_name
                   and file_name =rec.file_name
                   and status_code = 'STAGE-2'
                   and trunc(creation_date) =trunc(sysdate)
                   and file_imported_flag ='N'
             ;
          exception
             when others then
                print_log(' Error in update of status code From STAGE-2 to STAGE-3 for ban :'||rec.bank_name||', file :'||rec.file_name||', error =>'||sqlerrm);
                rollback;
                raise program_error;
          end;
         --
       end loop;
     --
    exception
       when no_data_found then
        print_log('@ no data found, no bank files.');
        raise program_error;
       when others then
        print_log('@ other errors, no bank files, error :'||sqlerrm);
        raise program_error;
    end;
    --
exception
   when others then
    print_log('outer block of process_bankfile, errror message :'||sqlerrm);
    raise program_error;
end backup_bankfile;
   --
   -- Begin Ver 1.3
   --
procedure rewrite_wf_file
   (
     retcode out varchar2
    ,errbuf  out varchar2
   ) is
  -- =================================================================================
  -- local variables
  -- =================================================================================
  l_input_file_id   UTL_FILE.FILE_TYPE;
  l_line_read       VARCHAR2(2000):= NULL;
  l_line_count      NUMBER :=0;
  --
  l_file_name varchar2(150) :=Null;
  l_lines_processed NUMBER :=0;
  l_org_id NUMBER :=163;
  --
  l_print_count     NUMBER :=0;
  b_move_fwd      BOOLEAN;
  v_unprocessed_flag VARCHAR2(1) :='Z';
  --
  o_return_status           VARCHAR2 (2000);
  o_msg_count                 NUMBER;
  o_msg_data                   VARCHAR2 (2000);
  --
  v_api_message             VARCHAR2 (4000);
  n_count                           NUMBER :=0;
  n_seq                               NUMBER :=0;
  --
  l_inbound_loc VARCHAR2 (150) :=Null;
  l_outbound_loc  VARCHAR2 (150) :=Null;
  --  
  type cur_type is REF CURSOR;
  c_wf cur_type;
  l_command VARCHAR2(400) :=Null;
  l_result VARCHAR2(4000) :=Null;
  --
  l_exists    BOOLEAN;
  l_length    NUMBER;
  l_first BOOLEAN;
  l_enter varchar2(2) :='
';
  --
  l_incl_begin_line number :=0;
  l_incl_end_line number :=0;
  l_excl_begin_line number :=0;
  l_excl_end_line number :=0;  
  --
  l_blocksize NUMBER :=0;  
  l_line NUMBER :=0;
  l_wf_wire_file_total varchar2(30) :='wf_payroll_wire_rec_total.txt';
  l_wf_wire_file_log    varchar2(30) :='wf_payroll_wire_log.txt';
  --
  l_sql_add_filter varchar2(32000) :=Null;
  l_sql varchar2(32000) :=
  'select A1.file_record 
  from  xxcus.xxcus_ce_current_day_records_b A1
  where 1 =1
       and A1.bank_name ='||'''WELLSFARGO'''||'
       and A1.status_code =''STAGE-3''
       and trunc(A1.creation_date) =trunc(sysdate)
   '
   ;
  --
  --
begin
    --
    print_log('102');
    -- 
    l_file_name :=get_wellsfargo_file;
    --
    if l_file_name is null then 
     raise program_error; 
    end if;
    --  
    l_inbound_loc :=get_location ( v_operation =>'DBA_DIRECTORY', v_type  =>'INBOUND_ARCHIVE');
   --
   begin 
        --
        UTL_FILE.FGETATTR ( l_inbound_loc, l_wf_wire_file_total, l_exists, l_length, l_blocksize);
        --
        IF (l_exists and l_length >0) THEN 
           --
           print_log('101');
           --
           print_log('Payroll wire is found, we need to rewrite the file for WellsFargo...');
           --
           l_input_file_id  := utl_file.fopen(l_inbound_loc, l_wf_wire_file_total, 'R');
           --
           utl_file.get_line(l_input_file_id, l_line_read);
           --
           utl_file.fclose(l_input_file_id);
           --
           l_blocksize :=nvl(to_number(l_line_read) , 0);
           --
           print_log('Total WellsFargo payroll wire records :'||l_blocksize);
           --           
        ELSE
           print_log('Payroll wire does not exist in the WellsFargo file, no need to rewrite the file.'); 
        END IF;    
        --
       print_log('Begin: Payroll wire, get line numbers and process rewrite...');
       --        
        if l_blocksize >0 then
         --
         l_first :=TRUE;
         -- 08/30/2018
               --
               begin
                -- ##### begin get file handle and read thru the file
                        print_log ('@ File :'||l_wf_wire_file_log||', before get file handle.');
                        l_input_file_id  := utl_file.fopen(l_inbound_loc, l_wf_wire_file_log, 'R');
                        print_log (' @ File :'||l_wf_wire_file_log||', after get file handle.');
                       --
                      loop
                          --
                           if l_sql_add_filter is null then
                             l_sql_add_filter :='    and line_number not in ( ';
                          end if;
                          --
                           begin
                              --
                              utl_file.get_line(l_input_file_id, l_line_read);
                              --
                              print_log ('l_line_read =>'||l_line_read);
                              --
                              begin
                                  l_excl_begin_line :=to_number(l_line_read);
                                  print_log('Converted wire ref '||l_line_read||' to its number value');
                              exception
                                when others then
                                  print_log('@ convert wire ref char to number, error =>'||sqlerrm);
                              end;
                              --
                              for idx in 1 .. 10  loop  
                                --
                                l_line :=l_line + 1;
                                --                                
                                if idx =1 then
                                 Null;
                                else                         
                                   l_excl_begin_line := l_excl_begin_line + 1;   
                                   l_first :=FALSE;                             
                                end if;
                                --
                                --print_log('l_excl_begin_line =>'||l_excl_begin_line);
                                --
                                l_sql_add_filter :=  l_sql_add_filter
                                                                  ||case 
                                                                             when (l_line =1 and (l_first) ) then to_char(l_excl_begin_line)
                                                                             else ', '|| to_char(l_excl_begin_line)
                                                                       end                                                                            
                                                                  ;
                               --print_log('l_sql_add_filter =>'||l_enter||l_sql_add_filter);
                              end loop;
                              --
                           exception
                                 when no_data_found then
                                   exit;
                                 when others then
                                   print_log('@rewrite_wf_file, read log file, Inside the file loop operation, error : '||sqlerrm);
                                   exit;
                           end;
                          --
                      end loop;

                        utl_file.fclose(l_input_file_id);
                        print_log ('After file2 close.');
                      -- @@@
                -- ##### end get file handle and close the file handle
               exception
                     when utl_file.invalid_path then
                      raise_application_error(-20010,'File: '||l_wf_wire_file_log||' Invalid Path: '||sqlerrm);
                     when utl_file.invalid_mode then
                      raise_application_error(-20010,'File: '||l_wf_wire_file_log||' Invalid Mode: '||sqlerrm);
                     when utl_file.invalid_operation then
                      raise_application_error(-20010,'File: '||l_wf_wire_file_log||' Invalid Operation: '||sqlerrm);
                     when utl_file.invalid_filehandle then
                      raise_application_error(-20010,'File: '||l_wf_wire_file_log||' Invalid File Handle: '||sqlerrm);
                     when utl_file.read_error then
                      raise_application_error(-20010,'File: '||l_wf_wire_file_log||' File Read Error: '||sqlerrm);
                     when utl_file.internal_error then
                      raise_application_error(-20010,'File: '||l_wf_wire_file_log||' File Internal Error: '||sqlerrm);
                     when others then
                       print_log ('Error in caller apps.xxcus_currentday_cash_pkg.backup_bankfile, file : =>'||l_wf_wire_file_log);
                       print_log ('Error in caller apps.xxcus_currentday_cash_pkg.backup_bankfile, error =>'||sqlerrm);
               end;
              --              
         -- 08/30/2018
               print_log('End: Payroll wire, get line numbers and process rewrite...');
               --          
               l_sql := l_sql || l_sql_add_filter||')'||l_enter||' order by A1.line_number asc';
               --
               print_log ('l_sql =>'||l_enter||' '||l_sql);
               -- 
               l_line_read :=Null;
               --
               l_inbound_loc :=get_location ( v_operation =>'DBA_DIRECTORY', v_type  =>'INBOUND');       
               --
               l_input_file_id  := utl_file.fopen(l_inbound_loc, l_file_name, 'w');
               --
               begin
                   open c_wf for l_sql;
                   loop
                    fetch c_wf into l_line_read;
                     utl_file.put_line(l_input_file_id, l_line_read);
                     exit when c_wf%notfound;
                   end loop;
                   close c_wf;       
               exception
                when no_data_found then
                 print_log('No data from cursor c_wf, error =>'||sqlerrm);
                when others then
                 print_log(' Error in file rewrite, msg ='||sqlerrm);
               end;
               --
               utl_file.fclose(l_input_file_id);
               --   
                   begin 
                    --
                     l_inbound_loc :=get_location ( v_operation =>'ABSOLUTE_PATH', v_type  =>'INBOUND');       
                     --            
                     l_command :='chmod 777 '
                               ||' '
                               ||l_inbound_loc
                               ||'/'
                               ||l_file_name --temp zip file
                               ; --temp zip file
                      --
                      print_log('Check rewritten WellsFargo file properties, OS command =>'||l_command);
                      --
                      select xxwc_edi_iface_pkg.xxwc_oscommand_run (l_command)
                      into   l_result
                      from   dual;
                      --
                   exception
                      when others then
                        print_log('Failed to check rewritten WellsFargo file properties, error : '||l_result||', ORAERR : '||sqlerrm);
                   end;           
               --         
        end if; -- l_blocksize >0 then
        --
   exception
    when others then
     print_log('@l_wf_wire_file_total ='||l_wf_wire_file_total||', error =>'||sqlerrm);
   end;
   --
exception
   when others then
    print_log('outer block of process_bankfile, errror message :'||sqlerrm);
    raise program_error;
end rewrite_wf_file;
   --   
   -- End Ver 1.3
   --
  PROCEDURE kickoff_sql_loader
  (
     retcode out varchar2
    ,errbuf  out varchar2
  ) IS
   --
   cursor c_bank_files is
    select A1.*
    from  xxcus.xxcus_ce_current_day_files_b A1
    where 1 =1
         and A1.status_code ='STAGE-3'
         and A1.file_imported_flag ='N'
         and A1.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date)) --10/25/2016
         and A1.creation_date =
                 (
                    select max(A2.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b A2
                    where 1 =1
                         and  A2.bank_name =A1.bank_name
                         and  A2.file_name =A1.file_name
                         and  A2.status_code =A1.status_code
                         and  A2.file_imported_flag =A1.file_imported_flag
                 )
          ;
   --
   TYPE l_bank_type IS table of c_bank_files%rowtype index by binary_integer;
   l_bank_rec l_bank_type;
   --
   ln_request_id       NUMBER :=0;
   n_req_id                NUMBER :=0;
   n_user_id              NUMBER;
  --
  l_inbound_loc VARCHAR2 (150) :=Null;
  l_outbound_loc  VARCHAR2 (150) :=Null;
   lc_request_data  VARCHAR2(20) :='';
   l_org_id                 NUMBER :=0;
   --
   -- =========================================================================================
    l_module        VARCHAR2(24) :='GSC -Treasury ';
    l_err_msg       CLOB;
    l_sec           VARCHAR2(255);
    v_location VARCHAR2(255);
    --
    l_err_callfrom  VARCHAR2(75) DEFAULT 'apps.xxcus_currentday_cash_pkg.kickoff_sql_loader';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   --
  begin
  --
   n_req_id        :=fnd_global.conc_request_id;
   n_user_id       :=fnd_global.user_id;
   l_org_id        :=mo_global.get_current_org_id;
   lc_request_data :=fnd_conc_global.request_data;
  --
   print_log('lc_request_data ='||lc_request_data);
  --
   If lc_request_data is null then
        --
         lc_request_data :='1';
        --
        l_inbound_loc :=get_location ( v_operation =>'ABSOLUTE_PATH', v_type  =>'INBOUND');
        --
        l_outbound_loc :=get_location ( v_operation =>'ABSOLUTE_PATH', v_type  =>'OUTBOUND');
        --
        open c_bank_files;
        fetch c_bank_files bulk collect into l_bank_rec;
        close c_bank_files;
        --
         if l_bank_rec.count >0 then
          --
               for idx in l_bank_rec.first .. l_bank_rec.last loop
                    --
                    g_data_file :=l_inbound_loc||'/'||l_bank_rec(idx).file_name;
                    --
                    g_file_name :=l_bank_rec(idx).file_name;
                    g_creation_date :=l_bank_rec(idx).creation_date;
                    --
                    if l_bank_rec(idx).bank_name =g_bank_WF then
                            g_program :=g_WF_bai2_sql_ldr_job;
                    elsif l_bank_rec(idx).bank_name =g_bank_BOA_US then
                            g_program :=g_BOA_US_bai2_sql_ldr_job;
                    elsif l_bank_rec(idx).bank_name =g_bank_BOA_CA then
                            g_program :=g_BOA_CA_bai2_sql_ldr_job;
                    else
                            g_program :=Null;
                    end if;
                    --
                  ln_request_id :=fnd_request.submit_request
                      (
                        application      =>'XXCUS'
                       ,program          =>g_program --'XXCUS_CESLRPROBAI2' --Run SQL*Loader- BAI2
                       ,description      =>''
                       ,start_time       =>''
                       ,sub_request      =>TRUE
                       ,argument1        =>g_data_file
                      );
                          --
                          if ln_request_id >0 then
                            --
                            fnd_file.put_line(fnd_file.log, 'Submitted custom BAI2 sql loader import file for bank ='
                                                             ||l_bank_rec(idx).bank_name
                                                             ||', file ='
                                                             ||g_data_file
                                                             ||', Request Id ='
                                                             ||ln_request_id
                                                             ||', creation_date '
                                                             ||to_char(l_bank_rec(idx).creation_date, 'DD-MON-YYYY HH24:MI:SS')
                                                            );
                                          --
                                          begin
                                           update xxcus.xxcus_ce_current_day_files_b
                                               set status_code ='STAGE-4'
                                                     ,comments =g_bank_file_imported
                                                     ,file_imported_flag ='Y'
                                                     ,last_update_date =sysdate
                                                     ,last_updated_by =g_user
                                             where 1 =1
                                                   and bank_name =l_bank_rec(idx).bank_name
                                                   and file_name =l_bank_rec(idx).file_name
                                                   and status_code = 'STAGE-3'
                                                   and trunc(creation_date) =trunc(sysdate)
                                                   and file_imported_flag ='N'
                                             ;
                                             --
                                             g_imported_flag :='Y';
                                             l_bank_rec(idx).file_imported_flag :='Y';
                                             --
                                          exception
                                             when others then
                                                print_log(' Error in update of status code From STAGE-2 to STAGE-3 for bank :'||l_bank_rec(idx).bank_name||', file :'||l_bank_rec(idx).file_name||', error =>'||sqlerrm);
                                                rollback;
                                                raise program_error;
                                          end;
                                         --
                                          begin
                                           update xxcus.xxcus_ce_current_day_records_b
                                               set status_code ='STAGE-4'
                                                     ,last_update_date =sysdate
                                                     ,last_updated_by =g_user
                                             where 1 =1
                                                   and bank_name =l_bank_rec(idx).bank_name
                                                   and file_name =l_bank_rec(idx).file_name
                                                   and status_code = 'STAGE-3'
                                                   and trunc(creation_date) =trunc(sysdate)
                                             ;
                                             --
                                          exception
                                             when others then
                                                print_log('@xxcus.xxcus_ce_current_day_records_b, Issue in update of status code From STAGE-3 to STAGE-4 for bank :'||l_bank_rec(idx).bank_name||', file :'||l_bank_rec(idx).file_name||', error =>'||sqlerrm);
                                                rollback;
                                                raise program_error;
                                          end;
                                         --
                          else
                            fnd_file.put_line(fnd_file.log, 'Failed to submit custom BAI2 sql loader import file for bank'
                                                             ||l_bank_rec(idx).bank_name
                                                             ||', file ='
                                                             ||g_data_file
                                                             ||', creation_date '
                                                             ||to_char(l_bank_rec(idx).creation_date, 'DD-MON-YYYY HH24:MI:SS')
                                                            );
                            raise program_error;
                          end if;
                            --
                            l_bank_rec(idx).request_id :=ln_request_id;
                            --
                --
                 fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>lc_request_data);
                --
                 lc_request_data :=to_char(to_number(lc_request_data)+1);
                --
               end loop; --idx in l_bank_rec.first .. l_bank_rec.last
               --
         else
          print_log('No bank file found');
          raise program_error;
         end if;
         --
   Else
       --
      retcode :=0;
      errbuf :='Child request completed. Exit HDS Treasury: Kickoff SQL Loader Wrapper';
      fnd_file.put_line(fnd_file.log,errbuf);
      --
   End If;
    --
  exception
  --
   when others then
    print_log('Outer block, other errors in calling kickoff_sql_loader, message ='||sqlerrm);
        l_err_msg := l_err_msg || ' ...Error_Stack...' ||
                     dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                     dbms_utility.format_error_backtrace();
        fnd_file.put_line(fnd_file.log, l_err_msg);
        l_err_callpoint := l_sec;
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => l_err_callpoint
                                            ,p_request_id        => fnd_global.conc_request_id
                                            ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                           dbms_utility.format_error_stack() ||
                                                                           ' Error_Backtrace...' ||
                                                                           dbms_utility.format_error_backtrace()
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => substr(l_err_msg
                                                                          ,1
                                                                          ,240)
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => l_module);
    rollback;
  --
  end kickoff_sql_loader;
 --
 procedure WELLSFARGO
   (
     retcode out varchar2
    ,errbuf  out varchar2
    ,p_bank_name in varchar2
   ) is
  -- =================================================================================
  -- local variables
  -- =================================================================================
  l_input_file_id   UTL_FILE.FILE_TYPE;
  l_line_read       VARCHAR2(2000):= NULL;
  l_line_count      NUMBER :=0;
  l_lines_processed NUMBER :=0;
  l_org_id NUMBER :=163;
  --
  l_print_count     NUMBER :=0;
  b_move_fwd      BOOLEAN;
  v_unprocessed_flag VARCHAR2(1) :='Z';
  --
  o_return_status           VARCHAR2 (2000);
  o_msg_count                 NUMBER;
  o_msg_data                   VARCHAR2 (2000);
  --
  v_api_message             VARCHAR2 (4000);
  n_count                           NUMBER :=0;
  n_seq                               NUMBER :=0;
  --
 l_field_delimit_count number :=0;
 l_last_field_delimiter_posn number :=0;
 n_loc varchar2(3) :=Null;
  --
  l_inbound_loc VARCHAR2 (150) :=Null;
  l_outbound_loc  VARCHAR2 (150) :=Null;
  --
  type t_max_lines_type is record
   (
    max_lines number
   );
  type t_max_lines_tbl is table of t_max_lines_type index by binary_integer;
  l_line_loop t_max_lines_tbl;
  --
  type t_num_type is record
   (
    current_line number,
    next_03_line_num number
   );
  type t_num_tbl is table of t_num_type index by binary_integer;
  t_num_rec t_num_tbl;
  --
   cursor c_bank_new_files is
    select A1.*
    from  xxcus.xxcus_ce_current_day_files_b A1
    where 1 =1
         and A1.bank_name =p_bank_name
         and A1.status_code ='STAGE-4'
         and A1.file_imported_flag ='Y'
         and trunc(A1.creation_date) =trunc(sysdate)
         and A1.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
         and A1.creation_date =
                 (
                    select max(A2.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b A2
                    where 1 =1
                         and  A2.bank_name =A1.bank_name
                         and  A2.file_name =A1.file_name
                         and  A2.status_code =A1.status_code
                         and  A2.file_imported_flag =A1.file_imported_flag
                         and  A2.request_id =A1.request_id
                 )
          ;
   --
   TYPE l_bank_new_type IS table of c_bank_new_files%rowtype index by binary_integer;
   l_bank_new_rec l_bank_new_type;
  --
  cursor get_next_03_line is
  select to_number(xmltbl.column_value) current_line
             ,nvl(lead(to_number(xmltbl.column_value), 1) over(order by to_number(xmltbl.column_value)),0) next_03_line_num
  from xmltable(g_all_03_lines) xmltbl;
  --
  cursor c_bank_rec_id ( l_rec_id_no IN VARCHAR2 ) is
  select
              b.file_name        file_name
             ,b.status_code   status_code
             ,c.rec_id_no        rec_id_no
             ,c.rec_no              rec_no
             ,b.file_record      file_record
             ,'N'                         extract_flag
             ,0                           lines_to_scan
  from xxcus.xxcus_ce_current_day_files_b a
           ,xxcus.xxcus_ce_current_day_records_b b
           ,xxcus.xxcus_ce_stmt_int_tmp2_WF c
  where 1 =1
       and b.bank_name =g_bank_name
       and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and c.rec_id_no =l_rec_id_no
       and trim(b.file_record) is not null
       and a.bank_name =b.bank_name
       and c.bank_name =a.bank_name
       and a.file_name =b.file_name
       and c.file_name =a.file_name
       and b.status_code ='STAGE-4'
       and a.status_code =b.status_code
       and c.status_code =a.status_code
       and trunc(b.creation_date) =trunc(sysdate)
       and trunc(a.creation_date) =trunc(b.creation_date)
       and trunc(c.creation_date) =trunc(c.creation_date)
       and a.file_imported_flag ='Y'
       and c.file_imported_flag =a.file_imported_flag
       and b.line_number =c.rec_no
       and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                         and  d.request_id =a.request_id
                 )
order by b.line_number asc;
  --
  type l_bank_rec_type is table of c_bank_rec_id%rowtype index by binary_integer;
  l_bank_single_rec l_bank_rec_type;
  --
  cursor c_bank_files is
  select b.file_name        file_name
             ,b.status_code   status_code
             ,b.line_number  line_number
             ,c.rec_id_no        rec_id_no
             ,c.rec_no              rec_no
             ,b.file_record      file_record
             ,'N'                         extract_flag
  from xxcus.xxcus_ce_current_day_files_b a
           ,xxcus.xxcus_ce_current_day_records_b b
           ,xxcus.xxcus_ce_stmt_int_tmp2_WF c
  where 1 =1
       and b.bank_name =g_bank_name
       and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and trim(b.file_record) is not null
       and a.bank_name =b.bank_name
       and c.bank_name =a.bank_name
       and a.file_name =b.file_name
       and c.file_name =a.file_name
       and b.status_code ='STAGE-4'
       and a.status_code =b.status_code
       and c.status_code =a.status_code
       and trunc(b.creation_date) =trunc(sysdate)
       and trunc(a.creation_date) =trunc(b.creation_date)
       and trunc(c.creation_date) =trunc(c.creation_date)
       and a.file_imported_flag ='Y'
       and c.file_imported_flag =a.file_imported_flag
       and b.line_number =c.rec_no
       and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                         and  d.request_id =a.request_id
                 )
  order by b.line_number asc
   ;
  --
  type l_bank_tbl is table of c_bank_files%rowtype index by binary_integer;
  l_bank_all_rec l_bank_tbl;
  --
  -- begin declaration of cursor c_extract_03_info
  cursor c_extract_03_info (l_rec_no in number) is
    select
         e.bank_name bank_name
        ,e.bank_branch_name bank_branch_name
        ,d.bank_account_name
        ,c.column1 bank_account_num
        ,nvl(c.column2, 'USD') currency_code
        ,c.column3 item_code
        ,case
           when trim(replace(c.column4, '/', '')) is not null then (to_number(replace(c.column4, '/', ''))/100)
           else to_number(null)
         end  item_amount
        ,case
           when trim(replace(c.column5, '/', '')) is not null then (to_number(replace(c.column5, '/', '')))
           else to_number(null)
         end  item_count
        ,trim(replace(c.column6, '/', '')) funds_type
        ,c.rec_id_no rec_id_no
        ,c.rec_no  rec_no
        ,c.file_name current_day_file_name
        ,g_sender_id sender_id
        ,g_receiver_id receiver_id
        ,to_date((g_file_creation_date||g_file_creation_time),'YYMMDDHH24MI') bank_file_create_date
        ,g_user created_by
        ,sysdate creation_date
        ,g_user last_updated_by
        ,sysdate last_update_date
        ,g_org_id org_id
        ,g_bank_name job_parameter
    from xxcus.xxcus_ce_current_day_files_b a
           ,xxcus.xxcus_ce_current_day_records_b b
           ,xxcus.xxcus_ce_stmt_int_tmp2_WF c
           ,apps.ce_bank_accounts  d
           ,apps.ce_bank_branches_v e
    where 1 =1
       and b.bank_name =g_bank_name
       and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and c.rec_id_no =g_rec_id_03
       and c.rec_no =l_rec_no
       and trim(b.file_record) is not null
       and trim(replace(c.column3, '/', '')) is not null
       and a.bank_name =b.bank_name
       and c.bank_name =a.bank_name
       and a.file_name =b.file_name
       and c.file_name =a.file_name
       and b.status_code ='STAGE-4'
       and a.status_code =b.status_code
       and c.status_code =a.status_code
       and trunc(b.creation_date) =trunc(sysdate)
       and trunc(a.creation_date) =trunc(b.creation_date)
       and trunc(c.creation_date) =trunc(c.creation_date)
       and a.file_imported_flag ='Y'
       and c.file_imported_flag =a.file_imported_flag
       and b.line_number =c.rec_no
       and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                         and  d.request_id =a.request_id
                 )
     and d.attribute_category=g_cash_posn_attr_cat
     and d.attribute2 =g_cash_posn_attribute2
     and d.bank_account_num =c.column1
     and e.bank_party_id =d.bank_id
     and e.branch_party_id =d.bank_branch_id
union all
    select
         e.bank_name bank_name
        ,e.bank_branch_name bank_branch_name
        ,d.bank_account_name
        ,c.column1 bank_account_num
        ,nvl(c.column2, 'USD') currency_code
        ,c.column7 item_code
        ,case
           when trim(replace(c.column8, '/', '')) is not null then (to_number(replace(c.column8, '/', ''))/100)
           else to_number(null)
         end  item_amount
        ,case
           when trim(replace(c.column9, '/', '')) is not null then (to_number(replace(c.column9, '/', '')))
           else to_number(null)
         end  item_count
        ,trim(replace(c.column10, '/', '')) funds_type
        ,c.rec_id_no rec_id_no
        ,c.rec_no  rec_no
        ,c.file_name current_day_file_name
        ,g_sender_id sender_id
        ,g_receiver_id receiver_id
        ,to_date((g_file_creation_date||g_file_creation_time),'YYMMDDHH24MI') bank_file_create_date
        ,g_user created_by
        ,sysdate creation_date
        ,g_user last_updated_by
        ,sysdate last_update_date
        ,g_org_id org_id
        ,g_bank_name job_parameter
    from xxcus.xxcus_ce_current_day_files_b a
           ,xxcus.xxcus_ce_current_day_records_b b
           ,xxcus.xxcus_ce_stmt_int_tmp2_WF c
           ,apps.ce_bank_accounts  d
           ,apps.ce_bank_branches_v e
    where 1 =1
       and b.bank_name =g_bank_name
       and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and c.rec_id_no =g_rec_id_03
       and c.rec_no =l_rec_no
       and trim(b.file_record) is not null
       and trim(replace(c.column7, '/', '')) is not null
       and a.bank_name =b.bank_name
       and c.bank_name =a.bank_name
       and a.file_name =b.file_name
       and c.file_name =a.file_name
       and b.status_code ='STAGE-4'
       and a.status_code =b.status_code
       and c.status_code =a.status_code
       and trunc(b.creation_date) =trunc(sysdate)
       and trunc(a.creation_date) =trunc(b.creation_date)
       and trunc(c.creation_date) =trunc(c.creation_date)
       and a.file_imported_flag ='Y'
       and c.file_imported_flag =a.file_imported_flag
       and b.line_number =c.rec_no
       and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                         and  d.request_id =a.request_id
                 )
     and d.attribute_category=g_cash_posn_attr_cat
     and d.attribute2 =g_cash_posn_attribute2
     and d.bank_account_num =c.column1
     and e.bank_party_id =d.bank_id
     and e.branch_party_id =d.bank_branch_id
union all
    select
         e.bank_name bank_name
        ,e.bank_branch_name bank_branch_name
        ,d.bank_account_name
        ,c.column1 bank_account_num
        ,nvl(c.column2, 'USD') currency_code
        ,c.column11 item_code
        ,case
           when trim(replace(c.column12, '/', '')) is not null then (to_number(replace(c.column12, '/', ''))/100)
           else to_number(null)
         end  item_amount
        ,case
           when trim(replace(c.column13, '/', '')) is not null then (to_number(replace(c.column13, '/', '')))
           else to_number(null)
         end  item_count
        ,trim(replace(c.column14, '/', '')) funds_type
        ,c.rec_id_no rec_id_no
        ,c.rec_no  rec_no
        ,c.file_name current_day_file_name
        ,g_sender_id sender_id
        ,g_receiver_id receiver_id
        ,to_date((g_file_creation_date||g_file_creation_time),'YYMMDDHH24MI') bank_file_create_date
        ,g_user created_by
        ,sysdate creation_date
        ,g_user last_updated_by
        ,sysdate last_update_date
        ,g_org_id org_id
        ,g_bank_name job_parameter
    from xxcus.xxcus_ce_current_day_files_b a
           ,xxcus.xxcus_ce_current_day_records_b b
           ,xxcus.xxcus_ce_stmt_int_tmp2_WF c
           ,apps.ce_bank_accounts  d
           ,apps.ce_bank_branches_v e
    where 1 =1
       and b.bank_name =g_bank_name
       and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and c.rec_id_no =g_rec_id_03
       and c.rec_no =l_rec_no
       and trim(b.file_record) is not null
       and trim(replace(c.column11, '/', '')) is not null
       and a.bank_name =b.bank_name
       and c.bank_name =a.bank_name
       and a.file_name =b.file_name
       and c.file_name =a.file_name
       and b.status_code ='STAGE-4'
       and a.status_code =b.status_code
       and c.status_code =a.status_code
       and trunc(b.creation_date) =trunc(sysdate)
       and trunc(a.creation_date) =trunc(b.creation_date)
       and trunc(c.creation_date) =trunc(c.creation_date)
       and a.file_imported_flag ='Y'
       and c.file_imported_flag =a.file_imported_flag
       and b.line_number =c.rec_no
       and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                         and  d.request_id =a.request_id
                 )
     and d.attribute_category=g_cash_posn_attr_cat
     and d.attribute2 =g_cash_posn_attribute2
     and d.bank_account_num =c.column1
     and e.bank_party_id =d.bank_id
     and e.branch_party_id =d.bank_branch_id
     ;  --end of cursor c_extract_03_info definition
  --
  type c_03_tbl is table of c_extract_03_info%rowtype index by binary_integer;
  c_03_rec c_03_tbl;
  --
  cursor c_extract_88_info
        (
                 l_rec_no in number
                ,p_bank_name in varchar2
                ,p_bank_br_name in varchar2
                ,p_bank_acct_name in varchar2
                ,p_bank_acct_num in varchar2
                ,p_bank_file_create_date in date
                ,p_currency_code in varchar2
        ) is
    select
         p_bank_name bank_name
        ,p_bank_br_name bank_branch_name
        ,p_bank_acct_name bank_account_name
        ,p_bank_acct_num bank_account_num
        ,p_currency_code currency_code
        ,c.column1 item_code
        ,case
           when trim(replace(c.column2, '/', '')) is not null then (to_number(replace(c.column2, '/', ''))/100)
           else to_number(null)
         end  item_amount
        ,case
           when trim(replace(c.column3, '/', '')) is not null then (to_number(replace(c.column3, '/', '')))
           else to_number(null)
         end  item_count
        ,trim(replace(c.column4, '/', '')) funds_type
        ,c.rec_id_no rec_id_no
        ,c.rec_no  rec_no
        ,c.file_name current_day_file_name
        ,g_sender_id sender_id
        ,g_receiver_id receiver_id
        ,p_bank_file_create_date bank_file_create_date
        ,g_user created_by
        ,sysdate creation_date
        ,g_user last_updated_by
        ,sysdate last_update_date
        ,g_org_id org_id
        ,g_bank_name job_parameter
    from xxcus.xxcus_ce_current_day_files_b a
           ,xxcus.xxcus_ce_current_day_records_b b
           ,xxcus.xxcus_ce_stmt_int_tmp2_WF c
    where 1 =1
       and b.bank_name =g_bank_name
       and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and c.rec_id_no =g_rec_id_88
       and c.rec_no =l_rec_no
       and trim(b.file_record) is not null
       and trim(replace(c.column1, '/', '')) is not null
       and a.bank_name =b.bank_name
       and c.bank_name =a.bank_name
       and a.file_name =b.file_name
       and c.file_name =a.file_name
       and b.status_code ='STAGE-4'
       and a.status_code =b.status_code
       and c.status_code =a.status_code
       and trunc(b.creation_date) =trunc(sysdate)
       and trunc(a.creation_date) =trunc(b.creation_date)
       and trunc(c.creation_date) =trunc(c.creation_date)
       and a.file_imported_flag ='Y'
       and c.file_imported_flag =a.file_imported_flag
       and b.line_number =c.rec_no
       and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                         and  d.request_id =a.request_id
                 )
union all
    select
         p_bank_name bank_name
        ,p_bank_br_name bank_branch_name
        ,p_bank_acct_name bank_account_name
        ,p_bank_acct_num bank_account_num
        ,p_currency_code currency_code
        ,c.column5 item_code
        ,case
           when trim(replace(c.column6, '/', '')) is not null then (to_number(replace(c.column6, '/', ''))/100)
           else to_number(null)
         end  item_amount
        ,case
           when trim(replace(c.column7, '/', '')) is not null then (to_number(replace(c.column7, '/', '')))
           else to_number(null)
         end  item_count
        ,trim(replace(c.column8, '/', '')) funds_type
        ,c.rec_id_no rec_id_no
        ,c.rec_no  rec_no
        ,c.file_name current_day_file_name
        ,g_sender_id sender_id
        ,g_receiver_id receiver_id
        ,p_bank_file_create_date bank_file_create_date
        ,g_user created_by
        ,sysdate creation_date
        ,g_user last_updated_by
        ,sysdate last_update_date
        ,g_org_id org_id
        ,g_bank_name job_parameter
    from xxcus.xxcus_ce_current_day_files_b a
           ,xxcus.xxcus_ce_current_day_records_b b
           ,xxcus.xxcus_ce_stmt_int_tmp2_WF c
    where 1 =1
       and b.bank_name =g_bank_name
       and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and c.rec_id_no =g_rec_id_88
       and c.rec_no =l_rec_no
       and trim(b.file_record) is not null
       and trim(replace(c.column5, '/', '')) is not null
       and a.bank_name =b.bank_name
       and c.bank_name =a.bank_name
       and a.file_name =b.file_name
       and c.file_name =a.file_name
       and b.status_code ='STAGE-4'
       and a.status_code =b.status_code
       and c.status_code =a.status_code
       and trunc(b.creation_date) =trunc(sysdate)
       and trunc(a.creation_date) =trunc(b.creation_date)
       and trunc(c.creation_date) =trunc(c.creation_date)
       and a.file_imported_flag ='Y'
       and c.file_imported_flag =a.file_imported_flag
       and b.line_number =c.rec_no
       and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                         and  d.request_id =a.request_id
                 )
union all
    select
         p_bank_name bank_name
        ,p_bank_br_name bank_branch_name
        ,p_bank_acct_name bank_account_name
        ,p_bank_acct_num bank_account_num
        ,p_currency_code currency_code
        ,c.column9 item_code
        ,case
           when trim(replace(c.column10, '/', '')) is not null then (to_number(replace(c.column10, '/', ''))/100)
           else to_number(null)
         end  item_amount
        ,case
           when trim(replace(c.column11, '/', '')) is not null then (to_number(replace(c.column11, '/', '')))
           else to_number(null)
         end  item_count
        ,trim(replace(c.column12, '/', '')) funds_type
        ,c.rec_id_no rec_id_no
        ,c.rec_no  rec_no
        ,c.file_name current_day_file_name
        ,g_sender_id sender_id
        ,g_receiver_id receiver_id
        ,p_bank_file_create_date bank_file_create_date
        ,g_user created_by
        ,sysdate creation_date
        ,g_user last_updated_by
        ,sysdate last_update_date
        ,g_org_id org_id
        ,g_bank_name job_parameter
    from xxcus.xxcus_ce_current_day_files_b a
           ,xxcus.xxcus_ce_current_day_records_b b
           ,xxcus.xxcus_ce_stmt_int_tmp2_WF c
    where 1 =1
       and b.bank_name =g_bank_name
       and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and c.rec_id_no =g_rec_id_88
       and c.rec_no =l_rec_no
       and trim(b.file_record) is not null
       and trim(replace(c.column9, '/', '')) is not null
       and a.bank_name =b.bank_name
       and c.bank_name =a.bank_name
       and a.file_name =b.file_name
       and c.file_name =a.file_name
       and b.status_code ='STAGE-4'
       and a.status_code =b.status_code
       and c.status_code =a.status_code
       and trunc(b.creation_date) =trunc(sysdate)
       and trunc(a.creation_date) =trunc(b.creation_date)
       and trunc(c.creation_date) =trunc(c.creation_date)
       and a.file_imported_flag ='Y'
       and c.file_imported_flag =a.file_imported_flag
       and b.line_number =c.rec_no
       and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                         and  d.request_id =a.request_id
                 )
union all
    select
         p_bank_name bank_name
        ,p_bank_br_name bank_branch_name
        ,p_bank_acct_name bank_account_name
        ,p_bank_acct_num bank_account_num
        ,p_currency_code currency_code
        ,c.column13 item_code
        ,case
           when trim(replace(c.column14, '/', '')) is not null then (to_number(replace(c.column10, '/', ''))/100)
           else to_number(null)
         end  item_amount
        ,case
           when trim(replace(c.column15, '/', '')) is not null then (to_number(replace(c.column11, '/', '')))
           else to_number(null)
         end  item_count
        ,trim(replace(c.column16, '/', '')) funds_type
        ,c.rec_id_no rec_id_no
        ,c.rec_no  rec_no
        ,c.file_name current_day_file_name
        ,g_sender_id sender_id
        ,g_receiver_id receiver_id
        ,p_bank_file_create_date bank_file_create_date
        ,g_user created_by
        ,sysdate creation_date
        ,g_user last_updated_by
        ,sysdate last_update_date
        ,g_org_id org_id
        ,g_bank_name job_parameter
    from xxcus.xxcus_ce_current_day_files_b a
           ,xxcus.xxcus_ce_current_day_records_b b
           ,xxcus.xxcus_ce_stmt_int_tmp2_WF c
    where 1 =1
       and b.bank_name =g_bank_name
       and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and c.rec_id_no =g_rec_id_88
       and c.rec_no =l_rec_no
       and trim(b.file_record) is not null
       and trim(replace(c.column13, '/', '')) is not null
       and a.bank_name =b.bank_name
       and c.bank_name =a.bank_name
       and a.file_name =b.file_name
       and c.file_name =a.file_name
       and b.status_code ='STAGE-4'
       and a.status_code =b.status_code
       and c.status_code =a.status_code
       and trunc(b.creation_date) =trunc(sysdate)
       and trunc(a.creation_date) =trunc(b.creation_date)
       and trunc(c.creation_date) =trunc(c.creation_date)
       and a.file_imported_flag ='Y'
       and c.file_imported_flag =a.file_imported_flag
       and b.line_number =c.rec_no
       and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                         and  d.request_id =a.request_id
                 )
     ;  --end of cursor c_extract_03_info definition
  --
  type c_88_tbl is table of c_extract_88_info%rowtype index by binary_integer;
  c_88_rec c_88_tbl;
  --
  l_file_name varchar(150) :=Null;
  --
begin -- Main Processing...
    --
    g_org_id :=nvl(mo_global.get_current_org_id, 163);
    --
    g_bank_name :=p_bank_name;
    --
    l_inbound_loc :=get_location ( v_operation =>'DBA_DIRECTORY', v_type  =>'INBOUND');
    --
    l_outbound_loc :=get_location ( v_operation =>'DBA_DIRECTORY', v_type  =>'OUTBOUND');
    --
    begin
         --
          delete  xxcus.xxcus_ce_stmt_hdr_int where job_parameter =p_bank_name;
         --
    exception
       when others then
         --
        print_log('@ other errors, error in flushing out xxcus.xxcus_ce_stmt_hdr_int table records for previous run:  bank '||p_bank_name||', error =>'||sqlerrm);
        raise program_error;
    end;
    --
    begin
         --
         open c_bank_new_files;
         fetch c_bank_new_files bulk collect into l_bank_new_rec;
         close c_bank_new_files;
         --
         if l_bank_new_rec.count >0 then
             --
             b_move_fwd :=TRUE;
             --
               for idx1 in l_bank_new_rec.first .. l_bank_new_rec.last loop
                    --
                     print_log(' Idx1 = '||idx1);
                     print_log('p_bank_name =>'||l_bank_new_rec(idx1).bank_name);
                     print_log('p_file_name =>'||l_bank_new_rec(idx1).file_name);
                     print_log('p_creation_date =>'||l_bank_new_rec(idx1).creation_date);
                     print_log('p_file_imported_flag =>'||l_bank_new_rec(idx1).file_imported_flag);
                     print_log('p_request_id =>'||l_bank_new_rec(idx1).request_id);
                     print_log('p_status_code =>'||l_bank_new_rec(idx1).status_code);
                     print_log('  ');
                    --
                       backup_stmt_int_tmp
                       (
                         p_bank_name=>l_bank_new_rec(idx1).bank_name
                        ,p_file_name =>l_bank_new_rec(idx1).file_name
                        ,p_creation_date =>l_bank_new_rec(idx1).creation_date
                        ,p_file_imported_flag =>l_bank_new_rec(idx1).file_imported_flag
                        ,p_request_id =>l_bank_new_rec(idx1).request_id
                        ,p_status_code =>l_bank_new_rec(idx1).status_code
                       );
                    --
               end loop;
             --
         else --l_bank_new_rec.count =0 then
             --
             b_move_fwd :=FALSE; --@101
             --
         end if;
         --
    exception
       when no_data_found then
         --
         b_move_fwd :=FALSE;
         --
       when others then
         --
         b_move_fwd :=FALSE;
         --
        print_log('@ other errors, error in flushing out xxcus.xxcus_ce_stmt_hdr_int table records for previous run:  bank '||p_bank_name||', error =>'||sqlerrm);
        raise program_error;
    end;
    --
    if (b_move_fwd) then -- @101, no issues in deleting previous xxcus.xxcus_ce_stmt_hdr_int records for the bank
     --
     -- PLSQL table l_bank_rec will hold all lines from the bank file ordered by line#
     --
     open c_bank_files;
     fetch c_bank_files bulk collect into l_bank_all_rec;
     close c_bank_files;
     --
     -- @ rec_id_no equal to 01 - PLSQL table l_bank_single_rec will hold all lines from the bank file for rec id 01
     --
     open c_bank_rec_id ( l_rec_id_no =>g_rec_id_01 );
     fetch c_bank_rec_id bulk collect into l_bank_single_rec;
     close c_bank_rec_id;
     --
         if l_bank_single_rec.count >0 then
          --
          print_log(' ');
          print_log('File record :'||l_bank_single_rec(1).file_record);
          print_log(' ');
              --
          print_log(' ');
          print_log('Position of end of logical record :'||regexp_instr(l_bank_single_rec(1).file_record, '/'));
          print_log(' ');
          --
          l_field_delimit_count :=regexp_count(l_bank_single_rec(1).file_record, g_field_delimiter);
          --
          l_last_field_delimiter_posn :=instr(l_bank_single_rec(1).file_record, g_field_delimiter, -1);
          --
          print_log('Total field delimiter count @ record 01 : '||l_field_delimit_count);
          print_log(' ');
          print_log('Position of last field delimiter @ record 01 : '||instr(l_bank_single_rec(1).file_record, g_field_delimiter, -1));
          print_log(' ');
          --
          --
              for ID01_Idx in 1 .. l_field_delimit_count+1 loop
                 --
                   if ID01_Idx =1 then
                     Null;
                  elsif ID01_Idx =2 then
                       g_sender_id :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='001';
                   elsif ID01_Idx =3 then
                       g_receiver_id :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='002';
                   elsif ID01_Idx =4 then
                       g_file_creation_date :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='003';
                   elsif ID01_Idx =5 then
                       g_file_creation_time :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='004';
                   elsif ID01_Idx =6 then
                       g_file_id_number :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='005';
                   elsif ID01_Idx =7 then
                       g_physical_rec_length :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='006';
                   elsif ID01_Idx =8 then
                       g_block_size :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='007';
                  else
                       g_version_number :=substr(l_bank_single_rec(1).file_record, l_last_field_delimiter_posn+1, (regexp_instr(l_bank_single_rec(1).file_record, '/')-1)-(l_last_field_delimiter_posn));
                       n_loc :='008';
                  end if;
                  --
              end loop;
              --
         end if;
     --
       print_log(' ');
       print_log('g_sender_id ='||g_sender_id);
       print_log('g_receiver_id ='||g_receiver_id);
       print_log('g_file_creation_date ='||g_file_creation_date);
       print_log('g_file_creation_time ='||g_file_creation_time);
       print_log('g_file_id_number ='||g_file_id_number);
       print_log('g_physical_rec_length ='||g_physical_rec_length);
       print_log('g_block_size ='||g_block_size);
       print_log('g_version_number ='||g_version_number);
       print_log(' ');
     --
     -- invoking procedure get_all_03_linenums will store all line numbers where rec id is 03 and in comma separated list of values
     --into the global variable g_all_03_lines;
     --
     begin
        get_all_03_linenums_wf;
     exception
      when others then
       print_log ('@ get_all_03_linenums_wf, error :'||sqlerrm);
     end;
     --
     open get_next_03_line;
     fetch get_next_03_line bulk collect into t_num_rec;
     close get_next_03_line;
     --
     l_field_delimit_count :=regexp_count(g_all_03_lines, g_field_delimiter);
     --
     print_log('@get_all_03_linenums_wf, l_field_delimit_count ='||l_field_delimit_count);
     --
     -- @ rec_id_no equal to 03 - PLSQL table l_bank_single_rec will hold all lines from the bank file for rec id 03
     --
     open c_bank_rec_id ( l_rec_id_no =>g_rec_id_03 );
     fetch c_bank_rec_id bulk collect into l_bank_single_rec;
     close c_bank_rec_id;
         --
         -- get the number of lines to move from the current line and assign it to the plsql table l_bank_single_rec
         --
        if l_bank_single_rec.count >0 then
               --
               for idx in 1 .. l_bank_single_rec.count loop
                  --
                  if idx != l_bank_single_rec.last then --do this for all records exception last one
                   --
                   --l_bank_single_rec(idx).lines_to_scan :=(t_num_rec(idx).next_03_line_num-1) -l_bank_single_rec(idx).rec_no;
                   l_bank_single_rec(idx).lines_to_scan
                        :=
                            get_03lines_excl_499899_wf
                              (
                                 p_from_line =>t_num_rec(idx).current_line
                                ,p_to_line =>t_num_rec(idx).next_03_line_num
                                ,p_last_rec =>'N'
                              );
                   --
                  else
                   -- when last record we will get the number of lines to scan using the below formula
                   --l_bank_single_rec(idx).lines_to_scan :=get_03lines_excl_499899 (p_last_03_line_num =>l_bank_single_rec(idx).rec_no);
                   l_bank_single_rec(idx).lines_to_scan
                        :=
                            get_03lines_excl_499899_wf
                              (
                                 p_from_line =>t_num_rec(idx).current_line
                                ,p_to_line =>t_num_rec(idx).next_03_line_num
                                ,p_last_rec =>'Y'
                              );
                   --
                  end if;
                  --
                  print_log('Rec# :'||l_bank_single_rec(idx).rec_no||', Max lines to scan :'||l_bank_single_rec(idx).lines_to_scan);
                   --
               end loop;
               --
        end if;
        --
        --
        --
          if l_bank_single_rec.count >0 then
                   --
               for idx in 1 .. l_bank_single_rec.count loop
                      --
                      l_file_name :=l_bank_single_rec(idx).file_name;
                      --
                      -- begin extracting 03 record and its bai codes
                      begin
                       --
                       open c_extract_03_info (l_rec_no =>l_bank_single_rec(idx).rec_no) ;
                       fetch c_extract_03_info bulk collect into c_03_rec;
                       close c_extract_03_info;
                       --
                           if c_03_rec.count >0 then
                                 -- begin run thru the first record to capture the bank details from EBS cash management tables.
                                 for i in 1 .. 1 loop
                                  --
                                     g_ce_bank_name  :=c_03_rec(i).bank_name;
                                     g_ce_bank_br_name  :=c_03_rec(i).bank_branch_name;
                                     g_bank_account_name  :=c_03_rec(i).bank_account_name;
                                     g_bank_account_num  :=c_03_rec(i).bank_account_num;
                                     g_bank_file_create_date  :=c_03_rec(i).bank_file_create_date;
                                     g_currency :=c_03_rec(i).currency_code;
                                  --
                                 end loop;
                                 --end run thru the first record to capture the bank details from EBS cash management tables.
                                 --
                                 --begin  insert 03 and its bai codes into the custom table
                                 begin
                                     --
                                     forall idx in 1 .. c_03_rec.count
                                       insert into xxcus.xxcus_ce_stmt_hdr_int values c_03_rec(idx) ;
                                     --
                                     g_proceed :=TRUE;
                                     --
                                 exception
                                  when others then
                                   print_log('Error in insert of c_03_rec, line# '||l_bank_single_rec(idx).rec_no||',message : '||sqlerrm);
                                   g_proceed :=FALSE;
                                   raise program_error;
                                 end;
                                --end insert 03 and its bai codes into the custom table
                                --
                                 if (g_proceed) then
                                   --
                                   for idx_88 in 1 .. (l_bank_single_rec(idx).lines_to_scan-1) loop
                                       --
                                           begin
                                             --
                                               open c_extract_88_info
                                                                    (
                                                                             -- l_rec_no use collection variable minus 1 [bcoz it also includes 03 record] plus the current 03 rec_no
                                                                             l_rec_no =>(l_bank_single_rec(idx).rec_no + idx_88)
                                                                              --l_rec_no =>(l_bank_single_rec(idx).rec_no + (l_bank_single_rec(idx).lines_to_scan-1))
                                                                            ,p_bank_name=>g_ce_bank_name
                                                                            ,p_bank_br_name =>g_ce_bank_br_name
                                                                            ,p_bank_acct_name =>g_bank_account_name
                                                                            ,p_bank_acct_num=>g_bank_account_num
                                                                            ,p_bank_file_create_date =>g_bank_file_create_date
                                                                            ,p_currency_code =>g_currency
                                                                    );
                                                fetch c_extract_88_info bulk collect into c_88_rec;
                                                close  c_extract_88_info;
                                             --
                                                  if (c_88_rec.count >0) then
                                                   --
                                                       begin
                                                        --
                                                        forall idx in 1 .. c_88_rec.count
                                                          insert into xxcus.xxcus_ce_stmt_hdr_int values c_88_rec(idx) ;
                                                        --
                                                       exception
                                                        when others then
                                                         print_log('Error in insert of 88 rec for line#, message ='||sqlerrm);
                                                       end;
                                                   --
                                                  end if;
                                             --
                                           exception
                                            when others then
                                             print_log('Error in fetching 88 records for line# '||sqlerrm);
                                             raise program_error;
                                           end;
                                       --
                                   end loop;
                                 end if;
                             --
                           end if;
                       --
                      exception
                       when no_data_found then
                        print_log('No records to fetch for rec_id_no 03 and line#  '||l_bank_single_rec(idx).rec_no||', msg ='||sqlerrm);
                        raise program_error;
                       when others then
                        print_log(' '||sqlerrm);
                        raise program_error;
                      end;
                      -- end extracting 03 record and its bai codes
                      --
               end loop; --end loop for for idx in 1 .. l_bank_single_rec.count
                   --
          end if; -- if l_bank_single_rec.count >0 then
         --
             --
              begin
               savepoint sqr2;
               update xxcus.xxcus_ce_current_day_records_b
                   set status_code ='STAGE-5'
                         ,last_update_date =sysdate
                         ,last_updated_by =g_user
                 where 1 =1
                       and bank_name =p_bank_name
                       and file_name =l_file_name
                       and status_code = 'STAGE-4'
                       and trunc(creation_date) =trunc(sysdate)
                 ;
                 --
              exception
                 when others then
                    print_log('@xxcus.xxcus_ce_current_day_records_b, Issue in update of status code From STAGE-4 to STAGE-5 for bank :'||p_bank_name||', file :'||l_file_name||', error =>'||sqlerrm);
                    rollback to sqr2;
                    raise program_error;
              end;
             --
              begin
               --
               print_log(' ');
               print_log('l_file_name ='||l_file_name);
               print_log('p_bank_name ='||p_bank_name);
               print_log('g_bai_codes_complete ='||g_bai_codes_complete);
               print_log(' ');
               --
               savepoint sqr1;
               --
               update  xxcus.xxcus_ce_current_day_files_b a
                   set a.status_code ='STAGE-5'
                         ,a.comments =g_bai_codes_complete
                         ,a.last_update_date =sysdate
                         ,a.last_updated_by =g_user
                 where 1 =1
                       and a.bank_name =p_bank_name
                       and a.file_name =l_file_name
                       and a.status_code = 'STAGE-4'
                       and a.file_imported_flag ='Y'
                       and a.creation_date =
                         (
                            select max(d.creation_date)
                            from  xxcus.xxcus_ce_current_day_files_b d
                            where 1 =1
                                 and  d.bank_name =a.bank_name
                                 and  d.file_name =a.file_name
                                 and  d.status_code =a.status_code
                                 and  d.file_imported_flag =a.file_imported_flag
                         )
                 ;
                 --
                 print_log('@xxcus.xxcus_ce_current_day_files_b: updated status code to STAGE-5 for bank '||p_bank_name||', total record :'||sql%rowcount);
                 print_log(' ');
                 --
              exception
                 when others then
                    print_log('@xxcus.xxcus_ce_current_day_files_b, Issue in update of status code From STAGE-4 to STAGE-5 for bank :'||p_bank_name||', file :'||l_file_name||', error =>'||sqlerrm);
                    rollback to sqr1;
                    raise program_error;
              end;
             --
    end if; --endif (b_move_fwd) then -- @101, no issues in deleting previous xxcus.xxcus_ce_stmt_hdr_int records for the bank
    --
exception
   when others then
    print_log('outer block of WELLSFARGO BAI2 post processing, error message :'||sqlerrm);
    raise program_error;
end WELLSFARGO;
   --
 procedure BOFA_US
   (
     retcode out varchar2
    ,errbuf  out varchar2
    ,p_bank_name in varchar2
   ) is
  -- =================================================================================
  -- local variables
  -- =================================================================================
  l_input_file_id   UTL_FILE.FILE_TYPE;
  l_line_read       VARCHAR2(2000):= NULL;
  l_line_count      NUMBER :=0;
  l_file_name varchar2(150) :=Null;
  l_lines_processed NUMBER :=0;
  l_org_id NUMBER :=163;
  --
  l_print_count     NUMBER :=0;
  b_move_fwd      BOOLEAN;
  v_unprocessed_flag VARCHAR2(1) :='Z';
  --
  o_return_status           VARCHAR2 (2000);
  o_msg_count                 NUMBER;
  o_msg_data                   VARCHAR2 (2000);
  --
  v_api_message             VARCHAR2 (4000);
  n_count                           NUMBER :=0;
  n_seq                               NUMBER :=0;
  --
 l_field_delimit_count number :=0;
 l_last_field_delimiter_posn number :=0;
 n_loc varchar2(3) :=Null;
  --
  l_inbound_loc VARCHAR2 (150) :=Null;
  l_outbound_loc  VARCHAR2 (150) :=Null;
  --
  type t_max_lines_type is record
   (
    max_lines number
   );
  type t_max_lines_tbl is table of t_max_lines_type index by binary_integer;
  l_line_loop t_max_lines_tbl;
  --
  type t_num_type is record
   (
    current_line number,
    next_03_line_num number
   );
  type t_num_tbl is table of t_num_type index by binary_integer;
  t_num_rec t_num_tbl;
  --
   cursor c_bank_new_files is
    select A1.*
    from  xxcus.xxcus_ce_current_day_files_b A1
    where 1 =1
         and A1.bank_name =p_bank_name
         and A1.status_code ='STAGE-4'
         and A1.file_imported_flag ='Y'
         and trunc(A1.creation_date) =trunc(sysdate)
         and A1.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
         and A1.creation_date =
                 (
                    select max(A2.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b A2
                    where 1 =1
                         and  A2.bank_name =A1.bank_name
                         and  A2.file_name =A1.file_name
                         and  A2.status_code =A1.status_code
                         and  A2.file_imported_flag =A1.file_imported_flag
                         and  A2.request_id =A1.request_id
                 )
          ;
   --
   TYPE l_bank_new_type IS table of c_bank_new_files%rowtype index by binary_integer;
   l_bank_new_rec l_bank_new_type;
  --
  cursor get_next_03_line is
  select to_number(xmltbl.column_value) current_line
             ,nvl(lead(to_number(xmltbl.column_value), 1) over(order by to_number(xmltbl.column_value)),0) next_03_line_num
  from xmltable(g_all_03_lines) xmltbl;
  --
  cursor c_bank_rec_id ( l_rec_id_no IN VARCHAR2 ) is
  select
              b.file_name        file_name
             ,b.status_code   status_code
             ,c.rec_id_no        rec_id_no
             ,c.rec_no              rec_no
             ,b.file_record      file_record
             ,'N'                         extract_flag
             ,0                            lines_to_scan
             ,0                            next_03_line_num
             ,'N'                         last_rec
  from xxcus.xxcus_ce_current_day_files_b a
           ,xxcus.xxcus_ce_current_day_records_b b
           ,xxcus.xxcus_ce_stmt_int_tmp2_BOA_US c
  where 1 =1
       and b.bank_name =g_bank_name
       and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and c.rec_id_no =l_rec_id_no
       and trim(b.file_record) is not null
       and a.bank_name =b.bank_name
       and c.bank_name =a.bank_name
       and a.file_name =b.file_name
       and c.file_name =a.file_name
       and b.status_code ='STAGE-4'
       and a.status_code =b.status_code
       and c.status_code =a.status_code
       and trunc(b.creation_date) =trunc(sysdate)
       and trunc(a.creation_date) =trunc(b.creation_date)
       and trunc(c.creation_date) =trunc(c.creation_date)
       and a.file_imported_flag ='Y'
       and c.file_imported_flag =a.file_imported_flag
       and b.line_number =c.rec_no
       and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                         and  d.request_id =a.request_id
                 )
order by b.line_number asc;
  --
  type l_bank_rec_type is table of c_bank_rec_id%rowtype index by binary_integer;
  l_bank_single_rec l_bank_rec_type;
  --
  cursor c_bank_files is
  select b.file_name        file_name
             ,b.status_code   status_code
             ,b.line_number  line_number
             ,c.rec_id_no        rec_id_no
             ,c.rec_no              rec_no
             ,b.file_record      file_record
             ,'N'                         extract_flag
  from xxcus.xxcus_ce_current_day_files_b a
           ,xxcus.xxcus_ce_current_day_records_b b
           ,xxcus.xxcus_ce_stmt_int_tmp2_BOA_US c
  where 1 =1
       and b.bank_name =g_bank_name
       and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and trim(b.file_record) is not null
       and a.bank_name =b.bank_name
       and c.bank_name =a.bank_name
       and a.file_name =b.file_name
       and c.file_name =a.file_name
       and b.status_code ='STAGE-4'
       and a.status_code =b.status_code
       and c.status_code =a.status_code
       and trunc(b.creation_date) =trunc(sysdate)
       and trunc(a.creation_date) =trunc(b.creation_date)
       and trunc(c.creation_date) =trunc(c.creation_date)
       and a.file_imported_flag ='Y'
       and c.file_imported_flag =a.file_imported_flag
       and b.line_number =c.rec_no
       and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                         and  d.request_id =a.request_id
                 )
  order by b.line_number asc
   ;
  --
  type l_bank_tbl is table of c_bank_files%rowtype index by binary_integer;
  l_bank_all_rec l_bank_tbl;
  --
  -- begin declaration of cursor c_extract_03_info
  cursor c_extract_03_info (l_rec_no in number) is
    select
         e.bank_name bank_name
        ,e.bank_branch_name bank_branch_name
        ,d.bank_account_name
        ,c.column1 bank_account_num
        ,trim(replace(c.column2, '/', '')) currency_code
        ,to_char(null) item_code
        ,to_number(null) item_amount
        ,to_number(null) item_count
        ,to_char(null) funds_type
        ,c.rec_id_no rec_id_no
        ,c.rec_no  rec_no
        ,c.file_name current_day_file_name
        ,g_sender_id sender_id
        ,g_receiver_id receiver_id
        ,to_date((g_file_creation_date||g_file_creation_time),'YYMMDDHH24MI') bank_file_create_date
        ,g_user created_by
        ,sysdate creation_date
        ,g_user last_updated_by
        ,sysdate last_update_date
        ,g_org_id org_id
        ,g_bank_name job_parameter
    from xxcus.xxcus_ce_current_day_files_b a
           ,xxcus.xxcus_ce_current_day_records_b b
           ,xxcus.xxcus_ce_stmt_int_tmp2_BOA_US c
           ,apps.ce_bank_accounts  d
           ,apps.ce_bank_branches_v e
    where 1 =1
       and b.bank_name =g_bank_name
       and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and c.rec_id_no =g_rec_id_03
       and c.rec_no =l_rec_no
       and trim(b.file_record) is not null
       and trim(replace(c.column1, '/', '')) is not null --atleast bank account number is expected for a summary line.
       and a.bank_name =b.bank_name
       and c.bank_name =a.bank_name
       and a.file_name =b.file_name
       and c.file_name =a.file_name
       and b.status_code ='STAGE-4'
       and a.status_code =b.status_code
       and c.status_code =a.status_code
       and trunc(b.creation_date) =trunc(sysdate)
       and trunc(a.creation_date) =trunc(b.creation_date)
       and trunc(c.creation_date) =trunc(c.creation_date)
       and a.file_imported_flag ='Y'
       and c.file_imported_flag =a.file_imported_flag
       and b.line_number =c.rec_no
       and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                         and  d.request_id =a.request_id
                 )
     and d.attribute_category=g_cash_posn_attr_cat
     and d.attribute2 =g_cash_posn_attribute2
     and d.bank_account_num =c.column1
     and e.bank_party_id =d.bank_id
     and e.branch_party_id =d.bank_branch_id
     ;
  --end of cursor c_extract_03_info definition
  --
  type c_03_tbl is table of c_extract_03_info%rowtype index by binary_integer;
  c_03_rec c_03_tbl;
  --
  -- begin declaration of cursor c_extract_88_info
  cursor c_extract_88_info
        (
                 p_bank_name in varchar2
                ,p_bank_br_name in varchar2
                ,p_bank_acct_name in varchar2
                ,p_bank_acct_num in varchar2
                ,p_bank_file_create_date in date
                ,p_currency_code in varchar2
                ,p_last_rec in varchar2
                ,p_from_line in number
                ,p_to_line in number
        ) is
    select
         p_bank_name bank_name
        ,p_bank_br_name bank_branch_name
        ,p_bank_acct_name bank_account_name
        ,p_bank_acct_num bank_account_num
        ,p_currency_code currency_code
        ,trim(replace(c.column1, '/', ''))  item_code
        ,case
           when trim(replace(c.column2, '/', '')) is not null then (to_number(replace(c.column2, '/', ''))/100)
           else to_number(null)
         end  item_amount
        ,case
           when trim(replace(c.column3, '/', '')) is not null then (to_number(replace(c.column3, '/', '')))
           else to_number(null)
         end  item_count
        ,trim(replace(c.column4, '/', '')) funds_type
        ,c.rec_id_no rec_id_no
        ,c.rec_no  rec_no
        ,c.file_name current_day_file_name
        ,g_sender_id sender_id
        ,g_receiver_id receiver_id
        ,p_bank_file_create_date bank_file_create_date
        ,g_user created_by
        ,sysdate creation_date
        ,g_user last_updated_by
        ,sysdate last_update_date
        ,g_org_id org_id
        ,g_bank_name job_parameter
    from xxcus.xxcus_ce_current_day_files_b a
           ,xxcus.xxcus_ce_current_day_records_b b
           ,xxcus.xxcus_ce_stmt_int_tmp2_BOA_US c
    where 1 =1
       and b.bank_name =g_bank_name
       and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and c.rec_id_no =g_rec_id_88
       and (
                  (p_last_rec ='N' and (c.rec_no between p_from_line and p_to_line and c.rec_id_no NOT IN (g_rec_id_49, g_rec_id_16, g_rec_id_02) ))
                OR
                  (p_last_rec ='Y' and c.rec_no >=p_from_line and c.rec_id_no NOT IN (g_rec_id_16, g_rec_id_49, g_rec_id_98, g_rec_id_99, g_rec_id_02) )
               )
       and trim(b.file_record) is not null
       and (trim(replace(c.column1, '/', '')) is not null and trim(replace(c.column1, '/', '')) not like '%CUR%')
       and a.bank_name =b.bank_name
       and c.bank_name =a.bank_name
       and a.file_name =b.file_name
       and c.file_name =a.file_name
       and b.status_code ='STAGE-4'
       and a.status_code =b.status_code
       and c.status_code =a.status_code
       and trunc(b.creation_date) =trunc(sysdate)
       and trunc(a.creation_date) =trunc(b.creation_date)
       and trunc(c.creation_date) =trunc(c.creation_date)
       and a.file_imported_flag ='Y'
       and c.file_imported_flag =a.file_imported_flag
       and b.line_number =c.rec_no
       and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                         and  d.request_id =a.request_id
                 )
      order by c.rec_no asc
     ;
  --end of cursor c_extract_88_info definition
  --
  type c_88_tbl is table of c_extract_88_info%rowtype index by binary_integer;
  c_88_rec c_88_tbl;
  --
  -- begin declaration of cursor c_extract_16_info
  cursor c_extract_16_info
        (
                 p_bank_name in varchar2
                ,p_bank_br_name in varchar2
                ,p_bank_acct_name in varchar2
                ,p_bank_acct_num in varchar2
                ,p_bank_file_create_date in date
                ,p_currency_code in varchar2
                ,p_last_rec in varchar2
                ,p_from_line in number
                ,p_to_line in number
        ) is
    select
         p_bank_name bank_name
        ,p_bank_br_name bank_branch_name
        ,p_bank_acct_name bank_account_name
        ,p_bank_acct_num bank_account_num
        ,p_currency_code currency_code
        ,trim(replace(c.column1, '/', ''))  item_code
        ,case
           when trim(replace(c.column2, '/', '')) is not null then (to_number(replace(c.column2, '/', ''))/100)
           else to_number(null)
         end  item_amount
        ,to_number(null) item_count
        ,case
           when trim(replace(c.column3, '/', '')) is not null then (trim(replace(c.column3, '/', '')))
           else null
         end  funds_type
        ,c.rec_id_no rec_id_no
        ,c.rec_no  rec_no
        ,c.file_name current_day_file_name
        ,g_sender_id sender_id
        ,g_receiver_id receiver_id
        ,p_bank_file_create_date bank_file_create_date
        ,g_user created_by
        ,sysdate creation_date
        ,g_user last_updated_by
        ,sysdate last_update_date
        ,g_org_id org_id
        ,g_bank_name job_parameter
    from xxcus.xxcus_ce_current_day_files_b a
           ,xxcus.xxcus_ce_current_day_records_b b
           ,xxcus.xxcus_ce_stmt_int_tmp2_BOA_US c
    where 1 =1
       and b.bank_name =g_bank_name
       and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and c.rec_id_no =g_rec_id_16
       and (
                  (p_last_rec ='N' and (c.rec_no between p_from_line and p_to_line and c.rec_id_no NOT IN (g_rec_id_49,g_rec_id_88, g_rec_id_02) ))
                OR
                  (p_last_rec ='Y' and c.rec_no >=p_from_line and c.rec_id_no NOT IN (g_rec_id_88, g_rec_id_49,g_rec_id_98, g_rec_id_99, g_rec_id_02) )
               )
       and trim(b.file_record) is not null
       and trim(replace(c.column1, '/', '')) is not null
       and a.bank_name =b.bank_name
       and c.bank_name =a.bank_name
       and a.file_name =b.file_name
       and c.file_name =a.file_name
       and b.status_code ='STAGE-4'
       and a.status_code =b.status_code
       and c.status_code =a.status_code
       and trunc(b.creation_date) =trunc(sysdate)
       and trunc(a.creation_date) =trunc(b.creation_date)
       and trunc(c.creation_date) =trunc(c.creation_date)
       and a.file_imported_flag ='Y'
       and c.file_imported_flag =a.file_imported_flag
       and b.line_number =c.rec_no
       and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                         and  d.request_id =a.request_id
                 )
     order by c.rec_no asc
     ;
  --end of cursor c_extract_88_info definition
  --
  type c_16_tbl is table of c_extract_16_info%rowtype index by binary_integer;
  c_16_rec c_16_tbl;
  --
  --
begin -- Main Processing...
    --
    g_org_id :=nvl(mo_global.get_current_org_id, 163);
    --
    g_bank_name :=p_bank_name;
    --
    l_inbound_loc :=get_location ( v_operation =>'DBA_DIRECTORY', v_type  =>'INBOUND');
    --
    l_outbound_loc :=get_location ( v_operation =>'DBA_DIRECTORY', v_type  =>'OUTBOUND');
    --
    begin
         --
          delete  xxcus.xxcus_ce_stmt_hdr_int where job_parameter =p_bank_name;
         --
    exception
       when others then
        print_log('@ other errors, error in flushing out xxcus.xxcus_ce_stmt_hdr_int table records for previous run:  bank '||p_bank_name||', error =>'||sqlerrm);
        raise program_error;
    end;
    --
    begin
         --
         open c_bank_new_files;
         fetch c_bank_new_files bulk collect into l_bank_new_rec;
         close c_bank_new_files;
         --
         if l_bank_new_rec.count >0 then
             --
             b_move_fwd :=TRUE;
             --
               for idx1 in l_bank_new_rec.first .. l_bank_new_rec.last loop
                    --
                     print_log(' Idx1 = '||idx1);
                     print_log('p_bank_name =>'||l_bank_new_rec(idx1).bank_name);
                     print_log('p_file_name =>'||l_bank_new_rec(idx1).file_name);
                     print_log('p_creation_date =>'||l_bank_new_rec(idx1).creation_date);
                     print_log('p_file_imported_flag =>'||l_bank_new_rec(idx1).file_imported_flag);
                     print_log('p_request_id =>'||l_bank_new_rec(idx1).request_id);
                     print_log('p_status_code =>'||l_bank_new_rec(idx1).status_code);
                     print_log('  ');
                    --
                       backup_stmt_int_tmp
                       (
                         p_bank_name=>l_bank_new_rec(idx1).bank_name
                        ,p_file_name =>l_bank_new_rec(idx1).file_name
                        ,p_creation_date =>l_bank_new_rec(idx1).creation_date
                        ,p_file_imported_flag =>l_bank_new_rec(idx1).file_imported_flag
                        ,p_request_id =>l_bank_new_rec(idx1).request_id
                        ,p_status_code =>l_bank_new_rec(idx1).status_code
                       );
                    --
               end loop;
             --
         else
             --
             b_move_fwd :=FALSE;
             --
         end if;
         --
    exception
       when no_data_found then
         --
         b_move_fwd :=FALSE;
         --
       when others then
         --
         b_move_fwd :=FALSE;
         --
        print_log('@ other errors in pulling bank file details for bank '||p_bank_name||', error =>'||sqlerrm);
        raise program_error;
    end;
    --
    if (b_move_fwd) then -- @101, no issues in deleting previous xxcus.xxcus_ce_stmt_hdr_int records for the bank
     --
     -- PLSQL table l_bank_rec will hold all lines from the bank file ordered by line#
     --
     open c_bank_files;
     fetch c_bank_files bulk collect into l_bank_all_rec;
     close c_bank_files;
     --
     -- @ rec_id_no equal to 01 - PLSQL table l_bank_single_rec will hold all lines from the bank file for rec id 01
     --
     open c_bank_rec_id ( l_rec_id_no =>g_rec_id_01 );
     fetch c_bank_rec_id bulk collect into l_bank_single_rec;
     close c_bank_rec_id;
     --
         if l_bank_single_rec.count >0 then
          --
          print_log(' ');
          print_log('File record :'||l_bank_single_rec(1).file_record);
          print_log(' ');
              --
          print_log(' ');
          print_log('Position of end of logical record :'||regexp_instr(l_bank_single_rec(1).file_record, '/'));
          print_log(' ');
          --
          l_field_delimit_count :=regexp_count(l_bank_single_rec(1).file_record, g_field_delimiter);
          --
          l_last_field_delimiter_posn :=instr(l_bank_single_rec(1).file_record, g_field_delimiter, -1);
          --
          print_log('Total field delimiter count @ record 01 : '||l_field_delimit_count);
          print_log(' ');
          print_log('Position of last field delimiter @ record 01 : '||instr(l_bank_single_rec(1).file_record, g_field_delimiter, -1));
          print_log(' ');
          --
          --
              for ID01_Idx in 1 .. l_field_delimit_count+1 loop
                 --
                   if ID01_Idx =1 then
                     Null;
                  elsif ID01_Idx =2 then
                       g_sender_id :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='001';
                   elsif ID01_Idx =3 then
                       g_receiver_id :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='002';
                   elsif ID01_Idx =4 then
                       g_file_creation_date :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='003';
                   elsif ID01_Idx =5 then
                       g_file_creation_time :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='004';
                   elsif ID01_Idx =6 then
                       g_file_id_number :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='005';
                   elsif ID01_Idx =7 then
                       g_physical_rec_length :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='006';
                   elsif ID01_Idx =8 then
                       g_block_size :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='007';
                  else
                       g_version_number :=substr(l_bank_single_rec(1).file_record, l_last_field_delimiter_posn+1, (regexp_instr(l_bank_single_rec(1).file_record, '/')-1)-(l_last_field_delimiter_posn));
                       n_loc :='008';
                  end if;
                  --
              end loop;
              --
         end if;
     --
       print_log(' ');
       print_log('g_sender_id ='||g_sender_id);
       print_log('g_receiver_id ='||g_receiver_id);
       print_log('g_file_creation_date ='||g_file_creation_date);
       print_log('g_file_creation_time ='||g_file_creation_time);
       print_log('g_file_id_number ='||g_file_id_number);
       print_log('g_physical_rec_length ='||g_physical_rec_length);
       print_log('g_block_size ='||g_block_size);
       print_log('g_version_number ='||g_version_number);
       print_log(' ');
     --
     -- invoking procedure get_all_03_linenums will store all line numbers where rec id is 03 and in comma separated list of values
     --into the global variable g_all_03_lines;
     --
     begin
        get_all_03_linenums_boa_us;
     exception
      when others then
       print_log ('@ get_all_03_linenums_boa_us, error :'||sqlerrm);
     end;
     --
     open get_next_03_line;
     fetch get_next_03_line bulk collect into t_num_rec;
     close get_next_03_line;
     --
     l_field_delimit_count :=regexp_count(g_all_03_lines, g_field_delimiter);
     --
     print_log('@get_all_03_linenums_boa_us, l_field_delimit_count ='||l_field_delimit_count);
     --
     -- @ rec_id_no equal to 03 - PLSQL table l_bank_single_rec will hold all lines from the bank file for rec id 03
     --
     open c_bank_rec_id ( l_rec_id_no =>g_rec_id_03 );
     fetch c_bank_rec_id bulk collect into l_bank_single_rec;
     close c_bank_rec_id;
         --
         -- get the number of lines to move from the current line and assign it to the plsql table l_bank_single_rec
         --
        if l_bank_single_rec.count >0 then
               --
               for idx in 1 .. l_bank_single_rec.count loop
                  --
                  if idx != l_bank_single_rec.last then --do this for all records exception last one
                        --
                         l_bank_single_rec(idx).lines_to_scan
                           :=
                            get_03lines_excl_499899_boa_us
                              (
                                 p_from_line =>t_num_rec(idx).current_line
                                ,p_to_line =>t_num_rec(idx).next_03_line_num
                                ,p_last_rec =>'N'
                              );
                         --
                         l_bank_single_rec(idx).next_03_line_num :=t_num_rec(idx).next_03_line_num;
                         --
                         l_bank_single_rec(idx).last_rec :='N';
                         --
                   --
                  else
                           -- when last record we will get the number of lines to scan using the below formula
                           --
                           l_bank_single_rec(idx).lines_to_scan
                             :=
                               get_03lines_excl_499899_boa_us
                                (
                                 p_from_line =>t_num_rec(idx).current_line
                                ,p_to_line =>t_num_rec(idx).next_03_line_num
                                ,p_last_rec =>'Y'
                                );
                         --
                         l_bank_single_rec(idx).next_03_line_num :=t_num_rec(idx).next_03_line_num;
                         --
                         l_bank_single_rec(idx).last_rec :='Y';
                         --
                   --
                  end if;
                  --
                  print_log(
                                           'Rec# '||
                                           l_bank_single_rec(idx).rec_no||
                                           ', Lines to scan : '||
                                           l_bank_single_rec(idx).lines_to_scan||
                                           ', Next 03 Line# '||l_bank_single_rec(idx).next_03_line_num||
                                           ', Last Record ? : '||l_bank_single_rec(idx).last_rec
                                   );
                   --
               end loop;
               --
        end if;
        --
        --
        --
          if l_bank_single_rec.count >0 then
                   --
               for idx in 1 .. l_bank_single_rec.count loop
                      --
                      l_file_name :=l_bank_single_rec(idx).file_name;
                      --
                      -- begin extracting 03 record and its bai codes
                      begin
                       --
                       open c_extract_03_info (l_rec_no =>l_bank_single_rec(idx).rec_no) ;
                       fetch c_extract_03_info bulk collect into c_03_rec;
                       close c_extract_03_info;
                       --
                           if c_03_rec.count >0 then
                                 --
                                 begin
                                      -- begin run thru the first record to capture the bank details from EBS cash management tables.
                                     for i in 1 .. 1 loop
                                      --
                                         g_ce_bank_name  :=c_03_rec(i).bank_name;
                                         g_ce_bank_br_name  :=c_03_rec(i).bank_branch_name;
                                         g_bank_account_name  :=c_03_rec(i).bank_account_name;
                                         g_bank_account_num  :=c_03_rec(i).bank_account_num;
                                         g_bank_file_create_date  :=c_03_rec(i).bank_file_create_date;
                                         g_currency :=c_03_rec(i).currency_code;
                                      --
                                         g_proceed :=TRUE;
                                      -- @@@@@@
                                            --
                                             if (g_proceed) then
                                               -- begin insert of 88 records
                                                   begin
                                                     --
                                                       open c_extract_88_info
                                                                            (
                                                                                     p_bank_name=>g_ce_bank_name
                                                                                    ,p_bank_br_name =>g_ce_bank_br_name
                                                                                    ,p_bank_acct_name =>g_bank_account_name
                                                                                    ,p_bank_acct_num=>g_bank_account_num
                                                                                    ,p_bank_file_create_date =>g_bank_file_create_date
                                                                                    ,p_currency_code =>g_currency
                                                                                    ,p_last_rec =>l_bank_single_rec(idx).last_rec
                                                                                    ,p_from_line =>l_bank_single_rec(idx).rec_no
                                                                                    ,p_to_line =>l_bank_single_rec(idx).next_03_line_num
                                                                            );
                                                        fetch c_extract_88_info bulk collect into c_88_rec;
                                                        close  c_extract_88_info;
                                                     --
                                                          if (c_88_rec.count >0) then
                                                           --
                                                               begin
                                                                --
                                                                forall idx in 1 .. c_88_rec.count
                                                                  insert into xxcus.xxcus_ce_stmt_hdr_int values c_88_rec(idx) ;
                                                                --
                                                               exception
                                                                when others then
                                                                 print_log('Error in insert of 88 rec for line# '||l_bank_single_rec(idx).rec_no||', message ='||sqlerrm);
                                                               end;
                                                           --
                                                          end if;
                                                     --
                                                   exception
                                                    when others then
                                                     print_log('Error in fetching 88 records for line# '||l_bank_single_rec(idx).rec_no||', msg ='||sqlerrm);
                                                     raise program_error;
                                                   end;
                                               -- end insert of 88 records
                                               -- begin insert of 16 records
                                               --
                                                   begin
                                                     --
                                                       open c_extract_16_info
                                                                            (
                                                                                     p_bank_name=>g_ce_bank_name
                                                                                    ,p_bank_br_name =>g_ce_bank_br_name
                                                                                    ,p_bank_acct_name =>g_bank_account_name
                                                                                    ,p_bank_acct_num=>g_bank_account_num
                                                                                    ,p_bank_file_create_date =>g_bank_file_create_date
                                                                                    ,p_currency_code =>g_currency
                                                                                    ,p_last_rec =>l_bank_single_rec(idx).last_rec
                                                                                    ,p_from_line =>l_bank_single_rec(idx).rec_no
                                                                                    ,p_to_line =>l_bank_single_rec(idx).next_03_line_num
                                                                            );
                                                        fetch c_extract_16_info bulk collect into c_16_rec;
                                                        close  c_extract_16_info;
                                                     --
                                                          if (c_16_rec.count >0) then
                                                           --
                                                               begin
                                                                --
                                                                forall idx in 1 .. c_16_rec.count
                                                                  insert into xxcus.xxcus_ce_stmt_hdr_int values c_16_rec(idx) ;
                                                                --
                                                               exception
                                                                when others then
                                                                 print_log('Error in insert of 16 rec for line# '||l_bank_single_rec(idx).rec_no||', message ='||sqlerrm);
                                                               end;
                                                           --
                                                          end if;
                                                     --
                                                   exception
                                                    when others then
                                                     print_log (' ');
                                                     print_log('Error in fetching 16 records for line# '||l_bank_single_rec(idx).rec_no||', msg ='||sqlerrm);
                                                     print_log (' ');
                                                     print_log (' Current Parameters ');
                                                     print_log (' p_bank_name=>'||g_ce_bank_name);
                                                     print_log (' p_bank_br_name=>'||g_ce_bank_br_name);
                                                     print_log (' p_bank_acct_name=>'||g_bank_account_name);
                                                     print_log (' p_bank_acct_num=>'||g_bank_account_num);
                                                     print_log (' p_bank_file_create_date=>'||g_bank_file_create_date);
                                                     print_log (' p_last_rec=>'||l_bank_single_rec(idx).last_rec);
                                                     print_log (' p_from_line=>'||l_bank_single_rec(idx).rec_no);
                                                     print_log (' p_to_line=>'||l_bank_single_rec(idx).next_03_line_num);
                                                     raise program_error;
                                                   end;
                                               --
                                               -- end insert of 16 records
                                             end if;
                                         --
                                      -- @@@@@@
                                     end loop;
                                      --
                                 exception
                                  when others then
                                   print_log('@ rec_id_no 16, get first record of rec_id_no 03 to assign global parameters, message : '||sqlerrm);
                                   raise program_error;
                                 end;
                                 --
                           end if;
                       --
                      exception
                       when no_data_found then
                        print_log('No records to fetch for rec_id_no 03 and line#  '||l_bank_single_rec(idx).rec_no||', msg ='||sqlerrm);
                        raise program_error;
                       when others then
                        print_log(' '||sqlerrm);
                        raise program_error;
                      end;
                      -- end extracting 03 record and its bai codes
                      --
               end loop; --end loop for for idx in 1 .. l_bank_single_rec.count
                   --
          end if; -- if l_bank_single_rec.count >0 then
         --
      --
     --
      begin
       savepoint sqr2;
       update xxcus.xxcus_ce_current_day_records_b
           set status_code ='STAGE-5'
                 ,last_update_date =sysdate
                 ,last_updated_by =g_user
         where 1 =1
               and bank_name =p_bank_name
               and file_name =l_file_name
               and status_code = 'STAGE-4'
               and trunc(creation_date) =trunc(sysdate)
         ;
         --
      exception
         when others then
            print_log('@xxcus.xxcus_ce_current_day_records_b, Issue in update of status code From STAGE-4 to STAGE-5 for bank :'||p_bank_name||', file :'||l_file_name||', error =>'||sqlerrm);
            rollback to sqr2;
            raise program_error;
      end;
     --
      begin
       --
       print_log(' ');
       print_log('l_file_name ='||l_file_name);
       print_log('p_bank_name ='||p_bank_name);
       print_log('g_bai_codes_complete ='||g_bai_codes_complete);
       print_log(' ');
       --
       savepoint sqr1;
       --
       update  xxcus.xxcus_ce_current_day_files_b a
           set a.status_code ='STAGE-5'
                 ,a.comments =g_bai_codes_complete
                 ,a.last_update_date =sysdate
                 ,a.last_updated_by =g_user
         where 1 =1
               and a.bank_name =p_bank_name
               and a.file_name =l_file_name
               and a.status_code = 'STAGE-4'
               and a.file_imported_flag ='Y'
               and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                 )
         ;
         --
         print_log('@xxcus.xxcus_ce_current_day_files_b: updated status code to STAGE-5 for bank '||p_bank_name||', total record :'||sql%rowcount);
         print_log(' ');
         --
      exception
         when others then
            print_log('@xxcus.xxcus_ce_current_day_files_b, Issue in update of status code From STAGE-4 to STAGE-5 for bank :'||p_bank_name||', file :'||l_file_name||', error =>'||sqlerrm);
            rollback to sqr1;
            raise program_error;
      end;
     --
    end if; --endif (b_move_fwd) then -- @101, no issues in deleting previous xxcus.xxcus_ce_stmt_hdr_int records for the bank
    --
exception
   when others then
    print_log('outer block of BANK OF AMERICA US BAI2 post processing, error message :'||sqlerrm);
    raise program_error;
end BOFA_US;
   -- ^^^^^^^
 procedure BOFA_CA
   (
     retcode out varchar2
    ,errbuf  out varchar2
    ,p_bank_name in varchar2
   ) is
  -- =================================================================================
  -- local variables
  -- =================================================================================
  l_input_file_id   UTL_FILE.FILE_TYPE;
  l_line_read       VARCHAR2(2000):= NULL;
  l_line_count      NUMBER :=0;
  l_file_name  varchar2(150) :=Null;
  l_lines_processed NUMBER :=0;
  l_org_id NUMBER :=163;
  --
  l_print_count     NUMBER :=0;
  b_move_fwd      BOOLEAN;
  v_unprocessed_flag VARCHAR2(1) :='Z';
  --
  o_return_status           VARCHAR2 (2000);
  o_msg_count                 NUMBER;
  o_msg_data                   VARCHAR2 (2000);
  --
  v_api_message             VARCHAR2 (4000);
  n_count                           NUMBER :=0;
  n_seq                               NUMBER :=0;
  --
 l_field_delimit_count number :=0;
 l_last_field_delimiter_posn number :=0;
 n_loc varchar2(3) :=Null;
  --
  l_inbound_loc VARCHAR2 (150) :=Null;
  l_outbound_loc  VARCHAR2 (150) :=Null;
  --
  type t_max_lines_type is record
   (
    max_lines number
   );
  type t_max_lines_tbl is table of t_max_lines_type index by binary_integer;
  l_line_loop t_max_lines_tbl;
  --
  type t_num_type is record
   (
    current_line number,
    next_03_line_num number
   );
  type t_num_tbl is table of t_num_type index by binary_integer;
  t_num_rec t_num_tbl;
  --
   cursor c_bank_new_files is
    select A1.*
    from  xxcus.xxcus_ce_current_day_files_b A1
    where 1 =1
         and A1.bank_name =p_bank_name
         and A1.status_code ='STAGE-4'
         and A1.file_imported_flag ='Y'
         and trunc(A1.creation_date) =trunc(sysdate)
         and A1.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
         and A1.creation_date =
                 (
                    select max(A2.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b A2
                    where 1 =1
                         and  A2.bank_name =A1.bank_name
                         and  A2.file_name =A1.file_name
                         and  A2.status_code =A1.status_code
                         and  A2.file_imported_flag =A1.file_imported_flag
                         and  A2.request_id =A1.request_id
                 )
          ;
   --
   TYPE l_bank_new_type IS table of c_bank_new_files%rowtype index by binary_integer;
   l_bank_new_rec l_bank_new_type;
  --
  cursor get_next_03_line is
  select to_number(xmltbl.column_value) current_line
             ,nvl(lead(to_number(xmltbl.column_value), 1) over(order by to_number(xmltbl.column_value)),0) next_03_line_num
  from xmltable(g_all_03_lines) xmltbl;
  --
  cursor c_bank_rec_id ( l_rec_id_no IN VARCHAR2 ) is
  select
              b.file_name        file_name
             ,b.status_code   status_code
             ,c.rec_id_no        rec_id_no
             ,c.rec_no              rec_no
             ,b.file_record      file_record
             ,'N'                         extract_flag
             ,0                            lines_to_scan
             ,0                            next_03_line_num
             ,'N'                         last_rec
  from xxcus.xxcus_ce_current_day_files_b a
           ,xxcus.xxcus_ce_current_day_records_b b
           ,xxcus.xxcus_ce_stmt_int_tmp2_BOA_CA c
  where 1 =1
       and b.bank_name =g_bank_name
       and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and c.rec_id_no =l_rec_id_no
       and trim(b.file_record) is not null
       and a.bank_name =b.bank_name
       and c.bank_name =a.bank_name
       and a.file_name =b.file_name
       and c.file_name =a.file_name
       and b.status_code ='STAGE-4'
       and a.status_code =b.status_code
       and c.status_code =a.status_code
       and trunc(b.creation_date) =trunc(sysdate)
       and trunc(a.creation_date) =trunc(b.creation_date)
       and trunc(c.creation_date) =trunc(c.creation_date)
       and a.file_imported_flag ='Y'
       and c.file_imported_flag =a.file_imported_flag
       and b.line_number =c.rec_no
       and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                         and  d.request_id =a.request_id
                 )
order by b.line_number asc;
  --
  type l_bank_rec_type is table of c_bank_rec_id%rowtype index by binary_integer;
  l_bank_single_rec l_bank_rec_type;
  --
  cursor c_bank_files is
  select b.file_name        file_name
             ,b.status_code   status_code
             ,b.line_number  line_number
             ,c.rec_id_no        rec_id_no
             ,c.rec_no              rec_no
             ,b.file_record      file_record
             ,'N'                         extract_flag
  from xxcus.xxcus_ce_current_day_files_b a
           ,xxcus.xxcus_ce_current_day_records_b b
           ,xxcus.xxcus_ce_stmt_int_tmp2_BOA_CA c
  where 1 =1
       and b.bank_name =g_bank_name
       and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and trim(b.file_record) is not null
       and a.bank_name =b.bank_name
       and c.bank_name =a.bank_name
       and a.file_name =b.file_name
       and c.file_name =a.file_name
       and b.status_code ='STAGE-4'
       and a.status_code =b.status_code
       and c.status_code =a.status_code
       and trunc(b.creation_date) =trunc(sysdate)
       and trunc(a.creation_date) =trunc(b.creation_date)
       and trunc(c.creation_date) =trunc(c.creation_date)
       and a.file_imported_flag ='Y'
       and c.file_imported_flag =a.file_imported_flag
       and b.line_number =c.rec_no
       and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                         and  d.request_id =a.request_id
                 )
  order by b.line_number asc
   ;
  --
  type l_bank_tbl is table of c_bank_files%rowtype index by binary_integer;
  l_bank_all_rec l_bank_tbl;
  --
  -- begin declaration of cursor c_extract_03_info
  cursor c_extract_03_info (l_rec_no in number) is
    select
         e.bank_name bank_name
        ,e.bank_branch_name bank_branch_name
        ,d.bank_account_name
        ,c.column1 bank_account_num
        ,trim(replace(c.column2, '/', '')) currency_code
        ,to_char(null) item_code
        ,to_number(null) item_amount
        ,to_number(null) item_count
        ,to_char(null) funds_type
        ,c.rec_id_no rec_id_no
        ,c.rec_no  rec_no
        ,c.file_name current_day_file_name
        ,g_sender_id sender_id
        ,g_receiver_id receiver_id
        ,to_date((g_file_creation_date||g_file_creation_time),'YYMMDDHH24MI') bank_file_create_date
        ,g_user created_by
        ,sysdate creation_date
        ,g_user last_updated_by
        ,sysdate last_update_date
        ,g_org_id org_id
        ,g_bank_name job_parameter
    from xxcus.xxcus_ce_current_day_files_b a
           ,xxcus.xxcus_ce_current_day_records_b b
           ,xxcus.xxcus_ce_stmt_int_tmp2_BOA_CA c
           ,apps.ce_bank_accounts  d
           ,apps.ce_bank_branches_v e
    where 1 =1
       and b.bank_name =g_bank_name
       and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and c.rec_id_no =g_rec_id_03
       and c.rec_no =l_rec_no
       and trim(b.file_record) is not null
       and trim(replace(c.column1, '/', '')) is not null --atleast bank account number is expected for a summary line.
       and a.bank_name =b.bank_name
       and c.bank_name =a.bank_name
       and a.file_name =b.file_name
       and c.file_name =a.file_name
       and b.status_code ='STAGE-4'
       and a.status_code =b.status_code
       and c.status_code =a.status_code
       and trunc(b.creation_date) =trunc(sysdate)
       and trunc(a.creation_date) =trunc(b.creation_date)
       and trunc(c.creation_date) =trunc(c.creation_date)
       and a.file_imported_flag ='Y'
       and c.file_imported_flag =a.file_imported_flag
       and b.line_number =c.rec_no
       and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                         and  d.request_id =a.request_id
                 )
     and d.attribute_category=g_cash_posn_attr_cat
     and d.attribute2 =g_cash_posn_attribute2
     and d.bank_account_num =c.column1
     and e.bank_party_id =d.bank_id
     and e.branch_party_id =d.bank_branch_id
     ;
  --end of cursor c_extract_03_info definition
  --
  type c_03_tbl is table of c_extract_03_info%rowtype index by binary_integer;
  c_03_rec c_03_tbl;
  --
  -- begin declaration of cursor c_extract_88_info
  cursor c_extract_88_info
        (
                 p_bank_name in varchar2
                ,p_bank_br_name in varchar2
                ,p_bank_acct_name in varchar2
                ,p_bank_acct_num in varchar2
                ,p_bank_file_create_date in date
                ,p_currency_code in varchar2
                ,p_last_rec in varchar2
                ,p_from_line in number
                ,p_to_line in number
        ) is
    select
         p_bank_name bank_name
        ,p_bank_br_name bank_branch_name
        ,p_bank_acct_name bank_account_name
        ,p_bank_acct_num bank_account_num
        ,p_currency_code currency_code
        ,trim(replace(c.column1, '/', ''))  item_code
        ,case
           when trim(replace(c.column2, '/', '')) is not null then (to_number(replace(c.column2, '/', ''))/100)
           else to_number(null)
         end  item_amount
        ,case
           when trim(replace(c.column3, '/', '')) is not null then (to_number(replace(c.column3, '/', '')))
           else to_number(null)
         end  item_count
        ,trim(replace(c.column4, '/', '')) funds_type
        ,c.rec_id_no rec_id_no
        ,c.rec_no  rec_no
        ,c.file_name current_day_file_name
        ,g_sender_id sender_id
        ,g_receiver_id receiver_id
        ,p_bank_file_create_date bank_file_create_date
        ,g_user created_by
        ,sysdate creation_date
        ,g_user last_updated_by
        ,sysdate last_update_date
        ,g_org_id org_id
        ,g_bank_name job_parameter
    from xxcus.xxcus_ce_current_day_files_b a
           ,xxcus.xxcus_ce_current_day_records_b b
           ,xxcus.xxcus_ce_stmt_int_tmp2_BOA_CA c
    where 1 =1
       and b.bank_name =g_bank_name
       and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and c.rec_id_no =g_rec_id_88
       and (
                  (p_last_rec ='N' and (c.rec_no between p_from_line and p_to_line and c.rec_id_no NOT IN (g_rec_id_49, g_rec_id_16, g_rec_id_02) ))
                OR
                  (p_last_rec ='Y' and c.rec_no >=p_from_line and c.rec_id_no NOT IN (g_rec_id_16, g_rec_id_49, g_rec_id_98, g_rec_id_99, g_rec_id_02) )
               )
       and trim(b.file_record) is not null
       and (trim(replace(c.column1, '/', '')) is not null and trim(replace(c.column1, '/', '')) not like '%CUR%')
       and a.bank_name =b.bank_name
       and c.bank_name =a.bank_name
       and a.file_name =b.file_name
       and c.file_name =a.file_name
       and b.status_code ='STAGE-4'
       and a.status_code =b.status_code
       and c.status_code =a.status_code
       and trunc(b.creation_date) =trunc(sysdate)
       and trunc(a.creation_date) =trunc(b.creation_date)
       and trunc(c.creation_date) =trunc(c.creation_date)
       and a.file_imported_flag ='Y'
       and c.file_imported_flag =a.file_imported_flag
       and b.line_number =c.rec_no
       and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                         and  d.request_id =a.request_id
                 )
      order by c.rec_no asc
     ;
  --end of cursor c_extract_88_info definition
  --
  type c_88_tbl is table of c_extract_88_info%rowtype index by binary_integer;
  c_88_rec c_88_tbl;
  --
  -- begin declaration of cursor c_extract_16_info
  cursor c_extract_16_info
        (
                 p_bank_name in varchar2
                ,p_bank_br_name in varchar2
                ,p_bank_acct_name in varchar2
                ,p_bank_acct_num in varchar2
                ,p_bank_file_create_date in date
                ,p_currency_code in varchar2
                ,p_last_rec in varchar2
                ,p_from_line in number
                ,p_to_line in number
        ) is
    select
         p_bank_name bank_name
        ,p_bank_br_name bank_branch_name
        ,p_bank_acct_name bank_account_name
        ,p_bank_acct_num bank_account_num
        ,p_currency_code currency_code
        ,trim(replace(c.column1, '/', ''))  item_code
        ,case
           when trim(replace(c.column2, '/', '')) is not null then (to_number(replace(c.column2, '/', ''))/100)
           else to_number(null)
         end  item_amount
        ,to_number(null) item_count
        ,case
           when trim(replace(c.column3, '/', '')) is not null then (trim(replace(c.column3, '/', '')))
           else null
         end  funds_type
        ,c.rec_id_no rec_id_no
        ,c.rec_no  rec_no
        ,c.file_name current_day_file_name
        ,g_sender_id sender_id
        ,g_receiver_id receiver_id
        ,p_bank_file_create_date bank_file_create_date
        ,g_user created_by
        ,sysdate creation_date
        ,g_user last_updated_by
        ,sysdate last_update_date
        ,g_org_id org_id
        ,g_bank_name job_parameter
    from xxcus.xxcus_ce_current_day_files_b a
           ,xxcus.xxcus_ce_current_day_records_b b
           ,xxcus.xxcus_ce_stmt_int_tmp2_BOA_CA c
    where 1 =1
       and b.bank_name =g_bank_name
       and a.request_id =(select max(request_id) from xxcus.xxcus_ce_current_day_files_b where trunc(sysdate) =trunc(creation_date))
       and c.rec_id_no =g_rec_id_16
       and (
                  (p_last_rec ='N' and (c.rec_no between p_from_line and p_to_line and c.rec_id_no NOT IN (g_rec_id_49,g_rec_id_88, g_rec_id_02) ))
                OR
                  (p_last_rec ='Y' and c.rec_no >=p_from_line and c.rec_id_no NOT IN (g_rec_id_88, g_rec_id_49,g_rec_id_98, g_rec_id_99, g_rec_id_02) )
               )
       and trim(b.file_record) is not null
       and trim(replace(c.column1, '/', '')) is not null
       and a.bank_name =b.bank_name
       and c.bank_name =a.bank_name
       and a.file_name =b.file_name
       and c.file_name =a.file_name
       and b.status_code ='STAGE-4'
       and a.status_code =b.status_code
       and c.status_code =a.status_code
       and trunc(b.creation_date) =trunc(sysdate)
       and trunc(a.creation_date) =trunc(b.creation_date)
       and trunc(c.creation_date) =trunc(c.creation_date)
       and a.file_imported_flag ='Y'
       and c.file_imported_flag =a.file_imported_flag
       and b.line_number =c.rec_no
       and a.creation_date =
                 (
                    select max(d.creation_date)
                    from  xxcus.xxcus_ce_current_day_files_b d
                    where 1 =1
                         and  d.bank_name =a.bank_name
                         and  d.file_name =a.file_name
                         and  d.status_code =a.status_code
                         and  d.file_imported_flag =a.file_imported_flag
                         and  d.request_id =a.request_id
                 )
     order by c.rec_no asc
     ;
  --end of cursor c_extract_88_info definition
  --
  type c_16_tbl is table of c_extract_16_info%rowtype index by binary_integer;
  c_16_rec c_16_tbl;
  --
  --
begin -- Main Processing...
    --
    g_org_id :=nvl(mo_global.get_current_org_id, 163);
    --
    g_bank_name :=p_bank_name;
    --
    l_inbound_loc :=get_location ( v_operation =>'DBA_DIRECTORY', v_type  =>'INBOUND');
    --
    l_outbound_loc :=get_location ( v_operation =>'DBA_DIRECTORY', v_type  =>'OUTBOUND');
    --
    begin
         --
          delete  xxcus.xxcus_ce_stmt_hdr_int where job_parameter =p_bank_name;
         --
    exception
       when others then
         --
        print_log('@ other errors, error in flushing out xxcus.xxcus_ce_stmt_hdr_int table records for previous run:  bank '||p_bank_name||', error =>'||sqlerrm);
        raise program_error;
    end;
    --
    begin
         --
         open c_bank_new_files;
         fetch c_bank_new_files bulk collect into l_bank_new_rec;
         close c_bank_new_files;
         --
         if l_bank_new_rec.count >0 then
             --
             b_move_fwd :=TRUE;
             --
           --
               for idx1 in l_bank_new_rec.first .. l_bank_new_rec.last loop
                    --
                     print_log(' Idx1 = '||idx1);
                     print_log('p_bank_name =>'||l_bank_new_rec(idx1).bank_name);
                     print_log('p_file_name =>'||l_bank_new_rec(idx1).file_name);
                     print_log('p_creation_date =>'||l_bank_new_rec(idx1).creation_date);
                     print_log('p_file_imported_flag =>'||l_bank_new_rec(idx1).file_imported_flag);
                     print_log('p_request_id =>'||l_bank_new_rec(idx1).request_id);
                     print_log('p_status_code =>'||l_bank_new_rec(idx1).status_code);
                     print_log('  ');
                    --
                       backup_stmt_int_tmp
                       (
                         p_bank_name=>l_bank_new_rec(idx1).bank_name
                        ,p_file_name =>l_bank_new_rec(idx1).file_name
                        ,p_creation_date =>l_bank_new_rec(idx1).creation_date
                        ,p_file_imported_flag =>l_bank_new_rec(idx1).file_imported_flag
                        ,p_request_id =>l_bank_new_rec(idx1).request_id
                        ,p_status_code =>l_bank_new_rec(idx1).status_code
                       );
                    --
               end loop;
           --
         else --l_bank_new_rec.count =0 then
             --
             b_move_fwd :=FALSE;
             --
         end if;
         --
    exception
       when no_data_found then
         --
         b_move_fwd :=FALSE;
         --
       when others then
         --
         b_move_fwd :=FALSE;
         --
        print_log('@ other errors, error in flushing out xxcus.xxcus_ce_stmt_hdr_int table records for previous run:  bank '||p_bank_name||', error =>'||sqlerrm);
        raise program_error;
    end;
    --
    if (b_move_fwd) then -- @101, no issues in deleting previous xxcus.xxcus_ce_stmt_hdr_int records for the bank
     --
     -- PLSQL table l_bank_rec will hold all lines from the bank file ordered by line#
     --
     open c_bank_files;
     fetch c_bank_files bulk collect into l_bank_all_rec;
     close c_bank_files;
     --
     -- @ rec_id_no equal to 01 - PLSQL table l_bank_single_rec will hold all lines from the bank file for rec id 01
     --
     open c_bank_rec_id ( l_rec_id_no =>g_rec_id_01 );
     fetch c_bank_rec_id bulk collect into l_bank_single_rec;
     close c_bank_rec_id;
     --
         if l_bank_single_rec.count >0 then
          --
          print_log(' ');
          print_log('File record :'||l_bank_single_rec(1).file_record);
          print_log(' ');
              --
          print_log(' ');
          print_log('Position of end of logical record :'||regexp_instr(l_bank_single_rec(1).file_record, '/'));
          print_log(' ');
          --
          l_field_delimit_count :=regexp_count(l_bank_single_rec(1).file_record, g_field_delimiter);
          --
          l_last_field_delimiter_posn :=instr(l_bank_single_rec(1).file_record, g_field_delimiter, -1);
          --
          print_log('Total field delimiter count @ record 01 : '||l_field_delimit_count);
          print_log(' ');
          print_log('Position of last field delimiter @ record 01 : '||instr(l_bank_single_rec(1).file_record, g_field_delimiter, -1));
          print_log(' ');
          --
          --
              for ID01_Idx in 1 .. l_field_delimit_count+1 loop
                 --
                   if ID01_Idx =1 then
                     Null;
                  elsif ID01_Idx =2 then
                       g_sender_id :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='001';
                   elsif ID01_Idx =3 then
                       g_receiver_id :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='002';
                   elsif ID01_Idx =4 then
                       g_file_creation_date :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='003';
                   elsif ID01_Idx =5 then
                       g_file_creation_time :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='004';
                   elsif ID01_Idx =6 then
                       g_file_id_number :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='005';
                   elsif ID01_Idx =7 then
                       g_physical_rec_length :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='006';
                   elsif ID01_Idx =8 then
                       g_block_size :=get_field(v_delimiter =>g_field_delimiter, n_field_no =>ID01_Idx, v_line_read=>l_bank_single_rec(1).file_record, p_which_line =>l_bank_single_rec(1).rec_no);
                       n_loc :='007';
                  else
                       g_version_number :=substr(l_bank_single_rec(1).file_record, l_last_field_delimiter_posn+1, (regexp_instr(l_bank_single_rec(1).file_record, '/')-1)-(l_last_field_delimiter_posn));
                       n_loc :='008';
                  end if;
                  --
              end loop;
              --
         end if;
     --
       print_log(' ');
       print_log('g_sender_id ='||g_sender_id);
       print_log('g_receiver_id ='||g_receiver_id);
       print_log('g_file_creation_date ='||g_file_creation_date);
       print_log('g_file_creation_time ='||g_file_creation_time);
       print_log('g_file_id_number ='||g_file_id_number);
       print_log('g_physical_rec_length ='||g_physical_rec_length);
       print_log('g_block_size ='||g_block_size);
       print_log('g_version_number ='||g_version_number);
       print_log(' ');
     --
     -- invoking procedure get_all_03_linenums will store all line numbers where rec id is 03 and in comma separated list of values
     --into the global variable g_all_03_lines;
     --
     begin
        get_all_03_linenums_boa_ca;
     exception
      when others then
       print_log ('@ get_all_03_linenums_boa_us, error :'||sqlerrm);
     end;
     --
     open get_next_03_line;
     fetch get_next_03_line bulk collect into t_num_rec;
     close get_next_03_line;
     --
     l_field_delimit_count :=regexp_count(g_all_03_lines, g_field_delimiter);
     --
     print_log('@get_all_03_linenums_boa_us, l_field_delimit_count ='||l_field_delimit_count);
     --
     -- @ rec_id_no equal to 03 - PLSQL table l_bank_single_rec will hold all lines from the bank file for rec id 03
     --
     open c_bank_rec_id ( l_rec_id_no =>g_rec_id_03 );
     fetch c_bank_rec_id bulk collect into l_bank_single_rec;
     close c_bank_rec_id;
         --
         -- get the number of lines to move from the current line and assign it to the plsql table l_bank_single_rec
         --
        if l_bank_single_rec.count >0 then
               --
               for idx in 1 .. l_bank_single_rec.count loop
                  --
                  if idx != l_bank_single_rec.last then --do this for all records exception last one
                        --
                         l_bank_single_rec(idx).lines_to_scan
                           :=
                            get_03lines_excl_499899_boa_us
                              (
                                 p_from_line =>t_num_rec(idx).current_line
                                ,p_to_line =>t_num_rec(idx).next_03_line_num
                                ,p_last_rec =>'N'
                              );
                         --
                         l_bank_single_rec(idx).next_03_line_num :=t_num_rec(idx).next_03_line_num;
                         --
                         l_bank_single_rec(idx).last_rec :='N';
                         --
                   --
                  else
                           -- when last record we will get the number of lines to scan using the below formula
                           --
                           l_bank_single_rec(idx).lines_to_scan
                             :=
                               get_03lines_excl_499899_boa_us
                                (
                                 p_from_line =>t_num_rec(idx).current_line
                                ,p_to_line =>t_num_rec(idx).next_03_line_num
                                ,p_last_rec =>'Y'
                                );
                         --
                         l_bank_single_rec(idx).next_03_line_num :=t_num_rec(idx).next_03_line_num;
                         --
                         l_bank_single_rec(idx).last_rec :='Y';
                         --
                   --
                  end if;
                  --
                  print_log(
                                           'Rec# '||
                                           l_bank_single_rec(idx).rec_no||
                                           ', Lines to scan : '||
                                           l_bank_single_rec(idx).lines_to_scan||
                                           ', Next 03 Line# '||l_bank_single_rec(idx).next_03_line_num||
                                           ', Last Record ? : '||l_bank_single_rec(idx).last_rec
                                   );
                   --
               end loop;
               --
        end if;
        --
        --###
        --
          if l_bank_single_rec.count >0 then
                   --
               for idx in 1 .. l_bank_single_rec.count loop
                      --
                      l_file_name :=l_bank_single_rec(idx).file_name;
                      --
                      -- begin extracting 03 record and its bai codes
                      begin
                       --
                       open c_extract_03_info (l_rec_no =>l_bank_single_rec(idx).rec_no) ;
                       fetch c_extract_03_info bulk collect into c_03_rec;
                       close c_extract_03_info;
                       --
                           if c_03_rec.count >0 then
                                 --
                                 begin
                                      -- begin run thru the first record to capture the bank details from EBS cash management tables.
                                     for i in 1 .. 1 loop
                                      --
                                         g_ce_bank_name  :=c_03_rec(i).bank_name;
                                         g_ce_bank_br_name  :=c_03_rec(i).bank_branch_name;
                                         g_bank_account_name  :=c_03_rec(i).bank_account_name;
                                         g_bank_account_num  :=c_03_rec(i).bank_account_num;
                                         g_bank_file_create_date  :=c_03_rec(i).bank_file_create_date;
                                         g_currency :=c_03_rec(i).currency_code;
                                      --
                                         g_proceed :=TRUE;
                                      -- @@@@@@
                                            --
                                             if (g_proceed) then
                                               -- begin insert of 88 records
                                                   begin
                                                     --
                                                       open c_extract_88_info
                                                                            (
                                                                                     p_bank_name=>g_ce_bank_name
                                                                                    ,p_bank_br_name =>g_ce_bank_br_name
                                                                                    ,p_bank_acct_name =>g_bank_account_name
                                                                                    ,p_bank_acct_num=>g_bank_account_num
                                                                                    ,p_bank_file_create_date =>g_bank_file_create_date
                                                                                    ,p_currency_code =>g_currency
                                                                                    ,p_last_rec =>l_bank_single_rec(idx).last_rec
                                                                                    ,p_from_line =>l_bank_single_rec(idx).rec_no
                                                                                    ,p_to_line =>l_bank_single_rec(idx).next_03_line_num
                                                                            );
                                                        fetch c_extract_88_info bulk collect into c_88_rec;
                                                        close  c_extract_88_info;
                                                     --
                                                          if (c_88_rec.count >0) then
                                                           --
                                                               begin
                                                                --
                                                                forall idx in 1 .. c_88_rec.count
                                                                  insert into xxcus.xxcus_ce_stmt_hdr_int values c_88_rec(idx) ;
                                                                --
                                                               exception
                                                                when others then
                                                                 print_log('Error in insert of 88 rec for line# '||l_bank_single_rec(idx).rec_no||', message ='||sqlerrm);
                                                               end;
                                                           --
                                                          end if;
                                                     --
                                                   exception
                                                    when others then
                                                     print_log('Error in fetching 88 records for line# '||l_bank_single_rec(idx).rec_no||', msg ='||sqlerrm);
                                                     raise program_error;
                                                   end;
                                               -- end insert of 88 records
                                               -- begin insert of 16 records
                                               --
                                                   begin
                                                     --
                                                       open c_extract_16_info
                                                                            (
                                                                                     p_bank_name=>g_ce_bank_name
                                                                                    ,p_bank_br_name =>g_ce_bank_br_name
                                                                                    ,p_bank_acct_name =>g_bank_account_name
                                                                                    ,p_bank_acct_num=>g_bank_account_num
                                                                                    ,p_bank_file_create_date =>g_bank_file_create_date
                                                                                    ,p_currency_code =>g_currency
                                                                                    ,p_last_rec =>l_bank_single_rec(idx).last_rec
                                                                                    ,p_from_line =>l_bank_single_rec(idx).rec_no
                                                                                    ,p_to_line =>l_bank_single_rec(idx).next_03_line_num
                                                                            );
                                                        fetch c_extract_16_info bulk collect into c_16_rec;
                                                        close  c_extract_16_info;
                                                     --
                                                          if (c_16_rec.count >0) then
                                                           --
                                                               begin
                                                                --
                                                                forall idx in 1 .. c_16_rec.count
                                                                  insert into xxcus.xxcus_ce_stmt_hdr_int values c_16_rec(idx) ;
                                                                --
                                                               exception
                                                                when others then
                                                                 print_log('Error in insert of 16 rec for line# '||l_bank_single_rec(idx).rec_no||', message ='||sqlerrm);
                                                               end;
                                                           --
                                                          end if;
                                                     --
                                                   exception
                                                    when others then
                                                     print_log (' ');
                                                     print_log('Error in fetching 16 records for line# '||l_bank_single_rec(idx).rec_no||', msg ='||sqlerrm);
                                                     print_log (' ');
                                                     print_log (' Current Parameters ');
                                                     print_log (' p_bank_name=>'||g_ce_bank_name);
                                                     print_log (' p_bank_br_name=>'||g_ce_bank_br_name);
                                                     print_log (' p_bank_acct_name=>'||g_bank_account_name);
                                                     print_log (' p_bank_acct_num=>'||g_bank_account_num);
                                                     print_log (' p_bank_file_create_date=>'||g_bank_file_create_date);
                                                     print_log (' p_last_rec=>'||l_bank_single_rec(idx).last_rec);
                                                     print_log (' p_from_line=>'||l_bank_single_rec(idx).rec_no);
                                                     print_log (' p_to_line=>'||l_bank_single_rec(idx).next_03_line_num);
                                                     raise program_error;
                                                   end;
                                               --
                                               -- end insert of 16 records
                                             end if;
                                         --
                                      -- @@@@@@
                                     end loop;
                                      --
                                 exception
                                  when others then
                                   print_log('@ rec_id_no 16, get first record of rec_id_no 03 to assign global parameters, message : '||sqlerrm);
                                   raise program_error;
                                 end;
                                 --
                           end if;
                       --
                      exception
                       when no_data_found then
                        print_log('No records to fetch for rec_id_no 03 and line#  '||l_bank_single_rec(idx).rec_no||', msg ='||sqlerrm);
                        raise program_error;
                       when others then
                        print_log(' '||sqlerrm);
                        raise program_error;
                      end;
                      -- end extracting 03 record and its bai codes
                      --
               end loop; --end loop for for idx in 1 .. l_bank_single_rec.count
                   --
          end if; -- if l_bank_single_rec.count >0 then
         --
             --
              begin
               savepoint sqr2;
               update xxcus.xxcus_ce_current_day_records_b
                   set status_code ='STAGE-5'
                         ,last_update_date =sysdate
                         ,last_updated_by =g_user
                 where 1 =1
                       and bank_name =p_bank_name
                       and file_name =l_file_name
                       and status_code = 'STAGE-4'
                       and trunc(creation_date) =trunc(sysdate)
                 ;
                 --
              exception
                 when others then
                    print_log('@xxcus.xxcus_ce_current_day_records_b, Issue in update of status code From STAGE-4 to STAGE-5 for bank :'||p_bank_name||', file :'||l_file_name||', error =>'||sqlerrm);
                    rollback to sqr2;
                    raise program_error;
              end;
             --
              begin
               --
               print_log(' ');
               print_log('l_file_name ='||l_file_name);
               print_log('p_bank_name ='||p_bank_name);
               print_log('g_bai_codes_complete ='||g_bai_codes_complete);
               print_log(' ');
               --
               savepoint sqr1;
               --
               update  xxcus.xxcus_ce_current_day_files_b a
                   set a.status_code ='STAGE-5'
                         ,a.comments =g_bai_codes_complete
                         ,a.last_update_date =sysdate
                         ,a.last_updated_by =g_user
                 where 1 =1
                       and a.bank_name =p_bank_name
                       and a.file_name =l_file_name
                       and a.status_code = 'STAGE-4'
                       and a.file_imported_flag ='Y'
                       and a.creation_date =
                         (
                            select max(d.creation_date)
                            from  xxcus.xxcus_ce_current_day_files_b d
                            where 1 =1
                                 and  d.bank_name =a.bank_name
                                 and  d.file_name =a.file_name
                                 and  d.status_code =a.status_code
                                 and  d.file_imported_flag =a.file_imported_flag
                         )
                 ;
                 --
                 print_log('@xxcus.xxcus_ce_current_day_files_b: updated status code to STAGE-5 for bank '||p_bank_name||', total record :'||sql%rowcount);
                 print_log(' ');
                 --
              exception
                 when others then
                    print_log('@xxcus.xxcus_ce_current_day_files_b, Issue in update of status code From STAGE-4 to STAGE-5 for bank :'||p_bank_name||', file :'||l_file_name||', error =>'||sqlerrm);
                    rollback to sqr1;
                    raise program_error;
              end;
             --
    end if; --endif (b_move_fwd) then -- @101, no issues in deleting previous xxcus.xxcus_ce_stmt_hdr_int records for the bank
    --
exception
   when others then
    print_log('outer block of BANK OF AMERICA CANADA BAI2 post processing, error message :'||sqlerrm);
    raise program_error;
end BOFA_CA;
   -- ^^^^^^^
 procedure create_files
   (
     retcode out varchar2
    ,errbuf  out varchar2
   ) is
  -- =================================================================================
  -- local variables
  -- =================================================================================
  l_output_file_id   UTL_FILE.FILE_TYPE;
  l_line_read       VARCHAR2(2000):= NULL;
  l_line_count      NUMBER :=0;
  l_lines_processed NUMBER :=0;
  l_org_id NUMBER :=163;
  l_result VARCHAR2(2000) :=Null;
  --
  l_print_count     NUMBER :=0;
  b_move_fwd      BOOLEAN;
  v_unprocessed_flag VARCHAR2(1) :='Z';
  --
  o_return_status           VARCHAR2 (2000);
  o_msg_count                 NUMBER;
  o_msg_data                   VARCHAR2 (2000);
  --
  v_api_message             VARCHAR2 (4000);
  n_count                           NUMBER :=0;
  n_seq                               NUMBER :=0;
  --
  l_inbound_loc VARCHAR2 (150) :=Null;
  l_outbound_loc  VARCHAR2 (150) :=Null;
  --
  l_codes_hdr varchar2(2000) :=Null;
  --
  cursor c_all_codes_hdr is
    select
     'BANK_NAME,BANK_ACCOUNT_NAME,BANK_ACCOUNT_NUM'
     ||',CURRENCY_CODE,CURRENT_DAY_FILE_NAME,BANK_FILE_CREATE_DATE,BAI_ITEM_CODE'
     ||',BAI_ITEM_NAME,BAI_ITEM_AMOUNT ,BAI_REC_ID_NO,BAI_REC_LINE_NUM' all_codes_header
    from dual;
  --
  cursor c_error_codes_hdr is
    select
    'BANK_NAME,BANK_ACCOUNT_NAME,BANK_ACCOUNT_NUM,'||
    'CURRENCY_CODE,BAI_CODE_CHECK,BANK_FILE_CREATE_DATE,CURRENT_DAY_FILE_NAME' ALL_CODES_ERROR
    from dual;
  --
  cursor all_files is
    select   distinct current_day_file_name file_name
    from    apps.xxcus_ce_curr_day_all_codes_v
    where 1 =1
  ;
  --
  cursor c_all_codes is
  select
        trim(regexp_replace(bank_name,'([^[:print:]])',' '))||','||
      trim(regexp_replace(bank_account_name,'([^[:print:]])',' '))||','||
       '"'||trim(regexp_replace(bank_account_num,'([^[:print:]])',' '))||'"'||','||
       trim(regexp_replace(currency_code,'([^[:print:]])',' '))||','||
       trim(regexp_replace(current_day_file_name,'([^[:print:]])',' '))||','||
       to_char(bank_file_create_date, 'MM-DD-YYYY HH24:MI:SS')||','||'"' ||
       trim(regexp_replace(item_code,'([^[:print:]])',' '))||'"'||','||
       trim(regexp_replace(item_description,'([^[:print:]])',' ')) ||','||
       trim(regexp_replace(item_amount,'([^[:print:]])',' '))||','||
       trim(regexp_replace(rec_id_no,'([^[:print:]])',' '))||','||
       trim(regexp_replace(rec_no,'([^[:print:]])',' ')) all_code_rec
  from apps.xxcus_ce_curr_day_all_codes_v
  where 1 =1
      order by bank_name asc, bank_account_num asc, rec_no asc
   ;
  --
  cursor c_reqd_codes is
  select
        trim(regexp_replace(bank_name,'([^[:print:]])',' '))||','||
      trim(regexp_replace(bank_account_name,'([^[:print:]])',' '))||','||
       '"'||trim(regexp_replace(bank_account_num,'([^[:print:]])',' '))||'"'||','||
       trim(regexp_replace(currency_code,'([^[:print:]])',' '))||','||
       trim(regexp_replace(current_day_file_name,'([^[:print:]])',' '))||','||
       to_char(bank_file_create_date, 'MM-DD-YYYY HH24:MI:SS')||','||'"'||
       trim(regexp_replace(item_code,'([^[:print:]])',' '))||'"'||','||
       trim(regexp_replace(item_description,'([^[:print:]])',' ')) ||','||
       trim(regexp_replace(item_amount,'([^[:print:]])',' '))||','||
       trim(regexp_replace(rec_id_no,'([^[:print:]])',' '))||','||
       trim(regexp_replace(rec_no,'([^[:print:]])',' ')) reqd_code_rec
  from apps.xxcus_ce_curr_day_reqd_codes_v
  where 1 =1
      order by bank_name asc, bank_account_num asc, rec_no asc
  ;
  --
  cursor all_error_codes is
    select
          trim(regexp_replace(bank_name,'([^[:print:]])',' '))||','||
          trim(regexp_replace(bank_account_name,'([^[:print:]])',' '))||','||
          '"'||trim(regexp_replace(bank_account_num,'([^[:print:]])',' '))||'"'||','||
          trim(regexp_replace(currency_code,'([^[:print:]])',' '))||','||
          trim(regexp_replace(bai_code_check,'([^[:print:]])',' '))||','||
       to_char(bank_file_create_date, 'MM-DD-YYYY HH24:MI:SS')||','||
          trim(regexp_replace(current_day_file_name,'([^[:print:]])',' ')) all_error_rec
    from   apps.xxcus_ce_curr_day_errors_v
    where 1 =1;
  --
begin
    --
    l_outbound_loc :=get_location ( v_operation =>'DBA_DIRECTORY', v_type  =>'OUTBOUND');
    -- begin all codes csv extract
    begin
       --
       g_out_file_prefix :='HDS_CURRDAY_ALL_CODES_';
       --
        g_out_file_name_temp :=g_out_file_prefix||TO_CHAR(SYSDATE, 'MMDDYY')||g_out_file_ext;
       --
        print_log ('@ File '||g_out_file_name_temp||' @ Write..., before get file handle.');
        --
        l_output_file_id  := utl_file.fopen(l_outbound_loc, g_out_file_name_temp, 'w');
        --
        print_log ('@ File '||g_out_file_name_temp||' @Write..., after get file handle.');
        --
        g_all_codes_file :=g_out_file_name_temp;
        --
        begin
         open c_all_codes_hdr;
         fetch c_all_codes_hdr into l_codes_hdr;
         close c_all_codes_hdr;
        end;
        --
        utl_file.put_line(l_output_file_id, l_codes_hdr);
        --
       for rec in c_all_codes loop
           --
           l_line_count :=0;
           --
           begin
                    --
                       begin
                          --
                          utl_file.put_line(l_output_file_id, rec.all_code_rec);
                          --
                          l_line_count:= l_line_count+1; --number of lines written
                          --
                       exception
                             when no_data_found then
                               exit;
                             when others then
                               print_log('Line# '||l_line_count||', Record: '|| rec.all_code_rec);
                               print_log('@ Inside file loop operation, error : '||sqlerrm);
                               exit;
                       end;
                  --
           exception
                 when utl_file.invalid_path then
                  raise_application_error(-20010,'File: '||g_out_file_name_temp||' Invalid Path: '||sqlerrm);
                 when utl_file.invalid_mode then
                  raise_application_error(-20010,'File: '||g_out_file_name_temp||' Invalid Mode: '||sqlerrm);
                 when utl_file.invalid_operation then
                  raise_application_error(-20010,'File: '||g_out_file_name_temp||' Invalid Operation: '||sqlerrm);
                 when utl_file.invalid_filehandle then
                  raise_application_error(-20010,'File: '||g_out_file_name_temp||' Invalid File Handle: '||sqlerrm);
                 when utl_file.read_error then
                  raise_application_error(-20010,'File: '||g_out_file_name_temp||' File Read Error: '||sqlerrm);
                 when utl_file.internal_error then
                  raise_application_error(-20010,'File: '||g_out_file_name_temp||' File Internal Error: '||sqlerrm);
                 when others then
                   print_log ('Error in caller apps.xxcus_currentday_cash_pkg.create_files, file : =>'||rec.all_code_rec);
                   print_log ('Error in caller apps.xxcus_currentday_cash_pkg.create_files, error =>'||sqlerrm);
           end;
          --
       end loop;
       --
                    utl_file.fclose(l_output_file_id);
                    print_log ('After file close.');
       --
    exception
       when no_data_found then
        print_log('@ no data found, no all bank code records found.');
        raise program_error;
       when others then
        print_log('@ other errors,  no all bank code records found, error :'||sqlerrm);
        raise program_error;
    end;
    -- end all codes csv extract
    -- begin required codes csv extract
    --
    l_outbound_loc :=get_location ( v_operation =>'DBA_DIRECTORY', v_type  =>'OUTBOUND');
    --
    begin
       --
       g_out_file_prefix :='HDS_CURRDAY_REQD_CODES_';
       --
        g_out_file_name_temp :=g_out_file_prefix||TO_CHAR(SYSDATE, 'MMDDYY')||g_out_file_ext;
       --
        print_log ('@ File '||g_out_file_name_temp||' @ Write..., before get file handle.');
        --
        l_output_file_id  := utl_file.fopen(l_outbound_loc, g_out_file_name_temp, 'w');
        --
        print_log ('@ File '||g_out_file_name_temp||' @Write..., after get file handle.');
        --
        g_reqd_codes_file :=g_out_file_name_temp;
        --
        begin
         open c_all_codes_hdr;
         fetch c_all_codes_hdr into l_codes_hdr;
         close c_all_codes_hdr;
        end;
        --
        utl_file.put_line(l_output_file_id, l_codes_hdr);
        --
       for rec in c_reqd_codes loop
           --
           l_line_count :=0;
           --
           begin
                    --
                       begin
                          --
                          utl_file.put_line(l_output_file_id, rec.reqd_code_rec);
                          --
                          l_line_count:= l_line_count+1; --number of lines written
                          --
                       exception
                             when no_data_found then
                               exit;
                             when others then
                               print_log('Line# '||l_line_count||', Record: '|| rec.reqd_code_rec);
                               print_log('@ Inside file loop operation, error : '||sqlerrm);
                               exit;
                       end;
                  --
           exception
                 when utl_file.invalid_path then
                  raise_application_error(-20010,'File: '||g_out_file_name_temp||' Invalid Path: '||sqlerrm);
                 when utl_file.invalid_mode then
                  raise_application_error(-20010,'File: '||g_out_file_name_temp||' Invalid Mode: '||sqlerrm);
                 when utl_file.invalid_operation then
                  raise_application_error(-20010,'File: '||g_out_file_name_temp||' Invalid Operation: '||sqlerrm);
                 when utl_file.invalid_filehandle then
                  raise_application_error(-20010,'File: '||g_out_file_name_temp||' Invalid File Handle: '||sqlerrm);
                 when utl_file.read_error then
                  raise_application_error(-20010,'File: '||g_out_file_name_temp||' File Read Error: '||sqlerrm);
                 when utl_file.internal_error then
                  raise_application_error(-20010,'File: '||g_out_file_name_temp||' File Internal Error: '||sqlerrm);
                 when others then
                   print_log ('Error in caller apps.xxcus_currentday_cash_pkg.create_files, file : =>'||rec.reqd_code_rec);
                   print_log ('Error in caller apps.xxcus_currentday_cash_pkg.create_files, error =>'||sqlerrm);
           end;
          --
       end loop;
       --
                    utl_file.fclose(l_output_file_id);
                    print_log ('After file close.');
       --
    exception
       when no_data_found then
        print_log('@ no data found, no all bank code records found.');
        raise program_error;
       when others then
        print_log('@ other errors,  no all bank code records found, error :'||sqlerrm);
        raise program_error;
    end;
    --
    -- end required codes csv extract
    -- begin error records extract
    l_outbound_loc :=get_location ( v_operation =>'DBA_DIRECTORY', v_type  =>'OUTBOUND');
    --
    begin
       --
       g_out_file_prefix :='HDS_CURRDAY_EXCEPTIONS_';
       --
        g_out_file_name_temp :=g_out_file_prefix||TO_CHAR(SYSDATE, 'MMDDYY')||g_out_file_ext;
       --
        print_log ('@ File '||g_out_file_name_temp||' @ Write..., before get file handle.');
        --
        l_output_file_id  := utl_file.fopen(l_outbound_loc, g_out_file_name_temp, 'w');
        --
        print_log ('@ File '||g_out_file_name_temp||' @Write..., after get file handle.');
        --
        g_error_codes_file := g_out_file_name_temp;
        --
        begin
         open c_error_codes_hdr;
         fetch c_error_codes_hdr into l_codes_hdr;
         close c_error_codes_hdr;
        end;
        --
        utl_file.put_line(l_output_file_id, l_codes_hdr);
        --
       for rec in all_error_codes loop
           --
           l_line_count :=0;
           --
           begin
                    --
                       begin
                          --
                          utl_file.put_line(l_output_file_id, rec.all_error_rec);
                          --
                          l_line_count:= l_line_count+1; --number of lines written
                          --
                       exception
                             when no_data_found then
                               exit;
                             when others then
                               print_log('Line# '||l_line_count||', Record: '|| rec.all_error_rec);
                               print_log('@ Inside file loop operation, error : '||sqlerrm);
                               exit;
                       end;
                  --
           exception
                 when utl_file.invalid_path then
                  raise_application_error(-20010,'File: '||g_out_file_name_temp||' Invalid Path: '||sqlerrm);
                 when utl_file.invalid_mode then
                  raise_application_error(-20010,'File: '||g_out_file_name_temp||' Invalid Mode: '||sqlerrm);
                 when utl_file.invalid_operation then
                  raise_application_error(-20010,'File: '||g_out_file_name_temp||' Invalid Operation: '||sqlerrm);
                 when utl_file.invalid_filehandle then
                  raise_application_error(-20010,'File: '||g_out_file_name_temp||' Invalid File Handle: '||sqlerrm);
                 when utl_file.read_error then
                  raise_application_error(-20010,'File: '||g_out_file_name_temp||' File Read Error: '||sqlerrm);
                 when utl_file.internal_error then
                  raise_application_error(-20010,'File: '||g_out_file_name_temp||' File Internal Error: '||sqlerrm);
                 when others then
                   print_log ('Error in caller apps.xxcus_currentday_cash_pkg.create_files, file : =>'||rec.all_error_rec);
                   print_log ('Error in caller apps.xxcus_currentday_cash_pkg.create_files, error =>'||sqlerrm);
           end;
          --
       end loop;
       --
                    utl_file.fclose(l_output_file_id);
                    print_log ('After file close.');
       --
    exception
       when no_data_found then
        print_log('@ no data found, no all bank code records found.');
        raise program_error;
       when others then
        print_log('@ other errors,  no all bank code records found, error :'||sqlerrm);
        raise program_error;
    end;
    -- end error records extract
    -- begin create zip file with all the three csv files compressed
    --
    /* --TMS:   20161215-00113 / ESMS 501131
    l_outbound_loc :=get_location ( v_operation =>'ABSOLUTE_PATH', v_type  =>'OUTBOUND');
    --
   g_out_file_prefix :='TEMP_HDS_CURRDAY_BAI_RESULTS';
   --
   g_out_file_name_temp :=g_out_file_prefix||g_out_file_zip;
   --
   print_log ('@ Zip File '||g_out_file_name_temp);
   --
        begin
                    -- m option is used to remove the detail file after it was added to the archive
                    -- j option is used to strip of the folder name "/xx_iface/ebiz???/outbound copied to the zip file
                    --
                     g_command := 'zip -mj'
                                 ||' '
                                 ||l_outbound_loc
                                 ||'/'
                                 ||g_out_file_name_temp
                                 ||' '
                                 ||l_outbound_loc
                                 ||'/'
                                 ||g_all_codes_file --All codes file is included
                                 ||' '
                                 ||l_outbound_loc
                                 ||'/'
                                 ||g_reqd_codes_file --All required BAI codes file is included
                                 ||' '
                                 ||l_outbound_loc
                                 ||'/'
                                 ||g_error_codes_file --All Error record files are included
                                 ;
                     --
                     print_log('ZIP command:');
                     print_log('============');
                     print_log(g_command);
                     --
                     select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
                     into   l_result
                     from   dual;
                     print_log('v_result '||l_result);    --ver1.3
                     --
                     if (l_result is null) then
                       --
                        begin
                                    -- m option is used to remove the detail file after it was added to the archive
                                    -- j option is used to strip of the folder name "/xx_iface/ebiz???/outbound copied to the zip file
                                    --
                                     g_command := 'mv '
                                                 ||' '
                                                 ||l_outbound_loc
                                                 ||'/'
                                                 ||g_out_file_name_temp
                                                 ||' '
                                                 ||l_outbound_loc
                                                 ||'/'
                                                 ||substr(g_out_file_name_temp, 6)
                                                 ;
                                     --
                                     print_log('ZIP command:');
                                     print_log('============');
                                     print_log(g_command);
                                     --
                                     select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
                                     into   l_result
                                     from   dual;
                                     print_log('v_result '||l_result);    --ver1.3
                                     --
                                     g_command := 'chmod 777 '
                                                 ||l_outbound_loc
                                                 ||'/'
                                                 ||substr(g_out_file_name_temp, 6)
                                                 ;
                                     --
                                     print_log('ZIP command:');
                                     print_log('============');
                                     print_log(g_command);
                                     --
                                     select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
                                     into   l_result
                                     from   dual;
                                     print_log('v_result '||l_result);    --ver1.3
                        exception
                            when others then
                             print_log('Error in creating zip file with all three csv files combined, message =>'||sqlerrm);
                             raise program_error;
                        end;
                        --
                     end if;
                     --
        exception
            when others then
             print_log('Error in creating zip file with all three csv files combined, message =>'||sqlerrm);
             raise program_error;
        end;
   -- end create zip file with all the three csv files compressed
   */ --TMS:   20161215-00113 / ESMS 501131
   -- begin move bai files to inbound archive folder
   --
    l_inbound_loc :=get_location ( v_operation =>'ABSOLUTE_PATH', v_type  =>'INBOUND');
   --
    begin
       --
        for rec in all_files loop --10/25/2016
             --
            begin
                        --
                         g_command := 'mv '
                                     ||' '
                                     ||l_inbound_loc
                                     ||'/'
                                     ||rec.file_name
                                     ||' '
                                     ||l_inbound_loc||'/archive'
                                     ||'/'
                                     ||rec.file_name
                                     ;
                         --
                         print_log('Move command:');
                         print_log('============');
                         print_log(g_command);
                         --
                         select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
                         into   l_result
                         from   dual;
                         print_log('v_result '||l_result);
                         --

                         --
            exception
                when others then
                 print_log('Error in archiving BAI file: '||rec.file_name||', message =>'||sqlerrm);
                 raise program_error;
            end;
            --
        end loop; --10/25/2016
        --
    exception
     when no_data_found then
      null;
     when others then
      print_log('@ Archive of BAI files, Error :'||sqlerrm);
    end;
   -- end move bai files to inbound archive folder
exception
   when others then
    print_log('outer block of create_files, errror message :'||sqlerrm);
    raise program_error;
end create_files;
--
  procedure submit_request_set
    (
        retcode out varchar2
       ,errbuf  out varchar2
       ,p_to_email in varchar2
       ,p_cc_email in varchar2
       ,p_send_attachments_to in varchar2
    ) is
    --
    l_request_id number :=0;
    --
    l_current_db_name   VARCHAR2(20) :=Null;
    l_prod_db_name      VARCHAR2(20) := 'ebsprd';
    l_to_email VARCHAR2(150) :=Null;
    l_cc_email VARCHAR2(240) :=Null;
    l_send_attachments_to   VARCHAR2(240) :=Null;
    --
    l_zip_file varchar2(240) :=Null;
    l_db varchar2(10) :=Null; -- Ver 1.3
    --Begin TMS:   20161215-00113 / ESMS 501131
    --
    l_all_codes_file varchar2(240) :=Null;
    l_reqd_codes_file varchar2(240) :=Null;
    l_exceptions_file varchar2(240) :=Null;
    --
    --End TMS:   20161215-00113 / ESMS 501131
    l_outbound_loc varchar2(240) :=Null;
    --
    l_max_files_allowed number :=1;
    --
    l_bool         BOOLEAN;
    l_iso_language VARCHAR2(30);
    --
    l_iso_territory VARCHAR2(30);
    --
    l_err_msg  VARCHAR2(3000);
    l_err_code NUMBER;
    l_sec      VARCHAR2(4000);

    l_ok                  BOOLEAN := TRUE;
    l_success             NUMBER := 0;
    l_user_id             NUMBER :=1229; --GLINTERFACE
    l_responsibility_id   NUMBER := 50761;  --50761 for HDS Cash Management Master User or 50661 for XXCUS_CON
    --
    l_resp_application_id NUMBER := 260; --260 for Cash Management OR 20003 for XXCUS_CON
     lc_request_data VARCHAR2(20) :='';
    l_sysdate             DATE;
    l_end_date            VARCHAR2(30);
    --
    srs_failed        EXCEPTION;
    submitprog_failed EXCEPTION;
    submitset_failed  EXCEPTION;
    --
    l_err_callfrom  VARCHAR2(75) DEFAULT 'xxcus_currentday_cash_pkg.submit_request_set';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    --
  BEGIN
   --
       lc_request_data :=fnd_conc_global.request_data;
   --
    if lc_request_data is null then
       --
       lc_request_data :='1';
       --
               print_log(' ');
               print_log('p_to_email =>'||p_to_email);
               print_log('p_cc_email =>'||p_cc_email);
               print_log('p_send_attachments_to =>'||p_send_attachments_to);
               print_log(' ');
              --
                    begin
                         --
                         select lower(name) into l_current_db_name from v$database;
                         --
                         l_outbound_loc :=get_location ( v_operation =>'ABSOLUTE_PATH', v_type  =>'OUTBOUND');
                         --
                         l_zip_file :=l_outbound_loc||'/'||g_final_product_file;
                         --
                         if  ( l_current_db_name =l_prod_db_name ) then --for EBSPRD only
                            --
                             l_to_email :=p_to_email; --p_send_attachments_to
                            --
                             if (p_cc_email is null) then
                                  --
                                  l_cc_email :=g_notif_email_cc;
                                  --
                             else
                                  --
                                  l_cc_email :=p_cc_email;
                                  --
                             end if;
                             --
                            --
                             if (p_send_attachments_to is null) then
                                  --
                                  l_send_attachments_to :=g_send_email_attach_to;
                                  --
                             else
                                  --
                                  l_send_attachments_to :=p_send_attachments_to;
                                  --
                             end if;
                             --
                         else --for non PRODUCTION databases only
                              --
                                 if (p_to_email is null) then
                                      --
                                      l_to_email :=g_notif_email_cc;
                                      --
                                 else
                                      --
                                      l_to_email :=p_to_email;
                                      --
                                 end if;
                              --
                              --
                                 if (p_cc_email is null) then
                                      --
                                      l_cc_email :=g_notif_email_cc;
                                      --
                                 else
                                      --
                                      l_cc_email :=p_cc_email;
                                      --
                                 end if;
                                  --
                                  l_send_attachments_to :=p_to_email;
                                  --
                         end if;
                         --
                     --
                    exception
                     when others then
                      print_log ('Failed to determine which instance, message =>'||sqlerrm);
                      raise program_error;
                    end;
               --
               print_log(' ');
               print_log('l_to_email =>'||l_to_email);
               print_log('l_cc_email =>'||l_cc_email);
               print_log('l_send_attachments_to =>'||l_send_attachments_to);
               print_log(' ');
               --
               --
                -- Begin Initialize the request set
                --
                l_sec := 'Initializing Request Set - HDS Treasury: Cash Positioning - Report Set';
                --
                fnd_file.put_line(fnd_file.log, l_sec);
                --
                l_ok := fnd_submit.set_request_set
                                  (
                                      application =>'XXCUS'
                                     ,request_set =>'XXCUS_CE_CASH_POSITIONING' --HDS Treasury: Cash Positioning - Report Set
                                  );
                -- End Initialize the request set
                --
                -- Submit each stage of the request set
                --
                -- Stage_100 : Fetch files for processing.
                --
                IF (l_ok AND l_success = 0) THEN
                  --
                  l_sec := 'Stage_100 : Fetch files for processing.';
                  --
                  fnd_file.put_line(fnd_file.log, l_sec);
                  --
                  l_ok := fnd_submit.submit_program
                                                  (
                                                    application => 'XXCUS'
                                                   ,program     => 'XXCUS_CURRENT_DAY_IMPORT'
                                                   ,stage       => 'STAGE_100'
                                                   ,argument1   =>l_current_db_name
                                                   ,argument2   =>l_to_email
                                                   ,argument3   =>l_max_files_allowed
                                                    );
                ELSE
                  l_success := -99;
                  print_log(l_sec || ':  Failed = ' || l_success);
                  RAISE submitprog_failed;
                END IF;
                --
                -- Stage_200 : Validate Pre-requisites.
                --
                IF (l_ok AND l_success = 0) THEN
                  --
                  l_sec := 'Stage_200 : Validate Pre-requisites.';
                  --
                  fnd_file.put_line(fnd_file.log, l_sec);
                  --
                  l_ok := fnd_submit.submit_program
                                                  (
                                                    application => 'XXCUS'
                                                   ,program     => 'XXCUS_CE_SETUP_PREREQS'
                                                   ,stage       => 'STAGE_200'
                                                   ,argument1   =>l_max_files_allowed
                                                   ,argument2   =>l_to_email
                                                   ,argument3   =>l_cc_email
                                                    );
                ELSE
                  l_success := -99;
                  print_log(l_sec || ':  Failed = ' || l_success);
                  RAISE submitprog_failed;
                END IF;
                --
                -- Stage_300 : Backup Bank File Data to Table.
                --
                IF (l_ok AND l_success = 0) THEN
                  --
                  l_sec := 'Stage_300 : Backup Bank File Data to Table.';
                  --
                  fnd_file.put_line(fnd_file.log, l_sec);
                  --
                  l_ok := fnd_submit.submit_program
                                                  (
                                                    application => 'XXCUS'
                                                   ,program     => 'XXCUS_CE_CURR_DAY_CASH_FILE'
                                                   ,stage       => 'STAGE_300'
                                                    );
                ELSE
                  l_success := -99;
                  print_log(l_sec || ':  Failed = ' || l_success);
                  RAISE submitprog_failed;
                END IF;
                --
                -- Begin Ver 1.3
                -- Stage_310 : HDS Treasury: Verify for Payroll Wires in WellsFargo File
                --
                IF (l_ok AND l_success = 0) THEN
                  --
                  l_sec := 'Stage_310 : Verify for Payroll Wires in WellsFargo File.';
                  --
                  fnd_file.put_line(fnd_file.log, l_sec);
                  --
                  l_db :=get_database_name;
                  --
                  l_ok := fnd_submit.submit_program
                                                  (
                                                    application   => 'XXCUS'
                                                   ,program       => 'XXCUS_CDCP_PAYROLL_WIRE_CHECK'
                                                   ,stage             =>  'STAGE_310'
                                                   ,argument1  => l_db
                                                   ,argument2  => l_to_email
                                                    );
                ELSE
                  l_success := -99;
                  print_log(l_sec || ':  Failed = ' || l_success);
                  RAISE submitprog_failed;
                END IF;
                --     
                -- Stage_320 : HDS Treasury: Unplug Payroll Wires from WF Cash Positioning File
                --
                IF (l_ok AND l_success = 0) THEN
                  --
                  l_sec := 'Stage_320 : Verify for Payroll Wires in WellsFargo File.';
                  --
                  fnd_file.put_line(fnd_file.log, l_sec);
                  --
                  l_ok := fnd_submit.submit_program
                                                  (
                                                    application   => 'XXCUS'
                                                   ,program       => 'XXCUS_CDCP_WF_REWRITE'
                                                   ,stage             =>  'STAGE_320'
                                                    );
                ELSE
                  l_success := -99;
                  print_log(l_sec || ':  Failed = ' || l_success);
                  RAISE submitprog_failed;
                END IF;
                --                                
                -- End Ver 1.3
                -- Stage_400 :Kickoff SQL Loader for all bank files.
                --
                IF (l_ok AND l_success = 0) THEN
                  --
                  l_sec := 'Stage_400 : Kickoff SQL Loader for all bank files.';
                  --
                  fnd_file.put_line(fnd_file.log, l_sec);
                  --
                  l_ok := fnd_submit.submit_program
                                                  (
                                                    application => 'XXCUS'
                                                   ,program     => 'XXCUS_CE_IMPORT_BAI2_FILE'
                                                   ,stage       => 'STAGE_400'
                                                    );
                ELSE
                  l_success := -99;
                  print_log(l_sec || ':  Failed = ' || l_success);
                  RAISE submitprog_failed;
                END IF;
                --
                -- Stage_500 : Extract BAI file content for WellsFargo.
                --
                IF (l_ok AND l_success = 0) THEN
                  --
                  l_sec := 'Stage_500 : Extract BAI file content for WellsFargo.';
                  --
                  fnd_file.put_line(fnd_file.log, l_sec);
                  --
                  l_ok := fnd_submit.submit_program
                                                  (
                                                    application => 'XXCUS'
                                                   ,program     => 'XXCUS_CE_EXTRACT_WF'
                                                   ,stage       => 'STAGE_500'
                                                   ,argument1 =>'WELLSFARGO'
                                                    );
                ELSE
                  l_success := -99;
                  print_log(l_sec || ':  Failed = ' || l_success);
                  RAISE submitprog_failed;
                END IF;
                --
                -- Stage_600 : Extract BAI file content for WellsFargo.
                --
                IF (l_ok AND l_success = 0) THEN
                  --
                  l_sec := 'Stage_600 : Extract BAI file content for Bank of America US.';
                  --
                  fnd_file.put_line(fnd_file.log, l_sec);
                  --
                  l_ok := fnd_submit.submit_program
                                                  (
                                                    application => 'XXCUS'
                                                   ,program     => 'XXCUS_CE_EXTRACT_BOA_US'
                                                   ,stage       => 'STAGE_600'
                                                   ,argument1 =>'BOA_US'
                                                    );
                ELSE
                  l_success := -99;
                  print_log(l_sec || ':  Failed = ' || l_success);
                  RAISE submitprog_failed;
                END IF;
                --
                -- Stage_700 : Extract BAI file content for Bank of America US.
                --
                IF (l_ok AND l_success = 0) THEN
                  --
                  l_sec := 'Stage_700 : Extract BAI file content for Bank of America CA.';
                  --
                  fnd_file.put_line(fnd_file.log, l_sec);
                  --
                  l_ok := fnd_submit.submit_program
                                                  (
                                                    application => 'XXCUS'
                                                   ,program     => 'XXCUS_CE_EXTRACT_BOA_CA'
                                                   ,stage       => 'STAGE_700'
                                                   ,argument1 =>'BOA_CA'
                                                    );
                ELSE
                  l_success := -99;
                  print_log(l_sec || ':  Failed = ' || l_success);
                  RAISE submitprog_failed;
                END IF;
                --
                -- Stage_800 : Extract BAI file content for Bank of America Canada.
                --
                IF (l_ok AND l_success = 0) THEN
                  --
                  l_sec := 'Stage_800 : Generate Final Product.';
                  --
                  fnd_file.put_line(fnd_file.log, l_sec);
                  --
                  l_ok := fnd_submit.submit_program
                                                  (
                                                    application => 'XXCUS'
                                                   ,program     => 'XXCUS_CE_GEN_CSV_EXTRACTS'
                                                   ,stage       => 'STAGE_800'
                                                    );
                ELSE
                  l_success := -99;
                  print_log(l_sec || ':  Failed = ' || l_success);
                  RAISE submitprog_failed;
                END IF;
                --
                -- Stage_900 : Email current day final product as a zip file to the business community.
                --
                IF (l_ok AND l_success = 0) THEN
                  --
                  l_sec := 'Stage_900 : Email Final Results File.';  -- TMS:   20161215-00113 / ESMS 501131
                  --
                  --fnd_file.put_line(fnd_file.log, l_sec||', l_zip_file ='||l_zip_file);  -- TMS:   20161215-00113 / ESMS 501131
                  --
                  l_all_codes_file :=l_outbound_loc||'/'||'HDS_CURRDAY_ALL_CODES_'||TO_CHAR(SYSDATE, 'MMDDYY')||g_out_file_ext;
                  --
                  l_reqd_codes_file :=l_outbound_loc||'/'||'HDS_CURRDAY_REQD_CODES_'||TO_CHAR(SYSDATE, 'MMDDYY')||g_out_file_ext;
                  --
                  l_exceptions_file :=l_outbound_loc||'/'||'HDS_CURRDAY_EXCEPTIONS_'||TO_CHAR(SYSDATE, 'MMDDYY')||g_out_file_ext;
                  --
                  l_ok := fnd_submit.submit_program
                                                  (
                                                    application => 'XXCUS'
                                                   ,program     => 'XXCUS_CE_EMAIL_CURRDAY_FILES'
                                                   ,stage       => 'STAGE_900'
                                                   --,argument1 =>l_zip_file --unix program parameter $5 --absolute path of zip file with the file name -- -- TMS:   20161215-00113 / ESMS 501131
                                                   ,argument1 =>l_all_codes_file --unix program parameter $5 --absolute path of all codes file path with the file name -- TMS:   20161215-00113 / ESMS 501131
                                                   ,argument2 =>l_send_attachments_to  --unix program parameter $6 -- Send Attachment TO email address
                                                   ,argument3 =>l_cc_email --unix program parameter $7 -- CC email addresses separated by commas
                                                   ,argument4 =>g_notif_email_from --unix program parameter $8    -- FROM email address
                                                   ,argument5 =>l_reqd_codes_file  -- $9 --absolute path of requried codes file path with the file name -- TMS:   20161215-00113 / ESMS 501131
                                                   ,argument6 =>l_exceptions_file  -- $10 --absolute path of exceptions code file path with the file name -- TMS:   20161215-00113 / ESMS 501131
                                                    );
                ELSE
                  l_success := -99;
                  print_log(l_sec || ':  Failed = ' || l_success);
                  RAISE submitprog_failed;
                END IF;
                --
                -- At this moment we have submitted the individual requests for each stage. now submit the request set.
                --
                IF l_ok AND l_success = 0 THEN
                     --
                     l_request_id := fnd_submit.submit_set(NULL, TRUE); --we use TRUE to let know that the current routine that submitted the set will pause until the request set is done
                     --
                    print_log ('Submitted request set HDS Treasury: Cash Positioning - Report Set, Request_id = ' || l_request_id);
                    --
                    fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>lc_request_data);
                    --
                     lc_request_data :=to_char(to_number(lc_request_data)+1);
                     --
                ELSE
                    l_success := -1000;
                    --
                    print_log ( 'Failedto submit request set HDS Treasury: Cash Positioning - Report Set, message = ' || l_success);
                    --
                    RAISE submitset_failed;
                    --
                END IF;
       --
    else
       --
        retcode :=0;
        errbuf :='Child request [HDS Treasury: Cash Positioning (Report Set) completed. Exiting HDS Treasury: Submit Cash Positioning Wrapper';
        print_log(errbuf);
       -- end copy
    end if;
   --
  EXCEPTION
    WHEN srs_failed THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := 'Call to set_request_set failed: ' || fnd_message.get ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN submitprog_failed THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Call to submit_program failed: ' || fnd_message.get ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN submitset_failed THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Call to submit_set failed: ' || fnd_message.get ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Request set submission failed - unknown error: ' ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END submit_request_set;
--

-- Start Version 1.2
PROCEDURE uc4_Cash_Positioning (p_errbuf               OUT VARCHAR2
                               ,p_retcode              OUT NUMBER
                               ,P_To_Email             IN VARCHAR2
                               ,P_CC_Email             IN VARCHAR2
                               ,P_Attach_Email         IN VARCHAR2
                               ,p_user_name            IN VARCHAR2
                               ,p_responsibility_name  IN VARCHAR2) IS
            --
    l_package     VARCHAR2(50) := 'xxcus_currentday_cash_pkg.uc4_Cash_Positioning';
    l_distro_list VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

    l_req_id     NUMBER NULL;
    l_phase      VARCHAR2(50);
    l_status     VARCHAR2(50);
    l_dev_status VARCHAR2(50);
    l_dev_phase  VARCHAR2(50);
    l_supplier_id NUMBER;
    l_rec_cnt     NUMBER := 0;
    l_interval    NUMBER := 30; -- In seconds
    l_max_time    NUMBER := 15000; -- In seconds
    l_message     VARCHAR2(2000);
    l_can_submit_request  BOOLEAN := TRUE;
    l_err_msg             VARCHAR2(3000);
    l_err_code            NUMBER;
    l_statement           CLOB;
    l_user_id             NUMBER;
    l_responsibility_id   NUMBER;
    l_resp_application_id NUMBER;
    l_program             VARCHAR2(30) := 'XXCUS_CURRDAY_REQSET_WRAPPER'; -- Program to process recon
    l_application         VARCHAR2(30) := 'XXCUS';
  BEGIN

    p_retcode := 0;
    -- Deriving User Id
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM applsys.fnd_user
       WHERE user_name = upper(p_user_name)
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'UserName - ' || p_user_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving user_id for UserName - ' ||
                     p_user_name;
        RAISE program_error;
    END;

    -- Deriving Responsibility Id and Responsibility Application Id
    BEGIN
      SELECT responsibility_id, application_id
        INTO l_responsibility_id, l_resp_application_id
        FROM apps.fnd_responsibility_vl
       WHERE responsibility_name = p_responsibility_name
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Responsibility - ' || p_responsibility_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving Responsibility_id for ResponsibilityName - ' ||
                     p_responsibility_name;
        RAISE program_error;
    END;


    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    fnd_global.apps_initialize(l_user_id
                              ,l_responsibility_id
                              ,l_resp_application_id);

    l_req_id := fnd_request.submit_request(application => l_application
                                          ,program     => l_program
                                          ,description => NULL
                                          ,start_time  => SYSDATE
                                          ,sub_request => FALSE
                                          ,argument1   => P_To_Email   -- TO Email ID
                                          ,argument2   => P_CC_Email -- CC Email ID
                                          ,argument3   => P_Attach_Email -- Send Attachments TO Email
                                          );
    COMMIT;

    IF (l_req_id != 0)
    THEN
      IF fnd_concurrent.wait_for_request(l_req_id
                                        ,l_interval
                                        ,l_max_time
                                        ,l_phase
                                        ,l_status
                                        ,l_dev_phase
                                        ,l_dev_status
                                        ,l_message)
      THEN
        l_err_msg := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                     l_dev_phase || ' DStatus ' || l_dev_status || chr(10) ||
                     ' MSG - ' || l_message;
        -- Error Returned
        IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
        THEN
          l_statement := 'An error occured in the running of HDS Treasury: Submit Cash Positioning Wrapper' ||
                         l_err_msg || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
      END IF;

    ELSE
      l_statement := 'An error occured when trying to submit HDS Treasury: Submit Cash Positioning Wrapper';
      RAISE program_error;
    END IF;

    dbms_output.put_line(l_statement);
    dbms_output.put_line('Request ID:  ' || l_req_id);
    dbms_output.put_line('Responsibility Name:  ' || p_responsibility_name);
    dbms_output.put_line('User Name:  ' || p_user_name);
    dbms_output.put_line('P_To_Email:  ' || P_To_Email);
    dbms_output.put_line('P_CC_Email:  ' || P_CC_Email);
    dbms_output.put_line('P_Attach_Email:  ' || P_Attach_Email);

    --For UC4 success notification
    dbms_output.put_line('Success');

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || '-' || l_statement || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('Concurrent short name :  ' || l_program);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package
                                          ,p_calling           => l_err_msg
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'CE');
      p_retcode := 2;
      p_errbuf  := l_err_msg;

    WHEN OTHERS THEN
      l_err_msg := l_err_msg || '-' || l_statement|| ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      dbms_output.put_line('Request ID:  ' || l_req_id);
      dbms_output.put_line('Concurrent short name :  ' || l_program);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_package
                                          ,p_calling           => l_err_msg
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'CE');
      p_retcode := 2;
      p_errbuf  := l_err_msg;

  END uc4_Cash_Positioning;
-- End Version 1.2
end xxcus_currentday_cash_pkg;
/