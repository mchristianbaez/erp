CREATE OR REPLACE PACKAGE BODY XXWC_AHH_USERS_CONV_PKG
IS
   /******************************************************************************************************************************************************
        $Header XXWC_AHH_USERS_CONV_PKG $
        Module Name: XXWC_AHH_USERS_CONV_PKG.pkb

        PURPOSE:   AHH User Conversion

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        07/18/2018  Sundaramoorthy    TMS#20180718-00046
   ******************************************************************************************************************************************************/


   g_err_callfrom      VARCHAR2 (1000) := 'XXWC_AHH_USERS_CONV_PKG';
   g_module            VARCHAR2 (100) := 'XXWC';
   g_distro_list       VARCHAR2 (80) := 'wc-itdevalerts-u1@hdsupply.com';
   gvc_debug_enabled   VARCHAR2 (1) := 'Y';

   PROCEDURE debug (P_RECORD_ID IN NUMBER, p_msg IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
      l_sec           VARCHAR2 (1000);
      lvc_procedure   VARCHAR2 (100) := 'DEBUG';
   BEGIN
      IF gvc_debug_enabled = 'Y'
      THEN
         l_sec := ' Inserting Data into Log Table';

         INSERT
           INTO XXWC.XXWC_AR_EQUIFAX_LOG_TBL (LOG_SEQ_NO, RECORD_ID, LOG_MSG)
         VALUES (XXWC.XXWC_AR_EQUIFAX_LOG_SEQ.NEXTVAL, P_RECORD_ID, P_MSG);

         COMMIT;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error Occured for '
                                     || p_record_id
                                     || 'record id in XXWC_AR_EQUIFAX_LOG_TBL.',
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;


   PROCEDURE main (x_errbuf OUT VARCHAR2, x_retcode OUT VARCHAR2 , p_user_name IN VARCHAR2)
   IS
      CURSOR CUR_MAIN (p_user IN VARCHAR2)
      IS
         SELECT *
           FROM XXWC.XXWC_AHH_USERS_STG_TBL XAUS
          WHERE XAUS.PROCESS_STATUS IN ( 'N', 'E')
            AND USER_ID = NVL(p_user , USER_ID) ;

      l_sec                 VARCHAR2 (32767);
      ln_user_id            NUMBER;
	  ln_trim_user_id       NUMBER;
      lvc_user_exist_flag   VARCHAR2 (1);
      lvc_role_exist_flag   VARCHAR2 (1);
      lvc_sup_exist_flag    VARCHAR2 (1);
      l_count               NUMBER;
      
      l_person_id                   PER_ALL_PEOPLE_F.PERSON_ID%TYPE;
      l_employee_number             PER_ALL_PEOPLE_F.EMPLOYEE_NUMBER%TYPE;
      l_full_name                   PER_ALL_PEOPLE_F.FULL_NAME%TYPE;
      L_ASSIGNMENT_ID               PER_ALL_ASSIGNMENTS_F.ASSIGNMENT_ID%TYPE;
	  l_email                       PER_ALL_PEOPLE_F.EMAIL_ADDRESS%TYPE;
      l_object_version_number       PER_ALL_ASSIGNMENTS_F.OBJECT_VERSION_NUMBER%TYPE;
      l_asg_object_version_number   NUMBER;
      l_per_effective_start_date    PER_ALL_PEOPLE_F.EFFECTIVE_START_DATE%TYPE;
      l_per_effective_end_date      PER_ALL_PEOPLE_F.EFFECTIVE_END_DATE%TYPE;
      l_per_comment_id              PER_ALL_PEOPLE_F.COMMENT_ID%TYPE;
      l_assignment_seq              PER_ALL_ASSIGNMENTS_F.ASSIGNMENT_SEQUENCE%TYPE;
      l_assignment_number           PER_ALL_ASSIGNMENTS_F.ASSIGNMENT_NUMBER%TYPE;
      l_name_combination_warning    BOOLEAN;
      l_assign_payroll_warning      BOOLEAN;
      l_orig_hire_warning           BOOLEAN;
      l_role_name                   UMX_ALL_ROLE_VL.name%TYPE; 
   
      l_user_password              VARCHAR2 (100) := 'password#1';
      l_error_msg                  VARCHAR2 (4000); 
      ln_person_id                 apps.per_people_f.person_id%TYPE;
      
      l_concatenated_segments    VARCHAR2 (1000) := NULL;
      l_soft_coding_keyflex_id   NUMBER;
      l_comment_id               NUMBER := NULL;
      l_effective_start_date     DATE := NULL;
      l_effective_end_date       DATE := NULL;
      l_no_managers_warning      BOOLEAN;
      l_other_manager_warning    BOOLEAN;
   BEGIN
      l_sec := 'Start Main';
      fnd_file.put_line (fnd_file.LOG, l_sec);

      SELECT COUNT (1) INTO l_count FROM XXWC.XXWC_AHH_USERS_STG_TBL;

      l_sec := 'Beefore insert l_count from XXWC.XXWC_AHH_USERS_STG_TBL: ' || l_count;
      fnd_file.put_line (fnd_file.LOG, l_sec);

      IF l_count <= 0
      THEN
         INSERT INTO XXWC.XXWC_AHH_USERS_STG_TBL (USER_ID,
                                                  FULL_NAME,
                                                  FIRST_NAME,
                                                  LAST_NAME,                                                  
                                                  JOB_TITLE_DESCRIPTION,
                                                  HIRE_DATE,
                                                  EMAIL,                                            
                                                  ORACLE_ROLE_ASSIGNMENT,
                                                  PROCESS_STATUS,
                                                  ROLE_ASSIGNMENT_FLAG)
            SELECT USER_ID,
                   FULL_NAME,
                   FIRST_NAME,
                   LAST_NAME,                   
                   JOB_TITLE_DESCRIPTION,
                   HIRE_DATE,
                   EMAIL,             
                   ORACLE_ROLE_ASSIGNMENT,
                   'N',--PROCESS_STATUS
                   'N'--ROLE_ASSIGNMENT_FLAG
              FROM XXWC.XXWC_AHH_USERS_STG_EXT_TBL;

         l_sec := 'Records inserted into XXWC.XXWC_AHH_USERS_STG_TBL: '|| SQL%ROWCOUNT;
         fnd_file.put_line (fnd_file.LOG, l_sec);
      
	  END IF;

      l_sec := 'Main Looping Starting';

      FOR rec_main IN cur_main (p_user_name)
      LOOP
         l_sec := '==========================================================================';
		 fnd_file.put_line (fnd_file.LOG, l_sec);
		 l_sec := 'User : ' || rec_main.USER_ID;		 
         fnd_file.put_line (fnd_file.LOG, l_sec);
         
		 lvc_user_exist_flag := 'N';

         BEGIN		
            SELECT user_id
              INTO ln_user_id
              FROM APPS.FND_USER
             WHERE UPPER (user_name) = UPPER (rec_main.USER_ID);
			
            l_sec := 'User Data fetched user id: ' || ln_user_id;

            IF ln_user_id IS NOT NULL
            THEN
               LVC_USER_EXIST_FLAG := 'Y';
			   fnd_file.put_line (fnd_file.LOG, 'User Exists'||  ln_user_id) ;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               lvc_user_exist_flag := 'N';
         END;


         IF lvc_user_exist_flag = 'N'
         THEN
            l_sec := 'Before hr_employee_api.create_employee Create User: ' || rec_main.USER_ID;
            fnd_file.put_line (fnd_file.LOG, l_sec);

            BEGIN	
              SELECT SUBSTR(REC_MAIN.USER_ID, 3) INTO LN_TRIM_USER_ID FROM DUAL;	
           		fnd_file.put_line (fnd_file.LOG, 'Trim User ID'|| 	LN_TRIM_USER_ID);   
               hr_employee_api.create_employee (
                  --INPUT Parameter
                  p_hire_date                   => TO_DATE (rec_main.HIRE_DATE, 'mm/dd/yyyy'),
                  p_business_group_id           => 0, --fnd_profile.value_specific('PER_BUSINESS_GROUP_ID'), Setup Business Group (BUSINESS_GROUP_ID= 0)
                  p_last_name                   => rec_main.last_name,
                  p_first_name                  => rec_main.first_name,
                  P_MIDDLE_NAMES                => NULL,
                  p_sex                         => 'F',             
                  P_DATE_OF_BIRTH               => NULL,
				  p_email_address               => rec_main.EMAIL,                 
                  --OUTPUT Parameter
                  p_employee_number             => LN_TRIM_USER_ID,
                  p_person_id                   => l_person_id,
                  p_assignment_id               => l_assignment_id,
                  p_per_object_version_number   => l_object_version_number,
                  p_asg_object_version_number   => l_asg_object_version_number,
                  p_per_effective_start_date    => l_per_effective_start_date,
                  p_per_effective_end_date      => l_per_effective_end_date,
                  p_full_name                   => l_full_name,
                  p_per_comment_id              => l_per_comment_id,
                  p_assignment_sequence         => l_assignment_seq,
                  p_assignment_number           => l_assignment_number,
                  p_name_combination_warning    => l_name_combination_warning,
                  p_assign_payroll_warning      => l_assign_payroll_warning,
                  p_orig_hire_warning           => l_orig_hire_warning);

               l_sec := 'Before fnd_user_pkg.createuser: ' || rec_main.USER_ID || ' l_person_id: '||l_person_id;
               fnd_file.put_line (fnd_file.LOG, l_sec);

               fnd_user_pkg.createuser (
                  x_user_name                => rec_main.user_id,
                  x_owner                    => NULL,
                  x_unencrypted_password     => l_user_password,
                  x_start_date               => TRUNC(SYSDATE),
                  x_end_date                 => NULL,
                  x_password_date            => TRUNC(SYSDATE),
                  x_password_lifespan_days   => 90,
				  x_description              => rec_main.last_name||', '||rec_main.first_name,
                  x_employee_id              => l_person_id,
                  x_email_address            => rec_main.email);
				  
			  UPDATE XXWC.XXWC_AHH_USERS_STG_TBL
                  SET PROCESS_STATUS = 'P'
                     ,error_message = NULL
                WHERE USER_ID =rec_main.USER_ID;
			 
               IF rec_main.ORACLE_ROLE_ASSIGNMENT IS NOT NULL THEN
                  BEGIN
				        SELECT name
                       INTO l_role_name
                      FROM APPS.UMX_ALL_ROLE_VL
                     WHERE display_name = REPLACE(rec_main.oracle_role_assignment,CHR(13),'');

                     IF l_role_name IS NOT NULL
                     THEN
                        lvc_role_exist_flag := 'Y';
                     END IF;
                  EXCEPTION
                  WHEN OTHERS
                  THEN
                      lvc_role_exist_flag := 'N';
					  l_sec := 'Role not exists for user : Role name  ' ||SQLERRM;
					  fnd_file.put_line (fnd_file.LOG, l_sec);
                  END;
                  
                  IF lvc_role_exist_flag = 'Y' THEN
                     l_sec := 'Before wf_local_synch.PropagateUserRole : ' || rec_main.USER_ID;
                     fnd_file.put_line (fnd_file.LOG, l_sec);

                     wf_local_synch.PropagateUserRole (
                                           p_user_name   => UPPER(rec_main.user_id),
                                           p_role_name   => l_role_name 
                                           );
                  END IF;                         
               END IF;   --rec_main.ORACLE_ROLE_ASSIGNMENT IS NOT NULL                                                        
                                                                           
              
			  UPDATE XXWC.XXWC_AHH_USERS_STG_TBL
                  SET ROLE_ASSIGNMENT_FLAG = 'P'
                     ,error_message = NULL
                WHERE USER_ID =rec_main.USER_ID;	  
                
               COMMIT;

            EXCEPTION
               WHEN OTHERS
               THEN
                  ROLLBACK;
                  
                  l_error_msg := SQLERRM;
                  l_sec:= 'Inside when others: '||l_error_msg;
                  
                 UPDATE XXWC.XXWC_AHH_USERS_STG_TBL
                    SET PROCESS_STATUS = 'E',
                        error_message = l_error_msg
                  WHERE USER_ID =rec_main.USER_ID;
                  
                  fnd_file.put_line (fnd_file.LOG, l_sec);
                  DBMS_OUTPUT.PUT_LINE (l_error_msg);
            END;
         END IF; --lvc_user_exist_flag
      END LOOP;     
    	  
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, 'error occured (Main):' || l_sec);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.MAIN',
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => NULL,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;

END XXWC_AHH_USERS_CONV_PKG;
/