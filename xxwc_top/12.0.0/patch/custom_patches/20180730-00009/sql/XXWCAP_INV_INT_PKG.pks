/********************************************************************************
Package       : XXWCAP_INV_INT_PKG
Purpose       : API to submit Payables Import Process.

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     07/30/2018    Pattabhi Avula   TMS#20180730-00009 - AHH AP Invoice Interface - 
                                       Call Invoice Validation Process
********************************************************************************/
create or replace 
PACKAGE      APPS.XXWCAP_INV_INT_PKG
AS

   PROCEDURE MAIN (
      p_errbuf            OUT      VARCHAR2,
      p_retcode           OUT      NUMBER,
      p_submit_intf       IN       VARCHAR2,
	  P_Submitted_From    IN       VARCHAR2  -- Ver#1.0
   );

   PROCEDURE load_interface;

   PROCEDURE submit_pay_interface(P_Submitted_From    IN       VARCHAR2); -- Ver#1.0

   PROCEDURE attach_document(
      p_errbuf            OUT      VARCHAR2,
      p_retcode           OUT      NUMBER
   );

   PROCEDURE load_global_temp_tbl;

   PROCEDURE uc4_call (p_errbuf              OUT VARCHAR2,
                       p_retcode             OUT NUMBER,
                       p_conc_prg_name        IN VARCHAR2,
                       p_conc_prg_arg1        IN VARCHAR2,
                       p_user_name            IN VARCHAR2,
                       p_responsibility_name  IN VARCHAR2,
                       p_org_name             IN VARCHAR2,
                       p_threshold_value      IN NUMBER
                      );

END XXWCAP_INV_INT_PKG;
/