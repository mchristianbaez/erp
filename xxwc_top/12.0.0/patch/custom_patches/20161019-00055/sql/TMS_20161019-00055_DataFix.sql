/*************************************************************************
  $Header TMS_20161019-00055.sql $
  Module Name: TMS_20161019-00055  Data Fix script for TMS# 20161019-00055

  PURPOSE: Data Fix script for TMS# 20161019-00055

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        30-SEP-2016  Gopi Damuluri         TMS#20161019-00055 
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
  l_sql_stmt  VARCHAR2(2000);
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20161019-00055    , Before Update');

   --Back up of data
   l_sql_stmt := 'CREATE TABLE xxwc.xxwc_mtl_material_txns_bkp
                  AS
                  SELECT * from MTL_MATERIAL_TRANSACTIONS
                   WHERE 1=1--organization_id = 365
                     AND costed_flag in (''N'',''E'')';
   
   EXECUTE IMMEDIATE l_sql_stmt;

   --To fix the issue, please proceed in a TEST Environment where this situation is replicated:  
   UPDATE MTL_MATERIAL_TRANSACTIONS MMT
      SET TRANSFER_COST_GROUP_ID =
             (SELECT default_cost_group_id
                FROM mtl_secondary_inventories
               WHERE organization_id          = 224
                 AND secondary_inventory_name = 'General')
    WHERE mmt.transaction_id = 479088943
      AND MMT.COSTED_FLAG = 'E'
      AND MMT.ORGANIZATION_ID = 365
      AND NOT EXISTS 
                 (SELECT 1
                    FROM MTL_TRANSACTION_ACCOUNTS MTA
                   WHERE MMT.TRANSACTION_ID = MTA.TRANSACTION_ID);

   DBMS_OUTPUT.put_line ('TMS: 20161019-00055  Sales order lines updated (Expected:1): '|| SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20161019-00055    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161019-00055 , Errors : ' || SQLERRM);
END;
/