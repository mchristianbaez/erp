/*************************************************************************
  $Header TMS_20170208_00264_Cannot_Receive_ISO.sql $
  Module Name: TMS_20170208_00264  Cannot Receive ISO

  PURPOSE: Data fix script for shipment line id 9486635

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        13-FEB-2017  Niraj Kumar Ranjan     TMS#20170208-00264 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
/* 
 * Always please ask customer to run the scripts on their TEST instance first
 * BEFORE applying it on PRODUCTION.  
 * Make sure the data is validated for correctness and related functionality is
 * verified after the script has been run on a test instance.  Customer is 
 * responsible to authenticate and verify correctness of data manipulation scripts.
 *
 * Data fix instructions:
 * 1) Run the script given below.
 * 2) For ISO and Inter-Org intransit shipments, once the script inserts the RSH
 *    record user has to update RSH.shipped_date column manually by knowing the 
 *    actual departure date of the shipment. 
 *
 *    Also update RSH.shipment number manually if it is not populated by the
 *    script. The datafix script populates the shipment number if the source 
 *    rsl.mmt_transaction_id is available otherwise user has to manually update 
 *    the shipment number.
 * 3) Finally check the output and if every thing is fine and as expected then 
 *    COMMIT the transaction else rollback and report the behaviour to Dev.
 */

------------------------------------------------------------
/* ALTER TRIGGER  APPS.ADS_RCV_SHIPMNT_HDRS_TRG1 DISABLE; */
------------------------------------------------------------
DECLARE  
        X_shipment_header_id   NUMBER := to_number(null); 
        X_receipt_num          rcv_shipment_headers.receipt_num%type;
        x_shipment_num         rcv_shipment_headers.shipment_num%TYPE;
        X_rowid                VARCHAR2(50); 
        X_prev_shid            NUMBER := -999;
        X_curr_shid            NUMBER;
        x_receipt_src_code     rcv_shipment_headers.receipt_source_code%type; 
        x_org_id               NUMBER; 
        x_from_orgn_id         NUMBER; 
        x_progress             NUMBER; 
        x_vendor_id            NUMBER;
        x_receipt_code         rcv_parameters.user_defined_receipt_num_code%type;
        X_temp_receipt_num     rcv_shipment_headers.receipt_num%type;
        X_receipt_exists       NUMBER;

        CURSOR headers  IS        
        SELECT  rsl.*
        FROM    rcv_shipment_lines rsl
        WHERE   rsl.source_document_code IN ('PO','RMA','REQ','INVENTORY') 
          AND rsl.Shipment_line_id=9486635 --we can probably add this condition.               
        AND NOT EXISTS
         (SELECT '1'
          FROM    rcv_shipment_headers rsh
          WHERE   rsh.shipment_header_id = rsl.shipment_header_id          
         )
        ORDER BY rsl.shipment_header_id;


        sline headers%rowtype;

BEGIN
        x_progress := 10;

        OPEN headers;
        LOOP
        x_progress := 15;
        FETCH headers INTO sline;
        EXIT WHEN headers%NOTFOUND;
        
        x_progress := 20;
        X_curr_shid         := sline.shipment_header_id;
        X_receipt_num       := null;
        x_progress := 30;

        IF (X_prev_shid     <> X_curr_shid) THEN

             X_prev_shid := X_curr_shid;
            
             BEGIN  
              x_progress := 40; 
                SELECT vendor_id  
                INTO x_vendor_id  
                FROM po_headers_all  
                WHERE po_header_id = sline.po_header_id; 
                EXCEPTION
                WHEN OTHERS THEN
                x_vendor_id := NULL;
             END;

              x_progress := 50;
              IF (sline.source_document_code ='PO') THEN

                  --Dbms_Output.put_line('PO Header ID is ' || sline.po_header_id);

                  x_receipt_src_code := 'VENDOR';
                  SELECT org_id
                  INTO x_org_id
                  FROM po_headers_all
                  WHERE po_header_id = sline.po_header_id;

              END IF;

              x_progress := 60; 
              IF (sline.source_document_code ='RMA') THEN 

                  x_receipt_src_code := 'CUSTOMER'; 
                  SELECT org_id  
                  INTO x_org_id  
                  FROM oe_order_headers_all  
                  WHERE header_id = sline.oe_order_header_id; 
                      
              END IF;

              x_progress := 70; 
              IF (sline.source_document_code ='REQ') THEN 

                  x_receipt_src_code := 'INTERNAL ORDER'; 
                  SELECT org_id  
                  INTO x_org_id  
                  FROM po_requisition_lines_all 
                  WHERE requisition_line_id = sline.requisition_line_id; 

              END IF; 

              x_progress := 80; 
              IF (sline.source_document_code ='INVENTORY') THEN 
                      
                  x_receipt_src_code := 'INVENTORY';
                  SELECT operating_unit  
                  INTO x_org_id  
                  FROM ORG_ORGANIZATION_DEFINITIONS  
                  WHERE organization_id = sline.to_organization_id 
                  and ROWNUM=1; 
                       
              END IF; 

              x_progress := 90;
              IF (sline.source_document_code IN ('REQ','INVENTORY') AND sline.mmt_transaction_id IS NOT null) THEN
               
                  SELECT shipment_number
                  INTO x_shipment_num 
                  FROM mtl_material_transactions 
                  WHERE transaction_id = sline.mmt_transaction_id; 
                       
              ELSE 
                  x_progress := 95; 
                  x_shipment_num := NULL; 
              END IF;

  
              x_progress := 100; 
              SELECT Decode(sline.source_document_code,'PO',sline.to_organization_id,'RMA',sline.to_organization_id,sline.from_organization_id)
              INTO x_from_orgn_id
              FROM dual;

              x_progress := 105;
              Dbms_Application_Info.set_client_info(x_org_id);

              x_progress := 110;

              select user_defined_receipt_num_code
              into   x_receipt_code
              from   rcv_parameters
              where  organization_id = sline.to_organization_id;
 
 
              IF (NVL(X_RECEIPT_CODE, 'MANUAL') = 'AUTOMATIC') THEN
 
                  select to_char(next_receipt_num + 1)
                  into X_temp_receipt_num
                  from rcv_parameters
                  where organization_id = sline.to_organization_id
                  FOR UPDATE OF next_receipt_num;

                  x_progress := 111;

                LOOP
                   SELECT COUNT(*)
                   INTO X_receipt_exists
                   FROM   rcv_shipment_headers rsh
                   WHERE  receipt_num = X_temp_receipt_num
                   AND    ship_to_org_id = sline.to_organization_id;

                   x_progress := 112;
 
                   IF X_receipt_exists = 0 THEN
 
                       update rcv_parameters
                       set next_receipt_num = X_temp_receipt_num
                       where organization_id = sline.to_organization_id;

                       x_progress := 113;
 
                       EXIT;
                   ELSE
                       X_temp_receipt_num := TO_CHAR(TO_NUMBER(X_temp_receipt_num) + 1);
                   END IF;
                   x_progress := 114;
                END LOOP;
             
                x_progress := 115; 
                X_receipt_num :=  X_temp_receipt_num;
 
              ELSE 
                x_progress := 116; 
                X_temp_receipt_num := X_receipt_num;
 
              END IF;
 
              x_progress := 117;
              INSERT INTO RCV_SHIPMENT_HEADERS(
                shipment_header_id,
                last_update_date,
                last_updated_by,
                creation_date,
                created_by,
                last_update_login,
                receipt_source_code,
                vendor_id,
                vendor_site_id,
                organization_id,
             ship_to_org_id,
                shipment_num,
                receipt_num,
                ship_to_location_id,
                bill_of_lading,
                packing_slip,
                shipped_date,
                freight_carrier_code,
                expected_receipt_date,
                employee_id,
                num_of_containers,
                waybill_airbill_num,
                comments,
                attribute_category,
                attribute1,
                attribute2,
                attribute3,
                attribute4,
                attribute5,
                attribute6,
                attribute7,
                attribute8,
                attribute9,
                attribute10,
                attribute11,
                attribute12,
                attribute13,
                attribute14,
                attribute15,
                ussgl_transaction_code,
                government_context,
             request_id,
             program_application_id,
                program_id,
             program_update_date,
             customer_id,
             customer_site_id
              ) VALUES (
                X_curr_shid,
                sline.Last_Update_Date,
                sline.Last_Updated_By,
                sline.Creation_Date,
                sline.Created_By,
                sline.Last_Update_Login,
                x_receipt_src_code,
                x_vendor_id,
                null,
                x_from_orgn_id,
             sline.to_organization_id,
                x_shipment_num,
                x_receipt_num,
                sline.deliver_to_location_id,
                null,
                null,
                null,
                null,
                sline.creation_date,
                sline.employee_id,
                To_Number(NULL),
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
             null,
             null,
             null,
                null,
             null,
             null
              );

        x_progress := 118;

        END IF;
        x_progress := 120;

        END LOOP;
        x_progress := 130;

        CLOSE headers;

        x_progress := 140;

EXCEPTION
     WHEN OTHERS THEN
        Dbms_Output.put_line('x_progress = ' || to_char(x_progress));
        ROLLBACK;
        CLOSE headers;
        RAISE;
END;
/
------------------------------------------------------------
 /* ALTER TRIGGER  APPS.ADS_RCV_SHIPMNT_HDRS_TRG1 ENABLE;  */
------------------------------------------------------------
update rcv_shipment_headers
   set SHIPPED_DATE=(select ACTUAL_SHIPMENT_DATE from oe_order_lines_all where header_id= 54455986 and line_number=5)
 where SHIPMENT_HEADER_ID=4420163;
COMMIT;
