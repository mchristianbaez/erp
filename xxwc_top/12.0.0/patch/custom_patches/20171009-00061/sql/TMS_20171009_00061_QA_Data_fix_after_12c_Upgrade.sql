/*************************************************************************
  $Header TMS_20171009_00061_QA_Data_fix_after_12c_Upgrade.sql 
  Module Name: TMS_20170912_00235  QA - Data Script to run after 12c Upgrade to fix error 

  PURPOSE: QA - Data Script to run after 12c Upgrade to fix error

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        09-Oct-2017  Sundaramoorthy R       TMS#20171009_00061
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;

BEGIN
UPDATE ar_system_parameters_all
SET AUTOMATCH_ENABLED_FLAG = 'N'
WHERE AUTOMATCH_ENABLED_FLAG = 'T'
AND org_id = 162;
COMMIT;
 EXCEPTION
  WHEN OTHERS THEN
  DBMS_OUTPUT.put_line('Error occurred: ' || SQLERRM);
  ROLLBACK;
 END;
 /  