/*
 TMS: 20160218-00175    
 Date: 02/18/2016
 Notes: Could you please change the branch in the header for CSP agreement# 9075, please change the header to Branch# 713. per request of account manager.
*/

SET SERVEROUTPUT ON SIZE 100000;
UPDATE XXWC.XXWC_OM_CONTRACT_PRICING_HDR
   SET ORGANIZATION_ID = 362
 WHERE AGREEMENT_ID = 9075
   AND ORGANIZATION_ID = 366; 
   
   commit;

/* Change Branch Number

For agreement# 9075, please change the header to Branch# 713.  

Agreement # 9075

--OLD ORG_ID 366 BRANCH 719
--NEW ORG_ID 362 BRANCH 713

SELECT *
  FROM XXWC.XXWC_OM_CONTRACT_PRICING_HDR
 WHERE agreement_id = 9075; 
 
SELECT *
  FROM APPS.ORG_ORGANIZATION_DEFINITIONS
 WHERE organization_id = '366'; 
 
SELECT *
  FROM XXWC.XXWC_OM_CONTRACT_PRICING_HDR
 WHERE agreement_id = 9075
   AND ORGANIZATION_ID = 366;   
 
SELECT *
  FROM APPS.ORG_ORGANIZATION_DEFINITIONS
 WHERE organization_code LIKE '713';  
 
SELECT *
  FROM APPS.ORG_ORGANIZATION_DEFINITIONS
 WHERE organization_id = 362;    */
 /
 



  

 

 


  
 
   

   
 



 