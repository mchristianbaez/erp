/*
 TMS: 20160119-00133     
 Date: 02/18/2016
 Notes: Could you please change the branch in the header for CSP agreement# 6648 from branch 015 to branch 003. per request of account manager.
*/

SET SERVEROUTPUT ON SIZE 100000;

UPDATE XXWC.XXWC_OM_CONTRACT_PRICING_HDR
   SET ORGANIZATION_ID = 230
 WHERE AGREEMENT_ID = 6648
   AND ORGANIZATION_ID = 241; 
 
Commit; 

/* Change Branch Number

For agreement# 6648, please change the header to Branch# 003. 

Agreement # 6648

--OLD ORG_ID 241 BRANCH 015
--NEW ORG_ID 230 BRANCH 003

SELECT *
  FROM XXWC.XXWC_OM_CONTRACT_PRICING_HDR
 WHERE agreement_id = 6648; 
 
SELECT *
  FROM APPS.ORG_ORGANIZATION_DEFINITIONS
 WHERE organization_id = '241'; 
 
SELECT *
  FROM XXWC.XXWC_OM_CONTRACT_PRICING_HDR
 WHERE agreement_id = 6648
   AND ORGANIZATION_ID = 241;   
 
SELECT *
  FROM APPS.ORG_ORGANIZATION_DEFINITIONS
 WHERE organization_code LIKE '003';  
 
SELECT *
  FROM APPS.ORG_ORGANIZATION_DEFINITIONS
 WHERE organization_id = 230;    */
 /
 



  

 

 


  
 
   

   
 



 