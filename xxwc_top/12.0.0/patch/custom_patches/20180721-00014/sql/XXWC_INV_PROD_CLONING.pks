/* Formatted on 12/6/2013 2:31:07 PM (QP5 v5.206) */
-- Start of DDL Script for Package APPS.XXWC_INV_PROD_CLONING
-- Generated 12/6/2013 2:31:06 PM from APPS@EBIZDEV
  /*******************************************************************************************************************************
  *    $Header XXWC_INV_PROD_CLONING $                                                                                           *
  *   Module Name: XXWC_INV_PROD_CLONING                                                                                         *
  *                                                                                                                              *
  *   PURPOSE:   This package is used for clonning items from one organization to another                                        *
  *                                                                                                                              *
  *   REVISIONS:                                                                                                                 *
  *   Ver        Date        Author                     Description                                                              *
  *   ---------  ----------  ---------------         -------------------------                                                   *
  *   1.0                                             Initial Version                                                            *
  *   1.4        07/25/2018  Naveen Kalidindi         TMS#20180721-00014  - XXWC INV Product Cloning Errors- Post AHH Go Live    *
  ********************************************************************************************************************************/

CREATE OR REPLACE PACKAGE apps.xxwc_inv_prod_cloning
AS
    TYPE list_items IS TABLE OF VARCHAR2 (64)
        INDEX BY BINARY_INTEGER;

    names                 list_items;

    TYPE comas IS TABLE OF NUMBER
        INDEX BY BINARY_INTEGER;

    g_temp_table_name_o   VARCHAR2 (128);

    PROCEDURE read_coma_dlmtd_string (p_string IN CLOB, p_list_values OUT xxwc_inv_prod_cloning.list_items);

    PROCEDURE read_param_list (p_list_id NUMBER);

    PROCEDURE mass_clone_item (p_errbuf          OUT VARCHAR2
                              ,p_retcode         OUT VARCHAR2
                              ,p_clone_purpose       VARCHAR2
                              ,p_item_list_id        VARCHAR2
                              ,p_org_list_id         VARCHAR2
							  ,p_enable_log    VARCHAR2);    -- Added parameter in Rev 1.4

    FUNCTION find_sourcing_rule (p_org_to_clone NUMBER, p_item_id_to_clone NUMBER)
        RETURN NUMBER;

    FUNCTION find_item_price (p_sourcing_rule       VARCHAR2
                             ,p_org_to_clone        NUMBER
                             ,p_item_id_to_clone    NUMBER
                             ,p_district            VARCHAR2
                             ,p_region              VARCHAR2)
        RETURN NUMBER;

    PROCEDURE assign_item_sourcing_rule (p_organization_id     IN            NUMBER
                                        ,p_inventory_item_id   IN            NUMBER
                                        ,p_sourcing_rule_id                  NUMBER
                                        ,x_return_status          OUT NOCOPY VARCHAR2
                                        ,x_msg_count              OUT NOCOPY NUMBER
                                        ,x_msg_data               OUT NOCOPY VARCHAR2
                                        ,p_rowid                             VARCHAR2);

    FUNCTION get_staging_error_message (p_row_id IN VARCHAR2)
        RETURN NUMBER;

    PROCEDURE create_item_cost (p_inventory_item_id   IN NUMBER
                               ,p_organization_id     IN NUMBER
                               ,p_item_cost           IN NUMBER
                               ,p_uom_code            IN VARCHAR2
                               ,p_rowid                  VARCHAR2);

    PROCEDURE process_create_item (p_rowid IN VARCHAR2);

    PROCEDURE verify_database (p_run_number VARCHAR2);

    PROCEDURE create_report_files (p_run_number VARCHAR2);

    PROCEDURE receiving_subinventory (p_org_id NUMBER, p_item_id NUMBER);

    --set item category - Sales Velocity ='N'
    PROCEDURE set_sales_velocity (p_inventory_item_id          NUMBER
                                 ,p_organization_id            NUMBER
                                 ,p_return_message      IN OUT VARCHAR2);

    PROCEDURE mass_clone_item_parallel_i (p_errbuf         OUT VARCHAR2
                                         ,p_retcode        OUT VARCHAR2
                                         ,p_table_name         VARCHAR2
                                         ,p_batch_number       NUMBER);

    PROCEDURE mass_clone_item_parallel (p_table_name      VARCHAR2
                                       ,p_batch_number    NUMBER
                                       ,p_resp_appl_id    NUMBER
                                       ,p_resp_id         NUMBER
                                       ,p_user_id         NUMBER
                                       ,p_org_id          NUMBER);

    PROCEDURE create_copy_table (p_owner VARCHAR2, p_table_name VARCHAR2, p_new_table_name VARCHAR2);
END xxwc_inv_prod_cloning;
/

-- End of DDL Script for Package APPS.XXWC_INV_PROD_CLONING
