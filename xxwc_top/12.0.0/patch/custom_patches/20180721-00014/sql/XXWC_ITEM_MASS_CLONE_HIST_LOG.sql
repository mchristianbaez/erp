  /********************************************************************************
  FILE NAME: XXWC_ITEM_MASS_CLONE_HIST_LOG.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: Item Mass Clone History Log
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)        DESCRIPTION
  ------- -----------   ---------------- ----------------------------------------------------
  1.0     07/27/2018    Naveen Kalidindi Initial Version.20180721-00014
  *******************************************************************************************/
WHENEVER SQLERROR CONTINUE
-- DROP TABLE
DROP TABLE XXWC.XXWC_ITEM_MASS_CLONE_HIST_LOG;

-- CREATE TABLE
CREATE TABLE XXWC.XXWC_ITEM_MASS_CLONE_HIST_LOG
(
  RUN_NUMBER       VARCHAR2(200),
  CLONE_ID         NUMBER,
  STEP_NUMBER      NUMBER,
  SUB_PROGRAM      VARCHAR2(240),
  LOG_MSG          VARCHAR2(1000),
  CREATED_BY       NUMBER,
  CREATION_DATE    DATE,
  LAST_UPDATED_BY  NUMBER,
  LAST_UPDATE_DATE DATE
);