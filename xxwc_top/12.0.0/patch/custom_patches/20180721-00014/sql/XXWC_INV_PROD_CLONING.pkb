CREATE OR REPLACE PACKAGE BODY xxwc_inv_prod_cloning AS
  /*******************************************************************************************************************************
  *    $Header XXWC_INV_PROD_CLONING $                                                                                           *
  *   Module Name: XXWC_INV_PROD_CLONING                                                                                         *
  *                                                                                                                              *
  *   PURPOSE:   This package is used for clonning items from one organization to another                                        *
  *                                                                                                                              *
  *   REVISIONS:                                                                                                                 *
  *   Ver        Date        Author                     Description                                                              *
  *   ---------  ----------  ---------------         -------------------------                                                   *
  *   1.0                                             Initial Version                                                            *
  *                                                                                                                              *
  *   1.1        09/09/2015  P.Vamshidhar             TMS# 20150626-00113 - Item Mass Clone Tool enhancements                    *
  *   1.2        10/13/2015  P.Vamshidhar             TMS# 20151012-00313 - Handling Exception in FIND_SOURCING_RULE proceducre  *
  *   1.3        11/30/2016  P.Vamshidhar             TMS#20160122-00091  - GL Code Enhancements to leverage GL API              *
  *   1.4        07/25/2018  Naveen Kalidindi         TMS#20180721-00014  - XXWC INV Product Cloning Errors- Post AHH Go Live    *
  ********************************************************************************************************************************/


  l_error_message CLOB;
  l_user_id       NUMBER := nvl(to_number(fnd_profile.value('USER_ID'))
                               ,-1);

  --g_temp_table_name_o   VARCHAR2 (128);
  g_resp_appl_id    PLS_INTEGER := fnd_global.resp_appl_id;
  g_resp_id         PLS_INTEGER := fnd_global.resp_id;
  g_user_id         PLS_INTEGER := fnd_global.user_id;
  g_org_id          PLS_INTEGER := fnd_profile.value('ORG_ID');
  g_conc_request_id PLS_INTEGER := fnd_global.conc_request_id;

  -- Added below variables in Revision 1.1 @ TMS# 20150626-00113
  g_err_callfrom VARCHAR2(100) := 'XXWC_INV_PROD_CLONING';
  g_distro_list  VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  -- 1.4
  g_step_number         NUMBER DEFAULT 0;
  g_enable_log          VARCHAR2(1) := 'N';
  g_log_conc_request_id NUMBER := -1;


  --*************************************
  -- Version 1.4 Added Logging
  --*************************************
  PROCEDURE insert_log(p_run_number  VARCHAR2
                      ,p_clone_id    NUMBER
                      ,p_sub_program VARCHAR2
                      ,p_log_msg     VARCHAR2) IS
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN 
    IF  g_enable_log ='Y' THEN
    g_step_number := g_step_number + 1;
  
    INSERT INTO xxwc.xxwc_item_mass_clone_hist_log
      (run_number
      ,clone_id
      ,step_number
      ,sub_program
      ,log_msg
      ,created_by
      ,creation_date
      ,last_updated_by
      ,last_update_date)
    VALUES
      (p_run_number -- request_id
      ,p_clone_id -- clone_id
      ,g_step_number -- step_number
      ,p_sub_program -- sub_program
      ,p_log_msg -- log_msg
      ,fnd_global.user_id -- created_by
      ,SYSDATE -- creation_date
      ,fnd_global.user_id -- last_updated_by
      ,SYSDATE -- last_update_date
       );
    --
    COMMIT;
    --
	END IF;
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END insert_log;

  --*************************************
  PROCEDURE create_copy_table(p_owner          VARCHAR2
                             ,p_table_name     VARCHAR2
                             ,p_new_table_name VARCHAR2) IS
    v_execute_string CLOB;
    l_errbuf         CLOB;
  BEGIN
    xxwc_common_tunning_helpers.drop_temp_table(p_owner
                                               ,p_new_table_name);
  
    FOR rt IN (SELECT table_name
                 FROM dba_tables
                WHERE table_name = upper(p_table_name)
                  AND owner = upper(p_owner)
                  AND rownum = 1) LOOP
      EXECUTE IMMEDIATE ' CREATE TABLE ' || p_owner || '.' || p_new_table_name ||
                        ' AS SELECT * FROM ' || p_owner || '.' || p_table_name || ' WHERE 1=2';
    END LOOP;
  
    v_execute_string := 'ALTER TABLE ' || p_owner || '.' || p_new_table_name || ' NOLOGGING';
  
    EXECUTE IMMEDIATE v_execute_string;
  
    v_execute_string := 'ALTER TABLE ' || p_owner || '.' || p_new_table_name || ' parallel 6';
  
    EXECUTE IMMEDIATE v_execute_string;
  EXCEPTION
    WHEN OTHERS THEN
      l_errbuf := ' Error_Stack...' || chr(10) || dbms_utility.format_error_stack() || chr(10) ||
                  ' Error_Backtrace...' || chr(10) || dbms_utility.format_error_backtrace();
    
      IF g_conc_request_id > 0 THEN
        fnd_file.put_line(fnd_file.log
                         ,l_errbuf);
      END IF;
    
      RAISE;
  END;

  --************************************
  PROCEDURE wait_for_jobs(p_job_what VARCHAR2) IS
  BEGIN
    FOR r IN (SELECT '1'
                FROM dba_jobs
               WHERE what LIKE '%' || p_job_what || '%'
                 AND rownum = 1) LOOP
      apps.xxwc_sleep(10);
      wait_for_jobs(p_job_what);
    END LOOP;
  END;

  PROCEDURE send_mail_with_attachement(p_from       IN VARCHAR2
                                      ,p_to         IN VARCHAR2
                                      ,p_good_file  IN VARCHAR2
                                      ,p_exist_file IN VARCHAR2
                                      ,p_bad_file   IN VARCHAR2
                                      ,p_subject    IN VARCHAR2) IS
    v_connection utl_smtp.connection;
    -- mime blocks (the sections of the email body that can become attachments)
    -- must be delimited by a string, this particular string is just an example
    c_mime_boundary CONSTANT VARCHAR2(256) := '-----AABCDEFBBCCC0123456789DE';
  
    p_smtp_hostname VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
    p_smtp_portnum  VARCHAR2(20) := '25';
    v_valid_email   NUMBER := 0;
    e_invalid_email EXCEPTION;
    l_error_message CLOB;
    v_to            VARCHAR2(256);
    l               NUMBER := 0;
  BEGIN
    v_to := p_to;
  
    IF v_to IS NULL THEN
      v_to := 'hdsoracledevelopers@hdsupply.com';
    END IF;
  
    v_connection := utl_smtp.open_connection(p_smtp_hostname
                                            ,p_smtp_portnum);
    utl_smtp.helo(v_connection
                 ,p_smtp_hostname);
    utl_smtp.mail(v_connection
                 ,p_from);
    utl_smtp.rcpt(v_connection
                 ,v_to);
    utl_smtp.open_data(v_connection);
  
    utl_smtp.write_data(v_connection
                       ,'From: ' || p_from || utl_tcp.crlf);
    utl_smtp.write_data(v_connection
                       ,'To: ' || v_to || utl_tcp.crlf);
    utl_smtp.write_data(v_connection
                       ,'Subject: ' || p_subject || utl_tcp.crlf);
    utl_smtp.write_data(v_connection
                       ,'MIME-Version: 1.0' || utl_tcp.crlf);
  
    utl_smtp.write_data(v_connection
                       ,'Content-Type: multipart/mixed; boundary="' || c_mime_boundary || '"' ||
                        utl_tcp.crlf);
    utl_smtp.write_data(v_connection
                       ,utl_tcp.crlf);
    utl_smtp.write_data(v_connection
                       ,'This is a multi-part message in MIME format.' || utl_tcp.crlf);
  
    -- Write attachment contents
    l := 0;
  
    FOR r IN (SELECT *
                FROM xxwc.email_attachment_clobs
               WHERE file_name = p_good_file
               ORDER BY insert_id) LOOP
      l := l + 1;
    
      IF l = 1 THEN
        utl_smtp.write_data(v_connection
                           ,'--' || c_mime_boundary || utl_tcp.crlf);
        utl_smtp.write_data(v_connection
                           ,'Content-Type: text/plain' || utl_tcp.crlf);
      
        -- Set up attachment header
        utl_smtp.write_data(v_connection
                           ,'Content-Disposition: attachment; filename="' || p_good_file || '"' ||
                            utl_tcp.crlf);
        utl_smtp.write_data(v_connection
                           ,utl_tcp.crlf);
      END IF;
    
      utl_smtp.write_data(v_connection
                         ,r.clob_less_32k);
    END LOOP;
  
    --existing items
  
    -- Write attachment contents
    l := 0;
  
    FOR r IN (SELECT *
                FROM xxwc.email_attachment_clobs
               WHERE file_name = p_exist_file
               ORDER BY insert_id) LOOP
      l := l + 1;
    
      IF l = 1 THEN
        utl_smtp.write_data(v_connection
                           ,'--' || c_mime_boundary || utl_tcp.crlf);
        utl_smtp.write_data(v_connection
                           ,'Content-Type: text/plain' || utl_tcp.crlf);
      
        -- Set up attachment header
        utl_smtp.write_data(v_connection
                           ,'Content-Disposition: attachment; filename="' || p_exist_file || '"' ||
                            utl_tcp.crlf);
        utl_smtp.write_data(v_connection
                           ,utl_tcp.crlf);
      END IF;
    
      utl_smtp.write_data(v_connection
                         ,r.clob_less_32k);
    END LOOP;
  
    --bad file
  
    -- Write attachment contents
    l := 0;
  
    FOR r IN (SELECT *
                FROM xxwc.email_attachment_clobs
               WHERE file_name = p_bad_file
               ORDER BY insert_id) LOOP
      l := l + 1;
    
      IF l = 1 THEN
        utl_smtp.write_data(v_connection
                           ,'--' || c_mime_boundary || utl_tcp.crlf);
        utl_smtp.write_data(v_connection
                           ,'Content-Type: text/plain' || utl_tcp.crlf);
      
        -- Set up attachment header
        utl_smtp.write_data(v_connection
                           ,'Content-Disposition: attachment; filename="' || p_bad_file || '"' ||
                            utl_tcp.crlf);
        utl_smtp.write_data(v_connection
                           ,utl_tcp.crlf);
      END IF;
    
      utl_smtp.write_data(v_connection
                         ,r.clob_less_32k);
    END LOOP;
  
    --
    -- End attachment
    utl_smtp.write_data(v_connection
                       ,utl_tcp.crlf);
    utl_smtp.write_data(v_connection
                       ,'--' || c_mime_boundary || '--' || utl_tcp.crlf);
  
    utl_smtp.close_data(v_connection);
    utl_smtp.quit(v_connection);
  EXCEPTION
    WHEN OTHERS THEN
      l_error_message := 'Error_Stack...' || dbms_utility.format_error_stack() ||
                         ' Error_Backtrace...' || dbms_utility.format_error_backtrace();
    
      IF g_conc_request_id > 0 THEN
        fnd_file.put_line(fnd_file.log
                         ,l_error_message);
      END IF;
    
      RAISE;
  END;

  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  PROCEDURE read_coma_dlmtd_string(p_string      IN CLOB
                                  ,p_list_values OUT xxwc_inv_prod_cloning.list_items) IS
    coma_positions xxwc_inv_prod_cloning.comas;
    i              NUMBER;
    ei             NUMBER;
    v_work_list    CLOB;
  BEGIN
    v_work_list := p_string;
  
    IF nvl(substr(v_work_list
                 ,-1
                 ,1)
          ,'*') = ',' THEN
      NULL;
    ELSE
      v_work_list := v_work_list || ',';
    END IF;
  
    BEGIN
      i := 0;
      coma_positions.delete;
      coma_positions(0) := 0;
    
      LOOP
        i := i + 1;
        coma_positions(i) := instr(v_work_list
                                  ,','
                                  ,1
                                  ,i);
      
        IF coma_positions(i) = 0 THEN
          ei := i - 1;
          EXIT;
        END IF;
      END LOOP;
    
      p_list_values.delete;
    
      FOR n IN 0 .. ei LOOP
        IF n > 0 THEN
          p_list_values(n) := substr(v_work_list
                                    ,coma_positions(n - 1) + 1
                                    ,coma_positions(n) - coma_positions(n - 1) - 1);
        END IF;
      END LOOP;
      --  names (ei + 1) := replace(SUBSTR (v_work_list, coma_positions.LAST + 1),' ','');
    
    END;
  EXCEPTION
    WHEN OTHERS THEN
      l_error_message := 'Error_Stack...' || dbms_utility.format_error_stack() ||
                         ' Error_Backtrace...' || dbms_utility.format_error_backtrace();
    
      IF g_conc_request_id > 0 THEN
        fnd_file.put_line(fnd_file.log
                         ,l_error_message);
      END IF;
    
      RAISE;
  END;

  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************

  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  PROCEDURE read_param_list(p_list_id NUMBER) IS
    v_list_values xxwc_inv_prod_cloning.list_items;
    v_run_string  CLOB;
    n             NUMBER := 0;
  BEGIN
    DELETE FROM xxwc.xxwc_param_list_values
     WHERE list_id = p_list_id;
  
    COMMIT;
  
    FOR r IN (SELECT list_id
                    ,list_name -- ,REPLACE (REPLACE (LIST_VALUES, CHR (10), ''), CHR (13), '') LIST_VALUES
                    ,regexp_replace(list_values
                                   ,'[[:cntrl:]]'
                                   ,NULL) list_values
                    ,list_type -- it is clob
                FROM xxwc_param_list
               WHERE list_id = p_list_id) LOOP
      xxwc_inv_prod_cloning.read_coma_dlmtd_string(r.list_values
                                                  ,v_list_values);
    
      FOR k IN v_list_values.first .. v_list_values.last LOOP
        n := 1 + n;
      
        IF v_list_values(k) IS NOT NULL THEN
          INSERT INTO xxwc.xxwc_param_list_values
          VALUES
            (r.list_id
            ,k
            ,r.list_name
            ,r.list_type
            ,rtrim(ltrim(v_list_values(k)
                        ,' ')
                  ,' ')
            ,NULL);
        END IF;
      
        IF MOD(n
              ,10000) = 0 THEN
          COMMIT;
        END IF;
      END LOOP;
    END LOOP;
  
    COMMIT;
  END;

  --**************************************************************************************
  /*******************************************************************************************************************************
  *    $Header mass_clone_item $                                                                                                 *
  *   Module Name: mass_clone_item                                                                                               *
  *                                                                                                                              *
  *   REVISIONS:                                                                                                                 *
  *   Ver        Date        Author                     Description                                                              *
  *   ---------  ----------  ---------------         -------------------------                                                   *
  *   1.0                                             Initial Version                                                            *
  *   1.4        07/25/2018  Naveen Kalidindi         TMS#20180721-00014  - XXWC INV Product Cloning Errors- Post AHH Go Live    *
  ********************************************************************************************************************************/
  --**************************************************************************************
  -- Added p_enable_log parameter in 1.4
  PROCEDURE mass_clone_item(p_errbuf        OUT VARCHAR2
                           ,p_retcode       OUT VARCHAR2
                           ,p_clone_purpose VARCHAR2
                           ,p_item_list_id  VARCHAR2
                           ,p_org_list_id   VARCHAR2
						   ,p_enable_log    VARCHAR2) IS
    l_error_message CLOB;
    v_unique_string VARCHAR2(64);
  
    p_org_code          VARCHAR2(64);
    v_organization_id   NUMBER;
    v_inventory_item_id NUMBER;
    v_from_org_id       NUMBER;
  
    v_primary_uom_code VARCHAR2(64);
  
    p_user_id NUMBER;
  
    l_retcode         VARCHAR2(10) := '0';
    v_temp_table_name VARCHAR2(128);
    -- g_temp_table_name_o   VARCHAR2 (128);
  
    vt_temp_table_part VARCHAR2(128);
    v_continue         NUMBER;
    e_nothing_to_process EXCEPTION;
  BEGIN
    g_enable_log :=SUBSTR(p_enable_log,1,1); -- Added 1.4
    --execute immediate'
    --plan
    --create table to keep all combinations and flags to use for reporting and history
    --use the same approach as original package, only change error handling- need to be saved and send in error report
    --purpose field will be be in the table
  
    l_retcode := '0';
    p_errbuf  := NULL;
    p_user_id := l_user_id;
  
    xxwc_inv_prod_cloning.read_param_list(p_item_list_id);
    xxwc_inv_prod_cloning.read_param_list(p_org_list_id);
  
    INSERT /*+append*/
    INTO xxwc.xxwc_item_mass_clone_archive
      SELECT *
        FROM xxwc.xxwc_item_mass_clone_history
       WHERE insert_date <= SYSDATE - 1;
  
    COMMIT;
  
    DELETE FROM xxwc.xxwc_item_mass_clone_history
     WHERE insert_date <= SYSDATE - 1;
  
    COMMIT;
  
    --EXECUTE IMMEDIATE 'alter table xxwc.xxwc_item_mass_clone_history enable row movement';
  
    --EXECUTE IMMEDIATE 'alter table xxwc.xxwc_item_mass_clone_history shrink space';
  
    -- EXECUTE IMMEDIATE 'alter table xxwc.xxwc_item_mass_clone_history disable row movement';
  
    SELECT dbms_random.string('A'
                             ,24)
      INTO v_unique_string
      FROM dual;
  
    g_temp_table_name_o := 'XXWC.' || v_unique_string || '##';
    v_temp_table_name   := v_unique_string || '##';
  
    vt_temp_table_part := v_unique_string;
  
    FOR r IN (SELECT item.list_value item_number
                    ,org.list_value  org_number
                FROM (SELECT *
                        FROM xxwc.xxwc_param_list_values
                       WHERE list_id = p_item_list_id) item
                    ,(SELECT *
                        FROM xxwc.xxwc_param_list_values
                       WHERE list_id = p_org_list_id) org) LOOP
      INSERT INTO xxwc.xxwc_item_mass_clone_history
        (run_number
        ,clone_id
        ,insert_date
        ,user_id
        ,item_number
        ,org_number
        ,clone_purpose)
      VALUES
        (v_unique_string
        ,xxwc.xxwc_item_mass_clone_history_s.nextval
        ,SYSDATE
        ,p_user_id
        ,r.item_number
        ,r.org_number
        ,p_clone_purpose);
    END LOOP;
  
    COMMIT;
  
    --tttttt
    DECLARE
      v_run_number VARCHAR2(64) := v_unique_string;
    BEGIN
      xxwc_common_tunning_helpers.drop_temp_table('XXWC'
                                                 ,vt_temp_table_part || '1##');
    
      EXECUTE IMMEDIATE 'CREATE TABLE xxwc.' || vt_temp_table_part || '1##
AS
    SELECT mtl_system_items.inventory_item_id
          ,mtl_system_items.primary_uom_code
          ,mtl_system_items.segment1
          ,xxwc.xxwc_item_mass_clone_history.ROWID v_row
          ,xxwc.xxwc_item_mass_clone_history.org_number
          ,  (SELECT organization_id
                  FROM org_organization_definitions
                 WHERE organization_code = xxwc.xxwc_item_mass_clone_history.org_number AND disable_date IS NULL) org_id
      FROM mtl_system_items, xxwc.xxwc_item_mass_clone_history
     WHERE     xxwc.xxwc_item_mass_clone_history.run_number = ''' ||
                        v_run_number || '''
           AND mtl_system_items.segment1 = xxwc.xxwc_item_mass_clone_history.item_number
           AND mtl_system_items.organization_id = 222';
    
      EXECUTE IMMEDIATE 'alter table xxwc.' || vt_temp_table_part || '1## add (error_message clob)';
    
      xxwc_inv_prod_cloning.create_copy_table('XXWC'
                                             ,vt_temp_table_part || '1##'
                                             ,vt_temp_table_part || '2##');
    
      EXECUTE IMMEDIATE 'INSERT/*+APPEND*/ INTO xxwc.' || vt_temp_table_part || '2##
SELECT a.inventory_item_id
      ,a.primary_uom_code
      ,a.segment1
      ,a.v_row
      ,a.org_number
      ,a.org_id
        ,decode(to_char(nvl( a.org_id,1)),''1'' ,(''**'' || ''Invalid Organization Code='' || a.org_number),null)
      || DECODE (
            NVL (b.SEGMENT1, ''N'')
           ,''N''
           ,NULL
           , (''**'' || '' Item= '' || a.segment1 || '' already exists for organization '' || a.org_number || '';''))
  FROM xxwc.' || vt_temp_table_part ||
                        '1## a, mtl_system_items b
 WHERE a.inventory_item_id = b.inventory_item_id(+) AND a.org_id = b.organization_id(+)';
    
      COMMIT;
      -- delete all items exists in oracle
      xxwc_inv_prod_cloning.create_copy_table('XXWC'
                                             ,vt_temp_table_part || '2##'
                                             ,vt_temp_table_part || '1##');
    
      EXECUTE IMMEDIATE 'INSERT/*+APPEND*/ INTO xxwc.' || vt_temp_table_part || '1##
SELECT a.inventory_item_id
      ,a.primary_uom_code
      ,a.segment1
      ,a.v_row
      ,a.org_number
      ,a.org_id
      ,a.error_message
  FROM xxwc.' || vt_temp_table_part || '2## a';
    
      -- where a.error_message not like''%already exists for organization%''';
    
      COMMIT;
      xxwc_common_tunning_helpers.drop_temp_table('XXWC'
                                                 ,vt_temp_table_part || '2##');
    
      xxwc_inv_prod_cloning.create_copy_table('XXWC'
                                             ,'xxwc_item_mass_clone_history'
                                             ,vt_temp_table_part || '3##');
    
      EXECUTE IMMEDIATE 'alter table xxwc.' || vt_temp_table_part || '3## add(error_flag number)';
    
      EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
      INTO  xxwc.' || vt_temp_table_part || '3##
    SELECT a.run_number
          ,a.clone_id
          ,a.insert_date
          ,a.user_id
          ,a.item_number
          ,a.org_number
          ,b.inventory_item_id
          ,b.org_id
          ,a.api_return_code
          ,b.error_message
          ,a.verify_database
          ,b.primary_uom_code
          ,a.sourcing_rule
          ,a.item_cost_price
          ,a.sourcing_rule_in_database
          ,a.clone_purpose
          ,a.sourcing_rule_id
          ,to_number(decode(nvl(dbms_lob.substr(b.error_message,2000,1),''1''), ''1'',''1'',''2'')) error_flag
      FROM xxwc.xxwc_item_mass_clone_history a, xxwc.' || vt_temp_table_part ||
                        '1## b
     WHERE b.v_row = a.ROWID';
    
      COMMIT;
    
      xxwc_inv_prod_cloning.create_copy_table('XXWC'
                                             ,vt_temp_table_part || '3##'
                                             ,vt_temp_table_part || '1##');
    
      EXECUTE IMMEDIATE 'alter table xxwc.' || vt_temp_table_part ||
                        '1##   add(region varchar2(124),district varchar2(124))';
    
      EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
      INTO  xxwc.' || vt_temp_table_part || '1##
    SELECT a.run_number
          ,a.clone_id
          ,a.insert_date
          ,a.user_id
          ,a.item_number
          ,a.org_number
          ,a.item_id
          ,a.org_id
          ,a.api_return_code
          ,a.error_message
          ,a.verify_database
          ,a.primary_uom_code
          ,a.sourcing_rule
          ,a.item_cost_price
          ,a.sourcing_rule_in_database
          ,a.clone_purpose
          ,DECODE (a.error_flag, 1, xxwc_inv_prod_cloning.find_sourcing_rule (a.org_id, a.item_id), NULL)
          ,a.error_flag
          ,DECODE (a.error_flag
                  ,1, (SELECT b.attribute9
                         FROM mtl_parameters b
                        WHERE b.attribute9 IS NOT NULL AND b.organization_id = a.org_id)
                  ,NULL)
          ,DECODE (a.error_flag
                  ,1, (SELECT b.attribute8
                         FROM mtl_parameters b
                        WHERE b.attribute9 IS NOT NULL AND b.organization_id = a.org_id)
                  ,NULL)
      FROM xxwc.' || vt_temp_table_part || '3## a';
    
      COMMIT;
    
      xxwc_inv_prod_cloning.create_copy_table('XXWC'
                                             ,vt_temp_table_part || '1##'
                                             ,vt_temp_table_part || '3##');
    
      EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
      INTO  xxwc.' || vt_temp_table_part || '3##
SELECT run_number
      ,clone_id
      ,insert_date
      ,user_id
      ,item_number
      ,org_number
      ,item_id
      ,org_id
      ,api_return_code
      ,error_message
      ,verify_database
      ,primary_uom_code
      ,DECODE (a.error_flag
                  ,1,(SELECT sourcing_rule_name
                          FROM mrp_sourcing_rules msr
                         WHERE sourcing_rule_id = a.sourcing_rule_id AND ROWNUM = 1) ,NULL)
      ,item_cost_price
      ,sourcing_rule_in_database
      ,clone_purpose
      ,sourcing_rule_id
      ,a.error_flag
      ,a.region
      ,a.district
  FROM xxwc.' || vt_temp_table_part || '1## a';
    
      COMMIT;
    
      xxwc_inv_prod_cloning.create_copy_table('XXWC'
                                             ,vt_temp_table_part || '3##'
                                             ,vt_temp_table_part || '1##');
    
      EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
      INTO  xxwc.' || vt_temp_table_part || '1##
    SELECT run_number
          ,clone_id
          ,insert_date
          ,user_id
          ,item_number
          ,org_number
          ,item_id
          ,org_id
          ,api_return_code
          ,error_message
          ,verify_database
          ,primary_uom_code
          ,a.sourcing_rule
          ,DECODE (a.error_flag
                  ,1, xxwc_inv_prod_cloning.find_item_price (a.sourcing_rule
                                                            ,a.org_id
                                                            ,a.item_id
                                                            ,a.district
                                                            ,a.region))
          ,sourcing_rule_in_database
          ,clone_purpose
          ,sourcing_rule_id
          ,a.error_flag
          ,a.region
          ,a.district
      FROM xxwc.' || vt_temp_table_part || '3## a';
    
      COMMIT;
    
      xxwc_common_tunning_helpers.drop_temp_table('XXWC'
                                                 ,vt_temp_table_part || '3##');
      xxwc_common_tunning_helpers.drop_temp_table('XXWC'
                                                 ,vt_temp_table_part || '2##');
    
      --ALTER TABLE SHRINK SPACE
      DELETE FROM xxwc.xxwc_item_mass_clone_history
       WHERE run_number = v_run_number;
    
      COMMIT;
    
      EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
          INTO  xxwc.xxwc_item_mass_clone_history
        SELECT run_number
      ,clone_id
      ,insert_date
      ,user_id
      ,item_number
      ,org_number
      ,item_id
      ,org_id
      ,api_return_code
      ,error_message
      ,verify_database
      ,primary_uom_code
      ,sourcing_rule
      ,item_cost_price
      ,sourcing_rule_in_database
      ,clone_purpose
      ,sourcing_rule_id
  FROM xxwc.' || vt_temp_table_part || '1##';
    
      COMMIT;
    
      xxwc_common_tunning_helpers.drop_temp_table('XXWC'
                                                 ,vt_temp_table_part || '1##');
    END;
  
    --************************ CREATE TEMP TABLE
    DECLARE
      vl_unique_string VARCHAR2(124) := v_unique_string;
      v_batch_number   NUMBER;
    BEGIN
      BEGIN
        EXECUTE IMMEDIATE 'drop table ' || g_temp_table_name_o;
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
    
      EXECUTE IMMEDIATE '
CREATE TABLE ' || g_temp_table_name_o || ' (
    v_row                       VARCHAR2 (164 BYTE)
   ,run_number                  VARCHAR2 (164 BYTE)
   ,clone_id                    NUMBER
   ,insert_date                 DATE
   ,user_id                     NUMBER
   ,item_number                 VARCHAR2 (164 BYTE)
   ,org_number                  VARCHAR2 (32 BYTE)
   ,item_id                     NUMBER
   ,org_id                      NUMBER
   ,api_return_code             VARCHAR2 (32 BYTE)
   ,error_message               CLOB
   ,verify_database             VARCHAR2 (32 BYTE)
   ,primary_uom_code            VARCHAR2 (24 BYTE)
   ,sourcing_rule               VARCHAR2 (128 BYTE)
   ,item_cost_price             NUMBER
   ,sourcing_rule_in_database   VARCHAR2 (128 BYTE)
   ,clone_purpose               VARCHAR2 (512 BYTE)
   ,sourcing_rule_id            NUMBER
)';
    
      EXECUTE IMMEDIATE 'ALTER TABLE ' || g_temp_table_name_o || ' NOLOGGING';
    
      EXECUTE IMMEDIATE 'ALTER TABLE ' || g_temp_table_name_o || ' parallel 6';
    
      EXECUTE IMMEDIATE 'insert/*+append*/ into ' || g_temp_table_name_o || '
     SELECT a.ROWID , a.*
                     FROM xxwc.xxwc_item_mass_clone_history a
                      WHERE a.run_number = :v_unique_string AND error_message IS NULL'
        USING vl_unique_string;
    
      COMMIT;
    
      SELECT COUNT(1)
        INTO v_continue
        FROM xxwc.xxwc_item_mass_clone_history a
       WHERE a.run_number = v_unique_string
         AND error_message IS NULL;
    
      IF v_continue = 0 THEN
        RAISE e_nothing_to_process;
      END IF;
    
      DECLARE
        v_rows_to_process    NUMBER;
        v_batches_to_process NUMBER;
      BEGIN
        SELECT decode(round(COUNT(1) / 300
                           ,0)
                     ,0
                     ,1)
              ,COUNT(1)
          INTO v_batches_to_process
              ,v_rows_to_process
          FROM xxwc.xxwc_item_mass_clone_history a
         WHERE a.run_number = v_unique_string
           AND error_message IS NULL;
      
        IF v_rows_to_process <= v_batches_to_process THEN
          v_batches_to_process := 1;
        END IF;
      
        IF v_batches_to_process = 0 THEN
          v_batches_to_process := 1;
        END IF;
      
        IF v_batches_to_process > 5 THEN
          v_batches_to_process := 5;
        ELSE
          NULL;
        END IF;
      
        v_batch_number := xxwc_common_tunning_helpers.add_tuning_parameter('XXWC'
                                                                          ,v_temp_table_name
                                                                          ,v_batches_to_process);
      END;
    END;
  
    --************************
    BEGIN
      FOR r IN (SELECT job
                  FROM dba_jobs
                 WHERE what LIKE '%mass_clone_item_parallel%') LOOP
        dbms_job.remove(r.job);
        COMMIT;
      END LOOP;
    END;
  
    --88888888888888
  
    DECLARE
      v_string CLOB;
      --  g_temp_table_name_o   VARCHAR2 (64) := 'xxwc.DJMFXVZDEGNVHHISSWAFZYAL##';
    
    BEGIN
      v_string := 'DECLARE
    v_ex   VARCHAR2 (250);
    v_table_name VARCHAR2 (250):=''' || g_temp_table_name_o || ''';
BEGIN
    FOR r IN (SELECT DISTINCT group_number FROM ' || g_temp_table_name_o || ')
    LOOP
        v_ex := ''BEGIN xxwc_inv_prod_cloning.mass_clone_item_parallel  (''''' ||
                  g_temp_table_name_o || ''''',''|| r.group_number || '',' || g_resp_appl_id || ',' ||
                  g_resp_id || ',' || g_user_id || ',' || g_org_id || '); END;'';

        DECLARE
            v_job   NUMBER;
        BEGIN
            DBMS_JOB.submit (v_job
                            ,v_ex
                            ,SYSDATE
                            ,NULL);
            COMMIT;
        END;
    END LOOP;
END;';
    
      EXECUTE IMMEDIATE v_string;
    END;
  
    wait_for_jobs('mass_clone_item_parallel');
    wait_for_jobs('mass_clone_item_parallel');
  
    --Default Receiving Subinventory Set to "General".
    verify_database(v_unique_string);
  
    -- here after items are created
  
    FOR jk IN (SELECT a.rowid v_row
                     ,a.*
                 FROM xxwc.xxwc_item_mass_clone_history a
                WHERE a.run_number = v_unique_string
                  AND a.verify_database = 'Y') LOOP
      DECLARE
        ex_validation_error EXCEPTION;
        v_msg_dummy        VARCHAR2(1000);
        v_return_status    VARCHAR2(1);
        v_running_message1 CLOB;
        vx_error_message   CLOB;
        v_msg_count        NUMBER;
        v_msg_data         VARCHAR2(2000);
        v_mst_org_id       NUMBER := fnd_profile.value('XXWC_ITEM_MASTER_ORG');
      BEGIN
        receiving_subinventory(jk.org_id
                              ,jk.item_id);
      
        UPDATE mtl_system_items_tl assignee
           SET (assignee.description
              ,assignee.long_description
              ,assignee.source_lang) =
               (SELECT description
                      ,long_description
                      ,source_lang
                  FROM mtl_system_items_tl master
                 WHERE inventory_item_id = jk.item_id
                   AND organization_id = nvl(v_mst_org_id
                                            ,222)
                   AND master."LANGUAGE" = assignee."LANGUAGE")
         WHERE inventory_item_id = jk.item_id
           AND organization_id = jk.org_id;
      
        COMMIT;
      
        IF jk.sourcing_rule_id IS NOT NULL THEN
          assign_item_sourcing_rule(jk.org_id
                                   ,jk.item_id --p_inventory_item_id   IN            NUMBER
                                   ,jk.sourcing_rule_id --p_sourcing_rule_id                     VARCHAR2
                                   ,v_return_status --x_return_status          OUT NOCOPY VARCHAR2
                                   ,v_msg_count --x_msg_count              OUT NOCOPY NUMBER
                                   ,v_msg_data --x_msg_data               OUT NOCOPY VARCHAR2
                                   ,jk.v_row --p_rowid                             VARCHAR2);
                                    );
        
          IF v_return_status != fnd_api.g_ret_sts_success THEN
            IF v_msg_count > 0 THEN
              v_running_message1 := '';
            
              FOR i IN 1 .. v_msg_count LOOP
                fnd_msg_pub.get(i
                               ,fnd_api.g_false
                               ,v_msg_data
                               ,v_msg_dummy);
              
                IF nvl(v_running_message1
                      ,'null') <> v_msg_data THEN
                  v_running_message1 := v_running_message1 ||
                                        substr('Msg' || to_char(i) || ': ' || v_msg_data
                                              ,1
                                              ,255);
                END IF;
              END LOOP;
            ELSE
              v_running_message1 := v_msg_data;
            END IF;
          
            vx_error_message := 'Error when run assign_item_sourcing_rule ' || jk.item_number ||
                                ' for org: ' || jk.org_number || ': inv_item_grp.create_item ' ||
                                REPLACE(v_running_message1
                                       ,'  '
                                       ,' ');
          
            RAISE ex_validation_error;
          END IF;
        END IF;
      
        IF jk.item_cost_price IS NOT NULL THEN
          create_item_cost(jk.item_id --p_inventory_item_id   IN NUMBER
                          ,jk.org_id --p_organization_id     IN NUMBER
                          ,jk.item_cost_price --p_item_cost           IN NUMBER
                          ,jk.primary_uom_code --p_uom_code            IN VARCHAR2
                          ,jk.v_row);
        END IF;
      
        COMMIT;
        --set item category - Sales Velocity ='N'
        v_running_message1 := NULL;
        set_sales_velocity(jk.item_id
                          ,jk.org_id
                          ,v_running_message1);
      
        IF v_running_message1 IS NOT NULL THEN
          vx_error_message := v_running_message1;
          RAISE ex_validation_error;
        END IF;
      EXCEPTION
        WHEN ex_validation_error THEN
          UPDATE xxwc.xxwc_item_mass_clone_history
             SET error_message   = error_message || ' ' || vx_error_message
                ,verify_database = NULL
           WHERE ROWID = jk.v_row;
        WHEN OTHERS THEN
          vx_error_message := vx_error_message || 'Error_Stack...' ||
                              dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                              dbms_utility.format_error_backtrace();
        
          -- RAISE_APPLICATION_ERROR (-20001, V_ERROR_MESSAGE);
        
          UPDATE xxwc.xxwc_item_mass_clone_history
             SET error_message   = error_message || ' ' || vx_error_message
                ,verify_database = NULL
           WHERE ROWID = jk.v_row;
        
          vx_error_message := NULL;
      END;
    END LOOP;
  
    -- end after processing;
  
    COMMIT;
    create_report_files(v_unique_string);
    xxwc_helpers.xxwc_log_debug('Please check your email for reports from the Concurrent Program.');
  
    DECLARE
      v_run_string VARCHAR2(64) := upper(v_unique_string);
    BEGIN
      FOR r IN (SELECT 'DROP TABLE ' || owner || '.' || object_name run_it_ddl
                  FROM all_objects
                 WHERE object_name LIKE '%' || v_run_string || '%##%'
                   AND object_type = 'TABLE') LOOP
        BEGIN
          EXECUTE IMMEDIATE r.run_it_ddl;
        EXCEPTION
          WHEN OTHERS THEN
            NULL;
        END;
      END LOOP;
    END;
  
    p_errbuf  := NULL;
    p_retcode := l_retcode;
  EXCEPTION
    WHEN e_nothing_to_process THEN
      BEGIN
        DECLARE
          v_run_string VARCHAR2(64) := upper(v_unique_string);
        BEGIN
          FOR r IN (SELECT 'DROP TABLE ' || owner || '.' || object_name run_it_ddl
                      FROM all_objects
                     WHERE object_name LIKE '%' || v_run_string || '%##%'
                       AND object_type = 'TABLE') LOOP
            BEGIN
              EXECUTE IMMEDIATE r.run_it_ddl;
            EXCEPTION
              WHEN OTHERS THEN
                NULL;
            END;
          END LOOP;
        END;
      
        create_report_files(v_unique_string);
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
    WHEN OTHERS THEN
    
      l_error_message := 'XXWC_INV_PROD_cloning.mass_clone_item ' || 'Error_Stack...' ||
                         dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                         dbms_utility.format_error_backtrace();
    
      IF g_conc_request_id > 0 THEN
        fnd_file.put_line(fnd_file.log
                         ,l_error_message);
      END IF;
    
      DECLARE
        v_run_string VARCHAR2(64) := upper(v_unique_string);
      BEGIN
        FOR r IN (SELECT 'DROP TABLE ' || owner || '.' || object_name run_it_ddl
                    FROM all_objects
                   WHERE object_name LIKE '%' || v_run_string || '%##%'
                     AND object_type = 'TABLE') LOOP
          BEGIN
            EXECUTE IMMEDIATE r.run_it_ddl;
          EXCEPTION
            WHEN OTHERS THEN
              NULL;
          END;
        END LOOP;
      END;
    
      /* BEGIN
          DELETE FROM xxwc.xxwc_item_mass_clone_history
                WHERE run_number = v_unique_string;
      
          COMMIT;
      EXCEPTION
          WHEN OTHERS
          THEN
              NULL;
      END;*/
    
      l_retcode := '2';
      p_errbuf  := l_error_message;
      p_retcode := l_retcode;
  END;

  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************

  FUNCTION find_sourcing_rule(p_org_to_clone     NUMBER
                             ,p_item_id_to_clone NUMBER) RETURN NUMBER IS
    /******************************************************************************************************************************************
      PROCEDURE : FIND_SOURCING_RULE
    
      REVISIONS:
      Ver            Date        Author              Description
      ---------  ----------   ---------------    --------------------------------------------------------------------------------------
      1.1        09/09/2015    P.Vamshidhar        TMS# 20150626-00113 - Item Mass Clone Tool enhancements
      1.2        10/13/2015    P.Vamshidhar        TMS# 20151012-00313 - Handling Exception in FIND_SOURCING_RULE proceducre
    ******************************************************************************************************************************************/
  
    v_sourcing_rule    VARCHAR2(255);
    v_sourcing_rule_id NUMBER;
    v_district         VARCHAR2(255);
    v_region           VARCHAR2(255);
    n                  NUMBER := 0;
    v_master_org_id    NUMBER := 0;
    g_start_date       DATE := SYSDATE - 1 / 12;
    -- Added below variable in Revision 1.1 @ TMS# 20150626-00113
    l_sec       VARCHAR2(1000);
    l_err_msg   VARCHAR2(1000);
    l_procedure VARCHAR2(1000) := 'FIND_SOURCING_RULE';
  BEGIN
    /*
    a.  Look for the item with in the district - use most common SR assigned,  if not found then,
    b.  Look for the item with in the region - use most common SR assigned, if not found then,
    
    Removed c and added c1 in TMS# 20150626-00113
    c.  Look for Supplier number in MST - then go back to district and find most common SR with Supplier number within the district, if not found then,
    c1. Look nationally by item number and use most common SR assigned, if not found then
    
    Removed d and added d1 in TMS# 20150626-00113
    d.  Look for Supplier number in MST - then go back to Region and find most common SR with Supplier number within the region. If not found then
    d1. Look for Supplier number in MST org, assign the sourcing rule that matches the MST supplier, if not found then,
    
    e.  Look nationally by Supplier number and use most common SR assigned.
    f.  If no common SR can be found or two SR's exist with equal assignments, then use SR created first.  (oldest creation date)
    */
    --find master organization
  
    l_sec := 'Finding Sourcing Rule for District';
  
    SELECT organization_id
      INTO v_master_org_id
      FROM org_organization_definitions
     WHERE organization_code = 'MST';
  
    --find region and district for organization
    FOR r_org IN (SELECT attribute9      region
                        ,attribute8      district
                        ,organization_id
                    FROM mtl_parameters
                   WHERE attribute9 IS NOT NULL
                     AND organization_id = p_org_to_clone) LOOP
      v_district := r_org.district;
      v_region   := r_org.region;
    END LOOP;
  
    -- 1.  Look for the item with in the district - use most common SR assigned
    -- If more than 1 SR is found at any level and no common SR can be identified,
    -- then use the SR with the earliest creation date
    n := 0;
  
    FOR r IN (SELECT m.sourcing_rule_name
                    ,m.count_v
                    ,sr.creation_date
                    ,m.sourcing_rule_id
                FROM (SELECT sourcing_rule_id
                            ,sourcing_rule_name
                            ,COUNT(sourcing_rule_name) count_v
                        FROM apps.xxwc_sr_for_item_cloning
                       WHERE inventory_item_id = p_item_id_to_clone
                         AND district = v_district
                         AND sr_assignment_creation_date < g_start_date
                       GROUP BY sourcing_rule_name
                               ,sourcing_rule_id) m
                    ,mrp_sourcing_rules sr
               WHERE sr.sourcing_rule_id = m.sourcing_rule_id
               ORDER BY count_v DESC
                       ,3       ASC) LOOP
      n := n + 1;
    
      IF n = 1 THEN
        v_sourcing_rule    := r.sourcing_rule_name;
        v_sourcing_rule_id := r.sourcing_rule_id;
        RETURN v_sourcing_rule_id;
      END IF;
    END LOOP;
  
    -- if district search brings nothing look in region:
    -- If more than 1 SR is found at any level and no common SR can be identified,
    -- then use the SR with the earliest creation date
    n := 0;
  
    l_sec := 'Finding Sourcing Rule for Region';
  
    IF v_sourcing_rule IS NULL THEN
      FOR rr IN (SELECT m.sourcing_rule_name
                       ,m.count_v
                       ,sr.creation_date
                       ,m.sourcing_rule_id
                   FROM (SELECT sourcing_rule_id
                               ,sourcing_rule_name
                               ,COUNT(sourcing_rule_name) count_v
                           FROM apps.xxwc_sr_for_item_cloning
                          WHERE inventory_item_id = p_item_id_to_clone
                            AND region = v_region
                            AND sr_assignment_creation_date < g_start_date
                          GROUP BY sourcing_rule_name
                                  ,sourcing_rule_id) m
                       ,mrp_sourcing_rules sr
                  WHERE sr.sourcing_rule_name = m.sourcing_rule_name
                  ORDER BY count_v DESC
                          ,3       ASC) LOOP
        n := n + 1;
      
        IF n = 1 THEN
          v_sourcing_rule    := rr.sourcing_rule_name;
          v_sourcing_rule_id := rr.sourcing_rule_id;
          RETURN v_sourcing_rule_id;
        END IF;
      END LOOP;
    END IF;
  
  
  
    -- Changed for 1.1v -- Start
    -- Added below code by Vamshi in TMS# 20150626-00113 1.1 V
    --3. Look nationally by item number and use most common SR assigned
  
    l_sec := 'Finding Sourcing Rule most commonly used';
    n     := 0;
  
    IF v_sourcing_rule IS NULL THEN
      FOR rnt IN (SELECT m.sourcing_rule_name
                        ,m.count_v
                        ,sr.creation_date
                        ,m.sourcing_rule_id
                    FROM (SELECT sourcing_rule_id
                                ,sourcing_rule_name
                                ,COUNT(sourcing_rule_name) count_v
                            FROM apps.xxwc_sr_for_item_cloning
                           WHERE inventory_item_id = p_item_id_to_clone
                             AND sr_assignment_creation_date < g_start_date
                           GROUP BY sourcing_rule_name
                                   ,sourcing_rule_id) m
                        ,mrp_sourcing_rules sr
                   WHERE sr.sourcing_rule_name = m.sourcing_rule_name
                   ORDER BY count_v DESC
                           ,3       ASC) LOOP
        n := n + 1;
      
        IF n = 1 THEN
          v_sourcing_rule    := rnt.sourcing_rule_name;
          v_sourcing_rule_id := rnt.sourcing_rule_id;
          RETURN v_sourcing_rule_id;
        END IF;
      END LOOP;
    END IF;
  
    --  Fliped section e and 4 -- TMS# 20151012-00313
    --  e.  Look nationally by Supplier number and use most common SR assigned.
    l_sec := 'Finding Sourcing Rule at nationally by Supplier';
  
    IF v_sourcing_rule IS NULL THEN
      FOR rrn IN (SELECT m.sourcing_rule_name
                        ,m.count_v
                        ,sr.creation_date
                        ,m.sourcing_rule_id
                    FROM (SELECT sourcing_rule_id
                                ,sourcing_rule_name
                                ,COUNT(sourcing_rule_name) count_v
                            FROM apps.xxwc_sr_for_item_cloning
                           WHERE inventory_item_id = p_item_id_to_clone
                             AND sr_assignment_creation_date < g_start_date
                           GROUP BY sourcing_rule_name
                                   ,sourcing_rule_id) m
                        ,mrp_sourcing_rules sr
                   WHERE sr.sourcing_rule_name = m.sourcing_rule_name
                   ORDER BY count_v DESC
                           ,3       ASC) LOOP
        n := n + 1;
      
        IF n = 1 THEN
          v_sourcing_rule    := rrn.sourcing_rule_name;
          v_sourcing_rule_id := rrn.sourcing_rule_id;
          RETURN v_sourcing_rule_id;
        END IF;
      END LOOP;
    END IF;
  
    --4. Look for Supplier number in MST org, assign the sourcing rule that matches the MST supplier
    n     := 0;
    l_sec := 'Finding Sourcing Rule at MST Org Supplier Level';
  
    IF v_sourcing_rule IS NULL THEN
      FOR rnt IN (SELECT attribute_char_value vendor_number
                    FROM apps.xxwc_wc_ego_item_mst_attr
                   WHERE attr_display_name = 'Master Vendor Number'
                     AND inventory_item_id = p_item_id_to_clone) LOOP
        n := n + 1;
      
        IF n = 1 THEN
          -- Commented below code by Vamshi in TMS# 20151012-00313
          --               SELECT a.SOURCING_RULE_NAME, a.SOURCING_RULE_ID
          --                 INTO v_sourcing_rule, v_sourcing_rule_id
          --                 FROM MRP_SOURCING_RULES a, MRP_SR_RECEIPT_ORG_V b
          --                WHERE     sourcing_rule_name = rnt.vendor_number
          --                      AND effective_date <= g_start_date
          --                      AND a.sourcing_rule_id = b.Sourcing_rule_id;
          --               RETURN v_sourcing_rule_id;
        
          -- Added below code by Vamshi in TMS# 20151012-00313  -- Start
          BEGIN
            SELECT a.sourcing_rule_name
                  ,a.sourcing_rule_id
              INTO v_sourcing_rule
                  ,v_sourcing_rule_id
              FROM mrp_sourcing_rules   a
                  ,mrp_sr_receipt_org_v b
             WHERE sourcing_rule_name = rnt.vendor_number
               AND effective_date <= g_start_date
               AND a.sourcing_rule_id = b.sourcing_rule_id;
          
            RETURN v_sourcing_rule_id;
          EXCEPTION
            WHEN no_data_found THEN
              v_sourcing_rule_id := NULL;
              n                  := 0;
            WHEN OTHERS THEN
              v_sourcing_rule_id := NULL;
              n                  := 0;
              l_err_msg          := ' Error occured while processing Vendor: ' || l_err_msg;
              xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                          l_procedure
                                                  ,p_calling           => l_sec
                                                  ,p_ora_error_msg     => substr(SQLERRM
                                                                                ,1
                                                                                ,2000)
                                                  ,p_error_desc        => substr(l_err_msg
                                                                                ,1
                                                                                ,240)
                                                  ,p_distribution_list => g_distro_list
                                                  ,p_module            => 'INV');
          END;
          -- Added above code by Vamshi in TMS# 20151012-00313  -- End
        END IF;
      END LOOP;
    END IF;
  
    -- Changed for 1.1v -- End
    /*  Below code Commented by Vamshi in TMS# 20150626-00113
    --3.  Look for Supplier number in MST org, If the supplier is found,
    --then find the most common SR for that supplier in the district.
    --If the Supplier dose not have an SR in the district, then look at the region, if no SR exist in the region
    IF v_sourcing_rule IS NULL
    THEN
       FOR rnt
          IN (SELECT attribute_char_value vendor_number
                FROM apps.xxwc_wc_ego_item_mst_attr
               WHERE     attr_display_name = 'Master Vendor Number'
                     AND inventory_item_id = p_item_id_to_clone)
       LOOP
          --then find the most common SR for that supplier in the district.
          IF v_sourcing_rule IS NULL
          THEN
             FOR rnt1
                IN (  SELECT m.sourcing_rule_name,
                             m.count_v,
                             sr.creation_date,
                             m.sourcing_rule_id
                        FROM (  SELECT sourcing_rule_id,
                                       sourcing_rule_name,
                                       COUNT (sourcing_rule_name) count_v
                                  FROM apps.xxwc_sr_for_item_cloning
                                 WHERE     vendor_number = rnt.vendor_number
                                       AND region = v_district
                                       AND sr_assignment_creation_date <
                                              g_start_date
                              GROUP BY sourcing_rule_name, sourcing_rule_id)
                             m,
                             mrp_sourcing_rules sr
                       WHERE sr.sourcing_rule_name = m.sourcing_rule_name
                    ORDER BY count_v DESC, 3 ASC)
             LOOP
                n := n + 1;
    
                IF n = 1
                THEN
                   v_sourcing_rule := rnt1.sourcing_rule_name;
                   v_sourcing_rule_id := rnt1.sourcing_rule_id;
                   RETURN v_sourcing_rule_id;
                END IF;
             END LOOP;
          END IF;
    
          --If the Supplier dose not have an SR in the district, then look at the region
          IF v_sourcing_rule IS NULL
          THEN
             FOR rnt1
                IN (  SELECT m.sourcing_rule_name,
                             m.count_v,
                             sr.creation_date,
                             m.sourcing_rule_id
                        FROM (  SELECT sourcing_rule_id,
                                       sourcing_rule_name,
                                       COUNT (sourcing_rule_name) count_v
                                  FROM apps.xxwc_sr_for_item_cloning
                                 WHERE     vendor_number = rnt.vendor_number
                                       AND region = v_region
                                       AND sr_assignment_creation_date <
                                              g_start_date
                              GROUP BY sourcing_rule_name, sourcing_rule_id)
                             m,
                             mrp_sourcing_rules sr
                       WHERE sr.sourcing_rule_name = m.sourcing_rule_name
                    ORDER BY count_v DESC, 3 ASC)
             LOOP
                n := n + 1;
    
                IF n = 1
                THEN
                   v_sourcing_rule := rnt1.sourcing_rule_name;
                   v_sourcing_rule_id := rnt1.sourcing_rule_id;
                   RETURN v_sourcing_rule_id;
                END IF;
             END LOOP;
          END IF;
       END LOOP;
    END IF;
     */
  
    RETURN v_sourcing_rule_id; -- Modified in TMS# 20151012-00313
    -- Added below exception in Revision 1.1
  EXCEPTION
    WHEN OTHERS THEN
      v_sourcing_rule_id := NULL;
      l_err_msg          := l_err_msg || ' ...Error_Stack...' || dbms_utility.format_error_stack() ||
                            ' Error_Backtrace...' || dbms_utility.format_error_backtrace();
    
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => substr(SQLERRM
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'INV');
    
      RETURN v_sourcing_rule_id; -- Modified in TMS# 20151012-00313
  END;

  FUNCTION find_item_price(p_sourcing_rule    VARCHAR2
                          ,p_org_to_clone     NUMBER
                          ,p_item_id_to_clone NUMBER
                          ,p_district         VARCHAR2
                          ,p_region           VARCHAR2) RETURN NUMBER IS
    /******************************************************************************************************************************************
      PROCEDURE : FIND_ITEM_PRICE
    
      REVISIONS:
      Ver        Date         Author                     Description
      ---------  ----------   ---------------    --------------------------------------------------------------------------------------
      1.1        09/09/2015    P.Vamshidhar        TMS# 20150626-00113 - Item Mass Clone Tool enhancements
      1.2        10/13/2015    P.Vamshidhar        TMS# 20151012-00313 - Handling Exception in find_item_price proceducre
    ******************************************************************************************************************************************/
  
    v_item_price     NUMBER := NULL;
    v_vendor_id      NUMBER;
    v_vendor_site_id NUMBER;
  
    -- Added below variable in Revision 1.1 @ TMS# 20150626-00113
    v_master_org_id NUMBER := fnd_profile.value('XXWC_ITEM_MASTER_ORG');
    l_sec           VARCHAR2(1000);
    l_err_msg       VARCHAR2(1000);
    l_procedure     VARCHAR2(1000) := 'FIND_ITEM_PRICE';
  BEGIN
    /*3.  List Price Logic
        Commented a and added a1 in TMS# 20150626-00113 by Vamshi
    a.  Look for BPA assigned to SR, if item exist on BPA, then use BPA price.  if item does not exist in BPA then,
    a1.    Look for BPA assigned to SR, if item exist on BPA, then use BPA national price.  if item does not exist in BPA then,
    b.  Look in district and use last receipt PO price, if item does not exist then,
    c.  Look in region and use last receipt PO price, if item does not exist then,
    d.  Look nationally and use last receipt price, if item does not exist then,
        Commented e and added e1 in TMS# 20150626-00113 by Vamshi
    e.  Use $0.00 for PO list price and avg cost
    e1. Check list price at MST and use value for Avg cost. If no value is found use $0.00 as list price and avg cost */
    --a.  Look for BPA assigned to SR, if item exist on BPA, then use BPA price.  if item does not exist in BPA then,
  
    l_sec := 'Finding Item Price -  BPA Level';
  
    IF p_sourcing_rule IS NOT NULL THEN
      FOR r IN (SELECT msr.sourcing_rule_name
                      ,msr.description
                      ,asa.segment1           supplier_number
                      ,asa.vendor_id
                      ,asa.vendor_name        supplier_name
                      ,assa.vendor_site_code  supplier_site
                      ,assa.vendor_site_id
                      ,assa.address_line1
                      ,assa.address_line2
                      ,assa.city
                      ,assa.state
                      ,assa.zip
                      ,asa.end_date_active
                  FROM apps.mrp_sourcing_rules    msr
                      ,apps.mrp_sr_receipt_org    msro
                      ,apps.mrp_sr_source_org     msso
                      ,apps.ap_suppliers          asa
                      ,apps.ap_supplier_sites_all assa
                 WHERE asa.vendor_id = assa.vendor_id
                   AND assa.org_id = 162
                   AND assa.purchasing_site_flag = 'Y'
                   AND assa.vendor_id = msso.vendor_id(+)
                   AND assa.vendor_site_id = msso.vendor_site_id(+)
                   AND msso.source_type(+) = 3
                   AND msso.sr_receipt_id = msro.sr_receipt_id(+)
                   AND trunc(SYSDATE) BETWEEN trunc(msro.effective_date(+)) AND
                       nvl(trunc(msro.disable_date(+))
                          ,trunc(SYSDATE))
                   AND msro.sourcing_rule_id = msr.sourcing_rule_id(+)
                   AND msr.sourcing_rule_name = p_sourcing_rule) LOOP
        v_vendor_id      := r.vendor_id;
        v_vendor_site_id := r.vendor_site_id;
      END LOOP;
    
      --a.  Look for BPA assigned to SR, if item exist on BPA, then use BPA price.  if item does not exist in BPA then,
      FOR x IN (SELECT ph.org_id
                      ,ph.segment1 bpa
                      ,ap.vendor_name
                      ,ap.segment1
                      ,pol.line_num
                      ,pol.item_number
                      ,pol.description
                      ,pol.supplier_item
                      ,pol.unit_price
                      ,pol.shipment_num
                      ,pol.ship_to_org
                      ,pol.quantity
                      ,pol.price_override
                      ,pol.start_date
                      ,pol.end_date
                      ,pol.not_to_exceed_price
                      ,pol.allow_price_override_flag
                      ,pol.item_id
                  FROM po_headers_all ph
                      ,(SELECT pl.item_id
                              ,pl.po_header_id
                              ,pl.po_line_id
                              ,pl.line_num
                              ,msi.segment1 item_number
                              ,msi.description
                              ,pl.vendor_product_num supplier_item --,pl.unit_price   -- Commented in TMS# 20150626-00113 by Vamshi
                              ,xbpz.price_zone_price unit_price -- Added in TMS# 20150626-00113 by Vamshi
                              ,pll.shipment_num
                              ,decode(mp.organization_code
                                     ,'MST'
                                     ,NULL
                                     ,mp.organization_code) ship_to_org
                              ,pll.quantity
                              ,pll.price_override
                              ,pll.start_date
                              ,pll.end_date
                              ,pl.not_to_exceed_price
                              ,pl.allow_price_override_flag
                          FROM po_lines_all            pl
                              ,po_line_locations_all   pll
                              ,mtl_system_items_b      msi
                              ,mtl_parameters          mp
                              ,xxwc_bpa_price_zone_tbl xbpz
                         WHERE pl.item_id = msi.inventory_item_id
                           AND pl.po_line_id = pll.po_line_id(+)
                           AND pll.po_release_id IS NULL
                              --  AND PLL.shipment_type(+) = 'PRICE BREAK'
                              --AND msi.organization_id = fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG') -- Commented in TMS# 20150626-00113
                           AND msi.organization_id = v_master_org_id -- added in  TMS# 20150626-00113
                           AND mp.organization_id(+) = pll.ship_to_organization_id
                           AND nvl(pl.cancel_flag
                                  ,'N') = 'N'
                           AND pl.po_header_id = xbpz.po_header_id(+)
                           AND pl.item_id = xbpz.inventory_item_id(+)
                           AND xbpz.price_zone = 0) pol
                      ,ap_suppliers ap
                 WHERE ph.po_header_id = pol.po_header_id(+)
                   AND ph.type_lookup_code = 'BLANKET'
                   AND ph.vendor_id = ap.vendor_id
                   AND ph.approved_flag = 'Y'
                   AND ph.approved_date < SYSDATE
                   AND ph.vendor_site_id = v_vendor_site_id
                   AND ap.vendor_id = nvl(v_vendor_id
                                         ,99999999999999)
                   AND pol.item_id = p_item_id_to_clone) LOOP
        v_item_price := x.unit_price;
        RETURN v_item_price;
      END LOOP;
    END IF;
  
    --b.  Look in district and use last receipt PO price, if item does not exist then,
  
    l_sec := 'Finding Item Price -  District Level - Last Receipt PO Price';
  
    FOR y IN (SELECT pol.unit_price unit_price
                    ,rt.transaction_type
                    ,rt.organization_id
                    ,rt.vendor_id
                    ,rt.vendor_site_id
                FROM rcv_transactions      rt
                    ,po_headers_all        poh
                    ,po_lines_all          pol
                    ,po_line_locations_all poll
               WHERE --AND rt.transaction_type = 'DELIVER'
               poh.po_header_id = rt.po_header_id
           AND pol.po_line_id = rt.po_line_id
           AND poll.line_location_id = rt.po_line_location_id
           AND destination_type_code = 'RECEIVING'
           AND source_document_code = 'PO'
           AND rt.organization_id IN (SELECT organization_id
                                        FROM mtl_parameters df
                                       WHERE df.attribute9 IS NOT NULL
                                         AND df.attribute8 = p_district)
           AND pol.item_id = p_item_id_to_clone
               ORDER BY rt.transaction_date DESC) LOOP
      v_item_price := y.unit_price;
      RETURN v_item_price;
    END LOOP;
  
    --  c.  Look in region and use last receipt PO price, if item does not exist then,
  
    l_sec := 'Finding Item Price -  Region Level - Last Receipt PO Price';
  
    FOR z IN (SELECT pol.unit_price unit_price
                    ,rt.transaction_type
                    ,rt.organization_id
                    ,rt.vendor_id
                    ,rt.vendor_site_id
                FROM rcv_transactions      rt
                    ,po_headers_all        poh
                    ,po_lines_all          pol
                    ,po_line_locations_all poll
               WHERE --AND rt.transaction_type = 'DELIVER'
               poh.po_header_id = rt.po_header_id
           AND pol.po_line_id = rt.po_line_id
           AND poll.line_location_id = rt.po_line_location_id
           AND destination_type_code = 'RECEIVING'
           AND source_document_code = 'PO'
           AND rt.organization_id IN (SELECT organization_id
                                        FROM mtl_parameters df
                                       WHERE df.attribute9 IS NOT NULL
                                         AND df.attribute9 = p_region)
           AND pol.item_id = p_item_id_to_clone
               ORDER BY rt.transaction_date DESC) LOOP
      v_item_price := z.unit_price;
      RETURN v_item_price;
    END LOOP;
  
    --  d.  Look nationally and use last receipt price, if item does not exist then,
    l_sec := 'Finding Item Price -  National Level';
  
    FOR z IN (SELECT pol.unit_price unit_price
                    ,rt.transaction_type
                    ,rt.organization_id
                    ,rt.vendor_id
                    ,rt.vendor_site_id
                FROM rcv_transactions      rt
                    ,po_headers_all        poh
                    ,po_lines_all          pol
                    ,po_line_locations_all poll
               WHERE --AND rt.transaction_type = 'DELIVER'
               poh.po_header_id = rt.po_header_id
           AND pol.po_line_id = rt.po_line_id
           AND poll.line_location_id = rt.po_line_location_id
           AND destination_type_code = 'RECEIVING'
           AND source_document_code = 'PO'
           AND pol.item_id = p_item_id_to_clone
               ORDER BY rt.transaction_date DESC) LOOP
      v_item_price := z.unit_price;
      RETURN v_item_price;
    END LOOP;
  
    -- e.  Use $0.00 for PO list price and avg cost*/
    l_sec := 'Finding Item Price -  List Price';
  
    IF v_item_price IS NULL THEN
      -- Commented and added below code as per requirement TMS# 20150626-00113 by Vamshi 1.2V
      --5.    Check list price at MST and use value for Avg cost. If no value is found use $0.00 as list price and avg cost
      --v_item_price := 0;
      BEGIN
        SELECT nvl(list_price_per_unit
                  ,0)
          INTO v_item_price
          FROM apps.mtl_system_items_b
         WHERE organization_id = v_master_org_id
           AND inventory_item_id = p_item_id_to_clone;
      
        RETURN v_item_price; -- Added in TMS# 20151012-00313 Rev 1.2
      EXCEPTION
        -- Modified exception part in TMS# 20151012-00313 Rev 1.2
        WHEN no_data_found THEN
          v_item_price := 0;
        WHEN OTHERS THEN
          v_item_price := 0;
          l_err_msg    := 'Error Occured while processing Item ' || p_item_id_to_clone;
          xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                      l_procedure
                                              ,p_calling           => l_sec
                                              ,p_ora_error_msg     => substr(SQLERRM
                                                                            ,1
                                                                            ,2000)
                                              ,p_error_desc        => substr(l_err_msg
                                                                            ,1
                                                                            ,240)
                                              ,p_distribution_list => g_distro_list
                                              ,p_module            => 'INV');
      END;
    END IF;
  
    RETURN v_item_price;
    -- Added below exception in Revision 1.1
  EXCEPTION
    WHEN OTHERS THEN
      v_item_price := 0;
      l_err_msg    := l_err_msg || ' ...Error_Stack...' || dbms_utility.format_error_stack() ||
                      ' Error_Backtrace...' || dbms_utility.format_error_backtrace();
    
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'INV');
      RETURN v_item_price;
  END;

  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************

  PROCEDURE assign_item_sourcing_rule(p_organization_id   IN NUMBER
                                     ,p_inventory_item_id IN NUMBER
                                     ,p_sourcing_rule_id  NUMBER
                                     ,x_return_status     OUT NOCOPY VARCHAR2
                                     ,x_msg_count         OUT NOCOPY NUMBER
                                     ,x_msg_data          OUT NOCOPY VARCHAR2
                                     ,p_rowid             VARCHAR2) IS
    --  l_return_status            VARCHAR2 (1);
    --  l_msg_count                NUMBER := 0;
    -- l_msg_data                 VARCHAR2 (1000);
  
    l_assignment_set_rec     mrp_src_assignment_pub.assignment_set_rec_type;
    l_assignment_set_val_rec mrp_src_assignment_pub.assignment_set_val_rec_type;
    l_assignment_tbl         mrp_src_assignment_pub.assignment_tbl_type;
    l_assignment_val_tbl     mrp_src_assignment_pub.assignment_val_tbl_type;
    o_assignment_set_rec     mrp_src_assignment_pub.assignment_set_rec_type;
    o_assignment_set_val_rec mrp_src_assignment_pub.assignment_set_val_rec_type;
    o_assignment_tbl         mrp_src_assignment_pub.assignment_tbl_type;
    o_assignment_val_tbl     mrp_src_assignment_pub.assignment_val_tbl_type;
    l_assignment_set_id      NUMBER;
    l_inventory_item_id      NUMBER;
  
    l_organization_id NUMBER;
  
    l_ndx NUMBER := 0;
  
    l_rule_id NUMBER;
  
    e_validation_error EXCEPTION;
  
    l_new_sourcing_rule_id NUMBER;
    v_error_message        CLOB;
    v_running_message1     CLOB;
    x_message_list         error_handler.error_tbl_type;
  BEGIN
    l_new_sourcing_rule_id := p_sourcing_rule_id;
    l_inventory_item_id    := p_inventory_item_id;
    l_organization_id      := p_organization_id;
    x_message_list.delete;
  
    SELECT assignment_set_id
      INTO l_assignment_set_id
      FROM mrp_assignment_sets
     WHERE assignment_set_name = 'WC Default';
  
    BEGIN
      SELECT MAX(sourcing_rule_id)
        INTO l_rule_id
        FROM mrp_sourcing_rules
       WHERE sourcing_rule_id = p_sourcing_rule_id;
    EXCEPTION
      WHEN OTHERS THEN
        IF l_new_sourcing_rule_id IS NOT NULL THEN
          v_error_message := 'Invalid Sourcing Rule id =' || l_new_sourcing_rule_id;
          RAISE e_validation_error;
        END IF;
    END;
  
    l_ndx := l_ndx + 1;
    l_assignment_tbl(l_ndx).assignment_set_id := l_assignment_set_id;
    l_assignment_tbl(l_ndx).assignment_type := 6;
    l_assignment_tbl(l_ndx).operation := mrp_globals.g_opr_create; --'CREATE';
    l_assignment_tbl(l_ndx).organization_id := p_organization_id;
    l_assignment_tbl(l_ndx).inventory_item_id := p_inventory_item_id;
  
    l_assignment_tbl(l_ndx).sourcing_rule_id := l_rule_id;
    l_assignment_tbl(l_ndx).sourcing_rule_type := 1;
    --Sourcing Rule
    mrp_src_assignment_pub.process_assignment(p_api_version_number     => 1.0
                                             ,p_init_msg_list          => fnd_api.g_false
                                             ,p_return_values          => fnd_api.g_false
                                             ,p_commit                 => fnd_api.g_false
                                             ,x_return_status          => x_return_status
                                             ,x_msg_count              => x_msg_count
                                             ,x_msg_data               => x_msg_data
                                             ,p_assignment_set_rec     => l_assignment_set_rec
                                             ,p_assignment_set_val_rec => l_assignment_set_val_rec
                                             ,p_assignment_tbl         => l_assignment_tbl
                                             ,p_assignment_val_tbl     => l_assignment_val_tbl
                                             ,x_assignment_set_rec     => o_assignment_set_rec
                                             ,x_assignment_set_val_rec => o_assignment_set_val_rec
                                             ,x_assignment_tbl         => o_assignment_tbl
                                             ,x_assignment_val_tbl     => o_assignment_val_tbl);
    v_error_message := NULL;
  
    IF x_return_status != fnd_api.g_ret_sts_success THEN
      error_handler.get_message_list(x_message_list);
    
      FOR i IN 1 .. x_message_list.count LOOP
        v_running_message1 := v_running_message1 || x_message_list(i).message_text;
      END LOOP;
    
      v_error_message := 'error calling  mrp_src_assignment_pub.process_assignment for item ' ||
                         p_inventory_item_id || ' error:' || v_running_message1;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      v_error_message := v_error_message || 'Error_Stack...' || dbms_utility.format_error_stack() ||
                         ' Error_Backtrace...' || dbms_utility.format_error_backtrace();
    
      -- RAISE_APPLICATION_ERROR (-20001, V_ERROR_MESSAGE);
    
      UPDATE xxwc.xxwc_item_mass_clone_history
         SET error_message = error_message || ' ' || v_error_message
       WHERE ROWID = p_rowid;
  END;

  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --it is copy from XXWC_INV_NEW_PROD_REQ_PKG package
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************

  PROCEDURE create_item_cost(p_inventory_item_id IN NUMBER
                            ,p_organization_id   IN NUMBER
                            ,p_item_cost         IN NUMBER
                            ,p_uom_code          IN VARCHAR2
                            ,p_rowid             VARCHAR2) IS
    l_transaction_type_id      NUMBER;
    l_transaction_interface_id NUMBER;
    l_mat_account              NUMBER;
    l_cost_group_id            NUMBER;
    v_error_message            CLOB;
  BEGIN
    -- concurrent_program_name ='INCTCM' (user name = Process transaction interface) RUNS EVERY 1 MIN
    SELECT transaction_type_id
      INTO l_transaction_type_id
      FROM mtl_transaction_types
     WHERE description = 'Update average cost information';
  
    SELECT mtl_material_transactions_s.nextval
      INTO l_transaction_interface_id
      FROM dual;
  
    SELECT material_account
          ,default_cost_group_id
      INTO l_mat_account
          ,l_cost_group_id
      FROM mtl_parameters
     WHERE organization_id = p_organization_id;
  
    INSERT INTO inv.mtl_transactions_interface
      (transaction_interface_id
      ,transaction_header_id
      ,source_code
      ,source_line_id
      ,source_header_id
      ,process_flag
      ,transaction_mode
      ,last_update_date
      ,last_updated_by
      ,creation_date
      ,created_by
      ,inventory_item_id
      ,organization_id
      ,transaction_date
      ,transaction_source_id
      ,transaction_action_id
      ,transaction_source_type_id
      ,transaction_type_id
      ,material_account
      ,material_overhead_account
      ,resource_account
      ,outside_processing_account
      ,overhead_account
      ,new_average_cost
      ,transaction_quantity
      ,transaction_uom
      ,cost_group_id)
    VALUES
      (l_transaction_interface_id
      ,22222
      ,'ACU'
      ,'1'
      ,'1'
      ,'1'
      ,'3'
      ,SYSDATE
      , --Run date
       fnd_global.user_id
      , --user_id
       SYSDATE
      , --Run date
       fnd_global.user_id
      , --user_id
       p_inventory_item_id
      , --Inventory Item Id
       p_organization_id
      , --Organization Id
       SYSDATE
      , --batch_run_date
       24
      , --transaction action id
       13
      , --transaction source type id
       80
      , --transaction type
       l_transaction_type_id
      ,l_mat_account
      , --material_account
       NULL
      , --,material_overhead_account
       NULL
      , --,resource_account
       NULL
      , --,outside_processing_account
       NULL
      , --,overhead_account
       p_item_cost
      , --new avg cost
       0
      , --Zero
       p_uom_code
      ,l_cost_group_id);
  
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      v_error_message := v_error_message || 'Error for create_item_cost; Error_Stack...' ||
                         dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                         dbms_utility.format_error_backtrace();
    
      UPDATE xxwc.xxwc_item_mass_clone_history
         SET error_message = error_message || ' ' || v_error_message
       WHERE ROWID = p_rowid;
  END;

  --**********************************

  FUNCTION get_staging_error_message(p_row_id IN VARCHAR2) RETURN NUMBER IS
    v_error NUMBER := 0;
  BEGIN
    FOR r IN (SELECT 'a'
                FROM xxwc.xxwc_item_mass_clone_history
               WHERE ROWID = p_row_id
                 AND error_message IS NOT NULL) LOOP
      v_error := 1;
    END LOOP;
  
    RETURN v_error;
  END;

  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  /*******************************************************************************************************************************
  *                                                                                                                              *
  *   Procedure: process_create_item                                                                                             *
  *                                                                                                                              *
  *   PURPOSE:   This Procedure to create items                                                                                  *
  *                                                                                                                              *
  *   REVISIONS:                                                                                                                 *
  *   Ver        Date        Author                     Description                                                              *
  *   ---------  ----------  ---------------         -------------------------                                                   *
  *   1.0                                             Initial Version                                                            *
  *   1.3        11/30/2016  P.Vamshidhar             TMS#20160122-00091  - GL Code Enhancements to leverage GL API
  ********************************************************************************************************************************/

  PROCEDURE process_create_item(p_rowid IN VARCHAR2) IS
    v_return_status VARCHAR2(1);
  
    v_msg_data              VARCHAR2(2000);
    v_mst_org_id            NUMBER := fnd_profile.value('XXWC_ITEM_MASTER_ORG');
    v_cogs_acc              VARCHAR2(64);
    v_sales_acc             VARCHAR2(64);
    v_expn_acc              VARCHAR2(64); -- Added in Rev 1.3
    v_org_cogs_id           NUMBER;
    v_org_sales_id          NUMBER;
    v_org_expn_id           NUMBER; -- Added in Rev 1.3
    v_cost_of_sales_account NUMBER;
    v_sales_account         NUMBER;
    v_expn_account          NUMBER; -- Added in Rev 1.3
    v_revision_rec          inv_item_grp.item_revision_rec_type;
    v_item_rec_in           inv_item_grp.item_rec_type;
    v_item_rec_out          inv_item_grp.item_rec_type;
    v_item_error_tbl        inv_item_grp.error_tbl_type;
    e_validation_error      EXCEPTION;
    e_validation_error_exit EXCEPTION;
  
    v_error_message CLOB;
  
    v_item_template_name VARCHAR2(164) := NULL;
    v_user_item_type     VARCHAR2(164) := NULL;
    lvc_cat_cogs_segs    VARCHAR2(150) := NULL; -- Added in Rev 1.3
    lvc_cat_sales_segs   VARCHAR2(150) := NULL; -- Added in Rev 1.3
    lvc_cat_expn_segs    VARCHAR2(150) := NULL; -- Added in Rev 1.3
    g_clone_id           NUMBER;
    g_run_number         VARCHAR2(200);
    l_api                VARCHAR2(100) := 'Process_Create_Item';
  
  BEGIN
    FOR ro IN (SELECT *
                 FROM xxwc.xxwc_item_mass_clone_history
                WHERE ROWID = p_rowid) LOOP
      -- 1.4
      g_clone_id    := ro.clone_id;
      g_step_number := 0;
      g_run_number  := ro.run_number;
      insert_log(g_run_number
                ,g_clone_id
                ,l_api
                ,' *** Start Of Procedure ***');
      -------
      -- Clear the Item GRP API message table before processing an item
      v_item_error_tbl.delete;
    
      v_item_rec_in.inventory_item_id   := ro.item_id;
      v_item_rec_in.organization_id     := ro.org_id;
      v_item_rec_in.primary_uom_code    := ro.primary_uom_code;
      v_item_rec_in.list_price_per_unit := ro.item_cost_price;
    
      -- 1.4 
      insert_log(g_run_number
                ,g_clone_id
                ,l_api
                ,'Item Rec> ItemID, OrgID, PrimUOM, CostPrice : ' ||
                 v_item_rec_in.inventory_item_id || ', ' || v_item_rec_in.organization_id || ', ' ||
                 v_item_rec_in.primary_uom_code || ', ' || v_item_rec_in.list_price_per_unit);
    
      --FIND item type
      FOR r IN (SELECT a.item_type
                      ,a.inventory_item_status_code
                      ,b.inventory_item_status_code_tl
                  FROM mtl_system_items_vl a
                      ,mtl_item_status     b
                 WHERE a.organization_id = v_mst_org_id
                   AND a.inventory_item_id = ro.item_id
                   AND a.inventory_item_status_code = b.inventory_item_status_code) LOOP
        IF r.inventory_item_status_code_tl IN ('New Item'
                                              ,'New-Price'
                                              ,'New-Purc'
                                              ,'New-EHS'
                                              ,'New-Data Custodian'
                                              ,'Discontinued'
                                              ,'Not for Sale') THEN
          v_error_message := 'Error creating item ' || ro.item_number || ' for org: ' ||
                             ro.org_number || ' :item status is ' ||
                             r.inventory_item_status_code_tl ||
                             ', cannot be cloned by WC Requirements';
          RAISE e_validation_error;
        END IF;
      
        -- 1.4
        insert_log(g_run_number
                  ,g_clone_id
                  ,l_api
                  ,'Invalid Item Type, ' || r.inventory_item_status_code_tl);
      
        FOR rin IN (SELECT meaning user_item_type
                          ,lookup_code item_type
                          ,(CASE meaning
                             WHEN 'Stock Item' THEN
                              'Non-Stock'
                             WHEN 'Non-Stock Item' THEN
                              'Non-Stock'
                             WHEN 'Special' THEN
                              'Special'
                             WHEN 'Intangible' THEN
                              'Intangible'
                             WHEN 'Rental' THEN
                              'Rental - Std'
                             WHEN 'Re-Rental' THEN
                              'Re-Rental'
                             WHEN 'Freight' THEN
                              'Intangible'
                             WHEN 'Service Item' THEN
                              'Fab Service'
                             WHEN 'Repair' THEN
                              'Repair'
                             ELSE
                              'Non-Stock'
                           END) template_name
                      FROM fnd_common_lookups
                     WHERE lookup_type = 'ITEM_TYPE'
                       AND enabled_flag = 'Y'
                       AND SYSDATE BETWEEN nvl(start_date_active
                                              ,SYSDATE) AND
                           nvl(end_date_active
                              ,SYSDATE)
                       AND lookup_code = r.item_type) LOOP
          v_user_item_type     := rin.user_item_type;
          v_item_template_name := rin.template_name;
        END LOOP;
        -- 1.4
        insert_log(g_run_number
                  ,g_clone_id
                  ,l_api
                  ,'Item Type, Template Name' || v_user_item_type || ', ' || v_item_template_name);
      END LOOP;
    
      FOR r1 IN (SELECT c.attribute1
                       ,c.attribute2
                       ,c.attribute8 -- Attribute8 added in Rev 1.3
                   FROM mtl_item_categories       a
                       ,mtl_default_category_sets b
                       ,mtl_categories            c
                  WHERE a.category_set_id = b.category_set_id
                    AND a.organization_id = nvl(v_mst_org_id
                                               ,222)
                    AND b.functional_area_id = 1
                    AND a.inventory_item_id = ro.item_id
                    AND a.category_id = c.category_id) LOOP
        v_cogs_acc  := r1.attribute1;
        v_sales_acc := r1.attribute2;
        v_expn_acc  := r1.attribute8; -- Added in Rev 1.3
      END LOOP;
    
      -- 1.4
      insert_log(g_run_number
                ,g_clone_id
                ,l_api
                ,'COGS Acc, Sales Account, Expense Account #1: ' || v_cogs_acc || ', ' ||
                 v_sales_acc || ', ' || v_expn_acc);
    
      FOR r2 IN (SELECT cost_of_sales_account
                       ,sales_account
                       ,expense_account -- expense account added in Rev 1.3
                   FROM mtl_parameters
                  WHERE organization_id = ro.org_id) LOOP
        v_org_cogs_id  := r2.cost_of_sales_account;
        v_org_sales_id := r2.sales_account;
        v_org_expn_id  := r2.expense_account; -- Added in Rev 1.3
      END LOOP;
    
      -- 1.4
      insert_log(g_run_number
                ,g_clone_id
                ,l_api
                ,'COGS Acc, Sales Account, Expense Account #2: ' || v_cogs_acc || ', ' ||
                 v_sales_acc || ', ' || v_expn_acc);
    
      IF v_cogs_acc IS NULL THEN
        v_cost_of_sales_account := v_org_cogs_id;
      ELSIF v_cogs_acc IS NOT NULL THEN
        -- Commented below code in 1.3
        /*
             FOR r3
                IN (SELECT c.code_combination_id
                      --  into l_gen_acc_id
                      FROM gl_code_combinations b, gl_code_combinations c
                     WHERE     b.code_combination_id = v_org_cogs_id
                           AND b.segment1 = c.segment1
                           AND b.segment2 = c.segment2
                           AND b.segment3 = c.segment3
                           AND b.segment5 = c.segment5
                           AND b.segment6 = c.segment6
                           AND b.segment7 = c.segment7
                           AND c.segment4 = v_cogs_acc)
             LOOP
                v_cost_of_sales_account := r3.code_combination_id;
             END LOOP;
        */
        -- Added below code in 1.3 - Begin
        BEGIN
          SELECT segment1 || '.' || segment2 || '.' || segment3 || '.' || v_cogs_acc || '.' ||
                 segment5 || '.' || segment6 || '.' || segment7
            INTO lvc_cat_cogs_segs
            FROM gl_code_combinations
           WHERE code_combination_id = v_org_cogs_id;
        EXCEPTION
          WHEN OTHERS THEN
            v_cost_of_sales_account := NULL;
        END;
      
        -- 1.4
        insert_log(g_run_number
                  ,g_clone_id
                  ,l_api
                  ,'lvc_cat_cogs_segs, v_cost_of_sales_account, v_org_cogs_id : ' ||
                   lvc_cat_cogs_segs || ', ' || v_cost_of_sales_account || ', ' || v_org_cogs_id);
      
      
        IF lvc_cat_cogs_segs IS NOT NULL THEN
          v_cost_of_sales_account := xxwc_inv_item_acct_update_pkg.create_ccid(lvc_cat_cogs_segs);
          IF v_cost_of_sales_account IS NULL THEN
            v_cost_of_sales_account := v_org_cogs_id;
          END IF;
        END IF;
      
        -- Added above code in 1.3 - End.
      
        -- 1.4
        insert_log(g_run_number
                  ,g_clone_id
                  ,l_api
                  ,'lvc_cat_cogs_segs, v_cost_of_sales_account, v_org_cogs_id  #2: ' ||
                   lvc_cat_cogs_segs || ', ' || v_cost_of_sales_account || ', ' || v_org_cogs_id);
      
      END IF;
    
      -- 1.4
      insert_log(g_run_number
                ,g_clone_id
                ,l_api
                ,'Final v_cost_of_sales_account, v_cogs_acc: ' || lvc_cat_cogs_segs || ', ' ||
                 v_cost_of_sales_account || ', ' || v_cogs_acc);
    
      IF v_sales_acc IS NULL THEN
        v_sales_account := v_org_sales_id;
      ELSIF v_sales_acc IS NOT NULL THEN
        /* -- Commented below code in Rev 1.3
              FOR r4
                 IN (SELECT c.code_combination_id
                       -- into l_gen_acc_id
                       FROM gl_code_combinations b, gl_code_combinations c
                      WHERE     b.code_combination_id = v_org_sales_id
                            AND b.segment1 = c.segment1
                            AND b.segment2 = c.segment2
                            AND b.segment3 = c.segment3
                            AND b.segment5 = c.segment5
                            AND b.segment6 = c.segment6
                            AND b.segment7 = c.segment7
                            AND c.segment4 = v_sales_acc)
              LOOP
                 v_sales_account := r4.code_combination_id;
              END LOOP;
        */
        -- Added below code in 1.3 - Begin
        BEGIN
          SELECT segment1 || '.' || segment2 || '.' || segment3 || '.' || v_sales_acc || '.' ||
                 segment5 || '.' || segment6 || '.' || segment7
            INTO lvc_cat_sales_segs
            FROM gl_code_combinations
           WHERE code_combination_id = v_org_sales_id;
        EXCEPTION
          WHEN OTHERS THEN
            v_sales_account := NULL;
        END;
        -- 1.4
        insert_log(g_run_number
                  ,g_clone_id
                  ,l_api
                  ,'#1 Sales Account lvc_cat_sales_segs, v_sales_acc, v_org_sales_id: ' ||
                   lvc_cat_sales_segs || ', ' || v_sales_acc || ', ' || v_org_sales_id);
      
      
        IF lvc_cat_sales_segs IS NOT NULL THEN
          v_sales_account := xxwc_inv_item_acct_update_pkg.create_ccid(lvc_cat_sales_segs);
          IF v_sales_account IS NULL THEN
            v_sales_account := v_org_sales_id;
          END IF;
        END IF;
      
        -- 1.4
        insert_log(g_run_number
                  ,g_clone_id
                  ,l_api
                  ,'#2 Sales Account lvc_cat_sales_segs, v_sales_account, v_org_sales_id: ' ||
                   lvc_cat_sales_segs || ', ' || v_sales_account || ', ' || v_org_sales_id);
      
        -- Added above code in 1.3 - End.
      
      END IF;
      -- 1.4
      insert_log(g_run_number
                ,g_clone_id
                ,l_api
                ,'Final Sales Account v_sales_acc, v_sales_account, v_org_sales_id: ' ||
                 v_sales_acc || ', ' || v_sales_account || ', ' || v_org_sales_id);
    
      -- Added above code in 1.3 - Begin.
    
      IF v_expn_acc IS NULL THEN
        --v_expn_acc := v_org_expn_id;
        v_expn_account := v_org_expn_id; -- 1.4 Fixed Expense account assignment
      ELSIF v_expn_acc IS NOT NULL THEN
        BEGIN
          SELECT segment1 || '.' || segment2 || '.' || segment3 || '.' || v_expn_acc || '.' ||
                 segment5 || '.' || segment6 || '.' || segment7
            INTO lvc_cat_expn_segs
            FROM gl_code_combinations
           WHERE code_combination_id = v_org_expn_id;
        EXCEPTION
          WHEN OTHERS THEN
            v_expn_account := NULL;
        END;
        -- 1.4
        insert_log(g_run_number
                  ,g_clone_id
                  ,l_api
                  ,'#1 Expense Account lvc_cat_expn_segs, v_expn_account, v_org_expn_id: ' ||
                   lvc_cat_expn_segs || ', ' || v_expn_account || ', ' || v_org_expn_id);
      
      
        IF lvc_cat_expn_segs IS NOT NULL THEN
          v_expn_account := xxwc_inv_item_acct_update_pkg.create_ccid(lvc_cat_expn_segs);
          IF v_expn_account IS NULL THEN
            v_expn_account := v_org_expn_id;
          END IF;
        END IF;
        -- 1.4
        insert_log(g_run_number
                  ,g_clone_id
                  ,l_api
                  ,'#2 Expense Account lvc_cat_expn_segs, v_expn_account, v_org_expn_id: ' ||
                   lvc_cat_expn_segs || ', ' || v_expn_account || ', ' || v_org_expn_id);
      
      END IF;
      -- Added above code in 1.3 - End.
      -- 1.4
      insert_log(g_run_number
                ,g_clone_id
                ,l_api
                ,'Final Expense Account v_expn_acc, v_expn_account, v_org_expn_id: ' || v_expn_acc || ', ' ||
                 v_expn_account || ', ' || v_org_expn_id);
    
    
      --**************************************
      v_item_rec_in.cost_of_sales_account := v_cost_of_sales_account;
      v_item_rec_in.sales_account         := v_sales_account;
      v_item_rec_in.expense_account       := v_expn_account; -- Added in Rev 1.3
      v_item_error_tbl.delete;
      inv_item_grp.create_item(p_commit        => 'T'
                              ,p_item_rec      => v_item_rec_in
                              ,p_revision_rec  => v_revision_rec
                              ,p_template_id   => NULL
                              ,p_template_name => v_item_template_name
                              ,x_item_rec      => v_item_rec_out
                              ,x_return_status => v_return_status
                              ,x_error_tbl     => v_item_error_tbl);
    
      --******************
      v_error_message := NULL;
      -- 1.4
      insert_log(g_run_number
                ,g_clone_id
                ,l_api
                ,' Create Item API Called : ' || v_return_status || ', ' || v_item_error_tbl.count);
    
      IF v_return_status != fnd_api.g_ret_sts_success THEN
        FOR errno IN 1 .. nvl(v_item_error_tbl.last
                             ,0) LOOP
          IF (v_item_error_tbl(errno).message_text IS NOT NULL) THEN
            v_error_message := v_item_error_tbl(errno).message_text || '::' || v_msg_data;
          ELSE
            v_error_message := v_item_error_tbl(errno).message_name || '::' || v_msg_data;
          END IF;
        END LOOP;
      
        v_error_message := 'Error creating item ' || ro.item_number || ' for org: ' ||
                           ro.org_number || ': inv_item_grp.create_item ' ||
                           REPLACE(v_error_message
                                  ,'  '
                                  ,' ');
        RAISE e_validation_error;
      END IF;
      -- 1.4
      insert_log(g_run_number
                ,g_clone_id
                ,l_api
                ,'*** End Of Procedure ***');
    END LOOP;
  EXCEPTION
    WHEN e_validation_error_exit THEN
      NULL;
    WHEN e_validation_error THEN
      UPDATE xxwc.xxwc_item_mass_clone_history
         SET error_message = error_message || ' ' || v_error_message
       WHERE ROWID = p_rowid;
    
      COMMIT;
      -- 1.4
      insert_log(g_run_number
                ,g_clone_id
                ,l_api
                ,'*** Abort Procedure - ' || v_error_message);
    
    WHEN OTHERS THEN
      v_error_message := v_error_message || 'Error_Stack...' || dbms_utility.format_error_stack() ||
                         ' Error_Backtrace...' || dbms_utility.format_error_backtrace();
    
      -- RAISE_APPLICATION_ERROR (-20001, V_ERROR_MESSAGE);
    
      UPDATE xxwc.xxwc_item_mass_clone_history
         SET error_message = error_message || ' ' || v_error_message
       WHERE ROWID = p_rowid;
    
      COMMIT;
      -- 1.4
      insert_log(g_run_number
                ,g_clone_id
                ,l_api
                ,'*** Abort Procedure> Unknown  - ' || v_error_message ||
                 substr(SQLERRM
                       ,1
                       ,250));
    
  END;

  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************

  PROCEDURE verify_database(p_run_number VARCHAR2) IS
    v_count NUMBER := 0;
  BEGIN
    wait_for_jobs('mass_clone_item_parallel');
    wait_for_jobs('mass_clone_item_parallel');
  
    FOR r IN (SELECT ROWID lrowid
                    ,item_id
                    ,org_id
                FROM xxwc.xxwc_item_mass_clone_history
               WHERE run_number = p_run_number
                 AND error_message IS NULL
                 AND nvl(verify_database
                        ,'X') <> 'Y') LOOP
      FOR ru IN (SELECT 'A'
                   FROM mtl_system_items_b i
                  WHERE i.inventory_item_id = r.item_id
                    AND i.organization_id = r.org_id) LOOP
        UPDATE xxwc.xxwc_item_mass_clone_history
           SET verify_database = 'Y'
         WHERE ROWID = r.lrowid;
      
      END LOOP;
    END LOOP;
  
    COMMIT;
  
    FOR r IN (SELECT ROWID lrowid
                    ,item_number
                    ,org_number
                    ,item_id
                    ,org_id
                FROM xxwc.xxwc_item_mass_clone_history
               WHERE run_number = p_run_number
                 AND error_message IS NULL
                 AND nvl(verify_database
                        ,'X') <> 'Y') LOOP
      UPDATE xxwc.xxwc_item_mass_clone_history
         SET verify_database = 'N'
            ,error_message   = 'Item:' || item_number || ' was not created for org:' || org_number ||
                               ' with unknown error;'
       WHERE ROWID = r.lrowid;
    END LOOP;
  
    COMMIT;
  END;

  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************
  --**************************************************************************************

  PROCEDURE create_report_files(p_run_number VARCHAR2) IS
    v_good_file          VARCHAR2(128);
    v_bad_file           VARCHAR2(128);
    v_exist_file         VARCHAR2(128);
    n_loop_counter       NUMBER := 0;
    n_loop_counter_inner NUMBER := 0;
    v_file_data          CLOB := NULL;
    v_dat_length         NUMBER := 0;
  
    v_recipient_email VARCHAR2(164);
    v_sender_address  VARCHAR2(164) := 'donotreply@hdsupply.com';
    v_subject         VARCHAR2(1064);
  
    v_sid        VARCHAR2(64);
    v_request_id VARCHAR2(64);
  BEGIN
    v_request_id := to_char(fnd_global.conc_request_id);
  
    SELECT lower(NAME)
      INTO v_sid
      FROM v$database;
  
    v_good_file  := 'Items cloned in ' || v_sid || ', request_id= ' || v_request_id || '.csv';
    v_exist_file := 'Items already in ' || v_sid || ', request_id= ' || v_request_id || '.csv';
    v_bad_file   := 'Items clone errors in ' || v_sid || ', request_id= ' || v_request_id || '.csv';
  
    UPDATE xxwc.xxwc_item_mass_clone_history
       SET api_return_code = to_char(fnd_global.conc_request_id)
     WHERE run_number = p_run_number;
  
    COMMIT;
  
    EXECUTE IMMEDIATE 'truncate table xxwc.email_attachment_clobs';
  
    -- create with item cloned successfully
    FOR r1 IN (SELECT user_id
                     ,clone_purpose
                 FROM xxwc.xxwc_item_mass_clone_history
                WHERE rownum = 1
                  AND run_number = p_run_number) LOOP
      v_subject := 'Mass Item Clonning report from instance ' || upper(v_sid) || ' batch "' ||
                   r1.clone_purpose || '", request_id= ' || v_request_id;
    
      FOR t IN (SELECT email_address
                  FROM fnd_user
                 WHERE user_id = r1.user_id) LOOP
        v_recipient_email := t.email_address;
      END LOOP;
    
      IF v_recipient_email IS NULL THEN
        v_recipient_email := 'hdsoracledevelopers@hdsupply.com';
      END IF;
    END LOOP;
  
    -- create with item cloned successfully
    --**************************************************************************************
    --**************************************************************************************
  
    FOR r IN (SELECT item_number || ',' || org_number || ',' || primary_uom_code || ',' ||
                     item_cost_price || ',' || sourcing_rule line_to_file
                FROM xxwc.xxwc_item_mass_clone_history
               WHERE run_number = p_run_number
                 AND error_message IS NULL
                 AND verify_database = 'Y') LOOP
      n_loop_counter       := n_loop_counter + 1;
      n_loop_counter_inner := n_loop_counter_inner + 1;
    
      IF n_loop_counter_inner = 1 THEN
        v_file_data := 'Items cloned for batch = ,' || p_run_number || chr(13);
        v_file_data := v_file_data ||
                       'item_number,org_number,primary_uom_code,item_cost_price,sourcing_rule' ||
                       chr(13);
      END IF;
    
      v_file_data  := v_file_data || r.line_to_file || chr(13);
      v_dat_length := v_dat_length + length(v_file_data);
    
      IF v_dat_length >= '25000' THEN
        INSERT INTO xxwc.email_attachment_clobs
          (file_name
          ,clob_less_32k
          ,insert_id)
        VALUES
          (v_good_file
          ,v_file_data
          ,n_loop_counter);
      
        COMMIT;
        v_file_data  := NULL;
        v_dat_length := 0;
      END IF;
    
      COMMIT;
    END LOOP;
  
    --insert last chunk and reset variables
    IF v_file_data IS NOT NULL THEN
      n_loop_counter := n_loop_counter + 1;
    
      INSERT INTO xxwc.email_attachment_clobs
        (file_name
        ,clob_less_32k
        ,insert_id)
      VALUES
        (v_good_file
        ,v_file_data
        ,n_loop_counter);
    
      COMMIT;
    END IF;
  
    v_file_data          := NULL;
    n_loop_counter_inner := 0;
    --  n_loop_counter := 0;
    v_dat_length := 0;
  
    -- create file for items already existed in oracle
    --**************************************************************************************
    --**************************************************************************************
    n_loop_counter := n_loop_counter + 1;
  
    FOR r IN (SELECT item_number || ',' || org_number line_to_file
                FROM xxwc.xxwc_item_mass_clone_history
               WHERE run_number = p_run_number
                 AND error_message IS NOT NULL
                 AND error_message LIKE '%already exists%') LOOP
      n_loop_counter := n_loop_counter + 1;
    
      n_loop_counter_inner := n_loop_counter_inner + 1;
    
      IF n_loop_counter_inner = 1 THEN
        v_file_data := 'Items Already in Oracle for batch =, ' || p_run_number || chr(13);
        v_file_data := v_file_data || 'item_number,org_number' || chr(13);
      END IF;
    
      v_file_data  := v_file_data || r.line_to_file || chr(13);
      v_dat_length := v_dat_length + length(v_file_data);
    
      IF v_dat_length >= '25000' THEN
        INSERT INTO xxwc.email_attachment_clobs
          (file_name
          ,clob_less_32k
          ,insert_id)
        VALUES
          (v_exist_file
          ,v_file_data
          ,n_loop_counter);
      
        COMMIT;
        v_file_data  := NULL;
        v_dat_length := 0;
      END IF;
    
      COMMIT;
    END LOOP;
  
    --insert last chunk and reset variables
    IF v_file_data IS NOT NULL THEN
      n_loop_counter := n_loop_counter + 1;
    
      INSERT INTO xxwc.email_attachment_clobs
        (file_name
        ,clob_less_32k
        ,insert_id)
      VALUES
        (v_exist_file
        ,v_file_data
        ,n_loop_counter);
    
      COMMIT;
    END IF;
  
    v_file_data          := NULL;
    n_loop_counter_inner := 0;
    v_dat_length         := 0;
  
    -- create file for items not cloned
    --**************************************************************************************
    --**************************************************************************************
    n_loop_counter := n_loop_counter + 1;
  
    FOR r IN (SELECT item_number || ',' || org_number || ',' ||
                     REPLACE(error_message
                            ,','
                            ,'*') line_to_file
                FROM xxwc.xxwc_item_mass_clone_history
               WHERE run_number = p_run_number
                 AND error_message IS NOT NULL
                 AND error_message NOT LIKE '%already exists%') LOOP
      n_loop_counter := n_loop_counter + 1;
    
      n_loop_counter_inner := n_loop_counter_inner + 1;
    
      IF n_loop_counter_inner = 1 THEN
        v_file_data := 'Items not cloned for batch =, ' || p_run_number || chr(13);
        v_file_data := v_file_data || 'item_number,org_number,error_message' || chr(13);
      END IF;
    
      v_file_data  := v_file_data || r.line_to_file || chr(13);
      v_dat_length := v_dat_length + length(v_file_data);
    
      IF v_dat_length >= '25000' THEN
        INSERT INTO xxwc.email_attachment_clobs
          (file_name
          ,clob_less_32k
          ,insert_id)
        VALUES
          (v_bad_file
          ,v_file_data
          ,n_loop_counter);
      
        COMMIT;
        v_file_data  := NULL;
        v_dat_length := 0;
      END IF;
    
      COMMIT;
    END LOOP;
  
    --insert last chunk and reset variables
    IF v_file_data IS NOT NULL THEN
      n_loop_counter := n_loop_counter + 1;
    
      INSERT INTO xxwc.email_attachment_clobs
        (file_name
        ,clob_less_32k
        ,insert_id)
      VALUES
        (v_bad_file
        ,v_file_data
        ,n_loop_counter);
    
      COMMIT;
    END IF;
  
    v_file_data          := NULL;
    n_loop_counter_inner := 0;
    v_dat_length         := 0;
  
    send_mail_with_attachement(v_sender_address
                              ,v_recipient_email
                              ,v_good_file
                              ,v_exist_file
                              ,v_bad_file
                              ,v_subject);
  END;

  --Default Receiving Subinventory Set to "General".
  --**************************************************************************************
  --**************************************************************************************

  PROCEDURE receiving_subinventory(p_org_id  NUMBER
                                  ,p_item_id NUMBER) IS
    v_found VARCHAR2(1);
    l_api   VARCHAR2(240) := 'Receiving_Subinventory';
  BEGIN
    BEGIN
      SELECT 'X'
        INTO v_found
        FROM mtl_secondary_inventories
       WHERE secondary_inventory_name = 'General'
         AND nvl(disable_date
                ,SYSDATE + 1) > SYSDATE
         AND organization_id = p_org_id;
    EXCEPTION
      WHEN OTHERS THEN
        v_found := NULL;
    END;
  
    IF v_found = 'X' THEN
      FOR r IN (SELECT b.inventory_item_id
                      ,b.organization_id
                  FROM mtl_system_items_b b
                 WHERE b.organization_id = p_org_id
                   AND b.inventory_item_id = p_item_id
                   AND NOT EXISTS (SELECT 'A'
                          FROM mtl_item_sub_defaults a
                         WHERE a.inventory_item_id = b.inventory_item_id
                           AND a.organization_id = b.organization_id
                           AND a.inventory_item_id = p_item_id)) LOOP
        INSERT INTO mtl_item_sub_defaults
          (inventory_item_id
          ,organization_id
          ,subinventory_code
          ,default_type
          ,last_update_date
          ,last_updated_by
          ,creation_date
          ,created_by)
        VALUES
          (r.inventory_item_id
          ,r.organization_id
          ,'General'
          ,'2'
          ,SYSDATE
          ,l_user_id
          ,SYSDATE
          ,l_user_id);
      END LOOP;
    
      COMMIT;
    END IF;
  END;

  --set item category - Sales Velocity ='N'

  PROCEDURE set_sales_velocity(p_inventory_item_id NUMBER
                              ,p_organization_id   NUMBER
                              ,p_return_message    IN OUT VARCHAR2) IS
    v_p_return_message    VARCHAR2(499) := NULL;
    l_new_category_set_id NUMBER := 0;
    l_new_category_id     NUMBER := 0;
  
    v_error_message CLOB;
    x_return_status VARCHAR2(300);
    x_errorcode     NUMBER;
    x_msg_count     NUMBER;
    x_msg_data      VARCHAR2(1024);
  
    v_running_message1 CLOB;
    x_message_list     error_handler.error_tbl_type;
  BEGIN
    BEGIN
      SELECT mcs.category_set_id
            ,mcv.category_id
        INTO l_new_category_set_id
            ,l_new_category_id
        FROM mtl_categories_kfv mcv
            ,mtl_category_sets  mcs
       WHERE mcs.category_set_name = 'Sales Velocity'
         AND mcs.structure_id = mcv.structure_id
         AND mcv.segment1 = 'N'
         AND enabled_flag = 'Y';
    EXCEPTION
      WHEN OTHERS THEN
        v_p_return_message := 'Invalid item category';
    END;
  
    inv_item_category_pub.create_category_assignment(p_api_version       => 1.0
                                                    ,p_init_msg_list     => 'T'
                                                    ,p_commit            => 'T'
                                                    ,p_category_id       => l_new_category_id
                                                    ,p_category_set_id   => l_new_category_set_id
                                                    ,p_inventory_item_id => p_inventory_item_id
                                                    ,p_organization_id   => p_organization_id
                                                    ,x_return_status     => x_return_status
                                                    ,x_errorcode         => x_errorcode
                                                    ,x_msg_count         => x_msg_count
                                                    ,x_msg_data          => x_msg_data);
  
    IF x_return_status != fnd_api.g_ret_sts_success THEN
      error_handler.get_message_list(x_message_list);
    
      FOR i IN 1 .. x_message_list.count LOOP
        v_running_message1 := v_running_message1 || x_message_list(i).message_text;
      END LOOP;
    
      v_error_message    := 'error calling  inv_item_category_pub.create_category_assignment for item ' ||
                            p_inventory_item_id || ' error:' || v_running_message1;
      v_p_return_message := substr(v_error_message
                                  ,1
                                  ,498);
    END IF;
  
    p_return_message := v_p_return_message;
  END;

  PROCEDURE mass_clone_item_parallel(p_table_name   VARCHAR2
                                    ,p_batch_number NUMBER
                                    ,p_resp_appl_id NUMBER
                                    ,p_resp_id      NUMBER
                                    ,p_user_id      NUMBER
                                    ,p_org_id       NUMBER) IS
    v_retcode         VARCHAR2(2) := '0';
    v_errbuf          VARCHAR2(2000) := NULL;
    v_appl_short_name VARCHAR2(16);
  BEGIN
    BEGIN
      SELECT a.application_short_name
        INTO v_appl_short_name
        FROM fnd_responsibility_vl r
            ,fnd_application       a
       WHERE responsibility_id = p_resp_id
         AND SYSDATE BETWEEN r.start_date AND
             nvl(r.end_date
                ,trunc(SYSDATE) + 1)
         AND r.application_id = a.application_id;
    EXCEPTION
      WHEN no_data_found THEN
        RAISE;
    END;
  
    fnd_global.apps_initialize(user_id      => p_user_id
                              ,resp_id      => p_resp_id
                              , --'HDS Credit Assoc Cash App Mgr - WC'
                               resp_appl_id => p_resp_appl_id);
    -- MO_GLOBAL.INIT ('AR');
    mo_global.init(v_appl_short_name);
    --  MO_GLOBAL.SET_POLICY_CONTEXT ('S', 162);
    mo_global.set_policy_context('S'
                                ,p_org_id);
    --fnd_request.set_org_id ( P_org_id);
    mass_clone_item_parallel_i(v_errbuf
                              ,v_retcode
                              ,p_table_name
                              ,p_batch_number);
  END;

  --parallel submission of CP

  PROCEDURE mass_clone_item_parallel_i(p_errbuf       OUT VARCHAR2
                                      ,p_retcode      OUT VARCHAR2
                                      ,p_table_name   VARCHAR2
                                      ,p_batch_number NUMBER) IS
    v_execute       CLOB;
    l_error_message CLOB;
  BEGIN
    p_retcode := '0';
    p_errbuf  := NULL;
    v_execute := '    BEGIN
    FOR rf IN (SELECT a.* FROM ' || p_table_name || ' a WHERE a.group_number =' ||
                 p_batch_number || ')
    LOOP
        IF rf.org_id <> 222
        THEN
            xxwc_inv_prod_cloning.process_create_item (rf.v_row);
        END IF;
    END LOOP;
END;';
  
    -- xxwc_common_tunning_helpers.write_log ('string' || v_execute);
  
    EXECUTE IMMEDIATE v_execute;
  
    p_retcode := '0';
  EXCEPTION
    WHEN OTHERS THEN
      l_error_message := 'XXWC_INV_PROD_cloning.mass_clone_item ' || 'Error_Stack...' ||
                         dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                         dbms_utility.format_error_backtrace();
    
      IF g_conc_request_id > 0 THEN
        fnd_file.put_line(fnd_file.log
                         ,l_error_message);
      END IF;
    
      p_errbuf  := l_error_message;
      p_retcode := '2';
  END;
END xxwc_inv_prod_cloning;
/