/*************************************************************************
$Header XXWC_PRICING_SEGMENT_TBL_GRANT.sql $
Module Name: XXWC_PRICING_SEGMENT_TBL_GRANT.sql

PURPOSE:   Grant on Table used to load Segment Modifiers to Oracle

REVISIONS:
Ver        Date        Author                  Description
---------  ----------  ---------------         -------------------------
1.0        10/09/2015  Gopi Damuluri           Initial Version
                                               TMS# 20140609-00256
****************************************************************************/

GRANT ALL ON XXWC.XXWC_PRICING_SEGMENT_TBL TO TD002849;
/

GRANT ALL ON XXWC.XXWC_PRICING_SEGMENT_TBL TO RV003897;
/