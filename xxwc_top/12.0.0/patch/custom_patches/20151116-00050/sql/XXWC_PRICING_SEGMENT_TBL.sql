/*************************************************************************
$Header XXWC_PRICING_SEGMENT_TBL.sql $
Module Name: XXWC_PRICING_SEGMENT_TBL.sql

PURPOSE:   This Table is used to load Segment Modifiers to Oracle

REVISIONS:
Ver        Date        Author                  Description
---------  ----------  ---------------         -------------------------
1.0        10/09/2015  Gopi Damuluri           Initial Version
                                               TMS# 20140609-00256
****************************************************************************/

CREATE TABLE xxwc.xxwc_pricing_segment_tbl
(  ACCOUNT_NUMBER        VARCHAR2(30)
 , NAME                  VARCHAR2(240)
 , CONTEXT               VARCHAR2(10)
 , ATTRIBUTE10           VARCHAR2(20)
 , LIST_TYPE_CODE        VARCHAR2(10)
 , AUTOMATIC_FLAG        VARCHAR2(1)
 , OPERATION             VARCHAR2(20)
 , START_DATE_ACTIVE     DATE
 , END_DATE_ACTIVE       DATE
 , STATUS                VARCHAR2(10) DEFAULT 'NEW'
);
/