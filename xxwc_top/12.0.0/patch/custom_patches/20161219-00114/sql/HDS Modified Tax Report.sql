--Report Name            : HDS Modified Tax Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_981488_PTEDYB_V
CREATE OR REPLACE VIEW APPS.XXEIS_981488_PTEDYB_V (CUST_ACCT_ID, ACCOUNT_NUMBER, ACCOUNT_NAME, SITE_NUMBER, SITE_CITY, SITE_STATE, INVOICE_DATE, INVOICE_NUMBER, LINE_NUMBER, SKU_NUMBER, SKU_DESC, TOTAL_INVOICE_AMT_ORIG, TOTAL_INVOICE_AMT_REM, TOTAL_INVOICE_TAX_AMT_ORIG, TOTAL_INVOICE_TAX_AMT_REM, NET_SALES_LINE, LINE_TAX_AMOUNT, TAX_EXEMPTION, TAX_EXEMPTION_TYPE, AVP_CODE, BRANCH_NUMBER, SHIP_VIA, ORDER_LINE_NUMBER, ORDER_NUMBER)
AS
  SELECT hca.cust_account_id cust_acct_id,
    hca.account_number account_number,
    hca.account_name account_name,
    hps.party_site_number site_number,
    hl.city site_city,
    hl.state site_state,
    rcta.trx_date invoice_date,
    rcta.trx_number invoice_number,
    rctla.line_number line_number,
    NVL (msib.segment1, 'UNKNOWN ITEM') sku_number,
    NVL (msib.description, rctla.description) sku_desc,
    apsa.amount_line_items_original total_invoice_amt_orig,
    apsa.amount_line_items_remaining total_invoice_amt_rem,
    apsa.tax_original total_invoice_tax_amt_orig,
    apsa.tax_remaining total_invoice_tax_amt_rem,
    rctla.extended_amount net_sales_line,
    rctla.tax_recoverable line_tax_amount,
    hcasa.attribute16 tax_exemption,
    hcasa.attribute15 tax_exemption_type,
    msib.attribute22 avp_code ,
    mp.organization_code branch_number --added for TMS#20161219-00114 on 2-Feb-17
    ,
    (SELECT flv.meaning
    FROM apps.oe_order_lines_all ool,
      apps.fnd_lookup_values_vl flv
    WHERE TO_CHAR(ool.line_id)  = rctla.interface_line_attribute6
    AND interface_line_context  ='ORDER ENTRY'
    AND ool.shipping_method_code=flv.lookup_code
    AND lookup_type             ='SHIP_METHOD'
    AND view_application_id     =3
    ) ship_via --added for TMS#20161219-00114 on 2-Feb-17
    ,
    (SELECT ool.line_number
    FROM apps.oe_order_lines_all ool
    WHERE TO_CHAR(ool.line_id) = rctla.interface_line_attribute6
    AND interface_line_context ='ORDER ENTRY'
    ) order_line_number --added for TMS#20161219-00114
    ,
    (SELECT ooh.order_number
    FROM apps.oe_order_headers_all ooh
    WHERE TO_CHAR(ooh.order_number)   = rcta.interface_header_attribute1
    AND rcta.interface_header_context ='ORDER ENTRY'
    ) order_number --added for TMS#20161219-00114
  FROM ar.ra_customer_trx_all rcta,
    ar.ra_customer_trx_lines_all rctla,
    apps.mtl_system_items_b msib,
    apps.ar_payment_schedules_all apsa,
    ar.hz_cust_site_uses_all hcsua,
    ar.hz_cust_acct_sites_all hcasa,
    ar.hz_party_sites hps,
    ar.hz_locations hl,
    ar.hz_cust_accounts hca,
    mtl_parameters mp --added for TMS#20161219-00114 on 2-Feb-17
  WHERE rcta.customer_trx_id    = rctla.customer_trx_id
  AND rctla.inventory_item_id   = msib.inventory_item_id
  AND msib.organization_id      = '222'
  AND rcta.trx_number           = apsa.trx_number
  AND apsa.customer_site_use_id = hcsua.site_use_id
  AND hcasa.cust_acct_site_id   = hcsua.cust_acct_site_id
  AND hcasa.party_site_id       = hps.party_site_id
  AND hps.location_id           = hl.location_id
  AND hcasa.cust_account_id     = hca.cust_account_id
  AND rctla.warehouse_id        = mp.organization_id(+)
  AND rcta.trx_date             > '01-JAN-2013' ;
/
prompt Creating Object Data XXEIS_981488_PTEDYB_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_981488_PTEDYB_V
xxeis.eis_rsc_ins.v( 'XXEIS_981488_PTEDYB_V',222,'Paste SQL View for HDS Modified Tax Report','1.0','','','LA023190','APPS','HDS Modified Tax Report View','X9PV','','Y','VIEW','US','','');
--Delete Object Columns for XXEIS_981488_PTEDYB_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_981488_PTEDYB_V',222,FALSE);
--Inserting Object Columns for XXEIS_981488_PTEDYB_V
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','CUST_ACCT_ID',222,'','','','','','LA023190','NUMBER','','','Cust Acct Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','ACCOUNT_NUMBER',222,'','','','','','LA023190','VARCHAR2','','','Account Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','ACCOUNT_NAME',222,'','','','','','LA023190','VARCHAR2','','','Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','SITE_NUMBER',222,'','','','','','LA023190','VARCHAR2','','','Site Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','SITE_CITY',222,'','','','','','LA023190','VARCHAR2','','','Site City','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','SITE_STATE',222,'','','','','','LA023190','VARCHAR2','','','Site State','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','INVOICE_DATE',222,'','','','','','LA023190','DATE','','','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','INVOICE_NUMBER',222,'','','','','','LA023190','VARCHAR2','','','Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','LINE_NUMBER',222,'','','','','','LA023190','NUMBER','','','Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','SKU_NUMBER',222,'','','','','','LA023190','VARCHAR2','','','Sku Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','SKU_DESC',222,'','','','','','LA023190','VARCHAR2','','','Sku Desc','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','TOTAL_INVOICE_AMT_ORIG',222,'','','','~T~D~2','','LA023190','NUMBER','','','Total Invoice Amt Orig','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','TOTAL_INVOICE_AMT_REM',222,'','','','~T~D~2','','LA023190','NUMBER','','','Total Invoice Amt Rem','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','TOTAL_INVOICE_TAX_AMT_ORIG',222,'','','','~T~D~2','','LA023190','NUMBER','','','Total Invoice Tax Amt Orig','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','TOTAL_INVOICE_TAX_AMT_REM',222,'','','','~T~D~2','','LA023190','NUMBER','','','Total Invoice Tax Amt Rem','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','NET_SALES_LINE',222,'','','','','','LA023190','NUMBER','','','Net Sales Line','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','LINE_TAX_AMOUNT',222,'','','','~T~D~2','','LA023190','NUMBER','','','Line Tax Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','TAX_EXEMPTION',222,'','','','','','LA023190','VARCHAR2','','','Tax Exemption','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','TAX_EXEMPTION_TYPE',222,'','','','','','LA023190','VARCHAR2','','','Tax Exemption Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','AVP_CODE',222,'','','','','','LA023190','VARCHAR2','','','Avp Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','BRANCH_NUMBER',222,'Branch Number','','','','','LA023190','VARCHAR2','','','Branch Number','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','SHIP_VIA',222,'Ship Via','','','','','LA023190','VARCHAR2','','','Ship Via','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','ORDER_LINE_NUMBER',222,'Order Line Number','','','','','LA023190','NUMBER','','','Order Line Number','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_981488_PTEDYB_V','ORDER_NUMBER',222,'Order Number','','','','','LA023190','NUMBER','','','Order Number','','','','');
--Inserting Object Components for XXEIS_981488_PTEDYB_V
--Inserting Object Component Joins for XXEIS_981488_PTEDYB_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report HDS Modified Tax Report
prompt Creating Report Data for HDS Modified Tax Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS Modified Tax Report
xxeis.eis_rsc_utility.delete_report_rows( 'HDS Modified Tax Report' );
--Inserting Report - HDS Modified Tax Report
xxeis.eis_rsc_ins.r( 222,'HDS Modified Tax Report','','HDS Modified Tax Report','','','','LA023190','XXEIS_981488_PTEDYB_V','Y','','SELECT hca.cust_account_id cust_acct_id,
  hca.account_number account_number,
  hca.account_name account_name,
  hps.party_site_number site_number,
  hl.city site_city,
  hl.state site_state,
  rcta.trx_date invoice_date,
  rcta.trx_number invoice_number,
  rctla.line_number line_number,
  NVL (msib.segment1, ''UNKNOWN ITEM'') sku_number,
  NVL (msib.description, rctla.description) sku_desc,
  apsa.amount_line_items_original total_invoice_amt_orig,
  apsa.amount_line_items_remaining total_invoice_amt_rem,
  apsa.tax_original total_invoice_tax_amt_orig,
  apsa.tax_remaining total_invoice_tax_amt_rem,
  rctla.extended_amount net_sales_line,
  rctla.tax_recoverable line_tax_amount,
  hcasa.attribute16 tax_exemption,
  hcasa.attribute15 tax_exemption_type,
  msib.attribute22 avp_code
  ,mp.organization_code branch_number --added for TMS#20161219-00114 on 2-Feb-17
  ,(SELECT flv.meaning
  FROM apps.oe_order_lines_all ool,
    apps.fnd_lookup_values_vl flv
  WHERE TO_CHAR(ool.line_id)  = rctla.interface_line_attribute6
  AND interface_line_context  =''ORDER ENTRY''
  AND ool.shipping_method_code=flv.lookup_code
  AND lookup_type             =''SHIP_METHOD''
  AND view_application_id     =3
  ) ship_via --added for TMS#20161219-00114 on 2-Feb-17
  ,(SELECT ool.line_number
  FROM apps.oe_order_lines_all ool
  WHERE TO_CHAR(ool.line_id)  = rctla.interface_line_attribute6
  and interface_line_context  =''ORDER ENTRY''
  ) order_line_number --added for TMS#20161219-00114
  ,(select ooh.order_number
  from apps.oe_order_headers_all ooh
  where to_char(ooh.order_number)  = rcta.interface_header_attribute1
  and rcta.interface_header_context  =''ORDER ENTRY''
  ) order_number --added for TMS#20161219-00114
FROM ar.ra_customer_trx_all rcta,
  ar.ra_customer_trx_lines_all rctla,
  apps.mtl_system_items_b msib,
  apps.ar_payment_schedules_all apsa,
  ar.hz_cust_site_uses_all hcsua,
  ar.hz_cust_acct_sites_all hcasa,
  ar.hz_party_sites hps,
  ar.hz_locations hl,
  ar.hz_cust_accounts hca,
  mtl_parameters mp --added for TMS#20161219-00114 on 2-Feb-17
WHERE rcta.customer_trx_id    = rctla.customer_trx_id
AND rctla.inventory_item_id   = msib.inventory_item_id
AND msib.organization_id      = ''222''
AND rcta.trx_number           = apsa.trx_number
AND apsa.customer_site_use_id = hcsua.site_use_id
AND hcasa.cust_acct_site_id   = hcsua.cust_acct_site_id
AND hcasa.party_site_id       = hps.party_site_id
AND hps.location_id           = hl.location_id
AND hcasa.cust_account_id     = hca.cust_account_id
AND rctla.warehouse_id        = mp.organization_id(+)
AND rcta.trx_date             > ''01-JAN-2013''
','LA023190','','N','White Cap Tax Reporting','','CSV,EXCEL,','N','','','','','','N','APPS','US','','','','');
--Inserting Report Columns - HDS Modified Tax Report
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'TAX_EXEMPTION_TYPE','Tax Exemption Type','','','','default','','13','N','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'ACCOUNT_NUMBER','Account Number','','','','default','','1','N','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'LINE_TAX_AMOUNT','Line Tax Amount','','','~T~D~2','default','','5','N','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'NET_SALES_LINE','Net Sales Line','','','~~~','default','','6','N','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'TAX_EXEMPTION','Tax Exemption','','','','default','','14','N','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'AVP_CODE','Avp Code','','','','default','','7','N','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'CUST_ACCT_ID','Cust Acct Id','','','~~~','default','','15','N','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'SITE_CITY','Site City','','','','default','','11','N','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'ACCOUNT_NAME','Account Name','','','','default','','2','N','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'SITE_NUMBER','Site Number','','','','default','','10','N','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'INVOICE_NUMBER','Invoice Number','','','','default','','3','N','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'SITE_STATE','Site State','','','','default','','12','N','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'BRANCH_NUMBER','Branch','Branch Number','VARCHAR2','','default','','9','','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'SHIP_VIA','Ship Via','Ship Via','VARCHAR2','','default','','8','','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'INVOICE_DATE','Invoice Date','','','','default','','16','','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'SKU_DESC','Sku Desc','','','','default','','24','','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'SKU_NUMBER','Sku Number','','','','default','','23','','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'TOTAL_INVOICE_AMT_ORIG','Total Invoice Amt Orig','','','~T~D~2','default','','18','','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'TOTAL_INVOICE_AMT_REM','Total Invoice Amt Rem','','','~T~D~2','default','','20','','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'TOTAL_INVOICE_TAX_AMT_ORIG','Total Invoice Tax Amt Orig','','','~T~D~2','default','','17','','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'TOTAL_INVOICE_TAX_AMT_REM','Total Invoice Tax Amt Rem','','','~T~D~2','default','','19','','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'ORDER_NUMBER','Order Number','Order Number','NUMBER','~~~','default','','21','','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Modified Tax Report',222,'ORDER_LINE_NUMBER','Order Line Number','Order Line Number','NUMBER','~~~','default','','22','','Y','','','','','','','LA023190','N','N','','XXEIS_981488_PTEDYB_V','','','','US','');
--Inserting Report Parameters - HDS Modified Tax Report
xxeis.eis_rsc_ins.rp( 'HDS Modified Tax Report',222,'Account Name','','ACCOUNT_NAME','IN','','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_981488_PTEDYB_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Modified Tax Report',222,'Account Number','','ACCOUNT_NUMBER','IN','','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_981488_PTEDYB_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Modified Tax Report',222,'Site Number','','SITE_NUMBER','IN','','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_981488_PTEDYB_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Modified Tax Report',222,'Site State','','SITE_STATE','IN','','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_981488_PTEDYB_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Modified Tax Report',222,'Site City','','SITE_CITY','IN','','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_981488_PTEDYB_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Modified Tax Report',222,'Tax Exemption Type','','TAX_EXEMPTION_TYPE','IN','','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_981488_PTEDYB_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Modified Tax Report',222,'Invoice Date From','','INVOICE_DATE','>=','','','DATE','N','Y','7','Y','Y','CONSTANT','LA023190','Y','N','','Start Date','','XXEIS_981488_PTEDYB_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Modified Tax Report',222,'Invoice Date To','','INVOICE_DATE','<=','','','DATE','N','Y','8','Y','Y','CONSTANT','LA023190','Y','N','','End Date','','XXEIS_981488_PTEDYB_V','','','US','');
--Inserting Dependent Parameters - HDS Modified Tax Report
--Inserting Report Conditions - HDS Modified Tax Report
xxeis.eis_rsc_ins.rcnh( 'HDS Modified Tax Report',222,'ACCOUNT_NAME IN :Account Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ACCOUNT_NAME','','Account Name','','','','','XXEIS_981488_PTEDYB_V','','','','','','IN','Y','Y','','','','','1',222,'HDS Modified Tax Report','ACCOUNT_NAME IN :Account Name ');
xxeis.eis_rsc_ins.rcnh( 'HDS Modified Tax Report',222,'ACCOUNT_NUMBER IN :Account Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ACCOUNT_NUMBER','','Account Number','','','','','XXEIS_981488_PTEDYB_V','','','','','','IN','Y','Y','','','','','1',222,'HDS Modified Tax Report','ACCOUNT_NUMBER IN :Account Number ');
xxeis.eis_rsc_ins.rcnh( 'HDS Modified Tax Report',222,'INVOICE_DATE >= :Invoice Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','INVOICE_DATE','','Invoice Date From','','','','','XXEIS_981488_PTEDYB_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',222,'HDS Modified Tax Report','INVOICE_DATE >= :Invoice Date From ');
xxeis.eis_rsc_ins.rcnh( 'HDS Modified Tax Report',222,'INVOICE_DATE <= :Invoice Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','INVOICE_DATE','','Invoice Date To','','','','','XXEIS_981488_PTEDYB_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',222,'HDS Modified Tax Report','INVOICE_DATE <= :Invoice Date To ');
xxeis.eis_rsc_ins.rcnh( 'HDS Modified Tax Report',222,'SITE_CITY IN :Site City ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SITE_CITY','','Site City','','','','','XXEIS_981488_PTEDYB_V','','','','','','IN','Y','Y','','','','','1',222,'HDS Modified Tax Report','SITE_CITY IN :Site City ');
xxeis.eis_rsc_ins.rcnh( 'HDS Modified Tax Report',222,'SITE_NUMBER IN :Site Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SITE_NUMBER','','Site Number','','','','','XXEIS_981488_PTEDYB_V','','','','','','IN','Y','Y','','','','','1',222,'HDS Modified Tax Report','SITE_NUMBER IN :Site Number ');
xxeis.eis_rsc_ins.rcnh( 'HDS Modified Tax Report',222,'SITE_STATE IN :Site State ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SITE_STATE','','Site State','','','','','XXEIS_981488_PTEDYB_V','','','','','','IN','Y','Y','','','','','1',222,'HDS Modified Tax Report','SITE_STATE IN :Site State ');
xxeis.eis_rsc_ins.rcnh( 'HDS Modified Tax Report',222,'TAX_EXEMPTION_TYPE IN :Tax Exemption Type ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TAX_EXEMPTION_TYPE','','Tax Exemption Type','','','','','XXEIS_981488_PTEDYB_V','','','','','','IN','Y','Y','','','','','1',222,'HDS Modified Tax Report','TAX_EXEMPTION_TYPE IN :Tax Exemption Type ');
--Inserting Report Sorts - HDS Modified Tax Report
xxeis.eis_rsc_ins.rs( 'HDS Modified Tax Report',222,'ACCOUNT_NUMBER','ASC','LA023190','1','');
xxeis.eis_rsc_ins.rs( 'HDS Modified Tax Report',222,'INVOICE_NUMBER','ASC','LA023190','2','');
xxeis.eis_rsc_ins.rs( 'HDS Modified Tax Report',222,'','ASC','LA023190','3','');
xxeis.eis_rsc_ins.rs( 'HDS Modified Tax Report',222,'ORDER_NUMBER','ASC','LA023190','4','');
xxeis.eis_rsc_ins.rs( 'HDS Modified Tax Report',222,'ORDER_LINE_NUMBER','ASC','LA023190','5','');
--Inserting Report Triggers - HDS Modified Tax Report
--inserting report templates - HDS Modified Tax Report
--Inserting Report Portals - HDS Modified Tax Report
--inserting report dashboards - HDS Modified Tax Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS Modified Tax Report','222','XXEIS_981488_PTEDYB_V','XXEIS_981488_PTEDYB_V','N','');
--inserting report security - HDS Modified Tax Report
xxeis.eis_rsc_ins.rsec( 'HDS Modified Tax Report','222','','XXWC_CRE_CREDIT_COLL_MGR_NOREC',222,'LA023190','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Modified Tax Report','222','','XXWC_CRE_CREDIT_COLL_MGR',222,'LA023190','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Modified Tax Report','222','','XXWC_CRE_ASSOC_CUST_MAINT',222,'LA023190','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Modified Tax Report','222','','XXWC_CRE_ASSOC_COLLECTIONS',222,'LA023190','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Modified Tax Report','222','','XXWC_CRE_ASSOC_CASH_APP',222,'LA023190','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Modified Tax Report','222','','XXWC_CRE_ASSOC_CASH_APP_MGR',222,'LA023190','','','');
--Inserting Report Pivots - HDS Modified Tax Report
--Inserting Report   Version details- HDS Modified Tax Report
xxeis.eis_rsc_ins.rv( 'HDS Modified Tax Report','','HDS Modified Tax Report','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
