/*************************************************************************
  $Header TMS_20170503_00169_DROP_BKP_TABLE.sql $
  Module Name: TMS_20170503_00169  Drop backup table XXWC_MTL_ONHAND_QTY_DTL_BKP 

  PURPOSE: Drop Backup table 'XXWC.XXWC_MTL_ONHAND_QTY_DTL_BKP'
  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        23-MAY-2017  Sundaramoorthy         TMS#20170503-00169 - Drop Backup Table

**************************************************************************/ 
BEGIN
EXECUTE IMMEDIATE 'DROP TABLE XXWC.XXWC_MTL_ONHAND_QTY_DTL_BKP';
END;
/
