CREATE OR REPLACE PACKAGE APPS.XXWC_INV_AGILITY_EXTRACT_PKG
IS
   /**************************************************************************************************************************************
        $Header XXWC_INV_AGILITY_EXTRACT_PKG $
        Module Name: XXWC_INV_AGILITY_EXTRACT_PKG.pks

        PURPOSE:   This package is called by the concurrent programs
                   Inventory data Extract for Agility system.
        REVISIONS:
        Ver        Date        Author             Description
        ---------  ----------  ---------------   ----------------------------------------------------------------------------------------
         1.0       01/29/2018  P.Vamshidhar      Initial Build - Task ID: 20171101-00037 - Agility PIM Project and Data Transfer from ERP
		 2.0       06-Mar-2018 Niraj K Ranjan    TMS#20171116-00176   Agility PIM Project Catclass Structure
      /***********************************************************************************************************************************

      /***********************************************************************************************************************************
        Procedure : generate_file

        PURPOSE   :This procedure generates inventory data file for Extract Agility system.
        REVISIONS:
        Ver        Date        Author             Description
        ---------  ----------  ---------------   ----------------------------------------------------------------------------------------
         1.0       01/29/2018  P.Vamshidhar      Initial Build - Task ID: 20171101-00037 - Agility PIM Project and Data Transfer from ERP
      ***********************************************************************************************************************************/
   PROCEDURE main (x_errbuf            OUT VARCHAR2,
                   x_retcode           OUT VARCHAR2,
                   p_extract_type   IN     VARCHAR2);

   PROCEDURE delta_extract;

   PROCEDURE FULL_EXTRACT;

   PROCEDURE print_item_data (p_inventory_item_id IN NUMBER);
   
   PROCEDURE Item_status_extract;
   
  /********************************************************************************
  -- PROCEDURE: GEN_CATCLASS_FILE
  --
  -- PURPOSE: procedure to generate catclass structure for Agility PIM
  --
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 2.0       06-Mar-2018 Niraj K Ranjan    TMS#20171116-00176   Agility PIM Project Catclass Structure
  
  *******************************************************************************/
  PROCEDURE gen_catclass_file(p_errbuf      OUT VARCHAR2
                             ,p_retcode     OUT VARCHAR2);
   
END XXWC_INV_AGILITY_EXTRACT_PKG;
/