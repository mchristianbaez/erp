create or replace 
PACKAGE  apps.XXWC_COMMON_UTILITIES_PKG
AS
  /********************************************************************************
  FILE NAME: APPS.XXWC_COMMON_UTILITIES_PKG.pks

  PROGRAM TYPE: PL/SQL Package spec

  PURPOSE: To Changing the file permission to particular directory file

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     03/27/2018    Pattabhi Avula  TMS#20171101-00037 Initial version.
  *******************************************************************************/
  
   PROCEDURE FILE_PERMISSIONS_CHNG ( p_dir_path       IN  VARCHAR2
									,p_file_name      IN  VARCHAR2
                                    );
								 
END XXWC_COMMON_UTILITIES_PKG;
/