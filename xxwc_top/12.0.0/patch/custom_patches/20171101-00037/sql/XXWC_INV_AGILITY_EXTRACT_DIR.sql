/*************************************************************************
  $Header XXWC_INV_AGILITY_FEED_DIR.sql $
  Module Name: XXWC_INV_AGILITY_FEED_DIR
  PURPOSE: To place Agility extract files.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------------
  1.0        16-Jan-2018  P.vamshidhar          TMS#20171101-00037 - Agility PIM Project and Data Transfer from ERP
*******************************************************************************************************************************************/ 
DECLARE
   lvc_environment   VARCHAR2 (100);
   lvc_sql           VARCHAR2 (4000);
BEGIN

   SELECT '/xx_iface/' || LOWER (NAME) || '/outbound/pdh/agility_feed'
     INTO lvc_environment
     FROM v$database;

   lvc_sql :=
         'CREATE OR REPLACE DIRECTORY XXWC_INV_AGILITY_FEED_DIR AS '
      || ''''
      || lvc_environment
      || '''';

   EXECUTE IMMEDIATE lvc_sql;
END;
/