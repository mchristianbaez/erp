/*
-- *********************************************************************************************************************************
-- $Header XXWC_INV_AGILITY_EXTRACT_T $
-- Module Name: XXWC_INV_AGILITY_EXTRACT_T

-- REVISIONS:
-- Ver        Date        Author           Description
-- ---------  ----------  ----------       ----------------
-- 1.0       01/29/2018  P.Vamshidhar      Initial Build - Task ID: 20171101-00037 - Agility PIM Project and Data Transfer from ERP
-- *********************************************************************************************************************************/

   
CREATE TABLE XXWC.XXWC_INV_AGILITY_EXTRACT_T
(
  INVENTORY_ITEM_ID  NUMBER,
  ITEM_TYPE          VARCHAR2(100 BYTE),
  ITEM_STATUS        VARCHAR2(100 BYTE),
  CREATION_DATE      DATE,
  PROCESS_FLAG       VARCHAR2(10 BYTE)
);