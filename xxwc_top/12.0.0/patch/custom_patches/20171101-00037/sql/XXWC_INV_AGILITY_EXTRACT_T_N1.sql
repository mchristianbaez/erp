/*
-- *********************************************************************************************************************************
-- $Header XXWC_INV_AGILITY_EXTRACT_T_N1 $
-- Module Name: XXWC_INV_AGILITY_EXTRACT_T_N1

-- REVISIONS:
-- Ver        Date        Author           Description
-- ---------  ----------  ----------       ----------------
-- 1.0       01/29/2018  P.Vamshidhar      Initial Build - Task ID: 20171101-00037 - Agility PIM Project and Data Transfer from ERP
-- *********************************************************************************************************************************/
   
CREATE INDEX XXWC.XXWC_INV_AGILITY_EXTRACT_T_N1 ON XXWC.XXWC_INV_AGILITY_EXTRACT_T
(INVENTORY_ITEM_ID);