create or replace 
PACKAGE BODY  apps.XXWC_COMMON_UTILITIES_PKG
  /********************************************************************************
  FILE NAME: APPS.XXWC_COMMON_UTILITIES_PKG.pkb

  PROGRAM TYPE: PL/SQL Package body

  PURPOSE: To Changing the file permission to particular directory file

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     03/27/2018    Pattabhi Avula  TMS#20171101-00037 Initial version.
  *******************************************************************************/
  
  AS
  l_package_name VARCHAR2(100) := 'XXWC_COMMON_UTILITIES_PKG.FILE_PERMISSIONS_CHNG';
  l_distro_list  VARCHAR2(100) DEFAULT 'WC-ITDevelopment-U1@HDSupply.com';
  
   PROCEDURE FILE_PERMISSIONS_CHNG ( p_dir_path       IN  VARCHAR2
									,p_file_name      IN  VARCHAR2
                                    )
    IS
	lvc_command         VARCHAR2(500);
	l_result            VARCHAR2(500);
	
	BEGIN
	
	  lvc_command :='chmod 777'
                    ||' '
                    ||p_dir_path
                    ||'/'
                    ||p_file_name;
       BEGIN
        select xxwc_edi_iface_pkg.xxwc_oscommand_run (lvc_command)
        into   l_result
        from   dual;
	   EXCEPTION
	     WHEN OTHERS THEN
		  fnd_file.put_line(fnd_file.log,'File permission Result is '||l_result); 
	   END;
	EXCEPTION
	  WHEN OTHERS THEN
	  fnd_file.put_line(fnd_file.log,'Error is '||SQLERRM);
	END;
	    
END XXWC_COMMON_UTILITIES_PKG;
/