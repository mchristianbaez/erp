/*************************************************************************
  $Header XXWC_PO_SUPP_EMAIL_PDF_DIR.sql $
  Module Name: XXWC_PO_SUPP_EMAIL_PDF_DIR
  PURPOSE: To access PO subject text and pdf files.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------------
  1.1        15-Jun-2017  P.vamshidhar          TMS#20170707-00224 - Custom Dependency Matrix
*******************************************************************************************************************************************/ 
DECLARE
   lvc_environment   VARCHAR2 (100);
   lvc_sql           VARCHAR2 (4000);
BEGIN
   SELECT '/xx_iface/' || LOWER (NAME) || '/outbound/po/email_po_supp'
     INTO lvc_environment
     FROM v$database;

   lvc_sql :=
         'CREATE OR REPLACE DIRECTORY XXWC_PO_SUPP_EMAIL_PDF_DIR AS '
      || ''''
      || lvc_environment
      || '''';

   EXECUTE IMMEDIATE lvc_sql;
END;
/