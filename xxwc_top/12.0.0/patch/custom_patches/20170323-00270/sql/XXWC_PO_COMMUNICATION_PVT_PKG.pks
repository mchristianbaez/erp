CREATE OR REPLACE PACKAGE APPS.XXWC_PO_COMMUNICATION_PVT_PKG
   AUTHID CURRENT_USER
AS
   /******************************************************************************************************
      -- File Name: XXWC_PO_COMMUNICATION_PVT_PKG.pks
      --
      -- PROGRAM TYPE: Package Spec
      --
      -- PURPOSE: Package for sending emails to Suppliers
      -- HISTORY
      -- ==========================================================================================================
      -- ==========================================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- --------------------------------------------------------------------
      -- 1.0     13-JUL-2017   P.Vamshidhar    TMS# 20170323-00270 - Auto-fill E-Mail Address on PO Approval form
      --                                       Initial version
      ************************************************************************************************************/

   PROCEDURE WC_PO_NEW_COMMUNICATION (ITEMTYPE    IN            VARCHAR2,
                                      ITEMKEY     IN            VARCHAR2,
                                      ACTID       IN            NUMBER,
                                      FUNCMODE    IN            VARCHAR2,
                                      RESULTOUT      OUT NOCOPY VARCHAR2);

   FUNCTION PO_COMMUNICATION_PROFILE
      RETURN VARCHAR2;

   FUNCTION PDF_DOWNLOAD (P_PO_NUMBER IN VARCHAR2, P_PO_REVISION IN VARCHAR2)
      RETURN BOOLEAN;


   PROCEDURE EMAIL_REQUEST_SUBMIT (P_PO_NUMBER          IN     NUMBER,
                                   P_REVISION_NUM       IN     NUMBER,
                                   P_EMAIL_SUBJECT      IN     VARCHAR2,
                                   P_EMAIL_BODY_FILE    IN     VARCHAR2,
                                   P_EMAIL_SENDER       IN     VARCHAR2,
                                   P_EMAIL_RECEIVER     IN     VARCHAR2,
                                   P_EMAIL_ATTACHMENT   IN     VARCHAR2,
                                   X_RETURN_STATUS         OUT VARCHAR2);

   PROCEDURE WRITE_LOG (P_MESSAGE IN VARCHAR2);

   --- Below procedure created to handle no wait on program completion.
   PROCEDURE WC_PO_NEW_COMM_NO_WAIT (ITEMTYPE    IN            VARCHAR2,
                                     ITEMKEY     IN            VARCHAR2,
                                     ACTID       IN            NUMBER,
                                     FUNCMODE    IN            VARCHAR2,
                                     RESULTOUT      OUT NOCOPY VARCHAR2);

   PROCEDURE EMAIL_REQUEST_SUBMIT_NO_WAIT (
      P_PO_NUMBER          IN     NUMBER,
      P_REVISION_NUM       IN     NUMBER,
      P_EMAIL_SUBJECT      IN     VARCHAR2,
      P_EMAIL_BODY_FILE    IN     VARCHAR2,
      P_EMAIL_SENDER       IN     VARCHAR2,
      P_EMAIL_RECEIVER     IN     VARCHAR2,
      P_EMAIL_ATTACHMENT   IN     VARCHAR2,
      X_RETURN_STATUS         OUT VARCHAR2);


   PROCEDURE PRE_PROCESS (x_errbuf          OUT VARCHAR2,
                          x_retcode         OUT VARCHAR2,
                          p_po_number    IN     VARCHAR2,
                          p_rev_number   IN     VARCHAR2,
                          p_rec_email    IN     VARCHAR2);

   PROCEDURE Initiate_conc_request (p_po_number     IN     VARCHAR2,
                                    p_rev_number    IN     VARCHAR2,
                                    p_rec_email     IN     VARCHAR2,
                                    x_return_stat      OUT VARCHAR2);
END XXWC_PO_COMMUNICATION_PVT_PKG;
/