/*************************************************************************
  $Header TMS_20180628-00090_XXWC_OM_HEADER_CLOSE.sql $
  Module Name: TMS_20180628-00090_XXWC_OM_HEADER_CLOSE.sql

  PURPOSE:   Created to process the order headers stucked in booked status
             and line  is in Fulfilled status.

  REVISIONS:
  Ver        Date        Author               Description
  ---------  ----------  ---------------      -------------------------
  1.0        07/27/2018  Pattabhi Avula       TMS#20180628-00090
  **************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
 DBMS_OUTPUT.put_line ('TMS: Datafix script    , Before Update');
 -- Updating the headers table
 
UPDATE apps.oe_order_headers_all
   SET FLOW_STATUS_CODE='CLOSED'
      ,open_Flag='N'
 WHERE header_id =71949423;

DBMS_OUTPUT.put_line ('Records updated for order#28222676 -' || SQL%ROWCOUNT);
		   	 
	 
UPDATE apps.oe_order_headers_all
   SET FLOW_STATUS_CODE='CLOSED'
      ,open_Flag='N'
 WHERE header_id =74544922;

DBMS_OUTPUT.put_line ('Records updated for order#28974352 -' || SQL%ROWCOUNT);
		   
COMMIT;

	  DBMS_OUTPUT.put_line ('TMS: 20180628-00090  , End Update');
EXCEPTION
 WHEN OTHERS THEN
 DBMS_OUTPUT.put_line ('TMS: 20180628-00090 , Errors : ' || SQLERRM);
END;
/