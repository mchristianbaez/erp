/*
 TMS:  20170127-00094
 ESMS: 453572 and 288676
 Scope: Fix country code for Real Estate Land Records LN001L1 and NS003L1
 Date: 01/25/2017
*/
set serveroutput on size 1000000;
declare
 --

 --
     procedure print_log(p_message in varchar2) is
     begin
      if apps.fnd_global.conc_request_id >0 then
       apps.fnd_file.put_line(fnd_file.log, p_message);
      else
       dbms_output.put_line(p_message);
      end if;
     end;
 --
begin
     --
     print_log('');     
     print_log('Begin OPN  data fixes.');
    -- 
      begin 
       print_log('@ Land record LN001L1, existing country code : United States');      
       update pn_addresses_all
         set country ='MX'
         where 1 =1
               and address_id =298946;
        --
        print_log('');          
        print_log('Total rows updated: '||sql%rowcount);
        print_log('');          
        --
        if (sql%rowcount >0) then
         --
         commit;
         --              
        end if;
        --
       print_log('@ Land record LN001L1, updated country code : Mexico');           
      exception
       when others then
        rollback;
        print_log('Error in updating ap.ap_expense_report_lines_all, msg ='||sqlerrm);
      end;
    --
      begin 
       print_log('@ Land record NS003L1, existing country code : United States');      
       update pn_addresses_all
         set country ='CA'
         where 1 =1
               and address_id =115948;
        --
        print_log('');          
        print_log('Total rows updated: '||sql%rowcount);
        print_log('');          
        --
        if (sql%rowcount >0) then
         --
         commit;
         --              
        end if;
        --
       print_log('@ Land record NS003L1, updated country code : Canada');           
      exception
       when others then
        rollback;
        print_log('Error in updating ap.ap_expense_report_lines_all, msg ='||sqlerrm);
      end;
    --    
     print_log('End of OPN data fixes.');
     print_log('');
     --    
exception
 when others then
  rollback;
  print_log('Outer block, message ='||sqlerrm);
end;
/