/*************************************************************************
  $Header TMS_20161027-00054_CLOSE_ORDER_LINE.sql $
  Module Name: TMS_20161027-00054  Data Fix script for 22404883

  PURPOSE: Data fix script for 22404883--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        10-OCT-2016  Niraj K Ranjan        TMS#20161027-00054

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20161027-00054    , Before Update');

   update apps.oe_order_lines_all
   set FULFILLED_QUANTITY=1,
       SHIPPING_QUANTITY =1,
	   ACTUAL_SHIPMENT_DATE = TO_DATE('10/25/2016','MM/DD/YYYY'),
	   OPEN_FLAG = 'N',
	   FULFILLED_FLAG = 'Y',
	   FLOW_STATUS_CODE='CLOSED',
	   FULFILLMENT_DATE=TO_DATE('10/25/2016','MM/DD/YYYY')
   where line_id = 82377269
   and header_id= 50440585;

   DBMS_OUTPUT.put_line (
         'TMS: 20161027-00054  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20161027-00054    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161027-00054 , Errors : ' || SQLERRM);
END;
/



