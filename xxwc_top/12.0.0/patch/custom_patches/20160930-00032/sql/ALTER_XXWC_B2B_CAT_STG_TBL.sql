/************************************************************************************************************************************
  $Header ALTER_XXWC_B2B_CAT_STG_TBL.sql $
  Module Name: TMS#20160930-00032 - Kiewit Phase 2 - Features and Benefits issue fix.

  PURPOSE: To alter XXWC.XXWC_B2B_CAT_STG_TBL

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------
  1.0        03-Oct-2016  P.Vamshidhar          TMS#20160930-00032 - Kiewit Phase 2 - Features and Benefits issue fix.

***********************************************************************************************************************************/ 

ALTER TABLE XXWC.XXWC_B2B_CAT_STG_TBL
   MODIFY (ERROR_MESSAGE VARCHAR2 (4000))
/