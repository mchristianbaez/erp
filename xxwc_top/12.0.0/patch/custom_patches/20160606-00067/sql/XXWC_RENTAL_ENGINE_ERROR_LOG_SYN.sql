 /**********************************************************************************
  -- Synonym Script: XXWC_RENTAL_ENGINE_ERROR_LOG.sql
  -- *******************************************************************************
  --
  -- PURPOSE: Synonym for table XXWC.XXWC_RENTAL_ENGINE_ERROR_LOG
  -- HISTORY
  -- ===============================================================================
  -- ===============================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -----------------------------------------
  -- 1.0     08-Jun-2016   Niraj K Ranjan   Initial Version
************************************************************************************/
CREATE OR REPLACE SYNONYM XXWC_RENTAL_ENGINE_ERROR_LOG FOR XXWC.XXWC_RENTAL_ENGINE_ERROR_LOG;