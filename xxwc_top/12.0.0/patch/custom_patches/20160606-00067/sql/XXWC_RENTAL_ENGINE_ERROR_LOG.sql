DROP TABLE XXWC.XXWC_RENTAL_ENGINE_ERROR_LOG CASCADE CONSTRAINTS;
/*******************************************************************************************************
  -- Table Name XXWC_RENTAL_ENGINE_ERROR_LOG
  -- ***************************************************************************************************
  --
  -- PURPOSE: Custom table used to log errorneous order
  -- HISTORY
  -- ===================================================================================================
  -- ===================================================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------------
  -- 1.0     08-Jun-2016   Niraj K Ranjan   Initial Version
********************************************************************************************************/
CREATE TABLE XXWC.XXWC_RENTAL_ENGINE_ERROR_LOG
(ORDER_HEADER_ID       NUMBER NOT NULL,
 ORDER_LINE_ID         NUMBER,
 REQUEST_ID            NUMBER NOT NULL,
 ERROR_CODE            VARCHAR2(200),
 ERROR_INDEX           NUMBER,
 ERROR_MESSAGE         VARCHAR2(3000),
 CREATION_DATE         DATE,
 CREATED_BY            NUMBER,
 LAST_UPDATE_DATE      DATE,
 LAST_UPDATED_BY       NUMBER,
 LAST_UPDATE_LOGIN     NUMBER 
);