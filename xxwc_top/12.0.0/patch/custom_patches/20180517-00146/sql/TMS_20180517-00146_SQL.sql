WHENEVER SQLERROR EXIT FAILURE ROLLBACK;
set serveroutput on size 999999
/*************************************************************************
      PURPOSE:  Drop Sub inventory in Branch 125 

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        03-JUN-2018   Sundaramoorthy     Initial Version - TMS #20180517-00146   
	************************************************************************/ 
declare
   cursor C1 is select /*+ index(moqd MTL_ONHAND_QUANTITIES_N5) */
               moqd.organization_id org, moqd.subinventory_code sub, moqd.inventory_item_id item,
               moqd.lot_number,
               moqd.revision, 
               moqd.locator_id, 
               sum(primary_transaction_quantity) trx_qty 
               from mtl_onhand_quantities_detail moqd
                where locator_id is null
                AND PLANNING_ORGANIZATION_ID = ORGANIZATION_ID
                AND OWNING_ORGANIZATION_ID = ORGANIZATION_ID
                AND PLANNING_TP_TYPE = 2
                AND OWNING_TP_TYPE = 2
                AND NVL(SECONDARY_TRANSACTION_QUANTITY,0) = 0
                and exists (select 1 from mtl_secondary_inventories msi
                            where msi.secondary_inventory_name = moqd.subinventory_code
                            and   msi.organization_id = moqd.organization_id
                            and   msi.locator_type in (2,3) )
                and exists (select 1 from mtl_parameters
                            where organization_id = moqd.organization_id
                            and  STOCK_LOCATOR_CONTROL_CODE = 4
                            AND  NOT (wms_enabled_flag = 'Y' OR process_enabled_flag = 'Y'))
                and exists (select 1 from mtl_system_items msit
                            where msit.organization_id = moqd.organization_id
                            and   msit.inventory_item_id = moqd.inventory_item_id
                            and   msit.serial_number_control_code in (1, 6) )
                group by moqd.organization_id, moqd.subinventory_code, moqd.inventory_item_id,
                      moqd.lot_number,moqd.revision,moqd.locator_id;
l_temp_id    Number;
l_header_id  Number;
l_prdid Number;
l_description VARCHAR2(2000);
l_location_control_code  Number;
l_restrict_subinv_code Number;
l_restrict_locators_code Number; 
l_revision_qty_control_code Number;
l_primary_uom_code VARCHAR2(3);
l_shelf_life_code Number;
l_shelf_life_days Number;
l_lot_control_code Number;
l_serial_control_code Number;
l_allowed_units_lookup_code Number;
l_locator_id Number;
l_dist_account_id  NUMBER;
l_lot_number VARCHAR2(30);
l_revision VARCHAR2(30);
l_revision_control_code NUMBER;
l_proj_enabled NUMBER;

begin

  dbms_output.put_line('Inside null_loc_fix.sql');
  dbms_output.put_line('please enter valid Distribution account_id ');
  l_dist_account_id := 6383; 

  select mtl_material_transactions_s.nextval
  into l_header_id from dual;
  dbms_output.put_line('Seq ');
   --Select header_id for Transactions

   l_proj_enabled := 2;
   
for c1_rec in C1 loop


      exit when C1%notfound;

      l_locator_id := -1;

      select 
             description 
            ,location_control_code
            ,restrict_subinventories_code
            ,restrict_locators_code
            ,revision_qty_control_code
            ,primary_uom_code
            ,shelf_life_code
            ,shelf_life_days
            ,nvl(lot_control_code,1)
            ,serial_number_control_code 
            ,allowed_units_lookup_code 
            ,REVISION_QTY_CONTROL_CODE
      into   l_description
            ,l_location_control_code
            ,l_restrict_subinv_code
            ,l_restrict_locators_code
            ,l_revision_qty_control_code
            ,l_primary_uom_code
            ,l_shelf_life_code
            ,l_shelf_life_days
            ,l_lot_control_code
            ,l_serial_control_code
            ,l_allowed_units_lookup_code
            ,l_revision_control_code
      from mtl_system_items
      where  inventory_item_id = c1_rec.item
      and    organization_id =  c1_rec.org;

		/* Added to take care of transfer of onhand from null locators to physical locators only */
	  select project_reference_enabled 
	  into l_proj_enabled 
	  from mtl_parameters
	  where organization_id =  c1_rec.org;
	  
	  if l_proj_enabled = 1 then /* For PJM org */ 
			if (l_restrict_locators_code = 1) then
					 begin
					   select INVENTORY_LOCATION_ID into l_locator_id from mtl_item_locations
					   where (DISABLE_DATE > SYSDATE or DISABLE_DATE IS NULL)
					   and   SUBINVENTORY_CODE = c1_rec.sub
					   and   ORGANIZATION_ID = c1_rec.org
					   and   INVENTORY_LOCATION_ID IN
							 (select SECONDARY_LOCATOR FROM MTL_SECONDARY_LOCATORS
							  where  INVENTORY_ITEM_ID = c1_rec.item
							  and    ORGANIZATION_ID = c1_rec.org
							  and    SUBINVENTORY_CODE = c1_rec.sub)
                                 and segment19 is null
                                 and segment20 is null
					   and   ROWNUM < 2;
					 exception
					   when no_data_found then
						 dbms_output.put_line('No physical locator defined in sub='||c1_rec.sub||', org_id'||c1_rec.org);					   
						 dbms_output.put_line('No locator defined for item_id='||c1_rec.item||', sub='||c1_rec.sub||', org_id'||c1_rec.org);
						 dbms_output.put_line('Pls. define at least one physical locator in this subinventory for this item');						 
						 l_locator_id := -1;
					 end;
				  elsif (l_restrict_locators_code = 2) then
					 begin
					   select INVENTORY_LOCATION_ID into l_locator_id from mtl_item_locations
					   where (DISABLE_DATE > SYSDATE or DISABLE_DATE IS NULL)
					   and   SUBINVENTORY_CODE = c1_rec.sub
					   and   ORGANIZATION_ID = c1_rec.org
                                 and segment19 is null
                                 and segment20 is null
					   and   ROWNUM < 2;
					 exception
					   when no_data_found then
						 dbms_output.put_line('No locator defined for sub='||c1_rec.sub||', org_id'||c1_rec.org);
						 dbms_output.put_line('Pls. define at least one physical locator in this subinventory');
						 l_locator_id := -1;
					 end;
				  end if;	  
	  
	  elsif l_proj_enabled = 2 then  /* non PJM org */
		  if (l_restrict_locators_code = 1) then
			 begin
			   select INVENTORY_LOCATION_ID into l_locator_id from mtl_item_locations
			   where (DISABLE_DATE > SYSDATE or DISABLE_DATE IS NULL)
			   and   SUBINVENTORY_CODE = c1_rec.sub
			   and   ORGANIZATION_ID = c1_rec.org
			   and   INVENTORY_LOCATION_ID IN
					 (select SECONDARY_LOCATOR FROM MTL_SECONDARY_LOCATORS
					  where  INVENTORY_ITEM_ID = c1_rec.item
					  and    ORGANIZATION_ID = c1_rec.org
					  and    SUBINVENTORY_CODE = c1_rec.sub)
			   and   ROWNUM < 2;
			 exception
			   when no_data_found then
				 dbms_output.put_line('No locator defined for item_id='||c1_rec.item||', sub='||c1_rec.sub||', org_id'||c1_rec.org);
				 l_locator_id := -1;
			 end;
		  elsif (l_restrict_locators_code = 2) then
			 begin
			   select INVENTORY_LOCATION_ID into l_locator_id from mtl_item_locations
			   where (DISABLE_DATE > SYSDATE or DISABLE_DATE IS NULL)
			   and   SUBINVENTORY_CODE = c1_rec.sub
			   and   ORGANIZATION_ID = c1_rec.org
			   and   ROWNUM < 2;
			 exception
			   when no_data_found then
				 dbms_output.put_line('No locator defined for sub='||c1_rec.sub||', org_id'||c1_rec.org);
				 l_locator_id := -1;
			 end;
		  end if;
		end if;   /* if PJM org */
 
      if (l_locator_id <> -1) then
       l_prdid := INV_TXN_MANAGER_PUB.get_open_period(c1_rec.org,sysdate,0);

       if (c1_rec.trx_qty > 0 ) then  --Quantity >0 need to subtransfer or issue material
        dbms_output.put_line('**Inserting sub-transfer record into MMTT with transfer_locator_id='||l_locator_id||' for item_id='||c1_rec.item||', org_id='||c1_rec.org||' and sub='||c1_rec.sub||', qty='||c1_rec.trx_qty);

        select mtl_material_transactions_s.nextval
        into l_temp_id from dual; 

        INSERT INTO mtl_material_transactions_temp
         (transaction_header_id
          ,transaction_temp_id
          ,transaction_mode
          ,lock_flag
          ,Process_flag
          ,last_update_date
          ,last_updated_by
          ,creation_date
          ,created_by
          ,last_update_login
          ,request_id
          ,program_application_id
          ,program_id
          ,program_update_date
          ,inventory_item_id
          ,revision
          ,organization_id
          ,subinventory_code
          ,locator_id
          ,transfer_subinventory
          ,transfer_to_location
          ,transfer_organization
          ,transaction_quantity
          ,primary_quantity
          ,transaction_uom
          ,transaction_type_id
          ,transaction_action_id
          ,transaction_source_type_id
          ,transaction_date
          ,acct_period_id
          ,distribution_account_id
          ,item_description
          ,item_location_control_code
          ,item_restrict_subinv_code
          ,item_restrict_locators_code
          ,item_revision_qty_control_code
          ,item_primary_uom_code
          ,item_shelf_life_code
          ,item_shelf_life_days
          ,item_lot_control_code
          ,item_serial_control_code
          ,allowed_units_lookup_code
          ,parent_transaction_temp_id
          ,lpn_id
          ,transfer_lpn_id
          ,cost_group_id)
     VALUES
          ( l_header_id,
            l_temp_id,
            3,
            'N',
            'Y',
            sysdate,
            -3782485,
            sysdate,
            -3782485,
            -3782485,
            NULL,
            NULL,
            NULL,
            NULL,
            c1_rec.item,
            c1_rec.revision, --revision
            c1_rec.org,
            c1_rec.sub,
            NULL, --locator_id
            c1_rec.sub,
            l_locator_id, --transfer_locator
            c1_rec.org,
            c1_rec.trx_qty, --l_mti_csr.transaction_quantity
            c1_rec.trx_qty, --l_mti_csr.primary_quantity
            l_primary_uom_code, --l_mti_csr.transaction_uom
            2, --l_mti_csr.transaction_type_id
            2, --l_mti_csr.transaction_action_id
            13, --l_mti_csr.transaction_source_type_id
            sysdate,
            l_prdid, --l_mti_csr.acct_period_id
            NULL, --l_mti_csr.distribution_account_id
            l_description,
            l_location_control_code,
            l_restrict_subinv_code,
            l_restrict_locators_code,
            l_revision_qty_control_code,
            l_primary_uom_code,
            l_shelf_life_code,
            l_shelf_life_days,
            l_lot_control_code,
            l_serial_control_code,
            l_allowed_units_lookup_code,
            NULL, --l_mti_csr.parent_id
            NULL, --l_mti_csr.lpn_id
            NULL, --l_mti_csr.transfer_lpn_id
            NULL);

            if c1_rec.lot_number is not null and l_lot_control_code =2 then
               INSERT INTO mtl_transaction_lots_temp
                (
                 transaction_temp_id
               , last_update_date
               , last_updated_by
               , creation_date
               , created_by
               , transaction_quantity
               , primary_quantity
               , lot_number
               , lot_expiration_date
               , serial_transaction_temp_id)
                  values (l_temp_id,sysdate,-1,sysdate,-1,-1*c1_rec.trx_qty,-1*c1_rec.trx_qty,
                  c1_rec.lot_number,null,null);
            end if;
       elsif (c1_rec.trx_qty < 0) then
        dbms_output.put_line('**Inserting misc. receipt record into MMTT with NULL locator_id for item_id='||c1_rec.item||', org_id='||c1_rec.org||' and sub='||c1_rec.sub||', qty='||-1*c1_rec.trx_qty);

        select mtl_material_transactions_s.nextval
        into l_temp_id from dual;

        begin
        INSERT INTO mtl_material_transactions_temp
         (transaction_header_id
          ,transaction_temp_id
          ,transaction_mode
          ,lock_flag
          ,Process_flag
          ,last_update_date
          ,last_updated_by
          ,creation_date
          ,created_by
          ,last_update_login
          ,request_id
          ,program_application_id
          ,program_id
          ,program_update_date
          ,inventory_item_id
          ,revision
          ,organization_id
          ,subinventory_code
          ,locator_id
          ,transaction_quantity
          ,primary_quantity
          ,transaction_uom
          ,transaction_type_id
          ,transaction_action_id
          ,transaction_source_type_id
          ,transaction_date
          ,acct_period_id
          ,distribution_account_id
          ,item_description
          ,item_location_control_code
          ,item_restrict_subinv_code
          ,item_restrict_locators_code
          ,item_revision_qty_control_code
          ,item_primary_uom_code
          ,item_shelf_life_code
          ,item_shelf_life_days
          ,item_lot_control_code
          ,item_serial_control_code
          ,allowed_units_lookup_code
          ,parent_transaction_temp_id
          ,lpn_id
          ,transfer_lpn_id
          ,cost_group_id
	  ,transaction_cost)
     VALUES
          ( l_header_id,
            l_temp_id,
            3,
            'N',
            'Y',
            sysdate,
            -3782485,
            sysdate,
            -3782485,
            -3782485,
            NULL,
            NULL,
            NULL,
            NULL,
            c1_rec.item,
            c1_rec.revision, --revision
            c1_rec.org,
            c1_rec.sub,
            NULL, --locator_id
            abs(c1_rec.trx_qty), --l_mti_csr.transaction_quantity
            abs(c1_rec.trx_qty), --l_mti_csr.primary_quantity
            l_primary_uom_code, --l_mti_csr.transaction_uom
            42, --l_mti_csr.transaction_type_id
            27, --l_mti_csr.transaction_action_id
            13, --l_mti_csr.transaction_source_type_id
            sysdate,
            l_prdid, --l_mti_csr.acct_period_id
            l_dist_account_id, --l_mti_csr.distribution_account_id
            l_description,
            l_location_control_code,
            l_restrict_subinv_code,
            l_restrict_locators_code,
            l_revision_qty_control_code,
            l_primary_uom_code,
            l_shelf_life_code,
            l_shelf_life_days,
            l_lot_control_code,
            l_serial_control_code,
            l_allowed_units_lookup_code,
            NULL, --l_mti_csr.parent_id
            NULL, --l_mti_csr.lpn_id
            NULL, --l_mti_csr.transfer_lpn_id
            NULL,
	    0);
            if c1_rec.lot_number is not null and l_lot_control_code =2 then
                INSERT INTO mtl_transaction_lots_temp
                (
                 transaction_temp_id
               , last_update_date
               , last_updated_by
               , creation_date
               , created_by
               , transaction_quantity
               , primary_quantity
               , lot_number
               , lot_expiration_date
               , serial_transaction_temp_id)
                  values (l_temp_id,sysdate,-1,sysdate,-1,1*c1_rec.trx_qty,1*c1_rec.trx_qty,
                  c1_rec.lot_number,null,null);
              end if;
         exception
             when others then
               dbms_output.put_line('Exp inserting receipt record '||sqlerrm);
         end;

        dbms_output.put_line('**Inserting misc. issue record into MMTT with locator_id='||l_locator_id||' for item_id='||c1_rec.item||', org_id='||c1_rec.org||' and sub='||c1_rec.sub||', qty='||c1_rec.trx_qty);

        select mtl_material_transactions_s.nextval
        into l_temp_id from dual;

        INSERT INTO mtl_material_transactions_temp
         (transaction_header_id
          ,transaction_temp_id
          ,transaction_mode
          ,lock_flag
          ,Process_flag
          ,last_update_date
          ,last_updated_by
          ,creation_date
          ,created_by
          ,last_update_login
          ,request_id
          ,program_application_id
          ,program_id
          ,program_update_date
          ,inventory_item_id
          ,revision
          ,organization_id
          ,subinventory_code
          ,locator_id
          ,transaction_quantity
          ,primary_quantity
          ,transaction_uom
          ,transaction_type_id
          ,transaction_action_id
          ,transaction_source_type_id
          ,transaction_date
          ,acct_period_id
          ,distribution_account_id
          ,item_description
          ,item_location_control_code
          ,item_restrict_subinv_code
          ,item_restrict_locators_code
          ,item_revision_qty_control_code
          ,item_primary_uom_code
          ,item_shelf_life_code
          ,item_shelf_life_days
          ,item_lot_control_code
          ,item_serial_control_code
          ,allowed_units_lookup_code
          ,parent_transaction_temp_id
          ,lpn_id
          ,transfer_lpn_id
          ,cost_group_id
	  ,transaction_cost)
     VALUES
          ( l_header_id,
            l_temp_id,
            3,
            'N',
            'Y',
            sysdate,
            -3782485,
            sysdate,
            -3782485,
            -3782485,
            NULL,
            NULL,
            NULL,
            NULL,
            c1_rec.item,
            c1_rec.revision, --revision
            c1_rec.org,
            c1_rec.sub,
            l_locator_id, --locator_id
            c1_rec.trx_qty, --l_mti_csr.transaction_quantity
            c1_rec.trx_qty, --l_mti_csr.primary_quantity
            l_primary_uom_code, --l_mti_csr.transaction_uom
            32, --l_mti_csr.transaction_type_id
            1, --l_mti_csr.transaction_action_id
            13, --l_mti_csr.transaction_source_type_id
            sysdate,
            l_prdid, --l_mti_csr.acct_period_id
            l_dist_account_id, --l_mti_csr.distribution_account_id
            l_description,
            l_location_control_code,
            l_restrict_subinv_code,
            l_restrict_locators_code,
            l_revision_qty_control_code,
            l_primary_uom_code,
            l_shelf_life_code,
            l_shelf_life_days,
            l_lot_control_code,
            l_serial_control_code,
            l_allowed_units_lookup_code,
            NULL, --l_mti_csr.parent_id
            NULL, --l_mti_csr.lpn_id
            NULL, --l_mti_csr.transfer_lpn_id
            NULL,
	    0);
            if c1_rec.lot_number is not null and l_lot_control_code =2 then
                INSERT INTO mtl_transaction_lots_temp
                (
                 transaction_temp_id
               , last_update_date
               , last_updated_by
               , creation_date
               , created_by
               , transaction_quantity
               , primary_quantity
               , lot_number
               , lot_expiration_date
               , serial_transaction_temp_id)
                  values (l_temp_id,sysdate,-1,sysdate,-1,1*c1_rec.trx_qty,1*c1_rec.trx_qty,
                  c1_rec.lot_number,null,null);
             end if;

       elsif (c1_rec.trx_qty = 0) then
        dbms_output.put_line('Deleting from MOQD with NULL locator_id for item_id='||c1_rec.item||', org_id='||c1_rec.org||' and sub='||c1_rec.sub||', qty='||c1_rec.trx_qty);

	delete from mtl_onhand_quantities_detail moqd
	where inventory_item_id = c1_rec.item
	and   organization_id = c1_rec.org
	and   subinventory_code = c1_rec.sub
	and   locator_id is null
        and exists (select 1 from mtl_secondary_inventories msi
                    where msi.secondary_inventory_name = moqd.subinventory_code
                    and   msi.organization_id = moqd.organization_id
                    and   msi.locator_type in (2,3) )
        and exists (select 1 from mtl_parameters
                    where organization_id = moqd.organization_id
                    and  STOCK_LOCATOR_CONTROL_CODE = 4
                    and  wms_enabled_flag = 'N');

        dbms_output.put_line('Total number of rows deleted from MOQD = '||SQL%ROWCOUNT);

       end if; --c1_rec.trx_qty > 0
     end if; --l_locator_id <> -1
       

    end loop;
 commit;
end;
/