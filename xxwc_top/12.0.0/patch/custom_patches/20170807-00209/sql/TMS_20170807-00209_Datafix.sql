SET SERVEROUTPUT ON SIZE 1000000

DECLARE
   CURSOR C1
   IS
      SELECT folder_id, OBJECT
        FROM apps.fnd_folders
       WHERE     name IN ('New Default PO Header View',
                          'NEW DEFAULT',
                          'New Default PO Lines View',
                          'NEW DEFAULT MISC TRANSACTIONS',
                          'New Default Shipments View',
                          'NEW DEFAULT SUBINVENTORY TRANSFER'
						  )
             AND created_by = CREATED_BY;


   CURSOR C3
   IS
      SELECT RESPONSIBILITY_NAME,
             RESPONSIBILITY_ID,
             APPLICATION_ID,
             'N' BEHAVIOR_MODE
        FROM apps.fnd_responsibility_vl
       WHERE     RESPONSIBILITY_NAME LIKE 'HDS%WC%'
             AND NVL (END_DATE, SYSDATE + 1) >= SYSDATE;

   LN_COUNT     NUMBER;
BEGIN
   FOR C2 IN C1
   LOOP
      FOR C4 IN C3
      LOOP
         BEGIN
            LN_COUNT := 0;

            SELECT COUNT (1)
              INTO ln_count
              FROM FND_DEFAULT_FOLDERS
             WHERE     USER_ID = TRIM ('-' || TO_CHAR (c4.RESPONSIBILITY_ID))
                   AND FOLDER_ID = C2.FOLDER_ID
                   AND OBJECT = C2.OBJECT;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               LN_COUNT := 0;
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.PUT_LINE (
                     'Exception occured (deriving Object) '
                  || SUBSTR (SQLERRM, 1, 250));
         END;

         IF NVL (ln_count, 0) > 0
         THEN
            DBMS_OUTPUT.put_line (
                  'Object '
               || C2.OBJECT
               || ' Folder_id '
               || c2.folder_id
               || ' already exist in Respnsibility_name '
               || c4.responsibility_name);
         ELSE
            apps.fnd_appfldr.insert_fnd_default_folders (
               l_object            => C2.OBJECT,
               l_user_id           => TRIM ('-' || TO_CHAR (c4.RESPONSIBILITY_ID)),
               l_folder_id         => C2.FOLDER_ID,
               l_created_by        => 16991,
               l_last_updated_by   => 16991);

            DBMS_OUTPUT.put_line (
                  'Object '
               || C2.OBJECT
               || ' Folder_id '
               || c2.folder_id
               || ' inserted in Respnsibility_name '
               || c4.responsibility_name);
         END IF;
      END LOOP;
   END LOOP;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Exception occured ' || SUBSTR (SQLERRM, 1, 250));
END;
/
DECLARE
   CURSOR C1
   IS
      SELECT A.ROWID row_id, SUBSTR (A.USER_ID, 2) RESP_ID
        FROM apps.fnd_default_folders A
       WHERE     A.application_id IS NULL
             AND A.behavior_mode IS NULL
             AND TRUNC (A.creation_date) = TRUNC (SYSDATE)
             AND A.folder_id IN (SELECT folder_id
                                   FROM apps.fnd_folders
                                  WHERE name IN ('New Default PO Header View',
                                                 'NEW DEFAULT',
                                                 'New Default PO Lines View',
                                                 'NEW DEFAULT MISC TRANSACTIONS',
                                                 'New Default Shipments View',
                                                 'NEW DEFAULT SUBINVENTORY TRANSFER'));

   ln_appl_id   NUMBER;
BEGIN
   FOR C2 IN C1
   LOOP
      BEGIN
         SELECT APPLICATION_ID
           INTO ln_appl_id
           FROM APPS.FND_RESPONSIBILITY
          WHERE RESPONSIBILITY_ID = C2.RESP_ID;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            ln_appl_id := 0;
      END;

         UPDATE fnd_default_folders
            SET APPLICATION_ID = ln_appl_id, BEHAVIOR_MODE = 'N'
          WHERE ROWID = C2.ROW_ID;
      COMMIT;
   END LOOP;
END;
/
