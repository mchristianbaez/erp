/******************************************************************************************************
  $Header XXEIS.EIS_XXWC_OM_INT_OPEN_ORDERS_V $
  Module Name : OM
  PURPOSE	  : Internal Sales Orders Report
  TMS Task Id : 20160418-00106,20160601-00134  
  REVISIONS   :
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     16-Apr-2016        Pramod   		 TMS#20160418-00106   Performance Tuning
  1.1     06-Jun-2016		 Siva			 TMS#20160601-00134  
  1.2  	  14-Mar-2017		 Siva			 TMS#20170209-00052 
**********************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_OM_INT_OPEN_ORDERS_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_OM_INT_OPEN_ORDERS_V ( PROCESS_ID,
  WAREHOUSE, SALES_PERSON_NAME, CREATED_BY, ORDER_NUMBER, LINE_NUMBER,
  ORDERED_DATE, LINE_CREATION_DATE, ORDER_TYPE, QUOTE_NUMBER, ORDER_LINE_STATUS
  , BACKORDER_QTY, DISTRICT, REGION, SHIP_QTY, ORDER_HEADER_STATUS,
  CUSTOMER_NUMBER, CUSTOMER_NAME, CUSTOMER_JOB_NAME, ORDER_AMOUNT,
  SHIPPING_METHOD, SHIP_TO_CITY, ZIP_CODE, SCHEDULE_SHIP_DATE, HEADER_STATUS,
  ITEM_NUMBER, ITEM_DESCRIPTION, QTY, PAYMENT_TERMS, SHIP_TO_ORG,
  ORDER_HEADER_ID, ORDER_LINE_ID, CUST_ACCOUNT_ID, CUST_ACCT_SITE_ID,
  SALESREP_ID, PARTY_ID, TRANSACTION_TYPE_ID, MTP_ORGANIZATION_ID,
  HZCS_SHIP_TO_STE_ID, HZPS_SHP_TO_PRT_ID, REQUISITION_NUMBER,
  PO_REQ_CREATED_BY, PO_REQ_CREATED_BY_ID, REQ_APPR_DATE, BIN1, BIN2, BIN3)
AS
  SELECT   
    PROCESS_ID,	
    WAREHOUSE,
    SALES_PERSON_NAME,
    CREATED_BY,
    ORDER_NUMBER,
    LINE_NUMBER,
    ORDERED_DATE,
    LINE_CREATION_DATE,
    ORDER_TYPE,
    QUOTE_NUMBER,
    ORDER_LINE_STATUS,
    BACKORDER_QTY,
    DISTRICT,
    REGION,
    SHIP_QTY,
    ORDER_HEADER_STATUS,
    CUSTOMER_NUMBER,
    CUSTOMER_NAME,
    CUSTOMER_JOB_NAME,
    ORDER_AMOUNT,
    SHIPPING_METHOD,
    SHIP_TO_CITY,
    ZIP_CODE,
    SCHEDULE_SHIP_DATE,
    HEADER_STATUS,
    ITEM_NUMBER,
    ITEM_DESCRIPTION,
    QTY,
    PAYMENT_TERMS,
    SHIP_TO_ORG,
    ORDER_HEADER_ID,
    ORDER_LINE_ID,
    CUST_ACCOUNT_ID,
    CUST_ACCT_SITE_ID,
    SALESREP_ID,
    PARTY_ID,
    TRANSACTION_TYPE_ID,
    MTP_ORGANIZATION_ID,
    HZCS_SHIP_TO_STE_ID,
    HZPS_SHP_TO_PRT_ID,
    REQUISITION_NUMBER,
    PO_REQ_CREATED_BY,
    PO_REQ_CREATED_BY_ID,
    REQ_APPR_DATE,
    BIN1, --added for version 1.2
    BIN2, --added for version 1.2
    BIN3 --added for version 1.2
  FROM
    XXEIS.EIS_XXWC_INTERNAL_SALES_TAB
/
