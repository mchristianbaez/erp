------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_INTERNAL_SALES_TAB
  Description: This table is used to load data from XXEIS.EIS_XXWC_INT_SALES_ORDERS_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     16-Apr-2016        Pramod   TMS#20160418-00106   Performance Tuning
  1.1	  14-Mar-2017		 Siva	  TMS#20170209-00052 
********************************************************************************/
DROP TABLE XXEIS.EIS_XXWC_INTERNAL_SALES_TAB CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE XXEIS.EIS_XXWC_INTERNAL_SALES_TAB  --added for version 1.1
  (
    PROCESS_ID           NUMBER,
    WAREHOUSE            VARCHAR2(30 BYTE),
    SALES_PERSON_NAME    VARCHAR2(360 BYTE),
    CREATED_BY           VARCHAR2(240 BYTE),
    ORDER_NUMBER         NUMBER,
    LINE_NUMBER          VARCHAR2(810 BYTE),
    ORDERED_DATE         DATE,
    LINE_CREATION_DATE   DATE,
    ORDER_TYPE           VARCHAR2(300 BYTE),
    QUOTE_NUMBER         NUMBER,
    ORDER_LINE_STATUS    VARCHAR2(2400 BYTE),
    BACKORDER_QTY        NUMBER,
    DISTRICT             VARCHAR2(2150 BYTE),
    REGION               VARCHAR2(2150 BYTE),
    SHIP_QTY             NUMBER,
    ORDER_HEADER_STATUS  VARCHAR2(280 BYTE),
    CUSTOMER_NUMBER      VARCHAR2(230 BYTE),
    CUSTOMER_NAME        VARCHAR2(2360 BYTE),
    CUSTOMER_JOB_NAME    VARCHAR2(240 BYTE),
    ORDER_AMOUNT         NUMBER,
    SHIPPING_METHOD      VARCHAR2(280 BYTE),
    SHIP_TO_CITY         VARCHAR2(260 BYTE),
    ZIP_CODE             VARCHAR2(260 BYTE),
    SCHEDULE_SHIP_DATE   DATE,
    HEADER_STATUS        VARCHAR2(230 BYTE),
    ITEM_NUMBER          VARCHAR2(240 BYTE),
    ITEM_DESCRIPTION     VARCHAR2(2240 BYTE),
    QTY                  NUMBER,
    PAYMENT_TERMS        VARCHAR2(215 BYTE),
    SHIP_TO_ORG          VARCHAR2(23 BYTE),
    ORDER_HEADER_ID      NUMBER,
    ORDER_LINE_ID        NUMBER,
    CUST_ACCOUNT_ID      NUMBER,
    CUST_ACCT_SITE_ID    NUMBER,
    SALESREP_ID          NUMBER,
    PARTY_ID             NUMBER,
    TRANSACTION_TYPE_ID  NUMBER,
    MTP_ORGANIZATION_ID  NUMBER,
    HZCS_SHIP_TO_STE_ID  NUMBER,
    HZPS_SHP_TO_PRT_ID   NUMBER,
    REQUISITION_NUMBER   VARCHAR2(220 BYTE),
    PO_REQ_CREATED_BY    VARCHAR2(2240 BYTE),
    PO_REQ_CREATED_BY_ID NUMBER,
    REQ_APPR_DATE        DATE,
	BIN1				 VARCHAR2(100 BYTE), --added for version 1.1
	BIN2				 VARCHAR2(100 BYTE), --added for version 1.1
	BIN3				 VARCHAR2(100 BYTE)  --added for version 1.1
)
ON COMMIT PRESERVE ROWS
RESULT_CACHE (mode default)
NOCACHE  --added for version 1.1
/
