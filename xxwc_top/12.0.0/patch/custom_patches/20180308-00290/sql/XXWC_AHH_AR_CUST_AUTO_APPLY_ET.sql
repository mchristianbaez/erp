 /********************************************************************************
  FILE NAME: XXWC.XXWC_AHH_AR_CUST_AUTO_APPLY_ET.sql

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Pattabhi Avula  TMS#20180308-00290  -- Initial Version
  ********************************************************************************/
  CREATE TABLE XXWC.XXWC_AHH_AR_CUST_AUTO_APPLY_ET
   (AHH_CODE		 VARCHAR2(20), 
	CUST_NAME		 VARCHAR2(240), 
	CUST_NUM		 VARCHAR2(20), 
	CUST_CITY		 VARCHAR2(50), 
	CUST_STATE		 VARCHAR2(20), 
	AUTO_APPLY		 VARCHAR2(20)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_AHH_AR_CUST_AUTO_APPLY.bad'
    DISCARDFILE 'XXWC_AHH_AR_CUST_AUTO_APPLY.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWC_AHH_AR_CUST_AUTO_APPLY.csv'
       )
    )
   REJECT LIMIT UNLIMITED;
   /