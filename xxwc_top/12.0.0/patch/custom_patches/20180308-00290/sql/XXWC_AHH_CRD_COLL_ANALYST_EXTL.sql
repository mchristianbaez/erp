/********************************************************************************
  FILE NAME: XXWC.XXWC_AHH_CRD_COLL_ANALYST_EXTL.sql

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Pattabhi Avula  TMS#20180308-00290  -- Initial Version
  ********************************************************************************/
  CREATE TABLE XXWC.XXWC_AHH_CRD_COLL_ANALYST_EXTL
   (AHH_COLL_CODE		 VARCHAR2(10), 
	ORA_COLL_ID			 NUMBER, 
	COLLECTOR_NAME		 VARCHAR2(150), 
	ORA_CRD_ANLST_ID	 NUMBER, 
	CRD_ANALYST_NAME	 VARCHAR2(150)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
       SKIP 1
       BADFILE 'XXWC_AHH_CRD_COLL_ANALYST.bad'
       DISCARDFILE 'XXWC_AHH_CRD_COLL_ANALYST.dsc'
       FIELDS TERMINATED BY '|'
       OPTIONALLY ENCLOSED BY '"' AND '"'
       REJECT ROWS WITH ALL NULL FIELDS
          )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWC_AHH_CRD_COLL_ANALYST.csv'
       )
    )
   REJECT LIMIT UNLIMITED;
   /