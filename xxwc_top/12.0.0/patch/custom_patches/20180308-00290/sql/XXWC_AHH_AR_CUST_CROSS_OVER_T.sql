 /********************************************************************************
  FILE NAME: XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T.sql

  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    --------------------------------------
  1.0     05/016/2018   Pattabhi Avula     TMS#20180308-00290  -- Initial Version
  ********************************************************************************/
  CREATE TABLE XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T 
   (CUST_NUM  			VARCHAR2(20), 
	CUST_SITE 			VARCHAR2(50), 
	CUST_NAME 			VARCHAR2(240), 
	CUST_ADD_LINE1  	VARCHAR2(240), 
	ORACLE_CUST_NUM 	VARCHAR2(20)
   );
   /