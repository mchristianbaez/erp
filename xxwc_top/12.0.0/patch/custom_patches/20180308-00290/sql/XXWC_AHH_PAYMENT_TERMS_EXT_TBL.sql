/********************************************************************************
  FILE NAME: XXWC.XXWC_AHH_PAYMENT_TERMS_EXT_TBL.sql

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Pattabhi Avula  TMS#20180308-00290  -- Initial Version
  ********************************************************************************/
  CREATE TABLE XXWC.XXWC_AHH_PAYMENT_TERMS_EXT_TBL 
   (AHH_TERM_NAME		 VARCHAR2(50), 
	TERM_NAME			 VARCHAR2(50), 
	TERM_DESCRIPTION	 VARCHAR2(150)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_AHH_PAYMENT_TERMS.bad'
    DISCARDFILE 'XXWC_AHH_PAYMENT_TERMS.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWC_AHH_PAYMENT_TERMS.csv'
       )
    )
   REJECT LIMIT UNLIMITED;
   /