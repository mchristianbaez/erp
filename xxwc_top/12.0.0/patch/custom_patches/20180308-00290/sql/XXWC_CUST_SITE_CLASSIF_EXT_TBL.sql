 /********************************************************************************
  FILE NAME: XXWC.XXWC_CUST_SITE_CLASSIF_EXT_TBL.sql

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Pattabhi Avula  TMS#20180308-00290  -- Initial Version
  ********************************************************************************/
 CREATE TABLE XXWC.XXWC_CUST_SITE_CLASSIF_EXT_TBL 
   (AHH_SITE_CLASSIFICATION 	VARCHAR2(10), 
	CUSTOMER_NAME   			VARCHAR2(240), 
	CUST_SITE_NAME  			VARCHAR2(30), 
	CUSTOMER_NUMBER 			VARCHAR2(20), 
	CUST_SITE_NUM   			VARCHAR2(30), 
	CITY  						VARCHAR2(50), 
	STATE 						VARCHAR2(50), 
	ENTER_DATE 					VARCHAR2(50), 
	TERMS_TYPE 					VARCHAR2(20), 
	SELL_TYPE  					VARCHAR2(10), 
	HIGH_BALANCE 				VARCHAR2(20), 
	AVG_PAY_DAYS 				VARCHAR2(10), 
	TOTAL_CURR_BALANCE 			VARCHAR2(20), 
	LAST_SALE_DT 				VARCHAR2(50), 
	LAST_PAY_DT  				VARCHAR2(50), 
	NAICS      					VARCHAR2(20), 
	SLSREPOUT  					VARCHAR2(20), 
	HOLDPER_CD 					VARCHAR2(20), 
	SIC_1 						VARCHAR2(20), 
	SIC_2 						VARCHAR2(20), 
	SIC_3 						VARCHAR2(20), 
	SALES_YTD      				VARCHAR2(20), 
	LAST_SALES_YTD 				VARCHAR2(20), 
	CREDIT_LIMIT   				VARCHAR2(20), 
	COLLECTOR_ID   				VARCHAR2(20), 
	SITE_CLASS     				VARCHAR2(50)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_CUST_SITE_CLASSIFICATION_CONV.bad'
    DISCARDFILE 'XXWC_CUST_SITE_CLASSIFICATION_CONV.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                 )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWC_CUST_SITE_CLASSIFICATION_CONV.csv'
       )
    )
   REJECT LIMIT UNLIMITED;
   /
  