 /********************************************************************************
  FILE NAME: XXWC.XXWC_AHH_AR_CUST_AUTO_APPLY_T.sql

  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    --------------------------------------
  1.0     05/016/2018   Pattabhi Avula     TMS#20180308-00290  -- Initial Version
  ********************************************************************************/  
  CREATE TABLE XXWC.XXWC_AHH_AR_CUST_AUTO_APPLY_T 
   (AHH_CODE  		VARCHAR2(20), 
	CUST_NAME 		VARCHAR2(240), 
	CUST_NUM  		VARCHAR2(20), 
	CUST_CITY 		VARCHAR2(50), 
	CUST_STATE 		VARCHAR2(20), 
	AUTO_APPLY 		VARCHAR2(20)
   );
   /