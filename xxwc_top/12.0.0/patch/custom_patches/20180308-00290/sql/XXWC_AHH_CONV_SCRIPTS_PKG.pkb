CREATE OR REPLACE 
PACKAGE BODY APPS.XXWC_AHH_CONV_SCRIPTS_PKG
IS
   /******************************************************************************************************************************************************
        $Header XXWC_AHH_CONV_SCRIPTS_PKG $
        Module Name: XXWC_AHH_CONV_SCRIPTS_PKG.pkb

        PURPOSE:   AH Harries Conversion Project

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        07/02/2018  Pattabhi Avula   TMS#20180308-00290 - AH HARRIS Conversion
   ******************************************************************************************************************************************************/

   g_err_callfrom      VARCHAR2 (1000) := 'XXWC_AHH_CONV_SCRIPTS_PKG.update_prism_site_dff';
   g_module            VARCHAR2 (100)  := 'XXWC';
   g_distro_list       VARCHAR2 (80)   := 'wc-itdevalerts-u1@hdsupply.com'; 
   l_sec               VARCHAR2 (240);   

   PROCEDURE Update_Prism_Site_DFF (errbuff       OUT VARCHAR2,
                                    retcode       OUT VARCHAR2,
									p_to_date     IN  VARCHAR2)
   IS
      -- =====================================================================================================================================================
      -- Procedure: Update_Prism_Site_DFF
      -- Purpose: Update_Prism_Site_DFF
      -- ====================================================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author            Description
      --  ---------  ---------- ---------------     ------------------------------------------------------------------------------------------------
      --  1.0        07/02/2018  Pattabhi Avula     TMS#20180308-00290 - AH HARRIS Conversion
      -- ====================================================================================================================================================
	  
	l_count           NUMBER:=0;
	l_locked_line     VARCHAR2(2);
    CURSOR cur
      IS
       SELECT ROWID, cust_acct_site_id
         FROM apps.hz_cust_acct_sites_all
        WHERE org_id = 162
          AND attribute17 IS NOT NULL
          AND TRUNC (creation_date) < FND_DATE.CANONICAL_TO_DATE(p_to_date);
		  
BEGIN
 l_sec:='Before loop';
 fnd_file.put_line(fnd_file.log, 'Starting of log file '||p_to_date);
   FOR rec IN cur
     LOOP
	 l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec.ROWID, 'HZ_CUST_ACCT_SITES_ALL'); 
     		
		IF l_locked_line = 'N' 
		 THEN
		   l_count:=l_count+1; 
           UPDATE /*+ HZ_CUST_ACCT_SITES_U1 */ apps.hz_cust_acct_sites_all
              SET attribute17 = NULL
            WHERE cust_acct_site_id = rec.cust_acct_site_id;			
		
		    IF l_count >= 10000 THEN
		    COMMIT;
		    l_count:=0;
		    END IF;
		END IF;
		
   END LOOP;

 l_sec:='After loop';
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      COMMIT;	
fnd_file.put_line(fnd_file.log, 'Unexpected error : '||SQLERRM);	  
xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom,
                                     p_calling           => l_sec,
                                     p_request_id        => fnd_global.conc_request_id,
                                     p_ora_error_msg     => SQLERRM,
                                     p_error_desc        => 'Error while updating the hz_cust_acct_sites_all table for Prism Site Number',
                                     p_distribution_list => g_distro_list,
                                     p_module            => g_module);

END Update_Prism_Site_DFF;
END XXWC_AHH_CONV_SCRIPTS_PKG;
/