 /********************************************************************************
  FILE NAME: XXWC.XXWC_RETURN_REASON_TBL.sql

  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    --------------------------------------
  1.0     05/016/2018   Pattabhi Avula     TMS#20180308-00290  -- Initial Version
  ********************************************************************************/
  CREATE TABLE XXWC.XXWC_RETURN_REASON_TBL 
   (DESCRIPTION 			VARCHAR2(100), 
	ORACLE_RETURN_REASON 	VARCHAR2(100)
   );
   /