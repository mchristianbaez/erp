 /********************************************************************************
  FILE NAME: XXWC.XXWC_AR_RECEIPTS_CONV.sql

  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    --------------------------------------
  1.0     05/016/2018   Pattabhi Avula     TMS#20180308-00290  -- Initial Version
  ********************************************************************************/
  CREATE TABLE XXWC.XXWC_AR_RECEIPTS_CONV 
   (RECEIPT_NUMBER    			VARCHAR2(50) NOT NULL ENABLE, 
	RECEIPT_METHOD 				VARCHAR2(50), 
	RECEIPT_DATE   				DATE, 
	RECEIPT_AMOUNT 				NUMBER, 
	CURRENCY_CODE  				VARCHAR2(10), 
	LEGACY_CUSTOMER_NUM  		VARCHAR2(20), 
	LEGACY_CUSTOMER_SITE 		VARCHAR2(30), 
	BANK_BRANCH_NAME     		VARCHAR2(50), 
	BANK_ACCOUNT_NUMBER  		VARCHAR2(50), 
	COMMENTS 					VARCHAR2(1000), 
	GL_DATE  					DATE, 
	CREATION_DATE 				DATE NOT NULL ENABLE, 
	CREATED_BY    				NUMBER NOT NULL ENABLE, 
	LAST_UPDATED_BY  			NUMBER NOT NULL ENABLE, 
	LAST_UPDATE_DATE 			DATE NOT NULL ENABLE, 
	LAST_UPDATE_LOGIN 			NUMBER, 
	PROCESS_STATUS    			VARCHAR2(1), 
	ERROR_MESSAGE     			VARCHAR2(2000), 
	RECEIPT_REFERENCE 			VARCHAR2(100), 
	DISPUTE_CODE      			VARCHAR2(100), 
	REQUEST_ID 					NUMBER, 
	ORG_ID     					NUMBER DEFAULT NVL(TO_NUMBER(DECODE(SUBSTRB(USERENV('CLIENT_INFO'),1,1), ' ', NULL,SUBSTRB(USERENV('CLIENT_INFO'),1,10))),-99)
   );
   /