 /********************************************************************************
  FILE NAME: XXWC.XXWC_AR_INVOICES_CONV.sql

  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    --------------------------------------
  1.0     05/016/2018   Pattabhi Avula     TMS#20180308-00290  -- Initial Version
  ********************************************************************************/
  CREATE TABLE XXWC.XXWC_AR_INVOICES_CONV 
   (INTERFACE_LINE_ATTRIBUTE1 		VARCHAR2(150), 
	ATTRIBUTE2 						VARCHAR2(150), 
	ATTRIBUTE3 						VARCHAR2(150), 
	ACCTD_AMOUNT 					NUMBER, 
	PAYING_CUSTOMER_ID 				NUMBER(15,0), 
	AMOUNT 							NUMBER, 
	TERM_NAME 						VARCHAR2(50), 
	ORIG_SYSTEM_BILL_CUSTOMER_REF 	VARCHAR2(240), 
	ORIG_SYSTEM_SOLD_CUSTOMER_REF 	VARCHAR2(240), 
	TRX_DATE 						DATE, 
	TRX_NUMBER 						VARCHAR2(30), 
	UNIT_SELLING_PRICE 				NUMBER, 
	PRIMARY_SALESREP_NUMBER 		VARCHAR2(30), 
	SALES_ORDER    					VARCHAR2(50), 
	PURCHASE_ORDER 					VARCHAR2(50), 
	PROCESS_STATUS 					VARCHAR2(20), 
	ERROR_MESSAGE  					VARCHAR2(2000), 
	CUST_TRX_TYPE_NAME 				VARCHAR2(30), 
	HDS_SITE_FLAG      				VARCHAR2(6), 
	SHIP_TO_PARTY_SITE_NUMBER 		VARCHAR2(50), 
	ORG_ID 	 						NUMBER DEFAULT NVL(TO_NUMBER(DECODE(SUBSTRB(USERENV('CLIENT_INFO'),1,1), ' ', NULL,SUBSTRB(USERENV('CLIENT_INFO'),1,10))),-99), 
	COMMENTS 						VARCHAR2(240)
   );
   /