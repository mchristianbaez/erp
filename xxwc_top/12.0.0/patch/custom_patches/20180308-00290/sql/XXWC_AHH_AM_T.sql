 /********************************************************************************
  FILE NAME: XXWC.XXWC_AHH_AM_T.sql

  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    --------------------------------------
  1.0     05/016/2018   Pattabhi Avula     TMS#20180308-00290  -- Initial Version
  ********************************************************************************/
  CREATE TABLE XXWC.XXWC_AHH_AM_T 
   (SLSREPOUT 		VARCHAR2(20), 
	SALESREP_ID 	NUMBER
   );
   /