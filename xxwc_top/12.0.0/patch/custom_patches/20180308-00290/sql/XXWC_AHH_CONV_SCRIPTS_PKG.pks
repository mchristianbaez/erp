CREATE OR REPLACE 
PACKAGE      APPS.XXWC_AHH_CONV_SCRIPTS_PKG
   AUTHID CURRENT_USER
AS
   /******************************************************************************************************************************************************
        $Header XXWC_AHH_CONV_SCRIPTS_PKG $
        Module Name: XXWC_AHH_CONV_SCRIPTS_PKG.pks

        PURPOSE:   AH Harries Conversion Project

        REVISIONS:
        Ver        Date        Author             Description
        ---------  ----------  --------------- ----------------------------------------------------------------------------------------------------------
        1.0        07/02/2018  Pattabhi Avula     TMS#20180308-00290 - AH HARRIS Conversion
   ******************************************************************************************************************************************************/

   PROCEDURE Update_Prism_Site_DFF (errbuff       OUT VARCHAR2,
                                    retcode       OUT VARCHAR2,
									p_to_date     IN  VARCHAR2);
END;
/