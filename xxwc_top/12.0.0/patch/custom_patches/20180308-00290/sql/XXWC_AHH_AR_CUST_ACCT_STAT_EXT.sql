 /********************************************************************************
  FILE NAME: XXWC.XXWC_AHH_AR_CUST_ACCT_STAT_EXT.sql

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Pattabhi Avula  TMS#20180308-00290  -- Initial Version
  ********************************************************************************/
  CREATE TABLE XXWC.XXWC_AHH_AR_CUST_ACCT_STAT_EXT
   (AHH_PROFILE_CODE 		VARCHAR2(20), 
	NAME 					VARCHAR2(150), 
	CUSTOMER_NUM 			VARCHAR2(20), 
	CITY  					VARCHAR2(50), 
	STATE 					VARCHAR2(50), 
	ENTERED_DATE 			VARCHAR2(50), 
	TERMS_TYPE   			VARCHAR2(20), 
	SELL_TYPE    			VARCHAR2(10), 
	HIGH_BALANCE 			VARCHAR2(50), 
	AVG_PAY_DAYS 			VARCHAR2(50), 
	TOTAL_CURR_BALANCE 		VARCHAR2(50), 
	LAST_SALE_DT 			VARCHAR2(50), 
	LAST_PAY_DT  			VARCHAR2(50), 
	NAICS 					VARCHAR2(20), 
	SLSREPOUT  				VARCHAR2(20), 
	HOLDPER_CD 				VARCHAR2(20), 
	SIC_1 					VARCHAR2(20), 
	SIC_2 					VARCHAR2(20), 
	SIC_3 					VARCHAR2(20), 
	SALES_YTD 				VARCHAR2(50), 
	LAST_SALES_YTD 			VARCHAR2(50), 
	CREDIT_LIMIT 			VARCHAR2(50), 
	COLLECTOR_ID 			VARCHAR2(50), 
	ACCOUNT_STATUS 			VARCHAR2(50)   
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_AHH_AR_CUST_ACCT_STAT.bad'
    DISCARDFILE 'XXWC_AHH_AR_CUST_ACCT_STAT.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                 )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWC_AHH_AR_CUST_ACCT_STAT.csv'
       )
    )
   REJECT LIMIT UNLIMITED;
   /