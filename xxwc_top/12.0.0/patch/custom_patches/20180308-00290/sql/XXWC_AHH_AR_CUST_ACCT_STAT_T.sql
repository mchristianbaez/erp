
 /********************************************************************************
  FILE NAME: XXWC.XXWC_AHH_AR_CUST_ACCT_STAT_T.sql

  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    --------------------------------------
  1.0     05/016/2018   Pattabhi Avula     TMS#20180308-00290  -- Initial Version
  ********************************************************************************/
  CREATE TABLE XXWC.XXWC_AHH_AR_CUST_ACCT_STAT_T 
   (AHH_PROFILE_CODE 		VARCHAR2(20), 
	NAME 					VARCHAR2(150), 
	CUSTOMER_NUM 			VARCHAR2(20), 
	CITY  					VARCHAR2(50), 
	STATE 					VARCHAR2(50), 
	ENTERED_DATE 			DATE, 
	TERMS_TYPE 				VARCHAR2(20), 
	SELL_TYPE  				VARCHAR2(10), 
	HIGH_BALANCE 			NUMBER, 
	AVG_PAY_DAYS 			NUMBER, 
	TOTAL_CURR_BALANCE 		NUMBER, 
	LAST_SALE_DT 			DATE, 
	LAST_PAY_DT  			DATE, 
	NAICS      				VARCHAR2(20), 
	SLSREPOUT  				VARCHAR2(20), 
	HOLDPER_CD 				VARCHAR2(20), 
	SIC_1 					VARCHAR2(20), 
	SIC_2 					VARCHAR2(20), 
	SIC_3 					VARCHAR2(20), 
	SALES_YTD      			NUMBER, 
	LAST_SALES_YTD 			NUMBER, 
	CREDIT_LIMIT 			NUMBER, 
	COLLECTOR_ID 			NUMBER, 
	ACCOUNT_STATUS 			VARCHAR2(50)
   );
   /