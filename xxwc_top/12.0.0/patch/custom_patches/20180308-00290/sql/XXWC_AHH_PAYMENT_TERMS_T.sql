 /********************************************************************************
  FILE NAME: XXWC.XXWC_AHH_PAYMENT_TERMS_T.sql

  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    --------------------------------------
  1.0     05/016/2018   Pattabhi Avula     TMS#20180308-00290  -- Initial Version
  ********************************************************************************/
  CREATE TABLE XXWC.XXWC_AHH_PAYMENT_TERMS_T 
   (AHH_TERM_NAME	 	VARCHAR2(50), 
	TERM_NAME		 	VARCHAR2(50), 
	TERM_DESCRIPTION	VARCHAR2(150)
   );
   /