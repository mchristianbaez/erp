 /********************************************************************************
  FILE NAME: XXWC.XXWC_AHH_CRD_COLL_ANALYST_T.sql

  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    --------------------------------------
  1.0     05/016/2018   Pattabhi Avula     TMS#20180308-00290  -- Initial Version
  ********************************************************************************/
  CREATE TABLE XXWC.XXWC_AHH_CRD_COLL_ANALYST_T 
   (AHH_COLL_CODE 		VARCHAR2(10), 
	ORA_COLL_ID 		NUMBER, 
	COLLECTOR_NAME 		VARCHAR2(150), 
	ORA_CRD_ANLST_ID 	NUMBER, 
	CRD_ANALYST_NAME 	VARCHAR2(150)
   );
   /