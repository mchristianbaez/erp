create or replace 
PACKAGE BODY   XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG
  --//============================================================================
  --//
  --// Object Name         		:: XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG
  --//
  --// Object Type         		:: Package Body
  --//
  --// Object Description       :: This package will trigger at before report level and insert the data into a temp table.
  --//
  --// Version Control
  --//============================================================================
  --// Vers      Author             Date          			 Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva  		  09-Jan-2018         		 TMS#20171228-00005 
  --//============================================================================
AS
  g_dflt_email VARCHAR2 (50) := 'HDSOracleDevelopers@hdsupply.com'; 

	FUNCTION GET_LOOKUP_MEANING (P_LOOKUP_CODE in VARCHAR2,p_lookup_type in VARCHAR2) RETURN VARCHAR2
	IS
--//============================================================================
--//
--// Object Name         :: get_lookup_meaning  
--//
--// Object Usage 		 :: This Object Referred by "Shipped Orders on Hold Report - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version      Author           Date          			Description
--//----------------------------------------------------------------------------
  --// 1.0        Siva  		  09-Jan-2018         		 TMS#20171228-00005 
--//============================================================================	
	L_HASH_INDEX varchar2(100);
	l_sql varchar2(32000);
	begin
	G_lookup_meaning := NULL;
	L_SQL :='SELECT flv.meaning
      FROM fnd_lookup_values flv
      WHERE flv.lookup_code = :1
	  AND flv.lookup_type   = :2'; 
		begin
			L_HASH_INDEX:=P_LOOKUP_CODE||'-'||P_LOOKUP_TYPE;
			G_lookup_meaning := G_lookup_meaning_VLDN_TBL(l_hash_index);
		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO G_lookup_meaning USING P_LOOKUP_CODE,p_lookup_type;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		G_lookup_meaning :=null;
		WHEN OTHERS THEN
		G_lookup_meaning :=null;
		END;      
						   l_hash_index:=P_LOOKUP_CODE||'-'||p_lookup_type;
						   G_lookup_meaning_VLDN_TBL(L_HASH_INDEX) := G_lookup_meaning;
		end;
		RETURN  G_lookup_meaning;
		EXCEPTION WHEN OTHERS THEN
		  G_lookup_meaning:=null;
		  RETURN  G_lookup_meaning;

	end GET_LOOKUP_MEANING;


	FUNCTION get_employee_name (P_created_by in number) RETURN VARCHAR2
	IS
--//============================================================================
--//
--// Object Name         :: get_employee_name  
--//
--// Object Usage 		 :: This Object Referred by "Shipped Orders on Hold Report - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version      Author           Date          			Description
--//----------------------------------------------------------------------------
  --// 1.0        Siva  		  09-Jan-2018         		 TMS#20171228-00005 
--//============================================================================	
	L_HASH_INDEX number;
	l_sql varchar2(32000);
	begin
	g_employee_name := NULL;
	L_SQL :='SELECT ppf.full_name
      FROM fnd_user fu,
        per_all_people_f ppf
      WHERE fu.user_id   = :1
      AND fu.employee_id = ppf.person_id
      AND TRUNC(sysdate) BETWEEN ppf.effective_start_date AND ppf.effective_end_date'; 
		begin
			L_HASH_INDEX:=P_created_by;
			g_employee_name := g_employee_name_vldn_tbl(l_hash_index);
		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO g_employee_name USING P_created_by;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		g_employee_name :=null;
		WHEN OTHERS THEN
		g_employee_name :=null;
		END;      
						   l_hash_index:=P_created_by;
						   g_employee_name_vldn_tbl(L_HASH_INDEX) := g_employee_name;
		end;
		RETURN  g_employee_name;
		EXCEPTION WHEN OTHERS THEN
		  g_employee_name:=null;
		  RETURN  g_employee_name;

	END get_employee_name;

FUNCTION get_header_hold_name (P_HEADER_ID in number) RETURN VARCHAR2
IS
--//============================================================================
--//
--// Object Name         :: get_header_hold_name  
--//
--// Object Usage 		 :: This Object Referred by "Shipped Orders on Hold Report - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version      Author           Date          			Description
--//----------------------------------------------------------------------------
  --// 1.0        Siva  		  09-Jan-2018         		 TMS#20171228-00005 
--//============================================================================	
	L_HASH_INDEX number;
	l_sql varchar2(32000);
	begin
	g_hdr_hold_name := NULL;
	L_SQL :='SELECT DECODE(COUNT(DISTINCT ohd.hold_id), 0 , NULL , 1 , MIN(ohd.name),''Multiple Holds Exist'') header_hold
      FROM oe_order_holds hold,
        oe_hold_sources ohs,
        oe_hold_definitions ohd
      WHERE hold.header_id      = :1
      AND hold.line_id         IS NULL
      AND hold.hold_source_id   = ohs.hold_source_id
      AND hold.hold_release_id IS NULL
      AND ohd.name             <> ''XXWC_PRICE_CHANGE_HOLD''
      AND ohs.hold_id           = ohd.hold_id
      GROUP BY hold.header_id,
        hold.line_id'; 
		begin
			L_HASH_INDEX:=P_HEADER_ID;
			g_hdr_hold_name := g_hdr_hold_name_vldn_tbl(l_hash_index);
		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO g_hdr_hold_name USING P_HEADER_ID;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		g_hdr_hold_name :=null;
		WHEN OTHERS THEN
		g_hdr_hold_name :=null;
		END;      
						   l_hash_index:=P_HEADER_ID;
						   g_hdr_hold_name_vldn_tbl(L_HASH_INDEX) := g_hdr_hold_name;
		end;
		RETURN  g_hdr_hold_name;
		EXCEPTION WHEN OTHERS THEN
		  g_hdr_hold_name:=null;
		  RETURN  g_hdr_hold_name;

	end get_header_hold_name;

FUNCTION get_hdr_header_hold_name (P_HEADER_ID in number) RETURN VARCHAR2
IS
--//============================================================================
--//
--// Object Name         :: get_hdr_header_hold_name  
--//
--// Object Usage 		 :: This Object Referred by "Shipped Orders on Hold Report - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version      Author           Date          			Description
--//----------------------------------------------------------------------------
  --// 1.0        Siva  		  09-Jan-2018         		 TMS#20171228-00005 
--//============================================================================	
	L_HASH_INDEX number;
	l_sql varchar2(32000);
	begin
	g_header_hold_name := NULL;
	L_SQL :='SELECT DECODE(COUNT(DISTINCT ohd.hold_id), 0 , ''No Hold'' , 1 , MIN(ohd.name),''Multiple Holds Exist'') header_hold
      FROM oe_order_holds hold,
        oe_hold_sources ohs,
        oe_hold_definitions ohd
      WHERE hold.header_id      = :1
      AND hold.line_id         IS NULL
      AND hold.hold_source_id   = ohs.hold_source_id
      AND hold.hold_release_id IS NULL
      AND ohd.name             <> ''XXWC_PRICE_CHANGE_HOLD''
      AND ohs.hold_id           = ohd.hold_id
      GROUP BY hold.header_id,
        hold.line_id'; 
		begin
			L_HASH_INDEX:=P_HEADER_ID;
			g_header_hold_name := g_header_hold_name_vldn_tbl(l_hash_index);
		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO g_header_hold_name USING P_HEADER_ID;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		g_header_hold_name :=null;
		WHEN OTHERS THEN
		g_header_hold_name :=null;
		END;      
						   l_hash_index:=P_HEADER_ID;
						   g_header_hold_name_vldn_tbl(L_HASH_INDEX) := g_header_hold_name;
		end;
		RETURN  g_header_hold_name;
		EXCEPTION WHEN OTHERS THEN
		  g_header_hold_name:=null;
		  RETURN  g_header_hold_name;

	end get_hdr_header_hold_name;

FUNCTION get_line_hold_name (P_HEADER_ID in number,P_LINE_ID in number) RETURN VARCHAR2
IS
--//============================================================================
--//
--// Object Name         :: get_line_hold_name  
--//
--// Object Usage 		 :: This Object Referred by "Shipped Orders on Hold Report - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version      Author           Date          			Description
--//----------------------------------------------------------------------------
  --// 1.0        Siva  		  09-Jan-2018         		 TMS#20171228-00005 
--//============================================================================	
	L_HASH_INDEX varchar2(100);
	l_sql varchar2(32000);
	begin
	g_line_hold_name := NULL;
	L_SQL :='SELECT DECODE(COUNT(DISTINCT ohd.hold_id), 0 , NULL , 1 , MIN(ohd.name),''Multiple Holds Exist'') line_hold
      FROM oe_order_holds hold,
        oe_hold_sources ohs,
        oe_hold_definitions ohd
      WHERE hold.header_id      = :1
      AND hold.line_id          = :2
      AND hold.hold_source_id   = ohs.hold_source_id
      AND hold.hold_release_id IS NULL
      AND ohd.name             <> ''XXWC_PRICE_CHANGE_HOLD''
      AND ohs.hold_id           = ohd.hold_id
      GROUP BY hold.header_id,
        hold.line_id'; 
		begin
			L_HASH_INDEX:=P_HEADER_ID||'-'||P_LINE_ID;
			g_line_hold_name := g_line_hold_name_vldn_tbl(l_hash_index);
		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO g_line_hold_name USING P_HEADER_ID,P_LINE_ID;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		g_line_hold_name :=null;
		WHEN OTHERS THEN
		g_line_hold_name :=null;
		END;      
						   l_hash_index:=P_HEADER_ID||'-'||P_LINE_ID;
						   g_line_hold_name_vldn_tbl(L_HASH_INDEX) := g_line_hold_name;
		end;
		RETURN  g_line_hold_name;
		EXCEPTION WHEN OTHERS THEN
		  g_line_hold_name:=null;
		  RETURN  g_line_hold_name;

	end get_line_hold_name;

FUNCTION get_header_hold_date (P_HEADER_ID in number) RETURN date
IS
--//============================================================================
--//
--// Object Name         :: get_header_hold_date  
--//
--// Object Usage 		 :: This Object Referred by "Shipped Orders on Hold Report - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version      Author           Date          			Description
--//----------------------------------------------------------------------------
  --// 1.0        Siva  		  09-Jan-2018         		 TMS#20171228-00005 
--//============================================================================	
	L_HASH_INDEX number;
	l_sql varchar2(32000);
	begin
	g_header_hold_date := NULL;
	L_SQL :='SELECT MAX(hold.creation_date) header_hold_date
      FROM oe_order_holds hold,
        oe_hold_sources ohs,
        oe_hold_definitions ohd
      WHERE hold.header_id      = :1
      AND hold.line_id         IS NULL
      AND hold.hold_source_id   = ohs.hold_source_id
      AND hold.hold_release_id IS NULL
      AND ohd.name             <> ''XXWC_PRICE_CHANGE_HOLD''
      AND ohs.hold_id           = ohd.hold_id
      GROUP BY hold.header_id'; 
		begin
			L_HASH_INDEX:=P_HEADER_ID;
			g_header_hold_date := g_hdr_hold_date_vldn_tbl(l_hash_index);
		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO g_header_hold_date USING P_HEADER_ID;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		g_header_hold_date :=null;
		WHEN OTHERS THEN
		g_header_hold_date :=null;
		END;      
						   l_hash_index:=P_HEADER_ID;
						   g_hdr_hold_date_vldn_tbl(L_HASH_INDEX) := g_header_hold_date;
		end;
		RETURN  g_header_hold_date;
		EXCEPTION WHEN OTHERS THEN
		  g_header_hold_date:=null;
		  RETURN  g_header_hold_date;

	end get_header_hold_date;


FUNCTION get_line_hold_by_ntid (p_hold_source_id in number,p_header_id in number,p_line_id in number) RETURN VARCHAR2
IS
--//============================================================================
--//
--// Object Name         :: g_line_hold_by_ntid  
--//
--// Object Usage 		 :: This Object Referred by "Shipped Orders on Hold Report - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version      Author           Date          			Description
--//----------------------------------------------------------------------------
  --// 1.0        Siva  		  09-Jan-2018         		 TMS#20171228-00005 
--//============================================================================	
	L_HASH_INDEX varchar2(100);
	l_sql varchar2(32000);
	begin
	g_line_hold_by_ntid := NULL;
	L_SQL :='SELECT fu.user_name
      FROM oe_order_holds oohd,
        fnd_user fu
      WHERE oohd.created_by     = fu.user_id
      AND oohd.line_id         IS NOT NULL
      AND oohd.hold_release_id IS NULL
      AND oohd.hold_source_id   = :1
      AND oohd.header_id        = :2
      AND oohd.line_id          = :3
      AND rownum                = 1'; 
	  
		begin
			L_HASH_INDEX:=p_hold_source_id||'-'||p_header_id||'-'||p_line_id;
			g_line_hold_by_ntid := g_line_holdby_ntid_vldn_tbl(l_hash_index);
		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO g_line_hold_by_ntid USING p_hold_source_id,p_header_id,p_line_id;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		g_line_hold_by_ntid :=null;
		WHEN OTHERS THEN
		g_line_hold_by_ntid :=null;
		END;      
						   l_hash_index:=p_hold_source_id||'-'||p_header_id||'-'||p_line_id;
						   g_line_holdby_ntid_vldn_tbl(L_HASH_INDEX) := g_line_hold_by_ntid;
		end;
		RETURN  g_line_hold_by_ntid;
		EXCEPTION WHEN OTHERS THEN
		  g_line_hold_by_ntid:=null;
		  RETURN  g_line_hold_by_ntid;

	END get_line_hold_by_ntid;

FUNCTION get_hdr_hold_by_ntid (p_hold_source_id in number,p_header_id in number) RETURN VARCHAR2
IS
--//============================================================================
--//
--// Object Name         :: g_hdr_hold_by_ntid  
--//
--// Object Usage 		 :: This Object Referred by "Shipped Orders on Hold Report - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version      Author           Date          			Description
--//----------------------------------------------------------------------------
  --// 1.0        Siva  		  09-Jan-2018         		 TMS#20171228-00005 
--//============================================================================	
	L_HASH_INDEX varchar2(100);
	l_sql varchar2(32000);
	begin
	g_hdr_hold_by_ntid := NULL;
	L_SQL :='SELECT fu.user_name
      FROM oe_order_holds oohd,
        fnd_user fu
      WHERE oohd.created_by     =fu.user_id
      AND oohd.line_id         IS NULL
      AND oohd.hold_release_id IS NULL
      AND oohd.hold_source_id   = :1
      AND oohd.header_id        = :2
      AND rownum                =1'; 
	  
		begin
			L_HASH_INDEX:=p_hold_source_id||'-'||p_header_id;
			g_hdr_hold_by_ntid := g_hdr_holdby_ntid_vldn_tbl(l_hash_index);
		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO g_hdr_hold_by_ntid USING p_hold_source_id,p_header_id;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		g_hdr_hold_by_ntid :=null;
		WHEN OTHERS THEN
		g_hdr_hold_by_ntid :=null;
		END;      
						   l_hash_index:= p_hold_source_id||'-'||p_header_id;
						   g_hdr_holdby_ntid_vldn_tbl(L_HASH_INDEX) := g_hdr_hold_by_ntid;
		end;
		RETURN  g_hdr_hold_by_ntid;
		EXCEPTION WHEN OTHERS THEN
		  g_hdr_hold_by_ntid:=null;
		  RETURN  g_hdr_hold_by_ntid;

	end get_hdr_hold_by_ntid;

FUNCTION get_hdr_hold_by_name (p_hold_source_id in number,p_header_id in number) RETURN VARCHAR2
IS
--//============================================================================
--//
--// Object Name         :: g_hdr_hold_by_name  
--//
--// Object Usage 		 :: This Object Referred by "Shipped Orders on Hold Report - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version      Author           Date          			Description
--//----------------------------------------------------------------------------
  --// 1.0        Siva  		  09-Jan-2018         		 TMS#20171228-00005 
--//============================================================================	
	L_HASH_INDEX varchar2(100);
	l_sql varchar2(32000);
	begin
	g_hdr_hold_by_name := NULL;
	L_SQL :='SELECT ppf.full_name
      FROM oe_order_holds oohd,
        fnd_user fu,
        per_all_people_f ppf
      WHERE oohd.created_by =fu.user_id
      AND fu.employee_id    =ppf.person_id
      AND TRUNC(sysdate) BETWEEN ppf.effective_start_date AND ppf.effective_end_date
      AND oohd.line_id         IS NULL
      AND oohd.hold_release_id IS NULL
      AND oohd.hold_source_id   = :1
      AND oohd.header_id        = :2
      AND rownum                = 1'; 
	  
		begin
			L_HASH_INDEX:=p_hold_source_id||'-'||p_header_id;
			g_hdr_hold_by_name := g_hdr_holdby_name_vldn_tbl(l_hash_index);
		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO g_hdr_hold_by_name USING p_hold_source_id,p_header_id;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		g_hdr_hold_by_name :=null;
		WHEN OTHERS THEN
		g_hdr_hold_by_name :=null;
		END;      
						   l_hash_index:= p_hold_source_id||'-'||p_header_id;
						   g_hdr_holdby_name_vldn_tbl(L_HASH_INDEX) := g_hdr_hold_by_name;
		end;
		RETURN  g_hdr_hold_by_name;
		EXCEPTION WHEN OTHERS THEN
		  g_hdr_hold_by_name:=null;
		  RETURN  g_hdr_hold_by_name;

	end get_hdr_hold_by_name;

FUNCTION get_line_hold_by_name (p_hold_source_id in number,p_header_id in number,p_line_id in number) RETURN VARCHAR2
IS
--//============================================================================
--//
--// Object Name         :: g_line_hold_by_name  
--//
--// Object Usage 		 :: This Object Referred by "Shipped Orders on Hold Report - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version      Author           Date          			Description
--//----------------------------------------------------------------------------
  --// 1.0        Siva  		  09-Jan-2018         		 TMS#20171228-00005 
--//============================================================================	
	L_HASH_INDEX varchar2(100);
	l_sql varchar2(32000);
	begin
	g_line_hold_by_name := NULL;
	L_SQL :='SELECT ppf.full_name
      FROM oe_order_holds oohd,
        fnd_user fu,
        per_all_people_f ppf
      WHERE oohd.created_by =fu.user_id
      AND fu.employee_id    =ppf.person_id
      AND TRUNC(sysdate) BETWEEN ppf.effective_start_date AND ppf.effective_end_date
      AND oohd.line_id         IS NOT NULL
      AND oohd.hold_release_id IS NULL
      AND oohd.hold_source_id   = :1
      AND oohd.header_id        = :2
      AND oohd.line_id          = :3
      AND rownum                =1'; 
	  
		begin
			L_HASH_INDEX:=p_hold_source_id||'-'||p_header_id||'-'||p_line_id;
			g_line_hold_by_name := g_line_holdby_name_vldn_tbl(l_hash_index);
		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO g_line_hold_by_name USING p_hold_source_id,p_header_id,p_line_id;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		g_line_hold_by_name :=null;
		WHEN OTHERS THEN
		g_line_hold_by_name :=null;
		END;      
						   l_hash_index:= p_hold_source_id||'-'||p_header_id||'-'||p_line_id;
						   g_line_holdby_name_vldn_tbl(L_HASH_INDEX) := g_line_hold_by_name;
		end;
		RETURN  g_line_hold_by_name;
		EXCEPTION WHEN OTHERS THEN
		  g_line_hold_by_name:=null;
		  RETURN  g_line_hold_by_name;

	end get_line_hold_by_name;
  
procedure order_hold_proc (p_warehouse       in varchar2,
							  p_order_date_from in date,
							  p_order_date_to   in date,
							  P_ORDER_TYPE      IN VARCHAR2,
							  P_HOLD_TYPE       IN VARCHAR2							  
							  )
IS
  --//============================================================================
  --//
  --// Object Name         :: ORDER_HOLD_PROC
  --//
  --// Object Type         :: Procedure
  --//
  --// Object Description  :: This procedure will insert the values into a temp table
  --//
  --// Version Control
  --//============================================================================
  --// Vers      Author           Date          			Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva  		  09-Jan-2018         		 TMS#20171228-00005 
  --//============================================================================
        l_err_msg         VARCHAR2 (3000);
        L_Err_Callfrom    Varchar2 (50);
        L_Err_Callpoint   Varchar2 (50);
        lc_where_cond 	  VARCHAR2 (32000);


  type l_header_hold_type_tbl
is
  table of xxeis.EIS_XXWC_HEADER_HOLDS_TBL%rowtype index by BINARY_INTEGER;
  l_header_hold_rec_tab l_header_hold_type_tbl;

  type l_line_hold_type_tbl
is
  table of xxeis.EIS_XXWC_LINE_HOLDS_TBL%rowtype index by BINARY_INTEGER;
  l_line_hold_rec_tab l_line_hold_type_tbl;


  TYPE L_SQL_QUERY1_TYPE_TBL
IS
  TABLE OF XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_TBL%ROWTYPE INDEX BY BINARY_INTEGER;
  L_SQL_QUERY1_TAB L_SQL_QUERY1_TYPE_TBL;
  
  TYPE L_SQL_QUERY2_TYPE_TBL
is
  TABLE OF XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_TBL%ROWTYPE INDEX BY BINARY_INTEGER;
  L_SQL_QUERY2_tab L_SQL_QUERY2_TYPE_TBL;

  TYPE L_SQL_QUERY3_TYPE_TBL
is
  TABLE OF XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_TBL%ROWTYPE INDEX BY BINARY_INTEGER;
  L_SQL_QUERY3_tab L_SQL_QUERY3_TYPE_TBL;

  TYPE L_SQL_QUERY4_TYPE_TBL
is
  TABLE OF XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_TBL%ROWTYPE INDEX BY BINARY_INTEGER;
  L_SQL_QUERY4_tab L_SQL_QUERY4_TYPE_TBL;

  TYPE L_SQL_QUERY5_TYPE_TBL
is
  TABLE OF XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_TBL%ROWTYPE INDEX BY BINARY_INTEGER;
  L_SQL_QUERY5_tab L_SQL_QUERY5_TYPE_TBL;

  TYPE L_SQL_QUERY6_TYPE_TBL
is
  TABLE OF XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_TBL%ROWTYPE INDEX BY BINARY_INTEGER;
  L_SQL_QUERY6_tab L_SQL_QUERY6_TYPE_TBL;  
  


  L_HEADER_HOLD_REF_CUR SYS_REFCURSOR;
  L_LINE_HOLD_REF_CUR SYS_REFCURSOR;
  L_SQL_QUERY1_CUR SYS_REFCURSOR;
  L_SQL_QUERY2_CUR SYS_REFCURSOR;
  L_SQL_QUERY3_CUR SYS_REFCURSOR;
  L_SQL_QUERY4_CUR SYS_REFCURSOR;
  L_SQL_QUERY5_CUR SYS_REFCURSOR;
  L_SQL_QUERY6_CUR SYS_REFCURSOR;
  

  
L_HEADER_HOLDS_SQL VARCHAR2(32000) :='SELECT MAX(hold_source_id) hold_source_id,
        TRUNC(MAX(creation_date)) header_hold_date,
        header_id
      FROM oe_order_holds
      WHERE line_id       IS NULL
      AND hold_release_id IS NULL
      GROUP BY header_id
  ';  
  
L_LINE_HOLDS_SQL VARCHAR2(32000) :='SELECT MAX(a.hold_source_id) hold_source_id,
        MAX(a.creation_date) line_hold_date,
        a.line_id,
        a.header_id
      FROM oe_order_holds a,
        oe_hold_sources b,
        oe_hold_definitions c
      WHERE a.line_id       IS NOT NULL
      AND a.hold_release_id IS NULL
      AND a.hold_source_id   = b.hold_source_id
      AND b.hold_id          = c.hold_id
      AND c.name            <> ''XXWC_PRICE_CHANGE_HOLD''
      GROUP BY a.header_id,
        a.line_id
  '; 


L_SQL_QUERY1 VARCHAR2(32000) := 'select /*+ INDEX(hca HZ_CUST_ACCOUNTS_U1)*/
      hca.account_number customer_number ,
      nvl (hca.account_name, hzp.party_name) customer_name,
      mp.organization_code loc,
      oh.order_number sales_order_number,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_employee_name(oh.created_by) created_by,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.GET_LOOKUP_MEANING(oh.flow_status_code,''FLOW_STATUS'') order_header_status,
      ottl.description order_type,
      TRUNC (oh.ordered_date) ordered_date ,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.GET_LOOKUP_MEANING(ol.flow_status_code,''LINE_FLOW_STATUS'') order_line_status,
      DECODE (ol.option_number,'''',(ol.line_number
      ||''.''
      ||ol.shipment_number),(ol.line_number
      ||''.''
      ||ol.shipment_number
      ||''.''
      ||ol.option_number)) order_line_number,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_header_hold_name(oh.header_id) order_header_hold_name,
      DECODE(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_header_hold_name(oh.header_id),NULL,''No'',''Yes'') order_header_hold,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_line_hold_name(oh.header_id,ol.line_id) order_line_hold_name,
      DECODE(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_line_hold_name(oh.header_id,ol.line_id),NULL,''No'',''Yes'') order_line_hold,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_header_hold_date(oh.header_id) header_hold_date,      
      hold.line_hold_date ,
      NVL(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_header_hold_date(oh.header_id),hold.line_hold_date) hold_date,
      msi.segment1 part_number,
      msi.description part_description,
      (DECODE(ohd.NAME,''Invoicing Hold'',(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_line_hold_by_ntid(hold.hold_source_id,hold.header_id,hold.line_id)))) HOLD_BY_NTID,
      (DECODE(ohd.NAME,''Invoicing Hold'',(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_line_hold_by_name(hold.hold_source_id,hold.header_id,hold.line_id)))) HOLD_BY_ASSOCIATE_NAME
    FROM
      XXEIS.EIS_XXWC_LINE_HOLDS_TBL hold,
      oe_hold_sources ohs,
      oe_hold_definitions ohd,
      oe_order_lines ol ,
      oe_order_headers oh,
      hz_cust_accounts hca ,
      hz_parties hzp,
      mtl_system_items_b msi,
      mtl_parameters mp,
      oe_transaction_types_tl ottl
    WHERE hold.hold_source_id     =ohs.hold_source_id
    AND ohd.name                 <> ''XXWC_PRICE_CHANGE_HOLD''
    AND ohs.hold_id               =ohd.hold_id
    AND hold.line_id              = ol.line_id
    AND ol.flow_status_code      <>''CLOSED''
    AND ol.header_id              = oh.header_id
    AND oh.sold_to_org_id         = hca.cust_account_id
    AND hca.party_id              = hzp.party_id
    AND ol.ship_from_org_id       = msi.organization_id
    AND ol.inventory_item_id      = msi.inventory_item_id
    AND ol.flow_status_code       = ''INVOICE_HOLD''
    AND ol.user_item_description = ''OUT_FOR_DELIVERY''
    and ol.ship_from_org_id       = mp.organization_id 
    AND oh.order_type_id          = ottl.transaction_type_id
    AND ottl.language             = userenv(''LANG'')
    AND ottl.NAME                 = ''STANDARD ORDER''
';

L_SQL_QUERY2 VARCHAR2(32000) := 'select /*+ INDEX(hca HZ_CUST_ACCOUNTS_U1)*/
      hca.account_number customer_number ,
      nvl (hca.account_name, hzp.party_name) customer_name,
      mp.organization_code loc,
      oh.order_number sales_order_number,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_employee_name(oh.created_by) created_by,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.GET_LOOKUP_MEANING(oh.flow_status_code,''FLOW_STATUS'') order_header_status,
      ottl.description order_type,
      TRUNC (oh.ordered_date) ordered_date ,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.GET_LOOKUP_MEANING(ol.flow_status_code,''LINE_FLOW_STATUS'') order_line_status,
      DECODE (ol.option_number,'''',(ol.line_number
      ||''.''
      ||ol.shipment_number),(ol.line_number
      ||''.''
      ||ol.shipment_number
      ||''.''
      ||ol.option_number)) order_line_number,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_header_hold_name(oh.header_id) order_header_hold_name,
      DECODE(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_header_hold_name(oh.header_id),NULL,''No'',''Yes'') order_header_hold,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_line_hold_name(oh.header_id,ol.line_id) order_line_hold_name,
      DECODE(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_line_hold_name(oh.header_id,ol.line_id),NULL,''No'',''Yes'') order_line_hold,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_header_hold_date(oh.header_id) header_hold_date,
      hold.line_hold_date ,
      NVL(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_header_hold_date(oh.header_id),hold.line_hold_date) hold_date,
      msi.segment1 part_number,
      msi.description part_description,
      (DECODE(ohd.name,''Invoicing Hold'',(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_line_hold_by_ntid(hold.hold_source_id,hold.header_id,hold.line_id)))) HOLD_BY_NTID,
      (DECODE(ohd.name,''Invoicing Hold'',(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_line_hold_by_name(hold.hold_source_id,hold.header_id,hold.line_id)))) HOLD_BY_ASSOCIATE_NAME
    FROM
      XXEIS.EIS_XXWC_LINE_HOLDS_TBL hold,
      oe_hold_sources ohs,
      oe_hold_definitions ohd,
      oe_order_lines ol ,
      oe_order_headers oh,
      hz_cust_accounts hca ,
      hz_parties hzp,
      mtl_system_items_b msi,
      mtl_parameters mp,
      oe_transaction_types_tl ottl
    WHERE hold.hold_source_id     =ohs.hold_source_id
    AND ohd.name                 <> ''XXWC_PRICE_CHANGE_HOLD''
    AND ohs.hold_id               =ohd.hold_id
    AND hold.line_id              = ol.line_id
    AND ol.flow_status_code      <>''CLOSED''
    AND ol.header_id              = oh.header_id
    AND oh.sold_to_org_id         = hca.cust_account_id
    AND hca.party_id              = hzp.party_id
    AND ol.ship_from_org_id       = msi.organization_id
    AND ol.inventory_item_id      = msi.inventory_item_id
    AND ol.flow_status_code       = ''INVOICE_HOLD''
    AND ol.user_item_description   = ''DELIVERED''
    and ol.ship_from_org_id       = mp.organization_id 
    AND oh.order_type_id          = ottl.transaction_type_id
    and ottl.language             = userenv(''LANG'')
    AND ottl.name                 = ''STANDARD ORDER''';


L_SQL_QUERY3 VARCHAR2(32000) :='select /*+ INDEX(hca HZ_CUST_ACCOUNTS_U1)*/
      hca.account_number customer_number ,
      NVL (hca.account_name, hzp.party_name) customer_name,
      mp.organization_code loc,
      oh.order_number sales_order_number,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_employee_name(oh.created_by) created_by,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.GET_LOOKUP_MEANING(oh.flow_status_code,''FLOW_STATUS'') order_header_status,
      ottl.description order_type,
      TRUNC (oh.ordered_date) ordered_date ,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.GET_LOOKUP_MEANING(ol.flow_status_code,''LINE_FLOW_STATUS'') order_line_status,
      DECODE (ol.option_number,'''',(ol.line_number
      ||''.''
      ||ol.shipment_number),(ol.line_number
      ||''.''
      ||ol.shipment_number
      ||''.''
      ||ol.option_number)) order_line_number,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_hdr_header_hold_name(oh.header_id) order_header_hold_name,
      DECODE(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_hdr_header_hold_name(oh.header_id),NULL,''No'',''Yes'') order_header_hold,
      NULL order_line_hold_name,
      ''No'' order_line_hold,
      hold.header_hold_date header_hold_date ,
      NULL line_hold_date,
      hold.header_hold_date hold_date,
      msi.segment1 part_number,
      msi.description part_description,
      (DECODE(ohd.NAME,''Invoicing Hold'',(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_hdr_hold_by_ntid(hold.hold_source_id,hold.header_id)))) HOLD_BY_NTID,
      (DECODE(ohd.name,''Invoicing Hold'',(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_hdr_hold_by_name(hold.hold_source_id,hold.header_id)))) HOLD_BY_ASSOCIATE_NAME
    FROM
      XXEIS.EIS_XXWC_HEADER_HOLDS_TBL hold,
      oe_hold_sources ohs,
      oe_hold_definitions ohd,
      oe_order_lines ol,
      oe_order_headers oh,
      hz_cust_accounts hca ,
      hz_parties hzp,
      mtl_system_items_b msi,
      mtl_parameters mp,
      oe_transaction_types_tl ottl
    WHERE hold.hold_source_id     = ohs.hold_source_id
    AND ohs.hold_id               = ohd.hold_id
    AND hold.header_id            = ol.header_id
    AND ol.header_id              = oh.header_id
    AND ol.flow_status_code      <>''CLOSED''
    AND oh.sold_to_org_id         = hca.cust_account_id
    AND hca.party_id              = hzp.party_id
    AND ol.ship_from_org_id       = msi.organization_id
    AND ol.inventory_item_id      = msi.inventory_item_id
    AND ol.flow_status_code       = ''INVOICE_HOLD''
    AND ol.user_item_description = ''OUT_FOR_DELIVERY''
    and ol.ship_from_org_id       = mp.organization_id
    AND oh.order_type_id          = ottl.transaction_type_id
    AND ottl.language             = userenv(''LANG'')
    AND ottl.name                 = ''STANDARD ORDER''
    AND NOT EXISTS
      (SELECT 1
      FROM oe_order_holds a ,
        oe_hold_sources b,
        oe_hold_definitions c
      WHERE a.header_id      = ol.header_id
      AND a.line_id          = ol.line_id
      AND a.hold_source_id   = b.hold_source_id
      AND b.hold_id          = c.hold_id
      AND c.name            <>''XXWC_PRICE_CHANGE_HOLD''
      AND a.hold_release_id IS NULL
      )';
      
      
L_SQL_QUERY4 VARCHAR2(32000) :='select /*+ INDEX(hca HZ_CUST_ACCOUNTS_U1)*/
      hca.account_number customer_number ,
      NVL (hca.account_name, hzp.party_name) customer_name,
      mp.organization_code loc,
      oh.order_number sales_order_number,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_employee_name(oh.created_by) created_by,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.GET_LOOKUP_MEANING(oh.flow_status_code,''FLOW_STATUS'') order_header_status,
      ottl.description order_type,
      TRUNC (oh.ordered_date) ordered_date ,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.GET_LOOKUP_MEANING(ol.flow_status_code,''LINE_FLOW_STATUS'') order_line_status,
      DECODE (ol.option_number,'''',(ol.line_number
      ||''.''
      ||ol.shipment_number),(ol.line_number
      ||''.''
      ||ol.shipment_number
      ||''.''
      ||ol.option_number)) order_line_number,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_hdr_header_hold_name(oh.header_id) order_header_hold_name,
      DECODE(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_hdr_header_hold_name(oh.header_id),NULL,''No'',''Yes'') order_header_hold,
      NULL order_line_hold_name,
      ''No'' order_line_hold,
      hold.header_hold_date header_hold_date ,
      NULL line_hold_date,
      hold.header_hold_date hold_date,
      msi.segment1 part_number,
      msi.description part_description,
      (DECODE(ohd.name,''Invoicing Hold'',(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_hdr_hold_by_ntid(hold.hold_source_id,hold.header_id)))) HOLD_BY_NTID,
      (DECODE(ohd.name,''Invoicing Hold'',(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_hdr_hold_by_name(hold.hold_source_id,hold.header_id)))) HOLD_BY_ASSOCIATE_NAME
    FROM
      XXEIS.EIS_XXWC_HEADER_HOLDS_TBL hold,
      oe_hold_sources ohs,
      oe_hold_definitions ohd,
      oe_order_lines ol,
      oe_order_headers oh,
      hz_cust_accounts hca ,
      hz_parties hzp,
      mtl_system_items_b msi,
      mtl_parameters mp,
      oe_transaction_types_tl ottl
    WHERE hold.hold_source_id     = ohs.hold_source_id
    AND ohs.hold_id               = ohd.hold_id
    AND hold.header_id            = ol.header_id
    AND ol.header_id              = oh.header_id
    AND ol.flow_status_code      <>''CLOSED''
    AND oh.sold_to_org_id         = hca.cust_account_id
    AND hca.party_id              = hzp.party_id
    AND ol.ship_from_org_id       = msi.organization_id
    AND ol.inventory_item_id      = msi.inventory_item_id
    AND ol.flow_status_code       = ''INVOICE_HOLD''
    AND ol.user_item_description   = ''DELIVERED''
    and ol.ship_from_org_id       = mp.organization_id
    AND oh.order_type_id          = ottl.transaction_type_id
    AND ottl.language             = userenv(''LANG'')
    AND ottl.name                 = ''STANDARD ORDER''
    AND NOT EXISTS
      (SELECT 1
      FROM oe_order_holds a ,
        oe_hold_sources b,
        oe_hold_definitions c
      WHERE a.header_id      = ol.header_id
      AND a.line_id          = ol.line_id
      AND a.hold_source_id   = b.hold_source_id
      AND b.hold_id          = c.hold_id
      AND c.name            <>''XXWC_PRICE_CHANGE_HOLD''
      AND a.hold_release_id IS NULL
      )';      
            
L_SQL_QUERY5 VARCHAR2(32000) :=' select /*+ INDEX(hca HZ_CUST_ACCOUNTS_U1)*/
      hca.account_number customer_number ,
      NVL (hca.account_name, hzp.party_name) customer_name,
      mp.organization_code loc,
      oh.order_number sales_order_number,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_employee_name(oh.created_by) created_by,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.GET_LOOKUP_MEANING(oh.flow_status_code,''FLOW_STATUS'') order_header_status,
      ottl.description order_type,
      TRUNC (oh.ordered_date) ordered_date ,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.GET_LOOKUP_MEANING(ol.flow_status_code,''LINE_FLOW_STATUS'') order_line_status,
      DECODE (ol.option_number,'''',(ol.line_number
      ||''.''
      ||ol.shipment_number),(ol.line_number
      ||''.''
      ||ol.shipment_number
      ||''.''
      ||ol.option_number)) order_line_number,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_header_hold_name(oh.header_id) order_header_hold_name,
      DECODE(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_header_hold_name(oh.header_id),NULL,''No'',''Yes'') order_header_hold,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_line_hold_name(oh.header_id,ol.line_id) order_line_hold_name,
      DECODE(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_line_hold_name(oh.header_id,ol.line_id),NULL,''No'',''Yes'') order_line_hold,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_header_hold_date(oh.header_id) header_hold_date,
      hold.line_hold_date ,
      NVL(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_header_hold_date(oh.header_id),hold.line_hold_date) hold_date,
      msi.segment1 part_number,
      msi.description part_description,
      DECODE(ottl.description,''RETURN ORDER'',NULL,(DECODE(ohd.name,''Invoicing Hold'',(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_line_hold_by_ntid(hold.hold_source_id,hold.header_id,hold.line_id))))) HOLD_BY_NTID,
      DECODE(ottl.description,''RETURN ORDER'',NULL,(DECODE(ohd.name,''Invoicing Hold'',(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_line_hold_by_name(hold.hold_source_id,hold.header_id,hold.line_id))))) HOLD_BY_ASSOCIATE_NAME
    FROM
      XXEIS.EIS_XXWC_LINE_HOLDS_TBL hold,
      oe_hold_sources ohs,
      oe_hold_definitions ohd,
      oe_order_lines ol ,
      oe_order_headers oh,
      hz_cust_accounts hca ,
      hz_parties hzp,
      mtl_system_items_b msi,
      mtl_parameters mp,
      oe_transaction_types_tl ottl
    WHERE hold.hold_source_id =ohs.hold_source_id
    AND ohd.name             <> ''XXWC_PRICE_CHANGE_HOLD''
    AND ohs.hold_id           =ohd.hold_id
    AND hold.line_id          = ol.line_id
    AND ol.flow_status_code  <>''CLOSED''
    AND ol.header_id          = oh.header_id
    AND oh.sold_to_org_id     = hca.cust_account_id
    AND hca.party_id          = hzp.party_id
    AND ol.ship_from_org_id   = msi.organization_id
    AND ol.inventory_item_id  = msi.inventory_item_id
    and ol.flow_status_code   = ''INVOICE_HOLD''
    and ol.ship_from_org_id   = mp.organization_id
    AND oh.order_type_id      = ottl.transaction_type_id
    AND ottl.language         = userenv(''LANG'')
    AND ottl.name            IN (''RETURN ORDER'',''COUNTER ORDER'')';
    
L_SQL_QUERY6 VARCHAR2(32000) :=' select  /*+ INDEX(hca HZ_CUST_ACCOUNTS_U1)*/
    hca.account_number customer_number ,
      NVL (hca.account_name, hzp.party_name) customer_name,
      mp.organization_code loc,
      oh.order_number sales_order_number,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_employee_name(oh.created_by) created_by,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.GET_LOOKUP_MEANING(oh.flow_status_code,''FLOW_STATUS'') order_header_status,
      ottl.description order_type,
      TRUNC (oh.ordered_date) ordered_date ,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.GET_LOOKUP_MEANING(ol.flow_status_code,''LINE_FLOW_STATUS'') order_line_status,
      DECODE (ol.option_number,'''',(ol.line_number
      ||''.''
      ||ol.shipment_number),(ol.line_number
      ||''.''
      ||ol.shipment_number
      ||''.''
      ||ol.option_number)) order_line_number,
      xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_hdr_header_hold_name(oh.header_id) order_header_hold_name,
      DECODE(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_hdr_header_hold_name(oh.header_id),NULL,''No'',''Yes'') order_header_hold,
      NULL order_line_hold_name,
      ''No'' order_line_hold,
      hold.header_hold_date header_hold_date ,
      NULL line_hold_date,
      hold.header_hold_date hold_date,
      msi.segment1 part_number,
      msi.description part_description,
      DECODE(ottl.description,''RETURN ORDER'',NULL,(DECODE(ohd.NAME,''Invoicing Hold'',(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_hdr_hold_by_ntid(hold.hold_source_id,hold.header_id))))) HOLD_BY_NTID,
      DECODE(ottl.description,''RETURN ORDER'',NULL,(DECODE(ohd.name,''Invoicing Hold'',(xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_hdr_hold_by_name(hold.hold_source_id,hold.header_id))))) HOLD_BY_ASSOCIATE_NAME
    FROM
      XXEIS.EIS_XXWC_HEADER_HOLDS_TBL hold,
      oe_hold_sources ohs,
      oe_hold_definitions ohd,
      oe_order_lines ol,
      oe_order_headers oh,
      hz_cust_accounts hca ,
      hz_parties hzp,
      mtl_system_items_b msi,
      mtl_parameters mp,
      oe_transaction_types_tl ottl
    WHERE hold.hold_source_id = ohs.hold_source_id
    AND ohs.hold_id           = ohd.hold_id
    AND hold.header_id        = ol.header_id
    AND ol.header_id          = oh.header_id
    and ol.flow_status_code  <>''CLOSED''
    AND oh.sold_to_org_id     = hca.cust_account_id
    AND hca.party_id          = hzp.party_id
    AND ol.ship_from_org_id   = msi.organization_id
    AND ol.inventory_item_id  = msi.inventory_item_id
    and ol.flow_status_code   = ''INVOICE_HOLD''
    and ol.ship_from_org_id   = mp.organization_id
    AND oh.order_type_id      = ottl.transaction_type_id
    AND ottl.language         = userenv(''LANG'')
    AND ottl.name            IN (''RETURN ORDER'',''COUNTER ORDER'')
    AND NOT EXISTS
      (SELECT 1
      FROM oe_order_holds a ,
        oe_hold_sources b,
        oe_hold_definitions c
      WHERE a.header_id      = ol.header_id
      AND a.line_id          = ol.line_id
      AND a.hold_source_id   = b.hold_source_id
      AND b.hold_id          = c.hold_id
      AND c.name            <>''XXWC_PRICE_CHANGE_HOLD''
      AND a.hold_release_id IS NULL
      )';

BEGIN

      L_ERR_CALLFROM := 'EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.ORDER_HOLD_PROC';
      L_ERR_CALLPOINT := 'Order Hold Proc';
      

  IF P_WAREHOUSE  IS NOT NULL THEN
  IF P_WAREHOUSE = 'All' THEN
  LC_WHERE_COND:= LC_WHERE_COND||' and 1=1';
  ELSE
    LC_WHERE_COND:= LC_WHERE_COND||' and mp.organization_code in (' || REPLACE (XXEIS.EIS_RSC_UTILITY.GET_PARAM_VALUES (P_PARAM_VALUE => TRIM (P_WAREHOUSE ) ), '%%', ', ' ) || ')';
  END IF;    
  END IF;

  IF p_order_date_from      IS NOT NULL THEN
    lc_where_cond:= lc_where_cond||' and TRUNC (oh.ordered_date)>=(' || REPLACE (XXEIS.EIS_RSC_UTILITY.GET_PARAM_VALUES (P_PARAM_VALUE => TRIM (p_order_date_from ) ), '%%', ', ' ) || ')';
  END IF;

  IF P_ORDER_DATE_TO      IS NOT NULL THEN
    LC_WHERE_COND:= LC_WHERE_COND||' and TRUNC (oh.ordered_date)<=(' || REPLACE (XXEIS.EIS_RSC_UTILITY.GET_PARAM_VALUES (P_PARAM_VALUE => TRIM (p_order_date_to ) ), '%%', ', ' ) || ')';
  END IF;

  IF P_ORDER_TYPE      IS NOT NULL THEN
    lc_where_cond:= lc_where_cond||' and ottl.description in (' || REPLACE (XXEIS.EIS_RSC_UTILITY.GET_PARAM_VALUES (P_PARAM_VALUE => TRIM (P_ORDER_TYPE ) ), '%%', ', ' ) || ')';
  END IF;

   IF P_HOLD_TYPE      IS NOT NULL THEN
    LC_WHERE_COND:= LC_WHERE_COND||' and xxeis.EIS_XXWC_OM_SHIP_ORD_HOLD_PKG.get_hdr_header_hold_name(oh.header_id) in (' || REPLACE (XXEIS.EIS_RSC_UTILITY.GET_PARAM_VALUES (P_PARAM_VALUE => TRIM (P_HOLD_TYPE ) ), '%%', ', ' ) || ')';
  END IF;
  
  
  l_header_hold_rec_tab.delete;
  L_LINE_HOLD_REC_TAB.DELETE;  
  L_SQL_QUERY1_TAB.DELETE;
  L_SQL_QUERY2_TAB.DELETE;
  L_SQL_QUERY3_TAB.DELETE;
  L_SQL_QUERY4_TAB.DELETE;
  L_SQL_QUERY5_TAB.DELETE;
  L_SQL_QUERY6_TAB.DELETE;
  
    L_SQL_QUERY1 := L_SQL_QUERY1||' '||LC_WHERE_COND;
    L_SQL_QUERY2 := L_SQL_QUERY2||' '||LC_WHERE_COND;
    L_SQL_QUERY3 := L_SQL_QUERY3||' '||LC_WHERE_COND;
    L_SQL_QUERY4 := L_SQL_QUERY4||' '||LC_WHERE_COND;
    L_SQL_QUERY5 := L_SQL_QUERY5||' '||LC_WHERE_COND;
    L_SQL_QUERY6 := L_SQL_QUERY6||' '||LC_WHERE_COND;
    
    FND_FILE.PUT_LINE(FND_FILE.LOG,'L_SQL_QUERY1'||L_SQL_QUERY1);
    FND_FILE.PUT_LINE(FND_FILE.LOG,'L_SQL_QUERY2'||L_SQL_QUERY2);
    FND_FILE.PUT_LINE(FND_FILE.LOG,'L_SQL_QUERY3'||L_SQL_QUERY3);
    FND_FILE.PUT_LINE(FND_FILE.LOG,'L_SQL_QUERY4'||L_SQL_QUERY4);
    FND_FILE.PUT_LINE(FND_FILE.LOG,'L_SQL_QUERY5'||L_SQL_QUERY5);
    Fnd_File.Put_Line(Fnd_File.Log,'L_SQL_QUERY6'||L_SQL_QUERY6);
    

  Fnd_File.Put_Line(Fnd_File.Log,'L_HEADER_HOLDS_SQL'||L_HEADER_HOLDS_SQL);
  FND_FILE.PUT_LINE(FND_FILE.LOG,'L_LINE_HOLDS_SQL'||L_LINE_HOLDS_SQL);
  
  
OPEN L_HEADER_HOLD_REF_CUR FOR L_HEADER_HOLDS_SQL;
loop
  fetch l_header_hold_ref_cur bulk collect into l_header_hold_rec_tab limit 10000;
  dbms_output.Put_Line('record count'||l_header_hold_rec_tab.COUNT);
   if l_header_hold_rec_tab.COUNT>0
	Then  
		FORALL i in 1..l_header_hold_rec_tab.COUNT 
		insert into XXEIS.EIS_XXWC_HEADER_HOLDS_TBL values l_header_hold_rec_tab(i);
  END IF;
IF L_HEADER_HOLD_REF_CUR%NOTFOUND Then    
CLOSE L_HEADER_HOLD_REF_CUR;
EXIT;
end if;
END LOOP; 


OPEN l_line_hold_ref_cur FOR L_LINE_HOLDS_SQL;
loop
  FETCH l_line_hold_ref_cur bulk collect into l_line_hold_rec_tab limit 10000;
   if l_line_hold_rec_tab.COUNT>0
	Then  
		FORALL i in 1..l_line_hold_rec_tab.COUNT 
		insert into XXEIS.EIS_XXWC_LINE_HOLDS_TBL values l_line_hold_rec_tab(i);
  END IF;
IF l_line_hold_ref_cur%NOTFOUND Then    
CLOSE l_line_hold_ref_cur;
EXIT;
end if;
END LOOP; 


OPEN L_SQL_QUERY1_CUR FOR L_SQL_QUERY1;
loop
  fetch L_SQL_QUERY1_CUR bulk collect into L_SQL_QUERY1_TAB limit 10000;
   if L_SQL_QUERY1_TAB.COUNT>0
	Then  
		FORALL i in 1..L_SQL_QUERY1_TAB.COUNT 
		insert into XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_TBL values L_SQL_QUERY1_TAB(i);
  END IF;
IF L_SQL_QUERY1_CUR%NOTFOUND Then    
CLOSE L_SQL_QUERY1_CUR;
EXIT;
END IF;
END LOOP;



OPEN L_SQL_QUERY2_CUR FOR L_SQL_QUERY2;
loop
  fetch L_SQL_QUERY2_CUR bulk collect into L_SQL_QUERY2_tab limit 10000;
   if L_SQL_QUERY2_tab.COUNT>0
	Then  
		FORALL i in 1..L_SQL_QUERY2_tab.COUNT 
		insert into XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_TBL values L_SQL_QUERY2_tab(i);
  END IF;
IF L_SQL_QUERY2_CUR%NOTFOUND Then    
CLOSE L_SQL_QUERY2_CUR;
EXIT;
END IF;
END LOOP;


OPEN L_SQL_QUERY3_CUR FOR L_SQL_QUERY3;
loop
  fetch L_SQL_QUERY3_CUR bulk collect into L_SQL_QUERY3_tab limit 10000;
   if L_SQL_QUERY3_tab.COUNT>0
	Then  
		FORALL i in 1..L_SQL_QUERY3_tab.COUNT 
		insert into XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_TBL values L_SQL_QUERY3_tab(i);
  END IF;
IF L_SQL_QUERY3_CUR%NOTFOUND Then    
CLOSE L_SQL_QUERY3_CUR;
EXIT;
END IF;
END LOOP;


OPEN L_SQL_QUERY4_CUR FOR L_SQL_QUERY4;
loop
  fetch L_SQL_QUERY4_CUR bulk collect into L_SQL_QUERY4_tab limit 10000;
   if L_SQL_QUERY4_tab.COUNT>0
	Then  
		FORALL i in 1..L_SQL_QUERY4_tab.COUNT 
		insert into XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_TBL values L_SQL_QUERY4_tab(i);
  END IF;
IF L_SQL_QUERY4_CUR%NOTFOUND Then    
CLOSE L_SQL_QUERY4_CUR;
EXIT;
END IF;
END LOOP;


OPEN L_SQL_QUERY5_CUR FOR L_SQL_QUERY5;
loop
  fetch L_SQL_QUERY5_CUR bulk collect into L_SQL_QUERY5_tab limit 10000;
   if L_SQL_QUERY5_tab.COUNT>0
	Then  
		FORALL i in 1..L_SQL_QUERY5_tab.COUNT 
		insert into XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_TBL values L_SQL_QUERY5_tab(i);
  END IF;
IF L_SQL_QUERY5_CUR%NOTFOUND Then    
CLOSE L_SQL_QUERY5_CUR;
EXIT;
END IF;
END LOOP;


OPEN L_SQL_QUERY6_CUR FOR L_SQL_QUERY6;
loop
  fetch L_SQL_QUERY6_CUR bulk collect into L_SQL_QUERY6_tab limit 10000;
   if L_SQL_QUERY6_tab.COUNT>0
	Then  
		FORALL i in 1..L_SQL_QUERY6_tab.COUNT 
		insert into XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_TBL values L_SQL_QUERY6_tab(i);
  END IF;
IF L_SQL_QUERY6_CUR%NOTFOUND Then    
CLOSE L_SQL_QUERY6_CUR;
EXIT;
END IF;
END LOOP;

--Dbms_Output.Put_Line('end time '||To_Char(Sysdate,'DD-MON-YY HH24:MI:SS'));

EXCEPTION
      WHEN OTHERS
      THEN
      FND_FILE.PUT_LINE(FND_FILE.LOG,'Exception....');

         apps.xxcus_error_pkg.xxcus_error_main_api (p_called_from         => l_err_callfrom
                                                   ,p_calling             => l_err_callpoint
                                                   ,p_request_id          => -1
                                                   ,p_ora_error_msg       => SQLERRM
                                                   ,p_error_desc          => l_err_msg
                                                   ,P_DISTRIBUTION_LIST   => G_DFLT_EMAIL
                                                   ,p_module              => 'APPS');
END ORDER_HOLD_PROC;


end EIS_XXWC_OM_SHIP_ORD_HOLD_PKG;
/
