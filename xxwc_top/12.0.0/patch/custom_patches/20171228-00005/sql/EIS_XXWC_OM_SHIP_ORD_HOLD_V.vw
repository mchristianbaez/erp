-----------------------------------------------------------------------------------------------
/* *****************************************************************************
  $Header XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_V.vw $
  Module Name: Order Management
  PURPOSE: Shipped Orders on Hold Report - WC
  TMS Task Id : 20171213-00051 
  REVISIONS: Initial Version
  Ver        	Date        	Author                			Description
  ---------  ----------  ------------------    				----------------
  1.0        27/12/2017  		Siva       			Initial Version --20171213-00051 
************************************************************************************/
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_V (CUSTOMER_NUMBER, CUSTOMER_NAME, LOC, SALES_ORDER_NUMBER, CREATED_BY, ORDER_HEADER_STATUS,
 ORDER_TYPE, ORDERED_DATE, ORDER_LINE_STATUS, ORDER_LINE_NUMBER, ORDER_HEADER_HOLD_NAME, ORDER_HEADER_HOLD, ORDER_LINE_HOLD_NAME, ORDER_LINE_HOLD,
 HOLD_DATE, PART_NUMBER, PART_DESCRIPTION, HOLD_BY_NTID, HOLD_BY_ASSOCIATE_NAME)
AS
  SELECT CUSTOMER_NUMBER ,
    CUSTOMER_NAME ,
    LOC ,
    SALES_ORDER_NUMBER ,
    CREATED_BY ,
    ORDER_HEADER_STATUS ,
    ORDER_TYPE ,
    ORDERED_DATE ,
    ORDER_LINE_STATUS ,
    ORDER_LINE_NUMBER ,
    ORDER_HEADER_HOLD_NAME ,
    ORDER_HEADER_HOLD ,
    ORDER_LINE_HOLD_NAME ,
    ORDER_LINE_HOLD ,
    HOLD_DATE ,
    PART_NUMBER ,
    PART_DESCRIPTION ,
    HOLD_BY_NTID ,
    HOLD_BY_ASSOCIATE_NAME
  FROM XXEIS.EIS_XXWC_OM_SHIP_ORD_HOLD_TBL
/
