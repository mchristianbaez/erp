/******************************************************************************************************
-- File Name: TMS_20160926-00059_XXWC_B2B_POD_SO_INFO_TBL_N1.sql
--
-- PROGRAM TYPE: Index script
--
-- PURPOSE: Index to improve performance for 'XXWC B2B Create POD Files' program
-- HISTORY
-- ==========================================================================================================
-- ==========================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- --------------------------------------------------------------------
-- 1.0     04-OCT-2016   Pattabhi Avula  TMS#20160926-00059  PERF: 'XXWC B2B Create POD Files' Concurrent Program 
                                         Performance issue fixes
--                                       Initial version

************************************************************************************************************/
CREATE INDEX xxwc.XXWC_B2B_POD_SO_INFO_TBL_N1 ON xxwc.xxwc_b2b_pod_so_info_tbl
     (status,NVL(delivery_id,0))
/
