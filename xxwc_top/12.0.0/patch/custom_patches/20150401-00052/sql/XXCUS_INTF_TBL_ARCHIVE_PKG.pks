CREATE OR REPLACE PACKAGE APPS.XXCUS_INTF_TBL_ARCHIVE_PKG
IS
/********************************************************************************
FILE NAME: APPS.XXCUS_INTF_TBL_ARCHIVE_PKG.pkb

PROGRAM TYPE: PL/SQL Package spec

PURPOSE: To purge or Archive the date based on given days in loopkup

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     07/24/2015    Pattabhi Avula  TMS#20150401-00052 Initial version.
********************************************************************************/
PROCEDURE data_purge(p_errbuf           OUT      VARCHAR2,
                     p_retcode          OUT      VARCHAR2);
						 
END XXCUS_INTF_TBL_ARCHIVE_PKG;
/