CREATE OR REPLACE
PACKAGE BODY APPS.XXCUS_INTF_TBL_ARCHIVE_PKG
  /********************************************************************************
  FILE NAME: APPS.XXCUS_INTF_TBL_ARCHIVE_PKG.pkb
  
  PROGRAM TYPE: PL/SQL Package Body
  
  PURPOSE: To Archive and purging the data based on given days in loopkup
  XXCUS_INTF_TBL_ARCHIVE_DAYS
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     07/24/2015    Pattabhi Avula  TMS#20150401-00052 Initial version.
  ********************************************************************************/
  
IS
  l_package_name VARCHAR2(100) := 'XXCUS_INTF_TBL_ARCHIVE_PKG.data_purge';
  l_distro_list  VARCHAR2(100) DEFAULT 'WC-ITDevelopment-U1@HDSupply.com';
  
  /********************************************************************************
  FILE NAME: APPS.XXCUS_INTF_TBL_ARCHIVE_PKG.pkb
  
  PROGRAM TYPE: data_purge Procedure
  
  PURPOSE: To Archive and purging the data based on given days in loopkup
  XXCUS_INTF_TBL_ARCHIVE_DAYS
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     07/24/2015    Pattabhi Avula  TMS#20150401-00052 Initial version.
  ********************************************************************************/
  
  -- --------------------------------------------------------------------------------
  -- Procedure to purge the audit data
  -- --------------------------------------------------------------------------------
PROCEDURE data_purge(
    p_errbuf OUT VARCHAR2,
    p_retcode OUT VARCHAR2)
IS
  l_error_msg   VARCHAR2(100);
  l_program_err EXCEPTION;
  l_calling     VARCHAR2(240);
  l_statement   VARCHAR2(300);
  l_insert_stmt VARCHAR2(500);
  l_arch_owner  VARCHAR2(10);
  l_arc_count   NUMBER(5);
  l_purg_owner  VARCHAR2(10);
  l_purg_count  NUMBER(5);
  l_dpurg_owner VARCHAR2(10);
  l_dpurg_count NUMBER(5);
  
  CURSOR C_purg
  IS
    SELECT lookup_code,
           tag,
           attribute1,
           attribute2,
		   attribute3
    FROM   fnd_lookup_values
    WHERE  lookup_type='XXCUS_INTF_TBL_ARCHIVE_DAYS'
    AND    language     ='US'
    AND    enabled_flag ='Y'
	AND TRUNC(SYSDATE) BETWEEN start_date_active AND NVL(end_date_active, TRUNC(SYSDATE) + 1);

BEGIN
  l_calling:='Calling Purg Cursor from data_purge procedure';
  
    FOR p_rec IN C_purg
    LOOP
	  BEGIN
        IF p_rec.attribute1 IS NOT NULL THEN
		
  -- --------------------------------------------------------------------------------
  -- Checking  Archive table exists or not in Database
  -- --------------------------------------------------------------------------------
        BEGIN
          SELECT owner,
                 COUNT(*)
          INTO   l_arch_owner,
                 l_arc_count
          FROM   dba_tables
          WHERE  table_name=p_rec.attribute1
          GROUP BY owner;
        EXCEPTION
        WHEN OTHERS THEN
          l_calling:='Error while fetching the Archive table count for '||' '||p_rec.attribute1;
          RAISE l_program_err;
        END;
		
        fnd_file.put_line(fnd_file.log,'Archive table count is : '||l_arc_count);
		
  -- --------------------------------------------------------------------------------
  -- Checking  purge table exists or not in Database
  -- --------------------------------------------------------------------------------       
        IF l_arc_count=1 THEN
		
          BEGIN
            SELECT owner,
                   COUNT(*)
            INTO   l_purg_owner,
                   l_purg_count
            FROM   dba_tables
            WHERE  table_name=p_rec.lookup_code
            GROUP BY owner;
          EXCEPTION
          WHEN OTHERS THEN
            l_calling:='Error while fetching the purge table count for '||' '||p_rec.lookup_code;
            RAISE l_program_err;
          END;
		  
          fnd_file.put_line(fnd_file.log,'Purge table count is : '||l_purg_count);
		  
        ELSE
          l_calling:='Error or Duplicate Archive tables found for '||' '||p_rec.attribute1;
          RAISE l_program_err;
        END IF;

  -- --------------------------------------------------------------------------------
  -- Checking purge table is exists or not, if exists then inserting the data into
  -- Archive table in case attribute3 value is not null case
  -- --------------------------------------------------------------------------------        
        IF l_purg_count =1 THEN
		
		  IF p_rec.attribute3 IS NOT NULL
		    THEN
              l_insert_stmt:='INSERT INTO '||l_arch_owner||'.'||p_rec.attribute1||' SELECT * FROM '||l_purg_owner||'.'||p_rec.lookup_code||' WHERE TRUNC(creation_date)< TRUNC(SYSDATE)-'||p_rec.attribute2|| 'AND '||p_rec.attribute3;
		      
             --  fnd_file.put_line(fnd_file.log,'After executed the insert statement ');
		      
              EXECUTE IMMEDIATE l_insert_stmt;		  
		      l_calling:='Error while executing the EXECUTE IMMEDIATE for Insert statement';
              COMMIT;
		      
           fnd_file.put_line(fnd_file.log,'Archive the data successfully for attrubute3 is not null condition  : '||' '||p_rec.attribute1);
		  
  -- --------------------------------------------------------------------------------
  -- purging data from the base table After Archive process finished for Attribute3
  -- value is not null 
  -- --------------------------------------------------------------------------------		  
              l_statement:= 'DELETE FROM '||l_purg_owner||'.'||p_rec.lookup_code||' WHERE TRUNC(creation_date)< TRUNC(SYSDATE) - '||p_rec.tag|| 'AND '||p_rec.attribute3;
		      
              EXECUTE IMMEDIATE l_statement;
		      l_calling:='Error while executing the EXECUTE IMMEDIATE for delete statement';
              COMMIT;
		      
              fnd_file.put_line(fnd_file.log,'Data Purged successfully for attrubute3 is not null condition :'||' '||p_rec.lookup_code);
			  
		  ELSE
  -- --------------------------------------------------------------------------------
  -- Inserting the data into Archive table in case attribute3 value IS NULL 
  -- --------------------------------------------------------------------------------  		  
		    l_insert_stmt:='INSERT INTO '||l_arch_owner||'.'||p_rec.attribute1||' SELECT * FROM '||l_purg_owner||'.'||p_rec.lookup_code||' WHERE TRUNC(creation_date)< TRUNC(SYSDATE)-'||p_rec.attribute2;
		   
           -- fnd_file.put_line(fnd_file.log,'insert statement: '||' '||l_insert_stmt);
		  
            EXECUTE IMMEDIATE l_insert_stmt;
		    l_calling:='Error while executing the EXECUTE IMMEDIATE for Insert statement';
            COMMIT;
		    
            fnd_file.put_line(fnd_file.log,'Archive the data successfully for attrubute3 is null condition  : '||' '||p_rec.attribute1);
		  
  -- --------------------------------------------------------------------------------
  -- Purging the data from base table in case attribute3 value IS NULL 
  -- --------------------------------------------------------------------------------		  
		    l_statement:= 'DELETE FROM '||l_purg_owner||'.'||p_rec.lookup_code||' WHERE TRUNC(creation_date)< TRUNC(SYSDATE)-'||p_rec.tag;
		    
            EXECUTE IMMEDIATE l_statement;
		    l_calling:='Error while executing the EXECUTE IMMEDIATE for delete statement';
            COMMIT;
			
			fnd_file.put_line(fnd_file.log,'Data Purged successfully, After Archive the tbale :'||' '||p_rec.lookup_code);
			
		  END IF;
		  
        ELSE
          l_calling:='Error or Duplicate Purge tables found for '||' '||p_rec.lookup_code;
          RAISE l_program_err;
        END IF;
		
      ELSE
	  
  -- --------------------------------------------------------------------------------
  -- Checking the Purge table exists or not in Database
  -- --------------------------------------------------------------------------------
        BEGIN
          SELECT owner,
                 COUNT(*)
          INTO   l_dpurg_owner,
                 l_dpurg_count
          FROM   dba_tables
          WHERE  table_name=p_rec.lookup_code
          GROUP BY OWNER;
        EXCEPTION
        WHEN OTHERS THEN
          l_calling:='Error while fetching the direct purge table count'||' '||p_rec.lookup_code;
          RAISE l_program_err;
        END;
		
        fnd_file.put_line(fnd_file.log,'Direct purge table count : '||l_dpurg_count);
		
  -- --------------------------------------------------------------------------------
  -- If Archive table is null then purging data from the base table directly Based on
  -- attribute3 condition
  -- --------------------------------------------------------------------------------	
        IF l_dpurg_count=1 THEN
		  
		  IF p_rec.attribute2 IS NULL AND p_rec.attribute3 IS NOT NULL
		    THEN
			
             l_statement  := 'DELETE FROM '||l_dpurg_owner||'.'||p_rec.lookup_code||' WHERE TRUNC(creation_date)< TRUNC(SYSDATE)-'||p_rec.tag|| 'AND '||p_rec.attribute3;
		     
             EXECUTE IMMEDIATE l_statement;
		      l_calling:='Error while executing the EXECUTE IMMEDIATE for direct delete statement';
             COMMIT;
		     
             fnd_file.put_line(fnd_file.log,'Data purged directly from the table incase attribute3 condition is not null for direct purge tables :'||' '||p_rec.lookup_code);
			 
		  ELSE
  -- --------------------------------------------------------------------------------
  -- Purging the data from base table directly
  -- --------------------------------------------------------------------------------		  
		    l_statement  := 'DELETE FROM '||l_dpurg_owner||'.'||p_rec.lookup_code||' WHERE TRUNC(creation_date)< TRUNC(SYSDATE)-'||p_rec.tag;
			
            EXECUTE IMMEDIATE l_statement;			
		    l_calling:='Error while executing the EXECUTE IMMEDIATE for direct delete statement';
            COMMIT;
			
            fnd_file.put_line(fnd_file.log,'Data purged directly from the table incase attribute2 and attribute3 condition is null for direct purge tables :'||' '||p_rec.lookup_code);
			
		  END IF;
		  
        ELSE
            l_calling:='Error or Duplicate direct Purge tables found for '||' '||p_rec.lookup_code;
          RAISE l_program_err;
        END IF;
      END IF;
  EXCEPTION
    WHEN l_program_err THEN
      xxcus_error_pkg.xxcus_error_main_api (p_called_from => l_package_name, 
	                                        p_calling => l_calling, 
	  									    p_request_id => fnd_global.conc_request_id, 
	  									    p_ora_error_msg => SUBSTR(SQLERRM,1,2000), 
	  									    p_error_desc => SUBSTR(l_error_msg,1,240), 
	  									    p_distribution_list => l_distro_list, 
	  									    p_module => 'OM'
	  									    );
    WHEN OTHERS THEN
      xxcus_error_pkg.xxcus_error_main_api (p_called_from => l_package_name, 
	                                        p_calling => l_calling, 
	  									    p_request_id => fnd_global.conc_request_id, 
	  									    p_ora_error_msg => SUBSTR(SQLERRM,1,2000), 
	  									    p_error_desc => SUBSTR(l_error_msg,1,240), 
	  									    p_distribution_list => l_distro_list, 
	  									    p_module => 'OM'
	  									    );
  END;
    END LOOP;
    -- COMMIT;
  
EXCEPTION
  WHEN OTHERS THEN
    xxcus_error_pkg.xxcus_error_main_api (p_called_from => l_package_name, 
	                                      p_calling => l_calling, 
										  p_request_id => fnd_global.conc_request_id, 
										  p_ora_error_msg => SUBSTR(SQLERRM,1,2000), 
										  p_error_desc => SUBSTR(l_error_msg,1,240), 
										  p_distribution_list => l_distro_list, 
										  p_module => 'OM'
										  );
END data_purge;

END XXCUS_INTF_TBL_ARCHIVE_PKG;
/