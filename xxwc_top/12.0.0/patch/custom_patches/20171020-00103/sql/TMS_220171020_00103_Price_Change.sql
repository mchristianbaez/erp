/********************************************************************************************************************************
PURPOSE:   PRICE CHANGE ON PO 2424192 
REVISIONS:
Ver        Date            Author                     Description
---------  ----------      ---------------         -------------------------------------------------------------------------------
1.0        25-Oct-2017     Sundar                   TMS #220171020-00103 
**************************************************************************************************************************************/

BEGIN
 DBMS_OUTPUT.put_line ('Before update');

 UPDATE rcv_transactions_interface
 SET request_id = NULL,
 processing_request_id = NULL,
 processing_status_code = 'PENDING',
 transaction_status_code = 'PENDING',
 processing_mode_code = 'BATCH',
 TRANSACTION_DATE = SYSDATE,
 last_update_date = SYSDATE,
 Last_updated_by = 16991
 WHERE processing_status_code = 'PENDING'
 AND transaction_status_code = 'PENDING'
 AND processing_mode_code = 'ONLINE'
 AND po_header_id=3795789;


 DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);
 COMMIT;
EXCEPTION
 WHEN OTHERS
 THEN
 DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
END;
/