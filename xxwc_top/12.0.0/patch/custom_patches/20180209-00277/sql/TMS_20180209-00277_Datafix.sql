/***********************************************************************************************************************************************
   NAME:       TMS_20180209-00277_Datefix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        02/09/2018  Rakesh Patel     TMS#20180209-00277
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before archive');
   
   INSERT INTO XXWC.XXWC_SIGNATURE_CAPTURE_ARC_TBL
   (SELECT *
      FROM xxwc.xxwc_signature_capture_tbl
     WHERE CREATION_DATE <= SYSDATE - 30);

   DBMS_OUTPUT.put_line ('Records archive -' || SQL%ROWCOUNT);
   
   DBMS_OUTPUT.put_line ('Before delete');

   DELETE FROM xxwc.xxwc_signature_capture_tbl
   WHERE CREATION_DATE <= SYSDATE - 30; 

   DBMS_OUTPUT.put_line ('Records deleted -' || SQL%ROWCOUNT);
   COMMIT;
   
  EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to archive records record ' || SQLERRM);
	  ROLLBACK;
END;
/