/* Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        03-Dec-2015  Kishorebabu V    TMS 20151203-00063 Data Fix for items stuck in Cycle Counts in orgs 069 and 251
                                            Initial Version   */

UPDATE mtl_cycle_count_entries
SET    export_flag       = NULL
WHERE  organization_id   IN (283,335)
AND    entry_status_code IN (1,3)
AND    export_flag       IS NOT NULL;
/
commit;
/