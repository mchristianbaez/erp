  /********************************************************************************
  FILE NAME: data_fix_delete_prior21Nov.sql

  PROGRAM TYPE: script

  PURPOSE: Delete Data prior to Nov 21 2016

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     11/30/2016   Neha Saini      TMS#: 20161128-00028 Delete data prior to NOv212016 from AIS Log File
  ********************************************************************************/
DELETE from  XXWC.XXWC_AIS_SEARCH_LOG_TBL where trunc(creation_date) < '21-NOV-2016';
COMMIT;