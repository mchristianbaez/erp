  /********************************************************************************
  FILE NAME: APPS.XXWC_AIS_SEARCH_ARCH_TBL.sql

  PROGRAM TYPE: Table script

  PURPOSE: To Archive and purging the data based on given days in loopkup
  XXCUS_INTF_TBL_ARCHIVE_DAYS this is Archiving table for XXWC_AIS_SEARCH_ARCH_TBL

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     11/30/2016   Neha SAini      TMS # TMS20161128-00028 Archive table for XXWC_AIS_SEARCH_ARCH_TBL AIS Search form
  ********************************************************************************/
DROP TABLE XXWC.XXWC_AIS_SEARCH_ARCH_TBL CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWC_AIS_SEARCH_ARCH_TBL
(
  SEQUENCE_ID        NUMBER,
  CATEGORY_DESC      VARCHAR2(100 BYTE),
  INVENTORY_ITEM_ID  NUMBER,
  ITEM_DESC          VARCHAR2(240 BYTE),
  ORDER_NUMBER       VARCHAR2(35 BYTE),
  ORGANIZATION_CODE  VARCHAR2(100 BYTE),
  ORGANIZATION_ID    NUMBER,
  QUOTE_NUMBER       NUMBER,
  SEGMENT1           VARCHAR2(100 BYTE),
  CREATED_BY         NUMBER,
  CREATION_DATE      DATE,
  LAST_UPDATE_DATE   DATE,
  QUERY_START_TIME   DATE,
  QUERY_END_TIME     DATE,
  ROW_COUNT          NUMBER,
  WHERE_CLAUSE       VARCHAR2(2000 BYTE),
  ORDER_BY           VARCHAR2(2000 BYTE),
  MESSAGE            VARCHAR2(2000 BYTE),
  ORG_ID             NUMBER
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            MAXSIZE          UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
