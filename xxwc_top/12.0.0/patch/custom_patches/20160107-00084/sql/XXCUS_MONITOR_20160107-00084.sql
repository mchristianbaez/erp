/*
 TMS: 20160107-00084
 ESMS: 312920
 Date: 01/07/2016
 Scope: Setup REBATES concurrent programs for 24*7 monitoring
*/
set serveroutput on size 1000000;
declare
 --
 cursor rbt_jobs is
 select concurrent_program_id, concurrent_program_name, application_id
from   apps.fnd_concurrent_programs_vl
where  1 =1
and user_concurrent_program_name IN
(
 'HDS Rebates: Generate TM GL Journals Extract'
,'HDS Rebates: Generate TM SLA Accruals Extract'
,'HDS Rebates: Submit Claims Master'
,'HDS Refresh Rebates Materialized Views'
,'HDS Create Deduction Document'
,'HDS Create Rebate Invoices'
,'HDS Create Rebate Payments'
,'HDS Create Rebate Reimbursement'
,'HDS Rebates: Extract GL Summary Data -By Period'
,'HDS Rebates: Extract YTD Income -By Period'
,'HDS Rebates: Gather Extended Stats'
,'HDS Rebates: Generate TM Customers Extract'
,'HDS Rebates: Generate TM Inventory Items Extract'
,'HDS Rebates: Generate TM SLA Accruals Extract -By Period'
,'HDS Rebates: Rebuild OZF Stack -Detail'
,'HDS Rebates: Rebuild OZF Stack -Master'
,'HDS Rebates: Rebuild TM GL Indexes'
,'HDS Rebates: Rebuild TM SLA Indexes'
,'HDS Rebates: SLA Period Close Exception'
,'HDS TM Branch Load Process'
,'HDS TM Customer Load Process'
,'HDS TM PAM Refresh'
,'HDS TM Product Load Process'
,'HDS TM Product Misc Load'
,'HDS TM Receipt Load Process By BU Name'
,'HDS TM Receipt Misc Load'
,'HDS Update Collector Program'
)
order by 2 asc;
 --
 l_program_1 varchar2(150) :=Null; 
 l_conc_program_id number :=0;
 l_app_id number :=0;
 l_sc_userid number :=15985; --Supply chain generic user XXWC_INT_SUPPLYCHAIN
 l_om_userid number :=15986; --Order managenent generic user XXWC_INT_SALESFULFILLMENT
 l_fin_userid number :=1290; --WC Finance generic user XXWC_INT_FINANCE
 --
     procedure print_log(p_message in varchar2) is
     begin
      if apps.fnd_global.conc_request_id >0 then
       apps.fnd_file.put_line(fnd_file.log, p_message);
      else
       dbms_output.put_line(p_message);
      end if;
     end;
 --
begin
     --
     print_log('');     
     print_log(' Begin setup of jobs for 24 * 7 monitoring...');
     print_log('');
     --       
     for rec in rbt_jobs loop 
              --
             begin 
              select b.concurrent_program_id, b.application_id
              into    l_conc_program_id, l_app_id
              from   xxcus.xxcus_monitor_cp_b  b
              where  1 =1
                   and  b.concurrent_program_id  =rec.concurrent_program_id
                   and  b.application_id =rec.application_id
                   and  b.owned_by ='REBATES';
                        --
                        print_log('@ Job: '||rec.concurrent_program_name||' is already setup for 24*7 monitoring.');
                        --                     
             exception
              when no_data_found then
                --
                 begin 
                  savepoint square2;
                    INSERT INTO XXCUS.XXCUS_MONITOR_CP_B 
                     (
                          concurrent_program_id,
                          email_when_warning,
                          email_when_error,
                          user_id,
                          created_by,
                          creation_date,
                          last_updated_by,
                          last_update_date,
                          warning_email_address,
                          error_email_address,
                          owned_by,
                          application_id
                       )
                         VALUES 
                       (
                         rec.concurrent_program_id,
                         'N',  --email_when_warning
                         'Y',  --email_when_error
                         null, --for all EBS users
                         15837,
                         SYSDATE,
                         15837,
                         SYSDATE,
                         'HDSRebateManagementSystemNotifications@hdsupply.com',
                         'HDSRebateManagementSystemNotifications@hdsupply.com',
                         'REBATES',
                         rec.application_id
                      );       
                        --
                        commit;
                        --
                        print_log('@ Job: '||rec.concurrent_program_name||' is successfully setup for 24*7 monitoring.');
                        --                                                 
                 exception
                  when others then
                   print_log('@ insert of job: '||rec.concurrent_program_name||', when-others, error ='||sqlerrm);                  
                   rollback to square2;
                 end;                        
                --
              when too_many_rows then
               print_log('@ program_1 = '||rec.concurrent_program_name);
               print_log('@ l_program_1 when-too many rows, error ='||sqlerrm);          
              when others then
               print_log('@ program_1 = '||rec.concurrent_program_name);
               print_log('@ l_program_1 when-others-1, error ='||sqlerrm);       
             end;
            --        
      --
     end loop;  
     --
     print_log('');     
     print_log(' End setup of jobs for 24 * 7 monitoring...');
     print_log('');
     --    
exception
 when others then
  rollback to square1;
  print_log('Outer block, message ='||sqlerrm);
end;
/