/*************************************************************************
  $Header TMS_20170711-00067_BEST_PRICE_WINS.sql $
  Module Name: 20170711-00067  converted all CSP agreements to Exclusive 
               CSP agreements 

  PURPOSE: Data fix for converted all CSP agreements to Exclusive CSP agreements  

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        25-JUL-2017  Pattabhi Avula        TMS# 20170711-00067 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170711-00067   , Before Update');

UPDATE xxwc.xxwc_om_contract_pricing_hdr
   SET incompatability_group='Exclusive'
 WHERE agreement_id in (243461,
       243415,
       255415,
       243413,
       242409,
       250442,
       242407,
       245407,
       248410);


   DBMS_OUTPUT.put_line (
         'TMS: 20170711-00067 CSP Lines Updated and number of records (Expected:9): '
      || SQL%ROWCOUNT);
	  
   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170711-00067   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170711-00067, Errors : ' || SQLERRM);
END;
/