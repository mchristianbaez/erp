/*************************************************************************
  $Header XXWC_OM_CUST_EMAIL_PDF_DIR.sql $
  Module Name: XXWC_OM_CUST_EMAIL_PDF_DIR
  PURPOSE: To access OM Customer email pdf files.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------------
  1.0        05-Sep-2017  P.vamshidhar          TMS#20170207-00206 - Research the issue where users are able to enter
                                                invalid email address and Fax numbers while app
*******************************************************************************************************************************************/ 
DECLARE
   lvc_environment   VARCHAR2 (100);
   lvc_sql           VARCHAR2 (4000);
BEGIN
   SELECT '/xx_iface/' || LOWER (NAME) || '/outbound/ont/email_to_cust'
     INTO lvc_environment
     FROM v$database;

   lvc_sql :=
         'CREATE OR REPLACE DIRECTORY XXWC_OM_CUST_EMAIL_PDF_DIR AS '
      || ''''
      || lvc_environment
      || '''';

   EXECUTE IMMEDIATE lvc_sql;
END;
/