CREATE OR REPLACE PACKAGE APPS.XXWC_OM_EMAIL_CUSTOMER_PKG
IS
   -- ==========================================================================================================
   -- Package: XXWC_OM_EMAIL_CUSTOMER_PKG.pks
   -- ==========================================================================================================
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0    11-Sep-2017    P.Vamshidhar    TMS#20170207-00206 - Research the issue where users are able to enter
   --                                       invalid email address and Fax numbers while app
   --                                       Initial version
   -- ==========================================================================================================

   PROCEDURE MAIN (x_errbuf              OUT VARCHAR2,
                   x_retcode             OUT NUMBER,
                   p_request_id       IN     NUMBER,
                   p_transaction_id   IN     VARCHAR2,
                   p_receiver_email   IN     VARCHAR2,
                   p_customer_name    IN     VARCHAR2,
                   p_contact          IN     VARCHAR2);

   PROCEDURE SENDING_EMAIL (p_email_subject        IN VARCHAR2,
                            p_email_body_file      IN VARCHAR2,
                            p_email_sender         IN VARCHAR2,
                            p_email_receiver       IN VARCHAR2,
                            p_email_attachment     IN VARCHAR2,
                            p_cp_output_pdf_file   IN VARCHAR2,
                            p_attachment_file      IN VARCHAR2);
END XXWC_OM_EMAIL_CUSTOMER_PKG;
/