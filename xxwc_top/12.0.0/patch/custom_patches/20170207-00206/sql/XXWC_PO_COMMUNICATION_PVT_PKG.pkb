CREATE OR REPLACE PACKAGE BODY APPS.XXWC_PO_COMMUNICATION_PVT_PKG
AS
   -- ==========================================================================================================
   -- Package: XXWC_PO_COMMUNICATION_PVT_PKG.pkb
   -- Procedure: Main
   -- ==========================================================================================================
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     13-JUL-2017   P.Vamshidhar    TMS# 20170323-00270 - Auto-fill E-Mail Address on PO Approval form
   --                                       Initial version
   -- 1.1     24-Aug-2017   P.Vamshidhar    TMS#20170824-00104 - FW: Email Po's to suppliers

   -- 1.2     13-Oct-2017   P.Vamshidhar    TMS#20170207-00206 - Research the issue where users are able to enter
   --                                       invalid email address and Fax numbers while app	     
   -- ==========================================================================================================

   g_po_wf_debug    VARCHAR2 (1)
      := NVL (FND_PROFILE.VALUE ('XXWC_PO_WF_EMAIL_DEBUG'), 'N');

   g_err_callfrom   VARCHAR2 (100) := 'XXWC_PO_COMMUNICATION_PVT_PKG';
   g_distro_list    VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   g_wf_item_key    VARCHAR2 (100);
   g_user_id        VARCHAR2 (100) := FND_GLOBAL.USER_ID;


   -- ==========================================================================================================
   -- Procedure: WC_PO_NEW_COMMUNICATION
   -- Purpose: Main procedure
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     13-JUL-2017   P.Vamshidhar    TMS# 20170323-00270 - Auto-fill E-Mail Address on PO Approval form
   --                                       Initial version
   -- 1.1     24-Aug-2017   P.Vamshidhar    TMS#20170824-00104 - FW: Email Po's to suppliers   
   -- ==========================================================================================================

   PROCEDURE WC_PO_NEW_COMMUNICATION (itemtype    IN            VARCHAR2,
                                      itemkey     IN            VARCHAR2,
                                      actid       IN            NUMBER,
                                      funcmode    IN            VARCHAR2,
                                      resultout      OUT NOCOPY VARCHAR2)
   IS
      lvc_email_sender       VARCHAR2 (32767);
      lvc_user_email         VARCHAR2 (32767):='noreply@hdsupply.com'; -- changed in Rev 1.1
      ln_user                FND_USER.USER_ID%TYPE := FND_GLOBAL.USER_ID;
      lvc_email_receiver     VARCHAR2 (32767);
      lvc_email_subject      VARCHAR2 (32767);
      ln_org_id              NUMBER := FND_PROFILE.VALUE ('ORG_ID');
      l_document_type        po_headers.type_lookup_code%TYPE;
      l_document_subtype     VARCHAR2 (1000);
      l_document_num_rel     VARCHAR2 (1000);
      l_revision_num         VARCHAR2 (1000);
      lvc_vendor_name        AP_SUPPLIERS.VENDOR_NAME%TYPE;
      ln_agent_id            PO_AGENTS.AGENT_ID%TYPE;
      lvc_buyer_phone        VARCHAR2 (1000);
      lb_pdf_exists          BOOLEAN;
      lvc_email_body_file    VARCHAR2 (1000);
      lvc_email_dir          VARCHAR2 (1000) := 'XXWC_PO_SUPP_EMAIL_PDF_DIR';
      lvc_utl_file           UTL_FILE.file_type;
      lvc_email_attachment   VARCHAR2 (1000);
      lvc_return_status      VARCHAR2 (30);
      lvc_procedure          VARCHAR2 (40) := 'WC_PO_NEW_COMMUNICATION';
      lvc_buyer_name         PER_ALL_PEOPLE_F.FULL_NAME%TYPE;
      lvc_wf_proc_enable     VARCHAR2 (30)
         := NVL (FND_PROFILE.VALUE ('XXWC_PO_CUST_EMAIL_WF_PROC_ENABLE'),
                 'Y');
      l_sec                  VARCHAR2 (4000);
   BEGIN
      g_wf_item_key := itemkey;

      IF g_po_wf_debug = 'Y'
      THEN
         l_sec := 'WC_PO_NEW_COMMUNICATION Process Started';
         write_log (l_sec);
      END IF;


      IF (funcmode <> wf_engine.eng_run)
      THEN
         resultout := wf_engine.eng_null;
         RETURN;
      END IF;

      IF ln_org_id = '162' AND lvc_wf_proc_enable = 'Y'
      THEN
         l_sec := 'White CAP PO';

         l_document_type :=
            PO_WF_UTIL_PKG.GetItemAttrText (itemtype   => itemtype,
                                            itemkey    => itemkey,
                                            aname      => 'DOCUMENT_TYPE');

         l_sec := 'l_document_type ' || l_document_type;


         l_document_subtype :=
            PO_WF_UTIL_PKG.GetItemAttrText (itemtype   => itemtype,
                                            itemkey    => itemkey,
                                            aname      => 'DOCUMENT_SUBTYPE');


         l_sec := 'l_document_subtype ' || l_document_subtype;

         l_document_num_rel :=
            PO_WF_UTIL_PKG.GetItemAttrText (itemtype   => itemtype,
                                            itemkey    => itemkey,
                                            aname      => 'DOCUMENT_NUM_REL');

         l_sec := 'l_document_num_rel ' || l_document_num_rel;


         l_revision_num :=
            PO_WF_UTIL_PKG.GetItemAttrNumber (itemtype   => itemtype,
                                              itemkey    => itemkey,
                                              aname      => 'REVISION_NUMBER');

         l_sec := 'l_revision_num ' || l_revision_num;

         lvc_email_receiver :=
            wf_engine.GetItemAttrText (itemtype   => itemtype,
                                       itemkey    => itemkey,
                                       aname      => 'EMAIL_ADDRESS');

         l_sec := 'lvc_email_receiver ' || lvc_email_receiver;


         IF g_po_wf_debug = 'Y'
         THEN
            l_sec :=
                  'Doc Type '
               || l_document_type
               || ' PO Num '
               || l_document_num_rel
               || ' Rev. '
               || l_revision_num
               || ' Supp email '
               || lvc_email_receiver;
            write_log (l_sec);
         END IF;


         IF l_document_num_rel IS NOT NULL
         THEN
            l_sec := ' Deriving email_id for buyer ' || ln_agent_id;

            IF NVL (ln_agent_id, 0) > 0
            THEN
               -- Deriving user email address.
			  -- commented below code in 1.1   
/*
               l_sec := ' Deriving user email address ';

               BEGIN
                  SELECT pap.email_address
                    INTO lvc_user_email
                    FROM apps.fnd_user fu, per_all_people_f pap
                   WHERE     fu.employee_id = pap.person_id
                         AND fu.user_id = ln_user
                         AND TRUNC (SYSDATE) BETWEEN NVL (
                                                        pap.EFFECTIVE_START_DATE,
                                                        TRUNC (SYSDATE))
                                                 AND NVL (
                                                        pap.EFFECTIVE_END_DATE,
                                                        TRUNC (SYSDATE) + 1)
                         AND TRUNC (SYSDATE) BETWEEN NVL (fu.START_DATE,
                                                          TRUNC (SYSDATE))
                                                 AND NVL (
                                                        fu.END_DATE,
                                                        TRUNC (SYSDATE) + 1)
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     lvc_user_email := NULL;
                  WHEN OTHERS
                  THEN
                     lvc_user_email := NULL;
                     xxcus_error_pkg.xxcus_error_main_api (
                        p_called_from         =>    g_err_callfrom
                                                 || '.'
                                                 || lvc_procedure,
                        p_calling             =>    l_sec
                                                 || ' wf_item_key '
                                                 || g_wf_item_key,
                        p_ora_error_msg       => SUBSTR (SQLERRM, 1, 1000),
                        p_error_desc          =>    'Error Occured in '
                                                 || l_document_num_rel,
                        p_distribution_list   => g_distro_list,
                        p_module              => 'XXWC');
               END;
*/
               l_sec := ' Deriving Buyer phone number and email address ';

               -- Deriving Buyer Phone Numberand email address
               BEGIN

                  SELECT --NVL (lvc_user_email, pap.email_address),  -- Commented in Rev 1.1
				         NVL (pap.email_address,lvc_user_email),  -- Added in Rev 1.1
                         pap.first_name || ' ' || pap.last_name
                    INTO lvc_email_sender, lvc_buyer_name
                    FROM per_all_people_f pap
                   WHERE     pap.person_id = ln_agent_id
                         AND TRUNC (SYSDATE) BETWEEN pap.effective_start_date
                                                 AND pap.effective_end_date;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     --lvc_buyer_phone := NULL;
                     lvc_email_sender := lvc_user_email;
                  WHEN OTHERS
                  THEN
                     -- lvc_buyer_phone := NULL;
                     lvc_email_sender := lvc_user_email;

                     xxcus_error_pkg.xxcus_error_main_api (
                        p_called_from         =>    g_err_callfrom
                                                 || '.'
                                                 || lvc_procedure,
                        p_calling             =>    l_sec
                                                 || ' wf_item_key '
                                                 || g_wf_item_key,
                        p_ora_error_msg       => SUBSTR (SQLERRM, 1, 1000),
                        p_error_desc          =>    'Error Occured in '
                                                 || l_document_num_rel,
                        p_distribution_list   => g_distro_list,
                        p_module              => 'XXWC');
               END;

               BEGIN
                  SELECT NVL (A.buyer_contact_phone, cp_ship_loc_phone)
                    INTO lvc_buyer_phone
                    FROM PO_HEADERS_XML A, PO_HEADERS_ALL B
                   WHERE     A.PO_HEADER_ID = B.PO_HEADER_ID
                         AND B.SEGMENT1 = l_document_num_rel;

                  IF lvc_buyer_phone IS NULL
                  THEN
                     BEGIN
                        SELECT TELEPHONE_NUMBER_1
                          INTO lvc_buyer_phone
                          FROM PER_ASSIGNMENTS_V7 pav7
                         WHERE     NVL (pav7.inactive_date, SYSDATE + 1) >=
                                      SYSDATE
                               AND person_id = ln_agent_id
                               AND ROWNUM = 1;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           lvc_buyer_phone := NULL;
                     END;
                  END IF;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL;
               END;



               IF lvc_buyer_phone IS NOT NULL
               THEN
                  SELECT CASE
                            WHEN LENGTH (lvc_buyer_phone) = 10
                            THEN
                                  '('
                               || SUBSTR (lvc_buyer_phone, 1, 3)
                               || ') '
                               || SUBSTR (lvc_buyer_phone, 4, 3)
                               || '-'
                               || SUBSTR (lvc_buyer_phone, 7)
                            ELSE
                               lvc_buyer_phone
                         END
                    INTO lvc_buyer_phone
                    FROM DUAL;
               END IF;



               IF lvc_email_sender IS NOT NULL
               THEN
                  l_sec := ' Deriving email body ';

                  FND_MESSAGE.clear;
                  fnd_message.set_name ('XXWC', 'XXWC_PO_PDF_EMAIL_TEXT');
                  fnd_message.set_token ('PO_DOCUMENT',
                                         TO_CHAR (l_document_num_rel));
                  fnd_message.set_token ('SUPPLIER_NAME', lvc_vendor_name);
                  fnd_message.set_token ('BUYER_NAME', lvc_buyer_name);
                  fnd_message.set_token ('BUYER_PHONE',
                                         TO_CHAR (lvc_buyer_phone));

                  lvc_email_body_file :=
                        'PO_'
                     || ln_org_id
                     || '_'
                     || l_document_num_rel
                     || '_'
                     || l_revision_num
                     || '_US.txt';

                  l_sec := ' Deriving email attachment ';

                  lvc_email_attachment :=
                        'PO_'
                     || ln_org_id
                     || '_'
                     || l_document_num_rel
                     || '_'
                     || l_revision_num
                     || '_US.pdf';

                  l_sec := ' generating email subject ';

                  lvc_utl_file :=
                     UTL_FILE.fopen (lvc_email_dir,
                                     lvc_email_body_file,
                                     'W',
                                     32767);

                  UTL_FILE.put_line (lvc_utl_file, FND_MESSAGE.GET);

                  UTL_FILE.fclose (lvc_utl_file);

                  fnd_message.set_name ('XXWC', 'XXWC_PO_PDF_EMAIL_SUBJECT');
                  fnd_message.set_token ('PO_NUMBER',
                                         TO_CHAR (l_document_num_rel));

                  lvc_email_subject := FND_MESSAGE.GET;

                  l_sec :=
                        ' generating email attachment PO '
                     || l_document_num_rel
                     || ' Rev '
                     || l_revision_num;

                  lb_pdf_exists :=
                     PDF_DOWNLOAD (p_po_number     => l_document_num_rel,
                                   p_po_revision   => l_revision_num);


                  IF (lb_pdf_exists)
                  THEN
                     l_sec := ' generating email attachment generated';

                     EMAIL_REQUEST_SUBMIT (
                        p_po_number          => l_document_num_rel,
                        p_revision_num       => l_revision_num,
                        p_email_subject      => lvc_email_subject,
                        p_email_body_file    => lvc_email_body_file,
                        p_email_sender       => lvc_email_sender,
                        p_email_receiver     => lvc_email_receiver,
                        p_email_attachment   => lvc_email_attachment,
                        x_return_status      => lvc_return_status);

                     l_sec :=
                        'Supplier email sent status ' || lvc_return_status;

                     IF (g_po_wf_debug = 'Y')
                     THEN
                        write_log ('lvc_return_status ' || lvc_return_status);
                     END IF;

                     IF lvc_return_status = 'SUCCESS'
                     THEN
                        IF PO_COMMUNICATION_PROFILE = 'T'
                        THEN
                           IF        l_document_type IN ('PO', 'PA')
                                 AND l_document_subtype IN ('STANDARD',
                                                            'BLANKET',
                                                            'CONTRACT')
                              OR (    l_document_type = 'RELEASE'
                                  AND l_document_subtype = 'BLANKET')
                           THEN
                              resultout :=
                                 wf_engine.eng_completed || ':' || 'Y';
                           ELSE
                              resultout :=
                                 wf_engine.eng_completed || ':' || 'N';
                           END IF;
                        ELSE
                           resultout := wf_engine.eng_completed || ':' || 'N';
                        END IF;
                     ELSE
                        resultout := wf_engine.eng_completed || ':' || 'N';
                     END IF;
                  ELSE
                     resultout := wf_engine.eng_completed || ':' || 'N';
                  END IF;
               ELSE
                  resultout := wf_engine.eng_completed || ':' || 'N';
               END IF;
            ELSE
               resultout := wf_engine.eng_completed || ':' || 'N';
            END IF;
         ELSE
            resultout := wf_engine.eng_completed || ':' || 'N';
         END IF;
      ELSE
         resultout := wf_engine.eng_completed || ':' || 'N';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec || ' wf_item_key ' || g_wf_item_key,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 1000),
            p_error_desc          => 'Error Occured in ' || l_document_num_rel,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');


         wf_core.context ('XXWC_PO_COMMUNICATION_PVT_PKG',
                          'WC_PO_NEW_COMMUNICATION',
                          'Exception occured ');
         RAISE;
   END;


   -- ==========================================================================================================
   -- Procedure: po_communication_profile
   -- Purpose: Deriving communication profile
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     13-JUL-2017   P.Vamshidhar    TMS# 20170323-00270 - Auto-fill E-Mail Address on PO Approval form
   --                                       Initial version
   -- ==========================================================================================================

   FUNCTION po_communication_profile
      RETURN VARCHAR2
   IS
      l_communication   VARCHAR2 (1);
      l_format          po_system_parameters_all.po_output_format%TYPE;
   BEGIN
      SELECT po_output_format INTO l_format FROM po_system_parameters;

      IF g_po_wf_debug = 'Y'
      THEN
         write_log ('l_format ' || l_format);
      END IF;


      IF (l_format = 'PDF')
      THEN
         RETURN FND_API.G_TRUE;
      ELSE
         RETURN FND_API.G_FALSE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         =>    g_err_callfrom
                                     || '.'
                                     || 'po_communication_profile',
            p_calling             => NULL,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 1000),
            p_error_desc          =>    'Error occured'
                                     || ' wf_item_key '
                                     || g_wf_item_key,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
   END po_communication_profile;

   -- ==========================================================================================================
   -- Procedure: PDF_DOWNLOAD
   -- Purpose: Downloading PDF file into UNIX direcotry
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     13-JUL-2017   P.Vamshidhar    TMS# 20170323-00270 - Auto-fill E-Mail Address on PO Approval form
   --                                       Initial version
   -- ==========================================================================================================


   FUNCTION PDF_DOWNLOAD (p_po_number IN VARCHAR2, p_po_revision IN VARCHAR2)
      RETURN BOOLEAN
   IS
      l_file          UTL_FILE.FILE_TYPE;
      l_buffer        RAW (32767);
      l_amount        BINARY_INTEGER := 32767;
      l_pos           INTEGER := 1;
      l_blob          BLOB;
      l_blob_len      INTEGER;
      ln_org_id       NUMBER := FND_PROFILE.VALUE ('ORG_ID');
      l_file_name     VARCHAR2 (100);
      lvc_procedure   VARCHAR2 (100) := 'PDF_DOWNLOAD';
      l_sec           VARCHAR2 (2000);
   BEGIN
      IF g_po_wf_debug = 'Y'
      THEN
         write_log ('PDF Download Starts');
      END IF;

      l_sec := 'Deriving file name';

      l_file_name :=
            'PO_'
         || ln_org_id
         || '_'
         || P_PO_NUMBER
         || '_'
         || P_PO_REVISION
         || '_US.pdf';

      l_sec := 'Deriving File Data';

      -- Get LOB locator
      BEGIN
         SELECT file_data
           INTO l_blob
           FROM (  SELECT file_data
                     FROM fnd_lobs
                    WHERE file_name LIKE l_file_name
                 ORDER BY upload_date DESC)
          WHERE ROWNUM = 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            RETURN FALSE;
      END;

      l_blob_len := DBMS_LOB.getlength (l_blob);

      -- Open the destination file.

      l_sec := 'Opening Destination file';

      BEGIN
         l_file :=
            UTL_FILE.fopen ('XXWC_PO_SUPP_EMAIL_PDF_DIR',
                            l_file_name,
                            'w',
                            32767);

         -- Read chunks of the BLOB and write them to the file
         -- until complete.
         WHILE l_pos < l_blob_len
         LOOP
            DBMS_LOB.read (l_blob,
                           l_amount,
                           l_pos,
                           l_buffer);
            UTL_FILE.put_raw (l_file, l_buffer, TRUE);
            l_pos := l_pos + l_amount;
         END LOOP;

         l_sec := 'File writing completed';
         -- Close the file.
         UTL_FILE.fclose (l_file);

         IF g_po_wf_debug = 'Y'
         THEN
            write_log ('PDF Download completd.');
         END IF;

         RETURN TRUE;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_sec := 'Error occured ' || SUBSTR (SQLERRM, 1, 250);

            -- Close the file if something goes wrong.
            IF UTL_FILE.is_open (l_file)
            THEN
               UTL_FILE.fclose (l_file);
            END IF;

            RAISE;
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 1000),
            p_error_desc          => NULL,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
         RETURN FALSE;
   END;


   -- ==========================================================================================================
   -- Procedure: EMAIL_REQUEST_SUBMIT
   -- Purpose: Sending email to supplier
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     13-JUL-2017   P.Vamshidhar    TMS# 20170323-00270 - Auto-fill E-Mail Address on PO Approval form
   --                                       Initial version
   -- 1.1     24-Aug-2017   P.Vamshidhar    TMS#20170824-00104 - FW: Email Po's to suppliers   
   -- ==========================================================================================================

   PROCEDURE EMAIL_REQUEST_SUBMIT (p_po_number          IN     NUMBER,
                                   p_revision_num       IN     NUMBER,
                                   p_email_subject      IN     VARCHAR2,
                                   p_email_body_file    IN     VARCHAR2,
                                   p_email_sender       IN     VARCHAR2,
                                   p_email_receiver     IN     VARCHAR2,
                                   p_email_attachment   IN     VARCHAR2,
                                   x_return_status         OUT VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
      lvc_dir_name         VARCHAR2 (100);
      ln_request_id        NUMBER;
      ln_interval          NUMBER := 5;
      ln_max_time          NUMBER := 10000;
      lvc_req_phase        VARCHAR2 (100);
      lvc_req_status       VARCHAR2 (100);
      lvc_del_phase        VARCHAR2 (50);
      lvc_del_status       VARCHAR2 (50);
      lvc_message          VARCHAR2 (4000);
      lvc_error_mess       VARCHAR2 (4000);
      l_sec                VARCHAR2 (32767);
      ln_user_id           NUMBER := FND_GLOBAL.USER_ID;
      ln_resp_id           NUMBER := FND_GLOBAL.RESP_ID;
      ln_resp_appl_id      NUMBER := FND_GLOBAL.RESP_APPL_ID;
      lvc_program_exp      EXCEPTION;
      lvc_procedure        VARCHAR2 (100) := 'EMAIL_REQUEST_SUBMIT';
      lvc_env_name         VARCHAR2 (100);
      lvc_email_subject    VARCHAR2 (4000);
      lvc_email_receiver   VARCHAR2 (4000);
      lvc_cc_email_id      VARCHAR2 (100);
      lvc_cc_req           VARCHAR2 (10);
         -- := NVL (FND_PROFILE.VALUE ('XXWC_PO_INCLUDE_BUYER_PO_EMAIL'), 'N');  commented in Rev 1.1
   BEGIN
      IF g_po_wf_debug = 'Y'
      THEN
         write_log (
               'Email Process Started PO'
            || p_po_number
            || ' Rev '
            || p_revision_num
            || 'Sub: '
            || p_email_subject
            || ' Body '
            || p_email_body_file);
         write_log (
               'Email Process Started Sender'
            || p_email_sender
            || ' Rec '
            || p_email_receiver
            || 'Att '
            || p_email_attachment);
      END IF;


      -- Intializing
      lvc_email_subject := p_email_subject;
      lvc_email_receiver := p_email_receiver;

      BEGIN
         l_sec := 'Is DB Directory exists';

         SELECT directory_path
           INTO lvc_dir_name
           FROM dba_directories
          WHERE directory_name = 'XXWC_PO_SUPP_EMAIL_PDF_DIR';
      EXCEPTION
         WHEN OTHERS
         THEN
            x_return_status := 'ERROR';
            lvc_error_mess := 'XXWC_PO_SUPP_EMAIL_PDF_DIR Directory not exist';
            RAISE lvc_program_exp;
      END;

      -- Added below code in Rev 1.1
      BEGIN
         SELECT NVL (B.PROFILE_OPTION_VALUE, 'N')
           INTO lvc_cc_req
           FROM APPS.FND_PROFILE_OPTIONS A,
                APPS.FND_PROFILE_OPTION_VALUES B,
                APPS.PO_HEADERS_ALL C,
                APPS.FND_USER D
          WHERE     A.PROFILE_OPTION_NAME = 'XXWC_PO_INCLUDE_BUYER_PO_EMAIL'
                AND A.PROFILE_OPTION_ID = B.PROFILE_OPTION_ID
                AND D.USER_ID = B.LEVEL_VALUE
                AND C.AGENT_ID = D.EMPLOYEE_ID
                AND C.SEGMENT1 = p_po_number;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            lvc_cc_req := 'N';
         WHEN OTHERS
         THEN
            lvc_cc_req := 'N';
      END;

      -- Added above code in Rev 1.1	  

      BEGIN
         SELECT LOWER (NAME) INTO lvc_env_name FROM v$database;

         IF g_po_wf_debug = 'Y'
         THEN
            write_log (
                  'Environment checking '
               || lvc_env_name
               || ' CC email flag '
               || lvc_cc_req);
         END IF;

         IF lvc_env_name NOT LIKE '%prd%'
         THEN
            --Subject changes
            lvc_email_subject :=
                  lvc_email_subject
               || ' [ THIS IS FROM TEST INSTANCE. PLEASE IGNORE ]';

            IF INSTR (LOWER (lvc_email_receiver), '@hdsupply', 1) = 0
            THEN
               lvc_email_receiver := p_email_sender;
            END IF;
         END IF;
      END;

      IF lvc_cc_req = 'Y'
      THEN
         lvc_cc_email_id := p_email_sender;
      END IF;

      l_sec := ' Initializing Apps variables';

      fnd_global.APPS_INITIALIZE (user_id        => ln_user_id,
                                  resp_id        => ln_resp_id,
                                  resp_appl_id   => ln_resp_appl_id);

      l_sec := 'Submitting XXWC_PO_EMAIL_SUPPLIER concurrent Program';

      ln_request_id :=
         fnd_request.submit_request (
            application   => 'XXWC',
            program       => 'XXWC_PO_EMAIL_SUPPLIER',
            description   => '',
            start_time    => '',
            sub_request   => FALSE,
            argument1     => lvc_email_subject,
            argument2     => lvc_dir_name || '/' || p_email_body_file,
            argument3     => p_email_sender,
            argument4     => lvc_email_receiver,
            argument5     => lvc_dir_name || '/' || p_email_attachment,
            argument6     => lvc_cc_email_id);

      COMMIT;

      l_sec :=
            'XXWC_PO_EMAIL_SUPPLIER concurrent Program submitted Request Id '
         || ln_request_id;

      IF NVL (ln_request_id, 0) > 0
      THEN
         IF fnd_concurrent.wait_for_request (ln_request_id,
                                             ln_interval,
                                             ln_max_time,
                                             lvc_req_phase,
                                             lvc_req_status,
                                             lvc_del_phase,
                                             lvc_del_status,
                                             lvc_message)
         THEN
            l_sec := 'XXWC_PO_EMAIL_SUPPLIER concurrent Program completed ';

            INSERT INTO XXWC.XXWC_PO_EMAIL_COMM_HIST_TBL (PO_NUMBER,
                                                          PO_REVISION_NUM,
                                                          SENDER_EMAIL_ADD,
                                                          RCV_EMAIL_ADD,
                                                          PO_EMAIL_SUBJECT,
                                                          PO_EMAIL_BODY,
                                                          PO_EMAIL_ATTACH,
                                                          CONC_REQUEST_ID,
                                                          CONC_REQ_STATUS,
                                                          CREATION_DATE)
                 VALUES (p_po_number,
                         p_revision_num,
                         p_email_sender,
                         lvc_email_receiver,
                         lvc_email_subject,
                         p_email_body_file,
                         p_email_attachment,
                         ln_request_id,
                         lvc_del_status,
                         SYSDATE);

            l_sec :=
               'validating XXWC_PO_EMAIL_SUPPLIER concurrent Program status';

            -- Error occured
            IF lvc_del_phase != 'COMPLETE' OR lvc_del_status != 'NORMAL'
            THEN
               l_sec :=
                     'An error occured in the running the invoice import'
                  || lvc_message
                  || '.';
               x_return_status := 'ERROR';
            ELSE
               x_return_status := 'SUCCESS';
            END IF;
         END IF;
      ELSE
         l_sec :=
               'Request Id not created for PO Number: '
            || p_po_number
            || ' Rev: '
            || p_revision_num
            || 'WF item key: '
            || g_wf_item_key;


         INSERT INTO XXWC.XXWC_PO_EMAIL_COMM_HIST_TBL (PO_NUMBER,
                                                       PO_REVISION_NUM,
                                                       SENDER_EMAIL_ADD,
                                                       RCV_EMAIL_ADD,
                                                       PO_EMAIL_SUBJECT,
                                                       PO_EMAIL_BODY,
                                                       PO_EMAIL_ATTACH,
                                                       CONC_REQUEST_ID,
                                                       CONC_REQ_STATUS,
                                                       CREATION_DATE,
                                                       NOTES)
              VALUES (p_po_number,
                      p_revision_num,
                      p_email_sender,
                      lvc_email_receiver,
                      lvc_email_subject,
                      p_email_body_file,
                      p_email_attachment,
                      ln_request_id,
                      lvc_del_status,
                      SYSDATE,
                      'Request Id not submitted');

         x_return_status := 'ERROR';
         RAISE lvc_program_exp;
      END IF;

      IF g_po_wf_debug = 'Y'
      THEN
         write_log ('x_return_status ' || x_return_status);
      END IF;

      COMMIT;
   EXCEPTION
      WHEN lvc_program_exp
      THEN
         COMMIT;
         x_return_status := 'ERROR';
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => NULL,
            p_error_desc          => lvc_error_mess,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
      WHEN OTHERS
      THEN
         COMMIT;
         x_return_status := 'ERROR';
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 1000),
            p_error_desc          => 'Error Occured in ' || p_po_number,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
   END;

   -- ==========================================================================================================
   -- Procedure: write_log
   --Purpose : Writing log table
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     13-JUL-2017   P.Vamshidhar    TMS# 20170323-00270 - Auto-fill E-Mail Address on PO Approval form
   --                                       Initial version
   -- ==========================================================================================================

   PROCEDURE write_log (p_message IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
      lvc_procedure   VARCHAR2 (30) := 'write_log';
   BEGIN
      INSERT INTO XXWC.XXWC_PO_EMAIL_COMM_LOG_TBL (WF_ITEM_KEY,
                                                   LOG_MESSAGE,
                                                   CREATION_DATE,
                                                   CREATED_BY)
           VALUES (g_wf_item_key,
                   p_message,
                   SYSDATE,
                   g_user_id);

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => '',
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 1000),
            p_error_desc          => 'Error Occured in ' || g_wf_item_key,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
   END;

   -- ==========================================================================================================
   -- Procedure: WC_PO_NEW_COMM_NO_WAIT
   -- Purpose: procedure created to handle no wait on program
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     13-JUL-2017   P.Vamshidhar    TMS# 20170323-00270 - Auto-fill E-Mail Address on PO Approval form
   --                                       Initial version
   -- 1.1     24-Aug-2017   P.Vamshidhar    TMS#20170824-00104 - FW: Email Po's to suppliers      
   -- ==========================================================================================================

   PROCEDURE WC_PO_NEW_COMM_NO_WAIT (itemtype    IN            VARCHAR2,
                                     itemkey     IN            VARCHAR2,
                                     actid       IN            NUMBER,
                                     funcmode    IN            VARCHAR2,
                                     resultout      OUT NOCOPY VARCHAR2)
   IS
      lvc_email_receiver   VARCHAR2 (32767);
      lvc_email_subject    VARCHAR2 (32767);
      ln_org_id            NUMBER := FND_PROFILE.VALUE ('ORG_ID');
      l_document_type      po_headers.type_lookup_code%TYPE;
      l_document_num_rel   PO_HEADERS.SEGMENT1%TYPE;
      l_revision_num       PO_HEADERS.REVISION_NUM%TYPE;
      l_document_subtype   VARCHAR2 (1000);
      x_return_status      VARCHAR2 (100);
      lvc_procedure        VARCHAR2 (40) := 'WC_PO_NEW_COMM_NO_WAIT';  -- Changed in Rev 1.1
      lvc_wf_proc_enable   VARCHAR2 (30)
         := NVL (FND_PROFILE.VALUE ('XXWC_PO_CUST_EMAIL_WF_PROC_ENABLE'),
                 'Y');
      l_sec                VARCHAR2 (4000);
   BEGIN
      g_wf_item_key := itemkey;

      IF g_po_wf_debug = 'Y'
      THEN
         l_sec := 'WC_PO_NEW_COMM_NO_WAIT Process Started';
         write_log (l_sec);
      END IF;


      IF (funcmode <> wf_engine.eng_run)
      THEN
         resultout := wf_engine.eng_null;
         RETURN;
      END IF;

      IF ln_org_id = '162' AND lvc_wf_proc_enable = 'Y'
      THEN
         l_sec := 'White CAP PO';

         l_document_type :=
            PO_WF_UTIL_PKG.GetItemAttrText (itemtype   => itemtype,
                                            itemkey    => itemkey,
                                            aname      => 'DOCUMENT_TYPE');

         l_sec := 'l_document_type ' || l_document_type;


         l_document_subtype :=
            PO_WF_UTIL_PKG.GetItemAttrText (itemtype   => itemtype,
                                            itemkey    => itemkey,
                                            aname      => 'DOCUMENT_SUBTYPE');


         l_sec := 'l_document_subtype ' || l_document_subtype;

         l_document_num_rel :=
            PO_WF_UTIL_PKG.GetItemAttrText (itemtype   => itemtype,
                                            itemkey    => itemkey,
                                            aname      => 'DOCUMENT_NUM_REL');

         l_sec := 'l_document_num_rel ' || l_document_num_rel;


         l_revision_num :=
            PO_WF_UTIL_PKG.GetItemAttrNumber (itemtype   => itemtype,
                                              itemkey    => itemkey,
                                              aname      => 'REVISION_NUMBER');

         l_sec := 'l_revision_num ' || l_revision_num;

         lvc_email_receiver :=
            wf_engine.GetItemAttrText (itemtype   => itemtype,
                                       itemkey    => itemkey,
                                       aname      => 'EMAIL_ADDRESS');

         l_sec := 'lvc_email_receiver ' || lvc_email_receiver;


         IF g_po_wf_debug = 'Y'
         THEN
            l_sec :=
                  'Doc Type '
               || l_document_type
               || ' PO Num '
               || l_document_num_rel
               || ' Rev. '
               || l_revision_num
               || ' Supp email '
               || lvc_email_receiver;
            write_log (l_sec);
         END IF;

         IF l_document_num_rel IS NOT NULL
         THEN
            l_sec := ' Initiating concurrent Program';

            Initiate_conc_request (p_po_number     => l_document_num_rel,
                                   p_rev_number    => l_revision_num,
                                   p_rec_email     => lvc_email_receiver,
                                   x_return_stat   => x_return_status);

            l_sec := 'Validating Program submittion.';

            IF x_return_status = 'SUCCESS'
            THEN
               IF PO_COMMUNICATION_PROFILE = 'T'
               THEN
                  IF        l_document_type IN ('PO', 'PA')
                        AND l_document_subtype IN ('STANDARD',
                                                   'BLANKET',
                                                   'CONTRACT')
                     OR (    l_document_type = 'RELEASE'
                         AND l_document_subtype = 'BLANKET')
                  THEN
                     resultout := wf_engine.eng_completed || ':' || 'Y';
                  ELSE
                     resultout := wf_engine.eng_completed || ':' || 'N';
                  END IF;
               ELSE
                  resultout := wf_engine.eng_completed || ':' || 'N';
               END IF;
            ELSE
               resultout := wf_engine.eng_completed || ':' || 'N';
            END IF;
         ELSE
            resultout := wf_engine.eng_completed || ':' || 'N';
         END IF;
      ELSE
         resultout := wf_engine.eng_completed || ':' || 'N';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec || ' wf_item_key ' || g_wf_item_key,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 1000),
            p_error_desc          => 'Error Occured in ' || l_document_num_rel,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');


         wf_core.context ('XXWC_PO_COMMUNICATION_PVT_PKG',
                          'WC_PO_NEW_COMMUNICATION',
                          'Exception occured ');
         RAISE;
   END;

   -- ==========================================================================================================
   -- Procedure: EMAIL_REQUEST_SUBMIT_NO_WAIT
   -- Purpose: Sending email to supplier
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     13-JUL-2017   P.Vamshidhar    TMS# 20170323-00270 - Auto-fill E-Mail Address on PO Approval form
   --                                       Initial version
   -- 1.1     24-Aug-2017   P.Vamshidhar    TMS#20170824-00104 - FW: Email Po's to suppliers         
   -- ==========================================================================================================

   PROCEDURE EMAIL_REQUEST_SUBMIT_NO_WAIT (
      p_po_number          IN     NUMBER,
      p_revision_num       IN     NUMBER,
      p_email_subject      IN     VARCHAR2,
      p_email_body_file    IN     VARCHAR2,
      p_email_sender       IN     VARCHAR2,
      p_email_receiver     IN     VARCHAR2,
      p_email_attachment   IN     VARCHAR2,
      x_return_status         OUT VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
      lvc_dir_name         VARCHAR2 (100);
      ln_request_id        NUMBER;
      ln_interval          NUMBER := 5;
      ln_max_time          NUMBER := 10000;
      lvc_req_phase        VARCHAR2 (100);
      lvc_req_status       VARCHAR2 (100);
      lvc_del_phase        VARCHAR2 (50);
      lvc_del_status       VARCHAR2 (50);
      lvc_message          VARCHAR2 (4000);
      lvc_error_mess       VARCHAR2 (4000);
      l_sec                VARCHAR2 (32767);
      lvc_program_exp      EXCEPTION;
      lvc_procedure        VARCHAR2 (100) := 'EMAIL_REQUEST_SUBMIT_NO_WAIT'; -- Changed in Rev 1.1
      lvc_env_name         VARCHAR2 (100);
      lvc_email_subject    VARCHAR2 (4000);
      lvc_email_receiver   VARCHAR2 (4000);
      lvc_cc_email_id      VARCHAR2 (100);
      lvc_cc_req           VARCHAR2 (10); 
         --:= NVL (FND_PROFILE.VALUE ('XXWC_PO_INCLUDE_BUYER_PO_EMAIL'), 'N'); -- commented in Rev 1.1
   BEGIN
      IF g_po_wf_debug = 'Y'
      THEN
         write_log (
               'Email Process Started PO'
            || p_po_number
            || ' Rev '
            || p_revision_num
            || 'Sub: '
            || p_email_subject
            || ' Body '
            || p_email_body_file);
         write_log (
               'Email Process Started Sender'
            || p_email_sender
            || ' Rec '
            || p_email_receiver
            || 'Att '
            || p_email_attachment);
      END IF;


      -- Intializing
      lvc_email_subject := p_email_subject;
      lvc_email_receiver := p_email_receiver;

      BEGIN
         l_sec := 'Is DB Directory exists';

         SELECT directory_path
           INTO lvc_dir_name
           FROM dba_directories
          WHERE directory_name = 'XXWC_PO_SUPP_EMAIL_PDF_DIR';
      EXCEPTION
         WHEN OTHERS
         THEN
            x_return_status := 'ERROR';
            lvc_error_mess := 'XXWC_PO_SUPP_EMAIL_PDF_DIR Directory not exist';
            RAISE lvc_program_exp;
      END;
	  
      -- Added below code in Rev 1.1
      BEGIN
         SELECT NVL (B.PROFILE_OPTION_VALUE, 'N')
           INTO lvc_cc_req
           FROM APPS.FND_PROFILE_OPTIONS A,
                APPS.FND_PROFILE_OPTION_VALUES B,
                APPS.PO_HEADERS_ALL C,
                APPS.FND_USER D
          WHERE     A.PROFILE_OPTION_NAME = 'XXWC_PO_INCLUDE_BUYER_PO_EMAIL'
                AND A.PROFILE_OPTION_ID = B.PROFILE_OPTION_ID
                AND D.USER_ID = B.LEVEL_VALUE
                AND C.AGENT_ID = D.EMPLOYEE_ID
                AND C.SEGMENT1 = p_po_number;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            lvc_cc_req := 'N';
         WHEN OTHERS
         THEN
            lvc_cc_req := 'N';
      END;

      -- Added above code in Rev 1.1	  

      BEGIN
         SELECT LOWER (NAME) INTO lvc_env_name FROM v$database;

         IF g_po_wf_debug = 'Y'
         THEN
            write_log (
                  'Environment checking '
               || lvc_env_name
               || ' CC email flag '
               || lvc_cc_req);
         END IF;

         IF lvc_env_name NOT LIKE '%prd%'
         THEN
            --Subject changes
            lvc_email_subject :=
                  lvc_email_subject
               || ' [ THIS IS FROM TEST INSTANCE. PLEASE IGNORE ]';

            IF INSTR (LOWER (lvc_email_receiver), '@hdsupply', 1) = 0
            THEN
               lvc_email_receiver := p_email_sender;
            END IF;
         END IF;
      END;

      IF lvc_cc_req = 'Y'
      THEN
         lvc_cc_email_id := p_email_sender;
      END IF;

      l_sec := 'Submitting XXWC_PO_EMAIL_SUPPLIER concurrent Program';

      ln_request_id :=
         fnd_request.submit_request (
            application   => 'XXWC',
            program       => 'XXWC_PO_EMAIL_SUPPLIER',
            description   => '',
            start_time    => '',
            sub_request   => FALSE,
            argument1     => lvc_email_subject,
            argument2     => lvc_dir_name || '/' || p_email_body_file,
            argument3     => p_email_sender,
            argument4     => lvc_email_receiver,
            argument5     => lvc_dir_name || '/' || p_email_attachment,
            argument6     => lvc_cc_email_id);

      COMMIT;

      l_sec :=
            'XXWC_PO_EMAIL_SUPPLIER concurrent Program submitted Request Id '
         || ln_request_id;

      IF NVL (ln_request_id, 0) > 0
      THEN
         l_sec := 'XXWC_PO_EMAIL_SUPPLIER concurrent Program submitted ';

         INSERT INTO XXWC.XXWC_PO_EMAIL_COMM_HIST_TBL (PO_NUMBER,
                                                       PO_REVISION_NUM,
                                                       SENDER_EMAIL_ADD,
                                                       RCV_EMAIL_ADD,
                                                       PO_EMAIL_SUBJECT,
                                                       PO_EMAIL_BODY,
                                                       PO_EMAIL_ATTACH,
                                                       CONC_REQUEST_ID,
                                                       CONC_REQ_STATUS,
                                                       CREATION_DATE)
              VALUES (p_po_number,
                      p_revision_num,
                      p_email_sender,
                      lvc_email_receiver,
                      lvc_email_subject,
                      p_email_body_file,
                      p_email_attachment,
                      ln_request_id,
                      lvc_del_status,
                      SYSDATE);

         x_return_status := 'SUCCESS';
      ELSE
         l_sec :=
               'Request Id not created for PO Number: '
            || p_po_number
            || ' Rev: '
            || p_revision_num
            || 'WF item key: '
            || g_wf_item_key;


         INSERT INTO XXWC.XXWC_PO_EMAIL_COMM_HIST_TBL (PO_NUMBER,
                                                       PO_REVISION_NUM,
                                                       SENDER_EMAIL_ADD,
                                                       RCV_EMAIL_ADD,
                                                       PO_EMAIL_SUBJECT,
                                                       PO_EMAIL_BODY,
                                                       PO_EMAIL_ATTACH,
                                                       CONC_REQUEST_ID,
                                                       CONC_REQ_STATUS,
                                                       CREATION_DATE,
                                                       NOTES)
              VALUES (p_po_number,
                      p_revision_num,
                      p_email_sender,
                      lvc_email_receiver,
                      lvc_email_subject,
                      p_email_body_file,
                      p_email_attachment,
                      ln_request_id,
                      lvc_del_status,
                      SYSDATE,
                      'Request Id not submitted');

         RAISE lvc_program_exp;
      END IF;

      IF g_po_wf_debug = 'Y'
      THEN
         write_log ('x_return_status ' || x_return_status);
      END IF;

      COMMIT;
   EXCEPTION
      WHEN lvc_program_exp
      THEN
         COMMIT;
         x_return_status := 'ERROR';
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => NULL,
            p_error_desc          => lvc_error_mess,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
      WHEN OTHERS
      THEN
         COMMIT;
         x_return_status := 'ERROR';
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 1000),
            p_error_desc          => 'Error Occured in ' || p_po_number,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
   END;

   -- ==========================================================================================================
   -- Procedure: Initiate_conc_request
   -- Purpose: 
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     13-JUL-2017   P.Vamshidhar    TMS# 20170323-00270 - Auto-fill E-Mail Address on PO Approval form
   --                                       Initial version
   -- ==========================================================================================================
   
   PROCEDURE Initiate_conc_request (p_po_number     IN     VARCHAR2,
                                    p_rev_number    IN     VARCHAR2,
                                    p_rec_email     IN     VARCHAR2,
                                    x_return_stat      OUT VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
      ln_request_id   NUMBER;
      l_sec           VARCHAR2 (1000);
      lvc_procedure   VARCHAR2 (1000) := 'INITIATE_CONC_REQUEST';
   BEGIN
      l_sec := 'Submitting concurrent program';
      ln_request_id :=
         fnd_request.submit_request (
            application   => 'XXWC',
            program       => 'XXWC_PO_EMAIL_SUPP_DIRECT',
            description   => '',
            start_time    => '',
            sub_request   => FALSE,
            argument1     => p_po_number,
            argument2     => p_rev_number,
            argument3     => p_rec_email);

      COMMIT;

      l_sec := 'Submitting submittion completed' || ln_request_id;

      IF NVL (ln_request_id, 0) > 0
      THEN
         x_return_stat := 'SUCCESS';
      ELSE
         x_return_stat := 'ERROR';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         COMMIT;
         x_return_stat := 'ERROR';
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 250),
            p_error_desc          => NULL,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
   END;

   -- ==========================================================================================================
   -- Procedure: PRE_PROCESS
   -- Purpose: 
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     13-JUL-2017   P.Vamshidhar    TMS# 20170323-00270 - Auto-fill E-Mail Address on PO Approval form
   --                                       Initial version
   -- 1.1     24-Aug-2017   P.Vamshidhar    TMS#20170824-00104 - FW: Email Po's to suppliers   
   -- ==========================================================================================================   

   PROCEDURE PRE_PROCESS (x_errbuf          OUT VARCHAR2,
                          x_retcode         OUT VARCHAR2,
                          p_po_number    IN     VARCHAR2,
                          p_rev_number   IN     VARCHAR2,
                          p_rec_email    IN     VARCHAR2)
   IS
      l_document_num_rel     PO_HEADERS.SEGMENT1%TYPE;
      l_revision_num         PO_HEADERS.REVISION_NUM%TYPE;
      l_sec                  VARCHAR2 (1000);
      lvc_vendor_name        ap_suppliers.vendor_name%TYPE;
      lvc_buyer_name         per_all_people_f.full_name%TYPE;
      ln_agent_id            po_agents.agent_id%TYPE;
      lvc_user_email         VARCHAR2 (32767):='noreply@hdsupply.com'; -- changed in Rev 1.1
      ln_user_id             fnd_user.user_id%TYPE := FND_GLOBAL.USER_ID;
      lvc_procedure          VARCHAR2 (100) := 'PRE_PROCESS';
      lvc_email_sender       PER_ALL_PEOPLE_F.EMAIL_ADDRESS%TYPE;
      lvc_email_receiver     PER_ALL_PEOPLE_F.EMAIL_ADDRESS%TYPE;
      lvc_buyer_phone        per_phones.PHONE_NUMBER%TYPE;
      lvc_email_subject      VARCHAR2 (32767);
      lb_pdf_exists          BOOLEAN;
      lvc_email_attachment   VARCHAR2 (4000);
      lvc_email_body_file    VARCHAR2 (4000);
      ln_org_id              NUMBER := FND_PROFILE.VALUE ('ORG_ID');
      lvc_utl_file           UTL_FILE.file_type;
      lvc_email_dir          VARCHAR2 (1000) := 'XXWC_PO_SUPP_EMAIL_PDF_DIR';
      lvc_return_status      VARCHAR2 (100);
   BEGIN
      l_document_num_rel := P_PO_NUMBER;
      l_revision_num := p_rev_number;
      lvc_email_receiver := p_rec_email;

      l_sec := ' Deriving Agent_id ' || l_document_num_rel;

      BEGIN
         SELECT pov.vendor_name, poh.agent_id
           INTO lvc_vendor_name, ln_agent_id
           FROM po_headers poh, po_vendors pov
          WHERE     poh.vendor_id = pov.vendor_id
                AND poh.segment1 = l_document_num_rel;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            ln_agent_id := 0;
         WHEN OTHERS
         THEN
            ln_agent_id := 0;
            xxcus_error_pkg.xxcus_error_main_api (
               p_called_from         => g_err_callfrom || '.' || lvc_procedure,
               p_calling             => l_sec,
               p_ora_error_msg       => SUBSTR (SQLERRM, 1, 1000),
               p_error_desc          =>    'Error Occured in PO'
                                        || l_document_num_rel,
               p_distribution_list   => g_distro_list,
               p_module              => 'XXWC');
      END;


      IF NVL (ln_agent_id, 0) > 0
      THEN
/*  -- Commented in Rev 1.1
	  -- Deriving user email address.
         l_sec := ' Deriving user email address ';

         BEGIN
            SELECT pap.email_address
              INTO lvc_user_email
              FROM apps.fnd_user fu, per_all_people_f pap
             WHERE     fu.employee_id = pap.person_id
                   AND fu.user_id = ln_user_id
                   AND TRUNC (SYSDATE) BETWEEN NVL (pap.EFFECTIVE_START_DATE,
                                                    TRUNC (SYSDATE))
                                           AND NVL (pap.EFFECTIVE_END_DATE,
                                                    TRUNC (SYSDATE) + 1)
                   AND TRUNC (SYSDATE) BETWEEN NVL (fu.START_DATE,
                                                    TRUNC (SYSDATE))
                                           AND NVL (fu.END_DATE,
                                                    TRUNC (SYSDATE) + 1)
                   AND ROWNUM = 1;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               lvc_user_email := NULL;
            WHEN OTHERS
            THEN
               lvc_user_email := NULL;
               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => g_err_callfrom || '.' || lvc_procedure,
                  p_calling             => l_sec,
                  p_ora_error_msg       => SUBSTR (SQLERRM, 1, 1000),
                  p_error_desc          =>    'Error Occured in '
                                           || l_document_num_rel,
                  p_distribution_list   => g_distro_list,
                  p_module              => 'XXWC');
         END;
*/

         -- Deriving Buyer Phone Numberand email address
         BEGIN
            SELECT --NVL (lvc_user_email, pap.email_address), -- Commented in Rev 1.1
                   NVL (pap.email_address,lvc_user_email), -- Added in Rev 1.1
                   pap.first_name || ' ' || pap.last_name
              INTO lvc_email_sender, lvc_buyer_name
              FROM per_all_people_f pap
             WHERE     pap.person_id = ln_agent_id
                   AND TRUNC (SYSDATE) BETWEEN pap.effective_start_date
                                           AND pap.effective_end_date;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               --lvc_buyer_phone := NULL;
               lvc_email_sender := lvc_user_email;
            WHEN OTHERS
            THEN
               -- lvc_buyer_phone := NULL;
               lvc_email_sender := lvc_user_email;

               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => g_err_callfrom || '.' || lvc_procedure,
                  p_calling             => l_sec || ' wf_item_key ' || g_wf_item_key,
                  p_ora_error_msg       => SUBSTR (SQLERRM, 1, 1000),
                  p_error_desc          =>    'Error Occured in '
                                           || l_document_num_rel,
                  p_distribution_list   => g_distro_list,
                  p_module              => 'XXWC');
         END;

         BEGIN
            SELECT NVL (A.buyer_contact_phone, cp_ship_loc_phone)
              INTO lvc_buyer_phone
              FROM PO_HEADERS_XML A, PO_HEADERS_ALL B
             WHERE     A.PO_HEADER_ID = B.PO_HEADER_ID
                   AND B.SEGMENT1 = l_document_num_rel;

            IF lvc_buyer_phone IS NULL
            THEN
               BEGIN
                  SELECT TELEPHONE_NUMBER_1
                    INTO lvc_buyer_phone
                    FROM PER_ASSIGNMENTS_V7 pav7
                   WHERE     NVL (pav7.inactive_date, SYSDATE + 1) >= SYSDATE
                         AND person_id = ln_agent_id
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     lvc_buyer_phone := NULL;
               END;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;


         IF lvc_buyer_phone IS NOT NULL
         THEN
            SELECT CASE
                      WHEN LENGTH (lvc_buyer_phone) = 10
                      THEN
                            '('
                         || SUBSTR (lvc_buyer_phone, 1, 3)
                         || ') '
                         || SUBSTR (lvc_buyer_phone, 4, 3)
                         || '-'
                         || SUBSTR (lvc_buyer_phone, 7)
                      ELSE
                         lvc_buyer_phone
                   END
              INTO lvc_buyer_phone
              FROM DUAL;
         END IF;



         IF lvc_email_sender IS NOT NULL
         THEN
            l_sec := ' Deriving email body ';

            FND_MESSAGE.clear;
            fnd_message.set_name ('XXWC', 'XXWC_PO_PDF_EMAIL_TEXT');
            fnd_message.set_token ('PO_DOCUMENT',
                                   TO_CHAR (l_document_num_rel));
            fnd_message.set_token ('SUPPLIER_NAME', lvc_vendor_name);
            fnd_message.set_token ('BUYER_NAME', lvc_buyer_name);
            fnd_message.set_token ('BUYER_PHONE', TO_CHAR (lvc_buyer_phone));

            lvc_email_body_file :=
                  'PO_'
               || ln_org_id
               || '_'
               || l_document_num_rel
               || '_'
               || l_revision_num
               || '_US.txt';

            l_sec := ' Deriving email attachment ';

            lvc_email_attachment :=
                  'PO_'
               || ln_org_id
               || '_'
               || l_document_num_rel
               || '_'
               || l_revision_num
               || '_US.pdf';

            l_sec := ' generating email subject ';

            lvc_utl_file :=
               UTL_FILE.fopen (lvc_email_dir,
                               lvc_email_body_file,
                               'W',
                               32767);

            UTL_FILE.put_line (lvc_utl_file, FND_MESSAGE.GET);

            UTL_FILE.fclose (lvc_utl_file);

            fnd_message.set_name ('XXWC', 'XXWC_PO_PDF_EMAIL_SUBJECT');
            fnd_message.set_token ('PO_NUMBER', TO_CHAR (l_document_num_rel));

            lvc_email_subject := FND_MESSAGE.GET;

            l_sec :=
                  ' generating email attachment PO '
               || l_document_num_rel
               || ' Rev '
               || l_revision_num;

            lb_pdf_exists :=
               PDF_DOWNLOAD (p_po_number     => l_document_num_rel,
                             p_po_revision   => l_revision_num);


            IF (lb_pdf_exists)
            THEN
               EMAIL_REQUEST_SUBMIT_NO_WAIT (
                  p_po_number          => l_document_num_rel,
                  p_revision_num       => l_revision_num,
                  p_email_subject      => lvc_email_subject,
                  p_email_body_file    => lvc_email_body_file,
                  p_email_sender       => lvc_email_sender,
                  p_email_receiver     => lvc_email_receiver,
                  p_email_attachment   => lvc_email_attachment,
                  x_return_status      => lvc_return_status);

               IF lvc_return_status = 'ERROR'
               THEN
                  x_retcode := 2;
                  x_errbuf :=
                     'Error Occured in Child Program, please check logs.';
               END IF;
            ELSE
               x_retcode := 2;
               x_errbuf :=
                  'PDF file not generated, please contact system administrator';
            END IF;
         END IF;
      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 250),
            p_error_desc          => NULL,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
    END;
	
   -- ==========================================================================================================
   -- Procedure: insert_log
   -- Purpose: Inserting log
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.2     13-Oct-2017   P.Vamshidhar    TMS#20170207-00206 - Research the issue where users are able to enter
   --                                       invalid email address and Fax numbers while app	       
   -- ========================================================================================================== 
   PROCEDURE insert_log (P_ITEM_KEY IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      DELETE FROM XXWC.XXWC_PO_NO_TRANSMISSION_LOG_T
            WHERE item_key=p_item_key;

      INSERT INTO XXWC.XXWC_PO_NO_TRANSMISSION_LOG_T (ITEM_KEY,
                                                      USER_ID,
                                                      RESP_ID,
                                                      RESP_APPL_ID,
                                                      CREATION_DATE)
           VALUES (P_ITEM_KEY,
                   FND_GLOBAL.USER_ID,
                   FND_GLOBAL.RESP_ID,
                   FND_GLOBAL.RESP_APPL_ID,
                   SYSDATE);
      COMMIT;
   EXCEPTION
   WHEN OTHERS THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' ||' insert_log',
            p_calling             => 'Inserting Log Table',
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 250),
            p_error_desc          => NULL,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
   END;

   -- ==========================================================================================================
   -- Procedure: delete_log
   -- Purpose: Deleting log
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.2     13-Oct-2017   P.Vamshidhar    TMS#20170207-00206 - Research the issue where users are able to enter
   --                                       invalid email address and Fax numbers while app	       
   -- ==========================================================================================================    
   PROCEDURE delete_log (P_ITEM_KEY IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      DELETE FROM XXWC.XXWC_PO_NO_TRANSMISSION_LOG_T
            WHERE item_key=p_item_key;
      COMMIT;
   
   EXCEPTION
   WHEN OTHERS THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' ||' delete_log',
            p_calling             => 'Deleting Log Table',
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 250),
            p_error_desc          => NULL,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
   END;

   -- ==========================================================================================================
   -- Procedure: WC_INITIALIZING_SESSION
   -- Purpose: Initiating session.
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.2     13-Oct-2017   P.Vamshidhar    TMS#20170207-00206 - Research the issue where users are able to enter
   --                                       invalid email address and Fax numbers while app	       
   -- ==========================================================================================================    
   PROCEDURE WC_INITIALIZING_SESSION (ITEMTYPE    IN            VARCHAR2,
                                      ITEMKEY     IN            VARCHAR2,
                                      ACTID       IN            NUMBER,
                                      FUNCMODE    IN            VARCHAR2,
                                      RESULTOUT      OUT NOCOPY VARCHAR2)
   IS
      ln_user_id           FND_USER.USER_ID%TYPE;
      ln_resp_id           FND_RESPONSIBILITY_VL.RESPONSIBILITY_ID%TYPE;
      ln_resp_appl_id      FND_RESPONSIBILITY_VL.APPLICATION_ID%TYPE;
      ln_org_id            NUMBER := FND_PROFILE.VALUE ('ORG_ID');
      lvc_wf_proc_enable   VARCHAR2 (30)
         := NVL (FND_PROFILE.VALUE ('XXWC_PO_CUST_SESSION_INITIATE_ENABLE'),
                 'Y');
    lvc_xml_flag PO_HEADERS_ALL.XML_FLAG%TYPE;
    lvc_notify_method PO_HEADERS_ALL.SUPPLIER_NOTIF_METHOD%TYPE;
   BEGIN

   IF ln_org_id = '162' AND lvc_wf_proc_enable = 'Y'
      THEN
       SELECT A.XML_FLAG, A.SUPPLIER_NOTIF_METHOD INTO lvc_xml_flag, lvc_notify_method FROM  po_headers_all a where a.WF_ITEM_KEY = itemkey;

         IF lvc_xml_flag = 'N' AND lvc_notify_method = 'NONE'
         THEN
               insert_log(itemkey); 
               SELECT user_id
                 INTO ln_user_id
                 FROM apps.fnd_user
                WHERE user_name = 'XXWC_INT_SUPPLYCHAIN';

               SELECT responsibility_id, application_id
                 INTO ln_resp_id, ln_resp_appl_id
                 FROM apps.fnd_responsibility_vl
                WHERE responsibility_name = 'HDS Purchasing Super User - WC';

            fnd_global.APPS_INITIALIZE (user_id        => ln_user_id,
                                        resp_id        => ln_resp_id,
                                        resp_appl_id   => ln_resp_appl_id);
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' ||' WC_INITIALIZING_SESSION',
            p_calling             => 'Initiating Session',
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 250),
            p_error_desc          => itemkey||' Error occured',
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
   END;

   -- ==========================================================================================================
   -- Procedure: WC_INITIALIZING_NEW_SESSION
   -- Purpose: Initiating Origional session.
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.2     13-Oct-2017   P.Vamshidhar    TMS#20170207-00206 - Research the issue where users are able to enter
   --                                       invalid email address and Fax numbers while app	       
   -- ==========================================================================================================    
   PROCEDURE WC_INITIALIZING_NEW_SESSION (ITEMTYPE    IN            VARCHAR2,
                                          ITEMKEY     IN            VARCHAR2,
                                          ACTID       IN            NUMBER,
                                          FUNCMODE    IN            VARCHAR2,
                                          RESULTOUT      OUT NOCOPY VARCHAR2)
   IS
      ln_user_id           FND_USER.USER_ID%TYPE;
      ln_resp_id           FND_RESPONSIBILITY_VL.RESPONSIBILITY_ID%TYPE;
      ln_resp_appl_id      FND_RESPONSIBILITY_VL.APPLICATION_ID%TYPE;
      ln_org_id            NUMBER := FND_PROFILE.VALUE ('ORG_ID');
      lvc_wf_proc_enable   VARCHAR2 (30)
         := NVL (FND_PROFILE.VALUE ('XXWC_PO_CUST_SESSION_INITIATE_ENABLE'),
                 'Y');
    lvc_xml_flag PO_HEADERS_ALL.XML_FLAG%TYPE;
    lvc_notify_method PO_HEADERS_ALL.SUPPLIER_NOTIF_METHOD%TYPE;
   BEGIN

      IF ln_org_id = '162' AND lvc_wf_proc_enable = 'Y'
      THEN
       SELECT A.XML_FLAG, A.SUPPLIER_NOTIF_METHOD INTO lvc_xml_flag, lvc_notify_method FROM  po_headers_all a where a.WF_ITEM_KEY = itemkey;

         IF lvc_xml_flag = 'N' AND lvc_notify_method = 'NONE'
         THEN

         BEGIN  
            select user_id, resp_id, resp_appl_id into ln_user_id, ln_resp_id, ln_resp_appl_id from XXWC.XXWC_PO_NO_TRANSMISSION_LOG_T
            where item_key=itemkey and rownum=1;

            fnd_global.APPS_INITIALIZE (user_id        => ln_user_id,
                                        resp_id        => ln_resp_id,
                                        resp_appl_id   => ln_resp_appl_id);

         
           delete_log(itemkey);
           
           exception
           when others then 
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' ||' WC_INITIALIZING_NEW_SESSION',
            p_calling             => ' Select query',
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 250),
            p_error_desc          => itemkey||' Error occured',
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
         end;  
         END IF;
      END IF;

   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' ||' WC_INITIALIZING_NEW_SESSION',
            p_calling             => 'Initiating Origional Session',
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 250),
            p_error_desc          => itemkey||' Error occured',
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
   END;
   
END XXWC_PO_COMMUNICATION_PVT_PKG;
/