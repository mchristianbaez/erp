/*************************************************************************
  $Header XXWC_OM_EMAIL_COMM_HIST_TBL.sql $
  Module Name: XXWC_OM_EMAIL_COMM_HIST_TBL

  PURPOSE: Table to maintain OM customers email .
  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------------
  1.1        13-Sep-2017  P.Vamshidhar         TMS#20170207-00206 - Research the issue where users are able to enter
                                               invalid email address and Fax numbers while app
*******************************************************************************************************************************************/ 
CREATE TABLE XXWC.XXWC_OM_EMAIL_COMM_HIST_TBL
(
   ORD_HEADER_ID      NUMBER,
   SENDER_EMAIL_ADD   VARCHAR2 (100 BYTE),
   RCV_EMAIL_ADD      VARCHAR2 (1000 BYTE),
   EMAIL_SUBJECT      VARCHAR2 (4000 BYTE),
   EMAIL_BODY         VARCHAR2 (4000 BYTE),
   EMAIL_ATTACH       VARCHAR2 (4000 BYTE),
   CONC_REQUEST_ID    NUMBER,
   CREATION_DATE      DATE
)
/