/*
 TMS: 20150928-00069    
 Date: 09/28/2015
 Notes: data fix script to process Pending Pre-bill line
*/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;


UPDATE apps.oe_order_lines_all
            SET 
            shipping_quantity = ordered_quantity,
                shipped_quantity = ordered_quantity,
                actual_shipment_date = TO_DATE('09/23/2015 11:18:48','MM/DD/YYYY HH24:MI:SS'),
                shipping_quantity_uom = order_quantity_uom,
                fulfilled_flag = 'Y',
                fulfillment_date = TO_DATE('09/23/2015 11:18:48','MM/DD/YYYY HH24:MI:SS'),
                fulfilled_quantity = ordered_quantity,
                --invoice_interface_status_code = 'YES',
               -- invoiced_quantity = ordered_quantity,
                --open_flag = 'N',
                --flow_status_code = 'CLOSED',
                last_updated_by = -1,
                last_update_date = SYSDATE
--                ACCEPTED_BY = 4716,
--                REVREC_SIGNATURE = 'TODD MYERS',
--                REVREC_SIGNATURE_DATE = '07-FEB-2013'
          WHERE line_id = 55247263
          and headeR_id=33647474;
		  
--1 row expected to be updated

commit;

/