drop table xxcus.xxcus_ozf_cashapp_log_archive cascade constraints;
create table xxcus.xxcus_ozf_cashapp_log_archive
(
  request_id     number,
  created_by     number,
  run_date date,
  run_mode   varchar2(20 byte),
  rebate_type      varchar2(30 byte),
  calendar_year    varchar2(10 byte),
  agreement        varchar2(2000 byte),
  customer_id      number,  
  customer_number  varchar2(30 byte),
  customer_name    varchar2(150 byte),
  cash_receipt_id  number,  
  receipt_number   varchar2(30 byte),
  receipt_date     date,
  receipt_amount   number,
  customer_trx_id  number,  
  invoice_number   varchar2(30 byte),
  invoice_date     date,
  invoice_amount   number,
  applied_amount   number,
  log_seq          number,
  api_status       varchar2(30 byte),
  api_msg_index    number,
  api_message      varchar2(4000 byte) 
)
tablespace xxcus_data
result_cache (mode default)
pctused    0
pctfree    10
initrans   1
maxtrans   255
storage    (
            initial          64k
            next             1m
            minextents       1
            maxextents       unlimited
            pctincrease      0
            buffer_pool      default
            flash_cache      default
            cell_flash_cache default
           )
logging 
nocompress 
nocache
noparallel
monitoring;
comment on table xxcus.xxcus_ozf_cashapp_log_archive is 'TMS 20150710-00251';
