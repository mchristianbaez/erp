/*************************************************************************
  $Header XXCUS_OZF_CASHAPP_LOG_TBL.sql $
  Module Name: XXCUS_OZF_CASHAPP_LOG_TBL

  PURPOSE: Used by HDS Rebates AR Auto Cash Application Process 

  REVISIONS:
  Ver        Date                   Author                          Description
  ---------  -----------          ------------------             ----------------
  1.0        03-Aug-2015  Balaguru Seshadri     Initial Version TMS # 20150710-00251

**************************************************************************/
DROP SEQUENCE XXCUS.XXCUS_OZF_CASHAPP_LOG_S;
CREATE SEQUENCE XXCUS.XXCUS_OZF_CASHAPP_LOG_S
  START WITH 20126
  MAXVALUE 9999999999999999999999999999
  MINVALUE 100
  NOCYCLE
  CACHE 5000
  ORDER;  
