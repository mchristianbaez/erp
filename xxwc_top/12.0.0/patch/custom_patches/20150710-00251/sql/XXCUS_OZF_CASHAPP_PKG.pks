CREATE OR REPLACE PACKAGE APPS.xxcus_ozf_cashapp_pkg as
   /*************************************************************************  
     $Header xxcus_ozf_cashapp_pkg.pks $
     Module Name: xxxcus_ozf_cashapp_pkg
     --
     PURPOSE:   This package is called by the concurrent program HDS Rebates: Auto Clear Rebate Payments
     --
     REVISIONS:
     Ver        Date           Author             Ticket                       Description
     ---------  ----------     --------------     -------------------------    -------------------------------------------------
     1.0        08/03/2015     Balaguru Seshadri  TMS: 20150710-00251          Created
                                                  ESMS: 256550
                                                                                      
   **************************************************************************/
 --
 -- Global Variables  
  g_customer_id       number       :=Null;
  g_type                       varchar2(20)  :=Null;  
  g_pmt_rbt_type     varchar2(20) :=Null; --01/27/2016
  g_calendar_year     varchar2(4)  :=Null;
  g_receipt_id        number       :=Null;       
  g_mode              varchar2(10);
  g_agreement_id      number       :=Null;
  g_agreement_code varchar2(240) :=Null;
  g_receipt_num       ar_cash_receipts_all.receipt_number%type :=Null;
  g_flush_stg_info varchar2(1) :=Null;
  g_inv_rbt_type varchar2(10) :=Null;
  g_exec_mode varchar2(1) :=Null;
  g_user_id number :=Null;
 --
  g_draft varchar2(20) :='DRAFT';
  g_final varchar2(20) :='FINAL'; 
  g_org_id number;
  g_request_id number :=0;
 --
    procedure extract_open_payments_invoices
     (
      p_cust_id in  number
     );    
 --
  function beforereport return boolean;
 --    
  function afterreport return boolean; 
   --
    procedure swap_receipt_offer      
    ( errbuf          out nocopy   varchar2
    ,retcode        out nocopy   varchar2
    ,p_customer_id     in           number
    ,p_old_offer_code       in           varchar2
    ,p_new_offer_code       in           varchar2    
    );            
   --              
end xxcus_ozf_cashapp_pkg;
/
