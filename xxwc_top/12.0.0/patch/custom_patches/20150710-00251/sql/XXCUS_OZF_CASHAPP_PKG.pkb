CREATE OR REPLACE PACKAGE BODY APPS.xxcus_ozf_cashapp_pkg as
   /*************************************************************************  
     $Header xxcus_ozf_cashapp_pkg.pks $
     Module Name: xxxcus_ozf_cashapp_pkg
     --
     PURPOSE:   This package is called by the concurrent program HDS Rebates: Auto Clear Rebate Payments
     --
     REVISIONS:
     Ver        Date           Author             Ticket                       Description
     ---------  ----------     --------------     -------------------------    -------------------------------------------------
     1.0        08/03/2015     Balaguru Seshadri  TMS: 20150710-00251          Created
                                                  ESMS: 256550
                                                                                      
   **************************************************************************/
    --
    procedure print_log (p_message in varchar2) is
    begin
     --
     if  xxcus_ozf_cashapp_pkg.g_exec_mode ='Y' then
      --
      -- Print debug log contents only when requested for troubleshooting...
      --
             if  fnd_global.conc_request_id >0 then
              fnd_file.put_line(fnd_file.log, p_message);
             else
              dbms_output.put_line(p_message);
             end if;      
      --
     end if;
     --
    end print_log;
    procedure print_out (p_message in varchar2) is
    begin
      --
             if  fnd_global.conc_request_id >0 then
              fnd_file.put_line(fnd_file.output, p_message);
             else
              dbms_output.put_line(p_message);
             end if;      
      --
    end print_out;    
    --    
   function get_field (v_delimiter in varchar2, n_field_no in number ,v_line_read in varchar2) return varchar2 is
       n_start_field_pos number;
       n_end_field_pos   number;
       v_get_field       varchar2(2000);
   begin
       if n_field_no = 1 then
          n_start_field_pos := 1;
       else
          n_start_field_pos := instr(v_line_read,v_delimiter,1,n_field_no-1)+1;
       end if;
      
       n_end_field_pos   := instr(v_line_read,v_delimiter,1,n_field_no) -1;
       if n_end_field_pos > 0 then
          v_get_field := substr(v_line_read,n_start_field_pos,(n_end_field_pos - n_start_field_pos)+1);
       else
          v_get_field := substr(v_line_read,n_start_field_pos); 
       end if;
      --
      return v_get_field;
      --
   exception
    when others then
     print_log ('Line ='||v_line_read);
     print_log ('get field: '||sqlerrm);
   end get_field;
  --    
    function get_cashapp_log_seq (p_mode in varchar2) return number is 
     --
     n_seq NUMBER :=Null;
     --
    begin 
     if p_mode ='NEXT_VALUE' then
       select xxcus.xxcus_ozf_cashapp_log_s.nextval
       into   n_seq
       from   dual;
     elsif p_mode ='CURRENT_VALUE' then       
       select xxcus.xxcus_ozf_cashapp_log_s.currval
       into   n_seq
       from   dual;     
     else
       Null;
     end if;
     return n_seq;
    exception
     when others then
      print_log('@get_cashapp_log_seq, when-others, message ='||sqlerrm);
      return Null;
    end get_cashapp_log_seq;
    --    
    procedure Cash_App_Log
      (
        p_rebate_type in varchar2
       ,p_cal_year in varchar2
       ,p_offer_code in varchar2
       ,p_customer_id in number
       ,p_customer_number in varchar2
       ,p_customer_name in varchar2         
       ,p_cash_receipt_id in number
       ,p_receipt_number  in varchar2
       ,p_receipt_date    in date
       ,p_receipt_amount  in number
       ,p_inv_id          in number
       ,p_inv_number      in varchar2
       ,p_invoice_date    in date
       ,p_inv_amount      in number
       ,p_applied_amount  in number
       ,p_api_status      in varchar2
       ,p_api_msg_index   in number
       ,p_api_msg         in varchar2       
      ) is
     --
     PRAGMA AUTONOMOUS_TRANSACTION;
     l_insert_seq NUMBER :=Null;
     --
    begin 
     --
     l_insert_seq :=get_cashapp_log_seq (p_mode =>'NEXT_VALUE');      
     --
     Insert Into xxcus.xxcus_ozf_cashapp_log
      (
          rebate_type --varchar2 (30)
         ,calendar_year --varchar2 (10)    
         ,agreement --varchar2(2000)
         ,customer_id --number
         ,customer_number --varchar2(30)
         ,customer_name --varchar2(240)
         ,cash_receipt_id  --number,
         ,receipt_number   --varchar2(30 byte),
         ,receipt_date     --date
         ,receipt_amount   --number,
         ,customer_trx_id  --number,
         ,invoice_number   --varchar2(30 byte)
         ,invoice_date     --date
         ,invoice_amount   --number
         ,applied_amount   --number
         ,log_seq          --number,
         ,api_status       --varchar2(30 byte),
         ,api_msg_index    --number,
         ,api_message      --varchar2(4000 byte)   
         ,execution_mode   --varchar2(20) 
         ,request_id --number 
         ,run_date --date
         ,created_by --number  
      )
     Values
      (
          p_rebate_type --varchar2(30)
         ,p_cal_year  --varchar2(10)
         ,p_offer_code --varchar2(2000)
         ,p_customer_id --number
         ,p_customer_number --varchar2(30)
         ,p_customer_name --varchar2(240)         
         ,p_cash_receipt_id --number,
         ,p_receipt_number  --varchar2(30 byte),
         ,p_receipt_date    --date
         ,p_receipt_amount  --number,
         ,p_inv_id          --number,
         ,p_inv_number      --varchar2(30 byte),
         ,p_invoice_date    --date
         ,p_inv_amount      --number,
         ,p_applied_amount  --number
         ,l_insert_seq      --number,
         ,p_api_status      --varchar2(1 byte),
         ,p_api_msg_index   --number,
         ,p_api_msg         --varchar2(4000 byte) 
         ,xxcus_ozf_cashapp_pkg.g_mode
         ,xxcus_ozf_cashapp_pkg.g_request_id
         ,sysdate
         ,xxcus_ozf_cashapp_pkg.g_user_id
      );
     --
     Commit; --Since this procedure is declared as a pragma auto transaction, we will commit here which is not going to affect the main cash app block
     --
    exception
     when others then
      rollback;
      print_log ('@Cash_App_Log');      
      print_log ('@Cash_App_Log, Cash_Receipt_Id ='||p_cash_receipt_id||', Customer_Trx_Id ='||p_inv_id);
      print_log ('@Cash_App_Log, '||'Message ='||sqlerrm);
      print_log ('@Cash_App_Log');
    end Cash_App_Log;
    --  
    procedure apply_cash
     (
      p_commit         in  varchar2,
      p_cust_id in number,
      p_cust_number in varchar2,
      p_cust_name in varchar2,     
      p_cr_id          in  number,
      p_rec_number     in  varchar2,
      p_rec_date       in  date,
      p_rec_amount     in  number,      
      p_inv_id         in  number,
      p_inv_number     in  varchar2,
      p_inv_date       in  date,
      p_inv_balance    in  number,
      p_amt_applied    in  number,      
      p_ret_status     out varchar2,
      p_reb_type in varchar2,
      p_year in varchar2,
      p_offer_code in varchar2          
     ) is
     --
      p_api_version        constant number := 1;
      p_init_msg_list      constant varchar2 (1) := fnd_api.g_true; 
      p_validation_level   constant number := fnd_api.g_valid_level_full;
      n_receipt_amount     constant number := 0;
      l_api_msg varchar2(2000) :=null;
      p_comments                    ar_receivable_applications.comments%type :=Null;
      --
      l_msg_count                   number := 0;
      l_msg_data                    varchar2 (255) :=Null;
      l_return_status               varchar2 (1) :=Null;
      l_cr_id                       number :=0;
      --
      d_gl_date                     date := trunc (sysdate);
      l_process_results             varchar2 (2000) :=Null;
      l_text                        varchar2 (200)  :=Null; 
      show_cr_id                    number          :=Null;
      --                   
    begin
      savepoint stage_10;
      if (sign(p_inv_balance) =-1) then
         p_comments :='Rebates: Auto apply negative invoices or credit memos';      
      else --invoices with a postive balance 
         p_comments :='Rebates: Auto apply postive invoices or debit memos';      
      end if;
      --print_message ('OUT', 'Commit Flag :' || p_commit);
      ar_receipt_api_pub.apply
                                 (p_api_version         =>p_api_version,
                                  p_init_msg_list       =>p_init_msg_list,
                                  p_commit              =>p_commit,
                                  p_validation_level    =>p_validation_level,
                                  x_return_status       =>l_return_status,
                                  x_msg_count           =>l_msg_count,
                                  x_msg_data            =>l_msg_data,
                                  p_cash_receipt_id     =>p_cr_id,
                                  p_customer_trx_id     =>p_inv_id,
                                  p_amount_applied      =>p_amt_applied,
                                  p_comments            =>p_comments
                                  --p_customer_reference  =>p_inv_reference,                                  
                                 );
        --                             
        p_ret_status  :=l_return_status;
        --    
      if l_return_status <> 'S' then
            for msg_indx in 1 .. l_msg_count loop
             --        
                Cash_App_Log
                 (
                    p_rebate_type  =>p_reb_type                           --in varchar2
                   ,p_cal_year =>p_year                                                              --in varchar2
                   ,p_offer_code =>p_offer_code --in varchar2
                   ,p_customer_id =>p_cust_id
                   ,p_customer_number =>p_cust_number
                   ,p_customer_name =>p_cust_name                                    
                   ,p_cash_receipt_id =>p_cr_id                              --in number
                   ,p_receipt_number  =>p_rec_number                         --in varchar2
                   ,p_receipt_date    =>p_rec_date                           --in date
                   ,p_receipt_amount  =>p_rec_amount                         --in number
                   ,p_inv_id          =>p_inv_id                             --in number
                   ,p_inv_number      =>p_inv_number                         --in varchar2
                   ,p_invoice_date    =>p_inv_date                           --in date
                   ,p_inv_amount      =>p_inv_balance                        --in number
                   ,p_applied_amount  =>Null                                 --in number
                   ,p_api_status      =>l_return_status                      --in varchar2
                   ,p_api_msg_index   =>msg_indx                             --in number
                   ,p_api_msg         =>substr (fnd_msg_pub.get (p_encoded => fnd_api.g_false), 1, 255) --in varchar2                            
                 );
             --
            end loop;
      else --api returned successful
              --
              l_api_msg :=Null;
              --      
              if (sign(p_inv_balance) =-1) then
                 l_api_msg :='Invoice Amount '||TO_CHAR(p_amt_applied, 'FM$999,999,990.90PR') ||' applied to Receipt  '||p_rec_number||'.';
              else --invoices with a postive balance 
                 l_api_msg :='Receipt  Amount '||TO_CHAR(p_amt_applied, 'FM$999,999,990.90PR')||' applied to Invoice  '||p_inv_number||'.';      
              end if;
              --     
        Cash_App_Log
         (
            p_rebate_type  =>p_reb_type                           --in varchar2
           ,p_cal_year =>p_year                                                              --in varchar2   
           ,p_offer_code =>p_offer_code                            --in varchar2
           ,p_customer_id =>p_cust_id
           ,p_customer_number =>p_cust_number
           ,p_customer_name =>p_cust_name                         
           ,p_cash_receipt_id =>p_cr_id                              --in number
           ,p_receipt_number  =>p_rec_number                         --in varchar2
           ,p_receipt_date    =>p_rec_date                           --in date
           ,p_receipt_amount  =>p_rec_amount                         --in number
           ,p_inv_id          =>p_inv_id                             --in number
           ,p_inv_number      =>p_inv_number                         --in varchar2
           ,p_invoice_date    =>p_inv_date                           --in date
           ,p_inv_amount      =>p_inv_balance                        --in number
           ,p_applied_amount  =>p_amt_applied                        --in number
           ,p_api_status      =>l_return_status                      --in varchar2
           ,p_api_msg_index   =>1                                    --in number
           ,p_api_msg         =>l_api_msg --'Amount '||p_amt_applied||' applied to Invoice  '||p_inv_number||'.'         --in varchar2                            
         );  
       --
      end if;
    exception
     --
      when others then
       --
         print_log ('Location       :apply_cash');
         print_log ('Cash_Receipt_Id/ Customer_Trx_id/ Inv Balance /Amt Applied :'||p_cr_id||'/ '||p_inv_id||'/ '||p_inv_balance||'/ '||p_amt_applied);
         print_log ('SQLCODE        :' || sqlcode);
         print_log ('SQLERRM        :' || sqlerrm);
         rollback to stage_10;
       --
    end apply_cash;    
    --
    -- 07/25/2014
    procedure extract_open_payments_invoices
     (
      p_cust_id in  number
     ) is
     --
     cursor rbt_pmts (l_req_id number) is
        select
        --select /*+ PARALLEL (arps 8) */        
               arps.customer_id                                                   customer_id
              ,to_number(null)                                                     customer_number
              ,to_char(null)                                                            customer_name
              ,arps.trx_number                                                    receipt_number
              ,arps.trx_date                                                           receipt_date
              ,sum(abs(arps.amount_due_remaining))        funds_available
              ,0                                                                                  funds_applied
              ,null                                                                              rebate_type
              ,null                                                                              calendar_year
              ,null                                                                              agreement_code
              ,arps.cash_receipt_id                                             cash_receipt_id
              ,arps.org_id                                                               org_id
              ,l_req_id                                                                      request_id      
        from   ar_payment_schedules arps
        where  1 =1
          and  status           ='OP'                               -- open receipts only
          and  class            ='PMT'                              -- payment type records only
          and  customer_id      =Nvl(p_cust_id, customer_id)        -- a specific customer id if asked for
          and  org_id           =xxcus_ozf_cashapp_pkg.g_org_id     -- Rebates US or Canadian org only
        group by arps.customer_id, arps.trx_number, arps.trx_date, arps.cash_receipt_id, arps.org_id
        having sum(abs(arps.amount_due_remaining)) <>0;                  -- we have some unapplied funds available
     --
     type rbt_pmts_tbl is table of rbt_pmts%rowtype index by binary_integer;
     rbt_pmts_rec rbt_pmts_tbl;
     --
     cursor unique_rcpt_customers is
        select customer_id                  customer_id
              ,customer_number           customer_number
              ,customer_name               customer_name
              ,case
                when rebate_type ='REBATE' then 'REB'
                else rebate_type
               end                                       rebate_type
              ,calendar_year                   calendar_year
              ,agreement_code              agreement_code
        from   xxcus.xxcus_ozf_cashapp_pmts_all
        where  1 =1
          and  request_id =xxcus_ozf_cashapp_pkg.g_request_id
          group by customer_id, customer_number, customer_name,rebate_type,calendar_year,agreement_code;    
        --
     cursor rbt_inv (l_cust_id in number, l_req_id number, l_reb_type in varchar2, l_cal_yr in varchar2, l_agreement_code in varchar2) is
        select --/*+ PARALLEL (arps 8) */
               arps.customer_id                                                customer_id
              ,to_number(null)                                                  customer_number
              ,to_char(null)                                                         customer_name
              ,arps.trx_number                                                 inv_number
              ,arps.trx_date                                                        inv_date
              ,sum(arps.amount_due_remaining)               inv_bal_due
              ,0                                                                               amount_applied
              ,0                                                                               amount_remaining
              ,l_agreement_code                                              agreement_code --01/27/2016            
              ,trx.ct_reference                                                   agreement_name       
              ,substr(trx.ct_reference, 3, 4)                          calendar_year
              ,case 
                when substr(trx.trx_number, 1, 4) ='COOP' then substr(trx.trx_number, 1, 4)
                else substr(trx.trx_number, 1, 3)
               end                                                                            rebate_type
              ,arps.customer_trx_id                                         customer_trx_id
              ,arps.org_id                                                            org_id
              ,'N'                                                                             offer_global_flag
              ,l_req_id                                                                  request_id
        from   ar_payment_schedules arps, ra_customer_trx trx
        where  1 =1
          and  arps.status                                   ='OP'                                -- open invoices only
          and  arps.class                                      ='INV'                              -- payment schedule class of type invoices only
          and  arps.customer_id                       =l_cust_id                       -- a specific customer id
          and  arps.org_id                                   =xxcus_ozf_cashapp_pkg.g_org_id     -- Rebates US and Canadian org only
          and  trx.customer_trx_id                    =arps.customer_trx_id
          and  
                  (
                   ( l_reb_type is not null and l_reb_type ='COOP'  and  substr(trx.trx_number, 1, 4) =l_reb_type) 
                           OR
                   ( l_reb_type is not null and l_reb_type ='REB'  and  substr(trx.trx_number, 1,3) =l_reb_type)
                           --OR
                   --( 3 =3 )
                 )
          and ( (l_cal_yr is not null and substr(trx.ct_reference, 3, 4)   =l_cal_yr) 
                      --OR (2 =2)
                  )
          and 
            ( --3
                     ( --1
                       l_agreement_code is not null and exists
                       (
                          select '1'
                          from ra_customer_trx_lines trxl
                                   ,ozf_claim_lines ozfcl
                                   ,qp_list_headers_vl qpb
                          where 1 =1
                               and trxl.customer_trx_id =trx.customer_trx_id
                               and ozfcl.claim_id =to_number(trxl.interface_line_attribute2)
                               and qpb.list_header_id =ozfcl.activity_id
                               and  qpb.name =l_agreement_code --04/22/2016                               
                               and  nvl(qpb.global_flag, 'N') ='N'
                       )
                     ) --1
                     OR
                     ( --2
                     '2' ='2'
                     ) --2         
            ) --3  
        group by arps.customer_id, arps.trx_number, arps.trx_date, trx.ct_reference, arps.customer_trx_id, arps.org_id,
        case 
                when substr(trx.trx_number, 1, 4) ='COOP' then substr(trx.trx_number, 1, 4)
                else substr(trx.trx_number, 1, 3)
        end
        having sum(abs(arps.amount_due_remaining)) <>0;  
     --
     type rbt_inv_tbl is table of rbt_inv%rowtype index by binary_integer;
     rbt_inv_rec rbt_inv_tbl;
     --     
     b_insert BOOLEAN;
     l_seq number :=0;
     n_count1 number :=0;
     n_count2 number :=0;
     --
    begin
     --
     print_log(' ');
     --     
     --execute immediate 'TRUNCATE TABLE xxcus.xxcus_ozf_cashapp_pmts_all';
     delete xxcus.xxcus_ozf_cashapp_pmts_all where request_id !=xxcus_ozf_cashapp_pkg.g_request_id;
     print_log('Deleted old data from table xxcus.xxcus_ozf_cashapp_pmts_all where request_id !='||xxcus_ozf_cashapp_pkg.g_request_id);
     print_log(' ');     
      
     --execute immediate 'TRUNCATE TABLE xxcus.xxcus_ozf_cashapp_inv_all';     
     delete xxcus.xxcus_ozf_cashapp_inv_all where request_id !=xxcus_ozf_cashapp_pkg.g_request_id;
     print_log('Deleted old data from table xxcus.xxcus_ozf_cashapp_inv_all where request_id !='||xxcus_ozf_cashapp_pkg.g_request_id);     
     
     --execute immediate 'truncate table xxcus.xxcus_ozf_cashapp_pmts_tmp_stg';
     delete xxcus.xxcus_ozf_cashapp_pmts_tmp_stg where request_id !=xxcus_ozf_cashapp_pkg.g_request_id;
     print_log('Deleted old data from table xxcus.xxcus_ozf_cashapp_pmts_tmp_stg where request_id !='||xxcus_ozf_cashapp_pkg.g_request_id);     
     
     --execute immediate 'truncate table xxcus.xxcus_ozf_cashapp_inv_tmp_stg'; 
     delete xxcus.xxcus_ozf_cashapp_inv_tmp_stg where request_id !=xxcus_ozf_cashapp_pkg.g_request_id;
     print_log('Deleted old data from table xxcus.xxcus_ozf_cashapp_inv_tmp_stg where request_id !='||xxcus_ozf_cashapp_pkg.g_request_id);     
     --          
     -- Extract open receipts...      
     --  
     open rbt_pmts 
                 (
                       l_req_id =>xxcus_ozf_cashapp_pkg.g_request_id
                 );
     fetch rbt_pmts bulk collect into rbt_pmts_rec;
     print_log ('@Main: rbt_pmts, Total records fetched ='||rbt_pmts_rec.count);
     close rbt_pmts;
     -- 
     if rbt_pmts_rec.count >0 then     
      --
      begin 
       --
        forall idx in rbt_pmts_rec.first .. rbt_pmts_rec.last
         insert into xxcus.xxcus_ozf_cashapp_pmts_all values rbt_pmts_rec(idx);
         --
        n_count1 :=sql%rowcount;
        print_log('');      
        print_log ('@Main: Receipts, Total records inserted ='||sql%rowcount);
         -- 
         commit;
         --  
         b_insert :=TRUE;       
         --
      exception
       when others then
        print_log ('@Receipts: Error in forall insert into table xxcus.xxcus_ozf_cashapp_pmts_all, msg ='||sqlerrm);
        b_insert :=FALSE;        
      end; 
      --   
     end if; -- rbt_pmts_rec.count >0
     --
     if (b_insert) then --insert into xxcus.xxcus_ozf_cashapp_pmts_all is complete..proceed to extract all attributes required for each payment...
          --
          print_log(' ');
          --
          begin  
            --
            -- We use the below temp table during merge operation as the updated table cannot be used as a source in the select statement...
            --
            print_log('Before insert of xxcus.xxcus_ozf_cashapp_pmts_tmp_stg table...');
            --
            Insert into xxcus.xxcus_ozf_cashapp_pmts_tmp_stg
              (
                 cash_receipt_id
                ,request_id
              )
              (
                 select cash_receipt_id, request_id 
                 from xxcus.xxcus_ozf_cashapp_pmts_all
                 where 1  =1
                       and request_id =xxcus_ozf_cashapp_pkg.g_request_id
              );
            --
            print_log('After insert of xxcus.xxcus_ozf_cashapp_pmts_tmp_stg table, Total records inserted ='||sql%rowcount); 
            --
            commit;
            --
            b_insert :=TRUE;
            --
          exception
            when others then
              print_log('Failed to xxcus.xxcus_ozf_cashapp_pmts_tmp_stg table...'||sqlerrm);
              b_insert :=FALSE;
          end;
          --
          print_log(' ');
           --
          if (b_insert) then  --Insert into  xxcus.xxcus_ozf_cashapp_pmts_tmp_stg table is successful
              --
               begin  
               --
               print_log ('@Receipts: Before update of customer attributes.');
               --               
               savepoint init_here1;
               --
                merge into xxcus.xxcus_ozf_cashapp_pmts_all t
                using (
                        select hzca.account_number   account_number
                              ,hzca.cust_account_id  cust_account_id 
                              ,hzp.party_name        party_name
                        from   hz_cust_accounts hzca
                              ,hz_parties       hzp
                        where  1 =1
                          and  hzp.party_id =hzca.party_id
                          and  exists
                                (
                                  select 1
                                  from   hz_cust_acct_sites_all hzcas
                                  where  1 =1
                                    and  hzcas.cust_account_id =hzca.cust_account_id
                                    and  hzcas.org_id =xxcus_ozf_cashapp_pkg.g_org_id
                                )
                       ) s
                on (t.customer_id = s.cust_account_id)
                when matched then update set customer_number =s.account_number, customer_name =s.party_name;  
                --
                commit;
                --
                b_insert :=TRUE;
                --  
                print_log ('@Receipts: After update of customer attributes.');                       
                --   
               exception
                when others then
                 rollback to init_here1;
                 print_log ('@Receipts: Error in updating customer attributes, Message ='||sqlerrm);
                 print_log (' ');
                 b_insert :=FALSE;
               end;
               --
          else
           print_log('Failed to insert into  xxcus.xxcus_ozf_cashapp_pmts_tmp_stg table is successful');
          end if; -- Insert into  xxcus.xxcus_ozf_cashapp_pmts_tmp_stg table is successful
          --   
           if (b_insert) then --merge of all customer attributes at the payment level is complete...
                   --
                   begin 
                     --
                      print_log(''); 
                      print_log ('@Receipts: Before update of receipt attributes calendar_year and rebate_type.'); 
                    --        
                    savepoint init_here2;
                    --
                     merge into xxcus.xxcus_ozf_cashapp_pmts_all t1
                     using (
                            select regexp_replace(cr.attribute10, '[[:cntrl:]]', ' ')       pmt_rebate_type
                                       ,regexp_replace(cr.attribute11, '[[:cntrl:]]', ' ')       pmt_cal_year
                                       ,regexp_replace(cr.attribute12, '[[:cntrl:]]', ' ')       pmt_offer_code   --01/27/2016, agreement_code                            
                                       ,cr.cash_receipt_id     cr_id 
                            from   ar_cash_receipts_all cr
                            where  1 =1
                              and  exists
                               (
                                  select 1
                                  from   xxcus.xxcus_ozf_cashapp_pmts_tmp_stg stg
                                  where  1 =1
                                       and  stg.cash_receipt_id     =cr.cash_receipt_id
                                       and  stg.request_id              =xxcus_ozf_cashapp_pkg.g_request_id
                               )
                           ) s
                     on (t1.cash_receipt_id = s.cr_id)
                     when matched then update set t1.rebate_type =s.pmt_rebate_type,  t1.calendar_year =s.pmt_cal_year
                                                                                 ,t1.agreement_code =s.pmt_offer_code;   --01/27/2016
                      --
                      commit;
                      --
                      b_insert :=TRUE;
                      --
                      print_log('@Receipts: After update of receipt attributes calendar_year and rebate_type.'); 
                      --           
                   exception
                    when others then
                     print_log ('@Receipts: Error in updating receipt attributes, Message ='||sqlerrm);
                     rollback to init_here2;
                     b_insert :=FALSE;
                   end;      
                  --  
           end if; --merge of all customer attributes at the payment level is complete...
           --         
           print_log ('');
           --        
     else
       print_log('@Receipts, Forall insert failed so no records to work with, exit');      
     end if; --insert into xxcus.xxcus_ozf_cashapp_pmts_all is complete..move to extract all attributes required for each payment...
     --
     -- remove all payments where agreement year or rebate type is blank
     --
     begin 
      savepoint sqr1;
      delete  xxcus.xxcus_ozf_cashapp_pmts_all
      where 1 =1
            and (rebate_type is null OR calendar_year is null);
      n_count2 :=sql%rowcount;            
      print_log('');                  
      print_log('Total payments removed where rebate type is blank or calendar year is blank =>'||sql%rowcount);
      print_log('');
      print_log('******** Total valid payments with rebate type and calendar year =>'||(n_count1 - n_count2));
      print_log('');              
     exception
      when others then
       rollback to sqr1;
       print_log('');
     end;
     --
     commit;
     --
     -- Extract open invoices...      
     --  
      if (b_insert) then --all levels of payment info extract is complete...start extracting open invoices...
         --
         l_seq :=0;
         --
         -- 02/15/2016
         begin 
          --
          Null;
          --
            for rec in unique_rcpt_customers loop 
             --
             print_log ('@Main: rbt_inv, Extract invoices for customer_id ='||rec.customer_id||', customer_name :'||rec.customer_name);
             --
                       rbt_inv_rec.delete;
             --
                 open rbt_inv
                         (
                               l_cust_id =>rec.customer_id 
                              ,l_req_id =>xxcus_ozf_cashapp_pkg.g_request_id
                             , l_reb_type =>rec.rebate_type --xxcus_ozf_cashapp_pkg.g_inv_rbt_type
                             , l_cal_yr =>rec.calendar_year --xxcus_ozf_cashapp_pkg.g_calendar_year
                             ,l_agreement_code =>rec.agreement_code --xxcus_ozf_cashapp_pkg.g_agreement_code
                         );
                 fetch rbt_inv bulk collect into rbt_inv_rec;
                 --print_log ('  ----@Main: rbt_inv, Total records fetched ='||rbt_inv_rec.count);
                 close rbt_inv;
                 -- 
                 if rbt_inv_rec.count >0 then --cursor rbt_inv has 1 or more invoices...move further....      
                      --
                      begin 
                       --
                        forall idx in rbt_inv_rec.first .. rbt_inv_rec.last
                         insert into xxcus.xxcus_ozf_cashapp_inv_all values rbt_inv_rec(idx);
                       --
                       print_log (' ----@Main: Invoices, Total records inserted for customer_id '||rec.customer_id||' ='||rbt_inv_rec.count);
                       -- 
                       commit;
                       --
                       b_insert :=TRUE;       
                       --
                      exception
                       when others then
                        print_log ('@Invoices: Error in forall insert into table xxcus.xxcus_ozf_cashapp_inv_all, msg ='||sqlerrm);
                        b_insert :=FALSE;        
                      end; 
                      --   
                 end if; -- if rbt_inv_rec.count >0  
                 --            
            end loop;  
                 --
                 if (b_insert) then --extracted open invoices into temp staging table xxcus.xxcus_ozf_cashapp_inv_all
                     --
                      begin  
                        --
                        -- We use the below temp table during merge operation as the updated table cannot be used as a source in the select statement...
                        --
                        print_log('');
                        print_log('Before insert of xxcus.xxcus_ozf_cashapp_inv_tmp_stg table...');
                        --
                        Insert into xxcus.xxcus_ozf_cashapp_inv_tmp_stg
                          (
                             customer_trx_id
                            ,request_id
                          )
                          (
                             select customer_trx_id
                                        ,request_id 
                             from xxcus.xxcus_ozf_cashapp_inv_all
                             where 1  =1
                                   and request_id =xxcus_ozf_cashapp_pkg.g_request_id
                                   /*
                                   and customer_id =rec.customer_id --04/22/2016
                                   and rebate_type =rec.rebate_type --04/22/2016
                                   and calendar_year =rec.calendar_year --04/22/2016
                                   and ((rec.agreement_code is not null and agreement_code rec.agreement_code) OR (2=2)) --04/22/2016
                                 */
                          );
                        --
                        print_log('After insert of xxcus.xxcus_ozf_cashapp_inv_tmp_stg table, Total records inserted ='||sql%rowcount); 
                        --
                        commit;
                        --
                        b_insert :=TRUE;
                        --
                      exception
                        when others then
                          print_log('Failed to  xxcus.xxcus_ozf_cashapp_inv_tmp_stg table...'||sqlerrm);
                          b_insert :=FALSE;
                      end;
                      --         
                 end if; --
                  --
                 if (b_insert) then -- table xxcus.xxcus_ozf_cashapp_inv_tmp_stg is extracted successfully...
                  --
                  print_log (' ');
                  --
                   begin   
                    --
                    print_log ('@Invoices: Before update of customer attributes.');           
                    --
                    savepoint init_here1;
                    --
                    merge into xxcus.xxcus_ozf_cashapp_inv_all t
                    using (
                            select hzca.account_number   account_number
                                  ,hzca.cust_account_id  cust_account_id 
                                  ,hzp.party_name        party_name
                            from   hz_cust_accounts hzca
                                  ,hz_parties       hzp
                            where  1 =1
                              and  hzp.party_id =hzca.party_id
                              and  exists
                                    (
                                      select 1
                                      from   hz_cust_acct_sites_all hzcas
                                      where  1 =1
                                        and  hzcas.cust_account_id =hzca.cust_account_id
                                        and  hzcas.org_id =xxcus_ozf_cashapp_pkg.g_org_id
                                    )
                           ) s
                    on (t.customer_id = s.cust_account_id)
                    when matched then update set customer_number =s.account_number, customer_name =s.party_name;        
                      --
                     commit;
                     --
                     print_log ('@Invoices: After update of customer attributes.');
                     --
                     b_insert :=TRUE;
                     --                                 
                   exception
                    when others then
                     rollback to init_here1;
                     print_log ('@Invoices: Error in updating customer attributes, Message ='||sqlerrm);
                     b_insert :=FALSE;
                   end;
                   --  
                 else
                   print_log('@Invoices: Forall insert failed so no records to work with, exit');      
                 end if; -- table xxcus.xxcus_ozf_cashapp_inv_all is extracted successfully..
                 --                    
         exception
          when no_data_found then
           print_log('No customer found with open payments.');
         end;
         -- 02/15/2016
         --   
     end if; 
     --open payments extracted successfully
    exception
     when others then
      print_log ('@API extract_open_payments_invoices, msg ='||sqlerrm);
      rollback;
    end extract_open_payments_invoices;
    --
    function beforereport return boolean as     
     --
     cursor c_customers is 
        select customer_id                 customer_id
              ,customer_number          customer_number
              ,customer_name               customer_name
              ,sum(nvl(funds_available, 0))     cust_funds_avbl
              ,0                                            cust_funds_applied
        from   xxcus.xxcus_ozf_cashapp_pmts_all
        where  1 =1
          and  request_id =xxcus_ozf_cashapp_pkg.g_request_id
        group by customer_id, customer_number, customer_name;
     --
     type t_customers is table of c_customers%rowtype index by binary_integer;
     cust_rec t_customers;        
     --
     cursor c_receipts (p_customer_id in number) is
        select nvl(funds_available, 0) rcpt_funds_avbl
              ,0                                      rcpt_funds_applied
              ,receipt_number          receipt_number
              ,receipt_date                 receipt_date
              ,calendar_year              rcpt_cal_year
              ,case
                when rebate_type ='REBATE' then 'REB' 
                else rebate_type
               end                                  rcpt_rbt_type
              ,agreement_code         rcpt_offer_code
              ,cash_receipt_id           cash_receipt_id
              ,customer_id                 customer_id              
        from   xxcus.xxcus_ozf_cashapp_pmts_all
        where  1 =1
          and  request_id    =xxcus_ozf_cashapp_pkg.g_request_id
          and  customer_id =p_customer_id
        order by receipt_date asc, nvl(funds_available, 0) desc;      --If planning to alter order by clause, check with rebates team before making any change...  
     --
     type t_receipts is table of c_receipts%rowtype index by binary_integer;
     rcpt_rec t_receipts; 
     --
     cursor c_positive_invoices (p_customer_id in number, p_year in varchar2, p_type in varchar2, p_offer_code in varchar2) is
        select customer_trx_id      inv_id
              ,inv_number                   inv_number
              ,inv_date                          inv_date
              ,nvl(inv_bal_due, 0)      inv_current_bal
              ,0                                        inv_amt_applied                  
              ,calendar_year               inv_cal_year
              ,rebate_type                   inv_rbt_type
              ,offer_global_flag          offer_global_flag
              ,agreement_code          offer_code
              ,agreement_name        offer_name
              ,rowid                               row_id
        from   xxcus.xxcus_ozf_cashapp_inv_all
        where  1 =1
          and  request_id        =xxcus_ozf_cashapp_pkg.g_request_id 
          and  customer_id    =p_customer_id
          and  calendar_year =p_year
          and  rebate_type     =p_type
          and  (
                       (p_offer_code is not null and agreement_code =p_offer_code) OR
                       (p_offer_code is null and 2 =2)
                   )          
          --and  agreement_code =nvl(p_offer_code, agreement_code)
          and  ( sign(inv_bal_due) =1 ) --Pull only invoices with a postive balance
        order by inv_date asc, inv_bal_due desc;
     --
     cursor c_negative_invoices (p_customer_id in number, p_year in varchar2, p_type in varchar2, p_offer_code in varchar2) is
        select customer_trx_id      inv_id
              ,inv_number                   inv_number
              ,inv_date                          inv_date
              ,nvl(inv_bal_due, 0)      inv_current_bal
              ,0                                        inv_amt_applied                  
              ,calendar_year               inv_cal_year
              ,rebate_type                   inv_rbt_type
              ,offer_global_flag          offer_global_flag
              ,agreement_code          offer_code
              ,agreement_name        offer_name
              ,rowid                               row_id
        from   xxcus.xxcus_ozf_cashapp_inv_all
        where  1 =1
          and  request_id      =xxcus_ozf_cashapp_pkg.g_request_id 
          and  customer_id   =p_customer_id
          and  calendar_year =p_year
          and  rebate_type     =p_type
          and  (
                       (p_offer_code is not null and agreement_code =p_offer_code) OR
                       (p_offer_code is null and 2 =2)
                   )           
          --and  agreement_code =nvl(p_offer_code, agreement_code)          
          and  ( sign(inv_bal_due) = -1 ) --Pull only invoices with a negative balance
        order by inv_date asc, inv_bal_due asc;             
     --
     type t_invoices is table of c_positive_invoices%rowtype index by binary_integer;
     inv_rec t_invoices; 
     --           
      l_move_fwd  boolean;
      l_commit    varchar2(1) :=Null;
      --
      l_receipts  boolean :=Null;
      l_invoices  boolean :=Null;
      l_go_thru   boolean :=Null;
      l_continue boolean :=Null;
     --
      l_amount_applied Number :=Null;
     --
      l_ret_status varchar2(1) :=Null; 
      l_api_msg    varchar2(255) :=Null;          
     --
    begin 
      --
      print_log('');
      print_log('Parameters:');
      print_log('===========');                
      print_log('g_calendar_year........=>'||xxcus_ozf_cashapp_pkg.g_calendar_year);
      print_log('g_type.......................=>'||xxcus_ozf_cashapp_pkg.g_type);
      --
      print_log('g_agreement_code....=>'||xxcus_ozf_cashapp_pkg.g_agreement_code);      
      print_log('g_customer_id...........=>'||xxcus_ozf_cashapp_pkg.g_customer_id);
      --      
      print_log('g_mode......................=>'||xxcus_ozf_cashapp_pkg.g_mode);
      print_log('g_flush_stg_info.........=>'||xxcus_ozf_cashapp_pkg.g_flush_stg_info);
      print_log('');
      --
      xxcus_ozf_cashapp_pkg.g_user_id :=apps.fnd_global.user_id;
      --
      if  (xxcus_ozf_cashapp_pkg.g_type) ='COOP' then
        --
        xxcus_ozf_cashapp_pkg.g_inv_rbt_type :='COOP';
        --
      else
        --
        xxcus_ozf_cashapp_pkg.g_inv_rbt_type :='REB'; --we are not assigning value "REBATE" because the invoice numbers do not have "REBATE" as the starting value
        --      
      end if;
      --
       xxcus_ozf_cashapp_pkg.g_request_id :=fnd_global.conc_request_id;
       --
      -- Set the operating unit to package variable g_org_id
      --
      xxcus_ozf_cashapp_pkg.g_org_id :=mo_global.get_current_org_id;
      --            
      print_log('Current Org Id  ='||xxcus_ozf_cashapp_pkg.g_org_id);
      print_log('');
      --
      --EXECUTE IMMEDIATE 'DELETE xxcus.xxcus_ozf_cashapp_log where request_id !='||xxcus_ozf_cashapp_pkg.g_request_id;
      --print_log ('All prior submissions data removed from custom table xxcus.xxcus_ozf_cashapp_log.');                
      --
      begin
        apps.xxcus_ozf_cashapp_pkg.extract_open_payments_invoices
          (
            p_cust_id =>xxcus_ozf_cashapp_pkg.g_customer_id
          );
         l_go_thru :=TRUE;
      exception
       when others then
        print_log('Error in xxcus_ozf_cashapp_pkg.extract_open_payments_invoices, msg ='||sqlerrm);
        l_go_thru :=FALSE;      
      end;      
      -- 01/27/2016
      --  
      if (NOT l_go_thru) then
       print_log ('Routine xxcus_ozf_cashapp_pkg.extract_open_payments_invoices failed, exit cash app stage.');      
      else
          /*
           g_mode is either DRAFT or FINAL
           DRAFT =>review the results from cash app process and rollback the changes.
           FINAL  =>perform cash application and save the work.          
          */
          --
          if upper(xxcus_ozf_cashapp_pkg.g_mode) =xxcus_ozf_cashapp_pkg.g_draft then    
              l_commit :=fnd_api.g_false;
          elsif upper(xxcus_ozf_cashapp_pkg.g_mode) =xxcus_ozf_cashapp_pkg.g_final then     
              l_commit :=fnd_api.g_true;
          else
              l_commit :=fnd_api.g_false;
          end if;
         --       
          open  c_customers;
          fetch c_customers bulk collect into cust_rec; 
          close c_customers;
          --
         if cust_rec.count >0 then --Customers are fetched along with the total funds available for cash app process...
          --
          print_log('');
          print_log ('cust_rec.count ='||cust_rec.count);
          --
          for cidx in cust_rec.first .. cust_rec.last loop 
               --           
               -- fetch all receipts for a customer 
               --           
              print_log ('MVID :'||cust_rec(cidx).customer_name||' ['||cust_rec(cidx).customer_number||']'||', Customer funds available ='||cust_rec(cidx).cust_funds_avbl);
                --
                begin
                rcpt_rec.delete;
               exception
                when others then
                 print_log ('@Negative Invoices: Failed to clean up existing receipts plsql table data for customer id ='||cust_rec(cidx).customer_id||', msg ='||sqlerrm);
               end;
                --
               begin
                inv_rec.delete;
               exception
                when others then
                 print_log ('@Negative Invoices: Failed to clean up existing invoices plsql table data for customer id ='||cust_rec(cidx).customer_id||', msg ='||sqlerrm);
               end;
                --           
                begin 
                --
                  open c_receipts (p_customer_id =>cust_rec(cidx).customer_id);
                  fetch c_receipts bulk collect into rcpt_rec;
                  close c_receipts;
                 --        
                exception
                 --
                 when no_data_found then
                  print_log ('@Negative Invoices: No receipts found for customer_id '||cust_rec(cidx).customer_id);
                 when others then
                  print_log ('@Negative Invoices: Other errors when fetching receipts for customer_id '||cust_rec(cidx).customer_id||', msg ='||sqlerrm);
                 --
                end;
                --
                if rcpt_rec.count >0 then
                 l_receipts :=TRUE;
                 print_log ('@Negative Invoices: Total receipts for customer_id '||cust_rec(cidx).customer_id||', name ='||cust_rec(cidx).customer_name||', count ='||rcpt_rec.count);
                else
                 l_receipts :=FALSE;
                 print_log ('@Negative Invoices: Failed to extract receipts for customer_id '||cust_rec(cidx).customer_id||', plsql table rcpt_rec.count ='||rcpt_rec.count);
                end if;
                --
                if (NOT l_receipts) then
                     --
                     print_log ('No payments for customer.., exit MVID :'||cust_rec(cidx).customer_name||', total receipts ='||rcpt_rec.count);
                     --                  
                else
                         for r_idx in rcpt_rec.first .. rcpt_rec.last loop  --loop thru receipts plsql table 
                          --
                          print_log('');
                          print_log('Begin Cash_Receipt_Id ='||rcpt_rec(r_idx).cash_receipt_id||', Receipt# :'||rcpt_rec(r_idx).receipt_number||', funds available ='||rcpt_rec(r_idx).rcpt_funds_avbl);
                              --
                              -- 03/02/2016
                              -- ****************************************
                                -- fetch all negative invoices for a customer
                                --
                                begin 
                                  --   
                                  -- Need to clear the invoice plsql table first before populating with negative invoices                 
                                  begin
                                   inv_rec.delete;
                                  exception
                                   when others then
                                     print_log('@Negative Invoices:  Before starting negative invoices: error in cleaning plsql table inv_rec, msg ='||sqlerrm);                                                             
                                  end;
                                  --                                          
                                  open c_negative_invoices 
                                     (
                                        p_customer_id =>rcpt_rec(r_idx).customer_id 
                                       ,p_year =>rcpt_rec(r_idx).rcpt_cal_year                       
                                       ,p_type =>rcpt_rec(r_idx).rcpt_rbt_type
                                       ,p_offer_code =>rcpt_rec(r_idx).rcpt_offer_code
                                     );
                                  fetch c_negative_invoices bulk collect into inv_rec;
                                  close c_negative_invoices;
                                 --        
                                exception
                                 --
                                 when no_data_found then
                                  print_log ('@Negative Invoices: No invoices found for customer_id '||cust_rec(cidx).customer_id);
                                 when others then
                                  print_log ('@Negative Invoices: Other errors when fetching negative invoices for customer_id '||cust_rec(cidx).customer_id||', msg ='||sqlerrm);
                                 --
                                end;
                                --
                                if inv_rec.count >0 then
                                 l_invoices :=TRUE;
                                 print_log ('@Negative Invoices:Total negative invoices for customer_id '||cust_rec(cidx).customer_id||', name ='||cust_rec(cidx).customer_name||', count ='||inv_rec.count);
                                else
                                 l_invoices :=FALSE;
                                 print_log ('@Negative Invoices: Failed to extract negative invoices for customer_id '||cust_rec(cidx).customer_id||', plsql table inv_rec.count ='||inv_rec.count);
                                end if;
                                --                              
                              -- ****************************************
                            if (NOT l_invoices) then 
                             --
                             print_log ('@Negative Invoices: No records found, exit MVID :'||cust_rec(cidx).customer_name||', total records ='||inv_rec.count);
                             --
                            else
                              --
                              print_log('');  
                              print_log('******* Begin Negative Invoices to Payments - Cash Application Process ***********'); 
                              --
                              --
                              for i_idx in inv_rec.first .. inv_rec.last loop  --loop thru negative invoices plsql table 
                                --
                                    if  (inv_rec(i_idx).inv_current_bal >=0) then
                                     l_move_fwd :=FALSE;
                                     print_log('@ Invoice Number '||inv_rec(i_idx).inv_number||', l_move_fwd is FALSE...current balance is not negative');                 
                                    else
                                     --
                                     l_move_fwd :=TRUE;  
                                     print_log('@ Invoice Number '||inv_rec(i_idx).inv_number||', l_move_fwd is TRUE, current balance is negative, $'||inv_rec(i_idx).inv_current_bal);                                        
                                     --           
                                    end if;
                                    --
                                    if (l_move_fwd) then
                                         -- clear the api return status variable 
                                         --
                                         l_ret_status :=Null;
                                         --
                                         l_amount_applied :=inv_rec(i_idx).inv_current_bal;
                                         --
                                         apply_cash
                                          (
                                            p_commit                =>l_commit,
                                            p_cust_id                 =>cust_rec(cidx).customer_id,
                                            p_cust_number     =>cust_rec(cidx).customer_number,
                                            p_cust_name          =>cust_rec(cidx).customer_name,
                                            p_cr_id                    =>rcpt_rec(r_idx).cash_receipt_id,
                                            p_rec_number      =>rcpt_rec(r_idx).receipt_number,
                                            p_rec_date             =>rcpt_rec(r_idx).receipt_date,
                                            p_rec_amount       =>rcpt_rec(r_idx).rcpt_funds_avbl,      
                                            p_inv_id                   =>inv_rec(i_idx).inv_id,
                                            p_inv_number       =>inv_rec(i_idx).inv_number,
                                            p_inv_date              =>inv_rec(i_idx).inv_date,
                                            p_inv_balance        =>inv_rec(i_idx).inv_current_bal,
                                            p_amt_applied       =>l_amount_applied,      
                                            p_ret_status           =>l_ret_status, 
                                            p_reb_type             =>inv_rec(i_idx).inv_rbt_type,
                                            p_year                      =>inv_rec(i_idx).inv_cal_year,
                                            p_offer_code          =>inv_rec(i_idx).offer_code
                                          );
                                         --
                                         if l_ret_status ='S' then
                                          --
                                          print_log('@ Negative Invoice: API return status =>S, amount '||l_amount_applied||' applied to Invoice '||inv_rec(i_idx).inv_number); 
                                          -- reset the invoice level plsql table amount fields
                                           inv_rec(i_idx).inv_amt_applied      :=inv_rec(i_idx).inv_amt_applied + l_amount_applied;
                                           inv_rec(i_idx).inv_current_bal    :=inv_rec(i_idx).inv_current_bal - inv_rec(i_idx).inv_amt_applied;
                                          --
                                          -- reset the receipt level plsql table amount fields
                                           rcpt_rec(r_idx).rcpt_funds_avbl     :=rcpt_rec(r_idx).rcpt_funds_avbl - inv_rec(i_idx).inv_amt_applied;
                                           print_log('@Negative Invoices: rcpt_rec(r_idx).rcpt_funds_avbl  ='||rcpt_rec(r_idx).rcpt_funds_avbl );
                                           rcpt_rec(r_idx).rcpt_funds_applied  :=rcpt_rec(r_idx).rcpt_funds_applied +inv_rec(i_idx).inv_amt_applied;
                                          --
                                          -- reset the customer level plsql table amount fields
                                           cust_rec(cidx).cust_funds_avbl      :=cust_rec(cidx).cust_funds_avbl -inv_rec(i_idx).inv_amt_applied;
                                           cust_rec(cidx).cust_funds_applied   :=cust_rec(cidx).cust_funds_applied + inv_rec(i_idx).inv_amt_applied;  
                                            --
                                            -- for positive invoices keep the balance due updated all the time...
                                            --  
                                              begin
                                               savepoint sqr2;
                                               update xxcus.xxcus_ozf_cashapp_inv_all
                                                       set inv_bal_due =inv_rec(i_idx).inv_current_bal
                                               where rowid =inv_rec(i_idx).row_id;
                                              exception
                                               when others then
                                                rollback to sqr2;
                                                fnd_file.put_line(fnd_file.log, 'Issue in updating balance for invoice rowid '||inv_rec(i_idx).row_id||', msg ='||sqlerrm);
                                              end;
                                          --            
                                         else 
                                          --
                                          print_log('@ Negative Invoice: API return status =>'||l_ret_status||', failed to apply amount '||l_amount_applied||' to '||inv_rec(i_idx).inv_number);                                          
                                          -- Cash application failed, let's exit the current invoice and move on to the next
                                          --
                                           inv_rec(i_idx).inv_amt_applied      :=inv_rec(i_idx).inv_amt_applied + 0;
                                           rcpt_rec(r_idx).rcpt_funds_applied  :=rcpt_rec(r_idx).rcpt_funds_applied + 0;
                                           cust_rec(cidx).cust_funds_applied   :=cust_rec(cidx).cust_funds_applied + 0;          
                                          --
                                          -- No need to call Cash_App_Log here as we will invoke it within apply_cash routine
                                          --             
                                         end if; --if o_ret_status ='S' then
                                     -- we didn't use else here bcoz the else condition was already logged into the exception table, check comment @101.001
                                     --            
                                    end if;
                                   --
                              end loop;  --loop thru invoices plsql table 
                              --        
                              print_log('');  
                              print_log('******* End Negative Invoices to Payments Cash Application Process ***********'); 
                              --                                                        
                            end if;  -- if (NOT l_invoices)  then --done with negative invoices                          
                              -- 03/02/2016
                              -- next for review
                              -- Need to clear the invoice plsql table first before populating with positive invoices                 
                              begin 
                               inv_rec.delete;
                              exception
                               when others then
                                 print_log('@Positive Invoices:  Before starting positive invoices: error in cleaning plsql table inv_rec populated with negative invoice, msg ='||sqlerrm);                                                             
                              end;
                              --    
                              -- fetch all positive invoices for a customer
                                --
                                begin 
                                 --         
                                  open c_positive_invoices 
                                     (
                                        p_customer_id =>rcpt_rec(r_idx).customer_id 
                                       ,p_year =>rcpt_rec(r_idx).rcpt_cal_year                       
                                       ,p_type =>rcpt_rec(r_idx).rcpt_rbt_type
                                       ,p_offer_code =>rcpt_rec(r_idx).rcpt_offer_code
                                     );
                                  fetch c_positive_invoices bulk collect into inv_rec;
                                  close c_positive_invoices;
                                 --        
                                exception
                                 --
                                 when no_data_found then
                                  print_log ('No positive invoices found for customer_id '||cust_rec(cidx).customer_id);
                                 when others then
                                  print_log ('Other errors when fetching postive invoices for customer_id '||cust_rec(cidx).customer_id||', msg ='||sqlerrm);
                                 --
                                end;
                                --
                                if inv_rec.count >0 then
                                 l_invoices :=TRUE;
                                 print_log ('Total positive invoices for customer_id '||cust_rec(cidx).customer_id||', name ='||cust_rec(cidx).customer_name||', count ='||inv_rec.count);
                                else
                                 l_invoices :=FALSE;
                                 print_log ('Failed to extract positive invoices for customer_id '||cust_rec(cidx).customer_id||', plsql table inv_rec.count ='||inv_rec.count);
                                end if;
                                --                              
                            if (NOT l_invoices) then 
                             --
                             print_log ('No positive invoices exists.., exit MVID '||cust_rec(cidx).customer_name||', total invoices ='||inv_rec.count);
                             --
                            else
                                  print_log('');  
                                  print_log('******* Begin Positive Invoices to Payments - Cash Application Process ***********'); 
                                  -- 
                                          print_log('');
                                          print_log('Begin Cash_Receipt_Id ='||rcpt_rec(r_idx).cash_receipt_id||', Receipt# :'||rcpt_rec(r_idx).receipt_number||', funds available ='||rcpt_rec(r_idx).rcpt_funds_avbl);
                                --
                               for i_idx in inv_rec.first .. inv_rec.last loop -- loop thru positive invoices
                                  --
                                    if  (inv_rec(i_idx).inv_current_bal >0 AND rcpt_rec(r_idx).rcpt_funds_avbl >0 ) then
                                     l_move_fwd :=TRUE;
                                   else
                                     l_move_fwd :=FALSE;
                                   end if; 
                                 -- 
                                    if  (l_move_fwd ) then  -- we've some balance left in the invoice and payment
                                       --
                                        if (inv_rec(i_idx).inv_current_bal <=rcpt_rec(r_idx).rcpt_funds_avbl) then
                                         --
                                         -- Invoice amount is less than or equal to the receipt amount, just apply the invoice amount 
                                         --
                                         l_amount_applied :=inv_rec(i_idx).inv_current_bal;
                                         --
                                        else
                                         --
                                         -- Invoice amount is more than the receipt unapplied amount, just take whatever is left from the receipt and apply to the invoice
                                         l_amount_applied :=rcpt_rec(r_idx).rcpt_funds_avbl;
                                         --
                                        end if; --end if (inv_rec(i_idx).inv_current_bal <=rcpt_rec(r_idx).rcpt_funds_avbl) then
                                     --
                                     l_ret_status :=Null;
                                     --
                                     apply_cash
                                      (
                                        p_commit                =>l_commit,
                                        p_cust_id                 =>cust_rec(cidx).customer_id,
                                        p_cust_number     =>cust_rec(cidx).customer_number,
                                        p_cust_name          =>cust_rec(cidx).customer_name,
                                        p_cr_id                    =>rcpt_rec(r_idx).cash_receipt_id,
                                        p_rec_number      =>rcpt_rec(r_idx).receipt_number,
                                        p_rec_date             =>rcpt_rec(r_idx).receipt_date,
                                        p_rec_amount       =>rcpt_rec(r_idx).rcpt_funds_avbl,      
                                        p_inv_id                   =>inv_rec(i_idx).inv_id,
                                        p_inv_number       =>inv_rec(i_idx).inv_number,
                                        p_inv_date              =>inv_rec(i_idx).inv_date,
                                        p_inv_balance        =>inv_rec(i_idx).inv_current_bal,
                                        p_amt_applied       =>l_amount_applied,      
                                        p_ret_status           =>l_ret_status, 
                                        p_reb_type             =>inv_rec(i_idx).inv_rbt_type,
                                        p_year                      =>inv_rec(i_idx).inv_cal_year,
                                        p_offer_code          =>inv_rec(i_idx).offer_code
                                      );   
                                      --
                                     if l_ret_status ='S' then
                                      --
                                      print_log('@ Positive Invoice: API return status =>S, amount '||l_amount_applied||' applied to Invoice '||inv_rec(i_idx).inv_number); 
                                      -- reset the invoice level plsql table amount fields
                                       inv_rec(i_idx).inv_amt_applied      :=inv_rec(i_idx).inv_amt_applied + l_amount_applied;
                                       inv_rec(i_idx).inv_current_bal    :=inv_rec(i_idx).inv_current_bal - inv_rec(i_idx).inv_amt_applied;
                                      --
                                      -- reset the receipt level plsql table amount fields
                                       rcpt_rec(r_idx).rcpt_funds_avbl     :=rcpt_rec(r_idx).rcpt_funds_avbl - inv_rec(i_idx).inv_amt_applied;
                                       print_log('@ Positive Invoices: rcpt_rec(r_idx).rcpt_funds_avbl  ='||rcpt_rec(r_idx).rcpt_funds_avbl );
                                       rcpt_rec(r_idx).rcpt_funds_applied  :=rcpt_rec(r_idx).rcpt_funds_applied +inv_rec(i_idx).inv_amt_applied;
                                      --
                                      -- reset the customer level plsql table amount fields
                                       cust_rec(cidx).cust_funds_avbl      :=cust_rec(cidx).cust_funds_avbl -inv_rec(i_idx).inv_amt_applied;
                                       cust_rec(cidx).cust_funds_applied   :=cust_rec(cidx).cust_funds_applied + inv_rec(i_idx).inv_amt_applied;  
                                        --
                                        -- for positive invoices keep the balance due updated all the time...
                                        --  
                                          begin
                                           savepoint sqr2;
                                           update xxcus.xxcus_ozf_cashapp_inv_all
                                                   set inv_bal_due =inv_rec(i_idx).inv_current_bal
                                           where rowid =inv_rec(i_idx).row_id;
                                          exception
                                           when others then
                                            rollback to sqr2;
                                            fnd_file.put_line(fnd_file.log, 'Issue in updating balance for invoice rowid '||inv_rec(i_idx).row_id||', msg ='||sqlerrm);
                                          end;
                                      --     
                                     else 
                                      --
                                      print_log('@ Positive Invoice: API return status =>'||l_ret_status||', failed to apply amount '||l_amount_applied||' to '||inv_rec(i_idx).inv_number);                                          
                                      -- Cash application failed, let's exit the current invoice and move on to the next
                                      --
                                       inv_rec(i_idx).inv_amt_applied      :=inv_rec(i_idx).inv_amt_applied + 0;
                                       rcpt_rec(r_idx).rcpt_funds_applied  :=rcpt_rec(r_idx).rcpt_funds_applied + 0;
                                       cust_rec(cidx).cust_funds_applied   :=cust_rec(cidx).cust_funds_applied + 0;          
                                      --        
                                     end if; --if l_ret_status ='S' then                                          
                                      --                                           
                                   else
                                    print_log('@ Positive Invoice: either invoice or receipt amount is zero, Invoice id : '||inv_rec(i_idx).inv_id||', receipt id :'|| rcpt_rec(r_idx).cash_receipt_id);
                                   end if;
                                   --                                 
                               end loop; -- end loop thru positive invoices
                               --
                               -- End cash app process for postiive invoices
                               --
                            end if;
                              -- 03/02/2016
                              --                              
                         end loop; --loop thru receipts plsql table 
                   -- 
                end if;
                --
          end loop; -- for cust_idx in cust_rec.first .. cust_rec.last loop
          --
         else
          print_log ('Failed to extract customers, plsql table cust_rec.count ='||cust_rec.count);
         end if; -- if cust_rec.count >0 then
         --
          if (upper(xxcus_ozf_cashapp_pkg.g_mode) =xxcus_ozf_cashapp_pkg.g_draft) then
           --
           -- Rollback everything because the process was ran in DRAFT mode.
           --       
            ROLLBACK; 
            print_log('');
            print_log ('************* All changes are reversed because the process was ran in DRAFT mode.');
            print_log('');            
           --
          else
           --
           Commit;
            print_log('');
            print_log ('************* All changes are saved completely as the process was ran in FINAL mode.');
            print_log('');            
           --
          end if; -- end if for (upper(xxcus_ozf_cashapp_pkg.p_mode) =xxcus_ozf_cashapp_pkg.g_draft)
          --
      end if;
      --    
      print_log('');  
      print_log('******* End Positive Invoices to Payments Cash Application Process ***********'); 
      --      
      -- 01/27/2016
      return TRUE;
      --
    exception
     --
     when others then
      --
      print_log('Error in xxcus_ozf_cashapp_pkg.beforereport '||sqlerrm);
      return FALSE;
      --
    end beforereport;
    --    
    function afterreport return boolean as  
    --
    l_created_by NUMBER :=APPS.FND_GLOBAL.USER_ID;
    l_request_id NUMBER :=APPS.FND_GLOBAL.CONC_REQUEST_ID;
    l_final VARCHAR2(10) :=xxcus_ozf_cashapp_pkg.g_final;
    --
    begin     
     --
     if (upper(xxcus_ozf_cashapp_pkg.g_mode) =xxcus_ozf_cashapp_pkg.g_final) then
           Insert into xxcus.xxcus_ozf_cashapp_log_archive
           (
              request_id,
              created_by,
              run_date,
              run_mode,
              rebate_type,
              calendar_year,
              agreement,
              customer_id,  
              customer_number,
              customer_name,
              cash_receipt_id,  
              receipt_number,
              receipt_date,
              receipt_amount,
              customer_trx_id,  
              invoice_number,
              invoice_date,
              invoice_amount,
              applied_amount,
              log_seq,
              api_status,
              api_msg_index,
              api_message
           )
         select
              request_id,
              created_by,
              run_date,
              l_final,
              rebate_type,
              calendar_year,
              agreement,
              customer_id,  
              customer_number,
              customer_name,
              cash_receipt_id,  
              receipt_number,
              receipt_date,
              receipt_amount,
              customer_trx_id,  
              invoice_number,
              invoice_date,
              invoice_amount,
              applied_amount,
              log_seq,
              api_status,
              api_msg_index,
              api_message            
          from xxcus.xxcus_ozf_cashapp_log
          where 1 =1
          and request_id =l_request_id;
          --
         print_log('');     
         print_log('Note: Cash application process current log is archived in the custom table xxcus.xxcus_ozf_cashapp_log_archive, total records: '||sql%rowcount);     
         print_log('');
         if (sql%rowcount >0)
          then
          DELETE xxcus.xxcus_ozf_cashapp_log where request_id !=xxcus_ozf_cashapp_pkg.g_request_id;
         end if;
         -- 
         commit;
         --           
     end if;
     --     
     print_log('');     
     print_log('Note: For all cash application process audit logs use the concurrent output [Save output as EXCEL]');     
     print_log('');          
     --
     return TRUE;
     --
    exception
     --
     when others then
      print_log('Error in xxcus_ozf_cashapp_pkg.afterreport '||sqlerrm);
      return FALSE;
    end afterreport;  
    --
    procedure swap_receipt_offer      
    ( errbuf          out nocopy   varchar2
    ,retcode        out nocopy   varchar2
    ,p_customer_id     in           number
    ,p_old_offer_code       in           varchar2
    ,p_new_offer_code       in           varchar2    
    )
    is
    --
    cursor pmts is
    select cash_receipt_id
              ,receipt_number
    from ar_cash_receipts_all arc
    where 1 =1
    and arc.pay_from_customer =p_customer_id --2330
    and arc.attribute12  =p_old_offer_code --'AMER12140341FLA'
    and exists
    (
    select 1
    from ar_payment_schedules arps
    where 1 =1
    and arps.cash_receipt_id =arc.cash_receipt_id
    and arps.class ='PMT'
    and arps.status ='OP'
    and arps.customer_id =arc.pay_from_customer
    );
    --
    l_user_id number :=apps.fnd_global.user_id;
    l_sysdate date :=sysdate;
    --
   begin -- Begin main processing
    --
     print_out('Parameters');
     print_out('===========');       
     print_out('Old offer code =>'||p_old_offer_code);     
     print_out('New offer code =>'||p_new_offer_code);
     print_out(' ');   
     for rec in pmts
      loop
       begin
        --
        update ar_cash_receipts_all
        set attribute12 =p_new_offer_code --we are only updating a descriptive flexfield
              ,last_updated_by =l_user_id
              ,last_update_date =l_sysdate
        where 1 =1
            and cash_receipt_id =rec.cash_receipt_id;
           if (sql%rowcount >0) then
            print_out('Successfully updated attribute12 [offer_code] for receipt_number ='||rec.receipt_number||', cash_receipt_id ='||rec.cash_receipt_id);           
           else
            print_out('Failed to update attribute12 [offer_code] for receipt_number ='||rec.receipt_number||', cash_receipt_id ='||rec.cash_receipt_id);
           end if;
        --  
        -- We will not issue a commit here because if something is wrong the outer block will rollback all changes. Otherwise the standard concurrent program
        -- will issue a commit during exit
        --     
       exception
        when others then
         rollback;
       end;
      end loop;             
    --             
   exception
    when others then
     print_log('Error in xxcus_ozf_cashapp_pkg.swap_receipt_offer, message ='||sqlerrm);
     rollback;
   end swap_receipt_offer;   -- End main processing
    --
--
end xxcus_ozf_cashapp_pkg;
/
show errors
/