/*************************************************************************
  $Header XXCUS_OZF_CASHAPP_INV_ALL_TBL.sql $
  Module Name: XXCUS_OZF_CASHAPP_INV_ALL_TBL

  PURPOSE: Used by HDS Rebates AR Auto Cash Application Process 

  REVISIONS:
  Ver        Date                   Author                          Description
  ---------  -----------          ------------------             ----------------
  1.0        03-Aug-2015  Balaguru Seshadri     Initial Version TMS # 20150710-00251

**************************************************************************/
--
DROP TABLE XXCUS.XXCUS_OZF_CASHAPP_INV_ALL CASCADE CONSTRAINTS;
--
CREATE TABLE XXCUS.XXCUS_OZF_CASHAPP_INV_ALL
(
  CUSTOMER_ID      NUMBER,
  CUSTOMER_NUMBER  VARCHAR2(30 BYTE),
  CUSTOMER_NAME    VARCHAR2(150 BYTE),
  INV_NUMBER       VARCHAR2(30 BYTE),
  INV_DATE         DATE,
  INV_BAL_DUE      NUMBER,
  AMOUNT_APPLIED   NUMBER,
  AMOUNT_REMAINING NUMBER,
  AGREEMENT_CODE   VARCHAR2(240BYTE),
  AGREEMENT_NAME   VARCHAR2(2000 BYTE),
  CALENDAR_YEAR    VARCHAR2(10 BYTE),
  REBATE_TYPE           VARCHAR2(30),
  CUSTOMER_TRX_ID  NUMBER,
  ORG_ID           NUMBER,
  OFFER_GLOBAL_FLAG VARCHAR2(1),
  REQUEST_ID NUMBER    
); 
--
COMMENT ON TABLE XXCUS.XXCUS_OZF_CASHAPP_INV_ALL IS 'TMS 20150710-00251';
--
CREATE INDEX XXCUS.XXCUS_OZF_CASHAPP_INV_ALL_N1 ON XXCUS.XXCUS_OZF_CASHAPP_INV_ALL (ORG_ID, CUSTOMER_ID, CUSTOMER_TRX_ID);
--
CREATE INDEX XXCUS.XXCUS_OZF_CASHAPP_INV_ALL_N2 ON XXCUS.XXCUS_OZF_CASHAPP_INV_ALL (REQUEST_ID);
--