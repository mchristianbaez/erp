/*************************************************************************
  $Header XXCUS_OZF_CASHAPP_PMTS_ALL_TBL.sql $
  Module Name: XXCUS_OZF_CASHAPP_PMTS_ALL_TBL

  PURPOSE: Used by HDS Rebates AR Auto Cash Application Process 

  REVISIONS:
  Ver        Date                   Author                          Description
  ---------  -----------          ------------------             ----------------
  1.0        03-Aug-2015  Balaguru Seshadri     Initial Version TMS # 20150710-00251

**************************************************************************/
--
DROP TABLE XXCUS.XXCUS_OZF_CASHAPP_PMTS_ALL CASCADE CONSTRAINTS;
--
CREATE TABLE XXCUS.XXCUS_OZF_CASHAPP_PMTS_ALL
(
  CUSTOMER_ID      NUMBER,
  CUSTOMER_NUMBER  VARCHAR2(30 BYTE),
  CUSTOMER_NAME    VARCHAR2(150 BYTE),
  RECEIPT_NUMBER   VARCHAR2(30 BYTE),
  RECEIPT_DATE     DATE,
  FUNDS_AVAILABLE  NUMBER,
  FUNDS_APPLIED    NUMBER,
  REBATE_TYPE        VARCHAR2(20 BYTE),
  CALENDAR_YEAR    VARCHAR2(10 BYTE),
  AGREEMENT_CODE   VARCHAR2(240 BYTE),
  CASH_RECEIPT_ID  NUMBER,
  ORG_ID           NUMBER,
  REQUEST_ID NUMBER
);
--
COMMENT ON TABLE XXCUS.XXCUS_OZF_CASHAPP_PMTS_ALL IS 'TMS 20150710-00251';
--
CREATE INDEX XXCUS.XXCUS_OZF_CASHAPP_PMTS_ALL_N1 ON XXCUS.XXCUS_OZF_CASHAPP_PMTS_ALL (ORG_ID, CUSTOMER_ID, CASH_RECEIPT_ID);
--
CREATE INDEX XXCUS.XXCUS_OZF_CASHAPP_PMTS_ALL_N2 ON XXCUS.XXCUS_OZF_CASHAPP_PMTS_ALL (REQUEST_ID);
--