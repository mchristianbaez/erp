CREATE OR REPLACE PACKAGE BODY APPS.XXWC_INV_TERM_CONSIGN_PKG
/*************************************************************************
  $Header XXWC_INV_TERM_CONSIGN_PKG.pkb $
  Module Name: XXWC_INV_TERM_CONSIGN_PKG

  PURPOSE: Terminate Supplier Consigned Inventory

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        22-Feb-2016  Manjula Chellappan    Initial Version TMS # 20160219-00081

**************************************************************************/

IS
   g_dflt_email   fnd_user.email_address%TYPE
                     := 'HDSOracleDevelopers@hdsupply.com';


   PROCEDURE TransferToRegular (p_errbuf                 OUT VARCHAR2,
                                p_retcode                OUT NUMBER,
                                p_supplier_id         IN     NUMBER,
                                p_organization_id     IN     NUMBER,
                                p_inventory_item_id   IN     NUMBER)
   /*************************************************************************
     $Header XXWC_INV_TERM_CONSIGN_PKG.TransferToRegular $
     Module Name: XXWC_INV_TERM_CONSIGN_PKG

     PURPOSE: Transfer Consigned onhand to Regular onhand

     REVISIONS:
     Ver        Date         Author                Description
     ---------  -----------  ------------------    ----------------
     1.0        22-Feb-2016  Manjula Chellappan    Initial Version TMS # 20160219-00081

   **************************************************************************/
   IS
      l_transaction_type_name        VARCHAR2 (50) := 'Transfer to Regular';
      l_sec                          VARCHAR2 (100);
      l_transaction_source           VARCHAR2 (50) := 'WC Transfer to Regular';
      l_transaction_type_id          NUMBER;
      l_transaction_source_type_id   NUMBER;
      l_transaction_source_id        NUMBER;
      l_transaction_source_code      VARCHAR2 (40) := 'EXPLICIT';
      l_transaction_interface_id     NUMBER;
      l_process_flag                 NUMBER := 1;
      l_transaction_mode             NUMBER := 2;
      l_lock_flag                    NUMBER := 3;
      l_sysdate                      DATE;
      l_start                        BOOLEAN;
      l_result                       BOOLEAN;
      l_err_code                     VARCHAR2 (100);
      l_err_desc                     VARCHAR2 (4000);
      l_status_msg                   VARCHAR2 (4000);
      l_err_msg                      VARCHAR2 (4000);
      l_record_count                 NUMBER;
      l_validation_error             EXCEPTION;

      CURSOR cur_trx_lines
      IS
         SELECT transaction_header_id
           FROM mtl_transactions_interface
          WHERE transaction_type_id = 74 AND process_flag = 1;
   BEGIN
      p_retcode := 0;

      l_sec := 'Apps Initialize';

      fnd_global.APPS_INITIALIZE (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id);

      l_sec := 'Begin';

      BEGIN
         l_sec := 'Get Transaction Type';

         SELECT transaction_type_id, transaction_source_type_id
           INTO l_transaction_type_id, l_transaction_source_type_id
           FROM mtl_transaction_types
          WHERE transaction_type_name = l_transaction_type_name;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_err_msg := l_err_msg || ' ' || l_sec || ' ' || SQLERRM;
            DBMS_OUTPUT.put_line ('Exception : ' || l_err_msg);
      END;

      INSERT INTO mtl_transactions_interface (transaction_header_id,
                                              transaction_type_id,
                                              transaction_source_type_id,
                                              transaction_source_id,
                                              source_code,
                                              process_flag,
                                              transaction_mode,
                                              lock_flag,
                                              created_by,
                                              creation_date,
                                              last_updated_by,
                                              last_update_date,
                                              source_header_id,
                                              source_line_id,
                                              inventory_item_id,
                                              organization_id,
                                              transaction_quantity,
                                              transaction_uom,
                                              transaction_date,
                                              subinventory_code,
                                              owning_tp_type,
                                              xfr_owning_organization_id,
                                              owning_organization_id)
         (SELECT mtl_material_transactions_s.NEXTVAL,
                 l_transaction_type_id,
                 l_transaction_source_type_id,
                 l_transaction_source_id,
                 'EXPLICIT',
                 1,
                 2,
                 3,
                 fnd_global.user_id,
                 SYSDATE,
                 fnd_global.user_id,
                 SYSDATE,
                 mtl_material_transactions_s.CURRVAL,
                 mtl_material_transactions_s.CURRVAL,
                 inventory_item_id,
                 organization_id,
                 transaction_quantity,
                 transaction_uom_code,
                 SYSDATE,
                 subinventory_code,
                 1,
                 organization_id,
                 owning_organization_id
            FROM mtl_onhand_quantities_detail
           WHERE     is_consigned = 1
                 AND organization_id =
                        NVL (p_organization_id, organization_id)
                 AND inventory_item_id =
                        NVL (p_inventory_item_id, inventory_item_id)
                 AND owning_organization_id IN (SELECT vendor_site_id
                                                  FROM ap_supplier_sites
                                                 WHERE vendor_id =
                                                          p_supplier_id));


      FOR rec_trx_lines IN CUR_TRX_LINES
      LOOP
         l_record_count := CUR_TRX_LINES%ROWCOUNT;
         l_err_code := NULL;
         l_err_desc := NULL;

         l_result :=
            mtl_online_transaction_pub.process_online (
               rec_trx_lines.transaction_header_id,
               NULL,
               l_err_code,
               l_err_desc);

         IF l_err_code IS NOT NULL
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
                  'Failed for transaction_header_id '
               || rec_trx_lines.transaction_header_id);
         END IF;
      END LOOP;

      fnd_file.put_line (
         fnd_file.LOG,
            ' Transfer To Regular completed. No Of Records Processed : '
         || l_record_count);
   EXCEPTION
      WHEN l_validation_error
      THEN
         p_retcode := 2;
         p_errbuf := l_status_msg;
      WHEN OTHERS
      THEN
         l_status_msg := 'Failed to create transactions ';

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_INV_TERM_CONSIGN_PKG.TransferToRegular',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => l_status_msg,
            p_distribution_list   => g_dflt_email,
            p_module              => 'INV');
   END TransferToRegular;


   PROCEDURE DisableASL (p_errbuf                 OUT VARCHAR2,
                         p_retcode                OUT NUMBER,
                         p_supplier_id         IN     NUMBER,
                         p_organization_id     IN     NUMBER,
                         p_inventory_item_id   IN     NUMBER)
   /*************************************************************************
     $Header XXWC_INV_TERM_CONSIGN_PKG.DisableASL $
     Module Name: XXWC_INV_TERM_CONSIGN_PKG

     PURPOSE: Disable the ASL for the Consigned Items

     REVISIONS:
     Ver        Date         Author                Description
     ---------  -----------  ------------------    ----------------
     1.0        22-Feb-2016  Manjula Chellappan    Initial Version TMS # 20160219-00081

   **************************************************************************/
   IS
      l_sec   VARCHAR2 (100);
   BEGIN
      l_sec := 'Disable ASL';

      UPDATE PO_APPROVED_SUPPLIER_LIST asl
         SET last_update_date = SYSDATE,
             last_updated_by = fnd_global.user_id,
             last_update_login = fnd_global.login_id,
             disable_flag = 'Y'
       WHERE     (owning_organization_id, item_id) IN (SELECT organization_id,
                                                              inventory_item_id
                                                         FROM apps.mtl_system_items_b
                                                        WHERE consigned_flag =
                                                                 '1')
             AND vendor_id = p_supplier_id
             AND owning_organization_id =
                    NVL (p_organization_id, owning_organization_id)
             AND item_id = NVL (p_inventory_item_id, item_id)
             AND NOT EXISTS
                        (SELECT 'x'
                           FROM mtl_onhand_quantities_detail
                          WHERE     inventory_item_id = asl.item_id
                                AND is_consigned = 1
                                AND organization_id =
                                       asl.owning_organization_id);

      fnd_file.put_line (
         fnd_file.LOG,
         ' ASL Update completed. No Of Records updated : ' || SQL%ROWCOUNT);
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_INV_TERM_CONSIGN_PKG.DisableASL',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => 'WHEN OTHERS',
            p_distribution_list   => g_dflt_email,
            p_module              => 'INV');
   END DisableASL;


   PROCEDURE UpdateItems (p_errbuf                 OUT VARCHAR2,
                          p_retcode                OUT NUMBER,
                          p_organization_id     IN     NUMBER,
                          p_inventory_item_id   IN     NUMBER)
   /*************************************************************************
     $Header XXWC_INV_TERM_CONSIGN_PKG.UpdateItems $
     Module Name: XXWC_INV_TERM_CONSIGN_PKG

     PURPOSE: Update the Item attributes for the Consigned Items

     REVISIONS:
     Ver        Date         Author                Description
     ---------  -----------  ------------------    ----------------
     1.0        22-Feb-2016  Manjula Chellappan    Initial Version TMS # 20160219-00081

   **************************************************************************/
   IS
      l_sec          VARCHAR2 (100);
      l_request_id   NUMBER;
   BEGIN
      l_sec := 'Insert into mtl_system_items_interface';

      INSERT INTO mtl_system_items_interface (inventory_item_id,
                                              process_flag,
                                              set_process_id,
                                              organization_id,
                                              transaction_type,
                                              consigned_flag,
                                              must_use_approved_vendor_flag,
                                              item_type)
         SELECT inventory_item_id,
                1,
                999,
                organization_id,
                'UPDATE',
                2,
                'N',
                'STOCK ITEM'
           FROM apps.mtl_system_items_b msib
          WHERE     consigned_flag = '1'
                AND inventory_item_id =
                       NVL (p_inventory_item_id, inventory_item_id)
                AND organization_id =
                       NVL (p_organization_id, organization_id)
                AND NOT EXISTS
                           (SELECT 'x'
                              FROM mtl_onhand_quantities_detail
                             WHERE     inventory_item_id =
                                          msib.inventory_item_id
                                   AND is_consigned = 1
                                   AND organization_id = msib.organization_id);

      fnd_file.put_line (
         fnd_file.LOG,
            ' Items load to interface table completed. No Of Records Loaded : '
         || SQL%ROWCOUNT);

      l_sec := 'Submit Import Items';

      l_request_id :=
         fnd_request.submit_request (application   => 'INV',
                                     program       => 'INCOIN',
                                     description   => NULL,
                                     start_time    => SYSDATE,
                                     sub_request   => FALSE,
                                     argument1     => 222,
                                     -- Organization id
                                     argument2     => 1,
                                     -- All organizations
                                     argument3     => 1,
                                     -- Validate Items
                                     argument4     => 1,
                                     -- Process Items
                                     argument5     => 1,
                                     -- Delete Processed Rows
                                     argument6     => 999,
                                     -- Process Set (Null for All)
                                     argument7     => 2,
                                     -- Create or Update Items
                                     argument8     => 1   -- Gather Statistics
                                                       );

      IF l_request_id > 0
      THEN
         COMMIT;
         fnd_file.put_line (
            fnd_file.LOG,
            'Import Items Submitted : Request Id ' || l_request_id);
      ELSE
         fnd_file.put_line (fnd_file.LOG, 'Import Items Not Submitted');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_INV_TERM_CONSIGN_PKG.UpdateItems',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => 'WHEN-OTHERS',
            p_distribution_list   => g_dflt_email,
            p_module              => 'INV');
   END UpdateItems;

   PROCEDURE main (errbuf                   OUT VARCHAR2,
                   retcode                  OUT NUMBER,
                   p_supplier_id         IN     NUMBER,
                   p_organization_id     IN     NUMBER,
                   p_inventory_item_id   IN     NUMBER,
                   p_action_type         IN     VARCHAR2)
   /*************************************************************************
       $Header XXWC_INV_TERM_CONSIGN_PKG.main $
       Module Name: XXWC_INV_TERM_CONSIGN_PKG

       PURPOSE: Procedure to use in the concurrent program

       REVISIONS:
       Ver        Date         Author                Description
       ---------  -----------  ------------------    ----------------
       1.0        22-Feb-2016  Manjula Chellappan    Initial Version TMS # 20160219-00081
     **************************************************************************/
   IS
      l_errbuf    VARCHAR2 (2000);
      l_retcode   NUMBER;
      l_sec       VARCHAR2 (100);
   BEGIN
      fnd_file.put_line (fnd_file.LOG, 'p_supplier_id ' || p_supplier_id);
      fnd_file.put_line (fnd_file.LOG,
                         'p_organization_id ' || p_organization_id);
      fnd_file.put_line (fnd_file.LOG,
                         'p_inventory_item_id ' || p_inventory_item_id);

      IF p_action_type = 0
      THEN
         l_sec := 'Call TransferToRegular';

         TransferToRegular (l_errbuf,
                            l_retcode,
                            p_supplier_id,
                            p_organization_id,
                            p_inventory_item_id);
							
		  IF l_retcode = 2
		  THEN
			 retcode := 2;
			 errbuf := l_errbuf;
		  END IF;							

         l_sec := 'Call DisableASL';

         DisableASL (l_errbuf,
                     l_retcode,
                     p_supplier_id,
                     p_organization_id,
                     p_inventory_item_id);
					 
		  IF l_retcode = 2
		  THEN
			 retcode := 2;
			 errbuf := l_errbuf;
		  END IF;							
					 

         l_sec := 'Call UpdateItems';

         UpdateItems (l_errbuf,
                      l_retcode,
                      p_organization_id,
                      p_inventory_item_id);
					  
		  IF l_retcode = 2
		  THEN
			 retcode := 2;
			 errbuf := l_errbuf;
		  END IF;							
					  
      ELSIF p_action_type = 1
      THEN
         l_sec := 'Call TransferToRegular';

         TransferToRegular (l_errbuf,
                            l_retcode,
                            p_supplier_id,
                            p_organization_id,
                            p_inventory_item_id);
							
		  IF l_retcode = 2
		  THEN
			 retcode := 2;
			 errbuf := l_errbuf;
		  END IF;							
							
      ELSIF p_action_type = 2
      THEN
         l_sec := 'Call DisableASL';

         DisableASL (l_errbuf,
                     l_retcode,
                     p_supplier_id,
                     p_organization_id,
                     p_inventory_item_id);

		  IF l_retcode = 2
		  THEN
			 retcode := 2;
			 errbuf := l_errbuf;
		  END IF;							
					 
      ELSIF p_action_type = 3
      THEN
         l_sec := 'Call UpdateItems';

         UpdateItems (l_errbuf,
                      l_retcode,
                      p_organization_id,
                      p_inventory_item_id);

		  IF l_retcode = 2
		  THEN
			 retcode := 2;
			 errbuf := l_errbuf;
		  END IF;							
					  
      END IF;

   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_INV_TERM_CONSIGN_PKG.main',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => 'WHEN OTHERS',
            p_distribution_list   => g_dflt_email,
            p_module              => 'INV');


         retcode := 2;
         errbuf := l_errbuf;
   END main;
END XXWC_INV_TERM_CONSIGN_PKG;
/