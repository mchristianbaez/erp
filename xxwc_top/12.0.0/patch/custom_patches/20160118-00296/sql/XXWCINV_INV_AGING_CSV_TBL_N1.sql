   /****************************************************************************************************************************************
     $Header XXWCINV_INV_AGING_CSV_TBL_N1
     File Name: XXWCINV_INV_AGING_CSV_TBL_N1.sql

     PURPOSE:   

     REVISIONS:
     Ver        Date         Author             Description
     ---------  ----------   ---------------    ------------------------------------------------------------------------------------
     1.0        01-FEB-2016  Kishorebabu V      TMS#20160118-00296 XXWC Inventory Aging Report - CSV Program Running for longer time
   ***************************************************************************************************************************************/

CREATE INDEX XXWC.XXWCINV_INV_AGING_CSV_TBL_N1
   ON XXWC.XXWCINV_INV_AGING_CSV_TBL (ITEM_ID,TO_ORGANIZATION_ID)
/