/***************************************************************************
    $Header XXWC.XXWCINV_INV_AGING_CSV_TBL $
    Module Name: XXWCINV_INV_AGING_CSV_TBL
    PURPOSE: Table used for XXWC Inventory Aging Report - CSV Program
 
    REVISIONS:
    Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        01-FEB-2016  Kishorebabu V    TMS #20160118-00296 - XXWC Inventory Aging Report - CSV Program Running for longer time
***************************************************************************/
CREATE TABLE  XXWC.XXWCINV_INV_AGING_CSV_TBL (VENDOR_ID          NUMBER
                                             ,ITEM_ID            NUMBER 
                                             ,TO_ORGANIZATION_ID NUMBER)
/