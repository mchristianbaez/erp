/*
 TMS:  20170705-00029
 Date: 07/06/2017
 Notes:  Rebates audit process failed because of an emergency concurrent manager shut down 
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
     --
     n_loc number :=0;
     l_partition_sdw varchar2(20) :='FM';
     l_bu_nm  varchar2(60) :='FACILITIES MAINTENANCE';
     l_dw_btch_id number :=1778182;
     l_oracle_bu_nm varchar2(80) :='FACILITIES MAINTENANCE';
     l_fiscal_wk_start date :='26-JUN-17';
     l_fiscal_wk_end date :='02-JUL-17';
 --
     --
      PROCEDURE print_log(p_message IN VARCHAR2) IS
      BEGIN
        IF fnd_global.conc_request_id > 0
        THEN
          fnd_file.put_line(fnd_file.log, p_message);
        ELSE
          dbms_output.put_line(p_message);
        END IF;
      END print_log;
      -- 
         PROCEDURE recon_audit_pp
                                        (
                                          p_bu_nm          IN VARCHAR2
                                         ,p_dw_btch_id IN NUMBER
                                         ,p_oracle_bu_nm IN VARCHAR2
                                         ,p_fiscal_wk_start IN DATE
                                         ,p_fiscal_wk_end IN DATE
                                        ) IS
        --
        -- Package Variables
        --
        l_req_id             NUMBER NULL;
        v_phase              VARCHAR2(50);
        v_status             VARCHAR2(50);
        v_dev_status         VARCHAR2(50);
        v_dev_phase          VARCHAR2(50);
        v_message            VARCHAR2(250);
        v_error_message      VARCHAR2(3000);
        v_interval           NUMBER := 30; -- In seconds
        v_max_time           NUMBER := 1500; -- In seconds
        l_err_msg            VARCHAR2(3000);
        l_err_code           NUMBER;
        l_sec                VARCHAR2(255);
        l_statement          VARCHAR2(9000);
        l_program            VARCHAR2(30) DEFAULT 'XXCUSOZF_REBATES_RECPT_EXCPT';
        l_app                VARCHAR2(30) DEFAULT 'XXCUS';
        l_responsibility     NUMBER;
        l_user               VARCHAR2(100);
        l_user_id            NUMBER;
        l_can_submit_request BOOLEAN := TRUE;
        -- Error DEBUG
        l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.recon_audit_pp'; --rebates recon audit post processor
        l_err_callpoint VARCHAR2(75) DEFAULT 'START';
        l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
        --
        l_err_count Number :=0;
        l_err_amount Number :=0;
        --
        l_loaded_count Number :=0;
        l_loaded_amount Number :=0;
        --
       type l_resale_line_type is RECORD
        (
            resale_line_id NUMBER
           ,price NUMBER
           ,qty NUMBER
           ,bu VARCHAR2(150)
           ,resale_receipt# VARCHAR2(150)
        );
        type l_resale_line_tbl is table of l_resale_line_type index by binary_integer;
         l_resale_lines_rec l_resale_line_tbl;
        --

      BEGIN
            --If we had successfully pushed the receipts over to the interface table ozf_resale_lines_int_all
            --then we must expect a status of CLOSED. Check if any record has a status of other than CLOSED.
            begin
              --
                    select /*+ parallel (a 2) */
                           NVL(sum(nvl(a.selling_price,0) * nvl(a.quantity,0)), 0) --sum(nvl(a.selling_price,0) * nvl(a.quantity,0))
                          ,count(a.line_attribute5)
                    into  l_err_amount
                            ,l_err_count
                    from ozf.ozf_resale_lines_int_all a
                    where 1 =1
                         and a.line_attribute2 =p_oracle_bu_nm
                         and a.date_ordered between p_fiscal_wk_start and p_fiscal_wk_end
                         and a.org_id in ('101', '102')
                         and a.status_code <> 'CLOSED'
                         ;
                    --
                    l_err_amount :=nvl(l_err_amount, 0);
                    --
              --
            exception
             when no_data_found then
              l_err_count :=-97.1;
              l_err_amount :=-97.1;
             when others then
              print_log('Error in fetch of failed fiscal week receipt records @ ct5, message ='||sqlerrm);
              l_err_count :=-97.2;
              l_err_amount :=-97.2;
            end;
          --
          begin
           savepoint ct4b;
           update xxcus.xxcus_rebates_recon_audit_b
                   set  ct5_total_rows =l_err_count
                         ,ct5_total_amount =l_err_amount
                         ,status_code ='IN PROCESS'
                         ,stage_code ='EBS-CT5'
                         ,last_update_date =sysdate
              where 1 =1
                   and bu_name =p_bu_nm
                   and dw_btch_id =p_dw_btch_id
                   and status_code ='IN PROCESS'
                   and stage_code ='EBS-CT4';
                print_log (' ');
                print_log ('CT5 Complete, Total rows :'||l_err_count||', Total Amount :'||l_err_amount);
                print_log (' ');
           commit;
          exception
           when others then
            rollback to ct4b;
            print_log('Error in updating recon audit table status code to EBS-CT5, message :'||sqlerrm);
          end;
          --
          COMMIT;
          --
            begin
              --
                select /*+ parallel (b 4) */
                          sum (nvl (b.selling_price, 0) * nvl (b.quantity, 0))
                         ,count (b.line_attribute5)
                into    l_loaded_amount
                          ,l_loaded_count
                from apps.ozf_resale_lines_all b
                where 1 =1
                     and b.date_ordered between p_fiscal_wk_start and p_fiscal_wk_end
                     and b.line_attribute2 =p_oracle_bu_nm
                     ;
              --
              print_log(' ');
              print_log('@CT6,l_loaded_count = '||l_loaded_count||', l_loaded_amount ='||l_loaded_amount);
              print_log(' ');
              --
                  begin
                   savepoint ct6b;
                   update xxcus.xxcus_rebates_recon_audit_b
                           set  ct6_total_rows =l_loaded_count
                                 ,ct6_total_amount =l_loaded_amount
                                 ,status_code ='COMPLETE'
                                 ,stage_code ='EBS-CT6'
                                 ,last_update_date =sysdate
                      where 1 =1
                           and bu_name =p_bu_nm
                           and dw_btch_id =p_dw_btch_id
                           and status_code ='IN PROCESS'
                           and stage_code ='EBS-CT5';
                        print_log (' ');
                        print_log ('CT6 Complete, Total rows :'||l_loaded_count||', Total Amount :'||l_loaded_amount);
                        print_log (' ');
                   commit;
                  exception
                   when others then
                    rollback to ct6b;
                    print_log('Error in updating recon audit table status code to EBS-CT6, message :'||sqlerrm);
                  end;
              --
            exception
             when no_data_found then
              l_loaded_count :=-98.1;
              l_loaded_amount :=-98.1;
             when others then
              print_log('Error in fetch of successfully loaded receipt records @ ct6, message ='||sqlerrm);
              l_loaded_count :=-98.2;
              l_loaded_amount :=-98.2;
            end;
          --
      EXCEPTION
        WHEN program_error THEN
          ROLLBACK;
          l_err_msg  := 'When program_error...' ||sqlerrm;
          print_log('l_err_msg =>'||l_err_msg);
        --
        WHEN OTHERS THEN
          ROLLBACK;        
          l_err_msg  := 'When Others...' ||sqlerrm;
          print_log('l_err_msg =>'||l_err_msg);
      END recon_audit_pp;     
 --
BEGIN --Main Processing...
    --
    recon_audit_pp
                                        (
                                          p_bu_nm          =>l_bu_nm --IN VARCHAR2
                                         ,p_dw_btch_id =>l_dw_btch_id --IN NUMBER
                                         ,p_oracle_bu_nm =>l_oracle_bu_nm --IN VARCHAR2
                                         ,p_fiscal_wk_start =>l_fiscal_wk_start --IN DATE
                                         ,p_fiscal_wk_end =>l_fiscal_wk_end --IN DATE
                                        );  
    --
    commit;
    --
    select count(1) into n_loc from xxcus.xxcus_rebate_receipt_sdw_tbl partition (FM);
    --
    print_log('Before delete, FM SDW table count =>'||n_loc);
    --
      EXECUTE IMMEDIATE 'alter table xxcus.xxcus_rebate_receipt_sdw_tbl truncate partition FM';
    --  
    select count(1) into n_loc from xxcus.xxcus_rebate_receipt_sdw_tbl partition (FM);
    --
    print_log('After delete, FM SDW table count =>'||n_loc);
    --                              
EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      print_log ('TMS:  20170705-00029, Outer block errors =' || SQLERRM);
END;
/