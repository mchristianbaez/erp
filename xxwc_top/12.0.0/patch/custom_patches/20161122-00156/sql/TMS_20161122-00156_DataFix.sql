/*************************************************************************
  $Header TMS_20161122-00156_DataFix.sql $
  Module Name: TMS_20161122-00156_DataFix  Data Fix script for TMS# 20161122-00156

  PURPOSE: Data Fix script for TMS# 20161122-00156

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        30-SEP-2016  Gopi Damuluri         TMS#20161122-00156 
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20161122-00156    , Before Update');

   DELETE FROM xxwc.xxwc_sales_order_mmsi_line;

   DBMS_OUTPUT.put_line ('TMS: 20161122-00156  # of rows deleted : '|| SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20161122-00156    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161122-00156 , Errors : ' || SQLERRM);
END;
/