/*
 TMS: 20151120-00026     
 Date: 01/25/2016
 Notes: @SF Data fix script for I646014
*/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;


update apps.oe_order_lines_all
set 
  INVOICE_INTERFACE_STATUS_CODE='YES'
,open_flag='N'
,flow_status_code='CLOSED'
,INVOICED_QUANTITY=1
where line_id=61933205
and headeR_id=37772111;
		  
--1 row expected to be updated

commit;

/

