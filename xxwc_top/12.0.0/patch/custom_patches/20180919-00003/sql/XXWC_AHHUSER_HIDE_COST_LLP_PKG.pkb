CREATE OR REPLACE 
PACKAGE BODY XXWC_AHHUSER_HIDE_COST_LLP_PKG 
AS
/*************************************************************************
     $Header XXWC_AHHUSER_HIDE_COST_LLP_PKG $
     Module Name: XXWC_AHHUSER_HIDE_COST_LLP_PKG.pkb

     PURPOSE:   Setting the profile to AHH Users not to see the Margins for orders

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        09/20/2018  Pattabhi Avula          TMS#20180919-00003 - Last price paid for North East AHH Branches
**************************************************************************/

PROCEDURE MAIN(ERRBUF OUT VARCHAR2, RETCODE OUT VARCHAR2,P_USER VARCHAR2)
IS
l_errbuf     varchar2(2000);
l_retcode    varchar2(500);

l_count      NUMBER:=0;
l_username   VARCHAR2(50);
l_str        VARCHAR2(100):=P_USER;
BEGIN
 IF P_USER IS NOT NULL THEN
  fnd_file.put_line(fnd_file.log,'START PROFILE_SET_FOR_AHH_P_USER procedure');
    fnd_file.put_line(fnd_file.log,'P_USER_NAME '||P_USER);
  SELECT REGEXP_COUNT(l_str, ',') REGEXP_COUNT
    INTO l_count
    FROM DUAL; 
   
   FOR I IN 1..L_COUNT+1 LOOP
     SELECT REGEXP_SUBSTR (l_str, '[^,]+', 1, I)
       INTO l_username
       FROM DUAL;
   
       fnd_file.put_line(fnd_file.log,'P_USER_NAME '||l_username);
   
       PROFILE_SET_FOR_AHH_P_USER(l_errbuf,l_retcode,l_username);
     
   END LOOP;
 fnd_file.put_line(fnd_file.log,'END PROFILE_SET_FOR_AHH_P_USER procedure');
ELSE

fnd_file.put_line(fnd_file.log,'Start PROFILE_SET_NULL_FOR_AHH_USER procedure');
PROFILE_SET_NULL_FOR_AHH_USER(l_errbuf,l_retcode); 
fnd_file.put_line(fnd_file.log,'END PROFILE_SET_NULL_FOR_AHH_USER procedure');

fnd_file.put_line(fnd_file.log,'Start PROFILE_SET_FOR_AHH_USER procedure');
PROFILE_SET_FOR_AHH_USER(l_errbuf,l_retcode);
fnd_file.put_line(fnd_file.log,'END PROFILE_SET_FOR_AHH_USER procedure');
END IF;
EXCEPTION
WHEN OTHERS THEN
fnd_file.put_line(fnd_file.log,'Uexpected error in XXWC_AHHUSER_HIDE_COST_LLP_PKG.Main PKG '||SQLERRM);
END;

PROCEDURE PROFILE_SET_NULL_FOR_AHH_USER(ERRBUF OUT VARCHAR2, RETCODE OUT VARCHAR2)
 IS
   l_Result Boolean;
   l_user_id NUMBER; 
   le_exception EXCEPTION;
   l_err_msg VARCHAR2(2000);
   l_profile_value  varchar2(5);
   
   CURSOR cur_user
   IS
    SELECT FU.USER_ID,FU.USER_NAME
      FROM fnd_profile_options fpo,
           fnd_profile_option_values fpov,
           fnd_user fu       
     WHERE 1 = 1
       AND fpo.PROFILE_OPTION_ID = fpov.PROFILE_OPTION_ID
       AND fpo.PROFILE_OPTION_NAME='XXWC_OM_VIEW_GROSS_MARGINS'
       AND FPOV.LEVEL_ID=10004
       AND FPOV.LEVEL_VALUE = FU.USER_ID
       AND (FU.END_DATE IS NULL OR FU.END_DATE > SYSDATE)
       AND FPOV.PROFILE_OPTION_VALUE IS NOT NULL
       --AND FU.USER_NAME='RV003897' --'10010564' --'PA022863' --'10011189'
       AND NOT EXISTS (SELECT 1
                         FROM APPLSYS.wf_user_role_assignments ur, 
                              apps.WF_ROLES wr,
                              apps.fnd_lookup_values FLV                        
                        WHERE  1=1  
                         -- AND ROLE_ORIG_SYSTEM IN ('FND_RESP', 'UMX') 
                          AND UPPER(wr.name) = UPPER(ur.role_name)
                          AND UR.USER_NAME= FU.USER_NAME                      
                          AND wr.status = 'ACTIVE'
						  AND UPPER(ur.role_name) = UPPER(flv.meaning)
                          AND (ur.user_end_date IS NULL OR ur.user_end_date > SYSDATE)
                          AND (ur.assigning_role_end_date IS NULL OR ur.assigning_role_end_date > SYSDATE)
                          and flv.lookup_type = 'XXWC_VIEW_GROSS_MARGINS_ROLE'
                          AND FLV.ENABLED_FLAG = 'Y'
                          AND SYSDATE BETWEEN NVL(flv.START_DATE_ACTIVE,SYSDATE-1) AND NVL(flv.END_DATE_ACTIVE,SYSDATE+1));
BEGIN
    fnd_file.put_line(fnd_file.log,'Start PROCEDURE PROFILE_SET_NULL_FOR_AHH_USER');
    FOR rec_user IN cur_user
    LOOP
             
         l_Result:= Fnd_profile.SAVE('XXWC_OM_VIEW_GROSS_MARGINS',' ','USER',rec_user.USER_ID);
		 fnd_file.put_line(fnd_file.log,'l_Result for User: '||rec_user.USER_NAME);
          IF l_Result
          THEN             
             fnd_file.put_line(fnd_file.log,'XXWC_OM_VIEW_GROSS_MARGINS profile is set to null for user '||rec_user.USER_NAME);
             COMMIT;
          ELSE
             l_err_msg := 'XXWC_OM_VIEW_GROSS_MARGINS Profile Not Updated for user '||rec_user.USER_NAME;
            -- RAISE le_exception; 
          END IF;
    END LOOP;
    fnd_file.put_line(fnd_file.log,'End Script for role changed');
EXCEPTION
   WHEN OTHERS THEN
      l_err_msg := SUBSTR(SQLERRM,1,2000);
      fnd_file.put_line(fnd_file.log,'Error: '||l_err_msg);
      ROLLBACK;
END;

-- Without parameter proc
PROCEDURE PROFILE_SET_FOR_AHH_USER(ERRBUF OUT VARCHAR2, RETCODE OUT VARCHAR2)
 IS
   l_Result Boolean;
   l_user_id NUMBER; 
   le_exception EXCEPTION;
   l_err_msg VARCHAR2(2000);
   l_profile_value  varchar2(5);
   
   CURSOR cur_role
   IS
     SELECT UR.USER_NAME, flv.meaning
       FROM   APPLSYS.wf_user_role_assignments ur, 
              apps.WF_ROLES wr,
              apps.fnd_lookup_values FLV
       WHERE  1=1 
       AND UPPER(wr.name) = UPPER(ur.role_name)
      -- AND ur.user_name ='SL069571'
       AND wr.status = 'ACTIVE'
	   AND UPPER(ur.role_name) = UPPER(flv.meaning)
       AND (ur.user_end_date IS NULL OR ur.user_end_date > SYSDATE)
	   AND (ur.end_date IS NULL OR ur.end_date > SYSDATE)
       AND (ur.assigning_role_end_date IS NULL OR ur.assigning_role_end_date > SYSDATE)
       AND flv.lookup_type='XXWC_VIEW_GROSS_MARGINS_ROLE'
       AND flv.enabled_flag = 'Y'
       AND SYSDATE BETWEEN NVL(flv.START_DATE_ACTIVE,SYSDATE-1) AND NVL(flv.END_DATE_ACTIVE,SYSDATE+1);   
BEGIN
    fnd_file.put_line(fnd_file.log,'Start PROCEDURE ');
    FOR rec_user_role IN cur_role
    LOOP      
          l_user_id := null;
          l_err_msg := NULL;
           BEGIN
              SELECT USER_ID 
              INTO l_user_id
              FROM FND_USER
              WHERE USER_NAME = upper(rec_user_role.USER_NAME);
           EXCEPTION
              WHEN NO_DATA_FOUND THEN
                 l_err_msg := 'User '||rec_user_role.USER_NAME||' not found';                 
           END;
           -- checking the profile option value from user level
          BEGIN
           SELECT fpov.profile_option_value
             INTO l_profile_value
             FROM fnd_profile_options fpo,
                  fnd_profile_option_values fpov                    
            WHERE 1 = 1
              AND fpo.PROFILE_OPTION_ID = fpov.PROFILE_OPTION_ID
              AND fpo.PROFILE_OPTION_NAME='XXWC_OM_VIEW_GROSS_MARGINS'
              AND fpov.level_id=10004
              AND fpov.level_value = l_user_id;
           EXCEPTION
            WHEN NO_DATA_FOUND THEN
              l_profile_value:=NULL;
            WHEN OTHERS THEN
            l_profile_value:=NULL;
            fnd_file.put_line(fnd_file.log,'Error while fetching the profile value for user '||rec_user_role.USER_NAME);
           END;
       
       IF l_profile_value IS NULL THEN
       
         l_Result:= Fnd_profile.SAVE('XXWC_OM_VIEW_GROSS_MARGINS','N','USER',l_user_id);
          IF l_Result
          THEN
             COMMIT;
             fnd_file.put_line(fnd_file.log,'XXWC_OM_VIEW_GROSS_MARGINS Profile is set to N for user '||rec_user_role.USER_NAME);
			 fnd_file.put_line(fnd_file.log,'Processed the record for role '||rec_user_role.meaning);
          ELSE
             l_err_msg := 'XXWC_OM_VIEW_GROSS_MARGINS Profile Not Updated for user '||rec_user_role.USER_NAME;
          END IF;
         END IF;
    END LOOP;
    fnd_file.put_line(fnd_file.log,'End Script');
EXCEPTION
   WHEN OTHERS THEN
      l_err_msg := SUBSTR(SQLERRM,1,2000);
      fnd_file.put_line(fnd_file.log,'Error: '||l_err_msg);
      ROLLBACK;
END;

-- with Parameter logic
PROCEDURE PROFILE_SET_FOR_AHH_P_USER(ERRBUF OUT VARCHAR2, RETCODE OUT VARCHAR2,P_USER IN VARCHAR2)
 IS
   l_Result Boolean;
   l_user_id NUMBER; 
   le_exception EXCEPTION;
   l_err_msg VARCHAR2(2000);
   l_profile_value  varchar2(5);
   
   CURSOR cur_role
   IS
     SELECT UR.USER_NAME,flv.meaning
       FROM   APPLSYS.wf_user_role_assignments ur, 
              apps.WF_ROLES wr,
              apps.fnd_lookup_values FLV
       WHERE  1=1 
       AND UPPER(wr.name) = UPPER(ur.role_name)
      AND ur.user_name = P_USER  --'SL069571'
       AND wr.status = 'ACTIVE'
	   AND UPPER(ur.role_name) = UPPER(flv.meaning)
       AND (ur.user_end_date IS NULL OR ur.user_end_date > SYSDATE) -- user role end date
	   AND (ur.end_date IS NULL OR ur.end_date > SYSDATE) -- user end date 
       AND (ur.assigning_role_end_date IS NULL OR ur.assigning_role_end_date > SYSDATE)
       AND flv.lookup_type='XXWC_VIEW_GROSS_MARGINS_ROLE'
       AND flv.enabled_flag = 'Y'
       AND SYSDATE BETWEEN NVL(flv.START_DATE_ACTIVE,SYSDATE-1) AND NVL(flv.END_DATE_ACTIVE,SYSDATE+1);   
BEGIN
    fnd_file.put_line(fnd_file.log,'Start PROCEDURE ');
    FOR rec_user_role IN cur_role
    LOOP      
          l_user_id := null;
          l_err_msg := NULL;
           BEGIN
              SELECT USER_ID 
              INTO l_user_id
              FROM FND_USER
              WHERE USER_NAME = upper(rec_user_role.USER_NAME);
           EXCEPTION
              WHEN NO_DATA_FOUND THEN
                 l_err_msg := 'User '||rec_user_role.USER_NAME||' not found';                 
           END;
           -- checking the profile option value from user level
          BEGIN
           SELECT fpov.profile_option_value
             INTO l_profile_value
             FROM fnd_profile_options fpo,
                  fnd_profile_option_values fpov                    
            WHERE 1 = 1
              AND fpo.PROFILE_OPTION_ID = fpov.PROFILE_OPTION_ID
              AND fpo.PROFILE_OPTION_NAME='XXWC_OM_VIEW_GROSS_MARGINS'
              AND fpov.level_id=10004
              AND fpov.level_value = l_user_id;
           EXCEPTION
            WHEN NO_DATA_FOUND THEN
              l_profile_value:=NULL;
            WHEN OTHERS THEN
            l_profile_value:=NULL;
            fnd_file.put_line(fnd_file.log,'Error while fetching the profile value for user '||rec_user_role.USER_NAME);
           END;
       
       IF l_profile_value IS NULL THEN
       
         l_Result:= Fnd_profile.SAVE('XXWC_OM_VIEW_GROSS_MARGINS','N','USER',l_user_id);
          IF l_Result
          THEN
             COMMIT;
             fnd_file.put_line(fnd_file.log,'XXWC_OM_VIEW_GROSS_MARGINS Profile is set to N for user '||rec_user_role.USER_NAME);
			 fnd_file.put_line(fnd_file.log,'Processed the record for role '||rec_user_role.meaning);
          ELSE
             l_err_msg := 'XXWC_OM_VIEW_GROSS_MARGINS Profile Not Updated for user '||rec_user_role.USER_NAME;
          END IF;
         END IF;
    END LOOP;
    fnd_file.put_line(fnd_file.log,'End Script');
EXCEPTION
   WHEN OTHERS THEN
      l_err_msg := SUBSTR(SQLERRM,1,2000);
      fnd_file.put_line(fnd_file.log,'Error: '||l_err_msg);
      ROLLBACK;
END;  -- parameter logic

END XXWC_AHHUSER_HIDE_COST_LLP_PKG;
/