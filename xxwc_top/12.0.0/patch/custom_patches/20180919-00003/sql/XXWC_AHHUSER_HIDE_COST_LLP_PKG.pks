CREATE OR REPLACE 
PACKAGE XXWC_AHHUSER_HIDE_COST_LLP_PKG
AS
/*************************************************************************
     $Header XXWC_AHHUSER_HIDE_COST_LLP_PKG $
     Module Name: XXWC_AHHUSER_HIDE_COST_LLP_PKG.pkb

     PURPOSE:   Setting the profile to AHH Users not to see the Margins for orders

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        09/20/2018  Pattabhi Avula          TMS#20180919-00003 - Last price paid for North East AHH Branches
**************************************************************************/
PROCEDURE MAIN(ERRBUF OUT VARCHAR2, RETCODE OUT VARCHAR2, P_USER IN VARCHAR2);

PROCEDURE PROFILE_SET_FOR_AHH_USER(ERRBUF OUT VARCHAR2, RETCODE OUT VARCHAR2);

PROCEDURE PROFILE_SET_NULL_FOR_AHH_USER(ERRBUF OUT VARCHAR2, RETCODE OUT VARCHAR2);

PROCEDURE PROFILE_SET_FOR_AHH_P_USER(ERRBUF OUT VARCHAR2, RETCODE OUT VARCHAR2,P_USER IN VARCHAR2);

END XXWC_AHHUSER_HIDE_COST_LLP_PKG;
/