-----------------------------------------------------------------------------------------------
/* *****************************************************************************
  $Header XXEIS.EIS_XXWC_MAN_PRICE_OVERRIDE_V.vw $
  Module Name: Order Management
  PURPOSE: Manual Price Override Report
  TMS Task Id :20171219-00082 
  REVISIONS: Initial Version
  Ver        	Date        	Author                			Description
  ---------  ----------  ------------------    				----------------
  1.0        03/01/2017  		Siva       			Initial Version --20171219-00082
************************************************************************************/
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_MAN_PRICE_OVERRIDE_V (PRICING_ZONE, REGION, BRANCH, CUSTOMER, CUSTOMER_NUMBER, CREATED_BY, ORDER_NUMBER, ITEM, ITEM_DESCRIPTION, CAT_CLASS, CAT_CLASS_DESC, QUANTITY, LIST_PRICE, ORIGINAL_UNIT_SELLING_PRICE, FINAL_SELLING_PRICE, REASON_CODE, APPLICATION_METHOD, ORDERED_DATE, AVERAGE_COST, MODIFIER_TYPE, MODIFER_NUMBER, PERCENTAGE_PRICE_CHANGE, ACCOUNT_MANAGER, PERIOD_NAME, DOLLARS_LOST, LIST_LINE_TYPE_CODE, OH_HEADER_ID, OL_LINE_ID, ADJ_MAN_PRICE_ADJUSTMENT_ID, MSI_INVENTORY_ITEM_ID, MSI_ORGANIZATION_ID, MP_ORGANIZATION_ID, HCA_CUST_ACCOUNT_ID)
AS 
  SELECT /*+ INDEX(adj_man XXWC_OE_PRICE_ADJUSTMENT_N1) INDEX(hca HZ_CUST_ACCOUNTS_U1) INDEX(qph QP_LIST_HEADERS_B_PK) */
    mp.attribute6 pricing_zone,
    mp.attribute9 region,
    mp.organization_code branch,
    hca.account_name customer,
    hca.account_number customer_number,
    (SELECT ppf.full_name
    FROM fnd_user fu,
      per_people_f ppf
    WHERE fu.user_id   = ol.created_by
    AND fu.employee_id = ppf.person_id
    AND TRUNC (ol.creation_date) BETWEEN ppf.effective_start_date AND ppf.effective_end_date
    )created_by,
    oh.order_number order_number,
    msi.segment1 item,
    msi.description item_description,
    xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class(msi.inventory_item_id,msi.organization_id) cat_class,
    xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class_desc(msi.inventory_item_id,msi.organization_id) cat_class_desc,
    ol.ordered_quantity quantity,
    ol.unit_list_price list_price,
    (ol.unit_list_price-xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_amt(ol.header_id,ol.line_id)) original_unit_selling_price,
    ol.unit_selling_price final_selling_price,
    (SELECT oel.meaning
    FROM oe_lookups oel
    WHERE oel.lookup_code = adj_man.change_reason_code
    AND oel.lookup_type   ='CHANGE_CODE'
    )reason_code,
    apps.qp_qp_form_pricing_attr.get_meaning(adj_man.arithmetic_operator, 'ARITHMETIC_OPERATOR') application_method,
    TRUNC(oh.ordered_date) ordered_date,
    NVL(ol.unit_cost,0) average_cost,
    qph.attribute10 modifier_type,
    xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_name(ol.header_id,ol.line_id) modifer_number,
    ROUND(DECODE((xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_name(ol.header_id,ol.line_id)),NULL, ((NVL(ol.unit_list_price,0)-NVL(ol.unit_selling_price,0))/NVL(ol.unit_list_price,0)), (((ol.unit_list_price-xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_amt(ol.header_id,ol.line_id))-NVL(ol.unit_selling_price,0))/(ol.unit_list_price-xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_amt(ol.header_id,ol.line_id))))*100,2) percentage_price_change,
    (SELECT rs.name
    FROM ra_salesreps rs
    WHERE ol.salesrep_id= rs.salesrep_id
    AND ol.org_id       = rs.org_id
    ) account_manager,
    (SELECT gps.period_name
    FROM gl_periods gps
    WHERE oh.ordered_date BETWEEN gps.start_date AND gps.end_date
    AND gps.period_set_name = '4-4-QTR'
    AND rownum              =1
    ) period_name,
    (NVL(ol.unit_selling_price,0)- (ol.unit_list_price-xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_amt(ol.header_id,ol.line_id)))dollars_lost,
    adj_man.list_line_type_code,
    ---Primary Keys
    oh.header_id oh_header_id,
    ol.line_id ol_line_id,
    adj_man.price_adjustment_id adj_man_price_adjustment_id,
    msi.inventory_item_id msi_inventory_item_id,
    msi.organization_id msi_organization_id,
    mp.organization_id mp_organization_id,
    hca.cust_account_id hca_cust_account_id
    --descr#flexfield#start
    --descr#flexfield#end
  FROM oe_order_headers oh,
    oe_order_lines ol,
    mtl_system_items_b msi,
    mtl_parameters mp,
    hz_cust_accounts hca,
    apps.oe_price_adjustments adj_man,
    apps.qp_list_headers_b qph,
    apps.qp_list_headers_tl qlht
  WHERE oh.header_id                 = ol.header_id
  AND ol.ship_from_org_id            = msi.organization_id
  AND ol.inventory_item_id           = msi.inventory_item_id
  AND ol.ship_from_org_id            = mp.organization_id
  AND oh.sold_to_org_id              = hca.cust_account_id
  AND ol.header_id                   = adj_man.header_id(+)
  AND ol.line_id                     = adj_man.line_id(+)
  AND adj_man.list_line_type_code(+) = 'DIS'
  AND adj_man.list_header_id         = qph.list_header_id
  AND qph.list_header_id             = qlht.list_header_id
  AND qlht.language(+)               = userenv('LANG')
  AND (upper(qlht.name)             IN ('AMOUNT_LINE_DISCOUNT','NEW PRICE','PERCENT_LINE_DISCOUNT','NEW_PRICE_DISCOUNT'))
  AND NVL(OL.UNIT_LIST_PRICE,0)      >0
  and NVL((OL.UNIT_LIST_PRICE - XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ADJ_AUTO_MODIFIER_AMT(OL.HEADER_ID,OL.LINE_ID)),0) > 0
  and nvl((ol.unit_list_price - xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_amt(ol.header_id,ol.line_id)),0) < 20000
  AND msi.segment1 NOT    IN ('FREIGHTOUT','SHIPPING OUT','REPAIR PARTS','910REBATE','CUTCHARGE','SHIPPING')
  AND MSI.ITEM_TYPE       != 'SPECIAL'
  and ol.ordered_quantity != 0
  and ABS(NVL(OL.UNIT_SELLING_PRICE,0)- (OL.UNIT_LIST_PRICE-XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ADJ_AUTO_MODIFIER_AMT(OL.HEADER_ID,OL.LINE_ID)))>500
/
