---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_INV_BIN_LOC_SALES_TBL $
  Module Name : Inventory
  PURPOSE	  : Bin Location With Sales History
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0 	  18-May-2016         Venu			  TMS#20160503-00088
**************************************************************************************************************/
CREATE TABLE XXEIS.EIS_XXWC_INV_BIN_LOC_SALES_TBL
  (
    PROCESS_ID          NUMBER,
    ORGANIZATION        VARCHAR2(3 BYTE),
    PART_NUMBER         VARCHAR2(40 BYTE),
    DESCRIPTION         VARCHAR2(240 BYTE),
    UOM                 VARCHAR2(3 BYTE),
    SELLING_PRICE       NUMBER,
    VENDOR_NAME         VARCHAR2(240 BYTE),
    VENDOR_NUMBER       VARCHAR2(240 BYTE),
    MIN_MINMAX_QUANTITY NUMBER,
    MAX_MINMAX_QUANTITY NUMBER,
    AVERAGECOST         NUMBER,
    WEIGHT              NUMBER,
    CAT                 VARCHAR2(481 BYTE),
    ONHAND              NUMBER,
    MTD_SALES           NUMBER,
    YTD_SALES           NUMBER,
    PRIMARY_BIN_LOC     VARCHAR2(4000 BYTE),
    ALTERNATE_BIN_LOC   VARCHAR2(4000 BYTE),
    BIN_LOC             VARCHAR2(114 BYTE),
    LOCATION            VARCHAR2(240 BYTE),
    STK                 CHAR(1 BYTE),
    OPEN_ORDER          CHAR(1 BYTE),
    OPEN_DEMAND         CHAR(1 BYTE),
    CATEGORY            VARCHAR2(163 BYTE),
    CATEGORY_SET_NAME   VARCHAR2(30 BYTE),
    INVENTORY_ITEM_ID   NUMBER,
    INV_ORGANIZATION_ID NUMBER,
    ORG_ORGANIZATION_ID NUMBER(15,0),
    APPLICATION_ID      NUMBER(15,0),
    SET_OF_BOOKS_ID     NUMBER(15,0),
    LAST_RECEIVED_DATE  DATE
  )
/




