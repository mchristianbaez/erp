---------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_EIS_VALID_BIN_SALES_N1
  File Name: XXWC_EIS_VALID_BIN_SALES_N1.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        16-May-2016  Venu        --TMS#20160503-00088 Bin Location With Sales History Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX XXEIS.XXWC_EIS_VALID_BIN_SALES_N1 ON XXEIS.EIS_XXWC_VALID_BIN_SALES_ORGS
  (
    ORGANIZATION_ID,
    PROCESS_ID
  )
  TABLESPACE APPS_TS_TX_DATA 
/




