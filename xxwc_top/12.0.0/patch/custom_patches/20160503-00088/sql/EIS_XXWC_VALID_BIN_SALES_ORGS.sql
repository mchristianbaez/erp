---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_VALID_BIN_SALES_ORGS $
  Module Name : Inventory
  PURPOSE	  : Bin Location With Sales History
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0 	  18-May-2016         Venu			  TMS#20160503-00088
**************************************************************************************************************/
CREATE TABLE XXEIS.EIS_XXWC_VALID_BIN_SALES_ORGS
  (
    PROCESS_ID      NUMBER,
    ORGANIZATION_ID NUMBER
  )
/







