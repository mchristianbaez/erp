---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_BIN_SALES_ORGS_TAB $
  Module Name : Inventory
  PURPOSE	  : Bin Location With Sales History
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0 	  18-May-2016         Venu			  TMS#20160503-00088
**************************************************************************************************************/
CREATE TABLE XXEIS.EIS_XXWC_BIN_SALES_ORGS_TAB
  (
    PROCESS_ID        NUMBER,
    INVENTORY_ITEM_ID NUMBER,
    ORGANIZATION_ID   NUMBER
  )
/


