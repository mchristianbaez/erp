---------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_BIN_SALES_ONHAND_TAB_N1
  File Name: XXWC_BIN_SALES_ONHAND_TAB_N1.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        16-May-2016  Venu        --TMS#20160503-00088 Bin Location With Sales History Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX XXEIS.XXWC_BIN_SALES_ONHAND_TAB_N1 ON XXEIS.EIS_XXWC_BIN_SALES_ONHAND_TAB
  (
    INVENTORY_ITEM_ID,
    ORGANIZATION_ID,
    PROCESS_ID
  )
  TABLESPACE APPS_TS_TX_DATA 
/
