---------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_MTL_CATEGORIES_B_N4
  File Name: XXWC_MTL_CATEGORIES_B_N4.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        16-May-2016  Venu        --TMS#20160503-00088 Bin Location With Sales History Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX INV.XXWC_MTL_CATEGORIES_B_N4 ON INV.MTL_CATEGORIES_B (SEGMENT3) TABLESPACE APPS_TS_TX_DATA 
/
