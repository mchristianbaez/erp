CREATE OR REPLACE PACKAGE APPS.XXWC_CUST_UPDATE_FILE_PKG
AS

  /**************************************************************************
     $Header XXWC_CUST_UPDATE_FILE_PKG $
     Module Name: XXWC_CUST_UPDATE_FILE_PKG.pks

     PURPOSE:   This package is called by the concurrent programs
                XXWC Credit Customer Update File Generation
     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       05/12/2016  Neha Saini         Initial Build - Task ID: 20131016-00419
   /*************************************************************************

   /*************************************************************************
     Procedure : generate_file

     PURPOSE   :This procedure creates file for XXWC Credit Customer Update File Generation
     PARAMETERS:  IN - None
                  OUT - errbuf,retcode
     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       05/12/2016  Neha Saini         Initial Build - Task ID: 20131016-00419
   ************************************************************************/
   PROCEDURE generate_file (errbuf OUT VARCHAR2, retcode OUT VARCHAR2);

END XXWC_CUST_UPDATE_FILE_PKG;
/