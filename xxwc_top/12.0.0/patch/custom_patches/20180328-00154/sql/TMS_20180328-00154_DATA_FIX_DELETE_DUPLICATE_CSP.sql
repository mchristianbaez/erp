/******************************************************************************
   NAME:       TMS_20180328-00154_DATA_FIX_DELETE_DUPLICATE_CSP.sql
   PURPOSE:    Delete IN PROGRESS records of csp which are duplicate
   
   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        29/03/2018  Niraj K Ranjan   TMS#20180328-00154   'In Progress' Draft Lines Examples
******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20180328-00154   , Before Update');

   DELETE FROM apps.xxwc_om_csp_notifications_tbl WHERE ROWID IN
     (SELECT aa.rowid FROM 
      apps.xxwc_om_csp_notifications_tbl aa,
      (SELECT MAX(submitted_date) submitted_date,agreement_id,revision_number 
      FROM apps.xxwc_om_csp_notifications_tbl
      WHERE 1=1
	  AND  AGREEMENT_ID in (11539, 10600 ,171364) --127,1,62
      AND  agreement_status = 'IN PROGRESS'
      GROUP BY agreement_id,revision_number
      HAVING COUNT(1) > 1) bb
      WHERE aa.agreement_id = bb.agreement_id
      AND aa.revision_number = bb.revision_number
      AND aa.submitted_date <> bb.submitted_date);
   DBMS_OUTPUT.put_line ('TMS: 20180328-00154    , Total records deleted: '|| SQL%ROWCOUNT);
   
   UPDATE XXWC_OM_CSP_NOTIFICATIONS_TBL xocn
   SET  agreement_status = 'AWAITING_APPROVAL' 
   WHERE 1=1
   AND   xocn.agreement_id = 11539
   AND   xocn.revision_number = 1
   AND   agreement_status IN ('IN PROGRESS');
   DBMS_OUTPUT.put_line ('TMS: 20180328-00154    , Total records Updated for agreement id 11539: '|| SQL%ROWCOUNT);
   
   UPDATE XXWC_OM_CSP_NOTIFICATIONS_TBL xocn
   SET  agreement_status = 'APPROVED' 
   WHERE 1=1
   AND   xocn.agreement_id = 10600
   AND   xocn.revision_number = 6
   AND   agreement_status IN ('IN PROGRESS');
   DBMS_OUTPUT.put_line ('TMS: 20180328-00154    , Total records Updated for agreement id 10600: '|| SQL%ROWCOUNT);
   
   UPDATE XXWC_OM_CSP_NOTIFICATIONS_TBL xocn
   SET  agreement_status = 'APPROVED' 
   WHERE 1=1
   AND   xocn.agreement_id = 171364
   AND   xocn.revision_number = 1
   AND   agreement_status IN ('IN PROGRESS');
   DBMS_OUTPUT.put_line ('TMS: 20180328-00154    , Total records Updated for agreement id 171364: '|| SQL%ROWCOUNT);
   
   COMMIT;
   DBMS_OUTPUT.put_line ('TMS: 20180328-00154    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20180328-00154 , Errors : ' || SQLERRM);
END;
/
