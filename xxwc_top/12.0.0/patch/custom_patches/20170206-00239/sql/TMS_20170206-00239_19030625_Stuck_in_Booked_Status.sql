/*************************************************************************
  $Header TMS_20170206-00239_19030625_Stuck_in_Booked_Status.sql $
  Module Name: TMS_20170206-00239


  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
   1.0       05-APR-2017  Pattabhi Avula        TMS#20170206-00239  
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE

BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170206-00239    , Before Update');

UPDATE apps.oe_order_headers_all
   SET flow_status_code='CLOSED',
       open_flag='N'
 WHERE header_id =36944741;

   DBMS_OUTPUT.put_line (
         'TMS: 20170206-00239  Updated record count: '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170206-00239     , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170206-00239 , Errors : ' || SQLERRM);
END;
/