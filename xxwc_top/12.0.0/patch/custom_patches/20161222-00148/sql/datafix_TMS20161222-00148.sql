 /*************************************************************************
   *   $Header datafix_TMS20161222-00148.sql$
   *   Module Name: datafix_TMS20161222-00148.sql
   *
   *   PURPOSE:   This is a datafix to fix all invoices junk data in attribute12 so 
                that ARS extract can send all invoices .
   *
   *   REVISIONS:
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -------------------------
   *   1.0        19-Dec-2016  Neha Saini             Initial Version TMS# TMS20161222-00148 datafix script
   * ***************************************************************************/
SET SERVEROUTPUT ON
BEGIN
   UPDATE APPS.RA_CUSTOMER_TRX_ALL
      SET attribute12 = NULL,
          last_update_date = SYSDATE,
          last_updated_by = 1290
    WHERE     1 = 1
          AND attribute12 IS NOT NULL
          AND TRUNC (CREATION_DATE) > '16-DEC-16'
          AND LENGTH (attribute12) > 10;

   DBMS_OUTPUT.PUT_LINE (
      'Total records updated in RA_CUSTOMER_TRX_ALL ' || SQL%ROWCOUNT);
   COMMIT;

   UPDATE APPS.RA_INTERFACE_LINES_ALL
      SET HEADER_attribute12 = NULL,
          last_update_date = SYSDATE,
          last_updated_by = 1290
    WHERE  interface_line_context = 'ORDER ENTRY';

   DBMS_OUTPUT.PUT_LINE (
      'Total records updated in RA_INTERFACE_LINES_ALL ' || SQL%ROWCOUNT);
   COMMIT;
END;
/