CREATE OR REPLACE PACKAGE BODY APPS.xxwc_inv_update_dff_pkg
AS
   /*************************************************************************
   *   $Header xxwc_inv_update_dff_pkg $
   *   Module Name: xxwc_inv_update_dff_pkg
   *
   *   PURPOSE:   Package used in Update Item PO and EHS DFF ADI
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        04/01/2012  Shankar Hariharan       Initial Version
   *   2.0        05/13/2013  Lee Spitzer             Updates to for HI Freight Burden
   *   3.0        01/20/2015  Gajendra M              Added UN Number,Hazard Class,Cas Number and Hazmat Materail Flag for TMS#20141229-00015
   *   3.1        01/29/2016  Pattabhi Avula          TMS#20151020-00224 - Shipping Weight Override Upload Tool -- Added >= condition as per 
   *                                                  Ram request for this TMS 
   * ***************************************************************************/
   PROCEDURE update_ehs_attributes (
      p_org_code               IN VARCHAR2,
      p_item_number            IN VARCHAR2,
      p_hazmat_desc            IN VARCHAR2 DEFAULT NULL,
      p_container_type         IN VARCHAR2 DEFAULT NULL,
      p_msds_number            IN VARCHAR2 DEFAULT NULL,
      p_ormd_flag              IN VARCHAR2 DEFAULT NULL,
      p_package_group          IN VARCHAR2 DEFAULT NULL,
      p_pesticide_flag         IN VARCHAR2 DEFAULT NULL,
      p_pesticide_flag_state   IN VARCHAR2 DEFAULT NULL,
      p_ca_prop65              IN VARCHAR2 DEFAULT NULL,
      p_voc_gl                 IN VARCHAR2 DEFAULT NULL,
      p_voc_category           IN VARCHAR2 DEFAULT NULL,
      p_voc_sub_category       IN VARCHAR2 DEFAULT NULL,
      p_un_number              IN VARCHAR2 DEFAULT NULL --Added UN Number for TMS#20141229-00015
                                                       ,
      p_hazard_class           IN VARCHAR2 DEFAULT NULL --Added Hazard Class for TMS#20141229-00015
                                                       ,
      p_cas_number             IN VARCHAR2 DEFAULT NULL --Added Cas Number for TMS#20141229-00015
                                                       ,
      p_hazmat_material_flag   IN VARCHAR2 DEFAULT NULL --Added Hazmat Materail Flag for TMS#20141229-00015
                                                       )
   IS
      -- Commented for Revision 3.0 Begin
      /*
      l_error_msg varchar2(240);
      l_inv_item_id number;
      l_org_id number;
      begin
          select b.organization_id, b.inventory_item_id
            into l_org_id, l_inv_item_id
            from mtl_parameters a, mtl_system_items b
           where a.organization_id=b.organization_id
             and a.organization_code=p_org_code
             and b.segment1=p_item_number;

           update inv.mtl_system_items_b
              set attribute17=p_hazmat_desc,
                  attribute18=p_container_type,
                  attribute8=p_msds_number,
                  attribute11=p_ormd_flag,
                  attribute9=p_package_group,
                  attribute3=p_pesticide_flag,
                  attribute5=p_pesticide_flag_state,
                  attribute1=p_ca_prop65,
                  attribute4=p_voc_gl,
                  attribute6=p_voc_category,
                  attribute7=p_voc_sub_category,
                  attribute_category='WC',
                  last_update_date=sysdate,
                  last_updated_by=fnd_global.user_id
            where inventory_item_id=l_inv_item_id
              and organization_id=l_org_id;
      exception
        when others then
           l_error_msg := substr(sqlerrm,1,240);
           RAISE_APPLICATION_ERROR(-20001,l_error_msg);
      */
      -- Commented for Revision 3.0 End
      -- Added for Revision 3.0 Begin
      -- --------------------------------------------------------------
      -- Declaring Global Exception
      -- --------------------------------------------------------------
      xxwc_error            EXCEPTION;
      -- --------------------------------------------------------------
      -- Declaring local variables
      -- --------------------------------------------------------------
      l_error_msg           VARCHAR2 (240);
      l_inv_item_id         NUMBER;
      l_org_id              NUMBER;
      l_hazmat_desc         VARCHAR2 (240);
      l_con_type            VARCHAR2 (240);
      l_msds_number         VARCHAR2 (240);
      l_ormd_flag           VARCHAR2 (240);
      l_pack_group          VARCHAR2 (240);
      l_pest_flag           VARCHAR2 (240);
      l_pest_state          VARCHAR2 (240);
      l_ca_prop65           VARCHAR2 (240);
      l_voc_gl              VARCHAR2 (240);
      l_voc_cat             VARCHAR2 (240);
      l_sub_cat             VARCHAR2 (240);
      l_un_number_id        NUMBER;
      l_un_number           NUMBER;
      l_hazard_class_id     NUMBER;
      l_hazard_class        NUMBER;
      l_cas_number          VARCHAR2 (30);
      l_haz_material_flag   VARCHAR2 (1);
      l_item_tbl            ego_item_pub.item_tbl_type;
      l_item_table          ego_item_pub.item_tbl_type;
      l_err_callpoint       VARCHAR2 (175) := 'START';
      l_return_status       VARCHAR2 (1);
      l_msg_count           NUMBER (10);
      l_msg_data            VARCHAR2 (1000);
      l_message_list        error_handler.error_tbl_type;
      l_transaction_type    VARCHAR2 (20) := 'UPDATE';
      l_cnt                 NUMBER := 0;
      l_module              VARCHAR2 (100) := 'INV';
      l_error_message       VARCHAR2 (2000);
      l_api_error_msg       VARCHAR2 (4000);
      l_err_callfrom        VARCHAR2 (175) := 'XXWC_INV_UPDATE_DFF_PKG';
      l_distro_list         VARCHAR2 (80)
                               := 'HDSOracleDevelopers@hdsupply.com';
      l_invoke_api          NUMBER := 0;
      l_display_msg         VARCHAR2 (4000) := NULL;
   BEGIN
      l_err_callpoint :=
         'Before Organization Code and Item Number Validation Logic';

      BEGIN
         SELECT b.organization_id,
                b.inventory_item_id,
                b.attribute17,
                b.attribute18,
                b.attribute8,
                b.attribute11,
                b.attribute9,
                b.attribute3,
                b.attribute5,
                b.attribute1,
                b.attribute4,
                b.attribute6,
                b.attribute7,
                (SELECT un_number_id
                   FROM po_un_numbers
                  WHERE    1 = 1 AND un_number IS NULL
                        OR un_number = p_un_number)
                   un_number,
                b.un_number_id,
                (SELECT hazard_class_id
                   FROM po_hazard_classes
                  WHERE    1 = 1 AND hazard_class IS NULL
                        OR hazard_class = p_hazard_class)
                   hazard_class,
                b.hazard_class_id,
                b.cas_number,
                b.hazardous_material_flag
           INTO l_org_id,
                l_inv_item_id,
                l_hazmat_desc,
                l_con_type,
                l_msds_number,
                l_ormd_flag,
                l_pack_group,
                l_pest_flag,
                l_pest_state,
                l_ca_prop65,
                l_voc_gl,
                l_voc_cat,
                l_sub_cat,
                l_un_number,
                l_un_number_id,
                l_hazard_class,
                l_hazard_class_id,
                l_cas_number,
                l_haz_material_flag
           FROM mtl_parameters a, mtl_system_items b
          WHERE     a.organization_id = b.organization_id
                AND a.organization_code = p_org_code
                AND b.segment1 = p_item_number;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_invoke_api := 1;
            l_display_msg :=
                  l_display_msg
               || ' Organization Code '
               || ' Item Number '
               || p_org_code
               || p_item_number
               || ' does not exists.';
      END;

      l_err_callpoint := 'Before Insert or Udpate or Delete in Item API';

      IF l_invoke_api = 0
      THEN
         l_item_tbl (1).transaction_type := l_transaction_type;
         l_item_tbl (1).organization_id := l_org_id;
         l_item_tbl (1).inventory_item_id := l_inv_item_id;
         l_item_tbl (1).attribute17 := NVL (p_hazmat_desc, l_hazmat_desc);
         l_item_tbl (1).attribute18 := NVL (p_container_type, l_con_type);
         l_item_tbl (1).attribute8 := NVL (p_msds_number, l_msds_number);
         l_item_tbl (1).attribute11 := NVL (p_ormd_flag, l_ormd_flag);
         l_item_tbl (1).attribute9 := NVL (p_package_group, l_pack_group);
         l_item_tbl (1).attribute3 := NVL (p_pesticide_flag, l_pest_flag);
         l_item_tbl (1).attribute5 :=
            NVL (p_pesticide_flag_state, l_pest_state);
         l_item_tbl (1).attribute1 := NVL (p_ca_prop65, l_ca_prop65);
         l_item_tbl (1).attribute4 := NVL (p_voc_gl, l_voc_gl);
         l_item_tbl (1).attribute6 := NVL (p_voc_category, l_voc_cat);
         l_item_tbl (1).attribute7 := NVL (p_voc_sub_category, l_sub_cat);
         l_item_tbl (1).un_number_id := NVL (l_un_number, l_un_number_id);
         l_item_tbl (1).hazard_class_id :=
            NVL (l_hazard_class, l_hazard_class_id);
         l_item_tbl (1).cas_number := NVL (p_cas_number, l_cas_number);
         l_item_tbl (1).hazardous_material_flag :=
            NVL (p_hazmat_material_flag, l_haz_material_flag);
         l_item_tbl (1).attribute_category := 'WC';
         l_item_tbl (1).last_update_date := SYSDATE;
         l_item_tbl (1).last_updated_by := fnd_global.user_id;
         l_item_tbl (1).creation_date := SYSDATE;
         l_item_tbl (1).created_by := fnd_global.user_id;
         l_item_tbl (1).last_update_login := fnd_global.login_id;
         ego_item_pub.process_items (p_api_version     => 1.0,
                                     p_init_msg_list   => fnd_api.g_true,
                                     p_commit          => fnd_api.g_true,
                                     p_item_tbl        => l_item_tbl,
                                     x_item_tbl        => l_item_table,
                                     x_return_status   => l_return_status,
                                     x_msg_count       => l_msg_count);

         IF l_return_status <> 'S'
         THEN
            FOR ndx IN 1 .. l_message_list.COUNT
            LOOP
               l_api_error_msg :=
                     l_api_error_msg
                  || ', '
                  || l_message_list (ndx).MESSAGE_TEXT;
            END LOOP;

            l_display_msg :=
               l_display_msg || ' API Error: ' || l_api_error_msg;
            fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
            fnd_message.set_token ('ERROR_MESSAGE', l_display_msg);
            RAISE xxwc_error;
         END IF;

         COMMIT;
      END IF;

      l_err_callpoint := 'Before End';
   EXCEPTION
      WHEN xxwc_error
      THEN
         ROLLBACK;
         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_display_msg);
      WHEN OTHERS
      THEN
         ROLLBACK;
         l_error_message :=
            'Un-Identified error occurred and Development Team is notified';
         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_error_message);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => SUBSTR (l_error_message, 1, 240),
            p_distribution_list   => l_distro_list,
            p_module              => l_module);
   -- Added for Revision 3.0 End

   END update_ehs_attributes;

   PROCEDURE update_po_attributes (p_org_code        IN VARCHAR2,
                                   p_item_number     IN VARCHAR2,
                                   p_reserve_stock   IN NUMBER,
                                   p_import_duty     IN NUMBER,
                                   p_coo             IN VARCHAR2,
                                   p_taxware_code    IN VARCHAR2)
   IS
      l_reserve_stock   NUMBER;
      l_import_duty     NUMBER;
      l_error_msg       VARCHAR2 (240);
      l_inv_item_id     NUMBER;
      l_org_id          NUMBER;
      l_mesg            VARCHAR2 (240);
      l_coo             VARCHAR2 (30);
      l_taxware_code    VARCHAR2 (30);
   BEGIN
      --  l_mesg := 'org='||p_org_code||' and item='||p_item_number;
      SELECT NVL (a.attribute21, '0'),
             NVL (a.attribute25, '0'),
             a.inventory_item_id,
             a.organization_id,
             NVL (a.attribute10, 'X'),
             NVL (a.attribute22, 'X')
        INTO l_reserve_stock,
             l_import_duty,
             l_inv_item_id,
             l_org_id,
             l_coo,
             l_taxware_code
        FROM mtl_system_items a, mtl_parameters b
       WHERE     a.organization_id = b.organization_id
             AND b.organization_code = p_org_code
             AND segment1 = p_item_number;

      --l_mesg := '2=org='||p_org_code||' and item='||p_item_number;

      IF NVL (p_import_duty, .1) > 1
      THEN
         l_error_msg :=
            'Import duty should be entered in decimals and should be less than 1';
         raise_application_error (-20001, l_error_msg);
      END IF;

      IF    l_reserve_stock <> NVL (p_reserve_stock, 0)
         OR l_import_duty <> NVL (p_import_duty, 0)
         OR l_coo <> NVL (p_coo, 'X')
         OR l_taxware_code <> NVL (p_taxware_code, 'X')
      THEN
         --l_mesg := '3=org='||p_org_code||' and item='||p_item_number;
         UPDATE mtl_system_items_b
            SET attribute21 = p_reserve_stock,
                attribute25 = p_import_duty,
                attribute22 = p_taxware_code,
                attribute10 = p_coo,
                attribute_category = 'WC',
                last_update_date = SYSDATE,
                last_updated_by = fnd_global.user_id
          WHERE     inventory_item_id = l_inv_item_id
                AND organization_id = l_org_id;
      END IF;
   --l_mesg := '4=org='||p_org_code||' and item='||p_item_number;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg := l_mesg || '--' || SUBSTR (SQLERRM, 1, 200);
         raise_application_error (-20001, l_error_msg);
   END update_po_attributes;
   
   
   /*************************************************************************
   *   $Header update_hi_ship_weight $
   *   Module Name: update_hi_ship_weight
   *
   *   PURPOSE:   Procedure used in Update Item PO and EHS DFF ADI
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   3.1        01/29/2016  Pattabhi Avula          TMS#20151020-00224 - Shipping Weight Override Upload Tool -- Added >= condition as per 
   *                                                  Ram request for this TMS 
   ***************************************************************************/
   
   PROCEDURE update_hi_ship_weight (p_org_code           IN VARCHAR2,
                                    p_item_number        IN VARCHAR2,
                                    p_item_description   IN VARCHAR2,
                                    p_unit_weight        IN NUMBER,
                                    p_ship_weight        IN NUMBER)
   IS
      l_error_msg     VARCHAR2 (20000);
      l_inv_item_id   NUMBER;
      l_org_id        NUMBER;
      l_mesg          VARCHAR2 (20000);
   BEGIN
      --  l_mesg := 'org='||p_org_code||' and item='||p_item_number;
      SELECT a.inventory_item_id, a.organization_id
        INTO l_inv_item_id, l_org_id
        FROM mtl_system_items a, mtl_parameters b
       WHERE     a.organization_id = b.organization_id
             AND b.organization_code = p_org_code
             AND a.segment1 = p_item_number;

      --l_mesg := '2=org='||p_org_code||' and item='||p_item_number;

      IF NVL (p_ship_weight, 0) < 0
      THEN
         l_error_msg := 'Ship Weight has to be a positive number';
         raise_application_error (-20001, l_error_msg);
      END IF;

      --l_mesg := '3=org='||p_org_code||' and item='||p_item_number;

    --  IF NVL (p_ship_weight, 0) > 0  -- Version# 3.1
	  IF NVL (p_ship_weight, 0) >= 0 -- Version# 3.1
      THEN
         UPDATE mtl_system_items_b
            SET attribute23 = p_ship_weight,
                attribute_category = 'WC',
                last_update_date = SYSDATE,
                last_updated_by = fnd_global.user_id
          WHERE     inventory_item_id = l_inv_item_id
                AND organization_id = l_org_id;
      END IF;
   --l_mesg := '4=org='||p_org_code||' and item='||p_item_number;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg := l_mesg || '--' || SUBSTR (SQLERRM, 1, 200);
         raise_application_error (-20001, l_error_msg);
   END update_hi_ship_weight;
END xxwc_inv_update_dff_pkg;
/