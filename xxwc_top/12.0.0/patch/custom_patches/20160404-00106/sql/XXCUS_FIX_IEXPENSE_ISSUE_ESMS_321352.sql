/*
 TMS: 20160404-00106
 ESMS: 321352 
 Date: 04/06/2016 -- 
*/
set serveroutput on size 1000000;
declare
 --

 --
     procedure print_log(p_message in varchar2) is
     begin
      if apps.fnd_global.conc_request_id >0 then
       apps.fnd_file.put_line(fnd_file.log, p_message);
      else
       dbms_output.put_line(p_message);
      end if;
     end;
 --
begin
     --
     print_log('');     
     print_log('Before create backup table to store the duplicate i-Expense distribution line');
     print_log('');
    --
    execute immediate 'create table xxcus.xxcus_esms_321352_backup as select * from ap.ap_exp_report_dists_all where report_header_id = 839256 and report_line_id =7704216';
    --
     print_log('');     
     print_log('After create backup table to store the duplicate i-Expense distribution line');
     print_log('');  
     --  
     print_log('');     
     print_log('Before delete of duplicate i-Expense distribution line');
     print_log('');
    --     
    delete ap.ap_exp_report_dists_all where report_header_id = 839256 and report_line_id =7704216;
    --
     print_log('');     
     print_log('After delete of duplicate i-Expense distribution line');
     print_log('');
   --    
    commit;
    --
     print_log('');     
     print_log('Commit issued.');
     print_log('');
     --    
exception
 when others then
  print_log('Outer block, message ='||sqlerrm);
end;
/