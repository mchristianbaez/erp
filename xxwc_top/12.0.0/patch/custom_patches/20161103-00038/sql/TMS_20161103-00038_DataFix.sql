/* Formatted on 11/11/2016 7:42:49 PM (QP5 v5.265.14096.38000) */
/*
 TMS: 20161103-00038 Data fix 
 Date: 11/11/2016
 */
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.PUT_LINE ('Before Update');

   UPDATE MTL_CC_ENTRIES_INTERFACE
      SET ACTION_CODE = 14,
          LAST_UPDATE_DATE = SYSDATE,
          LAST_UPDATED_BY = 28474,
          CREATED_BY = 28474,
          employee_id = 107166,
          COUNT_DATE = SYSDATE,
          COUNT_UOM = 'EA',
          PRIMARY_UOM_QUANTITY = 9,
          COUNT_QUANTITY = 9,
          LOCK_FLAG = 2,
          PROCESS_FLAG = 1,
          PROCESS_MODE = 3,
          VALID_FLAG = 1,
          status_flag = 4,
          DELETE_FLAG = 2
    WHERE CC_ENTRY_INTERFACE_ID IN (7348370,
                                    7348369,
                                    7348371,
                                    7348368);

   DBMS_OUTPUT.PUT_LINE ('Number Rows Updated ' || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.PUT_LINE ('After Update');
EXCEPTION
WHEN OTHERS THEN
DBMS_OUTPUT.PUT_LINE('Error occurred '||substr(sqlerrm,1,250));
END;
/