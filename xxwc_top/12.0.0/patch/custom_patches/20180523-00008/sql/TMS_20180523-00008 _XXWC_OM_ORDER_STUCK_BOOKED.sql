/*************************************************************************
  $Header TMS_20180523-00008 _XXWC_OM_ORDER_STUCK_BOOKED.sql $
  Module Name: TMS_XXWC_OM_ORDER_STUCK_BOOKED.sql

  PURPOSE:   Created to process the order headers stucked in booked status
             and lines are in CLOSED or CANCELLED status.

  REVISIONS:
  Ver        Date        Author               Description
  ---------  ----------  ---------------      -------------------------
  1.0        05/30/2018  Pattabhi Avula       TMS#20180523-00008,
                                              TMS#20180521-00243,
                                              TMS#20180521-00070,
											  TMS#20180518-00273,
											  TMS#20180517-00070,
											  TMS#20180525-00225,
											  TMS#20180525-00189,
                                              TMS#20180525-00273
											  Initial Version
**************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE

CURSOR cur_ords
  IS   
   SELECT rowid hdr_rowid, oh.header_id,oh.order_number
     FROM apps.oe_order_headers_all oh 
    WHERE oh.flow_status_code = 'BOOKED' 
	  AND oh.order_type_id IN (1001,1011,1004)
	 -- AND TRUNC(oh.last_update_date) <= TRUNC(SYSDATE-7) 
	 AND oh.order_number IN (28541293,28492558,28384634,28216140,28204405,28382256,28464397,28473069,28384634)
      AND EXISTS (SELECT 1
                    FROM apps.wf_items wi
                   WHERE wi.item_key(+) = oh.header_id
                     AND wi.item_type = 'OEOH'
                     AND  wi.end_date IS NULL)
      AND NOT EXISTS(SELECT 1
                       FROM apps.oe_order_lines_all ol   
                      WHERE ol.header_id=oh.header_id        
                        AND ol.flow_status_code NOT IN ('CLOSED','CANCELLED'))
   UNION
   SELECT rowid hdr_rowid, oh.header_id,oh.order_number 
     FROM apps.oe_order_headers_all oh
    WHERE oh.flow_status_code = 'BOOKED'
	  AND oh.order_type_id IN (1001,1011,1004)
     -- AND TRUNC(oh.last_update_date) <= TRUNC(SYSDATE-7)
      AND oh.order_number IN (28541293,28492558,28384634,28216140,28204405,28382256,28464397,28473069,28384634)	 
      AND NOT EXISTS (SELECT 1
                        FROM APPS.wf_items wi
                       WHERE WI.item_key(+) = oh.header_id
                         AND wi.item_type = 'OEOH')
      AND NOT EXISTS(SELECT 1
                       FROM  apps.oe_order_lines_all ol   
                      WHERE ol.header_id=oh.header_id        
                        AND ol.flow_status_code NOT IN ('CLOSED','CANCELLED'));
						
-- Local variables
 l_script_name                VARCHAR2 (256) := 'XXWC_OM_ORDER_STUCK_BOOKED datafix';
 l_updated_rowcount           NUMBER (20);
 l_sec                        VARCHAR2 (256);
 l_request_id                 NUMBER(38):= fnd_global.conc_request_id;
 l_locked_line                VARCHAR2(2);
 l_curr_date                  DATE DEFAULT SYSDATE-7;
 l_count                      NUMBER(10):=0;
 l_max_date                   DATE;
BEGIN
   DBMS_OUTPUT.put_line ('TMS: Datafix script    , Before Update');
   
   FOR I IN cur_ords 
     LOOP
	 
	 -- Inserting into custom table to track the orders.
	     l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (I.hdr_rowid, 'OE_ORDER_HEADERS_ALL');
      IF l_locked_line = 'N' THEN
		       l_sec := 'Insert record for '||l_script_name;
         xxwc_om_orders_datafix_pkg.insert_record_log( 
	                              p_order_header_id   => I.header_id
			                     ,p_order_line_id     => NULL
			                     ,p_request_id        => l_request_id
			                     ,p_script_name       => l_script_name
			                    );    
	  -- Fetching the line level max last update date.
	  BEGIN
	   SELECT MAX(TRUNC(LAST_UPDATE_DATE))
	     INTO l_max_date
	     FROM apps.oe_order_lines_all 
	    WHERE header_id = i.header_id;
	  EXCEPTION
	   WHEN OTHERS THEN
	     l_max_date:=SYSDATE;
	  END;
	  
	  -- checking the max last update date with sysdate-7 value
	  
	   IF l_max_date <= TRUNC(l_curr_date) THEN 
	    
		  -- Updating the headers table
		  UPDATE oe_order_headers_all
		     SET flow_status_code = 'CLOSED'
                 ,open_flag = 'N'
		   WHERE header_id = I.header_id;
	   END IF;	  
	  l_count:=l_count+1;
	  
	  END IF;  -- Lock checking end if
	  DBMS_OUTPUT.put_line (
         'TMS: Datafix script executed sucessfully for Order : '||i.order_number);
	 END LOOP;
	 DBMS_OUTPUT.put_line (
         'TMS: Datafix script,   Num of Sales orders  updated are: '||l_count);
	 COMMIT;
	  DBMS_OUTPUT.put_line ('TMS: Datafix script  , End Update');
EXCEPTION
 WHEN OTHERS THEN
 DBMS_OUTPUT.put_line ('TMS: 20180523-00008 , Errors : ' || SQLERRM);
END;
/