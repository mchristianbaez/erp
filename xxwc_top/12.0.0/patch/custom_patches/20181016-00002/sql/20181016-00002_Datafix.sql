/***********************************************************************************************************************************************
   NAME:     TMS_20181016-00002_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        10/16/2018  Rakesh Patel     TMS#20181016-00002-Insert missing group2 rental branches information to branch mapping table 
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Insert');

  INSERT INTO xxwc.XXWC_AP_BRANCH_LIST_STG_TBL (
                                      AHH_WAREHOUSE_NUMBER,
                                      ORACLE_BRANCH_NUMBER,
                                      ORACLE_BW_NUMBER
									  )
     VALUES ('R901',
             149,
			'BW549'
             );

   DBMS_OUTPUT.put_line ('Records Inserted -' || SQL%ROWCOUNT);

   INSERT INTO xxwc.XXWC_AP_BRANCH_LIST_STG_TBL (
                                      AHH_WAREHOUSE_NUMBER,
                                      ORACLE_BRANCH_NUMBER,
                                      ORACLE_BW_NUMBER
									  )
     VALUES ('R902',
             148,
			'BW548'
             );

   DBMS_OUTPUT.put_line ('Records Inserted -' || SQL%ROWCOUNT);

   INSERT INTO xxwc.XXWC_AP_BRANCH_LIST_STG_TBL (
                                      AHH_WAREHOUSE_NUMBER,
                                      ORACLE_BRANCH_NUMBER,
                                      ORACLE_BW_NUMBER
									  )
   VALUES ('R064',
              514,
		   'BW514'
             );

   DBMS_OUTPUT.put_line ('Records Inserted -' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to insert record ' || SQLERRM);
	  ROLLBACK;
END;
/