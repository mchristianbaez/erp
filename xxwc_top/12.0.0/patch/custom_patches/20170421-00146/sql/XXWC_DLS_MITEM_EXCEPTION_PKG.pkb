CREATE OR REPLACE PACKAGE BODY DLS.XXWC_DLS_MITEM_EXCEPTION_PKG
IS
   /*********************************************************************************************************************
   -- Package XXWC_DLS_MITEM_EXCEPTION_PKG
   -- *******************************************************************************************************************
   --
   -- PURPOSE: Package is to track exceptions in EDQP DSA at different data validations.
  --           Exception information will be tracked in 'XXWC.XXWC_EDQP_MASSITM_LOAD_EXP_TBL' table.
   -- HISTORY
   -- ===================================================================================================================
   -- ===================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ------------------------------------------------------
   -- 1.0     27-Mar-2015   P.Vamshidhar    Initial version TMS#20150223-00026 - Improve exception error handling in EDQP

   -- 1.1     08-Oct 2015   P.Vamshidhar    TMS#20150817-00105 - EDQP Bundle changes -- Added Validation to validate Vendor

   -- 1.2     24-May-2017   P.Vamshidhar    TMS#20170421-00146 - UPC Validation(EDQP)		   

   ***********************************************************************************************************************/
g_distro_list           VARCHAR2 (100)
                              := 'HDSOracleDevelopers@hdsupply.com';

   /*******************************************************************************************************************************
      Function : EXCEP_BUNDLE_FUNC

     PURPOSE:   To validate mandatory value
     Parameter:
            IN
               p_job_id
               p_id

        REVISIONS:
        Ver        Date        Author                     Description
        ---------  ----------  ---------------    ----------------------------------------------------------------------------------
        1.0        27-Mar-2015  Vamshidhar         Initial Version TMS# 20150223-00026 - Improve exception error handling in EDQP
                                                   Function will concate all exceptions

   *********************************************************************************************************************************/

   FUNCTION EXCEP_BUNDLE_FUNC (P_JOB_ID IN VARCHAR2, P_ID IN VARCHAR2)
      RETURN VARCHAR2
   IS
      t_output   VARCHAR2 (2000);

      CURSOR c1
      IS
           SELECT SUBSTR (ERROR_MESSAGE, INSTR (ERROR_MESSAGE, '-') + 1) DESCR
             FROM DLS.XXWC_EDQP_MASSITM_LOAD_EXP_TBL
            WHERE status = 'EXCEPTION' AND JOB_ID = P_JOB_ID AND ID = P_ID
         ORDER BY TO_NUMBER (
                     SUBSTR (ERROR_MESSAGE,
                             1,
                             INSTR (ERROR_MESSAGE, '-') - 1));

      ln_count   NUMBER := 0;
   BEGIN
      FOR c2 IN c1
      LOOP
         IF ln_count = 0
         THEN
            t_output := TRIM (t_output || ' | ' || c2.descr);
            ln_count := ln_count + 1;
         ELSE
            t_output := TRIM (t_output || ' | ' || c2.descr);
            ln_count := ln_count + 1;
         END IF;
      END LOOP;
      RETURN (SUBSTR (t_output, 1, 2000));
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   /*******************************************************************************************************************************
      Function : IS_UPC_VALID_FUNC

     PURPOSE:   To validate UPC value
     Parameter:
            IN
               p_job_id
               p_column_name
               p_column_value
               p_status
               p_error_message

        REVISIONS:
        Ver        Date          Author                     Description
        ---------  -----------  ---------------    ----------------------------------------------------------------------------------
        1.0        27-Mar-2015   Vamshidhar         Initial Version TMS# 20150223-00026 - Improve exception error handling in EDQP
                                                     To validate UPC
        1.2        24-May-2017   P.Vamshidhar        TMS#20170421-00146 - UPC Validation(EDQP)		   													 
													 
   *********************************************************************************************************************************/


   FUNCTION IS_UPC_VALID_FUNC (P_JOB_ID          IN VARCHAR2,
                               P_COLUMN_NAME     IN VARCHAR2,
                               P_COLUMN_VALUE    IN VARCHAR2,
                               P_STATUS          IN VARCHAR2,
                               P_ERROR_MESSAGE   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_upc_code   VARCHAR2 (100);
      x_ret_mess   VARCHAR2 (1000);	  
   BEGIN
      l_upc_code :=
         TRIM (SUBSTR (P_COLUMN_VALUE, INSTR (P_COLUMN_VALUE, '|||', 1) + 3));

     /*	commented below code in 1.2	 
      IF LENGTH (l_upc_code) NOT IN (12, 13) AND l_upc_code IS NOT NULL
      THEN
         INSERT INTO DLS.XXWC_EDQP_MASSITM_LOAD_EXP_TBL (JOB_ID,
                                                         ID,
                                                         VALIDATION_COLUMN,
                                                         COLUMN_VALUE,
                                                         STATUS,
                                                         ERROR_MESSAGE)
                 VALUES (
                           P_JOB_ID,
                           SUBSTR (P_COLUMN_VALUE,
                                   1,
                                   INSTR (P_COLUMN_VALUE, '|||', 1) - 1),
                           P_COLUMN_NAME,
                           SUBSTR (P_COLUMN_VALUE,
                                   INSTR (P_COLUMN_VALUE, '|||', 1) + 3),
                           P_STATUS,
                           P_ERROR_MESSAGE);

         COMMIT;
      ELSE
	  */
         IF l_upc_code IS NOT NULL
         THEN
            dls.xxwc_dls_mitem_exception_pkg.edqp_upc_validation (
               l_upc_code,
               X_RET_MESS);

			IF X_RET_MESS IS NOT NULL THEN
            INSERT
              INTO DLS.XXWC_EDQP_MASSITM_LOAD_EXP_TBL (JOB_ID,
                                                       ID,
                                                       VALIDATION_COLUMN,
                                                       COLUMN_VALUE,
                                                       STATUS,
                                                       ERROR_MESSAGE)
               VALUES (
                         P_JOB_ID,
                         SUBSTR (P_COLUMN_VALUE,
                                 1,
                                 INSTR (P_COLUMN_VALUE, '|||', 1) - 1),
                         P_COLUMN_NAME,
                         SUBSTR (P_COLUMN_VALUE,
                                 INSTR (P_COLUMN_VALUE, '|||', 1) + 3),
                         P_STATUS,
                         x_ret_mess);						 
          COMMIT;
         END IF;		  
         END IF;

		 RETURN 'UPC';
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'UPC';
   END;


   /*******************************************************************************************************************************
      Function : MANDATORY_VALUE_FUNC

     PURPOSE:   To validate mandatory value
     Parameter:
            IN
               p_job_id
               p_column_name
               p_column_value
               p_status
               p_error_message

        REVISIONS:
        Ver        Date           Author                     Description
        ---------  ------------  ---------------    ----------------------------------------------------------------------------------
        1.0        27-Mar-2015    Vamshidhar         Initial Version TMS# 20150223-00026 - Improve exception error handling in EDQP
                                                     To validate Mandatory value.

   *********************************************************************************************************************************/

   FUNCTION MANDATORY_VALUE_FUNC (P_JOB_ID          IN VARCHAR2,
                                  P_COLUMN_NAME     IN VARCHAR2,
                                  P_COLUMN_VALUE    IN VARCHAR2,
                                  P_STATUS          IN VARCHAR2,
                                  P_ERROR_MESSAGE   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_column_value   VARCHAR2 (100);
   BEGIN
      l_column_value :=
         SUBSTR (P_COLUMN_VALUE, INSTR (P_COLUMN_VALUE, '|||', 1) + 3);

      IF TRIM (
            REPLACE (
               SUBSTR (P_COLUMN_VALUE, INSTR (P_COLUMN_VALUE, '|||', 1) + 3),
               ' ',
               ''))
            IS NULL
      THEN
         INSERT INTO DLS.XXWC_EDQP_MASSITM_LOAD_EXP_TBL (JOB_ID,
                                                         ID,
                                                         VALIDATION_COLUMN,
                                                         COLUMN_VALUE,
                                                         STATUS,
                                                         ERROR_MESSAGE)
                 VALUES (
                           P_JOB_ID,
                           SUBSTR (P_COLUMN_VALUE,
                                   1,
                                   INSTR (P_COLUMN_VALUE, '|||', 1) - 1),
                           P_COLUMN_NAME,
                           SUBSTR (P_COLUMN_VALUE,
                                   INSTR (P_COLUMN_VALUE, '|||', 1) + 3),
                           P_STATUS,
                           P_ERROR_MESSAGE);

         COMMIT;
      END IF;

      RETURN 'MANDATORY';
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'MANDATORY';
   END;

   /*******************************************************************************************************************************
      Function : hazmat_value_func

     PURPOSE:   To validate hazmat value
     Parameter:
            IN
               p_job_id
               p_column_name
               p_column_value
               p_status
               p_error_message

        REVISIONS:
        Ver        Date           Author                     Description
        ---------  ------------  ---------------    ----------------------------------------------------------------------------------
        1.0        27-Mar-2015    Vamshidhar         Initial Version TMS# 20150223-00026 - Improve exception error handling in EDQP
                                                     To validate Hazmat value.

   *********************************************************************************************************************************/

   FUNCTION hazmat_value_func (P_JOB_ID          IN VARCHAR2,
                               P_COLUMN_NAME     IN VARCHAR2,
                               P_COLUMN_VALUE    IN VARCHAR2,
                               P_STATUS          IN VARCHAR2,
                               P_ERROR_MESSAGE   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_column_value   VARCHAR2 (100);
   BEGIN
      l_column_value :=
         SUBSTR (P_COLUMN_VALUE, INSTR (P_COLUMN_VALUE, '|||', 1) + 3);

      IF    TRIM (
               REPLACE (
                  SUBSTR (P_COLUMN_VALUE,
                          INSTR (P_COLUMN_VALUE, '|||', 1) + 3),
                  ' ',
                  ''))
               IS NULL
         OR (UPPER (SUBSTR (l_column_value, 1, 1)) NOT IN ('Y', 'N'))
      THEN
         INSERT INTO DLS.XXWC_EDQP_MASSITM_LOAD_EXP_TBL (JOB_ID,
                                                         ID,
                                                         VALIDATION_COLUMN,
                                                         COLUMN_VALUE,
                                                         STATUS,
                                                         ERROR_MESSAGE)
                 VALUES (
                           P_JOB_ID,
                           SUBSTR (P_COLUMN_VALUE,
                                   1,
                                   INSTR (P_COLUMN_VALUE, '|||', 1) - 1),
                           P_COLUMN_NAME,
                           SUBSTR (P_COLUMN_VALUE,
                                   INSTR (P_COLUMN_VALUE, '|||', 1) + 3),
                           P_STATUS,
                           P_ERROR_MESSAGE);

         COMMIT;
      END IF;

      RETURN 'MANDATORY';
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'MANDATORY';
   END;


   /*******************************************************************************************************************************
      Function : VALIDATE_BRAND_FUNC

     PURPOSE:   To validate Brand value
     Parameter:
            IN
               p_job_id
               p_column_name
               p_column_value
               p_status
               p_error_message

        REVISIONS:
        Ver        Date           Author                     Description
        ---------  ------------  ---------------    ----------------------------------------------------------------------------------
        1.0        27-Mar-2015    Vamshidhar         Initial Version TMS# 20150223-00026 - Improve exception error handling in EDQP
                                                     To validate Brand value.

   *********************************************************************************************************************************/

   FUNCTION VALIDATE_BRAND_FUNC (P_JOB_ID          IN VARCHAR2,
                                 P_COLUMN_NAME     IN VARCHAR2,
                                 P_COLUMN_VALUE    IN VARCHAR2,
                                 P_STATUS          IN VARCHAR2,
                                 P_ERROR_MESSAGE   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_column_value   VARCHAR2 (100);
      l_count          NUMBER := 0;
   BEGIN
      l_column_value :=
         TRIM (SUBSTR (P_COLUMN_VALUE, INSTR (P_COLUMN_VALUE, '|||', 1) + 3));

      IF REPLACE (l_column_value, ' ', '') IS NULL
      THEN
         RETURN 'N';
      END IF;

      SELECT COUNT (1)
        INTO l_count
        FROM apps.fnd_flex_value_sets a, apps.fnd_flex_values b
       WHERE     a.flex_value_set_id = b.flex_value_set_id
             AND a.flex_value_set_name = 'XXWC_ITEM_BRAND'
             AND b.flex_value = l_column_value
             AND b.enabled_flag = 'Y';

      IF l_count = 0
      THEN
         INSERT INTO DLS.XXWC_EDQP_MASSITM_LOAD_EXP_TBL (JOB_ID,
                                                         ID,
                                                         VALIDATION_COLUMN,
                                                         COLUMN_VALUE,
                                                         STATUS,
                                                         ERROR_MESSAGE)
                 VALUES (
                           P_JOB_ID,
                           SUBSTR (P_COLUMN_VALUE,
                                   1,
                                   INSTR (P_COLUMN_VALUE, '|||', 1) - 1),
                           P_COLUMN_NAME,
                           SUBSTR (P_COLUMN_VALUE,
                                   INSTR (P_COLUMN_VALUE, '|||', 1) + 3),
                           P_STATUS,
                           P_ERROR_MESSAGE);

         COMMIT;
      END IF;

      RETURN 'BRAND';
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'BRAND';
   END;

   /*******************************************************************************************************************************
      Function : IS_COUNTRY_CODE_VALID_FUNC

     PURPOSE:   To validate country value
     Parameter:
            IN
               p_job_id
               p_column_name
               p_column_value
               p_status
               p_error_message

        REVISIONS:
        Ver        Date           Author                     Description
        ---------  ------------  ---------------    ----------------------------------------------------------------------------------
        1.0        27-Mar-2015    Vamshidhar         Initial Version TMS# 20150223-00026 - Improve exception error handling in EDQP
                                                     To validate country value.

   *********************************************************************************************************************************/

   FUNCTION IS_COUNTRY_CODE_VALID_FUNC (P_JOB_ID          IN VARCHAR2,
                                        P_COLUMN_NAME     IN VARCHAR2,
                                        P_COLUMN_VALUE    IN VARCHAR2,
                                        P_STATUS          IN VARCHAR2,
                                        P_ERROR_MESSAGE   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_country_code   apps.fnd_lookup_values.meaning%TYPE;
   BEGIN
      l_country_code :=
         SUBSTR (P_COLUMN_VALUE, INSTR (P_COLUMN_VALUE, '|||', 1) + 3);

      IF SUBSTR (l_country_code, 1, 1) = 'N'
      THEN
         INSERT INTO DLS.XXWC_EDQP_MASSITM_LOAD_EXP_TBL (JOB_ID,
                                                         ID,
                                                         VALIDATION_COLUMN,
                                                         COLUMN_VALUE,
                                                         STATUS,
                                                         ERROR_MESSAGE)
                 VALUES (
                           P_JOB_ID,
                           SUBSTR (P_COLUMN_VALUE,
                                   1,
                                   INSTR (P_COLUMN_VALUE, '|||', 1) - 1),
                           P_COLUMN_NAME,
                           SUBSTR (P_COLUMN_VALUE,
                                   INSTR (P_COLUMN_VALUE, '|||', 1) + 3),
                           P_STATUS,
                           P_ERROR_MESSAGE);

         COMMIT;

         RETURN (SUBSTR (P_COLUMN_VALUE,
                         INSTR (P_COLUMN_VALUE, '|||', 1) + 5));
      END IF;

      RETURN (SUBSTR (P_COLUMN_VALUE, INSTR (P_COLUMN_VALUE, '|||', 1) + 3));
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'COUNTRY';
   END;

   /*******************************************************************************************************************************
      Function : IS_UOM_VALID_FUNC

     PURPOSE:   To validate UOM value
     Parameter:
            IN
               p_job_id
               p_column_name
               p_column_value
               p_status
               p_error_message

        REVISIONS:
        Ver        Date           Author                     Description
        ---------  ------------  ---------------    ----------------------------------------------------------------------------------
        1.0        27-Mar-2015    Vamshidhar         Initial Version TMS# 20150223-00026 - Improve exception error handling in EDQP
                                                     To validate UOM value.

   *********************************************************************************************************************************/

   FUNCTION IS_UOM_VALID_FUNC (P_JOB_ID          IN VARCHAR2,
                               P_COLUMN_NAME     IN VARCHAR2,
                               P_COLUMN_VALUE    IN VARCHAR2,
                               P_STATUS          IN VARCHAR2,
                               P_ERROR_MESSAGE   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_primary_uom_code   apps.mtl_units_of_measure_tl.unit_of_measure_tl%TYPE;
   BEGIN
      l_primary_uom_code :=
         TRIM (SUBSTR (P_COLUMN_VALUE, INSTR (P_COLUMN_VALUE, '|||', 1) + 3));

      IF SUBSTR (l_primary_uom_code, 1, 1) = 'N'
      THEN
         INSERT INTO DLS.XXWC_EDQP_MASSITM_LOAD_EXP_TBL (JOB_ID,
                                                         ID,
                                                         VALIDATION_COLUMN,
                                                         COLUMN_VALUE,
                                                         STATUS,
                                                         ERROR_MESSAGE)
                 VALUES (
                           P_JOB_ID,
                           SUBSTR (P_COLUMN_VALUE,
                                   1,
                                   INSTR (P_COLUMN_VALUE, '|||', 1) - 1),
                           P_COLUMN_NAME,
                           SUBSTR (P_COLUMN_VALUE,
                                   INSTR (P_COLUMN_VALUE, '|||', 1) + 3),
                           P_STATUS,
                           P_ERROR_MESSAGE);

         COMMIT;
         RETURN (SUBSTR (P_COLUMN_VALUE,
                         INSTR (P_COLUMN_VALUE, '|||', 1) + 5));
      END IF;

      RETURN (SUBSTR (P_COLUMN_VALUE, INSTR (P_COLUMN_VALUE, '|||', 1) + 3));
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'UOM';
   END;

   /*******************************************************************************************************************************
      Function : IS_ITEM_VALID_FUNC

     PURPOSE:   To validate Item number.
     Parameter:
            IN
               p_job_id
               p_column_name
               p_column_value
               p_status
               p_error_message

        REVISIONS:
        Ver        Date           Author                     Description
        ---------  ------------  ---------------    ----------------------------------------------------------------------------------
        1.0        27-Mar-2015    Vamshidhar         Initial Version TMS# 20150223-00026 - Improve exception error handling in EDQP
                                                     To validate Item number.

   *********************************************************************************************************************************/

   FUNCTION IS_ITEM_VALID_FUNC (P_JOB_ID          IN VARCHAR2,
                                P_COLUMN_NAME     IN VARCHAR2,
                                P_COLUMN_VALUE    IN VARCHAR2,
                                P_STATUS          IN VARCHAR2,
                                P_ERROR_MESSAGE   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_item_code   VARCHAR2 (1000);
   BEGIN
      l_item_code :=
         TRIM (SUBSTR (P_COLUMN_VALUE, INSTR (P_COLUMN_VALUE, '|||', 1) + 3));



      IF SUBSTR (l_item_code, 1, 1) = 'Y'
      THEN
         INSERT INTO DLS.XXWC_EDQP_MASSITM_LOAD_EXP_TBL (JOB_ID,
                                                         ID,
                                                         VALIDATION_COLUMN,
                                                         COLUMN_VALUE,
                                                         STATUS,
                                                         ERROR_MESSAGE)
                 VALUES (
                           P_JOB_ID,
                           SUBSTR (P_COLUMN_VALUE,
                                   1,
                                   INSTR (P_COLUMN_VALUE, '|||', 1) - 1),
                           P_COLUMN_NAME,
                           SUBSTR (P_COLUMN_VALUE,
                                   INSTR (P_COLUMN_VALUE, '|||', 1) + 3),
                           P_STATUS,
                           P_ERROR_MESSAGE);

         COMMIT;

         RETURN (SUBSTR (P_COLUMN_VALUE,
                         INSTR (P_COLUMN_VALUE, '|||', 1) + 5));
      END IF;

      RETURN (SUBSTR (P_COLUMN_VALUE, INSTR (P_COLUMN_VALUE, '|||', 1) + 3));
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'ITEM';
   END;

   /*******************************************************************************************************************************
      Function : IS_ICC_CAT_VALID_FUNC

     PURPOSE:   To validate Item ICC.
     Parameter:
            IN
               p_job_id
               p_column_name
               p_column_value
               p_status
               p_error_message

        REVISIONS:
        Ver        Date           Author                     Description
        ---------  ------------  ---------------    ----------------------------------------------------------------------------------
        1.0        27-Mar-2015    Vamshidhar         Initial Version TMS# 20150223-00026 - Improve exception error handling in EDQP
                                                     To Item ICC value.

   *********************************************************************************************************************************/


   FUNCTION IS_ICC_CAT_VALID_FUNC (P_JOB_ID          IN VARCHAR2,
                                   P_COLUMN_NAME     IN VARCHAR2,
                                   P_COLUMN_VALUE    IN VARCHAR2,
                                   P_STATUS          IN VARCHAR2,
                                   P_ERROR_MESSAGE   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_icc_code   VARCHAR2 (100);
   BEGIN
      l_icc_code :=
         TRIM (SUBSTR (P_COLUMN_VALUE, INSTR (P_COLUMN_VALUE, '|||', 1) + 3));

      IF SUBSTR (l_icc_code, 1, 1) = 'N'
      THEN
         INSERT INTO DLS.XXWC_EDQP_MASSITM_LOAD_EXP_TBL (JOB_ID,
                                                         ID,
                                                         VALIDATION_COLUMN,
                                                         COLUMN_VALUE,
                                                         STATUS,
                                                         ERROR_MESSAGE)
                 VALUES (
                           P_JOB_ID,
                           SUBSTR (P_COLUMN_VALUE,
                                   1,
                                   INSTR (P_COLUMN_VALUE, '|||', 1) - 1),
                           P_COLUMN_NAME,
                           SUBSTR (P_COLUMN_VALUE,
                                   INSTR (P_COLUMN_VALUE, '|||', 1) + 3),
                           P_STATUS,
                           P_ERROR_MESSAGE);

         COMMIT;
         RETURN (SUBSTR (P_COLUMN_VALUE,
                         INSTR (P_COLUMN_VALUE, '|||', 1) + 5));
      END IF;

      RETURN (SUBSTR (P_COLUMN_VALUE, INSTR (P_COLUMN_VALUE, '|||', 1) + 3));
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'ICC';
   END;

   -- Added below function in Rev 1.1 by Vamshi.

   FUNCTION IS_XREF_VENDOR_FUNC (P_JOB_ID          IN VARCHAR2,
                                 P_COLUMN_NAME     IN VARCHAR2,
                                 P_COLUMN_VALUE    IN VARCHAR2,
                                 P_STATUS          IN VARCHAR2,
                                 P_ERROR_MESSAGE   IN VARCHAR2)
      RETURN VARCHAR2
   /*******************************************************************************************************************************
      Function : IS_XREF_VENDOR_FUNC

     PURPOSE:   To validate Vendor Number.
     Parameter:
            IN
               p_job_id
               p_column_name
               p_column_value
               p_status
               p_error_message

        REVISIONS:
        Ver        Date           Author                     Description
        ---------  ------------  ---------------    ----------------------------------------------------------------------------------
        1.1        08-Oct-2015    Vamshidhar        TMS#20150817-00105 - EDQP Bundle changes -- Added Validation to validate Vendor

   *********************************************************************************************************************************/
   IS
      tcount       NUMBER;
      lvc_vendor   apps.ap_suppliers.segment1%TYPE;
   BEGIN
      lvc_vendor :=
         TRIM (SUBSTR (P_COLUMN_VALUE, INSTR (P_COLUMN_VALUE, '|||', 1) + 3));


      SELECT COUNT (1)
        INTO tcount
        FROM APPS.AP_SUPPLIERS
       WHERE SEGMENT1 = NVL (lvc_vendor, 0) AND ENABLED_flag = 'Y';

      IF tcount = 0 and lvc_vendor is not null
      THEN
         INSERT INTO DLS.XXWC_EDQP_MASSITM_LOAD_EXP_TBL (JOB_ID,
                                                         ID,
                                                         VALIDATION_COLUMN,
                                                         COLUMN_VALUE,
                                                         STATUS,
                                                         ERROR_MESSAGE)
                 VALUES (
                           P_JOB_ID,
                           SUBSTR (P_COLUMN_VALUE,
                                   1,
                                   INSTR (P_COLUMN_VALUE, '|||', 1) - 1),
                           P_COLUMN_NAME,
                           SUBSTR (P_COLUMN_VALUE,
                                   INSTR (P_COLUMN_VALUE, '|||', 1) + 3),
                           P_STATUS,
                           P_ERROR_MESSAGE);
                           
         COMMIT;
         RETURN (SUBSTR (P_COLUMN_VALUE,
                         INSTR (P_COLUMN_VALUE, '|||', 1) + 5));
      END IF;

      RETURN (SUBSTR (P_COLUMN_VALUE, INSTR (P_COLUMN_VALUE, '|||', 1) + 3));
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'VENDOR';
   END;
   
   /*******************************************************************************************************************************
      PROCEDURE : EDQP_UPC_VALIDATION

      REVISIONS:
     Ver        Date        Author                     Description
    ---------  ----------  ---------------    ----------------------------------------------------------------------------------

     -- 1.2    24-May-2017   P.Vamshidhar     TMS#20170421-00146 - UPC Validation(EDQP)
   *********************************************************************************************************************************/

   PROCEDURE EDQP_UPC_VALIDATION (P_CROSS_REF   IN     VARCHAR2,
                                  X_RET_MESS       OUT VARCHAR2)
   IS
      lvc_return_status   BOOLEAN;
      lvc_invalid_UPC     VARCHAR2 (300);
      l_procedure         VARCHAR2 (100) := 'EDQP_UPC_VALIDATION';
      l_sec               VARCHAR2 (300);
      lvc_item_number     VARCHAR2 (30);
      lvc_ret_mess1       VARCHAR2 (300) := ' is associated with item';
      lvc_ret_mess2       VARCHAR2 (300) := '.  Please re-validate UPC';
      lvc_ret_upc         VARCHAR2 (300);
      lvc_ret_item        VARCHAR2 (300);
      ln_count            NUMBER;
      ln_login_id         NUMBER := fnd_global.login_id;
      lvc_upc_exist       VARCHAR2 (1);
   BEGIN
      l_sec := ' Procedure Start - Cross Reference ' || P_CROSS_REF;

      IF P_CROSS_REF IS NULL OR P_CROSS_REF = 'UPCEXEMPT'
      THEN
         x_ret_mess := NULL;
      ELSE
         lvc_return_status := NULL;
         lvc_return_status :=
            APPS.xxwc_pim_chg_item_mgmt.upc_validation (p_cross_ref);

         IF (lvc_return_status)
         THEN
            BEGIN
               l_sec :=
                  ' Deriving Item number for Cross Reference ' || p_cross_ref;

               SELECT B.SEGMENT1
                 INTO lvc_item_number
                 FROM APPS.MTL_CROSS_REFERENCES A, APPS.MTL_SYSTEM_ITEMS_B B
                WHERE     A.INVENTORY_ITEM_ID = B.INVENTORY_ITEM_ID
                      AND B.ORGANIZATION_ID = 222
                      AND A.CROSS_REFERENCE_TYPE IN ('UPC',
                                                     'EAN',
                                                     'PENDING',
                                                     'GTIN')
                      AND A.CROSS_REFERENCE = p_cross_ref
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  lvc_item_number := NULL;
            END;

            IF lvc_item_number IS NOT NULL
            THEN
               X_RET_MESS :=
                     'UPC'
                  || P_CROSS_REF
                  || ' is associated to item '
                  || lvc_item_number;
            ELSE
               X_RET_MESS := NULL;
            END IF;
         ELSE
            X_RET_MESS := 'The UPC value ' || P_CROSS_REF || ' is invalid.';
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
       X_RET_MESS := NULL;
   END;
END XXWC_DLS_MITEM_EXCEPTION_PKG;
/