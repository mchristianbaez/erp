/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header Alter XXWC_MD_SEARCH_PRODUCT_GTT_TBL$
  Module Name: XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     10-Oct-2016   Pahwa, Nancy                Initially Created TMS# 20160801-00182  
**************************************************************************/
alter table xxwc.XXWC_MD_SEARCH_PRODUCT_GTT_TBL add open_sales_orders NUMBER;
alter table xxwc.XXWC_MD_SEARCH_PRODUCT_GTT_TBL add reservable NUMBER;
alter table xxwc.XXWC_MD_SEARCH_PRODUCT_GTT_TBL add list_price_2  NUMBER;