/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header APPS.XXWC_MD_PRODUCT_STORE_PREF1 $
  Module Name: APPS.XXWC_MD_PRODUCT_STORE_PREF1

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     1-SEP-2015   Pahwa, Nancy                Initially Created 
TMS# 20160801-00182  
**************************************************************************/

begin   
  ctx_ddl.create_preference ('XXWC_MD_PRODUCT_STORE_PREF1', 'multi_column_datastore');  
  ctx_ddl.set_attribute   
    ('XXWC_MD_PRODUCT_STORE_PREF1',   
     'columns',   
     'partnumber2,   
      shortdescription');  
  ctx_ddl.create_preference ('XXWC_MD_PRODUCT_STORE_LEX2', 'basic_lexer');  
  ctx_ddl.set_attribute ('XXWC_MD_PRODUCT_STORE_LEX2', 'whitespace', '/\|-_+,');  
  ctx_ddl.create_section_group ('XXWC_MD_PRODUCT_STORE_SG1', 'basic_section_group');
  ctx_ddl.add_sdata_section('XXWC_MD_PRODUCT_STORE_SG1', 'partnumber2', 'partnumber2', 'VARCHAR2'); 
  ctx_ddl.add_field_section ('XXWC_MD_PRODUCT_STORE_SG1', 'shortdescription', 'shortdescription', true); 
  end;
/