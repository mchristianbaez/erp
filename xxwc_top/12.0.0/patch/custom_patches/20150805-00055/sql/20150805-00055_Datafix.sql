-- TMS# 20150805-00055 By Manjula on 5-Aug-15
-- Reprocess the stuck Records freight Invoice Import

SET SERVEROUTPUT ON SIZE 1000000

BEGIN

	dbms_output.put_line('Before Update');

	UPDATE xxcus.XXCUSAP_TMS_INV_STG_TBL
	SET status = 'NEW_1'
	WHERE status = 'NEW';

	dbms_output.put_line('After Update with Status NEW_1: No Of records '||SQL%ROWCOUNT);

	commit;

	dbms_output.put_line('Before Update of Batch_no TMS_04-AUG-2015-1320');

	UPDATE xxcus.XXCUSAP_TMS_INV_STG_TBL
	SET status = 'NEW'
	WHERE status = 'NEW_1'
	AND batch_no = 'TMS_04-AUG-2015-1320';

	dbms_output.put_line('After Update with Status NEW: No Of records '||SQL%ROWCOUNT);

	commit;

	EXCEPTION WHEN OTHERS THEN

	dbms_output.put_line('Error in Update '||SQLERRM);

END;
/