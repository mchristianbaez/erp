  set serveroutput on;
 /*************************************************************************
      PURPOSE:  Cancel 26770748

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        29-JAN-2018  Sundaramoorthy     Initial Version - TMS #20180117-00002   
	************************************************************************/ 
 BEGIN
 dbms_output.put_line ('Start Update ');

 UPDATE oe_order_headers_all
  SET  flow_status_code ='CLOSED'
  , open_flag ='N'
   WHERE header_id =66837604;  
  
  dbms_output.put_line ('Update count '||SQL%ROWCOUNT);
  COMMIT;
  
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
	dbms_output.put_line ('Inside Exception '|| SUBSTR(SQLERRM,1,2000));	
END;
/	