  /* *************************************************************************************************************
  $Header XXEIS.XXWC_VENDOR_QUOTE_TEMP1.sql $
  PURPOSE: This Table is used in a view  xxeis.eis_xxwc_om_vendor_quote_v for Vendor Quote Batch Summary Report
  TMS Task Id : 20160226-00011  
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        02/26/2015  Manjula Chellappan    20160226-00011  - To revert the changes from 20150814-00022
  ************************************************************************************************************* */
ALTER TABLE XXEIS.XXWC_VENDOR_QUOTE_TEMP1 DROP COLUMN list_price_per_unit ;
/