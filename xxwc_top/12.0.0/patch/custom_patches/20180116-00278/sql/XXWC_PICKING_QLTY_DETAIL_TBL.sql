--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: XXWC_PICKING_QLTY_DETAIL_TBL
  Description: This table is used to get data from XXEIS.XXWC_PICKING_QLTY_CNTRL_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     28-Mar-2018        Siva   		TMS#TMS#20180116-00278
********************************************************************************/
DROP TABLE XXEIS.XXWC_PICKING_QLTY_DETAIL_TBL CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE XXEIS.XXWC_PICKING_QLTY_DETAIL_TBL 
   (	REPORT_TYPE VARCHAR2(10 BYTE), 
	BRANCH VARCHAR2(3 BYTE), 
	DISTRICT VARCHAR2(150 BYTE), 
	REGION VARCHAR2(150 BYTE), 
	ASSOCIATE_NAME VARCHAR2(200 BYTE), 
	TOTAL_NO_OF_ORDER_BY_ASSOCIATE NUMBER, 
	TOTAL_NO_OF_LINES_BY_ASSOCIATE NUMBER, 
	AVGTIME_PER_ORDER_BY_ASSOCIATE NUMBER, 
	AVGTIME_PER_LINE_BY_ASSOCIATE NUMBER, 
	PICK_ACCURACY_ASSOCIATE_PERCNT NUMBER, 
	NATION_WIDE_AVGTIME_PER_LINE NUMBER, 
	ORGANIZATION_ID NUMBER, 
	OPERATING_UNIT NUMBER
   ) ON COMMIT PRESERVE ROWS 
/
