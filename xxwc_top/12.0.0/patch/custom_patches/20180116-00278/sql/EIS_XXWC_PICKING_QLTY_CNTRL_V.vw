-------------------------------------------------------------------------------------------
/*****************************************************************************************
  $Header XXEIS.EIS_XXWC_SUBINV_TRANSFER_V.vw $
  Module Name: Order Management
  PURPOSE: Picking Quality Control Metrics Report - WC
  REVISIONS:
  Ver          Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0		 3/28/2018      Siva			TMS#20180116-00278
******************************************************************************************/
---------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_PICKING_QLTY_CNTRL_V (REPORT_TYPE, BRANCH, DISTRICT, REGION, AVG_TIME_PER_ORDER, BR_TIME_PER_ORDER, AVG_TIME_PER_LINE, BR_TIME_PER_LINE, PICKING_ACCURACY_PERCENT, BR_PICKING_ACCURACY_PERCENT, TOTAL_NO_OF_ORDERS, TOTAL_NO_OF_LINES, OVERALL_BRANCH_RANKING, OVERALL_DISTRICT_RANKING, OVERALL_REGIONAL_RANKING, ASSOCIATE_NAME, TOTAL_NO_OF_ORDER_BY_ASSOCIATE, TOTAL_NO_OF_LINES_BY_ASSOCIATE, AVGTIME_PER_ORDER_BY_ASSOCIATE, AVGTIME_PER_LINE_BY_ASSOCIATE, PICK_ACCURACY_ASSOCIATE_PERCNT, SPEED_RANKING_PERCENT, OVERALL_RANKING_FOR_ASSOCIATE, ORGANIZATION_ID)
AS
  SELECT REPORT_TYPE,
    BRANCH ,
    DISTRICT ,
    REGION ,
    AVG_TIME_PER_ORDER ,
    BR_TIME_PER_ORDER ,
    AVG_TIME_PER_LINE ,
    BR_TIME_PER_LINE ,
    PICKING_ACCURACY_PERCENT ,
    BR_PICKING_ACCURACY_PERCENT ,
    TOTAL_NO_OF_ORDERS ,
    TOTAL_NO_OF_LINES ,
    OVERALL_BRANCH_RANKING ,
    OVERALL_DISTRICT_RANKING ,
    OVERALL_REGIONAL_RANKING ,
    ASSOCIATE_NAME ,
    TOTAL_NO_OF_ORDER_BY_ASSOCIATE ,
    TOTAL_NO_OF_LINES_BY_ASSOCIATE ,
    AVGTIME_PER_ORDER_BY_ASSOCIATE ,
    AVGTIME_PER_LINE_BY_ASSOCIATE ,
    PICK_ACCURACY_ASSOCIATE_PERCNT ,
    speed_ranking_percent ,
    overall_ranking_for_associate,
    organization_id
  FROM XXEIS.XXWC_PICKING_QLTY_CNTRL_TBL
/
  