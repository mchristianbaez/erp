--Run this file in XXEIS user
-----------------------------------------------------------------------------------------------------------------------------
/*********************************************************************************************
-- HISTORY
-- ============================================================================================
-- ============================================================================================
-- VERSION   DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- ------------------------------------------------------
-- 1.0     28-Mar-2018       Siva	  	 TMS#20180116-00278   --grant to Apps                                     
***********************************************************************************************/
GRANT EXECUTE ON XXEIS.XXWC_PICKING_QLTY_CNTRL_PKG TO APPS
/
