/*************************************************************************
  $Header TMS_20180222-00161_DROP_SHIP_ERROR.sql $
  Module Name: TMS_20180222-00161 Data Fix Script

  PURPOSE: Data Fix for Unable to reapprove drop ship PO #1308330 
           vendor_product_num field has values with trailing spaces 
		   that needs to be trimmed.Po_line_id 9641025,9578911

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        28-FEB-2018  Pattabhi Avula         TMS#20180222-00161

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
DBMS_OUTPUT.put_line ('Before update');

UPDATE po_lines_all
SET VENDOR_PRODUCT_NUM = TRIM (VENDOR_PRODUCT_NUM)
WHERE po_line_id IN (9641025,9578911);

COMMIT;
DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);
EXCEPTION
WHEN OTHERS
THEN
DBMS_OUTPUT.put_line ('Error ' || SQLERRM);
END;
/