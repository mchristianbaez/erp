create or replace PACKAGE BODY APPS.XXWC_HZ_CONTACT_POINT
IS
   /*************************************************************************
     $Header XXWC_HZ_CONTACT_POINT.pkb $
     Module Name: XXWC_HZ_CONTACT_POINT.pkb

     PURPOSE:  Update Customer contact person phone and Email Address from XXWC_CUST_CONTACTS Form

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        10/26/2015  Pattabhi Avula        Initial Version(TMS 20150722-00210)
   **************************************************************************/
  g_err_callfrom VARCHAR2(75) DEFAULT 'XWC_HZ_CONTACT_POINT';
  g_distro_list  VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   /*====================================================================*/
   /*====================================================================*/

   PROCEDURE update_phone_email(p_owner_tbl_id NUMBER,p_area_code VARCHAR2,p_phone_num VARCHAR2,p_email VARCHAR2,x_retcode OUT VARCHAR2,x_errbuff OUT VARCHAR2)
   IS
   l_owner_tbl_id         NUMBER(20):=p_owner_tbl_id;
   l_phone_num            VARCHAR2(40):=p_phone_num;
   l_email1               VARCHAR2(2000):=p_email;
   l_ph_cnt_point_id      NUMBER(20);
   l_eml_cnt_point_id     NUMBER(20);
   l_ph_ln_type           VARCHAR2(30);
   l_sec                  VARCHAR2(100); 


   
   -- API declarations for phone contact creation
   p_contact_point_rec HZ_CONTACT_POINT_V2PUB.CONTACT_POINT_REC_TYPE;
   p_edi_rec           HZ_CONTACT_POINT_V2PUB.EDI_REC_TYPE;
   p_email_rec         HZ_CONTACT_POINT_V2PUB.EMAIL_REC_TYPE;
   p_phone_rec         HZ_CONTACT_POINT_V2PUB.PHONE_REC_TYPE;
   p_telex_rec         HZ_CONTACT_POINT_V2PUB.TELEX_REC_TYPE;
   p_web_rec           HZ_CONTACT_POINT_V2PUB.WEB_REC_TYPE;
   x_return_status     VARCHAR2(2000);
   x_msg_count         NUMBER;
   x_msg_data          VARCHAR2(2000);
   x_contact_point_id  NUMBER;
   
   
   -- API declarations for phone contact updation
  l_phone_rec HZ_CONTACT_POINT_V2PUB.phone_rec_type;
  l_email_rec HZ_CONTACT_POINT_V2PUB.email_rec_type;
  l_contact_point_rec HZ_CONTACT_POINT_V2PUB.contact_point_rec_type;
  l_contact_point_type VARCHAR2(100) := 'PHONE' ;--'EMAIL'; 
  l_cont_point_type_email VARCHAR2(100) := 'EMAIL' ;--'EMAIL';   
  l_primary_flag       VARCHAR2(1) := 'Y';  
  l_area_code          HZ_CONTACT_POINTS.phone_area_code%TYPE;
  l_area_code_upd      HZ_CONTACT_POINTS.phone_area_code%TYPE;
  l_phone_number       HZ_CONTACT_POINTS.phone_number%TYPE; --:=l_phone_num; 
  l_email              HZ_CONTACT_POINTS.email_address%TYPE  :=l_email1;
  l_obj_num            NUMBER(5);   
  l_em_obj_num         NUMBER(5); 
  p_party_id           HZ_PARTIES.party_id%TYPE; 
  l_format_num         VARCHAR2(40);
  l_format_num_cnt     NUMBER(20);
  l_ar_code            VARCHAR2(5);
   
   BEGIN
     -- Checking the contact person id for phone update
	 l_sec:='Checking the contact point id for phone update';
	 BEGIN
	   select contact_point_id
	   into   l_ph_cnt_point_id
	   from   hz_contact_points 
	   where  owner_table_id=l_owner_tbl_id
	   AND    contact_point_type='PHONE';
	 EXCEPTION
	   WHEN OTHERS THEN
	   NULL;
	 END;  --Ln 56 begin close
	 
	 -- Checking the contact person id for mail update
	 l_sec:='Checking the contact point id for mail update';
	 BEGIN
	   select contact_point_id
	   into   l_eml_cnt_point_id
	   from   hz_contact_points 
	   where  owner_table_id=l_owner_tbl_id
	   AND    contact_point_type='EMAIL';
	 EXCEPTION
	   WHEN OTHERS THEN
	   NULL;
	 END;  --Ln 56 begin close
	 
	 IF l_ph_cnt_point_id IS NULL THEN
	 -- creating the record for contact id for phone type
	   l_sec:='Creating the phone contact point Id for l_ph_cnt_point_id is null';

        BEGIN        
        -- Initializing the Mandatory API parameters
        p_contact_point_rec.contact_point_type    := 'PHONE';
        p_contact_point_rec.owner_table_name      := 'HZ_PARTIES';
        p_contact_point_rec.owner_table_id        := l_owner_tbl_id;
        p_contact_point_rec.primary_flag          := 'Y';
        p_contact_point_rec.contact_point_purpose := 'BUSINESS';
        p_contact_point_rec.created_by_module     := 'BO_API';
        p_phone_rec.phone_area_code               :=  p_area_code;
        p_phone_rec.phone_country_code            := '1';
        p_phone_rec.phone_number                  := l_phone_num;
        p_phone_rec.phone_line_type               := 'GEN';
        p_email_rec.email_address                 := l_email;
        DBMS_OUTPUT.PUT_LINE('Calling the API hz_contact_point_v2pub.create_contact_point'); 
        
        HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT(
                                                     p_init_msg_list      => FND_API.G_TRUE,
                                                     p_contact_point_rec  => p_contact_point_rec,
                                                     p_edi_rec            => p_edi_rec,
                                                     p_email_rec          => p_email_rec, 
                                                     p_phone_rec          => p_phone_rec,
                                                     p_telex_rec          => p_telex_rec,
                                                     p_web_rec            => p_web_rec,
                                                     x_contact_point_id   => x_contact_point_id,
                                                     x_return_status      => x_return_status,
                                                     x_msg_count          => x_msg_count,
                                                     x_msg_data           => x_msg_data
                                                    );
         IF  x_return_status = fnd_api.g_ret_sts_success THEN
         COMMIT;
		 --l_area_code_new:=NULL;
		 l_sec:='Phone contact record created successfully';
         DBMS_OUTPUT.PUT_LINE('Creation of Contact Point is Successful ');
         DBMS_OUTPUT.PUT_LINE('Output information ....');
         DBMS_OUTPUT.PUT_LINE('x_contact_point_id = '||x_contact_point_id);     
         ELSE
             DBMS_OUTPUT.put_line ('Creation of Contact Point got failed:'||x_msg_data);
             ROLLBACK;
             FOR i IN 1 .. x_msg_count
             LOOP
               x_msg_data := fnd_msg_pub.get( p_msg_index => i, p_encoded => 'F');
               dbms_output.put_line( i|| ') '|| x_msg_data);
             END LOOP;
         END IF; -- LN 96 IF CONDITION CLOSED
        END;
	 ELSE
	   BEGIN	    	   
       
	    -- Updating the customer contact phone number
		l_sec:='Updating the customer contact phone number';
        BEGIN
	      SELECT contact_point_id,
	             phone_area_code,
				 object_version_number,
				 phone_line_type,
                 SUBSTR(raw_phone_number,1,3)				 
	      INTO   l_ph_cnt_point_id,
	             l_area_code,
				 l_obj_num,
				 l_ph_ln_type,
				 l_ar_code
	      FROM   hz_contact_points 
	      WHERE  owner_table_id=l_owner_tbl_id
	      AND    contact_point_type='PHONE'
		  AND    owner_table_name='HZ_PARTIES'
		  AND    primary_flag='Y';
	      EXCEPTION
	        WHEN OTHERS THEN
	        NULL;
	      END;	 -- LN 133 begin closed here   
		 
				  
		    SELECT REGEXP_REPLACE(l_phone_num,'[(]|[)]|[-]|[ ]|[.]','')
            INTO l_format_num
            FROM dual;
			
			IF p_area_code IS NULL THEN
			   l_area_code_upd:=l_area_code;
			ELSE
			   l_area_code_upd:=p_area_code;
			END IF;
			
		    /*SELECT LENGTH(l_format_num)
            INTO l_format_num_cnt
            FROM dual; */
			
			/*IF l_area_code IS NOT NULL THEN
			   l_phone_number:=SUBSTR(l_format_num,4);
			ELSE
			   l_phone_number:=l_format_num;
			END IF; */
			/*ELSIF 
			   l_area_code IS NULL  AND l_format_num_cnt<8 THEN
			   l_phone_number:=l_ar_code||l_format_num;
			ELSIF 
			   l_area_code IS NOT NULL  AND l_format_num_cnt>7 THEN
			   l_phone_number:=l_area_code||SUBSTR(l_format_num,3);
			ELSE
			   l_phone_number:=l_area_code||SUBSTR(l_format_num,3);
			END IF; */
			
	      l_email_rec.email_address               := l_email;  
          l_phone_rec.phone_area_code             := l_area_code_upd; --l_area_code;   
          l_phone_rec.phone_number                := l_format_num;  
          --l_phone_rec.phone_line_type             := 'PHONE';
          l_phone_rec.phone_line_type             := l_ph_ln_type;		  
          l_contact_point_rec.contact_point_type  := l_contact_point_type;     --PHONE 
          l_contact_point_rec.status              := 'A';  
          l_contact_point_rec.owner_table_name    := 'HZ_PARTIES';  
          l_contact_point_rec.owner_table_id      := l_owner_tbl_id; --p_party_id;
          l_contact_point_rec.primary_flag        := l_primary_flag;  
          l_contact_point_rec.content_source_type := 'USER_ENTERED';  
          l_contact_point_rec.contact_point_id    := l_ph_cnt_point_id;     
          HZ_CONTACT_POINT_V2PUB.update_contact_point( p_init_msg_list          => FND_API.G_TRUE, 
                                                       p_contact_point_rec      => l_contact_point_rec, 
	      											   p_email_rec              => l_email_rec, 
	      											   p_phone_rec              => l_phone_rec, 
	      											   p_object_version_number  => l_obj_num, 
	      											   x_return_status          => x_return_status, 
	      											   x_msg_count              => x_msg_count, 
	      											   x_msg_data               => x_msg_data
													  );  
          dbms_output.put_line('Return Status :' || x_return_status);  
          IF x_return_status = fnd_api.g_ret_sts_success  THEN   
           COMMIT;	
		   l_sec:='Updated phone contact point successfully';
		   l_area_code    :=NULL;
		   l_format_num   :=NULL;
		   l_obj_num      :=NULL;
		   l_ph_ln_type   :=NULL;
		   l_ar_code      :=NULL;
          ELSE		   
            FOR k in 1 .. x_msg_count 
		    LOOP       
            x_msg_data := fnd_msg_pub.get ( p_msg_index => k, 
                                           p_encoded   => 'F'
            							   );               
            END LOOP;        
          END IF;
          END; -- ln 146 being closed here
	END IF;
	
	-- ===================================================================================
	-- Email contact creation or update for customer contact
	-- ===================================================================================
	IF l_eml_cnt_point_id IS NULL THEN
	l_sec:='Email record creation for contact point id null';
		
		BEGIN        
        -- Initializing the Mandatory API parameters
        p_contact_point_rec.contact_point_type    := 'EMAIL';
        p_contact_point_rec.owner_table_name      := 'HZ_PARTIES';
        p_contact_point_rec.owner_table_id        := l_owner_tbl_id;
        p_contact_point_rec.primary_flag          := 'Y';
        p_contact_point_rec.contact_point_purpose := 'BUSINESS';
        p_contact_point_rec.created_by_module     := 'BO_API';
        p_phone_rec.phone_area_code               :=  NULL;
        p_phone_rec.phone_country_code            := '1';
        p_phone_rec.phone_number                  := l_phone_num;
        p_phone_rec.phone_line_type               := 'GEN';
        p_email_rec.email_address                 := l_email;
        DBMS_OUTPUT.PUT_LINE('Calling the API hz_contact_point_v2pub.create_contact_point'); 
        
        HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT(
                                                     p_init_msg_list      => FND_API.G_TRUE,
                                                     p_contact_point_rec  => p_contact_point_rec,
                                                     p_edi_rec            => p_edi_rec,
                                                     p_email_rec          => p_email_rec, 
                                                     p_phone_rec          => p_phone_rec,
                                                     p_telex_rec          => p_telex_rec,
                                                     p_web_rec            => p_web_rec,
                                                     x_contact_point_id   => x_contact_point_id,
                                                     x_return_status      => x_return_status,
                                                     x_msg_count          => x_msg_count,
                                                     x_msg_data           => x_msg_data
                                                    );
         IF  x_return_status = fnd_api.g_ret_sts_success THEN
         COMMIT;
		 l_sec:='Email Record created successfully';
         DBMS_OUTPUT.PUT_LINE('Creation of Contact Point is Successful ');
         DBMS_OUTPUT.PUT_LINE('Output information ....');
         DBMS_OUTPUT.PUT_LINE('x_contact_point_id = '||x_contact_point_id);     
         ELSE
             DBMS_OUTPUT.put_line ('Creation of Contact Point got failed:'||x_msg_data);
             ROLLBACK;
             FOR i IN 1 .. x_msg_count
             LOOP
               x_msg_data := fnd_msg_pub.get( p_msg_index => i, p_encoded => 'F');
               dbms_output.put_line( i|| ') '|| x_msg_data);
             END LOOP;
         END IF; -- LN 96 IF CONDITION CLOSED
         END;
		 
	  ELSE
       
	    -- Updating the customer contact Email Address
		l_sec:='Updating the customer contact Email Address';
        BEGIN
	      SELECT contact_point_id,
	             object_version_number				 
	      INTO   l_eml_cnt_point_id,
	             l_em_obj_num				 
	      FROM   hz_contact_points 
	      WHERE  owner_table_id=l_owner_tbl_id
	      AND    contact_point_type='EMAIL'
		  AND    owner_table_name='HZ_PARTIES'
		  AND    primary_flag='Y';
	      EXCEPTION
	        WHEN OTHERS THEN
	        NULL;
	      END;	 -- LN 133 begin closed here   
		 
		BEGIN
	      l_email_rec.email_address               := l_email;  
          l_phone_rec.phone_area_code             := NULL; --l_area_code;   
          l_phone_rec.phone_number                := l_phone_number;  
          --l_phone_rec.phone_line_type             := 'PHONE';
          l_phone_rec.phone_line_type             := l_ph_ln_type;		  
          l_contact_point_rec.contact_point_type  := l_cont_point_type_email;     --PHONE 
          l_contact_point_rec.status              := 'A';  
          l_contact_point_rec.owner_table_name    := 'HZ_PARTIES';  
          l_contact_point_rec.owner_table_id      := l_owner_tbl_id; --p_party_id;
          l_contact_point_rec.primary_flag        := l_primary_flag;  
          l_contact_point_rec.content_source_type := 'USER_ENTERED';  
          l_contact_point_rec.contact_point_id    := l_eml_cnt_point_id;     
          HZ_CONTACT_POINT_V2PUB.update_contact_point( p_init_msg_list          => FND_API.G_TRUE, 
                                                       p_contact_point_rec      => l_contact_point_rec, 
	      											   p_email_rec              => l_email_rec, 
	      											   p_phone_rec              => l_phone_rec, 
	      											   p_object_version_number  => l_em_obj_num, 
	      											   x_return_status          => x_return_status, 
	      											   x_msg_count              => x_msg_count, 
	      											   x_msg_data               => x_msg_data
													  );  
          dbms_output.put_line('Return Status :' || x_return_status);  
          IF x_return_status = fnd_api.g_ret_sts_success  THEN  		  
           COMMIT;	
		   l_sec:='Email updated successfully for the given contact point id';
		   l_email :=NULL;
          ELSE		   
            FOR k in 1 .. x_msg_count 
		    loop       
            x_msg_data := fnd_msg_pub.get ( p_msg_index => k, 
                                           p_encoded   => 'F'
            							   );               
            END LOOP;        
          END IF;
          END; -- ln 146 being closed here
	END IF;
EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom
                                          ,p_calling           => l_sec
                                          ,p_request_id        => NULL
                                          ,p_ora_error_msg     => substr('SQLERRM'
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr('SQLERRM'
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'ONT');


END; -- proceudre end

END xxwc_hz_contact_point; -- main pkg
/