CREATE OR REPLACE VIEW APPS.xxwc_ar_cust_contact_query_vw
/**************************************************************************
     $Header xxwc_ar_cust_contact_query_vw $     

     PURPOSE:   Performance Improvement for customer contact form

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------     
	 1.0        09/04/2015  Manjula Chellappan       Initial Version TMS 20150901-00161 - XXWC_CUST_CONTACTS Performance Tuning
     1.1        11/05/2015  Pattabhi Avula           TMS 20150722-00210 - OM - Ability to edit contacts from the sales order (job on the fly)
   **************************************************************************/
AS
SELECT cari,
  party_id,
  cust_account_id,
  cust_acct_site_id,
  location_id,
  contact_level,
  level_value,
  current_role_state,
  primary_flag,
  role_type,
  status,
  contact_number,
  party_name,
  person_first_name,
  person_middle_name,
  person_last_name,
  ps_reference,
  sub_party_id,
  party_type,
  relationship_id,
  owner_table_name,
  owner_table_id,
  has_primary_bill_to,
  NVL(rn,1) rn
FROM
  (SELECT acct_role.cust_account_role_id cari,
    acct_role.current_role_state,
    acct_role.primary_flag,
    acct_role.party_id,
    acct_role.role_type,
    acct_role.cust_account_id,
    DECODE(acct_role.cust_acct_site_id,NULL,'ACCOUNT', 'ACCOUNT SITE') contact_level,
    DECODE(acct_role.cust_acct_site_id,NULL,hca.account_number,hps.party_site_number) level_value,
    cont_party.party_id sub_party_id,
    cont_party.party_type,
    cont_party.person_last_name ,
    cont_party.person_middle_name ,
    cont_party.person_first_name ,
    cont_party.party_name,
    cust_party.party_name customer_name,
    cust_party.party_name dummy_name,
    cust_party.person_last_name last_name,
    cust_party.person_first_name first_name,
    acct_role.cust_acct_site_id,
    hl.location_id,
    hl.city,
    hl.state,
    hl.postal_code,
    ter.description country,
    hl.country country_code,
    acct_role.status,
    org_contact.contact_number,
    cust_party.party_number,
    hca.account_number customer_number,
    hl.address1,
    hl.address2,
    hl.address3,
    hl.address4,
    hl.county,
    hl.province,
    cust_party.tax_reference tax_reg_number,
    cust_party.jgzz_fiscal_code taxpayer_id,
    hca.orig_system_reference REFERENCE,
    hca.customer_type type,
    cust_party.category_code category,
    hca.customer_class_code class,
    cust_party.sic_code,
    cust_party.party_type customer_type,
    cont_party.customer_key,
    hl.address_key,
    cust_party.customer_key,
    hca.account_name,
    hps.party_site_number,
    hps.orig_system_reference ps_reference,
    rel.relationship_id,
    'HZ_PARTIES' owner_table_name,
    acct_role.party_id owner_table_id,
    NVL(
    (SELECT hcsua.primary_flag
    FROM apps.hz_cust_site_uses hcsua
    WHERE hcsua.cust_acct_site_id = acct_role.cust_acct_site_id
    AND hcsua.status              = 'A'
    AND hcsua.site_use_code       = 'BILL_TO'
    AND primary_flag              = 'Y'
    AND rownum                    = 1
    ), 'N') has_primary_bill_to,
    phon.rn
  FROM hz_cust_account_roles acct_role,
    hz_relationships rel,
    hz_org_contacts org_contact,
    hz_parties cont_party,
    hz_cust_accounts hca,
    hz_cust_acct_sites_all hcs,
    hz_parties cust_party,
    hz_party_sites hps,
    hz_locations hl,
    fnd_territories_tl ter,
    (SELECT owner_table_name,
      owner_table_id,
      rn
    FROM
      (SELECT owner_table_name ,
        owner_table_id ,
        contact_point_type,
        phone_line_type,
        ROW_NUMBER () OVER ( PARTITION BY owner_table_name , owner_table_id , contact_point_type, phone_line_type ORDER BY owner_table_name , owner_table_id , contact_point_type ) rn
      FROM hz_contact_points
      WHERE owner_table_name    = 'HZ_PARTIES'
      AND contact_point_type IN ('PHONE','EMAIL')
	  AND primary_flag              = 'Y'  -- Vern 1.1
      )
    GROUP BY owner_table_name ,
      owner_table_id ,
      rn
    ) phon
  WHERE acct_role.party_id                     = rel.party_id
  AND acct_role.role_type                      = 'CONTACT'
  AND acct_role.cust_acct_site_id   IS NULL  -- Ver 1.1
  AND org_contact.party_relationship_id        = rel.relationship_id
  AND rel.subject_id                           = cont_party.party_id
  AND rel.object_id                            = hca.party_id
  AND rel.subject_table_name                   = 'HZ_PARTIES'
  AND rel.object_table_name                    = 'HZ_PARTIES'
  AND acct_role.cust_account_id                = hca.cust_account_id
  AND acct_role.cust_acct_site_id              = hcs.cust_acct_site_id (+)
  AND acct_role.cust_account_id                = hcs.cust_account_id (+)
  AND hca.party_id                             = cust_party.party_id (+)
  AND hcs.party_site_id                        = hps.party_site_id (+)
  AND hps.location_id                          = hl.location_id (+)
  AND hl.country                               = ter.territory_code (+)
  AND NVL(ter.language, USERENV('LANG'))       = USERENV('LANG')
  AND acct_role.party_id                       = phon.owner_table_id (+)
  AND NVL(phon.owner_table_name, 'HZ_PARTIES') = 'HZ_PARTIES'
  AND acct_role.status                         = 'A'
  -- <START>Added for Version 1.1
  AND NOT EXISTS(SELECT 1 FROM ar_contacts_v cont, 
                           hz_parties hp, 
                           hz_role_responsibility cont_role
                      WHERE   cont.contact_party_id = hp.party_id
                      AND    hp.party_id=cont_party.party_id
                      AND     cont.contact_id = cont_role.cust_account_role_id
                      AND     cont_role.responsibility_type IN ('AUTH_BUYER',
                                                                'MARKET',
                                                                'GENERAL',
                                                                'BONDING',
                                                                'ACC_PAY',
                                                                'ACK',
                                                                'CREDIT_CONTACT',
                                                                'INV',
                                                                'ADD_NTO',
                                                                'LEGAL',
                                                                'FINANCIAL',
                                                                'DUN',
                                                                'OWNER'))
-- <END>Added for Version 1.1
  )
  ORDER BY party_name
  -- Commented the below for Ver 1.1
  /*
UNION
SELECT NULL cari,
  NULL party_id,
  hca.cust_account_id,
  NULL cust_acct_site_id,
  NULL location_id,
  'PARTY' contact_level,
  hca.account_number level_value,
  NULL current_role_state,
  NULL primary_flag,
  NULL role_type,
  NULL status,
  NULL contact_number,
  NULL party_name,
  NULL person_first_name,
  NULL person_middle_name,
  NULL person_last_name,
  NULL ps_reference,
  NULL sub_party_id,
  NULL party_type,
  NULL relationship_id,
  'HZ_PARTIES' owner_table_name,
  hca.party_id owner_table_id,
  'N' has_primary_bill_to,
  NVL(phon.rn,1) rn
FROM hz_cust_accounts hca,
  (SELECT owner_table_name,
    owner_table_id,
    rn
  FROM
    (SELECT owner_table_name ,
      owner_table_id ,
      contact_point_type,
      phone_line_type,
      ROW_NUMBER () OVER ( PARTITION BY owner_table_name , owner_table_id , contact_point_type, phone_line_type ORDER BY owner_table_name , owner_table_id , contact_point_type ) rn
    FROM hz_contact_points
    WHERE 1                 =1 --owner_table_id    = 97216
    AND owner_table_name    = 'HZ_PARTIES'
    AND contact_point_type IN ('PHONE','EMAIL')
    )
  GROUP BY owner_table_name ,
    owner_table_id ,
    rn
  ) phon
WHERE hca.party_id        = phon.owner_table_id
AND phon.owner_table_name = 'HZ_PARTIES'*/
/