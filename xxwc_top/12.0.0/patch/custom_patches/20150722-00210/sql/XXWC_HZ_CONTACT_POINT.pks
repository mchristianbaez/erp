CREATE OR REPLACE
PACKAGE APPS.XXWC_HZ_CONTACT_POINT IS
/*************************************************************************
     $Header XXWC_HZ_CONTACT_POINT.pks $
     Module Name: XXWC_HZ_CONTACT_POINT.pks

     PURPOSE: Update Customer contact person phone and Email Address from XXWC_CUST_CONTACTS Form

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ------------------    ----------------
     1.0        10/26/2015  Pattabhi Avula        Initial Version(TMS20150722-00210)
   **************************************************************************/
   
   PROCEDURE update_phone_email(p_owner_tbl_id NUMBER,p_area_code VARCHAR2,p_phone_num VARCHAR2,p_email VARCHAR2,x_retcode OUT VARCHAR2,x_errbuff OUT VARCHAR2);
   
END;
/