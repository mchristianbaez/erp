CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_DMS2_IB_PKG AS
/******************************************************************************
     $Header XXWC_OM_DMS2_IB_PKG $
     Module Name: XXWC_OM_DMS2_IB_PKG.pkb

     PURPOSE:   DESCARTES Project

     REVISIONS:
     Ver        Date        Author                Description
     ---------  ----------  ---------------       -----------------------------
     1.0        12/19/2017  Rakesh Patel          TMS#20170901-00010 DMS Phase-2.0 Inbound Extract
     2.0        02/08/2018  Rakesh Patel          TMS#20180208-00103 Multiple minor fixes for the descartes errors
	 3.0        02/09/2018  Rakesh Patel          TMS#20180209-00125-Fixes for the descartes pilot errors
*******************************************************************************/

   --Email Defaults
   g_dflt_email                fnd_user.email_address%TYPE    := 'WC-ITDEVALERTS-U1@HDSupply.com';
   g_debug                     BOOLEAN := FALSE;
   g_request_id                NUMBER;
   g_retcode                   NUMBER;  
   g_err_msg                   VARCHAR2(3000);
   g_sqlcode                   NUMBER;   
   g_sqlerrm                   VARCHAR2(2000);
   g_message                   VARCHAR2(2000);
   g_package                   VARCHAR2(200) :=   'XXWC_OM_DMS2_IB_PKG';
   g_call_from                 VARCHAR2(200);
   g_module                    VARCHAR2 (80) := 'OM';
   g_err_callfrom              VARCHAR2(75) DEFAULT 'XXWC_OM_DMS2_IB_PKG';
   g_distro_list               VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   g_host                      VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
   g_hostport                  VARCHAR2(20) := '25';
  
   TYPE request_ids_tbl IS TABLE OF NUMBER INDEX BY PLS_INTEGER;
   g_request_ids_tbl   request_ids_tbl;
   g_request_id_count  NUMBER:= 0;
  
   PROCEDURE write_log ( p_header_id    IN NUMBER
                        ,p_line_id      IN NUMBER
                        ,p_log_message  IN VARCHAR2
                        ,p_log_sequence In NUMBER
                       )
   IS
   BEGIN
      IF g_debug THEN
       fnd_file.put_line(fnd_file.log, (p_header_id||'-'||p_line_id||'. '||p_log_sequence||': '||p_log_message));

       INSERT INTO xxwc.xxwc_dms2_log (
              header_id
            , line_id
            , log_message
            , creation_date
            , log_sequence
        ) VALUES (
              p_header_id
            , p_line_id
            , SUBSTR(p_log_message, 1, 4000)
            , SYSDATE
            , p_log_sequence
        );
    END IF;

   EXCEPTION
   WHEN OTHERS THEN
      NULL; -- noop
   END;
   
  /*******************************************************************************
   Procedure Name  :   DEBUG_LOG
   Description     :   This function will be used for log messages 
   ******************************************************************************/
   PROCEDURE DEBUG_LOG (p_xxwc_log_rec IN OUT xxwc_log_rec )
   IS
       PRAGMA AUTONOMOUS_TRANSACTION;
       l_order_number XXWC.XXWC_dms2_LOG_TBL.order_number%TYPE;
       l_update BOOLEAN := FALSE;
       
       l_sec           VARCHAR2 (2000);
   BEGIN
      l_sec := 'Start DEBUG_LOG';
      g_call_from := 'DEBUG_LOG';
      
      BEGIN
        SELECT order_number
          INTO l_order_number 
          FROM xxwc.xxwc_dms2_log_tbl
         WHERE interface_type   = 'INBOUND'
           AND service_name	    = p_xxwc_log_rec.service_name
		   AND sequence_id      = p_xxwc_log_rec.sequence_id;
         
         l_update := TRUE;
		 
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_update := FALSE;
      WHEN OTHERS THEN
        l_update := FALSE;
      END;   
        
      IF l_update = TRUE THEN
        UPDATE XXWC.XXWC_DMS2_LOG_TBL
           SET SERVICE_NAME            = p_xxwc_log_rec.service_name
              ,REQUEST_PAYLOAD         = p_xxwc_log_rec.request_payload
              ,RESPONSE_PAYLOAD        = p_xxwc_log_rec.response_payload
              ,WS_RESPONSECODE         = p_xxwc_log_rec.ws_responsecode
              ,WS_RESPONSEDESCRIPTION  = p_xxwc_log_rec.ws_responsedescription
              ,HTTP_STATUS_CODE        = p_xxwc_log_rec.http_status_code
              ,HTTP_REASON_PHRASE      = p_xxwc_log_rec.http_reason_phrase
              ,LAST_UPDATE_DATE        = SYSDATE
              ,LAST_UPDATED_BY         = FND_GLOBAL.user_id
              ,CONCURRENT_REQUEST_ID   = Fnd_global.conc_request_id 
			  ,ORDER_NUMBER            = p_xxwc_log_rec.order_number
			  ,DELIVERY_ID             = p_xxwc_log_rec.delivery_id
			  ,ORDER_TYPE              = p_xxwc_log_rec.order_type
			  ,cmd					   = p_xxwc_log_rec.cmd
			  ,stype				   = p_xxwc_log_rec.stype
              ,lcode                   = p_xxwc_log_rec.lcode
         WHERE interface_type          = 'INBOUND'		   
           AND service_name	           = p_xxwc_log_rec.service_name
		   AND sequence_id             = p_xxwc_log_rec.sequence_id;
      ELSE
         INSERT INTO XXWC.XXWC_DMS2_LOG_TBL
            (
             SERVICE_NAME            ,
             INTERFACE_TYPE          ,
             ORDER_NUMBER            ,  
             ORDER_HEADER_ID         ,
             LINE_ID                 ,
             DELIVERY_ID             , 
             ORDER_TYPE              ,
             SCHEDULE_DELIVERY_DATE  ,
             REQUEST_PAYLOAD         ,
             RESPONSE_PAYLOAD        ,
             WS_RESPONSECODE         ,
             WS_RESPONSEDESCRIPTION  ,
             HTTP_STATUS_CODE        ,
             HTTP_REASON_PHRASE      ,
             CONCURRENT_REQUEST_ID   ,
			 SEQUENCE_ID             ,
             CREATION_DATE           ,
             CREATED_BY              ,
             LAST_UPDATE_DATE        ,
             LAST_UPDATED_BY         ,
             ATTRIBUTE1              ,
             ATTRIBUTE2              ,
             ATTRIBUTE3              ,
             ATTRIBUTE4              ,
             ATTRIBUTE5              ,
			 REQUEST_PAYLOAD_XMLTYPE
             )
         VALUES
             (
              p_xxwc_log_rec.service_name              ,
              'INBOUND'                                , 
              p_xxwc_log_rec.order_number              ,
              p_xxwc_log_rec.header_id                 ,
              p_xxwc_log_rec.line_id                   ,
              p_xxwc_log_rec.delivery_id               ,
              p_xxwc_log_rec.order_type                ,
              p_xxwc_log_rec.schedule_delivery_date    ,
              p_xxwc_log_rec.request_payload           ,
              p_xxwc_log_rec.response_payload          ,
              p_xxwc_log_rec.ws_responsecode           ,
              p_xxwc_log_rec.ws_responsedescription    ,
              p_xxwc_log_rec.http_status_code          ,
              p_xxwc_log_rec.http_reason_phrase        ,
              Fnd_global.conc_request_id               ,
			  p_xxwc_log_rec.sequence_id               ,
              SYSDATE                                  ,
              FND_GLOBAL.user_id                       ,
              SYSDATE                                  ,
              FND_GLOBAL.user_id                       ,
              NULL                                     ,   
              NULL                                     ,
              NULL                                     ,
              NULL                                     ,
			  NULL                                     ,
			  p_xxwc_log_rec.REQUEST_PAYLOAD_XMLTYPE                                                 
             ) ;  
      END IF;
      COMMIT;
	  
      l_sec := 'End DEBUG_LOG';
	  
EXCEPTION
WHEN others THEN
	    
  g_sqlcode := SQLCODE;
  g_sqlerrm := SQLERRM;
  g_message := 'Error in generating debug log ';
    
  xxcus_error_pkg.xxcus_error_main_api(p_called_from         => g_package||'.'||g_call_from
                                     , p_calling             => l_sec
                                     , p_ora_error_msg       => SQLERRM
                                     , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                     , p_distribution_list   => g_dflt_email
                                     , p_module              => g_module);

   
END DEBUG_LOG;

PROCEDURE debug_on AS
BEGIN
   g_debug := TRUE;
END;

PROCEDURE debug_off AS
BEGIN
   g_debug := FALSE;
END;

PROCEDURE create_image_file_on_server(p_header_id              IN NUMBER
                                     ,p_concurrent_request_id  IN NUMBER
                                     ,p_resub_req_id           IN NUMBER
                                      )
   IS
      v_file              UTL_FILE.FILE_TYPE;
      l_buffer            RAW (32000);
      l_amount            BINARY_INTEGER := 32000;
      l_pos               NUMBER := 1;
      l_blob              BLOB;
      l_blob_len          NUMBER;
      v_name              VARCHAR2 (128);
      l_sec               VARCHAR2 (2000);
      l_error_code        NUMBER;
      l_error_msg         VARCHAR2(4000);
 
      CURSOR Cur_image (p_header_id              IN NUMBER
                       ,p_concurrent_request_id  IN NUMBER  
                        )
      IS 
          SELECT sc.signature_image_blob l_blob,
                DBMS_LOB.getlength (sc.signature_image_blob) l_blob_len
            FROM xxwc.xxwc_om_dms2_sign_cap_gtt_tbl sc--xxwc.xxwc_om_dms2_sign_capture_tbl sc
           WHERE 1=1 
             AND header_id                        = p_header_id
             AND NVL(sc.ship_confirm_status, 'X') <> 'COMPLETED'
             AND (concurrent_request_id            = p_concurrent_request_id 
                 OR concurrent_request_id IS NULL) -- concurrent_request_id will be null only for the WCD orders
             AND LCode                            = 1 
             AND sc.pod_method_image_type IN ('Signature', 'Photo of Delivered Product')
             AND ROWNUM=1;
             
      RecordsAffected     PLS_INTEGER := 0;
      v_CONTENT_LENGTH    PLS_INTEGER := 0;
      v_CONTENT_LENGTH2   PLS_INTEGER := 0;
      v_content           CLOB;
      j                   PLS_INTEGER := 0;
      k                   PLS_INTEGER;
      l                   PLS_INTEGER;
      vn_the_rest         PLS_INTEGER := 0;
      vn_the_rest2        PLS_INTEGER := 0;
      v_image_count       NUMBER;
      x                   NUMBER;
      l_log_seq           NUMBER := 111;
   BEGIN
      l_sec := 'Start of Procedure create_image_file_on_server Begin Time: ' || TO_CHAR (SYSDATE, 'MM/DD/YYYY hh:mm:ss');
      
      write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);

      FOR Rec IN Cur_image ( p_header_id             => p_header_id
                            ,p_concurrent_request_id => p_concurrent_request_id
                            )
      LOOP
         x := l_blob_len;
         -- Open the destination file.
         IF p_resub_req_id IS NOT NULL THEN
            v_name := p_resub_req_id||'.BMP';
         ELSE
            v_name := p_concurrent_request_id||'.BMP';
         END IF;   
        
         v_file :=
            UTL_FILE.FOPEN ('XXWC_DMS2_POD_IMAGES',
                            v_name,
                            'wb',
                            32767);

         IF Rec.l_blob_len < 32000
         THEN
            DBMS_LOB.read (Rec.l_blob,
                           Rec.l_blob_len,
                           l_pos,
                           l_buffer);
            UTL_FILE.put_raw (v_file, l_buffer, TRUE);
            UTL_FILE.fflush (v_file);
         ELSE
            WHILE l_pos < Rec.l_blob_len AND l_amount > 0
            LOOP
               DBMS_LOB.read (Rec.l_blob,
                              l_amount,
                              l_pos,
                              l_buffer);
               UTL_FILE.put_raw (v_file, l_buffer, TRUE);
               UTL_FILE.fflush (v_file);
               -- set the start position for the next cut
               l_pos := l_pos + l_amount;
               -- set the end position if less than 32000 bytes
               x := x - l_amount;

               IF x < 32000
               THEN
                  l_amount := x;
               END IF;
            END LOOP;

            l_pos := 1;
            l_buffer := NULL;
            l_blob := NULL;
            l_amount := 32000;
         END IF;

         RecordsAffected := RecordsAffected + 1;
         UTL_FILE.FCLOSE (v_file);
      END LOOP;

      l_sec := 'End of Procedure create_image_file_on_server End Time: ' || TO_CHAR (SYSDATE, 'MM/DD/YYYY hh:mm:ss');
      
      write_log ( p_header_id
                , NULL
                , l_sec
                , l_log_seq+1);
                
      write_log (p_header_id
                 , NULL
                 , 'Image Records Affected: ' || RecordsAffected
                 , l_log_seq+1);
   EXCEPTION
   WHEN UTL_FILE.invalid_path
   THEN
      l_error_msg := 'Invalid path Error in XXWC_OM_DMS2_IB_PKG.create_image_file_on_server is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM;
    
      write_log (   p_header_id
                  , NULL
                  , l_error_msg
                  , l_log_seq+1);
    
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.create_image_file_on_server',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);   

   WHEN UTL_FILE.WRITE_ERROR
   THEN
      l_error_msg := 'Operating system error in XXWC_OM_DMS2_IB_PKG.create_image_file_on_server is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM;    

      write_log (   p_header_id
                  , NULL
                  , l_error_msg
                  , l_log_seq+1);
                  
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.create_image_file_on_server',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);   
   WHEN UTL_FILE.INTERNAL_ERROR
   THEN
      l_error_msg := 'Unspecified PL/SQL error in XXWC_OM_DMS2_IB_PKG.create_image_file_on_server is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM;
      
      write_log (   p_header_id
                  , NULL
                  , l_error_msg
                  , l_log_seq+1);
    
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.create_image_file_on_server',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);     
   WHEN UTL_FILE.INVALID_OPERATION
   THEN
      l_error_msg := 'File could not error in XXWC_OM_DMS2_IB_PKG.create_image_file_on_server is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM;
    
      write_log (   p_header_id
                  , NULL
                  , l_error_msg
                  , l_log_seq+1);
                  
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.create_image_file_on_server',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);     
   WHEN UTL_FILE.INVALID_FILEHANDLE
   THEN
      l_error_msg := 'File handle was invalid error in XXWC_OM_DMS2_IB_PKG.create_image_file_on_server is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM;
    
      write_log (   p_header_id
                  , NULL
                  , l_error_msg
                  , l_log_seq+1);
    
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.create_image_file_on_server',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);  
   WHEN UTL_FILE.INVALID_MODE
   THEN
      l_error_msg := 'The open_mode parameter in FOPEN was invalid error in XXWC_OM_DMS2_IB_PKG.create_image_file_on_server is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM;
    
      write_log (   p_header_id
                  , NULL
                  , l_error_msg
                  , l_log_seq+1);
    
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.create_image_file_on_server',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);   
   WHEN UTL_FILE.INVALID_MAXLINESIZE
   THEN
      l_error_msg := 'Specified max_linesize is too large or too small error in XXWC_OM_DMS2_IB_PKG.create_image_file_on_server is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM;
    
      write_log (   p_header_id
                  , NULL
                  , l_error_msg
                  , l_log_seq+1);
    
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.create_image_file_on_server',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	    
   WHEN OTHERS
   THEN
      -- Close the file if something goes wrong.
      IF UTL_FILE.is_open (v_file)
      THEN
         UTL_FILE.fclose (v_file);
      END IF;

      l_error_msg := 'When Others error in XXWC_OM_DMS2_IB_PKG.create_image_file_on_server is SQLERROR CODE'||l_error_code|| 'SQLERRM : '||SQLERRM;
    
      write_log (   p_header_id
                  , NULL
                  , l_error_msg
                  , l_log_seq+1);
                  
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.create_image_file_on_server',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	 
      
   END;
   
   PROCEDURE wait_for_running_request ( p_request_id   IN NUMBER
                                       ,p_program_type IN VARCHAR2 )
   IS
      CURSOR cu_running_xdoburst_requests ( p_request_id IN NUMBER)
      IS
         SELECT /*+ Index(fnd_concurrent_requests_N7) */ request_id
           FROM apps.fnd_concurrent_requests fcr
          WHERE fcr.concurrent_program_id IN ( SELECT CONCURRENT_PROGRAM_ID 
                                                 FROM FND_CONCURRENT_PROGRAMS_VL 
                                                WHERE CONCURRENT_PROGRAM_NAME ='XXWC_XDOBURSTREP'
                                                AND ENABLED_FLAG = 'Y' ) 
            AND (status_code = 'R' OR status_code = 'I')
            AND (phase_code = 'R' OR phase_code ='P')
            AND request_id = p_request_id;

      CURSOR cu_running_cfd_requests ( p_request_id IN NUMBER)
      IS
         SELECT /*+ Index(fnd_concurrent_requests_N7) */ request_id
           FROM apps.fnd_concurrent_requests fcr
          WHERE fcr.concurrent_program_id IN ( SELECT CONCURRENT_PROGRAM_ID 
                                                 FROM FND_CONCURRENT_PROGRAMS_VL 
                                                WHERE CONCURRENT_PROGRAM_NAME IN ( 'XXWC_OM_SHORTRENT_ACKL'
                                                                                  ,'XXWC_OM_RENTAL_PS_RCT1'
                                                                                  ,'XXWC_OM_PACK_SLIP'
                                                                                  ,'XXWC_INT_ORD_PACK'
                                                                                  ,'XXWC_OM_PACK_SLIP_POD'
                                                                                  ,'XXWC_INT_ORD_PACK_POD'
                                                                                 )
                                                AND ENABLED_FLAG = 'Y' ) 
            AND (status_code = 'R' OR status_code = 'I')
            AND (phase_code = 'R' OR phase_code ='P')
            AND request_id = p_request_id;
            
      l_request_id    NUMBER;
      lc_phase        VARCHAR2 (100);
      lc_status       VARCHAR2 (100);
      lc_dev_phase    VARCHAR2 (100);
      lc_dev_status   VARCHAR2 (100);
      lc_message      VARCHAR2 (100);
      l_sec           VARCHAR2 (2000);
      lc_waited BOOLEAN;
      l_log_seq       NUMBER := '888';
   BEGIN
      LOOP
         IF p_program_type = 'XDOBURST' THEN
           l_sec := 'Inside XDOBURST wait_for_running_request order header id ' || p_request_id;
            write_log (  66666666
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
                 
            OPEN cu_running_xdoburst_requests( p_request_id );
           FETCH cu_running_xdoburst_requests INTO l_request_id;
  
            IF cu_running_xdoburst_requests%NOTFOUND
            THEN
               CLOSE cu_running_xdoburst_requests;
               EXIT;
            END IF;
            CLOSE cu_running_xdoburst_requests;
         ELSE 
           l_sec := 'Inside CFD wait_for_running_request order header id ' || p_request_id;
            write_log (  66666666
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
                 
           OPEN cu_running_cfd_requests( p_request_id );
           FETCH cu_running_cfd_requests INTO l_request_id;
  
            IF cu_running_cfd_requests%NOTFOUND
            THEN
               CLOSE cu_running_cfd_requests;
               EXIT;
            END IF;
            CLOSE cu_running_cfd_requests;
         END IF;  

         IF l_request_id > 0
         THEN
            lc_waited:=fnd_concurrent.wait_for_request (
                                             request_id      => l_request_id,
                                             INTERVAL        => 6,      --INTERVAL--interval Number of seconds to wait between checks
                                             max_wait        => 3600,   --max wait --Maximum number of seconds to wait for the request completion
                                             phase           => lc_phase,
                                             status          => lc_status,
                                             dev_phase       => lc_dev_phase,
                                             dev_status      => lc_dev_status,
                                             MESSAGE         => lc_message
                                            );
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_sec := 'Error in wait_for_running_request order header id ' || l_request_id;
         write_log (  NULL
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
         RAISE;
   END wait_for_running_request;
   
  /********************************************************************************
  ProcedureName : Submit_Bursting_Program
  Purpose       : This will used to submit the Bursting program to generate the POD
  ----------------------------------------------------------------------------------*/
  PROCEDURE Submit_Bursting_Program(  p_header_id              IN NUMBER  
                                     ,p_delivery_id            IN NUMBER  --Rev#3.0
                                     ,p_concurrent_request_id  IN NUMBER
                                     ,p_application_short_name IN VARCHAR2 
                                     ,p_batch_size             IN NUMBER 
                                   ) AS
    
    l_error_code         NUMBER;
    l_error_msg          VARCHAR2(4000);
	l_sec                VARCHAR2 (2000);
	l_sch_delivery_date  VARCHAR2(11);
    l_Shipment_Type      VARCHAR2 (150);
    l_request_id         NUMBER := 0;
    l_line_count         NUMBER := 0;
    l_order_type_id      NUMBER := 0;
    
    v_wait               BOOLEAN;                   
    v_phase              VARCHAR2 (50);
    v_status             VARCHAR2 (50);
    v_dev_status         VARCHAR2 (50);
    v_dev_phase          VARCHAR2 (50);
    v_message            VARCHAR2 (250);
    l_log_seq            NUMBER := 222;
    --l_request_id_count   NUMBER;
  BEGIN
     l_sec := 'Start of Procedure submit_Bursting_program for p_concurrent_request_id: '||p_concurrent_request_id;
     
     write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
     
 	 l_request_id := fnd_request.submit_request(application => 'XXWC'
	                                       , program     => 'XXWC_XDOBURSTREP'
	                                       , description => NULL
	                                       , start_time  => SYSDATE
	                                       , sub_request => FALSE
	                                       , argument1   => 'Y'
	                                       , argument2   => p_concurrent_request_id
	                                       , argument3   => 'N' 
	                                       );
     --Commit Updates 	
     COMMIT;
     
     l_sec := 'After submitting concurrent request XXWC_XDOBURSTREP l_request_id: '||l_request_id;
     
     write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
     
     IF NVL (l_request_id, 0) > 0 THEN
      
        UPDATE xxwc.xxwc_om_dms2_sign_cap_gtt_tbl 
           SET burst_pod_con_req_id = l_request_id
              ,last_update_date     = SYSDATE 
         WHERE header_id =  p_header_id
           AND NVL(delivery_id, -1) = NVL(p_delivery_id, -1)--Rev#3.0 
           AND lcode = 1
           AND pod_method_image_type IN ('Signature', 'Photo of Delivered Product');
       
        COMMIT;   
         
        g_request_id_count := g_request_id_count+1;
		g_request_ids_tbl (g_request_id_count) := l_request_id;
				
  		l_sec := 'XXWC_XDOBURSTREP Concurrent request '  || l_request_id || ' has been submitted for p_concurrent_request_id ' || p_concurrent_request_id;
		write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
     ELSE
        l_sec := 'Could not submit XXWC_XDOBURSTREP Concurrent request for p_concurrent_request_id ' || p_concurrent_request_id;
		write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
     END IF;
     		 
	 IF g_request_ids_tbl.COUNT >= p_batch_size THEN
	    FOR i IN g_request_ids_tbl.FIRST .. g_request_ids_tbl.LAST
	    LOOP
	       wait_for_running_request ( p_request_id   => g_request_ids_tbl(i)
	                                 ,p_program_type => 'XDOBURST'   );
	    END LOOP;  
		g_request_ids_tbl.delete;
	    g_request_id_count := 0;
     END IF;
			 
     l_sec := 'End of Procedure : submit_Bursting_program';
     
     write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
     
  EXCEPTION
    WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      l_error_msg := 'Error in the submit_Bursting_program procedure'||SQLERRM;
    
      write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
    
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.submit_Bursting_program',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	  						
  END submit_Bursting_program;
   
  PROCEDURE attach_POD_file( p_header_id              IN NUMBER
                            ,p_concurrent_request_id  IN NUMBER
                            ,p_delivery_id            IN NUMBER   
                            ,p_application_short_name IN VARCHAR2 
                           ) 
  IS
    l_rowid                ROWID;
    l_attached_document_id NUMBER;
    l_document_id          NUMBER;
    l_media_id             NUMBER;
    l_category_id          NUMBER;
    l_pk1_value            fnd_attached_documents.pk1_value%TYPE:= 0; --Primary Key information that uniquely identifies the information
    l_description          fnd_documents_tl.description%TYPE:= NULL;
    l_filename             VARCHAR2(240) := NULL;
    l_file_path            VARCHAR2(240) := 'XXWC_DMS2_POD_FILES'; --Server Directory Path for upload files
    l_seq_num              NUMBER;
    l_blob_data            BLOB;
    l_blob                 BLOB;
    l_bfile                BFILE;
    l_byte                 NUMBER;
    l_fnd_user_id          NUMBER;
    l_short_datatype_id    NUMBER;
    x_blob                 BLOB;
    fils                   BFILE;
    blob_length            integer;
    l_entity_name          VARCHAR2(100) := 'OE_ORDER_HEADERS'; --Must be defined before or use existing ones. Table: FND_DOCUMENT_ENTITIES
    l_category_name        VARCHAR2(100) := 'Order Attachment(nonprint)'; --Must be defined before or use existing ones.
    l_order_number         oe_order_headers_all.order_number%TYPE;   
    
    l_error_code           NUMBER;
    l_error_msg            VARCHAR2(4000);
	l_sec                  VARCHAR2 (2000);     
	
	l_log_seq              NUMBER := 333;
	l_order_type_id        apps.oe_order_headers_all.order_type_id%TYPE;
 BEGIN
      l_sec := 'Start of Procedure : attach_POD_file';
      write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
      
      IF p_application_short_name = 'ONT' THEN
        l_sec := 'Inside sales order assignment';   
         BEGIN
            SELECT order_number, order_type_id
              INTO l_order_number, l_order_type_id
              FROM oe_order_headers_all
             WHERE header_id = p_header_id;
         EXCEPTION 
         WHEN OTHERS THEN
            l_sec :='Order number not exist for p_header_id' || p_header_id;
            write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
         END;    
         
         IF l_order_type_id IN (FND_PROFILE.VALUE('XXWC_INTERNAL_ORDER_TYPE') , FND_PROFILE.VALUE('XXWC_STANDARD_ORDER_TYPE')) THEN
            l_description   := l_order_number ||'-'||p_delivery_id;
         ELSE
            l_description   := l_order_number ;
         END IF;   
         l_entity_name   := 'OE_ORDER_HEADERS';
         l_category_name := 'Order Attachment(nonprint)';
      ELSIF p_application_short_name = 'PO' THEN
         l_sec := 'Inside PO assignment';   
         NULL;
      END IF;   
      
     -- Select Category id for Attachments
     SELECT category_id
       INTO l_category_id
       FROM apps.fnd_document_categories_vl
      WHERE USER_NAME = l_category_name;
        
      l_filename    := p_concurrent_request_id||'.pdf';
      l_pk1_value   := p_header_id;
      
      SELECT fnd_documents_s.NEXTVAL
        INTO l_document_id
        FROM DUAL;

      SELECT fnd_attached_documents_s.NEXTVAL
        INTO l_attached_document_id
        FROM DUAL;

      SELECT NVL (MAX (seq_num), 0) + 10
        INTO l_seq_num
        FROM fnd_attached_documents
       WHERE pk1_value = l_pk1_value
         AND entity_name = l_entity_name;

        -- Select User_id
      SELECT user_id
        INTO l_fnd_user_id
        FROM apps.fnd_user
       WHERE user_name = 'XXWC_INT_SALESFULFILLMENT'; --Username who will be uploading file.

       -- Get Data type id for Short Text types of attachments
       SELECT datatype_id
         INTO l_short_datatype_id
         FROM apps.fnd_document_datatypes
        WHERE NAME = 'FILE';

       -- Select nexvalues of document id, attached document id and
      SELECT apps.fnd_documents_s.NEXTVAL,
             apps.fnd_attached_documents_s.NEXTVAL
        INTO l_document_id, l_attached_document_id
        FROM DUAL;

      SELECT MAX (file_id) + 1
        INTO l_media_id
       FROM fnd_lobs;
      
      l_sec := 'Before opening file';   
      fils := BFILENAME (l_file_path, l_filename);

      -- Obtain the size of the blob file
      DBMS_LOB.fileopen (fils, DBMS_LOB.file_readonly);
      blob_length := DBMS_LOB.getlength (fils);
      DBMS_LOB.fileclose (fils);

     -- Insert a new record into the table containing the
     -- filename you have specified and a LOB LOCATOR.
     -- Return the LOB LOCATOR and assign it to x_blob.
     l_sec := 'Before inserting into fnd_lobs';
      INSERT INTO fnd_lobs
       (file_id, file_name, file_content_type, upload_date,
         expiration_date, program_name, program_tag, file_data,
         LANGUAGE, oracle_charset, file_format
       )
      VALUES (l_media_id, l_filename, 'application/pdf',
              SYSDATE, NULL, 'FNDATTCH', NULL, EMPTY_BLOB (), 
              'US'
              ,'WE8MSWIN1252'
             -- , 'UTF8'
              , 'binary'
      )
      RETURNING file_data
      INTO x_blob;

      -- Load the file into the database as a BLOB
      DBMS_LOB.OPEN (fils, DBMS_LOB.lob_readonly);
      DBMS_LOB.OPEN (x_blob, DBMS_LOB.lob_readwrite);
      DBMS_LOB.loadfromfile (x_blob, fils, blob_length);

      -- Close handles to blob and file
      DBMS_LOB.CLOSE (x_blob);
      DBMS_LOB.CLOSE (fils);

      l_sec  := 'FND_LOBS File Id Created is ' || l_media_id;
      write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
      COMMIT;
      
      -- This package allows user to share file across multiple orgs or restrict to single org
      fnd_documents_pkg.insert_row
                                  (x_rowid             => l_rowid,
                                   x_document_id       => l_document_id,
                                   x_creation_date     => SYSDATE,
                                   x_created_by        => l_fnd_user_id,
                                   x_last_update_date  => SYSDATE,
                                   x_last_updated_by   => l_fnd_user_id,
                                   x_last_update_login => fnd_profile.VALUE('LOGIN_ID'),
                                   x_datatype_id       => l_short_datatype_id,
                                   x_security_id       => NULL, --Security ID defined in your Attchments, Usaully SOB ID/ORG_ID
                                   x_publish_flag      => 'Y', --This flag allow the file to share across multiple organization
                                   x_category_id       => l_category_id,
                                   x_security_type     => 4,
                                   x_usage_type        => 'O',
                                   x_language          => 'US', 
                                   x_description       => l_description,
                                   x_file_name         => l_filename,
                                   x_media_id          => l_media_id,
                                   x_title             => 'POD'
      );

      COMMIT;

      --Description informations will be stored in below table based on languages.
       fnd_documents_pkg.insert_tl_row
                                       (x_document_id       => l_document_id,
                                        x_creation_date     => SYSDATE,
                                        x_created_by        => l_fnd_user_id,
                                        x_last_update_date  => SYSDATE,
                                        x_last_updated_by   => l_fnd_user_id,
                                        x_last_update_login => fnd_profile.VALUE('LOGIN_ID'),
                                        x_language          => 'US',
                                        x_description       => l_description,
                                        x_title             => 'POD'
       );
      COMMIT;

      fnd_attached_documents_pkg.insert_row
                                            (x_rowid                    => l_rowid,
                                             x_attached_document_id     => l_attached_document_id,
                                             x_document_id              => l_document_id,
                                             x_creation_date            => SYSDATE,
                                             x_created_by               => l_fnd_user_id,
                                             x_last_update_date         => SYSDATE,
                                             x_last_updated_by          => l_fnd_user_id,
                                             x_last_update_login        => fnd_profile.VALUE('LOGIN_ID'),
                                             x_seq_num                  => l_seq_num,
                                             x_entity_name              => l_entity_name,
                                             x_column1                  => NULL,
                                             x_pk1_value                => l_pk1_value,
                                             x_pk2_value                => NULL,
                                             x_pk3_value                => NULL,
                                             x_pk4_value                => NULL,
                                             x_pk5_value                => NULL,
                                             x_automatically_added_flag => 'N',
                                             x_datatype_id              => 6,
                                             x_category_id              => l_category_id,
                                             x_security_type            => 4,
                                             x_security_id              => NULL, --Security ID defined in your Attchments, Usaully SOB ID/ORG_ID
                                             x_publish_flag             => 'Y',
                                             x_language                 => 'US',
                                             x_description              => l_description,
                                             x_file_name                => l_filename,
                                             x_media_id                 => l_media_id
                                      );

        l_sec := 'End of Procedure MEDIA ID CREATED IS: ' || l_media_id;
        
        write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
        COMMIT;         
   EXCEPTION
   WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      l_error_msg := 'Error in the attach_POD_file procedure'||SQLERRM;
      write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
    
     --Rev# 3.0 <Start There is no need to send the error to dev group the error will
     --be reported to BSA team when the bursting process runs
     /** xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.attach_POD_file',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	 **/
       --Rev# 3.0 >End            
   END;

  PROCEDURE Resubmit_CFD_request( p_header_id              IN NUMBER,
                                  p_delivery_id            IN NUMBER, 
                                  p_order_type_id          IN NUMBER,
                                  p_concurrent_request_id  IN NUMBER,
                                  p_order_type             IN VARCHAR2,
                                  o_request_id             OUT VARCHAR2,
                                  o_status                 OUT VARCHAR2
                                )
   AS
      l_error_code         NUMBER;
      l_error_msg          VARCHAR2(4000);
	  l_sec                VARCHAR2 (2000);
	  l_log_seq            NUMBER := 555;
	  l_line_count         NUMBER:=0;
	  
	  l_argument1 fnd_concurrent_requests.argument1%TYPE;
      l_argument2 fnd_concurrent_requests.argument2%TYPE; 
      l_argument3 fnd_concurrent_requests.argument3%TYPE; 
      l_argument4 fnd_concurrent_requests.argument4%TYPE; 
      l_argument5 fnd_concurrent_requests.argument5%TYPE; 
      l_argument6 fnd_concurrent_requests.argument6%TYPE; 
      l_argument7 fnd_concurrent_requests.argument7%TYPE; 
      l_argument8 fnd_concurrent_requests.argument8%TYPE; 
      l_argument9 fnd_concurrent_requests.argument9%TYPE; 
      l_argument10 fnd_concurrent_requests.argument10%TYPE; 
      l_argument11 fnd_concurrent_requests.argument11%TYPE;
      l_argument12 fnd_concurrent_requests.argument12%TYPE;
      l_resub_req_id              NUMBER;
      v_wait                      BOOLEAN;
      v_phase                     VARCHAR2 (50);
      v_status                    VARCHAR2 (50);
      v_dev_status                VARCHAR2 (50);
      v_dev_phase                 VARCHAR2 (50);
      v_message                   VARCHAR2 (250);
      l_cfd_concurrent_program_id NUMBER;
      l_cfd_pgm_short_name        VARCHAR2 (250);
      ln_wcd_count                NUMBER;
      l_report_err                EXCEPTION;  
   BEGIN
      l_sec := 'Start of Procedure : Resubmit_CFD_request';
      write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);  
                 
      l_resub_req_id := NULL; -- This value will be null when there is no line exceptions.
       
      l_sec := 'before checking for line exception p_header_id : '|| p_header_id ||' p_delivery_id: '||p_delivery_id
                ||' p_order_type_id: '||p_order_type_id ;    
      write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);  
                 

      IF p_order_type = 'WCD' THEN
         --Get parameter details for the WC direct
         l_sec := 'Inside WCD -Get the parameter details from apps.fnd_concurrent_requests the XXWC_OM_PACK_SLIP/XXWC_INT_ORD_PACK for p_header_id : ' ||p_header_id;
         write_log (  p_header_id
                     , NULL
                     , l_sec 
                     , l_log_seq+1); 
         BEGIN
            SELECT ARGUMENT1, ARGUMENT2, ARGUMENT3, ARGUMENT4, ARGUMENT5, ARGUMENT6, ARGUMENT7, ARGUMENT8, ARGUMENT9, ARGUMENT10, ARGUMENT11, ARGUMENT12, concurrent_program_id
              INTO l_argument1, l_argument2, l_argument3, l_argument4, l_argument5, l_argument6, l_argument7, l_argument8, l_argument9, l_argument10, l_argument11, l_argument12, l_cfd_concurrent_program_id
              FROM apps.fnd_concurrent_requests fcr
             WHERE 1=1
               AND argument2             = p_header_id
               AND argument10            = p_delivery_id
			   AND concurrent_program_id IN (70401 ,75404) --XXWC_OM_PACK_SLIP/ XXWC_INT_ORD_PACK
			   AND ROWNUM=1;
			   
			   l_sec := 'Inside WCD -Get the parameter details from fnd_concurrent_requests the XXWC_OM_PACK_SLIP/XXWC_INT_ORD_PACK for p_header_id : ' ||p_header_id;
               write_log (  p_header_id
                           , NULL
                           , l_sec 
                           , l_log_seq+1);  
         EXCEPTION
         WHEN NO_DATA_FOUND THEN
            BEGIN--get the parameter from print log 
               l_sec := 'Inside WCD -Get the parameter details from xxwc_print_log_tbl the XXWC_OM_PACK_SLIP/XXWC_INT_ORD_PACK for p_header_id : ' ||p_header_id;
               write_log (  p_header_id
                           , NULL
                           , l_sec 
                           , l_log_seq+1);  
         
               SELECT ARGUMENT1, ARGUMENT2, ARGUMENT3, ARGUMENT4, ARGUMENT5, ARGUMENT6, ARGUMENT7, ARGUMENT8, ARGUMENT9, ARGUMENT10, ARGUMENT11, ARGUMENT12, concurrent_program_id
                 INTO l_argument1, l_argument2, l_argument3, l_argument4, l_argument5, l_argument6, l_argument7, l_argument8, l_argument9, l_argument10, l_argument11, l_argument12, l_cfd_concurrent_program_id
                 FROM  xxwc.xxwc_print_log_tbl xplt
                WHERE 1=1
                  AND header_id             = p_header_id
                  AND argument10            = p_delivery_id
                  --AND request_id            = p_concurrent_request_id--p_concurrent_request_id will be null for the WC Direct orders
                  AND concurrent_program_id IN (70401 ,75404) --XXWC_OM_PACK_SLIP/ XXWC_INT_ORD_PACK
                  AND ROWNUM=1;
            EXCEPTION
            WHEN OTHERS THEN
               UPDATE xxwc.xxwc_om_dms2_sign_cap_gtt_tbl--xxwc_om_dms2_sign_capture_tbl
                  SET ship_confirm_status            = 'COMPLETED'--Rev#3.0
                     ,ship_confirm_exception	     = 'Request id not found in xxwc_print_log_tbl'
                     ,last_update_date               = SYSDATE 
                WHERE header_id                      = p_header_id
                  AND delivery_id                    = p_delivery_id
                  AND lcode                          = 1
                  AND pod_method_image_type IN ('Signature', 'Photo of Delivered Product') 
                  AND NVL(ship_confirm_status, 'NO') = 'NO';
              
               RAISE l_report_err;
            END;--get the parameter from print log 				  
         WHEN OTHERS THEN
            UPDATE xxwc.xxwc_om_dms2_sign_cap_gtt_tbl--xxwc_om_dms2_sign_capture_tbl
               SET ship_confirm_status            = 'COMPLETED'--Rev#3.0
                  ,ship_confirm_exception	     = 'Request id not found in fnd_concurrent_requests'
                  ,last_update_date               = SYSDATE 
             WHERE header_id                      = p_header_id
               AND delivery_id                    = p_delivery_id
               AND lcode                          = 1
               AND pod_method_image_type IN ('Signature', 'Photo of Delivered Product') 
               AND NVL(ship_confirm_status, 'NO') = 'NO';

           RAISE l_report_err;
         END;--get the parameter from fnd_concurrent_requests
      ELSIF p_order_type = 'STDINT' THEN
        BEGIN--get the parameter from print log 
           l_sec := 'Inside std/int -Get the parameter details from xxwc.xxwc_print_log_tbl the XXWC_OM_PACK_SLIP/XXWC_INT_ORD_PACK for p_header_id : ' ||p_header_id;
           write_log (  p_header_id
                       , NULL
                       , l_sec 
                       , l_log_seq+1);  
         
           SELECT ARGUMENT1, ARGUMENT2, ARGUMENT3, ARGUMENT4, ARGUMENT5, ARGUMENT6, ARGUMENT7, ARGUMENT8, ARGUMENT9, ARGUMENT10, ARGUMENT11, ARGUMENT12, concurrent_program_id
             INTO l_argument1, l_argument2, l_argument3, l_argument4, l_argument5, l_argument6, l_argument7, l_argument8, l_argument9, l_argument10, l_argument11, l_argument12, l_cfd_concurrent_program_id
             FROM  xxwc.xxwc_print_log_tbl xplt
            WHERE 1=1
              AND header_id             = p_header_id
              AND argument10            = p_delivery_id
              AND request_id            = p_concurrent_request_id
              AND concurrent_program_id IN (70401 ,75404, 338410 ) --XXWC_OM_PACK_SLIP/ XXWC_INT_ORD_PACK/XXWC_OM_PACK_SLIP_0_COPIES
              AND ROWNUM=1;
        EXCEPTION
        WHEN OTHERS THEN
           UPDATE xxwc.xxwc_om_dms2_sign_cap_gtt_tbl--xxwc_om_dms2_sign_capture_tbl
              SET ship_confirm_status            = 'COMPLETED'--Rev#3.0
                 ,ship_confirm_exception	     = 'Request id not found in xxwc_print_log_tbl'
                 ,last_update_date               = SYSDATE 
            WHERE header_id                      = p_header_id
              AND delivery_id                    = p_delivery_id
              AND concurrent_request_id          = p_concurrent_request_id
              AND lcode                          = 1
              AND pod_method_image_type IN ('Signature', 'Photo of Delivered Product') 
              AND NVL(ship_confirm_status, 'NO') = 'NO';
              
            COMMIT;           
   
           RAISE l_report_err;
        END;--get the parameter from print log 	
      ELSE
        NULL;-- Resubmit for the short term rental orders and long term rental orders.
      END IF;		
    
      --Rev# 3.0 <Start
      IF l_cfd_concurrent_program_id IN ( 70401 ,338410 ) THEN
         l_cfd_pgm_short_name := 'XXWC_OM_PACK_SLIP_POD';
      ELSE
         l_cfd_pgm_short_name := 'XXWC_INT_ORD_PACK_POD';
      END IF;   
      --Rev# 3.0 >End
      	
      l_sec := 'Before re-submitting the XXWC_OM_PACK_SLIP/XXWC_INT_ORD_PACK for l_cfd_pgm_short_name : ' ||l_cfd_pgm_short_name;
      write_log (  p_header_id
                     , NULL
                     , l_sec 
                     , l_log_seq+1); 


	  ----------------------------------------------------------------------
	  -- Submit "XXWC_OM_PACK_SLIP"
	  --------------------------------------------------------------------------
	  l_resub_req_id := fnd_request.submit_request(   application => 'XXWC'
	                                                , program     => l_cfd_pgm_short_name--Rev# 3.0
	                                                , description => NULL
                                                    , start_time  => SYSDATE
                                                    , sub_request => FALSE
                                                    , argument1   => l_argument1 --Organization
                                                    , argument2   => l_argument2 --Order Number
                                                    , argument3   => l_argument3 --Print Pricing
                                                    , argument4   => 'Y'--l_argument4 --Reprint
	                                                , argument5   => l_argument5 --Print Kit Details
	                                                , argument6   => l_argument6 --Send to Rightfax Yes or No?
	                                                , argument7   => l_argument7 --Fax Number
                                                    , argument8   => l_argument8 --Rightfax Comment
	                                                , argument9   => l_argument9 --Print Hazmat
	                                                , argument10  => l_argument10 --Delivery ID
										            , argument11  => l_argument11 --Print Allocations
										            , argument12  => l_argument12 --Group Id
	                                                );         
      COMMIT;
               
      UPDATE xxwc.xxwc_om_dms2_sign_cap_gtt_tbl--xxwc_om_dms2_sign_capture_tbl
         SET resubmit_pod_con_req_id        = l_resub_req_id
            ,last_update_date               = SYSDATE 
       WHERE header_id                      = p_header_id
         AND delivery_id                    = p_delivery_id
          --AND concurrent_request_id          = p_concurrent_request_id
         AND lcode                          = 1
         AND pod_method_image_type IN ('Signature', 'Photo of Delivered Product');
    
      COMMIT;
          
      l_sec := 'After submitting concurrent request '||l_cfd_pgm_short_name||' l_resub_req_id: '||l_resub_req_id;
     
      write_log (  p_header_id
                  , NULL
                  , l_sec 
                  , l_log_seq+1);
                    
      IF l_resub_req_id = 0 THEN
         o_status     := 'E';
      ELSE
         o_status     := 'S';
      END IF;
      o_request_id := l_resub_req_id;
      
      l_sec := 'End of Procedure : Resubmit_CFD_request';                
      write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);                                    
    EXCEPTION
    WHEN l_report_err THEN
      o_request_id := l_resub_req_id;
      o_status     := 'E';
    WHEN OTHERS THEN
      o_request_id := l_resub_req_id;
      o_status     := 'E';
       
      l_error_code    := SQLCODE;
      l_error_msg := 'Error in the Resubmit_CFD_request procedure'||SQLERRM;
    
      write_log (  p_header_id
                 , NULL
                 , l_error_msg 
                 , l_log_seq+1);
    
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.Resubmit_CFD_request',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	 
   END;--Resubmit_CFD_request
   
   PROCEDURE resubmit_requests  ( p_header_id              IN NUMBER,
                                  p_delivery_id            IN NUMBER, 
                                  p_order_type_id          IN NUMBER, 
                                  p_concurrent_request_id  IN NUMBER,
                                  p_batch_size             IN NUMBER 
                                )
   AS
      l_error_code         NUMBER;
      l_error_msg          VARCHAR2(4000);
	  l_sec                VARCHAR2 (2000);
	  l_log_seq            NUMBER := 444;
	  l_line_count         NUMBER:=0;
	  
	  l_argument1 fnd_concurrent_requests.argument1%TYPE;
      l_argument2 fnd_concurrent_requests.argument2%TYPE; 
      l_argument3 fnd_concurrent_requests.argument3%TYPE; 
      l_argument4 fnd_concurrent_requests.argument4%TYPE; 
      l_argument5 fnd_concurrent_requests.argument5%TYPE; 
      l_argument6 fnd_concurrent_requests.argument6%TYPE; 
      l_argument7 fnd_concurrent_requests.argument7%TYPE; 
      l_argument8 fnd_concurrent_requests.argument8%TYPE; 
      l_argument9 fnd_concurrent_requests.argument9%TYPE; 
      l_argument10 fnd_concurrent_requests.argument10%TYPE; 
      l_argument11 fnd_concurrent_requests.argument11%TYPE;
      l_argument12 fnd_concurrent_requests.argument12%TYPE;
      l_resub_req_id              NUMBER;
      v_wait                      BOOLEAN;
      v_phase                     VARCHAR2 (50);
      v_status                    VARCHAR2 (50);
      v_dev_status                VARCHAR2 (50);
      v_dev_phase                 VARCHAR2 (50);
      v_message                   VARCHAR2 (250);
      l_cfd_concurrent_program_id NUMBER;
      l_cfd_pgm_short_name        VARCHAR2 (250);
      ln_wcd_count                NUMBER;
      l_report_err                EXCEPTION;  
      l_o_request_id              NUMBER;
      l_o_status                  VARCHAR2 (1);
      l_fnd_request_count         NUMBER;--Rev#3.0    
   BEGIN
      l_sec := 'Start of Procedure : resubmit_requests';
      write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1); 
       
      l_sec := 'before checking for line exception p_header_id : '|| p_header_id ||' p_delivery_id: '||p_delivery_id
                ||' p_order_type_id: '||p_order_type_id || ' p_concurrent_request_id: ' ||p_concurrent_request_id;    
                
      write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);  
      
      l_resub_req_id := NULL; -- This value will be null when there is no line exceptions.
      
      IF p_order_type_id IN (FND_PROFILE.VALUE('XXWC_INTERNAL_ORDER_TYPE') , FND_PROFILE.VALUE('XXWC_STANDARD_ORDER_TYPE'), 1013, 1014) THEN
         
         l_sec := 'inside p_order_type_id'||p_order_type_id;
         
         write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
                 
         --Check if it is WC direct order, if yes then get the parameter details from the print log from and resubmit the request.
         SELECT count(*)
           INTO ln_wcd_count
           FROM fnd_lookup_values flv
               ,apps.oe_order_lines_all ool
               ,xxwc.xxwc_wsh_shipping_stg xwss
          WHERE flv.lookup_type  = 'XXWC_DMS2_WCD_SHIP_METHOD'
            AND flv.enabled_flag = 'Y'
            AND flv.description  = ool.shipping_method_code
            AND ool.line_id      = xwss.line_id
            AND ool.header_id    = p_header_id
            AND xwss.delivery_id = p_delivery_id
            AND SYSDATE BETWEEN flv.start_date_active AND NVL(flv.end_date_active, SYSDATE + 1);        
         
         --Below logic only execute for the WC Direct Orders-- Without exception
         IF p_order_type_id IN (FND_PROFILE.VALUE('XXWC_INTERNAL_ORDER_TYPE') , FND_PROFILE.VALUE('XXWC_STANDARD_ORDER_TYPE'))
           AND ln_wcd_count >0 THEN --If it is a WCD order then resubmit pack slip report using paramater from the print log table     
               l_sec := 'Before callling Resubmit_CFD_request';
               write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
                 
               Resubmit_CFD_request(   p_header_id              => p_header_id
                                     , p_delivery_id            => p_delivery_id
                                     , p_order_type_id          => p_order_type_id
                                     , p_concurrent_request_id  => p_concurrent_request_id
                                     , p_order_type             => 'WCD'
                                     , o_request_id             => l_o_request_id
                                     , o_status                 => l_o_status
                                   );
               l_sec := 'After callling Resubmit_CFD_request : '||l_o_request_id ||' l_o_status: '||l_o_status ;
               write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
                  
               IF l_o_status = 'E' THEN
                  RAISE l_report_err;
               ELSE
                  l_resub_req_id := l_o_request_id;
               END IF;
         ELSE
            SELECT COUNT(1) 
              INTO l_line_count
              FROM xxwc.xxwc_om_dms2_ship_confirm_tbl dms2_sc
              WHERE dms2_sc.header_id      = p_header_id
               AND (dms2_sc.delivery_id   =  p_delivery_id OR p_delivery_id IS NULL) --p_delivery_id is null for rental orders
               AND dms2_sc.line_exception IS NOT NULL; --'Customer Refused', 'Damaged Item', 'Wrong Product','Wrong QTY';
            
            --Rev#2.0 Start <
             SELECT COUNT(1)  --Rev#3.0
               INTO l_fnd_request_count --Rev#3.0
               FROM apps.fnd_concurrent_requests fcr
              WHERE request_id            = p_concurrent_request_id
	      	    AND concurrent_program_id IN  (70401 ,75404, 76401, 66418, 338410 ) --XXWC_OM_PACK_SLIP/ XXWC_INT_ORD_PACK/XXWC_OM_RENTAL_PS_RCT1/XXWC_OM_SHORTRENT_ACKL/XXWC_OM_PACK_SLIP_0_COPIES 
				AND rownum=1;  
            --Rev#2.0 End >    		     

            IF l_line_count > 0 
            OR l_fnd_request_count = 0--Rev#3.0
            THEN
               l_sec := 'Resubmit the cocurrent program for p_header_id : ' ||p_header_id;
               write_log (  p_header_id
                       , NULL
                       , l_sec 
                       , l_log_seq+1);
           
            BEGIN
               SELECT ARGUMENT1, ARGUMENT2, ARGUMENT3, ARGUMENT4, ARGUMENT5, ARGUMENT6, ARGUMENT7, ARGUMENT8, ARGUMENT9, ARGUMENT10, ARGUMENT11, ARGUMENT12, concurrent_program_id
                 INTO l_argument1, l_argument2, l_argument3, l_argument4, l_argument5, l_argument6, l_argument7, l_argument8, l_argument9, l_argument10, l_argument11, l_argument12,l_cfd_concurrent_program_id
                 FROM apps.fnd_concurrent_requests fcr
                WHERE request_id            = p_concurrent_request_id
				  AND concurrent_program_id IN  (70401 ,75404, 76401, 66418, 338410 ) --XXWC_OM_PACK_SLIP/ XXWC_INT_ORD_PACK/XXWC_OM_RENTAL_PS_RCT1/XXWC_OM_SHORTRENT_ACKL/XXWC_OM_PACK_SLIP_0_COPIES 
				  AND rownum=1;   
				
                IF l_cfd_concurrent_program_id IN  (70401, 75404, 338410) THEN
                
                   --Rev# 3.0 <Start
                   IF l_cfd_concurrent_program_id IN ( 70401 ,338410 ) THEN
                      l_cfd_pgm_short_name := 'XXWC_OM_PACK_SLIP_POD';
                   ELSE
                      l_cfd_pgm_short_name := 'XXWC_INT_ORD_PACK_POD';
                   END IF;   
                   --Rev# 3.0 >End
      
                   l_sec := 'Inside XXWC_OM_PACK_SLIP/XXWC_INT_ORD_PACK for p_concurrent_request_id : ' ||p_concurrent_request_id;
                   write_log (  p_header_id
                          , NULL
                          , l_sec 
                          , l_log_seq+1); 
                   
                   l_sec := 'Before re-submitting the XXWC_OM_PACK_SLIP/XXWC_INT_ORD_PACK for l_cfd_pgm_short_name : ' ||l_cfd_pgm_short_name;
                   write_log (  p_header_id
                          , NULL
                          , l_sec 
                          , l_log_seq+1); 
                                   
				   -----------------------------------------------------------------------
	               -- Submit "XXWC_OM_PACK_SLIP_POD"
	               --------------------------------------------------------------------------
	               l_resub_req_id := fnd_request.submit_request(  application => 'XXWC'
	                                                            , program     => l_cfd_pgm_short_name --Rev# 3.0
	                                                            , description => NULL
	                                                            , start_time  => SYSDATE
	                                                            , sub_request => FALSE
	                                                            , argument1   => l_argument1 --Organization
	                                                            , argument2   => l_argument2 --Order Number
	                                                            , argument3   => l_argument3 --Print Pricing
										                        , argument4   => 'Y'--l_argument4 --Reprint
	                                                            , argument5   => l_argument5 --Print Kit Details
	                                                            , argument6   => l_argument6 --Send to Rightfax Yes or No?
	                                                            , argument7   => l_argument7 --Fax Number
										                        , argument8   => l_argument8 --Rightfax Comment
	                                                            , argument9   => l_argument9 --Print Hazmat
	                                                            , argument10  => l_argument10 --Delivery ID
										                        , argument11  => l_argument11 --Print Allocations
										                        , argument12  => l_argument12 --Group Id
	                                                           );         
                   COMMIT;
                
                   UPDATE xxwc.xxwc_om_dms2_sign_cap_gtt_tbl--xxwc_om_dms2_sign_capture_tbl
                      SET resubmit_pod_con_req_id        = l_resub_req_id
                         ,last_update_date               = SYSDATE 
                    WHERE header_id                      = p_header_id
                      AND delivery_id                    = p_delivery_id
                      AND concurrent_request_id          = p_concurrent_request_id
                      AND lcode                          = 1
                      AND pod_method_image_type IN ('Signature', 'Photo of Delivered Product');
                           
                   COMMIT;
          
                   l_sec := 'After submitting concurrent request '||l_cfd_pgm_short_name||' l_resub_req_id: '||l_resub_req_id;
     
                   write_log (  p_header_id
                            , NULL
                            , l_sec 
                            , l_log_seq+1);
                
                ELSIF l_cfd_concurrent_program_id = 76401 THEN --XXWC_OM_RENTAL_PS_RCT1
                           
                   l_sec := 'Before re-submitting the XXWC_OM_RENTAL_PS_RCT1 for p_concurrent_request_id : '||p_concurrent_request_id;
                   write_log (  p_header_id
                          , NULL
                          , l_sec 
                          , l_log_seq+1); 
                                   
				   -----------------------------------------------------------------------
	               -- Submit "XXWC_OM_RENTAL_PS_RCT1"
	               --------------------------------------------------------------------------
	               l_resub_req_id := fnd_request.submit_request(  application => 'XXWC'
	                                                            , program     => 'XXWC_OM_RENTAL_PS_RCT1'
	                                                            , description => NULL
	                                                            , start_time  => SYSDATE
	                                                            , sub_request => FALSE
	                                                            , argument1   => l_argument1 --Organization
	                                                            , argument2   => l_argument2 --Order Number
	                                                            , argument3   => l_argument3 --Print Pricing
										                        , argument4   => 'Y'--l_argument4 --Reprint
	                                                            , argument5   => l_argument5 --Print Kit Details
	                                                            , argument6   => l_argument6 --Send to Rightfax Yes or No?
	                                                            , argument7   => l_argument7 --Fax Number
										                        , argument8   => l_argument8 --Rightfax Comment
	                                                            , argument9   => l_argument9 --Print Hazmat
	                                                            , argument10  => l_argument10 --Start Date
	                                                           );         
                   COMMIT;
                
                   UPDATE xxwc.xxwc_om_dms2_sign_cap_gtt_tbl--xxwc_om_dms2_sign_capture_tbl
                      SET resubmit_pod_con_req_id        = l_resub_req_id
                         ,last_update_date               = SYSDATE 
                    WHERE header_id                      = p_header_id
                      AND NVL(delivery_id, -1)           = NVL(p_delivery_id, -1) --Rev#3.0
                      AND concurrent_request_id          = p_concurrent_request_id
                      AND lcode                          = 1
                      AND pod_method_image_type IN ('Signature', 'Photo of Delivered Product');

                   COMMIT;
          
                   l_sec := 'After submitting concurrent request XXWC_OM_RENTAL_PS_RCT1 l_resub_req_id: '||l_resub_req_id;
     
                   write_log (  p_header_id
                            , NULL
                            , l_sec 
                            , l_log_seq+1);
                            
                ELSIF l_cfd_concurrent_program_id = 66418 THEN --XXWC_OM_SHORTRENT_ACKL
                           
                   l_sec := 'Before re-submitting the XXWC_OM_SHORTRENT_ACKL for p_concurrent_request_id : '||p_concurrent_request_id;
                   write_log (  p_header_id
                          , NULL
                          , l_sec 
                          , l_log_seq+1); 
                                   
				   -----------------------------------------------------------------------
	               -- Submit "XXWC_OM_SHORTRENT_ACKL"
	               --------------------------------------------------------------------------
	               l_resub_req_id := fnd_request.submit_request(  application => 'XXWC'
	                                                            , program     => 'XXWC_OM_SHORTRENT_ACKL'
	                                                            , description => NULL
	                                                            , start_time  => SYSDATE
	                                                            , sub_request => FALSE
	                                                            , argument1   => l_argument1 --Sales Order Number (Low)
	                                                            , argument2   => l_argument2 --Sales Order Number (High)
	                                                            , argument3   => l_argument3 --Printer Name
										                        , argument4   => 'Y'--l_argument4 --Reprint
	                                                            , argument5   => l_argument5 --Send to Rightfax Yes or No
	                                                            , argument6   => l_argument6 --Fax Number
	                                                            , argument7   => l_argument7 --Rightfax Comment
	                                                           );         
                   COMMIT;
                
                   UPDATE xxwc.xxwc_om_dms2_sign_cap_gtt_tbl--xxwc_om_dms2_sign_capture_tbl
                      SET resubmit_pod_con_req_id        = l_resub_req_id
                         ,last_update_date               = SYSDATE 
                    WHERE header_id                      = p_header_id
                      AND NVL(delivery_id, -1)           = NVL(p_delivery_id, -1) --Rev#3.0
                      AND concurrent_request_id          = p_concurrent_request_id
                      AND pod_method_image_type IN ('Signature', 'Photo of Delivered Product') 
                      AND lcode                          = 1;
     
                   COMMIT;
          
                   l_sec := 'After submitting concurrent request XXWC_OM_SHORTRENT_ACKL l_resub_req_id: '||l_resub_req_id;
     
                   write_log (  p_header_id
                            , NULL
                            , l_sec 
                            , l_log_seq+1);
                END IF; --l_cfd_concurrent_program_id 
                
                v_wait := fnd_concurrent.wait_for_request (  l_resub_req_id
                                                            , 6      --INTERVAL--interval Number of seconds to wait between checks
                                                            , 3600   --max wait --Maximum number of seconds to wait for the request completion
                                                            , v_phase
                                                            , v_status
                                                            , v_dev_phase
                                                            , v_dev_status
                                                            , v_message);
               
                l_sec := 'Program Completion Status: ' || SUBSTR (v_status, 1, 4000);
     
                write_log (  p_header_id
                            , NULL
                            , l_sec 
                            , l_log_seq+1);
                       
            EXCEPTION
            WHEN OTHERS  THEN
               l_sec := 'Request not found for the p_concurrent_request_id : ' ||p_concurrent_request_id;
               
               IF p_order_type_id IN (FND_PROFILE.VALUE('XXWC_INTERNAL_ORDER_TYPE') , FND_PROFILE.VALUE('XXWC_STANDARD_ORDER_TYPE')) THEN
                  Resubmit_CFD_request(   p_header_id              => p_header_id
                                        , p_delivery_id            => p_delivery_id
                                        , p_order_type_id          => p_order_type_id
                                        , p_concurrent_request_id  => p_concurrent_request_id
                                        , p_order_type             => 'STDINT'
                                        , o_request_id             => l_o_request_id
                                        , o_status                 => l_o_status
                                   );
                  l_sec := 'After callling Resubmit_CFD_request : '||l_o_request_id ||' l_o_status: '||l_o_status ;
                  write_log (  p_header_id
                             , NULL
                             , l_sec 
                             , l_log_seq+1);
                  
                  IF l_o_status = 'E' THEN
                     RAISE l_report_err;
                  ELSE
                     l_resub_req_id := l_o_request_id;
                  END IF;
               
                  write_log (  p_header_id
                             , NULL
                             , l_sec 
                             , l_log_seq+1);
                  --l_resub_req_id := p_concurrent_request_id; -- need to call the resubmit request using print log data
               ELSE
                  -- This can happen for the rental orders.
                  UPDATE xxwc.xxwc_om_dms2_sign_cap_gtt_tbl--xxwc_om_dms2_sign_capture_tbl
                     SET ship_confirm_status            = 'COMPLETED' --Rev#3.0
                        ,ship_confirm_exception	        = 'Request id not found in fnd_concurrent_requests'
                        ,last_update_date               = SYSDATE 
                   WHERE header_id                      = p_header_id
                     AND NVL(delivery_id, -1)           = NVL(p_delivery_id, -1)--Rev#3.0
                     AND concurrent_request_id          = p_concurrent_request_id
                     AND lcode                          = 1
                     AND pod_method_image_type IN ('Signature', 'Photo of Delivered Product')
                     AND NVL(ship_confirm_status, 'NO') = 'NO';
               END IF;--Resubmit_CFD_request using request data from xxwc_print_log_tbl table.
            END; --  Resubmit request from fnd_concurrent_requests                  
         END IF;   --l_line_count 
        END IF;--Above logic only execute for the WC Direct Orders-- Without exception    
      END IF;         
      
      IF NVL (l_resub_req_id, 0) > 0 THEN
        g_request_id_count := g_request_id_count+1;
		g_request_ids_tbl (g_request_id_count) := l_resub_req_id;
      END IF;
     		 
	  IF g_request_ids_tbl.COUNT >= p_batch_size THEN
	    FOR i IN g_request_ids_tbl.FIRST .. g_request_ids_tbl.LAST
	    LOOP
	       wait_for_running_request ( p_request_id   => g_request_ids_tbl(i) 
	                                 ,p_program_type => 'CFD');
	    END LOOP;  
		g_request_ids_tbl.delete;
	    g_request_id_count := 0;
      END IF;
  
      l_sec := 'End of Procedure : resubmit_requests';    
      
      write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);                                    
    EXCEPTION
    WHEN l_report_err THEN
      l_sec := 'Error submitting the request id';
      write_log (  p_header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);   
       
    WHEN OTHERS THEN
      l_error_code    := SQLCODE;
      l_error_msg := 'Error in the resubmit_requests procedure'||SQLERRM;
    
      write_log (  p_header_id
                 , NULL
                 , l_error_msg 
                 , l_log_seq+1);
    
      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package||'.resubmit_requests',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => g_module);	 
   END;
 
PROCEDURE ship_confirm(p_errbuf              OUT VARCHAR2
                      ,p_retcode             OUT VARCHAR2
                      ,p_user_name           IN VARCHAR2
                      ,p_responsibility_name IN VARCHAR2
                      ,p_order_type_id       IN NUMBER 
                      ,p_run_zone            IN VARCHAR2
					  ,p_order_number        IN VARCHAR2
					  ,p_debug_flag          IN VARCHAR2 
					) IS  

    l_user_id             fnd_user.user_id%TYPE;
    l_org_id              NUMBER := 162;
    l_responsibility_id   NUMBER;
    l_resp_application_id NUMBER;
    l_err_msg             CLOB;
    l_sec                 VARCHAR2(2000) DEFAULT 'START';
    l_procedure           VARCHAR2(50) := 'SHIP_CONFIRM';
    l_return_status       VARCHAR2(1);
    l_msg_data            VARCHAR2(2000);
    l_last_delta_date     DATE;
    l_hold_count          NUMBER;
    l_from_org_id         NUMBER;
    l_pick_rule           VARCHAR2(50);
    l_order_type          VARCHAR2(100);
    l_ship_from           VARCHAR2(100);
    l_exists              VARCHAR2(1) DEFAULT 'F';
    l_exception_exists    VARCHAR2(1) DEFAULT 'F';
    l_ship                VARCHAR2(1) DEFAULT 'N';
    l_shipping_method     NUMBER;

    l_sender              VARCHAR2(100);
    l_sid                 VARCHAR2(8);
    l_body                VARCHAR2(32767);
    l_body_header         VARCHAR2(32767);
    l_body_detail         VARCHAR2(32767);
    l_body_footer         VARCHAR2(32767);
    
    l_log_seq             NUMBER := 555;
    l_sc_request_id       NUMBER;

  BEGIN  
    IF NVL(p_debug_flag, 'N') = 'Y' THEN
       debug_on;
    ELSE
       debug_off;
    END IF;

    l_sec := 'Set policy context for org_id';
    -- Mandatory initialization for R12
    mo_global.set_policy_context('S', l_org_id);
    mo_global.init('ONT');		

    -- Deriving Ids from initialization variables
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM apps.fnd_user
       WHERE user_name = upper(p_user_name)
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'UserName ' || p_user_name || ' not defined in Oracle';
        RAISE program_error;
    END;

    BEGIN
      SELECT responsibility_id, application_id
        INTO l_responsibility_id, l_resp_application_id
        FROM apps.fnd_responsibility_vl
       WHERE responsibility_name = p_responsibility_name
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Responsibility ' || p_responsibility_name ||
                     ' not defined in Oracle';
        RAISE program_error;
    END;

    l_sc_request_id := fnd_profile.value('conc_request_id');
     
    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    l_sec := 'Apps Initialize';

    fnd_global.apps_initialize(l_user_id
                              ,l_responsibility_id
                              ,l_resp_application_id);


    UPDATE xxwc.XXWC_OM_DMS2_SHIP_CONFIRM_TBL
    SET    ship_Confirm_status='NO'
          ,last_update_date         = SYSDATE 
          ,ship_confirm_request_id  = l_sc_request_id
    WHERE 1=1 
    --AND order_number IS NOT NULL
	AND NVL(p_order_number, order_number) = order_number
    AND ship_confirm_status IS NULL
    AND delivery_id IS NOT NULL; -- Rental orders will have null delivery id's

    --------------------------------------------------------------------------
    -- Inserting into XXWC_OM_DMS2_SHIP_CONF_GTT_TBL
    --------------------------------------------------------------------------
  l_sec := 'Inserting into XXWC_OM_DMS2_SHIP_CONF_GTT_TBL';
  INSERT INTO xxwc.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL
  SELECT * FROM xxwc.XXWC_OM_DMS2_SHIP_CONFIRM_TBL
    WHERE ship_confirm_status = 'NO'
      --AND order_number IS NOT NULL
	  AND NVL(p_order_number, order_number) = order_number
      AND delivery_id IS NOT NULL;

  BEGIN
   UPDATE xxwc.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL stg
      SET stg.ship_confirm_status = 'ERROR'
        , stg.ship_confirm_exception = 'PAYMENT_HOLD_EXISTS'
        ,last_update_date         = SYSDATE 
    WHERE 1 = 1
      AND stg.delivery_id         IS NOT NULL
      AND stg.ship_confirm_status = 'NO'
      AND EXISTS (SELECT '1'
                     FROM apps.oe_hold_sources_all  ohs
                        , apps.oe_order_headers_all ooh
                    WHERE 1 = 1
                      AND stg.order_number                      = ooh.order_number
                      AND to_char(ooh.header_id)                = ohs.hold_entity_id
                      AND ohs.hold_entity_code                  = 'O'
                      AND ohs.hold_id                           IN (13,14)
                      AND NVL(p_order_number, ooh.order_number) = ooh.order_number
                      AND ohs.released_flag                     = 'N');
  EXCEPTION
  WHEN OTHERS THEN
     NULL;
  END;

    l_sec := 'Ready to start loop';

      FOR cur_ship IN (SELECT DISTINCT sc.order_number
                                    ,sc.delivery_id
                                    ,oh.header_id
                                    ,sc.signed_by
                                    ,sc.notes
                                    ,sc.dispatch_date
                                    ,ot.name order_type
                                    ,oh.order_type_id
                                    ,sc.route_number
                       FROM xxwc.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL sc
                           ,oe_order_headers             oh
                           ,oe_transaction_types_tl      ot
                      WHERE ship_confirm_status = 'NO'
                        AND sc.order_number = oh.order_number
                        AND oh.order_type_id = ot.transaction_type_id
                        AND (oh.order_type_id = p_order_type_id OR p_order_type_id IS NULL)
						AND NVL(p_order_number, oh.order_number) = oh.order_number
                        AND EXISTS (SELECT /*+ RESULT_CACHE */  flv.lookup_code
                                      FROM fnd_lookup_values flv
                                     WHERE 1 = 1
                                       AND oh.order_type_id = FND_PROFILE.VALUE('XXWC_STANDARD_ORDER_TYPE')
                                       AND flv.lookup_type           = 'XXWC_DMS_BRANCH_LIST'
                                       AND flv.enabled_flag          = 'Y'
                                       AND to_number(flv.meaning)    = oh.ship_from_org_id
                                       AND (p_run_zone IS NULL OR flv.tag = p_run_zone)
                                       AND TRUNC(SYSDATE) BETWEEN flv.start_date_active AND nvl(flv.end_date_active, SYSDATE + 1)
                                       UNION
                                       SELECT '1'
                                         FROM dual
                                        WHERE oh.order_type_id = FND_PROFILE.VALUE('XXWC_INTERNAL_ORDER_TYPE')
                                       ))
    LOOP

      l_hold_count       := NULL;
      l_return_status    := NULL;
      l_from_org_id      := NULL;
      l_ship_from        := NULL;
      l_exists           := 'F';
      l_exception_exists := 'F';
      l_ship             := 'N';
      l_shipping_method  := NULL;

	  l_sec := 'Inside Loop...cur_ship.header_id'||cur_ship.header_id;
	  
      write_log (  cur_ship.header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);

      BEGIN
        SELECT 'Y'
          INTO l_ship
          FROM fnd_lookup_values
         WHERE 1 = 1
           AND lookup_type = 'XXWC_DMS_ORDERTYPE_SHIPCONFIRM'
           AND enabled_flag = 'Y'
           AND SYSDATE BETWEEN start_date_active AND
               nvl(end_date_active, SYSDATE + 1)
           AND lookup_code = cur_ship.order_type_id;

      EXCEPTION
        WHEN no_data_found THEN
          DELETE FROM xxwc.XXWC_OM_DMS2_SHIP_CONFIRM_TBL
           WHERE order_number = cur_ship.order_number
             AND nvl(delivery_id, -1) = nvl(cur_ship.delivery_id, -1);

    --------------------------------------------------------------------------
    -- Delete from XXWC_OM_DMS2_SHIP_CONF_GTT_TBL
    --------------------------------------------------------------------------
          DELETE FROM xxwc.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL
           WHERE order_number = cur_ship.order_number
             AND nvl(delivery_id, -1) = nvl(cur_ship.delivery_id, -1);

          COMMIT;
      END;

      IF l_ship = 'Y'
      THEN
	  
	    l_sec := 'Inside l_ship...';
	    
	    write_log (  cur_ship.header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);

        --Need ship_from_org for the order header_id/delivery_id to ship confirm lines
        BEGIN

          SELECT DISTINCT ship_from_org_id
            INTO l_from_org_id
            FROM xxwc.xxwc_wsh_shipping_stg
           WHERE header_id = cur_ship.header_id
             AND delivery_id = cur_ship.delivery_id;
        EXCEPTION
          WHEN no_data_found THEN
            l_from_org_id := -1;

          WHEN too_many_rows THEN
            l_from_org_id := -1;

        END;

        IF l_from_org_id = -1
        THEN

          --lines cannot ship confirm issue with getting ship_from_org_id
          UPDATE xxwc.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL
             SET ship_confirm_exception = 'Delivery does not exists in shipping tbl'
                ,ship_confirm_status    = 'ERROR'
                ,last_update_date       = SYSDATE
                ,last_updated_by        = fnd_global.user_id()
           WHERE order_number = cur_ship.order_number
             AND delivery_id = cur_ship.delivery_id
             AND route_number = cur_ship.route_number; 

        ELSE

          -- need pick rule for ship comfirm
          SELECT organization_code
            INTO l_ship_from
            FROM org_organization_definitions
           WHERE organization_id = l_from_org_id;

          IF cur_ship.order_type LIKE '%STANDARD%'
          THEN
            l_pick_rule := l_ship_from || ' Standard'; 
          ELSE
            l_pick_rule := l_ship_from || '-All Internal';
          END IF;

          --Check for DMS delivery exceptions
          BEGIN
            BEGIN
               SELECT 'S'
                 INTO l_exception_exists
                 FROM xxwc.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL 
                WHERE order_number = cur_ship.order_number
                  AND delivery_id = cur_ship.delivery_id
                  AND route_number = cur_ship.route_number 
                   AND STOP_EXCEPTION IS NOT NULL 
                  AND rownum = 1;
            EXCEPTION
            WHEN no_data_found THEN
              l_exception_exists := 'F';
            END;
            
            IF l_exception_exists <> 'S' THEN
               IF FND_PROFILE.VALUE ('XXWC_OM_DMS2_SO_UPDATE_QTY') = 'Y' THEN --qty update profile-do not ship confirmt the order for 'Wrong QTY' exception
                  SELECT 'T'
                    INTO l_exception_exists
                    FROM xxwc.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL 
                   WHERE order_number = cur_ship.order_number
                     AND delivery_id = cur_ship.delivery_id
                     AND route_number = cur_ship.route_number 
                     AND ((LINE_EXCEPTION IS NOT NULL 
                         AND LINE_EXCEPTION <> 'Wrong QTY')
                      OR
                      STOP_EXCEPTION IS NOT NULL )
                    AND rownum = 1;
                ELSE
                  SELECT 'T'
                    INTO l_exception_exists
                    FROM xxwc.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL 
                   WHERE order_number = cur_ship.order_number
                     AND delivery_id = cur_ship.delivery_id
                     AND route_number = cur_ship.route_number 
                     AND (LINE_EXCEPTION IS NOT NULL
                      OR
                      STOP_EXCEPTION IS NOT NULL )
                    AND rownum = 1;
                END IF;    
             END IF;
            --'Cannot Ship Confirm an order with any exception other than 'WRONG QTY' from MyLogistics delivery confirmation data.'
          EXCEPTION
            WHEN no_data_found THEN
              l_exception_exists := 'F';
          END;

          IF l_exception_exists IN ('S', 'T')
          THEN
            --lines cannot ship confirm with a delivery exception
            UPDATE xxwc.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL 
               SET ship_confirm_exception = DECODE (l_exception_exists, 'S', 'Stop Exception', 'Delivery Exception')
                  ,ship_confirm_status    = 'EXCEPTION'
                  ,last_update_date       = SYSDATE
                  ,last_updated_by        = fnd_global.user_id()
             WHERE order_number = cur_ship.order_number
               AND delivery_id = cur_ship.delivery_id
               AND route_number = cur_ship.route_number; --Version 1.2

          ELSE
            -- Check if order is on hold
            SELECT xxwc_ont_routines_pkg.count_order_holds(cur_ship.header_id)
              INTO l_hold_count
              FROM dual;

            IF l_hold_count > 0
            THEN
              --lines cannot ship confirm delivery with holds
              UPDATE xxwc.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL 
                 SET ship_confirm_exception = 'Holds exists for Order'
                    ,ship_confirm_status    = 'ERROR'
                    ,last_update_date       = SYSDATE
                    ,last_updated_by        = fnd_global.user_id()
               WHERE order_number = cur_ship.order_number
                 AND delivery_id = cur_ship.delivery_id
                 AND route_number = cur_ship.route_number; 

            ELSE

              -- Prevent Ship Confirm if there is a special with item cost zero
              BEGIN

                SELECT 'T'
                  INTO l_exists
                  FROM xxwc_wsh_shipping_stg_v
                 WHERE header_id = cur_ship.header_id
                   AND delivery_id = cur_ship.delivery_id
                   AND item_type IN ('SPECIAL', 'REPAIR')
                   AND nvl(item_cost, 0) = 0
                   AND status = 'OUT_FOR_DELIVERY' 
                   AND rownum = 1;

                --dbms_output.put_line('l_exists:  '||l_exists);
              EXCEPTION
                WHEN no_data_found THEN
                  l_exists := 'F';
              END;

              IF l_exists = 'T'
              THEN
                --lines cannot ship confirm delivery with SPECIAL and no cost
                UPDATE xxwc.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL 
                   SET ship_confirm_exception = 'One line is SPECIAL/REPAIR and no cost'
                      ,ship_confirm_status    = 'ERROR'
                      ,last_update_date       = SYSDATE
                      ,last_updated_by        = fnd_global.user_id()
                 WHERE order_number = cur_ship.order_number
                   AND delivery_id = cur_ship.delivery_id
                   AND route_number = cur_ship.route_number; 

              ELSE

                BEGIN
                  --check if delivery has multiple ship method codes
                  SELECT COUNT(DISTINCT shipping_method_code)
                    INTO l_shipping_method
                    FROM oe_order_lines ol
                   WHERE header_id = cur_ship.header_id
                     AND ol.header_id IN
                         (SELECT header_id
                            FROM xxwc.xxwc_wsh_shipping_stg ws
                           WHERE ws.header_id = ol.header_id
                             AND ws.line_id = ol.line_id 
                             AND delivery_id = cur_ship.delivery_id);

                  --'Cannot Ship Confirm an order with multiple shipping methods for a delivery.'
                EXCEPTION
                  WHEN no_data_found THEN
                    l_shipping_method := -1;
                END;

                IF l_shipping_method <> 1
                THEN
                  --lines cannot ship confirm
                  UPDATE xxwc.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL 
                     SET ship_confirm_exception = 'Delivery has multiple shipping methods'
                        ,ship_confirm_status    = 'ERROR'
                        ,last_update_date       = SYSDATE
                        ,last_updated_by        = fnd_global.user_id()
                   WHERE order_number = cur_ship.order_number
                     AND delivery_id = cur_ship.delivery_id
                     AND route_number = cur_ship.route_number; 

                ELSE
                  l_sec := 'Update Order Quantity... cur_ship.header_id'|| cur_ship.header_id;   
                 
                  write_log (  cur_ship.header_id
                   , NULL
                   , l_sec 
                   , l_log_seq+1);
                  
                  l_sec := 'Update Order Quantity... XXWC_OM_DMS2_SO_UPDATE_QTY value '|| FND_PROFILE.VALUE ('XXWC_OM_DMS2_SO_UPDATE_QTY');  
                 
                  write_log (  cur_ship.header_id
                   , NULL
                   , l_sec 
                   , l_log_seq+1);
                  
                  IF FND_PROFILE.VALUE ('XXWC_OM_DMS2_SO_UPDATE_QTY') = 'Y' THEN
                     xxwc_wsh_shipping_extn_pkg.update_lines_qty( p_header_id        => cur_ship.header_id
                                                              ,p_delivery_id      => cur_ship.delivery_id  
                                                              ,p_ship_from_org_id => l_from_org_id
                                                              ,o_return_status    => l_return_status
                                                              ,o_return_msg       => l_msg_data);
                     l_sec :=  'update_lines_qty delivery_id = ' || cur_ship.delivery_id ||
                               ' status = ' || l_return_status ||
                               '  -  Return MSG = ' || l_msg_data;
                  ELSE 
                     l_return_status := 'S';
                  END IF;

                  write_log (  cur_ship.header_id
                   , NULL
                   , l_sec 
                   , l_log_seq+1);                  
                      
				  IF l_return_status = 'S' THEN

                    l_sec := 'Ship Confirming Order...';
                     
                    write_log (  cur_ship.header_id
                      , NULL
                      , l_sec 
                      , l_log_seq+1);     

                     xxwc_wsh_shipping_extn_pkg.ship_confirm_order(p_header_id     => cur_ship.header_id
                                                               ,p_from_org_id   => l_from_org_id
                                                               ,p_delivery_id   => cur_ship.delivery_id
                                                               ,p_pick_rule     => l_pick_rule
                                                               ,p_return_status => l_return_status
                                                               ,p_return_msg    => l_msg_data);

                     l_sec := 'ship_confirm_order delivery_id = ' || cur_ship.delivery_id ||
                               ' status = ' || l_return_status ||
                               '  -  Return MSG = ' || l_msg_data;
                     
                      write_log (  cur_ship.header_id
                      , NULL
                      , l_sec 
                      , l_log_seq+1);   
                           
                     IF l_return_status = 'S'
                     THEN
                       --ship confirmed
                       l_sec := 'API Return Status Success... update status for header_id: ' ||
                                cur_ship.header_id || 'delivery_id: ' ||
                                cur_ship.delivery_id;
                      
                       write_log (  cur_ship.header_id
                      , NULL
                      , l_sec 
                      , l_log_seq+1);   
                      
                       UPDATE xxwc.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL 
                          SET ship_confirm_status = 'COMPLETED'
                             ,last_update_date    = SYSDATE
                             ,last_updated_by     = fnd_global.user_id()
                        WHERE order_number = cur_ship.order_number
                          AND delivery_id = cur_ship.delivery_id
                          AND route_number = cur_ship.route_number;

                     ELSE

				  
                       UPDATE xxwc.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL 
                          SET ship_confirm_status    = 'ERROR'
                             ,ship_confirm_exception = l_msg_data
                             ,last_update_date       = SYSDATE
                             ,last_updated_by        = fnd_global.user_id()
                        WHERE order_number = cur_ship.order_number
                          AND delivery_id = cur_ship.delivery_id
                          AND route_number = cur_ship.route_number; 

                    --email information if ship confirm fails
                          email_errors(cur_ship.order_number
                                ,cur_ship.delivery_id
                                ,l_msg_data);

                     END IF; --ship confirm
                  ELSE
                       UPDATE xxwc.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL 
                          SET ship_confirm_status    = 'ERROR'
                             ,ship_confirm_exception = l_msg_data
                             ,last_update_date       = SYSDATE
                             ,last_updated_by        = fnd_global.user_id()
                        WHERE order_number = cur_ship.order_number
                          AND delivery_id = cur_ship.delivery_id
                          AND route_number = cur_ship.route_number; 

                    --email information if ship confirm fails
                          email_errors(cur_ship.order_number
                                ,cur_ship.delivery_id
                                ,l_msg_data);
                  END IF;-- Update Quantity 

                END IF; --shipping method
              END IF; --special
            END IF; --holds
          END IF; --DMS exceptions
        END IF; --ship from org
      END IF; --order type is in lookup
    END LOOP;
    COMMIT;
 				  
    l_sec := 'Insert xxwc_om_fulfill_acceptance';
    write_log (   -1
                 , NULL
                 , l_sec 
                 , l_log_seq+1);  
    --------------------------------------------------------------------------
    -- Insert into XXWC_OM_FULFILL_ACCEPTANCE table
    --------------------------------------------------------------------------
    l_sec := 'Insert into XXWC_OM_FULFILL_ACCEPTANCE table';
    INSERT INTO xxwc.xxwc_om_fulfill_acceptance
      (header_id
      ,accepted_by
      ,accepted_date
      ,accepted_signature
      ,acceptance_comments
      ,process_flag
      ,error_msg
      ,creation_date
      ,created_by
      ,last_update_date
      ,last_updated_by
      ,last_update_login
      ,line_id)
      SELECT x1.header_id -- Header_id
            ,l_user_id -- Accepted_by
            ,gtt.dispatch_date -- Acceptance Date
            ,gtt.signed_by -- Accepted Signature
            ,gtt.notes -- Acceptance Comments
            ,'N' -- Process Flag
            ,NULL -- Error Message
            ,SYSDATE -- Creation Date
            ,l_user_id -- created by
            ,SYSDATE -- last_update_date
            ,l_user_id -- last_updated_by
            ,NULL -- last_update_login
            ,x1.line_id -- line_id
        FROM xxwc.xxwc_wsh_shipping_stg x1
           , xxwc.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL gtt
       WHERE 1 = 1
         AND gtt.delivery_id = x1.delivery_id
         AND x1.status = 'DELIVERED';
    COMMIT;

    --------------------------------------------------------------------------
    -- Call to API - XXWC_WSH_SHIPPING_EXTN_PKG.SYNC_DEL_LINE_STATUS
    --------------------------------------------------------------------------
    l_sec := 'Call to API - XXWC_WSH_SHIPPING_EXTN_PKG.SYNC_DEL_LINE_STATUS';
    xxwc_wsh_shipping_extn_pkg.sync_del_line_status(errbuf            => p_errbuf
                                                   ,retcode           => p_retcode
                                                   ,p_header_id       => NULL
                                                   ,p_last_delta_date => l_last_delta_date);
    COMMIT;

    --------------------------------------------------------------------------
    -- Update XXWC_OM_DES_SHIP_CONFIRM_TBL from Global Temp Table
    --------------------------------------------------------------------------
    l_sec := 'Update XXWC_OM_DES_SHIP_CONFIRM_TBL from Global Temp Table';
    UPDATE xxwc.XXWC_OM_DMS2_SHIP_CONFIRM_TBL stg
       SET (stg.ship_confirm_status , stg.ship_confirm_exception , stg.last_update_date , stg.last_updated_by) = (SELECT ship_confirm_status
                                                                                                                       , ship_confirm_exception
                                                                                                                       , last_update_date
                                                                                                                       , last_updated_by
                                                                                                                    FROM xxwc.XXWC_OM_DMS2_SHIP_CONF_GTT_TBL  gtt
                                                                                                                   WHERE gtt.order_number = stg.order_number
                                                                                                                     AND gtt.delivery_id  = stg.delivery_id
                                                                                                                     AND gtt.route_number = stg.route_number
                                                                                                                     AND ROWNUM = 1)
      WHERE stg.ship_confirm_status = 'NO';

    COMMIT;
    -- Version# 1.10 < End

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'ONT');

      p_retcode := 2;
      p_errbuf  := l_err_msg;
	 

    WHEN OTHERS THEN
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'ONT');

      p_retcode := 2;
      p_errbuf  := l_err_msg;

  END ship_confirm;
  
PROCEDURE email_errors(p_header_id   IN NUMBER
                        ,p_delivery_id IN NUMBER
                        ,p_error_msg   VARCHAR2) IS

    --Intialize Variables

    l_dflt_email   fnd_user.email_address%TYPE := FND_PROFILE.value('XXWC_SHIP_CONFIRM_ERROR_EMAIL'); 
	l_procedure    VARCHAR2(100) := 'EMAIL_ERRORS';
    l_sender       VARCHAR2(100);
    l_sid          VARCHAR2(8);
    l_body         VARCHAR2(32767);
    l_body_header  VARCHAR2(32767);
    l_body_detail  VARCHAR2(32767);
    l_body_footer  VARCHAR2(32767);
    l_sql_hint     VARCHAR2(32767);
    l_error_hint   VARCHAR2(1000);
    l_newline      NUMBER(20) := 1;
    l_mins_elapsed NUMBER(20);
    l_mins_allowed NUMBER(20) := 0;
    l_max_date     DATE;
    l_count        NUMBER;
    l_record_cnt   NUMBER := 0;
    l_err_msg      CLOB;

  BEGIN

    -- Local variables
    SELECT lower(NAME) INTO l_sid FROM v$database;
    l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';

    l_body_header := '<style type="text/css">
.style1 {
    border-style: solid;
    border-width: 1px;
}
.style2 {
    border: 1px solid #000000;
}
.style3 {
    border: 1px solid #000000;
    background-color: #FFFF00;
}
.style4 {
    color: #FFFF00;
    border: 1px solid #000000;
    background-color: #000000;
}
.style5 {
    font-size: large;
}
.style6 {
    font-size: xx-large;
}
.style7 {
    color: #FFCC00;
}
</style>
<p><span class="style6"><span class="style7"><strong>SHIP CONFIRM</strong></span><br />
<span class="style5">ERROR REPORT FROM:   ' ||
                     upper(l_sid) ||
                     ' </span></p>
<BR><table border="2" cellpadding="2" cellspacing="2" width="100%">' ||
                     '<td class="style2"><B>ORDER HEADER ID</B></td>
                        <td class="style2"><B>DELIVERY ID</B></td>' || '
                        <td class="style2"><B>ORA ERROR MSG</B></td><tr>';

    l_body_detail := l_body_detail || '<td class="style3">' || p_header_id ||
                     '</td><td class="style3">' || p_delivery_id ||
                     '</td><td class="style3">' ||
                     regexp_replace(p_error_msg, '[[:cntrl:]]', NULL) ||
                     '</td><TR>';

    l_body_footer := '</tr></table><br>Please manually ship confirm this order.<BR>';
    l_body        := l_body_header || l_body_detail || l_body_footer;

    xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                             ,p_from          => l_sender
                             ,p_text          => 'test'
                             ,p_subject       => '***ERROR IN SHIP CONFIRM PROCESS***'
                             ,p_html          => l_body
                             ,p_smtp_hostname => g_host
                             ,p_smtp_portnum  => g_hostport);

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := 'Error_Stack...' || dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => 'Email ship confirm error'
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_dflt_email
                                          ,p_module            => 'ONT');

    WHEN OTHERS THEN
      l_err_msg := 'Error_Stack...' || dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => 'Email ship confirm error'
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_dflt_email
                                          ,p_module            => 'ONT');

  END email_errors;
  
  PROCEDURE email_POD_errors( p_pod_con_req_id IN NUMBER) IS

    --Intialize Variables

    l_dflt_email   fnd_user.email_address%TYPE := FND_PROFILE.value('XXWC_SHIP_CONFIRM_ERROR_EMAIL'); 
	l_procedure    VARCHAR2(100) := 'EMAIL_ERRORS';
    l_sender       VARCHAR2(100);
    l_sid          VARCHAR2(8);
    l_body         VARCHAR2(32767);
    l_body_header  VARCHAR2(32767);
    l_body_detail  VARCHAR2(32767);
    l_body_footer  VARCHAR2(32767);
    l_sql_hint     VARCHAR2(32767);
    l_error_hint   VARCHAR2(1000);
    l_newline      NUMBER(20) := 1;
    l_mins_elapsed NUMBER(20);
    l_mins_allowed NUMBER(20) := 0;
    l_max_date     DATE;
    l_count        NUMBER;
    l_record_cnt   NUMBER := 0;
    l_err_msg      CLOB;
    l_send_email   VARCHAR2(1);
  BEGIN
    l_send_email := 'N';
    
    -- Local variables
    SELECT lower(NAME) INTO l_sid FROM v$database;
    
    IF l_sid = 'ebsdev' THEN
       l_dflt_email := 'rakesh.patel@hdsupply.com;Raghavendra.Velichetti@hdsupply.com';
    END IF;
    
    l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';

    l_body_header := '<style type="text/css">
.style1 {
    border-style: solid;
    border-width: 1px;
}
.style2 {
    border: 1px solid #000000;
}
.style3 {
    border: 1px solid #000000;
    background-color: #FFFF00;
}
.style4 {
    color: #FFFF00;
    border: 1px solid #000000;
    background-color: #000000;
}
.style5 {
    font-size: large;
}
.style6 {
    font-size: xx-large;
}
.style7 {
    color: #FFCC00;
}
</style>
<p><span class="style6"><span class="style7"><strong>POD PROCESS</strong></span><br />
<span class="style5">ERROR REPORT FROM:   ' ||
                     upper(l_sid) ||
                     ' </span></p>
<BR><table border="2" cellpadding="2" cellspacing="2" width="100%">' ||
                     '<td class="style2"><B>ORDER HEADER ID</B></td>
                        <td class="style2"><B>DELIVERY ID</B></td>' || '
                        <td class="style2"><B>ORA ERROR MSG</B></td><tr>';

    FOR cur_pod IN (SELECT * 
                      FROM xxwc.xxwc_om_dms2_sign_capture_tbl
                     WHERE ship_confirm_status      = 'ERROR'
                       AND gen_attch_pod_con_req_id	= p_pod_con_req_id  )
    LOOP
       l_send_email := 'Y';
       l_body_detail := l_body_detail || '<td class="style3">' || cur_pod.header_id ||
                     '</td><td class="style3">' || cur_pod.delivery_id ||
                     '</td><td class="style3">' ||
                     regexp_replace(cur_pod.ship_confirm_exception, '[[:cntrl:]]', NULL) ||
                     '</td><TR>';
    
    END LOOP;
    
    -- check is the burst concurrent requests failed.
    FOR cur_pod_burst_req IN ( SELECT  dms2_sc.header_id,
                             dms2_sc.delivery_id,
                             fcr.request_id,
                             fcr.completion_text
                      FROM  fnd_concurrent_requests fcr
                           ,xxwc.xxwc_om_dms2_sign_capture_tbl dms2_sc
                     WHERE fcr.status_code              = 'E'
                       AND dms2_sc.burst_pod_con_req_id = fcr.request_id
                       AND gen_attch_pod_con_req_id	= p_pod_con_req_id  )
    LOOP
       l_send_email := 'Y';
       l_body_detail := l_body_detail || '<td class="style3">' || cur_pod_burst_req.header_id ||
                     '</td><td class="style3">' || cur_pod_burst_req.delivery_id ||
                     '</td><td class="style3">' ||
                     regexp_replace('Bursting program concurrent request id: '||
                     cur_pod_burst_req.request_id||' Error message : '||
                     cur_pod_burst_req.completion_text, '[[:cntrl:]]', NULL) ||
                     '</td><TR>';
    END LOOP;
    
    l_body_footer := '</tr></table><br>Please check concurrent request log for details.<BR>';
    l_body        := l_body_header || l_body_detail || l_body_footer;
    
    IF l_send_email = 'Y' THEN
       xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                                ,p_from          => l_sender
                                ,p_text          => 'test'
                                ,p_subject       => '***ERROR IN POD PROCESS***'
                                ,p_html          => l_body
                                ,p_smtp_hostname => g_host
                                ,p_smtp_portnum  => g_hostport);
    END IF;                           

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := 'Error_Stack...' || dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => 'Email POD error'
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_dflt_email
                                          ,p_module            => 'ONT');

    WHEN OTHERS THEN
      l_err_msg := 'Error_Stack...' || dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => 'Email POD error'
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_dflt_email
                                          ,p_module            => 'ONT');

  END email_POD_errors;
   
 /*************************************************************************
     $Header XXWC_OM_DMS2_IB_PKG $
     Module Name: INSERT_SHIPMENT_DATA

     PURPOSE:   DESCARTES Project	
					   
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------            -------------------------
     1.0        11/13/2017  Ashwin Sridhar             To extract and insert the shipment data from Descrates.
**************************************************************************/
PROCEDURE insert_shipment_data(p_sequence_id   IN  NUMBER
                              ,p_xxwc_log_rec IN OUT xxwc_log_rec
                              ,p_order_number  OUT NUMBER
							  ,p_order_type_id OUT NUMBER
							  ,p_delivery_id   OUT NUMBER
							  ,p_cmd           OUT NUMBER
							  ,p_stype         OUT NUMBER
							  ,p_route_id      OUT VARCHAR2
							  ,p_stop_id       OUT NUMBER
							  ,p_lcode         OUT NUMBER
							  ,p_err_msg       OUT VARCHAR2) IS

--Cursor to Fetch the shipment details from the Descartes payload.
CURSOR cu_order_ship IS
SELECT Q1.JOB_ID
,      Q1.ORDER_NUMBER
,      Q.ROUTE_NAME
,      Q.ROUTE_ID
,      Q.STOP_ID
,      Q.DRIVER_NAME
,      Q.BRANCH_NAME BRANCH
,      Q.CMD
,      Q.STYPE
,      P1.DELIVERY_ID
,      P1.REQUEST_ID
,      P.ITEM_NUMBER ITEM_NUMBER
,      P.ITEM_ID
,      P.LINE_NUMBER
,      NVL(R.SIGNED_BY, R2.Printed_Name) SIGNED_BY
,      R1.Comments
---    S.SIGNATURE
,      UPPER(S.POD_METHOD) pod_method
FROM XXWC.XXWC_DMS2_LOG_TBL T
LEFT JOIN XMLTABLE('//MLW'
                    PASSING T.REQUEST_PAYLOAD_XMLTYPE
                    COLUMNS JOB_GROUP XMLTYPE PATH  '//MLW/Job',                    
                    ROUTE_NAME   VARCHAR2(200)   PATH  '//MLW/@RouteName',
                    ROUTE_ID     VARCHAR2(200)   PATH  '//MLW/@RouteId',
                    STOP_ID      NUMBER          PATH  '//MLW/@StopId',
                    DRIVER_NAME  VARCHAR2(200)   PATH  '//MLW/@DriverName',
                    BRANCH_NAME  VARCHAR2(200)   PATH  '//MLW/@GroupName',
                    CMD          NUMBER          PATH  '//MLW/@Cmd',
                    STYPE        NUMBER          PATH  '//MLW/@SType',
                    SignList   XMLTYPE           PATH  '//MLW/FieldData/Field/Child',
                    SignList1  XMLTYPE           PATH  '//MLW/FieldData/Field'
                    ) Q
ON (1=1)
LEFT JOIN XMLTABLE('//Job'
                    PASSING  Q.JOB_GROUP
                    COLUMNS JOB_ID NUMBER PATH '//Job/@JobId',
                    ORDER_NUMBER  NUMBER PATH  '//Job/@DispId',
                    ITEMLIST   XMLTYPE           PATH  '//Job/Item',
                    DELLIST    XMLTYPE           PATH  '//Job/Extension'
                    ) Q1   
ON (1=1)

  LEFT JOIN XMLTABLE('//Item'
                    PASSING  Q1.ITEMLIST
                    COLUMNS ITEM_NUMBER     VARCHAR2(100)  PATH '//Extension[@Name="ITEMCUSTOM"]/@Value',
                            LINE_NUMBER     VARCHAR2(100)  PATH '//Extension[@Name="ITEMCUSTOM2"]/@Value',
                    ITEM_ID NUMBER PATH '//Item/@ItemId'
                    ) P    
ON (1=1)
  LEFT JOIN XMLTABLE('/Child[@Name="Signed By"]'
                    PASSING  Q.SIGNLIST
                    COLUMNS SIGNED_BY     VARCHAR2(100)  PATH '/Child/@Value'
                    ) R    
ON (1=1)
  LEFT JOIN XMLTABLE('/Child[@Name="Comments"]'
                    PASSING  Q.SIGNLIST
                    COLUMNS Comments     VARCHAR2(100)  PATH '/Child/@Value'
                    ) R1  
ON (1=1)
  LEFT JOIN XMLTABLE('/Child[@Name="Printed Name"]'
                    PASSING  Q.SIGNLIST
                    COLUMNS Printed_Name     VARCHAR2(100)  PATH '/Child/@Value'
                    ) R2 
ON (1=1)
  LEFT JOIN XMLTABLE('/Extension[@Name="INTERNALORDERID"]'
                    PASSING  Q1.DELLIST
                    COLUMNS DELIVERY_ID     NUMBER  PATH '/Extension/@Value'
                    ) P1    
ON (1=1)
  LEFT JOIN XMLTABLE('/Extension[@Name="REQUESTID"]'
                    PASSING  Q1.DELLIST
                    COLUMNS REQUEST_ID     NUMBER  PATH '/Extension/@Value'
                    ) P1    
ON (1=1)
 LEFT JOIN XMLTABLE('/Field[@Name="Select proof of delivery method"]'
                    PASSING  Q.SignList1
                    COLUMNS POD_METHOD  VARCHAR2(100)  PATH '/Field/@Value'
                    ) S   
ON (1=1)
WHERE 1=1
AND T.sequence_id = p_sequence_id;

l_err_msg              CLOB;
l_procedure            VARCHAR2(100) := 'INSERT_SHIPMENT_DATA';
lv_blob                BLOB;
ln_order_number        NUMBER;
ln_order_type_id       NUMBER;
ln_delivery_id         NUMBER;
ln_cmd                 NUMBER;
ln_stype               NUMBER;
ln_stop_id             NUMBER;
ln_request_id          NUMBER;
ln_org_id              NUMBER := 162;
ln_responsibility_id   NUMBER;
ln_resp_application_id NUMBER;
lv_route_id            VARCHAR2(30);
lv_order_type          VARCHAR2(30);
lv_user_name           VARCHAR2(100);
lv_resp_name           VARCHAR2(100):= 'HDS Order Mgmt Pricing Super - WC';
ln_user_id             fnd_user.user_id%TYPE:=fnd_global.user_id;
lv_error_msg           VARCHAR2(4000);
le_exception           EXCEPTION;
l_sec                  VARCHAR2(2000);
lv_exception           VARCHAR2(100);
l_header_id            apps.oe_order_headers_all.header_id%TYPE;
ln_ordered_qty         NUMBER;
ln_delivered_qty       NUMBER;
lv_exception_name      VARCHAR2(50);
lv_exception_reason    VARCHAR2(50);
l_lcode                VARCHAR2(30);
lv_image               CLOB;
ln_pod_conc_request_id NUMBER; -- "XXWC DMS2 Generate and attach POD" concurrent request id
ln_sc_conc_request_id  NUMBER; -- "XXWC DMS2 Ship Confirm Delivered Orders" concurrent request id
l_pod_method_image_type VARCHAR2(150);

l_log_seq           NUMBER := 555;
BEGIN
  mo_global.set_policy_context('S', ln_org_id);
  mo_global.init('ONT');
  
  l_lcode       :=NULL;
  lv_image      :=NULL;   
     
  l_sec := 'Start insert_shipment_data sequence_id : '|| p_sequence_id;  
  write_log (  -1
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
                 
  --Lopping through the records inserted into the GTT using XMLTABLE Concept...
  FOR rec_order_ship IN cu_order_ship LOOP
  
    l_sec := 'Inside rec_order_ship order number: '|| rec_order_ship.order_number;
    write_log (  -1
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
    
    ln_order_number    :=rec_order_ship.ORDER_NUMBER;
	ln_delivery_id     :=rec_order_ship.DELIVERY_ID;
	ln_cmd             :=rec_order_ship.cmd;
	ln_stype           :=rec_order_ship.stype;
	lv_route_id        :=rec_order_ship.ROUTE_ID;
	ln_stop_id         :=rec_order_ship.STOP_ID;
	ln_ordered_qty     :=NULL;
	ln_delivered_qty   :=NULL;
	
	BEGIN
	
       SELECT oel.order_type_id
       ,      oot.name
       ,      header_id
        INTO  ln_order_type_id
       ,      lv_order_type
       ,      l_header_id
        FROM   oe_order_headers_all oel
        ,      oe_order_types_v     oot
        WHERE 1=1
          AND oel.order_type_id = oot.transaction_type_id
          AND oel.order_number  = ln_order_number;  
  
    EXCEPTION
    WHEN others THEN
	
       ln_order_type_id:=NULL;
       lv_order_type   :=NULL;
	   
    END;
    
    p_xxwc_log_rec.order_type := lv_order_type;
    
    l_sec := 'ln_order_type_id: '|| ln_order_type_id;
     write_log (  -1
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
	
	BEGIN
	  IF ln_cmd = '35' THEN
         SELECT a.EXCEPTION_VAL
	       INTO   lv_exception
           FROM (
                  SELECT P.EXCEPTION_VAL
                  ,      P.OWNER_ID
                    FROM  XXWC.XXWC_DMS2_LOG_TBL T
                  LEFT JOIN XMLTABLE('//MLW'
                          PASSING T.REQUEST_PAYLOAD_XMLTYPE
                          COLUMNS EXCEPTION_LIST XMLTYPE PATH  '//MLW/FieldData'
                          ) Q
             ON (1=1)
             LEFT JOIN XMLTABLE('//FieldData[@LCode="3"]'
                            PASSING  Q.EXCEPTION_LIST
                            COLUMNS EXCEPTION_VAL    VARCHAR2(100)  PATH '//Field/@Value',
                            OWNER_ID NUMBER PATH '@OwnerId'
                            ) P    
             ON (1=1)
          WHERE 1=1
            AND T.SEQUENCE_ID = p_sequence_id) a
          WHERE 1=1
           AND a.OWNER_ID=rec_order_ship.ITEM_ID;	
     ELSE
      SELECT a.EXCEPTION_VAL
	  INTO   lv_exception
      FROM (
      SELECT P.EXCEPTION_VAL
      ,      P.OWNER_ID
      FROM  XXWC.XXWC_DMS2_LOG_TBL T
      LEFT JOIN XMLTABLE('//MLW'
                          PASSING T.REQUEST_PAYLOAD_XMLTYPE
                          COLUMNS EXCEPTION_LIST XMLTYPE PATH  '//MLW/FieldData'
                          ) Q
      ON (1=1)
          LEFT JOIN XMLTABLE('//FieldData[@LCode="3"]'
                            PASSING  Q.EXCEPTION_LIST
                             COLUMNS EXCEPTION_VAL    VARCHAR2(100)  PATH '//Field[@Name="Exception Type"]/@Value',
                            OWNER_ID NUMBER PATH '@OwnerId'
                            ) P    
          ON (1=1)
      WHERE 1=1
      AND T.SEQUENCE_ID = p_sequence_id) a
      WHERE 1=1
      AND a.OWNER_ID=rec_order_ship.ITEM_ID;	
     END IF;
       
	 
	EXCEPTION
	WHEN others THEN
	
	  lv_exception:=NULL;
	
	END;
	
	IF lv_exception='Wrong QTY' and ln_cmd = '8' THEN
	
	  BEGIN
	
	    SELECT A.ORDERED_QTY
        ,      A.DELIVERED_QTY
	    INTO   ln_ordered_qty
	    ,      ln_delivered_qty
        FROM (
        SELECT R.PLANNED_QTY ORDERED_QTY
        ,      R.DELIVERED_QTY
	    ,      R.OWNER_ID
        FROM  XXWC.XXWC_DMS2_LOG_TBL T
        LEFT JOIN XMLTABLE('//MLW'
                        PASSING T.REQUEST_PAYLOAD_XMLTYPE
                        COLUMNS EXCEPTION_LIST XMLTYPE PATH  '//MLW/FieldData'
                        ) Q
        ON (1=1)
        LEFT JOIN XMLTABLE('//FieldData[@LCode="3"]'
                            PASSING  Q.EXCEPTION_LIST
                            COLUMNS  DELIVERED_QTY  VARCHAR2(100)  PATH '//Field[@Name="Actual Delivered Qty"]/@Value',
                                     PLANNED_QTY    VARCHAR2(100)  PATH '//Field[@Name="Original Qty"]/@Value',
                            OWNER_ID NUMBER PATH '@OwnerId'
                            ) R  
        ON (1=1)
        WHERE 1=1
        AND T.SEQUENCE_ID = p_sequence_id) A
        WHERE 1=1
        AND a.OWNER_ID=rec_order_ship.ITEM_ID;	  
	  
	  EXCEPTION
	  WHEN others THEN
	
	    ln_ordered_qty  :=NULL;
		ln_delivered_qty:=NULL;
	  
	  END;
	  
	END IF;
	
	IF rec_order_ship.CMD='8' AND rec_order_ship.STYPE='3'  --AND lv_order_type IN ('STANDARD ORDER','INTERNAL ORDER') --Needed for all orders for line exceptions  
	THEN	
	BEGIN
       l_sec := 'Inside rec_order_ship order number: '|| rec_order_ship.order_number || 'CMD : '||rec_order_ship.CMD|| 'STYPE : '|| rec_order_ship.STYPE;
     
       write_log (  -1
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
     
       INSERT INTO XXWC.XXWC_OM_DMS2_SHIP_CONFIRM_TBL
       (ORDER_NUMBER    
       ,HEADER_ID             
       ,LINE_NUMBER                  
       ,DELIVERY_ID                  
       ,BRANCH                       
       ,ITEM_NUMBER   
       ,ORDERED_QUANTITY        
       ,DELIVERED_QTY                
       ,STOP_EXCEPTION               
       ,LINE_EXCEPTION               
       ,SIGNED_BY                    
       ,NOTES                        
       ,DRIVER_NAME                  
       ,ROUTE_NUMBER                 
       ,ROUTE_NAME                   
       ,DISPATCH_DATE                
       ,CREATION_DATE                
       ,CREATED_BY                   
       ,LAST_UPDATE_DATE             
       ,LAST_UPDATED_BY              
       ,SHIP_CONFIRM_STATUS          
       ,SHIP_CONFIRM_EXCEPTION       
       ,ORG_ID         
       ,DMS2_ITEM_ID	
       ,CONCURRENT_REQUEST_ID      
       ,sequence_id 
       )
       VALUES
       (rec_order_ship.ORDER_NUMBER   
       ,l_header_id   
       ,rec_order_ship.LINE_NUMBER        
       ,rec_order_ship.DELIVERY_ID       
       ,rec_order_ship.BRANCH           
       ,rec_order_ship.ITEM_NUMBER   
       ,ln_ordered_qty
       ,ln_delivered_qty
       ,null     
       ,lv_exception    
       ,rec_order_ship.SIGNED_BY          
       ,rec_order_ship.Comments        
       ,rec_order_ship.DRIVER_NAME        
       ,rec_order_ship.ROUTE_ID           
       ,rec_order_ship.ROUTE_NAME         
       ,TRUNC(SYSDATE)
       ,SYSDATE
       ,fnd_global.user_id
       ,SYSDATE
       ,fnd_global.user_id
       ,'NO'
       ,NULL
       ,fnd_global.org_id
	   ,rec_order_ship.ITEM_ID
	   ,rec_order_ship.REQUEST_ID
	   ,p_sequence_id 
        );
	  
        UPDATE XXWC.XXWC_OM_DMS2_SHIP_CONFIRM_TBL DMS2_SC
           SET inventory_item_id = (SELECT inventory_item_id
                                      FROM apps.mtl_system_items_b msi
                                     WHERE msi.SEGMENT1        = DMS2_SC.ITEM_NUMBER
                                       AND msi.organization_id = 222 )
              ,last_update_date         = SYSDATE                         
         WHERE order_number = rec_order_ship.order_number;
     
       --Submitting the ship confirm program for INTERNAL ORDERS..
       IF lv_order_type='INTERNAL ORDER' THEN		
          lv_user_name := 'XXWC_INT_SALESFULFILLMENT';  --RP
   
          BEGIN
             SELECT responsibility_id, application_id
               INTO ln_responsibility_id, ln_resp_application_id
               FROM apps.fnd_responsibility_vl
              WHERE responsibility_name = 'HDS Order Mgmt Pricing Super - WC'
                AND SYSDATE BETWEEN start_date 
                AND NVL(end_date, trunc(SYSDATE) + 1);
          EXCEPTION
          WHEN NO_DATA_FOUND THEN
             l_err_msg := 'Responsibility ' || lv_resp_name ||' not defined in Oracle';
             RAISE program_error;
          END;
  
          -- Deriving Ids from initialization variables
          BEGIN
             SELECT user_id
               INTO ln_user_id
               FROM apps.fnd_user
              WHERE user_name = upper(lv_user_name)
                AND SYSDATE BETWEEN start_date 
                AND NVL(end_date, trunc(SYSDATE) + 1);
          EXCEPTION
          WHEN NO_DATA_FOUND THEN
             l_err_msg := 'UserName ' || lv_user_name || ' not defined in Oracle';
             RAISE program_error;
          END;

          --Initializing the Apps Environment.
          fnd_global.apps_initialize(ln_user_id
                                    ,ln_responsibility_id
                                    ,ln_resp_application_id);

          l_sec := 'Before lv_order_type: '|| lv_order_type;
          write_log (  -1
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
 								
           --Ship confirm the sales order
          l_sec := 'Before submitting concurrent program XXWC_DMS2_SHIP_CONFIRM';
          write_log (  -1
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
   
          --Calling the xxwc OM dms2 Ship Confirm concurrent program to do the ship confirm
          ln_sc_conc_request_id := fnd_request.submit_request(application => 'XXWC'
                                                             ,program     => 'XXWC_DMS2_SHIP_CONFIRM'
                                                             ,description => NULL
                                                             ,start_time  => SYSDATE
                                                             ,sub_request => FALSE
                                                             ,argument1   => lv_user_name
                                                             ,argument2   => lv_resp_name
                                                             ,argument3   => ln_order_type_id
                                                             ,argument4   => ''
                                                             ,argument5   => ln_order_number
                                                             ,argument6   => 'N'
                                                            );
          COMMIT;
    
          UPDATE XXWC.XXWC_OM_DMS2_SHIP_CONFIRM_TBL 
             SET ship_confirm_request_id        = ln_sc_conc_request_id
                ,last_update_date               = SYSDATE 
           WHERE order_number                   = ln_order_number
             AND NVL(ship_confirm_status, 'NO') = 'NO';
     
          COMMIT;
   
          l_sec := 'After submitting concurrent program conc request id: '|| ln_sc_conc_request_id; 
          write_log (  -1
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
      
        END IF; -- End submitting the XXWC_DMS2_SHIP_CONFIRM concurrent program
        
	EXCEPTION
	WHEN others THEN
	
	  lv_error_msg:='Inside Exception '||SQLERRM;
	
    END;
    
  ELSIF rec_order_ship.CMD='35' AND rec_order_ship.STYPE='3' THEN
  
    BEGIN
	
      SELECT P.LCODE
        INTO l_lcode
        FROM  XXWC.XXWC_DMS2_LOG_TBL T
             LEFT JOIN XMLTABLE('//MLW'
                      PASSING T.REQUEST_PAYLOAD_XMLTYPE
                      COLUMNS LCODE    VARCHAR2(100)  PATH '//FieldData/@LCode'
                      ) P
         ON (1=1)
         WHERE 1=1
         AND T.SEQUENCE_ID = p_sequence_id;
			 
    EXCEPTION
	WHEN others THEN
	
	  l_lcode  :=NULL;
		
	END;
  
    l_sec := 'Inside rec_order_ship order number : '|| rec_order_ship.order_number || 'CMD : '||rec_order_ship.CMD|| 'STYPE : '|| rec_order_ship.STYPE;
    write_log (  -1
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
      
	IF l_lcode=1 THEN
	   lv_image      :=NULL;
	   l_sec := 'Inside rec_order_ship order number : '|| rec_order_ship.order_number || 'l_lcode: '||l_lcode;
       write_log (  -1
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
      
      --this is the case when signature is null and picture taken for delivery
      IF rec_order_ship.pod_method = 'CUSTOMER NOT AVAILABLE' THEN
         l_sec := 'Inside CUSTOMER NOT AVAILABLE rec_order_ship order number : '|| rec_order_ship.order_number || 'Inside lv_blob';
         write_log (  -1
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
      
         BEGIN
	        SELECT  S.IMAGE, S1.pod_method_image_type
	          INTO  lv_image, l_pod_method_image_type
            FROM XXWC.XXWC_DMS2_LOG_TBL T
            LEFT JOIN XMLTABLE('//MLW'
                        PASSING T.REQUEST_PAYLOAD_XMLTYPE
                        COLUMNS ExpList   XMLTYPE           PATH  '//MLW/FieldData/Field/Child'
                        ) Q
            ON (1=1)
            LEFT JOIN XMLTABLE('/Child[@Name="Photo of Delivered Product"]'
                        PASSING  Q.ExpList
                        COLUMNS IMAGE     CLOB  PATH '/Child/@Value'
                        ) S   
            ON (1=1)
            LEFT JOIN XMLTABLE('/Child[@Name="Photo of Delivered Product"]'
                        PASSING  Q.ExpList
                        COLUMNS pod_method_image_type     VARCHAR2(150)   PATH '/Child/@Name'
                       ) S1  
            ON (1=1)
            WHERE 1=1
              AND T.sequence_id = p_sequence_id;
         EXCEPTION
	     WHEN others THEN
	        lv_image:=NULL;
	     END;  
	     
	     --Converting Hexabinary to Binary image
         lv_blob:= hex_to_blob(lv_image);
      ELSE 
         l_sec := 'Inside CUSTOMER AVAILABLE rec_order_ship order number : '|| rec_order_ship.order_number || 'Inside lv_blob';
         write_log (  -1
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
      
         BEGIN
      	   SELECT  NVL(S1.Signature_IMAGE, S2.Photo_IMAGE),
              NVL(S3.Signature_image_type, S4.Photo_image_type) sign_photo
	        INTO  lv_image, l_pod_method_image_type
            FROM XXWC.XXWC_DMS2_LOG_TBL T
            LEFT JOIN XMLTABLE('//MLW'
                        PASSING T.REQUEST_PAYLOAD_XMLTYPE
                        COLUMNS ExpList   XMLTYPE           PATH  '//MLW/FieldData/Field/Child'
                        ) Q
            ON (1=1)
            LEFT JOIN XMLTABLE('/Child[@Name="Signature"]'
                        PASSING  Q.ExpList
                        COLUMNS Signature_IMAGE     CLOB  PATH '/Child/@Value'
                        ) S1   
            ON (1=1)
            LEFT JOIN XMLTABLE('/Child[@Name="Photo"]'
                         PASSING  Q.ExpList
                         COLUMNS Photo_IMAGE     CLOB  PATH '/Child/@Value'
                        ) S2   
            ON (1=1)
            LEFT JOIN XMLTABLE('/Child[@Name="Signature"]'
                        PASSING  Q.ExpList
                        COLUMNS Signature_image_type     VARCHAR2(150)   PATH '/Child/@Name'
                        ) S3  
            ON (1=1)
            LEFT JOIN XMLTABLE('/Child[@Name="Photo"]'
                        PASSING  Q.ExpList
                        COLUMNS Photo_image_type     VARCHAR2(150)   PATH '/Child/@Name'
                        ) S4
            ON (1=1)
            WHERE 1=1
              AND T.sequence_id = p_sequence_id;
         EXCEPTION
	     WHEN others THEN
	        lv_image:=NULL;
	     END;  
	     
	     --Converting Hexabinary to Binary image
         lv_blob:= hex_to_blob(lv_image);
      END IF;    
	ELSIF l_lcode=3 THEN
		
	  BEGIN
	  
	    SELECT  NVL(S.IMAGE,R.IMAGE) IMAGE
	    INTO   lv_image
        FROM XXWC.XXWC_DMS2_LOG_TBL T
        LEFT JOIN XMLTABLE('//MLW'
                        PASSING T.REQUEST_PAYLOAD_XMLTYPE
                        COLUMNS ExpList   XMLTYPE           PATH  '//MLW/FieldData/Field/Child'
                        ) Q
        ON (1=1)
        LEFT JOIN XMLTABLE('/Child[@Name="Damaged Item"]'
                        PASSING  Q.ExpList
                        COLUMNS IMAGE     CLOB  PATH '/Child/@Value'
                        ) S   
        ON (1=1)
        LEFT JOIN XMLTABLE('/Child[@Name="Wrong Item"]'
                    PASSING  Q.ExpList
                    COLUMNS IMAGE     CLOB  PATH '/Child/@Value'
                    ) R  
        ON (1=1)
        WHERE 1=1
        AND T.sequence_id = p_sequence_id;
	    
      EXCEPTION
	  WHEN others THEN
	  
	    lv_image:=NULL;
	    
	  END;
	  
      --Converting Hexabinary to Binary image
      lv_blob:= hex_to_blob(lv_image);
	
    ELSE
	
	  NULL;
	
	END IF;
	
   
    BEGIN
  
      INSERT INTO xxwc.xxwc_om_dms2_sign_capture_tbl
	  (ORDER_NUMBER     
	  ,HEADER_ID           
      ,DELIVERY_ID                 
      ,CONCURRENT_REQUEST_ID       
      ,SIGNATURE_NAME              
      ,SIGNATURE_IMAGE_BLOB        
      ,CREATION_DATE               
      ,CREATED_BY                  
      ,LAST_UPDATE_DATE            
      ,LAST_UPDATED_BY             
      ,SHIP_CONFIRM_STATUS         
      ,SHIP_CONFIRM_EXCEPTION      
      ,ORG_ID
      ,LCODE
      ,SEQUENCE_ID
      ,POD_METHOD
      ,pod_method_image_type
      )
	   VALUES 
       (rec_order_ship.ORDER_NUMBER
       ,l_header_id
	   ,rec_order_ship.DELIVERY_ID
	   ,rec_order_ship.REQUEST_ID
	   ,rec_order_ship.SIGNED_BY
	   ,NVL(lv_blob,lv_blob)
	   ,SYSDATE
	   ,fnd_global.user_id
	   ,SYSDATE
	   ,fnd_global.user_id
	   ,'NO'
	   ,NULL
	   ,fnd_global.org_id
	   ,l_lcode
	   ,p_sequence_id
	   ,rec_order_ship.POD_METHOD	
	   ,l_pod_method_image_type
	   );
	   
	EXCEPTION
	WHEN others THEN
	
	  lv_error_msg:='Inside Exception '||SQLERRM;
	  l_sec := 'Inside rec_order_ship others: '|| lv_error_msg;
      
      write_log (  -1
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
	  
    END;
	
  ELSIF rec_order_ship.CMD='9' AND rec_order_ship.STYPE='3' THEN
  
    BEGIN
  
      SELECT Q1.EXP_NAME
      ,      Q1.EXP_REASON
	  INTO   lv_exception_name      
      ,      lv_exception_reason    
      FROM   XXWC.XXWC_DMS2_LOG_TBL T
      LEFT JOIN XMLTABLE('//MLW'
                          PASSING T.REQUEST_PAYLOAD_XMLTYPE
                          COLUMNS ExpList   XMLTYPE  PATH  '//MLW/FieldData/Field'
                          ) Q
      ON (1=1)
      LEFT JOIN XMLTABLE('//Field[@Name="Missed Reason"]'
                          PASSING  Q.EXPLIST
                          COLUMNS EXP_NAME VARCHAR2(50) PATH '@Name',    
                          EXP_REASON VARCHAR2(50) PATH '@Value'
                          ) Q1   
      ON (1=1)
      WHERE 1=1
      AND T.sequence_id = p_sequence_id;
	
	EXCEPTION
	WHEN others THEN
	
	  lv_exception_name   :=NULL;
	  lv_exception_reason :=NULL;
	
	END;
	
	BEGIN
	
     l_sec := 'Inside rec_order_ship order number: '|| rec_order_ship.order_number || 'CMD : '||rec_order_ship.CMD|| 'STYPE : '|| rec_order_ship.STYPE;
     write_log (  -1
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
	 
     INSERT INTO XXWC.XXWC_OM_DMS2_SHIP_CONFIRM_TBL
       (ORDER_NUMBER    
       ,HEADER_ID             
       ,LINE_NUMBER                  
       ,DELIVERY_ID                  
       ,BRANCH                       
       ,ITEM_NUMBER   
       ,ORDERED_QUANTITY        
       ,DELIVERED_QTY                
       ,STOP_EXCEPTION               
       ,LINE_EXCEPTION               
       ,SIGNED_BY                    
       ,NOTES                        
       ,DRIVER_NAME                  
       ,ROUTE_NUMBER                 
       ,ROUTE_NAME                   
       ,DISPATCH_DATE                
       ,CREATION_DATE                
       ,CREATED_BY                   
       ,LAST_UPDATE_DATE             
       ,LAST_UPDATED_BY              
       ,SHIP_CONFIRM_STATUS          
       ,SHIP_CONFIRM_EXCEPTION       
       ,ORG_ID         
       ,DMS2_ITEM_ID	
       ,SEQUENCE_ID
       )
       VALUES
       (rec_order_ship.ORDER_NUMBER   
       ,l_header_id   
       ,rec_order_ship.LINE_NUMBER        
       ,rec_order_ship.DELIVERY_ID       
       ,rec_order_ship.BRANCH           
       ,rec_order_ship.ITEM_NUMBER   
       ,ln_ordered_qty
       ,ln_delivered_qty
       ,lv_exception_reason     
       ,lv_exception    
       ,rec_order_ship.SIGNED_BY          
       ,rec_order_ship.Comments
       ,rec_order_ship.DRIVER_NAME        
       ,rec_order_ship.ROUTE_ID           
       ,rec_order_ship.ROUTE_NAME         
       ,TRUNC(SYSDATE)
       ,SYSDATE
       ,fnd_global.user_id
       ,SYSDATE
       ,fnd_global.user_id
       ,'NO'
       ,NULL
       ,fnd_global.org_id
	   ,rec_order_ship.ITEM_ID
       ,p_sequence_id
     );
	  
      UPDATE XXWC.XXWC_OM_DMS2_SHIP_CONFIRM_TBL DMS2_SC
         SET inventory_item_id = (SELECT inventory_item_id
                                    FROM apps.mtl_system_items_b msi
                                   WHERE msi.SEGMENT1        = DMS2_SC.ITEM_NUMBER
                                     AND msi.organization_id = 222 )
            ,last_update_date         = SYSDATE                            
       WHERE order_number                   = ln_order_number
         AND NVL(ship_confirm_status, 'NO') = 'NO';                                 
     
      --Submitting the ship confirm program for INTERNAL ORDERS..
       IF lv_order_type='INTERNAL ORDER' THEN		
          lv_user_name := 'XXWC_INT_SALESFULFILLMENT';  
   
          BEGIN
             SELECT responsibility_id, application_id
               INTO ln_responsibility_id, ln_resp_application_id
               FROM apps.fnd_responsibility_vl
              WHERE responsibility_name = 'HDS Order Mgmt Pricing Super - WC'
                AND SYSDATE BETWEEN start_date 
                AND NVL(end_date, trunc(SYSDATE) + 1);
          EXCEPTION
          WHEN no_data_found THEN
             l_err_msg := 'Responsibility ' || lv_resp_name ||' not defined in Oracle';
             RAISE program_error;
          END;
  
          -- Deriving Ids from initialization variables
          BEGIN
             SELECT user_id
               INTO ln_user_id
               FROM apps.fnd_user
              WHERE user_name = upper(lv_user_name)
                AND SYSDATE BETWEEN start_date 
                AND NVL(end_date, trunc(SYSDATE) + 1);
          EXCEPTION
          WHEN no_data_found THEN
             l_err_msg := 'UserName ' || lv_user_name || ' not defined in Oracle';
             RAISE program_error;
          END;

          --Initializing the Apps Environment.
          fnd_global.apps_initialize(ln_user_id
                                    ,ln_responsibility_id
                                    ,ln_resp_application_id);

          l_sec := 'Before lv_order_type: '|| lv_order_type;
          write_log (  -1
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
 								
          l_sec := 'Before submitting concurrent program XXWC_DMS2_SHIP_CONFIRM';
          write_log (  -1
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
   
          --Calling the DES Ship Confirm concurrent program to do the ship confirm--For internal orders
          ln_sc_conc_request_id := fnd_request.submit_request(application => 'XXWC'
                                                             ,program     => 'XXWC_DMS2_SHIP_CONFIRM'
                                                             ,description => NULL
                                                             ,start_time  => SYSDATE
                                                             ,sub_request => FALSE
                                                             ,argument1   => lv_user_name
                                                             ,argument2   => lv_resp_name
                                                             ,argument3   => ln_order_type_id
                                                             ,argument4   => ''
                                                             ,argument5   => ln_order_number
                                                             ,argument6   => 'N'
                                                            );
          COMMIT;
    
          UPDATE XXWC.XXWC_OM_DMS2_SHIP_CONFIRM_TBL 
             SET ship_confirm_request_id        = ln_sc_conc_request_id
                ,last_update_date         = SYSDATE 
           WHERE order_number                   = ln_order_number
             AND NVL(ship_confirm_status, 'NO') = 'NO';
     
          COMMIT;
   
          l_sec := 'After submitting concurrent program conc request id: '|| ln_sc_conc_request_id; 
          write_log (  -1
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
      
        END IF; -- End submitting the XXWC_DMS2_SHIP_CONFIRM concurrent program
	EXCEPTION
	WHEN others THEN
	  lv_error_msg:='Inside Exception '||SQLERRM;
    END;
  ELSE
    NULL;
  END IF;
  
 END LOOP; 
 
 --Delete duplicate rows from signature capture table  
 DELETE FROM xxwc.xxwc_om_dms2_sign_capture_tbl DMS2_SIGN
  WHERE rowid NOT IN ( SELECT MAX(ROWID) 
                         FROM xxwc.xxwc_om_dms2_sign_capture_tbl
                        WHERE sequence_id = p_sequence_id
                        GROUP BY ORDER_NUMBER, DELIVERY_ID )
   AND dms2_sign.sequence_id = p_sequence_id;
 
 IF lv_error_msg IS NOT NULL THEN
 
   l_sec := 'Inside others: '|| lv_error_msg;
   write_log (  -1
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
   RAISE le_exception;
 
 END IF;
 
 COMMIT;
 
  --Assigning OUT variables...
  p_order_type_id:=ln_order_type_id;
  p_order_number :=ln_order_number;
  p_delivery_id  :=ln_delivery_id;
  p_cmd          :=ln_cmd;
  p_stype        :=ln_stype;
  p_route_id     :=lv_route_id;
  p_stop_id      :=ln_stop_id; 
  p_lcode        :=l_lcode;
  
  COMMIT;
EXCEPTION
WHEN le_exception THEN

  --Assigning OUT variables...
  p_order_type_id:=ln_order_type_id;
  p_order_number :=ln_order_number;
  p_delivery_id  :=ln_delivery_id;
  p_cmd          :=ln_cmd;
  p_stype        :=ln_stype;
  p_route_id     :=lv_route_id;
  p_stop_id      :=ln_stop_id;

  p_err_msg      :=lv_error_msg;

   l_err_msg := 'Error_Stack...' || dbms_utility.format_error_stack() ||
                ' Error_Backtrace...' ||
                dbms_utility.format_error_backtrace();
   fnd_file.put_line(fnd_file.log, lv_error_msg);
   dbms_output.put_line(lv_error_msg);

WHEN OTHERS THEN

   l_err_msg := 'Error_Stack...' || dbms_utility.format_error_stack() ||
                ' Error_Backtrace...' ||
                dbms_utility.format_error_backtrace();
   fnd_file.put_line(fnd_file.log, l_err_msg);
   dbms_output.put_line(l_err_msg);
   -- Calling ERROR API
   xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                               l_procedure
                                       ,p_calling           => l_sec
                                       ,p_request_id        => fnd_global.conc_request_id
                                       ,p_ora_error_msg     => substr(l_err_msg
                                                                     ,1
                                                                     ,2000)
                                       ,p_error_desc        => substr(l_err_msg
                                                                     ,1
                                                                     ,240)
                                       ,p_distribution_list => g_distro_list
                                       ,p_module            => 'ONT'); 
								  
END insert_shipment_data;

--Added for Hexadecimal to Binary Conversion of image data...
FUNCTION hex_to_blob (p_hex_data CLOB) RETURN BLOB IS

lv_blob    BLOB                := NULL;
lv_string  VARCHAR2(4000 CHAR) := NULL;
lv_length  NUMBER              := 4000;
  
BEGIN

  IF p_hex_data IS NOT NULL THEN
  
    BEGIN
   
      dbms_lob.createtemporary(lv_blob,FALSE);

      FOR i IN 0 .. LENGTH(p_hex_data)/4000 LOOP
	  
        dbms_lob.read(p_hex_data, lv_length, i * 4000 + 1, lv_string);
        dbms_lob.append(lv_blob, to_blob(hextoraw(lv_string)));
	  
      END LOOP;
	  
	EXCEPTION
    WHEN NO_DATA_FOUND THEN
	
      NULL;
			 
    END;
		 
  END IF;

  RETURN lv_blob;
  
END hex_to_blob;

PROCEDURE Generate_and_attach_POD(p_errbuf              OUT VARCHAR2
                                 ,p_retcode             OUT VARCHAR2
                                 ,p_user_name           IN VARCHAR2
                                 ,p_responsibility_name IN VARCHAR2
                                 ,p_run_zone            IN VARCHAR2
								 ,p_order_type_id       IN VARCHAR2
								 ,p_order_number        IN VARCHAR2
								 ,p_batch_size          IN NUMBER
								 ,p_debug_flag          IN VARCHAR2 
								 ) IS

    l_user_id             fnd_user.user_id%TYPE;
    l_org_id              NUMBER := mo_global.get_current_org_id;
    l_get_attach_req_id   NUMBER;
    l_responsibility_id   NUMBER;
    l_resp_application_id NUMBER;
    l_err_msg             CLOB;
    l_sec                 VARCHAR2(2000) DEFAULT 'START';
    l_procedure           VARCHAR2(50) := 'GENERATE_AND_ATTACH_POD';
    l_return_status       VARCHAR2(1);
    l_msg_data            VARCHAR2(2000);
    l_last_delta_date     DATE;
    l_hold_count          NUMBER;
    l_from_org_id         NUMBER;
    l_pick_rule           VARCHAR2(50);
    l_order_type          VARCHAR2(100);
    l_ship_from           VARCHAR2(100);
    l_exists              VARCHAR2(1) DEFAULT 'F';
    l_exception_exists    VARCHAR2(1) DEFAULT 'F';
    l_ship                VARCHAR2(1) DEFAULT 'N';
    l_shipping_method     NUMBER;
    l_qty                 NUMBER;
    l_count_status        NUMBER;
    l_date                DATE;  

    l_sender      VARCHAR2(100);
    l_sid         VARCHAR2(8);
    l_body        VARCHAR2(32767);
    l_body_header VARCHAR2(32767);
    l_body_detail VARCHAR2(32767);
    l_body_footer VARCHAR2(32767);
    
    l_log_seq     NUMBER := '777';

  BEGIN
    l_sec := 'Start of Procedure : MAIN';
        
    IF NVL(p_debug_flag, 'N') = 'Y' THEN
       debug_on;
    ELSE
       debug_off;
    END IF;
        
     fnd_file.put_line(fnd_file.log, '*** 1 *** run time: '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
     fnd_file.put_line(fnd_file.log, 'p_run_zone : '||p_run_zone);

     IF l_org_id IS NULL OR l_org_id = -1 OR l_org_id = -99 THEN
        l_org_id := fnd_profile.value('org_id');
     END IF;

     l_sec := 'Set policy context for org_id';
     
     -- Mandatory initialization for R12
     mo_global.set_policy_context('S', l_org_id);
     mo_global.init('ONT');

     l_get_attach_req_id := fnd_profile.value('conc_request_id');

     -- Deriving Ids from initalization variables
     BEGIN
        SELECT user_id
          INTO l_user_id
          FROM apps.fnd_user
         WHERE user_name = upper(p_user_name)
           AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
     EXCEPTION
     WHEN no_data_found THEN
        l_err_msg := 'UserName ' || p_user_name || ' not defined in Oracle';
        RAISE program_error;
     END;

     BEGIN
        SELECT responsibility_id, application_id
          INTO l_responsibility_id, l_resp_application_id
          FROM apps.fnd_responsibility_vl
         WHERE responsibility_name = p_responsibility_name
           AND SYSDATE BETWEEN start_date AND
               nvl(end_date, trunc(SYSDATE) + 1);
     EXCEPTION
     WHEN no_data_found THEN
        l_err_msg := 'Responsibility ' || p_responsibility_name ||
                     ' not defined in Oracle';
        RAISE program_error;
     END;

    fnd_file.put_line(fnd_file.log, 'Responsibility: '||p_responsibility_name);
    fnd_file.put_line(fnd_file.log, 'Run zone: '||p_run_zone);
    fnd_file.put_line(fnd_file.log, 'Run for date: '||l_date);
    
    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    l_sec := 'Apps Initialize';

    fnd_global.apps_initialize(l_user_id
                              ,l_responsibility_id
                              ,l_resp_application_id);

    l_sec := 'Loop for updating delivery_id';
    
    --------------------------------------------------------------------------
    -- Inserting into xxwc_om_dms2_sign_capture_gtt_tbl
    --------------------------------------------------------------------------
    
    UPDATE xxwc.xxwc_om_dms2_sign_capture_tbl
    SET    ship_Confirm_status='NO'
          ,last_update_date         = SYSDATE 
    WHERE 1=1 
    AND    NVL(p_order_number, order_number) = order_number
    AND    ship_confirm_status IS NULL;
    
    COMMIT;

    --------------------------------------------------------------------------
    -- Inserting into xxwc_om_dms2_sign_capture_gtt_tbl
    --------------------------------------------------------------------------
    l_sec := 'Inserting into XXWC_OM_DMS2_SHIP_CONF_GTT_TBL';
    /**INSERT INTO xxwc.xxwc_om_dms2_sign_cap_gtt_tbl
       SELECT * FROM xxwc.xxwc_om_dms2_sign_capture_tbl
        WHERE NVL(ship_confirm_status, 'NO') = 'NO'
          AND order_number = NVL(p_order_number, order_number)
          AND lcode      =1 
          AND pod_method_image_type IN ('Signature', 'Photo of Delivered Product');**/

    INSERT INTO xxwc.xxwc_om_dms2_sign_cap_gtt_tbl
       SELECT dms2_sc.* , oh.order_type_id
         FROM xxwc.xxwc_om_dms2_sign_capture_tbl dms2_sc
             ,oe_order_headers                   oh
        WHERE 1 = 1
          AND dms2_sc.order_number             = oh.order_number
          AND dms2_sc.order_number             = NVL( p_order_number, dms2_sc.order_number)
          AND oh.order_type_id                 = NVL( p_order_type_id, oh.order_type_id) 
          AND NVL(dms2_sc.ship_confirm_status, 'X') <> 'COMPLETED'
          AND dms2_sc.lcode                    = 1 -- Attach only POD's
          AND dms2_sc.pod_method_image_type   IN ('Signature', 'Photo of Delivered Product')
          AND EXISTS (SELECT /*+ RESULT_CACHE */  flv.lookup_code
                        FROM fnd_lookup_values flv
                       WHERE 1 = 1
                         AND flv.lookup_type           = 'XXWC_DMS_BRANCH_LIST'
                         AND flv.enabled_flag          = 'Y'
                         AND to_number(flv.meaning)    = oh.ship_from_org_id
                         AND (p_run_zone IS NULL OR flv.tag = p_run_zone)
                         AND TRUNC(SYSDATE) BETWEEN flv.start_date_active AND nvl(flv.end_date_active, SYSDATE + 1)
                     )--Rev 2.0 <Start
           AND EXISTS (SELECT 1                                                         
                        FROM XXWC.XXWC_OM_DMS2_SHIP_CONFIRM_TBL ship_stg   
                       WHERE 1 = 1
                         AND ship_stg.order_number = dms2_sc.order_number 
                         AND ship_stg.SIGNED_BY_FILE_UPDATE_FLAG IS NOT NULL
                         AND ROWNUM=1  
                      UNION    
                      SELECT 1
                        FROM XXWC.XXWC_OM_DMS2_SHIP_CONFIRM_TBL ship_stg  
                       WHERE 1 = 1
                         AND ship_stg.order_number = dms2_sc.order_number 
                         AND ship_stg.SIGNED_BY_FILE_UPDATE_FLAG IS NULL  
                         AND ROWNUM=1  
                         AND NOT EXISTS (SELECT 1
                                           FROM apps.FND_CONCURRENT_REQUESTS fcr
                                          WHERE fcr.request_id = ship_stg.concurrent_request_id 
                                        )  --Rev 2.0 > End   
                    );
       
    fnd_file.put_line(fnd_file.log, 'No of record inserted into xxwc_om_dms2_sign_cap_gtt_tbl: '||SQL%ROWCOUNT);
      
    fnd_file.put_line(fnd_file.log, '*** 2 *** run time: '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
    
    fnd_file.put_line(fnd_file.log, '*** 3 *** run time: '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

    l_sec := 'Loop for pod process';
    FOR cur_pod IN (SELECT * FROM xxwc.xxwc_om_dms2_sign_cap_gtt_tbl)
    LOOP
      l_sec := 'before calling resubmit_requests';   
      write_log (  cur_pod.header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
                 
      XXWC_OM_DMS2_IB_PKG.resubmit_requests(  p_header_id              => cur_pod.header_id
                                             ,p_delivery_id            => cur_pod.delivery_id
                                             ,p_order_type_id          => cur_pod.order_type_id
                                             ,p_concurrent_request_id  => cur_pod.concurrent_request_id
                                             ,p_batch_size             => p_batch_size
                                             );
      
      UPDATE xxwc.xxwc_om_dms2_sign_cap_gtt_tbl
         SET ship_confirm_status     = 'RESUBMIT-COMPLETED'
          ,last_update_date          = SYSDATE 
          ,gen_attch_pod_con_req_id  = l_get_attach_req_id
       WHERE header_id               =  cur_pod.header_id
	     AND NVL(delivery_id, -1)    = NVL(cur_pod.delivery_id, -1) --Rev#3.0
         AND pod_method_image_type IN ('Signature', 'Photo of Delivered Product')
         AND lcode = 1
         AND ship_confirm_status != 'ERROR';                                             
      COMMIT;
      
    END LOOP;

    l_sec := 'before wait for Bursting_program l_request_ids_tbl.count '||g_request_ids_tbl.count ;    
      
    write_log (  999999999
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
    
    IF g_request_ids_tbl.count >0 THEN
       --if for the running requests
       FOR i IN g_request_ids_tbl.FIRST .. g_request_ids_tbl.LAST
	   LOOP
	      wait_for_running_request ( p_request_id   => g_request_ids_tbl(i)
	                                ,p_program_type => 'CFD' );
	   END LOOP;
   	   g_request_ids_tbl.delete;
   	END IF;  
   	 
    fnd_file.put_line(fnd_file.log, '*** 5 *** run time: '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
    
    l_sec := 'Loop for image file creation on the server';
    FOR cur_pod IN (SELECT * 
                      FROM xxwc.xxwc_om_dms2_sign_cap_gtt_tbl
                     WHERE ship_confirm_status != 'ERROR' 
                    )
    LOOP
      l_sec := 'before calling create_image_file_on_server';   
      write_log (  cur_pod.header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
                             
      --1. Download the image from stage table and put into file system.
      --   This procedure will create a POD pdf file on the server location /xx_iface/ebsdev/inbound/DESCARTES/pod_images
      IF cur_pod.resubmit_pod_con_req_id IS NOT NULL THEN
         XXWC_OM_DMS2_IB_PKG.create_image_file_on_server (
                                                           p_header_id              => cur_pod.header_id,
                                                           p_concurrent_request_id  => cur_pod.concurrent_request_id,
                                                           p_resub_req_id           => cur_pod.resubmit_pod_con_req_id
                                                         );
      
      ELSE
         XXWC_OM_DMS2_IB_PKG.create_image_file_on_server (
                                                           p_header_id              => cur_pod.header_id,
                                                           p_concurrent_request_id  => cur_pod.concurrent_request_id,
                                                           p_resub_req_id           => NULL
                                                         );
      
     
      END IF;
      
      COMMIT;
         
      UPDATE xxwc.xxwc_om_dms2_sign_cap_gtt_tbl--xxwc.xxwc_om_dms2_sign_capture_tbl 
         SET SHIP_CONFIRM_STATUS     = 'IMAGEFILE-COMPLETED'
          ,last_update_date          = SYSDATE 
          ,gen_attch_pod_con_req_id  = l_get_attach_req_id
       WHERE header_id =  cur_pod.header_id
         AND NVL(delivery_id, -1) = NVL(cur_pod.delivery_id, -1) --Rev#3.0
         AND pod_method_image_type IN ('Signature', 'Photo of Delivered Product')
         AND lcode = 1
         AND ship_confirm_status != 'ERROR';                                              
      COMMIT;
      
    END LOOP;--Loop for image file creation on the server

    fnd_file.put_line(fnd_file.log, '*** 6 *** run time: '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

    l_sec := 'Loop for PDF file creation on the server';
    FOR cur_pod IN (SELECT * 
                      FROM xxwc.xxwc_om_dms2_sign_cap_gtt_tbl
                     WHERE ship_confirm_status != 'ERROR' )
    LOOP
      l_sec := 'before calling submit_Bursting_program';    
      
      write_log (  cur_pod.header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
                                                 
      --2. Submit the bursting program and wait for this to finish. 
      --   The program will create a POD pdf file on the server location /xx_iface/ebsdev/inbound/DESCARTES/pod
      XXWC_OM_DMS2_IB_PKG.submit_Bursting_program (      p_header_id              => cur_pod.header_id
                                                        ,p_delivery_id            => cur_pod.delivery_id --Rev#3.0
                                                        ,p_concurrent_request_id  => NVL( cur_pod.resubmit_pod_con_req_id, cur_pod.concurrent_request_id)
                                                        ,p_application_short_name => 'ONT'
                                                        ,p_batch_size             => p_batch_size
                                                       );  
     
      COMMIT;
         
      UPDATE xxwc.xxwc_om_dms2_sign_cap_gtt_tbl--xxwc.xxwc_om_dms2_sign_capture_tbl 
         SET SHIP_CONFIRM_STATUS     = 'PDFFILE-COMPLETED'
          ,last_update_date          = SYSDATE 
          ,gen_attch_pod_con_req_id  = l_get_attach_req_id
       WHERE header_id =  cur_pod.header_id
         AND NVL(delivery_id, -1)   = NVL(cur_pod.delivery_id, -1) --Rev#3.0 
         AND pod_method_image_type IN ('Signature', 'Photo of Delivered Product')
         AND lcode = 1
         AND ship_confirm_status != 'ERROR';                                             
      COMMIT;
      
    END LOOP;--Loop for image file creation on the server
    
    l_sec := 'before wait for Bursting_program l_request_ids_tbl.count '||g_request_ids_tbl.count ;    
      
    write_log (  999999999
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
    
    IF g_request_ids_tbl.count >0 THEN
       --if for the running requests
       FOR i IN g_request_ids_tbl.FIRST .. g_request_ids_tbl.LAST
	   LOOP
	      wait_for_running_request (  p_request_id => g_request_ids_tbl(i) 
	                                 ,p_program_type => 'XDOBURST'
	                               );
	   END LOOP;
   	   g_request_ids_tbl.delete;
   	END IF;   
			 
    fnd_file.put_line(fnd_file.log, '*** 7 *** run time: '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

    l_sec := 'Loop for PDF file creation on the server';
    FOR cur_pod IN (SELECT * 
                      FROM xxwc.xxwc_om_dms2_sign_cap_gtt_tbl
                     WHERE ship_confirm_status != 'ERROR' )
    LOOP
      l_sec := 'before calling attach_POD_file';   
      write_log (  cur_pod.header_id
                 , NULL
                 , l_sec 
                 , l_log_seq+1);
      
      --3. Attach POD to sales order / purchase order
      XXWC_OM_DMS2_IB_PKG.attach_POD_file (      p_header_id              => cur_pod.header_id
                                                ,p_concurrent_request_id  => NVL( cur_pod.resubmit_pod_con_req_id, cur_pod.concurrent_request_id)
                                                ,p_delivery_id            => cur_pod.delivery_id 
                                                ,p_application_short_name => 'ONT'
                                               );    
                                                 
    
      COMMIT;
         
      UPDATE xxwc.xxwc_om_dms2_sign_cap_gtt_tbl--xxwc.xxwc_om_dms2_sign_capture_tbl 
         SET SHIP_CONFIRM_STATUS     = 'COMPLETED'
          ,last_update_date          = SYSDATE 
          ,gen_attch_pod_con_req_id  = l_get_attach_req_id
       WHERE header_id =  cur_pod.header_id
         AND NVL(delivery_id, -1)   = NVL(cur_pod.delivery_id, -1) --Rev#3.0
         AND pod_method_image_type IN ('Signature', 'Photo of Delivered Product')
         AND lcode = 1
         AND ship_confirm_status != 'ERROR';                                              
      COMMIT;
      
    END LOOP;--Loop for image file creation on the server

    fnd_file.put_line(fnd_file.log, '*** 8 *** run time: '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
    
    --------------------------------------------------------------------------
    -- Update xxwc_om_dms2_sign_capture_tbl from Global Temp Table
    --------------------------------------------------------------------------
    l_sec := 'Update xxwc_om_dms2_sign_capture_tbl from Global Temp Table';
    UPDATE xxwc.xxwc_om_dms2_sign_capture_tbl stg
       SET (stg.ship_confirm_status , stg.ship_confirm_exception , stg.burst_pod_con_req_id
           ,stg.resubmit_pod_con_req_id, stg.gen_attch_pod_con_req_id,  stg.last_update_date , stg.last_updated_by)
                                                                                      = (SELECT  ship_confirm_status
                                                                                              , ship_confirm_exception
                                                                                              , burst_pod_con_req_id
                                                                                              , resubmit_pod_con_req_id
                                                                                              , gen_attch_pod_con_req_id
                                                                                              , last_update_date
                                                                                              , last_updated_by
                                                                                          FROM xxwc.xxwc_om_dms2_sign_cap_gtt_tbl  gtt
                                                                                         WHERE gtt.sequence_id           = stg.sequence_id
                                                                                           AND gtt.lcode                 = stg.lcode
                                                                                           AND gtt.pod_method_image_type = stg.pod_method_image_type
                                                                                           AND NVL(gtt.delivery_id, -1)  = NVL(stg.delivery_id, -1)
                                                                                               --AND stg.route_number = stg.route_number
                                                                                           AND ROWNUM = 1)
      WHERE stg.ship_confirm_status   = 'NO'
        AND stg.lcode                 = 1
        AND stg.pod_method_image_type IN ('Signature', 'Photo of Delivered Product');
    
     fnd_file.put_line(fnd_file.log, 'No of record updated into xxwc_om_dms2_sign_capture_tbl: '||SQL%ROWCOUNT);

     fnd_file.put_line(fnd_file.log, '*** 9 *** run time: '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
    COMMIT;
    
    email_POD_errors ( p_pod_con_req_id => l_get_attach_req_id );

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => l_get_attach_req_id--l_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'ONT');

      p_retcode := 2;
      p_errbuf  := l_err_msg;

    WHEN OTHERS THEN
	
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => l_get_attach_req_id--l_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'ONT');

      p_retcode := 2;
      p_errbuf  := l_err_msg;

  END Generate_and_attach_POD;

END XXWC_OM_DMS2_IB_PKG;
/