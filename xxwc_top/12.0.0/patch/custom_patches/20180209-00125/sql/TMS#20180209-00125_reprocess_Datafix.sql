/***********************************************************************************************************************************************
   NAME:     TMS_20180209-00125_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        02/12/2018  Rakesh Patel     TMS#20180209-00125-Fixes for the descartes pilot errors
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before update');
   
   UPDATE xxwc.XXWC_OM_DMS2_SIGN_CAPTURE_TBL
      SET SHIP_CONFIRM_STATUS = NULL 
	      ,LAST_UPDATED_BY     = -1
	      ,LAST_UPDATE_DATE    = SYSDATE
	 WHERE ORDER_NUMBER IN ( SELECT oh.order_number
                               FROM xxwc.XXWC_OM_DMS2_SIGN_CAPTURE_TBL xx,
                                    apps.oe_order_headers_all oh
                              WHERE 1=1
                                AND oh.header_id=xx.header_id 
                                AND CONCURRENT_REQUEST_ID IS NOT NULL
                                AND TRUNC(xx.creation_date) >= TO_CHAR('07-FEB-2018')
                                AND NOT EXISTS ( SELECT 1
                                                   FROM apps.FND_ATTACHED_DOCUMENTS 
                                                  WHERE pk1_value =  oh.header_id
                                                ) 
	                       );
   
   DBMS_OUTPUT.put_line ('Records Updated -' || SQL%ROWCOUNT);
   COMMIT;
   
  EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
	  ROLLBACK;
END;
/