/***********************************************************************************************************************************************
   NAME:     TMS_20180209-00125_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        02/12/2018  Rakesh Patel     TMS#20180209-00125-Fixes for the descartes pilot errors
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('Before creating');
   
   EXECUTE IMMEDIATE 'CREATE TABLE xxwc.XXWC_OM_DMS2_SIGN_CAP_ARC_TBL AS SELECT * from xxwc.XXWC_OM_DMS2_SIGN_CAPTURE_TBL WHERE 1=1';

   DBMS_OUTPUT.put_line ('Table XXWC_OM_DMS2_SIGN_CAP_ARC_TBL created.');
   
   EXECUTE IMMEDIATE 'CREATE TABLE xxwc.XXWC_OM_DMS2_SHIP_CONF_ARC_TBL AS SELECT * from xxwc.XXWC_OM_DMS2_SHIP_CONFIRM_TBL WHERE 1=1';

   DBMS_OUTPUT.put_line ('Table XXWC_OM_DMS2_SHIP_CONF_ARC_TBL created');
      
  EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to create table ' || SQLERRM);	  
END;
/