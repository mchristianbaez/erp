/*********************************************************************************
Copyright (c) 2017 HD Supply
All Rights Reserved

HEADER:
	FND_CONCURRENT_PROGRAMS_TO_INTERFACE_APEXWC

PROGRAM NAME:
	Concurrent Program Manager - APEX WC

DESCRIPTION:
	TMS# 20170329-00040
	Grant for interface_apexwc to retrieve name of concurrent program

LAST UPDATE DATE: 29-MARCH-2016

HISTORY
=======
VERSION	DATE          	AUTHOR(S)    		    DESCRIPTION
-------	---------------	------------------- -------------------------------------
1.0		  29-MAR-2017	    Christian Baez		  Creation

*********************************************************************************/
GRANT SELECT ON apps.FND_CONCURRENT_PROGRAMS_TL TO INTERFACE_APEXWC;

/
