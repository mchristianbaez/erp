CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_PSLIP_RCPT_EMAIL_PKG
AS
   /********************************************************************************************************************************
      $Header XXWC_OM_PSLIP_RCPT_EMAIL_PKG.PKS $

      PURPOSE:   This package is used to get distinct emails.

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         ----------------------------------------------------------------------------------------
      1.0        15-Apr-2018 P.Vamshidhar            Initial Version -
                                                      TMS# 20180414-00014- Add logic to remove duplicate emails being sent for SOAs and ASNs

   ******************************************************************************************************************************************/
   g_dflt_email   VARCHAR2 (50) := 'HDSOracleDevelopers@hdsupply.com';

   PROCEDURE DISTINCT_EMAIL (p_to_email_add   IN     VARCHAR2,
                             p_cc_email_add   IN     VARCHAR2,
                             x_to_email_add      OUT VARCHAR2,
                             x_cc_email_add      OUT VARCHAR2)
   IS
      lvc_email       VARCHAR2 (32767) := p_to_email_add;
      lvc_email1      VARCHAR2 (32767);
      lvc_email2      VARCHAR2 (32767);
      lvc_ret_email   VARCHAR2 (32767);
      ln_cnt          NUMBER := 1;
      ln_cc_count     NUMBER := 1;

      TYPE typ_email IS TABLE OF VARCHAR2 (32767)
         INDEX BY BINARY_INTEGER;

      obj_email       typ_email;

      TYPE typ_cc_email IS TABLE OF VARCHAR2 (32767)
         INDEX BY BINARY_INTEGER;

      obj_cc_email    typ_cc_email;
      lvc_sec         VARCHAR2 (32767);
   BEGIN
      lvc_sec := 'Procedure Started';

      IF p_to_email_add IS NOT NULL
      THEN
         lvc_email2 := LOWER(p_to_email_add);

         IF INSTR (lvc_email2, ',', 1) > 0
         THEN
            LOOP
               EXIT WHEN ln_cnt = 0;
               lvc_email1 := NULL;
               lvc_email1 :=
                  SUBSTR (lvc_email2, 1, INSTR (lvc_email2, ',', 1) - 1);

               IF obj_email.COUNT > 0
               THEN
                  FOR i IN 1 .. obj_email.COUNT
                  LOOP
                     IF obj_email (i) = lvc_email1
                     THEN
                        lvc_email2 :=
                           SUBSTR (lvc_email2,
                                   INSTR (lvc_email2, ',', 1) + 1);
                        GOTO LAST_STEP;
                     END IF;
                  END LOOP;

                  obj_email (ln_cnt) := lvc_email1;
                  ln_cnt := ln_cnt + 1;
               ELSE
                  obj_email (ln_cnt) := lvc_email1;
                  ln_cnt := ln_cnt + 1;
               END IF;

               lvc_email2 :=
                  SUBSTR (lvc_email2, INSTR (lvc_email2, ',', 1) + 1);

               IF INSTR (lvc_email2, ',', 1) = 0
               THEN
                  IF obj_email.COUNT > 0
                  THEN
                     FOR i IN 1 .. obj_email.COUNT
                     LOOP
                        IF obj_email (i) = lvc_email2
                        THEN
                           ln_cnt := 0;
                           GOTO LAST_STEP1;
                        END IF;
                     END LOOP;

                     obj_email (ln_cnt) := lvc_email2;
                     ln_cnt := 0;
                  ELSE
                     obj_email (ln_cnt) := lvc_email2;
                     ln_cnt := 0;
                     GOTO LAST_STEP1;
                  END IF;
               END IF;

              <<LAST_STEP>>
               NULL;
            END LOOP;
         ELSE
            IF obj_email.COUNT > 0
            THEN
               FOR i IN 1 .. obj_email.COUNT
               LOOP
                  IF obj_email (i) = lvc_email2
                  THEN
                     GOTO LAST_STEP1;
                  END IF;
               END LOOP;

               ln_cnt := obj_email.COUNT;
               ln_cnt := ln_cnt + 1;
               obj_email (ln_cnt) := lvc_email2;
            ELSE
               ln_cnt := 1;
               obj_email (ln_cnt) := lvc_email2;
            END IF;
         END IF;

        <<LAST_STEP1>>
         FOR i IN 1 .. obj_email.COUNT
         LOOP
            IF lvc_ret_email IS NULL
            THEN
               lvc_ret_email := obj_email (i);
            ELSE
               lvc_ret_email := lvc_ret_email || ',' || obj_email (i);
            END IF;
         END LOOP;

         x_to_email_add := lvc_ret_email;
      END IF;

      lvc_sec := 'CC Email Started';

      IF p_cc_email_add IS NOT NULL
      THEN
         lvc_email1 := NULL;
         lvc_email2 := LOWER(p_cc_email_add);
         lvc_ret_email := NULL;
         ln_cnt := obj_email.COUNT;

         IF INSTR (lvc_email2, ',', 1) > 0
         THEN
            LOOP
               EXIT WHEN ln_cnt = 0;
               lvc_email1 := NULL;
               lvc_email1 :=
                  SUBSTR (lvc_email2, 1, INSTR (lvc_email2, ',', 1) - 1);

               IF obj_email.COUNT > 0
               THEN
                  FOR i IN 1 .. obj_email.COUNT
                  LOOP
                     IF obj_email (i) = lvc_email1
                     THEN
                        lvc_email2 :=
                           SUBSTR (lvc_email2,
                                   INSTR (lvc_email2, ',', 1) + 1);
                        GOTO LAST_STEP2;
                     END IF;
                  END LOOP;

                  ln_cnt := ln_cnt + 1;
                  obj_email (ln_cnt) := lvc_email1;
                  obj_cc_email (ln_cc_count) := lvc_email1;
                  ln_cc_count := ln_cc_count + 1;
               ELSE
                  obj_email (ln_cnt) := lvc_email1;
                  ln_cnt := ln_cnt + 1;
                  obj_cc_email (ln_cc_count) := lvc_email1;
                  ln_cc_count := ln_cc_count + 1;
               END IF;

               lvc_email2 :=
                  SUBSTR (lvc_email2, INSTR (lvc_email2, ',', 1) + 1);

               IF INSTR (lvc_email2, ',', 1) = 0
               THEN
                  IF obj_email.COUNT > 0
                  THEN
                     FOR i IN 1 .. obj_email.COUNT
                     LOOP
                        IF obj_email (i) = lvc_email2
                        THEN
                           ln_cnt := 0;
                           GOTO LAST_STEP3;
                        END IF;
                     END LOOP;

                     ln_cnt := ln_cnt + 1;
                     obj_email (ln_cnt) := lvc_email2;
                     obj_cc_email (ln_cc_count) := lvc_email2;
                     ln_cnt := 0;
                  ELSE
                     ln_cnt := ln_cnt + 1;
                     obj_email (ln_cnt) := lvc_email2;
                     obj_cc_email (ln_cc_count) := lvc_email2;
                     ln_cnt := 0;
                     GOTO LAST_STEP3;
                  END IF;
               END IF;

              <<LAST_STEP2>>
               NULL;
            END LOOP;
         ELSE
            IF obj_email.COUNT > 0
            THEN
               FOR i IN 1 .. obj_email.COUNT
               LOOP
                  IF obj_email (i) = lvc_email2
                  THEN
                     GOTO LAST_STEP3;
                  END IF;
               END LOOP;

               ln_cnt := obj_email.COUNT;
               ln_cnt := ln_cnt + 1;
               obj_email (ln_cnt) := lvc_email2;
               obj_cc_email (ln_cc_count) := lvc_email2;
            ELSE
               ln_cnt := 1;
               obj_email (ln_cnt) := lvc_email2;
               obj_cc_email (ln_cc_count) := lvc_email2;
            END IF;
         END IF;

        <<LAST_STEP3>>
         FOR i IN 1 .. obj_cc_email.COUNT
         LOOP
            IF lvc_ret_email IS NULL
            THEN
               lvc_ret_email := obj_cc_email (i);
            ELSE
               lvc_ret_email := lvc_ret_email || ',' || obj_cc_email (i);
            END IF;
         END LOOP;

         x_cc_email_add := lvc_ret_email;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         XXCUS_error_pkg.XXCUS_error_main_api (
            p_called_from         => 'XXWC_OM_PSLIP_RCPT_EMAIL_PKG',
            p_calling             => lvc_sec,
            p_request_id          => -1,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => SUBSTR (
                                          'p_to_email_add:'
                                       || p_to_email_add
                                       || 'p_cc_email_add:'
                                       || p_cc_email_add,
                                       1,
                                       220),
            p_distribution_list   => g_dflt_email,
            p_module              => 'APPS');
   END;
END XXWC_OM_PSLIP_RCPT_EMAIL_PKG;
/