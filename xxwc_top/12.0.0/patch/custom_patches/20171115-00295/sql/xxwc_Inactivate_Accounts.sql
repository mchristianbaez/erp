/*************************************************************************
      Script Name: xxwc_Inactivate_Accounts.sql

      PURPOSE:   To inactivate Accounts

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        16/11/2017  Niraj K Ranjan          TMS#20171115-00295   Credit - one time data script to inactivate account with Prism data only
****************************************************************************/
CREATE TABLE XXWC.XXWC_INACTIVE_ACCOUNTS_TBL
AS
(SELECT *
  FROM (SELECT /*+leading(hca)*/
              cust.account_number,
               cust.account_name,
               cust.creation_date account_creation_date,
               cust.cust_account_id,
               party.party_number,
               party.party_name,
               party.party_id,
               coll.name collector,
               pc.name profile_class,
               (SELECT NVL (SUM (ps.AMOUNT_DUE_REMAINING), 0)
                  FROM AR.ar_payment_schedules_all ps
                 WHERE ps.customer_id = cust.cust_account_id)
                  balance,
               NVL (cust.object_version_number, 1) cust_object_version_number,
               NVL (party.object_version_number, 1)
                  party_object_version_number
          FROM AR.hz_cust_accounts cust,
               AR.hz_parties party,
               AR.hz_customer_profiles prof,
               AR.hz_cust_profile_classes pc,
               AR.ar_collectors coll
         WHERE     cust.party_id = party.party_id
               AND cust.cust_account_id = prof.cust_account_id
               AND prof.profile_class_id = pc.profile_class_id
               AND prof.collector_id = coll.collector_id(+)
               AND NVL (cust.status, 'A') = 'A'
               AND NVL (party.status, 'A') = 'A'
               AND prof.site_use_id IS NULL
               AND pc.name NOT IN ('DEFAULT',
                                   'Intercompany Customers',
                                   'WC Branches',
                                   'Branch Cash Account')
AND NOT EXISTS
                      (SELECT 1
                         FROM apps.hz_party_sites hps,
                              apps.hz_cust_acct_sites hcsa,
                              apps.hz_cust_site_uses hcsu
                        WHERE     hps.party_id = party.party_id
                              AND hps.party_site_id = hcsa.party_site_id
                              AND hcsa.cust_acct_site_id =
                                     hcsu.cust_acct_site_id
                              AND hps.status = 'A'
                              AND hcsu.site_use_code != 'BILL_TO')
               AND cust.creation_date between  (SYSDATE - 365 *5  ) and ( SYSDATE - 365*3 ))
WHERE balance = 0
);
/
ALTER TABLE XXWC.XXWC_INACTIVE_ACCOUNTS_TBL
ADD  (RECORD_STATUS VARCHAR2(50),ERROR_MESSAGE VARCHAR2(2000));
/
/*Start of Account Script*/
set serveroutput on
/
DECLARE
   l_errmsg VARCHAR2(2000);
   l_rec_count NUMBER;
   l_rec_success NUMBER;
   l_rec_error NUMBER;
   l_cust_account_rec HZ_CUST_ACCOUNT_V2PUB.CUST_ACCOUNT_REC_TYPE;
   l_object_version_number NUMBER;
   l_return_status         VARCHAR2 ( 1 ) ;
   l_msg_count             NUMBER;
   l_msg_data              VARCHAR2 ( 2000 ) ;
   CURSOR cr_acct
   IS
      (SELECT ROWID REC_ROWID,
              ACCOUNT_NUMBER,
              ACCOUNT_NAME,
              ACCOUNT_CREATION_DATE,
              CUST_ACCOUNT_ID,
              PARTY_NUMBER,
              PARTY_NAME,
              PARTY_ID,
              COLLECTOR,
              PROFILE_CLASS,
              BALANCE,
              CUST_OBJECT_VERSION_NUMBER,
              PARTY_OBJECT_VERSION_NUMBER,
              RECORD_STATUS,
              ERROR_MESSAGE
       FROM XXWC.XXWC_INACTIVE_ACCOUNTS_TBL 
       WHERE 1=1
	   AND RECORD_STATUS IS NULL OR RECORD_STATUS = 'ERROR');
BEGIN
   dbms_output.put_line('Start of Account Loop');
   l_rec_count := 0;
   l_rec_success := 0;
   l_rec_error := 0;
   FOR rec_acct IN cr_acct
   LOOP
      l_rec_count := l_rec_count + 1;
      BEGIN
	     l_errmsg := NULL;
         --CALL API TO INACTIVATE ACCOUNT
		 mo_global.init ( 'AR' ) ;
         --mo_global.set_org_context ( 204, NULL, 'AR' ) ;
         --fnd_global.set_nls_context ( 'AMERICAN' ) ;
         mo_global.set_policy_context ( 'S', 162) ;
         l_cust_account_rec.cust_account_id := rec_acct.cust_account_id;
		 l_cust_account_rec.status := 'I';
         --l_cust_account_rec.customer_type   := 'R';
         l_cust_account_rec.account_name    := rec_acct.account_name; --FND_API.G_MISS_CHAR;
         l_object_version_number            := rec_acct.cust_object_version_number;
		 
		 hz_cust_account_v2pub.update_cust_account ( 
		        p_init_msg_list          => FND_API.G_FALSE,
                p_cust_account_rec       => l_cust_account_rec,
                p_object_version_number  => l_object_version_number,
                x_return_status          => l_return_status,
                x_msg_count              => l_msg_count,
                x_msg_data               => l_msg_data) ;
         IF l_return_status <> 'S' and l_msg_count > 0 THEN
            FOR i IN 1 .. l_msg_count
            LOOP
               l_errmsg := SUBSTR(l_errmsg||' '||i||'.'|| SUBSTR (FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE), 1, 255),1,2000);
            END LOOP;
			UPDATE XXWC.XXWC_INACTIVE_ACCOUNTS_TBL
			SET RECORD_STATUS = 'ERROR',
			    ERROR_MESSAGE = l_errmsg
            WHERE ROWID = rec_acct.REC_ROWID;
			l_rec_error := l_rec_error + 1;
         ELSE
            UPDATE XXWC.XXWC_INACTIVE_ACCOUNTS_TBL
			SET RECORD_STATUS = 'SUCCESS',
			    ERROR_MESSAGE = l_errmsg
            WHERE ROWID = rec_acct.REC_ROWID;
			l_rec_success := l_rec_success + 1;
         END IF;
	     --END CALL OF API
		 COMMIT;
	  EXCEPTION
	     WHEN OTHERS THEN
		    l_errmsg := SUBSTR(SQLERRM,1,2000);
			ROLLBACK;
			UPDATE XXWC.XXWC_INACTIVE_ACCOUNTS_TBL
			SET RECORD_STATUS = 'ERROR',
			    ERROR_MESSAGE = l_errmsg
            WHERE ROWID = rec_acct.REC_ROWID;
	        COMMIT;
			l_rec_error := l_rec_error + 1;
	  END;
   END LOOP;
   dbms_output.put_line('End of Account Loop');
   dbms_output.put_line('Total account records fetched = '||l_rec_count);
   dbms_output.put_line('Total account records inactivated successfully = '||l_rec_success);
   dbms_output.put_line('Total account records Error out = '||l_rec_error);
EXCEPTION
   WHEN OTHERS THEN
      l_errmsg := SUBSTR(SQLERRM,1,2000);
	  dbms_output.put_line('Unexpected error during account inactivation: '||l_errmsg);
END;
/
/*End of Account Script*/