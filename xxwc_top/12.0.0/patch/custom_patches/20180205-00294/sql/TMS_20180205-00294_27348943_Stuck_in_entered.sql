  set serveroutput on;
 /*************************************************************************
      PURPOSE:  27348943 Stuck in entered

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        06-FEB-2018  Sundaramoorthy     Initial Version - TMS #20180205-00294
         Initial Version - TMS #20180205-00199 --Header_ID =68650488
         Initial Version - TMS #20180130-00024  --Header_ID =68557480
         Initial Version - TMS #20180126-00386  --Header_ID =67560849
         Initial Version - TMS #20180126-00382 --Header_ID =67430622
		 Initial Version - TMS #20180214-00301 --header_ID =68904405
	************************************************************************/ 
 BEGIN
 dbms_output.put_line ('Start Update ');

 UPDATE oe_order_headers_all
  SET  flow_status_code ='CANCELLED'
  , open_flag ='N'
   WHERE header_id in (68880942,68650488,68557480,67560849,67430622,68904405);  
  
  dbms_output.put_line ('Update count '||SQL%ROWCOUNT);
  COMMIT;
  
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
	dbms_output.put_line ('Inside Exception '|| SUBSTR(SQLERRM,1,2000));	
END;
/	
/*************************************************************************
 Initial Version - TMS #20180129-00286 
 ************************************************************************/
BEGIN
 dbms_output.put_line ('Start Update 2nd Script ');

 UPDATE oe_order_headers_all
  SET  flow_status_code ='CLOSED'
  , open_flag ='N'
   WHERE header_id in (42858281,67377682);  
  
  dbms_output.put_line ('Update count '||SQL%ROWCOUNT);
  COMMIT;
  
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
	dbms_output.put_line ('Inside Exception '|| SUBSTR(SQLERRM,1,2000));	
END;
/
/*************************************************************************
 Initial Version - TMS#20180220-00041 
 ************************************************************************/
BEGIN
 dbms_output.put_line ('Start Update 3rd Script ');

 UPDATE oe_order_lines_all
  SET  flow_status_code ='CANCELLED'
  , open_flag ='N'
   WHERE line_id = 115385411;  
  
  dbms_output.put_line ('Update count '||SQL%ROWCOUNT);
  COMMIT;
  
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
	dbms_output.put_line ('Inside Exception '|| SUBSTR(SQLERRM,1,2000));	
END;
/
/*************************************************************************
 Initial Version - TMS#20180215-00144 
 ************************************************************************/
BEGIN
 dbms_output.put_line ('Start Update 4th Script ');

 UPDATE oe_order_lines_all
  SET  flow_status_code ='CANCELLED'
  , open_flag ='N'
   WHERE header_id= 62980676;  
  
  dbms_output.put_line ('Update count '||SQL%ROWCOUNT);
  COMMIT;
  
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
	dbms_output.put_line ('Inside Exception '|| SUBSTR(SQLERRM,1,2000));	
END;
/