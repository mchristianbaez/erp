  /********************************************************************************
  FILE NAME: XXWC_AHH_ITEMS_CONV_STGARC.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: AHH Items Locators Conversion Staging table
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)        DESCRIPTION
  ------- -----------   ---------------- ----------------------------------------------------
  1.0     07/05/2018    Naveen Kalidindi Initial Version.
  *******************************************************************************************/
WHENEVER SQLERROR CONTINUE
-- DROP TABLE
DROP TABLE XXWC.XXWC_AHH_ITEMS_CONV_STGARC;

-- CREATE TABLE
CREATE TABLE XXWC.XXWC_AHH_ITEMS_CONV_STGARC
(
  BATCH_ID                       NUMBER,
  ORGANIZATION_CODE              VARCHAR2(3),
  ICSP_PROD                      VARCHAR2(100),
  ITEM                           VARCHAR2(100),
  ICSP_DESCRIP1                  VARCHAR2(2000),
  DESCRIPTION                    VARCHAR2(2000),
  ICSP_DESCRIP2                  VARCHAR2(2000),
  LONG_DESCRIPTION               VARCHAR2(2000),
  ICSP_STATUSTY                  VARCHAR2(100),
  ITEM_STATUS                    VARCHAR2(100),
  USER_ITEM_TYPE                 VARCHAR2(100),
  ICSW_STATUSTY                  VARCHAR2(100),
  ICSP_LIFOCAT                   VARCHAR2(100),
  SHELF_LIFE_DAYS                VARCHAR2(100),
  ICSW_ARPVENDNO                 VARCHAR2(100),
  VENDOR_OWNER_NUMBER            VARCHAR2(100),
  APSV_NAME                      VARCHAR2(100),
  VENDOR_NAME                    VARCHAR2(100),
  ICSW_VENDPROD                  VARCHAR2(100),
  VENDOR_PART_NUMBER             VARCHAR2(100),
  ICSP_UNITSTOCK                 VARCHAR2(100),
  ORACLE_PRIMARY_UNIT_OF_MEASURE VARCHAR2(100),
  ICSP_LENGTH                    VARCHAR2(100),
  UNIT_LENGTH                    VARCHAR2(100),
  ICSP_HEIGHT                    VARCHAR2(100),
  UNIT_HEIGHT                    VARCHAR2(100),
  ICSP_WIDTH                     VARCHAR2(100),
  UNIT_WIDTH                     VARCHAR2(100),
  ICSP_WEIGHT                    VARCHAR2(100),
  UNIT_WEIGHT                    VARCHAR2(100),
  PART_TYPE                      VARCHAR2(100),
  ICSP_USER1_DOT_CODE            VARCHAR2(100),
  INV_CAT_CLASS                  VARCHAR2(100),
  FINAL_CATMGT_GRP               VARCHAR2(2000),
  FINAL_CATMGT_SUBCATEGORY_DESC  VARCHAR2(2000),
  FINAL_CATCLASS_DESCRIPTION     VARCHAR2(2000),
  ICSW_NONTAXTY                  VARCHAR2(100),
  ICSW_TAXABLETY                 VARCHAR2(100),
  ICSW_TAXGROUP                  VARCHAR2(100),
  ICSW_TAXTYPE                   VARCHAR2(100),
  HAZMAT_DOT_CODE                VARCHAR2(100),
  HAMAT_DESCRIPTION              VARCHAR2(2000),
  HAZMAT_FLAG                    VARCHAR2(100),
  HAZARD_CLASS                   VARCHAR2(100),
  ENTERDT                        DATE,
  CREATE_DATE                    DATE,
  WC_AVP_CODE                    VARCHAR2(100),
  PROCESSED_DATE                 DATE,
  PROCESSED_BY                   VARCHAR2(30),
  VALIDATION_FLAG                VARCHAR2(1),
  STATUS                         VARCHAR2(50),
  ERROR_MESSAGE                  VARCHAR2(2000)
);
-- CREATE/RECREATE INDEXES 
CREATE INDEX XXWC.XXWC_AHH_ITEMS_CONV_STGARC_N1 ON XXWC.XXWC_AHH_ITEMS_CONV_STGARC (BATCH_ID, ITEM);
CREATE INDEX XXWC.XXWC_AHH_ITEMS_CONV_STGARC_N2 ON XXWC.XXWC_AHH_ITEMS_CONV_STGARC (BATCH_ID, ICSP_PROD);
