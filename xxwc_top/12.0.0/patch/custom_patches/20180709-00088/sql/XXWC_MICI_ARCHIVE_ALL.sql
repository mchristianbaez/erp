  /********************************************************************************
  FILE NAME: XXWC_MICI_ARCHIVE_ALL.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: Archive Table
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)        DESCRIPTION
  ------- -----------   ---------------- ----------------------------------------------------
  1.0     07/05/2018    Naveen Kalidindi Initial Version.
  *******************************************************************************************/
WHENEVER SQLERROR CONTINUE
-- DROP TABLE
DROP TABLE XXWC.XXWC_MICI_ARCHIVE_ALL;

-- Create table
CREATE TABLE XXWC.XXWC_MICI_ARCHIVE_ALL
(
  REQUESTID               NUMBER,
  INVENTORY_ITEM_ID       NUMBER,
  CATEGORY_SET_ID         NUMBER,
  CATEGORY_ID             NUMBER,
  LAST_UPDATE_DATE        DATE,
  LAST_UPDATED_BY         NUMBER,
  CREATION_DATE           DATE,
  CREATED_BY              NUMBER,
  LAST_UPDATE_LOGIN       NUMBER,
  REQUEST_ID              NUMBER,
  PROGRAM_APPLICATION_ID  NUMBER,
  PROGRAM_ID              NUMBER,
  PROGRAM_UPDATE_DATE     DATE,
  ORGANIZATION_ID         NUMBER,
  TRANSACTION_ID          NUMBER,
  PROCESS_FLAG            NUMBER,
  CATEGORY_SET_NAME       VARCHAR2(30),
  CATEGORY_NAME           VARCHAR2(81),
  ORGANIZATION_CODE       VARCHAR2(3),
  ITEM_NUMBER             VARCHAR2(81),
  TRANSACTION_TYPE        VARCHAR2(10),
  SET_PROCESS_ID          NUMBER NOT NULL,
  OLD_CATEGORY_ID         NUMBER,
  OLD_CATEGORY_NAME       VARCHAR2(81),
  SOURCE_SYSTEM_ID        NUMBER,
  SOURCE_SYSTEM_REFERENCE VARCHAR2(255),
  CHANGE_ID               NUMBER,
  CHANGE_LINE_ID          NUMBER,
  BUNDLE_ID               NUMBER
);
