CREATE OR REPLACE 
PACKAGE BODY XXEIS.EIS_XXWC_OM_RENTAL_PKG
AS
  --//============================================================================
  --//
  --// Change Request    :: Adding Columns
  --//
  --// Object Usage     :: This Object Referred by "Rental Utilization and Usage - WC"
  --//
  --// Object Name          :: EIS_XXWC_OM_RENTAL_PKG
  --//
  --// Object Type          :: Package Body
  --//
  --// Object Description    :: This Package will trigger in view and populate the values into View.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva        05/05/2016      Initial Build  -- Added by Siva for TMS#20160503-00221 on 05/05/2016
  --//============================================================================
FUNCTION GET_ITEM_CATEGORY(
    P_INVENTORY_ITEM_ID NUMBER,
    P_ORGANIZATION_ID   NUMBER,
    P_TYPE              VARCHAR2)
  RETURN VARCHAR2
IS
  --//============================================================================
  --//
  --// Change Request    :: Adding Columns
  --//
  --// Object Usage     :: This Object Referred by "Rental Utilization and Usage - WC"
  --//
  --// Object Name          :: GET_ITEM_CATEGORY
  --//
  --// Object Type          :: Function
  --//
  --// Object Description    :: This Function will populate the Category Class,Category and Sub Category  values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva        05/05/2016      Initial Build  -- Added by Siva for TMS#20160503-00221 on 05/05/2016
  --//============================================================================
  L_HASH_INDEX VARCHAR2(300);

  l_sql VARCHAR2(32000);
BEGIN
  G_CATEGORY_CLASS := NULL;
  G_CATEGORY       := NULL;
  G_SUB_CATEGORY   := NULL;
  g_cat_class_desc := NULL;
  
  l_SQL            :=  'SELECT    MC.CONCATENATED_SEGMENTS CATEGORY_CLASS                                           
,T_CAT.DESCRIPTION       CATEGORY                                             
,T_SUB_CAT.DESCRIPTION    SUBCATEGORY_DESC                   
,MCV.DESCRIPTION  CAT_CLASS_DESCRIPTION 
from       MTL_ITEM_CATEGORIES    MIC         
, MTL_CATEGORIES_B_KFV   MC         
,APPS.MTL_CATEGORIES_V   MCV         
,APPS.FND_FLEX_VALUES    B_CAT         
,APPS.FND_FLEX_VALUES_VL  T_CAT         
,APPS.FND_FLEX_VALUES    B_SUB_CAT         
,APPS.FND_FLEX_VALUES_VL  T_SUB_CAT   
where 1=1   
AND MIC.CATEGORY_SET_ID         = 1100000062     
AND MIC.INVENTORY_ITEM_ID       = :1   
AND MIC.ORGANIZATION_ID         = :2   
AND MIC.CATEGORY_ID             = MC.CATEGORY_ID   
AND MC.CATEGORY_ID              = MCV.CATEGORY_ID   
AND MCV.STRUCTURE_ID           = 101   
AND B_CAT.FLEX_VALUE          = MCV.ATTRIBUTE5   
AND B_CAT.FLEX_VALUE_ID       = T_CAT.FLEX_VALUE_ID   
AND T_CAT.FLEX_VALUE_SET_ID     = 1016377   
AND B_SUB_CAT.FLEX_VALUE        = MCV.ATTRIBUTE6     
AND B_SUB_CAT.FLEX_VALUE_ID     = T_SUB_CAT.FLEX_VALUE_ID   
AND T_SUB_CAT.FLEX_VALUE_SET_ID = 1016378'
  ;
  
  BEGIN
    L_HASH_INDEX       :=P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID;
    
    IF P_TYPE           ='CATEGORY_CLASS' THEN
      G_CATEGORY_CLASS := G_CATEGORY_CLASS_VLDN_TBL(L_HASH_INDEX);
    ELSIF P_TYPE        ='CATEGORY' THEN
      G_CATEGORY       := G_CATEGORY_VLDN_TBL(L_HASH_INDEX);
    ELSIF P_TYPE        ='SUB_CATEGORY' THEN
      G_SUB_CATEGORY   := G_SUB_CATEGORY_VLDN_TBL(L_HASH_INDEX);
    ELSIF P_TYPE        ='CAT_CLASS_DESC' THEN
      g_cat_class_desc := G_CAT_CLASS_DESC_VLDN_TBL(L_HASH_INDEX);
    END IF;
    
  EXCEPTION
  WHEN no_data_found THEN
    BEGIN
      EXECUTE IMMEDIATE L_SQL INTO G_CATEGORY_CLASS,G_CATEGORY,G_SUB_CATEGORY,g_cat_class_desc USING p_inventory_item_id,p_organization_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      IF P_TYPE           ='CATEGORY_CLASS' THEN
        G_CATEGORY_CLASS := NULL;
      ELSIF P_TYPE        ='CATEGORY' THEN
        G_CATEGORY       := NULL;
      ELSIF P_TYPE        ='SUB_CATEGORY' THEN
        G_SUB_CATEGORY   := NULL;
      ELSIF P_TYPE        ='CAT_CLASS_DESC' THEN
        G_CAT_CLASS_DESC := NULL;
      END IF;
    END;
    L_HASH_INDEX                                := P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID;
    G_CATEGORY_CLASS_VLDN_TBL(L_HASH_INDEX)     := G_CATEGORY_CLASS;
    G_CATEGORY_VLDN_TBL(L_HASH_INDEX)           := G_CATEGORY;
    G_SUB_CATEGORY_VLDN_TBL(L_HASH_INDEX)       := G_SUB_CATEGORY;
    G_CAT_CLASS_DESC_VLDN_TBL(L_HASH_INDEX)     := g_cat_class_desc;
  END;
  IF P_TYPE='CATEGORY_CLASS' THEN
    RETURN G_CATEGORY_CLASS;
  ELSIF P_TYPE='CATEGORY' THEN
    RETURN G_CATEGORY ;
  ELSIF P_TYPE='SUB_CATEGORY' THEN
    RETURN G_SUB_CATEGORY ;
  ELSIF P_TYPE='CAT_CLASS_DESC' THEN
    RETURN G_CAT_CLASS_DESC;
  END IF;
EXCEPTION
WHEN OTHERS THEN
  IF P_TYPE      ='CATEGORY_CLASS' THEN
    G_CATEGORY_CLASS := NULL;
    RETURN G_CATEGORY_CLASS;
  ELSIF P_TYPE   ='CATEGORY' THEN
    G_CATEGORY    := NULL;
    RETURN G_CATEGORY ;
  ELSIF P_TYPE      ='SUB_CATEGORY' THEN
    G_SUB_CATEGORY := NULL;
    RETURN G_SUB_CATEGORY ;
  ELSIF P_TYPE        ='CAT_CLASS_DESC' THEN
    G_CAT_CLASS_DESC := NULL;
    RETURN G_CAT_CLASS_DESC;
  END IF;
END GET_ITEM_CATEGORY;
END EIS_XXWC_OM_RENTAL_PKG;
/
