CREATE OR REPLACE 
PACKAGE  XXEIS.EIS_XXWC_OM_RENTAL_PKG IS
 --//============================================================================
--//  
--// Change Request 			:: Adding Columns  
--//
--// Object Usage 				:: This Object Referred by "Rental Utilization and Usage - WC"
--//
--// Object Name         	:: EIS_XXWC_OM_RENTAL_PKG
--//
--// Object Type         	:: Package Specification
--//
--// Object Description  		:: This Package will trigger in view and populate the values into View.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	     05/05/2016      Initial Build  -- Added by Pramod Kodali for TMS#20160503-00221 on 05/05/2016
--//============================================================================
type T_CASHTYPE_VLDN_TBL is table of varchar2(200) index by varchar2(200);-- Added by Pramod Kodali for TMS#20160503-00221 on 05/05/2016

G_CATEGORY_VLDN_TBL   T_CASHTYPE_VLDN_TBL;
G_SUB_CATEGORY_VLDN_TBL   T_CASHTYPE_VLDN_TBL;
G_CATEGORY_CLASS_VLDN_TBL   T_CASHTYPE_VLDN_TBL;
G_CAT_CLASS_DESC_VLDN_TBL   T_CASHTYPE_VLDN_TBL;

  G_CATEGORY_CLASS varchar2(300);
  G_CATEGORY       varchar2(300);
  G_SUB_CATEGORY   VARCHAR2(300);
  g_cat_class_desc VARCHAR2(300);
  
FUNCTION GET_ITEM_CATEGORY(P_INVENTORY_ITEM_ID number, P_ORGANIZATION_ID number,P_TYPE varchar2) return varchar2;
 --//============================================================================
--//  
--// Change Request 			:: Adding Columns  
--//
--// Object Usage 				:: This Object Referred by "Rental Utilization and Usage - WC"
--//
--// Object Name         	:: GET_ITEM_CATEGORY
--//
--// Object Type         	:: Function
--//
--// Object Description  		:: This Function will populate the Category Class,Category and Sub Category  values in the view.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	     05/05/2016      Initial Build  -- Added by Pramod Kodali for TMS#20160503-00221 on 05/05/2016
--//============================================================================
END;
/
