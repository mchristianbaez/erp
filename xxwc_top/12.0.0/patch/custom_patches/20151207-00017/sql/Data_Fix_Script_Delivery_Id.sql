/* Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        09-Dec-2015  Kishorebabu V    TMS#20151207-00017 data fix script to process month end transactions
                                            Initial Version   */

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;
BEGIN
  UPDATE apps.wsh_delivery_Details
  SET    oe_interfaced_flag = 'Y'
  WHERE  delivery_detail_id = 13328063
  AND    source_header_id   = 36555159
  AND    source_line_id     = 59972326;
--1 row expected to be updated
COMMIT;
EXCEPTION
  WHEN others THEN
    dbms_output.put_line('Error occured while updating OE_INTERFACED_FLAG: '||SQLERRM);
END;
/