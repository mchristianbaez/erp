/* Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        09-Dec-2015  Kishorebabu V    TMS#20151207-00017 data fix script to process month end transactions
                                            Initial Version   */

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;
BEGIN
update apps.wsh_delivery_Details
set OE_INTERFACED_FLAG  ='Y'
where DELIVERY_DETAIL_ID=13328062
and source_header_id    =36555159
and source_line_id      =59972323;
--1 row expected to be updated
commit;
EXCEPTION
  WHEN others THEN
    dbms_output.put_line('Error occured while updating OE_INTERFACED_FLAG: '||SQLERRM);
END;
/