CREATE OR REPLACE PACKAGE  XXEIS.EIS_RS_XXWC_EOM_COMM_REP_PKG
AUTHID CURRENT_USER
--//============================================================================
--//  
--// Object Name         		:: xxeis.EIS_RS_XXWC_EOM_COMM_REP_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Temp Table
--//
--// Version Control
--//============================================================================
--// Vers    	Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0      19-Jul-2016        Siva   		 TMS#20160708-00110
--//============================================================================
is
   procedure COMM_REP_PAR (P_PROCESS_ID       IN NUMBER
                          ,p_period_name      IN VARCHAR2
                          ,p_operating_unit   IN VARCHAR2
                          ,p_invoice_source   IN VARCHAR2
                          );
--//============================================================================
--//
--// Object Name         :: comm_rep_par
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure gets the record based on parameter and insert into temp table
--//
--// Version Control
--//============================================================================
--// Vers    Author             Date            Description
--//----------------------------------------------------------------------------
--// 1.0      19-Jul-2016        Siva   		 TMS#20160708-00110
--//============================================================================	
 PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER);
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0      19-Jul-2016        Siva   		TMS#20160708-00110
--//============================================================================    

END EIS_RS_XXWC_EOM_COMM_REP_PKG;
/
