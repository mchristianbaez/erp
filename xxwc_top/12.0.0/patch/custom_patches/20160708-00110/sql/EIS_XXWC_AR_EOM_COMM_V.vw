---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_AR_EOM_COMM_V $
  Module Name : Receivables
  PURPOSE	  : White Cap End of Month Commissions Report
  TMS Task Id : 20160708-00110
  REVISIONS   :
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     19-Jul-2016        Siva   		 TMS#20160708-00110  
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_AR_EOM_COMM_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_AR_EOM_COMM_V (PROCESS_ID,BUSINESSDATE, REPORT_TYPE, ORDER_NUMBER, INVOICENUMBER, LINENO, DESCRIPTION, QTY, UNITPRICE, EXTSALE,
 MASTERNUMBER, MASTERNAME, JOBNUMBER, CUSTOMERNAME, SALESREPNUMBER, SALESREPNAME, LOC, MP_ORGANIZATION_ID, REGIONNAME, PARTNO, CATCLASS, INVOICE_SOURCE,
 SHIPPED_QUANTITY, DIRECTFLAG, AVERAGECOST, TRADER, SPECIAL_COST, OPERATING_UNIT, LIST_PRICE, ORG_ID, INVENTORY_ITEM_ID, ORGANIZATION_ID, CUSTOMER_TRX_ID,
 PARTY_ID, PERIOD_NAME, MFGADJUST, SALESREP_TYPE, SALES_TYPE, INTERFACE_LINE_ATTRIBUTE2, INSIDESALESREP, INSIDESRNUM, PRISMSRNUM, LINETYPE, HRINSIDESRNAME, HRINSIDESRNUM) AS 
  SELECT 
		  PROCESS_ID
		 ,BUSINESSDATE  
         ,REPORT_TYPE
         ,ORDER_NUMBER
         ,INVOICENUMBER
         ,LINENO
         ,DESCRIPTION
         ,QTY
         ,UNITPRICE
         ,EXTSALE
         ,MASTERNUMBER
         ,MASTERNAME
         ,JOBNUMBER
         ,CUSTOMERNAME
         ,SALESREPNUMBER
         ,SALESREPNAME
         ,LOC
         ,MP_ORGANIZATION_ID
         ,REGIONNAME
         ,PARTNO
         ,CATCLASS
         ,INVOICE_SOURCE
         ,SHIPPED_QUANTITY
         ,DIRECTFLAG
         ,AVERAGECOST
         ,TRADER
         ,SPECIAL_COST
         ,OPERATING_UNIT
         ,LIST_PRICE
         ,ORG_ID
         ,INVENTORY_ITEM_ID
         ,ORGANIZATION_ID
         ,CUSTOMER_TRX_ID
         ,PARTY_ID
         ,PERIOD_NAME
         ,MFGADJUST
         ,SALESREP_TYPE
         ,SALES_TYPE
         ,INTERFACE_LINE_ATTRIBUTE2
         ,INSIDESALESREP
         ,INSIDESRNUM
         ,PRISMSRNUM
         ,LINETYPE
         ,HRINSIDESRNAME
         ,HRINSIDESRNUM
FROM XXEIS.EIS_XXWC_AR_EOM_COMM_TBL
/
