  /****************************************************************************************************************************************
  *   PROCEDURE: cancel_requisition_lines                                                                                                 *
  *                                   																									  *
  *   PURPOSE: Procedure will pick Internal and purchasing requisition which is in approved status and older than 7 days and canclled     *
  *                                   																									  *
  *   HISTORY                                																							  *
  *   ===========================================================================               									      *
  *   ===========================================================================              											  *
  *   VERSION DATE          AUTHOR(S)               DESCRIPTION                   														  *
  *   ------- -----------   ---------------         -------------------------------------             									  *
  *    1.0    01-MAR-2016   Pattabhi Avula          Initial Version TMS# 20160209-00200                                                   *
  ****************************************************************************************************************************************/
--SET SERVEROUTPUT ON SIZE 100000;
SET SERVEROUTPUT ON SIZE UNLIMITED;
SET VERIFY OFF;

DECLARE
  -- Approved Internal Requisitions
  l_return_status VARCHAR2 (1000);
  l_msg_count     NUMBER;
  l_msg_data      VARCHAR2 (1000);
  ln_header_id po_tbl_number;
  ln_line_id po_tbl_number;
  m               NUMBER := NULL;
  l_msg_dummy     VARCHAR2 (2000);
  l_output        VARCHAR2 (2000);
  ln_user_id      NUMBER;
  ln_resp_id      NUMBER;
  ln_resp_appl_id NUMBER;
  ln_org_id       NUMBER := 162;

  
  CURSOR cur_req_intnpur
  IS
    SELECT prh.segment1, prh.requisition_header_id, prl.requisition_line_id, prl.line_num        
    FROM po_requisition_headers_all prh, 
	     po_requisition_lines_all prl
   WHERE    1=1
         AND prh.requisition_header_id = prl.requisition_header_id
         AND prh.type_lookup_code='INTERNAL'
         AND prh.AUTHORIZATION_STATUS = 'APPROVED'
         AND NVL(prh.cancel_flag,'N')='N'
         AND NVL(prl.cancel_flag,'N')='N'
         AND NVL(prh.CLOSED_CODE,'OPEN') NOT IN ('FINALLY CLOSED','CLOSED')
         AND NVL(prl.CLOSED_CODE,'OPEN') NOT IN ('FINALLY CLOSED','CLOSED')
         AND PRH.CREATION_DATE <TRUNC(SYSDATE-7)
        -- and PRH.REQUISITION_HEADER_ID=489623
         AND NOT EXISTS (SELECT 1 FROM OE_ORDER_LINES_ALL OLA WHERE PRL.REQUISITION_LINE_ID = OLA.SOURCE_DOCUMENT_LINE_ID AND LINE_TYPE_ID=1012)
UNION ALL
  SELECT prh.segment1, prh.requisition_header_id, prl.requisition_line_id, prl.line_num        
    FROM po_requisition_headers_all prh, po_requisition_lines_all prl,po_req_distributions_all prd
   WHERE    1=1
         AND prh.requisition_header_id = prl.requisition_header_id
         AND prl.requisition_line_id=prd.requisition_line_id
         AND prh.type_lookup_code='PURCHASE'
         AND prh.AUTHORIZATION_STATUS = 'APPROVED'
         AND NVL(prh.cancel_flag,'N')='N'
         AND NVL(prl.cancel_flag,'N')='N'
         AND NVL(prh.CLOSED_CODE,'OPEN') NOT IN ('FINALLY CLOSED','CLOSED')
         AND NVL(prl.CLOSED_CODE,'OPEN') NOT IN ('FINALLY CLOSED','CLOSED')
        AND PRH.CREATION_DATE <TRUNC(SYSDATE-7)
       --- and PRH.REQUISITION_HEADER_ID=10686498
         AND NOT EXISTS (SELECT 1 FROM PO_DISTRIBUTIONS_ALL PDA WHERE REQ_DISTRIBUTION_ID=PRD.DISTRIBUTION_ID)
		 AND NOT EXISTS (SELECT 1 FROM oe_drop_ship_sources WHERE REQUISITION_LINE_ID= prl.requisition_line_id)
        ORDER BY 1;

BEGIN
  DBMS_OUTPUT.ENABLE(10000000);
  SELECT user_id
  INTO   ln_user_id
  FROM   fnd_user
  WHERE  user_name = 'XXWC_INT_SUPPLYCHAIN';
  
  
  SELECT responsibility_id,
         application_id
  INTO   ln_resp_id,
         ln_resp_appl_id
  FROM   fnd_responsibility_vl
  WHERE responsibility_name = 'HDS Purchasing Super User - WC';
  
  fnd_global.apps_initialize (user_id => ln_user_id, resp_id => ln_resp_id, --'HDS Purchasing Super User'
  resp_appl_id => ln_resp_appl_id);
  mo_global.init ('PO');
  mo_global.set_policy_context ('S', ln_org_id);
  fnd_request.set_org_id (fnd_global.org_id);
  
  -- Approved Internal and Purchase Requisitions
  FOR I    IN cur_req_intnpur
  LOOP
    m            := 1;
    ln_header_id := po_tbl_number (I.requisition_header_id);
    ln_line_id   := po_tbl_number (I.requisition_line_id);
    po_req_document_cancel_grp.cancel_requisition (p_api_version => 1.0, 
	                                               p_req_header_id => ln_header_id, 
												   p_req_line_id => ln_line_id, 
												   p_cancel_date => SYSDATE, 
												   p_cancel_reason => 'Cancelled for TMS 20160209-00200', 
												   p_source => 'REQUISITION', 
												   x_return_status => l_return_status, 
												   x_msg_count => l_msg_count, 
												   x_msg_data => l_msg_data
												  );
    COMMIT;
  -- DBMS_OUTPUT.PUT_LINE('For the ReqRequisition line id'||' '||I.requisition_line_id||' '||'status is :'||' '||l_return_status);
  DBMS_OUTPUT.PUT_LINE('Req_num -'||I.segment1||','||'Req_line-'||I.line_num||','||'Req_line_id-'||I.requisition_line_id||','||'status is-'||l_return_status);
   
    IF l_return_status <> 'S' THEN
      fnd_msg_pub.get (m, fnd_api.g_false, l_msg_data, l_msg_dummy);
      l_output   := (TO_CHAR (m) || ': ' || l_msg_data);
    --  DBMS_OUTPUT.PUT_LINE('Failed requisition id :'||I.requisition_line_id||' '||'and details is: '||l_output);
	DBMS_OUTPUT.PUT_LINE('Failed-Req_num -'||I.segment1||','||'Req_line-'||I.line_num||','||'Req_line_id-'||I.requisition_line_id||'Error is-'||l_output); 
    END IF;
  END LOOP;
 END;
 /