/*
 TMS: 20150804-00228
 Date: 02/16/2016
 Notes:  Update oracle owned tables pn_locations_all.attribute6 and pn_lease_details_all.attribute8 to blank when value matches %imaging2% 
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 b_go boolean :=Null;
 n_loc number :=Null;
 --
BEGIN --Main Processing...
   --
   b_go :=TRUE;
   --
   n_loc :=101;
   --
  if (b_go) then 
   --
   n_loc :=102;
   --
   begin
    --
     dbms_output.put_line('Before UPDATE of table apps.pn_locations_all ATTRIBUTE6 when value contains imaging2');
     dbms_output.put_line(' ');
    update apps.pn_locations_all
	set attribute6 =Null
	where 1 =1
	and attribute6 like '%imaging2%';
    dbms_output.put_line('After UPDATE of table apps.pn_locations_all ATTRIBUTE6 when value contains imaging2, rows updated :'||sql%rowcount); 
    dbms_output.put_line(' ');    
    --
    n_loc :=104;
    --
   exception
    when others then
     --
     n_loc :=105;
     --
     dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
     --   
   end;
   --
   begin 
    --
    n_loc :=106;
    --
    dbms_output.put_line('Before UPDATE of table apps.apps.pn_lease_details_all ATTRIBUTE8 when value contains imaging2');
     dbms_output.put_line(' ');    
    update apps.pn_lease_details_all
	set attribute8 =Null
	where 1 =1
	and attribute8 like '%imaging2%';
    dbms_output.put_line('After UPDATE of table apps.apps.pn_lease_details_all ATTRIBUTE8 when value contains imaging2, rows updated :'||sql%rowcount);
     dbms_output.put_line(' ');    
    --           
   exception
    when others then
     dbms_output.put_line(('****** @@@@@@ , '||sqlerrm));                  
   end;
   --
  else
   --
   n_loc :=103;
   --
   dbms_output.put_line('@b_go =FALSE, n_loc ='||n_loc);
   --
  end if;  --  if (b_go) then 
 --
 commit;
 --
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line (
         'TMS: 20150804-00228, Errors =' || SQLERRM);
END;
/