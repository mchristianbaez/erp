CREATE OR REPLACE PACKAGE BODY APPS.xxwc_po_reset_docs_pkg
AS
   /*************************************************************************
    Copyright (c) HD Supply Group
    All rights reserved.
   **************************************************************************
     $Header xxwc_req_or_po_stuck_in_wf $
     Module Name: xxwc_req_or_po_stuck_in_wf.pks

     PURPOSE:   

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        04/28/2017  Krishna Kumar          Initial Version(TMS#20160823-00016)
   /*************************************************************************
     Procedure : update_po_reqs_status

     PURPOSE:   This procedure will update the status for no open items for Requisitions
	            and Purchase orders in workflow
     Parameter:  

   ************************************************************************/
    PROCEDURE update_po_reqs_status (errbuf             OUT      VARCHAR2,
                                     retcode            OUT      VARCHAR2,
                                     p_document_type    IN       VARCHAR2,
                                     p_doc_number       IN       VARCHAR2,
	                                 p_del_Actn_Hist    IN       VARCHAR2
                                     )
      IS
	  
	  l_err_callfrom    VARCHAR2 (75)
                           := 'xxwc_po_reset_docs_pkg.update_po_reqs_status';
      l_sec             VARCHAR2 (150) := 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_message         VARCHAR2 (2000);
	  l_org_id          NUMBER(10):= FND_PROFILE.VALUE('ORG_ID');
	  l_request_id      NUMBER(20);
	  
	  -- Fetching the Requisition details
       CURSOR cur_reqtoreset
         IS
           SELECT  wf_item_type,
		           wf_item_key,
				   requisition_header_id, 
				   segment1,
                   type_lookup_code
           FROM    po_requisition_headers_all h
           WHERE   h.segment1 = p_doc_number
           AND     h.org_id = l_org_id
           AND     h.authorization_status IN ('IN PROCESS','PRE-APPROVED')
           AND     NVL(h.closed_code, 'OPEN') != 'FINALLY_CLOSED'
           AND     NVL(h.cancel_flag, 'N') = 'N';


      --  Fetching the workflow item details for requisitions
       CURSOR cur_wfstoabort(st_item_type VARCHAR2,st_item_key VARCHAR2)
         IS
          SELECT  level,
		          item_type,
				  item_key,
				  end_date
          FROM    wf_items
          START WITH
                  item_type = st_item_type 
		  AND     item_key =  st_item_key
          CONNECT BY
          PRIOR   item_type = parent_item_type AND     
		  PRIOR item_key = parent_item_key
          ORDER BY level DESC;
		  
 wf_rec cur_wfstoabort%ROWTYPE;
 TYPE enc_tbl_number is TABLE OF NUMBER;
 TYPE enc_tbl_flag is TABLE OF VARCHAR2(1);
 
 x_req_number varchar2(20);
 req_enc_flag varchar2(1);
 x_open_notif_exist varchar2(1);
 ros cur_reqtoreset%ROWTYPE;

 x_progress varchar2(500);
 x_count_po_assoc number;
 x_active_wf_exists varchar2(1);
 l_tax NUMBER;
 l_amount NUMBER;
 l_req_dist_id    enc_tbl_number;
 l_req_enc_flag   enc_tbl_flag;
 l_req_enc_amount enc_tbl_number;
 l_req_gl_amount  enc_tbl_number;
 l_req_price	    enc_tbl_number;
 l_req_dist_qty   enc_tbl_number;
 l_req_dist_rate  enc_tbl_number;
 l_manual_cand    enc_tbl_flag;

 g_po_debug       VARCHAR2(1) := 'Y';
 l_timestamp      DATE := sysdate;
 l_precision      fnd_currencies.precision%type;
 l_min_acc_unit   fnd_currencies.minimum_accountable_unit%TYPE;
 l_disallow_script VARCHAR2(1);
 l_req_encumbrance VARCHAR2(1);
		  
	
  -- Fetching Purchase order details
  
 CURSOR cur_potoreset(po_number VARCHAR2, l_org_id NUMBER) 
   IS
    SELECT wf_item_type, 
	       wf_item_key, 
		   po_header_id, 
		   segment1,
           revision_num, 
		   type_lookup_code,
		   approved_date
      FROM po_headers_all
     WHERE segment1 = po_number
       AND NVL(org_id,-99) = NVL(l_org_id,-99)
    -- bug 5015493: Need to allow reset of blankets and PPOs also.
    -- and type_lookup_code = 'STANDARD'
       AND authorization_status IN ('IN PROCESS', 'PRE-APPROVED')
       AND NVL(cancel_flag, 'N') = 'N'
       AND NVL(closed_code, 'OPEN') <> 'FINALLY_CLOSED';

/* select the max sequence number with NULL action code */

   CURSOR cur_maxseq(id number, subtype po_action_history.object_sub_type_code%type)
    IS
     SELECT nvl(max(sequence_num), 0)
       FROM po_action_history
      WHERE object_type_code IN ('PO', 'PA')
        AND object_sub_type_code = subtype
        AND object_id = id
        AND action_code is NULL;

/* select the max sequence number with submit action */

    CURSOR cur_poaction(id number, subtype po_action_history.object_sub_type_code%TYPE) 
	 IS
      SELECT nvl(max(sequence_num), 0)
        FROM po_action_history
       WHERE object_type_code IN ('PO', 'PA')
         AND object_sub_type_code = subtype
         AND object_id = id
         AND action_code = 'SUBMIT';

    

submitseq po_action_history.sequence_num%type;
nullseq po_action_history.sequence_num%type;

 x_organization_id number ;
 x_po_number varchar2(20);
 po_enc_flag varchar2(1);
 l_p_del_actn_hist VARCHAR2(1);
 l_po_count    NUMBER(10);
 pos cur_potoreset%ROWTYPE;
 
 x_cont varchar2(10);
 l_delete_act_hist varchar2(1);
 l_change_req_exists varchar2(1);
 l_res_seq po_action_history.sequence_num%TYPE;
 l_sub_res_seq po_action_history.sequence_num%TYPE;
 l_res_act po_action_history.action_code%TYPE;
 l_del_res_hist varchar2(1);
 

 /* For encumbrance actions */
 
 NAME_ALREADY_USED EXCEPTION;
 PRAGMA Exception_Init(NAME_ALREADY_USED,-955);
 X_STMT VARCHAR2(2000);
 disallow_script VARCHAR2(1);
 

l_dist_id      		enc_tbl_number;
l_enc_flag     		enc_tbl_flag;
l_enc_amount   		enc_tbl_number;
l_gl_amount    		enc_tbl_number;
l_manual_cand  		enc_tbl_flag;
l_req_dist_id  		enc_tbl_number;
l_req_enc_flag 		enc_tbl_flag;
l_req_enc_amount 	enc_tbl_number;
l_req_gl_amount 	enc_tbl_number;
l_req_qty_bill_del  	enc_tbl_number;
l_rate_table	      	enc_tbl_number;
l_price_table       	enc_tbl_number;
l_qty_ordered_table 	enc_tbl_number;
l_req_price_table   	enc_tbl_number;
l_req_encumbrance_flag	varchar2(1);
l_purch_encumbrance_flag varchar2(1);
l_remainder_qty         NUMBER;
l_bill_del_amount  	NUMBER;
l_req_bill_del_amount   NUMBER;
l_qty_bill_del     	NUMBER;
l_timestamp    		date;
l_eff_quantity 		NUMBER;
l_rate         		NUMBER;
l_price        		NUMBER;
l_ordered_quantity 	NUMBER;
l_tax 	       		NUMBER;
l_amount       		NUMBER;
l_precision    		fnd_currencies.precision%type;
l_min_acc_unit 		fnd_currencies.minimum_accountable_unit%TYPE;
l_approved_flag 	po_line_locations_all.approved_flag%TYPE;
i 			number;
j 			number;
k 			number;  
 
 BEGIN
  fnd_file.put_line (fnd_file.LOG,'Document Number '||p_doc_number||' in org '||l_org_id);
	
   IF p_document_type='Requisition'  AND p_doc_number IS NOT NULL
	THEN
	BEGIN
	 BEGIN
	  l_sec :='Before checking the Open Notification for Requisition';
       SELECT  'Y'
        INTO   x_open_notif_exist
        FROM   dual
        WHERE  EXISTS(SELECT  'open notifications'
 	   	              FROM    wf_item_activity_statuses wias,
 	   		                  wf_notifications wfn,
 	   		                  po_requisition_headers_all porh
 	   	              WHERE   wias.notification_id IS NOT NULL
 	   	              AND     wias.notification_id = wfn.group_id
 	   	              AND     wfn.status = 'OPEN'
 	   	              AND     wias.item_type = 'REQAPPRV'
 	   	              AND     wias.item_key = porh.wf_item_key
 	   	              AND     porh.org_id = l_org_id
 	   	              AND     porh.segment1=p_doc_number
 	   	              AND     porh.authorization_status IN ('IN PROCESS','PRE-APPROVED'));
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
	     fnd_file.put_line (fnd_file.LOG,'No such requisition with req number
                     		 '||p_doc_number);
      x_open_notif_exist:='N';
	   WHEN OTHERS THEN
         fnd_file.put_line (fnd_file.LOG,'Unexpected error for '||p_doc_number||
		                   'Error Message is '||SQLERRM);
     END;
	 
   IF (x_open_notif_exist = 'N') THEN   
   
   BEGIN
    l_sec := 'Checking the Requisition with associate POs';
     SELECT  count(*)
     INTO    x_count_po_assoc
     FROM    po_requisition_lines_all prl,
             po_requisition_headers_all prh
     WHERE   prh.segment1= p_doc_number
     AND     prh.org_id = l_org_id
     AND     prh.requisition_header_id = prl.requisition_header_id
     AND    (prl.line_location_id IS NOT NULL OR
            NVL(prh.transferred_to_oe_flag,'N') = 'Y');
   EXCEPTION
     WHEN others THEN
	 fnd_file.put_line(fnd_file.LOG,'Unexpected error while fetching the count for '
	                   ||p_doc_number||' Error details is '||SQLERRM);
   END;
   
   IF (x_count_po_assoc > 0) THEN
      fnd_file.put_line(fnd_file.LOG,'This '||p_doc_number|| 'requisition is associated with a PO or sales order 
	                     and hence cannot be reset.');
      RETURN;
   END IF;

   OPEN cur_reqtoreset;

   FETCH cur_reqtoreset INTO ros;

   IF cur_reqtoreset%NOTFOUND THEN
     fnd_file.put_line(fnd_file.LOG,'No such requisition with req number '||p_doc_number||
	                   ' exists which requires to be reset');
	--NTINUE;
    --RETURN;
	GOTO LAST_STEP;
	
   END IF;

   IF (g_po_debug = 'Y') then
       fnd_file.put_line(fnd_file.LOG,'Processing '||ros.type_lookup_code
                             ||' Req Number: '
                             ||ros.segment1);
       fnd_file.put_line(fnd_file.LOG,'......................................'); --116
   END IF;
   
   l_disallow_script := 'N'; 
    BEGIN
     SELECT 'Y'
     INTO   l_disallow_script
     FROM   dual
     WHERE  EXISTS (SELECT 'Wrong Encumbrance Amount'
                    FROM   po_requisition_lines_all l,
                           po_req_distributions_all d
                    WHERE  l.requisition_header_id = ros.requisition_header_id
                    AND    d.requisition_line_id = l.requisition_line_id
                    AND    l.matching_basis = 'QUANTITY'
                    AND    Nvl(d.encumbered_flag, 'N') = 'Y'
                    AND    Nvl(l.cancel_flag, 'N') = 'N'
                    AND    Nvl(l.closed_code, 'OPEN') <> 'FINALLY CLOSED'
                    AND    Nvl(d.prevent_encumbrance_flag, 'N') = 'N'
                    AND    d.budget_account_id IS NOT NULL
                    AND    ROUND(Nvl(d.encumbered_amount, 0), 2) != 
                               ROUND(l.unit_price * d.req_line_quantity  + NVL(d.nonrecoverable_tax, 0), 2)
                    UNION
                    SELECT 'Wrong Encumbrance Amount'
                    FROM   po_requisition_lines_all l,
                           po_req_distributions_all d
                    WHERE  l.requisition_header_id = ros.requisition_header_id
                    AND    d.requisition_line_id = l.requisition_line_id
                    AND    l.matching_basis = 'AMOUNT'
                    AND    Nvl(d.encumbered_flag, 'N') = 'Y'
                    AND    Nvl(l.cancel_flag, 'N') = 'N'
                    AND    Nvl(l.closed_code, 'OPEN') <> 'FINALLY CLOSED'
                    AND    Nvl(d.prevent_encumbrance_flag, 'N') = 'N'
                    AND    d.budget_account_id IS NOT NULL
                    AND    ROUND(Nvl(d.encumbered_amount, 0), 2) != 
                               ROUND(d.req_line_amount  + NVL(d.nonrecoverable_tax, 0), 2));
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
      fnd_file.put_line(fnd_file.LOG,'Error while executing Encumbrance script for '
	            ||p_doc_number||' and  Error Details are :'||SQLERRM);
	l_disallow_script:='N';
    END;
	
	IF l_disallow_script = 'Y' THEN
      fnd_file.put_line(fnd_file.LOG,'This '||p_doc_number||' Requisition has at least one distribution 
	                       with wrong Encumbrance amount.');
      fnd_file.put_line(fnd_file.LOG,'Hence this'||p_doc_number||' Requisition can not be reset');
     CLOSE cur_reqtoreset;
     RETURN;
    END IF;


/* abort workflow processes if they exists */

     -- first check whether the wf process exists or not



    BEGIN
     SELECT 'Y'
     INTO   x_active_wf_exists
     FROM   wf_items wfi
     WHERE  wfi.item_type = ros.wf_item_type
     AND    wfi.item_key = ros.wf_item_key
 	 AND    wfi.end_date IS NULL;
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
       	    X_active_wf_exists := 'N';
    END;
	
	-- if the wf process is not already aborted then abort it.

    IF (x_active_wf_exists = 'Y') THEN

       IF (g_po_debug = 'Y') THEN
          fnd_file.put_line(fnd_file.LOG,'Aborting Workflow...');
       END IF;
	   
	OPEN cur_wfstoabort(ros.wf_item_type,ros.wf_item_key);

      LOOP
         FETCH cur_wfstoabort INTO wf_rec;
         IF (g_po_debug = 'Y') THEN
	          fnd_file.put_line(fnd_file.LOG,wf_rec.item_type||wf_rec.item_key);
         END IF;
		 
 	     IF cur_wfstoabort%NOTFOUND THEN
 	        CLOSE cur_wfstoabort;
 	         EXIT;
 	     END IF;

 	     IF (wf_rec.end_date IS NULL) THEN
 	       BEGIN
 	  	       WF_Engine.AbortProcess(wf_rec.item_type, wf_rec.item_key);
      	   EXCEPTION
               WHEN OTHERS THEN
                  fnd_file.put_line(fnd_file.LOG,'Could not abort the workflow for REQ :'
 	                                    ||ros.segment1);
                  ROLLBACK;
 	               RETURN;
      	   END;

 	     END IF;
 	  END LOOP;
    END IF;
	
	/* Update the authorization status of the requisition to incomplete */
    IF (g_po_debug = 'Y') THEN
        fnd_file.put_line(fnd_file.LOG,'Updating the Status for Requisition: '||p_doc_number);
    END IF;

     UPDATE po_requisition_headers_all
     SET    authorization_status = 'INCOMPLETE',
            wf_item_type = NULL,
            wf_item_key = NULL
     WHERE  requisition_header_id = ros.requisition_header_id;



/* Update Action history setting the last null action code to NO ACTION */
    IF (g_po_debug = 'Y') THEN
       fnd_file.put_line(fnd_file.LOG,'Updating PO Action History for Requisition: '||ros.segment1);
    END IF;

    BEGIN
      SELECT  NVL(MAX(sequence_num), 0)
      INTO    nullseq
	  FROM    po_action_history
	  WHERE   object_type_code = 'REQUISITION'
	  AND     object_sub_type_code = ros.type_lookup_code
	  AND     object_id = ros.requisition_header_id
	  AND     action_code IS NULL;
	EXCEPTION
      WHEN OTHERS THEN
       fnd_file.put_line(fnd_file.LOG,'Error while executing the sequence_num script for : '||ros.segment1); 
    END;

 	UPDATE po_action_history
 	SET    action_code = 'NO ACTION',
 	       action_date = TRUNC(SYSDATE),
 	       note = 'updated by reset script on '||TO_CHAR(TRUNC(SYSDATE))
    WHERE  object_id = ros.requisition_header_id
    AND    object_type_code = 'REQUISITION'
    AND    object_sub_type_code = ros.type_lookup_code
    AND    sequence_num = nullseq
    AND    action_code IS NULL;
	
	SELECT NVL(req_encumbrance_flag ,'N')
    INTO   l_req_encumbrance
    FROM   financials_system_params_all
    WHERE  org_id =  l_org_id;



    IF l_req_encumbrance  = 'N' THEN
    	fnd_file.put_line (fnd_file.LOG,'Done Processing for :'||ros.segment1);
    	fnd_file.put_line (fnd_file.LOG,'................');
    	fnd_file.put_line (fnd_file.LOG,'Please issue commit, if no errors found for '||ros.segment1);
    	RETURN;
    END IF;

   CLOSE cur_reqtoreset;

 ELSE -- x_open_notif_exist IF Condition
      fnd_file.put_line (fnd_file.LOG,'      ');
      fnd_file.put_line (fnd_file.LOG,'An Open notification exists for this document '||p_doc_number||'
                      you may want to use the notification to process this document'); 

 END IF; 
 
 <<LAST_STEP>>
 NULL;
 
EXCEPTION
WHEN OTHERS THEN
 ROLLBACK;
   CLOSE cur_reqtoreset;   
xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_sec
           ,p_request_id          => l_request_id
           ,p_ora_error_msg       => l_message
           ,p_error_desc          => 'Error while executing xxwc_po_reset_docs_pkg fix, Error details are '||SQLERRM
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXWC');
 END;	
 
ELSIF p_document_type='Purchase Order'  AND p_doc_number IS NOT NULL
 THEN
  BEGIN
  x_progress := '010: start';
  l_sec:=' Checking the open notifications for PO';
   BEGIN
    SELECT 'Y'
    INTO    x_open_notif_exist
    FROM    dual
    WHERE EXISTS (SELECT  'open notifications'
		          FROM    wf_item_activity_statuses wias,
			              wf_notifications wfn,
			              po_headers_all poh
		          WHERE   wias.notification_id IS NOT NULL
		          AND     wias.notification_id = wfn.group_id
		          AND     wfn.status = 'OPEN'
		          AND     wias.item_type = 'POAPPRV'
		          AND     wias.item_key = poh.wf_item_key
		          AND     NVL(poh.org_id,-99) = NVL(l_org_id,-99)
		          AND     poh.segment1=p_doc_number
		          AND     poh.authorization_status IN ('IN PROCESS', 'PRE-APPROVED'));
   EXCEPTION 
   WHEN NO_DATA_FOUND THEN
     fnd_file.put_line (fnd_file.LOG,'Error while checking the open notifications for 
	                    po '||p_doc_number||' and error details are :'||SQLERRM);
	x_open_notif_exist:='N';
   END;
		      
   x_progress := '020: selected open notif';

   IF (x_open_notif_exist = 'N') THEN
   
    BEGIN
     SELECT  'Y'
     INTO    l_change_req_exists
     FROM    dual
     WHERE EXISTS (SELECT  'po with change request'
  		           FROM    po_headers_all h
		           WHERE   h.segment1 = p_doc_number
		           AND     NVL(h.org_id, -99) = NVL(x_organization_id, -99)
		           AND     h.change_requested_by IN ('REQUESTER', 'SUPPLIER'));
    EXCEPTION 
       WHEN NO_DATA_FOUND THEN
         fnd_file.put_line (fnd_file.LOG,'No data  found for PO '||p_doc_number||' while checkig the open 
		                    change request for po and error details are :'||SQLERRM);
    l_change_req_exists:='N';
    END;
    
    IF (l_change_req_exists = 'Y') THEN
       fnd_file.put_line (fnd_file.LOG,'  ');
       fnd_file.put_line (fnd_file.LOG,'ATTENTION !!! There is an open change request against
	                      this PO '||p_doc_number||' You should respond to the notification for the same.');
       RETURN;
    END IF;

    OPEN cur_potoreset(p_doc_number, l_org_id);

    FETCH cur_potoreset INTO pos;
    IF cur_potoreset%NOTFOUND THEN 
       fnd_file.put_line (fnd_file.LOG,'No PO with PO Number '||p_doc_number ||
       			' exists in org '||TO_CHAR(x_organization_id) 
    			|| ' which requires to be reset');
       --RETURN;
    GOTO LAST_STEP;
    END IF;
    CLOSE cur_potoreset;
    
     x_progress := '030 checking enc action ';

 -- If there exists any open shipment with one of its distributions reserved, then
 -- 1. For a Standard PO, check whether the present Encumbrance amount on the distribution
 --    is correct or not. If its not correct do not reset the document.
 -- 2. For a Blanket PO (irrespective of Encumbrance enabled or not), reset the document.
 -- 3. For a Planned PO, always do not reset the document.
     l_disallow_script := 'N'; 
	l_sec:='Checking Wrong Encurmbrance Amount';
	BEGIN
     SELECT  'Y'
     INTO    l_disallow_script
     FROM    dual
     WHERE   EXISTS (SELECT 'Wrong Encumbrance Amount'
                     FROM   po_headers_all h,
                            po_lines_all l,
                            po_line_locations_all s,
                            po_distributions_all d
                     WHERE  s.line_location_id = d.line_location_id
                     AND    l.po_line_id = s.po_line_id
                     AND    h.po_header_id = d.po_header_id
                     AND    d.po_header_id = pos.po_header_id
                     AND    l.matching_basis = 'QUANTITY'
                     AND    Nvl(d.encumbered_flag, 'N') = 'Y'
                     AND    Nvl(s.cancel_flag, 'N') = 'N'
                     AND    Nvl(s.closed_code, 'OPEN') <> 'FINALLY CLOSED'
                     AND    Nvl(d.prevent_encumbrance_flag, 'N') = 'N'
                     AND    d.budget_account_id IS NOT NULL
                     AND    Nvl(s.shipment_type,'BLANKET') = 'STANDARD'
                     AND    (Round(Nvl(d.encumbered_amount, 0), 2) <> 
                             Round((s.price_override * d.quantity_ordered *
                                    Nvl(d.rate, 1) + Nvl(d.nonrecoverable_tax, 0) *
                                    Nvl(d.rate, 1) ), 2))
                     UNION
                 SELECT 'Wrong Encumbrance Amount'
                 FROM   po_headers_all h,
                        po_lines_all l,
                        po_line_locations_all s,
                        po_distributions_all d
                 WHERE  s.line_location_id = d.line_location_id
                        AND l.po_line_id = s.po_line_id
                        AND h.po_header_id = d.po_header_id
                        AND d.po_header_id = pos.po_header_id
                        AND l.matching_basis = 'AMOUNT'
                        AND Nvl(d.encumbered_flag, 'N') = 'Y'
                        AND Nvl(s.cancel_flag, 'N') = 'N'
                        AND Nvl(s.closed_code, 'OPEN') <> 'FINALLY CLOSED'
                        AND Nvl(d.prevent_encumbrance_flag, 'N') = 'N'
                        AND d.budget_account_id IS NOT NULL
                        AND Nvl(s.shipment_type,'BLANKET') = 'STANDARD'
                        AND (Round(Nvl(d.encumbered_amount, 0), 2) <>
                             Round((d.amount_ordered + Nvl(d.nonrecoverable_tax, 0) ) *
                                    Nvl(d.rate, 1),2))
                 UNION
                 SELECT 'Wrong Encumbrance Amount'
                 FROM   po_headers_all h,
                        po_lines_all l,
                        po_line_locations_all s,
                        po_distributions_all d
                 WHERE  s.line_location_id = d.line_location_id
                        AND l.po_line_id = s.po_line_id
                        AND h.po_header_id = d.po_header_id
                        AND d.po_header_id = pos.po_header_id
                        AND Nvl(d.encumbered_flag, 'N') = 'Y'
                        AND Nvl(d.prevent_encumbrance_flag, 'N') = 'N'
                        AND d.budget_account_id IS NOT NULL
                        AND Nvl(s.shipment_type,'BLANKET') = 'PLANNED'); 
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      fnd_file.put_line (fnd_file.LOG,'No data exists for encumbrance for PO '||p_doc_number||' disturbutions, Error is:'
	                     ||SQLERRM);
     l_disallow_script:='N';
    END;

    IF l_disallow_script = 'Y' THEN
       fnd_file.put_line (fnd_file.LOG,'This PO '||p_doc_number||' has at least one distribution with wrong Encumbrance amount.');
       fnd_file.put_line (fnd_file.LOG,'Hence this PO '||p_doc_number||' can not be reset.');
       RETURN;
    END IF;                
    
         fnd_file.put_line (fnd_file.LOG,'Processing '||pos.type_lookup_code
                               ||' PO Number: '
                               ||pos.segment1);
         fnd_file.put_line (fnd_file.LOG,'......................................');
    l_sec:='Checking Active workflow items';
	BEGIN 
     SELECT 'Y' 
     INTO   x_active_wf_exists
     FROM   wf_items wfi
     WHERE  wfi.item_type = pos.wf_item_type
   	 AND    wfi.item_key = pos.wf_item_key
	 AND    wfi.end_date IS NULL;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
    x_active_wf_exists := 'N';
    END;

    IF (x_active_wf_exists = 'Y') THEN
         fnd_file.put_line (fnd_file.LOG,'Aborting Workflow...');
         OPEN cur_wfstoabort(pos.wf_item_type,pos.wf_item_key);
        LOOP
         FETCH cur_wfstoabort INTO wf_rec;
	      IF cur_wfstoabort%NOTFOUND THEN
	       CLOSE cur_wfstoabort;
	       EXIT;
	      END IF;

	     IF (wf_rec.end_date IS NULL) THEN
	      BEGIN
	        WF_Engine.AbortProcess(wf_rec.item_type, wf_rec.item_key);
              EXCEPTION
                WHEN OTHERS THEN
                  fnd_file.put_line (fnd_file.LOG,' workflow not aborted :'
	     	        ||wf_rec.item_type ||'-'||wf_rec.item_key);
                     
          END;

	     END IF;
	    END LOOP;
    END IF;
 
      fnd_file.put_line (fnd_file.LOG,'Updating PO Status..'); 
      UPDATE po_headers_all
         SET authorization_status = decode(pos.approved_date, NULL, 'INCOMPLETE',
                                        'REQUIRES REAPPROVAL'),
          wf_item_type = NULL,
          wf_item_key = NULL,
	      approved_flag = decode(pos.approved_date, NULL, 'N', 'R') 
       WHERE po_header_id = pos.po_header_id;

      OPEN  cur_maxseq(pos.po_header_id, pos.type_lookup_code); 
      FETCH cur_maxseq into nullseq;
      CLOSE cur_maxseq;

      OPEN  cur_poaction(pos.po_header_id, pos.type_lookup_code);
      FETCH cur_poaction into submitseq;
      CLOSE cur_poaction;
      IF nullseq > submitseq THEN
        
	    IF NVL(l_p_del_actn_hist,'N') = 'N' THEN
   	       UPDATE  po_action_history
   	       SET     action_code = 'NO ACTION',
   	               action_date = TRUNC(SYSDATE),
   	               note = 'updated by reset script on '||TO_CHAR(TRUNC(SYSDATE))
           WHERE   object_id = pos.po_header_id
           AND     object_type_code = DECODE(pos.type_lookup_code,
	         				                'STANDARD','PO',
	    				                     'PLANNED', 'PO', --future plan to enhance for planned PO
	    				                     'PA'
											 )
            AND object_sub_type_code = pos.type_lookup_code
            AND sequence_num = nullseq
            AND action_code IS NULL;
        ELSE
         -- Stop deleting the open PO notifications
		 l_sec:='Stop deleting the open PO notifications';
		  BEGIN
		    SELECT NVL(COUNT(*),0)
			  INTO l_po_count
              FROM   po_headers_all pha
             WHERE   authorization_status IN ('IN PROCESS', 'PRE-APPROVED')
               AND   org_id = l_org_id
			   AND   pha.segment1=p_doc_number
               AND NOT EXISTS (SELECT  'open notifications'
                                 FROM  wf_item_activity_statuses wias,
                                       wf_notifications wfn
                                WHERE     wias.notification_id IS NOT NULL
                                  AND wias.notification_id = wfn.GROUP_ID
                                  AND wfn.status = 'OPEN'
                                  AND wias.item_type = 'POAPPRV'
                                  AND wias.item_key = pha.wf_item_key);
			EXCEPTION
             WHEN others THEN
              l_po_count:=0;
			END;
			  IF p_document_type='Purchase Order' AND p_del_Actn_Hist='Yes' AND l_po_count>0 THEN
	           DELETE po_action_history
	           WHERE object_id = pos.po_header_id
	             AND object_type_code = DECODE(pos.type_lookup_code,
	            				    'STANDARD','PO',
	    	   			        'PLANNED', 'PO', --future plan to enhance for planned PO
	    	   			        'PA')
	             AND object_sub_type_code = pos.type_lookup_code				    
	             AND sequence_num >= submitseq
	             AND sequence_num <= nullseq;
               END IF;
	    END IF;
	    
      END IF;

        fnd_file.put_line (fnd_file.LOG,'Done Approval Processing for '||pos.segment1);
      
        SELECT  NVL(purch_encumbrance_flag,'N')
          INTO  l_purch_encumbrance_flag
          FROM  financials_system_params_all fspa
         WHERE  NVL(fspa.org_id,-99) = NVL(x_organization_id,-99);
   
        IF (l_purch_encumbrance_flag='N') 
           -- bug 5015493 : Need to allow reset for blankets also
           OR (pos.type_lookup_code = 'BLANKET') THEN
        
           IF (pos.type_lookup_code = 'BLANKET') THEN
           	fnd_file.put_line (fnd_file.LOG,'document reset successfully for '||pos.segment1);
           	fnd_file.put_line (fnd_file.LOG,'If you are using Blanket encumbrance, Please ROLLBACK, else COMMIT');
           ELSE
           	fnd_file.put_line (fnd_file.LOG,'document reset successfully for '||pos.segment1);
           	fnd_file.put_line (fnd_file.LOG,'please COMMIT data');
           END IF;
           RETURN;
        END IF;
 
-- reserve action history stuff
-- check the action history and delete any reserve to submit actions if all the distributions
-- are now unencumbered, this should happen only if we are deleting the action history

    IF l_p_del_actn_hist = 'Y' THEN

      -- first get the last sequence and action code from action history
      BEGIN 
         SELECT  sequence_num, action_code
           INTO  l_res_seq, l_res_act
	       FROM  po_action_history pah
	      WHERE  pah.object_id = pos.po_header_id
            AND  pah.object_type_code = DECODE(pos.type_lookup_code,
       				    'STANDARD','PO',
	   			        'PLANNED', 'PO', --future plan to enhance for planned PO
	   			        'PA')
            AND  pah.object_sub_type_code = pos.type_lookup_code
	        AND  sequence_num IN (SELECT  MAX(sequence_num)
	     			                FROM  po_action_history pah1
	   			                   WHERE  pah1.object_id = pah.object_id
             			             AND  pah1.object_type_code =pah.object_type_code
             		                 AND  pah1.object_sub_type_code =pah.object_sub_type_code);
      EXCEPTION
      WHEN TOO_MANY_ROWS THEN
        fnd_file.put_line (fnd_file.LOG,'action history needs to be corrected separately for '||pos.segment1);
      WHEN NO_DATA_FOUND THEN
        NULL;
      END;

   -- now if the last action is reserve get the last submit action sequence
    l_sec:='Checking if the last action is reserve get the last submit action sequence';
    IF (l_res_act = 'RESERVE') THEN
     BEGIN
        SELECT  max(sequence_num)
          INTO  l_sub_res_seq
          FROM  po_action_history pah
         WHERE  action_code = 'SUBMIT'
           AND  pah.object_id = pos.po_header_id
           AND  pah.object_type_code = decode(pos.type_lookup_code,
      				                          'STANDARD','PO',
	  			                              'PLANNED', 'PO', --future plan to enhance for planned PO
	  			                              'PA'
											  )
           AND  pah.object_sub_type_code = pos.type_lookup_code;
     EXCEPTION
     WHEN NO_DATA_FOUND THEN
       NULL;	   
     END;

      -- check if we need to delete the action history, ie. if all the distbributions
      -- are unreserved

    IF ((l_sub_res_seq IS NOT NULL ) AND (l_res_seq > l_sub_res_seq)) THEN

	 BEGIN
         SELECT  'Y'
           INTO  l_del_res_hist
           FROM  dual
           WHERE  NOT EXISTS (SELECT 'encumbered dist'
     			                FROM po_distributions_all pod
			                   WHERE pod.po_header_id = pos.po_header_id
			                     AND NVL(pod.encumbered_flag,'N') = 'Y'
			                     AND NVL(pod.prevent_encumbrance_flag,'N')='N');
      EXCEPTION
	  WHEN NO_DATA_FOUND THEN
	    l_del_res_hist := 'N';
      END;

    IF l_del_res_hist = 'Y' THEN

	    fnd_file.put_line (fnd_file.LOG,'deleting reservation action history for '||pos.segment1);

            DELETE po_action_history pah
             WHERE pah.object_id = pos.po_header_id
               AND pah.object_type_code = DECODE(pos.type_lookup_code,
       				                             'STANDARD','PO',
   				                                 'PLANNED', 'PO', --future plan to enhance for planned PO
   				                                 'PA'
												 )
                AND pah.object_sub_type_code = pos.type_lookup_code
                AND sequence_num >= l_sub_res_seq
                AND sequence_num <= l_res_seq;
    END IF;
       
    END IF; -- l_res_seq > l_sub_res_seq
	
   END IF;

  END IF;
  
  ELSE 
      fnd_file.put_line (fnd_file.LOG,'  ');
      fnd_file.put_line (fnd_file.LOG,'An Open notification exists for this document '||p_doc_number ||'
                         you may want to use the notification to process this document');
						 
   END IF;  -- x_open_notif_exist FOR PO Condition
END;
	<<LAST_STEP>>
    NULL;

END IF;
EXCEPTION
  WHEN OTHERS THEN  
       xxcus_error_pkg.xxcus_error_main_api (
          p_called_from         => l_err_callfrom
         ,p_calling             => l_sec
         ,p_ora_error_msg       => SQLERRM
         ,p_error_desc          => l_message
         ,p_distribution_list   => l_distro_list
         ,p_module              => 'XXWC');

END update_po_reqs_status; 
END xxwc_po_reset_docs_pkg;
/