BEGIN 
  FOR I IN (select L.AGREEMENT_LINE_ID, a.LIST_HEADER_ID from xxwc.XXWC_OM_CONTRACT_PRICING_LINES L, APPS.qp_list_lines QL, APPS.qp_list_headers_all A,
xxwc.XXWC_OM_CONTRACT_PRICING_HDR c
WHERE A.ATTRIBUTE14 = L.AGREEMENT_ID
AND C.AGREEMENT_ID = A.ATTRIBUTE14
AND QL.ATTRIBUTE2 = L.AGREEMENT_LINE_ID
AND QL.ATTRIBUTE5 = 'Segmented Price'
and c.agreement_type = 'MTX'
and l.list_header_id = a.LIST_HEADER_ID
) LOOP
update APPS.qp_list_lines QL set attribute2 = null where ql.list_header_id = i.list_header_id;
DELETE FROM xxwc.XXWC_OM_CONTRACT_PRICING_LINES L WHERE L.AGREEMENT_LINE_ID = I.AGREEMENT_LINE_ID;
DELETE FROM XXWC.XXWC_OM_CSP_LINES_ARCHIVE  L WHERE L.AGREEMENT_LINE_ID = I.AGREEMENT_LINE_ID;
COMMIT;
END LOOP;
END;
/