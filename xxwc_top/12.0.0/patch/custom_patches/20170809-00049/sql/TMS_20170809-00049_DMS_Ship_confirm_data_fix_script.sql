/******************************************************************************
  $Header TMS_20170809-00049_DMS_Ship_confirm_data_fix_script.sql $
  Module Name:Data Fix script for 20170809-00049

  PURPOSE: Data fix script for 20170809-00049 Ship Confirm Data Fix...

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        10-AUG-2017  Pattabhi Avula       TMS#20170809-00049

*******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

DECLARE

ln_count NUMBER:=0;

BEGIN
   
  DBMS_OUTPUT.put_line ('Before Insert');

  /*
  SELECT COUNT(xx.id)
  INTO   ln_count
  FROM   apps.oe_order_headers_all oh
  ,      apps.oe_order_lines_all ol
  ,      apps.mtl_parameters od
  ,      xxwc.XXWC_SIGNATURE_CAPTURE_arc_TBL xx
  WHERE 1=1
  AND   oh.header_id=ol.header_id
  AND   oh.ship_from_org_id=od.organization_id
  AND   oh.header_id =xx.id
  AND   oh.header_id=xx.id
  AND   xx.delivery_id is not null
  AND   xx.signature_name!='***CANCEL***'
  AND   xx.SHIP_CONFIRM_STATUS is null
  AND   ol.flow_status_code not in ('CLOSED','CANCELLED','INVOICE_HOLD')
  AND   trunc (xx.creation_date) ='07-AUG-2017';
  */
  
  --Query to count the no of records...
  SELECT COUNT(xx.id)
  INTO   ln_count
  FROM   APPS.OE_ORDER_HEADERS_ALL OH
  ,      APPS.MTL_PARAMETERS OD
  ,      xxwc.XXWC_SIGNATURE_CAPTURE_arc_TBL xx
  WHERE 1=1
  AND   oh.ship_from_org_id=od.organization_id
  AND   OH.HEADER_ID =XX.ID
  AND   xx.delivery_id is not null
  AND   xx.signature_name!='***CANCEL***'
  AND   xx.SHIP_CONFIRM_STATUS is null
  AND TRUNC (XX.CREATION_DATE) ='10-AUG-2017';
  /*AND EXISTS
  (SELECT 1
   FROM  APPS.OE_ORDER_LINES_ALL OL
   WHERE 1=1
   AND   OH.HEADER_ID=OL.HEADER_ID
   AND   ol.flow_status_code not in ('CLOSED','CANCELLED','INVOICE_HOLD'));  */

  DBMS_OUTPUT.put_line ('Total Records to be Inserted-' ||ln_count);  
 
  INSERT INTO XXWC.XXWC_SIGNATURE_CAPTURE_TBL (ID                          
                                              ,SIGNATURE_IMAGE_CLOB        
                                              ,SIGNATURE_IMAGE_BLOB        
                                              ,CREATION_DATE               
                                              ,CREATED_BY                  
                                              ,HEADER_ID                   
                                              ,SIGNATURE_NAME              
                                              ,REQUEST_ID                  
                                              ,DELIVERY_ID                 
                                              ,RIGHT_FAX                   
                                              ,FAX                         
                                              ,FAX_COMMENT                 
                                              ,GROUP_ID                    
                                              ,SHIP_CONFIRM_STATUS         
                                              ,SHIP_CONFIRM_EXCEPTION      
                                              ,LAST_UPDATE_DATE            
                                              ,LAST_UPDATED_BY 
                                              )  
  SELECT XX.ID                          
        ,XX.SIGNATURE_IMAGE_CLOB        
        ,XX.SIGNATURE_IMAGE_BLOB        
        ,XX.CREATION_DATE               
        ,XX.CREATED_BY                  
        ,XX.HEADER_ID                   
        ,XX.SIGNATURE_NAME              
        ,XX.REQUEST_ID                  
        ,XX.DELIVERY_ID                 
        ,XX.RIGHT_FAX                   
        ,XX.FAX                         
        ,XX.FAX_COMMENT                 
        ,XX.GROUP_ID                    
        ,XX.SHIP_CONFIRM_STATUS         
        ,XX.SHIP_CONFIRM_EXCEPTION      
        ,XX.LAST_UPDATE_DATE            
        ,XX.LAST_UPDATED_BY 
   FROM  APPS.OE_ORDER_HEADERS_ALL OH
  ,      APPS.MTL_PARAMETERS OD
  ,      xxwc.XXWC_SIGNATURE_CAPTURE_arc_TBL xx
  WHERE  1=1
  AND    oh.ship_from_org_id=od.organization_id
  AND    OH.HEADER_ID =XX.ID
  AND    xx.delivery_id is not null
  AND    xx.signature_name!='***CANCEL***'
  AND    xx.SHIP_CONFIRM_STATUS is null
  AND TRUNC (XX.CREATION_DATE) ='10-AUG-2017';
 /* AND EXISTS
  (SELECT 1
   FROM  APPS.OE_ORDER_LINES_ALL OL
   WHERE 1=1
   AND   OH.HEADER_ID=OL.HEADER_ID
   AND   ol.flow_status_code not in ('CLOSED','CANCELLED','INVOICE_HOLD')); */

  DBMS_OUTPUT.put_line ('Records Inserted-' ||SQL%ROWCOUNT);
  
  COMMIT;
  
EXCEPTION
WHEN others THEN

  DBMS_OUTPUT.put_line ('Unable to Insert record ' || SQLERRM);

END;
/