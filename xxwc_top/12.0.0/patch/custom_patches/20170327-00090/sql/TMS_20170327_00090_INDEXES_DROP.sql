/*****************************************************************************************************************************************************
  $Header TMS_20170327-00090 Dropping INDEXES

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        27-Mar-2017 P.Vamshidhar            TMS#20170327-00090 - Drop indexes for forecast upload to prevent “Index lock contention” issue.
*****************************************************************************************************************************************************/
set serverout on

 BEGIN
 DBMS_OUTPUT.PUT_LINE('MRP.MRP_FORECAST_DATES_U1 Index Dropping Begin');
 Execute Immediate 'DROP INDEX MRP.MRP_FORECAST_DATES_U1';
 DBMS_OUTPUT.PUT_LINE('MRP.MRP_FORECAST_DATES_U1 Index Dropping End');
 DBMS_OUTPUT.PUT_LINE('MRP.MRP_FORECAST_DATES_N4 Index Dropping Begin');
 Execute Immediate 'DROP INDEX MRP.MRP_FORECAST_DATES_N4'; 
 DBMS_OUTPUT.PUT_LINE('MRP.MRP_FORECAST_DATES_N4 Index Dropping End');
 END;
/
