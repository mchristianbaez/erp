CREATE OR REPLACE PACKAGE BODY APPS.xxwc_om_dms_pkg AS

  g_err_callfrom VARCHAR2(75) DEFAULT 'XXWC_OM_DMS_PKG';
  g_distro_list  VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  g_host         VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
  g_hostport     VARCHAR2(20) := '25';

  /*********************************************************************************
  -- Package XXWC_OM_DMS_PKG
  -- *******************************************************************************
  --
  -- PURPOSE: Package is for DMS "Digital Management System" interface to MyLogistics
  --          Sales Orders to be sent for delivery/routing in MyLogistics.
  --          MyLogistics will send a file that will allow standard orders to be
  --          ship confirmed.
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.0     20-May-2014   Kathy Poling    Created this package. TMS 20140606-00082
  -- 1.1     06-Nov-2014   Kathy Poling    Changes for rentals and cancelled orders in 
                                           procedure gen_file. Change for procedure
                                           ship_confirm TMS 20141112-00097
   -- 1.2    20-Nov-2014   Kathy Poling    Added procedure ship_confirm_sig_capt and
                                           function line_status.  Fix in gen_file for
                                           cancelled orders.  Fix ship_confirm added 
                                           route_number.  TMS 20140606-00082  
  -- 1.3     16-Jan-2015   Kathy Poling    TMS 20150118-00002 to address the date issue 
                                           in the program for scheduling the concurrent
                                           request in the procedure ship_confirm_sig_capt 
  -- 1.4     23-Jan-2015   Kathy Poling    TMS 20150123-00064 change procedure
                                           ship_confirm_sig_capt
  -- 1.5     20-Feb-2015   Gopi Damuluri   TMS# 20150128-00043 XXWC DMS File creation modification
  -- 1.6     25-Feb-2015   Pattabhi Avula  TMS# 20150223-00248 Fix New Line characters on DMS File.
  -- 1.7     03-Mar-2015   Mahesh Kudle    TMS#20150224-00150 Added scipt to run XXWC DMS Copy CFD to DocLink
  -- 1.8     12-Mar-2015   Gopi Damuluri   TMS# 20150220-00127 - Handle logic of XXWC_OE_ORDER_LINES_BRU2_TRG in this Package.
  -- 1.9     01-Apr-2015   Gopi Damuluri   TMS# 20150401-00126 - Trim Shipping Instructions to 250 Characters.
  -- 1.10    04-Apr-2015   Gopi Damuluri   TMS# 20150227-00035 - Peformance Tuning of SHIP_CONFIRM_SIG_CAPT process
                                           Added new procedure - UPDATE_OFD_DELIVERY_RUN
                                           TMS# 20150226-00083 - Performance issue for XXWC DMS Ship Confirm Delivered Orders
                                           TMS# 20150226-00084 - DMS - PERF fix for XXWC DMS Ship Confirm Signature Captured Orders
  -- 1.11    12-May-2015   Gopi Damuluri   TMS# 20150512-00161 - DMS bug fix related to payment hold
  -- 1.12    20-Jul-2015   Pattabhi Avula  TMS# 20150519-00037 - DMS -- added the source_type_code<>'EXTERNAL'
                                           condition to GEN_FILE procedure
  -- 1.14    27-Jul-2015   Pattabhi Avula  TMS#20150706-00024 -- Removed Special Characters in GEN_FILE procedure
  *******************************************************************************/

  /********************************************************************************
  -- PROCEDURE: GEN_FILE
  --
  -- PURPOSE: Package for is to generate file to send to MyLogistics
  --
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.0     20-May-2014   Kathy Poling    Created this package.  TMS 20140606-00082
  -- 1.1     04-Nov-2014   Kathy Poling    Changes for rental orders and cancelled orders
  --                                       TMS 20141112-00097
  -- 1.2     20-Nov-2014   Kathy Poling    Fix for cancelled orders to not send if
  --                                       other lines in that delivery have a delivered
  --                                       status
  -- 1.5     20-Feb-2015   Gopi Damuluri   TMS# 20150128-00043 XXWC DMS File creation modification
  -- 1.6     25-Feb-2015   Pattabhi Avula  TMS# 20150223-00248 Fix New Line characters on DMS File. 
  -- 1.8     12-Mar-2015   Gopi Damuluri   TMS# 20150220-00127 - Handle logic of XXWC_OE_ORDER_LINES_BRU2_TRG in this Package.
  -- 1.9     01-Apr-2015   Gopi Damuluri   TMS# 20150401-00126 - Trim Shipping Instructions to 250 Characters.
  -- 1.10    20-Jul-2015   Pattabhi Avula  TMS# 20150519-00037 - DMS - Ignore Source = 'External' for the 
                                           XXWC DMS File Creation program -- added the external condition
  -- 1.11    27-Jul-2015   Pattabhi Avula  TMS#20150706-00024 -- Removed Special Characters in GEN_FILE procedure
  *******************************************************************************/
  PROCEDURE gen_file(p_errbuf      OUT VARCHAR2
                    ,p_retcode     OUT VARCHAR2
                    ,p_order_type  IN VARCHAR2
                    ,p_dummy       IN VARCHAR2
                    ,p_header_id   IN NUMBER
                    ,p_delivery_id IN NUMBER) IS
  
    l_phase  VARCHAR2(240);
    l_status VARCHAR2(240);
  
    l_err_msg          CLOB;
    l_sec              VARCHAR2(110) DEFAULT 'START';
    l_user_id          fnd_user.user_id%TYPE;
    l_procedure        VARCHAR2(50) := 'GEN_FILE';
    l_application_name VARCHAR2(30) := 'XXWC';
    l_sequence         NUMBER;
    l_run_date         DATE;
  
    l_filename             VARCHAR2(100);
    l_file_dir             VARCHAR2(100) := 'XXWC_OM_DMS_OB_DIR';
    l_file_handle          utl_file.file_type;
    l_file_name_temp       VARCHAR2(100);
    l_base_file_exists     BOOLEAN;
    l_base_file_length     NUMBER;
    l_base_file_block_size BINARY_INTEGER;
  
    l_location          VARCHAR2(40);
    l_phone             VARCHAR2(40);
    l_areacode          VARCHAR2(10);
    l_bill_add1         VARCHAR2(240);
    l_bill_add2         VARCHAR2(240);
    l_bill_add3         VARCHAR2(240);
    l_bill_add5         VARCHAR2(180);
    l_bill_areacode     VARCHAR2(10);
    l_bill_phone        VARCHAR2(50);
    l_bill_site         VARCHAR2(30);
    l_ship_location     VARCHAR2(40);
    l_ship_add1         VARCHAR2(240);
    l_ship_add2         VARCHAR2(240);
    l_ship_add3         VARCHAR2(240);
    l_ship_add5         VARCHAR2(180);
    l_ship_areacode     VARCHAR2(10);
    l_ship_phone        VARCHAR2(50);
    l_ship_site         VARCHAR2(30);
    l_job_site_areacode VARCHAR2(10);
    l_job_site_phone    VARCHAR2(40);
    l_job_site_contact  VARCHAR2(120);
    l_deliver_date      VARCHAR2(50);
    l_item_type         VARCHAR2(55);
    l_cont_name         VARCHAR2(360);
    l_branch            VARCHAR2(60);
    l_ship_qty          NUMBER;
    l_dms_org           VARCHAR2(1);
    l_bill_ret_code     NUMBER;
    l_bill_ret_msg      VARCHAR2(240);
    l_ship_ret_code     NUMBER;
    l_ship_ret_msg      VARCHAR2(240);
    l_order_number      NUMBER;
    l_org_id            NUMBER := mo_global.get_current_org_id;
  
  BEGIN
  
    BEGIN
      --if adding a new org will need to insert a start point into this table
      SELECT MAX(creation_date) INTO l_run_date FROM xxwc_om_dms_tbl;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find last run date in xxwc_om_DMS_tbl';
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
  
    fnd_file.put_line(fnd_file.log
                     ,'Beginning EBS To DMS File Generation ');
    fnd_file.put_line(fnd_file.log
                     ,'========================================================');
    fnd_file.put_line(fnd_file.log, '  ');
    --Initialize the Out Parameter
    p_errbuf  := '';
    p_retcode := '0';
  
    --Derive the file name 
    SELECT 'WC_OM_' || to_char(systimestamp, 'MMDDYYYY_HH_MI_SSSSS') ||
           '.csv'
      INTO l_filename
      FROM dual;
  
    --Set the file name and open the file
    l_file_name_temp := 'TEMP_' || l_filename;
  
    fnd_file.put_line(fnd_file.log, 'File Name:  ' || l_filename);
  
    --Open the file handler
    l_file_handle := utl_file.fopen(location  => l_file_dir
                                   ,filename  => l_file_name_temp
                                   ,open_mode => 'w');
    fnd_file.put_line(fnd_file.log, ' Writing to the file ... ');
  
    --only populate audit table when pulling all orders to be sent to MyLogistics
    IF p_header_id IS NULL
    THEN
    
      SELECT xxwc_om_dms_seq.nextval INTO l_sequence FROM dual;
    
      --audit table and used to get only orders updated after the last pull
      INSERT INTO xxwc_om_dms_tbl
        (file_id
        ,file_name
        ,request_id
        ,status
        ,created_by
        ,creation_date
        ,last_updated_by
        ,last_update_date)
      VALUES
        (l_sequence
        ,l_filename
        ,fnd_global.conc_request_id
        ,'Submitted'
        ,fnd_global.user_id()
        ,SYSDATE
        ,fnd_global.user_id()
        ,SYSDATE);
    
    ELSE
    
      SELECT xxwc_om_dms_brnch_seq.nextval INTO l_sequence FROM dual;
    
      BEGIN
        SELECT order_number
          INTO l_order_number
          FROM oe_order_headers
         WHERE header_id = p_header_id;
      EXCEPTION
        WHEN no_data_found THEN
          l_order_number := NULL;
      END;
    
      INSERT INTO xxwc_om_dms_brnch_file_tbl
        (file_id
        ,order_number
        ,header_id
        ,delivery_id
        ,file_name
        ,request_id
        ,status
        ,created_by
        ,creation_date
        ,last_updated_by
        ,last_update_date)
      VALUES
        (l_sequence
        ,l_order_number
        ,p_header_id
        ,p_delivery_id
        ,l_filename
        ,fnd_global.conc_request_id
        ,'Submitted'
        ,fnd_global.user_id()
        ,SYSDATE
        ,fnd_global.user_id()
        ,SYSDATE);
    
    END IF;
  
      -- Version# 1.5 > Start
      BEGIN
        INSERT INTO xxwc.xxwc_om_dms_change_gtt_tbl
        SELECT * 
          FROM xxwc_om_dms_change_tbl chng 
         WHERE TRUNC(chng.last_update_date) = TRUNC(SYSDATE)
           AND NOT EXISTS (SELECT '1' 
                             FROM xxwc_om_dms_change_archive_tbl arc2 
                            WHERE arc2.header_id = chng.header_id 
                              AND NVL(arc2.delivery_id, -1) = NVL(chng.delivery_id, -1));
      EXCEPTION
      WHEN OTHERS THEN
        NULL;
      END;
    -- Version# 1.5 < End
  
    --table xxwc.xxwc_om_DMS_change_tbl populated from trigger on xxwc.xxwc_wsh_shipping_stg.  
    --the shipping table is not updated with last update date and if a line is cancelled or clear all
    --on the delivery need to know what should be sent to MyLogistics
  
    FOR cur_order_rec IN (SELECT DISTINCT header_id, delivery_id
                            FROM xxwc.xxwc_om_dms_change_gtt_tbl ct -- Version# 1.5
                           WHERE 1 = 1
                             -- AND last_update_date > l_run_date -- Version# 1.5
                             AND p_header_id IS NULL
                             AND p_delivery_id IS NULL
                             AND NOT EXISTS
                           (SELECT header_id, delivery_id
                                    FROM xxwc_om_dms_brnch_file_tbl bft
                                   WHERE bft.header_id = ct.header_id
                                     AND bft.delivery_id = ct.delivery_id)
--                             -- Version# 1.5 > Start
--                          UNION
--                          --Rentals and Repair Orders
--                          SELECT DISTINCT ol.header_id, NULL delivery_id
--                            FROM oe_order_lines ol, oe_order_headers oh
--                           WHERE ol.header_id = oh.header_id
--                             AND oh.order_type_id NOT IN
--                                 (SELECT /*+ RESULT_CACHE */
--                                   lookup_code
--                                    FROM fnd_lookup_values
--                                   WHERE 1 = 1
--                                     AND lookup_type =
--                                         'XXWC_OM_DMS_ORDERTYPE_GEN_FILE'
--                                     AND enabled_flag = 'Y'
--                                     AND SYSDATE BETWEEN start_date_active AND
--                                         nvl(end_date_active, SYSDATE + 1)) --STANDARD ORDER, INTERNAL ORDER, COUNTER ORDER
--                             AND ol.last_update_date > l_run_date
--                             AND oh.flow_status_code = 'BOOKED'
--                                --Version 1.1 to fix Rental Orders 11/4/14
--                             AND ol.line_type_id NOT IN (1021, 1007, 1008) --RETURN WITH RECEIPT, WC BILL ONLY, CREDIT ONLY
--                             AND ol.inventory_item_id <> 2931193 --Rental Charge    
--                             AND ol.shipping_method_code IN
--                                 (SELECT /*+ RESULT_CACHE */
--                                   meaning
--                                    FROM apps.fnd_lookup_values
--                                   WHERE 1 = 1
--                                     AND lookup_type =
--                                         'XXWC_DMS_SHIPPING_METHOD_CODE'
--                                     AND enabled_flag = 'Y'
--                                     AND SYSDATE BETWEEN start_date_active AND
--                                         nvl(end_date_active, SYSDATE + 1))
--                             AND ol.open_flag = 'Y'
--                                --AND ol.attribute8 IS NULL
--                                --end 11/4/14      
--                             AND p_header_id IS NULL
--                        Version# 1.5 > End
                          UNION
                          --added for branch to be able to send one order on demand
                          SELECT DISTINCT ol.header_id, NULL delivery_id
                            FROM oe_order_lines ol, oe_order_headers oh
                           WHERE ol.header_id = oh.header_id
						    AND ol.source_type_code='INTERNAL'   -- Ver# 1.10 TMS# 20150519-00037
                             AND oh.order_type_id NOT IN
                                 (SELECT /*+ RESULT_CACHE */
                                   lookup_code
                                    FROM fnd_lookup_values
                                   WHERE 1 = 1
                                     AND lookup_type =
                                         'XXWC_OM_DMS_ORDERTYPE_GEN_FILE'
                                     AND enabled_flag = 'Y'
                                     AND SYSDATE BETWEEN start_date_active AND
                                         nvl(end_date_active, SYSDATE + 1)) --STANDARD ORDER, INTERNAL ORDER, COUNTER ORDER                           
                             AND oh.flow_status_code = 'BOOKED'
                             AND oh.header_id = p_header_id
                             AND p_delivery_id IS NULL
                          UNION
                          SELECT DISTINCT header_id, delivery_id
                            FROM xxwc.xxwc_om_dms_change_gtt_tbl -- Version# 1.5
                           WHERE header_id = p_header_id
                             AND delivery_id = p_delivery_id)
    LOOP
    
      l_bill_add1         := NULL;
      l_bill_add2         := NULL;
      l_bill_add3         := NULL;
      l_bill_add5         := NULL;
      l_bill_areacode     := NULL;
      l_bill_phone        := NULL;
      l_bill_site         := NULL;
      l_ship_add1         := NULL;
      l_ship_add2         := NULL;
      l_ship_add3         := NULL;
      l_ship_add5         := NULL;
      l_ship_areacode     := NULL;
      l_ship_phone        := NULL;
      l_ship_site         := NULL;
      l_job_site_areacode := NULL;
      l_job_site_phone    := NULL;
      l_job_site_contact  := NULL;
      l_deliver_date      := NULL;
      l_item_type         := NULL;
      l_cont_name         := NULL;
      l_branch            := NULL;
      l_ship_qty          := NULL;
    
      FOR cur_line IN ( --Standard/Internal Orders with delivery ID
                       SELECT oha.header_id
                              ,oha.order_number
                              ,stg.delivery_id
                              ,hp.party_name customer_name
                              ,hca.account_number
                              ,to_char(oha.ordered_date, 'MM/DD/YYYY') ordered_date
                              ,oha.ship_to_org_id
                              ,ola.ship_from_org_id
                              ,oha.invoice_to_org_id
                              ,oha.ship_to_contact_id
                              ,oha.sold_to_contact_id
                              ,oha.payment_term_id
                              ,term.name termname
                              ,oha.cust_po_number
                              ,to_char(oha.request_date, 'MM/DD/YYYY') request_date
                              ,oha.salesrep_id
                              ,ola.line_id
                              ,ola.line_number
                              ,ola.ordered_item
                              ,ola.inventory_item_id
                              ,ola.order_quantity_uom
                               --,nvl(ola.ordered_quantity, 0) - nvl(ola.cancelled_quantity, 0) ord_quantity  Version 1.1
                              ,nvl(ola.ordered_quantity, 0) ord_quantity
                              ,nvl(stg.transaction_qty, 0) shipped_quantity
                              ,ola.shipping_method_code
                              ,ola.packing_instructions
                              ,ola.freight_carrier_code
                              ,ola.user_item_description
                              ,ship_ps.location_id
                              ,msib.unit_weight
                              ,msib.item_type
                              ,msib.description
                              ,ott.name order_type
                               --,NULL status   11/17/14  Version 1.1
                              ,CASE
                                 WHEN ola.open_flag = 'N' THEN
                                  'D'
                                 ELSE
                                  NULL
                               END status
                              ,ola.attribute14
                              ,TRIM(regexp_replace(asciistr(regexp_replace(oha.shipping_instructions
                                                                          ,'[[:cntrl:]]'
                                                                          ,NULL))
                                                  ,'\\[[:xdigit:]]{4}'
                                                  ,'')) shipping_instructions
                              ,ola.line_type_id --Version 1.1 11/18/14                 
                         FROM xxwc_wsh_shipping_stg       stg
                              ,oe_order_headers            oha
                              ,oe_order_lines              ola
                              ,ont.oe_transaction_types_tl ott
                              ,ar.ra_terms_tl              term
                              ,ar.hz_parties               hp
                              ,ar.hz_cust_accounts         hca
                              ,hz_cust_site_uses_all       ship_su
                              ,hz_cust_acct_sites_all      ship_cas
                              ,hz_party_sites              ship_ps
                              ,inv.mtl_parameters          ship_from_org
                              ,inv.mtl_system_items_b      msib
                        WHERE (stg.status IN
                              (SELECT /*+ RESULT_CACHE */
                                 meaning
                                  FROM fnd_lookup_values
                                 WHERE 1 = 1
                                   AND lookup_type =
                                       'XXWC_DMS_DELIVERY_STATUS'
                                   AND enabled_flag = 'Y'
                                   AND tag = 'N' -- Version# 1.5
                                   AND SYSDATE BETWEEN start_date_active AND
                                       nvl(end_date_active, SYSDATE + 1)) OR
                              stg.status = 'CANCELLED') --Version 1.1  11/17/2014   
                          AND NOT EXISTS --Version 1.2  11/26/2014
                        (SELECT '1'
                                 FROM xxwc.xxwc_wsh_shipping_stg ws
                                WHERE header_id = cur_order_rec.header_id
                                  AND ws.delivery_id =
                                      cur_order_rec.delivery_id
                                  AND ws.status IN ('DELIVERED'))
                          AND oha.order_type_id = ott.transaction_type_id
                          AND stg.header_id = oha.header_id
                          AND oha.flow_status_code IN ('BOOKED', 'CANCELLED') --Version 1.1 added cancelled 11/17/2014
                          AND oha.header_id = ola.header_id
                          AND stg.line_id = ola.line_id
                             --AND ola.open_flag = 'Y'   11/17/14  Version 1.1
                          AND ola.shipping_method_code IN
                              (SELECT /*+ RESULT_CACHE */
                                meaning
                                 FROM fnd_lookup_values
                                WHERE 1 = 1
                                  AND lookup_type =
                                      'XXWC_DMS_SHIPPING_METHOD_CODE'
                                  AND enabled_flag = 'Y'
                                  AND SYSDATE BETWEEN start_date_active AND
                                      nvl(end_date_active, SYSDATE + 1))
                          AND oha.sold_to_org_id = hca.cust_account_id
                          AND hca.party_id = hp.party_id
                          AND oha.payment_term_id = term.term_id(+)
                          AND oha.ship_to_org_id = ship_su.site_use_id(+)
                          AND ship_su.cust_acct_site_id =
                              ship_cas.cust_acct_site_id(+)
                          AND ship_cas.party_site_id =
                              ship_ps.party_site_id(+)
                          AND ola.ship_from_org_id =
                              ship_from_org.organization_id(+)
                          AND msib.inventory_item_id = ola.inventory_item_id
                          AND msib.organization_id =
                              ship_from_org.organization_id
                          AND msib.inventory_item_id = ola.inventory_item_id
                          AND oha.header_id = cur_order_rec.header_id
                          AND stg.delivery_id = cur_order_rec.delivery_id
						  AND ola.source_type_code='INTERNAL'  -- Ver# 1.10 TMS# 20150519-00037
                       UNION
                       --to get the clear deliveries lines to send as deleted
                       SELECT oha.header_id
                             ,oha.order_number
                             ,cur_order_rec.delivery_id
                             ,hp.party_name customer_name
                             ,hca.account_number
                             ,to_char(oha.ordered_date, 'MM/DD/YYYY') ordered_date
                             ,oha.ship_to_org_id
                             ,ola.ship_from_org_id
                             ,oha.invoice_to_org_id
                             ,oha.ship_to_contact_id
                             ,oha.sold_to_contact_id
                             ,oha.payment_term_id
                             ,term.name termname
                             ,oha.cust_po_number
                             ,to_char(oha.request_date, 'MM/DD/YYYY') request_date
                             ,oha.salesrep_id
                             ,ola.line_id
                             ,ola.line_number
                             ,ola.ordered_item
                             ,ola.inventory_item_id
                             ,ola.order_quantity_uom
                              --,nvl(ola.ordered_quantity, 0) - nvl(ola.cancelled_quantity, 0) ord_quantity  Version 1.1
                             ,nvl(ola.ordered_quantity, 0) ord_quantity
                             ,0 shipped_quantity
                             ,ola.shipping_method_code
                             ,ola.packing_instructions
                             ,ola.freight_carrier_code
                             ,ola.user_item_description
                             ,ship_ps.location_id
                             ,msib.unit_weight
                             ,msib.item_type
                             ,msib.description
                             ,ott.name order_type
                             ,'D' status
                             ,ola.attribute14
                             ,TRIM(regexp_replace(asciistr(regexp_replace(oha.shipping_instructions
                                                                         ,'[[:cntrl:]]'
                                                                         ,NULL))
                                                 ,'\\[[:xdigit:]]{4}'
                                                 ,'')) shipping_instructions
                             ,ola.line_type_id --Version 1.1 11/18/14                     
                         FROM oe_order_headers            oha
                             ,oe_order_lines              ola
                             ,ont.oe_transaction_types_tl ott
                             ,ar.ra_terms_tl              term
                             ,ar.hz_parties               hp
                             ,ar.hz_cust_accounts         hca
                             ,hz_cust_site_uses_all       ship_su
                             ,hz_cust_acct_sites_all      ship_cas
                             ,hz_party_sites              ship_ps
                             ,inv.mtl_parameters          ship_from_org
                             ,inv.mtl_system_items_b      msib
                             ,xxwc.xxwc_om_dms_change_gtt_tbl      xc -- Version# 1.5
                        WHERE ola.user_item_description IN
                              (SELECT /*+ RESULT_CACHE */
                                meaning
                                 FROM fnd_lookup_values
                                WHERE 1 = 1
                                  AND lookup_type = 'XXWC_DMS_DELIVERY_STATUS'
                                  AND tag = 'N' -- Version# 1.5
                                  AND enabled_flag = 'Y'
                                  AND SYSDATE BETWEEN start_date_active AND
                                      nvl(end_date_active, SYSDATE + 1))
                          AND oha.order_type_id = ott.transaction_type_id
                          AND oha.flow_status_code = 'BOOKED'
                          AND oha.header_id = ola.header_id
                          AND ola.open_flag = 'Y'
                          AND ola.shipping_method_code IN
                              (SELECT /*+ RESULT_CACHE */
                                meaning
                                 FROM fnd_lookup_values
                                WHERE 1 = 1
                                  AND lookup_type =
                                      'XXWC_DMS_SHIPPING_METHOD_CODE'
                                  AND enabled_flag = 'Y'
                                  AND SYSDATE BETWEEN start_date_active AND
                                      nvl(end_date_active, SYSDATE + 1))
                          AND oha.sold_to_org_id = hca.cust_account_id
                          AND hca.party_id = hp.party_id
                          AND oha.payment_term_id = term.term_id(+)
                          AND oha.ship_to_org_id = ship_su.site_use_id(+)
                          AND ship_su.cust_acct_site_id =
                              ship_cas.cust_acct_site_id(+)
                          AND ship_cas.party_site_id =
                              ship_ps.party_site_id(+)
                          AND ola.ship_from_org_id =
                              ship_from_org.organization_id(+)
                          AND msib.inventory_item_id = ola.inventory_item_id
                          AND msib.organization_id =
                              ship_from_org.organization_id
                          AND msib.inventory_item_id = ola.inventory_item_id
                          AND ola.header_id = xc.header_id
                          AND ola.line_id = xc.line_id
                          AND xc.delivery_id = cur_order_rec.delivery_id
                          AND change_type = 'D'
                          AND oha.header_id = cur_order_rec.header_id
						  AND ola.source_type_code='INTERNAL'  -- Ver# 1.10 TMS# 20150519-00037
                          AND NOT EXISTS
                        (SELECT '1'
                                 FROM xxwc_wsh_shipping_stg stg
                                WHERE 1 = 1
                                  AND stg.header_id = cur_order_rec.header_id
                                  AND stg.delivery_id =
                                      cur_order_rec.delivery_id)
                       UNION
                       --get cancelled orders  --11/17/14 
                       --Version 1.1 
                       SELECT oha.header_id
                             ,oha.order_number
                             ,cur_order_rec.delivery_id
                             ,hp.party_name customer_name
                             ,hca.account_number
                             ,to_char(oha.ordered_date, 'MM/DD/YYYY') ordered_date
                             ,oha.ship_to_org_id
                             ,ola.ship_from_org_id
                             ,oha.invoice_to_org_id
                             ,oha.ship_to_contact_id
                             ,oha.sold_to_contact_id
                             ,oha.payment_term_id
                             ,term.name termname
                             ,oha.cust_po_number
                             ,to_char(oha.request_date, 'MM/DD/YYYY') request_date
                             ,oha.salesrep_id
                             ,ola.line_id
                             ,ola.line_number
                             ,ola.ordered_item
                             ,ola.inventory_item_id
                             ,ola.order_quantity_uom
                              --,nvl(ola.ordered_quantity, 0) - nvl(ola.cancelled_quantity, 0) Version 1.1
                             ,nvl(ola.ordered_quantity, 0) ord_quantity
                             ,0 shipped_quantity
                             ,ola.shipping_method_code
                             ,ola.packing_instructions
                             ,ola.freight_carrier_code
                             ,ola.user_item_description
                             ,ship_ps.location_id
                             ,msib.unit_weight
                             ,msib.item_type
                             ,msib.description
                             ,ott.name order_type
                             ,'D' status
                             ,ola.attribute14
                             ,TRIM(regexp_replace(asciistr(regexp_replace(oha.shipping_instructions
                                                                         ,'[[:cntrl:]]'
                                                                         ,NULL))
                                                 ,'\\[[:xdigit:]]{4}'
                                                 ,'')) shipping_instructions
                             ,ola.line_type_id --Version 1.1 11/18/14                      
                         FROM oe_order_headers            oha
                             ,oe_order_lines              ola
                             ,ont.oe_transaction_types_tl ott
                             ,ar.ra_terms_tl              term
                             ,ar.hz_parties               hp
                             ,ar.hz_cust_accounts         hca
                             ,hz_cust_site_uses_all       ship_su
                             ,hz_cust_acct_sites_all      ship_cas
                             ,hz_party_sites              ship_ps
                             ,inv.mtl_parameters          ship_from_org
                             ,inv.mtl_system_items_b      msib
                             ,xxwc.xxwc_om_dms_change_gtt_tbl      xc -- Version# 1.5
                        WHERE oha.flow_status_code = 'CANCELLED'
                          AND oha.order_type_id = ott.transaction_type_id
                             --AND ola.user_item_description = 'CANCELLED'
                          AND oha.header_id = ola.header_id
                          AND oha.open_flag = 'N'
                          AND ola.shipping_method_code IN
                              (SELECT /*+ RESULT_CACHE */
                                meaning
                                 FROM fnd_lookup_values
                                WHERE 1 = 1
                                  AND lookup_type =
                                      'XXWC_DMS_SHIPPING_METHOD_CODE'
                                  AND enabled_flag = 'Y'
                                  AND SYSDATE BETWEEN start_date_active AND
                                      nvl(end_date_active, SYSDATE + 1))
                          AND oha.sold_to_org_id = hca.cust_account_id
                          AND hca.party_id = hp.party_id
                          AND oha.payment_term_id = term.term_id(+)
                          AND oha.ship_to_org_id = ship_su.site_use_id(+)
                          AND ship_su.cust_acct_site_id =
                              ship_cas.cust_acct_site_id(+)
                          AND ship_cas.party_site_id =
                              ship_ps.party_site_id(+)
                          AND ola.ship_from_org_id =
                              ship_from_org.organization_id(+)
                          AND msib.inventory_item_id = ola.inventory_item_id
                          AND msib.organization_id =
                              ship_from_org.organization_id
                          AND msib.inventory_item_id = ola.inventory_item_id
                          AND ola.header_id = xc.header_id
                          AND ola.line_id = xc.line_id
                          AND change_type IN ('D', 'C')
                          AND oha.header_id = cur_order_rec.header_id
						  AND ola.source_type_code='INTERNAL'  -- Ver# 1.10 TMS# 20150519-00037
                       /*        AND EXISTS
                           (SELECT '1'
                                    FROM xxwc_wsh_shipping_stg stg
                                   WHERE 1 = 1
                                     AND stg.header_id = cur_order_rec.header_id
                                     AND stg.delivery_id =
                                         cur_order_rec.delivery_id
                                     AND stg.status = 'CANCELLED')
                       */ --11/17/14              
                       UNION
                       --Rentals and Repair Orders
                       SELECT oha.header_id
                             ,oha.order_number
                             ,NULL delivery_id
                             ,hp.party_name customer_name
                             ,hca.account_number
                             ,to_char(oha.ordered_date, 'MM/DD/YYYY') ordered_date
                             ,oha.ship_to_org_id
                             ,ola.ship_from_org_id
                             ,oha.invoice_to_org_id
                             ,oha.ship_to_contact_id
                             ,oha.sold_to_contact_id
                             ,oha.payment_term_id
                             ,term.name termname
                             ,oha.cust_po_number
                             ,to_char(oha.request_date, 'MM/DD/YYYY') request_date
                             ,oha.salesrep_id
                             ,ola.line_id
                             ,ola.line_number
                             ,ola.ordered_item
                             ,ola.inventory_item_id
                             ,ola.order_quantity_uom
                              -- ,nvl(ola.ordered_quantity, 0) - nvl(ola.cancelled_quantity, 0) ord_quantity  Version 1.2
                             ,nvl(ola.ordered_quantity, 0) ord_quantity
                              --,nvl(ola.ordered_quantity, 0) - nvl(ola.cancelled_quantity, 0) shipped_quantity  Version 1.2
                             ,nvl(ola.ordered_quantity, 0) shipped_quantity
                             ,ola.shipping_method_code
                             ,ola.packing_instructions
                             ,ola.freight_carrier_code
                             ,ola.user_item_description
                             ,ship_ps.location_id
                             ,msib.unit_weight
                             ,msib.item_type
                             ,msib.description
                             ,ott.name order_type
                              --,NULL status   11/17/14  Version 1.1
                             ,CASE
                                WHEN ola.open_flag = 'N' THEN
                                 'D'
                                ELSE
                                 NULL
                              END status
                             ,ola.attribute14
                             ,TRIM(regexp_replace(asciistr(regexp_replace(oha.shipping_instructions
                                                                         ,'[[:cntrl:]]'
                                                                         ,NULL))
                                                 ,'\\[[:xdigit:]]{4}'
                                                 ,'')) shipping_instructions
                             ,ola.line_type_id --Version 1.1 11/18/14                      
                         FROM oe_order_headers            oha
                             ,oe_order_lines              ola
                             ,ont.oe_transaction_types_tl ott
                             ,ar.ra_terms_tl              term
                             ,ar.hz_parties               hp
                             ,ar.hz_cust_accounts         hca
                             ,hz_cust_site_uses_all       ship_su
                             ,hz_cust_acct_sites_all      ship_cas
                             ,hz_party_sites              ship_ps
                             ,inv.mtl_parameters          ship_from_org
                             ,inv.mtl_system_items_b      msib
                        WHERE oha.flow_status_code = 'BOOKED'
                          AND oha.order_type_id = ott.transaction_type_id
                          AND oha.header_id = ola.header_id
                             --AND ola.open_flag = 'Y'  Version 1.1 11/17/14
                          AND ola.user_item_description IS NULL
                          AND ola.shipping_method_code IN
                              (SELECT /*+ RESULT_CACHE */
                                meaning
                                 FROM fnd_lookup_values
                                WHERE 1 = 1
                                  AND lookup_type =
                                      'XXWC_DMS_SHIPPING_METHOD_CODE'
                                  AND enabled_flag = 'Y'
                                  AND SYSDATE BETWEEN start_date_active AND
                                      nvl(end_date_active, SYSDATE + 1))
                             --Version 1.1 to fix Rental Orders 11/6/2014           
                          AND ola.line_type_id NOT IN (1021, 1007, 1008) --RETURN WITH RECEIPT, WC BILL ONLY, CREDIT ONLY
                          AND ola.inventory_item_id <> 2931193 --Rental Charge 
                          AND ola.flow_status_code <> 'CLOSED'
                          AND ola.attribute8 IS NULL
                             --            
                          AND oha.sold_to_org_id = hca.cust_account_id
                          AND hca.party_id = hp.party_id
                          AND oha.payment_term_id = term.term_id(+)
                          AND oha.ship_to_org_id = ship_su.site_use_id(+)
                          AND ship_su.cust_acct_site_id =
                              ship_cas.cust_acct_site_id(+)
                          AND ship_cas.party_site_id =
                              ship_ps.party_site_id(+)
                          AND ola.ship_from_org_id =
                              ship_from_org.organization_id(+)
                          AND msib.inventory_item_id = ola.inventory_item_id
                          AND msib.organization_id =
                              ship_from_org.organization_id
                          AND msib.inventory_item_id = ola.inventory_item_id
                          AND oha.header_id = cur_order_rec.header_id
						  AND ola.source_type_code='INTERNAL' -- Ver# 1.10 TMS# 20150519-00037
                        ORDER BY 18)
      LOOP
      
        --use lookup to check org is DMS enabled   
        l_dms_org := dms_org(cur_line.ship_from_org_id);
      
        IF l_dms_org = 'Y'
        THEN
        
          --will used transaction qty for shipped qty for standard and internal orders
          IF cur_line.order_type IN ('INTERNAL ORDER', 'STANDARD ORDER')
          THEN
            l_ship_qty := cur_line.shipped_quantity;
          ELSE
            l_ship_qty := cur_line.ord_quantity;
          END IF;
        
          --Branch info
          BEGIN
            SELECT substr(hl.location_code
                         ,1
                         ,instr(hl.location_code, ' ') - 1) branch
            
              INTO l_branch
              FROM hr_all_organization_units hou, hr_locations_all hl
             WHERE hou.organization_id = cur_line.ship_from_org_id
               AND hou.location_id = hl.location_id;
          EXCEPTION
            WHEN no_data_found THEN
              l_branch := NULL;
          END;
        
          --Bill To information   
          BEGIN
          
            xxwc_gen_routines_pkg.site_address(p_site_use_code => 'BILL_TO'
                                              ,p_site_use_id   => cur_line.invoice_to_org_id
                                              ,p_addr1         => l_bill_add1
                                              ,p_loc           => l_location
                                              ,p_addr2         => l_bill_add2
                                              ,p_addr3         => l_bill_add3
                                              ,p_addr5         => l_bill_add5
                                              ,p_code          => l_bill_areacode
                                              ,p_phone         => l_bill_phone
                                              ,p_ret_code      => l_bill_ret_code
                                              ,p_ret_msg       => l_bill_ret_msg
                                              ,p_site_number   => l_bill_site);
          EXCEPTION
            WHEN no_data_found THEN
              l_bill_add1     := NULL;
              l_bill_add2     := NULL;
              l_bill_add3     := NULL;
              l_bill_add5     := NULL;
              l_bill_areacode := NULL;
              l_bill_phone    := NULL;
              l_bill_site     := NULL;
            
          END;
        
          --Contact info
          IF cur_line.sold_to_contact_id IS NOT NULL
          THEN
            BEGIN
              SELECT party.party_name
                INTO l_cont_name
                FROM hz_cust_account_roles acct_role
                    ,hz_parties            party
                    ,hz_relationships      rel
                    ,hz_org_contacts       org_cont
               WHERE acct_role.cust_account_role_id =
                     cur_line.sold_to_contact_id
                 AND acct_role.party_id = rel.party_id
                 AND acct_role.role_type = 'CONTACT'
                 AND org_cont.party_relationship_id = rel.relationship_id
                 AND rel.subject_table_name = 'HZ_PARTIES'
                 AND rel.object_table_name = 'HZ_PARTIES'
                 AND rel.directional_flag = 'F'
                 AND rel.subject_id = party.party_id;
            EXCEPTION
              WHEN no_data_found THEN
                l_cont_name := NULL;
            END;
          END IF;
        
          --Ship To information
          BEGIN
          
            xxwc_gen_routines_pkg.site_address(p_site_use_code => 'SHIP_TO'
                                              ,p_site_use_id   => cur_line.ship_to_org_id
                                              ,p_addr1         => l_ship_add1
                                              ,p_loc           => l_ship_location
                                              ,p_addr2         => l_ship_add2
                                              ,p_addr3         => l_ship_add3
                                              ,p_addr5         => l_ship_add5
                                              ,p_code          => l_ship_areacode
                                              ,p_phone         => l_ship_phone
                                              ,p_ret_code      => l_ship_ret_code
                                              ,p_ret_msg       => l_ship_ret_msg
                                              ,p_site_number   => l_ship_site);
          
          EXCEPTION
            WHEN no_data_found THEN
              l_ship_add1     := NULL;
              l_location      := NULL;
              l_ship_add2     := NULL;
              l_ship_add3     := NULL;
              l_ship_add5     := NULL;
              l_ship_areacode := NULL;
              l_ship_phone    := NULL;
              l_ship_site     := NULL;
          END;
        
          BEGIN
          
            xxwc_gen_routines_pkg.job_site_contact(p_ship_to_contact_id => cur_line.ship_to_contact_id
                                                  ,p_contact_name       => l_job_site_contact
                                                  ,p_contact_number     => l_job_site_phone);
          EXCEPTION
            WHEN no_data_found THEN
              l_job_site_phone   := NULL;
              l_job_site_contact := NULL;
            
          END;
        
          IF cur_line.item_type = 'SPECIAL'
          THEN
            l_item_type := 'THIS ITEM IS SPECIAL ORDER AND MAY BE NON-RETURNABLE';
          ELSE
            l_item_type := NULL;
          END IF;
        
          l_sec := 'Writing file';
          -- Writting file -- Version# 1.6 > Start 
		  -- Removed special Chars in file  -- Version# 1.11 < Start
          utl_file.put_line(l_file_handle
                           ,l_branch || '|' || cur_line.order_number || '|' ||
                            cur_line.delivery_id || '|' ||
                            TRANSLATE(cur_line.customer_name,'<>";/()-+?{}[]\|=_*&^%$#@!~`',' ') || '|' ||
                            cur_line.account_number || '|' || regexp_replace(l_location,  '[[:cntrl:]]') || '|' ||
                            TRANSLATE(regexp_replace(l_bill_add1 ,  '[[:cntrl:]]'),'<>";/()-+?{}[]\|=_*&^%$#@!~`',' ') || '|' || TRANSLATE(regexp_replace(l_bill_add5 ,  '[[:cntrl:]]'),'<>";/()-+?{}[]\|=_*&^%$#@!~`',' ') || '|' ||
                            l_bill_phone || '|' || TRANSLATE(regexp_replace(l_ship_add1 ,  '[[:cntrl:]]'),'<>";/()-+?{}[]\|=_*&^%$#@!~`',' ') || '|' ||
                            TRANSLATE(regexp_replace(l_ship_add2 ,  '[[:cntrl:]]'),'<>";/()-+?{}[]\|=_*&^%$#@!~`',' ') || '|' || TRANSLATE(regexp_replace(l_ship_add3 ,  '[[:cntrl:]]'),'<>";/()-+?{}[]\|=_*&^%$#@!~`',' ') || '|' ||
                            TRANSLATE(regexp_replace(l_ship_add5 ,  '[[:cntrl:]]'),'<>";/()-+?{}[]\|=_*&^%$#@!~`',' ') || '|' ||
                            nvl(regexp_replace(l_job_site_phone,  '[[:cntrl:]]'), l_ship_phone) || '|' ||
                            l_ship_site || '|' || TRANSLATE(regexp_replace(l_job_site_contact,  '[[:cntrl:]]'),'<>";/()-+?{}[]\|=_*&^%$#@!~`',' ') || '|' ||
                            TRANSLATE(regexp_replace(l_cont_name ,  '[[:cntrl:]]'),'<>";/()-+?{}[]\|=_*&^%$#@!~`',' ') || '|' || cur_line.ordered_date || '|' ||
                            cur_line.request_date || '|' ||
                            TRANSLATE(regexp_replace(cur_line.cust_po_number,  '[[:cntrl:]]'),'<>";/()-+?{}[]\|=_*&^%$#@!~`',' ') || '|' ||
                            cur_line.termname || '|' ||
                            cur_line.freight_carrier_code || '|' ||
                            cur_line.line_number || '|' ||
                            cur_line.ordered_item || '|' ||
                            TRANSLATE(regexp_replace(cur_line.description,  '[[:cntrl:]]'),'<>";/()-+?{}[]\|=_*&^%$#@!~`',' ') || '|' ||
                            cur_line.unit_weight || '|' ||
                            cur_line.ord_quantity || '|' || l_ship_qty || '|' ||
                            cur_line.order_quantity_uom || '|' ||
                            TRANSLATE(regexp_replace(SUBSTR(cur_line.shipping_instructions,1,250) ,  '[[:cntrl:]]'),'<>";/()-+?{}[]\|=_*&^%$#@!~`',' ') || '|' ||  -- Version# 1.9
                            l_item_type || TRANSLATE(cur_line.attribute14,'<>";/()-+?{}[]\|=_*&^%$#@!~`',' ') || '|' ||
                            cur_line.order_type || '|' || cur_line.status);
                            -- Version# 1.6 < End
							-- Version# 1.11 < End

-- Version# 1.8 > Start        
    BEGIN
      INSERT INTO xxwc_om_dms_change_archive_tbl arc
      SELECT * 
        FROM xxwc_om_dms_change_tbl chng 
       WHERE 1=1 
         AND chng.header_id   = cur_line.header_id
         AND chng.delivery_id = cur_line.delivery_id
         AND NOT EXISTS (SELECT '1' 
                           FROM xxwc_om_dms_change_archive_tbl arc2 
                          WHERE arc2.header_id = chng.header_id 
                            AND NVL(arc2.delivery_id, -1) = NVL(chng.delivery_id, -1));         
    EXCEPTION
    WHEN OTHERS THEN
      l_err_msg := 'Error inserting into XXWC_OM_DMS_CHANGE_ARCHIVE_TBL';
      fnd_file.put_line(fnd_file.log, l_err_msg);
    END;
-- Version# 1.8 < End

        END IF;
      
      END LOOP;
    END LOOP;
    --end of file line  Version 1.1
    utl_file.put_line(l_file_handle, 'EOF');
    --Close the file
    l_sec := 'Closing the File; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
    utl_file.fclose(l_file_handle);
  
    l_sec := 'Verifying if data exists in file';
    fnd_file.put_line(fnd_file.log, 'Verifying if data exists in file');
    utl_file.fgetattr(location    => l_file_dir
                     ,filename    => l_file_name_temp
                     ,fexists     => l_base_file_exists
                     ,file_length => l_base_file_length
                     ,block_size  => l_base_file_block_size);
  
    fnd_file.put_line(fnd_file.log
                     ,'l_base_file_length: ' || l_base_file_length ||
                      ' l_base_file_block_size: ' ||
                      l_base_file_block_size);
  
    IF p_header_id IS NULL
    THEN
    
      UPDATE xxwc_om_dms_tbl
         SET status           = 'Complete'
            ,last_update_date = SYSDATE
            ,file_size        = l_base_file_length
       WHERE file_id = l_sequence;
    
      COMMIT;
      l_sec := 'Update table xxwc_om_DMS_tbl where file_id = ' ||
               l_sequence;
      fnd_file.put_line(fnd_file.log, l_sec);
    
    ELSE
      UPDATE xxwc_om_dms_brnch_file_tbl
         SET status           = 'Complete'
            ,last_update_date = SYSDATE
            ,file_size        = l_base_file_length
       WHERE header_id = p_header_id
         AND delivery_id = p_delivery_id;
    
      l_sec := 'Update table xxwc_om_dms_brnch_file_tbl header_id = ' ||
               p_header_id || ' Delivery_ID = ' || p_delivery_id;
      fnd_file.put_line(fnd_file.log, l_sec);
    
    END IF;
  
    IF l_base_file_length <= 4 --Version 1.1 change from 0 to allow for end of line record
    THEN
    
      utl_file.fremove(location => l_file_dir
                      ,filename => l_file_name_temp);
    ELSE
      utl_file.frename(l_file_dir
                      ,l_file_name_temp
                      ,l_file_dir
                      ,l_filename);
    END IF;
  
    --remove on demand orders that were sent to MyLogistics by the user so if another change happens it will be picked up in
    --the next standard run submitted thru UC4
    DELETE FROM xxwc_om_dms_brnch_file_tbl
     WHERE creation_date < l_run_date;
  
    -- Version# 1.5 > Start

-- Version# 1.8 > Start  
/*
    -- Delete from XXWC_OM_DMS_CHANGE_TBL
    BEGIN
      INSERT INTO xxwc_om_dms_change_archive_tbl arc
      SELECT * 
        FROM xxwc_om_dms_change_tbl chng 
       WHERE 1=1 
         AND TRUNC(chng.last_update_date) = TRUNC(SYSDATE)
         -- AND ool.shipment_method = Lookup Value
         AND NOT EXISTS (SELECT '1'
                           FROM xxwc_om_dms_change_archive_tbl arc2 
                          WHERE arc2.header_id = chng.header_id 
                            AND NVL(arc2.delivery_id, -1) = NVL(chng.delivery_id, -1));
      COMMIT;
    EXCEPTION
    WHEN OTHERS THEN
      l_err_msg := 'Error inserting into XXWC_OM_DMS_CHANGE_ARCHIVE_TBL';
      fnd_file.put_line(fnd_file.log, l_err_msg);
    END;
*/

    fnd_file.put_line(fnd_file.log, 'Ship Delete Start Time - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH:MI:SS'));
    BEGIN
      DELETE FROM xxwc_om_dms_change_tbl chng 
       WHERE 1 = 1 
         AND EXISTS (SELECT '1' 
                       FROM xxwc.xxwc_wsh_shipping_stg shp
                      WHERE shp.header_id            = chng.header_id
                        AND shp.delivery_id          = chng.delivery_id
                        AND shp.status               = 'DELIVERED');
      COMMIT;
    EXCEPTION
    WHEN OTHERS THEN
      l_err_msg := 'Error deleting DELIVERED records from XXWC_OM_DMS_CHANGE_TBL';
      fnd_file.put_line(fnd_file.log, l_err_msg);
    END;
    fnd_file.put_line(fnd_file.log, 'Ship Delete End Time - '||TO_CHAR(SYSDATE,'DD-MON-YYYY HH:MI:SS'));
-- Version# 1.8 < End

    BEGIN
      DELETE FROM xxwc_om_dms_change_tbl chng 
       WHERE 1 = 1 
         AND EXISTS (SELECT '1' 
                       FROM xxwc_om_dms_change_archive_tbl arc2
                      WHERE arc2.header_id = chng.header_id
                        AND NVL(arc2.delivery_id, -1) = NVL(chng.delivery_id, -1));
      COMMIT;
    EXCEPTION
    WHEN OTHERS THEN
      l_err_msg := 'Error deleting ARCHIVED records from XXWC_OM_DMS_CHANGE_TBL';
      fnd_file.put_line(fnd_file.log, l_err_msg);
    END;
    -- Version# 1.5 < End
  
  EXCEPTION
    WHEN utl_file.invalid_path THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'File Path is invalid.';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'OM');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN utl_file.invalid_mode THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'The open_mode parameter in FOPEN is invalid.';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'OM');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN utl_file.invalid_filehandle THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'File handle is invalid..';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'OM');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN utl_file.invalid_operation THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'File could not be opened or operated on as requested';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'OM');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN utl_file.write_error THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'Operating system error occurred during the write operation';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'OM');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN utl_file.internal_error THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'Unspecified PL/SQL error.';
      fnd_file.put_line(fnd_file.log, l_err_msg);
    
    WHEN utl_file.file_open THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'The requested operation failed because the file is open.';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'OM');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN utl_file.invalid_filename THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'The filename parameter is invalid.';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'OM');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN utl_file.access_denied THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := 'Permission to access to the file location is denied.';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'OM');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'OM');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN OTHERS THEN
      utl_file.fclose(l_file_handle);
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'ONT');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
  END gen_file;

/********************************************************************************
  -- PROCEDURE: SHIP_CONFIRM
  --
  -- PURPOSE: Ship Confirm delivered orders that doesn't have any exceptions.
  --          File is received from MyLogistics 
  --
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.0     20-Aug-2014   Kathy Poling    Created this package. TMS 20140606-00082
  -- 1.1     06-Nov-2014   Kathy Poling    Fix multiple ship methods TMS 20141112-00097
  -- 1.2     01-Dec-2014   Kathy Poling    Added route_number this will allow a delivery     
                                           to be put back on a route in MyLogistics
                                           delivered and the ship confirm process
                                           will validate based on that route 
  -- 1.10    04-Apr-2015   Gopi Damuluri   TMS# 20150227-00035 - Peformance Tuning of SHIP_CONFIRM_SIG_CAPT process
                                           Added new procedure - UPDATE_OFD_DELIVERY_RUN
                                           TMS# 20150226-00083 - Performance issue for XXWC DMS Ship Confirm Delivered Orders
                                           TMS# 20150226-00084 - DMS - PERF fix for XXWC DMS Ship Confirm Signature Captured Orders
  -- 1.11    12-May-2015   Gopi Damuluri   TMS# 20150512-00161 - DMS bug fix related to payment hold
  *******************************************************************************/

  PROCEDURE ship_confirm(p_errbuf              OUT VARCHAR2
                        ,p_retcode             OUT VARCHAR2
                        ,p_user_name           IN VARCHAR2
                        ,p_responsibility_name IN VARCHAR2) IS
  
    l_user_id             fnd_user.user_id%TYPE;
    l_org_id              NUMBER := mo_global.get_current_org_id;
    l_responsibility_id   NUMBER;
    l_resp_application_id NUMBER;
    l_err_msg             CLOB;
    l_sec                 VARCHAR2(110) DEFAULT 'START';
    l_procedure           VARCHAR2(50) := 'SHIP_CONFIRM';
    l_return_status       VARCHAR2(1);
    l_msg_data            VARCHAR2(2000);
    l_last_delta_date     DATE;
    l_hold_count          NUMBER;
    l_from_org_id         NUMBER;
    l_pick_rule           VARCHAR2(50);
    l_order_type          VARCHAR2(100);
    l_ship_from           VARCHAR2(100);
    l_exists              VARCHAR2(1) DEFAULT 'F';
    l_exception_exists    VARCHAR2(1) DEFAULT 'F';
    l_ship                VARCHAR2(1) DEFAULT 'N';
    l_shipping_method     NUMBER;
  
    l_sender      VARCHAR2(100);
    l_sid         VARCHAR2(8);
    l_body        VARCHAR2(32767);
    l_body_header VARCHAR2(32767);
    l_body_detail VARCHAR2(32767);
    l_body_footer VARCHAR2(32767);
  
  BEGIN
  
    l_sec := 'Set policy context for org_id';
    -- Mandatory initialization for R12
    mo_global.set_policy_context('S', l_org_id);
    mo_global.init('ONT');
  
    -- Deriving Ids from initalization variables
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM apps.fnd_user
       WHERE user_name = upper(p_user_name)
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'UserName ' || p_user_name || ' not defined in Oracle';
        RAISE program_error;
    END;
  
    BEGIN
      SELECT responsibility_id, application_id
        INTO l_responsibility_id, l_resp_application_id
        FROM apps.fnd_responsibility_vl
       WHERE responsibility_name = p_responsibility_name
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Responsibility ' || p_responsibility_name ||
                     ' not defined in Oracle';
        RAISE program_error;
    END;
  
    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    l_sec := 'Apps Initialize';
  
    fnd_global.apps_initialize(l_user_id
                              ,l_responsibility_id
                              ,l_resp_application_id);
  
  -- Version# 1.10 > Start
    --------------------------------------------------------------------------
    -- Inserting into XXWC_OM_DMS_SHIP_CONF_GTT_TBL
    --------------------------------------------------------------------------

  l_sec := 'Inserting into XXWC_OM_DMS_SHIP_CONF_GTT_TBL';
  INSERT INTO xxwc.xxwc_om_dms_ship_conf_gtt_tbl
  SELECT * FROM xxwc_om_dms_ship_confirm_tbl
    WHERE ship_confirm_status = 'NO'
      AND order_number IS NOT NULL
      AND delivery_id IS NOT NULL;
  -- Version# 1.10 < End

-- Version# 1.11 > Start
  BEGIN
   UPDATE xxwc.xxwc_om_dms_ship_conf_gtt_tbl stg
      SET stg.ship_confirm_status = 'ERROR'
        , stg.ship_confirm_exception = 'PAYMENT_HOLD_EXISTS'
    WHERE 1 = 1 
      AND stg.delivery_id         IS NOT NULL
      AND stg.ship_confirm_status = 'NO'
      AND EXISTS (SELECT '1' 
                     FROM apps.oe_hold_sources_all  ohs
                        , apps.oe_order_headers_all ooh
                    WHERE 1 = 1
                      AND stg.order_number        = ooh.order_number
                      AND to_char(ooh.header_id)  = ohs.hold_entity_id
                      AND ohs.hold_entity_code    = 'O'
                      AND ohs.hold_id            IN (13,14)
                      AND ohs.released_flag       = 'N');
  EXCEPTION
  WHEN OTHERS THEN
     NULL;
  END;
-- Version# 1.11 < End
  
    l_sec := 'Ready to start loop';
  
    FOR cur_ship IN (SELECT DISTINCT sc.order_number
                                    ,sc.delivery_id
                                    ,oh.header_id
                                    ,sc.signed_by
                                    ,sc.notes
                                    ,sc.dispatch_date
                                    ,ot.name order_type
                                    ,oh.order_type_id
                                    ,sc.route_number --Version 1.2
                       FROM xxwc.xxwc_om_dms_ship_conf_gtt_tbl sc -- Version# 1.10
                           -- ,xxwc_om_dms_ship_confirm_tbl sc
                           ,oe_order_headers             oh
                           ,oe_transaction_types_tl      ot
                      WHERE ship_confirm_status = 'NO'
                        AND sc.order_number = oh.order_number
                        AND oh.order_type_id = ot.transaction_type_id)
    LOOP
    
      l_hold_count       := NULL;
      l_return_status    := NULL;
      l_from_org_id      := NULL;
      l_ship_from        := NULL;
      l_exists           := 'F';
      l_exception_exists := 'F';
      l_ship             := 'N';
      l_shipping_method  := NULL;
    
      --dbms_output.put_line('order number = '||cur_ship.order_number||' delivery id = '||cur_ship.delivery_id);
    
      BEGIN
        SELECT 'Y'
          INTO l_ship
          FROM fnd_lookup_values
         WHERE 1 = 1
           AND lookup_type = 'XXWC_DMS_ORDERTYPE_SHIPCONFIRM'
           AND enabled_flag = 'Y'
           AND SYSDATE BETWEEN start_date_active AND
               nvl(end_date_active, SYSDATE + 1)
           AND lookup_code = cur_ship.order_type_id;
      
      EXCEPTION
        WHEN no_data_found THEN
          DELETE FROM xxwc_om_dms_ship_confirm_tbl
           WHERE order_number = cur_ship.order_number
             AND nvl(delivery_id, -1) = nvl(cur_ship.delivery_id, -1);

-- Version# 1.10 > Start
    --------------------------------------------------------------------------
    -- Delete from XXWC_OM_DMS_SHIP_CONF_GTT_TBL
    --------------------------------------------------------------------------
          DELETE FROM xxwc.xxwc_om_dms_ship_conf_gtt_tbl
           WHERE order_number = cur_ship.order_number
             AND nvl(delivery_id, -1) = nvl(cur_ship.delivery_id, -1);
-- Version# 1.10 < End

          COMMIT;
      END;
    
      IF l_ship = 'Y'
      THEN
      
        --Need ship_from_org for the order header_id/delivery_id to ship confirm lines 
        BEGIN
        
          SELECT DISTINCT ship_from_org_id
            INTO l_from_org_id
            FROM xxwc.xxwc_wsh_shipping_stg
           WHERE header_id = cur_ship.header_id
             AND delivery_id = cur_ship.delivery_id;
          --dbms_output.put_line('l_from_org_id = ' ||l_from_org_id);
        EXCEPTION
          WHEN no_data_found THEN
            l_from_org_id := -1;
          
          WHEN too_many_rows THEN
            l_from_org_id := -1;
          
        END;
      
        IF l_from_org_id = -1
        THEN
        
          --lines cannot ship confirm issue with getting ship_from_org_id
          UPDATE xxwc.xxwc_om_dms_ship_conf_gtt_tbl -- xxwc_om_dms_ship_confirm_tbl -- Version# 1.10
             SET ship_confirm_exception = 'Delivery does not exists in shipping tbl'
                ,ship_confirm_status    = 'ERROR'
                ,last_update_date       = SYSDATE
                ,last_updated_by        = fnd_global.user_id()
           WHERE order_number = cur_ship.order_number
             AND delivery_id = cur_ship.delivery_id
             AND route_number = cur_ship.route_number; --Version 1.2
        
        ELSE
        
          -- need pick rule for ship comfirm    
          SELECT organization_code
            INTO l_ship_from
            FROM org_organization_definitions
           WHERE organization_id = l_from_org_id;
        
          IF cur_ship.order_type LIKE '%STANDARD%'
          THEN
            l_pick_rule := l_ship_from || ' Standard'; --' Standard'; --'-Unreleased Std';
          ELSE
            l_pick_rule := l_ship_from || '-All Internal';
          END IF;
        
          --dbms_output.put_line('l_pick_rule:  '||l_pick_rule);
        
          --Check for DMS delivery exceptions
          BEGIN
          
            SELECT 'T'
              INTO l_exception_exists
              FROM xxwc.xxwc_om_dms_ship_conf_gtt_tbl -- xxwc_om_dms_ship_confirm_tbl -- Version# 1.10
             WHERE order_number = cur_ship.order_number
               AND delivery_id = cur_ship.delivery_id
               AND route_number = cur_ship.route_number --Version 1.2
               AND (nvl(line_exception, 'x') <> 'x' OR
                   nvl(stop_exception, 'x') <> 'x')
               AND rownum = 1;
          
            --'Cannot Ship Confirm an order with any exception from MyLogistics delivery confirmation data.'      
          EXCEPTION
            WHEN no_data_found THEN
              l_exception_exists := 'F';
          END;
        
          IF l_exception_exists = 'T'
          THEN
            --lines cannot ship confirm with a delivery exception
            UPDATE xxwc.xxwc_om_dms_ship_conf_gtt_tbl -- xxwc_om_dms_ship_confirm_tbl -- Version# 1.10
               SET ship_confirm_exception = 'Delivery Exception'
                  ,ship_confirm_status    = 'EXCEPTION'
                  ,last_update_date       = SYSDATE
                  ,last_updated_by        = fnd_global.user_id()
             WHERE order_number = cur_ship.order_number
               AND delivery_id = cur_ship.delivery_id
               AND route_number = cur_ship.route_number; --Version 1.2
          
          ELSE
            -- Check if order is on hold
            SELECT xxwc_ont_routines_pkg.count_order_holds(cur_ship.header_id)
              INTO l_hold_count
              FROM dual;
          
            IF l_hold_count > 0
            THEN
              --lines cannot ship confirm delivery with holds
              UPDATE xxwc.xxwc_om_dms_ship_conf_gtt_tbl -- xxwc_om_dms_ship_confirm_tbl -- Version# 1.10
                 SET ship_confirm_exception = 'Holds exists for Order'
                    ,ship_confirm_status    = 'ERROR'
                    ,last_update_date       = SYSDATE
                    ,last_updated_by        = fnd_global.user_id()
               WHERE order_number = cur_ship.order_number
                 AND delivery_id = cur_ship.delivery_id
                 AND route_number = cur_ship.route_number; --Version 1.2
            
            ELSE
            
              -- Prevent Ship Confirm if there is a special with item cost zero    
              BEGIN
              
                SELECT 'T'
                  INTO l_exists
                  FROM xxwc_wsh_shipping_stg_v
                 WHERE header_id = cur_ship.header_id
                   AND delivery_id = cur_ship.delivery_id
                   AND item_type IN ('SPECIAL', 'REPAIR')
                   AND nvl(item_cost, 0) = 0
                   AND status = 'OUT_FOR_DELIVERY' --Version 1.2
                   AND rownum = 1;
              
                --dbms_output.put_line('l_exists:  '||l_exists);    
              EXCEPTION
                WHEN no_data_found THEN
                  l_exists := 'F';
              END;
            
              IF l_exists = 'T'
              THEN
                --lines cannot ship confirm delivery with SPECIAL and no cost
                UPDATE xxwc.xxwc_om_dms_ship_conf_gtt_tbl -- xxwc_om_dms_ship_confirm_tbl -- Version# 1.10
                   SET ship_confirm_exception = 'One line is SPECIAL/REPAIR and no cost'
                      ,ship_confirm_status    = 'ERROR'
                      ,last_update_date       = SYSDATE
                      ,last_updated_by        = fnd_global.user_id()
                 WHERE order_number = cur_ship.order_number
                   AND delivery_id = cur_ship.delivery_id
                   AND route_number = cur_ship.route_number; --Version 1.2
              
              ELSE
              
                BEGIN
                  --check if delivery has multiple ship method codes
                  SELECT COUNT(DISTINCT shipping_method_code)
                    INTO l_shipping_method
                    FROM oe_order_lines ol
                   WHERE header_id = cur_ship.header_id
                     AND ol.header_id IN
                         (SELECT header_id
                            FROM xxwc.xxwc_wsh_shipping_stg ws
                           WHERE ws.header_id = ol.header_id
                             AND ws.line_id = ol.line_id --Version 1.1 
                             AND delivery_id = cur_ship.delivery_id);
                
                  --'Cannot Ship Confirm an order with multiple shipping methods for a delivery.'      
                EXCEPTION
                  WHEN no_data_found THEN
                    l_shipping_method := -1;
                END;
              
                IF l_shipping_method <> 1
                THEN
                  --lines cannot ship confirm 
                  UPDATE xxwc.xxwc_om_dms_ship_conf_gtt_tbl -- xxwc_om_dms_ship_confirm_tbl -- Version# 1.10
                     SET ship_confirm_exception = 'Delivery has multiple shipping methods'
                        ,ship_confirm_status    = 'ERROR'
                        ,last_update_date       = SYSDATE
                        ,last_updated_by        = fnd_global.user_id()
                   WHERE order_number = cur_ship.order_number
                     AND delivery_id = cur_ship.delivery_id
                     AND route_number = cur_ship.route_number; --Version 1.2
                
                ELSE
                
                  xxwc_wsh_shipping_extn_pkg.ship_confirm_order(p_header_id     => cur_ship.header_id
                                                               ,p_from_org_id   => l_from_org_id
                                                               ,p_delivery_id   => cur_ship.delivery_id
                                                               ,p_pick_rule     => l_pick_rule
                                                               ,p_return_status => l_return_status
                                                               ,p_return_msg    => l_msg_data);
                
                  l_err_msg := 'delivery_id = ' || cur_ship.delivery_id ||
                               ' status = ' || l_return_status ||
                               '  -  Return MSG = ' || l_msg_data;
                
                  IF l_return_status = 'S'
                  THEN
                  
                    l_sec := 'Insert xxwc_om_fulfill_acceptance for header_id: ' ||
                             cur_ship.header_id || ' Delivery_ID: ' ||
                             cur_ship.delivery_id;
                  
                  /* -- Version# 1.10 > Start
                  
                    INSERT INTO xxwc.xxwc_om_fulfill_acceptance
                      (header_id
                      ,accepted_by
                      ,accepted_date
                      ,accepted_signature
                      ,acceptance_comments
                      ,process_flag
                      ,error_msg
                      ,creation_date
                      ,created_by
                      ,last_update_date
                      ,last_updated_by
                      ,last_update_login
                      ,line_id)
                      SELECT x1.header_id -- Header_id
                            ,l_user_id -- Accepted_by
                            ,cur_ship.dispatch_date -- Acceptance Date
                            ,cur_ship.signed_by -- Accepted Signature
                            ,cur_ship.notes -- Acceptance Comments
                            ,'N' -- Process Flag
                            ,NULL -- Error Message
                            ,SYSDATE -- Creation Date
                            ,l_user_id -- created by
                            ,SYSDATE -- last_update_date
                            ,l_user_id -- last_updated_by
                            ,NULL -- last_update_login
                            ,x1.line_id -- line_id
                        FROM xxwc.xxwc_wsh_shipping_stg x1
                       WHERE x1.header_id = cur_ship.header_id
                         AND x1.delivery_id = cur_ship.delivery_id
                         AND x1.status = 'DELIVERED';
                  
                    -- Call the procedure to sync lines to delivered 
                    l_sec := 'sync_del_line_status for header_id: ' ||
                             cur_ship.header_id || 'delivery_id: ' ||
                             cur_ship.delivery_id;
                  

                    xxwc_wsh_shipping_extn_pkg.sync_del_line_status(errbuf            => p_errbuf
                                                                   ,retcode           => p_retcode
                                                                   ,p_header_id       => cur_ship.header_id
                                                                   ,p_last_delta_date => l_last_delta_date);
                  */ -- Version# 1.10 < End
                  
                    --ship confirmed 
                    l_sec := 'update status for header_id: ' ||
                             cur_ship.header_id || 'delivery_id: ' ||
                             cur_ship.delivery_id;
                  
                    UPDATE xxwc.xxwc_om_dms_ship_conf_gtt_tbl -- xxwc_om_dms_ship_confirm_tbl -- Version# 1.10
                       SET ship_confirm_status = 'COMPLETED'
                          ,last_update_date    = SYSDATE
                          ,last_updated_by     = fnd_global.user_id()
                     WHERE order_number = cur_ship.order_number
                       AND delivery_id = cur_ship.delivery_id
                       AND route_number = cur_ship.route_number; --Version 1.2
                  
                  ELSE
                    UPDATE xxwc.xxwc_om_dms_ship_conf_gtt_tbl -- xxwc_om_dms_ship_confirm_tbl -- Version# 1.10
                       SET ship_confirm_status    = 'ERROR'
                          ,ship_confirm_exception = l_msg_data
                          ,last_update_date       = SYSDATE
                          ,last_updated_by        = fnd_global.user_id()
                     WHERE order_number = cur_ship.order_number
                       AND delivery_id = cur_ship.delivery_id
                       AND route_number = cur_ship.route_number; --Version 1.2
                  
                    --email information if ship confirm fails
                    email_errors(cur_ship.order_number
                                ,cur_ship.delivery_id
                                ,l_msg_data);
                  
                  END IF; --ship confirm
                END IF; --shipping method
              END IF; --special
            END IF; --holds
          END IF; --DMS exceptions
        END IF; --ship from org
      END IF; --order type is in lookup
    END LOOP;
    COMMIT;
    
    -- Version# 1.10 > Start

    --------------------------------------------------------------------------
    -- Insert into XXWC_OM_FULFILL_ACCEPTANCE table
    --------------------------------------------------------------------------
    l_sec := 'Insert into XXWC_OM_FULFILL_ACCEPTANCE table';
    INSERT INTO xxwc.xxwc_om_fulfill_acceptance
      (header_id
      ,accepted_by
      ,accepted_date
      ,accepted_signature
      ,acceptance_comments
      ,process_flag
      ,error_msg
      ,creation_date
      ,created_by
      ,last_update_date
      ,last_updated_by
      ,last_update_login
      ,line_id)
      SELECT x1.header_id -- Header_id
            ,l_user_id -- Accepted_by
            ,gtt.dispatch_date -- Acceptance Date
            ,gtt.signed_by -- Accepted Signature
            ,gtt.notes -- Acceptance Comments
            ,'N' -- Process Flag
            ,NULL -- Error Message
            ,SYSDATE -- Creation Date
            ,l_user_id -- created by
            ,SYSDATE -- last_update_date
            ,l_user_id -- last_updated_by
            ,NULL -- last_update_login
            ,x1.line_id -- line_id
        FROM xxwc.xxwc_wsh_shipping_stg x1
           , xxwc.xxwc_om_dms_ship_conf_gtt_tbl gtt
       WHERE 1 = 1
         AND gtt.delivery_id = x1.delivery_id
         AND x1.status = 'DELIVERED';
    COMMIT;

    --------------------------------------------------------------------------
    -- Call to API - XXWC_WSH_SHIPPING_EXTN_PKG.SYNC_DEL_LINE_STATUS
    --------------------------------------------------------------------------
    l_sec := 'Call to API - XXWC_WSH_SHIPPING_EXTN_PKG.SYNC_DEL_LINE_STATUS';
    xxwc_wsh_shipping_extn_pkg.sync_del_line_status(errbuf            => p_errbuf
                                                   ,retcode           => p_retcode
                                                   ,p_header_id       => NULL
                                                   ,p_last_delta_date => l_last_delta_date);
    COMMIT;

    --------------------------------------------------------------------------
    -- Update XXWC_OM_DMS_SHIP_CONFIRM_TBL from Global Temp Table
    --------------------------------------------------------------------------
    l_sec := 'Update XXWC_OM_DMS_SHIP_CONFIRM_TBL from Global Temp Table';
    UPDATE xxwc_om_dms_ship_confirm_tbl stg
       SET (stg.ship_confirm_status , stg.ship_confirm_exception , stg.last_update_date , stg.last_updated_by) = (SELECT ship_confirm_status
                                                                                                                       , ship_confirm_exception
                                                                                                                       , last_update_date
                                                                                                                       , last_updated_by
                                                                                                                    FROM xxwc.xxwc_om_dms_ship_conf_gtt_tbl  gtt
                                                                                                                   WHERE gtt.order_number = stg.order_number
                                                                                                                     AND gtt.delivery_id  = stg.delivery_id
                                                                                                                     AND gtt.route_number = stg.route_number
                                                                                                                     AND ROWNUM = 1)
      WHERE stg.ship_confirm_status = 'NO';

    COMMIT;
    -- Version# 1.10 < End
    
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'ONT');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN OTHERS THEN
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'ONT');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
  END ship_confirm;

  /**************************************************************************
  *
  * PROCEDURE   DMS_ORG
  *
  * DESCRIPTION Function to check if the InvOrg is DMS Enabled
  *
  *   REVISIONS:
  *   Ver        Date        Author                     Description
  *   ---------  ----------  ---------------         -------------------------
  *   1.0        07/31/2014  Gopi Damuluri             Initial Version  TMS 20140606-00082  
  *************************************************************************/

  FUNCTION dms_org(p_ship_from_org_id IN NUMBER) RETURN VARCHAR2 IS
    l_dms_branch_flag VARCHAR2(1) := 'N';
  BEGIN
    SELECT 'Y'
      INTO l_dms_branch_flag
      FROM fnd_lookup_values
     WHERE 1 = 1
       AND lookup_type = 'XXWC_DMS_BRANCH_LIST'
       AND meaning = to_char(p_ship_from_org_id)
       AND enabled_flag = 'Y'
       AND SYSDATE BETWEEN start_date_active AND
           nvl(end_date_active, SYSDATE + 1)
       AND rownum = 1;
  
    RETURN l_dms_branch_flag;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'N';
  END dms_org;

  /*************************************************************************
     PROCEDURE Name: uc4_submit
  
     PURPOSE:   To load files from XXIFACE Directory to Staging Tables using 
                the generic loader or XXWC DMS Ship Confirm Delivered Orders
                to ship confirm lines delivered thru MyLogistics
  
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        08/04/2014  Kathy Poling            Initial Version TMS 20140606-00082
     1.1        11/06/2014  Kathy Poling            Changed max time for waiting on concurrent request
     1.10       04-Apr-2015 Gopi Damuluri           TMS# 20150227-00035 - Peformance Tuning of SHIP_CONFIRM_SIG_CAPT process
                                                    Added new procedure - UPDATE_OFD_DELIVERY_RUN
                                                    TMS# 20150226-00083 - Performance issue for XXWC DMS Ship Confirm Delivered Orders
                                                    TMS# 20150226-00084 - DMS - PERF fix for XXWC DMS Ship Confirm Signature Captured Orders	 
  ****************************************************************************/
  PROCEDURE uc4_submit(p_errbuf         OUT VARCHAR2
                      ,p_retcode        OUT VARCHAR2
                      ,p_conc_prog_name IN VARCHAR2
                      ,p_user_name      IN VARCHAR2
                      ,p_resp_name      IN VARCHAR2) AS
  
    l_request_id NUMBER;
    l_phase      VARCHAR2(80);
    l_status     VARCHAR2(80);
    l_dev_phase  VARCHAR2(80);
    l_dev_status VARCHAR2(80);
    l_message    VARCHAR2(4000);
    l_interval   NUMBER := 60;
    -- l_max_time   NUMBER := 7200; --Version 1.1 -- Version# 1.10
    l_max_time   NUMBER := 2400; -- Version# 1.11
    l_success    BOOLEAN;
  
    l_user_id NUMBER;
    l_resp_id NUMBER;
    l_appl_id NUMBER := 660; -- OrderManagement
    l_org_id  NUMBER := mo_global.get_current_org_id; -- HDS White Cap - Org
    l_exception EXCEPTION;
  
    -- File Variables
    l_data_file         VARCHAR2(200);
    l_data_file_path    VARCHAR2(200);
    l_control_file      VARCHAR2(200);
    l_control_file_path VARCHAR2(200);
    l_log_file_path     VARCHAR2(200);
    l_sec               VARCHAR2(100);
    
    l_directory_path   VARCHAR2 (1000);
  
    l_order_type VARCHAR2(100);
    l_dummy      VARCHAR2(100);
    l_order_num  VARCHAR2(100);
    l_delivery   VARCHAR2(100);
  
    l_procedure VARCHAR2(50) := 'UC4_SUBMIT';
    l_err_msg   CLOB;
  
  BEGIN
    p_errbuf  := 'Success';
    p_retcode := '0';
  
    l_sec := 'Derive UserId';
    ----------------------------------------------------------------------------------
    -- Derive UserId
    ----------------------------------------------------------------------------------
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM fnd_user
       WHERE user_name = p_user_name;
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to find user id based on parm: ' ||
                     p_user_name;
        RAISE program_error;
    END;
  
    l_sec := 'Derive ResponsibilityId';
    ----------------------------------------------------------------------------------
    -- Derive ResponsibilityId
    ----------------------------------------------------------------------------------
    BEGIN
      SELECT responsibility_id
        INTO l_resp_id
        FROM fnd_responsibility_vl
       WHERE responsibility_name = p_resp_name
         AND application_id = l_appl_id;
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to find responsibility id based on parm: ' ||
                     p_resp_name;
        RAISE program_error;
    END;
  
    l_sec := 'Apps Initialize';
    ----------------------------------------------------------------------------------
    -- Apps Initialize
    ----------------------------------------------------------------------------------
    apps.fnd_global.apps_initialize(user_id      => l_user_id
                                   ,resp_id      => l_resp_id
                                   ,resp_appl_id => l_appl_id);
  
    apps.mo_global.set_policy_context('S', l_org_id);
    apps.mo_global.init('ONT');
  
    IF p_conc_prog_name = 'XXWCGLP'
    THEN
      BEGIN
        SELECT attribute1 dat_file
              ,attribute2 dat_file_path
              ,attribute3 ctl_file
              ,attribute4 ctl_file_path
              ,attribute5 log_file_path
          INTO l_data_file
              ,l_data_file_path
              ,l_control_file
              ,l_control_file_path
              ,l_log_file_path
          FROM fnd_lookup_values
         WHERE lookup_type = 'XXWC_LOADER_COMMON_LOOKUP'
           AND LANGUAGE = userenv('LANG')
           AND meaning = 'XXWC_DMS_DELIVERY_CONF_CTL'
           AND enabled_flag = 'Y'
           AND trunc(SYSDATE) BETWEEN trunc(start_date_active) AND
               trunc(nvl(end_date_active, SYSDATE));
      EXCEPTION
        WHEN OTHERS THEN
          fnd_file.put_line(fnd_file.log
                           ,'In Others Of Common Lookup..' || SQLERRM);
      END;
    
      l_sec := 'Call Concurrent Program - XXWC Genric Loader Program';
      ----------------------------------------------------------------------------------
      -- Call Concurrent Program - XXWC Genric Loader Program
      ----------------------------------------------------------------------------------
      l_request_id := fnd_request.submit_request(application => 'XXWC'
                                                ,program     => p_conc_prog_name
                                                ,description => NULL
                                                ,start_time  => SYSDATE
                                                ,sub_request => FALSE
                                                ,argument1   => l_data_file
                                                ,argument2   => l_data_file_path
                                                ,argument3   => l_control_file
                                                ,argument4   => l_control_file_path
                                                ,argument5   => l_log_file_path);
    
    ELSIF p_conc_prog_name = 'XXWC_DMS_SHIP_CONFIRM'
    THEN
      fnd_request.set_org_id(l_org_id);
      l_sec := 'Call Concurrent Program - XXWC DMS Ship Confirm Delivered Orders';
      ----------------------------------------------------------------------------------
      -- Call Concurrent Program - XXWC DMS Ship Confirm Delivered Orders
      ----------------------------------------------------------------------------------
      l_request_id := fnd_request.submit_request(application => 'XXWC'
                                                ,program     => p_conc_prog_name
                                                ,description => NULL
                                                ,start_time  => SYSDATE
                                                ,sub_request => FALSE
                                                ,argument1   => p_user_name
                                                ,argument2   => p_resp_name);
    
    ELSIF p_conc_prog_name = 'XXWC_OM_DMS_FILE'
    THEN
      fnd_request.set_org_id(l_org_id);
      l_sec := 'Call Concurrent Program - XXWC DMS File Creation';
      ----------------------------------------------------------------------------------
      -- Call Concurrent Program - XXWC DMS Ship Confirm Delivered Orders
      ----------------------------------------------------------------------------------
      l_request_id := fnd_request.submit_request(application => 'XXWC'
                                                ,program     => p_conc_prog_name
                                                ,description => NULL
                                                ,start_time  => SYSDATE
                                                ,sub_request => FALSE
                                                ,argument1   => l_order_type
                                                ,argument2   => l_dummy
                                                ,argument3   => l_order_num
                                                ,argument4   => l_delivery);
    
    -- Version# 1.7 > Start 
    ELSIF p_conc_prog_name = 'XXWC_DMS_DOCLINK'
    THEN
      fnd_request.set_org_id(l_org_id);
      
      SELECT directory_path||'/pod'  INTO l_directory_path
      FROM all_directories
      WHERE directory_name = 'XXWC_OM_DMS_OB_DIR';
      
      l_sec := 'Call Concurrent Program - XXWC DMS Copy CFD to DocLink';
      ----------------------------------------------------------------------------------
      -- Call Concurrent Program - XXWC DMS Copy CFD to DocLink
      ----------------------------------------------------------------------------------
      l_request_id := fnd_request.submit_request(application => 'XXWC'
                                                ,program     => p_conc_prog_name
                                                ,description => NULL
                                                ,start_time  => SYSDATE
                                                ,sub_request => FALSE
                                                ,argument1   => l_directory_path
                                                ,argument2   => NULL
                                                ,argument3   => NULL
                                                ,argument4   => 'hdswcsalesfulfillmentalerts@hdsupply.net');
    -- Version# 1.7 < End
    
    END IF;
    COMMIT;
  
    dbms_output.put_line('Concurrent Program Request ID: ' || l_request_id);
  
    IF (l_request_id != 0)
    THEN
    
      l_sec := 'Wait for Concurrent Program ' || p_conc_prog_name;
      ----------------------------------------------------------------------------------
      -- Wait for Concurrent Program
      ----------------------------------------------------------------------------------
      IF fnd_concurrent.wait_for_request(request_id => l_request_id
                                        ,INTERVAL   => l_interval
                                        ,max_wait   => l_max_time
                                        ,phase      => l_phase
                                        ,status     => l_status
                                        ,dev_phase  => l_dev_phase
                                        ,dev_status => l_dev_status
                                        ,message    => l_message)
      THEN
        dbms_output.put_line('Phase = ' || l_phase || ' and status = ' ||
                             l_status);
        dbms_output.put_line('Request Completion Message = ' || l_message);
      
        -- return completion information
        IF l_dev_phase = 'COMPLETE'
        THEN
          CASE l_dev_status
            WHEN 'NORMAL' THEN
              p_errbuf  := l_status;
              p_retcode := '0';
            WHEN 'ERROR' THEN
              p_errbuf  := l_status;
              p_retcode := '2';
            WHEN 'WARNING' THEN
              p_errbuf  := l_status;
              p_retcode := '1';
            ELSE
              p_errbuf  := l_status;
              p_retcode := '2';
          END CASE;
        ELSE
          dbms_output.put_line('Request Has Not Completed prior to wait process max_wait');
          dbms_output.put_line('DEV Phase = ' || l_dev_phase ||
                               ' and DEV status = ' || l_dev_status);
          p_errbuf  := l_status;
          p_retcode := '1';
        END IF;
      ELSE
        dbms_output.put_line('Unable to determine completion status of concurrent program');
        p_errbuf  := 'Unable to determine completion status';
        p_retcode := '2';
      END IF;
    END IF;
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'ONT');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN OTHERS THEN
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'ONT');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
  END uc4_submit;

  /*************************************************************************
     PROCEDURE: email_errors
  
     PURPOSE:   Email notification if ship confirm process fails on an order
  
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        08/04/2014  Kathy Poling            Initial Version 
                                                    TMS 20140606-00082  
  ****************************************************************************/

  PROCEDURE email_errors(p_header_id   IN NUMBER
                        ,p_delivery_id IN NUMBER
                        ,p_error_msg   VARCHAR2) IS
  
    --Intialize Variables
  
    l_dflt_email   fnd_user.email_address%TYPE := 'hdswcsalesfulfillmentalerts@hdsupply.net';
    l_procedure    VARCHAR2(100) := 'EMAIL_ERRORS';
    l_sender       VARCHAR2(100);
    l_sid          VARCHAR2(8);
    l_body         VARCHAR2(32767);
    l_body_header  VARCHAR2(32767);
    l_body_detail  VARCHAR2(32767);
    l_body_footer  VARCHAR2(32767);
    l_sql_hint     VARCHAR2(32767);
    l_error_hint   VARCHAR2(1000);
    l_newline      NUMBER(20) := 1;
    l_mins_elapsed NUMBER(20);
    l_mins_allowed NUMBER(20) := 0;
    l_max_date     DATE;
    l_count        NUMBER;
    l_record_cnt   NUMBER := 0;
    l_err_msg      CLOB;
  
  BEGIN
  
    -- Local variables
    SELECT lower(NAME) INTO l_sid FROM v$database;
    l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
  
    l_body_header := '<style type="text/css">
.style1 {
    border-style: solid;
    border-width: 1px;
}
.style2 {
    border: 1px solid #000000;
}
.style3 {
    border: 1px solid #000000;
    background-color: #FFFF00;
}
.style4 {
    color: #FFFF00;
    border: 1px solid #000000;
    background-color: #000000;
}
.style5 {
    font-size: large;
}
.style6 {
    font-size: xx-large;
}
.style7 {
    color: #FFCC00;
}
</style>
<p><span class="style6"><span class="style7"><strong>SHIP CONFIRM</strong></span><br />
<span class="style5">ERROR REPORT FROM:   ' ||
                     upper(l_sid) ||
                     ' </span></p>
<BR><table border="2" cellpadding="2" cellspacing="2" width="100%">' ||
                     '<td class="style2"><B>ORDER HEADER ID</B></td>
                        <td class="style2"><B>DELIVERY ID</B></td>' || '
                        <td class="style2"><B>ORA ERROR MSG</B></td><tr>';
  
    l_body_detail := l_body_detail || '<td class="style3">' || p_header_id ||
                     '</td><td class="style3">' || p_delivery_id ||
                     '</td><td class="style3">' ||
                     regexp_replace(p_error_msg, '[[:cntrl:]]', NULL) ||
                     '</td><TR>';
  
    l_body_footer := '</tr></table><br>Please manually ship confirm this order.<BR>';
    l_body        := l_body_header || l_body_detail || l_body_footer;
  
    xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                             ,p_from          => l_sender
                             ,p_text          => 'test'
                             ,p_subject       => '***ERROR IN SHIP CONFIRM PROCESS***'
                             ,p_html          => l_body
                             ,p_smtp_hostname => g_host
                             ,p_smtp_portnum  => g_hostport);
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := 'Error_Stack...' || dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => 'Email ship confirm error'
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_dflt_email
                                          ,p_module            => 'ONT');
    
    WHEN OTHERS THEN
      l_err_msg := 'Error_Stack...' || dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => 'Email ship confirm error'
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_dflt_email
                                          ,p_module            => 'ONT');
    
  END email_errors;

  /********************************************************************************
  -- PROCEDURE: SHIP_CONFIRM_SIG_CAPT
  --
  -- PURPOSE: Ship Confirm standard orders that doesn't have any exceptions and
  --          Picked up at the branch.  Based on the customer signature captured 
  --          at the branch 
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.2     20-Aug-2014   Kathy Poling    Created this package. TMS 20140606-00082
  -- 1.3     16-Jan-2015   Kathy Poling    TMS 20150118-00002 to address the date issue 
                                           in the program for scheduling the concurrent
                                           request in the procedure ship_confirm_sig_capt
  -- 1.4     23-Jan-2015   Kathy Poling    TMS 20150123-00064 change to exlude lines
                                           that signature is captured as ***CANCEL***
  -- 1.10    04-Apr-2015   Gopi Damuluri   TMS# 20150227-00035 - Peformance Tuning of SHIP_CONFIRM_SIG_CAPT process
                                           Added new procedure - UPDATE_OFD_DELIVERY_RUN
                                           TMS# 20150226-00083 - Performance issue for XXWC DMS Ship Confirm Delivered Orders
                                           TMS# 20150226-00084 - DMS - PERF fix for XXWC DMS Ship Confirm Signature Captured Orders
  -- 1.11    12-May-2015   Gopi Damuluri   TMS# 20150512-00161 - DMS bug fix related to payment hold
  *******************************************************************************/

  PROCEDURE ship_confirm_sig_capt(p_errbuf              OUT VARCHAR2
                                 ,p_retcode             OUT VARCHAR2
                                 ,p_user_name           IN VARCHAR2
                                 ,p_responsibility_name IN VARCHAR2
                                 ,p_run_zone            IN VARCHAR2
                                 ,p_date                IN VARCHAR2) IS
  
    l_user_id             fnd_user.user_id%TYPE;
    l_org_id              NUMBER := mo_global.get_current_org_id;
    l_request_id          NUMBER := fnd_global.conc_request_id;
    l_responsibility_id   NUMBER;
    l_resp_application_id NUMBER;
    l_err_msg             CLOB;
    l_sec                 VARCHAR2(110) DEFAULT 'START';
    l_procedure           VARCHAR2(50) := 'SHIP_CONFIRM_SIG_CAPT';
    l_return_status       VARCHAR2(1);
    l_msg_data            VARCHAR2(2000);
    l_last_delta_date     DATE;
    l_hold_count          NUMBER;
    l_from_org_id         NUMBER;
    l_pick_rule           VARCHAR2(50);
    l_order_type          VARCHAR2(100);
    l_ship_from           VARCHAR2(100);
    l_exists              VARCHAR2(1) DEFAULT 'F';
    l_exception_exists    VARCHAR2(1) DEFAULT 'F';
    l_ship                VARCHAR2(1) DEFAULT 'N';
    l_shipping_method     NUMBER;
    l_qty                 NUMBER;
    l_count_status        NUMBER;
    l_date                DATE;  --Version 1.3
  
    l_sender      VARCHAR2(100);
    l_sid         VARCHAR2(8);
    l_body        VARCHAR2(32767);
    l_body_header VARCHAR2(32767);
    l_body_detail VARCHAR2(32767);
    l_body_footer VARCHAR2(32767);
  
  BEGIN
  fnd_file.put_line(fnd_file.log, '*** 1 *** run time: '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
  fnd_file.put_line(fnd_file.log, 'p_run_zone : '||p_run_zone);
  
  IF l_org_id IS NULL OR l_org_id = -1 OR l_org_id = -99 THEN
     l_org_id := fnd_profile.value('org_id');
  END IF;
  
    l_sec := 'Set policy context for org_id';
    -- Mandatory initialization for R12
    mo_global.set_policy_context('S', l_org_id);
    mo_global.init('ONT');
  
    -- Deriving Ids from initalization variables
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM apps.fnd_user
       WHERE user_name = upper(p_user_name)
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'UserName ' || p_user_name || ' not defined in Oracle';
        RAISE program_error;
    END;
  
    BEGIN
      SELECT responsibility_id, application_id
        INTO l_responsibility_id, l_resp_application_id
        FROM apps.fnd_responsibility_vl
       WHERE responsibility_name = p_responsibility_name
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Responsibility ' || p_responsibility_name ||
                     ' not defined in Oracle';
        RAISE program_error;
    END;
  
    --Version 1.3
    IF p_date IS NULL
    THEN
      SELECT trunc(SYSDATE) INTO l_date FROM dual;
    ELSE
      l_date := p_date;
    END IF;
    
    fnd_file.put_line(fnd_file.log, 'Responsibility: '||p_responsibility_name);
    fnd_file.put_line(fnd_file.log, 'Run zone: '||p_run_zone);
    fnd_file.put_line(fnd_file.log, 'Run for date: '||l_date);
    fnd_file.put_line(fnd_file.log, 'Date Passed with parm: '||p_date);
    -- 1/16/15
    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    l_sec := 'Apps Initialize';
  
    fnd_global.apps_initialize(l_user_id
                              ,l_responsibility_id
                              ,l_resp_application_id);
  
    l_sec := 'Loop for updating delivery_id';
  
  fnd_file.put_line(fnd_file.log, '*** 2 *** run time: '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
    FOR cur_delivery IN (SELECT sc.id
                               ,sc.request_id
                                --,plt.argument10
                               ,cr.argument10
                               ,sc.delivery_id
                           FROM xxwc.xxwc_signature_capture_tbl sc
                               ,oe_order_headers                oh
                                --,xxwc.xxwc_print_log_tbl         plt
                               ,fnd_concurrent_requests cr
                          WHERE trunc(sc.creation_date) = l_date  --Version 1.3
                            AND sc.id = oh.header_id
                            AND nvl(sc.delivery_id, -1) = -1
                            AND oh.order_type_id IN
                                (SELECT /*+ RESULT_CACHE */
                                  lookup_code
                                   FROM fnd_lookup_values
                                  WHERE 1 = 1
                                    AND lookup_type =
                                        'XXWC_DMS_ORDERTYPE_SHIPCONFIRM'
                                    AND enabled_flag = 'Y'
                                    AND SYSDATE BETWEEN start_date_active AND
                                        nvl(end_date_active, SYSDATE + 1))
                               --AND sc.request_id = plt.request_id
                            AND sc.request_id = cr.request_id
                            FOR UPDATE OF sc.delivery_id skip locked)
    
    LOOP
    
      UPDATE xxwc.xxwc_signature_capture_tbl
         SET delivery_id = cur_delivery.argument10
       WHERE id = cur_delivery.id
         AND request_id = cur_delivery.request_id;
    
    END LOOP;
  
  fnd_file.put_line(fnd_file.log, '*** 3 *** run time: '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
  
   -- Version# 1.10 > Start
    COMMIT;
    BEGIN

    INSERT INTO xxwc.xxwc_signature_capture_gtt_tbl
    SELECT * 
      FROM xxwc.xxwc_signature_capture_tbl 
     WHERE 1 = 1
       -- AND creation_date = l_date 
       AND signature_name != '***CANCEL***';

    EXCEPTION
    WHEN OTHERS THEN
      RAISE program_error;
      l_err_msg := 'Error populating the XXWC_SIGNATURE_CAPTURE_GTT_TBL - '||SQLERRM;
    END;
   -- Version# 1.10 < End

-- Version# 1.11 > Start
   BEGIN
     UPDATE xxwc.xxwc_signature_capture_gtt_tbl stg
        SET stg.ship_confirm_status           = 'ERROR'
          , stg.ship_confirm_exception        = 'PAYMENT_HOLD_EXISTS'
      WHERE 1 = 1 
         AND stg.delivery_id         IS NOT NULL
         AND stg.ship_confirm_status IS NULL
         AND EXISTS (SELECT '1' 
                       FROM apps.oe_hold_sources_all ohs
                      WHERE 1 = 1
                        AND to_char(stg.id)        = ohs.hold_entity_id
                        AND ohs.hold_entity_code   = 'O'
                        AND ohs.hold_id           IN (13,14)
                        AND ohs.released_flag      = 'N');
   EXCEPTION
   WHEN OTHERS THEN
      RAISE program_error;
      l_err_msg := 'Error updating XXWC_SIGNATURE_CAPTURE_GTT_TBL with Payment Hold Exceptions - '||SQLERRM;
   END;
-- Version# 1.11 < End

fnd_file.put_line(fnd_file.log, '*** 4 *** run time: '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

    l_sec := 'Loop for ship confirm process';
    FOR cur_ship IN (SELECT DISTINCT sc.id
                                    ,sc.delivery_id
                                    ,sc.creation_date
                                    ,oh.header_id
                                    ,sc.signature_name
                                   -- ,ot.name order_type              -- Version# 1.10
                                    ,oh.order_type_id
                                    ,oh.flow_status_code
                                    -- ,l.timezone_code                -- Version# 1.10
                                    -- ,l.timezone_display             -- Version# 1.10
                       FROM xxwc.xxwc_signature_capture_gtt_tbl sc
                           ,oe_order_headers                oh
                          -- ,oe_transaction_types_tl         ot
                          -- ,hr_locations_v                  l
                      WHERE 1 = 1
                        -- AND trunc(sc.creation_date) = l_date  --Version 1.3 -- Version# 1.10
                        AND oh.flow_status_code = 'BOOKED'
                        AND sc.id = oh.header_id                        
                        -- AND oh.order_type_id = ot.transaction_type_id  -- Version# 1.10
                        -- AND nvl(ship_confirm_status, 'x') = 'x'        -- Version# 1.10
                        AND ship_confirm_status IS NULL                          -- Version# 1.10
                        -- AND l.inventory_organization_id = oh.ship_from_org_id -- Version# 1.10
                        -- AND bill_to_site_flag = 'Y'                           -- Version# 1.10
                        -- AND sc.signature_name <> '***CANCEL***'
                        AND oh.order_type_id IN (1001, 1021) 
-- Version# 1.10 > Start
                        AND EXISTS (SELECT /*+ RESULT_CACHE */  flv.lookup_code
                                      FROM fnd_lookup_values flv
                                     WHERE 1 = 1
                                       AND flv.lookup_type           = 'XXWC_DMS_BRANCH_LIST'
                                       AND flv.enabled_flag          = 'Y'
                                       AND to_number(flv.meaning)    = oh.ship_from_org_id
                                       AND (p_run_zone IS NULL OR flv.tag = p_run_zone)
                                       AND TRUNC(SYSDATE) BETWEEN flv.start_date_active AND nvl(flv.end_date_active, SYSDATE + 1))
-- Version# 1.10 < End
--                            (SELECT /*+ RESULT_CACHE */
--                              lookup_code
--                               FROM fnd_lookup_values
--                              WHERE 1 = 1
--                                AND lookup_type =
--                                    'XXWC_DMS_ORDERTYPE_SHIPCONFIRM'
--                                AND enabled_flag = 'Y'
--                                AND SYSDATE BETWEEN start_date_active AND
--                                    nvl(end_date_active, SYSDATE + 1))
-- Version# 1.10 > Start    
--                        AND l.timezone_display IN
--                            (SELECT /*+ RESULT_CACHE */
--                              meaning
--                               FROM fnd_lookup_values
--                              WHERE 1 = 1
--                                AND lookup_type =
--                                    'XXWC_DMS_SHIP_CONFIRM_SIGN_CAP'
--                                AND enabled_flag = 'Y'
--                                AND SYSDATE BETWEEN start_date_active AND
--                                    nvl(end_date_active, SYSDATE + 1)
--                                   --AND lookup_code = p_time_zone
--                                AND description = p_run_zone)
-- Version# 1.10 < End
                        AND sc.creation_date IN
                            (SELECT MAX(creation_date)
                               FROM xxwc.xxwc_signature_capture_gtt_tbl sc2
                              WHERE sc2.id = sc.id
                                AND sc2.delivery_id = sc.delivery_id
                                -- AND sc2.signature_name <> '***CANCEL***' -- Version# 1.10
                                )
                     /*SELECT DISTINCT sc.id
                                      ,plt.argument10      delivery_id
                                      ,sc.creation_date
                                      ,oh.header_id
                                      ,sc.signature_name
                                      ,ot.name             order_type
                                      ,oh.order_type_id
                                      ,oh.flow_status_code
                                      ,l.timezone_code
                                      ,l.timezone_display
                                      ,sc.group_id
                                      ,sc.request_id
                         FROM xxwc.xxwc_signature_capture_gtt_tbl sc
                             ,oe_order_headers                oh
                             ,oe_transaction_types_tl         ot
                             ,hr_locations_v                  l
                             ,xxwc.xxwc_print_log_tbl         plt
                        WHERE trunc(sc.creation_date) = p_date
                          AND sc.id = oh.header_id
                          AND oh.flow_status_code = 'BOOKED'
                          AND oh.order_type_id = ot.transaction_type_id
                          AND nvl(ship_confirm_status, 'x') = 'x'
                          AND l.inventory_organization_id =
                              oh.ship_from_org_id
                          AND bill_to_site_flag = 'Y'
                          AND oh.order_type_id IN
                              (SELECT lookup_code
                                 FROM fnd_lookup_values
                                WHERE 1 = 1
                                  AND lookup_type =
                                      'XXWC_DMS_ORDERTYPE_SHIPCONFIRM'
                                  AND enabled_flag = 'Y'
                                  AND SYSDATE BETWEEN start_date_active AND
                                      nvl(end_date_active, SYSDATE + 1))
                          AND l.timezone_display IN
                              (SELECT meaning
                                 FROM fnd_lookup_values
                                WHERE 1 = 1
                                  AND lookup_type =
                                      'XXWC_DMS_SHIP_CONFIRM_SIGN_CAP'
                                  AND enabled_flag = 'Y'
                                  AND SYSDATE BETWEEN start_date_active AND
                                      nvl(end_date_active, SYSDATE + 1)
                                     --AND lookup_code = p_time_zone
                                  AND description = p_run_zone)
                          AND sc.creation_date IN
                              (SELECT MAX(creation_date)
                                 FROM xxwc.xxwc_signature_capture_gtt_tbl sc2
                                WHERE sc2.id = sc.id
                                     --AND sc2.delivery_id = sc.delivery_id
                                  AND trunc(sc2.creation_date) = p_date)
                          AND sc.request_id = plt.request_id
                     */
                     )
    LOOP
    fnd_file.put_line(fnd_file.log, '*** 5 *** run time: '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
      l_hold_count       := NULL;
      l_return_status    := NULL;
      l_from_org_id      := NULL;
      l_ship_from        := NULL;
      l_exists           := 'F';
      l_exception_exists := 'F';
      l_ship             := 'N';
      l_shipping_method  := NULL;
      l_msg_data         := NULL; -- Version# 1.10
    
      fnd_file.put_line(fnd_file.log
                       ,'order ID = ' || cur_ship.id || ' delivery id = ' ||
                        cur_ship.delivery_id);
    
      /*      BEGIN
          SELECT 'Y'
            INTO l_ship
            FROM fnd_lookup_values
           WHERE 1 = 1
             AND lookup_type = 'XXWC_DMS_ORDERTYPE_SHIPCONFIRM'
             AND enabled_flag = 'Y'
             AND SYSDATE BETWEEN start_date_active AND
                 nvl(end_date_active, SYSDATE + 1)
             AND lookup_code = cur_ship.order_type_id;
        
        EXCEPTION
          WHEN no_data_found THEN
            l_ship := 'N';
        END;
      
        IF l_ship = 'Y'
        THEN
      */
      --Need ship_from_org for the order header_id/delivery_id to ship confirm lines 
      BEGIN
      
        SELECT DISTINCT ship_from_org_id
          INTO l_from_org_id
          FROM xxwc.xxwc_wsh_shipping_stg
         WHERE header_id = cur_ship.header_id
           AND delivery_id = cur_ship.delivery_id;
        --dbms_output.put_line('l_from_org_id = ' ||l_from_org_id);
      EXCEPTION
        WHEN no_data_found THEN
          l_from_org_id := -1;
        
        WHEN too_many_rows THEN
          l_from_org_id := -1;
        
      END;
    
      IF l_from_org_id = -1
      THEN
      
        --lines cannot ship confirm issue with getting ship_from_org_id
        UPDATE xxwc.xxwc_signature_capture_gtt_tbl
           SET ship_confirm_exception = 'Delivery does not exists in shipping tbl'
              ,ship_confirm_status    = 'ERROR'
              ,last_update_date       = SYSDATE
              ,last_updated_by        = l_user_id
         WHERE id = cur_ship.id
           AND delivery_id = cur_ship.delivery_id
              --AND group_id = cur_ship.group_id
              --AND request_id = cur_ship.request_id
           AND trunc(creation_date) = l_date;   --Version 1.3
      
      ELSE
      
        --Check ordered line status
        l_count_status := line_status(cur_ship.header_id
                                     ,cur_ship.delivery_id);
      
        --fnd_file.put_line(fnd_file.log ,'Line status count = ' || l_count_status);
      
        IF l_count_status > 0
        THEN
        
          --lines cannot ship confirm issue with line status
          UPDATE xxwc.xxwc_signature_capture_gtt_tbl
             SET ship_confirm_exception = 'At least one line already delivered'
                ,ship_confirm_status    = 'ERROR'
                ,last_update_date       = SYSDATE
                ,last_updated_by        = l_user_id
           WHERE id = cur_ship.id
             AND delivery_id = cur_ship.delivery_id
                --AND group_id = cur_ship.group_id
                --AND request_id = cur_ship.request_id
             AND trunc(creation_date) = l_date;   --Version 1.3
        
        ELSE
        
          --Check ordered qty to deliver qty
          BEGIN
            SELECT SUM(ola.ordered_quantity) - SUM(stg.transaction_qty)
              INTO l_qty
              FROM oe_order_lines ola, xxwc_wsh_shipping_stg stg
             WHERE stg.delivery_id = cur_ship.delivery_id
               AND stg.header_id = cur_ship.header_id
               AND stg.header_id = ola.header_id
               AND stg.line_id = ola.line_id;
          
          EXCEPTION
            WHEN OTHERS THEN
              l_qty := -1;
            
          END;
        
          IF l_qty <> 0
          THEN
          
            --lines cannot ship confirm issue with ordered qty vs delivered qty
            UPDATE xxwc.xxwc_signature_capture_gtt_tbl
               SET ship_confirm_exception = 'QTY did not match on at least one line'
                  ,ship_confirm_status    = 'ERROR'
                  ,last_update_date       = SYSDATE
                  ,last_updated_by        = l_user_id
             WHERE id = cur_ship.id
               AND delivery_id = cur_ship.delivery_id
                  --AND group_id = cur_ship.group_id
                  --AND request_id = cur_ship.request_id
               AND trunc(creation_date) = l_date;   --Version 1.3
          
          ELSE
          
            -- need pick rule for ship comfirm    
            SELECT organization_code
              INTO l_ship_from
              FROM org_organization_definitions
             WHERE organization_id = l_from_org_id;
          
            l_pick_rule := l_ship_from || ' Standard'; -- Version# 1.10
/* -- Version# 1.10 > Start
            IF cur_ship.order_type LIKE '%STANDARD%'
            THEN
              l_pick_rule := l_ship_from || ' Standard'; --' Standard'; --'-Unreleased Std';
            ELSE
              l_pick_rule := l_ship_from || '-All Internal';
            END IF;
*/ -- Version# 1.10 < End
          
            --dbms_output.put_line('l_pick_rule:  '||l_pick_rule);
          
            -- Check if order is on hold
            SELECT xxwc_ont_routines_pkg.count_order_holds(cur_ship.header_id)
              INTO l_hold_count
              FROM dual;
          
            IF l_hold_count > 0
            THEN
              --lines cannot ship confirm delivery with holds
              UPDATE xxwc.xxwc_signature_capture_gtt_tbl
                 SET ship_confirm_exception = 'Holds exists for Order'
                    ,ship_confirm_status    = 'ERROR'
                    ,last_update_date       = SYSDATE
                    ,last_updated_by        = l_user_id
               WHERE id = cur_ship.id
                 AND delivery_id = cur_ship.delivery_id
                    --AND group_id = cur_ship.group_id
                    --AND request_id = cur_ship.request_id
                 AND trunc(creation_date) = l_date;   --Version 1.3
            
            ELSE
            
              -- Prevent Ship Confirm if there is a special with item cost zero    
              BEGIN
              
                SELECT 'T'
                  INTO l_exists
                  FROM xxwc_wsh_shipping_stg_v
                 WHERE header_id = cur_ship.header_id
                   AND delivery_id = cur_ship.delivery_id
                   AND item_type IN ('SPECIAL', 'REPAIR')
                   AND nvl(item_cost, 0) = 0
                   AND status = 'OUT_FOR_DELIVERY'
                   AND rownum = 1;
              
                --dbms_output.put_line('l_exists:  '||l_exists);    
              EXCEPTION
                WHEN no_data_found THEN
                  l_exists := 'F';
              END;
            
              IF l_exists = 'T'
              THEN
                --lines cannot ship confirm delivery with SPECIAL and no cost
                UPDATE xxwc.xxwc_signature_capture_gtt_tbl
                   SET ship_confirm_exception = 'One line is SPECIAL/REPAIR and no cost'
                      ,ship_confirm_status    = 'ERROR'
                      ,last_update_date       = SYSDATE
                      ,last_updated_by        = l_user_id
                 WHERE id = cur_ship.id
                   AND delivery_id = cur_ship.delivery_id
                      --AND group_id = cur_ship.group_id
                      --AND request_id = cur_ship.request_id
                   AND trunc(creation_date) = l_date;   --Version 1.3
              
              ELSE
              
                BEGIN
                  --check if delivery has multiple ship method codes
                  SELECT COUNT(DISTINCT shipping_method_code)
                    INTO l_shipping_method
                    FROM oe_order_lines ol
                   WHERE header_id = cur_ship.header_id
                     AND ol.header_id IN
                         (SELECT header_id
                            FROM xxwc.xxwc_wsh_shipping_stg ws
                           WHERE ws.header_id = ol.header_id
                             AND ws.line_id = ol.line_id --Version 1.1 
                             AND delivery_id = cur_ship.delivery_id);
                
                  --'Cannot Ship Confirm an order with multiple shipping methods for a delivery.'      
                EXCEPTION
                  WHEN no_data_found THEN
                    l_shipping_method := -1;
                END;
              
                IF l_shipping_method <> 1
                THEN
                  --lines cannot ship confirm 
                  UPDATE xxwc.xxwc_signature_capture_gtt_tbl
                     SET ship_confirm_exception = 'Delivery has multiple shipping methods'
                        ,ship_confirm_status    = 'ERROR'
                        ,last_update_date       = SYSDATE
                        ,last_updated_by        = l_user_id
                   WHERE id = cur_ship.id
                     AND delivery_id = cur_ship.delivery_id
                        --AND group_id = cur_ship.group_id
                        --AND request_id = cur_ship.request_id
                     AND trunc(creation_date) = l_date;   --Version 1.3
                
                ELSE
                
                  xxwc_wsh_shipping_extn_pkg.ship_confirm_order(p_header_id     => cur_ship.header_id
                                                               ,p_from_org_id   => l_from_org_id
                                                               ,p_delivery_id   => cur_ship.delivery_id
                                                               ,p_pick_rule     => l_pick_rule
                                                               ,p_return_status => l_return_status
                                                               ,p_return_msg    => l_msg_data);
                
                  l_err_msg := 'delivery_id = ' || cur_ship.delivery_id ||
                               ' status = ' || l_return_status ||
                               '  -  Return MSG = ' || l_msg_data;
                
                  /*   fnd_file.put_line(fnd_file.log
                                     ,'Status =  ' || l_return_status ||
                                      '   -  Return MSG = ' || l_msg_data);
                  */
                  IF l_return_status = 'S'
                  THEN
                  
                    l_sec := 'Insert xxwc_om_fulfill_acceptance for header_id: ' ||
                             cur_ship.header_id || ' Delivery_ID: ' ||
                             cur_ship.delivery_id;
                  
                    INSERT INTO xxwc.xxwc_om_fulfill_acceptance
                      (header_id
                      ,accepted_by
                      ,accepted_date
                      ,accepted_signature
                      ,acceptance_comments
                      ,process_flag
                      ,error_msg
                      ,creation_date
                      ,created_by
                      ,last_update_date
                      ,last_updated_by
                      ,last_update_login
                      ,line_id)
                      SELECT x1.header_id -- Header_id
                            ,l_user_id -- Accepted_by
                            ,cur_ship.creation_date -- Acceptance Date
                            ,cur_ship.signature_name -- Accepted Signature
                            ,NULL -- Acceptance Comments
                            ,'N' -- Process Flag
                            ,NULL -- Error Message
                            ,SYSDATE -- Creation Date
                            ,l_user_id -- created by
                            ,SYSDATE -- last_update_date
                            ,l_user_id -- last_updated_by
                            ,NULL -- last_update_login
                            ,x1.line_id -- line_id
                        FROM xxwc.xxwc_wsh_shipping_stg x1
                       WHERE x1.header_id = cur_ship.header_id
                         AND x1.delivery_id = cur_ship.delivery_id
                         AND x1.status = 'DELIVERED';
                  
                    -- Call the procedure to sync lines to delivered 
                    l_sec := 'sync_del_line_status for header_id: ' ||
                             cur_ship.header_id || 'delivery_id: ' ||
                             cur_ship.delivery_id;
                  
                    xxwc_wsh_shipping_extn_pkg.sync_del_line_status(errbuf            => p_errbuf
                                                                   ,retcode           => p_retcode
                                                                   ,p_header_id       => cur_ship.header_id
                                                                   ,p_last_delta_date => l_last_delta_date);
                  
                    --ship confirmed 
                    l_sec := 'update status for header_id: ' ||
                             cur_ship.header_id || 'delivery_id: ' ||
                             cur_ship.delivery_id;
                  
                    UPDATE xxwc.xxwc_signature_capture_gtt_tbl
                       SET ship_confirm_status = 'COMPLETED'
                          ,last_update_date    = SYSDATE
                          ,last_updated_by     = l_user_id
                     WHERE id = cur_ship.id
                       AND delivery_id = cur_ship.delivery_id
                          --AND group_id = cur_ship.group_id
                          --AND request_id = cur_ship.request_id
                       AND trunc(creation_date) = l_date;   --Version 1.3
                  
                  ELSE
                    UPDATE xxwc.xxwc_signature_capture_gtt_tbl
                       SET ship_confirm_status    = 'ERROR'
                          ,ship_confirm_exception = l_msg_data
                          ,last_update_date       = SYSDATE
                          ,last_updated_by        = l_user_id
                     WHERE id = cur_ship.id
                       AND delivery_id = cur_ship.delivery_id
                          --AND group_id = cur_ship.group_id
                          --AND request_id = cur_ship.request_id
                       AND trunc(creation_date) = l_date;   --Version 1.3
                  
                    --email information if ship confirm fails
                    email_errors(cur_ship.id --change this to order number
                                ,cur_ship.delivery_id
                                ,l_msg_data);
                  
                  END IF; --ship confirm
                END IF; --shipping method
              END IF; --special
            END IF; --holds 
          END IF; --QTY issue 
        END IF; --line status       
      END IF; --ship from org
    
    fnd_file.put_line(fnd_file.log, 'l_msg_data: '||l_msg_data);
    
    END LOOP;
    fnd_file.put_line(fnd_file.log, '*** 6 *** run time: '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
    -- Version# 1.10 > Start
    BEGIN
        UPDATE xxwc.xxwc_signature_capture_tbl stg
           SET (ship_confirm_exception ,ship_confirm_status ,last_update_date ,last_updated_by) = (SELECT ship_confirm_exception 
                                                                                                        , ship_confirm_status 
                                                                                                        , last_update_date 
                                                                                                        , last_updated_by 
                                                                                                     FROM xxwc.xxwc_signature_capture_gtt_tbl gtt
                                                                                                    WHERE 1 = 1
                                                                                                      AND stg.id          = gtt.id
                                                                                                      AND stg.delivery_id = gtt.delivery_id
                                                                                                      AND stg.group_id    = gtt.group_id
                                                                                                      AND rownum          = 1)
         WHERE 1 = 1
           AND ship_confirm_status IS NULL
           AND trunc(creation_date) = l_date;
    EXCEPTION
    WHEN OTHERS THEN
      RAISE program_error;
    END;
    -- Version# 1.10 < End
    fnd_file.put_line(fnd_file.log, '*** 7 *** run time: '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
    COMMIT;
    
    update_ofd_delivery_run(NULL); -- Version# 1.10 -- $$$$$
    fnd_file.put_line(fnd_file.log, '*** 8 *** run time: '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
    COMMIT;
    
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'ONT');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
    WHEN OTHERS THEN
      l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      dbms_output.put_line(l_err_msg);
      -- Calling ERROR API
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  l_procedure
                                          ,p_calling           => l_sec
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'ONT');
    
      p_retcode := 2;
      p_errbuf  := l_err_msg;
    
  END ship_confirm_sig_capt;

  /********************************************************************************
  -- FUNCTION: LINE_STATUS
  --
  -- PURPOSE: Ship Confirm Signature Captured standard orders that doesn't have 
  --          any exceptions and Picked up at the branch using customer signature  
  --          captured at the branch will be ship confirmed but need to validate
  --          the line status for all of the delivery hasn't be delivered manually
  --          by a user
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.2     20-Aug-2014   Kathy Poling    Created this package. TMS 20140606-00082
  *******************************************************************************/

  FUNCTION line_status(p_header_id IN NUMBER, p_delivery_id IN NUMBER)
    RETURN NUMBER IS
    l_count_status NUMBER;
  
  BEGIN
    SELECT COUNT(ola.user_item_description)
      INTO l_count_status
      FROM oe_order_lines ola, xxwc_wsh_shipping_stg stg
     WHERE stg.delivery_id = p_delivery_id
       AND stg.header_id = p_header_id
       AND stg.header_id = ola.header_id
       AND stg.line_id = ola.line_id
       AND ola.user_item_description = 'DELIVERED'
       AND rownum = 1;
  
    RETURN(nvl(l_count_status, 0));
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN(0);
    
  END line_status;

-- Version# 1.10 > Start
  /********************************************************************************
  -- PROCEDURE: update_ofd_delivery_run
  --
  -- PURPOSE: Used to fix Sales Order lines with incompatible line FLOW_STATUS_CODE
  -- and USER_ITEM_DESCRIPTION
  --
  -- HISTORY
  -- ===========================================================================
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
  -- 1.10    04-Apr-2015   Gopi Damuluri   TMS# 20150227-00035 - Peformance Tuning of SHIP_CONFIRM_SIG_CAPT process
  --                                       Added new procedure - UPDATE_OFD_DELIVERY_RUN
  *******************************************************************************/
    PROCEDURE update_ofd_delivery_run (p_header_id NUMBER)
    IS
        v_delivery_status   VARCHAR2 (128);
    BEGIN
        UPDATE oe_order_lines          ool
           SET user_item_description = 'DELIVERED'
             , attribute16           = 'D'
         WHERE 1 = 1 
           AND TRUNC(ool.last_update_date)           = TRUNC(SYSDATE)
           AND (p_header_id IS NULL OR ool.header_id = p_header_id)
           AND ool.flow_status_code                  IN ('CLOSED', 'SHIPPED', 'PRE-BILLING_ACCEPTANCE', 'INVOICE_HOLD')
           AND ool.user_item_description             IN ('OUT_FOR_DELIVERY')
           AND apps.xxwc_om_force_ship_pkg.is_row_locked (ool.rowid, 'OE_ORDER_LINES_ALL') = 'N';

        COMMIT;
    EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom || '.' ||
                                                                  'UPDATE_OFD_DELIVERY_RUN'
                                          ,p_calling           => ''
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr('SQLERRM'
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr('SQLERRM'
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => g_distro_list
                                          ,p_module            => 'ONT');

    END update_ofd_delivery_run;
-- Version# 1.10 < End

END xxwc_om_dms_pkg;
/