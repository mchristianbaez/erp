/*************************************************************************
 $Header TMS_20180801-00028_UPDATE_ORDER_HEADER.sql $
 Module Name: TMS_20180801-00028_UPDATE_ORDER_HEADER.sql

 PURPOSE: Delete the issue order

 REVISIONS:
 Ver        Date       Author           Description
 --------- ----------  ---------------  -------------------------
 1.0       08/22/2018  Pattabhi Avula   TMS#20180801-00028 - Order #29141704
 **************************************************************************/
  SET SERVEROUTPUT ON SIZE 1000000;
  DECLARE
  
  l_count        NUMBER:=0;
  BEGIN
  DBMS_OUTPUT.put_line ('TMS: Datafix script    , Before Update');
-- Updating the headers table
 
UPDATE apps.oe_order_headers_all
   SET FLOW_STATUS_CODE='CLOSED'
      ,open_Flag='N'
 WHERE header_id =75129575; 
l_count:=SQL%ROWCOUNT;
DBMS_OUTPUT.put_line ('Records updated - ' || l_count);
		   
COMMIT;

	  DBMS_OUTPUT.put_line ('TMS: 20180801-00028  , End Update');
EXCEPTION
 WHEN OTHERS THEN
 DBMS_OUTPUT.put_line ('TMS: 20180801-00028 , Errors : ' || SQLERRM);
END;
/