/***********************************************************************************************************************************************
   NAME:     TMS_20180928-00004_Datafix
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        09/28/2018  Rakesh Patel     TMS_20180928-00004_Datafix
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Select');

   SELECT count(1) 
   FROM apps.oe_order_headers_all 
   WHERE rownum <=10;

   DBMS_OUTPUT.put_line ('Records count -' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to get records count ' || SQLERRM);
	  ROLLBACK;
END;
/