/*
 TMS:  20170713-00081
 Date: 08/30/2017
 Notes:  Backup the 130 receipts from rebates history table, copy them into curr table and then delete from the history table
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 v_db varchar2(30) :=Null;
 v_ok boolean :=Null;
 --
 type t_rbt_hist_type is table of xxcus.xxcus_rebate_recpt_history_tbl%rowtype index by binary_integer;
 c_RR_adj_rcpts_rec t_rbt_hist_type;
 l_sql varchar2(4000) :=Null;
 --
 cursor c_RR_adj_rcpts is
 SELECT *
  FROM xxcus.xxcus_rebate_recpt_history_tbl
WHERE     1 = 1
       AND bu_nm = 'REPAIR_REMODEL'
       AND vndr_recpt_unq_nbr in ('5637172969',
'5637177136',
'5637183583',
'5637183072',
'5637183070',
'5637176545',
'5637176547',
'5637173342',
'5637179006',
'5637183606',
'5637178993',
'5637182599',
'5637178996',
'5637183603',
'5637183076',
'5637182594',
'5637178998',
'5637183079',
'5637183596',
'5637182602',
'5637183077',
'5637183601',
'5637183083',
'5637183598',
'5637182603',
'5637183605',
'5637183599',
'5637183543',
'5637183542',
'5637183544',
'5637183541',
'5637176322',
'5637176321',
'5637183075',
'5637182600',
'5637183081',
'5637182597',
'5637179003',
'5637179002',
'5637183600',
'5637179000',
'5637179230',
'5637177942',
'5637177940',
'5637183584',
'5637177206',
'5637173285',
'5637177207',
'5637175645',
'5637176869',
'5637181537',
'5637177858',
'5637176546',
'5637176548',
'5637183069',
'5637183071',
'5637183068',
'5637181528',
'5637173232',
'5637177409',
'5637177411',
'5637177410',
'5637177412',
'5637176842',
'5637175866',
'5637176510',
'5637176504',
'5637177803',
'5637175872',
'5637177804',
'5637176506',
'5637176500',
'5637176846',
'5637177809',
'5637177805',
'5637176505',
'5637175868',
'5637176503',
'5637176502',
'5637176511',
'5637176840',
'5637177808',
'5637175864',
'5637175865',
'5637176844',
'5637175869',
'5637175874',
'5637176507',
'5637176501',
'5637176867',
'5637175646',
'5637175647',
'5637182062',
'5637183186',
'5637176845',
'5637176509',
'5637175870',
'5637175871',
'5637176508',
'5637176841',
'5637176847',
'5637175873',
'5637176848',
'5637177806',
'5637176843',
'5637177807',
'5637175867',
'5637172971',
'5637172970',
'5637172968',
'5637172967',
'5637181913',
'5637183074',
'5637183080',
'5637182595',
'5637183595',
'5637178999',
'5637182601',
'5637178995',
'5637179004',
'5637183597',
'5637183594',
'5637183602',
'5637183073',
'5637182598',
'5637182596',
'5637178994',
'5637183604',
'5637178997',
'5637183078')
;
 --
BEGIN --Main Processing...
   --
   n_loc :=101;
   --
   BEGIN 
     --
	 n_loc :=102;
	 --
     execute immediate 'create table xxcus.xxcus_tms_20170713_00081_bkp as  
 SELECT *
 FROM xxcus.xxcus_rebate_recpt_history_tbl
 WHERE     1 = 1
       AND bu_nm = ''REPAIR_REMODEL''
       AND vndr_recpt_unq_nbr IN
              (
                ''5637172969'',
                ''5637177136'',
                ''5637183583'',
                ''5637183072'',
                ''5637183070'',
                ''5637176545'',
                ''5637176547'',
                ''5637173342'',
                ''5637179006'',
                ''5637183606'',
                ''5637178993'',
                ''5637182599'',
                ''5637178996'',
                ''5637183603'',
                ''5637183076'',
                ''5637182594'',
                ''5637178998'',
                ''5637183079'',
                ''5637183596'',
                ''5637182602'',
                ''5637183077'',
                ''5637183601'',
                ''5637183083'',
                ''5637183598'',
                ''5637182603'',
                ''5637183605'',
                ''5637183599'',
                ''5637183543'',
                ''5637183542'',
                ''5637183544'',
                ''5637183541'',
                ''5637176322'',
                ''5637176321'',
                ''5637183075'',
                ''5637182600'',
                ''5637183081'',
                ''5637182597'',
                ''5637179003'',
                ''5637179002'',
                ''5637183600'',
                ''5637179000'',
                ''5637179230'',
                ''5637177942'',
                ''5637177940'',
                ''5637183584'',
                ''5637177206'',
                ''5637173285'',
                ''5637177207'',
                ''5637175645'',
                ''5637176869'',
                ''5637181537'',
                ''5637177858'',
                ''5637176546'',
                ''5637176548'',
                ''5637183069'',
                ''5637183071'',
                ''5637183068'',
                ''5637181528'',
                ''5637173232'',
                ''5637177409'',
                ''5637177411'',
                ''5637177410'',
                ''5637177412'',
                ''5637176842'',
                ''5637175866'',
                ''5637176510'',
                ''5637176504'',
                ''5637177803'',
                ''5637175872'',
                ''5637177804'',
                ''5637176506'',
                ''5637176500'',
                ''5637176846'',
                ''5637177809'',
                ''5637177805'',
                ''5637176505'',
                ''5637175868'',
                ''5637176503'',
                ''5637176502'',
                ''5637176511'',
                ''5637176840'',
                ''5637177808'',
                ''5637175864'',
                ''5637175865'',
                ''5637176844'',
                ''5637175869'',
                ''5637175874'',
                ''5637176507'',
                ''5637176501'',
                ''5637176867'',
                ''5637175646'',
                ''5637175647'',
                ''5637182062'',
                ''5637183186'',
                ''5637176845'',
                ''5637176509'',
                ''5637175870'',
                ''5637175871'',
                ''5637176508'',
                ''5637176841'',
                ''5637176847'',
                ''5637175873'',
                ''5637176848'',
                ''5637177806'',
                ''5637176843'',
                ''5637177807'',
                ''5637175867'',
                ''5637172971'',
                ''5637172970'',
                ''5637172968'',
                ''5637172967'',
                ''5637181913'',
                ''5637183074'',
                ''5637183080'',
                ''5637182595'',
                ''5637183595'',
                ''5637178999'',
                ''5637182601'',
                ''5637178995'',
                ''5637179004'',
                ''5637183597'',
                ''5637183594'',
                ''5637183602'',
                ''5637183073'',
                ''5637182598'',
                ''5637182596'',
                ''5637178994'',
                ''5637183604'',
                ''5637178997'',
                ''5637183078''
              )';   
	 --
     v_ok :=TRUE;
	 --    
	 n_loc :=103;
	 --
     DBMS_OUTPUT.put_line ('Backup table xxcus.xxcus_tms_20170713_00081_bkp created.');
     --
   EXCEPTION
    WHEN OTHERS THEN 
	 --
	 v_ok :=FALSE;
	 --
	 n_loc :=104;
	 --
   END;
   --
   if (v_ok) then
       -- 
       n_loc :=105;
       --
       v_ok :=Null;	   
	   --
	   begin
		--
		n_loc :=106;
		--
		open c_RR_adj_rcpts;
		fetch c_RR_adj_rcpts bulk collect into c_RR_adj_rcpts_rec;
		close c_RR_adj_rcpts;
		--
		n_loc :=107;
		--
		if c_RR_adj_rcpts_rec.count >0 then
		 --
		 n_loc :=108;
		 --
		 forall idx in 1 .. c_RR_adj_rcpts_rec.count
		 --
		 insert into xxcus.xxcus_rebate_receipt_curr_tbl values c_RR_adj_rcpts_rec(idx);
		 --
		 v_ok :=TRUE;
		 --
          for idx in 1 .. c_RR_adj_rcpts_rec.count loop
            --
            delete xxcus.xxcus_rebate_recpt_history_tbl where bu_nm = c_RR_adj_rcpts_rec(idx).bu_nm and vndr_recpt_unq_nbr =c_RR_adj_rcpts_rec(idx).vndr_recpt_unq_nbr;
            --
            dbms_output.put_line('Deleted receipt :'||c_RR_adj_rcpts_rec(idx).vndr_recpt_unq_nbr||', for BU :'||c_RR_adj_rcpts_rec(idx).bu_nm);
            --
          end loop;
          --         
		end if;
		--
		n_loc :=109;
		--
	   exception
		when others then
		 --
		 n_loc :=103;
		 --
		 dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
		 --
	   end;
	   --
   else
    DBMS_OUTPUT.put_line ('v_ok is FALSE...check xpatch log files for error messages.');
   end if;
   --
  commit;
   --
EXCEPTION
   WHEN OTHERS THEN
      DBMS_OUTPUT.put_line ('TMS:   20170713-00081, Errors =' || SQLERRM);
      rollback;
END;
/