/*
 TMS: 20160526-00054 Unable to update EHS Inactive status to two items
 */
SET SERVEROUTPUT ON SIZE 1000000


BEGIN

   /*
   SELECT wdd.source_header_id,
       wdd.source_header_number order_number,
       wdd.inventory_item_id itemid,
       wdd.source_line_number line_no,
       wdd.source_line_id,
       wdd.delivery_detail_id,
       wdd.released_status,
       ol.FLOW_STATUS_CODE,
       ol.ordered_item,
       (SELECT organization_code
          FROM org_organization_definitions
         WHERE organization_id = ol.ship_from_org_id)
          branch
  FROM wsh_delivery_details wdd, oe_order_lines_all ol
 WHERE     wdd.inventory_item_id IN (2925094, 2991964)
       AND wdd.pickable_flag = 'Y'
       AND NVL (wdd.inv_interfaced_flag, 'N') NOT IN ('Y', 'X')
       AND wdd.released_status <> 'D'
       AND ol.FLOW_STATUS_CODE = 'CANCELLED'
       AND wdd.source_line_id = ol.line_id;*/
   DBMS_OUTPUT.put_line ('Before update');

   UPDATE wsh_delivery_details
      SET released_status = 'D',
          src_requested_quantity = 0,
          requested_quantity = 0,
          shipped_quantity = 0,
          cycle_count_quantity = 0,
          cancelled_quantity =
             DECODE (requested_quantity,
                     0, cancelled_quantity,
                     requested_quantity),
          subinventory = NULL,
          serial_number = NULL,
          locator_id = NULL,
          lot_number = NULL,
          revision = NULL,
          inv_interfaced_flag = 'X',
          oe_interfaced_flag = 'X'
    WHERE delivery_detail_id IN (7499385,4883699,10885860);

   DBMS_OUTPUT.put_line ('Records updated1-' || SQL%ROWCOUNT);

   UPDATE wsh_delivery_assignments
      SET delivery_id = NULL
    WHERE delivery_detail_id IN (7499385,4883699,10885860);

   DBMS_OUTPUT.put_line ('Records updated2-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
END;
/