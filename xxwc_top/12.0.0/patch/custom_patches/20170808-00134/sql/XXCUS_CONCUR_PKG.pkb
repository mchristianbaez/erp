CREATE OR REPLACE PACKAGE BODY APPS.xxcus_concur_pkg is 
  --
 /*
   Ticket#                                                                  Date                Ver#            Author                       Notes
   -----------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS: 20170407-00204 / ESMS 554216   02/17/2017   1.0              Balaguru Seshadri  Concur process related routines  
   TMS: 20170508-00108                                 05/08/2017    1.1              Balaguru Seshadri  Bug fixes 
  TMS : 20170519-00135 / ESMS 561573  05/19/2017     1.2             OOP file failed to generate   
  TMS :  20170521-00001 / ESMS 561766 05/23/2017     1.3             Exclude any from of recaptrure tax from audit report header total and
                                                                                                                               add a new file for canadian summary tax report 
   TMS: 20170609-00120 / ESMS 570047   06/09/2017    1.4             Balaguru Seshadri  Fixes for WW actuals files from pipe to csv format and remove
                                                                                                                                 RT% lines from CI Brafasco JE detail files   
   TMS 20170628-00103                                   06/28/2017    1.5            CAD location should be Z0436   and product 50     
   TMS 20170712-00039 / RITM0011266    07/12/2017    1.6            Create out of pocket for Waterworks only and comment the same in OOP logic 
                                                                                                                                and stop creating GSC intercompany journals in GSC. Fix logic for CI CAD GP 
                                                                                                                                to pull journal eligible lines based on the sign of the posted amount     
   TMS 20170808-00134 / RITM0011184   08/10/2017     1.7            1) Split CASH USD in other totals section of the audit report to WW and Others    
                                                                                                                               2) Use rpt_vw_actual_tot_due_emp instead of rpt_vw_tot_due_emp in the OOP file generation [ WW as well as Others]
                                                                                                                               3) Fix CI CAD GP summary report                                                                                                                                                                                                                                                                                                                                                                             
 */
  --
  g_pkg_name varchar2(30) :='XXCUS_CONCUR_PKG';
  g_prd_database varchar2(15) :='EBSPRD';
  g_database varchar2(10) :=Null;
  g_email_id varchar2(240) :=Null;
  --
   g_report_set_request_id number :=0;
   g_wrapper_req_id number :=0;
   g_batch_1 varchar2(30) :=Null;
   g_push_to_sharepoint varchar2(1) :=Null;
  --
  g_instance varchar2(30) :=Null;
  g_Y varchar2(1) :='Y';
  g_N varchar2(1) :='N';
  --
  g_je_actuals varchar2(15) :='Actuals';
  g_je_accruals varchar2(15) :='Accruals';
  --
  g_email_mime_type  varchar2(150) :='text; charset=us-ascii';    
  g_notif_email_from varchar2(240) :='OracleEBS.Concur.Extracts@hdsupply.com';
  g_sharepoint_email varchar2(240) :=Null;
  g_email_subject varchar2(240) :=Null;
  --
  g_notif_email_to varchar2(240) :='iExpenseAdmins@hdsupply.com';
  g_notif_email_cc varchar2(240) :='HDS.OracleGLSupport@hdsupply.com';  
  g_non_prod_email varchar2(240) :=Null;
  g_send_email_attach_to varchar2(240) :=Null;
  --
  g_notif_email_body varchar2(4000) :=Null;
  g_carriage_return varchar2(1) :='
';
  --
  g_err_email1  varchar2(200) :=Null;
  g_err_email2  varchar2(200) :=Null;
  g_last_batch_id number :=0;
  g_last_batch_count number :=0;  
  --
  g_outbound_loc VARCHAR2(240) :=Null;
  g_directory_path VARCHAR2(150) :=Null;
  g_total number :=0;
  --
  g_current_batch_id number :=0;  
  g_current_batch_count number :=0;    
  g_status_new VARCHAR2(20) :='NEW';
  g_status_process VARCHAR2(20) :='IN PROCESS';
  g_status_finish VARCHAR2(20) :='COMPLETE';    
  --
  g_payer_pmt_type varchar2(10) :='Company';
  g_payee_pmt_type varchar2(10) :='Employee';
  g_zero number :=0;
  --
  --Begin WaterWorks related variables
  g_WW_gl_prod_code varchar2(10) :='01';
  g_WW_gl_je_type varchar2(3) :='UL';
  --End WaterWorks related variables
  --
  type g_concur_gl_intf_type is table of XXCUS.xxcus_concur_gl_iface%rowtype index by binary_integer;
  g_concur_gl_intf_rec g_concur_gl_intf_type;
  --
  --Begin WhiteCap US related variables  
  g_WC_US_gl_prod_code varchar2(10) :='0W';
  --End WhiteCap US related variables  
  --
  --
  --Begin WhiteCap Canada [ Brafasco, Great Plains system] related variables  
  g_WC_GP_gl_prod_code varchar2(10) :='37';
  --End  WhiteCap Canada [ Brafasco, Great Plains system] related variables  
  --  
  --Begin FM US related variables    
  g_FM_US_gl_prod_code varchar2(10) :='';
  --End FM US related variables  
  --  
  --Begin FM Canada related variables    
  g_FM_CAD_gl_prod_code varchar2(10) :='';
  --End FM Canada related variables   
  --
  --Begin Repair and Remodel related variables       
  g_RR_gl_product_code varchar2(10) :=''; 
  --End Repair and Remodel related variables       
  --
  -- Begin Concur GL Group id constants 
   g_WW_grp_id number :=11; --WaterWorks 
   g_CI_GP_grp_id number :=22; --Construction and Industrial Brafasco Canada
   --
   g_GSC_grp_id number :=33; --HDS Global Support Center
   g_FM_US_grp_id  number :=44; --Facilities Maintenance US
   --
   g_FM_CAD_grp_id number :=55; --Facilities Maintenance Canada
   g_CI_US_grp_id number :=66; --Construction and Industrial US
   --
   g_CI_HIS_grp_id number :=77; --Construction and Industrial Home Improvement Solution     
   --   
  -- End Concur GL group id Constants 
  -- 
  -- Begin Concur Employee  Group Constants 
   g_WW_emp_grp varchar2(20) :='WW MINCRON'; --WaterWorks 
   g_CI_GP_emp_grp varchar2(20) :='CI CAD GP'; --Construction and Industrial Brafasco Canada
   g_GSC_emp_grp varchar2(20) :='GSC ORACLE'; --HDS Global Support Center
   g_FM_US_emp_grp  varchar2(20) :='FM SAP'; --Facilities Maintenance US
   g_FM_CAD_emp_grp varchar2(20) :='FM CAD INFOR'; --Facilities Maintenance Canada
   g_CI_US_emp_grp varchar2(20) :='CI ORACLE'; --Construction and Industrial US
   g_CI_HIS_emp_grp varchar2(20) :='CI HIS DYNAMICS'; --Construction and Industrial Home Improvement Solutions   
  -- End Concur Employee  Group Constants
  --
  -- Begin Concur Employee  Group Constants used for file creation only 
  g_WW_MINCRON varchar2(20) :='WW_MINCRON';
  g_GSC_ORACLE varchar2(20) :='GSC_ORACLE';
  g_FMSAP  varchar2(20) :='FM_SAP';
  g_FMCADINFOR varchar2(20) :='FM_CAD_INFOR';
  g_CI_ORACLE varchar2(20) :='CI_ORACLE';
  g_CI_HIS_DYNAMICS varchar2(20) :='CI_HIS_DYNAMICS';
  g_CI_CAD_GP varchar2(20) :='CI_CAD_GP';
  -- End Concur Employee  Group Constants  used for file creation only 
  --
  -- Begin GL Interface related constants
  --
  g_JE_Actual_Flag xxcus.xxcus_concur_gl_iface.actual_flag%type :='A'; --Journal Entry Type flag A - Actuals, B -Budget
  g_JE_Iface_Status xxcus.xxcus_concur_gl_iface.status%type :='NEW'; --GL interface insert status code
  g_JE_Category xxcus.xxcus_concur_gl_iface.user_je_category_name%type :='Expenses'; --Journal Entry Category
  g_JE_Source xxcus.xxcus_concur_gl_iface.user_je_source_name%type :='Concur';--Journal Entry Source
  --
  g_Currency_USD varchar2(3) :='USD'; --US Dollar
  g_Currency_CAD varchar2(3) :='CAD'; --Canadian Dollar
  g_US_ledger_id number :=2061; --US Ledger internal id
  --
  g_CAD_ledger_id number :=2063; --Canadian ledger internal id
  g_hds_gl_iface_status1 varchar2(30) :='EBS-GL-IFACE-XFER-PENDING'; 
  g_hds_gl_iface_status2 varchar2(30) :='EBS-GL-IFACE-XFER-COMPLETE';
  --
  -- End GL Interface related constants  
  --
  g_Interco_US_Product   xxcus.xxcus_concur_gl_iface.segment1%type :='48'; --For US Intercompany only
  g_Interco_CA_Product   xxcus.xxcus_concur_gl_iface.segment1%type :='50';  --For Canada Intercompany only
  g_future_use_1                xxcus.xxcus_concur_gl_iface.segment6%type :='00000';
  g_future_use_2                xxcus.xxcus_concur_gl_iface.segment7%type :='00000';  
  --
  g_WW_override_dept varchar2(30) :='030'; --WW Non Fab
  g_inactive_count varchar2(30) :='0';
  g_inactive_dollars varchar2(30) :='0.00';
  g_file_prefix varchar2(30) :=Null;
  --
  g_batch_date date :=Null;
  g_concur_batch_id number :=0;
  g_run_id number :=0;
  --
  g_fiscal_period varchar2(10) :=Null;
  g_fiscal_start date :=Null;
  g_fiscal_end date :=Null;  
  --
  g_perz_status1 varchar2(60) :='Personal journal lines extracted.';
  g_perz_status2 varchar2(60) :='Personal journal lines transferred to GL Interface.'; 
  g_perz_je_indicator varchar2(30) :='PERSONAL JE';     
  g_event_type_EXP  varchar2(30) :='EXPENSE LINES';
  g_event_type_IC  varchar2(30) :='INTERCO LINES';  
  g_event_type_PEREXP   varchar2(30) :='PERSONAL EXPENSE LINES';
  -- 
  -- variables g_control_type*  are used to determine the system controls like group id and other things
  g_control_type_AA varchar2(30) :='ACTUALS AND ACCRUALS';
  g_control_type_OOP varchar2(30) :='OOP';
  g_control_type_CARD varchar2(30) :='CARD';
  g_control_type_PERSONAL varchar2(30) :='PERSONAL';
  g_control_type_AUDIT varchar2(30) :='AUDIT';    
  g_ww_separation_total number :=0;    -- Ver 1.6
  --
  procedure PRINT_LOG(p_message in varchar2) is
  begin
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||sqlerrm);
  end PRINT_LOG;
  -- 
  function IS_ERP_INACTIVE (p_erp in varchar2) return varchar2 is
    --
    l_sub_routine varchar2(30) :='is_erp_inactive'; 
    l_flag varchar2(1) :=Null;      
    --    
  begin 
     --
      select nvl(enabled_flag, 'N')
      into     l_flag
      from    fnd_lookup_values
      where 1 =1
            and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
            and lookup_code =p_erp
          ;  
     --
     if l_flag =g_Y then  
         --
         print_log(' ');
         print_log('ERP : '||p_erp||' Status =>Active');
         print_log(' ');          
         --     
        return g_Y;
        --
     else
         --
         print_log(' ');
         print_log('ERP : '||p_erp||' Status =>Inactive');
         print_log(' ');          
         --     
        return g_N;
        -- 
     end if;
     --
  exception
   when no_data_found then
         --
         print_log(' ');
         print_log('ERP : '||p_erp||' Status =>Not Available because ERP is not setup in the lookup type XXCUS_CONCUR_ACTIVE_BU');
         print_log(' ');          
         --    
         return g_N;
         --
   when too_many_rows then
         --
         print_log(' ');
         print_log('ERP : '||p_erp||' Status =>Inactive, more than 1 record found');
         print_log(' ');          
         --    
         raise program_error;
         --
         return g_N;
         --  
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
         --
         print_log(' ');
         print_log('ERP : '||p_erp||' Status =>Inactive');
         print_log(' ');          
         --    
         return g_N;
         --
  end IS_ERP_INACTIVE;
  --     
  function GET_TIE_OUT_RUN_ID return number is
    --
    l_sub_routine varchar2(30) :='get_tie_out_run_id';
    l_run_id number :=0;       
    --    
  begin 
     --
        select xxcus.xxcus_concur_tieout_s.nextval        
        into     l_run_id
        from   dual
        where 1 =1
          ;  
     --
     print_log(' ');
     print_log('Run ID =>'||l_run_id);
     print_log(' ');          
     --
     return l_run_id;
     --
  exception
   when no_data_found then
    return l_run_id;  
   when too_many_rows then
    return l_run_id;    
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    return l_run_id;
  end GET_TIE_OUT_RUN_ID;
  --  
  procedure INS_AUDIT_TIE_OUT_RPT
   (
     p_report_type in varchar2
    ,p_extract_type in varchar2
    ,p_seq in number
    ,p_item_name in varchar2
    ,p_item_amount in number
    ,p_line_comments in varchar2
    ,p_creation_date in date
    ,p_created_by in number
    ,p_request_id in number   
    ,p_run_id in number
   )  IS
   --
   l_sub_routine varchar2(30) :='ins_audit_tie_out_rpt'; 
   --
  begin  
        --
         insert into xxcus.xxcus_concur_tie_out_report
           (
             report_type
            ,extract_type
            ,sequence
            ,line_item_name
            ,line_item_amount
            ,line_comments
            ,status
            ,email_flag  
            ,crt_btch_id  
            ,creation_date
            ,created_by
            ,request_id
            ,run_id
            ,concur_batch_id
            ,concur_batch_date
            ,fiscal_period           
           ) 
         values 
           (
             p_report_type
            ,p_extract_type
            ,p_seq
            ,p_item_name
            ,p_item_amount
            ,p_line_comments
            ,g_status_new
            ,g_N    
            ,g_current_batch_id
            ,p_creation_date
            ,p_created_by
            ,p_request_id
            ,p_run_id
            ,g_concur_batch_id
            ,g_batch_date 
            ,g_fiscal_period           
           );
         --
  exception             
   when others then
     print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
     RAISE program_error;       
  end INS_AUDIT_TIE_OUT_RPT;
  --  
  function beforeReport return boolean as
   --
   n_loc number;
   ln_request_id number :=0;
   --
   l_sub_routine varchar2(30) :='beforeReport';    
   --
    cursor get_params is
    select 
                a.crt_btch_id            crt_btch_id
               ,a.concur_batch_id  concur_batch_id                       
               ,a.run_id                      run_id
    from  xxcus.xxcus_concur_tie_out_report a
    where 1 =1
          and a.status         =g_status_new
          and a.email_flag =g_N
          and a.run_id in
           (
            select max(b.run_id)
            from   xxcus.xxcus_concur_tie_out_report b
            where 1 =1
                  and b.crt_btch_id =a.crt_btch_id        
                  and b.status           =g_status_new
                  and b.email_flag   =a.email_flag
           )
    group by
                crt_btch_id
               ,concur_batch_id
               ,run_id
               ;   
   --
  begin   
   --
   n_loc :=100;
   --
   savepoint s1;
   --
    open get_params;
    --
     fetch get_params
     into xxcus_concur_pkg.g_rpt_crt_btch_id
            ,xxcus_concur_pkg.g_rpt_concur_batch_id
            ,xxcus_concur_pkg.g_rpt_run_id
     ;
    --
    close get_params;
   --
   print_log (
                        '@beforeReport, g_rpt_crt_btch_id ='||
                        xxcus_concur_pkg.g_rpt_crt_btch_id||
                        ', g_rpt_concur_batch_id ='||
                        xxcus_concur_pkg.g_rpt_concur_batch_id||
                        ', g_rpt_run_id = '||xxcus_concur_pkg.g_rpt_run_id
                     );   
   print_log ('@beforeReport, audit report params fetch complete');
   --
   n_loc :=101;
   --
   commit;
   --
   n_loc :=102;
   --   
   return true;
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', loc => '||n_loc||', msg ='||sqlerrm);
    return false;
  end beforeReport;
  --    
  function afterReport return boolean as
   --
   n_loc number;
   ln_request_id number :=0;
   --
   l_sub_routine varchar2(30) :='afterReport';    
   --
  begin   
   --
   n_loc :=100;
   --
   savepoint s1;
   --
   update xxcus.xxcus_concur_sae_header
           set batch_status ='Audit report generation complete.'
                 ,email_flag =g_Y
   where 1 =1
        and crt_btch_id           =xxcus_concur_pkg.g_rpt_crt_btch_id
        and concur_batch_id =xxcus_concur_pkg.g_rpt_concur_batch_id
        and run_id                    =xxcus_concur_pkg.g_rpt_run_id
    ;
   --
   print_log ('@afterReport, audit report generation complete, total rows updated : '||sql%rowcount);
   --
   n_loc :=101;
   --
   update xxcus.xxcus_concur_tie_out_report
           set  status ='Audit report generation complete.'
                 ,email_flag =g_Y
   where 1 =1
        and crt_btch_id           =xxcus_concur_pkg.g_rpt_crt_btch_id
        and concur_batch_id =xxcus_concur_pkg.g_rpt_concur_batch_id
        and run_id                    =xxcus_concur_pkg.g_rpt_run_id
    ;
   --
   print_log ('@afterReport, audit report generation complete, total rows updated : '||sql%rowcount);
   --   
   commit;
   --
   n_loc :=102;
   --   
   return true;
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', loc => '||n_loc||', msg ='||sqlerrm);
    rollback to s1;
    return false;
  end afterReport;
  --  
  procedure IS_THIS_PRODUCTION
   -- This routine sets global variable g_database and g_instance to tell us if we are running the job in a non prod or prod system
  IS
   --
   l_sub_routine varchar2(30) :='is_this_production'; 
   --
  begin  
        --
        select upper(name)
        into    g_database
        from  v$database
        where 1 =1
        ;
        --
        if (g_database ='EBSPRD') then  
             -- 
             g_instance :=Null;
             --
        else 
             --
              g_instance :='( NON PROD - '||g_database||' )';
             --
              if g_non_prod_email is null then 
               print_log('Enter a valid email address to receive the file attachments and other Concur notifications.');
               raise program_error;
              else 
                 --
                 g_email_id :=g_non_prod_email;
                 g_notif_email_to :=g_email_id;
                 g_notif_email_cc :=g_email_id;
                 --
              end if;
              --
        end if;
        --
  exception             
   when others then
     print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);  
     g_sharepoint_email :=Null;  
     g_database :=Null;
     g_instance :=Null;          
  end IS_THIS_PRODUCTION;
  --  
  procedure SET_BU_SHAREPOINT_EMAIL
   -- This routine sets global variable g_notif_email_body. Make sure the variable is always reset before invoking this routine
    ( 
      p_emp_group in varchar2
     ,p_type in varchar2
    )  IS
   --
   l_sub_routine varchar2(30) :='set_bu_sharepoint_email'; 
   --
  begin  
        if (g_database ='EBSPRD') then 
            -- 
            select sharepoint_email_address
            into    g_sharepoint_email
            from  xxcus.xxcus_concur_extract_controls 
            where 1 =1
                 and employee_group =p_emp_group
                 and type =p_type;
            --
        else 
          --
              if g_push_to_sharepoint =g_Y then 
                select sharepoint_email_address
                into    g_sharepoint_email
                from  xxcus.xxcus_concur_extract_controls 
                where 1 =1
                     and employee_group =p_emp_group
                     and type =p_type;           
              else
                 g_sharepoint_email :=g_email_id;          
              end if;
          --
        end if;  
        --
  exception             
   when others then
     print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm); 
     raise program_error;  
     g_sharepoint_email :=Null;  
  end SET_BU_SHAREPOINT_EMAIL;
  --  
  procedure SETUP_EMAIL_BODY
   -- This routine sets global variable g_notif_email_body. Make sure the variable is always reset before invoking this routine
    ( 
      p_text1 in varchar2
     ,p_emp_group in varchar2
     ,p_type in VARCHAR2
    )  IS
   --
   l_sub_routine varchar2(30) :='setup_email_body'; 
   --
  begin  
      --
      if p_type ='OOP' then 
       --
                g_notif_email_body :=
                                       '**** Concur - EBS: Out of pocket outbound extract to SharePoint  **** '
                                   ||g_carriage_return  
                                   ||' '
                                   ||g_carriage_return
                                   ||p_text1 --Send in BU tag
                                   ||g_carriage_return                                           
                                   ||' '
                                   ||g_carriage_return
                                   ||'All - The OOP extracts are copied over to the ConCur GSC Oracle SharePoint library. The file extracts are delimited by PIPE [|].'
                                   ||g_carriage_return                                           
                                   ||' '
                                   ||g_carriage_return 
                                   ||'Note: Please expect a couple minutes delay for files to show up in SharePoint.'
                                   ||g_carriage_return                                           
                                   ||' '
                                   ||g_carriage_return                                                                       
                                   ||'Link: https://finance.share.hdsupply.net/ap/default.aspx'
                                   ||g_carriage_return                                           
                                   ||' '                                   
                                   ||g_carriage_return
                                   ||'This is an automated email. Please DO NOT reply to this message.'
                                   ||g_carriage_return                           
                                   ||' '
                                   ||g_carriage_return                             
                                   ||'Thanks.'
                                   ||g_carriage_return
                                   ||' '
                                   ||g_carriage_return      
                                   ||'HDS GL Support'
                                  ;  
      -- 
      elsif p_type ='WW_OOP' then 
       --
                g_notif_email_body :=
                                       '**** Concur - EBS: WW Out of pocket outbound extract to SharePoint  **** '
                                   ||g_carriage_return  
                                   ||' '
                                   ||g_carriage_return
                                   ||p_text1 --Send in BU tag
                                   ||g_carriage_return                                           
                                   ||' '
                                   ||g_carriage_return
                                   ||'All - The OOP extracts are copied over to the Concur WW Mincron Oracle SharePoint library. The file extracts are delimited by PIPE [|].'
                                   ||g_carriage_return                                           
                                   ||' '
                                   ||g_carriage_return 
                                   ||'Note: Please expect a couple minutes delay for files to show up in SharePoint.'
                                   ||g_carriage_return                                           
                                   ||' '
                                   ||g_carriage_return                                                                       
                                   ||'Link: https://finance.share.hdsupply.net/ap/default.aspx'
                                   ||g_carriage_return                                           
                                   ||' '                                   
                                   ||g_carriage_return
                                   ||'This is an automated email. Please DO NOT reply to this message.'
                                   ||g_carriage_return                           
                                   ||' '
                                   ||g_carriage_return                             
                                   ||'Thanks.'
                                   ||g_carriage_return
                                   ||' '
                                   ||g_carriage_return      
                                   ||'HDS GL Support'
                                  ;                                              
       --
      elsif p_type ='OTHERS' then  --other than out of pocket
       --
           --
           if p_emp_group IN ('CI ORACLE', 'GSC ORACLE') then
                    g_notif_email_body :=
                                           '**** Concur - EBS: Interface JE Actuals to GL  **** '
                                       ||g_carriage_return  
                                       ||' '
                                       ||g_carriage_return
                                       ||p_text1 --Send in BU tag
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return
                                       ||'All - The concur JE actuals are pushed over to Oracle GL interface for the respective BU. The journals will be auto submitted for import.'
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return
                                       ||'This is an automated email. Please DO NOT reply to this message.'
                                       ||g_carriage_return                           
                                       ||' '
                                       ||g_carriage_return                             
                                       ||'Thanks.'
                                       ||g_carriage_return
                                       ||' '
                                       ||g_carriage_return      
                                       ||'HDS GL Support'
                                      ;        
           else
                    g_notif_email_body :=
                                           '**** Concur - EBS: Outbound extracts to SharePoint  **** '
                                       ||g_carriage_return  
                                       ||' '
                                       ||g_carriage_return
                                       ||p_text1 --Send in BU tag
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return
                                       ||'All - The concur extracts are copied over to the SharePoint library for the respective BU. The file extracts are delimited by PIPE [|].'
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return 
                                       ||'Note: Please expect a couple minutes delay for files to show up in SharePoint.'
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return                                                                       
                                       ||'Link: https://finance.share.hdsupply.net/ap/default.aspx'
                                       ||g_carriage_return                                           
                                       ||' '                                   
                                       ||g_carriage_return
                                       ||'This is an automated email. Please DO NOT reply to this message.'
                                       ||g_carriage_return                           
                                       ||' '
                                       ||g_carriage_return                             
                                       ||'Thanks.'
                                       ||g_carriage_return
                                       ||' '
                                       ||g_carriage_return      
                                       ||'HDS GL Support'
                                      ;       
           end if; 
           -- 
      elsif p_type ='ORACLE_SOURCED' then
                    g_notif_email_body :=
                                           '**** Concur - EBS: Outbound extracts to SharePoint  **** '
                                       ||g_carriage_return  
                                       ||' '
                                       ||g_carriage_return
                                       ||p_text1 --Send in BU tag
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return
                                       ||'All - The concur extracts are copied over to the SharePoint library for the respective BU. The file extracts are delimited by PIPE [|].'
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return 
                                       ||'Note: Please expect a couple minutes delay for files to show up in SharePoint.'
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return                                                                       
                                       ||'Link: https://finance.share.hdsupply.net/ap/default.aspx'
                                       ||g_carriage_return                                           
                                       ||' '                                   
                                       ||g_carriage_return
                                       ||'This is an automated email. Please DO NOT reply to this message.'
                                       ||g_carriage_return                           
                                       ||' '
                                       ||g_carriage_return                             
                                       ||'Thanks.'
                                       ||g_carriage_return
                                       ||' '
                                       ||g_carriage_return      
                                       ||'HDS GL Support'
                                      ;   
      elsif p_type ='ALL_BU_PERSONAL_EXP' then
                    g_notif_email_body :=
                                           '**** Concur - EBS: Outbound extracts to SharePoint  **** '
                                       ||g_carriage_return  
                                       ||' '
                                       ||g_carriage_return
                                       ||p_text1 --Send in BU tag
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return
                                       ||'All - The concur personal detail extracts are copied over to the SharePoint library under Concur PERSONAL. The file extracts are delimited by PIPE [|].'
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return 
                                       ||'Note: Please expect a couple minutes delay for files to show up in SharePoint.'
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return                                                                       
                                       ||'Link: https://finance.share.hdsupply.net/ap/default.aspx'
                                       ||g_carriage_return                                           
                                       ||' '                                   
                                       ||g_carriage_return
                                       ||'This is an automated email. Please DO NOT reply to this message.'
                                       ||g_carriage_return                           
                                       ||' '
                                       ||g_carriage_return                             
                                       ||'Thanks.'
                                       ||g_carriage_return
                                       ||' '
                                       ||g_carriage_return      
                                       ||'HDS GL Support'
                                      ; 
      elsif p_type ='ALL_BU_CARD_EXP' then
                    g_notif_email_body :=
                                           '**** Concur - EBS: Outbound extracts to SharePoint  **** '
                                       ||g_carriage_return  
                                       ||' '
                                       ||g_carriage_return
                                       ||p_text1 --Send in BU tag
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return
                                       ||'All - The concur CARD SAE detail extracts are copied over to the SharePoint library under Concur CARD sharepint library. The file extracts are delimited by PIPE [|].'
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return 
                                       ||'Note: Please expect a couple minutes delay for files to show up in SharePoint.'
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return                                                                       
                                       ||'Link: https://finance.share.hdsupply.net/ap/default.aspx'
                                       ||g_carriage_return                                           
                                       ||' '                                   
                                       ||g_carriage_return
                                       ||'This is an automated email. Please DO NOT reply to this message.'
                                       ||g_carriage_return                           
                                       ||' '
                                       ||g_carriage_return                             
                                       ||'Thanks.'
                                       ||g_carriage_return
                                       ||' '
                                       ||g_carriage_return      
                                       ||'HDS GL Support'
                                      ;                                                                                     
      else --For inactive bu reports only p_type ='INACTIVE_BU'
       --
                g_notif_email_body :=
                                       '**** Concur - Inactive BU journals report to SharePoint  **** '
                                   ||g_carriage_return  
                                   ||' '
                                   ||g_carriage_return
                                   ||p_text1 --Send in BU tag
                                   ||g_carriage_return                                           
                                   ||' '
                                   ||g_carriage_return
                                   ||'All - The inactive journal report for allocated erp '||p_emp_group||' are copied over to the ConCur GSC Oracle SharePoint library. The file extracts are delimited by PIPE [|].'
                                   ||g_carriage_return   --g_inactive_count                                         
                                   ||' '
                                   ||g_carriage_return 
                                   ||'Allocated ERP: '||p_emp_group||', Total journal lines: '||g_inactive_count||', Total journal amount: '||g_inactive_dollars
                                   ||g_carriage_return   --g_inactive_count                                         
                                   ||' '
                                   ||g_carriage_return                                    
                                   ||'Note: Please expect a couple minutes delay for files to show up in SharePoint.'
                                   ||g_carriage_return                                           
                                   ||' '
                                   ||g_carriage_return                                                                       
                                   ||'Link: https://finance.share.hdsupply.net/ap/default.aspx'
                                   ||g_carriage_return                                           
                                   ||' '                                   
                                   ||g_carriage_return
                                   ||'This is an automated email. Please DO NOT reply to this message.'
                                   ||g_carriage_return                           
                                   ||' '
                                   ||g_carriage_return                             
                                   ||'Thanks.'
                                   ||g_carriage_return
                                   ||' '
                                   ||g_carriage_return      
                                   ||'HDS GL Support'
                                  ;                                                    
       --
      end if;
      --
      print_log(' ');
  exception             
   when others then
     print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm); 
     raise program_error;              
  end SETUP_EMAIL_BODY;
  --
 procedure SHAREPOINT_INTEGRATION
   (
        p_emp_group in varchar2,
        p_file in varchar2,
        p_sharepoint_email in varchar2,        
        p_dummy_email_from in varchar2,
        p_email_subject in varchar2
   ) is  
    --
    l_sub_routine varchar2(30) :='sharepoint_integration';
    l_req_id NUMBER :=0;
    --    
  begin    
      --      
        l_req_id := apps.fnd_request.submit_request
                                    (
                                        application =>'XXCUS'
                                       ,program      =>'XXCUS_CONCUR_EMAIL'
                                       ,description =>Null
                                       ,start_time   =>Null --Now
                                       ,sub_request =>FALSE
                                       ,argument1 =>p_file
                                       ,argument2 =>p_sharepoint_email
                                       ,argument3 =>p_dummy_email_from
                                       ,argument4 =>p_email_subject
                                    );
      --
      print_log('@ '||p_emp_group||' email integration to SharePoint , request id :'||l_req_id);
      --
      --Commit;
      --
  exception
   when others then
    print_log('@ '||p_emp_group||', Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
    print_log('Issue in integration to sharepoint : message =>'||sqlerrm);
  end SHAREPOINT_INTEGRATION;
  --    
 procedure SEND_EMAIL_NOTIF
   (
        p_email_from in varchar2,
        p_email_to in varchar2,
        p_email_cc in varchar2,
        p_email_subject in varchar2,
        p_email_body in varchar2,
        p_mime_type in varchar2
   ) is
    l_sub_routine varchar2(30) :='send_email_notif';    
  begin   
      --
      UTL_MAIL.send
          (
            sender          =>p_email_from,
            recipients    =>p_email_to,
            cc                  =>p_email_cc,            
            subject        =>p_email_subject,
            message     =>p_email_body,
            mime_type =>p_mime_type
         );
      --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
    print_log('Issue in sending email notification : message =>'||sqlerrm);
  end SEND_EMAIL_NOTIF;
  --  
  function TRANSFORM_WW_BRANCH (p_project_code VARCHAR2, p_branch in varchar2, p_concur_batch_id in number) return VARCHAR2 is
    --
    l_sub_routine varchar2(30) :='transform_ww_branch';
    l_branch_code varchar2(10) :=Null;       
    --    
  begin 
     --
        select /*+ RESULT_CACHE */ a.branch_code        
        into     l_branch_code
        from    xxcus.xxcus_concur_ww_br_proj_ref a
        where 1 =1
             and a.project_code =p_project_code
          ;  
     --
     return l_branch_code;
     --
  exception
   when no_data_found then
    return p_branch;  
   when too_many_rows then
    return p_branch;    
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    return p_branch;
  end TRANSFORM_WW_BRANCH;
  --   
  function GET_LINE_NUMBERS_PER_BATCH (p_emp_group VARCHAR2, p_prod_code in varchar2, p_concur_batch_id in number) return VARCHAR2 is
    --
    l_sub_routine varchar2(30) :='get_line_numbers_per_batch';
    l_line_numbers varchar2(600) :=Null;   
    --    
  begin 
     --
        select to_char(listagg(a.line_number, ',') within group (order by a.line_number))
        into     l_line_numbers
        from (
                    select line_number line_number
                    from xxcus.xxcus_concur_eligible_je
                    where 1 =1
                        and crt_btch_id =g_current_batch_id
                        and concur_batch_id =p_concur_batch_id
                        and emp_group =p_emp_group            
                        and allocation_company =p_prod_code  
            ) a
          ;  
     --
     return l_line_numbers;
     --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    return l_line_numbers;
  end GET_LINE_NUMBERS_PER_BATCH;
  --    
  function GET_CURRENCY (p_emp_group VARCHAR2) return VARCHAR2 is
    --
    l_sub_routine varchar2(30) :='get_currency';
    l_currency varchar2(3) :=Null;   
    --    
  begin 
     --
     if p_emp_group IN (
                                             g_WW_emp_grp --WaterWorks 
                                            ,g_GSC_emp_grp  --HDS Global Support Center
                                            ,g_FM_US_emp_grp --Facilities Maintenance US
                                            ,g_CI_US_emp_grp --Construction and Industrial US
                                            ,g_CI_HIS_emp_grp --Construction and Industrial Home Improvement Solutions
                                          ) then
          --
          l_currency :=g_Currency_USD;
          --
     elsif p_emp_group IN (
                                             g_CI_GP_emp_grp --Construction and Industrial Brafasco Canada
                                            ,g_FM_CAD_emp_grp --Facilities Maintenance Canada
                                          ) then
           --
           l_currency := g_Currency_CAD;
           --
     else
          --
          l_currency := 'NONE'; --Anything else return NONE so we can catch it during journal import rather than raising an exception during insert of xxcus_concur_gl_iface table
          --  
     end if;
     --
     return l_currency;
     --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    return l_currency;
  end GET_CURRENCY;
  --    
  function GET_INTERCO_GL_PRODUCT (p_emp_group VARCHAR2) return VARCHAR2 is
    --
    l_sub_routine varchar2(30) :='get_interco_gl_product';
    l_Interco_prod_code varchar2(3) :=Null;   
    --    
  begin 
     --
     if p_emp_group IN (
                                             g_WW_emp_grp --WaterWorks 
                                            ,g_GSC_emp_grp  --HDS Global Support Center
                                            ,g_FM_US_emp_grp --Facilities Maintenance US
                                            ,g_CI_US_emp_grp --Construction and Industrial US
                                            ,g_CI_HIS_emp_grp --Construction and Industrial Home Improvement Solutions
                                          ) then
          --
          l_Interco_prod_code :=g_Interco_US_Product;
          --
     elsif p_emp_group IN (
                                             g_CI_GP_emp_grp --Construction and Industrial Brafasco Canada
                                            ,g_FM_CAD_emp_grp --Facilities Maintenance Canada
                                          ) then
           --
           l_Interco_prod_code := g_Interco_CA_Product;
           --
     else
          --
          l_Interco_prod_code := 'NONE'; --Anything else return NONE so we can catch it during journal import rather than raising an exception during insert of xxcus_concur_gl_iface table
          --  
     end if;
     --
     return l_Interco_prod_code;
     --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    return l_Interco_prod_code;
  end GET_INTERCO_GL_PRODUCT;
  --  
  procedure FLUSH_CONCUR_GL_IFACE (p_emp_group VARCHAR2, p_oop_or_card in varchar2, p_group_id in number)  is
    --
    l_sub_routine varchar2(30) :='flush_concur_gl_iface';
    --    
  begin 
     --
     if p_emp_group IN (
                                             g_WW_emp_grp --WaterWorks 
                                            ,g_CI_GP_emp_grp --Construction and Industrial Brafasco Canada
                                            ,g_FM_US_emp_grp --Facilities Maintenance US
                                            ,g_FM_CAD_emp_grp --Facilities Maintenance Canada                                            
                                          ) then
          --
         begin   
              --
              delete xxcus.xxcus_concur_gl_iface
              where 1 =1
                   and group_id =p_group_id
                   and status = g_je_iface_status
                   ;
              --
              print_log('@Flush existing Interco journals in concur gl interface table with NEW status  for GROUP_ID : '||p_group_id||', total deleted :'||sql%rowcount);
              -- 
          exception
            when  others then
             print_log('Error in flusing out journals from concur gl interface for emp group :'||p_emp_group||', msg :'||sqlerrm);
             raise program_error;      
         end; 
          --
     elsif p_emp_group IN (
                                                   g_GSC_emp_grp  --HDS Global Support Center
                                                  ,g_CI_US_emp_grp --Construction and Industrial US
                                                  ,g_CI_HIS_emp_grp --Construction and Industrial Home Improvement Solutions
                                               ) then
           --
         begin   
              --
              delete xxcus.xxcus_concur_gl_iface
              where 1 =1
                   and group_id =p_group_id
                   and status = g_je_iface_status
                   and nvl(reference30,'X') !=g_perz_je_indicator
                   --and reference23 =p_oop_or_card
                   ;
              --
              print_log('@Flush existing Interco journals in concur gl interface table with NEW status  for GROUP_ID : '||p_group_id||', total deleted :'||sql%rowcount);
              -- 
          exception
            when  others then
             print_log('Error in flusing out journals from concur gl interface for emp group :'||p_emp_group||', msg :'||sqlerrm);
             raise program_error;      
         end; 
          --
     else
          --
          Null;
          --  
     end if;
     --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end FLUSH_CONCUR_GL_IFACE;  
  --
  procedure POPULATE_CONCUR_GL_IFACE 
    (
       p_concur_gl_intf_rec in out g_concur_gl_intf_type
      ,p_group_id in number
    ) is
  --
  l_sub_routine varchar2(30) :='populate_concur_gl_iface';
  l_created_by number :=fnd_global.user_id;
  l_creation_date date :=sysdate;    
  l_group_id number :=p_group_id;
  --l_emp_group varchar2(20) :=p_emp_group;
  --l_oop_or_card varchar2(20) :=p_oop_or_card;
  --
  begin
    --
    savepoint last_point;
    --
    if p_concur_gl_intf_rec.count >0 then 
      --
      for idx in 1 .. p_concur_gl_intf_rec.count loop
          --
          p_concur_gl_intf_rec(idx).user_je_source_name :=g_JE_Source;
          p_concur_gl_intf_rec(idx).user_je_category_name :=g_JE_Category;              
          --           
          p_concur_gl_intf_rec(idx).status :=g_JE_Iface_Status;
          p_concur_gl_intf_rec(idx).actual_flag :=g_JE_Actual_Flag;
          --
          p_concur_gl_intf_rec(idx).created_by :=l_created_by;
          p_concur_gl_intf_rec(idx).date_created :=l_creation_date;  
          --
          p_concur_gl_intf_rec(idx).attribute1 :=g_hds_gl_iface_status1; --During insert set it to EBS-TRANSFER-PENDING
          --            
          p_concur_gl_intf_rec(idx).reference29 :=g_run_id; --During insert set it to EBS-TRANSFER-PENDING
          --          
      end loop;
      --
      forall idx in 1 .. p_concur_gl_intf_rec.count
       insert into xxcus.xxcus_concur_gl_iface values p_concur_gl_intf_rec(idx);
       --
    else
      --
      print_log('@'||g_pkg_name||'.'||l_sub_routine||', total records in the plsql collection is '||p_concur_gl_intf_rec.count);
      --
    end if;
    --
    commit;
    --
  exception
   when program_error then
    rollback to last_point;
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then
    rollback to last_point;   
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end POPULATE_CONCUR_GL_IFACE;
  --   
  procedure POPULATE_EBS_GL_IFACE
  --Push HDS concur gl interface records to actual Oracle GL interface 
    (
      p_group_id in number
     ,p_bu_desc in varchar2
    ) is
  --
  l_sub_routine varchar2(30) :='populate_ebs_gl_iface';
  l_created_by number :=fnd_global.user_id;
  --
  l_inserted number :=0;
  l_creation_date date :=sysdate;    
  l_group_id number :=p_group_id;  
  --
  cursor c_journals_iface (l_group_id in number) is
  select 
             status
            ,set_of_books_id ledger_id
            ,accounting_date
            ,currency_code
            ,date_created
            ,created_by
            ,actual_flag
            ,user_je_category_name
            ,user_je_source_name
            ,segment1
            ,segment2
            ,segment3
            ,segment4
            ,segment5
            ,segment6
            ,segment7
            ,entered_dr
            ,entered_cr
            ,reference1
            ,reference2
            ,reference4
            ,reference5
            ,reference10
            ,reference21
            ,reference22
            ,reference23
            ,reference24
            ,g_perz_je_indicator reference30
            ,g_hds_gl_iface_status1 attribute1
            ,attribute2
            ,group_id gl_grp_id 
            ,rowid concur_gl_iface_rowid
  from   xxcus.xxcus_concur_gl_iface
  where 1 =1
       and user_je_source_name =g_JE_Source
       and user_je_category_name =g_JE_Category
       and group_id =l_group_id
       and reference29 =g_run_id
       and attribute1 =g_hds_gl_iface_status1 --g_hds_gl_iface_status1 sbala
       and nvl(reference30, 'X') !=g_perz_je_indicator
   ;
  --
  type c_journals_iface_type is table of c_journals_iface%rowtype index by binary_integer;
  c_journals_iface_rec c_journals_iface_type;
  --
  begin
    --
    print_log('@ POPULATE_EBS_GL_IFACE: G_Run_ID : '||g_run_id);
   --    
   /* -- Begin Ver 1.7
      delete  apps.gl_interface
      where 1 =1
           and user_je_source_name =g_JE_Source
           and user_je_category_name =g_JE_Category
           and group_id =l_group_id
           --and nvl(attribute1, g_hds_gl_iface_status1) =g_hds_gl_iface_status1
           and nvl(reference30, 'X') !=g_perz_je_indicator           
       ;    
 */  -- End Ver 1.7     
    --         
    print_log('@ '||p_bu_desc||', Deleted existing gl interface records for group id '||p_group_id||', total records :'||sql%rowcount);
    --
    open c_journals_iface (l_group_id =>p_group_id);
    fetch c_journals_iface bulk collect into c_journals_iface_rec;
    close c_journals_iface;
    --
    if c_journals_iface_rec.count >0 then 
        --
        begin 
                --
                 for idx in 1 .. c_journals_iface_rec.count loop 
                    --
                    begin 
                      --
                       insert into apps.gl_interface
                        (
                             status
                            ,ledger_id
                            ,accounting_date
                            ,currency_code
                            ,date_created
                            ,created_by
                            ,actual_flag
                            ,user_je_category_name
                            ,user_je_source_name
                            ,segment1
                            ,segment2
                            ,segment3
                            ,segment4
                            ,segment5
                            ,segment6
                            ,segment7
                            ,entered_dr
                            ,entered_cr
                            ,reference1
                            ,reference2
                            ,reference4
                            ,reference5
                            ,reference10
                            ,reference21
                            ,reference22
                            ,reference23
                            ,reference24
                            ,group_id  
                            ,attribute1   
                            ,attribute2 
                            ,reference29         
                        )
                       values
                        (
                             c_journals_iface_rec(idx).status
                            ,c_journals_iface_rec(idx).ledger_id
                            ,c_journals_iface_rec(idx).accounting_date
                            ,c_journals_iface_rec(idx).currency_code
                            ,c_journals_iface_rec(idx).date_created
                            ,c_journals_iface_rec(idx).created_by
                            ,c_journals_iface_rec(idx).actual_flag
                            ,c_journals_iface_rec(idx).user_je_category_name
                            ,c_journals_iface_rec(idx).user_je_source_name
                            ,c_journals_iface_rec(idx).segment1
                            ,c_journals_iface_rec(idx).segment2
                            ,c_journals_iface_rec(idx).segment3
                            ,c_journals_iface_rec(idx).segment4
                            ,c_journals_iface_rec(idx).segment5
                            ,c_journals_iface_rec(idx).segment6
                            ,c_journals_iface_rec(idx).segment7
                            ,c_journals_iface_rec(idx).entered_dr
                            ,c_journals_iface_rec(idx).entered_cr
                            ,c_journals_iface_rec(idx).reference1
                            ,c_journals_iface_rec(idx).reference2
                            ,c_journals_iface_rec(idx).reference4
                            ,c_journals_iface_rec(idx).reference5
                            ,c_journals_iface_rec(idx).reference10
                            ,c_journals_iface_rec(idx).reference21
                            ,c_journals_iface_rec(idx).reference22
                            ,c_journals_iface_rec(idx).reference23
                            ,c_journals_iface_rec(idx).reference24
                            ,c_journals_iface_rec(idx).gl_grp_id     
                            ,g_hds_gl_iface_status1 
                            ,c_journals_iface_rec(idx).attribute2    
                            ,g_run_id      
                        );
                       --
                       l_inserted := l_inserted + sql%rowcount;
                       --
                          update xxcus.xxcus_concur_gl_iface
                                set attribute1 =g_hds_gl_iface_status2
                          where 1 =1
                               and rowid =c_journals_iface_rec(idx).concur_gl_iface_rowid
                           ;
                       --
                    exception
                     when others then
                      rollback; 
                      print_log('Failed to insert into Oracle EBS GL interface, concur gl interface row id :'||c_journals_iface_rec(idx).concur_gl_iface_rowid||', message :'||sqlerrm);
                    end;
                    --
                 end loop;
                --        
             --
             commit;
             --
             print_log('  @ '||p_bu_desc||', Total records inserted into gl interface: '||l_inserted);
             --
        exception
         when others then
          rollback;
          print_log('@populate '||sqlerrm);
        end;
        --
    else --c_journals_iface_rec.count =0
         print_log('No [Excluding Personal ] records to push to Oracle GL interface for BU : '||p_bu_desc||', Group ID :'||p_group_id);
    end if; -- if c_journals_iface_rec.count >0 then
    --
  exception
   when program_error then
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then  
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end POPULATE_EBS_GL_IFACE;
  -- 
  procedure POP_PERSONAL_GL_IFACE
  --Push HDS concur gl interface records to actual Oracle GL interface 
    (
      p_group_id in number
     ,p_bu_desc in varchar2
    ) is
  --
  l_sub_routine varchar2(30) :='pop_personal_gl_iface';
  l_created_by number :=fnd_global.user_id;
  --
  l_inserted number :=0;
  l_creation_date date :=sysdate;    
  l_group_id number :=p_group_id;  
  --
  cursor c_journals_iface (l_group_id in number) is
  select 
             status
            ,set_of_books_id ledger_id
            ,accounting_date
            ,currency_code
            ,date_created
            ,created_by
            ,actual_flag
            ,user_je_category_name
            ,user_je_source_name
            ,segment1
            ,segment2
            ,segment3
            ,segment4
            ,segment5
            ,segment6
            ,segment7
            ,entered_dr
            ,entered_cr
            ,reference1
            ,reference2
            ,reference4
            ,reference5
            ,reference10
            ,reference21
            ,reference22
            ,reference23
            ,reference24
            ,g_perz_je_indicator reference30
            ,g_hds_gl_iface_status1 attribute1
            ,attribute2
            ,group_id gl_grp_id 
            ,rowid concur_gl_iface_rowid
  from   xxcus.xxcus_concur_gl_iface
  where 1 =1
       and user_je_source_name =g_JE_Source
       and user_je_category_name =g_JE_Category
       and group_id =l_group_id
       and attribute1 =g_hds_gl_iface_status1
       and attribute2 =g_event_type_PEREXP
       and reference29 =g_run_id
       and reference30 =g_perz_je_indicator
   ;
  --
  type c_journals_iface_type is table of c_journals_iface%rowtype index by binary_integer;
  c_journals_iface_rec c_journals_iface_type;
  --
  begin
    --
    print_log('@ POP PERSONAL GL IFACE: G_Run_ID : '||g_run_id);
    --   
    /* - -Begin Ver 1.7 
      delete  apps.gl_interface
      where 1 =1
           and user_je_source_name =g_JE_Source
           and user_je_category_name =g_JE_Category
           and group_id =l_group_id
           --and nvl(attribute1, g_hds_gl_iface_status1) =g_hds_gl_iface_status1
           and attribute2 =g_event_type_PEREXP
           and nvl(reference30, g_perz_je_indicator) =g_perz_je_indicator
       ;    
  */ -- End Ver 1.7       
    --
    print_log('@ POP PERSONAL GL IFACE: total records deleted from gl interface : '||sql%rowcount);
    --
    open c_journals_iface (l_group_id =>p_group_id);
    fetch c_journals_iface bulk collect into c_journals_iface_rec;
    close c_journals_iface;
    --
    if c_journals_iface_rec.count >0 then 
        --
        begin 
                --
                 for idx in 1 .. c_journals_iface_rec.count loop 
                    --
                    begin 
                      --
                       insert into apps.gl_interface
                        (
                             status
                            ,ledger_id
                            ,accounting_date
                            ,currency_code
                            ,date_created
                            ,created_by
                            ,actual_flag
                            ,user_je_category_name
                            ,user_je_source_name
                            ,segment1
                            ,segment2
                            ,segment3
                            ,segment4
                            ,segment5
                            ,segment6
                            ,segment7
                            ,entered_dr
                            ,entered_cr
                            ,reference1
                            ,reference2
                            ,reference4
                            ,reference5
                            ,reference10
                            ,reference21
                            ,reference22
                            ,reference23
                            ,reference24
                            ,group_id  
                            ,attribute1 
                            ,attribute2  
                            ,reference29     
                            ,reference30     
                        )
                       values
                        (
                             c_journals_iface_rec(idx).status
                            ,c_journals_iface_rec(idx).ledger_id
                            ,c_journals_iface_rec(idx).accounting_date
                            ,c_journals_iface_rec(idx).currency_code
                            ,c_journals_iface_rec(idx).date_created
                            ,c_journals_iface_rec(idx).created_by
                            ,c_journals_iface_rec(idx).actual_flag
                            ,c_journals_iface_rec(idx).user_je_category_name
                            ,c_journals_iface_rec(idx).user_je_source_name
                            ,c_journals_iface_rec(idx).segment1
                            ,c_journals_iface_rec(idx).segment2
                            ,c_journals_iface_rec(idx).segment3
                            ,c_journals_iface_rec(idx).segment4
                            ,c_journals_iface_rec(idx).segment5
                            ,c_journals_iface_rec(idx).segment6
                            ,c_journals_iface_rec(idx).segment7
                            ,c_journals_iface_rec(idx).entered_dr
                            ,c_journals_iface_rec(idx).entered_cr
                            ,c_journals_iface_rec(idx).reference1
                            ,c_journals_iface_rec(idx).reference2
                            ,c_journals_iface_rec(idx).reference4
                            ,c_journals_iface_rec(idx).reference5
                            ,c_journals_iface_rec(idx).reference10
                            ,c_journals_iface_rec(idx).reference21
                            ,c_journals_iface_rec(idx).reference22
                            ,c_journals_iface_rec(idx).reference23
                            ,c_journals_iface_rec(idx).reference24
                            ,c_journals_iface_rec(idx).gl_grp_id     
                            ,g_hds_gl_iface_status2 --changed 4/8 - Bala from status 1 to status 2
                            ,c_journals_iface_rec(idx).attribute2
                            ,g_run_id
                            ,g_perz_je_indicator
                        );
                       --
                       l_inserted := l_inserted + sql%rowcount;
                       --
                          update xxcus.xxcus_concur_gl_iface
                                set attribute1 =g_hds_gl_iface_status2
                          where 1 =1
                               and rowid =c_journals_iface_rec(idx).concur_gl_iface_rowid
                           ;
                       --
                    exception
                     when others then
                      rollback; 
                      print_log('Failed to insert into Oracle EBS GL interface, concur gl interface row id :'||c_journals_iface_rec(idx).concur_gl_iface_rowid||', message :'||sqlerrm);
                    end;
                    --
                 end loop;
                --        
             --
             commit;
             --
             print_log('Total records inserted into gl interface: '||l_inserted);
             --
        exception
         when others then
          rollback;
          print_log('@populate '||sqlerrm);
        end;
        --
    else --c_journals_iface_rec.count =0
         print_log('No personal expense records to push to Oracle GL interface for BU : '||p_bu_desc||', Group ID :'||p_group_id);
    end if; -- if c_journals_iface_rec.count >0 then
    --
  exception
   when program_error then
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then  
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end POP_PERSONAL_GL_IFACE;
  --  
  procedure PUSH_CONCUR_JE_TO_EBS_GL is
  --
  l_sub_routine varchar2(30) :='push_concur_je_to_ebs_gl';
  l_created_by number :=fnd_global.user_id;
  l_creation_date date :=sysdate;  
  --
  cursor C_GL_ELIGIBLE_BU is
  select  lookup_type
              ,lookup_code
              ,to_number(meaning) gl_iface_group_id
              ,description lookup_desc
              ,nvl(enabled_flag, 'N') enabled_flag
  from   fnd_lookup_values
  where 1 =1
        and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
   ;
  --
  type C_GL_ELIGIBLE_BU_TYPE is table of C_GL_ELIGIBLE_BU%rowtype index by binary_integer;
  c_gl_eligible_bu_rec C_GL_ELIGIBLE_BU_TYPE;
  --
  begin
    --
    open C_GL_ELIGIBLE_BU;
    fetch C_GL_ELIGIBLE_BU bulk collect into c_gl_eligible_bu_rec;
    close C_GL_ELIGIBLE_BU;
    --
    if c_gl_eligible_bu_rec.count >0 then 
         --
         for idx in 1 .. c_gl_eligible_bu_rec.count loop 
            --
            if c_gl_eligible_bu_rec(idx).enabled_flag ='Y' then 
              --
              populate_ebs_gl_iface  (p_group_id =>c_gl_eligible_bu_rec(idx).gl_iface_group_id, p_bu_desc =>c_gl_eligible_bu_rec(idx).lookup_desc);
              --
              print_log( c_gl_eligible_bu_rec(idx).lookup_desc||' [Group ID :'||c_gl_eligible_bu_rec(idx).gl_iface_group_id||']'||' concur eligible journals [Excluding Personal] ] to Oracle EBS GL interface transfer is complete.');
              -- 
              if c_gl_eligible_bu_rec(idx).gl_iface_group_id =g_GSC_grp_id then 
               --
               --Null;
               pop_personal_gl_iface  (p_group_id =>c_gl_eligible_bu_rec(idx).gl_iface_group_id, p_bu_desc =>c_gl_eligible_bu_rec(idx).lookup_desc);
               --
               print_log( c_gl_eligible_bu_rec(idx).lookup_desc||' [Group ID :'||c_gl_eligible_bu_rec(idx).gl_iface_group_id||']'||' concur eligible journals [Personal] ] to Oracle EBS GL interface transfer is incomplete.');
               --                
              end if;  
              --           
            else --c_gl_eligible_bu_rec(idx).enabled_flag =N 
              --
              print_log( c_gl_eligible_bu_rec(idx).lookup_desc||' [Group ID :'||c_gl_eligible_bu_rec(idx).gl_iface_group_id||']'||' business unit is not enabled for GL transfer functionality. If required, enable the BU under lookup type XXCUS_CONCUR_ACTIVE_BU from Application Developer \Lookups \Common.');
              --              
            end if;        
            --
         end loop;
         --
    else
     print_log('None of the business units from look up are enabled for GL transfer functionality. Please enable the records from Application Developer \Lookups \Common.');
    end if; 
    --
  exception
   when program_error then
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then  
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end PUSH_CONCUR_JE_TO_EBS_GL;
  --       
  function GET_OUTBOUND_FILE_LOCATION (v_operation in varchar2, v_emp_group  in varchar2) return varchar2 is
    --
    v_loc varchar2(240) :=Null;
    l_sub_routine varchar2(30) :='get_outbound_file_location';
    l_directory_path varchar2(240) :=Null;
    --
  begin
      --      
      if v_operation ='DBA_DIRECTORY' then
       --
           if v_emp_group =g_WW_MINCRON then 
               -- 
               v_loc := 'XXCUS_CONCUR_WW_MINCRON_OB';
               --
           elsif v_emp_group =g_GSC_ORACLE then
               -- 
               v_loc := 'XXCUS_CONCUR_GSC_ORACLE_OB';
               --
           elsif v_emp_group =g_FMSAP then
               -- 
               v_loc := 'XXCUS_CONCUR_FM_US_OB';
               -- 
           elsif v_emp_group =g_FMCADINFOR then
               -- 
               v_loc := 'XXCUS_CONCUR_FM_CA_OB';
               --
           elsif v_emp_group =g_CI_ORACLE then
               -- 
               v_loc := 'XXCUS_CONCUR_CI_US_OB';
               --       
           elsif v_emp_group =g_CI_HIS_DYNAMICS then
               -- 
               v_loc := 'XXCUS_CONCUR_CI_HIS_DYN_OB';
               --
           elsif v_emp_group =g_CI_CAD_GP then
               -- 
               v_loc := 'XXCUS_CONCUR_CI_GP_OB';
               --                                                                       
           else
               -- 
               v_loc := Null;
               --
           end if;
       --
      else
               -- 
               v_loc := Null;
               --
      end if;
      --
      begin
        select directory_path
        into     g_directory_path 
        from   dba_directories
        where 1 =1
             and directory_name =v_loc;
        print_log('The absolute path for directory name '||v_loc||' is '||g_directory_path);
      exception
       when others then
        print_log('Failed to fet the directory path for directory name '||g_directory_path||', message :'||sqlerrm);
        raise program_error;
      end;
      --
       return v_loc;
      --
  exception
    when others then
     print_log ('@GET_OUTBOUND_FILE_LOCATION, error : '||sqlerrm);
     return null;
     raise program_error;
  end GET_OUTBOUND_FILE_LOCATION;    
   --  
  procedure GET_LAST_BATCH is
   l_sub_routine varchar2(30) :='get_last_batch';
  begin 
     select nvl(max(crt_btch_id), 0) --nvl(max(concur_batch_id), 0)
     into     g_last_batch_id
     from   xxcus.xxcus_src_concur_sae_n_hist
     where 1 =1;
     --
     if g_last_batch_id >0 then
          select count(crt_btch_id) --count(concur_batch_id)
          into     g_last_batch_count
         from   xxcus.xxcus_src_concur_sae_n_hist
         where 1 =1
              and crt_btch_id =g_last_batch_id
              --and concur_batch_id =g_last_batch_id              
              ;
     else
         g_last_batch_count :=0;     
     end if;
     --      
     print_log('g_last_batch_id [crt_btch_id] : '||g_last_batch_id||', g_last_batch_count :'||g_last_batch_count);
     --    
  exception
   when no_data_found then
    print_log('@no_data_found, Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);  
    g_last_batch_count :=0;
    g_last_batch_id :=0;
   when others then
    print_log('@others, Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    g_last_batch_count :=0;
    g_last_batch_id :=0;    
  end GET_LAST_BATCH;
  --  
  function GET_HISTORY_BATCH_COUNT (l_batch_id number) return number is
    l_batch_count number :=0;
    l_sub_routine varchar2(30) :='get_history_batch_count';    
  begin 
     select count(concur_batch_id)
     into     l_batch_count
     from   xxcus.xxcus_src_concur_sae_n_hist
     where 1 =1
           and concur_batch_id =l_batch_id;
     return l_batch_count;
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    return l_batch_count;
  end GET_HISTORY_BATCH_COUNT;
  --  
  procedure GET_CURRENT_BATCH is
   l_sub_routine varchar2(30) :='get_current_batch';
  begin 
     select nvl(max(crt_btch_id), 0) 
     into     g_current_batch_id
     from   xxcus.xxcus_src_concur_sae_n
     where 1 =1             
            and exists
             (
                  select '1'
                  from    fnd_lookup_values
                  where 1 =1
                        and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
                        and nvl(enabled_flag, 'N') ='Y'
                        and lookup_code =emp_custom21
             )      
     ;
     --
     if g_current_batch_id >0 then
          select count(crt_btch_id) --count(concur_batch_id)
          into     g_current_batch_count
         from   xxcus.xxcus_src_concur_sae_n --xxcus_src_concur_sae_n_hist
         where 1 =1
              and crt_btch_id =g_current_batch_id;
     else
         g_current_batch_count :=0;     
     end if;
     --     
     print_log('g_current_batch_id [crt_btch_id] : '||g_current_batch_id||',  g_current_batch_count :'||g_current_batch_count);
     --        
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    raise program_error;
  end GET_CURRENT_BATCH;
  -- 
  procedure FETCH_CONCUR_BATCH_DATE  is 
  --Set global variable g_batch_date, g_concur_batch_id, g_fiscal_period, g_fiscal_start, g_fiscal_end
    l_sub_routine varchar2(30) :='fetch_concur_batch_date';    
  begin 
     --
     select batch_date
                ,concur_batch_id
                ,b.period_name
                ,b.start_date
                ,b.end_date
     into    g_batch_date
               ,g_concur_batch_id
               ,g_fiscal_period
               ,g_fiscal_start
               ,g_fiscal_end               
     from   xxcus.xxcus_concur_sae_header a
                ,gl_periods b
     where 1 =1
           and crt_btch_id =g_current_batch_id
           and batch_date between b.start_date and b.end_date
           and b.adjustment_period_flag ='N' 
           ;
     --
     print_log(' ');     
     print_log('g_batch_date :  '||to_char(g_batch_date, 'MM/DD/YYYY'));
     print_log('g_concur_batch_id :  '||g_concur_batch_id);          
     print_log('g_fiscal_period :  '||g_fiscal_period);
     print_log('g_fiscal_start :  '||to_char(g_fiscal_start, 'MM/DD/YYYY'));
     print_log('g_fiscal_end :  '||to_char(g_fiscal_end, 'MM/DD/YYYY'));               
     print_log(' ');
     --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    g_batch_date :=Null;
  end FETCH_CONCUR_BATCH_DATE;
  --  
  procedure SET_SAE_HDR_STATUS (p_status in varchar2)  is 
    --
    l_sub_routine varchar2(30) :='set_sae_hdr_status';
    l_user_id number :=fnd_global.user_id;
    l_date date :=sysdate;    
    l_request_id number :=fnd_global.conc_request_id;
   --
  begin  
     --
     savepoint begin_here;
     --
     UPDATE xxcus.xxcus_concur_sae_header
     SET batch_status = p_status
            ,last_updated_by =l_user_id
            ,last_update_date =l_date
            ,request_id =l_request_id
            ,run_id =g_run_id
            ,fiscal_period =g_fiscal_period
     where 1 =1
           and crt_btch_id =g_current_batch_id;
    --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    rollback to begin_here;
  end SET_SAE_HDR_STATUS;
  --    
  function GET_EBS_USER_ID (p_user_name in varchar2) return number is
   l_sub_routine varchar2(30) :='get_ebs_user_id';
   l_user_id number :=Null;   
  begin 
     select user_id
     into     l_user_id
     from   apps.fnd_user
     where 1 =1
           and user_name =p_user_name;
     --
     return l_user_id;
     --  
  exception
   when no_data_found then
    return l_user_id;
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);    
    return l_user_id;
  end GET_EBS_USER_ID;
  --  
  function GET_WW_JE_SEQ (p_fiscal_period in varchar2) return varchar2 is
   l_sub_routine varchar2(30) :='get_ww_je_seq';
   l_begin_je_number number :=3890;
   l_last_je_number number :=Null;   
   l_je_number number :=Null;
  begin 
     select max(last_je_number)
     into     l_last_je_number
     from   xxcus.xxcus_concur_ww_je_controls
     where 1 =1
           and fiscal_period =p_fiscal_period;
     --
     if l_last_je_number is not null then 
      --
      l_je_number :=l_last_je_number+1;
     else
      l_je_number :=l_begin_je_number;
     end if;
     --  
     print_log('Fiscal period :'||p_fiscal_period||', returned JE# '||l_je_number);
     --
     return l_je_number;
     --  
  exception
   when no_data_found then
    return l_begin_je_number;
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);    
    raise program_error;
  end GET_WW_JE_SEQ;
  --
  procedure ARCHIVE_CURRENT_BATCH (p_archive_flag in varchar2, p_archive_date in date) is
  --
    l_sub_routine varchar2(30) :='archive_current_batch';  
    l_total number :=0;
  --
  type t_concur_sae_n_hist_tbl is table of  xxcus.xxcus_src_concur_sae_n_hist%rowtype index by binary_integer;   
  --
   t_concur_sae_n_hist_rec t_concur_sae_n_hist_tbl;
   --
   cursor c_last_batch is
   select 
      (
         select /*+ RESULT_CACHE */ 
                     period_name
         from  apps.gl_periods
         where 1 =1
               and concur_batch_date between start_date and end_date
               and adjustment_period_flag ='N'
      ) fiscal_period
    ,p_archive_flag  ebs_processed_flag
    ,p_archive_date ebs_processed_date
    ,row_type
    ,concur_batch_id
    ,concur_batch_date
    ,line_number
    ,emp_id
    ,emp_last_name
    ,emp_first_name
    ,emp_mi
    ,emp_custom21
    ,emp_org_unit1
    ,emp_org_unit2
    ,emp_org_unit3
    ,emp_org_unit4
    ,emp_org_unit5
    ,emp_org_unit6
    ,emp_bank_acct_number
    ,emp_bank_routing_number
    ,amt_net_tax_tot_rclm
    ,rpt_id
    ,rpt_key
    ,ledger_ledger_code
    ,emp_currency_alpha_code
    ,country_lang_name
    ,rpt_submit_date
    ,rpt_user_defined_date
    ,rpt_pmt_processing_date
    ,rpt_name
    ,rpt_image_required
    ,rpt_has_vat_entry
    ,rpt_has_ta_entry
    ,rpt_vw_tot_posted_amt
    ,rpt_vw_tot_approved_amt
    ,policy_lang_name
    ,rpt_entry_bdgt_accrual_date
    ,rpt_org_unit_1
    ,rpt_org_unit_2
    ,rpt_org_unit_3
    ,rpt_org_unit_4
    ,rpt_org_unit_5
    ,rpt_org_unit_6
    ,rpt_custom1
    ,rpt_custom2
    ,rpt_custom3
    ,rpt_custom4
    ,rpt_custom5
    ,rpt_custom6
    ,rpt_custom7
    ,rpt_custom8
    ,rpt_custom9
    ,rpt_custom10
    ,rpt_custom11
    ,rpt_custom12
    ,rpt_custom13
    ,rpt_custom14
    ,rpt_custom15
    ,rpt_custom16
    ,rpt_custom17
    ,rpt_custom18
    ,rpt_custom19
    ,rpt_custom20
    ,rpt_entry_key
    ,rpt_entry_trans_type
    ,rpt_entry_expense_type_name
    ,rpt_entry_trans_date
    ,rpt_entry_currency_alpha_code
    ,rpt_entry_exchange_rate
    ,rpt_entry_exchg_rt_direction
    ,rpt_entry_is_personal
    ,rpt_entry_desc
    ,rpt_entry_vendor_name
    ,rpt_entry_vendor_desc
    ,rpt_entry_receipt_received
    ,rpt_entry_receipt_type
    ,tot_emp_attendee
    ,tot_spouse_attendee
    ,tot_business_attendee
    ,rpt_entry_org_unit_1
    ,rpt_entry_org_unit_2
    ,rpt_entry_org_unit_3
    ,rpt_entry_org_unit_4
    ,rpt_entry_org_unit_5
    ,rpt_entry_org_unit_6
    ,rpt_entry_custom1
    ,rpt_entry_custom2
    ,rpt_entry_custom3
    ,rpt_entry_custom4
    ,rpt_entry_custom5
    ,rpt_entry_custom6
    ,rpt_entry_custom7
    ,rpt_entry_custom8
    ,rpt_entry_custom9
    ,rpt_entry_custom10
    ,rpt_entry_custom11
    ,rpt_entry_custom12
    ,rpt_entry_custom13
    ,rpt_entry_custom14
    ,rpt_entry_custom15
    ,rpt_entry_custom16
    ,rpt_entry_custom17
    ,rpt_entry_custom18
    ,rpt_entry_custom19
    ,rpt_entry_custom20
    ,rpt_entry_custom21
    ,rpt_entry_custom22
    ,rpt_entry_custom23
    ,rpt_entry_custom24
    ,rpt_entry_custom25
    ,rpt_entry_custom26
    ,rpt_entry_custom27
    ,rpt_entry_custom28
    ,rpt_entry_custom29
    ,rpt_entry_custom30
    ,rpt_entry_custom31
    ,rpt_entry_custom32
    ,rpt_entry_custom33
    ,rpt_entry_custom34
    ,rpt_entry_custom35
    ,rpt_entry_custom36
    ,rpt_entry_custom37
    ,rpt_entry_custom38
    ,rpt_entry_custom39
    ,rpt_entry_custom40
    ,rpt_entry_trans_amt
    ,rpt_entry_posted_amt
    ,rpt_entry_approved_amt
    ,rpt_entry_pmt_type_code
    ,rpt_entry_pmt_code_name
    ,rpt_pmt_reimbursement_type
    ,cc_trans_billing_date
    ,billed_cc_acct_number
    ,billed_cc_acct_desc
    ,cc_trans_jr_key
    ,cc_trans_ref_no
    ,cc_trans_cct_key
    ,cc_trans_cct_type
    ,cc_trans_trans_id
    ,cc_trans_trans_amt
    ,cc_trans_tax_amt
    ,cc_trans_trans_curncy
    ,cc_trans_posted_amt
    ,cc_trans_posted_currency
    ,cc_trans_trans_date
    ,cc_trans_posted_date
    ,cc_trans_desc
    ,cc_trans_mc_code
    ,cc_trans_merchant_name
    ,cc_trans_merchant_city
    ,cc_trans_merchant_state
    ,cc_trans_merchant_ctry_code
    ,cc_trans_merchant_ref_num
    ,cc_trans_billing_type
    ,bill_to_emp_exchange_rt
    ,journal_billing_amt
    ,individual_cc_acct_number
    ,individual_cc_acct_name
    ,cc_acct_doing_business_as
    ,cc_trans_acquirer_ref_no
    ,rpt_entry_loc_ctry_code
    ,rpt_entry_loc_ctry_sub_code
    ,rpt_entry_foreign_domestic
    ,cc_acct_provider_market
    ,cc_trans_processor_ref_no
    ,journal_payer_pmt_type_name
    ,journal_payer_pmt_code_name
    ,journal_payee_pmt_type_name
    ,journal_payee_pmt_code_name
    ,journal_acct_code
    ,journal_debit_or_credit
    ,journal_amt
    ,journal_rpj_key
    ,car_log_entry_business_dist
    ,car_log_entry_personal_dist
    ,car_log_entry_passenger_count
    ,car_vehicle_id
    ,cc_trans_sales_tax_amt
    ,eft_batch_cc_vendor_name
    ,cash_adv_req_amt
    ,cash_adv_req_curr_alpha_code
    ,cash_adv_req_curr_num_code
    ,cash_adv_exchange_rate
    ,cash_adv_currency_alpha_code
    ,cash_adv_currency_num_code
    ,cash_adv_issued_date
    ,cash_adv_pmt_code_name
    ,cash_adv_trans_type
    ,cash_adv_req_date
    ,cash_adv_ca_key
    ,cash_adv_pmt_method
    ,allocation_key
    ,allocation_pct
    ,allocation_custom1
    ,allocation_custom2
    ,allocation_custom3
    ,allocation_custom4
    ,allocation_custom5
    ,allocation_custom6
    ,allocation_custom7
    ,allocation_custom8
    ,allocation_custom9
    ,allocation_custom10
    ,allocation_custom11
    ,allocation_custom12
    ,allocation_custom13
    ,allocation_custom14
    ,allocation_custom15
    ,allocation_custom16
    ,allocation_custom17
    ,allocation_custom18
    ,allocation_custom19
    ,allocation_custom20
    ,amt_net_tax_tot_adj
    ,ta_reimburs_meal_lodging_code
    ,ta_reimburs_display_limit
    ,ta_reimburs_allowance_limit
    ,ta_reimburs_allow_threshold
    ,ta_fixed_meal_lodging_type
    ,ta_fixed_base_amt
    ,ta_fixed_allowance_amt
    ,ta_fixed_overnight
    ,ta_fixed_breakfast_provided
    ,ta_fixed_lunch_provided
    ,ta_fixed_dinner_provided
    ,tax_alloc_tot_vw_adj_amt
    ,taxaloc_tot_vw_rclm_adj_amt
    ,tax_auth_lang_auth_name
    ,tax_auth_lang_tax_label
    ,rpt_entry_tax_trans_amt
    ,rpt_entry_tax_posted_amt
    ,rpt_entry_tax_source
    ,rpt_ent_tax_rclm_trans_amt
    ,rpt_entry_tax_rclm_post_amt
    ,rpt_entry_tax_tax_code
    ,tax_config_rclm_domestic
    ,rpt_entry_tax_adj_amt
    ,rpt_entry_tax_rclm_adj_amt
    ,rpt_entry_tax_rclm_code
    ,rpt_ent_tax_rclm_trns_adj_amt
    ,rpt_entry_tax_alloc_rclm_cd
    ,auth_req_req_id
    ,auth_req_req_name
    ,auth_req_tot_posted_amt
    ,auth_req_tot_apprvd_amt
    ,auth_req_start_date
    ,auth_req_end_date
    ,auth_req_auth_date
    ,rpt_entry_tot_tax_posted_amt
    ,rpt_entry_vw_net_tax_amt
    ,rpt_entry_tot_rclm_adj_amt
    ,rpt_ent_vw_net_rclm_adj_amt
    ,rpt_entry_pmt_type_name
    ,card_pgm_type_code
    ,card_pgm_statement_start_date
    ,card_pgm_statement_end_date
    ,eft_batch_cash_acct_code
    ,eft_batch_liability_acct_cd
    ,estimated_pmt_date
    ,eft_pmt_funding_trace
    ,cash_adv_req_disburse_date
    ,cash_adv_name
    ,future_use_260
    ,future_use_261
    ,future_use_262
    ,future_use_263
    ,emp_login_id
    ,emp_email_address
    ,emp_custom1
    ,emp_custom2
    ,emp_custom3
    ,emp_custom4
    ,emp_custom5
    ,emp_custom6
    ,emp_custom7
    ,emp_custom8
    ,emp_custom9
    ,emp_custom10
    ,emp_custom11
    ,emp_custom12
    ,emp_custom13
    ,emp_custom14
    ,emp_custom15
    ,emp_custom16
    ,emp_custom17
    ,emp_custom18
    ,emp_custom19
    ,emp_custom20
    ,future_use_286
    ,future_use_287
    ,future_use_288
    ,future_use_289
    ,future_use_290
    ,emp_bank_is_active
    ,eft_bank_acct_type
    ,emp_bank_name_on_acct
    ,emp_bank_bank_name
    ,emp_bank_branch_location
    ,emp_bank_address_line_1
    ,emp_bank_address_line_2
    ,emp_bank_city
    ,emp_bank_region
    ,emp_bank_postal_code
    ,emp_bank_ctry_code
    ,emp_bank_tax_id
    ,emp_bank_is_resident
    ,emp_bank_name
    ,emp_bank_currency_alpha_code
    ,future_use_306
    ,future_use_307
    ,future_use_308
    ,future_use_309
    ,future_use_310
    ,rpt_vw_tot_personal_amt
    ,rpt_vw_tot_claimed_amt
    ,rpt_vw_tot_due_emp
    ,rpt_vw_tot_due_cmpny_card
    ,rpt_vw_tot_rejected_amt
    ,rpt_vw_tot_cmpny_paid
    ,rpt_vw_tot_due_cmpny
    ,rpt_vw_tot_confirmed_paid
    ,rpt_vw_actual_tot_due_emp
    ,rpt_vw_actl_tot_due_cmp_crd
    ,rpt_report_type
    ,rpt_entry_net_adj_tax_amt
    ,rpt_entry_vw_net_adj_tax_amt
    ,rpt_start_date
    ,rpt_end_date
    ,future_use_326
    ,rpt_ca_returns_amt
    ,rpt_ca_return_received
    ,rpt_ca_utilized_amt
    ,future_use_330
    ,future_use_331
    ,future_use_332
    ,future_use_333
    ,rpt_entry_location_city_name
    ,rpt_entry_is_billable
    ,rpt_entry_from_location
    ,rpt_entry_to_location
    ,rpt_entry_location_name
    ,rpt_entry_ctry_code
    ,rpt_entry_ctry_sub_code
    ,xml_receipt_uuid
    ,xml_receipt_tax_id
    ,future_use_343
    ,future_use_344
    ,future_use_345
    ,future_use_346
    ,txa_tot_vw_trans_amt
    ,txa_tot_vw_posted_amt
    ,txa_tot_vw_rclm_trns_amt
    ,txa_tot_vw_rclm_posted_amt
    ,txa_tot_vw_rclm_trns_adj_amt
    ,rpt_entry_merchant_tax_id
    ,future_use_353
    ,future_use_354
    ,future_use_355
    ,future_use_356
    ,trvl_req_name
    ,trvl_req_tot_posted_amt
    ,trvl_req_tot_apprv_amt
    ,trvl_req_start_date
    ,trvl_req_end_date
    ,trvl_req_auth_date
    ,trvl_req_req_id
    ,future_use_364
    ,future_use_365
    ,future_use_366
    ,future_use_367
    ,future_use_368
    ,future_use_369
    ,future_use_370
    ,future_use_371
    ,future_use_372
    ,future_use_373
    ,future_use_374
    ,future_use_375
    ,future_use_376
    ,future_use_377
    ,future_use_378
    ,future_use_379
    ,future_use_380
    ,future_use_381
    ,future_use_382
    ,future_use_383
    ,future_use_384
    ,future_use_385
    ,future_use_386
    ,future_use_387
    ,future_use_388
    ,future_use_389
    ,future_use_390
    ,future_use_391
    ,future_use_392
    ,future_use_393
    ,future_use_394
    ,future_use_395
    ,future_use_396
    ,future_use_397
    ,future_use_398
    ,future_use_399
    ,future_use_400
    ,etl_proc_dt
    ,rec_digest
    ,crt_btch_id
from  xxcus.xxcus_src_concur_sae_n
where 1 =1
      and crt_btch_id >g_last_batch_id
      --and concur_batch_id >g_last_batch_id
      ;   
   --
  begin 
       --
       if g_current_batch_id =g_last_batch_id then 
            --
            print_log('Current crt_btch_id :'||g_current_batch_id||' records already archived into history table xxcus.xxcus_src_concur_sae_n_hist.');
            --   
       else
            --
            open c_last_batch;
            --
            loop 
             --
             fetch c_last_batch bulk collect into t_concur_sae_n_hist_rec limit xxcus_concur_pkg.g_limit;
             --
             exit when t_concur_sae_n_hist_rec.count =0;
             --
                 if t_concur_sae_n_hist_rec.count >0 then 
                  --
                  l_total := l_total + t_concur_sae_n_hist_rec.count;
                  --
                  forall idx in 1 .. t_concur_sae_n_hist_rec.count
                   insert into xxcus.xxcus_src_concur_sae_n_hist values t_concur_sae_n_hist_rec(idx);
                  --
                 end if;
                  --
            end loop;
            --
            close c_last_batch;
            --
            print_log('Inserted '||l_total||' records into history table xxcus.xxcus_src_concur_sae_n_hist.');
            --
            if l_total >0 then
               -- 
               commit;   
               --           
            else
                  Null;    
            end if;
            --
       end if;
       --
  exception
   when program_error then
    print_log('Issue in program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then
    print_log('Issue in others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end ARCHIVE_CURRENT_BATCH;
  --
  procedure EXTRACT_PEOPLESOFT_OOP is --people soft out of pocket for all LOB's
   --
    l_sub_routine varchar2(30) :='extract_peoplesoft_oop'; 
    l_request_id number :=fnd_global.conc_request_id;
    --
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;
    l_count number :=0;
    --
    l_utl_file_id   UTL_FILE.FILE_TYPE;
    l_line_buffer VARCHAR2(30000):= Null;
    l_file VARCHAR2(150) :=Null;        
    --
    cursor c_oop_group is --lookup XXCUS_CONCUR_OOP_PAYMENT_TYPES is the driver table here
        select concur_batch_id payment_batch
                   ,concur_batch_date check_date
                   ,'C'||rpt_key check_number
                   ,emp_id employee_number
                   ,emp_first_name
                   ,emp_last_name
                   ,to_date(null) terminated_date
                   ,'Active' employee_status
                   ,rpt_vw_actual_tot_due_emp --rpt_vw_tot_due_emp amount -- Ver 1.7
                   ,'Concur' payment_type_flag
                   ,Null line_numbers --listagg(line_number, ',') within group (order by line_number) line_numbers --Ver 1.2
                   ,l_created_by ebs_created_by
                   ,l_creation_date ebs_creation_date
                   ,l_request_id request_id
                   ,emp_custom21 erp_platform
                   ,g_status_new record_status
                   ,g_current_batch_id crt_btch_id
                   ,concur_batch_id concur_batch_id
        from xxcus.xxcus_src_concur_sae_n
        where 1 =1
         and crt_btch_id =g_current_batch_id --new
         and emp_custom21 !=g_WW_emp_grp --Ver 1.6
         and rpt_entry_pmt_type_code IN 
          (
            select lookup_code
            from  apps.fnd_lookup_values_vl
            where 1 =1
                 and lookup_type ='XXCUS_CONCUR_OOP_PAYMENT_TYPES'
                 and enabled_flag ='Y'
          )
        and exists
         (
              select '1'
              from    fnd_lookup_values
              where 1 =1
                    and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
                    and nvl(enabled_flag, 'N') ='Y'
                    and lookup_code =emp_custom21
         )
        group by
                     concur_batch_id
                   ,concur_batch_date
                   ,'C'||rpt_key
                   ,emp_id
                   ,emp_first_name
                   ,emp_last_name
                   ,rpt_vw_actual_tot_due_emp --rpt_vw_tot_due_emp -- Ver 1.7
                   ,l_created_by
                   ,l_creation_date
                   ,l_request_id
                   ,emp_custom21   
                   ,g_status_new
                   ,g_current_batch_id
                   ,concur_batch_id
           ;    
    --
    type  c_oop_group_type is table of c_oop_group%rowtype index by binary_integer;
    c_oop_group_rec c_oop_group_type;
    --
    cursor oop_file_set is 
        select 
                 'PAYMENT_BATCH'||'|'||
                 'CHECK_DATE'||'|'||
                 'CHECK_NUMBER'||'|'||
                 'EMPLOYEE_NUMBER'||'|'||
                 'EMP_FIRST_NAME'||'|'||
                 'EMP_LAST_NAME'||'|'||
                 'TERMINATED_DATE'||'|'||
                 'EMPLOYEE_STATUS'||'|'||
                 'AMOUNT'||'|'||
                 'PAYMENT_TYPE_FLAG'||'|'||
                 'EBS_CREATION_DATE'||'|'||
                 'REQUEST_ID'||'|'||
                 'ERP_PLATFORM'||'|'||
                 'CONCUR_BATCH_ID'||'|'||                 
                 'HDS_CRT_BTCH_ID'||'|' hds_concur_oop_record
        from dual
        union all        
        select 
                payment_batch||'|'||
                to_char(check_date,'mm/dd/yyyy')||'|'||
                check_number||'|'||
                employee_number||'|'||
                emp_first_name||'|'||
                emp_last_name||'|'||
                terminated_date||'|'||
                employee_status||'|'||
                TO_CHAR(amount,'FM999,999,990.90')||'|'||
                payment_type_flag||'|'||
                to_char(ebs_creation_date,'mm/dd/yyyy')||'|'||
                request_id||'|'||
                erp_platform||'|'||
                concur_batch_id||'|'||                
                crt_btch_id||'|' hds_concur_oop_record
        from xxcus.xxcus_src_concur_sae_oop
        where 1 =1
             and crt_btch_id =g_current_batch_id
             and record_status =g_status_new
             and erp_platform !=g_WW_emp_grp --Ver 1.6
             ;
    --
    type oop_file_type is table of oop_file_set%rowtype index by binary_integer;
    oop_rec oop_file_type;            
    --
    -- Begin Ver 1.6
    --
    cursor oop_file_set_ww is 
        select 
                 'PAYMENT_BATCH'||'|'||
                 'CHECK_DATE'||'|'||
                 'CHECK_NUMBER'||'|'||
                 'EMPLOYEE_NUMBER'||'|'||
                 'EMP_FIRST_NAME'||'|'||
                 'EMP_LAST_NAME'||'|'||
                 'TERMINATED_DATE'||'|'||
                 'EMPLOYEE_STATUS'||'|'||
                 'AMOUNT'||'|'||
                 'PAYMENT_TYPE_FLAG'||'|'||
                 'EBS_CREATION_DATE'||'|'||
                 'REQUEST_ID'||'|'||
                 'ERP_PLATFORM'||'|'||
                 'CONCUR_BATCH_ID'||'|'||                 
                 'HDS_CRT_BTCH_ID'||'|' hds_concur_oop_record
        from dual
        union all        
        select 
                payment_batch||'|'||
                to_char(check_date,'mm/dd/yyyy')||'|'||
                check_number||'|'||
                employee_number||'|'||
                emp_first_name||'|'||
                emp_last_name||'|'||
                terminated_date||'|'||
                employee_status||'|'||
                TO_CHAR(amount,'FM999,999,990.90')||'|'||
                payment_type_flag||'|'||
                to_char(ebs_creation_date,'mm/dd/yyyy')||'|'||
                request_id||'|'||
                erp_platform||'|'||
                concur_batch_id||'|'||                
                crt_btch_id||'|' hds_concur_oop_record
        from xxcus.xxcus_src_concur_sae_oop_ww
        where 1 =1
             and crt_btch_id =g_current_batch_id
             and record_status =g_status_new
             and erp_platform =g_WW_emp_grp
             ;
    --   
    cursor ww_adp_oop_file_set is 
        select 
                 'CO_CODE'||','||
                 'BATCH_ID'||','||
                 'FILE#'||','||
                 'ADJUST_DED_CODE'||','||
                 'ADJUST_DED_AMOUNT'||','  hds_adp_ww_oop_record
        from dual
        union all        
        select 
                 '2UJ'||','||
                 'CONCUR'||concur_batch_id||','||
                 employee_number||','||
                 'IEX'||','||
                 TO_CHAR((-1)*amount,'FM999999990.90')||','  hds_adp_ww_oop_record
        from xxcus.xxcus_src_concur_sae_oop_ww
        where 1 =1
             and crt_btch_id =g_current_batch_id
             and record_status =g_status_new
             and erp_platform =g_WW_emp_grp
             ;
    --      
    type oop_adp_file_type is table of ww_adp_oop_file_set%rowtype index by binary_integer;
    oop_adp_rec oop_adp_file_type;     
    --     
    cursor c_ww_oop is
    select concur_batch_id payment_batch
                   ,concur_batch_date check_date
                   ,'C'||rpt_key check_number
                   ,emp_id employee_number
                   ,emp_first_name
                   ,emp_last_name
                   ,to_date(null) terminated_date
                   ,'Active' employee_status
                   ,rpt_vw_actual_tot_due_emp --rpt_vw_tot_due_emp amount -- Ver 1.7
                   ,'Concur' payment_type_flag
                   ,Null line_numbers
                   ,l_created_by ebs_created_by
                   ,l_creation_date ebs_creation_date
                   ,l_request_id request_id
                   ,emp_custom21 erp_platform
                   ,g_status_new record_status
                   ,g_current_batch_id crt_btch_id
                   ,concur_batch_id concur_batch_id
                   ,null attribute_1
                   ,null attribute_2
                   ,null attribute_3
                   ,null attribute_4
                   ,null attribute_5
                   ,null attribute_6
                   ,null attribute_7
                   ,null attribute_8
                   ,null attribute_9
                   ,null attribute_10
                   ,null attribute_11
                   ,null attribute_12
                   ,null attribute_13
                   ,null attribute_14
                   ,null attribute_15                                                                                                                                                                                                                                                                        
     From xxcus.xxcus_src_concur_sae_n
     where 1 =1
         and crt_btch_id =g_current_batch_id
         and emp_custom21 =g_WW_emp_grp
         and rpt_entry_pmt_type_code IN 
          (
            select lookup_code
            from  apps.fnd_lookup_values_vl
            where 1 =1
                 and lookup_type ='XXCUS_CONCUR_OOP_PAYMENT_TYPES'
                 and enabled_flag ='Y'
          )
        and exists
         (
              select '1'
              from    fnd_lookup_values
              where 1 =1
                    and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
                    and nvl(enabled_flag, 'N') ='Y'
                    and lookup_code =emp_custom21
         )
        group by
                     concur_batch_id
                   ,concur_batch_date
                   ,'C'||rpt_key
                   ,emp_id
                   ,emp_first_name
                   ,emp_last_name
                   ,rpt_vw_actual_tot_due_emp -- rpt_vw_tot_due_emp -- Ver 1.7
                   ,l_created_by
                   ,l_creation_date
                   ,l_request_id
                   ,emp_custom21   
                   ,g_status_new
                   ,g_current_batch_id
                   ,concur_batch_id
           ;    
    --
    type  c_ww_oop_type is table of c_ww_oop%rowtype index by binary_integer;
    c_ww_oop_group_rec c_ww_oop_type;
    --    
    -- End Ver 1.6
  begin
    --
    delete xxcus.xxcus_src_concur_sae_oop_ww
    where 1 =1
         and crt_btch_id =g_current_batch_id;
    --
     print_log('@Out of pocket [WW only], begin cleanup for current concur batch ids where crt_btch_id = '||g_current_batch_id||', records deleted :'||sql%rowcount);
    --
    open c_oop_group;
     loop 
       fetch c_oop_group bulk collect into c_oop_group_rec limit xxcus_concur_pkg.g_limit; 
       exit when c_oop_group_rec.count =0;
       if c_oop_group_rec.count >0 then
        forall idx in 1 .. c_oop_group_rec.count
         insert into xxcus.xxcus_src_concur_sae_oop values c_oop_group_rec(idx);
         l_count :=l_count + sql%rowcount;
       end if;
     end loop;
    close c_oop_group;
    --
    print_log('@'||l_sub_routine||', total records inserted :'||l_count);
    --
    commit;
    --
    -- Begin Ver 1.6
    open c_ww_oop;
     loop 
       fetch c_ww_oop bulk collect into c_ww_oop_group_rec limit xxcus_concur_pkg.g_limit; 
       exit when c_ww_oop_group_rec.count =0;
       if c_ww_oop_group_rec.count >0 then
        forall idx in 1 .. c_ww_oop_group_rec.count
         insert into xxcus.xxcus_src_concur_sae_oop_ww values c_ww_oop_group_rec(idx);
         l_count :=l_count + sql%rowcount;
       end if;
     end loop;
    close c_ww_oop;
    --
    print_log('@ WW OOP transactions : '||l_sub_routine||', total records inserted :'||l_count);
    --
    commit;
    --    
    -- End Ver 1.6
    --
    print_log('Begin: Extract OOP file [Excluding WaterWorks].'); -- Ver 1.6
    --    
    open oop_file_set;
    --
    fetch oop_file_set bulk collect into oop_rec;
    --
    close oop_file_set;
    --
    if oop_rec.count >0 then   
                 --
                 l_count :=0;
                 --
                 -- OOP file goes to GSC SharePoint folder 
                 g_outbound_loc :=GET_OUTBOUND_FILE_LOCATION ( v_operation =>'DBA_DIRECTORY', v_emp_group  =>g_GSC_ORACLE);
                 --
                 print_log ('Current working DBA directory is '||g_outbound_loc);
                 --
                 --
                 if g_database =g_prd_database then   
                   g_file_prefix :=g_GSC_ORACLE;
                 else
                   g_file_prefix :=g_database||'_'||g_GSC_ORACLE;
                 end if;
                 --
                 l_file :=g_file_prefix||'_OOP_'||TO_CHAR(g_batch_date, 'MMDDYYYY')||'.txt';
                 --
                 print_log('Before file open for OOP PIPE [|] extract');
                 --
                 l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
                 --
                 print_log ('File handle initiated for OOP PIPE [|] extract');
                 --
                 for idx in 1 .. oop_rec.count loop 
                  --
                       begin
                          --
                          utl_file.put_line ( l_utl_file_id, oop_rec(idx).hds_concur_oop_record);
                          -- 
                          l_count:= l_count+1; --number of lines written for OOP including header
                          --
                       exception
                             when no_data_found then
                               exit;
                             when others then
                               print_log('Last Line# '||l_count||', Record: '||oop_rec(idx).hds_concur_oop_record);                               
                               print_log('@ Inside file loop operation, error : '||sqlerrm);
                               exit;
                       end;
                  --
                 end loop; 
                 --
                 utl_file.fclose ( l_utl_file_id );
                 print_log ('After close for OOP PIPE [|] extract file : '||l_file);
                 --  
                 -- Begin logging file details for each BU along with their concur batch id
                 begin 
                        --
                        savepoint start_here;
                        --
                        insert into xxcus.xxcus_concur_ebs_bu_files
                        (
                         request_id
                        ,concur_batch_id
                        ,emp_group
                        ,extracted_file_name
                        ,extracted_file_folder
                        ,created_by 
                        ,creation_date
                        ,crt_btch_id
                        )
                        values
                        (
                         l_request_id
                        ,Null -- will be blank when extracting for all out of pocket for all BU's
                        ,g_GSC_ORACLE
                        ,l_file
                        ,g_directory_path
                        ,l_created_by
                        ,l_creation_date
                        ,g_current_batch_id
                        )
                        ;
                        --
                        commit;
                        --
                        -- setup the email body
                        setup_email_body 
                          (
                             p_text1 =>'BU CONCUR employee group : '||g_GSC_emp_grp
                            ,p_emp_group =>g_GSC_emp_grp
                            ,p_type =>'OOP'
                          ); --g_notif_email_body is assigned here
                        --print_log('Email Body :'||g_notif_email_body);
                        --
                        g_email_subject :=g_instance||' '||g_GSC_emp_grp||' CONCUR OOP extract to SharePoint - Status Update';
                        print_log('Email Subject :'||g_email_subject);
                        --
                        set_bu_sharepoint_email (p_emp_group =>g_GSC_emp_grp, p_type =>g_control_type_OOP); --g_sharepoint_email is assigned here
                        print_log('SharePoint Email  :'||g_sharepoint_email);                        
                        --
                        -- push the extract to sharepoint
                        print_log('Begin: Push OOP PIPE extract file to SharePoint.');
                        --
                        sharepoint_integration
                           (
                                p_emp_group =>g_GSC_emp_grp,
                                p_file =>g_directory_path||'/'||l_file,
                                p_sharepoint_email =>g_sharepoint_email,
                                p_dummy_email_from =>g_notif_email_from,
                                p_email_subject =>g_email_subject
                           )
                           ;
                        --
                        print_log('End: Push OOP PIPE extract file to SharePoint.');                           
                        -- email the teams about the extract
                        send_email_notif
                           (
                                p_email_from      =>g_notif_email_from,
                                p_email_to           =>g_notif_email_to,
                                p_email_cc           =>g_notif_email_cc,
                                p_email_subject =>g_email_subject, 
                                p_email_body     =>g_notif_email_body,
                                p_mime_type      =>g_email_mime_type
                           )
                          ; 
                        print_log('After sending email notification about WaterWorks status');                                                                                                
                        --                     
                 exception
                  when others then
                   print_log('@Failed to log file details for BU, @ emp_group '||g_GSC_emp_grp||', message =>'||sqlerrm);
                 end;                                                    
     --
    else
     --
     print_log('No OOP record found for extract into file.');
     --
    end if;
    --
    print_log('End: Extract OOP file [Excluding WaterWorks] file.'); --Ver 1.6
    --   
    -- Begin Ver 1.6
    print_log('Begin: Extract OOP file [WaterWorks only].');   
    -- 
    oop_rec.delete; --Cleanup the plsql table before populating. if we add more columns then we need to use a diff plsql table
    --
    open oop_file_set_ww; 
    --
    fetch oop_file_set_ww bulk collect into oop_rec;
    --
    close oop_file_set_ww;
    --
    if oop_rec.count >0 then   --Extract OOP file [WaterWorks only].
                 --
                 l_count :=0;
                 --
                 -- WW OOP file goes to GSC SharePoint folder 
                 g_outbound_loc :=GET_OUTBOUND_FILE_LOCATION ( v_operation =>'DBA_DIRECTORY', v_emp_group  =>g_WW_MINCRON);
                 --
                 print_log ('Current working DBA directory is '||g_outbound_loc);
                 --
                 --
                 if g_database =g_prd_database then   
                   g_file_prefix :=g_WW_MINCRON;
                 else
                   g_file_prefix :=g_database||'_'||g_WW_MINCRON;
                 end if;
                 --
                 l_file :='WW_OOP_CONCUR'||g_concur_batch_id||'_'||TO_CHAR(g_batch_date, 'MMDDYYYY')||'.txt';
                 --
                 print_log('Before file open for WW OOP PIPE [|] extract');
                 --
                 l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
                 --
                 print_log ('File handle initiated for WW OOP PIPE [|] extract');
                 --
                 for idx in 1 .. oop_rec.count loop 
                  --
                       begin
                          --
                          utl_file.put_line ( l_utl_file_id, oop_rec(idx).hds_concur_oop_record);
                          -- 
                          l_count:= l_count+1; --number of lines written for OOP including header
                          --
                       exception
                             when no_data_found then
                               exit;
                             when others then
                               print_log('Last Line# '||l_count||', WW OOP Record: '||oop_rec(idx).hds_concur_oop_record);                               
                               print_log('@ Inside file loop operation, error : '||sqlerrm);
                               exit;
                       end;
                  --
                 end loop; 
                 --
                 utl_file.fclose ( l_utl_file_id );
                 print_log ('After close for WW OOP PIPE [|] extract file : '||l_file);
                 --  
                 -- Begin logging file details for each BU along with their concur batch id
                 begin 
                        --
                        -- setup the email body
                        setup_email_body 
                          (
                             p_text1 =>'BU CONCUR employee group : '||g_WW_emp_grp
                            ,p_emp_group =>g_WW_emp_grp
                            ,p_type =>'WW_OOP'
                          ); 
                        --
                        --
                        g_email_subject :=g_instance||' '||g_WW_emp_grp||' CONCUR OOP extract to SharePoint - Status Update';
                        print_log('Email Subject :'||g_email_subject);
                        --
                        set_bu_sharepoint_email (p_emp_group =>g_WW_emp_grp, p_type =>g_control_type_AA); --g_sharepoint_email is assigned here
                        print_log('SharePoint Email  :'||g_sharepoint_email);                        
                        --
                        -- push the extract to sharepoint
                        print_log('Begin: Push WaterWorks OOP PIPE extract file to SharePoint.');
                        --
                        sharepoint_integration
                           (
                                p_emp_group =>g_WW_emp_grp,
                                p_file =>g_directory_path||'/'||l_file,
                                p_sharepoint_email =>g_sharepoint_email,
                                p_dummy_email_from =>g_notif_email_from,
                                p_email_subject =>g_email_subject
                           )
                           ;
                        --
                        print_log('End: Push WaterWorks OOP PIPE extract file to SharePoint.');                           
                        -- email the teams about the extract
                        send_email_notif
                           (
                                p_email_from      =>g_notif_email_from,
                                p_email_to           =>g_notif_email_to,
                                p_email_cc           =>g_notif_email_cc,
                                p_email_subject =>g_email_subject, 
                                p_email_body     =>g_notif_email_body,
                                p_mime_type      =>g_email_mime_type
                           )
                          ; 
                        print_log('After sending email notification about WaterWorks OOP file status.');                                                                                                
                        --                     
                 exception
                  when others then
                   print_log('@Failed to log file details for BU, @ emp_group '||g_WW_emp_grp||', message =>'||sqlerrm);
                 end;    
                 --                                              
     --
    else
     --
     print_log('No OOP record found for extract into file.');
     --
    end if; -- Extract OOP file [WaterWorks only].
    --
    print_log('End: Extract OOP file [WaterWorks only] file.');
    --      
    -- Begin WW ADP file
    print_log('Begin: Extract OOP ADP file [WaterWorks only].');   
    -- 
    oop_rec.delete; --Cleanup the plsql table before populating. if we add more columns then we need to use a diff plsql table
    --
    open ww_adp_oop_file_set; 
    --
    fetch ww_adp_oop_file_set bulk collect into oop_adp_rec;
    --
    close ww_adp_oop_file_set;
    --
    if oop_adp_rec.count >0 then   --Extract OOP ADP file [WaterWorks only].
                 --
                 l_count :=0;
                 --
                 -- WW OOP file goes to GSC SharePoint folder 
                 g_outbound_loc :=GET_OUTBOUND_FILE_LOCATION ( v_operation =>'DBA_DIRECTORY', v_emp_group  =>g_WW_MINCRON);
                 --
                 print_log ('Current working DBA directory is '||g_outbound_loc);
                 --
                 --
                 if g_database =g_prd_database then   
                   g_file_prefix :=g_WW_MINCRON;
                 else
                   g_file_prefix :=g_database||'_'||g_WW_MINCRON;
                 end if;
                 --
                 l_file :='WW_OOP_CONCUR'||g_concur_batch_id||'_ADP_'||TO_CHAR(g_batch_date, 'MMDDYYYY')||'.txt';
                 --
                 print_log('Before file open for WW OOP ADP csv [,] extract');
                 --
                 l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
                 --
                 print_log ('File handle initiated for WW OOP ADP csv  [,] extract');
                 --
                 for idx in 1 .. oop_adp_rec.count loop 
                  --
                       begin
                          --
                          utl_file.put_line ( l_utl_file_id, oop_adp_rec(idx).hds_adp_ww_oop_record);
                          -- 
                          l_count:= l_count+1;
                          --
                       exception
                             when no_data_found then
                               exit;
                             when others then
                               print_log('Last Line# '||l_count||', WW OOP Record: '||oop_adp_rec(idx).hds_adp_ww_oop_record);                               
                               print_log('@ Inside file loop operation, error : '||sqlerrm);
                               exit;
                       end;
                  --
                 end loop; 
                 --
                 utl_file.fclose ( l_utl_file_id );
                 print_log ('After close for WW OOP adp csv [|] extract file : '||l_file);
                 --  
                 -- Begin logging file details for each BU along with their concur batch id
                 begin 
                        --
                        -- setup the email body
                        setup_email_body 
                          (
                             p_text1 =>'BU CONCUR employee group : '||g_WW_emp_grp
                            ,p_emp_group =>g_WW_emp_grp
                            ,p_type =>'WW_OOP'
                          ); 
                        --
                        --
                        g_email_subject :=g_instance||' '||g_WW_emp_grp||' CONCUR OOP ADP extract to SharePoint - Status Update';
                        print_log('Email Subject :'||g_email_subject);
                        --
                        set_bu_sharepoint_email (p_emp_group =>g_WW_emp_grp, p_type =>g_control_type_AA); --g_sharepoint_email is assigned here
                        print_log('SharePoint Email  :'||g_sharepoint_email);                        
                        --
                        -- push the extract to sharepoint
                        print_log('Begin: Push WaterWorks OOP ADP csv extract file to SharePoint.');
                        --
                        sharepoint_integration
                           (
                                p_emp_group =>g_WW_emp_grp,
                                p_file =>g_directory_path||'/'||l_file,
                                p_sharepoint_email =>g_sharepoint_email,
                                p_dummy_email_from =>g_notif_email_from,
                                p_email_subject =>g_email_subject
                           )
                           ;
                        --
                        print_log('End: Push WaterWorks OOP ADP csv extract file to SharePoint.');                           
                        -- email the teams about the extract
                        send_email_notif
                           (
                                p_email_from      =>g_notif_email_from,
                                p_email_to           =>g_notif_email_to,
                                p_email_cc           =>g_notif_email_cc,
                                p_email_subject =>g_email_subject, 
                                p_email_body     =>g_notif_email_body,
                                p_mime_type      =>g_email_mime_type
                           )
                          ; 
                        print_log('After sending email notification about WaterWorks OOP ADP csv file status.');                                                                                                
                        --                     
                 exception
                  when others then
                   print_log('@Failed to log file details for BU, @ emp_group '||g_WW_emp_grp||', message =>'||sqlerrm);
                 end;    
                 --                                              
     --
    else
     --
     print_log('No OOP record found for extract into file.');
     --
    end if; -- Extract OOP file [WaterWorks only].
    --
    print_log('End: Extract OOP ADP file [WaterWorks only] file.');
    --            
    -- End WW ADP file
    -- End Ver 1.6 
  exception
   when program_error then
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end EXTRACT_PEOPLESOFT_OOP;
  --
  procedure WW_EXTRACT_ELIGIBLE_GL_JE is
    --extract WaterWorks GL journal related info 
    --
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_char_60_4 varchar2(30) :=Null;
    l_creation_date date :=sysdate;  
    l_je_seq_logged boolean :=Null;
    --
    l_prev_batch_date date :=Null;
    l_sub_routine varchar2(30) :='ww_extract_eligible_gl_je'; 
    l_ww_je_seq varchar2(10) :=Null;
    --
    cursor c_fiscal_info is
        select period_name fiscal_period_name
                   ,effective_period_num fiscal_period_num
                   ,period_year fiscal_period_year
                   ,start_date fiscal_period_start
                   ,end_date fiscal_period_end
                   ,to_char(null) je_month
                   ,to_char(null) je_year                   
        from gl_period_statuses
        where 1 =2 -- forced so we can get the rowtype declared without any record retrieved
        ;    
    --
    c_fiscal_details c_fiscal_info%rowtype;
    --
    cursor c_uniq_concur_batches is
        select concur_batch_id
        from  xxcus.xxcus_src_concur_sae_n
        where 1 =1
            and crt_btch_id =g_current_batch_id --HDS DW loads this field per import..so we can expect mor than one concur_batch_id per crt_btch_id
            and allocation_custom1 =g_WW_emp_grp --For US use allocation_custom1
            and exists
             (
                  select '1'
                  from    fnd_lookup_values
                  where 1 =1
                        and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
                        and nvl(enabled_flag, 'N') ='Y'
                        and lookup_code =allocation_custom1
             )             
        group by concur_batch_id
        order by concur_batch_id asc  
        ;    
    --
    cursor ww_je_eligible_records (p_concur_batch_id in number) is
    select 
         null fiscal_period_name --Populated run time when the routine is invoked.
        ,null fiscal_period_num --Populated run time when the routine is invoked.
        ,null fiscal_period_year --Populated run time when the routine is invoked.
        ,null fiscal_period_start --Populated run time when the routine is invoked.
        ,null fiscal_period_end --Populated run time when the routine is invoked.
        ,null date_1 --not in use currently
        ,null date_2 --not in use currently
        ,null date_3 --not in use currently
        ,null date_4 --not in use currently
        ,null date_5 --not in use currently
        ,g_WW_gl_prod_code char_60_1  --WW je entity code
        ,null                                          char_60_2  --WW je month
        ,g_WW_gl_je_type              char_60_3  --WW je type
        ,lpad(allocation_custom3, 3, '0')                            char_60_4                                                                  -- WW je branch
        ,lpad(allocation_custom4, 3, '0')                            char_60_5                                                                  -- WW je cost center
        ,regexp_substr(journal_acct_code,'[0-9]+',1,1) char_60_6                                                                 --WW je main account
        ,regexp_substr(journal_acct_code,'[0-9]+',1,2) char_60_7                                                                 --WW je sub account
        --,substr((substr(rpt_key,-4)||'-'||emp_last_name||'-'||emp_first_name), 1, 25) char_60_8 --WW je expense description
        ,substr(rpt_key||'-'||emp_last_name||'-'||emp_first_name, 1, 25) char_60_8 --WW je expense description        
        ,null char_60_9 --not in use currently
        ,null char_60_10 --not in use currently
        ,null                                   num_1 --WW je year
        ,null                                   num_2 --WW je sequence starting from 3890 for the fiscal period
        ,journal_amt                  num_3 --WW je amount. Staright copy of the concur field amt_net_tax_tot_adj
       -- ,amt_net_tax_tot_adj  num_3 --WW je amount. Staright copy of the concur field amt_net_tax_tot_adj        
        ,null num_4 --not in use currently
        ,null num_5 --not in use currently
        ,null num_6 --not in use currently
        ,null num_7 --not in use currently
        ,null num_8 --not in use currently
        ,null num_9 --not in use currently
        ,null num_10 --not in use currently
        ,null char_150_11 --not in use currently
        ,null char_150_12 --not in use currently
        ,null char_150_13 --not in use currently
        ,null char_150_14 --not in use currently
        ,null char_150_15 --not in use currently
        ,null char_150_16 --not in use currently
        ,null char_150_17 --not in use currently
        ,null char_150_18 --not in use currently
        ,null char_150_19 --not in use currently
        ,null char_150_20 --not in use currently
        ,null char_1_21 --not in use currently
        ,null char_1_22 --not in use currently
        ,null char_1_23 --not in use currently
        ,null char_1_24 --not in use currently
        ,null char_1_25 --not in use currently
        ,null char_10_26 --not in use currently
        ,null char_10_27 --not in use currently
        ,null char_10_28 --not in use currently
        ,null char_10_29 --not in use currently
        ,null char_10_30 --not in use currently
        ,null char_25_31 --not in use currently
        ,null char_25_32 --not in use currently
        ,null char_25_33 --not in use currently
        ,null char_25_34 --not in use currently
        ,null char_25_35 --not in use currently
        ,null char_25_36 --not in use currently
        ,null coa_code_1 --not in use currently
        ,null coa_code_2 --not in use currently
        ,null coa_code_3 --not in use currently
        ,null coa_code_4 --not in use currently
        ,null coa_code_5 --not in use currently
        ,null coa_code_6 --not in use currently
        ,null coa_code_7 --not in use currently
        ,null coa_desc_1 --not in use currently
        ,null coa_desc_2 --not in use currently
        ,null coa_desc_3 --not in use currently
        ,null coa_desc_4 --not in use currently
        ,null coa_desc_5 --not in use currently
        ,null coa_desc_6 --not in use currently
        ,null coa_desc_7 --not in use currently
        ,concur_batch_id --Straight copy from Concur.
        ,concur_batch_date --Straight copy from Concur.
        ,rpt_id --Straight copy from Concur.
        ,rpt_key --Straight copy from Concur.
        ,rpt_entry_key --Straight copy from Concur.
        ,journal_rpj_key --Straight copy from Concur.
        ,allocation_key --Straight copy from Concur.
        ,line_number --Straight copy from Concur.
        ,emp_id --Straight copy from Concur.
        ,emp_last_name --Straight copy from Concur.
        ,emp_first_name --Straight copy from Concur.
        ,emp_mi --Straight copy from Concur.
        ,emp_custom21 emp_group --Straight copy from Concur.
        ,emp_org_unit1 emp_org_erp             --For "US" use emp_org_unit1. Since WW has no Canadian org its always emp_org_unit1.
        ,emp_org_unit2 emp_org_company  --For "US" use emp_org_unit2. Since WW has no Canadian org its always emp_org_unit2.
        ,emp_org_unit3 emp_org_branch      --For "US" use emp_org_unit3. Since WW has no Canadian org its always emp_org_unit3.
        ,emp_org_unit4 emp_org_dept           --For "US" use emp_org_unit4. Since WW has no Canadian org its always emp_org_unit4.
        ,emp_org_unit6 emp_org_project      --For "US" use emp_org_unit6. Since WW has no Canadian org its always emp_org_unit6.
        ,ledger_ledger_code ledger_code --Straight copy from Concur.
        ,emp_currency_alpha_code --Straight copy from Concur.
        ,rpt_submit_date --Straight copy from Concur.
        ,rpt_user_defined_date --Straight copy from Concur.
        ,rpt_pmt_processing_date --Straight copy from Concur.
        ,rpt_name --Straight copy from Concur.
        ,rpt_entry_trans_type --Straight copy from Concur.
        ,rpt_entry_expense_type_name --Straight copy from Concur.
        ,rpt_entry_vendor_name --Straight copy from Concur.
        ,rpt_entry_vendor_desc --Straight copy from Concur.
        ,cc_trans_merchant_name --Straight copy from Concur.
        ,cc_trans_desc --Straight copy from Concur.
        ,rpt_entry_location_city_name --Straight copy from Concur.
        ,cc_trans_merchant_city --Straight copy from Concur.
        ,rpt_entry_trans_date --Straight copy from Concur.
        ,cc_trans_trans_date --Straight copy from Concur.
        ,cc_trans_trans_id --Straight copy from Concur.
        ,cc_trans_merchant_state --Straight copy from Concur.  
        ,rpt_entry_from_location --Straight copy from Concur.
        ,rpt_entry_to_location --Straight copy from Concur.
        ,car_log_entry_business_dist --Straight copy from Concur.       
        ,rpt_entry_ctry_sub_code --Straight copy from Concur.   
        ,rpt_entry_pmt_type_code --Straight copy from Concur.
        ,rpt_entry_pmt_code_name --Straight copy from Concur.
        ,journal_payer_pmt_type_name --Straight copy from Concur.
        ,journal_payer_pmt_code_name --Straight copy from Concur.
        ,journal_payee_pmt_type_name --Straight copy from Concur.
        ,journal_payee_pmt_code_name --Straight copy from Concur.
        ,journal_acct_code --Straight copy from Concur.
        ,journal_debit_or_credit --Straight copy from Concur.
        ,journal_amt --Straight copy from Concur.
        ,allocation_pct --Straight copy from Concur.
        ,allocation_custom1 allocation_erp            --For "US" use allocation_custom1. For Canada use allocation_custom7. For WW there is no canadian allocation.
        ,allocation_custom2 allocation_company --For "US" use allocation_custom2. For Canada use allocation_custom8. For WW there is no canadian allocation.
        ,allocation_custom3 allocation_branch     --For "US" use allocation_custom3. For Canada use allocation_custom9. For WW there is no canadian allocation.
        ,allocation_custom4 allocation_dept          --For "US" use allocation_custom4. For Canada use allocation_custom10.For WW there is no canadian allocation.
        ,allocation_custom11 allocation_project     --For "US" use allocation_custom11. For Canada use allocation_custom12.For WW there is no canadian allocation.
        ,amt_net_tax_tot_adj --Straight copy from Concur.
        ,amt_net_tax_tot_rclm --Straight copy from Concur.
        ,rpt_vw_tot_personal_amt --Straight copy from Concur.
        ,rpt_vw_tot_claimed_amt --Straight copy from Concur.
        ,rpt_vw_tot_due_emp --Straight copy from Concur.
        ,rpt_vw_tot_due_cmpny_card --Straight copy from Concur.
        ,rpt_vw_actual_tot_due_emp --Straight copy from Concur.
        ,rpt_vw_actl_tot_due_cmp_crd --Straight copy from Concur.
        ,rpt_vw_tot_posted_amt --Straight copy from Concur.
        ,rpt_vw_tot_approved_amt --Straight copy from Concur.
        ,rpt_entry_is_personal --Straight copy from Concur.
        ,rpt_entry_trans_amt --Straight copy from Concur.
        ,rpt_entry_posted_amt --Straight copy from Concur.
        ,rpt_entry_approved_amt --Straight copy from Concur.
        ,rpt_entry_tot_tax_posted_amt --Straight copy from Concur.
        ,rpt_entry_vw_net_tax_amt --Straight copy from Concur.
        ,rpt_entry_tot_rclm_adj_amt --Straight copy from Concur.
        ,rpt_ent_vw_net_rclm_adj_amt --Straight copy from Concur.
        ,rpt_entry_vw_net_adj_tax_amt --Straight copy from Concur.
        ,g_current_batch_id crt_btch_id
    from  xxcus.xxcus_src_concur_sae_n
    where 1 =1
         and crt_btch_id =g_current_batch_id --HDS DW loads this field per import..so we can expect mor than one concur_batch_id per crt_btch_id
         and concur_batch_id =p_concur_batch_id
         and allocation_custom1 =g_WW_emp_grp --For US use allocation_custom1
         --and allocation_custom2 =g_WW_gl_prod_code -- journals marked for WaterWorks only
    ;
    --
    type ww_elig_je_rec_type is table of xxcus.xxcus_concur_eligible_je%rowtype index by binary_integer;
    ww_elig_je_rec ww_elig_je_rec_type;
    --
  begin  
    --
    for rec in c_uniq_concur_batches loop
     -- @@@
            delete xxcus.xxcus_concur_eligible_je 
            where 1 =1
                 and crt_btch_id =g_current_batch_id
                 and concur_batch_id =rec.concur_batch_id
                 and allocation_erp =g_WW_emp_grp
                 --and allocation_company =g_WW_gl_prod_code
                 ;-- journals marked for WaterWorks only          
            --
            print_log('@ '||l_sub_routine||' Flush any previous loaded eligible JE records for employee group '||g_WW_emp_grp||', crt_btch_id : '||g_current_batch_id||', concur_batch_id :'||rec.concur_batch_id||', total deleted :'||sql%rowcount);
            --
              l_je_seq_logged :=FALSE; --This tell us we have not logged the JE sequence for the fiscal period. We do this one time only per concur batch id.             
            --            
            open ww_je_eligible_records ( p_concur_batch_id =>rec.concur_batch_id );
            --
            loop
             --
             fetch ww_je_eligible_records bulk collect into ww_elig_je_rec limit xxcus_concur_pkg.g_limit;
             --
             exit when ww_elig_je_rec.count =0;
                 --
                 if ww_elig_je_rec.count >0 then 
                  --
                    for idx in 1 .. ww_elig_je_rec.count loop 
                      --
                      -- get fiscal period details
                      --
                      if idx =1 then 
                           --  since it's first record get the fiscal period details
                            begin --begin get fiscal info
                             --
                             l_prev_batch_date :=ww_elig_je_rec(idx).concur_batch_date; 
                             --
                                select /*+ RESULT_CACHE */
                                             period_name fiscal_period_name
                                           ,effective_period_num fiscal_period_num
                                           ,period_year fiscal_period_year
                                           ,start_date fiscal_period_start
                                           ,end_date fiscal_period_end
                                           ,case
                                              when period_num !=12 --Except for Jan'YY fiscal period
                                                then lpad(to_char((period_num+1)), 2,'0')
                                               else lpad('1', 2,'0')
                                            end  je_month
                                           ,case
                                              when to_number(to_char(end_date, 'YYYY')) >to_number(to_char(start_date, 'YYYY'))
                                                then to_char((period_year+1))
                                               else to_char(period_year)
                                            end  je_year 
                                into   c_fiscal_details
                                from gl_period_statuses
                                where 1 =1
                                    and application_id =101
                                    and ledger_id =g_US_ledger_id
                                    and ww_elig_je_rec(idx).concur_batch_date between start_date and end_date
                                    ;                 
                             --
                            exception
                             when no_data_found then
                              c_fiscal_details :=null;
                             when others then
                              c_fiscal_details :=null;
                            end; --end get fiscal info
                            --    
                           -- Begin logging the first time generated je sequence in a control table
                           if (NOT l_je_seq_logged) then
                            --
                            l_ww_je_seq := Get_WW_JE_Seq ( c_fiscal_details.fiscal_period_name );
                            --
                             begin
                                    insert into xxcus.xxcus_concur_ww_je_controls
                                    (
                                     request_id
                                    ,last_je_number
                                    ,created_by 
                                    ,creation_date
                                    ,fiscal_period
                                    ,concur_batch_id
                                    )
                                    values
                                    (
                                     l_request_id
                                    ,l_ww_je_seq
                                    ,l_created_by 
                                    ,l_creation_date
                                    ,c_fiscal_details.fiscal_period_name
                                    ,rec.concur_batch_id
                                    );
                             exception
                              when others then
                               rollback to sqr1;
                               print_log('Failed to insert into xxcus.xxcus_concur_ww_je_controls, new je seq# '||l_ww_je_seq||', message =>'||sqlerrm);     
                             end;
                            --      
                            l_je_seq_logged :=TRUE;
                            --
                           end if;
                           --End logging the first time generated je sequence in a control table                   
                           --                                            
                      else
                       --
                           if l_prev_batch_date != ww_elig_je_rec(idx).concur_batch_date then
                                begin --begin get fiscal info
                                  --
                                    select /*+ RESULT_CACHE */
                                                period_name fiscal_period_name
                                               ,effective_period_num fiscal_period_num
                                               ,period_year fiscal_period_year
                                               ,start_date fiscal_period_start
                                               ,end_date fiscal_period_end
                                               ,case
                                                  when period_num !=12 --Except for Jan'YY fiscal period
                                                    then lpad(to_char((period_num+1)), 2,'0')
                                                   else lpad('1', 2,'0')
                                                end  je_month
                                               ,case
                                                  when to_number(to_char(end_date, 'YYYY')) >to_number(to_char(start_date, 'YYYY'))
                                                    then to_char((period_year+1))
                                                   else to_char(period_year)
                                                end  je_year                                       
                                    into   c_fiscal_details
                                    from gl_period_statuses
                                    where 1 =1
                                        and application_id =101
                                        and ledger_id =g_US_ledger_id
                                        and ww_elig_je_rec(idx).concur_batch_date between start_date and end_date
                                        ;                 
                                 --
                                exception
                                 when no_data_found then
                                  c_fiscal_details :=null;
                                 when others then
                                  c_fiscal_details :=null;
                                end; --end get fiscal info               
                           else
                            Null; --What this means is DO NOT reset the rowtype variable c_fiscal_details
                           end if;
                             --
                             l_prev_batch_date :=ww_elig_je_rec(idx).concur_batch_date; 
                             --
                      end if;
                      --
                      -- Assign fiscal info related fields
                      --               
                        ww_elig_je_rec(idx).fiscal_period_name  :=c_fiscal_details.fiscal_period_name;
                        ww_elig_je_rec(idx).fiscal_period_num    :=c_fiscal_details.fiscal_period_num;
                        ww_elig_je_rec(idx).fiscal_period_year    :=c_fiscal_details.fiscal_period_year;
                        ww_elig_je_rec(idx).fiscal_period_start   :=c_fiscal_details.fiscal_period_start;
                        ww_elig_je_rec(idx).fiscal_period_end     :=c_fiscal_details.fiscal_period_end;
                        ww_elig_je_rec(idx).char_60_2                  :=c_fiscal_details.je_month;
                        ww_elig_je_rec(idx).num_1                         :=substr(c_fiscal_details.je_year, 3,2);
                      --
                         ww_elig_je_rec(idx).num_2 :=l_ww_je_seq; --Assign a unique JE SEQ per extract
                      --
                      -- transform incoming allocation_branch if we have a corresponding cross mapping
                        --ww_elig_je_rec(idx).char_60_4 :=
                         l_char_60_4 :=
                          transform_ww_branch 
                                 (
                                   p_project_code =>ww_elig_je_rec(idx).allocation_project
                                  ,p_branch =>lpad(ww_elig_je_rec(idx).allocation_branch, 3, '0')
                                  ,p_concur_batch_id =>ww_elig_je_rec(idx).concur_batch_id
                                 );
                      --
                      -- When the branch override happens because of project code we default the cost center to 030 as well as the branch
                      --
                          if (l_char_60_4 != lpad(ww_elig_je_rec(idx).allocation_branch, 3, '0')) then 
                           ww_elig_je_rec(idx).char_60_4 :=l_char_60_4;
                           ww_elig_je_rec(idx).char_60_5 :=g_WW_override_dept;                       
                          else
                           null; --What this means is we will leave the collection ww_elig_je_rec(idx).char_60_5 as it is
                          end if;
                      --
                    end loop;
                      --
                      -- bulk insert into xxcus.xxcus_concur_ww_eligible_je table
                      --
                      begin
                       forall idx in 1 .. ww_elig_je_rec.count 
                        insert into xxcus.xxcus_concur_eligible_je values ww_elig_je_rec(idx);
                      exception
                       when others then
                        print_log('Error in bulk insert of xxcus.xxcus_concur_ww_eligible_je, message =>'||sqlerrm);
                        raise program_error;
                      end;
                  --
                 end if;
                 --
            end loop;
            close ww_je_eligible_records;
            --
     -- @@@ 
    end loop;
    --
  exception
   when program_error then
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end WW_EXTRACT_ELIGIBLE_GL_JE;        
  --
  procedure WW_OB_GL_JE  IS
   --
    l_sub_routine varchar2(30) :='ww_ob_gl_je';
    l_line_count number :=0;
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;      
    --
    l_Interco_Acct_Date Date :=Trunc(Sysdate);--WW accounting date used for Intercompany journal entries
    l_Interco_Location    xxcus.xxcus_concur_gl_iface.segment2%type :='Z0186';
    l_Interco_Cost_Ctr    xxcus.xxcus_concur_gl_iface.segment3%type :='0000';
    l_Interco_Liability_Acct     xxcus.xxcus_concur_gl_iface.segment4%type :='211121';
    l_Interco_Clearing_Acct     xxcus.xxcus_concur_gl_iface.segment4%type :='910137';    
    l_Interco_Project       xxcus.xxcus_concur_gl_iface.segment5%type :='00000';
    --
    l_Interco_Liability_Desc  xxcus.xxcus_concur_gl_iface.reference10%type :='Cleared with Payroll';
    l_Interco_Clearing_Desc  xxcus.xxcus_concur_gl_iface.reference10%type :='GSC IC with WW';
    --
    l_utl_file_id   UTL_FILE.FILE_TYPE;
    l_line_buffer VARCHAR2(30000):= Null;
    l_file VARCHAR2(150) :=Null;
    -- 
--    cursor c_uniq_batch is
--        select to_char(listagg(a.batch_id, ',') within group (order by a.batch_id)) batch_id
--        from (
--                    select distinct concur_batch_id batch_id
--                    from xxcus.xxcus_concur_eligible_je
--                    where 1 =1
--                        and crt_btch_id =g_current_batch_id
--                        and emp_group =g_WW_emp_grp            
--                        and allocation_company =g_WW_gl_prod_code            
--            ) a
--          ;    
    --
--    c_uniq_batch_rec c_uniq_batch%rowtype;
    --
    cursor c_uniq_concur_batches is
        select concur_batch_id, to_char(concur_batch_date, 'mm/dd/yyyy') concur_batch_date                                                                  
        from xxcus.xxcus_concur_eligible_je
        where 1 =1
            and crt_btch_id =g_current_batch_id
            and allocation_erp =g_WW_emp_grp
        group by concur_batch_id ,to_char(concur_batch_date, 'mm/dd/yyyy') 
        order by concur_batch_id asc  
        ;  
    --
    cursor c_summary_je (p_concur_batch_id in number) is
        select CHAR_60_1   je_entity          
                   ,CHAR_60_2   je_month
                   ,NUM_1           je_year
                   ,CHAR_60_3   je_type
                   ,NUM_2           je_seq 
                   ,CHAR_60_4   je_branch
                   ,CHAR_60_5   je_dept
                   ,CHAR_60_6   je_main_acct
                   ,CHAR_60_7   je_sub_acct
                   ,CHAR_60_8   je_expense_description           
                   ,NUM_3           je_amount                                                                      
        from xxcus.xxcus_concur_eligible_je
        where 1 =1
            and crt_btch_id =g_current_batch_id
            and allocation_erp =g_WW_emp_grp
            and concur_batch_id =p_concur_batch_id --Cursor parameter
        order by rpt_key asc, line_number asc
                   ;
    --
    type c_summary_je_type is table of c_summary_je%rowtype index by binary_integer;
    c_summary_rec  c_summary_je_type; 
    interco_liab_rec  c_summary_je_type;
    -- 
    cursor c_WW_je_supporting_info (p_concur_batch_id in number) is
        select CHAR_60_1   je_entity          
                   ,CHAR_60_2   je_month
                   ,NUM_1           je_year
                   ,CHAR_60_3   je_type
                   ,NUM_2           je_seq 
                   ,CHAR_60_4   je_branch
                   ,allocation_branch concur_branch
                   ,CHAR_60_5   je_dept
                   ,CHAR_60_6   je_main_acct
                   ,CHAR_60_7   je_sub_acct
                   ,CHAR_60_8   je_expense_description           
                   ,NUM_3           je_amount
                  ,emp_id                                                                     emp_id            
                  ,emp_last_name                                                     emp_last_name
                  ,emp_first_name                                                    emp_first_name
                  ,emp_mi                                                                    emp_mi
                  ,rpt_key                                                                     rpt_key
                  ,to_char(rpt_submit_date,'DD-MON-YYYY')  rpt_submit_date
                  ,allocation_project                                                report_entry_org_project
                  ,rpt_entry_expense_type_name                       rpt_entry_expense_type_name
                  ,rpt_entry_vendor_name                                    rpt_entry_vendor_name
                  ,rpt_entry_vendor_desc                                       rpt_entry_vendor_desc
                  ,cc_trans_merchant_name                                 merchant_name
                  ,cc_trans_desc                                                        merchant_description
                 ,cc_trans_merchant_city                                      transaction_city                  
                 ,rpt_entry_location_city_name                          rpt_entry_location_city
                 ,cc_trans_merchant_state                                   transaction_state
                 ,rpt_entry_trans_date                                          report_entry_trans_date
                 ,cc_trans_trans_date                                            credit_card_trans_date
                 ,cc_trans_trans_id                                                 credit_card_trans_id
                ,rpt_entry_from_location                                    rpt_entry_from_location
                ,rpt_entry_to_location                                         rpt_entry_to_location
                ,car_log_entry_business_dist                            car_business_distance 
                ,rpt_entry_ctry_sub_code                                  rpt_entry_ctry_sub_code
        from xxcus.xxcus_concur_eligible_je
        where 1 =1
            and crt_btch_id =g_current_batch_id
            and allocation_erp =g_WW_emp_grp
            and concur_batch_id =p_concur_batch_id --Cursor parameter
        order by rpt_key asc, line_number asc
                   ;    
    --  
    type c_WW_JE_details_type is table of c_WW_je_supporting_info%rowtype index by binary_integer;
    c_WW_JE_details_rec  c_WW_JE_details_type; 
    -- 
    --     
  begin
     --
     g_outbound_loc :=GET_OUTBOUND_FILE_LOCATION ( v_operation =>'DBA_DIRECTORY', v_emp_group  =>g_WW_MINCRON);
     --
     print_log ('Current working DBA directory is '||g_outbound_loc);
     --     
     for rec in c_uniq_concur_batches loop
       --
       g_concur_gl_intf_rec.delete;
       --
       -- ### @ begin rec in c_uniq_concur_batches loop
                 --
                 if g_database =g_prd_database then   
                   g_file_prefix :=g_WW_MINCRON; --Null; -- Ver 1.1
                 else
                   g_file_prefix :=g_database||'_'||g_WW_MINCRON;
                 end if;
                 --
                 l_file :=g_file_prefix||'_BATCH_'||rec.concur_batch_id||'_JE_ACTUALS_'||TO_CHAR(g_batch_date, 'MMDDYYYY')||'.txt';                 
                 --
                 print_log('Before file open for WW JE Actuals.');
                 --
                 l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
                 --
                 print_log ('File handle initiated for WW JE Actuals : '||l_file);
                 --                         
                 open c_summary_je (p_concur_batch_id =>rec.concur_batch_id);
                 --
                 loop
                  --
                  fetch c_summary_je bulk collect into c_summary_rec;
                  --
                  exit when c_summary_rec.count =0;
                  --
                      if c_summary_rec.count >0 then 
                         --
                         Null;
                         --
                         -- Begin copy the WW JE actuals header record to the file              
                         --
                         begin
                              --
                              -- Begin Ver 1.4
                              l_line_buffer := 'ENTITY CODE'
                                  ||','
                                  ||''
                                  ||','          
                                  ||'MONTH'
                                  ||','
                                  ||''
                                  ||','            
                                  ||'YEAR'
                                  ||','
                                  ||''
                                  ||','           
                                  ||'JE TYPE'
                                  ||','         
                                  ||'JE #'
                                  ||','
                                  ||''
                                  ||','           
                                  ||'BRANCH'
                                  ||','
                                  ||''
                                  ||','           
                                  ||'DEPT'
                                  ||','
                                  ||''
                                  ||','
                                  ||'MINCRON GL MAIN'
                                  ||','
                                  ||''
                                  ||','           
                                  ||'MINCRON GL SUB'
                                  ||','
                                  ||''
                                  ||',' 
                                  ||'AMOUNT'
                                  ||','
                                  ||''
                                  ||','
                                  ||'EXPENSE DESCRIPTION'
                                  ||','
                                  ;        
                              -- End Ver 1.4                                            
                              --
                              utl_file.put_line ( l_utl_file_id, l_line_buffer);
                              -- 
                              l_line_count:= l_line_count+1; --number of lines written
                              --
                          exception
                                 when no_data_found then
                                   exit;
                                 when others then
                                   print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                   print_log('@ Inside file loop operation, error : '||sqlerrm);
                                   exit;
                           end;              
                         --
                         --End copy the WW JE actuals header record to the file              
                         --
                         l_line_buffer :=Null;
                         --
                         --Begin copy the WW JE actuals detail records to the file              
                         --
                         for idx in 1 .. c_summary_rec.count loop 
                           --
                           if idx =1 then
                            interco_liab_rec(1) :=c_summary_rec(idx); --Just to get some common attributes so we can get the liability intercompany clearing records created later
                           end if;
                           --
                           l_line_buffer :=
                              -- Begin Ver 1.4                             
                                                        c_summary_rec(idx).je_entity
                                                                  ||','
                                                                  ||''
                                                                  ||','          
                                                                  ||c_summary_rec(idx).je_month
                                                                  ||','
                                                                  ||''
                                                                  ||','            
                                                                  ||c_summary_rec(idx).je_year
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_summary_rec(idx).je_type
                                                                  ||','       
                                                                  ||c_summary_rec(idx).je_seq
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_summary_rec(idx).je_branch
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_summary_rec(idx).je_dept
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_summary_rec(idx).je_main_acct
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_summary_rec(idx).je_sub_acct
                                                                  ||','
                                                                  ||''
                                                                  ||',' 
                                                                  ||c_summary_rec(idx).je_amount
                                                                  ||','
                                                                  ||''
                                                                  ||','                    
                                                                  ||c_summary_rec(idx).je_expense_description
                                                                  ||','
                              -- End Ver 1.4                                                                    
                                                                  ;                
                           --
                                   begin
                                      --
                                      utl_file.put_line ( l_utl_file_id, l_line_buffer);
                                      -- 
                                      l_line_count:= l_line_count+1; --number of lines written
                                      --
                                      g_total := g_total + c_summary_rec(idx).je_amount;
                                      --
                                   exception
                                         when no_data_found then
                                           exit;
                                         when others then
                                           print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                           print_log('@ Inside file loop operation, error : '||sqlerrm);
                                           exit;
                                   end;                
                           --
                          end loop;
                         --
                         --End copy the WW JE actuals detail records to the file              
                         --
                      end if; 
                  --
                 end loop;
                 --
                 close c_summary_je;
                 --
                 -- Begin copy interco clearing rec [WW IC with GSC]
                 --
                 begin
                          --
                           l_line_buffer :=
                              -- Begin Ver 1.4                            
                                                        g_WW_gl_prod_code --interco_liab_rec(1).je_entity --???? Check with Mike Macrae
                                                                  ||','
                                                                  ||''
                                                                  ||','          
                                                                  ||interco_liab_rec(1).je_month
                                                                  ||','
                                                                  ||''
                                                                  ||','            
                                                                  ||interco_liab_rec(1).je_year
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||interco_liab_rec(1).je_type
                                                                  ||','        
                                                                  ||interco_liab_rec(1).je_seq
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||'901' --interco_liab_rec(1).je_branch
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||'0' --interco_liab_rec(1).je_dept 
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||'4413'  -- '3300' --interco_liab_rec(1).je_main_acct -- Ver 1.6
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||'000'  -- '137' --interco_liab_rec(1).je_sub_acct -- Ver 1.6 
                                                                  ||','
                                                                  ||''
                                                                  ||',' 
                                                                  ||( (-1) * g_total )
                                                                  ||','
                                                                  ||''
                                                                  ||','                    
                                                                  ||'WW IC with GSC' 
                                                                  ||','
                              -- End Ver 1.4                                                                   
                                                                  ;              
                          --
                          utl_file.put_line ( l_utl_file_id, l_line_buffer);
                          -- 
                          l_line_count:= l_line_count+1; --number of lines written
                          --
                          print_log('Copied interco clearing record [WW IC with GSC] to file '||l_file);
                          --
                 exception
                     when others then
                       print_log('@ Interco clearing, error =>'||sqlerrm||', line :'||l_line_buffer);
                 end;
                 --                         
                 -- End copy interco clearing rec [WW IC with GSC]
                 --
                 Null;
                 --
                 -- Begin copy liability rec [ Insert to a table..do not copy to WW JE actuals file]
                 --
                 begin
                          -- Set set of books id
                          g_concur_gl_intf_rec(1).set_of_books_id :=g_US_ledger_id;
                          -- Set accounting date
                          g_concur_gl_intf_rec(1).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                          -- Set currency          
                          g_concur_gl_intf_rec(1).currency_code := Get_Currency (p_emp_group =>g_WW_emp_grp); --Set currency based on concur emp group
                          -- Set Batch Name and description
                          g_concur_gl_intf_rec(1).reference1 :='Concur Batch '||rec.concur_batch_id;                                                    
                          g_concur_gl_intf_rec(1).reference2 :='Employee Group :'||g_WW_emp_grp;                          
                          --
                          -- Set Journal entry name and description
                          g_concur_gl_intf_rec(1).reference4 :='Interco OOP-Card '||',Batch '||rec.concur_batch_id; --GL used first 25 characters to prepend the journal entry name                                                   
                          g_concur_gl_intf_rec(1).reference5 :='Employee Group :' -- Journal entry description
                                                                                                  ||g_WW_emp_grp
                                                                                                  ||', Concur Batch Id ('||rec.concur_batch_id||') '
                                                                                                  ||'Interco for OOP and Card.'
                                                                                                  ;                          
                          --
                          -- Set Journal entry line description                                                                            
                          g_concur_gl_intf_rec(1).reference10 :='Concur Batch '||rec.concur_batch_id
                                                                                                     ||', '
                                                                                                     ||l_Interco_Clearing_Desc
                                                                                                  ;                          
                          --      
                          g_concur_gl_intf_rec(1).group_id :=g_WW_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                          --
                          g_concur_gl_intf_rec(1).reference21 :=interco_liab_rec(1).je_month||' / '||interco_liab_rec(1).je_year; --WW JE Month and Year
                          g_concur_gl_intf_rec(1).reference22 :=interco_liab_rec(1).je_type||' / '||interco_liab_rec(1).je_seq; --WW JE Type and Sequence
                          g_concur_gl_intf_rec(1).reference23 :='Concur Batch ID - '||rec.concur_batch_id; --Concur Batch ID
                          g_concur_gl_intf_rec(1).reference24 :='Concur Batch Date - '||rec.concur_batch_date; --Concur Batch Date
                          --
                          /*
                          l_line_numbers :=Get_Line_Numbers_Per_Batch --Concur  line numbers for a concur_batch_id
                                                                                                       (
                                                                                                         p_emp_group =>g_WW_emp_grp
                                                                                                        ,p_prod_code =>g_WW_gl_prod_code
                                                                                                        ,p_concur_batch_id =>rec.concur_batch_id
                                                                                                       ) ;
                          */                          
                          --
                          g_concur_gl_intf_rec(1).reference25 :=Null;  
                          g_concur_gl_intf_rec(1).reference26 :=Null;                          
                          g_concur_gl_intf_rec(1).reference27 :=Null; 
                          g_concur_gl_intf_rec(1).reference28 :=Null;
                          g_concur_gl_intf_rec(1).reference29 :=g_run_id;
                          g_concur_gl_intf_rec(1).reference30 :=Null;                                                                          
                          --                    
                          --Begin chart of accounts segments
                          --
                          -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                          g_concur_gl_intf_rec(1).segment1 := Get_Interco_GL_Product (p_emp_group =>g_WW_emp_grp); --Set segment1 based on concur emp group
                          --
                          g_concur_gl_intf_rec(1).segment2 :=l_Interco_Location;
                          g_concur_gl_intf_rec(1).segment3 :=l_Interco_Cost_Ctr;
                          g_concur_gl_intf_rec(1).segment4 :=l_Interco_Liability_Acct;
                          --
                          g_concur_gl_intf_rec(1).segment5 :=l_Interco_Project;
                          g_concur_gl_intf_rec(1).segment6 :=g_future_use_1;
                          g_concur_gl_intf_rec(1).segment7 :=g_future_use_2;
                          g_concur_gl_intf_rec(1).attribute2 :=g_event_type_IC;                          
                          --
                          --End chart of accounts segments    
                          -- set Credit amount         
                          g_concur_gl_intf_rec(1).entered_cr :=g_total;                          
                          --
                          print_log('Finished interco liability record');
                          --
                          -- ^^^^ Setup intercompnay clearing debit journal 
                          -- Set set of books id
                          g_concur_gl_intf_rec(2).set_of_books_id :=g_US_ledger_id;
                          -- Set accounting date
                          g_concur_gl_intf_rec(2).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                          -- Set currency          
                          g_concur_gl_intf_rec(2).currency_code := Get_Currency (p_emp_group =>g_WW_emp_grp); --Set currency based on concur emp group
                          -- Set Batch Name and description
                          g_concur_gl_intf_rec(2).reference1 :='Concur Batch '||rec.concur_batch_id;                                                    
                          g_concur_gl_intf_rec(2).reference2 :='Employee Group :'||g_WW_emp_grp;                          
                          --
                          -- Set Journal entry name and description
                          g_concur_gl_intf_rec(2).reference4 :='Interco OOP-Card '||',Batch '||rec.concur_batch_id; --GL used first 25 characters to prepend the journal entry name                                                   
                          g_concur_gl_intf_rec(2).reference5 :='Employee Group :' -- Journal entry description
                                                                                                  ||g_WW_emp_grp
                                                                                                  ||', Concur Batch Id ('||rec.concur_batch_id||') '
                                                                                                  ||'Interco for OOP and Card.'
                                                                                                  ;                          
                          --
                          -- Set Journal entry line description                                                                            
                          g_concur_gl_intf_rec(2).reference10 :='Concur Batch '||rec.concur_batch_id
                                                                                                     ||', '                          
                                                                                                     ||l_Interco_Clearing_Desc
                                                                                                  ;                          
                          --      
                          g_concur_gl_intf_rec(2).group_id :=g_WW_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                          --
                          g_concur_gl_intf_rec(2).reference21 :=interco_liab_rec(1).je_month||' / '||interco_liab_rec(1).je_year; --WW JE Month and Year
                          g_concur_gl_intf_rec(2).reference22 :=interco_liab_rec(1).je_type||' / '||interco_liab_rec(1).je_seq; --WW JE Type and Sequence
                          g_concur_gl_intf_rec(2).reference23 :='Concur Batch ID - '||rec.concur_batch_id; --Concur Batch ID
                          g_concur_gl_intf_rec(2).reference24 :='Concur Batch Date - '||rec.concur_batch_date; --Concur Batch Date
                          --
                          /*
                          l_line_numbers :=Get_Line_Numbers_Per_Batch --Concur  line numbers for a concur_batch_id
                                                                                                       (
                                                                                                         p_emp_group =>g_WW_emp_grp
                                                                                                        ,p_prod_code =>g_WW_gl_prod_code
                                                                                                        ,p_concur_batch_id =>rec.concur_batch_id
                                                                                                       ) ;
                          */                          
                          --
                          g_concur_gl_intf_rec(2).reference25 :=Null;  
                          g_concur_gl_intf_rec(2).reference26 :=Null;                          
                          g_concur_gl_intf_rec(2).reference27 :=Null; 
                          g_concur_gl_intf_rec(2).reference28 :=Null;
                          g_concur_gl_intf_rec(2).reference29 :=g_run_id;
                          g_concur_gl_intf_rec(2).reference30 :=Null;                                                                               
                          --                    
                          --Begin chart of accounts segments
                          --
                          -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                          g_concur_gl_intf_rec(2).segment1 := Get_Interco_GL_Product (p_emp_group =>g_WW_emp_grp); --Set segment1 based on concur emp group
                          --
                          g_concur_gl_intf_rec(2).segment2 :=l_Interco_Location;
                          g_concur_gl_intf_rec(2).segment3 :=l_Interco_Cost_Ctr;
                          g_concur_gl_intf_rec(2).segment4 :=l_Interco_Clearing_Acct;
                          --
                          g_concur_gl_intf_rec(2).segment5 :=l_Interco_Project;
                          g_concur_gl_intf_rec(2).segment6 :=g_future_use_1;
                          g_concur_gl_intf_rec(2).segment7 :=g_future_use_2;
                          g_concur_gl_intf_rec(2).attribute2 :=g_event_type_IC;                          
                          --
                          --End chart of accounts segments    
                          -- set Debit amount         
                          g_concur_gl_intf_rec(2).entered_dr :=g_total;                          
                          --
                          print_log('Finished interco clearing record');
                          --                                                    
                          -- ^^^^
                          -- Insert into xxcus_concur_gl_iface table
                          begin
                            /*
                            -- Begin Ver 1.6 --This is to comment the whole GSC intercompany with WW and not required any more
                            flush_concur_gl_iface 
                              (
                                 p_emp_group     =>g_WW_emp_grp
                                ,p_oop_or_card  =>Null --Not necessary
                                ,p_group_id         =>g_WW_grp_id
                              );                            
                            --
                            populate_concur_gl_iface 
                                (
                                   p_concur_gl_intf_rec  =>g_concur_gl_intf_rec
                                  ,p_group_id                    =>g_WW_grp_id                          
                                );
                            -- End Ver 1.6                              
                            */ --This is to comment the whole GSC intercompany with WW and not required any more
                            --
                             g_ww_separation_total :=g_total; --Ver 1.6
                             --                             
                          exception
                           when others then
                            print_log('@populate_concur_gl_iface, calling block error : '||sqlerrm);
                            raise program_error;
                          end;
                          --
                 exception
                     when others then
                        print_log('@Setup Interco Liability and Clearing, error =>'||sqlerrm||', line :'||l_line_buffer);
                 end;        
                 -- 
                 --             
                 print_log('Total records written into the file [Header and Lines] , l_line_count =>'||l_line_count);
                 --
                 -- Close file handle 
                 --
                 utl_file.fclose ( l_utl_file_id );
                 print_log ('After close for WW JE Actuals file : '||l_file);
                 --
                 -- Begin logging file details for each BU along with their concur batch id
                 begin 
                        --
                        savepoint start_here;
                        --
                        insert into xxcus.xxcus_concur_ebs_bu_files
                        (
                         request_id
                        ,crt_btch_id
                        ,concur_batch_id
                        ,emp_group
                        ,extracted_file_name
                        ,extracted_file_folder
                        ,created_by 
                        ,creation_date
                        )
                        values
                        (
                         l_request_id
                         ,g_current_batch_id
                        ,rec.concur_batch_id --c_uniq_batch_rec.batch_id
                        ,g_WW_emp_grp
                        ,l_file
                        ,g_directory_path
                        ,l_created_by
                        ,l_creation_date
                        )
                        ;
                        --
                        commit;
                        --
                        -- setup the email body
                        --setup_email_body (p_text1 =>'BU CONCUR employee group : '||g_WW_emp_grp); --g_notif_email_body is assigned here
                        setup_email_body 
                          (
                             p_text1 =>'BU CONCUR employee group : '||g_WW_emp_grp
                            ,p_emp_group =>g_WW_emp_grp
                            ,p_type =>'OTHERS'
                          ); --g_notif_email_body is assigned here                        
                        --print_log('Email Body :'||g_notif_email_body);
                        --
                        g_email_subject :=g_instance||' '||g_WW_emp_grp||' CONCUR extracts to SharePoint - Status Update';
                        print_log('Email Subject :'||g_email_subject);                        
                        --
                        set_bu_sharepoint_email (p_emp_group =>g_WW_emp_grp, p_type=>g_control_type_AA); --g_sharepoint_email is assigned here
                        print_log('SharePoint Email  :'||g_sharepoint_email);                        
                        --
                        -- push the extract to sharepoint
                        print_log('Begin: Push WW JE Actuals to SharePoint.');                        
                        sharepoint_integration
                           (
                                p_emp_group =>g_WW_emp_grp,
                                p_file =>g_directory_path||'/'||l_file,
                                p_sharepoint_email =>g_sharepoint_email,
                                p_dummy_email_from =>g_notif_email_from,
                                p_email_subject =>g_email_subject
                           )
                           ;
                        print_log('End: Push WW JE Actuals to SharePoint.');                           
                        --                    
                 exception
                  when others then
                   print_log('@Failed to log file details for BU, @ emp_group '||g_WW_emp_grp||', message =>'||sqlerrm);
                   rollback to start_here;
                 end;
                 -- End logging file details for each BU along with their concur batch id
                 --
                 -- Begin WW Je with Supporting details
                 --
                 if g_database =g_prd_database then   
                   g_file_prefix :=g_WW_MINCRON; --Null; -- Ver 1.1
                 else
                   g_file_prefix :=g_database||'_'||g_WW_MINCRON;
                 end if;
                 --                 
                 l_file :=g_file_prefix||'_BATCH_'||rec.concur_batch_id||'_JE_DETAILS_'||TO_CHAR(g_batch_date, 'MMDDYYYY')||'.txt';
                 --
                 print_log('Before file open for WW JE Actuals with supporting details.');
                 --
                 l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
                 --
                 print_log ('File handle initiated for WW JE Actuals with supporting details : '||l_file);
                 --                         
                 open c_WW_je_supporting_info (p_concur_batch_id =>rec.concur_batch_id) ;
                 --
                 loop
                  --
                  fetch c_WW_je_supporting_info bulk collect into c_WW_JE_details_rec;
                  --
                  exit when c_WW_JE_details_rec.count =0;
                  --
                      if c_WW_JE_details_rec.count >0 then 
                         --
                          begin
                              --
                              -- Begin Ver 1.4                               
                              l_line_buffer := 'ENTITY CODE'
                                  ||','
                                  ||''
                                  ||','          
                                  ||'MONTH'
                                  ||','
                                  ||''
                                  ||','            
                                  ||'YEAR'
                                  ||','
                                  ||''
                                  ||','           
                                  ||'JE TYPE'
                                  ||','
                                  ||''
                                  ||','           
                                  ||'JE #'
                                  ||','
                                  ||''
                                  ||','           
                                  ||'JE BRANCH'
                                  ||','
                                  ||''
                                  ||','                                  
                                  ||'CONCUR_BRANCH'
                                  ||','   
                                  ||''
                                  ||','                                          
                                  ||'DEPT'
                                  ||','
                                  ||''
                                  ||','
                                  ||'MINCRON GL MAIN'
                                  ||','
                                  ||''
                                  ||','           
                                  ||'MINCRON GL SUB'
                                  ||','
                                  ||''
                                  ||',' 
                                  ||'AMOUNT'
                                  ||','
                                  ||''
                                  ||','
                                  ||'EXPENSE DESCRIPTION'
                                  ||','                    
                                  ||'EMP_ID'                                                      
                                  ||','                    
                                  ||'EMP_LAST_NAME'
                                  ||','                    
                                  ||'EMP_FIRST_NAME'
                                  ||','                    
                                  ||'EMP_MI'
                                  ||','                    
                                  ||'RPT_KEY'    
                                  ||','                    
                                  ||'RPT_SUBMIT_DATE'                                                      
                                  ||','                    
                                  ||'REPORT_ENTRY_ORG_PROJECT'
                                  ||','                    
                                  ||'RPT_ENTRY_EXPENSE_TYPE_NAME'
                                  ||','                    
                                  ||'RPT_ENTRY_VENDOR_NAME'
                                  ||','                    
                                  ||'RPT_ENTRY_VENDOR_DESC'  
                                  ||','                    
                                  ||'MERCHANT_NAME'                                                      
                                  ||','                    
                                  ||'MERCHANT_DESCRIPTION'
                                  ||','                    
                                  ||'TRANSACTION_CITY'
                                  ||','                    
                                  ||'RPT_ENTRY_LOCATION_CITY'
                                  ||','                    
                                  ||'TRANSACTION_STATE'
                                  ||','                    
                                  ||'RPT_ENTRY_CTRY_SUB_CODE'                                                     
                                  ||','                    
                                  ||'REPORT_ENTRY_TRANS_DATE'
                                  ||','                    
                                  ||'CREDIT_CARD_TRANS_DATE'
                                  ||','                    
                                  ||'CREDIT_CARD_TRANS_ID'
                                  ||','                    
                                  ||'RPT_ENTRY_FROM_LOCATION' 
                                  ||','                    
                                  ||'RPT_ENTRY_TO_LOCATION'                                                      
                                  ||','                    
                                  ||'CAR_BUSINESS_DISTANCE' 
                                  ||','   
                              -- End Ver 1.4                                                      
                                  ;
                              --
                              utl_file.put_line ( l_utl_file_id, l_line_buffer);
                              -- 
                              l_line_count:= l_line_count+1; --number of lines written
                              --
                          exception
                                 when no_data_found then
                                   exit;
                                 when others then
                                   print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                   print_log('@ Inside file loop operation, error : '||sqlerrm);
                                   exit;
                           end;              
                         --
                         l_line_buffer :=Null;
                         --
                         for idx in 1 .. c_WW_JE_details_rec.count loop 
                           --
                           l_line_buffer :=
                              -- Begin Ver 1.4                            
                                                        c_WW_JE_details_rec(idx).je_entity
                                                                  ||','
                                                                  ||''
                                                                  ||','          
                                                                  ||c_WW_JE_details_rec(idx).je_month
                                                                  ||','
                                                                  ||''
                                                                  ||','            
                                                                  ||c_WW_JE_details_rec(idx).je_year
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_WW_JE_details_rec(idx).je_type
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_WW_JE_details_rec(idx).je_seq
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_WW_JE_details_rec(idx).je_branch
                                                                  ||','
                                                                  ||''
                                                                  ||','                                  
                                                                  ||c_WW_JE_details_rec(idx).concur_branch
                                                                  ||','   
                                                                  ||''
                                                                  ||','             
                                                                  ||c_WW_JE_details_rec(idx).je_dept
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_WW_JE_details_rec(idx).je_main_acct
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_WW_JE_details_rec(idx).je_sub_acct
                                                                  ||','
                                                                  ||''
                                                                  ||',' 
                                                                  ||c_WW_JE_details_rec(idx).je_amount
                                                                  ||','
                                                                  ||''
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).je_expense_description
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).emp_id                                                      
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).emp_last_name
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).emp_first_name
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).emp_mi
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).rpt_key    
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).rpt_submit_date                                                      
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).report_entry_org_project
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).rpt_entry_expense_type_name
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).rpt_entry_vendor_name
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).rpt_entry_vendor_desc  
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).merchant_name                                                      
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).merchant_description
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).transaction_city
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).rpt_entry_location_city
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).transaction_state
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).rpt_entry_ctry_sub_code                                                      
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).report_entry_trans_date
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).credit_card_trans_date
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).credit_card_trans_id
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).rpt_entry_from_location 
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).rpt_entry_to_location                                                      
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).car_business_distance
                                                                  ||','
                              -- End Ver 1.4                                                                   
                                                                  ;                                                           
                           --
                                   begin
                                      --
                                      utl_file.put_line ( l_utl_file_id, l_line_buffer);
                                      -- 
                                      l_line_count:= l_line_count+1; --number of lines written
                                      --
                                   exception
                                         when no_data_found then
                                           exit;
                                         when others then
                                           print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                           print_log('@ Inside file loop operation, error : '||sqlerrm);
                                           exit;
                                   end;                
                           --
                          end loop;
                         --    
                      end if; 
                  --
                 end loop;
                 --
                 close c_WW_je_supporting_info;
                 --             
                 print_log('Total records written into the file [Header and Lines] , l_line_count =>'||l_line_count);
                 --
                 utl_file.fclose ( l_utl_file_id );
                 print_log ('After close for WW JE Actuals file with supporting details : '||l_file);
                 --     
                 -- Close file handle      
                 -- End WW Je with Supporting details
                 --       
                 -- Begin logging file details for each BU along with their concur batch id
                 begin 
                        --
                        savepoint start_here;
                        --
                        insert into xxcus.xxcus_concur_ebs_bu_files
                        (
                         request_id
                        ,concur_batch_id
                        ,emp_group
                        ,extracted_file_name
                        ,extracted_file_folder
                        ,created_by 
                        ,creation_date
                        ,crt_btch_id
                        )
                        values
                        (
                         l_request_id
                        ,rec.concur_batch_id --c_uniq_batch_rec.batch_id
                        ,g_WW_emp_grp
                        ,l_file
                        ,g_directory_path
                        ,l_created_by
                        ,l_creation_date
                        ,g_current_batch_id
                        )
                        ;
                        --
                        commit;
                        --
                        -- setup the email body
                        --setup_email_body (p_text1 =>'BU CONCUR employee group : '||g_WW_emp_grp); --g_notif_email_body is assigned here
                        setup_email_body 
                          (
                             p_text1 =>'BU CONCUR employee group : '||g_WW_emp_grp
                            ,p_emp_group =>g_WW_emp_grp
                            ,p_type =>'OTHERS'
                          ); --g_notif_email_body is assigned here                           
                        --print_log('Email Body :'||g_notif_email_body);
                        --
                        g_email_subject :=g_instance||' '||g_WW_emp_grp||' CONCUR extracts to SharePoint - Status Update';
                        print_log('Email Subject :'||g_email_subject);                        
                        --
                        set_bu_sharepoint_email (p_emp_group =>g_WW_emp_grp, p_type =>g_control_type_AA); --g_sharepoint_email is assigned here
                        print_log('SharePoint Email  :'||g_sharepoint_email);                        
                        --
                        -- push the extract to sharepoint
                        print_log('Begin: Push WW JE Actuals with supporting details to SharePoint.');                        
                        sharepoint_integration
                           (
                                p_emp_group =>g_WW_emp_grp,
                                p_file =>g_directory_path||'/'||l_file,
                                p_sharepoint_email =>g_sharepoint_email,
                                p_dummy_email_from =>g_notif_email_from,
                                p_email_subject =>g_email_subject
                           )
                           ;
                        print_log('End: Push WW JE Actuals with supporting details to SharePoint.');                           
                        -- email the teams about the extract
                        send_email_notif
                           (
                                p_email_from      =>g_notif_email_from,
                                p_email_to           =>g_notif_email_to,
                                p_email_cc           =>g_notif_email_cc,
                                p_email_subject =>g_email_subject, 
                                p_email_body     =>g_notif_email_body,
                                p_mime_type      =>g_email_mime_type
                           )
                          ; 
                        print_log('After sending email notification about WaterWorks status');                                                                                                
                        --                     
                 exception
                  when others then
                   print_log('@Failed to log file details for BU, @ emp_group '||g_WW_emp_grp||', message =>'||sqlerrm);
                   rollback to start_here;
                 end;
                 -- End logging file details for each BU along with their concur batch id     
       -- ### @ end of rec in c_uniq_concur_batches loop
     end loop;     
     --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    raise program_error;
  end WW_OB_GL_JE;
  --  
  -- Begin CI Great Plains
  procedure CI_GP_EXTRACT_ELIGIBLE_GL_JE is 
    --extract Construction and Industrical [CI] Brafasco GL journal related info 
    --
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;  
    --
    l_prev_batch_date date :=Null;
    l_sub_routine varchar2(30) :='ci_gp_extract_eligible_gl_je'; --'WW_OB_GL_JE';
    l_ww_je_seq varchar2(10) :=Null;
    --
    cursor c_fiscal_info is
        select period_name fiscal_period_name
                   ,effective_period_num fiscal_period_num
                   ,period_year fiscal_period_year
                   ,start_date fiscal_period_start
                   ,end_date fiscal_period_end
                   ,to_char(null) je_month
                   ,to_char(null) je_year                   
        from gl_period_statuses
        where 1 =2 -- forced so we can get the rowtype declared without any record retrieved
        ;    
    --
    c_fiscal_details c_fiscal_info%rowtype;
    --
    cursor c_uniq_concur_batches is
        select concur_batch_id
        from  xxcus.xxcus_src_concur_sae_n
        where 1 =1
             and crt_btch_id =g_current_batch_id --HDS DW loads this field per import..so we can expect mor than one concur_batch_id per crt_btch_id
             and allocation_custom7 =g_CI_GP_emp_grp ----For Canada use allocation_custom7
             --and journal_payer_pmt_type_name =g_payer_pmt_type -- Ver 1.6 
             and journal_payee_pmt_type_name NOT LIKE 'RT%' --Ver 1.2
             --and journal_amt <>g_zero 
            and exists
             (
                  select '1'
                  from    fnd_lookup_values
                  where 1 =1
                        and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
                        and nvl(enabled_flag, 'N') ='Y'
                        and lookup_code =allocation_custom7
             )                                      
        group by concur_batch_id
        order by concur_batch_id asc  
        ;    
    --    
    cursor gp_je_eligible_records (p_concur_batch_id in number) is
    select 
         null fiscal_period_name --Populated run time when the routine is invoked.
        ,null fiscal_period_num --Populated run time when the routine is invoked.
        ,null fiscal_period_year --Populated run time when the routine is invoked.
        ,null fiscal_period_start --Populated run time when the routine is invoked.
        ,null fiscal_period_end --Populated run time when the routine is invoked.
        ,null date_1 --not in use currently
        ,null date_2 --not in use currently
        ,null date_3 --not in use currently
        ,null date_4 --not in use currently
        ,null date_5 --not in use currently
        ,allocation_custom8                                                  char_60_1  --product code
        ,to_char(concur_batch_date,'mm/dd/yyyy')     char_60_2  --je asofdte as of date [concur_batch_date]
        ,journal_acct_code                                                     char_60_3  -- je account code
        ,lpad(allocation_custom9, 3, '0')                            char_60_4 
        ,allocation_custom10                                                 char_60_5
        --,regexp_substr(journal_acct_code,'[0-9]+',1,1)                                                                 char_60_6  --account
        ,journal_acct_code                                                                                                                      char_60_6  --account        
        ,regexp_substr(journal_acct_code,'[0-9]+',1,2)                                                                 char_60_7  --sub account
        ,null                                                                                                                                                  char_60_8 --je expense description
        ,allocation_custom12                                                                                                                 char_60_9 --project code 
        ,case
           when rpt_entry_pmt_type_code in ('CASH', 'IBIP') then 'OOP'
           when rpt_entry_pmt_type_code in ('IBCP','CBCP') then 'CARD'
           else 'NONE'
         end                                                                                                                                                 char_60_10 --OOP or CARD
        ,(rpt_entry_posted_amt - rpt_entry_tot_tax_posted_amt)          num_1               --line_amount_less_system_tax
        ,rpt_entry_tot_tax_posted_amt          num_2               --line_tax_from_system
        ,journal_amt                          num_3               --Straight copy from concur extract
        ,rpt_entry_posted_amt      num_4               --line_item_total_incl_system_calculated_tax
        ,null num_5  --not in use currently
        ,null num_6  --not in use currently
        ,to_number(rpt_entry_custom20) num_7  --canadian gst_amt_from_tax_receipt
        ,to_number(rpt_entry_custom21) num_8  --canadian qst_amt_from_tax_receipt
        ,to_number(rpt_entry_custom22) num_9  --canadian hst_amt_from_tax_receipt
        ,to_number(rpt_entry_custom23) num_10 -- canadian pst_amt_from_tax_receipt
        ,rpt_entry_custom9 char_150_11 --po_number -- Ver 1.6
        ,null char_150_12 --not in use currently
        ,null char_150_13 --not in use currently
        ,null char_150_14 --not in use currently
        ,null char_150_15 --not in use currently
        ,null char_150_16 --not in use currently
        ,null char_150_17 --not in use currently
        ,null char_150_18 --not in use currently
        ,null char_150_19 --not in use currently
        ,null char_150_20 --not in use currently
        ,null char_1_21 --not in use currently
        ,null char_1_22 --not in use currently
        ,null char_1_23 --not in use currently
        ,null char_1_24 --not in use currently
        ,null char_1_25 --not in use currently
        ,substr(rpt_entry_loc_ctry_sub_code, 4, 2)      char_10_26 --tax province
        ,substr(rpt_entry_custom38, 3, 3)                       char_10_27 --tax code
        ,null char_10_28 --not in use currently
        ,null char_10_29 --not in use currently
        ,null char_10_30 --not in use currently
        ,rpt_entry_custom20                                     char_25_31  --only for canada...gst_amt_from_tax_receipt
        ,rpt_entry_custom21                                     char_25_32  --only for canada...qst_amt_from_tax_receipt
        ,rpt_entry_custom22                                     char_25_33  --only for canada...hst_amt_from_tax_receipt
        ,rpt_entry_custom23                                     char_25_34  --only for canada...pst_amt_from_tax_receipt
        ,null char_25_35 --not in use currently
        ,null char_25_36 --not in use currently
        ,null coa_code_1 --not in use currently
        ,null coa_code_2 --not in use currently
        ,null coa_code_3 --not in use currently
        ,null coa_code_4 --not in use currently
        ,null coa_code_5 --not in use currently
        ,null coa_code_6 --not in use currently
        ,null coa_code_7 --not in use currently
        ,null coa_desc_1 --not in use currently
        ,null coa_desc_2 --not in use currently
        ,null coa_desc_3 --not in use currently
        ,null coa_desc_4 --not in use currently
        ,null coa_desc_5 --not in use currently
        ,null coa_desc_6 --not in use currently
        ,null coa_desc_7 --not in use currently
        ,concur_batch_id --Straight copy from Concur.
        ,concur_batch_date --Straight copy from Concur.
        ,rpt_id --Straight copy from Concur.
        ,rpt_key --Straight copy from Concur.
        ,rpt_entry_key --Straight copy from Concur.
        ,journal_rpj_key --Straight copy from Concur.
        ,allocation_key --Straight copy from Concur.
        ,line_number --Straight copy from Concur.
        ,emp_id --Straight copy from Concur.
        ,emp_last_name --Straight copy from Concur.
        ,emp_first_name --Straight copy from Concur.
        ,emp_mi --Straight copy from Concur.
        ,emp_custom21 emp_group --Straight copy from Concur.
        ,rpt_custom1 emp_org_erp             --For Canadian org its always rpt_custom1
        ,rpt_custom2 emp_org_company  --For Canadian org its always rpt_custom2
        ,rpt_custom3 emp_org_branch      --For Canadian org its always rpt_custom3
        ,rpt_custom4 emp_org_dept           --For Canadian org its always rpt_custom4.
        ,rpt_custom6 emp_org_project      --For Canadian org its always rpt_custom6
        ,ledger_ledger_code ledger_code --Straight copy from Concur.
        ,emp_currency_alpha_code --Straight copy from Concur.
        ,rpt_submit_date --Straight copy from Concur.
        ,rpt_user_defined_date --Straight copy from Concur.
        ,rpt_pmt_processing_date --Straight copy from Concur.
        ,rpt_name --Straight copy from Concur.
        ,rpt_entry_trans_type --Straight copy from Concur.
        ,rpt_entry_expense_type_name --Straight copy from Concur.
        ,rpt_entry_vendor_name --Straight copy from Concur.
        ,rpt_entry_vendor_desc --Straight copy from Concur.
        ,cc_trans_merchant_name --Straight copy from Concur.
        ,cc_trans_desc --Straight copy from Concur.
        ,rpt_entry_location_city_name --Straight copy from Concur.
        ,cc_trans_merchant_city --Straight copy from Concur.
        ,rpt_entry_trans_date --Straight copy from Concur.
        ,cc_trans_trans_date --Straight copy from Concur.
        ,cc_trans_trans_id --Straight copy from Concur.
        ,cc_trans_merchant_state --Straight copy from Concur.  
        ,rpt_entry_from_location --Straight copy from Concur.
        ,rpt_entry_to_location --Straight copy from Concur.
        ,car_log_entry_business_dist --Straight copy from Concur.       
        ,rpt_entry_ctry_sub_code --Straight copy from Concur.   
        ,rpt_entry_pmt_type_code --Straight copy from Concur.
        ,rpt_entry_pmt_code_name --Straight copy from Concur.
        ,journal_payer_pmt_type_name --Straight copy from Concur.
        ,journal_payer_pmt_code_name --Straight copy from Concur.
        ,journal_payee_pmt_type_name --Straight copy from Concur.
        ,journal_payee_pmt_code_name --Straight copy from Concur.
        ,journal_acct_code --Straight copy from Concur.
        ,journal_debit_or_credit --Straight copy from Concur.
        ,journal_amt --Straight copy from Concur.
        ,allocation_pct --Straight copy from Concur.
        ,allocation_custom7 allocation_erp            --For "US" use allocation_custom1. For Canada use allocation_custom7. For WW there is no canadian allocation.
        ,allocation_custom8 allocation_company --For "US" use allocation_custom2. For Canada use allocation_custom8. For WW there is no canadian allocation.
        ,allocation_custom9 allocation_branch     --For "US" use allocation_custom3. For Canada use allocation_custom9. For WW there is no canadian allocation.
        ,allocation_custom10 allocation_dept          --For "US" use allocation_custom4. For Canada use allocation_custom10.For WW there is no canadian allocation.
        ,allocation_custom12 allocation_project     --For "US" use allocation_custom11. For Canada use allocation_custom12.For WW there is no canadian allocation.
        ,amt_net_tax_tot_adj --Straight copy from Concur.
        ,amt_net_tax_tot_rclm --Straight copy from Concur.
        ,rpt_vw_tot_personal_amt --Straight copy from Concur.
        ,rpt_vw_tot_claimed_amt --Straight copy from Concur.
        ,rpt_vw_tot_due_emp --Straight copy from Concur.
        ,rpt_vw_tot_due_cmpny_card --Straight copy from Concur.
        ,rpt_vw_actual_tot_due_emp --Straight copy from Concur.
        ,rpt_vw_actl_tot_due_cmp_crd --Straight copy from Concur.
        ,rpt_vw_tot_posted_amt --Straight copy from Concur.
        ,rpt_vw_tot_approved_amt --Straight copy from Concur.
        ,rpt_entry_is_personal --Straight copy from Concur.
        ,rpt_entry_trans_amt --Straight copy from Concur.
        ,rpt_entry_posted_amt --Straight copy from Concur.
        ,rpt_entry_approved_amt --Straight copy from Concur.
        ,rpt_entry_tot_tax_posted_amt --Straight copy from Concur.
        ,rpt_entry_vw_net_tax_amt --Straight copy from Concur.
        ,rpt_entry_tot_rclm_adj_amt --Straight copy from Concur.
        ,rpt_ent_vw_net_rclm_adj_amt --Straight copy from Concur.
        ,rpt_entry_vw_net_adj_tax_amt --Straight copy from Concur.
        ,g_current_batch_id crt_btch_id 
    from  xxcus.xxcus_src_concur_sae_n
    where 1 =1       
         and crt_btch_id =g_current_batch_id --HDS DW loads this field per import..so we can expect mor than one concur_batch_id per crt_btch_id
         and allocation_custom7 =g_CI_GP_emp_grp  --For Canada use allocation_custom7
         and concur_batch_id =p_concur_batch_id 
         and journal_payer_pmt_type_name =g_payer_pmt_type -- Ver 1.6
         and journal_payee_pmt_type_name NOT LIKE 'RT%' --Ver 1.2
         and sign(rpt_entry_posted_amt) = (1) --Ver 1.6     
         --and journal_amt <>g_zero   
        -- Begin ver 1.6
    union
    select 
         null fiscal_period_name --Populated run time when the routine is invoked.
        ,null fiscal_period_num --Populated run time when the routine is invoked.
        ,null fiscal_period_year --Populated run time when the routine is invoked.
        ,null fiscal_period_start --Populated run time when the routine is invoked.
        ,null fiscal_period_end --Populated run time when the routine is invoked.
        ,null date_1 --not in use currently
        ,null date_2 --not in use currently
        ,null date_3 --not in use currently
        ,null date_4 --not in use currently
        ,null date_5 --not in use currently
        ,allocation_custom8                                                  char_60_1  --product code
        ,to_char(concur_batch_date,'mm/dd/yyyy')     char_60_2  --je asofdte as of date [concur_batch_date]
        ,journal_acct_code                                                     char_60_3  -- je account code
        ,lpad(allocation_custom9, 3, '0')                            char_60_4 
        ,allocation_custom10                                                 char_60_5
        --,regexp_substr(journal_acct_code,'[0-9]+',1,1)                                                                 char_60_6  --account
        ,journal_acct_code                                                                                                                      char_60_6  --account        
        ,regexp_substr(journal_acct_code,'[0-9]+',1,2)                                                                 char_60_7  --sub account
        ,null                                                                                                                                                  char_60_8 --je expense description
        ,allocation_custom12                                                                                                                 char_60_9 --project code 
        ,case
           when rpt_entry_pmt_type_code in ('CASH', 'IBIP') then 'OOP'
           when rpt_entry_pmt_type_code in ('IBCP','CBCP') then 'CARD'
           else 'NONE'
         end                                                                                                                                                 char_60_10 --OOP or CARD
        ,(rpt_entry_posted_amt - to_number(rpt_entry_custom39))          num_1               --line_amount_less_system_tax
        ,to_number(rpt_entry_custom39)          num_2               --line_tax_from_system
        ,journal_amt                          num_3               --Straight copy from concur extract
        ,rpt_entry_posted_amt      num_4               --line_item_total_incl_system_calculated_tax
        ,null num_5  --not in use currently
        ,null num_6  --not in use currently
        ,to_number(rpt_entry_custom20) num_7  --canadian gst_amt_from_tax_receipt
        ,to_number(rpt_entry_custom21) num_8  --canadian qst_amt_from_tax_receipt
        ,to_number(rpt_entry_custom22) num_9  --canadian hst_amt_from_tax_receipt
        ,to_number(rpt_entry_custom23) num_10 -- canadian pst_amt_from_tax_receipt
        ,rpt_entry_custom9 char_150_11 --po_number -- Ver 1.6
        ,null char_150_12 --not in use currently
        ,null char_150_13 --not in use currently
        ,null char_150_14 --not in use currently
        ,null char_150_15 --not in use currently
        ,null char_150_16 --not in use currently
        ,null char_150_17 --not in use currently
        ,null char_150_18 --not in use currently
        ,null char_150_19 --not in use currently
        ,null char_150_20 --not in use currently
        ,null char_1_21 --not in use currently
        ,null char_1_22 --not in use currently
        ,null char_1_23 --not in use currently
        ,null char_1_24 --not in use currently
        ,null char_1_25 --not in use currently
        ,substr(rpt_entry_loc_ctry_sub_code, 4, 2)      char_10_26 --tax province
        ,substr(rpt_entry_custom38, 3, 3)                       char_10_27 --tax code
        ,null char_10_28 --not in use currently
        ,null char_10_29 --not in use currently
        ,null char_10_30 --not in use currently
        ,rpt_entry_custom20                                     char_25_31  --only for canada...gst_amt_from_tax_receipt
        ,rpt_entry_custom21                                     char_25_32  --only for canada...qst_amt_from_tax_receipt
        ,rpt_entry_custom22                                     char_25_33  --only for canada...hst_amt_from_tax_receipt
        ,rpt_entry_custom23                                     char_25_34  --only for canada...pst_amt_from_tax_receipt
        ,null char_25_35 --not in use currently
        ,null char_25_36 --not in use currently
        ,null coa_code_1 --not in use currently
        ,null coa_code_2 --not in use currently
        ,null coa_code_3 --not in use currently
        ,null coa_code_4 --not in use currently
        ,null coa_code_5 --not in use currently
        ,null coa_code_6 --not in use currently
        ,null coa_code_7 --not in use currently
        ,null coa_desc_1 --not in use currently
        ,null coa_desc_2 --not in use currently
        ,null coa_desc_3 --not in use currently
        ,null coa_desc_4 --not in use currently
        ,null coa_desc_5 --not in use currently
        ,null coa_desc_6 --not in use currently
        ,null coa_desc_7 --not in use currently
        ,concur_batch_id --Straight copy from Concur.
        ,concur_batch_date --Straight copy from Concur.
        ,rpt_id --Straight copy from Concur.
        ,rpt_key --Straight copy from Concur.
        ,rpt_entry_key --Straight copy from Concur.
        ,journal_rpj_key --Straight copy from Concur.
        ,allocation_key --Straight copy from Concur.
        ,line_number --Straight copy from Concur.
        ,emp_id --Straight copy from Concur.
        ,emp_last_name --Straight copy from Concur.
        ,emp_first_name --Straight copy from Concur.
        ,emp_mi --Straight copy from Concur.
        ,emp_custom21 emp_group --Straight copy from Concur.
        ,rpt_custom1 emp_org_erp             --For Canadian org its always rpt_custom1
        ,rpt_custom2 emp_org_company  --For Canadian org its always rpt_custom2
        ,rpt_custom3 emp_org_branch      --For Canadian org its always rpt_custom3
        ,rpt_custom4 emp_org_dept           --For Canadian org its always rpt_custom4.
        ,rpt_custom6 emp_org_project      --For Canadian org its always rpt_custom6
        ,ledger_ledger_code ledger_code --Straight copy from Concur.
        ,emp_currency_alpha_code --Straight copy from Concur.
        ,rpt_submit_date --Straight copy from Concur.
        ,rpt_user_defined_date --Straight copy from Concur.
        ,rpt_pmt_processing_date --Straight copy from Concur.
        ,rpt_name --Straight copy from Concur.
        ,rpt_entry_trans_type --Straight copy from Concur.
        ,rpt_entry_expense_type_name --Straight copy from Concur.
        ,rpt_entry_vendor_name --Straight copy from Concur.
        ,rpt_entry_vendor_desc --Straight copy from Concur.
        ,cc_trans_merchant_name --Straight copy from Concur.
        ,cc_trans_desc --Straight copy from Concur.
        ,rpt_entry_location_city_name --Straight copy from Concur.
        ,cc_trans_merchant_city --Straight copy from Concur.
        ,rpt_entry_trans_date --Straight copy from Concur.
        ,cc_trans_trans_date --Straight copy from Concur.
        ,cc_trans_trans_id --Straight copy from Concur.
        ,cc_trans_merchant_state --Straight copy from Concur.  
        ,rpt_entry_from_location --Straight copy from Concur.
        ,rpt_entry_to_location --Straight copy from Concur.
        ,car_log_entry_business_dist --Straight copy from Concur.       
        ,rpt_entry_ctry_sub_code --Straight copy from Concur.   
        ,rpt_entry_pmt_type_code --Straight copy from Concur.
        ,rpt_entry_pmt_code_name --Straight copy from Concur.
        ,journal_payer_pmt_type_name --Straight copy from Concur.
        ,journal_payer_pmt_code_name --Straight copy from Concur.
        ,journal_payee_pmt_type_name --Straight copy from Concur.
        ,journal_payee_pmt_code_name --Straight copy from Concur.
        ,journal_acct_code --Straight copy from Concur.
        ,journal_debit_or_credit --Straight copy from Concur.
        ,journal_amt --Straight copy from Concur.
        ,allocation_pct --Straight copy from Concur.
        ,allocation_custom7 allocation_erp            --For "US" use allocation_custom1. For Canada use allocation_custom7. For WW there is no canadian allocation.
        ,allocation_custom8 allocation_company --For "US" use allocation_custom2. For Canada use allocation_custom8. For WW there is no canadian allocation.
        ,allocation_custom9 allocation_branch     --For "US" use allocation_custom3. For Canada use allocation_custom9. For WW there is no canadian allocation.
        ,allocation_custom10 allocation_dept          --For "US" use allocation_custom4. For Canada use allocation_custom10.For WW there is no canadian allocation.
        ,allocation_custom12 allocation_project     --For "US" use allocation_custom11. For Canada use allocation_custom12.For WW there is no canadian allocation.
        ,amt_net_tax_tot_adj --Straight copy from Concur.
        ,amt_net_tax_tot_rclm --Straight copy from Concur.
        ,rpt_vw_tot_personal_amt --Straight copy from Concur.
        ,rpt_vw_tot_claimed_amt --Straight copy from Concur.
        ,rpt_vw_tot_due_emp --Straight copy from Concur.
        ,rpt_vw_tot_due_cmpny_card --Straight copy from Concur.
        ,rpt_vw_actual_tot_due_emp --Straight copy from Concur.
        ,rpt_vw_actl_tot_due_cmp_crd --Straight copy from Concur.
        ,rpt_vw_tot_posted_amt --Straight copy from Concur.
        ,rpt_vw_tot_approved_amt --Straight copy from Concur.
        ,rpt_entry_is_personal --Straight copy from Concur.
        ,rpt_entry_trans_amt --Straight copy from Concur.
        ,rpt_entry_posted_amt --Straight copy from Concur.
        ,rpt_entry_approved_amt --Straight copy from Concur.
        ,rpt_entry_tot_tax_posted_amt --Straight copy from Concur.
        ,rpt_entry_vw_net_tax_amt --Straight copy from Concur.
        ,rpt_entry_tot_rclm_adj_amt --Straight copy from Concur.
        ,rpt_ent_vw_net_rclm_adj_amt --Straight copy from Concur.
        ,rpt_entry_vw_net_adj_tax_amt --Straight copy from Concur.
        ,g_current_batch_id crt_btch_id 
    from  xxcus.xxcus_src_concur_sae_n
    where 1 =1       
         and crt_btch_id =g_current_batch_id --HDS DW loads this field per import..so we can expect mor than one concur_batch_id per crt_btch_id
         and allocation_custom7 =g_CI_GP_emp_grp  --For Canada use allocation_custom7
         and concur_batch_id =p_concur_batch_id 
         and journal_payer_pmt_type_name !=g_payer_pmt_type -- Ver 1.6
         --and journal_payee_pmt_type_name =g_payer_pmt_type -- Ver 1.6         
         and (journal_payee_pmt_type_name NOT LIKE 'CA%' or journal_payee_pmt_type_name NOT LIKE 'RT%') --Ver 1.6
         and sign(rpt_entry_posted_amt) = (-1) --Ver 1.6           
        -- End Ver 1.6            
    ;
    --
    type gp_je_eligible_rec_type is table of xxcus.xxcus_concur_eligible_je%rowtype index by binary_integer;
    gp_je_eligible_rec gp_je_eligible_rec_type;
    --
  begin
     -- ****
     for rec in c_uniq_concur_batches loop
                 --
                        delete xxcus.xxcus_concur_eligible_je 
                        where 1 =1
                             and crt_btch_id =g_current_batch_id
                             and allocation_erp =g_CI_GP_emp_grp                             
                             and concur_batch_id =rec.concur_batch_id
                             ;-- journals marked for CI Great Plains only          
                 --
                  print_log('@ '||l_sub_routine||' Flush any previous loaded eligible JE records for employee group '||g_CI_GP_emp_grp||', crt_btch_id : '||g_current_batch_id||', concur_batch_id :'||rec.concur_batch_id||', total deleted :'||sql%rowcount);
                 --
                open gp_je_eligible_records (p_concur_batch_id =>rec.concur_batch_id);
                loop 
                 --
                 fetch gp_je_eligible_records bulk collect into gp_je_eligible_rec limit xxcus_concur_pkg.g_limit;
                 --
                 exit when gp_je_eligible_rec.count =0;
                     --
                     if gp_je_eligible_rec.count >0 then 
                      --
                        for idx in 1 .. gp_je_eligible_rec.count loop 
                          --
                          -- get fiscal period details
                          --
                          if idx =1 then 
                           -- since it's first record get the fiscal period details
                                begin --begin get fiscal info
                                 --
                                 l_prev_batch_date :=gp_je_eligible_rec(idx).concur_batch_date; 
                                 --
                                        select /*+ RESULT_CACHE */
                                                    period_name                                            fiscal_period_name
                                                   ,effective_period_num                           fiscal_period_num
                                                   ,period_year                                              fiscal_period_year
                                                   ,start_date                                                  fiscal_period_start
                                                   ,end_date                                                   fiscal_period_end
                                                   ,lpad(to_char((period_num)), 2,'0')   je_month
                                                   ,to_char(period_year)                            je_year 
                                    into   c_fiscal_details
                                    from gl_period_statuses
                                    where 1 =1
                                        and application_id =101
                                        and ledger_id =g_CAD_ledger_id
                                        and gp_je_eligible_rec(idx).concur_batch_date between start_date and end_date
                                        ;                 
                                 --
                                exception
                                 when no_data_found then
                                  c_fiscal_details :=null;
                                 when others then
                                  c_fiscal_details :=null;
                                end; --end get fiscal info
                          else
                           --
                               if l_prev_batch_date != gp_je_eligible_rec(idx).concur_batch_date then
                                    begin --begin get fiscal info
                                      --
                                        select /*+ RESULT_CACHE */
                                                    period_name                                            fiscal_period_name
                                                   ,effective_period_num                           fiscal_period_num
                                                   ,period_year                                              fiscal_period_year
                                                   ,start_date                                                  fiscal_period_start
                                                   ,end_date                                                   fiscal_period_end
                                                   ,lpad(to_char((period_num)), 2,'0')   je_month
                                                   ,to_char(period_year)                            je_year                                       
                                        into   c_fiscal_details
                                        from gl_period_statuses
                                        where 1 =1
                                            and application_id =101
                                            and ledger_id =g_CAD_ledger_id
                                            and gp_je_eligible_rec(idx).concur_batch_date between start_date and end_date
                                            ;                 
                                     --
                                    exception
                                     when no_data_found then
                                      c_fiscal_details :=null;
                                     when others then
                                      c_fiscal_details :=null;
                                    end; --end get fiscal info               
                               else
                                Null; --What this means is DO NOT reset the rowtype variable c_fiscal_details
                               end if;
                                 --
                                 l_prev_batch_date :=gp_je_eligible_rec(idx).concur_batch_date; 
                                 --
                          end if;
                          --
                          -- Assign fiscal info related fields
                          --               
                            gp_je_eligible_rec(idx).fiscal_period_name  :=c_fiscal_details.fiscal_period_name;
                            gp_je_eligible_rec(idx).fiscal_period_num    :=c_fiscal_details.fiscal_period_num;
                            gp_je_eligible_rec(idx).fiscal_period_year    :=c_fiscal_details.fiscal_period_year;
                            gp_je_eligible_rec(idx).fiscal_period_start   :=c_fiscal_details.fiscal_period_start;
                            gp_je_eligible_rec(idx).fiscal_period_end     :=c_fiscal_details.fiscal_period_end;
                          --
                        end loop;
                          --
                          -- bulk insert into xxcus.xxcus_concur_ww_eligible_je table
                          --
                          begin
                           forall idx in 1 .. gp_je_eligible_rec.count 
                            insert into xxcus.xxcus_concur_eligible_je values gp_je_eligible_rec(idx);
                          exception
                           when others then
                            print_log('Error in bulk insert of xxcus.xxcus_concur_ww_eligible_je, message =>'||sqlerrm);
                            raise program_error;
                          end;
                      --
                     end if;
                     --
                end loop;
                close gp_je_eligible_records;
                --      
     end loop;     
     -- ****
  exception
   when program_error then
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end CI_GP_EXTRACT_ELIGIBLE_GL_JE;        
  --  
  -- End CI Great Plains extract gl eligible je
  -- Begin CI Great Plains extract je files and generate Interco gl journal interface records 
  procedure CI_GP_OB_GL_JE is
   --
    l_sub_routine varchar2(30) :='ci_gp_ob_gl_je';
    l_line_count number :=0;
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;      
    --
    l_Interco_Acct_Date Date :=Trunc(Sysdate);--WW accounting date used for Intercompany journal entries
    l_Interco_Location              xxcus.xxcus_concur_gl_iface.segment2%type :='Z0436';
    l_Interco_Cost_Ctr              xxcus.xxcus_concur_gl_iface.segment3%type :='0000';
    l_Interco_OOP_Liability     xxcus.xxcus_concur_gl_iface.segment4%type :='211121';
    l_Interco_CARD_Liability   xxcus.xxcus_concur_gl_iface.segment4%type :='211126';    
    l_Interco_OOP_Clearing    xxcus.xxcus_concur_gl_iface.segment4%type :='910121';    
    l_Interco_Project                 xxcus.xxcus_concur_gl_iface.segment5%type :='00000';
    l_Brafasco_IC_GSC_Acct   xxcus.xxcus_concur_gl_iface.segment4%type :='000-21001';
    --
    l_Brafasco_IC_GSC_Desc  xxcus.xxcus_concur_gl_iface.reference10%type :='Brafasco IC with GSC';
    l_Interco_Clearing_Desc  xxcus.xxcus_concur_gl_iface.reference10%type :='GSC IC with Brafasco';
    --
    l_utl_file_id   UTL_FILE.FILE_TYPE;
    l_line_buffer VARCHAR2(30000):= Null;
    l_file VARCHAR2(150) :=Null;
    l_line_idx Number :=0;
    -- 
    --
    cursor c_uniq_concur_batches is
        select concur_batch_id, to_char(concur_batch_date, 'mm/dd/yyyy') concur_batch_date                                                                  
        from xxcus.xxcus_concur_eligible_je
        where 1 =1
            and crt_btch_id =g_current_batch_id
            and allocation_erp =g_CI_GP_emp_grp
             --and journal_payer_pmt_type_name =g_payer_pmt_type --Ver 1.1 -- Ver 1.6
             and journal_payee_pmt_type_name NOT LIKE 'RT%' --Ver 1.4          
        group by concur_batch_id ,to_char(concur_batch_date, 'mm/dd/yyyy') 
        order by concur_batch_id asc  
        ;  
    --
    cursor c_CI_GP_summary_je (p_concur_batch_id in number) is
        select replace(upper(fiscal_period_name), '-','')||' CONCUR BATCH '||p_concur_batch_id BATCHID
                  ,to_char(concur_batch_date, 'MM/DD/YYYY') ASOFDTE
                  ,char_60_10 payment_type
                  ,journal_acct_code||'-'||char_60_4 journal_acct_code
                  ,sum(num_3) total_amount
        from xxcus.xxcus_concur_eligible_je
        where 1 =1
            and crt_btch_id =g_current_batch_id
            and allocation_erp =g_CI_GP_emp_grp
            and concur_batch_id =p_concur_batch_id --Cursor parameter  
            --and journal_payer_pmt_type_name =g_payer_pmt_type --Ver 1.1 -- Ver 1.6
            and journal_payee_pmt_type_name NOT LIKE 'RT%' --Ver 1.4                  
        group by
                    replace(upper(fiscal_period_name), '-','')||' CONCUR BATCH '||p_concur_batch_id
                  ,to_char(concur_batch_date, 'MM/DD/YYYY') 
                  ,journal_acct_code||'-'||char_60_4
                  ,char_60_10 --payment_type
        ;
    --
    type c_summary_je_type is table of c_CI_GP_summary_je%rowtype index by binary_integer;
    c_summary_rec  c_summary_je_type; 
    --
    cursor c_CI_GP_IC (p_concur_batch_id in number) is --Used only for Brafasco IC with GSC
        select replace(upper(fiscal_period_name), '-','')||' CONCUR BATCH '||p_concur_batch_id BATCHID
                  ,to_char(concur_batch_date, 'MM/DD/YYYY') ASOFDTE
                  ,char_60_10 payment_type
                  ,sum(num_3) brafasco_ic_amt
        from xxcus.xxcus_concur_eligible_je
        where 1 =1
            and crt_btch_id =g_current_batch_id
            and allocation_erp =g_CI_GP_emp_grp
            and concur_batch_id =p_concur_batch_id --Cursor parameter
            --and journal_payer_pmt_type_name =g_payer_pmt_type --Ver 1.1 -- Ver 1.6
            and journal_payee_pmt_type_name NOT LIKE 'RT%' --Ver 1.4                    
        group by
                     replace(upper(fiscal_period_name), '-','')||' CONCUR BATCH '||p_concur_batch_id --BATCHID
                  ,to_char(concur_batch_date, 'MM/DD/YYYY') --concur_batch_date
                  ,char_60_10 --payment_type
        ;
    --
    type c_CI_GP_IC_rec_type is table of c_CI_GP_IC%rowtype index by binary_integer;     
    c_CI_GP_IC_rec c_CI_GP_IC_rec_type;
    -- 
    cursor c_CI_GP_je_details (p_concur_batch_id in number) is
        select 
                 concur_batch_id
                ,to_char(concur_batch_date,'mm/dd/yyyy') concur_batch_date
                ,rpt_id
                ,rpt_key
                ,rpt_entry_key
                ,journal_rpj_key
                ,allocation_key
                ,line_number
                ,emp_id
                ,emp_last_name
                ,emp_first_name
                ,emp_mi
                ,emp_group
                ,emp_org_erp
                ,emp_org_company
                ,emp_org_branch
                ,emp_org_dept
                ,emp_org_project
                ,ledger_code
                ,emp_currency_alpha_code
                ,to_char(rpt_submit_date,'mm/dd/yyyy') rpt_submit_date                
                ,to_char(rpt_user_defined_date,'mm/dd/yyyy') rpt_user_defined_date                
                ,to_char(rpt_pmt_processing_date,'mm/dd/yyyy') rpt_pmt_processing_date
                ,rpt_name
                ,rpt_entry_trans_type
                ,rpt_entry_expense_type_name
                ,rpt_entry_vendor_name
                ,rpt_entry_vendor_desc
                ,cc_trans_merchant_name                      merchant_name
                ,cc_trans_desc                                             merchant_description
                ,rpt_entry_location_city_name              rpt_entry_location_city
                ,cc_trans_merchant_city                          transaction_city
                ,to_char(rpt_entry_trans_date,'mm/dd/yyyy') rpt_entry_trans_date                
                ,to_char(cc_trans_trans_date,'mm/dd/yyyy') cc_trans_trans_date
                ,cc_trans_trans_id                                     cc_trans_trans_id
                ,cc_trans_merchant_state                      transaction_state
                ,rpt_entry_from_location
                ,rpt_entry_to_location
                ,car_log_entry_business_dist
                ,rpt_entry_ctry_sub_code
                ,rpt_entry_pmt_type_code
                ,rpt_entry_pmt_code_name
                ,journal_payer_pmt_type_name
                ,journal_payer_pmt_code_name
                ,journal_payee_pmt_type_name
                ,journal_payee_pmt_code_name
                ,journal_acct_code
                ,journal_debit_or_credit
                ,journal_amt
                ,allocation_pct
                ,allocation_erp
                ,allocation_company
                ,allocation_branch
                ,allocation_dept
                ,allocation_project
                ,char_60_10   oop_or_card
                ,char_25_31    gst_amt_from_tax_receipt
                ,char_25_32    qst_amt_from_tax_receipt
                ,char_25_33    hst_amt_from_tax_receipt
                ,char_25_34    pst_amt_from_tax_receipt                
                ,amt_net_tax_tot_adj
                ,amt_net_tax_tot_rclm
                ,rpt_vw_tot_personal_amt
                ,rpt_vw_tot_claimed_amt
                ,rpt_vw_tot_due_emp
                ,rpt_vw_tot_due_cmpny_card
                ,rpt_vw_actual_tot_due_emp
                ,rpt_vw_actl_tot_due_cmp_crd
                ,rpt_vw_tot_posted_amt
                ,rpt_vw_tot_approved_amt
                ,rpt_entry_is_personal
                ,rpt_entry_trans_amt
                ,rpt_entry_posted_amt
                ,rpt_entry_approved_amt
                ,rpt_entry_tot_tax_posted_amt
                ,rpt_entry_vw_net_tax_amt
                ,rpt_entry_tot_rclm_adj_amt
                ,rpt_ent_vw_net_rclm_adj_amt
                ,rpt_entry_vw_net_adj_tax_amt
                ,crt_btch_id
                ,char_150_11 po_number -- Ver 1.6                                          
        from xxcus.xxcus_concur_eligible_je
        where 1 =1
            and crt_btch_id =g_current_batch_id
            and allocation_erp =g_CI_GP_emp_grp
            and concur_batch_id =p_concur_batch_id --Cursor parameter    
            and journal_payee_pmt_type_name NOT LIKE 'RT%' --Ver 1.4               
        order by rpt_key asc, line_number asc
                   ;
    --  
    type c_CI_GP_je_details_type is table of c_CI_GP_je_details%rowtype index by binary_integer;
    c_CI_GP_je_details_rec  c_CI_GP_je_details_type; 
    -- 
    cursor c_CI_GP_interco_je (p_concur_batch_id in number) is --Used only for GSC IC with Brafasco
        select
                   to_char(concur_batch_date, 'MM/DD/YYYY') concur_batch_date
                  ,char_60_10 payment_type
                  ,g_Interco_CA_Product product
                  ,l_Interco_Location  location
                  ,l_Interco_Cost_Ctr cost_ctr
                  ,case
                    when char_60_10 ='OOP' then l_Interco_OOP_Liability
                    when char_60_10 ='CARD' then l_Interco_CARD_Liability
                    else 'NONE'
                   end liability_account
                  ,l_Interco_Project project_code
                  ,g_future_use_1  fuse1
                  ,g_future_use_2  fuse2                   
                  ,l_Interco_OOP_Clearing interco_clearing 
                  ,case 
                     when char_60_10 ='OOP'  then 'Interco OOP'||', Concur Batch '||p_concur_batch_id 
                     when char_60_10 ='CARD'  then 'Interco Card'||', Concur Batch '||p_concur_batch_id        
                     else 'Other than OOP/Card found'             
                   end reference4
                  ,case 
                     when char_60_10 ='OOP'  then 'Employee Group :'||g_CI_GP_emp_grp||', Concur Batch '||p_concur_batch_id ||', Interco for OOP'
                     when char_60_10 ='CARD'  then 'Employee Group :'||g_CI_GP_emp_grp||', Concur Batch '||p_concur_batch_id ||', Interco for CARD'                             
                     else 'Journal for other than OOP/Card'             
                   end reference5                   
                  ,'Concur Batch '||p_concur_batch_id||', '||'GSC IC with Brafasco' reference10                  
                  ,sum(num_3) debit
                  ,sum(num_3) credit 
        from xxcus.xxcus_concur_eligible_je
        where 1 =1
            and crt_btch_id =g_current_batch_id
            and allocation_erp =g_CI_GP_emp_grp
            and concur_batch_id =p_concur_batch_id --Cursor parameter  
            --and journal_payer_pmt_type_name =g_payer_pmt_type --Ver 1.1 -- Ver 1.6
            and journal_payee_pmt_type_name NOT LIKE 'RT%' --Ver 1.2              
        group by
                   to_char(concur_batch_date, 'MM/DD/YYYY') --concur_batch_date
                  ,char_60_10 --payment_type
                  ,g_Interco_CA_Product 
                  ,l_Interco_Location  
                  ,l_Interco_Cost_Ctr 
                  ,case
                    when char_60_10 ='OOP' then l_Interco_OOP_Liability
                    when char_60_10 ='CARD' then l_Interco_CARD_Liability
                    else 'NONE'
                   end 
                  ,l_Interco_Project 
                  ,g_future_use_1
                  ,g_future_use_2
                  ,l_Interco_OOP_Clearing
                  ,case 
                     when char_60_10 ='OOP'  then 'Interco OOP'||', Concur Batch '||p_concur_batch_id 
                     when char_60_10 ='CARD'  then 'Interco Card'||', Concur Batch '||p_concur_batch_id        
                     else 'Other than OOP/Card found'             
                   end 
                  ,case 
                     when char_60_10 ='OOP'  then 'Employee Group :'||g_CI_GP_emp_grp||', Concur Batch '||p_concur_batch_id ||', Interco for OOP'
                     when char_60_10 ='CARD'  then 'Employee Group :'||g_CI_GP_emp_grp||', Concur Batch '||p_concur_batch_id ||', Interco for CARD'                             
                     else 'Journal for other than OOP/Card'             
                   end                    
                  ,'Concur Batch '||p_concur_batch_id||', '||'GSC IC with Brafasco'                    
        ;
    --
    type c_CI_GP_interco_je_type is table of c_CI_GP_interco_je%rowtype index by binary_integer;
    c_interco_rec  c_CI_GP_interco_je_type; 
    --   
    -- Begin Ver 1.3
    --
        cursor c_ci_gp_summ_rpt_set (p_concur_batch_id in number) is
        select to_char(rpt_submit_date, 'MM/DD/YYYY')                                         date_of_entry
                   ,to_char(rpt_entry_trans_date, 'MM/DD/YYYY')                               date_of_expense
                   ,rpt_name                                                                                                      report_name
                   ,rpt_entry_expense_type_name                                                             expense_type
                   ,emp_last_name||', '||emp_first_name                                             emp_full_name
                   ,rpt_entry_vendor_desc                                                                           vendor_name
                   ,rpt_entry_location_city_name                                                              city
                   ,substr(rpt_entry_loc_ctry_sub_code, 4, 2)                                       tax_province
                   ,substr(rpt_entry_custom38, 3, 3)                                                        tax_code
                   ,journal_acct_code||'-'||lpad(allocation_custom9, 3, '0')            journal_acct_code
                   ,allocation_custom7                                                                                  allocation_erp
                   ,lpad(allocation_custom9, 3, '0')                                                            allocation_branch
                   ,rpt_custom3                                                                                                emp_org_branch
                   ,journal_amt                                                                                                 line_total
                   ,rpt_entry_custom20                                                                                 gst_amt
                   ,rpt_entry_custom21                                                                                 qst_amt
                   ,rpt_entry_custom22                                                                                 hst_amt
                   ,rpt_entry_custom23                                                                                 pst_amt
                   ,nvl ( (journal_amt - (to_number(rpt_entry_custom20) + to_number(rpt_entry_custom21) + to_number(rpt_entry_custom22) + to_number(rpt_entry_custom23))) ,0) subtotal
                   ,tax_alloc_tot_vw_adj_amt
                   ,rpt_entry_tax_trans_amt
                   ,to_number(rpt_entry_custom39) reclaim_amount_in_cad
        from  xxcus.xxcus_src_concur_sae_n
        where 1 =1       
             and crt_btch_id =g_current_batch_id --HDS DW loads this field per import..so we can expect mor than one concur_batch_id per crt_btch_id
             and allocation_custom7 =g_ci_gp_emp_grp  --For Canada use allocation_custom7
             and concur_batch_id =p_concur_batch_id 
             and journal_payer_pmt_type_name =g_payer_pmt_type    
             and journal_payee_pmt_type_name NOT LIKE 'RT%' --Ver 1.4
             -- Begin Ver 1.7                   
             and sign(rpt_entry_posted_amt) = (1) 
        union all
        select to_char(rpt_submit_date, 'MM/DD/YYYY')                                         date_of_entry
                   ,to_char(rpt_entry_trans_date, 'MM/DD/YYYY')                               date_of_expense
                   ,rpt_name                                                                                                      report_name
                   ,rpt_entry_expense_type_name                                                             expense_type
                   ,emp_last_name||', '||emp_first_name                                             emp_full_name
                   ,rpt_entry_vendor_desc                                                                           vendor_name
                   ,rpt_entry_location_city_name                                                              city
                   ,substr(rpt_entry_loc_ctry_sub_code, 4, 2)                                       tax_province
                   ,substr(rpt_entry_custom38, 3, 3)                                                        tax_code
                   ,journal_acct_code||'-'||lpad(allocation_custom9, 3, '0')            journal_acct_code
                   ,allocation_custom7                                                                                  allocation_erp
                   ,lpad(allocation_custom9, 3, '0')                                                            allocation_branch
                   ,rpt_custom3                                                                                                emp_org_branch
                   ,journal_amt                                                                                                 line_total
                   ,rpt_entry_custom20                                                                                 gst_amt
                   ,rpt_entry_custom21                                                                                 qst_amt
                   ,rpt_entry_custom22                                                                                 hst_amt
                   ,rpt_entry_custom23                                                                                 pst_amt
                   ,nvl ( (journal_amt - (to_number(rpt_entry_custom20) + to_number(rpt_entry_custom21) + to_number(rpt_entry_custom22) + to_number(rpt_entry_custom23))) ,0) subtotal
                   ,tax_alloc_tot_vw_adj_amt
                   ,rpt_entry_tax_trans_amt
                   ,to_number(rpt_entry_custom39) reclaim_amount_in_cad
        from  xxcus.xxcus_src_concur_sae_n
        where 1 =1       
             and crt_btch_id =g_current_batch_id
             and allocation_custom7 =g_ci_gp_emp_grp
             and concur_batch_id =p_concur_batch_id 
             and journal_payer_pmt_type_name !=g_payer_pmt_type    
             and (journal_payee_pmt_type_name NOT LIKE 'CA%' or journal_payee_pmt_type_name NOT LIKE 'RT%')
             and sign(rpt_entry_posted_amt) = (-1)         
             -- End Ver 1.7             
        order by 5 --rpt_key asc, line_number asc -- Ver 1.7
                   ;    
        type c_ci_gp_summ_rpt_type is table of c_ci_gp_summ_rpt_set%rowtype index by binary_integer;
        c_ci_gp_summ_rpt          c_ci_gp_summ_rpt_type;
    --        
    -- End Ver 1.3  
    -- 
  begin
     --
     g_outbound_loc :=GET_OUTBOUND_FILE_LOCATION ( v_operation =>'DBA_DIRECTORY', v_emp_group  =>g_CI_CAD_GP);
     --
     print_log ('Current working DBA directory is '||g_outbound_loc);
     --     
       g_concur_gl_intf_rec.delete;
       g_total :=0;
    --  
     for rec in c_uniq_concur_batches loop
       -- ### @ begin rec in c_uniq_concur_batches loop
                 --
                 if g_database =g_prd_database then   
                   g_file_prefix :=g_CI_CAD_GP; --Null; --Ver 1.1
                 else
                   g_file_prefix :=g_database||'_'||g_CI_CAD_GP;
                 end if;
                 --          
                 l_file :=g_file_prefix||'_BATCH_'||rec.concur_batch_id||'_JE_ACTUALS_'||TO_CHAR(g_batch_date, 'MMDDYYYY')||'.txt';
                 --
                 print_log('Before file open for CI CAD GP JE Actuals.');
                 --
                 l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
                 --
                 print_log ('File handle initiated for CI CAD GP JE Actuals : '||l_file);
                 --                         
                 open c_CI_GP_summary_je (p_concur_batch_id =>rec.concur_batch_id);
                 --
                 loop
                  --
                  fetch c_CI_GP_summary_je bulk collect into c_summary_rec;
                  --
                  exit when c_summary_rec.count =0;
                  --
                      if c_summary_rec.count >0 then 
                         --
                         Null;
                         --
                         -- Begin copy the WW JE actuals header record to the file              
                         --
                         begin
                              --
                              l_line_buffer := 'BATCHID'
                                  ||'|'
                                  ||'ASOFDTE'
                                  ||'|'    
                                  ||'TYPE [OOP OR CARD]'
                                  ||'|'
                                  ||'JOURNAL_ACCT_CODE'
                                  ||'|'
                                  ||'DEBIT'
                                  ||'|'       
                                  ||'CREDIT'
                                  ||'|'
                                  ;                  
                              --
                              utl_file.put_line ( l_utl_file_id, l_line_buffer);
                              -- 
                              l_line_count:= l_line_count+1; --number of lines written
                              --
                         exception
                                 when no_data_found then
                                   exit;
                                 when others then
                                   print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                   print_log('@ Inside file loop operation, error : '||sqlerrm);
                                   exit;
                         end;              
                         --
                         --End copy the WW JE actuals header record to the file              
                         --
                         l_line_buffer :=Null;
                         --
                         --Begin copy the WW JE actuals detail records to the file              
                         --
                         for idx in 1 .. c_summary_rec.count loop 
                           --
                           l_line_buffer :=
                                                        c_summary_rec(idx).batchid
                                                                  ||'|'          
                                                                  ||c_summary_rec(idx).asofdte
                                                                  ||'|'
                                                                  ||case
                                                                         when c_summary_rec(idx).payment_type ='OOP' then 'Out of Pocket'
                                                                         when c_summary_rec(idx).payment_type ='CARD' then 'Credit Card'
                                                                         else 'None'                                                        
                                                                       end
                                                                  ||'|'
                                                                  ||c_summary_rec(idx).journal_acct_code
                                                                  ||'|'           
                                                                  ||c_summary_rec(idx).total_amount
                                                                  ||'|'           
                                                                  ||g_zero
                                                                  ||'|' 
                                                                  ;                
                           --
                                   begin
                                      --
                                      utl_file.put_line ( l_utl_file_id, l_line_buffer);
                                      -- 
                                      l_line_count:= l_line_count+1; --number of lines written
                                      --
                                      g_total := g_total + c_summary_rec(idx).total_amount;
                                      --
                                   exception
                                         when no_data_found then
                                           exit;
                                         when others then
                                           print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                           print_log('@ Inside file loop operation, error : '||sqlerrm);
                                           exit;
                                   end;                
                           --
                          end loop;
                         --
                         --End copy the WW JE actuals detail records to the file              
                         --
                      end if; 
                  --
                 end loop;
                 --
                 close c_CI_GP_summary_je;
                 --
                 -- Begin copy interco clearing rec [CI Brafasco IC with GSC]
                 --
                 begin
                          --
                          open c_CI_GP_IC (p_concur_batch_id =>rec.concur_batch_id);
                          --
                          loop
                           --
                            fetch c_CI_GP_IC bulk collect into c_CI_GP_IC_rec;
                            exit when c_CI_GP_IC_rec.count =0;
                            --
                             if c_CI_GP_IC_rec.count >0 then
                               --
                               for idx in 1 .. c_CI_GP_IC_rec.count loop 
                                --
                                   l_line_buffer :=
                                          c_CI_GP_IC_rec(idx).batchid --Check with Mike Macrae
                                      ||'|'
                                      ||c_CI_GP_IC_rec(idx).asofdte --Check with Mike Macrae
                                      ||'|'          
                                      ||l_Brafasco_IC_GSC_Desc||' ['||c_CI_GP_IC_rec(idx).payment_type||']'
                                      ||'|'          
                                      ||l_Brafasco_IC_GSC_Acct
                                      ||'|'
                                      ||g_zero                                      
                                      ||'|'
                                      ||c_CI_GP_IC_rec(idx).brafasco_ic_amt
                                      ||'|'
                                      ;              
                                      --
                                      utl_file.put_line ( l_utl_file_id, l_line_buffer);
                                      -- 
                                      l_line_count:= l_line_count+1; --number of lines written
                                      --
                                      print_log('Copied interco clearing record [Brafasco IC with GSC] to file '||l_file);
                                      --                                    
                                --
                               end loop;                         
                               --
                             end if;
                           --
                          end loop;
                          --
                          close c_CI_GP_IC;
                          --
                 exception
                     when others then
                       print_log('@ Interco clearing, error =>'||sqlerrm||', line :'||l_line_buffer);
                 end;
                 --                         
                 -- End copy interco clearing rec [CI Brafasco IC with GSC]
                 --                 
                 --             
                 print_log('Total records written into the file [Header and Lines] , l_line_count =>'||l_line_count);
                 --
                 -- Close file handle 
                 --
                 utl_file.fclose ( l_utl_file_id );
                 print_log ('After close for CI CAD GP JE Actuals file : '||l_file);
                 --    
                 -- Begin logging file details for each BU along with their concur batch id
                 begin 
                        --
                        savepoint start_here;
                        --
                        insert into xxcus.xxcus_concur_ebs_bu_files
                        (
                         request_id
                        ,crt_btch_id
                        ,concur_batch_id
                        ,emp_group
                        ,extracted_file_name
                        ,extracted_file_folder
                        ,created_by 
                        ,creation_date
                        )
                        values
                        (
                         l_request_id
                         ,g_current_batch_id
                        ,rec.concur_batch_id --c_uniq_batch_rec.batch_id
                        ,g_CI_GP_emp_grp
                        ,l_file
                        ,g_directory_path
                        ,l_created_by
                        ,l_creation_date
                        )
                        ;
                        --
                        commit;
                        --
                                -- setup the email body
                             --setup_email_body (p_text1 =>'BU CONCUR employee group : '||g_CI_GP_emp_grp); --g_notif_email_body is assigned here
                             setup_email_body 
                               (
                                  p_text1 =>'BU CONCUR employee group : '||g_CI_GP_emp_grp
                                 ,p_emp_group =>g_CI_GP_emp_grp
                                 ,p_type =>'OTHERS'
                               ); --g_notif_email_body is assigned here
                                --print_log('Email Body :'||g_notif_email_body);
                                --
                                g_email_subject :=g_instance||' '||g_CI_GP_emp_grp||' CONCUR extracts to SharePoint - Status Update';
                                print_log('Email Subject :'||g_email_subject);                        
                                --
                                set_bu_sharepoint_email (p_emp_group =>g_CI_GP_emp_grp, p_type =>g_control_type_AA); --g_sharepoint_email is assigned here
                                print_log('SharePoint Email  :'||g_sharepoint_email);                        
                                --
                                -- push the extract to sharepoint
                                print_log('Begin: Push CI CAD Great Plains JE Actuals to SharePoint.');                        
                                sharepoint_integration
                                   (
                                        p_emp_group =>g_CI_GP_emp_grp,
                                        p_file =>g_directory_path||'/'||l_file,
                                        p_sharepoint_email =>g_sharepoint_email,
                                        p_dummy_email_from =>g_notif_email_from,
                                        p_email_subject =>g_email_subject
                                   )
                                   ;
                                print_log('End: Push CI CAD Great Plains JE Actuals to SharePoint.');                  
                 exception
                  when others then
                   print_log('@Failed to log file details for BU, @ emp_group '||g_WW_emp_grp||', message =>'||sqlerrm);
                   rollback to start_here;
                 end;
                 -- End logging file details for each BU along with their concur batch id                              
                 Null;
                 -- 
                 -- %%%%
                         -- Begin copy liability rec [ Insert to concur gl interface table]
                         --
                         g_concur_gl_intf_rec.delete; --Cleanup plsql collection 
                         -- $$$$
                             --                       
                             open c_CI_GP_interco_je (p_concur_batch_id =>rec.concur_batch_id);
                             --
                             loop
                              --
                              fetch c_CI_GP_interco_je bulk collect into c_interco_rec;
                              --
                              exit when c_interco_rec.count =0;
                              --
                                  if c_interco_rec.count >0 then 
                                     --
                                     Null;
                                     --
                                     l_line_idx :=0;
                                     --
                                     --Begin copy the WW JE actuals detail records to the file              
                                     --
                                     for idx in 1 .. c_interco_rec.count loop          
                                       --
                                       l_line_idx :=l_line_idx+1;
                                       --
                                               begin
                                                  -- ++++
                                                     begin
                                                              -- Set set of books id
                                                              g_concur_gl_intf_rec(l_line_idx).set_of_books_id :=g_CAD_ledger_id;
                                                              -- Set accounting date
                                                              g_concur_gl_intf_rec(l_line_idx).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                                              -- Set currency          
                                                              g_concur_gl_intf_rec(l_line_idx).currency_code := Get_Currency (p_emp_group =>g_CI_GP_emp_grp); --Set currency based on concur emp group
                                                              -- Set Batch Name and description
                                                              g_concur_gl_intf_rec(l_line_idx).reference1 :='Concur Batch '||rec.concur_batch_id;                                                    
                                                              g_concur_gl_intf_rec(l_line_idx).reference2 :='Employee Group :'||g_CI_GP_emp_grp;                          
                                                              --
                                                              -- Set Journal entry name and description
                                                              g_concur_gl_intf_rec(l_line_idx).reference4 :=c_interco_rec(idx).reference4; --GL used first 25 characters to prepend the journal entry name                                                   
                                                              g_concur_gl_intf_rec(l_line_idx).reference5 :=c_interco_rec(idx).reference5;                          
                                                              --
                                                              -- Set Journal entry line description                                                                            
                                                              g_concur_gl_intf_rec(l_line_idx).reference10 :=c_interco_rec(idx).reference10;
                                                              --      
                                                              g_concur_gl_intf_rec(l_line_idx).group_id :=g_CI_GP_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                                              --
                                                              g_concur_gl_intf_rec(l_line_idx).reference21 :='Concur Batch  '||rec.concur_batch_id; --Concur batch id
                                                              g_concur_gl_intf_rec(l_line_idx).reference22 :='Concur Batch Date - '||rec.concur_batch_date; --Concur Batch Date
                                                              --
                                                              g_concur_gl_intf_rec(l_line_idx).reference25 :=Null;  
                                                              g_concur_gl_intf_rec(l_line_idx).reference26 :=Null;                          
                                                              g_concur_gl_intf_rec(l_line_idx).reference27 :=Null; 
                                                              g_concur_gl_intf_rec(l_line_idx).reference28 :=Null;
                                                              g_concur_gl_intf_rec(l_line_idx).reference29 :=g_run_id;
                                                              g_concur_gl_intf_rec(l_line_idx).reference30 :=Null;                                                                          
                                                              --                    
                                                              --Begin chart of accounts segments
                                                              --
                                                              -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                                              g_concur_gl_intf_rec(l_line_idx).segment1 := Get_Interco_GL_Product (p_emp_group =>g_CI_GP_emp_grp); --Set segment1 based on concur emp group
                                                              --
                                                              g_concur_gl_intf_rec(l_line_idx).segment2 :=c_interco_rec(idx).location;
                                                              g_concur_gl_intf_rec(l_line_idx).segment3 :=c_interco_rec(idx).cost_ctr;
                                                              g_concur_gl_intf_rec(l_line_idx).segment4 :=c_interco_rec(idx).liability_account;
                                                              --
                                                              g_concur_gl_intf_rec(l_line_idx).segment5 :=c_interco_rec(idx).project_code;
                                                              g_concur_gl_intf_rec(l_line_idx).segment6 :=c_interco_rec(idx).fuse1;
                                                              g_concur_gl_intf_rec(l_line_idx).segment7 :=c_interco_rec(idx).fuse2;
                                                              g_concur_gl_intf_rec(l_line_idx).attribute2 :=g_event_type_IC;                                                              
                                                              --
                                                              --End chart of accounts segments    
                                                              -- set Credit amount         
                                                              g_concur_gl_intf_rec(l_line_idx).entered_cr :=c_interco_rec(idx).credit;                          
                                                              --
                                                              print_log('Finished interco liability record');
                                                              --
                                                              l_line_idx :=l_line_idx+1;  --this is required bcoz we fetch only liability records for insert and have to insert interco clearing for every liability
                                                              --                                                          
                                                              -- ^^^^ Setup intercompany clearing debit journal 
                                                              -- Set set of books id
                                                              g_concur_gl_intf_rec(l_line_idx).set_of_books_id :=g_CAD_ledger_id;
                                                              -- Set accounting date
                                                              g_concur_gl_intf_rec(l_line_idx).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                                              -- Set currency          
                                                              g_concur_gl_intf_rec(l_line_idx).currency_code := Get_Currency (p_emp_group =>g_CI_GP_emp_grp); --Set currency based on concur emp group
                                                              -- Set Batch Name and description
                                                              g_concur_gl_intf_rec(l_line_idx).reference1 :='Concur Batch '||rec.concur_batch_id;                                                    
                                                              g_concur_gl_intf_rec(l_line_idx).reference2 :='Employee Group :'||g_CI_GP_emp_grp;                          
                                                              --
                                                              --
                                                              -- Set Journal entry name and description
                                                              g_concur_gl_intf_rec(l_line_idx).reference4 :=c_interco_rec(idx).reference4; --GL used first 25 characters to prepend the journal entry name                                                   
                                                              g_concur_gl_intf_rec(l_line_idx).reference5 :=c_interco_rec(idx).reference5;                          
                                                              --
                                                              -- Set Journal entry line description                                                                            
                                                              g_concur_gl_intf_rec(l_line_idx).reference10 :=c_interco_rec(idx).reference10;
                                                              --    
                                                              g_concur_gl_intf_rec(l_line_idx).group_id :=g_CI_GP_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                                              --
                                                              g_concur_gl_intf_rec(l_line_idx).reference21 :='Concur Batch  '||rec.concur_batch_id; --Concur batch id
                                                              g_concur_gl_intf_rec(l_line_idx).reference22 :='Concur Batch Date - '||rec.concur_batch_date; --Concur Batch Date
                                                              --
                                                              g_concur_gl_intf_rec(l_line_idx).reference25 :=Null;  
                                                              g_concur_gl_intf_rec(l_line_idx).reference26 :=Null;                          
                                                              g_concur_gl_intf_rec(l_line_idx).reference27 :=Null; 
                                                              g_concur_gl_intf_rec(l_line_idx).reference28 :=Null;
                                                              g_concur_gl_intf_rec(l_line_idx).reference29 :=g_run_id;
                                                              g_concur_gl_intf_rec(l_line_idx).reference30 :=Null;                                                                               
                                                              --                    
                                                              --Begin chart of accounts segments
                                                              --
                                                              -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                                              g_concur_gl_intf_rec(l_line_idx).segment1 := Get_Interco_GL_Product (p_emp_group =>g_CI_GP_emp_grp); --Set segment1 based on concur emp group
                                                              --
                                                              g_concur_gl_intf_rec(l_line_idx).segment2 :=c_interco_rec(idx).location;
                                                              g_concur_gl_intf_rec(l_line_idx).segment3 :=c_interco_rec(idx).cost_ctr;
                                                              g_concur_gl_intf_rec(l_line_idx).segment4 :=c_interco_rec(idx).interco_clearing;
                                                              --
                                                              g_concur_gl_intf_rec(l_line_idx).segment5 :=c_interco_rec(idx).project_code;
                                                              g_concur_gl_intf_rec(l_line_idx).segment6 :=c_interco_rec(idx).fuse1;
                                                              g_concur_gl_intf_rec(l_line_idx).segment7 :=c_interco_rec(idx).fuse2;
                                                              g_concur_gl_intf_rec(l_line_idx).attribute2 :=g_event_type_IC;                                                              
                                                              --
                                                              --End chart of accounts segments    
                                                              -- set Debit amount         
                                                              g_concur_gl_intf_rec(l_line_idx).entered_dr :=c_interco_rec(idx).debit;                          
                                                              --
                                                              print_log('Finished interco clearing record');
                                                              --                                                    
                                                              -- ^^^^
                                                              -- Insert into xxcus_concur_gl_iface table
                                                              begin 
                                                                --
                                                                    flush_concur_gl_iface 
                                                                      (
                                                                         p_emp_group     =>g_CI_GP_emp_grp
                                                                        ,p_oop_or_card  =>Null --Not necessary
                                                                        ,p_group_id         =>g_CI_GP_grp_id
                                                                      );                                                                 
                                                                --
                                                                populate_concur_gl_iface 
                                                                    (
                                                                       p_concur_gl_intf_rec  =>g_concur_gl_intf_rec
                                                                      ,p_group_id                    =>g_CI_GP_grp_id
                                                                    );                                                                    
                                                              exception
                                                               when others then
                                                                print_log('@populate_concur_gl_iface, calling block error : '||sqlerrm);
                                                                raise program_error;
                                                              end;
                                                              --
                                                     exception
                                                         when others then
                                                            print_log('@Setup Interco Liability and Clearing, error =>'||sqlerrm||', line :'||l_line_buffer);
                                                     end;        
                                                     --                                               
                                                  -- ++++
                                               exception
                                                     when others then                         
                                                       print_log('@interco journal, Inside file loop operation, error : '||sqlerrm);
                                                       exit;
                                               end;
                                       --
                                     end loop;
                                     --
                                     --End copy the WW JE actuals detail records to the file              
                                     --
                                  end if; 
                              --
                             end loop;
                             --
                             close c_CI_GP_interco_je;
                             --    
                         -- $$$$     
                         -- End copy liability rec [ Insert to concur gl interface table]                                   
                 -- %%%%         
                         -- Begin CI GP Je with Supporting details
                         
                         l_file :=g_file_prefix||'_BATCH_'||rec.concur_batch_id||'_JE_DETAILS_'||TO_CHAR(g_batch_date, 'MMDDYYYY')||'.txt';
                         --
                         print_log('Before file open for CI CAD GP JE Actuals with supporting details.');
                         --
                         l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
                         --
                         print_log ('File handle initiated for CI CAD GP JE Actuals with supporting details : '||l_file);
                         --                         
                         open c_CI_GP_je_details (p_concur_batch_id =>rec.concur_batch_id) ;
                         --
                         loop
                          --
                          fetch c_CI_GP_je_details bulk collect into c_CI_GP_je_details_rec;
                          --
                          exit when c_CI_GP_je_details_rec.count =0;
                          --
                              if c_CI_GP_je_details_rec.count >0 then 
                                 --
                                  begin
                                      --
                                      l_line_buffer :=
                                               'CONCUR_BATCH_ID'
                                            ||'|'
                                            ||'CONCUR_BATCH_DATE'
                                            ||'|'                                            
                                            ||'RPT_ID'
                                            ||'|'                                            
                                            ||'RPT_KEY'
                                            ||'|'                                            
                                            ||'RPT_ENTRY_KEY'
                                            ||'|'                                            
                                            ||'JOURNAL_RPJ_KEY'
                                            ||'|'                                            
                                            ||'ALLOCATION_KEY'
                                            ||'|'                                            
                                            ||'LINE_NUMBER'
                                            ||'|'                                            
                                            ||'EMP_ID'
                                            ||'|'                                            
                                            ||'EMP_LAST_NAME'
                                            ||'|'                                            
                                            ||'EMP_FIRST_NAME'
                                            ||'|'                                            
                                            ||'EMP_MI'
                                            ||'|'                                            
                                            ||'EMP_GROUP'
                                            ||'|'                                            
                                            ||'EMP_ORG_ERP'
                                            ||'|'                                            
                                            ||'EMP_ORG_COMPANY'
                                            ||'|'                                            
                                            ||'EMP_ORG_BRANCH'
                                            ||'|'                                            
                                            ||'EMP_ORG_DEPT'
                                            ||'|'                                            
                                            ||'EMP_ORG_PROJECT'
                                            ||'|'                                            
                                            ||'LEDGER_CODE'
                                            ||'|'                                            
                                            ||'EMP_CURRENCY_ALPHA_CODE'
                                            ||'|'                                            
                                            ||'RPT_SUBMIT_DATE'
                                            ||'|'                                            
                                            ||'RPT_USER_DEFINED_DATE'
                                            ||'|'                                            
                                            ||'RPT_PMT_PROCESSING_DATE'
                                            ||'|'                                            
                                            ||'RPT_NAME'
                                            ||'|'                                            
                                            ||'RPT_ENTRY_TRANS_TYPE'
                                            ||'|'                                            
                                            ||'RPT_ENTRY_EXPENSE_TYPE_NAME'
                                            ||'|'                                            
                                            ||'RPT_ENTRY_VENDOR_NAME'
                                            ||'|'                                            
                                            ||'RPT_ENTRY_VENDOR_DESC'
                                            ||'|'                                            
                                            ||'MERCHANT_NAME'
                                            ||'|'                                            
                                            ||'MERCHANT_DESCRIPTION'
                                            ||'|'                                            
                                            ||'RPT_ENTRY_LOCATION_CITY'
                                            ||'|'                                            
                                            ||'TRANSACTION_CITY'
                                            ||'|'                                            
                                            ||'RPT_ENTRY_TRANS_DATE'
                                            ||'|'                                            
                                            ||'CC_TRANS_TRANS_DATE'
                                            ||'|'                                            
                                            ||'CC_TRANS_TRANS_ID'
                                            ||'|'                                            
                                            ||'TRANSACTION_STATE'
                                            ||'|'                                            
                                            ||'RPT_ENTRY_FROM_LOCATION'
                                            ||'|'                                            
                                            ||'RPT_ENTRY_TO_LOCATION'
                                            ||'|'                                            
                                            ||'CAR_LOG_ENTRY_BUSINESS_DIST'
                                            ||'|'                                            
                                            ||'RPT_ENTRY_CTRY_SUB_CODE'
                                            ||'|'                                            
                                            ||'RPT_ENTRY_PMT_TYPE_CODE'
                                            ||'|'                                            
                                            ||'RPT_ENTRY_PMT_CODE_NAME'
                                            ||'|'                                            
                                            ||'JOURNAL_PAYER_PMT_TYPE_NAME'
                                            ||'|'                                            
                                            ||'JOURNAL_PAYER_PMT_CODE_NAME'
                                            ||'|'                                            
                                            ||'JOURNAL_PAYEE_PMT_TYPE_NAME'
                                            ||'|'                                            
                                            ||'JOURNAL_PAYEE_PMT_CODE_NAME'
                                            ||'|'                                            
                                            ||'JOURNAL_ACCT_CODE'
                                            ||'|'                                            
                                            ||'JOURNAL_DEBIT_OR_CREDIT'
                                            ||'|'                                            
                                            ||'JOURNAL_AMT'
                                            ||'|'                                            
                                            ||'ALLOCATION_PCT'
                                            ||'|'                                            
                                            ||'ALLOCATION_ERP'
                                            ||'|'                                            
                                            ||'ALLOCATION_COMPANY'
                                            ||'|'                                            
                                            ||'ALLOCATION_BRANCH'
                                            ||'|'                                            
                                            ||'ALLOCATION_DEPT'
                                            ||'|'                                            
                                            ||'ALLOCATION_PROJECT'
                                            ||'|'                                            
                                            ||'OOP_OR_CARD'
                                            ||'|'                                            
                                            --||'GST_AMT_FROM_TAX_RECEIPT'
                                            --||'|'                                            
                                            --||'QST_AMT_FROM_TAX_RECEIPT'
                                            --||'|'                                            
                                            --||'HST_AMT_FROM_TAX_RECEIPT'
                                            --||'|'                                            
                                            --||'PST_AMT_FROM_TAX_RECEIPT'
                                            --||'|'                                            
                                            ||'AMT_NET_TAX_TOT_ADJ'
                                            ||'|'                                            
                                            ||'AMT_NET_TAX_TOT_RCLM'
                                            ||'|'                                            
                                            ||'RPT_VW_TOT_PERSONAL_AMT'
                                            ||'|'                                            
                                            ||'RPT_VW_TOT_CLAIMED_AMT'
                                            ||'|'                                            
                                            ||'RPT_VW_TOT_DUE_EMP'
                                            ||'|'                                            
                                            ||'RPT_VW_TOT_DUE_CMPNY_CARD'
                                            ||'|'                                            
                                            ||'RPT_VW_ACTUAL_TOT_DUE_EMP'
                                            ||'|'                                            
                                            ||'RPT_VW_ACTL_TOT_DUE_CMP_CRD'
                                            ||'|'                                            
                                            ||'RPT_VW_TOT_POSTED_AMT'
                                            ||'|'                                            
                                            ||'RPT_VW_TOT_APPROVED_AMT'
                                            ||'|'                                            
                                            ||'RPT_ENTRY_IS_PERSONAL'
                                            ||'|'                                            
                                            ||'RPT_ENTRY_TOT_TAX_POSTED_AMT'
                                            ||'|'                                            
                                            ||'RPT_ENTRY_VW_NET_TAX_AMT'
                                            ||'|'                                            
                                            ||'RPT_ENTRY_TOT_RCLM_ADJ_AMT'
                                            ||'|'                                            
                                            ||'RPT_ENT_VW_NET_RCLM_ADJ_AMT'
                                            ||'|'                                            
                                            ||'RPT_ENTRY_VW_NET_ADJ_TAX_AMT'
                                            ||'|'                                            
                                            ||'CRT_BTCH_ID'
                                            ||'|'         
                                            ||'PO_NUMBER' -- Ver .16
                                            ||'|' --Ver 1.6
                                          ;
                                      --
                                      utl_file.put_line ( l_utl_file_id, l_line_buffer);
                                      -- 
                                      l_line_count:= l_line_count+1; --number of lines written
                                      --
                                  exception
                                         when no_data_found then
                                           exit;
                                         when others then
                                           print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                           print_log('@ Inside file loop operation, error : '||sqlerrm);
                                           exit;
                                   end;              
                                 --
                                 l_line_buffer :=Null;
                                 --
                                 for idx in 1 .. c_CI_GP_je_details_rec.count loop 
                                   --
                                   l_line_buffer := 
                                            c_ci_gp_je_details_rec(idx).concur_batch_id
                                        ||'|'
                                        ||c_ci_gp_je_details_rec(idx).concur_batch_date
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_id
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_key
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_entry_key
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).journal_rpj_key
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).allocation_key
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).line_number
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).emp_id
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).emp_last_name
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).emp_first_name
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).emp_mi
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).emp_group
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).emp_org_erp
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).emp_org_company
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).emp_org_branch
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).emp_org_dept
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).emp_org_project
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).ledger_code
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).emp_currency_alpha_code
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_submit_date
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_user_defined_date
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_pmt_processing_date
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_name
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_entry_trans_type
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_entry_expense_type_name
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_entry_vendor_name
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_entry_vendor_desc
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).merchant_name
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).merchant_description
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_entry_location_city
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).transaction_city
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_entry_trans_date
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).cc_trans_trans_date
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).cc_trans_trans_id
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).transaction_state
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_entry_from_location
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_entry_to_location
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).car_log_entry_business_dist
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_entry_ctry_sub_code
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_entry_pmt_type_code
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_entry_pmt_code_name
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).journal_payer_pmt_type_name
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).journal_payer_pmt_code_name
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).journal_payee_pmt_type_name
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).journal_payee_pmt_code_name
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).journal_acct_code
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).journal_debit_or_credit
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).journal_amt
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).allocation_pct
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).allocation_erp
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).allocation_company
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).allocation_branch
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).allocation_dept
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).allocation_project
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).oop_or_card
                                        ||'|'                                        
                                        --||c_ci_gp_je_details_rec(idx).gst_amt_from_tax_receipt
                                        --||'|'                                        
                                        --||c_ci_gp_je_details_rec(idx).qst_amt_from_tax_receipt
                                        --||'|'                                        
                                        --||c_ci_gp_je_details_rec(idx).hst_amt_from_tax_receipt
                                        --||'|'                                        
                                        --||c_ci_gp_je_details_rec(idx).pst_amt_from_tax_receipt
                                        --||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).amt_net_tax_tot_adj
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).amt_net_tax_tot_rclm
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_vw_tot_personal_amt
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_vw_tot_claimed_amt
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_vw_tot_due_emp
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_vw_tot_due_cmpny_card
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_vw_actual_tot_due_emp
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_vw_actl_tot_due_cmp_crd
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_vw_tot_posted_amt
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_vw_tot_approved_amt
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_entry_is_personal
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_entry_tot_tax_posted_amt
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_entry_vw_net_tax_amt
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_entry_tot_rclm_adj_amt
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_ent_vw_net_rclm_adj_amt
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).rpt_entry_vw_net_adj_tax_amt
                                        ||'|'                                        
                                        ||c_ci_gp_je_details_rec(idx).crt_btch_id 
                                        ||'|'        
                                        ||c_ci_gp_je_details_rec(idx).po_number -- Ver .16
                                        ||'|' --Ver 1.6                                                                                                         
                                            ;                                                           
                                   --
                                           begin
                                              --
                                              utl_file.put_line ( l_utl_file_id, l_line_buffer);
                                              -- 
                                              l_line_count:= l_line_count+1; --number of lines written
                                              --
                                           exception
                                                 when no_data_found then
                                                   exit;
                                                 when others then
                                                   print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                                   print_log('@ Inside file loop operation, error : '||sqlerrm);
                                                   exit;
                                           end;                
                                   --
                                  end loop;
                                 --    
                              end if; 
                          --
                         end loop;
                         --
                         close c_CI_GP_je_details;
                         --             
                         print_log('Total records written into the file [Header and Lines] , l_line_count =>'||l_line_count);
                         --
                         utl_file.fclose ( l_utl_file_id );
                         print_log ('After close for CI CAD GP JE Actuals file with supporting details : '||l_file);
                         --     
                         -- Close file handle      
                         -- End WW Je with Supporting details
                         --       
                         -- Begin logging file details for each BU along with their concur batch id
                         begin 
                                --
                                savepoint start_here;
                                --
                                insert into xxcus.xxcus_concur_ebs_bu_files
                                (
                                 request_id
                                ,concur_batch_id
                                ,emp_group
                                ,extracted_file_name
                                ,extracted_file_folder
                                ,created_by 
                                ,creation_date
                                ,crt_btch_id
                                )
                                values
                                (
                                 l_request_id
                                ,rec.concur_batch_id --c_uniq_batch_rec.batch_id
                                ,g_CI_GP_emp_grp
                                ,l_file
                                ,g_directory_path
                                ,l_created_by
                                ,l_creation_date
                                ,g_current_batch_id
                                )
                                ;
                                --
                                commit;
                                --
                                -- setup the email body
                                --setup_email_body (p_text1 =>'BU CONCUR employee group : '||g_CI_GP_emp_grp); --g_notif_email_body is assigned here
                                 setup_email_body 
                                   (
                                      p_text1 =>'BU CONCUR employee group : '||g_CI_GP_emp_grp
                                     ,p_emp_group =>g_CI_GP_emp_grp
                                     ,p_type =>'OTHERS'
                                   ); --g_notif_email_body is assigned here                                
                                --print_log('Email Body :'||g_notif_email_body);
                                --
                                g_email_subject :=g_instance||' '||g_CI_GP_emp_grp||' CONCUR extracts to SharePoint - Status Update';
                                print_log('Email Subject :'||g_email_subject);                        
                                --
                                set_bu_sharepoint_email (p_emp_group =>g_CI_GP_emp_grp, p_type =>g_control_type_AA); --g_sharepoint_email is assigned here
                                print_log('SharePoint Email  :'||g_sharepoint_email);                        
                                --
                                -- push the extract to sharepoint
                                print_log('Begin: Push CI CAD Great Plains JE Actuals with supporting details to SharePoint.');                        
                                sharepoint_integration
                                   (
                                        p_emp_group =>g_CI_GP_emp_grp,
                                        p_file =>g_directory_path||'/'||l_file,
                                        p_sharepoint_email =>g_sharepoint_email,
                                        p_dummy_email_from =>g_notif_email_from,
                                        p_email_subject =>g_email_subject
                                   )
                                   ;
                                print_log('End: Push CI CAD Great Plains JE Actuals with supporting details to SharePoint.');                           
                                -- email the teams about the extract
                                send_email_notif
                                   (
                                        p_email_from      =>g_notif_email_from,
                                        p_email_to           =>g_notif_email_to,
                                        p_email_cc           =>g_notif_email_cc,
                                        p_email_subject =>g_email_subject, 
                                        p_email_body     =>g_notif_email_body,
                                        p_mime_type      =>g_email_mime_type
                                   )
                                  ; 
                                print_log('After sending email notification about CI CAD Great Plains JE Actual and Details status');                                                                                                
                                --                                  
                         exception
                          when others then
                           print_log('@Failed to log file details for BU, @ emp_group '||g_WW_emp_grp||', message =>'||sqlerrm);
                           rollback to start_here;
                         end;
                         -- End logging file details for each BU along with their concur batch id  
                         -- Begin Ver 1.3
                         --      
                         l_line_count :=0;
                         --                    
                         l_file :=g_file_prefix||'_BATCH_'||rec.concur_batch_id||'_SUMMARY_RPT_'||TO_CHAR(g_batch_date, 'MMDDYYYY')||'.txt';
                         --
                         print_log('Before file open for CI CAD GP summary report.');
                         --
                         l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
                         --
                         print_log ('File handle initiated for CI CAD GP new summary report : '||l_file);
                         --                         
                         open c_ci_gp_summ_rpt_set (p_concur_batch_id =>rec.concur_batch_id) ;
                         --
                         loop
                          --
                          fetch c_ci_gp_summ_rpt_set bulk collect into c_ci_gp_summ_rpt;
                          --
                          exit when c_ci_gp_summ_rpt.count =0;
                          --
                              if c_ci_gp_summ_rpt.count >0 then 
                                 --
                                  print_log('After file open for CI CAD GP summary report, c_ci_gp_summ_rpt.count ='||c_ci_gp_summ_rpt.count);
                                 --                                 
                                  begin
                                      --
                                      l_line_buffer :=
                                               'DATE_OF_ENTRY'
                                            ||'|'
                                            ||'DATE_OF_EXPENSE'
                                            ||'|'                                            
                                            ||'REPORT_NAME'
                                            ||'|'                                            
                                            ||'EXPENSE_TYPE'
                                            ||'|'                                            
                                            ||'EMP_FULL_NAME'
                                            ||'|'                                            
                                            ||'VENDOR_NAME'
                                            ||'|'                                            
                                            ||'CITY'
                                            ||'|'                                            
                                            ||'TAX_PROVINCE'
                                            ||'|'                                            
                                            ||'TAX_CODE'
                                            ||'|'                                            
                                            ||'JOURNAL_ACCT_CODE'
                                            ||'|'                                            
                                            ||'ALLOCATION_ERP'
                                            ||'|'                                            
                                            ||'ALLOCATION_BRANCH'
                                            ||'|'                                            
                                            ||'EMP_ORG_BRANCH'
                                            ||'|'                                            
                                            ||'LINE_TOTAL'
                                            ||'|'                                            
                                            ||'GST_AMT'
                                            ||'|'                                            
                                            ||'QST_AMT'
                                            ||'|'                                            
                                            ||'HST_AMT'
                                            ||'|'                                            
                                            ||'PST_AMT'
                                            ||'|'                                            
                                            ||'SUBTOTAL'
                                            ||'|'                                            
                                            ||'TAX_ALLOC_TOT_VW_ADJ_AMT'
                                            ||'|'                                            
                                            ||'RPT_ENTRY_TAX_TRANS_AMT'
                                            ||'|'                                            
                                            ||'RECLAIM_AMOUNT_IN_CAD'
                                            ||'|'                                                       
                                          ;
                                      --
                                      utl_file.put_line ( l_utl_file_id, l_line_buffer);
                                      -- 
                                      l_line_count:= l_line_count+1; --number of lines written
                                      --
                                  exception
                                         when no_data_found then
                                           exit;
                                         when others then
                                           print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                           print_log('@ Inside file loop operation, error : '||sqlerrm);
                                           exit;
                                   end;              
                                 --
                                 l_line_buffer :=Null;
                                 --
                                 for idx in 1 .. c_ci_gp_summ_rpt.count loop 
                                   --
                                   l_line_buffer := 
                                              c_ci_gp_summ_rpt(idx).DATE_OF_ENTRY
                                            ||'|'
                                            ||c_ci_gp_summ_rpt(idx).DATE_OF_EXPENSE
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).REPORT_NAME
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).EXPENSE_TYPE
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).EMP_FULL_NAME
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).VENDOR_NAME
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).CITY
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).TAX_PROVINCE
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).TAX_CODE
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).JOURNAL_ACCT_CODE
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).ALLOCATION_ERP
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).ALLOCATION_BRANCH
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).EMP_ORG_BRANCH
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).LINE_TOTAL
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).GST_AMT
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).QST_AMT
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).HST_AMT
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).PST_AMT
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).SUBTOTAL
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).TAX_ALLOC_TOT_VW_ADJ_AMT
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).RPT_ENTRY_TAX_TRANS_AMT
                                            ||'|'                                            
                                            ||c_ci_gp_summ_rpt(idx).RECLAIM_AMOUNT_IN_CAD
                                            ||'|'                                                                           
                                            ;                                                           
                                   --
                                           begin
                                              --
                                              utl_file.put_line ( l_utl_file_id, l_line_buffer);
                                              -- 
                                              l_line_count:= l_line_count+1; --number of lines written
                                              --
                                           exception
                                                 when no_data_found then
                                                   exit;
                                                 when others then
                                                   print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                                   print_log('@ Inside file loop operation, error : '||sqlerrm);
                                                   exit;
                                           end;                
                                   --
                                  end loop;
                                 --    
                              end if; 
                          --
                         end loop;
                         --
                         close c_ci_gp_summ_rpt_set;
                         --             
                         print_log('Total records written into the file [Header and Lines] , l_line_count =>'||l_line_count);
                         --
                         utl_file.fclose ( l_utl_file_id );
                         print_log('After file close for CI CAD GP new summary report.');
                         --     
                         -- Close file handle      
                         -- End CI CAD GP new summary report with supporting details
                         --
                         begin 
                                --
                                savepoint start_here;
                                --
                                insert into xxcus.xxcus_concur_ebs_bu_files
                                (
                                 request_id
                                ,concur_batch_id
                                ,emp_group
                                ,extracted_file_name
                                ,extracted_file_folder
                                ,created_by 
                                ,creation_date
                                ,crt_btch_id
                                )
                                values
                                (
                                 l_request_id
                                ,rec.concur_batch_id --c_uniq_batch_rec.batch_id
                                ,g_CI_GP_emp_grp
                                ,l_file
                                ,g_directory_path
                                ,l_created_by
                                ,l_creation_date
                                ,g_current_batch_id
                                )
                                ;
                                --
                                commit;
                                --
                                -- setup the email body
                                --setup_email_body (p_text1 =>'BU CONCUR employee group : '||g_CI_GP_emp_grp); --g_notif_email_body is assigned here
                                 setup_email_body 
                                   (
                                      p_text1 =>'BU CONCUR employee group : '||g_CI_GP_emp_grp
                                     ,p_emp_group =>g_CI_GP_emp_grp
                                     ,p_type =>'OTHERS'
                                   ); --g_notif_email_body is assigned here                                
                                --print_log('Email Body :'||g_notif_email_body);
                                --
                                g_email_subject :=g_instance||' '||g_CI_GP_emp_grp||' CONCUR extracts to SharePoint - Status Update';
                                print_log('Email Subject :'||g_email_subject);                        
                                --
                                set_bu_sharepoint_email (p_emp_group =>g_CI_GP_emp_grp, p_type =>g_control_type_AA); --g_sharepoint_email is assigned here
                                print_log('SharePoint Email  :'||g_sharepoint_email);                        
                                --
                                -- push the extract to sharepoint
                                print_log('Begin: Push CI CAD Great Plains JE Actuals with supporting details to SharePoint.');                        
                                sharepoint_integration
                                   (
                                        p_emp_group =>g_CI_GP_emp_grp,
                                        p_file =>g_directory_path||'/'||l_file,
                                        p_sharepoint_email =>g_sharepoint_email,
                                        p_dummy_email_from =>g_notif_email_from,
                                        p_email_subject =>g_email_subject
                                   )
                                   ;
                                print_log('End: Push CI CAD Great Plains JE Actuals with supporting details to SharePoint.');                           
                                -- email the teams about the extract
                                send_email_notif
                                   (
                                        p_email_from      =>g_notif_email_from,
                                        p_email_to           =>g_notif_email_to,
                                        p_email_cc           =>g_notif_email_cc,
                                        p_email_subject =>g_email_subject, 
                                        p_email_body     =>g_notif_email_body,
                                        p_mime_type      =>g_email_mime_type
                                   )
                                  ; 
                                print_log('After sending email notification about CI CAD Great Plains JE Actual and Details status');                                                                                                
                                --                                  
                         exception
                          when others then
                           print_log('@Failed to log file details for BU, @ emp_group '||g_WW_emp_grp||', message =>'||sqlerrm);
                           rollback to start_here;
                         end;
                         --                           
                         -- End Ver 1.3
                                                     
       -- ### @ end of rec in c_uniq_concur_batches loop
     end loop;     
     --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    raise program_error;
  end CI_GP_OB_GL_JE;
  -- End CI Great Plains extract je files and generate Interco gl journal interface records   
  --     
  -- Begin CI US extract gl eligible je
  procedure CI_ORACLE_US_ELIGIBLE_GL_JE is 
    --extract Construction and Industrical [CI] US GL journal related info 
    --
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;  
    --
    l_prev_batch_date date :=Null;
    l_sub_routine varchar2(30) :='ci_oracle_us_eligible_gl_je';
    l_ww_je_seq varchar2(10) :=Null;
    --
    cursor c_fiscal_info is
        select period_name fiscal_period_name
                   ,effective_period_num fiscal_period_num
                   ,period_year fiscal_period_year
                   ,start_date fiscal_period_start
                   ,end_date fiscal_period_end
                   ,to_char(null) je_month
                   ,to_char(null) je_year                   
        from gl_period_statuses
        where 1 =2 -- forced so we can get the rowtype declared without any record retrieved
        ;    
    --
    c_fiscal_details c_fiscal_info%rowtype;
    --
    cursor c_uniq_concur_batches is
        select concur_batch_id
        from  xxcus.xxcus_src_concur_sae_n
        where 1 =1
            and crt_btch_id =g_current_batch_id --HDS DW loads this field per import..so we can expect mor than one concur_batch_id per crt_btch_id
            and allocation_custom1 =g_CI_US_emp_grp
            and exists
             (
                  select '1'
                  from    fnd_lookup_values
                  where 1 =1
                        and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
                        and nvl(enabled_flag, 'N') ='Y'
                        and lookup_code =allocation_custom1
             )              
        group by concur_batch_id
        order by concur_batch_id asc  
        ;    
    --    
    cursor gp_je_eligible_records (p_concur_batch_id in number) is
    select 
         null fiscal_period_name --Populated run time when the routine is invoked.
        ,null fiscal_period_num --Populated run time when the routine is invoked.
        ,null fiscal_period_year --Populated run time when the routine is invoked.
        ,null fiscal_period_start --Populated run time when the routine is invoked.
        ,null fiscal_period_end --Populated run time when the routine is invoked.
        ,null date_1 --not in use currently
        ,null date_2 --not in use currently
        ,null date_3 --not in use currently
        ,null date_4 --not in use currently
        ,null date_5 --not in use currently      
        ,allocation_custom2           char_60_1  --WC product code
        ,null                                          char_60_2 --not in use currently
        ,null                                          char_60_3 --not in use currently 
        ,allocation_custom3           char_60_4  --WC location
        ,allocation_custom4           char_60_5  --WC cost center
        ,journal_acct_code              char_60_6 --WC account
        ,allocation_custom11         char_60_7 --WC project code
        ,null                                          char_60_8 --not in use currently
        ,null                                          char_60_9 --not in use currently
        ,case
           when rpt_entry_pmt_type_code in ('CASH', 'IBIP') then 'OOP'
           when rpt_entry_pmt_type_code in ('IBCP', 'CBCP') then 'CARD'
           else 'NONE'           
         end                                   char_60_10
        ,null                                   num_1 --WW je year
        ,null                                   num_2 --WW je sequence starting from 3890 for the fiscal period
        ,journal_amt                  num_3 --WW je amount. Staright copy of the concur field amt_net_tax_tot_adj
       -- ,amt_net_tax_tot_adj  num_3 --WW je amount. Staright copy of the concur field amt_net_tax_tot_adj        
        ,null num_4 --not in use currently
        ,null num_5 --not in use currently
        ,null num_6 --not in use currently
        ,null num_7 --not in use currently
        ,null num_8 --not in use currently
        ,null num_9 --not in use currently
        ,null num_10 --not in use currently
        ,rpt_entry_custom9 char_150_11 -- Ver 1.6 -- po number
        ,null char_150_12 --not in use currently
        ,null char_150_13 --not in use currently
        ,null char_150_14 --not in use currently
        ,null char_150_15 --not in use currently
        ,null char_150_16 --not in use currently
        ,null char_150_17 --not in use currently
        ,null char_150_18 --not in use currently
        ,null char_150_19 --not in use currently
        ,null char_150_20 --not in use currently
        ,null char_1_21 --not in use currently
        ,null char_1_22 --not in use currently
        ,null char_1_23 --not in use currently
        ,null char_1_24 --not in use currently
        ,null char_1_25 --not in use currently
        ,null char_10_26 --not in use currently
        ,null char_10_27 --not in use currently
        ,null char_10_28 --not in use currently
        ,null char_10_29 --not in use currently
        ,null char_10_30 --not in use currently
        ,null char_25_31 --not in use currently
        ,null char_25_32 --not in use currently
        ,null char_25_33 --not in use currently
        ,null char_25_34 --not in use currently
        ,null char_25_35 --not in use currently
        ,null char_25_36 --not in use currently
        ,null coa_code_1 --not in use currently
        ,null coa_code_2 --not in use currently
        ,null coa_code_3 --not in use currently
        ,null coa_code_4 --not in use currently
        ,null coa_code_5 --not in use currently
        ,null coa_code_6 --not in use currently
        ,null coa_code_7 --not in use currently
        ,null coa_desc_1 --not in use currently
        ,null coa_desc_2 --not in use currently
        ,null coa_desc_3 --not in use currently
        ,null coa_desc_4 --not in use currently
        ,null coa_desc_5 --not in use currently
        ,null coa_desc_6 --not in use currently
        ,null coa_desc_7 --not in use currently
        ,concur_batch_id --Straight copy from Concur.
        ,concur_batch_date --Straight copy from Concur.
        ,rpt_id --Straight copy from Concur.
        ,rpt_key --Straight copy from Concur.
        ,rpt_entry_key --Straight copy from Concur.
        ,journal_rpj_key --Straight copy from Concur.
        ,allocation_key --Straight copy from Concur.
        ,line_number --Straight copy from Concur.
        ,emp_id --Straight copy from Concur.
        ,emp_last_name --Straight copy from Concur.
        ,emp_first_name --Straight copy from Concur.
        ,emp_mi --Straight copy from Concur.
        ,emp_custom21 emp_group --Straight copy from Concur.
        ,emp_org_unit1 emp_org_erp             --For "US" use emp_org_unit1. Since WW has no Canadian org its always emp_org_unit1.
        ,emp_org_unit2 emp_org_company  --For "US" use emp_org_unit2. Since WW has no Canadian org its always emp_org_unit2.
        ,emp_org_unit3 emp_org_branch      --For "US" use emp_org_unit3. Since WW has no Canadian org its always emp_org_unit3.
        ,emp_org_unit4 emp_org_dept           --For "US" use emp_org_unit4. Since WW has no Canadian org its always emp_org_unit4.
        ,emp_org_unit6 emp_org_project      --For "US" use emp_org_unit6. Since WW has no Canadian org its always emp_org_unit6.
        ,ledger_ledger_code ledger_code --Straight copy from Concur.
        ,emp_currency_alpha_code --Straight copy from Concur.
        ,rpt_submit_date --Straight copy from Concur.
        ,rpt_user_defined_date --Straight copy from Concur.
        ,rpt_pmt_processing_date --Straight copy from Concur.
        ,rpt_name --Straight copy from Concur.
        ,rpt_entry_trans_type --Straight copy from Concur.
        ,rpt_entry_expense_type_name --Straight copy from Concur.
        ,rpt_entry_vendor_name --Straight copy from Concur.
        ,rpt_entry_vendor_desc --Straight copy from Concur.
        ,cc_trans_merchant_name --Straight copy from Concur.
        ,cc_trans_desc --Straight copy from Concur.
        ,rpt_entry_location_city_name --Straight copy from Concur.
        ,cc_trans_merchant_city --Straight copy from Concur.
        ,rpt_entry_trans_date --Straight copy from Concur.
        ,cc_trans_trans_date --Straight copy from Concur.
        ,cc_trans_trans_id --Straight copy from Concur.
        ,cc_trans_merchant_state --Straight copy from Concur.  
        ,rpt_entry_from_location --Straight copy from Concur.
        ,rpt_entry_to_location --Straight copy from Concur.
        ,car_log_entry_business_dist --Straight copy from Concur.       
        ,rpt_entry_ctry_sub_code --Straight copy from Concur.   
        ,rpt_entry_pmt_type_code --Straight copy from Concur.
        ,rpt_entry_pmt_code_name --Straight copy from Concur.
        ,journal_payer_pmt_type_name --Straight copy from Concur.
        ,journal_payer_pmt_code_name --Straight copy from Concur.
        ,journal_payee_pmt_type_name --Straight copy from Concur.
        ,journal_payee_pmt_code_name --Straight copy from Concur.
        ,journal_acct_code --Straight copy from Concur.
        ,journal_debit_or_credit --Straight copy from Concur.
        ,journal_amt --Straight copy from Concur.
        ,allocation_pct --Straight copy from Concur.
        ,allocation_custom1 allocation_erp            --For "US" use allocation_custom1. For Canada use allocation_custom7. For WW there is no canadian allocation.
        ,allocation_custom2 allocation_company --For "US" use allocation_custom2. For Canada use allocation_custom8. For WW there is no canadian allocation.
        ,allocation_custom3 allocation_branch     --For "US" use allocation_custom3. For Canada use allocation_custom9. For WW there is no canadian allocation.
        ,allocation_custom4 allocation_dept          --For "US" use allocation_custom4. For Canada use allocation_custom10.For WW there is no canadian allocation.
        ,allocation_custom11 allocation_project     --For "US" use allocation_custom11. For Canada use allocation_custom12.For WW there is no canadian allocation.
        ,amt_net_tax_tot_adj --Straight copy from Concur.
        ,amt_net_tax_tot_rclm --Straight copy from Concur.
        ,rpt_vw_tot_personal_amt --Straight copy from Concur.
        ,rpt_vw_tot_claimed_amt --Straight copy from Concur.
        ,rpt_vw_tot_due_emp --Straight copy from Concur.
        ,rpt_vw_tot_due_cmpny_card --Straight copy from Concur.
        ,rpt_vw_actual_tot_due_emp --Straight copy from Concur.
        ,rpt_vw_actl_tot_due_cmp_crd --Straight copy from Concur.
        ,rpt_vw_tot_posted_amt --Straight copy from Concur.
        ,rpt_vw_tot_approved_amt --Straight copy from Concur.
        ,rpt_entry_is_personal --Straight copy from Concur.
        ,rpt_entry_trans_amt --Straight copy from Concur.
        ,rpt_entry_posted_amt --Straight copy from Concur.
        ,rpt_entry_approved_amt --Straight copy from Concur.
        ,rpt_entry_tot_tax_posted_amt --Straight copy from Concur.
        ,rpt_entry_vw_net_tax_amt --Straight copy from Concur.
        ,rpt_entry_tot_rclm_adj_amt --Straight copy from Concur.
        ,rpt_ent_vw_net_rclm_adj_amt --Straight copy from Concur.
        ,rpt_entry_vw_net_adj_tax_amt --Straight copy from Concur.
        ,g_current_batch_id crt_btch_id
    from  xxcus.xxcus_src_concur_sae_n
    where 1 =1       
             and crt_btch_id =g_current_batch_id --HDS DW loads this field per import..so we can expect mor than one concur_batch_id per crt_btch_id
             and allocation_custom1 =g_CI_US_emp_grp ----For Canada use allocation_custom1 
    ;
    --
    type gp_je_eligible_rec_type is table of xxcus.xxcus_concur_eligible_je%rowtype index by binary_integer;
    gp_je_eligible_rec gp_je_eligible_rec_type;
    --
  begin
     -- ****
     for rec in c_uniq_concur_batches loop
                 --
                        delete xxcus.xxcus_concur_eligible_je 
                        where 1 =1
                             and crt_btch_id =g_current_batch_id
                             and allocation_erp =g_CI_US_emp_grp                             
                             and concur_batch_id =rec.concur_batch_id
                             ;-- journals marked for CI US only          
                 --
                  print_log('@ '||l_sub_routine||' Flush any previous loaded eligible JE records for employee group '||g_CI_US_emp_grp||', crt_btch_id : '||g_current_batch_id||', concur_batch_id :'||rec.concur_batch_id||', total deleted :'||sql%rowcount);
                 --
                open gp_je_eligible_records (p_concur_batch_id =>rec.concur_batch_id);
                loop 
                 --
                 fetch gp_je_eligible_records bulk collect into gp_je_eligible_rec limit xxcus_concur_pkg.g_limit;
                 --
                 exit when gp_je_eligible_rec.count =0;
                     --
                     if gp_je_eligible_rec.count >0 then 
                      --
                        for idx in 1 .. gp_je_eligible_rec.count loop 
                          --
                          -- get fiscal period details
                          --
                          if idx =1 then 
                           -- since it's first record get the fiscal period details
                                begin --begin get fiscal info
                                 --
                                 l_prev_batch_date :=gp_je_eligible_rec(idx).concur_batch_date; 
                                 --
                                        select /*+ RESULT_CACHE */
                                                    period_name                                            fiscal_period_name
                                                   ,effective_period_num                           fiscal_period_num
                                                   ,period_year                                              fiscal_period_year
                                                   ,start_date                                                  fiscal_period_start
                                                   ,end_date                                                   fiscal_period_end
                                                   ,lpad(to_char((period_num)), 2,'0')   je_month
                                                   ,to_char(period_year)                            je_year 
                                    into   c_fiscal_details
                                    from gl_period_statuses
                                    where 1 =1
                                        and application_id =101
                                        and ledger_id =g_US_ledger_id
                                        and gp_je_eligible_rec(idx).concur_batch_date between start_date and end_date
                                        ;                 
                                 --
                                exception
                                 when no_data_found then
                                  c_fiscal_details :=null;
                                 when others then
                                  c_fiscal_details :=null;
                                end; --end get fiscal info
                          else
                           --
                               if l_prev_batch_date != gp_je_eligible_rec(idx).concur_batch_date then
                                    begin --begin get fiscal info
                                      --
                                        select /*+ RESULT_CACHE */
                                                    period_name                                            fiscal_period_name
                                                   ,effective_period_num                           fiscal_period_num
                                                   ,period_year                                              fiscal_period_year
                                                   ,start_date                                                  fiscal_period_start
                                                   ,end_date                                                   fiscal_period_end
                                                   ,lpad(to_char((period_num)), 2,'0')   je_month
                                                   ,to_char(period_year)                            je_year                                       
                                        into   c_fiscal_details
                                        from gl_period_statuses
                                        where 1 =1
                                            and application_id =101
                                            and ledger_id =g_US_ledger_id
                                            and gp_je_eligible_rec(idx).concur_batch_date between start_date and end_date
                                            ;                 
                                     --
                                    exception
                                     when no_data_found then
                                      c_fiscal_details :=null;
                                     when others then
                                      c_fiscal_details :=null;
                                    end; --end get fiscal info               
                               else
                                Null; --What this means is DO NOT reset the rowtype variable c_fiscal_details
                               end if;
                                 --
                                 l_prev_batch_date :=gp_je_eligible_rec(idx).concur_batch_date; 
                                 --
                          end if;
                          --
                          -- Assign fiscal info related fields
                          --               
                            gp_je_eligible_rec(idx).fiscal_period_name  :=c_fiscal_details.fiscal_period_name;
                            gp_je_eligible_rec(idx).fiscal_period_num    :=c_fiscal_details.fiscal_period_num;
                            gp_je_eligible_rec(idx).fiscal_period_year    :=c_fiscal_details.fiscal_period_year;
                            gp_je_eligible_rec(idx).fiscal_period_start   :=c_fiscal_details.fiscal_period_start;
                            gp_je_eligible_rec(idx).fiscal_period_end     :=c_fiscal_details.fiscal_period_end;
                          --
                        end loop;
                          --
                          -- bulk insert into xxcus.xxcus_concur_ww_eligible_je table
                          --
                          begin
                           forall idx in 1 .. gp_je_eligible_rec.count 
                            insert into xxcus.xxcus_concur_eligible_je values gp_je_eligible_rec(idx);
                            print_log('Total CI ORACLE US eligible je lines inserted : '||sql%rowcount);                            
                          exception
                           when others then
                            print_log('Error in bulk insert of xxcus.xxcus_concur_ww_eligible_je, message =>'||sqlerrm);
                            raise program_error;
                          end;
                      --
                     end if;
                     --
                end loop;
                close gp_je_eligible_records;
                --      
     end loop;     
     -- ****
  exception
   when program_error then
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end CI_ORACLE_US_ELIGIBLE_GL_JE;        
  --  
  -- End CI US extract gl eligible je  
  -- Begin CI US insert je actuals and generate Interco gl journal interface records
  procedure CI_US_GL_JE is
   --
    l_sub_routine varchar2(30) :='ci_us_gl_je'; --construction and industrial US gl journal entries
    l_line_count number :=0;
    --
    l_wc_oop_clearing_amt number :=0;
    l_wc_card_clearing_amt number :=0;    
    --
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;      
    --
    -- Begin variables used for GSC Interco with CI US    
    l_Interco_Acct_Date Date :=Trunc(Sysdate);
    I_CI_GSC_Interco_loc            xxcus.xxcus_concur_gl_iface.segment2%type  :='BW080';
    I_CI_GSC_Interco_Dept        xxcus.xxcus_concur_gl_iface.segment3%type :='0000';
    I_CI_GSC_Interco_Acct         xxcus.xxcus_concur_gl_iface.segment4%type  :='930118';    
    l_CI_US_IC_GSC_Desc  xxcus.xxcus_concur_gl_iface.reference10%type :='CI US IC with GSC';    
    -- End variables used for GSC Interco with CI US    
    --
    -- Begin variables are used for GSC Interco with CI US
    l_Interco_Location              xxcus.xxcus_concur_gl_iface.segment2%type :='Z0186';
    l_Interco_Cost_Ctr              xxcus.xxcus_concur_gl_iface.segment3%type :='0000';
    l_Interco_OOP_Liability     xxcus.xxcus_concur_gl_iface.segment4%type :='211121'; --'211120'; --Ver 1.1
    l_Interco_CARD_Liability   xxcus.xxcus_concur_gl_iface.segment4%type :='211126'; --'211125'; --Ver 1.1    
    l_Interco_OOP_Clearing    xxcus.xxcus_concur_gl_iface.segment4%type :='910122';
    l_Interco_Project                 xxcus.xxcus_concur_gl_iface.segment5%type :='00000';
    l_Interco_Clearing_Desc  xxcus.xxcus_concur_gl_iface.reference10%type :='GSC IC with CI US';
    -- End variables are used for GSC Interco with CI US        
    --
    l_utl_file_id   UTL_FILE.FILE_TYPE;
    l_line_buffer VARCHAR2(30000):= Null;
    l_file VARCHAR2(150) :=Null;
    l_line_idx Number :=0;
    --
    cursor c_uniq_concur_batches is
        select concur_batch_id, to_char(concur_batch_date, 'mm/dd/yyyy') concur_batch_date                                                                  
        from xxcus.xxcus_concur_eligible_je
        where 1 =1
            and crt_btch_id =g_current_batch_id
            and allocation_erp =g_CI_US_emp_grp
        group by concur_batch_id ,to_char(concur_batch_date, 'mm/dd/yyyy') 
        order by concur_batch_id asc  
        ;  
    --
    cursor c_CI_US_summary_je (p_concur_batch_id in number) is
    select 
                       to_char(concur_batch_date, 'MM/DD/YYYY') concur_batch_date
                      ,char_60_10 payment_type
                      ,char_60_1  gl_product
                      ,char_60_4  gl_location
                      ,char_60_5 cost_ctr
                      ,char_60_6 charge_account
                      ,NVL(char_60_7,'00000') project_code
                      ,g_future_use_1  fuse1
                      ,g_future_use_2  fuse2
                      --,rpt_entry_expense_type_name line_description
                      ,Substr('Name: '||emp_last_name||','||emp_first_name||', Rpt Key:'||rpt_key||', Line:'||line_number||', Vendor:'||rpt_entry_vendor_name, 1, 240) line_description
                      ,case 
                         when char_60_10 ='OOP'  then 'Interco OOP'||', Concur Batch '||p_concur_batch_id 
                         when char_60_10 ='CARD'  then 'Interco Card'||', Concur Batch '||p_concur_batch_id        
                         else 'Other than OOP/Card found'             
                       end reference4
                      ,case 
                         when char_60_10 ='OOP'  then 'Employee Group '||g_CI_US_emp_grp||', Concur Batch '||p_concur_batch_id ||', Interco for OOP'
                         when char_60_10 ='CARD'  then 'Employee Group '||g_CI_US_emp_grp||', Concur Batch '||p_concur_batch_id ||', Interco for CARD'                             
                         else 'Journal for other than OOP/Card'             
                       end reference5                   
                      ,'Concur Batch '||p_concur_batch_id||', '||'CI US IC with GSC' reference10                  
                      ,sum(num_3) charge_amount 
    from xxcus.xxcus_concur_eligible_je
    where 1 =1 
             and crt_btch_id =g_current_batch_id
             and allocation_erp =g_CI_US_emp_grp
             and concur_batch_id =p_concur_batch_id
    group by         
                       to_char(concur_batch_date, 'MM/DD/YYYY') --concur_batch_date
                      ,char_60_10 --payment_type
                      ,char_60_1  --gl_product
                      ,char_60_4  --gl_location
                      ,char_60_5 --cost_ctr
                      ,char_60_6 --charge_account
                      ,NVL(char_60_7,'00000') --project_code
                      ,g_future_use_1  --fuse1
                      ,g_future_use_2  --fuse2                   
                      --,rpt_entry_expense_type_name --actual expense type name
                     ,Substr('Name: '||emp_last_name||','||emp_first_name||', Rpt Key:'||rpt_key||', Line:'||line_number||', Vendor:'||rpt_entry_vendor_name, 1, 240) --line_description 
                      ,case 
                         when char_60_10 ='OOP'  then 'Interco OOP'||', Concur Batch '||p_concur_batch_id 
                         when char_60_10 ='CARD'  then 'Interco Card'||', Concur Batch '||p_concur_batch_id        
                         else 'Other than OOP/Card found'             
                       end --reference4
                      ,case 
                         when char_60_10 ='OOP'  then 'Employee Group '||g_CI_US_emp_grp||', Concur Batch '||p_concur_batch_id ||', Interco for OOP'
                         when char_60_10 ='CARD'  then 'Employee Group '||g_CI_US_emp_grp||', Concur Batch '||p_concur_batch_id ||', Interco for CARD'                             
                         else 'Journal for other than OOP/Card'             
                       end --reference5                   
                      ,'Concur Batch '||p_concur_batch_id||', '||'CI US IC with GSC' --reference10
        ;
    --
    type c_summary_je_type is table of c_CI_US_summary_je%rowtype index by binary_integer;
    c_summary_rec  c_summary_je_type; 
    --
    cursor c_CI_US_je_details (p_concur_batch_id in number) is
        select 
                 fiscal_period_name
                ,char_60_1                  gl_product
                ,char_60_4                  gl_location
                ,char_60_5                  gl_cost_center
                ,char_60_6                  gl_account
                ,char_60_7                  gl_project
                ,char_60_10                oop_or_card
                ,num_3                         journal_amount
                ,rpt_key
                ,line_number
                ,emp_id
                ,emp_last_name
                ,emp_first_name
                ,emp_group
                ,emp_org_erp
                ,emp_org_company
                ,emp_org_branch
                ,emp_org_dept
                ,emp_org_project
                ,ledger_code
                ,emp_currency_alpha_code
                ,rpt_entry_expense_type_name
                ,rpt_entry_vendor_name
                ,rpt_entry_vendor_desc
                ,cc_trans_merchant_name
                ,cc_trans_desc
                ,rpt_entry_location_city_name
                ,to_char(rpt_entry_trans_date, 'mm/dd/yyyy') rpt_entry_trans_date
                ,cc_trans_trans_id
                ,rpt_entry_pmt_type_code
                ,rpt_entry_pmt_code_name
                ,journal_payer_pmt_type_name
                ,journal_payee_pmt_type_name
                ,journal_acct_code
                ,journal_debit_or_credit
                ,journal_amt
                ,allocation_erp
                ,allocation_company
                ,allocation_branch
                ,allocation_dept
                ,allocation_project
                ,rpt_vw_tot_personal_amt
                ,rpt_entry_is_personal
                ,rpt_entry_posted_amt
                ,crt_btch_id
                ,concur_batch_id
                ,to_char(concur_batch_date, 'mm/dd/yyyy') concur_batch_date
                ,char_150_11 po_number -- Ver 1.6                                                
        from xxcus.xxcus_concur_eligible_je
        where 1 =1
            and crt_btch_id =g_current_batch_id
            and allocation_erp =g_CI_US_emp_grp
            and concur_batch_id =p_concur_batch_id --Cursor parameter                   
        order by rpt_key asc, line_number asc
                   ;
    --  
    type  c_CI_US_je_details_type is table of c_CI_US_je_details%rowtype index by binary_integer;
    c_CI_US_je_details_rec  c_CI_US_je_details_type; 
    --
    l_oop_interco_clearing c_summary_je_type;
    l_card_interco_clearing c_summary_je_type;
    --    
    cursor ci_us_header is
        select
        'FISCAL_PERIOD_NAME'||'|'||
        'GL PRODUCT'||'|'||
        'GL LOCATION'||'|'||
        'GL COST CENTER'||'|'||
        'GL ACCOUNT'||'|'||
        'GL PROJECT'||'|'||
        'OOP_OR_CARD'||'|'||
        'JOURNAL AMOUNT'||'|'||
        'RPT_KEY'||'|'||
        'LINE_NUMBER'||'|'||
        'EMP_ID'||'|'||
        'EMP_LAST_NAME'||'|'||
        'EMP_FIRST_NAME'||'|'||
        'EMP_GROUP'||'|'||
        'EMP_ORG_ERP'||'|'||
        'EMP_ORG_COMPANY'||'|'||
        'EMP_ORG_BRANCH'||'|'||
        'EMP_ORG_DEPT'||'|'||
        'EMP_ORG_PROJECT'||'|'||
        'LEDGER_CODE'||'|'||
        'EMP_CURRENCY_ALPHA_CODE'||'|'||
        'RPT_ENTRY_EXPENSE_TYPE_NAME'||'|'||
        'RPT_ENTRY_VENDOR_NAME'||'|'||
        'RPT_ENTRY_VENDOR_DESC'||'|'||
        'CC_TRANS_MERCHANT_NAME'||'|'||
        'CC_TRANS_DESC'||'|'||
        'RPT_ENTRY_LOCATION_CITY_NAME'||'|'||
        'RPT_ENTRY_TRANS_DATE'||'|'||
        'CC_TRANS_TRANS_ID'||'|'||
        'RPT_ENTRY_PMT_TYPE_CODE'||'|'||
        'RPT_ENTRY_PMT_CODE_NAME'||'|'||
        'JOURNAL_PAYER_PMT_TYPE_NAME'||'|'||
        'JOURNAL_PAYEE_PMT_TYPE_NAME'||'|'||
        'JOURNAL_ACCT_CODE'||'|'||
        'JOURNAL_DEBIT_OR_CREDIT'||'|'||
        'JOURNAL_AMOUNT'||'|'||
        'ALLOCATION_ERP'||'|'||
        'ALLOCATION_COMPANY'||'|'||
        'ALLOCATION_BRANCH'||'|'||
        'ALLOCATION_DEPT'||'|'||
        'ALLOCATION_PROJECT'||'|'||
        'RPT_VW_TOT_PERSONAL_AMT'||'|'||
        'RPT_ENTRY_IS_PERSONAL'||'|'||
        'RPT_ENTRY_POSTED_AMT'||'|'||
        'CRT_BTCH_ID'||'|'||
        'CONCUR_BATCH_ID'||'|'||
        'CONCUR_BATCH_DATE'||'|'|| -- Ver 1.6
        'PO_NUMBER'||'|' --Ver 1.6 
        my_data_header
        from dual 
        ; 
    --
    l_data_header    varchar2(32000) :=Null;
    -- 
  begin
     --
       g_concur_gl_intf_rec.delete;
       g_total :=0;
       l_wc_oop_clearing_amt  :=0;
       l_wc_card_clearing_amt :=0; 
     --
     g_outbound_loc :=GET_OUTBOUND_FILE_LOCATION ( v_operation =>'DBA_DIRECTORY', v_emp_group  =>g_CI_ORACLE);
     --
     print_log ('Current working DBA directory is '||g_outbound_loc);
     --                 
    --  
     for rec in c_uniq_concur_batches loop
       -- ### @ begin rec in c_uniq_concur_batches loop
                 --                         
                 open c_CI_US_summary_je (p_concur_batch_id =>rec.concur_batch_id);
                 --
                 loop
                  --
                  fetch c_CI_US_summary_je bulk collect into c_summary_rec;
                  --
                  exit when c_summary_rec.count =0;
                  --
                      if c_summary_rec.count >0 then 
                         --
                         Null;
                         --
                         --Begin insert of actuals charges into concur gl interface for whitecap
                         --
                         for idx in 1 .. c_summary_rec.count loop         
                               --
                               if c_summary_rec(idx).payment_type ='OOP' then
                                  --
                                  if l_oop_interco_clearing.EXISTS(1) = FALSE then
                                      --
                                      l_oop_interco_clearing(1) :=c_summary_rec(idx); --need just one record of type OOP if found
                                      --                          
                                  end if;
                                  --
                                  l_wc_oop_clearing_amt :=l_wc_oop_clearing_amt + c_summary_rec(idx).charge_amount;
                                  -- 
                               elsif c_summary_rec(idx).payment_type ='CARD' then
                                  --
                                  if l_card_interco_clearing.EXISTS(1) = FALSE then
                                      --
                                      l_card_interco_clearing(1) :=c_summary_rec(idx); --need just one record of type CARD if found
                                      -- 
                                  end if;
                                  -- 
                                  l_wc_card_clearing_amt :=l_wc_card_clearing_amt + c_summary_rec(idx).charge_amount;
                                  --                         
                               else 
                                  --
                                  Null; 
                                  --                          
                               end if;
                               --
                               begin
                                  -- $$$$
                                    begin
                                              -- Set set of books id
                                              g_concur_gl_intf_rec(idx).set_of_books_id :=g_US_ledger_id;
                                              -- Set accounting date
                                              g_concur_gl_intf_rec(idx).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                              -- Set currency          
                                              g_concur_gl_intf_rec(idx).currency_code := Get_Currency (p_emp_group =>g_CI_US_emp_grp); --Set currency based on concur emp group
                                              -- Set Batch Name and description
                                              g_concur_gl_intf_rec(idx).reference1 :='Concur Batch '||rec.concur_batch_id;                                                    
                                              g_concur_gl_intf_rec(idx).reference2 :='Employee Group :'||g_CI_US_emp_grp;                          
                                              --
                                              -- Set Journal entry name and description
                                                    g_concur_gl_intf_rec(idx).reference4 :=c_summary_rec(idx).reference4;   
                                                    g_concur_gl_intf_rec(idx).reference5 :=c_summary_rec(idx).reference5;
                                                    -- Set Journal entry line description                                                                            
                                                    g_concur_gl_intf_rec(idx).reference10 :=c_summary_rec(idx).line_description;                   
                                              --      
                                              g_concur_gl_intf_rec(idx).group_id :=g_CI_US_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                              --
                                              g_concur_gl_intf_rec(idx).reference21 :='Concur Batch ID - '||rec.concur_batch_id; --Concur Batch ID
                                              g_concur_gl_intf_rec(idx).reference22 :='Concur Batch Date - '||rec.concur_batch_date; --Concur Batch Date
                                              g_concur_gl_intf_rec(idx).reference23 :=c_summary_rec(idx).payment_type;
                                              g_concur_gl_intf_rec(idx).reference24 :=c_summary_rec(idx).reference10;
                                              --
                                              g_concur_gl_intf_rec(idx).reference25 :=Null;  
                                              g_concur_gl_intf_rec(idx).reference26 :=Null;                          
                                              g_concur_gl_intf_rec(idx).reference27 :=Null; 
                                              g_concur_gl_intf_rec(idx).reference28 :=Null;
                                              g_concur_gl_intf_rec(idx).reference29 :=g_run_id;
                                              g_concur_gl_intf_rec(idx).reference30 :=Null;                                                                          
                                              --                    
                                              --Begin chart of accounts segments
                                              --
                                              -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                              g_concur_gl_intf_rec(idx).segment1 := c_summary_rec(idx).gl_product; 
                                              --
                                              g_concur_gl_intf_rec(idx).segment2 :=c_summary_rec(idx).gl_location;
                                              g_concur_gl_intf_rec(idx).segment3 :=c_summary_rec(idx).cost_ctr;
                                              g_concur_gl_intf_rec(idx).segment4 :=c_summary_rec(idx).charge_account;
                                              --
                                              g_concur_gl_intf_rec(idx).segment5 :=c_summary_rec(idx).project_code;
                                              g_concur_gl_intf_rec(idx).segment6 :=c_summary_rec(idx).fuse1;
                                              g_concur_gl_intf_rec(idx).segment7 :=c_summary_rec(idx).fuse2;
                                              g_concur_gl_intf_rec(idx).attribute2 :=g_event_type_EXP;
                                              --
                                              --End chart of accounts segments
                                              -- set Credit amount         
                                              g_concur_gl_intf_rec(idx).entered_dr :=c_summary_rec(idx).charge_amount;                          
                                              --
                                              print_log('Finished interco clearing [CI US IC WITH GSC] record for '||'OOP');
                                              --
                                    exception
                                         when others then
                                            print_log('@Setup Interco Liability and Clearing, error =>'||sqlerrm||', line :'||l_line_buffer);
                                    end;        
                                     --                                       
                                  -- $$$$
                                  l_line_count:= l_line_count+1; --number of lines written
                                  --
                               exception
                                     when no_data_found then
                                       exit;
                                     when others then                             
                                       print_log('@ Inside file loop operation, error : '||sqlerrm);
                                       exit;
                               end;                
                           --
                          end loop;
                         --
                         --End copy the CI US JE actuals detail records to the file              
                         --
                         -- ^^^^
                          -- Insert into xxcus_concur_gl_iface table
                          begin
                            --
                                flush_concur_gl_iface  --run only once
                                  (
                                     p_emp_group     =>g_CI_US_emp_grp
                                    ,p_oop_or_card  =>'OOP' --All gl interface records where reference23 is OOP, group id 66 with status NEW
                                    ,p_group_id         =>g_CI_US_grp_id
                                  ); 
                            -- 
                                flush_concur_gl_iface  --run only once
                                  (
                                     p_emp_group     =>g_CI_US_emp_grp
                                    ,p_oop_or_card  =>'CARD' --All gl interface records where reference23 is CARD, group id 66 with status NEW
                                    ,p_group_id         =>g_CI_US_grp_id
                                  );                                                                                                  
                            --        
                            populate_concur_gl_iface 
                                (
                                   p_concur_gl_intf_rec  =>g_concur_gl_intf_rec
                                  ,p_group_id                    =>g_CI_US_grp_id
                                );                                
                          exception
                           when others then
                            print_log('@populate_concur_gl_iface, calling block error : '||sqlerrm);
                            raise program_error;
                          end;
                         -- ^^^^                        
                      end if; 
                  --
                 end loop;
                 --
                 close c_CI_US_summary_je;
                 --
                 g_concur_gl_intf_rec.delete; --cleanup records from plsql collection table
                 --
                 l_line_count :=0;
                 --
                 -- Begin insert of CI US Interco clearing with GSC for Out of Pocket AND GSC IC WITH CI US
                 if l_oop_interco_clearing.count >0 then
                   --
                   for idx in 1 .. l_oop_interco_clearing.count loop
                        --
                        l_line_count := l_line_count + 1;
                        --
                        begin
                                  -- Set set of books id
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_CI_US_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Concur Batch '||rec.concur_batch_id;                                                    
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_CI_US_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_oop_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_oop_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_oop_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_CI_US_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Concur Batch ID - '||rec.concur_batch_id; --Concur Batch ID
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Concur Batch Date - '||rec.concur_batch_date; --Concur Batch Date
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_oop_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_oop_interco_clearing(idx).reference10; --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 := l_oop_interco_clearing(idx).gl_product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=I_CI_GSC_Interco_loc;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=I_CI_GSC_Interco_Dept;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=I_CI_GSC_Interco_Acct;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_oop_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_oop_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_EXP;
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_cr :=l_wc_oop_clearing_amt;                          
                                  --
                                  print_log('Finished interco clearing [CI US IC WITH GSC] record for '||'OOP');
                                  --
                        exception
                             when others then
                                print_log('@CI US to GSC: Setup Interco Clearing, error =>'||sqlerrm);
                        end;
                         --  
                         -- Begin GSC Interco with CI US                       
                        begin
                                   --
                                   l_line_count :=l_line_count +1; --increment plsql collection index so we can add record for GSC interco with CI US
                                   --                        
                                  -- Set set of books id
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_CI_US_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Concur Batch '||rec.concur_batch_id;                                                    
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_CI_US_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_oop_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_oop_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_Interco_Clearing_Desc; --l_oop_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_CI_US_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Concur Batch ID - '||rec.concur_batch_id; --Concur Batch ID
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Concur Batch Date - '||rec.concur_batch_date; --Concur Batch Date
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_oop_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_oop_interco_clearing(idx).reference10; --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 :=g_Interco_US_Product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=l_Interco_Location;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=l_Interco_Cost_Ctr;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=l_Interco_OOP_Liability;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_oop_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_oop_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_IC;                                  
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_cr :=l_wc_oop_clearing_amt;                          
                                  --
                                  print_log('Finished OOP clearing [GSC Interco with CI US] liability record for '||'OOP');
                                  --
                                  l_line_count :=l_line_count +1; --increment plsql collection index so we can add record for GSC interco with CI US
                                  -- ~~~~~~
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_CI_US_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Concur Batch '||rec.concur_batch_id;                                                    
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_CI_US_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_oop_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_oop_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_Interco_Clearing_Desc; --l_oop_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_CI_US_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Concur Batch ID - '||rec.concur_batch_id; --Concur Batch ID
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Concur Batch Date - '||rec.concur_batch_date; --Concur Batch Date
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_oop_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_oop_interco_clearing(idx).reference10; --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 :=g_Interco_US_Product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=l_Interco_Location;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=l_Interco_Cost_Ctr;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=l_Interco_OOP_Clearing;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_oop_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_oop_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_IC;                                  
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_dr :=l_wc_oop_clearing_amt;                          
                                  --
                                  --print_log('Finished Interco clearing [GSC Interco with CI US] record for '||'OOP');
                                  --                                                                    
                                  -- ~~~~~~
                                  -- 
                        exception
                             when others then
                                print_log('@CI US to GSC: Setup Interco Clearing, error =>'||sqlerrm);
                        end;
                         --                           
                         -- End GSC Interco with CI US
                         --   
                   end loop;
                    --
                         -- ^^^^
                          -- Insert into xxcus_concur_gl_iface table
                          begin
                             populate_concur_gl_iface 
                                ( 
                                   p_concur_gl_intf_rec =>g_concur_gl_intf_rec
                                  ,p_group_id                   =>g_CI_US_grp_id
                                );
                          exception
                           when others then
                            print_log('@populate_concur_gl_iface, calling block error : '||sqlerrm);
                            raise program_error;
                          end;
                         -- ^^^^                       
                 else
                 --
                 print_log ('Not able to derive interco clearning record for CI US IC with GSC');
                 --
                 end if;                              
                 --    
                 -- End insert of CI US Interco clearing with GSC for Out of Pocket  
                 --              
                 -- Begin logging insert details for CI US Interco clearing with GSC for Out of Pocket
                 begin 
                        --
                        savepoint start_here;
                        --
                        insert into xxcus.xxcus_concur_ebs_bu_files
                        (
                         request_id
                        ,crt_btch_id
                        ,concur_batch_id
                        ,emp_group
                        ,extracted_file_name
                        ,extracted_file_folder
                        ,created_by 
                        ,creation_date
                        )
                        values
                        (
                         l_request_id
                         ,g_current_batch_id
                        ,rec.concur_batch_id --c_uniq_batch_rec.batch_id
                        ,g_CI_US_emp_grp
                        ,substr('Note: GL Interface Journals: CI US Interco with GSC -Complete for Out of Pocket.',1 ,150)
                        ,'Not Applicable because this is a direct transfer to GL within Oracle EBS'
                        ,l_created_by
                        ,l_creation_date
                        )
                        ;
                        --
                        commit;
                        --
                 exception
                  when others then
                   print_log('@Failed to log CI US Interco with GSC GL interface for emp_group '||g_CI_US_emp_grp||', message =>'||sqlerrm);
                   rollback to start_here;
                 end;
                 -- End logging insert details for each CI US Interco clearing with GSC for Out of Pocket
                 --   
                 --
                 g_concur_gl_intf_rec.delete; --cleanup records from plsql collection table 
                 --
                 l_line_count :=0;
                 --
                 -- Begin insert of CI US Interco clearing with GSC for Out of Pocket
                 if l_card_interco_clearing.count >0 then
                   --
                   for idx in 1 .. l_card_interco_clearing.count loop
                        --
                        l_line_count := l_line_count + 1;                   
                        --
                        begin
                                  -- Set set of books id
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_CI_US_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Concur Batch '||rec.concur_batch_id;                                                    
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_CI_US_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_card_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_card_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_card_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_CI_US_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Concur Batch ID - '||rec.concur_batch_id; --Concur Batch ID
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Concur Batch Date - '||rec.concur_batch_date; --Concur Batch Date
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_card_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_card_interco_clearing(idx).reference10;  --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 := l_card_interco_clearing(idx).gl_product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=I_CI_GSC_Interco_loc;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=I_CI_GSC_Interco_Dept;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=I_CI_GSC_Interco_Acct;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_card_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_card_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_EXP;                                  
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_cr :=l_wc_card_clearing_amt;                          
                                  --
                                  print_log('Finished interco clearing [CI US IC WITH GSC] record for '||'CARD.');
                                  --
                        exception
                             when others then
                                print_log('@CI US to GSC: Setup Interco Clearing, error =>'||sqlerrm);
                        end;
                         --  
                         -- Begin GSC Interco with CI US for CARD                      
                        begin
                                   --
                                   l_line_count :=l_line_count +1; --increment plsql collection index so we can add record for GSC interco with CI US
                                   --                        
                                  -- Set set of books id
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_CI_US_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Concur Batch '||rec.concur_batch_id;                                                    
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_CI_US_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_card_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_card_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_Interco_Clearing_Desc; --l_oop_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_CI_US_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Concur Batch ID - '||rec.concur_batch_id; --Concur Batch ID
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Concur Batch Date - '||rec.concur_batch_date; --Concur Batch Date
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_card_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_card_interco_clearing(idx).reference10; --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 :=g_Interco_US_Product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=l_Interco_Location;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=l_Interco_Cost_Ctr;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=l_Interco_CARD_Liability;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_card_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_card_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_IC;                                  
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_cr :=l_wc_card_clearing_amt;                          
                                  --
                                  print_log('Finished  clearing [GSC Interco with CI US] liability record for '||'CARD');
                                  --
                                  l_line_count :=l_line_count +1; --increment plsql collection index so we can add record for GSC interco with CI US
                                  -- ~~~~~~
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_CI_US_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Concur Batch '||rec.concur_batch_id;                                                    
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_CI_US_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_card_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_card_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_Interco_Clearing_Desc; --l_oop_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_CI_US_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Concur Batch ID - '||rec.concur_batch_id; --Concur Batch ID
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Concur Batch Date - '||rec.concur_batch_date; --Concur Batch Date
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_card_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_card_interco_clearing(idx).reference10; --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 := g_Interco_US_Product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=l_Interco_Location;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=l_Interco_Cost_Ctr;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=l_Interco_OOP_Clearing;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_card_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_card_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_IC;                                  
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_dr :=l_wc_card_clearing_amt;                          
                                  --
                                  print_log('Finished Interco clearing [GSC Interco with CI US] record for '||'CARD.');
                                  --                                                                    
                                  -- ~~~~~~
                                  -- 
                        exception
                             when others then
                                print_log('@CI US to GSC: Setup Interco Clearing, error =>'||sqlerrm);
                        end;
                         --                           
                         -- End GSC Interco with CI US for CARD                       
                   end loop;
                    --
                     -- ^^^^
                     -- Insert into xxcus_concur_gl_iface table
                      begin
                         populate_concur_gl_iface 
                            ( 
                               p_concur_gl_intf_rec =>g_concur_gl_intf_rec
                              ,p_group_id                   =>g_CI_US_grp_id
                            );
                      exception
                       when others then
                        print_log('@populate_concur_gl_iface, calling block error : '||sqlerrm);
                        raise program_error;
                      end;
                     -- ^^^^                       
                 else
                 --
                 print_log ('Not able to derive interco clearning record for CI US IC with GSC');
                 --
                 end if;
                 --    
                 -- End insert of CI US Interco clearing with GSC for Out of Pocket  
                 --              
                 -- Begin logging insert details for Interco clearing with GSC for CARD
                 begin 
                        --
                        savepoint start_here;
                        --
                        insert into xxcus.xxcus_concur_ebs_bu_files
                        (
                         request_id
                        ,crt_btch_id
                        ,concur_batch_id
                        ,emp_group
                        ,extracted_file_name
                        ,extracted_file_folder
                        ,created_by 
                        ,creation_date
                        )
                        values
                        (
                         l_request_id
                         ,g_current_batch_id
                        ,rec.concur_batch_id --c_uniq_batch_rec.batch_id
                        ,g_CI_US_emp_grp
                        ,substr('Note: GL Interface Journals: CI US Interco with GSC -Complete for CARD.',1 ,150)
                        ,'Not Applicable because this is a direct transfer to GL within Oracle EBS'
                        ,l_created_by
                        ,l_creation_date
                        )
                        ;
                        --
                        commit;
                        --
                                -- setup the email body
                                setup_email_body 
                                 (
                                    p_text1 =>'BU CONCUR employee group : '||g_CI_US_emp_grp
                                   ,p_emp_group =>g_CI_US_emp_grp
                                   ,p_type =>'OTHERS'
                                 ); --g_notif_email_body is assigned here
                                --print_log('Email Body :'||g_notif_email_body);
                                --
                                g_email_subject :=g_instance||' '||g_CI_US_emp_grp||' CONCUR JE Actuals Interface to Oracle GL - Status Update';
                                print_log('Email Subject :'||g_email_subject);                        
                                --
                                set_bu_sharepoint_email (p_emp_group =>g_CI_US_emp_grp, p_type =>g_control_type_AA); --g_sharepoint_email is assigned here
                                print_log('SharePoint Email  :'||g_sharepoint_email);                        
                                --                          
                                -- email the teams about the extract
                                send_email_notif
                                   (
                                        p_email_from      =>g_notif_email_from,
                                        p_email_to           =>g_notif_email_to,
                                        p_email_cc           =>g_notif_email_cc,
                                        p_email_subject =>g_email_subject, 
                                        p_email_body     =>g_notif_email_body,
                                        p_mime_type      =>g_email_mime_type
                                   )
                                  ; 
                                print_log('After sending email notification about CI CAD Great Plains JE Actual and Details status');                                                                                                
                                --                             
                 exception
                  when others then
                   print_log('@Failed to log CI US Interco with GSC GL interface for emp_group '||g_CI_US_emp_grp||', message =>'||sqlerrm);
                   rollback to start_here;
                 end;
                 -- End logging insert details for Interco clearing with GSC for CARD.
                 --                  
                 -- Begin CI US details file              
                 open c_ci_us_je_details (p_concur_batch_id =>rec.concur_batch_id);
                 fetch c_ci_us_je_details bulk collect into c_CI_US_je_details_rec;
                 close c_ci_us_je_details;
                 --
                 if c_CI_US_je_details_rec.count >0 then 
                      --
                      open ci_us_header;
                      fetch ci_us_header into l_data_header; 
                      close ci_us_header;
                      --
                             -- Create whitecap detail file
                             --
                             if g_database =g_prd_database then   
                               g_file_prefix :=g_CI_ORACLE; --Null; --Ver 1.1
                             else
                               g_file_prefix :=g_database||'_'||g_CI_ORACLE;
                             end if;
                             --        
                             l_file :=g_file_prefix||'_BATCH_'||rec.concur_batch_id||'_DETAILS_'||to_char(g_batch_date,'mmddyyyy')||'.txt';
                             --
                             print_log('Before file open for whitecap details file, concur_batch_id ='||rec.concur_batch_id);
                             --
                             l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
                             --
                             print_log ('Handle initiated for whitecap details file : '||l_file);
                             --
                             -- Copy header record              
                             --                       
                             begin
                                  --                              
                                  l_line_buffer :=l_data_header;
                                  --
                                  utl_file.put_line ( l_utl_file_id, l_line_buffer);
                                  -- 
                                  l_line_count:= l_line_count+1; --number of lines written including header
                                  --                                         
                             exception
                                     when others then
                                       print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                       print_log('@ Inside file loop operation, error : '||sqlerrm);
                             end;              
                             --                       
                      for idx in 1 .. c_CI_US_je_details_rec.count loop
                       -- ))))
                             --  
                                  --
                                  -- Copy WC file detail records              
                                  --       
                                  begin 
                                         --
                                              l_line_buffer := 
                                                            c_ci_us_je_details_rec(idx).fiscal_period_name
                                                    ||'|'||c_ci_us_je_details_rec(idx).gl_product
                                                    ||'|'||c_ci_us_je_details_rec(idx).gl_location
                                                    ||'|'||c_ci_us_je_details_rec(idx).gl_cost_center
                                                    ||'|'||c_ci_us_je_details_rec(idx).gl_account
                                                    ||'|'||c_ci_us_je_details_rec(idx).gl_project
                                                    ||'|'||c_ci_us_je_details_rec(idx).oop_or_card
                                                    ||'|'||c_ci_us_je_details_rec(idx).journal_amount
                                                    ||'|'||c_ci_us_je_details_rec(idx).rpt_key
                                                    ||'|'||c_ci_us_je_details_rec(idx).line_number
                                                    ||'|'||c_ci_us_je_details_rec(idx).emp_id
                                                    ||'|'||c_ci_us_je_details_rec(idx).emp_last_name
                                                    ||'|'||c_ci_us_je_details_rec(idx).emp_first_name
                                                    ||'|'||c_ci_us_je_details_rec(idx).emp_group
                                                    ||'|'||c_ci_us_je_details_rec(idx).emp_org_erp
                                                    ||'|'||c_ci_us_je_details_rec(idx).emp_org_company
                                                    ||'|'||c_ci_us_je_details_rec(idx).emp_org_branch
                                                    ||'|'||c_ci_us_je_details_rec(idx).emp_org_dept
                                                    ||'|'||c_ci_us_je_details_rec(idx).emp_org_project
                                                    ||'|'||c_ci_us_je_details_rec(idx).ledger_code
                                                    ||'|'||c_ci_us_je_details_rec(idx).emp_currency_alpha_code
                                                    ||'|'||c_ci_us_je_details_rec(idx).rpt_entry_expense_type_name
                                                    ||'|'||c_ci_us_je_details_rec(idx).rpt_entry_vendor_name
                                                    ||'|'||c_ci_us_je_details_rec(idx).rpt_entry_vendor_desc
                                                    ||'|'||c_ci_us_je_details_rec(idx).cc_trans_merchant_name
                                                    ||'|'||c_ci_us_je_details_rec(idx).cc_trans_desc
                                                    ||'|'||c_ci_us_je_details_rec(idx).rpt_entry_location_city_name
                                                    ||'|'||c_ci_us_je_details_rec(idx).rpt_entry_trans_date
                                                    ||'|'||c_ci_us_je_details_rec(idx).cc_trans_trans_id
                                                    ||'|'||c_ci_us_je_details_rec(idx).rpt_entry_pmt_type_code
                                                    ||'|'||c_ci_us_je_details_rec(idx).rpt_entry_pmt_code_name
                                                    ||'|'||c_ci_us_je_details_rec(idx).journal_payer_pmt_type_name
                                                    ||'|'||c_ci_us_je_details_rec(idx).journal_payee_pmt_type_name
                                                    ||'|'||c_ci_us_je_details_rec(idx).journal_acct_code
                                                    ||'|'||c_ci_us_je_details_rec(idx).journal_debit_or_credit
                                                    ||'|'||c_ci_us_je_details_rec(idx).journal_amt
                                                    ||'|'||c_ci_us_je_details_rec(idx).allocation_erp
                                                    ||'|'||c_ci_us_je_details_rec(idx).allocation_company
                                                    ||'|'||c_ci_us_je_details_rec(idx).allocation_branch
                                                    ||'|'||c_ci_us_je_details_rec(idx).allocation_dept
                                                    ||'|'||c_ci_us_je_details_rec(idx).allocation_project
                                                    ||'|'||c_ci_us_je_details_rec(idx).rpt_vw_tot_personal_amt
                                                    ||'|'||c_ci_us_je_details_rec(idx).rpt_entry_is_personal
                                                    ||'|'||c_ci_us_je_details_rec(idx).rpt_entry_posted_amt
                                                    ||'|'||c_ci_us_je_details_rec(idx).crt_btch_id
                                                    ||'|'||c_ci_us_je_details_rec(idx).concur_batch_id
                                                    ||'|'||c_ci_us_je_details_rec(idx).concur_batch_date
                                                    ||'|'||c_ci_us_je_details_rec(idx).po_number -- Ver 1.6
                                                    ||'|'
                                                  ;
                                              --
                                              utl_file.put_line ( l_utl_file_id, l_line_buffer);
                                              -- 
                                              l_line_count:= l_line_count+1; --number of lines written including header
                                              --                          
                                  exception                 
                                    when others then
                                     print_log('@ file-write : personal journals detail records, messge =>'||sqlerrm);
                                     raise program_error;
                                  end;   
                                  --                                                     
                       -- ))))
                      end loop;
                      --
                      utl_file.fclose ( l_utl_file_id );
                      --
                             -- Begin logging file details for CI US detail file
                             begin 
                                    --
                                    insert into xxcus.xxcus_concur_ebs_bu_files
                                    (
                                     request_id
                                    ,concur_batch_id
                                    ,emp_group
                                    ,extracted_file_name
                                    ,extracted_file_folder
                                    ,created_by 
                                    ,creation_date
                                    ,crt_btch_id
                                    )
                                    values
                                    (
                                     l_request_id
                                    ,rec.concur_batch_id
                                    ,g_CI_US_emp_grp
                                    ,l_file
                                    ,g_directory_path
                                    ,l_created_by
                                    ,l_creation_date
                                    ,g_current_batch_id
                                    )
                                    ;
                                    --
                                    commit;
                                    --
                                    -- setup the email body
                                    --setup_email_body (p_text1 =>'BU CONCUR employee group : '||g_WW_emp_grp); --g_notif_email_body is assigned here
                                    setup_email_body 
                                      (
                                         p_text1 =>'BU CONCUR employee group : '||g_CI_US_emp_grp
                                        ,p_emp_group =>g_CI_US_emp_grp
                                        ,p_type =>'ORACLE_SOURCED'
                                      ); --g_notif_email_body is assigned here                           
                                    --print_log('Email Body :'||g_notif_email_body);
                                    --
                                    g_email_subject :=g_instance||' '||g_CI_US_emp_grp||' detail extracts to SharePoint - Status Update';
                                    print_log('Email Subject :'||g_email_subject);                        
                                    --
                                    set_bu_sharepoint_email (p_emp_group =>g_CI_US_emp_grp, p_type =>g_control_type_AA); --g_sharepoint_email is assigned here
                                    print_log('SharePoint Email  :'||g_sharepoint_email);                        
                                    --
                                    -- push the extract to sharepoint
                                    print_log('Begin: Push CI US Concur SAE details to SharePoint.'); 
                                    --                       
                                    sharepoint_integration
                                       (
                                            p_emp_group =>g_CI_US_emp_grp,
                                            p_file =>g_directory_path||'/'||l_file,
                                            p_sharepoint_email =>g_sharepoint_email,
                                            p_dummy_email_from =>g_notif_email_from,
                                            p_email_subject =>g_email_subject
                                       )
                                       ;
                                    --
                                    print_log('End: Push CI US Concur SAE details to SharePoint.');                           
                                    -- email the teams about the extract
                                    send_email_notif
                                       (
                                            p_email_from      =>g_notif_email_from,
                                            p_email_to           =>g_notif_email_to,
                                            p_email_cc           =>g_notif_email_cc,
                                            p_email_subject =>g_email_subject, 
                                            p_email_body     =>g_notif_email_body,
                                            p_mime_type      =>g_email_mime_type
                                       )
                                      ; 
                                    print_log('After sending email notification about CI US detail file.');                                                                                                
                                    --              
                             exception
                              when others then
                               print_log('@Failed to log file details for BU, @ emp_group '||g_CI_US_emp_grp||', message =>'||sqlerrm);
                             end;
                             -- End logging file details for CI US detail file                      
                 else
                  print_log('No CI US journals found, exiting without creating detail file.');
                 end if;
                 -- End CI US details file
       -- ### @ end of rec in c_uniq_concur_batches loop
     end loop;     
     --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    raise program_error;
  end CI_US_GL_JE;
  -- End CI US extract je files and generate Interco gl journal interface records   
  -- @@@@@@@@@@@@@@@@@
  -- Begin GSC US extract gl eligible je
  procedure GSC_ORACLE_US_ELIGIBLE_GL_JE is 
    --extract Construction and Industrical [CI] US GL journal related info 
    --
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;  
    --
    l_prev_batch_date date :=Null;
    l_sub_routine varchar2(30) :='gsc_oracle_us_eligible_gl_je';
    l_ww_je_seq varchar2(10) :=Null;
    --
    cursor c_fiscal_info is
        select period_name fiscal_period_name
                   ,effective_period_num fiscal_period_num
                   ,period_year fiscal_period_year
                   ,start_date fiscal_period_start
                   ,end_date fiscal_period_end
                   ,to_char(null) je_month
                   ,to_char(null) je_year                   
        from gl_period_statuses
        where 1 =2 -- forced so we can get the rowtype declared without any record retrieved
        ;    
    --
    c_fiscal_details c_fiscal_info%rowtype;
    --
    cursor c_uniq_concur_batches is
        select concur_batch_id
        from  xxcus.xxcus_src_concur_sae_n
        where 1 =1
             and crt_btch_id =g_current_batch_id --HDS DW loads this field per import..so we can expect mor than one concur_batch_id per crt_btch_id
             and allocation_custom1 =g_GSC_emp_grp
            and exists
             (
                  select '1'
                  from    fnd_lookup_values
                  where 1 =1
                        and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
                        and nvl(enabled_flag, 'N') ='Y'
                        and lookup_code =allocation_custom1
             )              
        group by concur_batch_id
        order by concur_batch_id asc  
        ;    
    --    
    cursor gp_je_eligible_records (p_concur_batch_id in number) is
    select 
         null fiscal_period_name --Populated run time when the routine is invoked.
        ,null fiscal_period_num --Populated run time when the routine is invoked.
        ,null fiscal_period_year --Populated run time when the routine is invoked.
        ,null fiscal_period_start --Populated run time when the routine is invoked.
        ,null fiscal_period_end --Populated run time when the routine is invoked.
        ,null date_1 --not in use currently
        ,null date_2 --not in use currently
        ,null date_3 --not in use currently
        ,null date_4 --not in use currently
        ,null date_5 --not in use currently      
        ,allocation_custom2           char_60_1  -- product code
        ,null                                          char_60_2 --not in use currently
        ,null                                          char_60_3 --not in use currently 
        ,allocation_custom3           char_60_4  -- location
        ,allocation_custom4           char_60_5  -- cost center
        ,journal_acct_code              char_60_6 -- account
        ,allocation_custom11         char_60_7 -- project code
        ,null                                          char_60_8 --not in use currently
        ,null                                          char_60_9 --not in use currently
        ,case
           when rpt_entry_pmt_type_code in ('CASH', 'IBIP') then 'OOP'
           when rpt_entry_pmt_type_code in ('IBCP','CBCP') then 'CARD'
           else 'NONE'
         end                                   char_60_10
        ,null                                   num_1 --WW je year
        ,null                                   num_2 --WW je sequence starting from 3890 for the fiscal period
        ,journal_amt                  num_3 --WW je amount. Staright copy of the concur field amt_net_tax_tot_adj
       -- ,amt_net_tax_tot_adj  num_3 --WW je amount. Staright copy of the concur field amt_net_tax_tot_adj        
        ,null num_4 --not in use currently
        ,null num_5 --not in use currently
        ,null num_6 --not in use currently
        ,null num_7 --not in use currently
        ,null num_8 --not in use currently
        ,null num_9 --not in use currently
        ,null num_10 --not in use currently
        ,rpt_entry_custom9 char_150_11 -- Ver 1.6
        ,null char_150_12 --not in use currently
        ,null char_150_13 --not in use currently
        ,null char_150_14 --not in use currently
        ,null char_150_15 --not in use currently
        ,null char_150_16 --not in use currently
        ,null char_150_17 --not in use currently
        ,null char_150_18 --not in use currently
        ,null char_150_19 --not in use currently
        ,null char_150_20 --not in use currently
        ,null char_1_21 --not in use currently
        ,null char_1_22 --not in use currently
        ,null char_1_23 --not in use currently
        ,null char_1_24 --not in use currently
        ,null char_1_25 --not in use currently
        ,null char_10_26 --not in use currently
        ,null char_10_27 --not in use currently
        ,null char_10_28 --not in use currently
        ,null char_10_29 --not in use currently
        ,null char_10_30 --not in use currently
        ,null char_25_31 --not in use currently
        ,null char_25_32 --not in use currently
        ,null char_25_33 --not in use currently
        ,null char_25_34 --not in use currently
        ,null char_25_35 --not in use currently
        ,null char_25_36 --not in use currently
        ,null coa_code_1 --not in use currently
        ,null coa_code_2 --not in use currently
        ,null coa_code_3 --not in use currently
        ,null coa_code_4 --not in use currently
        ,null coa_code_5 --not in use currently
        ,null coa_code_6 --not in use currently
        ,null coa_code_7 --not in use currently
        ,null coa_desc_1 --not in use currently
        ,null coa_desc_2 --not in use currently
        ,null coa_desc_3 --not in use currently
        ,null coa_desc_4 --not in use currently
        ,null coa_desc_5 --not in use currently
        ,null coa_desc_6 --not in use currently
        ,null coa_desc_7 --not in use currently
        ,concur_batch_id --Straight copy from Concur.
        ,concur_batch_date --Straight copy from Concur.
        ,rpt_id --Straight copy from Concur.
        ,rpt_key --Straight copy from Concur.
        ,rpt_entry_key --Straight copy from Concur.
        ,journal_rpj_key --Straight copy from Concur.
        ,allocation_key --Straight copy from Concur.
        ,line_number --Straight copy from Concur.
        ,emp_id --Straight copy from Concur.
        ,emp_last_name --Straight copy from Concur.
        ,emp_first_name --Straight copy from Concur.
        ,emp_mi --Straight copy from Concur.
        ,emp_custom21 emp_group --Straight copy from Concur.
        ,emp_org_unit1 emp_org_erp             --For "US" use emp_org_unit1. Since WW has no Canadian org its always emp_org_unit1.
        ,emp_org_unit2 emp_org_company  --For "US" use emp_org_unit2. Since WW has no Canadian org its always emp_org_unit2.
        ,emp_org_unit3 emp_org_branch      --For "US" use emp_org_unit3. Since WW has no Canadian org its always emp_org_unit3.
        ,emp_org_unit4 emp_org_dept           --For "US" use emp_org_unit4. Since WW has no Canadian org its always emp_org_unit4.
        ,emp_org_unit6 emp_org_project      --For "US" use emp_org_unit6. Since WW has no Canadian org its always emp_org_unit6.
        ,ledger_ledger_code ledger_code --Straight copy from Concur.
        ,emp_currency_alpha_code --Straight copy from Concur.
        ,rpt_submit_date --Straight copy from Concur.
        ,rpt_user_defined_date --Straight copy from Concur.
        ,rpt_pmt_processing_date --Straight copy from Concur.
        ,rpt_name --Straight copy from Concur.
        ,rpt_entry_trans_type --Straight copy from Concur.
        ,rpt_entry_expense_type_name --Straight copy from Concur.
        ,rpt_entry_vendor_name --Straight copy from Concur.
        ,rpt_entry_vendor_desc --Straight copy from Concur.
        ,cc_trans_merchant_name --Straight copy from Concur.
        ,cc_trans_desc --Straight copy from Concur.
        ,rpt_entry_location_city_name --Straight copy from Concur.
        ,cc_trans_merchant_city --Straight copy from Concur.
        ,rpt_entry_trans_date --Straight copy from Concur.
        ,cc_trans_trans_date --Straight copy from Concur.
        ,cc_trans_trans_id --Straight copy from Concur.
        ,cc_trans_merchant_state --Straight copy from Concur.  
        ,rpt_entry_from_location --Straight copy from Concur.
        ,rpt_entry_to_location --Straight copy from Concur.
        ,car_log_entry_business_dist --Straight copy from Concur.       
        ,rpt_entry_ctry_sub_code --Straight copy from Concur.   
        ,rpt_entry_pmt_type_code --Straight copy from Concur.
        ,rpt_entry_pmt_code_name --Straight copy from Concur.
        ,journal_payer_pmt_type_name --Straight copy from Concur.
        ,journal_payer_pmt_code_name --Straight copy from Concur.
        ,journal_payee_pmt_type_name --Straight copy from Concur.
        ,journal_payee_pmt_code_name --Straight copy from Concur.
        ,journal_acct_code --Straight copy from Concur.
        ,journal_debit_or_credit --Straight copy from Concur.
        ,journal_amt --Straight copy from Concur.
        ,allocation_pct --Straight copy from Concur.
        ,allocation_custom1 allocation_erp            --For "US" use allocation_custom1. For Canada use allocation_custom7. For WW there is no canadian allocation.
        ,allocation_custom2 allocation_company --For "US" use allocation_custom2. For Canada use allocation_custom8. For WW there is no canadian allocation.
        ,allocation_custom3 allocation_branch     --For "US" use allocation_custom3. For Canada use allocation_custom9. For WW there is no canadian allocation.
        ,allocation_custom4 allocation_dept          --For "US" use allocation_custom4. For Canada use allocation_custom10.For WW there is no canadian allocation.
        ,allocation_custom11 allocation_project     --For "US" use allocation_custom11. For Canada use allocation_custom12.For WW there is no canadian allocation.
        ,amt_net_tax_tot_adj --Straight copy from Concur.
        ,amt_net_tax_tot_rclm --Straight copy from Concur.
        ,rpt_vw_tot_personal_amt --Straight copy from Concur.
        ,rpt_vw_tot_claimed_amt --Straight copy from Concur.
        ,rpt_vw_tot_due_emp --Straight copy from Concur.
        ,rpt_vw_tot_due_cmpny_card --Straight copy from Concur.
        ,rpt_vw_actual_tot_due_emp --Straight copy from Concur.
        ,rpt_vw_actl_tot_due_cmp_crd --Straight copy from Concur.
        ,rpt_vw_tot_posted_amt --Straight copy from Concur.
        ,rpt_vw_tot_approved_amt --Straight copy from Concur.
        ,rpt_entry_is_personal --Straight copy from Concur.
        ,rpt_entry_trans_amt --Straight copy from Concur.
        ,rpt_entry_posted_amt --Straight copy from Concur.
        ,rpt_entry_approved_amt --Straight copy from Concur.
        ,rpt_entry_tot_tax_posted_amt --Straight copy from Concur.
        ,rpt_entry_vw_net_tax_amt --Straight copy from Concur.
        ,rpt_entry_tot_rclm_adj_amt --Straight copy from Concur.
        ,rpt_ent_vw_net_rclm_adj_amt --Straight copy from Concur.
        ,rpt_entry_vw_net_adj_tax_amt --Straight copy from Concur.
        ,g_current_batch_id crt_btch_id
    from  xxcus.xxcus_src_concur_sae_n
    where 1 =1       
             and crt_btch_id =g_current_batch_id --HDS DW loads this field per import..so we can expect mor than one concur_batch_id per crt_btch_id
             and allocation_custom1 =g_GSC_emp_grp ----For Canada use allocation_custom1 
    ;
    --
    type gp_je_eligible_rec_type is table of xxcus.xxcus_concur_eligible_je%rowtype index by binary_integer;
    gp_je_eligible_rec gp_je_eligible_rec_type;
    --
  begin
     -- ****
     for rec in c_uniq_concur_batches loop
                 --
                        delete xxcus.xxcus_concur_eligible_je 
                        where 1 =1
                             and crt_btch_id =g_current_batch_id
                             and allocation_erp =g_GSC_emp_grp                             
                             and concur_batch_id =rec.concur_batch_id
                             ;-- journals marked for GSC US only          
                 --
                  print_log('@ '||l_sub_routine||' Flush any previous loaded eligible JE records for employee group '||g_GSC_emp_grp||', crt_btch_id : '||g_current_batch_id||', concur_batch_id :'||rec.concur_batch_id||', total deleted :'||sql%rowcount);
                 --
                open gp_je_eligible_records (p_concur_batch_id =>rec.concur_batch_id);
                loop 
                 --
                 fetch gp_je_eligible_records bulk collect into gp_je_eligible_rec limit xxcus_concur_pkg.g_limit;
                 --
                 exit when gp_je_eligible_rec.count =0;
                     --
                     if gp_je_eligible_rec.count >0 then 
                      --
                        for idx in 1 .. gp_je_eligible_rec.count loop 
                          --
                          -- get fiscal period details
                          --
                          if idx =1 then 
                           -- since it's first record get the fiscal period details
                                begin --begin get fiscal info
                                 --
                                 l_prev_batch_date :=gp_je_eligible_rec(idx).concur_batch_date; 
                                 --
                                        select /*+ RESULT_CACHE */
                                                    period_name                                            fiscal_period_name
                                                   ,effective_period_num                           fiscal_period_num
                                                   ,period_year                                              fiscal_period_year
                                                   ,start_date                                                  fiscal_period_start
                                                   ,end_date                                                   fiscal_period_end
                                                   ,lpad(to_char((period_num)), 2,'0')   je_month
                                                   ,to_char(period_year)                            je_year 
                                    into   c_fiscal_details
                                    from gl_period_statuses
                                    where 1 =1
                                        and application_id =101
                                        and ledger_id =g_US_ledger_id
                                        and gp_je_eligible_rec(idx).concur_batch_date between start_date and end_date
                                        ;                 
                                 --
                                exception
                                 when no_data_found then
                                  c_fiscal_details :=null;
                                 when others then
                                  c_fiscal_details :=null;
                                end; --end get fiscal info
                          else
                           --
                               if l_prev_batch_date != gp_je_eligible_rec(idx).concur_batch_date then
                                    begin --begin get fiscal info
                                      --
                                        select /*+ RESULT_CACHE */
                                                    period_name                                            fiscal_period_name
                                                   ,effective_period_num                           fiscal_period_num
                                                   ,period_year                                              fiscal_period_year
                                                   ,start_date                                                  fiscal_period_start
                                                   ,end_date                                                   fiscal_period_end
                                                   ,lpad(to_char((period_num)), 2,'0')   je_month
                                                   ,to_char(period_year)                            je_year                                       
                                        into   c_fiscal_details
                                        from gl_period_statuses
                                        where 1 =1
                                            and application_id =101
                                            and ledger_id =g_US_ledger_id
                                            and gp_je_eligible_rec(idx).concur_batch_date between start_date and end_date
                                            ;                 
                                     --
                                    exception
                                     when no_data_found then
                                      c_fiscal_details :=null;
                                     when others then
                                      c_fiscal_details :=null;
                                    end; --end get fiscal info               
                               else
                                Null; --What this means is DO NOT reset the rowtype variable c_fiscal_details
                               end if;
                                 --
                                 l_prev_batch_date :=gp_je_eligible_rec(idx).concur_batch_date; 
                                 --
                          end if;
                          --
                          -- Assign fiscal info related fields
                          --               
                            gp_je_eligible_rec(idx).fiscal_period_name  :=c_fiscal_details.fiscal_period_name;
                            gp_je_eligible_rec(idx).fiscal_period_num    :=c_fiscal_details.fiscal_period_num;
                            gp_je_eligible_rec(idx).fiscal_period_year    :=c_fiscal_details.fiscal_period_year;
                            gp_je_eligible_rec(idx).fiscal_period_start   :=c_fiscal_details.fiscal_period_start;
                            gp_je_eligible_rec(idx).fiscal_period_end     :=c_fiscal_details.fiscal_period_end;
                          --
                        end loop;
                          --
                          -- bulk insert into xxcus.xxcus_concur_ww_eligible_je table
                          --
                          begin
                           forall idx in 1 .. gp_je_eligible_rec.count 
                            insert into xxcus.xxcus_concur_eligible_je values gp_je_eligible_rec(idx);
                            print_log('Total GSC ORACLE eligible je lines inserted : '||sql%rowcount);
                          exception
                           when others then
                            print_log('Error in bulk insert of xxcus.xxcus_concur_eligible_je, message =>'||sqlerrm);
                            raise program_error;
                          end;
                      --
                     end if;
                     --
                end loop;
                close gp_je_eligible_records;
                --      
     end loop;     
     -- ****
  exception
   when program_error then
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end GSC_ORACLE_US_ELIGIBLE_GL_JE;        
  --  
  -- End GSC US extract gl eligible je  
  -- Begin GSC US insert je actuals and generate Interco gl journal interface records
  procedure GSC_US_GL_JE is
   --
    l_sub_routine varchar2(30) :='gsc_us_gl_je'; --construction and industrial US gl journal entries
    l_line_count number :=0;
    --
    l_wc_oop_clearing_amt number :=0;
    l_wc_card_clearing_amt number :=0;    
    --
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;      
    --
    -- Begin variables used for GSC Interco with Self
    l_Interco_Acct_Date Date :=Trunc(Sysdate);
    I_CI_GSC_Interco_loc            xxcus.xxcus_concur_gl_iface.segment2%type  :='Z0186';
    I_CI_GSC_Interco_Dept        xxcus.xxcus_concur_gl_iface.segment3%type :='0000';
    I_CI_GSC_Interco_Acct         xxcus.xxcus_concur_gl_iface.segment4%type  :='930118';    
    l_CI_US_IC_GSC_Desc  xxcus.xxcus_concur_gl_iface.reference10%type :='IC with GSC';    
    -- End variables used for GSC Interco with CI US    
    --
    -- Begin variables are used for GSC Interco with Self
    l_Interco_Location              xxcus.xxcus_concur_gl_iface.segment2%type :='Z0186';
    l_Interco_Cost_Ctr              xxcus.xxcus_concur_gl_iface.segment3%type :='0000';
    l_Interco_OOP_Liability     xxcus.xxcus_concur_gl_iface.segment4%type :='211121';   --'211120'
    l_Interco_CARD_Liability   xxcus.xxcus_concur_gl_iface.segment4%type :='211126';   --'211125' 
    l_Interco_OOP_Clearing    xxcus.xxcus_concur_gl_iface.segment4%type :='910118';
    l_Interco_Project                 xxcus.xxcus_concur_gl_iface.segment5%type :='00000';
    l_Interco_Clearing_Desc  xxcus.xxcus_concur_gl_iface.reference10%type :='IC with GSC';
    -- End variables are used for GSC Interco with CI US        
    --
    l_utl_file_id   UTL_FILE.FILE_TYPE;
    l_line_buffer VARCHAR2(30000):= Null;
    l_file VARCHAR2(150) :=Null;
    l_line_idx Number :=0;
    --
    cursor c_uniq_concur_batches is
        select concur_batch_id, to_char(concur_batch_date, 'mm/dd/yyyy') concur_batch_date                                                                  
        from xxcus.xxcus_concur_eligible_je
        where 1 =1
            and crt_btch_id =g_current_batch_id
            and allocation_erp =g_GSC_emp_grp
        group by concur_batch_id ,to_char(concur_batch_date, 'mm/dd/yyyy') 
        order by concur_batch_id asc  
        ;  
    --
    cursor c_GSC_summary_je (p_concur_batch_id in number) is
    select 
                       to_char(concur_batch_date, 'MM/DD/YYYY') concur_batch_date
                      ,char_60_10 payment_type
                      ,char_60_1  gl_product
                      ,char_60_4  gl_location
                      ,char_60_5 cost_ctr
                      ,char_60_6 charge_account
                      ,NVL(char_60_7,'00000') project_code
                      ,g_future_use_1  fuse1
                      ,g_future_use_2  fuse2
                      --,rpt_entry_expense_type_name line_description
                      ,Substr('Name: '||emp_last_name||','||emp_first_name||', Rpt Key:'||rpt_key||', Line:'||line_number||', Vendor:'||nvl(rpt_entry_vendor_name, rpt_entry_vendor_desc), 1, 240) line_description --Ver 1.1
                      ,case 
                         when char_60_10 ='OOP'  then 'Interco OOP'||', Concur Batch '||p_concur_batch_id 
                         when char_60_10 ='CARD'  then 'Interco Card'||', Concur Batch '||p_concur_batch_id        
                         else 'Other than OOP/Card found'             
                       end reference4
                      ,case 
                         when char_60_10 ='OOP'  then 'Employee Group '||g_GSC_emp_grp||', Concur Batch '||p_concur_batch_id ||', Interco for OOP'
                         when char_60_10 ='CARD'  then 'Employee Group '||g_GSC_emp_grp||', Concur Batch '||p_concur_batch_id ||', Interco for CARD'                             
                         else 'Journal for other than OOP/Card'             
                       end reference5                   
                      ,'Concur Batch '||p_concur_batch_id||', '||'IC with GSC' reference10  --Ver 1.1                
                      ,sum(num_3) charge_amount 
    from xxcus.xxcus_concur_eligible_je
    where 1 =1 
             and crt_btch_id =g_current_batch_id
             and allocation_erp =g_GSC_emp_grp
             and concur_batch_id =p_concur_batch_id
    group by         
                       to_char(concur_batch_date, 'MM/DD/YYYY') --concur_batch_date
                      ,char_60_10 --payment_type
                      ,char_60_1  --gl_product
                      ,char_60_4  --gl_location
                      ,char_60_5 --cost_ctr
                      ,char_60_6 --charge_account
                      ,NVL(char_60_7,'00000') --project_code
                      ,g_future_use_1  --fuse1
                      ,g_future_use_2  --fuse2                   
                      --,rpt_entry_expense_type_name --actual expense type name
                     ,Substr('Name: '||emp_last_name||','||emp_first_name||', Rpt Key:'||rpt_key||', Line:'||line_number||', Vendor:'||nvl(rpt_entry_vendor_name, rpt_entry_vendor_desc), 1, 240) --line_description --Ver 1.1 
                      ,case 
                         when char_60_10 ='OOP'  then 'Interco OOP'||', Concur Batch '||p_concur_batch_id 
                         when char_60_10 ='CARD'  then 'Interco Card'||', Concur Batch '||p_concur_batch_id        
                         else 'Other than OOP/Card found'             
                       end --reference4
                      ,case 
                         when char_60_10 ='OOP'  then 'Employee Group '||g_GSC_emp_grp||', Concur Batch '||p_concur_batch_id ||', Interco for OOP'
                         when char_60_10 ='CARD'  then 'Employee Group '||g_GSC_emp_grp||', Concur Batch '||p_concur_batch_id ||', Interco for CARD'                             
                         else 'Journal for other than OOP/Card' 
                       end --reference5                   
                      ,'Concur Batch '||p_concur_batch_id||', '||'IC with GSC' --reference10 --Ver 1.1
        ;
    --
    type c_summary_je_type is table of c_GSC_summary_je%rowtype index by binary_integer;
    c_summary_rec  c_summary_je_type; 
    --
    cursor c_GSC_je_details (p_concur_batch_id in number) is
        select 
                 fiscal_period_name
                ,char_60_1                  gl_product
                ,char_60_4                  gl_location
                ,char_60_5                  gl_cost_center
                ,char_60_6                  gl_account
                ,char_60_7                  gl_project
                ,char_60_10                oop_or_card
                ,num_3                         journal_amount
                ,rpt_key
                ,line_number
                ,emp_id
                ,emp_last_name
                ,emp_first_name
                ,emp_group
                ,emp_org_erp
                ,emp_org_company
                ,emp_org_branch
                ,emp_org_dept
                ,emp_org_project
                ,ledger_code
                ,emp_currency_alpha_code
                ,rpt_entry_expense_type_name
                ,rpt_entry_vendor_name
                ,rpt_entry_vendor_desc
                ,cc_trans_merchant_name
                ,cc_trans_desc
                ,rpt_entry_location_city_name
                ,to_char(rpt_entry_trans_date, 'mm/dd/yyyy') rpt_entry_trans_date
                ,cc_trans_trans_id
                ,rpt_entry_pmt_type_code
                ,rpt_entry_pmt_code_name
                ,journal_payer_pmt_type_name
                ,journal_payee_pmt_type_name
                ,journal_acct_code
                ,journal_debit_or_credit
                ,journal_amt
                ,allocation_erp
                ,allocation_company
                ,allocation_branch
                ,allocation_dept
                ,allocation_project
                ,rpt_vw_tot_personal_amt
                ,rpt_entry_is_personal
                ,rpt_entry_posted_amt
                ,crt_btch_id
                ,concur_batch_id
                ,to_char(concur_batch_date, 'mm/dd/yyyy') concur_batch_date
                ,char_150_11 po_number -- Ver 1.6                                               
        from xxcus.xxcus_concur_eligible_je
        where 1 =1
            and crt_btch_id =g_current_batch_id
            and allocation_erp =g_GSC_emp_grp --g_CI_US_emp_grp -- Ver 1.1
            and concur_batch_id =p_concur_batch_id --Cursor parameter                   
        order by rpt_key asc, line_number asc
                   ;
    --  
    type c_GSC_je_details_type is table of c_GSC_je_details%rowtype index by binary_integer;
    c_GSC_je_details_rec  c_GSC_je_details_type; 
    --   
    l_oop_interco_clearing c_summary_je_type;
    l_card_interco_clearing c_summary_je_type;
    --     
    --    
    cursor gsc_us_header is
        select
        'FISCAL_PERIOD_NAME'||'|'||
        'GL PRODUCT'||'|'||
        'GL LOCATION'||'|'||
        'GL COST CENTER'||'|'||
        'GL ACCOUNT'||'|'||
        'GL PROJECT'||'|'||
        'OOP_OR_CARD'||'|'||
        'JOURNAL AMOUNT'||'|'||
        'RPT_KEY'||'|'||
        'LINE_NUMBER'||'|'||
        'EMP_ID'||'|'||
        'EMP_LAST_NAME'||'|'||
        'EMP_FIRST_NAME'||'|'||
        'EMP_GROUP'||'|'||
        'EMP_ORG_ERP'||'|'||
        'EMP_ORG_COMPANY'||'|'||
        'EMP_ORG_BRANCH'||'|'||
        'EMP_ORG_DEPT'||'|'||
        'EMP_ORG_PROJECT'||'|'||
        'LEDGER_CODE'||'|'||
        'EMP_CURRENCY_ALPHA_CODE'||'|'||
        'RPT_ENTRY_EXPENSE_TYPE_NAME'||'|'||
        'RPT_ENTRY_VENDOR_NAME'||'|'||
        'RPT_ENTRY_VENDOR_DESC'||'|'||
        'CC_TRANS_MERCHANT_NAME'||'|'||
        'CC_TRANS_DESC'||'|'||
        'RPT_ENTRY_LOCATION_CITY_NAME'||'|'||
        'RPT_ENTRY_TRANS_DATE'||'|'||
        'CC_TRANS_TRANS_ID'||'|'||
        'RPT_ENTRY_PMT_TYPE_CODE'||'|'||
        'RPT_ENTRY_PMT_CODE_NAME'||'|'||
        'JOURNAL_PAYER_PMT_TYPE_NAME'||'|'||
        'JOURNAL_PAYEE_PMT_TYPE_NAME'||'|'||
        'JOURNAL_ACCT_CODE'||'|'||
        'JOURNAL_DEBIT_OR_CREDIT'||'|'||
        'JOURNAL_AMOUNT'||'|'||
        'ALLOCATION_ERP'||'|'||
        'ALLOCATION_COMPANY'||'|'||
        'ALLOCATION_BRANCH'||'|'||
        'ALLOCATION_DEPT'||'|'||
        'ALLOCATION_PROJECT'||'|'||
        'RPT_VW_TOT_PERSONAL_AMT'||'|'||
        'RPT_ENTRY_IS_PERSONAL'||'|'||
        'RPT_ENTRY_POSTED_AMT'||'|'||
        'CRT_BTCH_ID'||'|'||
        'CONCUR_BATCH_ID'||'|'||
        'CONCUR_BATCH_DATE'||'|'|| -- Ver 1.6
        'PO_NUMBER'||'|' --Ver 1.6          
        my_data_header
        
        from dual 
        ; 
    --
    l_data_header    varchar2(32000) :=Null;
    --     
  begin
     --
       g_concur_gl_intf_rec.delete;
       g_total :=0;
       l_wc_oop_clearing_amt  :=0;
       l_wc_card_clearing_amt :=0;           
    --  
     g_outbound_loc :=GET_OUTBOUND_FILE_LOCATION ( v_operation =>'DBA_DIRECTORY', v_emp_group  =>g_GSC_ORACLE);
     --
     print_log ('Current working DBA directory is '||g_outbound_loc);
     --      
     for rec in c_uniq_concur_batches loop
       -- ### @ begin rec in c_uniq_concur_batches loop
                 --                         
                 open c_GSC_summary_je (p_concur_batch_id =>rec.concur_batch_id);
                 --
                 loop
                  --
                  fetch c_GSC_summary_je bulk collect into c_summary_rec;
                  --
                  exit when c_summary_rec.count =0;
                  --
                      if c_summary_rec.count >0 then 
                         --
                         Null;
                         --
                         --Begin insert of actuals charges into concur gl interface for whitecap
                         --
                         for idx in 1 .. c_summary_rec.count loop         
                               --
                               if c_summary_rec(idx).payment_type ='OOP' then
                                  --
                                  if l_oop_interco_clearing.EXISTS(1) = FALSE then
                                      --
                                      l_oop_interco_clearing(1) :=c_summary_rec(idx); --need just one record of type OOP if found
                                      --                          
                                  end if;
                                  --
                                  l_wc_oop_clearing_amt :=l_wc_oop_clearing_amt + c_summary_rec(idx).charge_amount;
                                  -- 
                               elsif c_summary_rec(idx).payment_type ='CARD' then
                                  --
                                  if l_card_interco_clearing.EXISTS(1) = FALSE then
                                      --
                                      l_card_interco_clearing(1) :=c_summary_rec(idx); --need just one record of type CARD if found
                                      -- 
                                  end if;
                                  -- 
                                  l_wc_card_clearing_amt :=l_wc_card_clearing_amt + c_summary_rec(idx).charge_amount;
                                  --                         
                               else 
                                  --
                                  Null; 
                                  --                          
                               end if;
                               --
                               begin
                                  -- $$$$
                                    begin
                                              -- Set set of books id
                                              g_concur_gl_intf_rec(idx).set_of_books_id :=g_US_ledger_id;
                                              -- Set accounting date
                                              g_concur_gl_intf_rec(idx).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                              -- Set currency          
                                              g_concur_gl_intf_rec(idx).currency_code := Get_Currency (p_emp_group =>g_GSC_emp_grp); --Set currency based on concur emp group
                                              -- Set Batch Name and description
                                              g_concur_gl_intf_rec(idx).reference1 :='Concur Batch '||rec.concur_batch_id;                                                    
                                              g_concur_gl_intf_rec(idx).reference2 :='Employee Group :'||g_GSC_emp_grp;                          
                                              --
                                              -- Set Journal entry name and description
                                                    g_concur_gl_intf_rec(idx).reference4 :=c_summary_rec(idx).reference4;   
                                                    g_concur_gl_intf_rec(idx).reference5 :=c_summary_rec(idx).reference5;
                                                    -- Set Journal entry line description                                                                            
                                                    g_concur_gl_intf_rec(idx).reference10 :=c_summary_rec(idx).line_description;                   
                                              --      
                                              g_concur_gl_intf_rec(idx).group_id :=g_GSC_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                              --
                                              g_concur_gl_intf_rec(idx).reference21 :='Concur Batch ID - '||rec.concur_batch_id; --Concur Batch ID
                                              g_concur_gl_intf_rec(idx).reference22 :='Concur Batch Date - '||rec.concur_batch_date; --Concur Batch Date
                                              g_concur_gl_intf_rec(idx).reference23 :=c_summary_rec(idx).payment_type;
                                              g_concur_gl_intf_rec(idx).reference24 :=c_summary_rec(idx).reference10;
                                              --
                                              g_concur_gl_intf_rec(idx).reference25 :=Null;  
                                              g_concur_gl_intf_rec(idx).reference26 :=Null;                          
                                              g_concur_gl_intf_rec(idx).reference27 :=Null; 
                                              g_concur_gl_intf_rec(idx).reference28 :=Null;
                                              g_concur_gl_intf_rec(idx).reference29 :=g_run_id;
                                              g_concur_gl_intf_rec(idx).reference30 :=Null;                                                                          
                                              --                    
                                              --Begin chart of accounts segments
                                              --
                                              -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                              g_concur_gl_intf_rec(idx).segment1 := c_summary_rec(idx).gl_product; 
                                              --
                                              g_concur_gl_intf_rec(idx).segment2 :=c_summary_rec(idx).gl_location;
                                              g_concur_gl_intf_rec(idx).segment3 :=c_summary_rec(idx).cost_ctr;
                                              g_concur_gl_intf_rec(idx).segment4 :=c_summary_rec(idx).charge_account;
                                              --
                                              g_concur_gl_intf_rec(idx).segment5 :=c_summary_rec(idx).project_code;
                                              g_concur_gl_intf_rec(idx).segment6 :=c_summary_rec(idx).fuse1;
                                              g_concur_gl_intf_rec(idx).segment7 :=c_summary_rec(idx).fuse2;
                                              g_concur_gl_intf_rec(idx).attribute2 :=g_event_type_EXP;                                              
                                              --
                                              --End chart of accounts segments
                                              -- set Credit amount         
                                              g_concur_gl_intf_rec(idx).entered_dr :=c_summary_rec(idx).charge_amount;                          
                                              --
                                              print_log('Finished interco clearing [IC WITH GSC] record for '||'OOP');
                                              --
                                    exception
                                         when others then
                                            print_log('@Setup Interco Liability and Clearing, error =>'||sqlerrm||', line :'||l_line_buffer);
                                    end;        
                                     --                                       
                                  -- $$$$
                                  l_line_count:= l_line_count+1; --number of lines written
                                  --
                               exception
                                     when no_data_found then
                                       exit;
                                     when others then                             
                                       print_log('@ Inside file loop operation, error : '||sqlerrm);
                                       exit;
                               end;                
                           --
                         end loop;
                         --
                         --End copy the  JE actuals detail records to the file              
                         --
                         -- ^^^^
                          -- Insert into xxcus_concur_gl_iface table
                          begin
                            --
                                flush_concur_gl_iface  --run only once
                                  (
                                     p_emp_group     =>g_GSC_emp_grp
                                    ,p_oop_or_card  =>'OOP' --All gl interface records where reference23 is OOP, group id 66 with status NEW
                                    ,p_group_id         =>g_GSC_grp_id
                                  ); 
                            -- 
                                flush_concur_gl_iface  --run only once
                                  (
                                     p_emp_group     =>g_GSC_emp_grp
                                    ,p_oop_or_card  =>'CARD' --All gl interface records where reference23 is CARD, group id 66 with status NEW
                                    ,p_group_id         =>g_GSC_grp_id
                                  );                                                                                                  
                            --        
                            populate_concur_gl_iface 
                                (
                                   p_concur_gl_intf_rec  =>g_concur_gl_intf_rec
                                  ,p_group_id                    =>g_GSC_grp_id
                                );                                
                          exception
                           when others then
                            print_log('@populate_concur_gl_iface, calling block error : '||sqlerrm);
                            raise program_error;
                          end;
                         -- ^^^^                        
                      end if; 
                  --
                 end loop;
                 --
                 close c_GSC_summary_je;
                 --
                 g_concur_gl_intf_rec.delete; --cleanup records from plsql collection table
                 --
                 l_line_count :=0;
                 --
                 -- Begin insert of GSC US Interco clearing with GSC for Out of Pocket
                 if l_oop_interco_clearing.count >0 then
                       --
                       for idx in 1 .. l_oop_interco_clearing.count loop
                            --
                            l_line_count := l_line_count + 1;
                            --
                            begin
                                      -- Set set of books id
                                      g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                      -- Set accounting date
                                      g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                      -- Set currency          
                                      g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_GSC_emp_grp); --Set currency based on concur emp group
                                      -- Set Batch Name and description
                                      g_concur_gl_intf_rec(l_line_count).reference1 :='Concur Batch '||rec.concur_batch_id;                                                    
                                      g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_GSC_emp_grp;                          
                                      --
                                      -- Set Journal entry name and description
                                            g_concur_gl_intf_rec(l_line_count).reference4 :=l_oop_interco_clearing(idx).reference4;   
                                            g_concur_gl_intf_rec(l_line_count).reference5 :=l_oop_interco_clearing(idx).reference5;
                                            -- Set Journal entry line description                                                                            
                                            g_concur_gl_intf_rec(l_line_count).reference10 :=l_oop_interco_clearing(idx).reference10;                   
                                      --      
                                      g_concur_gl_intf_rec(l_line_count).group_id :=g_GSC_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                      --
                                      g_concur_gl_intf_rec(l_line_count).reference21 :='Concur Batch ID - '||rec.concur_batch_id; --Concur Batch ID
                                      g_concur_gl_intf_rec(l_line_count).reference22 :='Concur Batch Date - '||rec.concur_batch_date; --Concur Batch Date
                                      g_concur_gl_intf_rec(l_line_count).reference23 :=l_oop_interco_clearing(idx).payment_type;
                                      g_concur_gl_intf_rec(l_line_count).reference24 :=l_oop_interco_clearing(idx).reference10; --Null;
                                      --
                                      g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                      g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                      g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                      g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                      g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                      g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                      --                    
                                      --Begin chart of accounts segments
                                      --
                                      -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                      g_concur_gl_intf_rec(l_line_count).segment1 := l_oop_interco_clearing(idx).gl_product; 
                                      --
                                      g_concur_gl_intf_rec(l_line_count).segment2 :=I_CI_GSC_Interco_loc;
                                      g_concur_gl_intf_rec(l_line_count).segment3 :=I_CI_GSC_Interco_Dept;
                                      g_concur_gl_intf_rec(l_line_count).segment4 :=I_CI_GSC_Interco_Acct;
                                      --
                                      g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                      g_concur_gl_intf_rec(l_line_count).segment6 :=l_oop_interco_clearing(idx).fuse1;
                                      g_concur_gl_intf_rec(l_line_count).segment7 :=l_oop_interco_clearing(idx).fuse2;
                                      g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_EXP;                                      
                                      --
                                      --End chart of accounts segments
                                      -- set Credit amount         
                                      g_concur_gl_intf_rec(l_line_count).entered_cr :=l_wc_oop_clearing_amt;                          
                                      --
                                      print_log('Finished interco clearing [IC WITH GSC] actuals for '||'OOP');
                                      --
                            exception
                                 when others then
                                    print_log('@GSC WITH GSC: Setup Interco Clearing, error =>'||sqlerrm);
                            end;
                             --  
                             -- Begin GSC Interco with GSC US                       
                            begin
                                       --
                                       l_line_count :=l_line_count +1; --increment plsql collection index so we can add record for GSC interco with CI US
                                       --                        
                                      -- Set set of books id
                                      g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                      -- Set accounting date
                                      g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                      -- Set currency          
                                      g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_GSC_emp_grp); --Set currency based on concur emp group
                                      -- Set Batch Name and description
                                      g_concur_gl_intf_rec(l_line_count).reference1 :='Concur Batch '||rec.concur_batch_id;                                                    
                                      g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_GSC_emp_grp;                          
                                      --
                                      -- Set Journal entry name and description
                                            g_concur_gl_intf_rec(l_line_count).reference4 :=l_oop_interco_clearing(idx).reference4;   
                                            g_concur_gl_intf_rec(l_line_count).reference5 :=l_oop_interco_clearing(idx).reference5;
                                            -- Set Journal entry line description                                                                            
                                            g_concur_gl_intf_rec(l_line_count).reference10 :=l_Interco_Clearing_Desc; --l_oop_interco_clearing(idx).reference10;                   
                                      --      
                                      g_concur_gl_intf_rec(l_line_count).group_id :=g_GSC_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                      --
                                      g_concur_gl_intf_rec(l_line_count).reference21 :='Concur Batch ID - '||rec.concur_batch_id; --Concur Batch ID
                                      g_concur_gl_intf_rec(l_line_count).reference22 :='Concur Batch Date - '||rec.concur_batch_date; --Concur Batch Date
                                      g_concur_gl_intf_rec(l_line_count).reference23 :=l_oop_interco_clearing(idx).payment_type;
                                      g_concur_gl_intf_rec(l_line_count).reference24 :=l_oop_interco_clearing(idx).reference10; --Null;
                                      --
                                      g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                      g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                      g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                      g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                      g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                      g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                      --                    
                                      --Begin chart of accounts segments
                                      --
                                      -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                      g_concur_gl_intf_rec(l_line_count).segment1 :=g_Interco_US_Product; 
                                      --
                                      g_concur_gl_intf_rec(l_line_count).segment2 :=l_Interco_Location;
                                      g_concur_gl_intf_rec(l_line_count).segment3 :=l_Interco_Cost_Ctr;
                                      g_concur_gl_intf_rec(l_line_count).segment4 :=l_Interco_OOP_Liability;
                                      --
                                      g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                      g_concur_gl_intf_rec(l_line_count).segment6 :=l_oop_interco_clearing(idx).fuse1;
                                      g_concur_gl_intf_rec(l_line_count).segment7 :=l_oop_interco_clearing(idx).fuse2;
                                      g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_IC;                                      
                                      --
                                      --End chart of accounts segments
                                      -- set Credit amount         
                                      g_concur_gl_intf_rec(l_line_count).entered_cr :=l_wc_oop_clearing_amt;                          
                                      --
                                      print_log('Finished OOP clearing [GSC Interco with CI US] liability record for '||'OOP');
                                      --
                                      l_line_count :=l_line_count +1; --increment plsql collection index so we can add record for GSC interco with CI US
                                      -- ~~~~~~
                                      g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                      -- Set accounting date
                                      g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                      -- Set currency          
                                      g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_GSC_emp_grp); --Set currency based on concur emp group
                                      -- Set Batch Name and description
                                      g_concur_gl_intf_rec(l_line_count).reference1 :='Concur Batch '||rec.concur_batch_id;                                                    
                                      g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_GSC_emp_grp;                          
                                      --
                                      -- Set Journal entry name and description
                                            g_concur_gl_intf_rec(l_line_count).reference4 :=l_oop_interco_clearing(idx).reference4;   
                                            g_concur_gl_intf_rec(l_line_count).reference5 :=l_oop_interco_clearing(idx).reference5;
                                            -- Set Journal entry line description                                                                            
                                            g_concur_gl_intf_rec(l_line_count).reference10 :=l_Interco_Clearing_Desc; --l_oop_interco_clearing(idx).reference10;                   
                                      --      
                                      g_concur_gl_intf_rec(l_line_count).group_id :=g_GSC_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                      --
                                      g_concur_gl_intf_rec(l_line_count).reference21 :='Concur Batch ID - '||rec.concur_batch_id; --Concur Batch ID
                                      g_concur_gl_intf_rec(l_line_count).reference22 :='Concur Batch Date - '||rec.concur_batch_date; --Concur Batch Date
                                      g_concur_gl_intf_rec(l_line_count).reference23 :=l_oop_interco_clearing(idx).payment_type;
                                      g_concur_gl_intf_rec(l_line_count).reference24 :=l_oop_interco_clearing(idx).reference10; --Null;
                                      --
                                      g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                      g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                      g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                      g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                      g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                      g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                      --                    
                                      --Begin chart of accounts segments
                                      --
                                      -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                      g_concur_gl_intf_rec(l_line_count).segment1 :=g_Interco_US_Product; 
                                      --
                                      g_concur_gl_intf_rec(l_line_count).segment2 :=l_Interco_Location;
                                      g_concur_gl_intf_rec(l_line_count).segment3 :=l_Interco_Cost_Ctr;
                                      g_concur_gl_intf_rec(l_line_count).segment4 :=l_Interco_OOP_Clearing;
                                      --
                                      g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                      g_concur_gl_intf_rec(l_line_count).segment6 :=l_oop_interco_clearing(idx).fuse1;
                                      g_concur_gl_intf_rec(l_line_count).segment7 :=l_oop_interco_clearing(idx).fuse2;
                                      g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_IC;                                      
                                      --
                                      --End chart of accounts segments
                                      -- set Credit amount         
                                      g_concur_gl_intf_rec(l_line_count).entered_dr :=l_wc_oop_clearing_amt;                          
                                      --
                                      --print_log('Finished Interco clearing [GSC Interco with CI US] record for '||'OOP');
                                      --                                                                    
                                      -- ~~~~~~
                                      -- 
                            exception
                                 when others then
                                    print_log('@CI US to GSC: Setup Interco Clearing, error =>'||sqlerrm);
                            end;
                             --                           
                             -- End GSC Interco with CI US
                             --   
                       end loop;
                        --
                         -- ^^^^
                          -- Insert into xxcus_concur_gl_iface table
                          begin
                             populate_concur_gl_iface 
                                ( 
                                   p_concur_gl_intf_rec =>g_concur_gl_intf_rec
                                  ,p_group_id                   =>g_GSC_grp_id
                                );
                          exception
                           when others then
                            print_log('@populate_concur_gl_iface, calling block error : '||sqlerrm);
                            raise program_error;
                          end;
                         -- ^^^^                       
                 else
                 --
                 print_log ('No record found in collection l_oop_interco_clearing for GSC US IC.');
                 --
                 end if;                              
                 --    
                 -- End insert of GSC US Interco clearing with GSC for Out of Pocket  
                 --              
                 -- Begin logging insert details for GSC US Interco clearing with GSC for Out of Pocket
                 begin 
                        --
                        savepoint start_here;
                        --
                        insert into xxcus.xxcus_concur_ebs_bu_files
                        (
                         request_id
                        ,crt_btch_id
                        ,concur_batch_id
                        ,emp_group
                        ,extracted_file_name
                        ,extracted_file_folder
                        ,created_by 
                        ,creation_date
                        )
                        values
                        (
                         l_request_id
                         ,g_current_batch_id
                        ,rec.concur_batch_id --c_uniq_batch_rec.batch_id
                        ,g_GSC_emp_grp
                        ,substr('Note: GL Interface Journals: CI US Interco with GSC -Complete for Out of Pocket.',1 ,150)
                        ,'Not Applicable because this is a direct transfer to GL within Oracle EBS'
                        ,l_created_by
                        ,l_creation_date
                        )
                        ;
                        --
                        commit;
                        --
                 exception
                  when others then
                   print_log('@Failed to log CI US Interco with GSC GL interface for emp_group '||g_GSC_emp_grp||', message =>'||sqlerrm);
                   rollback to start_here;
                 end;
                 -- End logging insert details for each GSC US Interco clearing with GSC for Out of Pocket
                 --   
                 --
                 g_concur_gl_intf_rec.delete; --cleanup records from plsql collection table 
                 --
                 l_line_count :=0;
                 --
                 -- Begin insert of GSC US Interco clearing with GSC for Out of Pocket
                 if l_card_interco_clearing.count >0 then
                   --
                   for idx in 1 .. l_card_interco_clearing.count loop
                        --
                        l_line_count := l_line_count + 1;                   
                        --
                        begin
                                  -- Set set of books id
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_GSC_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Concur Batch '||rec.concur_batch_id;                                                    
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_GSC_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_card_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_card_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_card_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_GSC_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Concur Batch ID - '||rec.concur_batch_id; --Concur Batch ID
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Concur Batch Date - '||rec.concur_batch_date; --Concur Batch Date
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_card_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_card_interco_clearing(idx).reference10;  --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 := l_card_interco_clearing(idx).gl_product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=I_CI_GSC_Interco_loc;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=I_CI_GSC_Interco_Dept;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=I_CI_GSC_Interco_Acct;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_card_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_card_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_EXP;                                  
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_cr :=l_wc_card_clearing_amt;                          
                                  --
                                  print_log('Finished interco clearing [CI US IC WITH GSC] record for '||'CARD.');
                                  --
                        exception
                             when others then
                                print_log('@CI US to GSC: Setup Interco Clearing, error =>'||sqlerrm);
                        end;
                         --  
                         -- Begin GSC Interco with GSC US for CARD                      
                        begin
                                   --
                                   l_line_count :=l_line_count +1; --increment plsql collection index so we can add record for GSC interco with CI US
                                   --                        
                                  -- Set set of books id
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_GSC_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Concur Batch '||rec.concur_batch_id;                                                    
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_GSC_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_card_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_card_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_Interco_Clearing_Desc; --l_oop_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_GSC_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Concur Batch ID - '||rec.concur_batch_id; --Concur Batch ID
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Concur Batch Date - '||rec.concur_batch_date; --Concur Batch Date
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_card_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_card_interco_clearing(idx).reference10; --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 :=g_Interco_US_Product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=l_Interco_Location;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=l_Interco_Cost_Ctr;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=l_Interco_CARD_Liability;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_card_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_card_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_IC;                                  
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_cr :=l_wc_card_clearing_amt;                          
                                  --
                                  print_log('Finished  clearing [GSC Interco with CI US] liability record for '||'CARD');
                                  --
                                  l_line_count :=l_line_count +1; --increment plsql collection index so we can add record for GSC interco with CI US
                                  -- ~~~~~~
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_GSC_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Concur Batch '||rec.concur_batch_id;                                                    
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_GSC_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_card_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_card_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_Interco_Clearing_Desc; --l_oop_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_GSC_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Concur Batch ID - '||rec.concur_batch_id; --Concur Batch ID
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Concur Batch Date - '||rec.concur_batch_date; --Concur Batch Date
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_card_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_card_interco_clearing(idx).reference10; --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 := g_Interco_US_Product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=l_Interco_Location;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=l_Interco_Cost_Ctr;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=l_Interco_OOP_Clearing;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_card_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_card_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_IC;                                  
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_dr :=l_wc_card_clearing_amt;                          
                                  --
                                  print_log('Finished Interco clearing [GSC Interco with CI US] record for '||'CARD.');
                                  --
                        exception
                             when others then
                                print_log('@CI US to GSC: Setup Interco Clearing, error =>'||sqlerrm);
                        end;
                         --                           
                         -- End GSC Interco with CI US for CARD                       
                   end loop;
                    --
                     -- ^^^^
                     -- Insert into xxcus_concur_gl_iface table
                      begin
                         populate_concur_gl_iface 
                            ( 
                               p_concur_gl_intf_rec =>g_concur_gl_intf_rec
                              ,p_group_id                   =>g_GSC_grp_id
                            );
                      exception
                       when others then
                        print_log('@populate_concur_gl_iface, calling block error : '||sqlerrm);
                        raise program_error;
                      end;
                     -- ^^^^                       
                 else
                 --
                 print_log ('Not able to derive interco clearning record for CI US IC with GSC');
                 --
                 end if;                              
                 --    
                 -- End insert of CI US Interco clearing with GSC for Out of Pocket  
                 --              
                 -- Begin logging insert details for Interco clearing with GSC for CARD
                 begin 
                        --
                        savepoint start_here;
                        --
                        insert into xxcus.xxcus_concur_ebs_bu_files
                        (
                         request_id
                        ,crt_btch_id
                        ,concur_batch_id
                        ,emp_group
                        ,extracted_file_name
                        ,extracted_file_folder
                        ,created_by 
                        ,creation_date
                        )
                        values
                        (
                         l_request_id
                         ,g_current_batch_id
                        ,rec.concur_batch_id --c_uniq_batch_rec.batch_id
                        ,g_GSC_emp_grp
                        ,substr('Note: GL Interface Journals: CI US Interco with GSC -Complete for CARD.',1 ,150)
                        ,'Not Applicable because this is a direct transfer to GL within Oracle EBS'
                        ,l_created_by
                        ,l_creation_date
                        )
                        ;
                        --
                        commit;
                        --
                                -- setup the email body
                                setup_email_body 
                                 (
                                    p_text1 =>'BU CONCUR employee group : '||g_GSC_emp_grp
                                   ,p_emp_group =>g_GSC_emp_grp
                                   ,p_type =>'OTHERS'
                                 ); --g_notif_email_body is assigned here
                                --print_log('Email Body :'||g_notif_email_body);
                                --
                                g_email_subject :=g_instance||' '||g_GSC_emp_grp||' CONCUR JE Actuals Interface to Oracle GL - Status Update';
                                print_log('Email Subject :'||g_email_subject);                        
                                --
                                set_bu_sharepoint_email (p_emp_group =>g_GSC_emp_grp, p_type =>g_control_type_AA); --g_sharepoint_email is assigned here
                                print_log('SharePoint Email  :'||g_sharepoint_email);                        
                                --                          
                                -- email the teams about the extract
                                send_email_notif
                                   (
                                        p_email_from      =>g_notif_email_from,
                                        p_email_to           =>g_notif_email_to,
                                        p_email_cc           =>g_notif_email_cc,
                                        p_email_subject =>g_email_subject, 
                                        p_email_body     =>g_notif_email_body,
                                        p_mime_type      =>g_email_mime_type
                                   )
                                  ; 
                                print_log('After sending email notification about GSC JE actuals status');                                                                                                
                                --                            
                 exception
                  when others then
                   print_log('@Failed to log GSC Interco with self for emp_group '||g_GSC_emp_grp||', message =>'||sqlerrm);
                   rollback to start_here;
                 end;
                 -- End logging insert details for Interco clearing with GSC for CARD.
                 --                  
                 -- Begin CI US details file              
                 open c_GSC_je_details (p_concur_batch_id =>rec.concur_batch_id);
                 fetch c_GSC_je_details bulk collect into c_GSC_je_details_rec;
                 close c_GSC_je_details;
                 --
                 if c_GSC_je_details_rec.count >0 then 
                      --
                      open gsc_us_header;
                      fetch gsc_us_header into l_data_header; 
                      close gsc_us_header;
                      --
                             -- Create GSC detail file
                             --
                             if g_database =g_prd_database then   
                               g_file_prefix :=g_GSC_ORACLE; --Null; --Ver 1.1
                             else
                               g_file_prefix :=g_database||'_'||g_GSC_ORACLE;
                             end if;
                             --        
                             l_file :=g_file_prefix||'_BATCH_'||rec.concur_batch_id||'_DETAILS_'||to_char(g_batch_date,'mmddyyyy')||'.txt';
                             --
                             print_log('Before file open for GSC details file, concur_batch_id ='||rec.concur_batch_id);
                             --
                             l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
                             --
                             print_log ('Handle initiated for GSC details file : '||l_file);
                             --
                             -- Copy header record              
                             --                       
                             begin
                                  --                              
                                  l_line_buffer :=l_data_header;
                                  --
                                  utl_file.put_line ( l_utl_file_id, l_line_buffer);
                                  -- 
                                  l_line_count:= l_line_count+1; --number of lines written including header
                                  --                                         
                             exception
                                     when others then
                                       print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                       print_log('@ Inside file loop operation, error : '||sqlerrm);
                             end;              
                             --                       
                      for idx in 1 .. c_GSC_je_details_rec.count loop
                       -- ))))
                             --  
                                  --
                                  -- Copy GSC file detail records              
                                  --       
                                  begin 
                                         --
                                         for idx in 1 .. c_GSC_je_details_rec.count loop 
                                                      --
                                              l_line_buffer := 
                                                            c_GSC_je_details_rec(idx).fiscal_period_name
                                                    ||'|'||c_GSC_je_details_rec(idx).gl_product
                                                    ||'|'||c_GSC_je_details_rec(idx).gl_location
                                                    ||'|'||c_GSC_je_details_rec(idx).gl_cost_center
                                                    ||'|'||c_GSC_je_details_rec(idx).gl_account
                                                    ||'|'||c_GSC_je_details_rec(idx).gl_project
                                                    ||'|'||c_GSC_je_details_rec(idx).oop_or_card
                                                    ||'|'||c_GSC_je_details_rec(idx).journal_amount
                                                    ||'|'||c_GSC_je_details_rec(idx).rpt_key
                                                    ||'|'||c_GSC_je_details_rec(idx).line_number
                                                    ||'|'||c_GSC_je_details_rec(idx).emp_id
                                                    ||'|'||c_GSC_je_details_rec(idx).emp_last_name
                                                    ||'|'||c_GSC_je_details_rec(idx).emp_first_name
                                                    ||'|'||c_GSC_je_details_rec(idx).emp_group
                                                    ||'|'||c_GSC_je_details_rec(idx).emp_org_erp
                                                    ||'|'||c_GSC_je_details_rec(idx).emp_org_company
                                                    ||'|'||c_GSC_je_details_rec(idx).emp_org_branch
                                                    ||'|'||c_GSC_je_details_rec(idx).emp_org_dept
                                                    ||'|'||c_GSC_je_details_rec(idx).emp_org_project
                                                    ||'|'||c_GSC_je_details_rec(idx).ledger_code
                                                    ||'|'||c_GSC_je_details_rec(idx).emp_currency_alpha_code
                                                    ||'|'||c_GSC_je_details_rec(idx).rpt_entry_expense_type_name
                                                    ||'|'||c_GSC_je_details_rec(idx).rpt_entry_vendor_name
                                                    ||'|'||c_GSC_je_details_rec(idx).rpt_entry_vendor_desc
                                                    ||'|'||c_GSC_je_details_rec(idx).cc_trans_merchant_name
                                                    ||'|'||c_GSC_je_details_rec(idx).cc_trans_desc
                                                    ||'|'||c_GSC_je_details_rec(idx).rpt_entry_location_city_name
                                                    ||'|'||c_GSC_je_details_rec(idx).rpt_entry_trans_date
                                                    ||'|'||c_GSC_je_details_rec(idx).cc_trans_trans_id
                                                    ||'|'||c_GSC_je_details_rec(idx).rpt_entry_pmt_type_code
                                                    ||'|'||c_GSC_je_details_rec(idx).rpt_entry_pmt_code_name
                                                    ||'|'||c_GSC_je_details_rec(idx).journal_payer_pmt_type_name
                                                    ||'|'||c_GSC_je_details_rec(idx).journal_payee_pmt_type_name
                                                    ||'|'||c_GSC_je_details_rec(idx).journal_acct_code
                                                    ||'|'||c_GSC_je_details_rec(idx).journal_debit_or_credit
                                                    ||'|'||c_GSC_je_details_rec(idx).journal_amt
                                                    ||'|'||c_GSC_je_details_rec(idx).allocation_erp
                                                    ||'|'||c_GSC_je_details_rec(idx).allocation_company
                                                    ||'|'||c_GSC_je_details_rec(idx).allocation_branch
                                                    ||'|'||c_GSC_je_details_rec(idx).allocation_dept
                                                    ||'|'||c_GSC_je_details_rec(idx).allocation_project
                                                    ||'|'||c_GSC_je_details_rec(idx).rpt_vw_tot_personal_amt
                                                    ||'|'||c_GSC_je_details_rec(idx).rpt_entry_is_personal
                                                    ||'|'||c_GSC_je_details_rec(idx).rpt_entry_posted_amt
                                                    ||'|'||c_GSC_je_details_rec(idx).crt_btch_id
                                                    ||'|'||c_GSC_je_details_rec(idx).concur_batch_id
                                                    ||'|'||c_GSC_je_details_rec(idx).concur_batch_date
                                                    ||'|'||c_GSC_je_details_rec(idx).po_number --Ver 1.6                                                    
                                                    ||'|'
                                                  ;                  
                                              --
                                              utl_file.put_line ( l_utl_file_id, l_line_buffer);
                                              -- 
                                              l_line_count:= l_line_count+1; --number of lines written including header
                                              --                          
                                          --
                                         end loop;
                                         --
                                  exception                 
                                    when others then
                                     print_log('@ file-write : personal journals detail records, messge =>'||sqlerrm);
                                     raise program_error;
                                  end;   
                                  --                                                     
                       -- ))))
                      end loop;
                      --
                      utl_file.fclose ( l_utl_file_id );
                      --
                      -- 04/12/2017
                             -- Begin logging file details for GSC US detail file
                             begin 
                                    --
                                    insert into xxcus.xxcus_concur_ebs_bu_files
                                    (
                                     request_id
                                    ,concur_batch_id
                                    ,emp_group
                                    ,extracted_file_name
                                    ,extracted_file_folder
                                    ,created_by 
                                    ,creation_date
                                    ,crt_btch_id
                                    )
                                    values
                                    (
                                     l_request_id
                                    ,rec.concur_batch_id
                                    ,g_GSC_emp_grp
                                    ,l_file
                                    ,g_directory_path
                                    ,l_created_by
                                    ,l_creation_date
                                    ,g_current_batch_id
                                    )
                                    ;
                                    --
                                    commit;
                                    --
                                    -- setup the email body
                                    --setup_email_body (p_text1 =>'BU CONCUR employee group : '||g_WW_emp_grp); --g_notif_email_body is assigned here
                                    setup_email_body 
                                      (
                                         p_text1 =>'BU CONCUR employee group : '||g_GSC_emp_grp
                                        ,p_emp_group =>g_GSC_emp_grp
                                        ,p_type =>'ORACLE_SOURCED'
                                      ); --g_notif_email_body is assigned here                           
                                    --print_log('Email Body :'||g_notif_email_body);
                                    --
                                    g_email_subject :=g_instance||' '||g_GSC_emp_grp||' detail extracts to SharePoint - Status Update';
                                    print_log('Email Subject :'||g_email_subject);                        
                                    --
                                    set_bu_sharepoint_email (p_emp_group =>g_GSC_emp_grp, p_type =>g_control_type_AA); --g_sharepoint_email is assigned here
                                    print_log('SharePoint Email  :'||g_sharepoint_email);                        
                                    --
                                    -- push the extract to sharepoint
                                    print_log('Begin: Push GSC Concur SAE details to SharePoint.'); 
                                    --                       
                                    sharepoint_integration
                                       (
                                            p_emp_group =>g_GSC_emp_grp,
                                            p_file =>g_directory_path||'/'||l_file,
                                            p_sharepoint_email =>g_sharepoint_email,
                                            p_dummy_email_from =>g_notif_email_from,
                                            p_email_subject =>g_email_subject
                                       )
                                       ;
                                    --
                                    print_log('End: Push GSC Concur SAE details to SharePoint.');                           
                                    -- email the teams about the extract
                                    send_email_notif
                                       (
                                            p_email_from      =>g_notif_email_from,
                                            p_email_to           =>g_notif_email_to,
                                            p_email_cc           =>g_notif_email_cc,
                                            p_email_subject =>g_email_subject, 
                                            p_email_body     =>g_notif_email_body,
                                            p_mime_type      =>g_email_mime_type
                                       )
                                      ; 
                                    print_log('After sending email notification about GSC SAE detail file.');                                                                                                
                                    --                     
                             exception
                              when others then
                               print_log('@Failed to log file details for BU, @ emp_group '||g_GSC_emp_grp||', message =>'||sqlerrm);
                             end;
                             -- End logging file details for GSC US detail file                           
                      -- 04/12/2017
                      --
                 else
                  print_log('No CI US journals found, exiting without creating detail file.');
                 end if;
                 -- End GSC US details file
                 --      
       -- ### @ end of rec in c_uniq_concur_batches loop
     end loop;     
     --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    raise program_error;
  end GSC_US_GL_JE;  
  -- 
  procedure EXTRACT_PERSONAL_JOURNALS  IS
   --
    l_sub_routine varchar2(30) :='extract_personal_journals';
    l_line_count number :=0;
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;      
    --
    l_utl_file_id   UTL_FILE.FILE_TYPE;
    l_line_buffer VARCHAR2(30000):= Null;
    l_file VARCHAR2(150) :=Null;
    l_running_total number :=0;
    --
    -- Begin variables used for personal expenses
    l_perz_DB_acct     xxcus.xxcus_concur_gl_iface.segment4%type :='132105';
    l_perz_CR_acct         xxcus.xxcus_concur_gl_iface.segment4%type  :='211126'; 
    l_perz_desc  xxcus.xxcus_concur_gl_iface.reference10%type :='GSC receivable';    
    -- End variables used for personal expenses
    --
    -- Begin variables are used for personal expenses
    l_Location              xxcus.xxcus_concur_gl_iface.segment2%type :='Z0186';
    l_CAD_Location   xxcus.xxcus_concur_gl_iface.segment2%type :='Z0436'; --Ver 1.5    
    l_Cost_Ctr              xxcus.xxcus_concur_gl_iface.segment3%type :='0000';
    l_Project                 xxcus.xxcus_concur_gl_iface.segment5%type :='00000';    
    -- End variables are used for personal expenses
    --    
    --
    -- --Set global variable g_batch_date, g_concur_batch_id, g_fiscal_period, g_fiscal_start, g_fiscal_end
    cursor c_personal_journals is
    select 
           g_fiscal_period fiscal_period,
           g_fiscal_start fiscal_start_date,
           g_fiscal_end fiscal_end_date,
           emp_id,
           emp_last_name,
           emp_first_name,
           rpt_key,
           line_number,
           rpt_entry_expense_type_name,
           rpt_entry_posted_amt rpt_entry_posted_amt,
           rpt_vw_tot_personal_amt,
           rpt_entry_desc,
           journal_payer_pmt_type_name,
           journal_payee_pmt_type_name,
           rpt_vw_tot_due_cmpny,
           crt_btch_id,
           concur_batch_id,
           concur_batch_date,
           rpt_name,
           rpt_entry_trans_date,
           rpt_entry_vendor_name,
           rpt_entry_vendor_desc,
           cc_trans_billing_date,
           billed_cc_acct_number,
           cc_trans_ref_no,
           cc_trans_trans_id,
           cc_trans_posted_amt,
           cc_trans_desc,
           cc_trans_merchant_name,
           cc_trans_merchant_city,
           cc_trans_merchant_state,
           cc_trans_merchant_ctry_code,
           g_perz_status1 status,
           g_N gl_interfaced_flag,
           nvl(emp_org_unit1, rpt_custom1) emp_org_erp,
           nvl(emp_org_unit2, rpt_custom2) emp_org_company,
           nvl(emp_org_unit3, rpt_custom3) emp_org_branch,
           nvl(emp_org_unit4, rpt_custom4) emp_org_dept,
           nvl(emp_org_unit6, rpt_custom6) emp_org_project,
           g_run_id,
           rpt_entry_currency_alpha_code currency,
           case
             when ledger_ledger_code ='US' then g_us_ledger_id
             else g_cad_ledger_id 
           end ledger_id
      FROM xxcus.xxcus_src_concur_sae_n a
     WHERE     1 = 1
           AND a.crt_btch_id =g_current_batch_id
           AND a.concur_batch_id =g_concur_batch_id
           AND a.rpt_vw_tot_personal_amt <> g_zero
           AND a.rpt_entry_is_personal =g_Y
           AND a.journal_payer_pmt_type_name =g_payer_pmt_type
           AND a.rpt_entry_pmt_type_code IN ('IBCP', 'CBCP') --Only charges on company card
      ;
    -- 
    type c_personal_je_type is table of c_personal_journals%rowtype index by binary_integer;
    c_personal_je_rec c_personal_je_type;
    --
    cursor c_personal_exp_header is
    select 
                ledger_id
               ,currency     
               ,case 
                     when currency =g_Currency_USD then g_Interco_US_Product
                     when currency =g_Currency_CAD then g_Interco_CA_Product
                     else null
                end gl_prod
               ,case 
                     when currency =g_Currency_USD then l_Location
                     when currency =g_Currency_CAD then l_CAD_Location
                     else null             
                end gl_locn
    from   xxcus.xxcus_concur_personal_journals
    where 1 =1
          and crt_btch_id =g_current_batch_id
          and concur_batch_id =g_concur_batch_id 
          and gl_interfaced_flag =g_N    
     group by
                ledger_id
               ,currency
               ,case 
                     when currency =g_Currency_USD then g_Interco_US_Product
                     when currency =g_Currency_CAD then g_Interco_CA_Product
                     else null
                end
               ,case 
                     when currency =g_Currency_USD then g_Interco_US_Product
                     when currency =g_Currency_CAD then g_Interco_CA_Product
                     else null             
                end               
     ;        
    --
    cursor c_personal_header (p_ledger_id in number, p_currency in varchar2)  is
        select  
                    substr (
                                    'Emp :'||emp_id||', Name: '||emp_last_name||', '||emp_first_name||', Rpt Key :'||rpt_key
                                   ,1 
                                   ,240
                                  ) je_line_desc  
                  ,rpt_vw_tot_personal_amt personal_amt
                   ,rpt_key glintf_ref21_jel_ref1              
                   ,concur_batch_id glintf_ref22_jel_ref2
                   ,to_char(concur_batch_date, 'mm/dd/yyyy') glintf_ref23_jel_ref3
                   ,journal_payee_pmt_type_name glintf_ref24_jel_ref4
                   ,'Emp Org :'||emp_org_company glintf_ref25_jel_ref5                           
                   ,'Emp Branch :'||emp_org_branch glintf_ref26_jel_ref6       
        from   xxcus.xxcus_concur_personal_journals
        where 1 =1
              and crt_btch_id =g_current_batch_id
              and concur_batch_id =g_concur_batch_id 
              and gl_interfaced_flag =g_N
              and ledger_id =p_ledger_id
               and currency =p_currency            
        group by 
         substr (
                                    'Emp :'||emp_id||', Name: '||emp_last_name||', '||emp_first_name||', Rpt Key :'||rpt_key
                                   ,1 
                                   ,240
                                  )  
                   ,rpt_vw_tot_personal_amt 
                   ,rpt_key       --rpt_key              
                   ,concur_batch_id  --concur_batch_id   
                   ,to_char(concur_batch_date, 'mm/dd/yyyy')  --concur_batch_date
                   ,journal_payee_pmt_type_name  --journal_payer_pmt_type_name
                   ,'Emp Org :'||emp_org_company    
                   ,'Emp Branch :'||emp_org_branch                  
    ;   
    --
    type c_perz_header_type is table of c_personal_header%rowtype index by binary_integer;
    c_summary_rec c_perz_header_type;
    --   
  begin
     --
     g_outbound_loc :=GET_OUTBOUND_FILE_LOCATION ( v_operation =>'DBA_DIRECTORY', v_emp_group  =>g_GSC_ORACLE);
     --
     print_log ('Current working DBA directory is '||g_outbound_loc);
     --
     begin 
      --
      open c_personal_journals;
      fetch c_personal_journals bulk collect into c_personal_je_rec;
      close c_personal_journals;
      --
          if c_personal_je_rec.count >0 then
           --
           forall idx in 1 .. c_personal_je_rec.count 
           insert into xxcus.xxcus_concur_personal_journals values c_personal_je_rec(idx);
           --
           print_log('Personal expense lines inserted, total : '||sql%rowcount);
             --
             -- Create personal journals file
             --
             if g_database =g_prd_database then   
               g_file_prefix :=g_GSC_ORACLE; --Null; --Ver 1.1
             else
               g_file_prefix :=g_database||'_'||g_GSC_ORACLE;
             end if;
             --               
             l_file :=g_file_prefix||'_PERSONAL_EXP_BATCH_'||g_concur_batch_id||'_'||TO_CHAR(g_batch_date, 'MMDDYYYY')||'.txt';
             --
             print_log('Before file open for personal journals file, concur_batch_id ='||g_concur_batch_id);
             --
             l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
             --
             print_log ('Handle initiated for personal journals file : '||l_file);
             --
             -- Copy personal journals file header record              
             --                       
             begin
                  --                              
                  l_line_buffer := 
                          'CONCUR_BATCH_ID'
                      ||'|'
                      ||'CONCUR_BATCH_DATE'
                      ||'|'  
                      ||'RPT_KEY'    
                      ||'|'        
                      ||'EMP_ID'                                                      
                      ||'|'                    
                      ||'EMP_LAST_NAME'
                      ||'|'                    
                      ||'EMP_FIRST_NAME'
                      ||'|'                                                                         
                      ||'LINE_NUMBER'
                      ||'|'
                      ||'EMP_GROUP' --emp_org_erp
                      ||'|'
                      ||'RPT_NAME'
                      ||'|'                                       
                      ||'RPT_ENTRY_DESC'
                      ||'|'     
                      ||'RPT_ENTRY_VENDOR_NAME'
                      ||'|'     
                      ||'RPT_ENTRY_VENDOR_DESC'
                      ||'|'                                                                                                           
                      ||'RPT_ENTRY_EXPENSE_TYPE_NAME'
                      ||'|'           
                      ||'RPT_ENTRY_POSTED_AMT'
                      ||'|'
                      ||'RPT_VW_TOT_PERSONAL_AMT'
                      ||'|'
                      ||'CRT_BTCH_ID'
                      ||'|'
                      ;                  
                  --
                  utl_file.put_line ( l_utl_file_id, l_line_buffer);
                  -- 
                  l_line_count:= l_line_count+1; --number of lines written including header
                  --
                  --
                  -- Copy personal journals file detail records              
                  --       
                  begin 
                         --
                         for idx in 1 .. c_personal_je_rec.count loop 
                                      --
                              l_line_buffer := 
                                      c_personal_je_rec(idx).concur_batch_id
                                  ||'|'
                                  ||c_personal_je_rec(idx).concur_batch_date
                                  ||'|'  
                                  ||c_personal_je_rec(idx).rpt_key    
                                  ||'|'        
                                  ||c_personal_je_rec(idx).emp_id                                                      
                                  ||'|'                    
                                  ||c_personal_je_rec(idx).emp_last_name
                                  ||'|'                    
                                  ||c_personal_je_rec(idx).emp_first_name
                                  ||'|'                                                                          
                                  ||c_personal_je_rec(idx).line_number
                                  ||'|'
                                  ||c_personal_je_rec(idx).emp_org_erp
                                  ||'|'
                                  ||c_personal_je_rec(idx).rpt_name
                                  ||'|'                                       
                                  ||c_personal_je_rec(idx).rpt_entry_desc
                                  ||'|'     
                                  ||c_personal_je_rec(idx).rpt_entry_vendor_name
                                  ||'|'     
                                  ||c_personal_je_rec(idx).rpt_entry_vendor_desc
                                  ||'|'                                                                                                           
                                  ||c_personal_je_rec(idx).rpt_entry_expense_type_name
                                  ||'|'          
                                  ||c_personal_je_rec(idx).rpt_entry_posted_amt
                                  ||'|'
                                  ||c_personal_je_rec(idx).rpt_vw_tot_personal_amt
                                  ||'|'                                                  
                                  ||c_personal_je_rec(idx).crt_btch_id
                                  ||'|'
                                  ;                  
                              --
                              utl_file.put_line ( l_utl_file_id, l_line_buffer);
                              -- 
                              l_line_count:= l_line_count+1; --number of lines written including header
                              --                          
                          --
                         end loop;
                         --
                  exception                 
                    when others then
                     print_log('@ file-write : personal journals detail records, messge =>'||sqlerrm);
                     raise program_error;
                  end;   
                  --          
             exception
                     when others then
                       print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                       print_log('@ Inside file loop operation, error : '||sqlerrm);
             end;              
             --    
             utl_file.fclose ( l_utl_file_id );
             --         
          else
             print_log('No personal journals found.');
          end if;
          -- 04/12/17
          -- Add email to sharepoint integration here
         -- Begin logging file details for GSC US detail file
         begin 
                --
                insert into xxcus.xxcus_concur_ebs_bu_files
                (
                 request_id
                ,concur_batch_id
                ,emp_group
                ,extracted_file_name
                ,extracted_file_folder
                ,created_by 
                ,creation_date
                ,crt_btch_id
                )
                values
                (
                 l_request_id
                ,g_concur_batch_id
                ,g_GSC_emp_grp
                ,l_file
                ,g_directory_path
                ,l_created_by
                ,l_creation_date
                ,g_current_batch_id
                )
                ;
                --
                commit;
                --
                -- setup the email body
                --setup_email_body (p_text1 =>'BU CONCUR employee group : '||g_WW_emp_grp); --g_notif_email_body is assigned here
                setup_email_body 
                  (
                     p_text1 =>'BU CONCUR employee group : '||g_GSC_emp_grp
                    ,p_emp_group =>g_GSC_emp_grp
                    ,p_type =>'ALL_BU_PERSONAL_EXP'
                  ); --g_notif_email_body is assigned here                           
                --print_log('Email Body :'||g_notif_email_body);
                --
                g_email_subject :=g_instance||' '||g_GSC_emp_grp||' personal detail extracts to SharePoint - Status Update';
                print_log('Email Subject :'||g_email_subject);                        
                --
                set_bu_sharepoint_email (p_emp_group =>g_GSC_emp_grp, p_type =>g_control_type_PERSONAL); --g_sharepoint_email is assigned here
                print_log('SharePoint Email  :'||g_sharepoint_email);                        
                --
                -- push the extract to sharepoint
                print_log('Begin: Push GSC Concur PERSONAL SAE details to SharePoint.'); 
                --                       
                sharepoint_integration
                   (
                        p_emp_group =>g_GSC_emp_grp,
                        p_file =>g_directory_path||'/'||l_file,
                        p_sharepoint_email =>g_sharepoint_email,
                        p_dummy_email_from =>g_notif_email_from,
                        p_email_subject =>g_email_subject
                   )
                   ;
                --
                print_log('End: Push GSC Concur PERSONAL SAE details to SharePoint.');                           
                -- email the teams about the extract
                send_email_notif
                   (
                        p_email_from      =>g_notif_email_from,
                        p_email_to           =>g_notif_email_to,
                        p_email_cc           =>g_notif_email_cc,
                        p_email_subject =>g_email_subject, 
                        p_email_body     =>g_notif_email_body,
                        p_mime_type      =>g_email_mime_type
                   )
                  ; 
                print_log('After sending email notification about GSC PERSONAL SAE detail file.');                                                                                                
                --                     
         exception
          when others then
           print_log('@Failed to log file details for BU, @ emp_group '||g_GSC_emp_grp||', message =>'||sqlerrm);
         end;
         -- End logging file details for GSC US detail file           
          --
          delete xxcus.xxcus_concur_gl_iface
          where 1 =1
               and group_id =g_GSC_grp_id
               and status = g_je_iface_status
               and reference22 =g_concur_batch_id
               and attribute2 =g_event_type_PEREXP --sbala 
               and reference30 =g_perz_je_indicator
          ;
          --          
          for rec in c_personal_exp_header loop
                      open c_personal_header (rec.ledger_id, rec.currency);
                      --
                      fetch c_personal_header bulk collect into c_summary_rec;
                          --
                          if c_summary_rec.count >0 then 
                               --
                              print_log('@ ledger currency :'||rec.currency||', total records deleted with personal journals with status NEW and reference30 PERSONAL_JE : '||sql%rowcount);
                               --
                               l_line_count :=0;
                               g_concur_gl_intf_rec.delete;
                               l_running_total :=0;
                               --
                               for idx in 1 .. c_summary_rec.count loop
                                    --
                                    begin 
                                              -- Begin Debit journal
                                              l_line_count :=l_line_count +1;
                                              --                    
                                              -- Set set of books id
                                              g_concur_gl_intf_rec(l_line_count).set_of_books_id :=rec.ledger_id;
                                              -- Set accounting date
                                              g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                              -- Set currency          
                                              g_concur_gl_intf_rec(l_line_count).currency_code := rec.currency; --Set currency based on concur emp group
                                              -- Set Batch Name and description
                                              g_concur_gl_intf_rec(l_line_count).reference1 :='PE-Concur batch# '||c_summary_rec(idx).glintf_ref22_jel_ref2;                                                    
                                              g_concur_gl_intf_rec(l_line_count).reference2 :='Batch for all personal expenses';                          
                                              --
                                             --print_log('1-first pass');                              
                                              -- Set Journal entry name and description
                                                    g_concur_gl_intf_rec(l_line_count).reference4 :='PE-Concur JE for batch '||c_summary_rec(idx).glintf_ref22_jel_ref2;   
                                                    g_concur_gl_intf_rec(l_line_count).reference5 :='JE for all personal charges';
                                                    -- Set Journal entry line description                                                                            
                                                    g_concur_gl_intf_rec(l_line_count).reference10 :=c_summary_rec(idx).je_line_desc;                   
                                              --      
                                              g_concur_gl_intf_rec(l_line_count).group_id :=g_GSC_grp_id;
                                              --                             
                                              g_concur_gl_intf_rec(l_line_count).reference21 :=c_summary_rec(idx).glintf_ref21_jel_ref1; --rpt_key
                                              g_concur_gl_intf_rec(l_line_count).reference22 :=c_summary_rec(idx).glintf_ref22_jel_ref2; --concur_batch_id
                                              g_concur_gl_intf_rec(l_line_count).reference23 :=c_summary_rec(idx).glintf_ref23_jel_ref3; --concur_batch_date
                                              g_concur_gl_intf_rec(l_line_count).reference24 :=c_summary_rec(idx).glintf_ref24_jel_ref4; --journal_payee_pmt_type_name
                                              --
                                              g_concur_gl_intf_rec(l_line_count).reference25 :=c_summary_rec(idx).glintf_ref25_jel_ref5;  --emp_org_company
                                              g_concur_gl_intf_rec(l_line_count).reference26 :=c_summary_rec(idx).glintf_ref26_jel_ref6;  --emp_org_branch                     
                                              g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                              g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                              g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                              g_concur_gl_intf_rec(l_line_count).reference30 :=g_perz_je_indicator;                                                                          
                                              --                    
                                             --print_log('1-second pass');                              
                                              --Begin chart of accounts segments
                                              --
                                              -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                              g_concur_gl_intf_rec(l_line_count).segment1 :=rec.gl_prod; --g_Interco_US_Product; -- Ver 1.5 
                                              --
                                              g_concur_gl_intf_rec(l_line_count).segment2 :=rec.gl_locn; -- l_Location; -- Ver 1.5
                                              g_concur_gl_intf_rec(l_line_count).segment3 :=l_Cost_Ctr;
                                              g_concur_gl_intf_rec(l_line_count).segment4 :=l_perz_DB_acct;
                                              --
                                              g_concur_gl_intf_rec(l_line_count).segment5 :=l_Project;
                                              g_concur_gl_intf_rec(l_line_count).segment6 :=g_future_use_1;
                                              g_concur_gl_intf_rec(l_line_count).segment7 :=g_future_use_1;
                                              g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_PEREXP; 
                                              --
                                              --End chart of accounts segments
                                              -- Debit to account 132105    
                                              g_concur_gl_intf_rec(l_line_count).entered_dr :=c_summary_rec(idx).personal_amt;    
                                             --print_log('1-third pass');                                                    
                                         --
                                         -- End of debit journal 
                                         -- 
                                         l_running_total :=l_running_total +   c_summary_rec(idx).personal_amt;
                                         --
                                              IF idx =c_summary_rec.count then
                                                     -- Begin credit journal
                                                          --                                  
                                                          l_line_count :=l_line_count +1;
                                                          --                    
                                                          -- Set set of books id
                                                          g_concur_gl_intf_rec(l_line_count).set_of_books_id :=rec.ledger_id;
                                                          -- Set accounting date
                                                          g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; 
                                                          -- Set currency          
                                                          g_concur_gl_intf_rec(l_line_count).currency_code := rec.currency;
                                                          -- Set Batch Name and description
                                                          g_concur_gl_intf_rec(l_line_count).reference1 :='PE-Concur batch# '||c_summary_rec(idx).glintf_ref22_jel_ref2;                                                    
                                                          g_concur_gl_intf_rec(l_line_count).reference2 :='Batch for all personal expenses ('||rec.currency||')'; 
                                                        --print_log('2-first pass');                                                       
                                                          --
                                                          -- Set Journal entry name and description
                                                                g_concur_gl_intf_rec(l_line_count).reference4 :='PE-Concur JE for batch '||c_summary_rec(idx).glintf_ref22_jel_ref2;   
                                                                g_concur_gl_intf_rec(l_line_count).reference5 :='JE for all personal charges ('||rec.currency||')';
                                                                -- Set Journal entry line description                                                                            
                                                                g_concur_gl_intf_rec(l_line_count).reference10 :=c_summary_rec(idx).je_line_desc;
                                                          --      
                                                          g_concur_gl_intf_rec(l_line_count).group_id :=g_GSC_grp_id;
                                                          --                             
                                                          g_concur_gl_intf_rec(l_line_count).reference21 :=c_summary_rec(idx).glintf_ref21_jel_ref1;
                                                          g_concur_gl_intf_rec(l_line_count).reference22 :=c_summary_rec(idx).glintf_ref22_jel_ref2;
                                                          g_concur_gl_intf_rec(l_line_count).reference23 :=c_summary_rec(idx).glintf_ref23_jel_ref3;
                                                          g_concur_gl_intf_rec(l_line_count).reference24 :=c_summary_rec(idx).glintf_ref24_jel_ref4;
                                                          --
                                                          g_concur_gl_intf_rec(l_line_count).reference25 :=c_summary_rec(idx).glintf_ref25_jel_ref5;  
                                                          g_concur_gl_intf_rec(l_line_count).reference26 :=c_summary_rec(idx).glintf_ref26_jel_ref6;                          
                                                          g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                                          g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                                          g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                                          g_concur_gl_intf_rec(l_line_count).reference30 :=g_perz_je_indicator;            
                                                         --print_log('2-second pass');                                                                                            
                                                          --                    
                                                          --Begin chart of accounts segments
                                                          --
                                                          -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                                          g_concur_gl_intf_rec(l_line_count).segment1 :=rec.gl_prod; -- g_Interco_US_Product; -- Ver 1.5 
                                                          --
                                                          g_concur_gl_intf_rec(l_line_count).segment2 :=rec.gl_locn; -- l_Location; -- Ver 1.5
                                                          g_concur_gl_intf_rec(l_line_count).segment3 :=l_Cost_Ctr;
                                                          g_concur_gl_intf_rec(l_line_count).segment4 :=l_perz_CR_acct;
                                                          --
                                                          g_concur_gl_intf_rec(l_line_count).segment5 :=l_Project;
                                                          g_concur_gl_intf_rec(l_line_count).segment6 :=g_future_use_1;
                                                          g_concur_gl_intf_rec(l_line_count).segment7 :=g_future_use_1;
                                                          g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_PEREXP;                              
                                                          --
                                                          --End chart of accounts segments
                                                          -- Debit to account 132105    
                                                          g_concur_gl_intf_rec(l_line_count).entered_cr :=l_running_total;   
                                                         --print_log('2-third pass, g_concur_gl_intf_rec(l_line_count).entered_cr ='||g_concur_gl_intf_rec(l_line_count).entered_cr||', g_concur_gl_intf_rec(l_line_count).entered_dr ='||g_concur_gl_intf_rec(l_line_count-1).entered_dr);                                                     
                                                          --                         
                                                     --End of credit journal                                   
                                                        --  if l_card_interco_clearing.EXISTS(1) = FALSE then
                                                        -- 
                                                        -- l_card_interco_clearing(1) :=c_summary_rec(idx); --need just one record of type CARD if found
                                                        -- 
                                                        -- end if;   
                                              ELSE
                                                      Null;                             
                                              END IF;
                                    exception
                                         when others then
                                            print_log('@Setup GSC Receivable and Liability, error =>'||sqlerrm||', line :'||l_line_count);
                                    end;
                                    --
                                    --
                               end loop;
                               --
                              begin
                                 populate_concur_gl_iface 
                                    ( 
                                       p_concur_gl_intf_rec =>g_concur_gl_intf_rec
                                      ,p_group_id                   =>g_GSC_grp_id
                                    );
                              exception
                               when others then
                                print_log('@populate_concur_gl_iface, calling block error : '||sqlerrm);
                                raise program_error;
                              end;
                              --               
                          else
                           print_log('No personal header records found.');
                          end if;
                          --
                      close c_personal_header;
                      --
          end loop; -- @ for rec in c_personal_exp_header loop
     exception
      when others then
       print_log(''||sqlerrm);
       raise program_error;
     end;
     --       
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    raise program_error;
  end EXTRACT_PERSONAL_JOURNALS;  
  --    
  procedure EXTRACT_CARD_DETAILS  IS
   --
    l_sub_routine varchar2(30) :='extract_card_details';
    l_line_count number :=0;
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;      
    --
    l_utl_file_id   UTL_FILE.FILE_TYPE;
    l_line_buffer VARCHAR2(30000):= Null;
    l_file VARCHAR2(150) :=Null;
    --
    -- 
    cursor c_card_journals is
    select 
           g_fiscal_period fiscal_period,
           emp_custom21 employee_erp,           
           emp_id,
           emp_last_name,
           emp_first_name,
           rpt_key,
           line_number,
           rpt_entry_pmt_type_name,--new
           rpt_entry_expense_type_name,
           rpt_entry_is_personal, --new           
           rpt_entry_posted_amt, 
           rpt_vw_tot_personal_amt,
           rpt_entry_desc,
           nvl(allocation_custom1, allocation_custom7) je_allocation_erp, 
           nvl(allocation_custom2, allocation_custom8) je_allocation_company, 
           nvl(allocation_custom3, allocation_custom9) je_allocation_branch, 
           nvl(allocation_custom4, allocation_custom10) je_allocation_dept, 
           nvl(allocation_custom11, allocation_custom12) je_allocation_project,        
           journal_acct_code,
           journal_debit_or_credit,    
           journal_amt journal_amount, 
           journal_payer_pmt_type_name,
           journal_payee_pmt_type_name,
           rpt_vw_tot_due_cmpny,
           crt_btch_id,
           concur_batch_id,
           to_char(concur_batch_date, 'mm/dd/yyyy') concur_batch_date,
           rpt_name,
           to_char(rpt_entry_trans_date, 'mm/dd/yyyy') rpt_entry_trans_date,           
           rpt_entry_vendor_name,
           rpt_entry_vendor_desc,
           to_char(cc_trans_billing_date, 'mm/dd/yyyy') cc_trans_billing_date,           
           billed_cc_acct_number,
           cc_trans_ref_no,
           cc_trans_trans_id,
           cc_trans_posted_amt,
           cc_trans_desc,
           cc_trans_merchant_name,
           cc_trans_merchant_city,
           cc_trans_merchant_state,
           cc_trans_merchant_ctry_code,
           nvl(emp_org_unit1, rpt_custom1) emp_org_erp,
           nvl(emp_org_unit2, rpt_custom2) emp_org_company,
           nvl(emp_org_unit3, rpt_custom3) emp_org_branch,
           nvl(emp_org_unit4, rpt_custom4) emp_org_dept,
           nvl(emp_org_unit6, rpt_custom6) emp_org_project     
      FROM xxcus.xxcus_src_concur_sae_n a
     WHERE     1 = 1
           AND a.crt_btch_id =g_current_batch_id
           AND a.concur_batch_id =g_concur_batch_id
           --AND a.journal_amt<> g_zero
           --AND a.journal_payer_pmt_type_name =g_payer_pmt_type
           AND a.rpt_entry_pmt_type_code IN ('IBCP', 'CBCP') -- Only charges on company card
      ;
    -- 
    type c_card_journals_type is table of c_card_journals%rowtype index by binary_integer;
    c_card_journals_rec c_card_journals_type;
    --
  begin
     --
     g_outbound_loc :=GET_OUTBOUND_FILE_LOCATION ( v_operation =>'DBA_DIRECTORY', v_emp_group  =>g_GSC_ORACLE);
     --
     print_log ('Current working DBA directory is '||g_outbound_loc);
     --
     begin 
      --
      open c_card_journals;
      fetch c_card_journals bulk collect into c_card_journals_rec limit xxcus_concur_pkg.g_limit;
      close c_card_journals;
      --
          if c_card_journals_rec.count >0 then
           --
           print_log('CARD expense lines fetched for all BU, total : '||c_card_journals_rec.count);
             --
             -- Create CARD details file
             --
             if g_database =g_prd_database then   
               g_file_prefix :=g_GSC_ORACLE; --Null; -- Ver 1.1
             else
               g_file_prefix :=g_database||'_'||g_GSC_ORACLE;
             end if;
             --               
             l_file :=g_file_prefix||'_CONCUR_BATCH_'||g_concur_batch_id||'_CARD_'||TO_CHAR(g_batch_date, 'MMDDYYYY')||'.txt';
             --
             print_log('Before file open for CARD details file, concur_batch_id ='||g_concur_batch_id);
             --
             l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
             --
             print_log ('Handle initiated for Concur CARD  details file : '||l_file);
             --
             -- Copy CARD details file header record              
             --                       
             begin
                  --                              
                  l_line_buffer := 
                          'CONCUR_BATCH_ID'
                      ||'|'
                      ||'CONCUR_BATCH_DATE'
                      ||'|'
                      ||'FISCAL_PERIOD'
                      ||'|'  
                      ||'RPT_ENTRY_PMT_TYPE_NAME'
                      ||'|'                                                          
                      ||'EMP_ID'                                                      
                      ||'|'                    
                      ||'EMP_LAST_NAME'
                      ||'|'                    
                      ||'EMP_FIRST_NAME'
                      ||'|'
                      ||'RPT_KEY'    
                      ||'|'                                                                                              
                      ||'LINE_NUMBER'
                      ||'|'       
                      ||'RPT_ENTRY_EXPENSE_TYPE_NAME'
                      ||'|'                           
                      ||'EMPLOYEE_ERP'
                      ||'|'
                      ||'EMP_ORG_ERP'
                      ||'|'
                      ||'EMP_ORG_COMPANY'
                      ||'|'
                      ||'EMP_ORG_BRANCH'
                      ||'|'
                      ||'EMP_ORG_DEPT'
                      ||'|'  
                      ||'EMP_ORG_PROJECT'
                      ||'|'   
                      ||'JOURNAL_AMOUNT'
                      ||'|'
                      ||'JOURNAL_ACCT_CODE'
                      ||'|'     
                      ||'JOURNAL_DEBIT_OR_CREDIT'
                      ||'|'     
                      ||'JOURNAL_PAYER_PMT_TYPE_NAME'
                      ||'|'                
                      ||'JOURNAL_PAYEE_PMT_TYPE_NAME'
                      ||'|' 
                      ||'JE_ALLOCATION_ERP'
                      ||'|'         
                      ||'JE_ALLOCATION_COMPANY'
                      ||'|'     
                      ||'JE_ALLOCATION_BRANCH'
                      ||'|'     
                      ||'JE_ALLOCATION_DEPT'
                      ||'|'     
                      ||'JE_ALLOCATION_PROJECT'
                      ||'|'                                                                                                                                                                                                                                                             
                      ||'RPT_NAME'
                      ||'|'      
                      ||'RPT_ENTRY_TRANS_DATE'
                      ||'|'                         
                      ||'RPT_ENTRY_IS_PERSONAL'
                      ||'|'                        
                      ||'RPT_ENTRY_POSTED_AMOUNT'
                      ||'|'
                      ||'RPT_VW_TOT_PERSONAL_AMT'
                      ||'|'                                                                                                              
                      ||'RPT_ENTRY_DESC'
                      ||'|'     
                      ||'RPT_ENTRY_VENDOR_NAME'
                      ||'|'     
                      ||'RPT_ENTRY_VENDOR_DESC'
                      ||'|'       
                      ||'BILLED_CC_ACCT_NUMBER'
                      ||'|'
                      ||'CC_TRANS_REF_NO'
                      ||'|'
                      ||'CC_TRANS_TRANS_ID'
                      ||'|'
                      ||'CC_TRANS_POSTED_AMT'
                      ||'|'
                      ||'CC_TRANS_DESC'
                      ||'|'
                      ||'CC_TRANS_MERCHANT_NAME'
                      ||'|'
                      ||'CC_TRANS_MERCHANT_CITY'
                      ||'|'
                      ||'CC_TRANS_MERCHANT_STATE'
                      ||'|'
                      ||'CC_TRANS_MERCHANT_CTRY_CODE'
                      ||'|'                                                                                                                                                                                                
                      ||'CRT_BTCH_ID'
                      ||'|'
                      ;                  
                  --
                  utl_file.put_line ( l_utl_file_id, l_line_buffer);
                  -- 
                  l_line_count:= l_line_count+1; --number of lines written including header
                  --
                  --
                  -- Copy CARD detail records              
                  --       
                  begin 
                         --
                         for idx in 1 .. c_card_journals_rec.count loop 
                                      --
                                      l_line_buffer := 
                                              c_card_journals_rec(idx).concur_batch_id
                                          ||'|'
                                          ||c_card_journals_rec(idx).concur_batch_date
                                          ||'|'
                                          ||c_card_journals_rec(idx).fiscal_period
                                          ||'|'  
                                          ||c_card_journals_rec(idx).rpt_entry_pmt_type_name
                                          ||'|'                                                          
                                          ||c_card_journals_rec(idx).emp_id                                                      
                                          ||'|'                    
                                          ||c_card_journals_rec(idx).emp_last_name
                                          ||'|'                    
                                          ||c_card_journals_rec(idx).emp_first_name
                                          ||'|'
                                          ||c_card_journals_rec(idx).rpt_key    
                                          ||'|'                                                                                              
                                          ||c_card_journals_rec(idx).line_number
                                          ||'|'       
                                          ||c_card_journals_rec(idx).rpt_entry_expense_type_name
                                          ||'|'                           
                                          ||c_card_journals_rec(idx).employee_erp
                                          ||'|'
                                          ||c_card_journals_rec(idx).emp_org_erp
                                          ||'|'
                                          ||c_card_journals_rec(idx).emp_org_company
                                          ||'|'
                                          ||c_card_journals_rec(idx).emp_org_branch
                                          ||'|'
                                          ||c_card_journals_rec(idx).emp_org_dept
                                          ||'|'  
                                          ||c_card_journals_rec(idx).emp_org_project
                                          ||'|'   
                                          ||c_card_journals_rec(idx).journal_amount
                                          ||'|'
                                          ||c_card_journals_rec(idx).journal_acct_code
                                          ||'|'     
                                          ||c_card_journals_rec(idx).journal_debit_or_credit
                                          ||'|'     
                                          ||c_card_journals_rec(idx).journal_payer_pmt_type_name
                                          ||'|'                
                                          ||c_card_journals_rec(idx).journal_payee_pmt_type_name
                                          ||'|' 
                                          ||c_card_journals_rec(idx).je_allocation_erp
                                          ||'|'         
                                          ||c_card_journals_rec(idx).je_allocation_company
                                          ||'|'     
                                          ||c_card_journals_rec(idx).je_allocation_branch
                                          ||'|'     
                                          ||c_card_journals_rec(idx).je_allocation_dept
                                          ||'|'     
                                          ||c_card_journals_rec(idx).je_allocation_project
                                          ||'|'                                                                                                                                                                                                                                                             
                                          ||c_card_journals_rec(idx).rpt_name
                                          ||'|'      
                                          ||c_card_journals_rec(idx).rpt_entry_trans_date
                                          ||'|'                         
                                          ||c_card_journals_rec(idx).rpt_entry_is_personal
                                          ||'|'                        
                                          ||c_card_journals_rec(idx).rpt_entry_posted_amt
                                          ||'|'
                                          ||c_card_journals_rec(idx).rpt_vw_tot_personal_amt
                                          ||'|'                                                                                                              
                                          ||c_card_journals_rec(idx).rpt_entry_desc
                                          ||'|'     
                                          ||c_card_journals_rec(idx).rpt_entry_vendor_name
                                          ||'|'     
                                          ||c_card_journals_rec(idx).rpt_entry_vendor_desc
                                          ||'|'       
                                          ||c_card_journals_rec(idx).billed_cc_acct_number
                                          ||'|'
                                          ||c_card_journals_rec(idx).cc_trans_ref_no
                                          ||'|'
                                          ||c_card_journals_rec(idx).cc_trans_trans_id
                                          ||'|'
                                          ||c_card_journals_rec(idx).cc_trans_posted_amt
                                          ||'|'
                                          ||c_card_journals_rec(idx).cc_trans_desc
                                          ||'|'
                                          ||c_card_journals_rec(idx).cc_trans_merchant_name
                                          ||'|'
                                          ||c_card_journals_rec(idx).cc_trans_merchant_city
                                          ||'|'
                                          ||c_card_journals_rec(idx).cc_trans_merchant_state
                                          ||'|'
                                          ||c_card_journals_rec(idx).cc_trans_merchant_ctry_code
                                          ||'|'                                                                                                                                                                                                
                                          ||c_card_journals_rec(idx).crt_btch_id
                                          ||'|'
                                  ;                  
                              --
                              utl_file.put_line ( l_utl_file_id, l_line_buffer);
                              -- 
                              l_line_count:= l_line_count+1; --number of lines written including header
                              --                          
                          --
                         end loop;
                         --
                  exception                 
                    when others then
                     print_log('@ file-write : personal journals detail records, messge =>'||sqlerrm);
                     raise program_error;
                  end;   
                  --          
             exception
                     when others then
                       print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                       print_log('@ Inside file loop operation, error : '||sqlerrm);
             end;              
             --    
             utl_file.fclose ( l_utl_file_id );
             --         
          else
             print_log('No CARD SAE detail records found for crt_btch_id ='||g_current_batch_id||', concur_batch_id ='||g_concur_batch_id);
          end if;
          --
          -- Add email to sharepoint integration here
         -- Begin logging file details for CARD details file for all Concur ERP's
         begin 
                --
                insert into xxcus.xxcus_concur_ebs_bu_files
                (
                 request_id
                ,concur_batch_id
                ,emp_group
                ,extracted_file_name
                ,extracted_file_folder
                ,created_by 
                ,creation_date
                ,crt_btch_id
                )
                values
                (
                 l_request_id
                ,g_concur_batch_id
                ,g_GSC_emp_grp
                ,l_file
                ,g_directory_path
                ,l_created_by
                ,l_creation_date
                ,g_current_batch_id
                )
                ;
                --
                commit;
                --
                -- setup the email body
                --setup_email_body (p_text1 =>'BU CONCUR employee group : '||g_WW_emp_grp); --g_notif_email_body is assigned here
                setup_email_body 
                  (
                     p_text1 =>'BU CONCUR employee group : '||g_GSC_emp_grp
                    ,p_emp_group =>g_GSC_emp_grp
                    ,p_type =>'ALL_BU_CARD_EXP'
                  ); --g_notif_email_body is assigned here                           
                --print_log('Email Body :'||g_notif_email_body);
                --
                g_email_subject :=g_instance||' '||g_GSC_emp_grp||' CARD detail extracts to SharePoint - Status Update';
                print_log('Email Subject :'||g_email_subject);                        
                --
                set_bu_sharepoint_email (p_emp_group =>g_GSC_emp_grp, p_type =>g_control_type_CARD); --g_sharepoint_email is assigned here
                print_log('SharePoint Email  :'||g_sharepoint_email);                        
                --
                -- push the extract to sharepoint
                print_log('Begin: Push GSC Concur CARD SAE details to SharePoint.'); 
                --                       
                sharepoint_integration
                   (
                        p_emp_group =>g_GSC_emp_grp,
                        p_file =>g_directory_path||'/'||l_file,
                        p_sharepoint_email =>g_sharepoint_email,
                        p_dummy_email_from =>g_notif_email_from,
                        p_email_subject =>g_email_subject
                   )
                   ;
                --
                print_log('End: Push GSC Concur CARD SAE details to SharePoint.');                           
                -- email the teams about the extract
                send_email_notif
                   (
                        p_email_from      =>g_notif_email_from,
                        p_email_to           =>g_notif_email_to,
                        p_email_cc           =>g_notif_email_cc,
                        p_email_subject =>g_email_subject, 
                        p_email_body     =>g_notif_email_body,
                        p_mime_type      =>g_email_mime_type
                   )
                  ; 
                print_log('After sending email notification about GSC CARD SAE detail file.');                                                                                                
                --                     
         exception
          when others then
           print_log('@Failed to log file details for BU, @ emp_group '||g_GSC_emp_grp||', message =>'||sqlerrm);
         end;
         -- End logging file details for CARD details file for all Concur ERP's           

     exception
      when others then
       print_log(''||sqlerrm);
       raise program_error;
     end;
     --       
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    raise program_error;
  end EXTRACT_CARD_DETAILS;  
  --    
  procedure EXTRACT_INACTIVE_BU  IS
   --
    l_sub_routine varchar2(30) :='extract_inactive_bu';
    l_line_count number :=0;
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;      
    --
    l_utl_file_id   UTL_FILE.FILE_TYPE;
    l_line_buffer VARCHAR2(30000):= Null;
    l_file VARCHAR2(150) :=Null;
    --
    cursor c_uniq_concur_batches is
        select concur_batch_id
                   ,to_char(concur_batch_date, 'mm/dd/yyyy') concur_batch_date
                   ,nvl(allocation_custom1, allocation_custom7) allocation_erp 
                   ,replace(nvl(allocation_custom1, allocation_custom7),' ','_') transformed_erp
                   ,sum(journal_amt) total_journal_amt
                   ,count(concur_batch_date) total_journal_lines                                                                  
        from xxcus.xxcus_src_concur_sae_n
        where 1 =1
            and crt_btch_id =g_current_batch_id
            and exists
             (
                  select '1'
                  from    fnd_lookup_values
                  where 1 =1
                        and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
                        and nvl(enabled_flag, 'N') ='N' --Pull only inactive business units that are pending implementation
                        and lookup_code =nvl(allocation_custom1, allocation_custom7)
             )            
        group by concur_batch_id 
                         ,to_char(concur_batch_date, 'mm/dd/yyyy')
                         ,nvl(allocation_custom1, allocation_custom7)
                         ,replace(nvl(allocation_custom1, allocation_custom7),' ','_')
        order by concur_batch_id asc  
        ;  
    --
    cursor c_inactive_bu (p_concur_batch_id in number, p_alloc_erp in varchar2) is
    select 
                 concur_batch_id
                ,to_char(concur_batch_date,'mm/dd/yyyy') concur_batch_date
                ,rpt_id
                ,rpt_key
                ,rpt_entry_key
                ,journal_rpj_key
                ,allocation_key
                ,line_number
                ,emp_id
                ,emp_last_name
                ,emp_first_name
                ,emp_mi
                ,emp_custom21 emp_group
                ,ledger_ledger_code ledger_code
                ,emp_currency_alpha_code
                ,to_char(rpt_submit_date,'mm/dd/yyyy') rpt_submit_date                
                ,to_char(rpt_user_defined_date,'mm/dd/yyyy') rpt_user_defined_date                
                ,to_char(rpt_pmt_processing_date,'mm/dd/yyyy') rpt_pmt_processing_date
                ,rpt_name
                ,rpt_entry_trans_type
                ,rpt_entry_expense_type_name
                ,rpt_entry_vendor_name
                ,rpt_entry_vendor_desc
                ,cc_trans_merchant_name                      merchant_name
                ,cc_trans_desc                                             merchant_description
                ,rpt_entry_location_city_name              rpt_entry_location_city
                ,cc_trans_merchant_city                          transaction_city
                ,to_char(rpt_entry_trans_date,'mm/dd/yyyy') rpt_entry_trans_date                
                ,to_char(cc_trans_trans_date,'mm/dd/yyyy') cc_trans_trans_date
                ,cc_trans_trans_id                                     cc_trans_trans_id
                ,cc_trans_merchant_state                      transaction_state
                ,rpt_entry_from_location
                ,rpt_entry_to_location
                ,car_log_entry_business_dist
                ,rpt_entry_ctry_sub_code
                ,rpt_entry_pmt_type_code
                ,rpt_entry_pmt_code_name
                ,journal_payer_pmt_type_name
                ,journal_payer_pmt_code_name
                ,journal_payee_pmt_type_name
                ,journal_payee_pmt_code_name
                ,journal_acct_code
                ,journal_debit_or_credit
                ,journal_amt
                ,allocation_pct
                ,nvl(allocation_custom1, allocation_custom7)allocation_erp
                ,nvl(allocation_custom2, allocation_custom8) allocation_company
                ,nvl(allocation_custom3, allocation_custom9) allocation_branch
                ,nvl(allocation_custom4, allocation_custom10) allocation_dept
                ,nvl(allocation_custom11, allocation_custom12) allocation_project
                ,crt_btch_id                                          
        from xxcus.xxcus_src_concur_sae_n
        where 1 =1
            and crt_btch_id =g_current_batch_id
            and concur_batch_id =p_concur_batch_id --Cursor parameter 
            and nvl(allocation_custom1, allocation_custom7) =p_alloc_erp                   
        order by rpt_key asc, line_number asc
                   ;    
    --  
    type c_inactive_bu_journals_type is table of c_inactive_bu%rowtype index by binary_integer;
    c_inactive_journals_rec  c_inactive_bu_journals_type; 
    -- 
    --     
  begin --All inactive files will be dropped into the Concur GSC ORACLE sharepoint folder
       -- 
       g_outbound_loc :=GET_OUTBOUND_FILE_LOCATION ( v_operation =>'DBA_DIRECTORY', v_emp_group  =>g_GSC_ORACLE);
       --
       print_log ('Current working DBA directory is '||g_outbound_loc);
       --       
     for rec in c_uniq_concur_batches loop
       --
       -- ### @ begin rec in c_uniq_concur_batches loop
       --         
       g_inactive_count   :=to_char(rec.total_journal_lines, 'FM999,990');
       g_inactive_dollars :=to_char(rec.total_journal_amt, 'FM999,999,990.90');
       --
                 --                        
                 open c_inactive_bu (p_concur_batch_id =>rec.concur_batch_id, p_alloc_erp =>rec.allocation_erp);
                 --
                 loop
                  --
                  fetch c_inactive_bu bulk collect into c_inactive_journals_rec;
                  --
                  print_log('Inactive ERP group allocated journals : '||c_inactive_journals_rec.count);
                  --
                  exit when c_inactive_journals_rec.count =0;
                  --
                      if c_inactive_journals_rec.count >0 then
                         --
                         if g_database =g_prd_database then   
                           g_file_prefix :=rec.transformed_erp||'_';
                         else
                           g_file_prefix :=g_database||'_'||rec.transformed_erp||'_';
                         end if;                    
                         --
                         l_file :=g_file_prefix||'CONCUR_INACTIVE_BU_BATCH_'||rec.concur_batch_id||'_'||TO_CHAR(g_batch_date, 'MMDDYYYY')||'.txt';
                         --
                         print_log('Before file open for inactive business unit journals, @ allocation_erp  ='||rec.allocation_erp||', concur_batch_id ='||rec.concur_batch_id);
                         --
                         l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
                         --
                         print_log ('File handle initiated for inactive business unit journals : '||l_file);
                         -- 
                         -- Begin copy the WW JE actuals header record to the file              
                         --
                         begin
                              --                              
                              l_line_buffer := 
                                      'CONCUR_BATCH_ID'
                                  ||'|'
                                  ||'CONCUR_BATCH_DATE'
                                  ||'|'  
                                  ||'RPT_KEY'    
                                  ||'|'        
                                  ||'EMP_ID'                                                      
                                  ||'|'                    
                                  ||'EMP_LAST_NAME'
                                  ||'|'                    
                                  ||'EMP_FIRST_NAME'
                                  ||'|'                    
                                  ||'EMP_MI'
                                  ||'|'                                                                             
                                  ||'LINE_NUMBER'
                                  ||'|'
                                  ||'EMP_GROUP'
                                  ||'|'            
                                  ||'RPT_SUBMIT_DATE'
                                  ||'|'
                                  ||'RPT_ENTRY_EXPENSE_TYPE_NAME'
                                  ||'|'           
                                  ||'RPT_ENTRY_PMT_CODE_NAME'
                                  ||'|'
                                  ||'JOURNAL_ACCT_CODE'
                                  ||'|'           
                                  ||'JOURNAL_AMT'
                                  ||'|'
                                  ||'ALLOCATION_ERP'
                                  ||'|'           
                                  ||'ALLOCATION_COMPANY'
                                  ||'|'
                                  ||'ALLOCATION_BRANCH'
                                  ||'|'                                  
                                  ||'ALLOCATION_DEPT'
                                  ||'|'   
                                  ||'ALLOCATION_PROJECT'
                                  ||'|'
                                  ||'CRT_BTCH_ID'
                                  ||'|'
                                  ;                  
                              --
                              utl_file.put_line ( l_utl_file_id, l_line_buffer);
                              -- 
                              l_line_count:= l_line_count+1; --number of lines written including header
                              --
                         exception
                                 when no_data_found then
                                   exit;
                                 when others then
                                   print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                   print_log('@ Inside file loop operation, error : '||sqlerrm);
                                   exit;
                         end;              
                         --
                         --End copy the inactive business unit journals record to the file              
                         --
                         l_line_buffer :=Null;
                         --
                         --Begin copy inactive business unit journals records to the file              
                         --
                         for idx in 1 .. c_inactive_journals_rec.count loop 
                           --
                           l_line_buffer :=
                                      c_inactive_journals_rec(idx).concur_batch_id
                                  ||'|'
                                  ||c_inactive_journals_rec(idx).concur_batch_date
                                  ||'|'  
                                  ||c_inactive_journals_rec(idx).rpt_key   
                                  ||'|'        
                                  ||c_inactive_journals_rec(idx).emp_id                                                     
                                  ||'|'                    
                                  ||c_inactive_journals_rec(idx).emp_last_name
                                  ||'|'                    
                                  ||c_inactive_journals_rec(idx).emp_first_name
                                  ||'|'                    
                                  ||c_inactive_journals_rec(idx).emp_mi
                                  ||'|'                                                                             
                                  ||c_inactive_journals_rec(idx).line_number
                                  ||'|'
                                  ||c_inactive_journals_rec(idx).emp_group
                                  ||'|'            
                                  ||c_inactive_journals_rec(idx).rpt_submit_date
                                  ||'|'
                                  ||c_inactive_journals_rec(idx).rpt_entry_expense_type_name
                                  ||'|'           
                                  ||c_inactive_journals_rec(idx).rpt_entry_pmt_code_name
                                  ||'|'
                                  ||c_inactive_journals_rec(idx).journal_acct_code
                                  ||'|'           
                                  ||c_inactive_journals_rec(idx).journal_amt
                                  ||'|'
                                  ||c_inactive_journals_rec(idx).allocation_erp
                                  ||'|'           
                                  ||c_inactive_journals_rec(idx).allocation_company
                                  ||'|'
                                  ||c_inactive_journals_rec(idx).allocation_branch
                                  ||'|'                                  
                                  ||c_inactive_journals_rec(idx).allocation_dept
                                  ||'|'   
                                  ||c_inactive_journals_rec(idx).allocation_project
                                  ||'|'                                  
                                  ||c_inactive_journals_rec(idx).crt_btch_id
                                  ||'|'
                                  ;                
                               --
                               begin
                                  --
                                  utl_file.put_line ( l_utl_file_id, l_line_buffer);
                                  -- 
                                  l_line_count:= l_line_count+1; --number of lines written including header
                                  --
                               exception
                                     when no_data_found then
                                       exit;
                                     when others then
                                       print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                       print_log('@ Inside file loop operation, error : '||sqlerrm);
                                       exit;
                               end;                
                               --
                         end loop;
                         --
                         --End copy inactive business unit journals records to the file              
                         --
                             print_log('Total records written into the file [Header and Lines] , l_line_count =>'||l_line_count);
                             --
                             -- Close file handle 
                             --
                             utl_file.fclose ( l_utl_file_id );
                             print_log ('After close for inactive business unit journals file : '||l_file);
                             -- 
                             -- Begin logging file details for each BU along with their concur batch id
                             begin 
                                    --
                                    insert into xxcus.xxcus_concur_ebs_bu_files
                                    (
                                     request_id
                                    ,concur_batch_id
                                    ,emp_group
                                    ,extracted_file_name
                                    ,extracted_file_folder
                                    ,created_by 
                                    ,creation_date
                                    ,crt_btch_id
                                    )
                                    values
                                    (
                                     l_request_id
                                    ,rec.concur_batch_id --c_uniq_batch_rec.batch_id
                                    ,rec.allocation_erp
                                    ,l_file
                                    ,g_directory_path
                                    ,l_created_by
                                    ,l_creation_date
                                    ,g_current_batch_id
                                    )
                                    ;
                                    --
                                    commit;
                                    --
                                    -- setup the email body
                                    --setup_email_body (p_text1 =>'BU CONCUR employee group : '||g_WW_emp_grp); --g_notif_email_body is assigned here
                                    setup_email_body 
                                      (
                                         p_text1 =>'BU CONCUR employee group : '||rec.allocation_erp
                                        ,p_emp_group =>rec.allocation_erp
                                        ,p_type =>'INACTIVE_BU'
                                      ); --g_notif_email_body is assigned here                           
                                    --print_log('Email Body :'||g_notif_email_body);
                                    --
                                    g_email_subject :=g_instance||' '||rec.allocation_erp||' Inactive journals extract to SharePoint - Status Update';
                                    print_log('Email Subject :'||g_email_subject);                        
                                    --
                                    set_bu_sharepoint_email (p_emp_group =>g_GSC_emp_grp, p_type =>g_control_type_AA); --g_sharepoint_email is assigned here
                                    print_log('SharePoint Email  :'||g_sharepoint_email);                        
                                    --
                                    -- push the extract to sharepoint
                                    print_log('Begin: Push inactive business unit journal details to GSC library in SharePoint.'); 
                                    --                       
                                    sharepoint_integration
                                       (
                                            p_emp_group =>rec.allocation_erp,
                                            p_file =>g_directory_path||'/'||l_file,
                                            p_sharepoint_email =>g_sharepoint_email,
                                            p_dummy_email_from =>g_notif_email_from,
                                            p_email_subject =>g_email_subject
                                       )
                                       ;
                                    --
                                    print_log('End: Push inactive business unit journal details to GSC library in SharePoint.');                           
                                    -- email the teams about the extract
                                    send_email_notif
                                       (
                                            p_email_from      =>g_notif_email_from,
                                            p_email_to           =>g_notif_email_to,
                                            p_email_cc           =>g_notif_email_cc,
                                            p_email_subject =>g_email_subject, 
                                            p_email_body     =>g_notif_email_body,
                                            p_mime_type      =>g_email_mime_type
                                       )
                                      ; 
                                    print_log('After sending email notification about inactive business unit journal details.');                                                                                                
                                    --                     
                             exception
                              when others then
                               print_log('@Failed to log file details for BU, @ emp_group '||rec.allocation_erp||', message =>'||sqlerrm);
                             end;
                             -- End logging file details for each BU along with their concur batch id
                      else
                       print_log('No inactive ERP group allocated journals found.');
                      end if; 
                  --
                 end loop;
                 --
                 close c_inactive_bu;
                 --    
       -- ### @ end of rec in c_uniq_concur_batches loop
     end loop;     
     --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    raise program_error;
  end EXTRACT_INACTIVE_BU;
  --    
  procedure EXTRACT_TIE_OUT_DETAILS is --main driver extract used for HDS Internal Audit Controls
   --
    l_sub_routine varchar2(30) :='extract_tie_out_details'; 
    l_request_id number :=fnd_global.conc_request_id;
    --
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;
    l_count number :=0;
    l_run_id number :=0;
    l_seq number :=0;
    --
    l_utl_file_id   UTL_FILE.FILE_TYPE;
    l_line_buffer VARCHAR2(30000):= Null;
    l_file VARCHAR2(150) :=Null;        
    --
    cursor c_tie_out_dtls is --lookup XXCUS_CONCUR_OOP_PAYMENT_TYPES is the driver table here
        select
              g_je_actuals je_type     
            ,emp_custom21 empl_erp_group
            ,nvl(allocation_custom1, allocation_custom7) allocation_erp_group
            ,nvl(b.enabled_flag, 'N') allocation_erp_active
            ,g_current_batch_id crt_btch_id  
            ,concur_batch_id   concur_batch_id
            ,(
                 CASE
                  WHEN rpt_entry_pmt_type_code IN 
                  (
                    select lookup_code
                    from  apps.fnd_lookup_values_vl
                    where 1 =1
                         and lookup_type ='XXCUS_CONCUR_OOP_PAYMENT_TYPES'
                         and enabled_flag ='Y'
                  )    THEN 'OOP'
                 ELSE 'CARD'    
                 end     
              ) oop_card
            ,sum( case when  journal_payee_pmt_type_name LIKE 'RT%' then 0 else journal_amt end) journal_amount --Ver 1.3
            ,count(journal_amt) journal_lines 
            ,sum(
               case
                 when ( nvl(allocation_custom1, allocation_custom7) =g_CI_GP_emp_grp and journal_payer_pmt_type_name !=g_payer_pmt_type)  then journal_amt 
                 else 0 --For all US org canadian dollars will be zero 
               end
              ) cad_taxes
            ,sum
             (
               case when nvl(b.enabled_flag, 'N') ='N' then journal_amt else 0 end
             ) inactive_erp_dollars
            ,l_creation_date creation_date
            ,l_created_by created_by
            ,l_request_id request_id
            ,g_N email_flag
            ,g_status_new status
            ,g_run_id run_id 
        from  xxcus.xxcus_src_concur_sae_n a
                  ,fnd_lookup_values b
        where 1 =1
             and a.crt_btch_id =g_current_batch_id
             and nvl(a.allocation_custom1, a.allocation_custom7) is not null
             and b.lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
             and b.lookup_code =nvl(allocation_custom1, allocation_custom7)
        group by cube
                     (
                          emp_custom21 
                        ,nvl(allocation_custom1, allocation_custom7)
                        ,nvl(b.enabled_flag, 'N')
                        ,concur_batch_id
                        ,(
                                     CASE
                                      WHEN rpt_entry_pmt_type_code IN 
                                      (
                                        select lookup_code
                                        from  apps.fnd_lookup_values_vl
                                        where 1 =1
                                             and lookup_type ='XXCUS_CONCUR_OOP_PAYMENT_TYPES'
                                             and enabled_flag ='Y'
                                      )    THEN 'OOP'
                                     ELSE 'CARD'    
                                     end     
                         )
                     )
        having
                     (
                        grouping (concur_batch_id) =0
                       and grouping (emp_custom21) =0
                       and grouping(nvl(b.enabled_flag, 'N') ) =0
                       and grouping (nvl(allocation_custom1, allocation_custom7)) =0 
                       and grouping    (
                 CASE
                  WHEN rpt_entry_pmt_type_code IN 
                  (
                    select lookup_code
                    from  apps.fnd_lookup_values_vl
                    where 1 =1
                         and lookup_type ='XXCUS_CONCUR_OOP_PAYMENT_TYPES'
                         and enabled_flag ='Y'
                  )    THEN 'OOP'
                 ELSE 'CARD'
                 end     
              ) =0
                     )             
        order by 1 ,2
           ;    
    --
    type  c_tie_out_dtls_type is table of c_tie_out_dtls%rowtype index by binary_integer;
    c_tie_out_dtls_rec c_tie_out_dtls_type;
    --
    cursor tie_out_hdr is 
    select 
                 b.je_type                                                                        je_type 
                ,sum(b.journal_amount)                                            header_total --Ver 1.1
                ,sum(b.cad_taxes)                                                        cad_tax_amt
                ,sum(b.inactive_erp_dollars)                                    inactive_erp_amt
                ,(
                    select 
                                 sum(a.entered_dr)
                    from   apps.gl_interface a
                    where 1 =1
                       and user_je_source_name =g_je_source
                       and user_je_category_name =g_je_category
                       and group_id =g_GSC_grp_id
                       and reference29 =g_run_id
                       and status =g_status_new
                       and attribute2 =g_event_type_PEREXP                         
                 )                                                                                         personal_chgs                 
    from   xxcus.xxcus_concur_sae_header a
               ,xxcus.xxcus_concur_tie_out_details b
    where 1 =1
         and b.crt_btch_id =g_current_batch_id
         and b.concur_batch_id =g_concur_batch_id
         and b.run_id =g_run_id
         and a.crt_btch_id =b.crt_btch_id
         and a.concur_batch_id =b.concur_batch_id                  
         and a.run_id =b.run_id
    group by      
     b.je_type 
    ,a.journal_amount
    ;
    --
    type tie_out_hdr_type is table of tie_out_hdr%rowtype index by binary_integer;
    tie_out_hdr_rec tie_out_hdr_type;            
    --
    cursor tie_out_interco is
    select b.employee_group allocated_group
               ,sum(a.entered_cr) allocated_amount 
               ,g_event_type_EXP which_set  
               ,1 pull_seq               
    from   apps.gl_interface a
               ,xxcus.xxcus_concur_extract_controls b
    where 1 =1
       and a.user_je_source_name =g_je_source
       and a.user_je_category_name =g_je_category
       and a.attribute2 =g_event_type_IC
       and a.status =g_status_new
       and a.reference29 =g_run_id
       and b.gl_group_id =a.group_id
       and b.type =g_control_type_AA --because GSC has multiple types in the extract control table, we just pull one record to get the group id
    group by b.employee_group
    union all
    select b.employee_group allocated_group
               ,sum(a.entered_cr) allocated_amount
               ,g_event_type_PEREXP which_set
               ,2 pull_seq                                                
    from   apps.gl_interface a
               ,xxcus.xxcus_concur_extract_controls b
    where 1 =1
       and a.user_je_source_name =g_je_source
       and a.user_je_category_name =g_je_category
       and a.attribute2 =g_event_type_PEREXP
       and a.status =g_status_new
       and a.reference29 =g_run_id
       and b.gl_group_id =a.group_id
       and b.type =g_control_type_AA --because GSC has multiple types in the extract control table, we just pull one record to get the group id
    group by b.employee_group
    -- Begin Ver 1.6
    union all
    select g_WW_emp_grp allocated_group
               ,g_ww_separation_total allocated_amount 
               ,g_event_type_EXP which_set  
               ,3 pull_seq               
    from  dual
    where 1 =1
        and exists
         (
              select '1'
              from    fnd_lookup_values
              where 1 =1
                    and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
                    and nvl(enabled_flag, 'N') =g_Y
                    and lookup_code =g_WW_emp_grp
         )    
    -- End Ver 1.6             
    order by 4 asc
    ;    
    --
    cursor c_other_totals is
    select
               g_current_batch_id
              ,g_concur_batch_id
              ,g_run_id 
              ,rpt_entry_pmt_type_name  other_item
               ,sum(journal_amt)                  other_amount
               ,2                                                   pull_seq
    from   xxcus.xxcus_src_concur_sae_n
    where 1 =1
             and crt_btch_id =g_current_batch_id
             --and journal_payer_pmt_type_name =g_payer_pmt_type --Ver 1.1
             and rpt_entry_pmt_type_code IN ('IBCP', 'CBCP')  --only company card payments
             and (  (allocation_custom7 =g_CI_GP_emp_grp and journal_payer_pmt_type_name =g_payer_pmt_type and journal_payee_pmt_type_name NOT LIKE 'RT%') OR (allocation_custom1 !=g_CI_GP_emp_grp and 2 =2)) --Ver 1.2          
    group by rpt_entry_pmt_type_name 
    union all    
    select
               g_current_batch_id
              ,g_concur_batch_id
              ,g_run_id 
              --,rpt_entry_pmt_type_name||' ('||emp_currency_alpha_code||')'  other_item -- Ver 1.7
              -- Begin Ver 1.7
               ,case  
                when nvl(allocation_custom1, allocation_custom7) =g_CI_GP_emp_grp 
                      then rpt_entry_pmt_type_name||' ('||emp_currency_alpha_code||')'
                when nvl(allocation_custom1, allocation_custom7) =g_WW_emp_grp 
                      then rpt_entry_pmt_type_name||' ('||emp_currency_alpha_code||') - WW'  
                when nvl(allocation_custom1, allocation_custom7) !=g_WW_emp_grp 
                      then rpt_entry_pmt_type_name||' ('||emp_currency_alpha_code||') - Others'                                    
               end                                                                                                                         other_item              
              -- End Ver 1.7
               ,sum(journal_amt)                                                                                            other_amount
               ,1                                                                                                                            pull_seq
    from   xxcus.xxcus_src_concur_sae_n
    where 1 =1
             and crt_btch_id =g_current_batch_id
             --and journal_payer_pmt_type_name =g_payer_pmt_type --Ver 1.1
             and rpt_entry_pmt_type_code IN ('IBIP', 'CASH') --only cash payments
             and (  (allocation_custom7 =g_CI_GP_emp_grp and journal_payer_pmt_type_name =g_payer_pmt_type and journal_payee_pmt_type_name NOT LIKE 'RT%') OR  (allocation_custom1 !=g_CI_GP_emp_grp and 2=2))   --Ver 1.2         
    --group by rpt_entry_pmt_type_name||' ('||emp_currency_alpha_code||')' -- Ver 1.7
    -- Begin Ver 1.7
    group by
               case  
                when nvl(allocation_custom1, allocation_custom7) =g_CI_GP_emp_grp 
                      then rpt_entry_pmt_type_name||' ('||emp_currency_alpha_code||')'
                when nvl(allocation_custom1, allocation_custom7) =g_WW_emp_grp 
                      then rpt_entry_pmt_type_name||' ('||emp_currency_alpha_code||') - WW'  
                when nvl(allocation_custom1, allocation_custom7) !=g_WW_emp_grp 
                      then rpt_entry_pmt_type_name||' ('||emp_currency_alpha_code||') - Others'                                    
               end
    -- End Ver 1.7                   
    union all
    select
               g_current_batch_id
              ,g_concur_batch_id
              ,g_run_id  
              ,line_item_name
              ,sum((-1)*line_item_amount)   line_item_amount 
              ,3                                                        pull_seq
    from   xxcus.xxcus_concur_tie_out_report
    where 1 =1
    and crt_btch_id           =g_current_batch_id
    and concur_batch_id =g_concur_batch_id
    and run_id                     =g_run_id
    and report_type           ='Tie Out Batch'
    and sequence                =3 --Inactive Concur ERP's
    group by line_item_name
    -- begin ver 1.4
    union all
    select 
               g_current_batch_id
              ,g_concur_batch_id
              ,g_run_id  
              ,'Personal charges'     line_item_name
              ,sum(a.entered_dr)   line_item_amount 
              ,4                                     pull_seq                 
    from   apps.gl_interface a
    where 1 =1
       and user_je_source_name =g_je_source
       and user_je_category_name =g_je_category
       and group_id =g_GSC_grp_id
       and reference29 =g_run_id
       and status =g_status_new
       and attribute2 =g_event_type_PEREXP
    union all
    select 
               g_current_batch_id
              ,g_concur_batch_id
              ,g_run_id  
              ,'Personal charges'     line_item_name
              ,0                                     line_item_amount 
              ,4                                     pull_seq 
    from dual
    where 1 =1
    and not exists
    (  
        select 'Y'              
        from   apps.gl_interface a
        where 1 =1
           and user_je_source_name =g_je_source
           and user_je_category_name =g_je_category
           and group_id =g_GSC_grp_id
           and reference29 =g_run_id
           and status =g_status_new
           and attribute2 =g_event_type_PEREXP
    )                 
    -- end ver 1.4
    order by
       --6 asc --Ver 1.3
      4 asc --Ver 1.3
    ;    
    --
  begin
    --
    -- print_log('@Tie out :, begin cleanup  where crt_btch_id = '||g_current_batch_id||', records deleted :'||sql%rowcount);
    --
    l_sub_routine :='extract_tie_out_details';
    --
    open c_tie_out_dtls;
    --
    fetch c_tie_out_dtls bulk collect into c_tie_out_dtls_rec;
    --
       if c_tie_out_dtls_rec.count >0 then
            --
            forall idx in 1 .. c_tie_out_dtls_rec.count
            insert into xxcus.xxcus_concur_tie_out_details values c_tie_out_dtls_rec(idx);
             --
             l_count :=l_count + sql%rowcount;
             --
       end if;
    --
    close c_tie_out_dtls;
    --
    print_log('@'||l_sub_routine||', total records inserted :'||l_count);
    -- 
    l_count :=0;   
    --
    SET_SAE_HDR_STATUS (p_status =>'Start: Tie out Details/Summary extracts.');
    --
    --print_log('@Tie out report extract: begin cleanup  where crt_btch_id = '||g_current_batch_id||', records deleted :'||sql%rowcount);
     l_count :=0;
    --     
     print_log('@Before Cursor tie_out_hdr:  crt_btch_id = '||g_current_batch_id||', concur_batch_id :'||g_concur_batch_id||', run_id :'||g_run_id);               
    --
    open tie_out_hdr;
    --
    fetch tie_out_hdr bulk collect into tie_out_hdr_rec;
    --
    if tie_out_hdr_rec.count >0 then
         --        
           INS_AUDIT_TIE_OUT_RPT
           (
             p_report_type =>'Tie Out Batch'
            ,p_extract_type =>'Actuals'
            ,p_seq =>1
            ,p_item_name =>'Header Total'
            ,p_item_amount =>tie_out_hdr_rec(1).header_total
            ,p_line_comments =>'Header total from Concur SAE extract'
            ,p_creation_date =>l_creation_date
            ,p_created_by =>l_created_by
            ,p_request_id =>l_request_id  
            ,p_run_id =>g_run_id 
           );
         --     
         l_count :=l_count + 1; --1'st record
         --
           INS_AUDIT_TIE_OUT_RPT
           (
             p_report_type =>'Tie Out Batch'
            ,p_extract_type =>'Actuals'
            ,p_seq =>2
            ,p_item_name =>'Less CAD Taxes'
            ,p_item_amount =>tie_out_hdr_rec(1).cad_tax_amt
            ,p_line_comments =>'Less CAD total taxes from Concur SAE extract'
            ,p_creation_date =>l_creation_date
            ,p_created_by =>l_created_by
            ,p_request_id =>l_request_id
            ,p_run_id =>g_run_id               
           );
         --     
         l_count :=l_count + 1; --2'nd record
         --
           INS_AUDIT_TIE_OUT_RPT
           (
             p_report_type =>'Tie Out Batch'
            ,p_extract_type =>'Actuals'
            ,p_seq =>3
            ,p_item_name =>'Inactive Concur ERP''s'
            ,p_item_amount =>tie_out_hdr_rec(1).inactive_erp_amt
            ,p_line_comments =>'Inactive ERP total journal amount from Concur SAE extract'
            ,p_creation_date =>l_creation_date
            ,p_created_by =>l_created_by
            ,p_request_id =>l_request_id
            ,p_run_id =>g_run_id               
           );
         --     
         l_count :=l_count + 1; --3'rd record
         -- 
           INS_AUDIT_TIE_OUT_RPT
           (
             p_report_type =>'Tie Out Batch'
            ,p_extract_type =>'Actuals'
            ,p_seq =>4
            ,p_item_name =>'Personal charges'
            ,p_item_amount =>tie_out_hdr_rec(1).personal_chgs
            ,p_line_comments =>'Personal charges on company card from Concur SAE extract'
            ,p_creation_date =>l_creation_date
            ,p_created_by =>l_created_by
            ,p_request_id =>l_request_id
            ,p_run_id =>g_run_id                         
           );
         --     
         --l_count :=l_count + 1; --4'th record
         --
         print_log('@tie out report, total records inserted :'||l_count);
         --
    else
     print_log('No Tie out batch records found in collection tie_out_hdr_rec.count :'||tie_out_hdr_rec.count);
    end if;
    --
    close tie_out_hdr;
    --
    for rec in tie_out_interco loop 
         --
         l_seq :=l_seq +1;
             --
             if rec.which_set =g_event_type_EXP then
              --
                   --
                   INS_AUDIT_TIE_OUT_RPT
                   (
                     p_report_type =>'Tie Out Interco'
                    ,p_extract_type =>'Actuals'
                    ,p_seq =>l_seq
                    ,p_item_name =>rec.allocated_group||' JE'
                    ,p_item_amount =>rec.allocated_amount
                    ,p_line_comments =>'GSC Interco for '||rec.allocated_group
                    ,p_creation_date =>l_creation_date
                    ,p_created_by =>l_created_by
                    ,p_request_id =>l_request_id
                    ,p_run_id =>g_run_id               
                   );
                 --      
              --
             else
              --
                   --
                   INS_AUDIT_TIE_OUT_RPT
                   (
                     p_report_type =>'Tie Out Interco'
                    ,p_extract_type =>'Actuals'
                    ,p_seq =>l_seq
                    ,p_item_name =>'Personal charges'
                    ,p_item_amount =>rec.allocated_amount
                    ,p_line_comments =>'GSC personal charges receivable'
                    ,p_creation_date =>l_creation_date
                    ,p_created_by =>l_created_by
                    ,p_request_id =>l_request_id
                    ,p_run_id =>g_run_id               
                   );
                 --      
              --
             end if;
             --
    end loop;
    --
    print_log('@tie out report interco, total records inserted :'||l_seq);  
    --
    l_seq :=0;
    --
    for rec in c_other_totals loop
       --
       l_seq :=l_seq +1;
       --     
       INS_AUDIT_TIE_OUT_RPT
       (
         p_report_type =>'Other Totals'
        ,p_extract_type =>'Actuals'
        ,p_seq =>l_seq
        ,p_item_name =>rec.other_item
        ,p_item_amount =>rec.other_amount
        ,p_line_comments =>'Other totals by cash or card type.'
        ,p_creation_date =>l_creation_date
        ,p_created_by =>l_created_by
        ,p_request_id =>l_request_id
        ,p_run_id =>g_run_id               
       );     
     --
    end loop;
    --
    print_log('@tie out report other totals, total records inserted :'||l_seq);  
    --    
    --
    commit;
    --  
  exception
   when program_error then
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end EXTRACT_TIE_OUT_DETAILS;  
  --  
  procedure PROCESS_NEW_BATCH 
    (
        retcode out varchar2
       ,errbuf out varchar2
       ,p_non_prod_email in varchar2
       ,p_push_to_sharepoint in varchar2
    ) IS
    --
    l_sub_routine varchar2(30) :='process_new_batch';
    l_ok boolean :=FALSE;
    --  
  begin
    --
    g_non_prod_email :=p_non_prod_email;
    --
    g_push_to_sharepoint :=p_push_to_sharepoint;
    --
    g_run_id :=get_tie_out_run_id(); --get run id 
    --    
    Is_This_Production;
    --
    print_log('Enter: GET_LAST_BATCH [ Find from SAE history table, the last HDS datawarehouse owned CRT_BTCH_ID].');
    get_last_batch;
    SET_SAE_HDR_STATUS (p_status =>'get_last_batch complete.');
    print_log('Exit: GET_LAST_BATCH');
    print_log(' ');
    --
    print_log('Enter: GET_CURRENT_BATCH [Find from current SAE table, the HDS datawarehouse owned CRT_BTCH_ID].');    
    get_current_batch; --set global variables for use down the line
    SET_SAE_HDR_STATUS (p_status =>'get_current_batch complete.');    
    print_log('Exit: GET_CURRENT_BATCH');    
    print_log(' ');    
    --    
    print_log('Enter: ARCHIVE_CURRENT_BATCH -Move current SAE table data to SAE history table using crt_btch_id ['||g_last_batch_id||']');    
    archive_current_batch (p_archive_flag =>'Y', p_archive_date =>sysdate);
    SET_SAE_HDR_STATUS (p_status =>'archive_current_batch complete.');    
    print_log('Exit: ARCHIVE_CURRENT_BATCH');
    print_log(' ');
    --
    print_log('Enter: FETCH_CONCUR_BATCH_DATE [SAE header table batch date for the corresponding CRT_BTCH_ID].');    
    FETCH_CONCUR_BATCH_DATE; --set global variables for use down the line..
    --Sets global variables g_batch_date, g_concur_batch_id, g_fiscal_period, g_fiscal_start, g_fiscal_end
    SET_SAE_HDR_STATUS (p_status =>'fetch_concur_batch_date complete.');    
    print_log('Exit: FETCH_CONCUR_BATCH_DATE');
    print_log(' ');
    --
    print_log('Enter: EXTRACT_PEOPLESOFT_OOP --Extract out of pocket for all LOB''s.');    
    extract_peoplesoft_oop;
    SET_SAE_HDR_STATUS (p_status =>'Concur eligible out of pocket table inserts complete.');    
    print_log('Exit: EXTRACT_PEOPLESOFT_OOP');    
    print_log(' ');    
    --
    print_log('Enter: WW_EXTRACT_ELIGIBLE_GL_JE --extract WaterWorks eligible je records.');    
    WW_EXTRACT_ELIGIBLE_GL_JE;
    SET_SAE_HDR_STATUS (p_status =>'Flag Waterworks GL JE eligible lines.');    
    print_log('Exit: WW_EXTRACT_ELIGIBLE_GL_JE');    
    print_log(' ');    
    --
    print_log('Enter: WW_OB_GL_JE --extract WaterWorks Je actuals, flat files and interco journals.');    
    WW_OB_GL_JE; --extracts all details required to generate GL journal entries for WaterWorks
    SET_SAE_HDR_STATUS (p_status =>'Waterworks JE file extracts complete.');    
    print_log('Exit: WW_OB_GL_JE');    
    print_log(' ');    
    --
    print_log('Enter: CI_GP_EXTRACT_ELIGIBLE_GL_JE --extract CI Brafasco eligible je records.');    
    CI_GP_EXTRACT_ELIGIBLE_GL_JE;
    SET_SAE_HDR_STATUS (p_status =>'Flag CI Canada Brafasco GL JE eligible lines.');    
    print_log('Exit: CI_GP_EXTRACT_ELIGIBLE_GL_JE');    
    print_log(' ');    
    --
    print_log('Enter: CI_GP_OB_GL_JE --extract CI Brafasco Great Plains JE actuals, flat files and interco journals.');    
    CI_GP_OB_GL_JE;
    SET_SAE_HDR_STATUS (p_status =>'CI Canada Brafasco JE file extracts complete.');    
    print_log('Exit: CI_GP_OB_GL_JE');    
    print_log(' ');    
    -- 
    print_log('Enter: CI_ORACLE_US_ELIGIBLE_GL_JE --extract CI US JE eligible je records.');    
    CI_ORACLE_US_ELIGIBLE_GL_JE;
    SET_SAE_HDR_STATUS (p_status =>'Flag CI ORACLE US JE eligible lines.');    
    print_log('Exit: CI_ORACLE_US_ELIGIBLE_GL_JE');    
    print_log(' '); 
    --
    print_log('Enter: CI_US_GL_JE; --extract GL journals for CI US actuals and GSC Interco with CI US to GL interface.');    
    CI_US_GL_JE;
    SET_SAE_HDR_STATUS (p_status =>'CI ORACLE US JE push to staging complete');    
    print_log('Exit: CI_US_GL_JE');    
    print_log(' ');    
    --       
    print_log('Enter: GSC_ORACLE_US_ELIGIBLE_GL_JE --extract GSC JE eligible je records.');    
    GSC_ORACLE_US_ELIGIBLE_GL_JE;
    SET_SAE_HDR_STATUS (p_status =>'Flag GSC ORACLE US JE eligible lines.');    
    print_log('Exit: GSC_ORACLE_US_ELIGIBLE_GL_JE');    
    print_log(' '); 
    --
    print_log('Enter: GSC_US_GL_JE; --extract GL journals for CI US actuals and GSC Interco with CI US to GL interface.');    
    GSC_US_GL_JE;
    SET_SAE_HDR_STATUS (p_status =>'GSC ORACLE US JE push to staging complete.');    
    print_log('Exit: GSC_US_GL_JE');    
    print_log(' ');    
    --    
    print_log('Enter: Extract Personal Journals');          
    EXTRACT_PERSONAL_JOURNALS;
    SET_SAE_HDR_STATUS (p_status =>'Personal journals extract complete.');    
    -- Exit: Extract Inactive BU journals extract.
    print_log('Exit: Extract Personal Journals');
    print_log(' ');   
    --    
    -- paste everything back above this line    
    -- Begin transfer gl journal interface records from concur gl interface to actual ebs gl interface 
    print_log('Enter: Transfer Concur GL JE interface journals to Oracle EBS.');     
    print_log (' '); 
    PUSH_CONCUR_JE_TO_EBS_GL;
    SET_SAE_HDR_STATUS (p_status =>'Push concur JE from staging to GL Interface complete.');    
    print_log('Exit: Transfer Concur GL JE interface journals to Oracle EBS.');                    
    -- End transfer gl journal interface records from concur gl interface to actual ebs gl interface    
    --
    -- Enter: Extract CARD details 
    print_log(' ');     
    print_log('Enter: Extract CARD details.');          
    EXTRACT_CARD_DETAILS;
    SET_SAE_HDR_STATUS (p_status =>'CARD details for all BU are extracted and complete.');    
    -- Exit: Extract CARD details
    print_log('Exit: Extract CARD details.');          
    --      
    -- Enter: Extract Inactive BU journals extract.
    print_log(' ');     
    print_log('Enter: Extract Inactive BU journals.');          
    EXTRACT_INACTIVE_BU;
    SET_SAE_HDR_STATUS (p_status =>'Inactive business units file extracts complete.');    
    -- Exit: Extract Inactive BU journals extract.
    print_log('Exit: Extract Inactive BU journals.');          
    --     
    -- Enter: Extract Inactive BU journals extract.
    print_log(' ');
    print_log('Enter: Extract tie out information.');     
    EXTRACT_TIE_OUT_DETAILS;
    SET_SAE_HDR_STATUS (p_status =>'End: Tie out Details/Summary extracts.');    
    -- Exit: Extract Inactive BU journals extract.
    print_log('Exit: Extract tie out information.');  
    --    
    commit;
    --
  exception
   when program_error then
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end PROCESS_NEW_BATCH;        
  --
  procedure DELETE_REQUEST_SET_LOG  is
   l_sub_routine varchar2(30) :='delete_request_set_log';
   PRAGMA AUTONOMOUS_TRANSACTION;
  begin 
         --
        DELETE xxcus.xxcus_concur_report_set_reqs;
         --
         print_log ('Deleted records from xxcus_concur_report_set_reqs, total records :'||sql%rowcount);
         --
         commit;
         --
  exception
   when others then
   raise program_error;
   print_log('Failed to delete xxcus_concur_report_set_reqs, message =>'||sqlerrm);
  end DELETE_REQUEST_SET_LOG;  
  --
  procedure INSERT_REQUEST_SET_LOG (p_set_req_id IN NUMBER, p_wrapper_req_id IN NUMBER) is
   l_sub_routine varchar2(30) :='insert_request_set_log';
   PRAGMA AUTONOMOUS_TRANSACTION;
  begin 
         --
         insert into xxcus.xxcus_concur_report_set_reqs
          (
            report_set_request_id
           ,wrapper_req_id
          )
         values
          (
             p_set_req_id
            ,p_wrapper_req_id
          )
          ;
         --
         print_log ('Inserted into xxcus_concur_report_set_reqs, total records :'||sql%rowcount);
         --
         commit;
         --
  exception
   when others then
   raise program_error;
   print_log('Failed to insert xxcus_concur_report_set_reqs, message =>'||sqlerrm);
  end INSERT_REQUEST_SET_LOG;
  --
  procedure SUBMIT_REQUEST_SET
    (
        retcode out varchar2
       ,errbuf  out varchar2  
       ,p_non_prod_email in varchar2
       ,p_push_to_sharepoint in varchar2                      
    ) is
    --
    l_request_id number :=0;
    l_req_id number :=0;  
    l_running_req_id number :=fnd_global.conc_request_id;  
    l_wrapper_req_id number :=fnd_global.conc_request_id;
    l_email_subject varchar2(80) :=Null;
    --
    l_non_prod_email  varchar2(240) :=Null;
    l_non_prod_cc_email  varchar2(240) :=Null;    
    l_job_code varchar(80) :='XXCUS_CONCUR_AUDIT';
    --
    l_current_db_name   VARCHAR2(20) :=Null;
    l_prod_db_name      VARCHAR2(20) := 'ebsprd';
    --
    l_zip_file varchar2(240) :=Null;
    --
    l_file_name varchar2(240) :=Null;    
    --    
    l_outbound_loc varchar2(240) :=Null;
    --
    l_max_files_allowed number :=1;
    --
    l_APPLCSF_OUT varchar2(60) :='/ebizlog/conc/out';
    --
    l_bool         BOOLEAN;
    l_iso_language VARCHAR2(30);
    --
    l_iso_territory VARCHAR2(30);
    --
    l_err_msg  VARCHAR2(3000);
    l_err_code NUMBER;
    l_sec      VARCHAR2(4000);

    l_ok                  BOOLEAN := TRUE;
    l_success             NUMBER := 0;
    l_user_id             NUMBER :=15837; --1229; --GLINTERFACE
    l_responsibility_id   NUMBER :=50661; --50761;  --50761 for HDS Cash Management Master User or 50661 for XXCUS_CON
    --
    l_resp_application_id NUMBER :=20003; -- 260; --260 for Cash Management OR 20003 for XXCUS_CON
     lc_request_data VARCHAR2(20) :='';
    l_sysdate             DATE;
    l_end_date            VARCHAR2(30);
    -- 
    srs_failed        EXCEPTION;
    submitprog_failed EXCEPTION;
    submitset_failed  EXCEPTION;    
    --
    l_err_callfrom  VARCHAR2(75) DEFAULT 'xxcus_concur_pkg.submit_request_set';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    --
   cursor c_xml_defn is
    select 
      a.default_language dflt_lang,
      a.default_territory dflt_territory,
      a.default_output_type dflt_output_type
    from xdo_templates_vl a
    where 1 =1
        and a.template_code =l_job_code
        and nvl(a.end_date,sysdate) >= sysdate
        and a.object_version_number = 
            (
               select max(xdt1.object_version_number)
               from xdo_templates_vl xdt1
               where 1 =1
                     and xdt1.template_code =a.template_code
                     and nvl(xdt1.end_date,sysdate) >= sysdate
            )
        and rownum < 2;
   --
   c_xml_defn_rec  c_xml_defn%rowtype :=Null;        
   --
   cursor get_audit_rpt_id  is
    select b.report_set_request_id report_set_request_id,
               a.request_id audit_rpt_req_id, a.program job, a.phase_code, a.status_code
    from apps.fnd_amp_requests_v a
              ,xxcus.xxcus_concur_report_set_reqs b
    where 1 =1
    and a.concurrent_program_id !=36034 --Exclude request set stages
    and a.concurrent_program_name =l_job_code
    --and b.wrapper_req_id =l_wrapper_req_id
    and a.phase_code ='C' --Complete
    connect by prior a.request_id  =a.parent_request_id
    start with a.request_id =b.report_set_request_id
    ;  
   get_audit_rpt_rec get_audit_rpt_id%rowtype :=Null;  
   --
   cursor c_new_file is
    select 
               (  select 
                       'XXCUS_CONCUR_BATCH'||concur_batch_id||'_'||to_char(batch_date,'MMDDYY')||'.pdf'
                    from  xxcus.xxcus_concur_sae_header
                    where request_id =a.request_id
                ) audit_rpt_file_name
    from apps.fnd_amp_requests_v a
              ,xxcus.xxcus_concur_report_set_reqs b 
    where 1 =1
    and concurrent_program_id !=36034
    and concurrent_program_name ='XXCUS_CONCUR_PROCESS_NEW_BATCH'
    --and b.wrapper_req_id =l_wrapper_req_id
    connect by prior a.request_id  =a.parent_request_id
    start with a.request_id =b.report_set_request_id
        ;   
   --
    l_new_file varchar2(240) :=Null;
   --
  BEGIN
   --
       lc_request_data :=fnd_conc_global.request_data;
   -- 
    if lc_request_data is null then     
       --
       lc_request_data :='1';
       -- 
       DELETE_REQUEST_SET_LOG; --delete xxcus.xxcus_concur_report_set_reqs;
       --
               print_log(' ');
               print_log('p_non_prod_email =>'||p_non_prod_email);
               -- 
               g_non_prod_email :=p_non_prod_email;      
               -- 
               print_log(' ');          
              -- 
              -- Begin Initialize the request set 
              --
                l_sec := 'Initializing Request Set - HDS CONCUR (Set) : Process New Batch';
                --
                fnd_file.put_line(fnd_file.log, l_sec);
                --
                l_ok := fnd_submit.set_request_set
                                  (
                                      application =>'XXCUS'
                                     ,request_set =>'XXCUS_CONCUR_SET'
                                  );
                -- End Initialize the request set 
                --                 
                -- Submit each stage of the request set
                --
                -- Stage_100 : Import and process concur batch.
                --    
                IF (l_ok AND l_success = 0) THEN
                  --
                  l_sec := 'Stage_1 : Import and process concur batch.';
                  --
                  fnd_file.put_line(fnd_file.log, l_sec);
                  --
                  l_ok := fnd_submit.submit_program
                                                  (
                                                        application => 'XXCUS'
                                                       ,program     => 'XXCUS_CONCUR_PROCESS_NEW_BATCH'
                                                       ,stage       => 'STAGE_1'
                                                       ,argument1   =>p_non_prod_email
                                                       ,argument2   =>p_push_to_sharepoint
                                                    );
                ELSE
                  l_success := -91;
                  print_log(l_sec || ':  Failed = ' || l_success);
                  RAISE submitprog_failed;
                END IF;
                -- 
                -- Stage_2 : Generate audit report.
                --    
                IF (l_ok AND l_success = 0) THEN
                  --
                  l_sec := 'Stage_2 : Set audit report template [RTF]';
                  --
                  fnd_file.put_line(fnd_file.log, l_sec);
                  --
                     begin 
                      --
                      open c_xml_defn;
                      fetch c_xml_defn into c_xml_defn_rec.dflt_lang, c_xml_defn_rec.dflt_territory, c_xml_defn_rec.dflt_output_type ;
                      close c_xml_defn;
                      --
                     exception
                      when others then
                       c_xml_defn_rec.dflt_lang :='en';
                       c_xml_defn_rec.dflt_territory :='00';
                       c_xml_defn_rec.dflt_output_type :='PDF';              
                     end;
                      --                  
                      l_ok :=fnd_submit.add_layout
                                    (
                                      template_appl_name =>'XXCUS',
                                      template_code =>l_job_code,
                                      template_language =>c_xml_defn_rec.dflt_lang,
                                      template_territory =>c_xml_defn_rec.dflt_territory,
                                      output_format =>c_xml_defn_rec.dflt_output_type
                                    );
                        -- 
                      l_sec := 'Stage_2 : Generate audit report.';
                      --
                      fnd_file.put_line(fnd_file.log, l_sec);
                      --                                     
                      l_ok := fnd_submit.submit_program
                                                      (
                                                        application => 'XXCUS'
                                                       ,program     => 'XXCUS_CONCUR_AUDIT'
                                                       ,stage       => 'STAGE_2'                                        
                                                        );
                ELSE
                  l_success := -92;
                  print_log(l_sec || ':  Failed = ' || l_success);
                  RAISE submitprog_failed;
                END IF;
                --                                   
                -- At this moment we have submitted the individual requests for each stage. now submit the request set.
                --
                IF l_ok AND l_success = 0 THEN 
                     --
                     l_request_id := fnd_submit.submit_set(NULL, TRUE); --we use TRUE to let know that the current routine that submitted the set will pause until the request set is done
                     --
                     -- g_report_set_request_id :=l_request_id;
                     -- g_wrapper_req_id             :=l_wrapper_req_id;
                     --
                     -- INSERT_REQUEST_SET_LOG (p_set_req_id =>l_request_id, p_wrapper_req_id =>l_wrapper_req_id);
                     --
                    print_log ('Submitted request set HDS CONCUR (Set) : Process New Batch, Request_id = ' || l_request_id);
                    --
                     INSERT_REQUEST_SET_LOG (p_set_req_id =>l_request_id, p_wrapper_req_id =>l_wrapper_req_id);
                     --                    
                    fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>lc_request_data);
                    --                
                     lc_request_data :=to_char(to_number(lc_request_data)+1);
                     --                        
                ELSE
                    l_success := -1000;
                    --
                    print_log ( 'Failedto submit request set HDS CONCUR (Set) : Process New Batch, message = ' || l_success||', msg =>'||sqlerrm);
                    --
                    RAISE submitset_failed;
                    --
                END IF;       
       --
    else     
          --   
          -- Email audit file.
          --    
          l_sec := 'Begin: Email audit file.';
          --
          fnd_file.put_line(fnd_file.log, l_sec);
          --
          begin  
               --
                select upper(name)
                into    g_database
                from  v$database
                where 1 =1
                ;
                --
                if (g_database ='EBSPRD') then  
                     -- 
                     g_instance :=Null;
                     l_non_prod_cc_email :=g_notif_email_cc;
                     --
                else 
                     --
                      g_instance :='( NON PROD - '||g_database||' ) ';
                      l_non_prod_cc_email :=p_non_prod_email;
                     --
                end if;
               --
               open get_audit_rpt_id;
               fetch get_audit_rpt_id into get_audit_rpt_rec;
               close get_audit_rpt_id;
                --
               if  get_audit_rpt_rec.audit_rpt_req_id is  null then
                print_log(
                                   'Not able to fetch details from cursor get_audit_rpt_id using wrapper id :'||l_wrapper_req_id
                                 );
                 raise program_error;
               end if; 
               --                 
                l_email_subject :=g_instance||'HDS CONCUR: Import Batch [Actuals] Summary - Final Update';
                --
                l_file_name :=l_job_code||'_'||get_audit_rpt_rec.audit_rpt_req_id||'_1.PDF';            
                --
                print_log('l_file_name ='||l_file_name);            
                --
               open c_new_file;
               fetch c_new_file into l_new_file;
               close c_new_file;
               --
               if  l_new_file is  null then
                print_log(
                                   'Not able to derive new file name for audit report using report set request id :'||get_audit_rpt_rec.audit_rpt_req_id
                                   ||', wrapper id :'||l_wrapper_req_id
                                 );
                 raise program_error;
               else
                print_log (' l_new_file =>'||l_new_file);
               end if; 
               --      
               if p_push_to_sharepoint =g_Y then
                     --
                     begin 
                            --
                            select sharepoint_email_address 
                            into     l_non_prod_email
                            from  xxcus.xxcus_concur_extract_controls
                            where 1 =1
                                  and employee_group =g_GSC_emp_grp
                                  and type                         =g_control_type_AUDIT
                            ; 
                            --
                     exception
                      when no_data_found then
                         l_non_prod_email :='hds_concur_audit@share.hdsupply.com';
                      when others then
                       print_log('Error in fetching sharepoint email address for employee group GSC ORACLE and type AUDIT');
                       l_non_prod_email :='hds_concur_audit@share.hdsupply.com';                                                      
                     end;
                     -- 
               else
                     l_non_prod_email :=p_non_prod_email;                              
               end if;                            
               --      
               l_req_id := fnd_request.submit_request
                 (
                   'XXCUS'
                  ,'XXCUS_CONCUR_EMAIL_RPT'
                  ,NULL
                  ,NULL
                  ,FALSE
                  ,l_file_name --absolute file path and name. Example /ebizlog/conc/out/XXCUS_CONCUR_AUDIT_18900888_1.PDF                  
                  ,l_non_prod_email -- non production recipient email id
                  ,g_notif_email_from --sender email id
                  ,l_email_subject --email subject  
                  ,l_new_file --new file name that goes to sharepoint folder Concur Audit Reports 
                  ,l_non_prod_cc_email               
                 );
              --
              l_sec := 'End: Email audit file, request_id ='||l_req_id;
              --
                  fnd_file.put_line(fnd_file.log, l_sec);
              --
          exception
           when no_data_found then
            print_log ('Failed to find audit report request id');
            raise program_error;
           when program_error then
            raise;
           when others then
            print_log ('when others - failed to find audit report request id');
            raise program_error;            
          end;
          --
        retcode :=0;
        errbuf :='Child request [ HDS CONCUR (Set) : Process New Batch completed. Exiting HDS CONCUR : Kickoff Actuals Import Wrapper ]';
        print_log(errbuf);
       -- end copy
    end if;
   --   
  EXCEPTION
    WHEN srs_failed THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := 'Call to set_request_set failed: ' || fnd_message.get ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN submitprog_failed THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Call to submit_program failed: ' || fnd_message.get ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN submitset_failed THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Call to submit_set failed: ' || fnd_message.get ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Request set submission failed - unknown error: ' ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END SUBMIT_REQUEST_SET;
  --       
end xxcus_concur_pkg;
/