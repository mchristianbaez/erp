/*************************************************************************
  $Header TMS_20160726-00224_ITEMS_NOT_GENERATING_A_PURCHASING_REQUEST.sql $
  Module Name: TMS_20160726-00224  Data Fix script for 20160726-00224

  PURPOSE: Data fix script for 20160726-00224--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        30-AUG-2016  Pattabhi Avula         TMS#20160726-00224 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160726-00224    , Before Delete');

DELETE WIP_JOB_SCHEDULE_INTERFACE a
 WHERE     process_phase = '2'--Validate
       AND process_status = 1 --Pending
       AND EXISTS
              (SELECT 1
                 FROM mtl_system_items_b
                WHERE     organization_id = a.organization_id
                      AND inventory_item_id = a.primary_item_id
                      AND PLANNING_MAKE_BUY_CODE = '2'); --Buy item

   DBMS_OUTPUT.put_line (
         'TMS: 20160726-00224  Sales order lines updated (Expected:12): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160726-00224    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160726-00224 , Errors : ' || SQLERRM);
END;
/