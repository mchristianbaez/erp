/***********************************************************************************************************************************************
   NAME:     TMS_20180506-00013_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        05/07/2018  Rakesh Patel     TMS#20180506-00013-Data Fix script to update entry status code to 4
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before update');

  UPDATE apps.mtl_cycle_count_entries
    SET entry_status_code = 4
  WHERE entry_status_code =  1
    AND TO_DATE (count_due_date, 'DD/MM/RRRR HH24:MI:SS')
          BETWEEN TO_DATE ('03/07/2018 00:00:00', 'MM-DD-YYYY HH24:MI:SS')
              AND TO_DATE ('05/04/2018 23:59:59', 'MM-DD-YYYY HH24:MI:SS');
              
   DBMS_OUTPUT.put_line ('Records Updated -' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
	  ROLLBACK;
END;
/