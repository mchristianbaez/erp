  /****************************************************************************************************
  Table: FND_USERS & PER_ALL_PEOPLE_F
  Description: Updating email address for with 'donotreply@hdsupply.com'
               email id
  HISTORY
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  1.0     21-Feb-2017        Niraj K Ranjan  Initial version TMS#20170221-00054   Update email address in qa
  *****************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

   UPDATE apps.fnd_user
      SET email_address = 'donotreply@hdsupply.com'
    WHERE email_address = 'HDSHRITUNITTesting@hdsupply.com';
    
    DBMS_OUTPUT.put_line ('Records updated (FND_USERS): ' || SQL%ROWCOUNT);
    
    
   UPDATE apps.per_all_people_f
      SET email_address = 'donotreply@hdsupply.com'
    WHERE email_address = 'HDSHRITUNITTesting@hdsupply.com';
   
    DBMS_OUTPUT.put_line (
      'Records updated (PER_ALL_PEOPLE_F): ' || SQL%ROWCOUNT);
  
   COMMIT;

   DBMS_OUTPUT.put_line ('After Update');
EXCEPTION
   WHEN OTHERS THEN
      DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
END;
/
