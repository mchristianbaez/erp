/**********************************************************************************************************
    $Header xxwc_sales_order_acknowlg_file.pkb $
    Module Name: xxwc_sales_order_acknowlg_file
    PURPOSE: TO send PO ack
    REVISIONS:
    Ver    Date         Author                Description
    ------ -----------  ------------------    ----------------
	1.0    13-Mar-2013  Rasikha Galimova      Generated 3/13/2013 5:04:55 PM from APPS@EBIZPRD
    1.1    11-Jul-2017  P.Vamshidhar          TMS#20170707-00117 -  Berg/ Material Management/White Cap System
	1.2    27-Jul-2017  P.Vamshidhar          TMS#20170725-00089 - Adding Bill_to column to extract
***********************************************************************************************************/

CREATE OR REPLACE FORCE VIEW apps.xxwc_sales_order_mmsi_v
(
   format_name
  ,customer_name
  ,customer_mm_id
  ,customer_po_number
  ,order_date
  ,required_date
  ,distributor_name
  ,distributor_mmd_id
  ,distributor_location_id
  ,job_name
  ,address_id
  ,ship_to_name
  ,ship_to_address_1
  ,ship_to_address_2
  ,ship_to_city
  ,ship_to_state
  ,ship_to_zip
  ,order_number
  ,line_number
  ,shipment_number
  ,description
  ,quantity
  ,item_required_date
  ,price_unit_of_measure
  ,price_each
  ,unit_price
  ,upc
  ,distributor_item_id
  ,customer_item_number
  ,extended_price
  ,qty_shipping
  ,qty_backord
  ,cost_code
  ,phase_code
  ,cost_type_code
  ,general_ledger_code
  ,release_number
  ,buyer
  ,item_mfg_name
  ,item_mfg_cat_num
  ,orig_sales_order_num
  ,cust_job_number
  ,account_number
  ,line_id
  ,booked_date
  ,flow_status_code
  ,order_category_code
  ,header_id
  ,bill_to_contact -- Added in 1.2
)
AS
   ( (SELECT 'FLATFORMAT5' AS format_name
            ,hp.party_name AS customer_name,
            --,'7607461003' AS customer_mm_id
			/*
			,(SELECT attribute1
  FROM fnd_flex_values_vl
 WHERE     (   ('' IS NULL)
            OR (structured_hierarchy_level IN (SELECT hierarchy_id
                                                 FROM fnd_flex_hierarchies_vl h
                                                WHERE     h.flex_value_set_id =
                                                             (SELECT flex_value_set_id
                                                                FROM fnd_flex_value_sets
                                                               WHERE flex_value_set_name =
                                                                        'XXWC_MMSI_accounts')
                                                      AND h.hierarchy_name LIKE '')))
       AND flex_value_set_id = (SELECT flex_value_set_id
                                  FROM fnd_flex_value_sets
                                 WHERE flex_value_set_name = 'XXWC_MMSI_accounts')
       AND enabled_flag = 'Y'
       AND NVL (end_date_active, SYSDATE + 1 / 24) > SYSDATE
       AND flex_value = hca.account_number
       AND value_category = 'XXWC_MMSI_CUST_IDS') AS customer_mm_id   commented in Rev 1.1*/
             -- APPS.XXWC_SALES_ORDER_ACKNOWLG_FILE.DERIVE_MM_CUSTOMER_ID(hca.account_number,hl_ship.city,hl_ship.state) AS customer_mm_id -- Added in Rev 1.1 -- Commented in 1.2
             APPS.XXWC_SALES_ORDER_ACKNOWLG_FILE.DERIVE_MM_CUSTOMER_ID (
                hca.account_number,
                xoh.invoice_to_contact)
                AS customer_mm_id                          -- Added in Rev 1.2
            ,oh.cust_po_number AS customer_po_number
            ,TO_CHAR (oh.booked_date, 'MM/DD/YYYY') AS order_date
            ,'' AS required_date                                                         ---intentionally null
            ,'HD SUPPLY - WHITE CAP CONSTRUCTION SUPPLY' AS distributor_name
            ,'9497945300' AS distributor_mmd_id
            ,mp.organization_code AS distributor_location_id
            ,hcsua_ship.location AS job_name
            ,hps_ship.party_site_number AS address_id
            ,hcsua_ship.location AS ship_to_name                     -- that is what the view translates it to
            ,hl_ship.address1 AS ship_to_address_1
            ,hl_ship.address2 AS ship_to_address_2
            ,hl_ship.city AS ship_to_city
            ,hl_ship.state AS ship_to_state
            ,hl_ship.postal_code AS ship_to_zip
            ,oh.order_number AS order_number
            ,ol.line_number AS line_number
            ,ol.shipment_number
            ,msi.description AS description
            ,DECODE (tp.order_category_code
                    ,'ORDER', ol.ordered_quantity
                    ,'RETURN', -ol.ordered_quantity
                    ,ol.ordered_quantity)
                AS quantity
            ,TO_CHAR (ol.request_date, 'MM/DD/YYYY') AS item_required_date
            , (CASE
                  WHEN ol.order_quantity_uom = ol.pricing_quantity_uom
                  THEN
                     1
                  ELSE
                     (SELECT conversion_rate
                        FROM inv.mtl_uom_conversions
                       WHERE uom_code = ol.pricing_quantity_uom AND disable_date IS NULL AND ROWNUM = 1)
               END)
                price_unit_of_measure
            , (CASE
                  WHEN ol.order_quantity_uom = ol.pricing_quantity_uom
                  THEN
                     ol.unit_selling_price
                  ELSE
                     (  ol.unit_selling_price
                      / NVL (
                           (SELECT conversion_rate
                              FROM inv.mtl_uom_conversions
                             WHERE uom_code = ol.pricing_quantity_uom AND disable_date IS NULL AND ROWNUM = 1)
                          ,1))
               END)
                price_each
            ,ol.unit_selling_price AS unit_price
            ,apps.xxwc_mv_routines_pkg.get_item_cross_reference (ol.inventory_item_id, 'UPC', 1) AS upc
            ,ol.ordered_item AS distributor_item_id
            ,'' AS customer_item_number                                                   --intentionally null
            ,DECODE (tp.order_category_code
                    ,'ORDER', ol.ordered_quantity * ol.unit_selling_price
                    ,'RETURN', -ol.ordered_quantity * ol.unit_selling_price
                    ,ol.ordered_quantity * ol.unit_selling_price)
                AS extended_price
            ,NVL (ol.shipped_quantity, 0) qty_shipping
            ,DECODE (tp.order_category_code
                    ,'RETURN', 0
                    , (ol.ordered_quantity - NVL (ol.shipped_quantity, 0)))
                AS qty_backord
            ,'' AS cost_code                                                              --intentionally null
            ,'' AS phase_code                                                             --intentionally null
            ,'' AS cost_type_code                                                         --intentionally null
            ,'' AS general_ledger_code                                                    --intentionally null
            ,'' AS release_number                                                         --intentionally null
            ,'' AS buyer                                                     --intentionally null at this time
            ,'' AS item_mfg_name                                             --intentionally null at this time
            ,'' AS item_mfg_cat_num                                          --intentionally null at this time
            ,'' AS orig_sales_order_num                                      --intentionally null at this time
            ,'' AS cust_job_number
            -- additional fields
            ,hca.account_number
            ,ol.line_id
            ,oh.booked_date
            ,ol.flow_status_code
            ,oh.order_category_code
            ,oh.header_id
			,xoh.invoice_to_contact -- Rev 1.2
        --intentionally null at this time
        FROM ont.oe_order_headers_all oh
            ,ont.oe_order_lines_all ol
            ,inv.mtl_parameters mp
            ,ar.hz_cust_accounts hca
            ,ar.hz_parties hp
            ,ar.hz_cust_site_uses_all hcsua_ship
            ,ar.hz_cust_acct_sites_all hcsa_ship
            ,ar.hz_party_sites hps_ship
            ,ar.hz_locations hl_ship
            ,apps.mtl_system_items msi
            ,apps.oe_transaction_types_all tp
			,apps.xxwc_oe_order_headers_v xoh -- Added in Rev 1.2
       -- additional
       WHERE     oh.sold_from_org_id = '162'
             -- AND oh.booked_date > TRUNC (SYSDATE - 1)
             --  AND oh.booked_date < TRUNC (SYSDATE)
             --AND oh.order_category_code in ('ORDER','MIXED')
             AND oh.booked_flag = 'Y'
             AND oh.header_id = ol.header_id
             AND oh.ship_from_org_id = mp.organization_id
             AND oh.sold_to_org_id = hca.cust_account_id
			 and oh.header_id = xoh.header_id -- Added in Rev 1.2
             --  AND hca.account_number IN ('1076000', '2586000')                              --'154984000','118830000'
             AND hca.party_id = hp.party_id
             AND oh.ship_to_org_id = hcsua_ship.site_use_id
             AND hcsua_ship.cust_acct_site_id = hcsa_ship.cust_acct_site_id
             AND hcsa_ship.party_site_id = hps_ship.party_site_id
             AND hps_ship.location_id = hl_ship.location_id
             --AND ol.flow_status_code NOT IN ('CANCELLED')
             AND ol.inventory_item_id = msi.inventory_item_id
             AND ol.ship_from_org_id = msi.organization_id
             AND tp.transaction_type_code = 'LINE'
             AND tp.transaction_type_id = ol.line_type_id));