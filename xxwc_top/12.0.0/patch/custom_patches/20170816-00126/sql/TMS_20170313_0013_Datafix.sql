/***********************************************************************************************************************************************
   NAME:      TMS_20170313_0013_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        08/21/2017  Rakesh Patel     TMS#20170313_0013
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

   UPDATE apps.MTL_MATERIAL_TRANSACTIONS
      SET attribute7 = '59731.2'  
    WHERE transaction_id = 571536863 ; 

   DBMS_OUTPUT.put_line ('After Update -' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
	  ROLLBACK;
END;
/