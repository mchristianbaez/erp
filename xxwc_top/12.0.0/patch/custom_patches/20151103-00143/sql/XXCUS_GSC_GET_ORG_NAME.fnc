create or replace function apps.xxcus_test_get_org_name (p_org_id in number) return varchar2 as
/*
 -- Author: Balaguru Seshadri
 -- Parameters: org_id --data type "number"
 -- Scope: 
 -- Modification History
 -- ESMS/TMS     Date                  Version   Comments
 -- =========== ========         ======== =========================================
 -- 20151103-00143   17-DEC-2014    1.0          Install.
 */   
 --
 v_org_name varchar2(150) :=null;
 -- 
 procedure print_log(p_message in varchar2) is
 begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.log, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
 end print_log;
 --
 procedure print_output(p_message in varchar2) is
 begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.output, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
 end print_output;
 --
begin
      --
      begin 
       --
        select name
        into v_org_name
        from hr_operating_units
        where 1 =1
             and organization_id =p_org_id;
       --
       return v_org_name;
       --
      exception
       when no_data_found then
        return 'NONE';
       when others then
        return null;
      end;     
      --  
exception
 when others then
  print_log('Outer block, message ='||sqlerrm);
  return null;
end xxcus_test_get_org_name;
/