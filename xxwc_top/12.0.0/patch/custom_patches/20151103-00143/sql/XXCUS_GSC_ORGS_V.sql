create or replace force view apps.xxcus_gsc_orgs_v as
   /* ************************************************************************
    HD Supply
    All rights reserved.
    Ticket                             Ver          Date                 Author                             Description
    ----------------------     ----------   ----------             ------------------------     -------------------------     
    TMS 20151103-00143     1.0           08/21/2015   Balaguru Seshadri        Add oracle_location, creation_date and vendor_name
   ************************************************************************ */
select *
from hr_operating_units
where 1 =1
and organization_id in (163, 167, 101, 102);
--
comment on table apps.xxcus_gsc_orgs_v is 'TMS20151103-00143';
--