declare
begin
dbms_output.put_line('Before insert of taxprodconv table');
Insert into TAXWARE.TAXPRODCONV
   (MERCHANTID, BUSNLOCN, USERPRODCODE1, USERPRODCODE2, TWIPRODCODE)
Values
   ('WCI', 'DEFAULT', '90012', '90012', '00010');
dbms_output.put_line('After insert, msg ='||sql%rowcount);
commit;
end;
/