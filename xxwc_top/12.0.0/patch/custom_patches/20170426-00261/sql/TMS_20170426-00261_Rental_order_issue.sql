/*****************************************************************************
Script: TMS_20170426-00261_Rental_order_issue.sql 
Description: Trying to return Rental items and stop the rent error message 
             appeared as below
 
=============================================================================
VERSION DATE               AUTHOR(S)         DESCRIPTION 
------- -----------------  ---------------   ---------------------------------
 1.0     08-Jun-2017        Pattabhi Avula   20170426-00261 - Trying to return
                                             Rental items and stop the rent 
                                             error message appeared as below
*******************************************************************************/
DECLARE
   CURSOR lines IS
   SELECT line_id
    FROM oe_order_lines_all
    WHERE line_id=94508888
     AND open_flag='Y'
     AND flow_status_code='AWAITING_RETURN'
     AND Nvl(shipped_quantity,0)=0;

   l_result      VARCHAR2(30);
   l_activity_id  NUMBER;
   l_file_val  varchar2(500);

 BEGIN

 --oe_debug_pub.debug_on;
-- oe_debug_pub.initialize;
 --l_file_val:=OE_DEBUG_PUB.Set_Debug_Mode('FILE');
 --oe_Debug_pub.setdebuglevel(5);
 dbms_output.put_line(' Inside the script');
 -- dbms_output.put_line('file path is:'||l_file_val);

 FOR i IN lines LOOP
    OE_Standard_WF.OEOL_SELECTOR
    (p_itemtype => 'OEOL'
    ,p_itemkey => to_char(i.line_id)
    ,p_actid => 12345
    ,p_funcmode => 'SET_CTX'
    ,p_result => l_result
    );
 -- oe_debug_pub.add('Retrun value of OEOL_selector function is'||l_result);
  UPDATE oe_order_lines_all
     SET ordered_quantity =1
   WHERE line_id = i.line_id;

  UPDATE oe_order_lines_all
  SET    shipped_quantity=ordered_quantity,
         fulfilled_quantity=ordered_quantity,
         flow_status_code='AWAITING_FULFILLMENT',
         last_update_date=SYSDATE,
         last_updated_by=-16516164
   WHERE line_id=i.line_id
     AND flow_status_code='AWAITING_RETURN';

    wf_engine.handleError('OEOL', to_char(i.line_id),'RMA_WAIT_FOR_RECEIVING','SKIP','COMPLETE');

    wf_engine.handleError('OEOL', to_char(i.line_id),'RMA_WAIT_FOR_INSPECTION','SKIP','COMPLETE');

    END LOOP;
--oe_debug_pub.debug_off;

UPDATE oe_order_lines_all
   SET UNIT_LIST_PRICE = 0,
       UNIT_LIST_PRICE_PER_PQTY = NULL,
       link_to_line_id = 86085181,
       RETURN_ATTRIBUTE1 = 52752643,
       return_attribute2 = 86085181
 WHERE line_id = 96605627;

COMMIT;

end;
/
DECLARE
   CURSOR lines is
   SELECT line_id
     FROM oe_order_lines_all
    WHERE line_id=92924470
      AND open_flag='Y'
      AND flow_status_code='AWAITING_RETURN'
      AND Nvl(shipped_quantity,0)=0;

   l_result       VARCHAR2(30);
   l_activity_id  NUMBER;
   l_file_val     VARCHAR2(500);

 BEGIN

 /*oe_debug_pub.debug_on;
 oe_debug_pub.initialize;
 l_file_val:=OE_DEBUG_PUB.Set_Debug_Mode('FILE');
 oe_Debug_pub.setdebuglevel(5); */
 dbms_output.put_line(' Inside the script');
 -- dbms_output.put_line('file path is:'||l_file_val);

 FOR i IN lines LOOP
    OE_Standard_WF.OEOL_SELECTOR
    (p_itemtype => 'OEOL'
    ,p_itemkey => to_char(i.line_id)
    ,p_actid => 12345
    ,p_funcmode => 'SET_CTX'
    ,p_result => l_result
    );
  --oe_debug_pub.add('Retrun value of OEOL_selector function is'||l_result);
  UPDATE oe_order_lines_all
     SET ordered_quantity =40
   WHERE line_id = i.line_id;

  UPDATE oe_order_lines_all
     SET shipped_quantity=ordered_quantity,
         fulfilled_quantity=ordered_quantity,
         flow_status_code='AWAITING_FULFILLMENT',
         last_update_date=SYSDATE,
         last_updated_by=-16516164
   WHERE line_id=i.line_id
    AND  flow_status_code='AWAITING_RETURN';

    wf_engine.handleError('OEOL', to_char(i.line_id),'RMA_WAIT_FOR_RECEIVING','SKIP','COMPLETE');

    wf_engine.handleError('OEOL', to_char(i.line_id),'RMA_WAIT_FOR_INSPECTION','SKIP','COMPLETE');

    END LOOP;
-- oe_debug_pub.debug_off;

UPDATE oe_order_lines_all
   SET UNIT_LIST_PRICE = 0,
       UNIT_LIST_PRICE_PER_PQTY = NULL,
       link_to_line_id = 92223018,
       RETURN_ATTRIBUTE1 = 56665958,
       return_attribute2 = 92223018
 WHERE line_id = 96607586;

COMMIT;

END;
/

--RR10825    I returned 5 of 31 

DECLARE
   CURSOR lines IS
   SELECT line_id
     FROM oe_order_lines_all
    WHERE line_id= 94508616
      AND open_flag='Y'
      AND flow_status_code='AWAITING_RETURN'
      AND Nvl(shipped_quantity,0)=0;

   l_result       VARCHAR2(30);
   l_activity_id  NUMBER;
   l_file_val     VARCHAR2(500);

 BEGIN

 /*oe_debug_pub.debug_on;
 oe_debug_pub.initialize;
 l_file_val:=OE_DEBUG_PUB.Set_Debug_Mode('FILE');
 oe_Debug_pub.setdebuglevel(5);
 dbms_output.put_line('file path is:'||l_file_val); */
  dbms_output.put_line(' Inside the script');

 FOR i IN lines LOOP
    OE_Standard_WF.OEOL_SELECTOR
    (p_itemtype => 'OEOL'
    ,p_itemkey => to_char(i.line_id)
    ,p_actid => 12345
    ,p_funcmode => 'SET_CTX'
    ,p_result => l_result
    );
 -- oe_debug_pub.add('Retrun value of OEOL_selector function is'||l_result);
  UPDATE oe_order_lines_all
     SET ordered_quantity =34
   WHERE line_id = i.line_id;

  UPDATE oe_order_lines_all
     SET shipped_quantity=ordered_quantity,
         fulfilled_quantity=ordered_quantity,
         flow_status_code='AWAITING_FULFILLMENT',
         last_update_date=SYSDATE,
         last_updated_by=-16516164
  WHERE line_id=i.line_id
   AND flow_status_code='AWAITING_RETURN';

    wf_engine.handleError('OEOL', to_char(i.line_id),'RMA_WAIT_FOR_RECEIVING','SKIP','COMPLETE');

    wf_engine.handleError('OEOL', to_char(i.line_id),'RMA_WAIT_FOR_INSPECTION','SKIP','COMPLETE');

    END LOOP;
-- oe_debug_pub.debug_off;

UPDATE oe_order_lines_all
   SET UNIT_LIST_PRICE = 0,
       UNIT_LIST_PRICE_PER_PQTY = NULL,
       link_to_line_id = 93934333,
       RETURN_ATTRIBUTE1 = 57724623,
       return_attribute2 = 93934333
 WHERE line_id = 96611015;

COMMIT;

END;
/
