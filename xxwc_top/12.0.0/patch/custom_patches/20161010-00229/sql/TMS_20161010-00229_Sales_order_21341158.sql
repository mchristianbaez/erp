/*************************************************************************
  $Header TMS_20161010-00229_Sales_order_21341158.sql $
  Module Name: TMS_20161010-00229  Data Fix script for I675908

  PURPOSE: Data fix script for Sales order 21341158 --No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        17-JAN-2017  Pattabhi avula         TMS#20161010-00229 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20161010-00229    , Before Update');

update apps.oe_order_lines_all
set FLOW_STATUS_CODE='CANCELLED',
CANCELLED_FLAG='Y'
where line_id = 75861504
and header_id= 46106118;

   DBMS_OUTPUT.put_line (
         'TMS: 20161010-00229  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20161010-00229    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161010-00229 , Errors : ' || SQLERRM);
END;
/