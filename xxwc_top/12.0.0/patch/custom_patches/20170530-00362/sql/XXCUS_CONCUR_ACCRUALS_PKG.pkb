CREATE OR REPLACE PACKAGE BODY APPS.xxcus_concur_accruals_pkg is 
  --
 /*
   Ticket#                            Date         Author               Notes
   ----------------------------------------------------------------------------------------------------------------------------------------
   TMS:20170521-00002 / ESMS 561767   05/28/2017   Balaguru Seshadri    Concur accrual process related routines  
   TMS: 20170530-00362 / ESMS 561767   05/30/2017   Balaguru Seshadri    Fix accounting_date logic and post update of rpt_entry_pmt_code_name field  
 */
  --
  g_CI_US_INV_Program varchar2(60) :=Null;
  --
  g_pkg_name varchar2(30) :='XXCUS_CONCUR_ACCRUALS_PKG';
  g_prd_database varchar2(15) :='EBSPRD';
  g_database varchar2(10) :=Null;
  g_email_id varchar2(240) :=Null;
  --
  g_file_name varchar2(240) :=Null;
  g_delimiter varchar2(1) :=',';
  --
   g_report_set_request_id number :=0;
   g_wrapper_req_id number :=0;
   g_batch_1 varchar2(30) :=Null;
   g_push_to_sharepoint varchar2(1) :=Null;
  --
  g_instance varchar2(30) :=Null;
  g_Y varchar2(1) :='Y';
  g_N varchar2(1) :='N';
  --
  g_je_actuals varchar2(15) :='Actuals';
  g_je_accruals varchar2(15) :='Accruals';
  --
  g_email_mime_type  varchar2(150) :='text; charset=us-ascii';    
  g_notif_email_from varchar2(240) :='OracleEBS.Concur.Extracts@hdsupply.com';
  g_sharepoint_email varchar2(240) :=Null;
  g_email_subject varchar2(240) :=Null;
  --
  g_notif_email_to varchar2(240) :='iExpenseAdmins@hdsupply.com';
  g_notif_email_cc varchar2(240) :='HDS.OracleGLSupport@hdsupply.com';  
  g_non_prod_email varchar2(240) :=Null;
  g_send_email_attach_to varchar2(240) :=Null;
  --
  g_notif_email_body varchar2(4000) :=Null;
  g_carriage_return varchar2(1) :='
';
  --
  g_err_email1  varchar2(200) :=Null;
  g_err_email2  varchar2(200) :=Null;
  g_last_batch_id number :=0;
  g_last_batch_count number :=0;  
  --
  g_outbound_loc VARCHAR2(240) :=Null;
  g_inbound_loc VARCHAR2(240) :=Null;
  g_directory_path VARCHAR2(150) :=Null;
  g_total number :=0;
  --
  g_current_batch_id number :=0;  
  g_current_batch_count number :=0;    
  g_status_new VARCHAR2(20) :='NEW';
  g_status_process VARCHAR2(20) :='IN PROCESS';
  g_status_finish VARCHAR2(20) :='COMPLETE';    
  --
  g_payer_pmt_type varchar2(10) :='Company';
  g_payee_pmt_type varchar2(10) :='Employee';
  g_zero number :=0;
  --
  --Begin WaterWorks related variables
  g_WW_gl_prod_code varchar2(10) :='01';
  g_WW_gl_je_type varchar2(3) :='UL';
  g_WW_accrual_seq number :=3899;
  --End WaterWorks related variables
  --
  type g_concur_gl_intf_type is table of xxcus.xxcus_concur_accrual_gl_iface%rowtype index by binary_integer;
  g_concur_gl_intf_rec g_concur_gl_intf_type;
  --
  --Begin WhiteCap US related variables  
  g_WC_US_gl_prod_code varchar2(10) :='0W';
  --End WhiteCap US related variables  
  --
  --
  --Begin WhiteCap Canada [ Brafasco, Great Plains system] related variables  
  g_WC_GP_gl_prod_code varchar2(10) :='37';
  --End  WhiteCap Canada [ Brafasco, Great Plains system] related variables  
  --  
  --Begin FM US related variables    
  g_FM_US_gl_prod_code varchar2(10) :='';
  --End FM US related variables  
  --  
  --Begin FM Canada related variables    
  g_FM_CAD_gl_prod_code varchar2(10) :='';
  --End FM Canada related variables   
  --
  --Begin Repair and Remodel related variables       
  g_RR_gl_product_code varchar2(10) :=''; 
  --End Repair and Remodel related variables       
  --
  -- Begin Concur GL Group id constants 
   g_WW_grp_id number :=11; --WaterWorks 
   g_CI_GP_grp_id number :=22; --Construction and Industrial Brafasco Canada
   --
   g_GSC_grp_id number :=33; --HDS Global Support Center
   g_FM_US_grp_id  number :=44; --Facilities Maintenance US
   --
   g_FM_CAD_grp_id number :=55; --Facilities Maintenance Canada
   g_CI_US_grp_id number :=66; --Construction and Industrial US
   --
   g_CI_HIS_grp_id number :=77; --Construction and Industrial Home Improvement Solution     
   --   
  -- End Concur GL group id Constants 
  -- 
  -- Begin Concur Employee  Group Constants 
   g_WW_emp_grp varchar2(20) :='WW MINCRON'; --WaterWorks 
   g_CI_GP_emp_grp varchar2(20) :='CI CAD GP'; --Construction and Industrial Brafasco Canada
   g_GSC_emp_grp varchar2(20) :='GSC ORACLE'; --HDS Global Support Center
   g_FM_US_emp_grp  varchar2(20) :='FM SAP'; --Facilities Maintenance US
   g_FM_CAD_emp_grp varchar2(20) :='FM CAD INFOR'; --Facilities Maintenance Canada
   g_CI_US_emp_grp varchar2(20) :='CI ORACLE'; --Construction and Industrial US
   g_CI_HIS_emp_grp varchar2(20) :='CI HIS DYNAMICS'; --Construction and Industrial Home Improvement Solutions   
  -- End Concur Employee  Group Constants
  --
  -- Begin Concur Employee  Group Constants used for file creation only 
  g_WW_MINCRON varchar2(20) :='WW_MINCRON';
  g_GSC_ORACLE varchar2(20) :='GSC_ORACLE';
  g_FMSAP  varchar2(20) :='FM_SAP';
  g_FMCADINFOR varchar2(20) :='FM_CAD_INFOR';
  g_CI_ORACLE varchar2(20) :='CI_ORACLE';
  g_CI_HIS_DYNAMICS varchar2(20) :='CI_HIS_DYNAMICS';
  g_CI_CAD_GP varchar2(20) :='CI_CAD_GP';
  -- End Concur Employee  Group Constants  used for file creation only 
  --
  -- Begin GL Interface related constants
  --
  g_JE_Actual_Flag xxcus.xxcus_concur_gl_iface.actual_flag%type :='A'; --Journal Entry Type flag A - Actuals, B -Budget
  g_JE_Iface_Status xxcus.xxcus_concur_gl_iface.status%type :='NEW'; --GL interface insert status code
  g_JE_Category xxcus.xxcus_concur_gl_iface.user_je_category_name%type :='Expenses Accrual'; --Journal Entry Category
  g_JE_Source xxcus.xxcus_concur_gl_iface.user_je_source_name%type :='Concur';--Journal Entry Source
  --
  g_Currency_USD varchar2(3) :='USD'; --US Dollar
  g_Currency_CAD varchar2(3) :='CAD'; --Canadian Dollar
  g_US_ledger_id number :=2061; --US Ledger internal id
  --
  g_CAD_ledger_id number :=2063; --Canadian ledger internal id
  g_hds_gl_iface_status1 varchar2(30) :='EBS-GL-IFACE-XFER-PENDING'; 
  g_hds_gl_iface_status2 varchar2(30) :='EBS-GL-IFACE-XFER-COMPLETE';
  --
  -- End GL Interface related constants  
  --
  g_Interco_US_Product   xxcus.xxcus_concur_gl_iface.segment1%type :='48'; --For US Intercompany only
  g_Interco_CA_Product   xxcus.xxcus_concur_gl_iface.segment1%type :='50';  --For Canada Intercompany only
  g_future_use_1                xxcus.xxcus_concur_gl_iface.segment6%type :='00000';
  g_future_use_2                xxcus.xxcus_concur_gl_iface.segment7%type :='00000';  
  --
  g_WW_override_dept varchar2(30) :='030'; --WW Non Fab
  g_inactive_count varchar2(30) :='0';
  g_inactive_dollars varchar2(30) :='0.00';
  g_file_prefix varchar2(30) :=Null;
  --
  g_batch_date date :=Null;
  g_concur_batch_id number :=0;
  g_run_id number :=0;
  --
  g_fiscal_period varchar2(10) :=Null;
  g_fiscal_start date :=Null;
  g_fiscal_end date :=Null;  
  g_US_accrual_period varchar2(10) :=Null;
  g_US_fiscal_start date :=Null;
  g_US_fiscal_end date :=Null;    
  g_US_rev_accrual_period varchar2(10) :=Null;  
--  g_US_currency varchar2(3) :=Null;
  g_CA_accrual_period varchar2(10) :=Null;
  g_CA_fiscal_start date :=Null;
  g_CA_fiscal_end date :=Null;    
  g_CA_rev_accrual_period varchar2(10) :=Null;
  g_period_num number :=Null;
--  g_CA_currency varchar2(3) :=Null;  
  --
  g_perz_status1 varchar2(60) :='Personal journal lines extracted.';
  g_perz_status2 varchar2(60) :='Personal journal lines transferred to GL Interface.'; 
  g_perz_je_indicator varchar2(30) :='PERSONAL JE';     
  g_event_type_EXP  varchar2(30) :='EXPENSE LINES';
  g_event_type_IC  varchar2(30) :='INTERCO LINES';  
  g_event_type_PEREXP   varchar2(30) :='PERSONAL EXPENSE LINES';
  -- 
  -- variables g_control_type*  are used to determine the system controls like group id and other things
  g_control_type_AA varchar2(30) :='ACTUALS AND ACCRUALS';
  g_control_type_OOP varchar2(30) :='OOP';
  g_control_type_CARD varchar2(30) :='CARD';
  g_control_type_PERSONAL varchar2(30) :='PERSONAL';
  g_control_type_AUDIT varchar2(30) :='AUDIT';        
  --
  procedure PRINT_LOG(p_message in varchar2) is
  begin
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||sqlerrm);
  end PRINT_LOG;
  -- 
  function get_field (v_delimiter in varchar2, n_field_no in number ,v_line_read in varchar2, p_which_line in number ) return varchar2 is
      l_sub_routine varchar2(30) :='get_field'; 
       n_start_field_pos number;
       n_end_field_pos   number;
       v_get_field       varchar2(2000);
  begin
       if n_field_no = 1 then
          n_start_field_pos := 1;
       else
          n_start_field_pos := instr(v_line_read,v_delimiter,1,n_field_no-1)+1;
       end if;
       print_log('n_start_field_pos '||n_start_field_pos||', n_field_no '||n_field_no);
       n_end_field_pos   := instr(v_line_read,v_delimiter,1,n_field_no) -1;
       print_log('n_end_field_pos '||n_end_field_pos||', n_field_no '||n_field_no);       
       if n_end_field_pos > 0 then
          v_get_field := substr(v_line_read,n_start_field_pos,(n_end_field_pos - n_start_field_pos)+1);
           print_log('v_get_field when >0 is '||v_get_field||', n_field_no '||n_field_no);          
       else
          v_get_field := substr(v_line_read,n_start_field_pos); 
           print_log('v_get_field when 0 is '||v_get_field||', n_field_no '||n_field_no);          
       end if;

       return ltrim(rtrim(v_get_field));

  exception
    when others then
     print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);    
     print_log ('Line# ='||p_which_line);
     print_log ('Line ='||v_line_read);
     print_log ('get field: '||sqlerrm);
  end get_field;    
  --
  function IS_ERP_INACTIVE (p_erp in varchar2) return varchar2 is
    --
    l_sub_routine varchar2(30) :='is_erp_inactive'; 
    l_flag varchar2(1) :=Null;      
    --    
  begin 
     --
      select nvl(enabled_flag, 'N')
      into     l_flag
      from    fnd_lookup_values
      where 1 =1
            and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
            and lookup_code =p_erp
          ;  
     --
     if l_flag =g_Y then  
         --
         print_log(' ');
         print_log('ERP : '||p_erp||' Status =>Active');
         print_log(' ');          
         --     
        return g_Y;
        --
     else
         --
         print_log(' ');
         print_log('ERP : '||p_erp||' Status =>Inactive');
         print_log(' ');          
         --     
        return g_N;
        -- 
     end if;
     --
  exception
   when no_data_found then
         --
         print_log(' ');
         print_log('ERP : '||p_erp||' Status =>Not Available because ERP is not setup in the lookup type XXCUS_CONCUR_ACTIVE_BU');
         print_log(' ');          
         --    
         return g_N;
         --
   when too_many_rows then
         --
         print_log(' ');
         print_log('ERP : '||p_erp||' Status =>Inactive, more than 1 record found');
         print_log(' ');          
         --    
         raise program_error;
         --
         return g_N;
         --  
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
         --
         print_log(' ');
         print_log('ERP : '||p_erp||' Status =>Inactive');
         print_log(' ');          
         --    
         return g_N;
         --
  end IS_ERP_INACTIVE;
  --     
  function GET_TIE_OUT_RUN_ID return number is
    --
    l_sub_routine varchar2(30) :='get_tie_out_run_id';
    l_run_id number :=0;       
    --    
  begin 
     --
        select xxcus.xxcus_concur_tieout_s.nextval        
        into     l_run_id
        from   dual
        where 1 =1
          ;  
     --
     print_log(' ');
     print_log('Run ID =>'||l_run_id);
     print_log(' ');          
     --
     return l_run_id;
     --
  exception
   when no_data_found then
    return l_run_id;  
   when too_many_rows then
    return l_run_id;    
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    return l_run_id;
  end GET_TIE_OUT_RUN_ID;
  --  
  procedure INS_AUDIT_TIE_OUT_RPT
   (
     p_report_type in varchar2
    ,p_extract_type in varchar2
    ,p_seq in number
    ,p_item_name in varchar2
    ,p_item_amount in number
    ,p_line_comments in varchar2
    ,p_creation_date in date
    ,p_created_by in number
    ,p_request_id in number   
    ,p_run_id in number
   )  IS
   --
   l_sub_routine varchar2(30) :='ins_audit_tie_out_rpt'; 
   --
  begin  
        --
         insert into xxcus.xxcus_concur_tie_out_accr_rpt
           (
             report_type
            ,extract_type
            ,sequence
            ,line_item_name
            ,line_item_amount
            ,line_comments
            ,status
            ,email_flag  
            ,creation_date
            ,created_by
            ,request_id
            ,run_id
            ,accrual_run_date
            ,fiscal_period           
           ) 
         values 
           (
             p_report_type
            ,p_extract_type
            ,p_seq
            ,p_item_name
            ,p_item_amount
            ,p_line_comments
            ,g_status_new
            ,g_N    
            ,p_creation_date
            ,p_created_by
            ,p_request_id
            ,p_run_id
            ,sysdate 
            ,g_US_accrual_period --g_fiscal_period           
           );
         --
  exception             
   when others then
     print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
     RAISE program_error;       
  end INS_AUDIT_TIE_OUT_RPT;
  --  
  function beforeReport return boolean as
   --
   n_loc number;
   ln_request_id number :=0;
   --
   l_sub_routine varchar2(30) :='beforeReport';    
   --
    cursor get_params is
    select max(a.run_id)                      run_id
    from  xxcus.xxcus_concur_tie_out_accr_rpt  a
    where 1 =1
          and a.status         =g_status_new
          and a.email_flag =g_N
     ;   
   --
  begin   
   --
   n_loc :=100;
   --
   savepoint s1;
   --
    open get_params;
    --
     fetch get_params
     into   xxcus_concur_pkg.g_rpt_run_id
     ;
    --
    close get_params;
   --
   print_log (
                        '@beforeReport, g_rpt_run_id = '||xxcus_concur_pkg.g_rpt_run_id
                     );   
   print_log ('@beforeReport, audit report params fetch complete');
   --
   n_loc :=101;
   --
   commit;
   --
   n_loc :=102;
   --   
   return true;
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', loc => '||n_loc||', msg ='||sqlerrm);
    return false;
  end beforeReport;
  --    
  function afterReport return boolean as
   --
   n_loc number;
   ln_request_id number :=0;
   --
   l_sub_routine varchar2(30) :='afterReport';    
   --
  begin   
   --
   n_loc :=100;
   --
   savepoint s1;
   --
   update xxcus.xxcus_concur_tie_out_accr_rpt 
           set status ='Audit report generation complete.'
                 ,email_flag =g_Y
   where 1 =1
        and run_id =xxcus_concur_pkg.g_rpt_run_id
    ;
   --
   print_log ('@afterReport, audit report generation complete, total rows updated : '||sql%rowcount);
   --
   n_loc :=101;
   --
   update xxcus.xxcus_concur_tie_out_report
           set  status ='Audit report generation complete.'
                 ,email_flag =g_Y
   where 1 =1
        and crt_btch_id           =xxcus_concur_pkg.g_rpt_crt_btch_id
        and concur_batch_id =xxcus_concur_pkg.g_rpt_concur_batch_id
        and run_id                    =xxcus_concur_pkg.g_rpt_run_id
    ;
   --
   print_log ('@afterReport, audit report generation complete, total rows updated : '||sql%rowcount);
   --   
   commit;
   --
   n_loc :=102;
   --   
   return true;
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', loc => '||n_loc||', msg ='||sqlerrm);
    rollback to s1;
    return false;
  end afterReport;
  --  
  procedure IS_THIS_PRODUCTION
   -- This routine sets global variable g_database and g_instance to tell us if we are running the job in a non prod or prod system
  IS
   --
   l_sub_routine varchar2(30) :='is_this_production'; 
   --
  begin  
        --
        select upper(name)
        into    g_database
        from  v$database
        where 1 =1
        ;
        --
        if (g_database ='EBSPRD') then  
             -- 
             g_instance :=Null;
             --
        else 
             --
              g_instance :='( NON PROD - '||g_database||' )';
             --
              if g_non_prod_email is null then 
               print_log('Enter a valid email address to receive the file attachments and other Concur notifications.');
               raise program_error;
              else 
                 --
                 g_email_id :=g_non_prod_email;
                 g_notif_email_to :=g_email_id;
                 g_notif_email_cc :=g_email_id;
                 --
              end if;
              --
        end if;
        --
  exception             
   when others then
     print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);  
     g_sharepoint_email :=Null;  
     g_database :=Null;
     g_instance :=Null;          
  end IS_THIS_PRODUCTION;
  --  
  procedure SET_BU_SHAREPOINT_EMAIL
   -- This routine sets global variable g_notif_email_body. Make sure the variable is always reset before invoking this routine
    ( 
      p_emp_group in varchar2
     ,p_type in varchar2
    )  IS
   --
   l_sub_routine varchar2(30) :='set_bu_sharepoint_email'; 
   --
  begin  
        if (g_database ='EBSPRD') then 
            -- 
            select sharepoint_email_address
            into    g_sharepoint_email
            from  xxcus.xxcus_concur_extract_controls 
            where 1 =1
                 and employee_group =p_emp_group
                 and type =p_type;
            --
        else 
          --
              if g_push_to_sharepoint =g_Y then 
                select sharepoint_email_address
                into    g_sharepoint_email
                from  xxcus.xxcus_concur_extract_controls 
                where 1 =1
                     and employee_group =p_emp_group
                     and type =p_type;           
              else
                 g_sharepoint_email :=g_email_id;          
              end if;
          --
        end if;  
        --
  exception             
   when others then
     print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm); 
     raise program_error;  
     g_sharepoint_email :=Null;  
  end SET_BU_SHAREPOINT_EMAIL;
  --  
  procedure SETUP_EMAIL_BODY
   -- This routine sets global variable g_notif_email_body. Make sure the variable is always reset before invoking this routine
    ( 
      p_text1 in varchar2
     ,p_emp_group in varchar2
     ,p_type in VARCHAR2
    )  IS
   --
   l_sub_routine varchar2(30) :='setup_email_body'; 
   --
  begin  
      --
      if p_type ='OOP' then 
       --
                g_notif_email_body :=
                                       '**** Concur - EBS: Out of pocket outbound extract to SharePoint  **** '
                                   ||g_carriage_return  
                                   ||' '
                                   ||g_carriage_return
                                   ||p_text1 --Send in BU tag
                                   ||g_carriage_return                                           
                                   ||' '
                                   ||g_carriage_return
                                   ||'All - The OOP extracts are copied over to the ConCur GSC Oracle SharePoint library. The file extracts are delimited by PIPE [|].'
                                   ||g_carriage_return                                           
                                   ||' '
                                   ||g_carriage_return 
                                   ||'Note: Please expect a couple minutes delay for files to show up in SharePoint.'
                                   ||g_carriage_return                                           
                                   ||' '
                                   ||g_carriage_return                                                                       
                                   ||'Link: https://finance.share.hdsupply.net/ap/default.aspx'
                                   ||g_carriage_return                                           
                                   ||' '                                   
                                   ||g_carriage_return
                                   ||'This is an automated email. Please DO NOT reply to this message.'
                                   ||g_carriage_return                           
                                   ||' '
                                   ||g_carriage_return                             
                                   ||'Thanks.'
                                   ||g_carriage_return
                                   ||' '
                                   ||g_carriage_return      
                                   ||'HDS GL Support'
                                  ;           
       --
      elsif p_type ='OTHERS' then  --other than out of pocket
       --
           --
           if p_emp_group IN ('CI ORACLE', 'GSC ORACLE') then
                    g_notif_email_body :=
                                           '**** Concur - EBS: Interface JE accruals to GL  **** '
                                       ||g_carriage_return  
                                       ||' '
                                       ||g_carriage_return
                                       ||p_text1 --Send in BU tag
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return
                                       ||'All - The concur JE actuals are pushed over to Oracle GL interface for the respective BU. The journals will be auto submitted for import.'
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return
                                       ||'This is an automated email. Please DO NOT reply to this message.'
                                       ||g_carriage_return                           
                                       ||' '
                                       ||g_carriage_return                             
                                       ||'Thanks.'
                                       ||g_carriage_return
                                       ||' '
                                       ||g_carriage_return      
                                       ||'HDS GL Support'
                                      ;        
           else
                    g_notif_email_body :=
                                           '**** Concur - EBS: Outbound accrual extracts to SharePoint  **** '
                                       ||g_carriage_return  
                                       ||' '
                                       ||g_carriage_return
                                       ||p_text1 --Send in BU tag
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return
                                       ||'All - The concur extracts are copied over to the SharePoint library for the respective BU. The file extracts are delimited by PIPE [|].'
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return 
                                       ||'Note: Please expect a couple minutes delay for files to show up in SharePoint.'
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return                                                                       
                                       ||'Link: https://finance.share.hdsupply.net/ap/default.aspx'
                                       ||g_carriage_return                                           
                                       ||' '                                   
                                       ||g_carriage_return
                                       ||'This is an automated email. Please DO NOT reply to this message.'
                                       ||g_carriage_return                           
                                       ||' '
                                       ||g_carriage_return                             
                                       ||'Thanks.'
                                       ||g_carriage_return
                                       ||' '
                                       ||g_carriage_return      
                                       ||'HDS GL Support'
                                      ;       
           end if; 
           -- 
      elsif p_type ='ORACLE_SOURCED' then
                    g_notif_email_body :=
                                           '**** Concur - EBS: Outbound extracts to SharePoint  **** '
                                       ||g_carriage_return  
                                       ||' '
                                       ||g_carriage_return
                                       ||p_text1 --Send in BU tag
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return
                                       ||'All - The concur accrual extracts are copied over to the SharePoint library for the respective BU. The file extracts are delimited by PIPE [|].'
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return 
                                       ||'Note: Please expect a couple minutes delay for files to show up in SharePoint.'
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return                                                                       
                                       ||'Link: https://finance.share.hdsupply.net/ap/default.aspx'
                                       ||g_carriage_return                                           
                                       ||' '                                   
                                       ||g_carriage_return
                                       ||'This is an automated email. Please DO NOT reply to this message.'
                                       ||g_carriage_return                           
                                       ||' '
                                       ||g_carriage_return                             
                                       ||'Thanks.'
                                       ||g_carriage_return
                                       ||' '
                                       ||g_carriage_return      
                                       ||'HDS GL Support'
                                      ;   
      elsif p_type ='ALL_BU_PERSONAL_EXP' then
                    g_notif_email_body :=
                                           '**** Concur - EBS: Outbound extracts to SharePoint  **** '
                                       ||g_carriage_return  
                                       ||' '
                                       ||g_carriage_return
                                       ||p_text1 --Send in BU tag
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return
                                       ||'All - The concur personal detail extracts are copied over to the SharePoint library under Concur PERSONAL. The file extracts are delimited by PIPE [|].'
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return 
                                       ||'Note: Please expect a couple minutes delay for files to show up in SharePoint.'
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return                                                                       
                                       ||'Link: https://finance.share.hdsupply.net/ap/default.aspx'
                                       ||g_carriage_return                                           
                                       ||' '                                   
                                       ||g_carriage_return
                                       ||'This is an automated email. Please DO NOT reply to this message.'
                                       ||g_carriage_return                           
                                       ||' '
                                       ||g_carriage_return                             
                                       ||'Thanks.'
                                       ||g_carriage_return
                                       ||' '
                                       ||g_carriage_return      
                                       ||'HDS GL Support'
                                      ; 
      elsif p_type ='ALL_BU_CARD_EXP' then
                    g_notif_email_body :=
                                           '**** Concur - EBS: Outbound extracts to SharePoint  **** '
                                       ||g_carriage_return  
                                       ||' '
                                       ||g_carriage_return
                                       ||p_text1 --Send in BU tag
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return
                                       ||'All - The concur CARD SAE detail extracts are copied over to the SharePoint library under Concur CARD sharepint library. The file extracts are delimited by PIPE [|].'
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return 
                                       ||'Note: Please expect a couple minutes delay for files to show up in SharePoint.'
                                       ||g_carriage_return                                           
                                       ||' '
                                       ||g_carriage_return                                                                       
                                       ||'Link: https://finance.share.hdsupply.net/ap/default.aspx'
                                       ||g_carriage_return                                           
                                       ||' '                                   
                                       ||g_carriage_return
                                       ||'This is an automated email. Please DO NOT reply to this message.'
                                       ||g_carriage_return                           
                                       ||' '
                                       ||g_carriage_return                             
                                       ||'Thanks.'
                                       ||g_carriage_return
                                       ||' '
                                       ||g_carriage_return      
                                       ||'HDS GL Support'
                                      ;                                                                                     
      else --For inactive bu reports only p_type ='INACTIVE_BU'
       --
                g_notif_email_body :=
                                       '**** Concur - Inactive BU journals report to SharePoint  **** '
                                   ||g_carriage_return  
                                   ||' '
                                   ||g_carriage_return
                                   ||p_text1 --Send in BU tag
                                   ||g_carriage_return                                           
                                   ||' '
                                   ||g_carriage_return
                                   ||'All - The inactive journal report for allocated erp '||p_emp_group||' are copied over to the ConCur GSC Oracle SharePoint library. The file extracts are delimited by PIPE [|].'
                                   ||g_carriage_return   --g_inactive_count                                         
                                   ||' '
                                   ||g_carriage_return 
                                   ||'Allocated ERP: '||p_emp_group||', Total journal lines: '||g_inactive_count||', Total journal amount: '||g_inactive_dollars
                                   ||g_carriage_return   --g_inactive_count                                         
                                   ||' '
                                   ||g_carriage_return                                    
                                   ||'Note: Please expect a couple minutes delay for files to show up in SharePoint.'
                                   ||g_carriage_return                                           
                                   ||' '
                                   ||g_carriage_return                                                                       
                                   ||'Link: https://finance.share.hdsupply.net/ap/default.aspx'
                                   ||g_carriage_return                                           
                                   ||' '                                   
                                   ||g_carriage_return
                                   ||'This is an automated email. Please DO NOT reply to this message.'
                                   ||g_carriage_return                           
                                   ||' '
                                   ||g_carriage_return                             
                                   ||'Thanks.'
                                   ||g_carriage_return
                                   ||' '
                                   ||g_carriage_return      
                                   ||'HDS GL Support'
                                  ;                                                    
       --
      end if;
      --
      print_log(' ');
  exception             
   when others then
     print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm); 
     raise program_error;              
  end SETUP_EMAIL_BODY;
  --
 procedure SHAREPOINT_INTEGRATION
   (
        p_emp_group in varchar2,
        p_file in varchar2,
        p_sharepoint_email in varchar2,        
        p_dummy_email_from in varchar2,
        p_email_subject in varchar2
   ) is  
    --
    l_sub_routine varchar2(30) :='sharepoint_integration';
    l_req_id NUMBER :=0;
    --    
  begin    
      --      
        l_req_id := apps.fnd_request.submit_request
                                    (
                                        application =>'XXCUS'
                                       ,program      =>'XXCUS_CONCUR_EMAIL'
                                       ,description =>Null
                                       ,start_time   =>Null --Now
                                       ,sub_request =>FALSE
                                       ,argument1 =>p_file
                                       ,argument2 =>p_sharepoint_email
                                       ,argument3 =>p_dummy_email_from
                                       ,argument4 =>p_email_subject
                                    );
      --
      print_log('@ '||p_emp_group||' email integration to SharePoint , request id :'||l_req_id);
      --
      --Commit;
      --
  exception
   when others then
    print_log('@ '||p_emp_group||', Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
    print_log('Issue in integration to sharepoint : message =>'||sqlerrm);
  end SHAREPOINT_INTEGRATION;
  --    
 procedure SEND_EMAIL_NOTIF
   (
        p_email_from in varchar2,
        p_email_to in varchar2,
        p_email_cc in varchar2,
        p_email_subject in varchar2,
        p_email_body in varchar2,
        p_mime_type in varchar2
   ) is
    l_sub_routine varchar2(30) :='send_email_notif';    
  begin   
      --
      UTL_MAIL.send
          (
            sender          =>p_email_from,
            recipients    =>p_email_to,
            cc                  =>p_email_cc,            
            subject        =>p_email_subject,
            message     =>p_email_body,
            mime_type =>p_mime_type
         );
      --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
    print_log('Issue in sending email notification : message =>'||sqlerrm);
  end SEND_EMAIL_NOTIF;
  --  
  function TRANSFORM_WW_BRANCH (p_project_code VARCHAR2, p_branch in varchar2)  return VARCHAR2 is
    --
    l_sub_routine varchar2(30) :='transform_ww_branch';
    l_branch_code varchar2(10) :=Null;       
    --    
  begin 
     --
        select /*+ RESULT_CACHE */ a.branch_code        
        into     l_branch_code
        from    xxcus.xxcus_concur_ww_br_proj_ref a
        where 1 =1
             and a.project_code =p_project_code
          ;  
     --
     return l_branch_code;
     --
  exception
   when no_data_found then
    return p_branch;  
   when too_many_rows then
    return p_branch;    
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    return p_branch;
  end TRANSFORM_WW_BRANCH;
  --   
  function GET_LINE_NUMBERS_PER_BATCH (p_emp_group VARCHAR2, p_prod_code in varchar2, p_concur_batch_id in number) return VARCHAR2 is
    --
    l_sub_routine varchar2(30) :='get_line_numbers_per_batch';
    l_line_numbers varchar2(600) :=Null;   
    --    
  begin 
     --
        select to_char(listagg(a.line_number, ',') within group (order by a.line_number))
        into     l_line_numbers
        from (
                    select line_number line_number
                    from xxcus.xxcus_concur_eligible_je
                    where 1 =1
                        and crt_btch_id =g_current_batch_id
                        and concur_batch_id =p_concur_batch_id
                        and emp_group =p_emp_group            
                        and allocation_company =p_prod_code  
            ) a
          ;  
     --
     return l_line_numbers;
     --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    return l_line_numbers;
  end GET_LINE_NUMBERS_PER_BATCH;
  --    
  function GET_CURRENCY (p_emp_group VARCHAR2) return VARCHAR2 is
    --
    l_sub_routine varchar2(30) :='get_currency';
    l_currency varchar2(3) :=Null;   
    --    
  begin 
     --
     if p_emp_group IN (
                                             g_WW_emp_grp --WaterWorks 
                                            ,g_GSC_emp_grp  --HDS Global Support Center
                                            ,g_FM_US_emp_grp --Facilities Maintenance US
                                            ,g_CI_US_emp_grp --Construction and Industrial US
                                            ,g_CI_HIS_emp_grp --Construction and Industrial Home Improvement Solutions
                                          ) then
          --
          l_currency :=g_Currency_USD;
          --
     elsif p_emp_group IN (
                                             g_CI_GP_emp_grp --Construction and Industrial Brafasco Canada
                                            ,g_FM_CAD_emp_grp --Facilities Maintenance Canada
                                          ) then
           --
           l_currency := g_Currency_CAD;
           --
     else
          --
          l_currency := 'NONE'; --Anything else return NONE so we can catch it during journal import rather than raising an exception during insert of xxcus_concur_gl_iface table
          --  
     end if;
     --
     return l_currency;
     --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    return l_currency;
  end GET_CURRENCY;
  --    
  function GET_INTERCO_GL_PRODUCT (p_emp_group VARCHAR2) return VARCHAR2 is
    --
    l_sub_routine varchar2(30) :='get_interco_gl_product';
    l_Interco_prod_code varchar2(3) :=Null;   
    --    
  begin 
     --
     if p_emp_group IN (
                                             g_WW_emp_grp --WaterWorks 
                                            ,g_GSC_emp_grp  --HDS Global Support Center
                                            ,g_FM_US_emp_grp --Facilities Maintenance US
                                            ,g_CI_US_emp_grp --Construction and Industrial US
                                            ,g_CI_HIS_emp_grp --Construction and Industrial Home Improvement Solutions
                                          ) then
          --
          l_Interco_prod_code :=g_Interco_US_Product;
          --
     elsif p_emp_group IN (
                                             g_CI_GP_emp_grp --Construction and Industrial Brafasco Canada
                                            ,g_FM_CAD_emp_grp --Facilities Maintenance Canada
                                          ) then
           --
           l_Interco_prod_code := g_Interco_CA_Product;
           --
     else
          --
          l_Interco_prod_code := 'NONE'; --Anything else return NONE so we can catch it during journal import rather than raising an exception during insert of xxcus_concur_gl_iface table
          --  
     end if;
     --
     return l_Interco_prod_code;
     --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    return l_Interco_prod_code;
  end GET_INTERCO_GL_PRODUCT;
  --  
  procedure FLUSH_CONCUR_GL_IFACE (p_emp_group VARCHAR2, p_oop_or_card in varchar2, p_group_id in number)  is
    --
    l_sub_routine varchar2(30) :='flush_concur_gl_iface';
    --    
  begin 
     --
     if p_emp_group IN (
                                             g_WW_emp_grp --WaterWorks 
                                            ,g_CI_GP_emp_grp --Construction and Industrial Brafasco Canada
                                            ,g_FM_US_emp_grp --Facilities Maintenance US
                                            ,g_FM_CAD_emp_grp --Facilities Maintenance Canada                                            
                                          ) then
          --
         begin   
              --
              delete xxcus.xxcus_concur_accrual_gl_iface
              where 1 =1
                   and group_id =p_group_id
                   and status = g_je_iface_status
                   ;
              --
              print_log('@Flush existing Interco journals in concur gl interface table with NEW status  for GROUP_ID : '||p_group_id||', total deleted :'||sql%rowcount);
              -- 
          exception
            when  others then
             print_log('Error in flusing out journals from concur gl interface for emp group :'||p_emp_group||', msg :'||sqlerrm);
             raise program_error;      
         end; 
          --
     elsif p_emp_group IN (
                                                   g_GSC_emp_grp  --HDS Global Support Center
                                                  ,g_CI_US_emp_grp --Construction and Industrial US
                                                  ,g_CI_HIS_emp_grp --Construction and Industrial Home Improvement Solutions
                                               ) then
           --
         begin   
              --
              delete xxcus.xxcus_concur_accrual_gl_iface
              where 1 =1
                   and group_id =p_group_id
                   and status = g_je_iface_status
                   and nvl(reference30,'X') !=g_perz_je_indicator
                   --and reference23 =p_oop_or_card
                   ;
              --
              print_log('@Flush existing Interco journals in concur gl interface table with NEW status  for GROUP_ID : '||p_group_id||', total deleted :'||sql%rowcount);
              -- 
          exception
            when  others then
             print_log('Error in flusing out journals from concur gl interface for emp group :'||p_emp_group||', msg :'||sqlerrm);
             raise program_error;      
         end; 
          --
     else
          --
          Null;
          --  
     end if;
     --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end FLUSH_CONCUR_GL_IFACE;  
  --
  procedure POPULATE_CONCUR_GL_IFACE 
    (
       p_concur_gl_intf_rec in out g_concur_gl_intf_type
      ,p_group_id in number
    ) is
  --
  l_sub_routine varchar2(30) :='populate_concur_gl_iface';
  l_created_by number :=fnd_global.user_id;
  l_creation_date date :=sysdate;    
  l_group_id number :=p_group_id;
  --l_emp_group varchar2(20) :=p_emp_group;
  --l_oop_or_card varchar2(20) :=p_oop_or_card;
  --
  begin
    --
    savepoint last_point;
    --
    if p_concur_gl_intf_rec.count >0 then 
      --
      for idx in 1 .. p_concur_gl_intf_rec.count loop
          --
          p_concur_gl_intf_rec(idx).accounting_date :=g_batch_date; -- TMS: 20170530-00362 / ESMS 561767 
          --
          p_concur_gl_intf_rec(idx).user_je_source_name :=g_JE_Source;
          p_concur_gl_intf_rec(idx).user_je_category_name :=g_JE_Category;              
          --           
          p_concur_gl_intf_rec(idx).status :=g_JE_Iface_Status;
          p_concur_gl_intf_rec(idx).actual_flag :=g_JE_Actual_Flag;
          --
          p_concur_gl_intf_rec(idx).created_by :=l_created_by;
          p_concur_gl_intf_rec(idx).date_created :=l_creation_date;  
          --
          p_concur_gl_intf_rec(idx).attribute1 :=g_hds_gl_iface_status1; --During insert set it to EBS-TRANSFER-PENDING
          --            
          p_concur_gl_intf_rec(idx).reference29 :=g_run_id; --During insert set it to EBS-TRANSFER-PENDING
          --        
          p_concur_gl_intf_rec(idx).reference7 :='Yes'; -- 
          --  
          if p_concur_gl_intf_rec(idx).currency_code = g_Currency_USD then
              p_concur_gl_intf_rec(idx).reference8 :=g_US_rev_accrual_period;        
          elsif p_concur_gl_intf_rec(idx).currency_code = g_Currency_CAD then
              p_concur_gl_intf_rec(idx).reference8 :=g_CA_rev_accrual_period;          
          else
              p_concur_gl_intf_rec(idx).reference8 :=Null;         
          end if;
          --
      end loop;
      --
      forall idx in 1 .. p_concur_gl_intf_rec.count
       insert into xxcus.xxcus_concur_accrual_gl_iface values p_concur_gl_intf_rec(idx);
       --
    else
      --
      print_log('@'||g_pkg_name||'.'||l_sub_routine||', total records in the plsql collection is '||p_concur_gl_intf_rec.count);
      --
    end if;
    --
    commit;
    --
  exception
   when program_error then
    rollback to last_point;
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then
    rollback to last_point;   
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end POPULATE_CONCUR_GL_IFACE;
  --   
  procedure POPULATE_EBS_GL_IFACE
  --Push HDS concur gl interface records to actual Oracle GL interface 
    (
      p_group_id in number
     ,p_bu_desc in varchar2
    ) is
  --
  l_sub_routine varchar2(30) :='populate_ebs_gl_iface';
  l_created_by number :=fnd_global.user_id;
  --
  l_inserted number :=0;
  l_creation_date date :=sysdate;    
  l_group_id number :=p_group_id;  
  --
  cursor c_journals_iface (l_group_id in number) is
  select 
             status
            ,set_of_books_id ledger_id
            ,accounting_date
            ,currency_code
            ,date_created
            ,created_by
            ,actual_flag
            ,user_je_category_name
            ,user_je_source_name
            ,segment1
            ,segment2
            ,segment3
            ,segment4
            ,segment5
            ,segment6
            ,segment7
            ,entered_dr
            ,entered_cr
            ,reference1
            ,reference2
            ,reference4
            ,reference5
            ,reference7
            ,reference8                        
            ,reference10
            ,reference21
            ,reference22
            ,reference23
            ,reference24
            ,g_perz_je_indicator reference30
            ,g_hds_gl_iface_status1 attribute1
            ,attribute2
            ,group_id gl_grp_id 
            ,rowid concur_gl_iface_rowid
  from   xxcus.xxcus_concur_accrual_gl_iface
  where 1 =1
       and user_je_source_name =g_JE_Source
       and user_je_category_name =g_JE_Category
       and group_id =l_group_id
       --and reference29 =g_run_id
       and attribute1 =g_hds_gl_iface_status1 --g_hds_gl_iface_status1 sbala
       --and nvl(reference30, 'X') !=g_perz_je_indicator
   ;
  --
  type c_journals_iface_type is table of c_journals_iface%rowtype index by binary_integer;
  c_journals_iface_rec c_journals_iface_type;
  --
  begin
    --
    print_log(' ');
    print_log('@ POPULATE_EBS_GL_IFACE: G_Run_ID : '||g_run_id);
   --    
      delete  apps.gl_interface
      where 1 =1
           and user_je_source_name =g_JE_Source
           and user_je_category_name =g_JE_Category
           and group_id =l_group_id
           and nvl(attribute1, g_hds_gl_iface_status1) =g_hds_gl_iface_status1
           --and nvl(reference30, 'X') !=g_perz_je_indicator
       ;    
    --         
    print_log('@ '||p_bu_desc||', Deleted existing gl interface records for group id '||p_group_id||', total records :'||sql%rowcount);
    --
    open c_journals_iface (l_group_id =>p_group_id);
    fetch c_journals_iface bulk collect into c_journals_iface_rec;
    close c_journals_iface;
    --
    if c_journals_iface_rec.count >0 then 
        --
        begin 
                --
                 for idx in 1 .. c_journals_iface_rec.count loop 
                    --
                    begin 
                      --
                       insert into apps.gl_interface
                        (
                             status
                            ,ledger_id
                            ,accounting_date
                            ,currency_code
                            ,date_created
                            ,created_by
                            ,actual_flag
                            ,user_je_category_name
                            ,user_je_source_name
                            ,segment1
                            ,segment2
                            ,segment3
                            ,segment4
                            ,segment5
                            ,segment6
                            ,segment7
                            ,entered_dr
                            ,entered_cr
                            ,reference1
                            ,reference2
                            ,reference4
                            ,reference5
                            ,reference7
                            ,reference8                                                                                    
                            ,reference10
                            ,reference21
                            ,reference22
                            ,reference23
                            ,reference24
                            ,group_id  
                            ,attribute1   
                            ,attribute2 
                            ,reference29         
                        )
                       values
                        (
                             c_journals_iface_rec(idx).status
                            ,c_journals_iface_rec(idx).ledger_id
                            ,c_journals_iface_rec(idx).accounting_date
                            ,c_journals_iface_rec(idx).currency_code
                            ,c_journals_iface_rec(idx).date_created
                            ,c_journals_iface_rec(idx).created_by
                            ,c_journals_iface_rec(idx).actual_flag
                            ,c_journals_iface_rec(idx).user_je_category_name
                            ,c_journals_iface_rec(idx).user_je_source_name
                            ,c_journals_iface_rec(idx).segment1
                            ,c_journals_iface_rec(idx).segment2
                            ,c_journals_iface_rec(idx).segment3
                            ,c_journals_iface_rec(idx).segment4
                            ,c_journals_iface_rec(idx).segment5
                            ,c_journals_iface_rec(idx).segment6
                            ,c_journals_iface_rec(idx).segment7
                            ,c_journals_iface_rec(idx).entered_dr
                            ,c_journals_iface_rec(idx).entered_cr
                            ,c_journals_iface_rec(idx).reference1
                            ,c_journals_iface_rec(idx).reference2
                            ,c_journals_iface_rec(idx).reference4
                            ,c_journals_iface_rec(idx).reference5
                            ,c_journals_iface_rec(idx).reference7
                            ,c_journals_iface_rec(idx).reference8                             
                            ,c_journals_iface_rec(idx).reference10
                            ,c_journals_iface_rec(idx).reference21
                            ,c_journals_iface_rec(idx).reference22
                            ,c_journals_iface_rec(idx).reference23
                            ,c_journals_iface_rec(idx).reference24
                            ,c_journals_iface_rec(idx).gl_grp_id     
                            ,g_hds_gl_iface_status1 
                            ,c_journals_iface_rec(idx).attribute2    
                            ,g_run_id      
                        );
                       --
                       l_inserted := l_inserted + sql%rowcount;
                       --
                          update xxcus.xxcus_concur_accrual_gl_iface
                                set attribute1 =g_hds_gl_iface_status2
                          where 1 =1
                               and rowid =c_journals_iface_rec(idx).concur_gl_iface_rowid
                           ;
                       --
                    exception
                     when others then
                      print_log('Failed to insert into Oracle EBS GL interface, concur gl interface row id :'||c_journals_iface_rec(idx).concur_gl_iface_rowid||', message :'||sqlerrm);
                    end;
                    --
                 end loop;
                --        
             --
             commit;
             --
             print_log('  @ '||p_bu_desc||', Total records inserted into gl interface: '||l_inserted);
             --
        exception
         when others then
          rollback;
          print_log('@populate '||sqlerrm);
        end;
        --
    else --c_journals_iface_rec.count =0
         print_log('No [Excluding Personal ] records to push to Oracle GL interface for BU : '||p_bu_desc||', Group ID :'||p_group_id);
    end if; -- if c_journals_iface_rec.count >0 then
    --
    print_log(' ');    
  exception
   when program_error then
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then  
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end POPULATE_EBS_GL_IFACE;
  -- 
  procedure POP_PERSONAL_GL_IFACE
  --Push HDS concur gl interface records to actual Oracle GL interface 
    (
      p_group_id in number
     ,p_bu_desc in varchar2
    ) is
  --
  l_sub_routine varchar2(30) :='pop_personal_gl_iface';
  l_created_by number :=fnd_global.user_id;
  --
  l_inserted number :=0;
  l_creation_date date :=sysdate;    
  l_group_id number :=p_group_id;  
  --
  cursor c_journals_iface (l_group_id in number) is
  select 
             status
            ,set_of_books_id ledger_id
            ,accounting_date
            ,currency_code
            ,date_created
            ,created_by
            ,actual_flag
            ,user_je_category_name
            ,user_je_source_name
            ,segment1
            ,segment2
            ,segment3
            ,segment4
            ,segment5
            ,segment6
            ,segment7
            ,entered_dr
            ,entered_cr
            ,reference1
            ,reference2
            ,reference4
            ,reference5
            ,reference10
            ,reference21
            ,reference22
            ,reference23
            ,reference24
            ,g_perz_je_indicator reference30
            ,g_hds_gl_iface_status1 attribute1
            ,attribute2
            ,group_id gl_grp_id 
            ,rowid concur_gl_iface_rowid
  from   xxcus.xxcus_concur_accrual_gl_iface
  where 1 =1
       and user_je_source_name =g_JE_Source
       and user_je_category_name =g_JE_Category
       and group_id =l_group_id
       and attribute1 =g_hds_gl_iface_status1
       and attribute2 =g_event_type_PEREXP
       and reference29 =g_run_id
       and reference30 =g_perz_je_indicator
   ;
  --
  type c_journals_iface_type is table of c_journals_iface%rowtype index by binary_integer;
  c_journals_iface_rec c_journals_iface_type;
  --
  begin
    --
    print_log('@ POP PERSONAL GL IFACE: G_Run_ID : '||g_run_id);
    --    
      delete  apps.gl_interface
      where 1 =1
           and user_je_source_name =g_JE_Source
           and user_je_category_name =g_JE_Category
           and group_id =l_group_id
           --and nvl(attribute1, g_hds_gl_iface_status1) =g_hds_gl_iface_status1
           and attribute2 =g_event_type_PEREXP
           and nvl(reference30, g_perz_je_indicator) =g_perz_je_indicator
       ;    
    --
    print_log('@ POP PERSONAL GL IFACE: total records deleted from gl interface : '||sql%rowcount);
    --
    open c_journals_iface (l_group_id =>p_group_id);
    fetch c_journals_iface bulk collect into c_journals_iface_rec;
    close c_journals_iface;
    --
    if c_journals_iface_rec.count >0 then 
        --
        begin 
                --
                 for idx in 1 .. c_journals_iface_rec.count loop 
                    --
                    begin 
                      --
                       insert into apps.gl_interface
                        (
                             status
                            ,ledger_id
                            ,accounting_date
                            ,currency_code
                            ,date_created
                            ,created_by
                            ,actual_flag
                            ,user_je_category_name
                            ,user_je_source_name
                            ,segment1
                            ,segment2
                            ,segment3
                            ,segment4
                            ,segment5
                            ,segment6
                            ,segment7
                            ,entered_dr
                            ,entered_cr
                            ,reference1
                            ,reference2
                            ,reference4
                            ,reference5
                            ,reference10
                            ,reference21
                            ,reference22
                            ,reference23
                            ,reference24
                            ,group_id  
                            ,attribute1 
                            ,attribute2  
                            ,reference29     
                            ,reference30     
                        )
                       values
                        (
                             c_journals_iface_rec(idx).status
                            ,c_journals_iface_rec(idx).ledger_id
                            ,c_journals_iface_rec(idx).accounting_date
                            ,c_journals_iface_rec(idx).currency_code
                            ,c_journals_iface_rec(idx).date_created
                            ,c_journals_iface_rec(idx).created_by
                            ,c_journals_iface_rec(idx).actual_flag
                            ,c_journals_iface_rec(idx).user_je_category_name
                            ,c_journals_iface_rec(idx).user_je_source_name
                            ,c_journals_iface_rec(idx).segment1
                            ,c_journals_iface_rec(idx).segment2
                            ,c_journals_iface_rec(idx).segment3
                            ,c_journals_iface_rec(idx).segment4
                            ,c_journals_iface_rec(idx).segment5
                            ,c_journals_iface_rec(idx).segment6
                            ,c_journals_iface_rec(idx).segment7
                            ,c_journals_iface_rec(idx).entered_dr
                            ,c_journals_iface_rec(idx).entered_cr
                            ,c_journals_iface_rec(idx).reference1
                            ,c_journals_iface_rec(idx).reference2
                            ,c_journals_iface_rec(idx).reference4
                            ,c_journals_iface_rec(idx).reference5
                            ,c_journals_iface_rec(idx).reference10
                            ,c_journals_iface_rec(idx).reference21
                            ,c_journals_iface_rec(idx).reference22
                            ,c_journals_iface_rec(idx).reference23
                            ,c_journals_iface_rec(idx).reference24
                            ,c_journals_iface_rec(idx).gl_grp_id     
                            ,g_hds_gl_iface_status2 --changed 4/8 - Bala from status 1 to status 2
                            ,c_journals_iface_rec(idx).attribute2
                            ,g_run_id
                            ,g_perz_je_indicator
                        );
                       --
                       l_inserted := l_inserted + sql%rowcount;
                       --
                          update xxcus.xxcus_concur_accrual_gl_iface
                                set attribute1 =g_hds_gl_iface_status2
                          where 1 =1
                               and rowid =c_journals_iface_rec(idx).concur_gl_iface_rowid
                           ;
                       --
                    exception
                     when others then
                      rollback; 
                      print_log('Failed to insert into Oracle EBS GL interface, concur gl interface row id :'||c_journals_iface_rec(idx).concur_gl_iface_rowid||', message :'||sqlerrm);
                    end;
                    --
                 end loop;
                --        
             --
             commit;
             --
             print_log('Total records inserted into gl interface: '||l_inserted);
             --
        exception
         when others then
          rollback;
          print_log('@populate '||sqlerrm);
        end;
        --
    else --c_journals_iface_rec.count =0
         print_log('No personal expense records to push to Oracle GL interface for BU : '||p_bu_desc||', Group ID :'||p_group_id);
    end if; -- if c_journals_iface_rec.count >0 then
    --
  exception
   when program_error then
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then  
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end POP_PERSONAL_GL_IFACE;
  --  
  procedure PUSH_CONCUR_JE_TO_EBS_GL is
  --
  l_sub_routine varchar2(30) :='push_concur_je_to_ebs_gl';
  l_created_by number :=fnd_global.user_id;
  l_creation_date date :=sysdate;  
  --
  cursor C_GL_ELIGIBLE_BU is
  select  lookup_type
              ,lookup_code
              ,to_number(meaning) gl_iface_group_id
              ,description lookup_desc
              ,nvl(enabled_flag, 'N') enabled_flag
  from   fnd_lookup_values
  where 1 =1
        and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
        and lookup_code !=g_WW_emp_grp --We do not interface WW interco accrual entries to GL so no need to invoke gl interface process pop_ebs_gl_iface
   ;
  --
  type C_GL_ELIGIBLE_BU_TYPE is table of C_GL_ELIGIBLE_BU%rowtype index by binary_integer;
  c_gl_eligible_bu_rec C_GL_ELIGIBLE_BU_TYPE;
  --
  begin
    --
    open C_GL_ELIGIBLE_BU;
    fetch C_GL_ELIGIBLE_BU bulk collect into c_gl_eligible_bu_rec;
    close C_GL_ELIGIBLE_BU;
    --
    if c_gl_eligible_bu_rec.count >0 then 
         --
         for idx in 1 .. c_gl_eligible_bu_rec.count loop 
            --
            if c_gl_eligible_bu_rec(idx).enabled_flag ='Y' then 
              --
              populate_ebs_gl_iface  (p_group_id =>c_gl_eligible_bu_rec(idx).gl_iface_group_id, p_bu_desc =>c_gl_eligible_bu_rec(idx).lookup_desc);
              --
              print_log( c_gl_eligible_bu_rec(idx).lookup_desc||' [Group ID :'||c_gl_eligible_bu_rec(idx).gl_iface_group_id||']'||' concur eligible journals [Excluding Personal] ] to Oracle EBS GL interface transfer is complete.');
              -- 
              if c_gl_eligible_bu_rec(idx).gl_iface_group_id =g_GSC_grp_id then 
               --
               --Null;
               pop_personal_gl_iface  (p_group_id =>c_gl_eligible_bu_rec(idx).gl_iface_group_id, p_bu_desc =>c_gl_eligible_bu_rec(idx).lookup_desc);
               --
               print_log( c_gl_eligible_bu_rec(idx).lookup_desc||' [Group ID :'||c_gl_eligible_bu_rec(idx).gl_iface_group_id||']'||' concur eligible journals [Personal] ] to Oracle EBS GL interface transfer is incomplete.');
               --                
              end if;  
              --           
            else --c_gl_eligible_bu_rec(idx).enabled_flag =N 
              --
              print_log( c_gl_eligible_bu_rec(idx).lookup_desc||' [Group ID :'||c_gl_eligible_bu_rec(idx).gl_iface_group_id||']'||' business unit is not enabled for GL transfer functionality. If required, enable the BU under lookup type XXCUS_CONCUR_ACTIVE_BU from Application Developer \Lookups \Common.');
              --              
            end if;        
            --
         end loop;
         --
    else
     print_log('None of the business units from look up are enabled for GL transfer functionality. Please enable the records from Application Developer \Lookups \Common.');
    end if; 
    --
    print_log(' ');
  exception
   when program_error then
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then  
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end PUSH_CONCUR_JE_TO_EBS_GL;
  --   
  function GET_CI_US_CARD_PROGRAM_NAME return varchar2 is
    --
    l_ci_us_card_pgm varchar2(240) :=Null;
    l_sub_routine varchar2(30) :='get_ci_us_card_program_name';
    --
  begin
      --
        select fnd_profile.value('XXCUS_CONCUR_CI_INV_PROGRAM') 
        into   l_ci_us_card_pgm 
        from   dual;
        --
        print_log ('l_ci_us_card_pgm ='||l_ci_us_card_pgm);      
      --
      if l_ci_us_card_pgm is null then 
       raise program_error;
      end if;
      --
       return l_ci_us_card_pgm;
      --
  exception
    when no_data_found then
     print_log ('@GET_CI_US_CARD_PROGRAM_NAME, error1 : '||sqlerrm);
     raise;    
    when program_error then
     print_log ('@GET_CI_US_CARD_PROGRAM_NAME, error2 : '||sqlerrm);    
     raise;
    when others then
     print_log ('@GET_CI_US_CARD_PROGRAM_NAME, error3 : '||sqlerrm);
     raise;
  end GET_CI_US_CARD_PROGRAM_NAME;    
   --        
  function GET_INBOUND_FILE_LOCATION return varchar2 is
    --
    v_loc varchar2(240) :=Null;
    l_sub_routine varchar2(30) :='GET_INBOUND_FILE_LOCATION';
    --
  begin
      --
      v_loc := 'XXCUS_CONCUR_ACCRUAL_IB';
      --
      begin
        select directory_path
        into     g_directory_path 
        from   dba_directories
        where 1 =1
             and directory_name =v_loc;
        print_log('The absolute path for directory name '||v_loc||' is '||g_directory_path);
      exception
       when others then
        print_log('Failed to fet the directory path for directory name '||g_directory_path||', message :'||sqlerrm);
        raise program_error;
      end;
      --
       return v_loc;
      --
  exception
    when others then
     print_log ('@GET_INBOUND_FILE_LOCATION, error : '||sqlerrm);
     raise program_error;
     raise;
  end GET_INBOUND_FILE_LOCATION;    
   --       
  function GET_OUTBOUND_FILE_LOCATION (v_operation in varchar2, v_emp_group  in varchar2) return varchar2 is
    --
    v_loc varchar2(240) :=Null;
    l_sub_routine varchar2(30) :='get_outbound_file_location';
    l_directory_path varchar2(240) :=Null;
    --
  begin
      --      
      if v_operation ='DBA_DIRECTORY' then
       --
           if v_emp_group =g_WW_MINCRON then 
               -- 
               v_loc := 'XXCUS_CONCUR_WW_ACCRUAL_OB';
               --
           elsif v_emp_group =g_GSC_ORACLE then
               -- 
               v_loc := 'XXCUS_CONCUR_GSC_ACCRUAL_OB';
               --
           elsif v_emp_group =g_FMSAP then
               -- 
               v_loc := 'XXCUS_CONCUR_FM_US_ACCRUAL_OB';
               -- 
           elsif v_emp_group =g_FMCADINFOR then
               -- 
               v_loc := 'XXCUS_CONCUR_FMCAD_ACCRUAL_OB';
               --
           elsif v_emp_group =g_CI_ORACLE then
               -- 
               v_loc := 'XXCUS_CONCUR_CI_US_ACCRUAL_OB';
               --       
           elsif v_emp_group =g_CI_HIS_DYNAMICS then
               -- 
               v_loc := 'XXCUS_CONCUR_CI_HIS_ACCR_OB';
               --
           elsif v_emp_group =g_CI_CAD_GP then
               -- 
               v_loc := 'XXCUS_CONCUR_CI_GP_ACCRUAL_OB';
               --                                                                       
           else
               -- 
               v_loc := Null;
               --
           end if;
       --
      else
               -- 
               v_loc := Null;
               --
      end if;
      --
      begin
        select directory_path
        into     g_directory_path 
        from   dba_directories
        where 1 =1
             and directory_name =v_loc;
        print_log('The absolute path for directory name '||v_loc||' is '||g_directory_path);
      exception
       when others then
        print_log('Failed to fet the directory path for directory name '||g_directory_path||', message :'||sqlerrm);
        raise program_error;
      end;
      --
       return v_loc;
      --
  exception
    when others then
     print_log ('@GET_OUTBOUND_FILE_LOCATION, error : '||sqlerrm);
     return null;
     raise program_error;
  end GET_OUTBOUND_FILE_LOCATION;    
   --  
  procedure GET_LAST_BATCH is
   l_sub_routine varchar2(30) :='get_last_batch';
  begin 
     select nvl(max(crt_btch_id), 0) --nvl(max(concur_batch_id), 0)
     into     g_last_batch_id
     from   xxcus.xxcus_src_concur_sae_n_hist
     where 1 =1;
     --
     if g_last_batch_id >0 then
          select count(crt_btch_id) --count(concur_batch_id)
          into     g_last_batch_count
         from   xxcus.xxcus_src_concur_sae_n_hist
         where 1 =1
              and crt_btch_id =g_last_batch_id
              --and concur_batch_id =g_last_batch_id              
              ;
     else
         g_last_batch_count :=0;     
     end if;
     --      
     print_log('g_last_batch_id [crt_btch_id] : '||g_last_batch_id||', g_last_batch_count :'||g_last_batch_count);
     --    
  exception
   when no_data_found then
    print_log('@no_data_found, Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);  
    g_last_batch_count :=0;
    g_last_batch_id :=0;
   when others then
    print_log('@others, Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    g_last_batch_count :=0;
    g_last_batch_id :=0;    
  end GET_LAST_BATCH;
  --  
  function GET_HISTORY_BATCH_COUNT (l_batch_id number) return number is
    l_batch_count number :=0;
    l_sub_routine varchar2(30) :='get_history_batch_count';    
  begin 
     select count(concur_batch_id)
     into     l_batch_count
     from   xxcus.xxcus_src_concur_sae_n_hist
     where 1 =1
           and concur_batch_id =l_batch_id;
     return l_batch_count;
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    return l_batch_count;
  end GET_HISTORY_BATCH_COUNT;
  --  
  procedure GET_CURRENT_BATCH is
   l_sub_routine varchar2(30) :='get_current_batch';
  begin 
     select nvl(max(crt_btch_id), 0) 
     into     g_current_batch_id
     from   xxcus.xxcus_src_concur_sae_n
     where 1 =1             
            and exists
             (
                  select '1'
                  from    fnd_lookup_values
                  where 1 =1
                        and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
                        and nvl(enabled_flag, 'N') ='Y'
                        and lookup_code =emp_custom21
             )      
     ;
     --
     if g_current_batch_id >0 then
          select count(crt_btch_id) --count(concur_batch_id)
          into     g_current_batch_count
         from   xxcus.xxcus_src_concur_sae_n --xxcus_src_concur_sae_n_hist
         where 1 =1
              and crt_btch_id =g_current_batch_id;
     else
         g_current_batch_count :=0;     
     end if;
     --     
     print_log('g_current_batch_id [crt_btch_id] : '||g_current_batch_id||',  g_current_batch_count :'||g_current_batch_count);
     --        
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    raise program_error;
  end GET_CURRENT_BATCH;
  -- 
  procedure LOAD_ACCRUAL_DATA is
      -- local variables   
      -- =================================================================================  
      input_file_id   UTL_FILE.FILE_TYPE;
      line_read       VARCHAR2(32767):= NULL;
      line_count      NUMBER := 1;
      lines_processed NUMBER :=0;
      --
      print_count     NUMBER :=0;
      b_move_fwd      BOOLEAN;   
      l_sub_routine varchar2(30) :='load_accrual_data';
      --             
    l_emp_id varchar2(48) :=null;
    l_emp_full_name varchar2(80) :=null;
    l_rpt_entry_pmt_code_name varchar2(80) :=null;
    l_program_name varchar2(60) :=null;
    l_approval_status varchar2(60) :=null;
    --
    l_rpt_id varchar2(32) :=null;
    l_rpt_name varchar2(80) :=null;
    l_date_first_submitted date :=null;
    l_rpt_entry_vendor_name varchar2(80) :=null;
    l_posted_date date :=null;
    --
    l_trans_date date :=null;
    l_country varchar2(30) :=null;
    l_reimbursement_currency varchar2(3) :=null;
    l_posted_amount number :=null;
    l_account_code1 varchar2(30) :=null;
    --
    l_account_code2 varchar2(30) :=null;
    l_rpt_org_unit_1 varchar2(48) :=null;
    l_rpt_org_unit_2 varchar2(48) :=null;
    l_rpt_org_unit_3 varchar2(48) :=null;
    l_rpt_org_unit_4 varchar2(48) :=null;
    --
    l_rpt_org_unit_6 varchar2(48) :=null;
    l_personal_flag varchar2(1) :=null;
    l_rpt_entry_pmt_type_code varchar2(4) :=null;
    l_rpt_custom1 varchar2(48) :=null;
    l_rpt_custom2 varchar2(48) :=null;
    --
    l_rpt_custom3 varchar2(48) :=null;
    l_rpt_custom4 varchar2(48) :=null;
    l_rpt_custom6 varchar2(48) :=null;
    l_emp_org_unit1 varchar2(48) :=null;
    l_emp_org_unit2 varchar2(48) :=null;
    --
    l_emp_org_unit3 varchar2(48) :=null;
    l_emp_org_unit4 varchar2(48) :=null;
    l_emp_org_unit6 varchar2(48) :=null;
    l_emp_custom1 varchar2(48) :=null;
    l_emp_custom2 varchar2(48) :=null;
    --
    l_emp_custom3 varchar2(48) :=null;
    l_emp_custom4 varchar2(48) :=null;
    l_emp_custom6 varchar2(48) :=null;
    l_rpt_key number :=null;
    l_expense_type varchar2(64) :=null;
    l_expense_code varchar2(64) :=null;      
    --
    l_command varchar2(2000) :=Null;
    l_result varchar2(2000) :=Null;
    --
  begin 
    --
    g_inbound_loc :=GET_INBOUND_FILE_LOCATION();
    --
    print_log('g_inbound_loc ='||g_inbound_loc);
    --
    begin   
             --
--             l_command :='tr -d '||''''||'\r'||''''||' <'||g_directory_path||'/'||g_file_name||'> '||g_directory_path||'/'||g_file_name||'.nn' ;
--             --
--             print_log ('before cleaning file, l_command ='||l_command);
--             --
--            select xxwc_edi_iface_pkg.xxwc_oscommand_run (l_command)
--             into   l_result
--             from   dual;
--             --
--             print_log('Trim CR LF and other characters...l_result '||l_result);
--             --
--             l_command :='mv '||g_directory_path||'/'||g_file_name||'.nn '||g_directory_path||'/'||g_file_name;
--             --
--             print_log ('affter cleaning file, l_command ='||l_command);
--             --
--            select xxwc_edi_iface_pkg.xxwc_oscommand_run (l_command)
--             into   l_result
--             from   dual;
--             --
--             print_log('Final cleaned, l_result '||l_result);
             --          
             Null;
             --   
    exception
     when program_error then
      print_log (' '||sqlerrm);
      raise;
    end;
    --
    input_file_id  := utl_file.fopen(g_inbound_loc, g_file_name, 'R', 32767);
    print_log ('File opened for reading, file handle succesfully initiated...');
     -- 
     delete xxcus.xxcus_concur_accruals_staging;
     --
      loop                           
           begin 
             --             
             utl_file.get_line(input_file_id, line_read, 32767);
             --             
              if (line_count =1) then
                 Null; --ignore the header line          
                 print_log('header :'||line_count||', Ignore header record.');   
                 --                          
                 line_count:= line_count+1; --set the line number to the next one in the file
                 --                 
              else    
                     --
                     print_log('line :'||line_count); --regexp_replace();
                     --                          
                    --                      
                     begin 
                        --  
                        begin 
                          --  
                            l_emp_id :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 1, line_read, line_count), '([^[:print:]])','');
                            print_log('l_emp_id =>'||l_emp_id);
                            l_emp_full_name :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 2, line_read, line_count), '([^[:print:]])','');
                            print_log('l_emp_full_name =>'||l_emp_full_name);                            
                            l_rpt_entry_pmt_code_name :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 3, line_read, line_count), '([^[:print:]])','');
                            print_log('l_rpt_entry_pmt_code_name =>'||l_rpt_entry_pmt_code_name);                            
                            l_program_name :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 4, line_read, line_count), '([^[:print:]])','');
                            print_log('l_program_name =>'||l_program_name);                            
                            l_approval_status :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 5, line_read, line_count), '([^[:print:]])','');
                            print_log('l_approval_status =>'||l_approval_status);
                          --
                            l_rpt_id :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 6, line_read, line_count), '([^[:print:]])','');
                            print_log('l_rpt_id =>'||l_rpt_id);                            
                            l_rpt_name :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 7, line_read, line_count), '([^[:print:]])','');
                            print_log('l_rpt_name =>'||l_rpt_name);                            
                            --
                            if get_field(xxcus_concur_accruals_pkg.g_delimiter, 8, line_read, line_count) is not null then
                              l_date_first_submitted :=Trunc(to_date(regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 8, line_read, line_count), '([^[:print:]])',''), 'YYYY-MM-DD HH24:MI:SS'));                            
                            else
                              l_date_first_submitted :=Null;
                            end if;
                            --
                            print_log('l_date_first_submitted =>'||l_date_first_submitted);
                            --                            
                            l_rpt_entry_vendor_name :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 9, line_read, line_count), '([^[:print:]])','');
                            --
                            print_log('l_rpt_entry_vendor_name =>'||l_rpt_entry_vendor_name);
                            --                            
                            if get_field(xxcus_concur_accruals_pkg.g_delimiter, 10, line_read, line_count) is not null then
                              l_posted_date :=Trunc(to_date(regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 10, line_read, line_count), '([^[:print:]])',''), 'YYYY-MM-DD HH24:MI:SS'));                            
                            else
                              l_posted_date :=Null;
                            end if;
                            --
                            print_log('l_posted_date =>'||l_posted_date);
                            --       
                            --
                            if get_field(xxcus_concur_accruals_pkg.g_delimiter, 11, line_read, line_count) is not null then
                              l_trans_date :=Trunc(to_date(regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 11, line_read, line_count), '([^[:print:]])',''), 'YYYY-MM-DD HH24:MI:SS'));                            
                            else
                              l_trans_date :=Null;
                            end if;
                            --
                            print_log('l_trans_date =>'||l_trans_date);
                            -- 
                            l_country :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 12, line_read, line_count), '([^[:print:]])','');
                            l_reimbursement_currency :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 13, line_read, line_count), '([^[:print:]])','');
                            l_posted_amount :=to_number(regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 14, line_read, line_count), '([^[:print:]])',''));
                            l_account_code1 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 15, line_read, line_count), '([^[:print:]])','');
                          --
                            l_account_code2 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 16, line_read, line_count), '([^[:print:]])','');
                            l_rpt_org_unit_1 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 17, line_read, line_count), '([^[:print:]])','');
                            l_rpt_org_unit_2 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 18, line_read, line_count), '([^[:print:]])','');
                            l_rpt_org_unit_3 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 19, line_read, line_count), '([^[:print:]])','');
                            l_rpt_org_unit_4 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 20, line_read, line_count), '([^[:print:]])','');         
                          --
                            l_rpt_org_unit_6 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 21, line_read, line_count), '([^[:print:]])','');
                            l_personal_flag :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 22, line_read, line_count), '([^[:print:]])','');
                            l_rpt_entry_pmt_type_code :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 23, line_read, line_count), '([^[:print:]])','');
                            l_rpt_custom1 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 24, line_read, line_count), '([^[:print:]])','');
                            l_rpt_custom2 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 25, line_read, line_count), '([^[:print:]])',''); 
                          --
                            l_rpt_custom3 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 26, line_read, line_count), '([^[:print:]])','');
                            l_rpt_custom4 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 27, line_read, line_count), '([^[:print:]])','');
                            l_rpt_custom6 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 28, line_read, line_count), '([^[:print:]])','');
                            l_emp_org_unit1 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 29, line_read, line_count), '([^[:print:]])','');
                            l_emp_org_unit2 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 30, line_read, line_count), '([^[:print:]])','');     
                          --
                            l_emp_org_unit3 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 31, line_read, line_count), '([^[:print:]])',''); 
                            l_emp_org_unit4 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 32, line_read, line_count), '([^[:print:]])',''); 
                            l_emp_org_unit6 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 33, line_read, line_count), '([^[:print:]])',''); 
                            l_emp_custom1 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 34, line_read, line_count), '([^[:print:]])',''); 
                            l_emp_custom2 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 35, line_read, line_count), '([^[:print:]])','');    
                          --
                            l_emp_custom3 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 36, line_read, line_count), '([^[:print:]])',''); 
                            l_emp_custom4 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 37, line_read, line_count), '([^[:print:]])',''); 
                            l_emp_custom6 :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 38, line_read, line_count), '([^[:print:]])',''); 
                            --l_rpt_key :=to_number(get_field(xxcus_concur_accruals_pkg.g_delimiter, 39, line_read, line_count)); 
                            l_expense_type :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 39, line_read, line_count), '([^[:print:]])',''); 
                            l_expense_code :=regexp_replace(get_field(xxcus_concur_accruals_pkg.g_delimiter, 40, line_read, line_count), '([^[:print:]])','');             
                          --
                          b_move_fwd :=TRUE; --if this happens then we did not have an issue with the insert 
                          --          
                        exception
                          when others then
                            print_log ('Error in get fields, line_count ='||line_count||', msg  ='||sqlerrm);
                            b_move_fwd :=FALSE; 
                        end;                     
                        --    
                        if (b_move_fwd) then 
                            --
                              Insert Into xxcus.xxcus_concur_accruals_staging
                                   (
                                         emp_id
                                        ,emp_full_name
                                        ,rpt_entry_pmt_code_name
                                        ,program_name
                                        ,approval_status
                                        ,rpt_id
                                        ,rpt_name
                                        ,date_first_submitted
                                        ,rpt_entry_vendor_name
                                        ,posted_date
                                        ,trans_date
                                        ,country
                                        ,reimbursement_currency
                                        ,posted_amount
                                        ,account_code1
                                        ,account_code2
                                        ,rpt_org_unit_1
                                        ,rpt_org_unit_2
                                        ,rpt_org_unit_3
                                        ,rpt_org_unit_4
                                        ,rpt_org_unit_6
                                        ,personal_flag
                                        ,rpt_entry_pmt_type_code
                                        ,rpt_custom1
                                        ,rpt_custom2
                                        ,rpt_custom3
                                        ,rpt_custom4
                                        ,rpt_custom6
                                        ,emp_org_unit1
                                        ,emp_org_unit2
                                        ,emp_org_unit3
                                        ,emp_org_unit4
                                        ,emp_org_unit6
                                        ,emp_custom1
                                        ,emp_custom2
                                        ,emp_custom3
                                        ,emp_custom4
                                        ,emp_custom6
                                        ,rpt_key
                                        ,expense_type
                                        ,expense_code       
                                   )
                              Values
                                   (
                                         l_emp_id
                                        ,l_emp_full_name
                                        ,l_rpt_entry_pmt_code_name
                                        ,l_program_name
                                        ,l_approval_status
                                        ,l_rpt_id
                                        ,l_rpt_name
                                        ,l_date_first_submitted
                                        ,l_rpt_entry_vendor_name
                                        ,l_posted_date
                                        ,l_trans_date
                                        ,l_country
                                        ,l_reimbursement_currency
                                        ,l_posted_amount
                                        ,l_account_code1
                                        ,l_account_code2
                                        ,l_rpt_org_unit_1
                                        ,l_rpt_org_unit_2
                                        ,l_rpt_org_unit_3
                                        ,l_rpt_org_unit_4
                                        ,l_rpt_org_unit_6
                                        ,l_personal_flag
                                        ,l_rpt_entry_pmt_type_code
                                        ,l_rpt_custom1
                                        ,l_rpt_custom2
                                        ,l_rpt_custom3
                                        ,l_rpt_custom4
                                        ,l_rpt_custom6
                                        ,l_emp_org_unit1
                                        ,l_emp_org_unit2
                                        ,l_emp_org_unit3
                                        ,l_emp_org_unit4
                                        ,l_emp_org_unit6
                                        ,l_emp_custom1
                                        ,l_emp_custom2
                                        ,l_emp_custom3
                                        ,l_emp_custom4
                                        ,l_emp_custom6
                                        ,l_rpt_key
                                        ,l_expense_type
                                        ,l_expense_code                           
                                   );                       
                            --                        
                        end if; -- if (b_move_fwd) then 
                        --
                     exception
                      when others then 
                       print_log ('error in reading line, line_count ='||line_count||', msg :'||sqlerrm);
                       raise program_error;
                       raise;          
                     end;
                    --
                 --                          
                 line_count:= line_count+1; --set the line number to the next one in the file
                 --                    
              end if;            
             --          
           exception
                 when no_data_found then                             
                   exit;
                 when others then
                   raise_application_error(-20010,'Line: '||line_read);
                   raise_application_error(-20010,' Unknown Errors: '||sqlerrm);
                   exit;
           end;         
      end loop;     
     --
    utl_file.fclose(input_file_id);          
    print_log ('After file close.');   
    --     
  exception
     when utl_file.invalid_path then
      raise_application_error(-20010,'File: '||g_file_name||' Invalid Path: '||sqlerrm);
     when utl_file.invalid_mode then
      raise_application_error(-20010,'File: '||g_file_name||' Invalid Mode: '||sqlerrm);
     when utl_file.invalid_operation then
      raise_application_error(-20010,'File: '||g_file_name||' Invalid Operation: '||sqlerrm);
     when utl_file.invalid_filehandle then
      raise_application_error(-20010,'File: '||g_file_name||' Invalid File Handle: '||sqlerrm);
     when utl_file.read_error then
      raise_application_error(-20010,'File: '||g_file_name||' File Read Error: '||sqlerrm);
     when utl_file.internal_error then
      raise_application_error(-20010,'File: '||g_file_name||' File Internal Error: '||sqlerrm);  
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    raise program_error;
    raise;
  end LOAD_ACCRUAL_DATA;
  --   
  -- BEGIN TMS: 20170530-00362 / ESMS 561767
  function GET_MAX_TRANS_DATE return date is
    l_batch_date date :=null;
    l_sub_routine varchar2(30) :='get_max_trans_date';    
  begin 
     select max(trunc(trans_date))
     into     l_batch_date
     from   xxcus.xxcus_concur_accruals_staging
     where 1 =1
     ;
     --
     if l_batch_date is null then  
      raise_application_error(-20001, 'Not able to derive max of posted date used for accounting the accruals');
     end if; 
     --
     print_log('  ');
     print_log(' MAX POSTED DATE : '||l_batch_date);
     print_log('  ');
     --
     return l_batch_date;
     --
  exception
   when no_data_found then
    raise program_error;
     return l_batch_date;    
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    return l_batch_date;
  end GET_MAX_TRANS_DATE;
  --   
  -- END TMS: 20170530-00362 / ESMS 561767
  --  
  procedure FETCH_AND_UPDATE_ATTRIBUTES is
    -- 
    l_sub_routine varchar2(30) :='fetch_accrual_period_info';    
    l_request_id number :=fnd_global.conc_request_id;
    l_count number :=0;
    --  
  begin 
     --     
        select 
                   period_name
                  ,start_date
                  ,end_date
                  ,period_num
                  ,(
                    select   period_name
                    from    apps.gl_period_statuses
                    where 1 =1
                        and  application_id =a.application_id
                        and  ledger_id =a.set_of_books_id
                        and  effective_period_num =a.effective_period_num + 1          
                   ) rev_period
        into
                   g_US_accrual_period
                  ,g_US_fiscal_start
                  ,g_US_fiscal_end   
                  ,g_period_num 
                  ,g_US_rev_accrual_period  
        from apps.gl_period_statuses a
        where 1 =1
        and a.application_id =101
        and a.set_of_books_id =g_US_ledger_id
        --and a.closing_status ='O' --Open period only -- TMS: 20170530-00362 / ESMS 561767
        and g_batch_date between a.start_date and a.end_date -- TMS: 20170530-00362 / ESMS 561767
        and a.adjustment_period_flag =g_N
           ;
     --
        select 
                   period_name
                  ,start_date
                  ,end_date
                  ,period_num
                  ,(
                    select   period_name
                    from    apps.gl_period_statuses
                    where 1 =1
                        and  application_id =a.application_id
                        and  ledger_id =a.set_of_books_id
                        and  effective_period_num =a.effective_period_num + 1          
                   ) rev_period
        into
                   g_CA_accrual_period
                  ,g_CA_fiscal_start
                  ,g_CA_fiscal_end 
                  ,g_period_num  
                  ,g_CA_rev_accrual_period
        from apps.gl_period_statuses a
        where 1 =1
        and a.application_id =101
        and a.set_of_books_id =g_CAD_ledger_id
        --and a.closing_status ='O' --Open period only -- TMS: 20170530-00362 / ESMS 561767
        and g_batch_date between a.start_date and a.end_date -- TMS: 20170530-00362 / ESMS 561767
        and a.adjustment_period_flag =g_N
           ;
     --     
     print_log(' ');     
     print_log('g_US_accrual_period :  '||g_US_accrual_period);
     print_log('g_US_rev_accrual_period :  '||g_US_rev_accrual_period);          
--     print_log('g_US_currency :  '||g_US_currency);
     print_log('g_US_fiscal_start :  '||to_char(g_US_fiscal_start, 'MM/DD/YYYY'));
     print_log('g_US_fiscal_end :  '||to_char(g_US_fiscal_end, 'MM/DD/YYYY'));               
     print_log(' ');
     print_log('g_CA_accrual_period :  '||g_CA_accrual_period);
     print_log('g_CA_rev_accrual_period :  '||g_CA_rev_accrual_period);          
--     print_log('g_CA_currency :  '||g_CA_currency);
     print_log('g_CA_fiscal_start :  '||to_char(g_CA_fiscal_start, 'MM/DD/YYYY'));
     print_log('g_CA_fiscal_end :  '||to_char(g_CA_fiscal_end, 'MM/DD/YYYY'));               
     print_log(' ');     
     --
     UPDATE xxcus.xxcus_concur_accruals_staging
     SET account_code1 ='7900-000'
            ,rpt_org_unit_4 ='030'
     WHERE 1 =1
            AND nvl(rpt_org_unit_1, emp_org_unit1) =g_WW_emp_grp     
            AND nvl(account_code1, account_code2) is null
      ;     
     --
     print_log(' ');       
     print_log('Updated staging table xxcus_concur_accruals_staging with default cost center and account code for ERP =>'|| g_WW_emp_grp||', rows updated : '||sql%rowcount);
     print_log(' ');
     --     
     UPDATE xxcus.xxcus_concur_accruals_staging
     SET account_code1 ='664010'
     WHERE 1 =1
            AND nvl(rpt_org_unit_1, emp_org_unit1) =g_GSC_emp_grp     
            AND nvl(account_code1, account_code2) is null
      ; 
      --
     print_log(' ');       
     print_log('Updated staging table xxcus_concur_accruals_staging with default account code for ERP =>'|| g_GSC_emp_grp||', rows updated : '||sql%rowcount);
     print_log(' ');
     --          
     UPDATE xxcus.xxcus_concur_accruals_staging
     SET account_code1 ='501808'
     WHERE 1 =1
            AND nvl(rpt_org_unit_1, emp_org_unit1) =g_CI_US_emp_grp     
            AND nvl(account_code1, account_code2) is null
            AND rpt_entry_pmt_code_name =g_CI_US_INV_Program
      ;   
      --
     print_log(' ');       
     print_log('Updated staging table xxcus_concur_accruals_staging with account code for ERP =>'|| g_CI_US_emp_grp||' and Inventory card program, rows updated : '||sql%rowcount);
     print_log(' ');
     --        
     UPDATE xxcus.xxcus_concur_accruals_staging
     SET account_code1 ='658020'
     WHERE 1 =1
            AND nvl(rpt_org_unit_1, emp_org_unit1) =g_CI_US_emp_grp     
            AND nvl(account_code1, account_code2) is null
            AND rpt_entry_pmt_code_name !=g_CI_US_INV_Program            
      ;
      --
     print_log(' ');       
     print_log('Updated staging table xxcus_concur_accruals_staging with account code for ERP =>'|| g_CI_US_emp_grp||' and all other programs exception Inventory card, rows updated : '||sql%rowcount);
     print_log(' ');
     --                  
     --
     UPDATE xxcus.xxcus_concur_accruals_staging
     SET account_code1 ='000-78601'
     WHERE 1 =1
            AND nvl(rpt_custom1, emp_custom1) =g_CI_GP_emp_grp     
            AND nvl(account_code1, account_code2) is null
      ;     
     --
     print_log(' ');       
     print_log('Updated staging table with default cost center and account code for ERP =>'|| g_CI_GP_emp_grp||', rows updated : '||sql%rowcount);
     print_log(' ');
     -- 
     -- BEGIN TMS: 20170530-00362 / ESMS 561767   
     UPDATE xxcus.xxcus_concur_accruals_staging
     SET rpt_entry_pmt_type_code
             = case 
                  when rpt_entry_pmt_code_name like '%WW' then 'IBIP'
                  else 'CBCP'     
                end  
     WHERE 1 =1
          AND trim(rpt_entry_pmt_type_code) IS NULL
     ;      
     print_log(' ');       
     print_log('Updated staging table with default rpt_entry_pmt_type_code when blank, rows updated : '||sql%rowcount);
     print_log(' ');     
     --       
     -- END TMS: 20170530-00362 / ESMS 561767 
     --        
     UPDATE xxcus.xxcus_concur_accruals_staging
     SET run_id =g_run_id
           ,request_id =l_request_id
           ,accrual_period_num =g_period_num
           ,accrual_erp =case
                                              when reimbursement_currency =g_Currency_USD then nvl(rpt_org_unit_1, emp_org_unit1)
                                              when reimbursement_currency =g_Currency_CAD then nvl(rpt_custom1, emp_custom1)
                                              else Null                                                                                            
                                            end
           ,accrual_period =case
                                              when reimbursement_currency =g_Currency_USD then g_US_accrual_period
                                              when reimbursement_currency =g_Currency_CAD then g_CA_accrual_period
                                              else Null                                                                                            
                                            end 
           ,accrual_fiscal_start =case
                                              when reimbursement_currency =g_Currency_USD then g_US_fiscal_start
                                              when reimbursement_currency =g_Currency_CAD then g_CA_fiscal_start
                                              else Null                                                                                            
                                            end   
           ,accrual_fiscal_end =case
                                              when reimbursement_currency =g_Currency_USD then g_US_fiscal_end
                                              when reimbursement_currency =g_Currency_CAD then g_CA_fiscal_end
                                              else Null                                                                                            
                                            end
           ,reversal_accrual_period =case
                                              when reimbursement_currency =g_Currency_USD then g_US_rev_accrual_period
                                              when reimbursement_currency =g_Currency_CAD then g_CA_rev_accrual_period
                                              else Null                                                                                            
                                            end
           ,ledger_id =case
                                  when reimbursement_currency =g_Currency_USD then g_US_ledger_id
                                  when reimbursement_currency =g_Currency_CAD then g_CAD_ledger_id
                                  else Null                                                                                            
                                end  
           ,segment1 =case
                                              when reimbursement_currency =g_Currency_USD then nvl(rpt_org_unit_2, emp_org_unit2)
                                              when reimbursement_currency =g_Currency_CAD then nvl(rpt_custom2, emp_custom2)
                                              else Null                                                                                            
                                            end 
           ,segment2 =case
                                              when reimbursement_currency =g_Currency_USD AND nvl(rpt_org_unit_1, emp_org_unit1) =g_WW_emp_grp then LPAD(nvl(rpt_org_unit_3, emp_org_unit3),3,'0')
                                              when reimbursement_currency =g_Currency_USD AND nvl(rpt_org_unit_1, emp_org_unit1) !=g_WW_emp_grp then nvl(rpt_org_unit_3, emp_org_unit3)                                              
                                              when reimbursement_currency =g_Currency_CAD then lpad(nvl(rpt_custom3, emp_custom3), 3, '0')
                                              else Null                                                                                            
                                            end 
           ,segment3 =case
                                              when reimbursement_currency =g_Currency_USD AND nvl(rpt_org_unit_1, emp_org_unit1) =g_WW_emp_grp then LPAD(nvl(rpt_org_unit_4, emp_org_unit4), 3, '0')
                                              when reimbursement_currency =g_Currency_USD AND nvl(rpt_org_unit_1, emp_org_unit1) !=g_WW_emp_grp then nvl(rpt_org_unit_4, emp_org_unit4) 
                                              when reimbursement_currency =g_Currency_CAD then nvl(rpt_custom4, emp_custom4)
                                              else Null                                                                                            
                                            end  
           ,segment4 =nvl(account_code1, account_code2) 
           ,segment5 =case
                                              when reimbursement_currency =g_Currency_USD then nvl(rpt_org_unit_6, emp_org_unit6)
                                              when reimbursement_currency =g_Currency_CAD then nvl(rpt_custom6, emp_custom6)
                                              else Null                                                                                            
                                            end  
         ,OOP_CARD =
          case
            when rpt_entry_pmt_type_code in ('CASH', 'IBIP') then 'OOP'
            when rpt_entry_pmt_type_code in ('IBCP','CBCP') then 'CARD'
            else 'NONE'
          end                                                                                                                                                                                                                                                                                                      
          ;
     --
     print_log(' ');       
     print_log('Updated staging table with additional attributes, rows updated : '||sql%rowcount);
     -- 
     UPDATE xxcus.xxcus_concur_accruals_staging
     SET segment5 ='00000'
     WHERE 1 =1
          AND segment5 is null
      ;     
     --
     print_log(' ');       
     print_log('Updated staging table with default project 00000 for all Concur ERP, rows updated : '||sql%rowcount);
     print_log(' ');
     -- 
     UPDATE xxcus.xxcus_concur_accruals_staging
     SET segment3 ='0000'
     WHERE 1 =1
          AND segment3 ='0'
      ;           
     --
     UPDATE xxcus.xxcus_concur_accruals_staging
     SET segment5 ='00000'
     WHERE 1 =1
          AND segment5 ='0'
      ;  
     -- 
     UPDATE xxcus.xxcus_concur_accruals_staging
     SET personal_flag =g_N
     WHERE 1 =1
          AND personal_flag is null
      ;
    --
     select count(1)
     into l_count
     from xxcus.xxcus_concur_accruals_staging
     WHERE 1 =1
            AND (
                          accrual_erp is null OR
                          segment1 is null OR
                          segment1 = 'N/A' OR
                          segment2 is null OR
                          segment2 = 'N/A' OR                          
                          segment3 is null OR
                          segment3 = 'N/A' OR                            
                          segment4 is null OR
                          segment4 = 'N/A' OR                    
                          segment5 is null OR
                          segment5 = 'N/A'                          
                      ); 
     --
     print_log(' ');         
     print_log('Checking to see if we still have any records with accrual erp or segment1 thru segment5 empty OR N/A : total records found =>'||l_count);         
     --
     UPDATE xxcus.xxcus_concur_accruals_staging
     SET validation_failed =g_Y
     WHERE 1 =1
            AND (
                          accrual_erp is null OR
                          segment1 is null OR
                          segment2 is null OR
                          segment3 is null OR
                          segment4 is null OR
                          segment5 is null
                      )
     ;     
     --
     print_log(' ');       
     print_log('Updated staging table with validation_failed flag value of Y, rows updated : '||sql%rowcount);
     print_log(' ');       
     --     
     commit;
     --
  exception
   when no_data_found then
    raise program_error;
    raise;
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    g_batch_date :=Null;
  end FETCH_AND_UPDATE_ATTRIBUTES; --TMS: 20170530-00362 / ESMS 561767
  --    
  procedure FETCH_CONCUR_BATCH_DATE  is 
  --Set global variable g_batch_date, g_concur_batch_id, g_fiscal_period, g_fiscal_start, g_fiscal_end
    l_sub_routine varchar2(30) :='fetch_concur_batch_date';    
  begin 
     --
     select batch_date
                ,concur_batch_id
                ,b.period_name
                ,b.start_date
                ,b.end_date
     into    g_batch_date
               ,g_concur_batch_id
               ,g_fiscal_period
               ,g_fiscal_start
               ,g_fiscal_end               
     from   xxcus.xxcus_concur_sae_header a
                ,gl_periods b
     where 1 =1
           and crt_btch_id =g_current_batch_id
           and batch_date between b.start_date and b.end_date
           and b.adjustment_period_flag ='N' 
           ;
     --
     print_log(' ');     
     print_log('g_batch_date :  '||to_char(g_batch_date, 'MM/DD/YYYY'));
     print_log('g_concur_batch_id :  '||g_concur_batch_id);          
     print_log('g_fiscal_period :  '||g_fiscal_period);
     print_log('g_fiscal_start :  '||to_char(g_fiscal_start, 'MM/DD/YYYY'));
     print_log('g_fiscal_end :  '||to_char(g_fiscal_end, 'MM/DD/YYYY'));               
     print_log(' ');
     --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    g_batch_date :=Null;
  end FETCH_CONCUR_BATCH_DATE;
  --  
  procedure SET_SAE_HDR_STATUS (p_status in varchar2)  is 
    --
    l_sub_routine varchar2(30) :='set_sae_hdr_status';
    l_user_id number :=fnd_global.user_id;
    l_date date :=sysdate;    
    l_request_id number :=fnd_global.conc_request_id;
   --
  begin  
     --
     savepoint begin_here;
     --
     UPDATE xxcus.xxcus_concur_sae_header
     SET batch_status = p_status
            ,last_updated_by =l_user_id
            ,last_update_date =l_date
            ,request_id =l_request_id
            ,run_id =g_run_id
            ,fiscal_period =g_fiscal_period
     where 1 =1
           and crt_btch_id =g_current_batch_id;
    --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    rollback to begin_here;
  end SET_SAE_HDR_STATUS;
  --    
  function GET_EBS_USER_ID (p_user_name in varchar2) return number is
   l_sub_routine varchar2(30) :='get_ebs_user_id';
   l_user_id number :=Null;   
  begin 
     select user_id
     into     l_user_id
     from   apps.fnd_user
     where 1 =1
           and user_name =p_user_name;
     --
     return l_user_id;
     --  
  exception
   when no_data_found then
    return l_user_id;
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);    
    return l_user_id;
  end GET_EBS_USER_ID;
  --  
  function GET_WW_JE_SEQ (p_fiscal_period in varchar2) return varchar2 is
   l_sub_routine varchar2(30) :='get_ww_je_seq';
   l_begin_je_number number :=3890;
   l_last_je_number number :=Null;   
   l_je_number number :=Null;
  begin 
     select max(last_je_number)
     into     l_last_je_number
     from   xxcus.xxcus_concur_ww_je_controls
     where 1 =1
           and fiscal_period =p_fiscal_period;
     --
     if l_last_je_number is not null then 
      --
      l_je_number :=l_last_je_number+1;
     else
      l_je_number :=l_begin_je_number;
     end if;
     --  
     print_log('Fiscal period :'||p_fiscal_period||', returned JE# '||l_je_number);
     --
     return l_je_number;
     --  
  exception
   when no_data_found then
    return l_begin_je_number;
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);    
    raise program_error;
  end GET_WW_JE_SEQ;
  --
  procedure ARCHIVE_CURRENT_BATCH (p_archive_flag in varchar2, p_archive_date in date) is
  --
    l_sub_routine varchar2(30) :='archive_current_batch';  
    l_total number :=0;
  --
  type t_concur_sae_n_hist_tbl is table of  xxcus.xxcus_src_concur_sae_n_hist%rowtype index by binary_integer;   
  --
   t_concur_sae_n_hist_rec t_concur_sae_n_hist_tbl;
   --
   cursor c_last_batch is
   select 
      (
         select /*+ RESULT_CACHE */ 
                     period_name
         from  apps.gl_periods
         where 1 =1
               and concur_batch_date between start_date and end_date
               and adjustment_period_flag ='N'
      ) fiscal_period
    ,p_archive_flag  ebs_processed_flag
    ,p_archive_date ebs_processed_date
    ,row_type
    ,concur_batch_id
    ,concur_batch_date
    ,line_number
    ,emp_id
    ,emp_last_name
    ,emp_first_name
    ,emp_mi
    ,emp_custom21
    ,emp_org_unit1
    ,emp_org_unit2
    ,emp_org_unit3
    ,emp_org_unit4
    ,emp_org_unit5
    ,emp_org_unit6
    ,emp_bank_acct_number
    ,emp_bank_routing_number
    ,amt_net_tax_tot_rclm
    ,rpt_id
    ,rpt_key
    ,ledger_ledger_code
    ,emp_currency_alpha_code
    ,country_lang_name
    ,rpt_submit_date
    ,rpt_user_defined_date
    ,rpt_pmt_processing_date
    ,rpt_name
    ,rpt_image_required
    ,rpt_has_vat_entry
    ,rpt_has_ta_entry
    ,rpt_vw_tot_posted_amt
    ,rpt_vw_tot_approved_amt
    ,policy_lang_name
    ,rpt_entry_bdgt_accrual_date
    ,rpt_org_unit_1
    ,rpt_org_unit_2
    ,rpt_org_unit_3
    ,rpt_org_unit_4
    ,rpt_org_unit_5
    ,rpt_org_unit_6
    ,rpt_custom1
    ,rpt_custom2
    ,rpt_custom3
    ,rpt_custom4
    ,rpt_custom5
    ,rpt_custom6
    ,rpt_custom7
    ,rpt_custom8
    ,rpt_custom9
    ,rpt_custom10
    ,rpt_custom11
    ,rpt_custom12
    ,rpt_custom13
    ,rpt_custom14
    ,rpt_custom15
    ,rpt_custom16
    ,rpt_custom17
    ,rpt_custom18
    ,rpt_custom19
    ,rpt_custom20
    ,rpt_entry_key
    ,rpt_entry_trans_type
    ,rpt_entry_expense_type_name
    ,rpt_entry_trans_date
    ,rpt_entry_currency_alpha_code
    ,rpt_entry_exchange_rate
    ,rpt_entry_exchg_rt_direction
    ,rpt_entry_is_personal
    ,rpt_entry_desc
    ,rpt_entry_vendor_name
    ,rpt_entry_vendor_desc
    ,rpt_entry_receipt_received
    ,rpt_entry_receipt_type
    ,tot_emp_attendee
    ,tot_spouse_attendee
    ,tot_business_attendee
    ,rpt_entry_org_unit_1
    ,rpt_entry_org_unit_2
    ,rpt_entry_org_unit_3
    ,rpt_entry_org_unit_4
    ,rpt_entry_org_unit_5
    ,rpt_entry_org_unit_6
    ,rpt_entry_custom1
    ,rpt_entry_custom2
    ,rpt_entry_custom3
    ,rpt_entry_custom4
    ,rpt_entry_custom5
    ,rpt_entry_custom6
    ,rpt_entry_custom7
    ,rpt_entry_custom8
    ,rpt_entry_custom9
    ,rpt_entry_custom10
    ,rpt_entry_custom11
    ,rpt_entry_custom12
    ,rpt_entry_custom13
    ,rpt_entry_custom14
    ,rpt_entry_custom15
    ,rpt_entry_custom16
    ,rpt_entry_custom17
    ,rpt_entry_custom18
    ,rpt_entry_custom19
    ,rpt_entry_custom20
    ,rpt_entry_custom21
    ,rpt_entry_custom22
    ,rpt_entry_custom23
    ,rpt_entry_custom24
    ,rpt_entry_custom25
    ,rpt_entry_custom26
    ,rpt_entry_custom27
    ,rpt_entry_custom28
    ,rpt_entry_custom29
    ,rpt_entry_custom30
    ,rpt_entry_custom31
    ,rpt_entry_custom32
    ,rpt_entry_custom33
    ,rpt_entry_custom34
    ,rpt_entry_custom35
    ,rpt_entry_custom36
    ,rpt_entry_custom37
    ,rpt_entry_custom38
    ,rpt_entry_custom39
    ,rpt_entry_custom40
    ,rpt_entry_trans_amt
    ,rpt_entry_posted_amt
    ,rpt_entry_approved_amt
    ,rpt_entry_pmt_type_code
    ,rpt_entry_pmt_code_name
    ,rpt_pmt_reimbursement_type
    ,cc_trans_billing_date
    ,billed_cc_acct_number
    ,billed_cc_acct_desc
    ,cc_trans_jr_key
    ,cc_trans_ref_no
    ,cc_trans_cct_key
    ,cc_trans_cct_type
    ,cc_trans_trans_id
    ,cc_trans_trans_amt
    ,cc_trans_tax_amt
    ,cc_trans_trans_curncy
    ,cc_trans_posted_amt
    ,cc_trans_posted_currency
    ,cc_trans_trans_date
    ,cc_trans_posted_date
    ,cc_trans_desc
    ,cc_trans_mc_code
    ,cc_trans_merchant_name
    ,cc_trans_merchant_city
    ,cc_trans_merchant_state
    ,cc_trans_merchant_ctry_code
    ,cc_trans_merchant_ref_num
    ,cc_trans_billing_type
    ,bill_to_emp_exchange_rt
    ,journal_billing_amt
    ,individual_cc_acct_number
    ,individual_cc_acct_name
    ,cc_acct_doing_business_as
    ,cc_trans_acquirer_ref_no
    ,rpt_entry_loc_ctry_code
    ,rpt_entry_loc_ctry_sub_code
    ,rpt_entry_foreign_domestic
    ,cc_acct_provider_market
    ,cc_trans_processor_ref_no
    ,journal_payer_pmt_type_name
    ,journal_payer_pmt_code_name
    ,journal_payee_pmt_type_name
    ,journal_payee_pmt_code_name
    ,journal_acct_code
    ,journal_debit_or_credit
    ,journal_amt
    ,journal_rpj_key
    ,car_log_entry_business_dist
    ,car_log_entry_personal_dist
    ,car_log_entry_passenger_count
    ,car_vehicle_id
    ,cc_trans_sales_tax_amt
    ,eft_batch_cc_vendor_name
    ,cash_adv_req_amt
    ,cash_adv_req_curr_alpha_code
    ,cash_adv_req_curr_num_code
    ,cash_adv_exchange_rate
    ,cash_adv_currency_alpha_code
    ,cash_adv_currency_num_code
    ,cash_adv_issued_date
    ,cash_adv_pmt_code_name
    ,cash_adv_trans_type
    ,cash_adv_req_date
    ,cash_adv_ca_key
    ,cash_adv_pmt_method
    ,allocation_key
    ,allocation_pct
    ,allocation_custom1
    ,allocation_custom2
    ,allocation_custom3
    ,allocation_custom4
    ,allocation_custom5
    ,allocation_custom6
    ,allocation_custom7
    ,allocation_custom8
    ,allocation_custom9
    ,allocation_custom10
    ,allocation_custom11
    ,allocation_custom12
    ,allocation_custom13
    ,allocation_custom14
    ,allocation_custom15
    ,allocation_custom16
    ,allocation_custom17
    ,allocation_custom18
    ,allocation_custom19
    ,allocation_custom20
    ,amt_net_tax_tot_adj
    ,ta_reimburs_meal_lodging_code
    ,ta_reimburs_display_limit
    ,ta_reimburs_allowance_limit
    ,ta_reimburs_allow_threshold
    ,ta_fixed_meal_lodging_type
    ,ta_fixed_base_amt
    ,ta_fixed_allowance_amt
    ,ta_fixed_overnight
    ,ta_fixed_breakfast_provided
    ,ta_fixed_lunch_provided
    ,ta_fixed_dinner_provided
    ,tax_alloc_tot_vw_adj_amt
    ,taxaloc_tot_vw_rclm_adj_amt
    ,tax_auth_lang_auth_name
    ,tax_auth_lang_tax_label
    ,rpt_entry_tax_trans_amt
    ,rpt_entry_tax_posted_amt
    ,rpt_entry_tax_source
    ,rpt_ent_tax_rclm_trans_amt
    ,rpt_entry_tax_rclm_post_amt
    ,rpt_entry_tax_tax_code
    ,tax_config_rclm_domestic
    ,rpt_entry_tax_adj_amt
    ,rpt_entry_tax_rclm_adj_amt
    ,rpt_entry_tax_rclm_code
    ,rpt_ent_tax_rclm_trns_adj_amt
    ,rpt_entry_tax_alloc_rclm_cd
    ,auth_req_req_id
    ,auth_req_req_name
    ,auth_req_tot_posted_amt
    ,auth_req_tot_apprvd_amt
    ,auth_req_start_date
    ,auth_req_end_date
    ,auth_req_auth_date
    ,rpt_entry_tot_tax_posted_amt
    ,rpt_entry_vw_net_tax_amt
    ,rpt_entry_tot_rclm_adj_amt
    ,rpt_ent_vw_net_rclm_adj_amt
    ,rpt_entry_pmt_type_name
    ,card_pgm_type_code
    ,card_pgm_statement_start_date
    ,card_pgm_statement_end_date
    ,eft_batch_cash_acct_code
    ,eft_batch_liability_acct_cd
    ,estimated_pmt_date
    ,eft_pmt_funding_trace
    ,cash_adv_req_disburse_date
    ,cash_adv_name
    ,future_use_260
    ,future_use_261
    ,future_use_262
    ,future_use_263
    ,emp_login_id
    ,emp_email_address
    ,emp_custom1
    ,emp_custom2
    ,emp_custom3
    ,emp_custom4
    ,emp_custom5
    ,emp_custom6
    ,emp_custom7
    ,emp_custom8
    ,emp_custom9
    ,emp_custom10
    ,emp_custom11
    ,emp_custom12
    ,emp_custom13
    ,emp_custom14
    ,emp_custom15
    ,emp_custom16
    ,emp_custom17
    ,emp_custom18
    ,emp_custom19
    ,emp_custom20
    ,future_use_286
    ,future_use_287
    ,future_use_288
    ,future_use_289
    ,future_use_290
    ,emp_bank_is_active
    ,eft_bank_acct_type
    ,emp_bank_name_on_acct
    ,emp_bank_bank_name
    ,emp_bank_branch_location
    ,emp_bank_address_line_1
    ,emp_bank_address_line_2
    ,emp_bank_city
    ,emp_bank_region
    ,emp_bank_postal_code
    ,emp_bank_ctry_code
    ,emp_bank_tax_id
    ,emp_bank_is_resident
    ,emp_bank_name
    ,emp_bank_currency_alpha_code
    ,future_use_306
    ,future_use_307
    ,future_use_308
    ,future_use_309
    ,future_use_310
    ,rpt_vw_tot_personal_amt
    ,rpt_vw_tot_claimed_amt
    ,rpt_vw_tot_due_emp
    ,rpt_vw_tot_due_cmpny_card
    ,rpt_vw_tot_rejected_amt
    ,rpt_vw_tot_cmpny_paid
    ,rpt_vw_tot_due_cmpny
    ,rpt_vw_tot_confirmed_paid
    ,rpt_vw_actual_tot_due_emp
    ,rpt_vw_actl_tot_due_cmp_crd
    ,rpt_report_type
    ,rpt_entry_net_adj_tax_amt
    ,rpt_entry_vw_net_adj_tax_amt
    ,rpt_start_date
    ,rpt_end_date
    ,future_use_326
    ,rpt_ca_returns_amt
    ,rpt_ca_return_received
    ,rpt_ca_utilized_amt
    ,future_use_330
    ,future_use_331
    ,future_use_332
    ,future_use_333
    ,rpt_entry_location_city_name
    ,rpt_entry_is_billable
    ,rpt_entry_from_location
    ,rpt_entry_to_location
    ,rpt_entry_location_name
    ,rpt_entry_ctry_code
    ,rpt_entry_ctry_sub_code
    ,xml_receipt_uuid
    ,xml_receipt_tax_id
    ,future_use_343
    ,future_use_344
    ,future_use_345
    ,future_use_346
    ,txa_tot_vw_trans_amt
    ,txa_tot_vw_posted_amt
    ,txa_tot_vw_rclm_trns_amt
    ,txa_tot_vw_rclm_posted_amt
    ,txa_tot_vw_rclm_trns_adj_amt
    ,rpt_entry_merchant_tax_id
    ,future_use_353
    ,future_use_354
    ,future_use_355
    ,future_use_356
    ,trvl_req_name
    ,trvl_req_tot_posted_amt
    ,trvl_req_tot_apprv_amt
    ,trvl_req_start_date
    ,trvl_req_end_date
    ,trvl_req_auth_date
    ,trvl_req_req_id
    ,future_use_364
    ,future_use_365
    ,future_use_366
    ,future_use_367
    ,future_use_368
    ,future_use_369
    ,future_use_370
    ,future_use_371
    ,future_use_372
    ,future_use_373
    ,future_use_374
    ,future_use_375
    ,future_use_376
    ,future_use_377
    ,future_use_378
    ,future_use_379
    ,future_use_380
    ,future_use_381
    ,future_use_382
    ,future_use_383
    ,future_use_384
    ,future_use_385
    ,future_use_386
    ,future_use_387
    ,future_use_388
    ,future_use_389
    ,future_use_390
    ,future_use_391
    ,future_use_392
    ,future_use_393
    ,future_use_394
    ,future_use_395
    ,future_use_396
    ,future_use_397
    ,future_use_398
    ,future_use_399
    ,future_use_400
    ,etl_proc_dt
    ,rec_digest
    ,crt_btch_id
from  xxcus.xxcus_src_concur_sae_n
where 1 =1
      and crt_btch_id >g_last_batch_id
      --and concur_batch_id >g_last_batch_id
      ;   
   --
  begin 
       --
       if g_current_batch_id =g_last_batch_id then 
            --
            print_log('Current crt_btch_id :'||g_current_batch_id||' records already archived into history table xxcus.xxcus_src_concur_sae_n_hist.');
            --   
       else
            --
            open c_last_batch;
            --
            loop 
             --
             fetch c_last_batch bulk collect into t_concur_sae_n_hist_rec limit xxcus_concur_pkg.g_limit;
             --
             exit when t_concur_sae_n_hist_rec.count =0;
             --
                 if t_concur_sae_n_hist_rec.count >0 then 
                  --
                  l_total := l_total + t_concur_sae_n_hist_rec.count;
                  --
                  forall idx in 1 .. t_concur_sae_n_hist_rec.count
                   insert into xxcus.xxcus_src_concur_sae_n_hist values t_concur_sae_n_hist_rec(idx);
                  --
                 end if;
                  --
            end loop;
            --
            close c_last_batch;
            --
            print_log('Inserted '||l_total||' records into history table xxcus.xxcus_src_concur_sae_n_hist.');
            --
            if l_total >0 then
               -- 
               commit;   
               --           
            else
                  Null;    
            end if;
            --
       end if;
       --
  exception
   when program_error then
    print_log('Issue in program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then
    print_log('Issue in others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end ARCHIVE_CURRENT_BATCH;
  --
--  procedure EXTRACT_PEOPLESOFT_OOP is --people soft out of pocket for all LOB's
--   --
--    l_sub_routine varchar2(30) :='extract_peoplesoft_oop'; 
--    l_request_id number :=fnd_global.conc_request_id;
--    --
--    l_created_by number :=fnd_global.user_id;
--    l_creation_date date :=sysdate;
--    l_count number :=0;
--    --
--    l_utl_file_id   UTL_FILE.FILE_TYPE;
--    l_line_buffer VARCHAR2(30000):= Null;
--    l_file VARCHAR2(150) :=Null;        
--    --
--    cursor c_oop_group is --lookup XXCUS_CONCUR_OOP_PAYMENT_TYPES is the driver table here
--        select concur_batch_id payment_batch
--                   ,concur_batch_date check_date
--                   ,'C'||rpt_key check_number
--                   ,emp_id employee_number
--                   ,emp_first_name
--                   ,emp_last_name
--                   ,to_date(null) terminated_date
--                   ,'Active' employee_status
--                   ,rpt_vw_tot_due_emp amount
--                   ,'Concur' payment_type_flag
--                   ,listagg(line_number, ',') within group (order by line_number) line_numbers
--                   ,l_created_by ebs_created_by
--                   ,l_creation_date ebs_creation_date
--                   ,l_request_id request_id
--                   ,emp_custom21 erp_platform
--                   ,g_status_new record_status
--                   ,g_current_batch_id crt_btch_id
--                   ,concur_batch_id concur_batch_id
--        from xxcus.xxcus_src_concur_sae_n
--        where 1 =1
--         and crt_btch_id =g_current_batch_id --new
--         and rpt_entry_pmt_type_code IN 
--          (
--            select lookup_code
--            from  apps.fnd_lookup_values_vl
--            where 1 =1
--                 and lookup_type ='XXCUS_CONCUR_OOP_PAYMENT_TYPES'
--                 and enabled_flag ='Y'
--          )
--        and exists
--         (
--              select '1'
--              from    fnd_lookup_values
--              where 1 =1
--                    and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
--                    and nvl(enabled_flag, 'N') ='Y'
--                    and lookup_code =emp_custom21
--         )
--        group by
--                     concur_batch_id
--                   ,concur_batch_date
--                   ,'C'||rpt_key
--                   ,emp_id
--                   ,emp_first_name
--                   ,emp_last_name
--                   ,rpt_vw_tot_due_emp
--                   ,l_created_by
--                   ,l_creation_date
--                   ,l_request_id
--                   ,emp_custom21   
--                   ,g_status_new
--                   ,g_current_batch_id
--                   ,concur_batch_id
--           ;    
--    --
--    type  c_oop_group_type is table of c_oop_group%rowtype index by binary_integer;
--    c_oop_group_rec c_oop_group_type;
--    --
--    cursor oop_file_set is 
--        select 
--                 'PAYMENT_BATCH'||'|'||
--                 'CHECK_DATE'||'|'||
--                 'CHECK_NUMBER'||'|'||
--                 'EMPLOYEE_NUMBER'||'|'||
--                 'EMP_FIRST_NAME'||'|'||
--                 'EMP_LAST_NAME'||'|'||
--                 'TERMINATED_DATE'||'|'||
--                 'EMPLOYEE_STATUS'||'|'||
--                 'AMOUNT'||'|'||
--                 'PAYMENT_TYPE_FLAG'||'|'||
--                 'EBS_CREATION_DATE'||'|'||
--                 'REQUEST_ID'||'|'||
--                 'ERP_PLATFORM'||'|'||
--                 'CONCUR_BATCH_ID'||'|'||                 
--                 'HDS_CRT_BTCH_ID'||'|' hds_concur_oop_record
--        from dual
--        union all        
--        select 
--                payment_batch||'|'||
--                to_char(check_date,'mm/dd/yyyy')||'|'||
--                check_number||'|'||
--                employee_number||'|'||
--                emp_first_name||'|'||
--                emp_last_name||'|'||
--                terminated_date||'|'||
--                employee_status||'|'||
--                TO_CHAR(amount,'FM999,999,990.90')||'|'||
--                payment_type_flag||'|'||
--                to_char(ebs_creation_date,'mm/dd/yyyy')||'|'||
--                request_id||'|'||
--                erp_platform||'|'||
--                concur_batch_id||'|'||                
--                crt_btch_id||'|' hds_concur_oop_record
--        from xxcus.xxcus_src_concur_sae_oop
--        where 1 =1
--             and crt_btch_id =g_current_batch_id
--             and record_status =g_status_new
--             ;
--    --
--    type oop_file_type is table of oop_file_set%rowtype index by binary_integer;
--    oop_rec oop_file_type;            
--    --
--  begin
--    --    
--    delete xxcus.xxcus_src_concur_sae_oop
--    where 1 =1
--         and crt_btch_id =g_current_batch_id;
--    --
--     print_log('@Out of pocket, begin cleanup for current concur batch ids where crt_btch_id = '||g_current_batch_id||', records deleted :'||sql%rowcount);
--    --
--    open c_oop_group;
--     loop 
--       fetch c_oop_group bulk collect into c_oop_group_rec limit xxcus_concur_pkg.g_limit; 
--       exit when c_oop_group_rec.count =0;
--       if c_oop_group_rec.count >0 then
--        forall idx in 1 .. c_oop_group_rec.count
--         insert into xxcus.xxcus_src_concur_sae_oop values c_oop_group_rec(idx);
--         l_count :=l_count + sql%rowcount;
--       end if;
--     end loop;
--    close c_oop_group;
--    --
--    print_log('@'||l_sub_routine||', total records inserted :'||l_count);
--    --
--    commit;
--    --
--    print_log('Begin: Extract OOP file.');
--    --    
--    open oop_file_set;
--    --
--    fetch oop_file_set bulk collect into oop_rec;
--    --
--    close oop_file_set;
--    --
--    if oop_rec.count >0 then   
--                 --
--                 l_count :=0;
--                 --
--                 -- OOP file goes to GSC SharePoint folder 
--                 g_outbound_loc :=GET_OUTBOUND_FILE_LOCATION ( v_operation =>'DBA_DIRECTORY', v_emp_group  =>g_GSC_ORACLE);
--                 --
--                 print_log ('Current working DBA directory is '||g_outbound_loc);
--                 --
--                 --
--                 if g_database =g_prd_database then   
--                   g_file_prefix :=g_GSC_ORACLE;
--                 else
--                   g_file_prefix :=g_database||'_'||g_GSC_ORACLE;
--                 end if;
--                 --
--                 l_file :=g_file_prefix||'_OOP_'||TO_CHAR(g_batch_date, 'MMDDYYYY')||'.txt';
--                 --
--                 print_log('Before file open for OOP PIPE [|] extract');
--                 --
--                 l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
--                 --
--                 print_log ('File handle initiated for OOP PIPE [|] extract');
--                 --
--                 for idx in 1 .. oop_rec.count loop 
--                  --
--                       begin
--                          --
--                          utl_file.put_line ( l_utl_file_id, oop_rec(idx).hds_concur_oop_record);
--                          -- 
--                          l_count:= l_count+1; --number of lines written for OOP including header
--                          --
--                       exception
--                             when no_data_found then
--                               exit;
--                             when others then
--                               print_log('Last Line# '||l_count||', Record: '||oop_rec(idx).hds_concur_oop_record);                               
--                               print_log('@ Inside file loop operation, error : '||sqlerrm);
--                               exit;
--                       end;
--                  --
--                 end loop; 
--                 --
--                 utl_file.fclose ( l_utl_file_id );
--                 print_log ('After close for OOP PIPE [|] extract file : '||l_file);
--                 --  
--                 -- Begin logging file details for each BU along with their concur batch id
--                 begin 
--                        --
--                        savepoint start_here;
--                        --
--                        insert into xxcus.xxcus_concur_ebs_bu_files
--                        (
--                         request_id
--                        ,concur_batch_id
--                        ,emp_group
--                        ,extracted_file_name
--                        ,extracted_file_folder
--                        ,created_by 
--                        ,creation_date
--                        ,crt_btch_id
--                        )
--                        values
--                        (
--                         l_request_id
--                        ,Null -- will be blank when extracting for all out of pocket for all BU's
--                        ,g_GSC_ORACLE
--                        ,l_file
--                        ,g_directory_path
--                        ,l_created_by
--                        ,l_creation_date
--                        ,g_current_batch_id
--                        )
--                        ;
--                        --
--                        commit;
--                        --
--                        -- setup the email body
--                        setup_email_body 
--                          (
--                             p_text1 =>'BU CONCUR employee group : '||g_GSC_emp_grp
--                            ,p_emp_group =>g_GSC_emp_grp
--                            ,p_type =>'OOP'
--                          ); --g_notif_email_body is assigned here
--                        --print_log('Email Body :'||g_notif_email_body);
--                        --
--                        g_email_subject :=g_instance||' '||g_GSC_emp_grp||' CONCUR OOP extract to SharePoint - Status Update';
--                        print_log('Email Subject :'||g_email_subject);
--                        --
--                        set_bu_sharepoint_email (p_emp_group =>g_GSC_emp_grp, p_type =>g_control_type_OOP); --g_sharepoint_email is assigned here
--                        print_log('SharePoint Email  :'||g_sharepoint_email);                        
--                        --
--                        -- push the extract to sharepoint
--                        print_log('Begin: Push OOP PIPE extract file to SharePoint.');
--                        --
--                        sharepoint_integration
--                           (
--                                p_emp_group =>g_GSC_emp_grp,
--                                p_file =>g_directory_path||'/'||l_file,
--                                p_sharepoint_email =>g_sharepoint_email,
--                                p_dummy_email_from =>g_notif_email_from,
--                                p_email_subject =>g_email_subject
--                           )
--                           ;
--                        --
--                        print_log('End: Push OOP PIPE extract file to SharePoint.');                           
--                        -- email the teams about the extract
--                        send_email_notif
--                           (
--                                p_email_from      =>g_notif_email_from,
--                                p_email_to           =>g_notif_email_to,
--                                p_email_cc           =>g_notif_email_cc,
--                                p_email_subject =>g_email_subject, 
--                                p_email_body     =>g_notif_email_body,
--                                p_mime_type      =>g_email_mime_type
--                           )
--                          ; 
--                        print_log('After sending email notification about WaterWorks status');                                                                                                
--                        --                     
--                 exception
--                  when others then
--                   print_log('@Failed to log file details for BU, @ emp_group '||g_GSC_emp_grp||', message =>'||sqlerrm);
--                 end;                                                    
--     --
--    else
--     --
--     print_log('No OOP record found for extract into file.');
--     --
--    end if;
--    --
--    print_log('End: Extract OOP file.');
--    --    
--  exception
--   when program_error then
--    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
--   when others then
--    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
--  end EXTRACT_PEOPLESOFT_OOP;
--  --
  procedure WW_EXTRACT_ELIGIBLE_GL_JE is
    --extract WaterWorks GL journal related info 
    --
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_char_150_15 varchar2(30) :=Null;
    l_creation_date date :=sysdate;  
    --
    l_prev_batch_date date :=Null;
    l_sub_routine varchar2(30) :='ww_extract_eligible_gl_je'; 
    l_ww_je_seq varchar2(10) :=Null;
    --
    cursor ww_je_eligible_records is
    select 
             rpt_id
            ,emp_id
            ,emp_full_name
            ,accrual_erp
            ,segment1 accrual_company
            ,segment2 accrual_branch
            ,segment3 accrual_dept
            ,segment4 accrual_account
            ,segment5 accrual_project
            ,run_id
            ,accrual_period accrual_period
            ,reversal_accrual_period reversal_period
            ,accrual_fiscal_start
            ,accrual_fiscal_end
            ,accrual_status
            ,RPT_ENTRY_PMT_CODE_NAME char_150_1
            ,RPT_ENTRY_PMT_TYPE_CODE char_150_2
            ,PROGRAM_NAME char_150_3
            ,rpt_id char_150_4
            ,rpt_name char_150_5
            ,rpt_entry_vendor_name char_150_6
            ,reimbursement_currency char_150_7
            ,g_WW_gl_prod_code char_150_8
            ,g_WW_gl_je_type char_150_9
            ,g_WW_accrual_seq char_150_10
            ,regexp_substr(segment4,'[0-9]+',1,1) char_150_11 --je main account
            ,regexp_substr(segment4,'[0-9]+',1,2) char_150_12 -- je sub account
            ,case
              when accrual_period_num !=12 --Except for Jan'YY fiscal period
                then lpad(to_char((accrual_period_num+1)), 2,'0')
               else lpad('1', 2,'0')
             end  char_150_13 --WW je month 
            ,case
              when to_number(to_char(accrual_fiscal_end, 'YYYY')) >to_number(to_char(accrual_fiscal_start, 'YYYY'))
                then to_char(accrual_fiscal_end, 'YYYY')
               else to_char(accrual_fiscal_start, 'YYYY')
             end  char_150_14 --WW je year 
            ,segment2 char_150_15 --may get replaced because of routine transform_ww_branch
            ,segment3 char_150_16 --may get replaced because of the resulting value from routine transform_ww_branch
            ,null char_150_17
            ,null char_150_18
            ,null char_150_19
            ,null char_150_20
            ,ledger_id num_1
            ,posted_amount num_2
            ,rpt_key num_3
            ,null num_4
            ,null num_5
            ,date_first_submitted date_1
            ,posted_date date_2
            ,trans_date date_3
            ,null date_4
            ,null date_5
            ,oop_card
            ,expense_type
            ,expense_code
    from xxcus.xxcus_concur_accruals_staging
    where 1 =1
    and accrual_erp =g_WW_emp_grp
    and accrual_status =g_status_new
    and personal_flag =g_N
    and run_id =g_run_id
    and validation_failed =g_N
    and exists
             (
                  select '1'
                  from    fnd_lookup_values
                  where 1 =1
                        and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
                        and nvl(enabled_flag, 'N') ='Y'
                        and lookup_code =accrual_erp
             )         
    ;
    --
    type ww_elig_je_rec_type is table of xxcus.xxcus_concur_accruals_elig_je%rowtype index by binary_integer;
    ww_elig_je_rec ww_elig_je_rec_type;
    --
  begin  
    --
     -- @@@
--            delete xxcus.xxcus_concur_accruals_elig_je 
--            where 1 =1
--                 and accrual_erp =g_WW_emp_grp
--                 and accrual_status =g_status_NEW
--            ;
            --
            print_log('@ '||l_sub_routine||' Flush any previous loaded eligible accrual records for employee group '||g_WW_emp_grp||', total deleted :'||sql%rowcount);
            --       
            open ww_je_eligible_records;
            --
            loop
             --
             fetch ww_je_eligible_records bulk collect into ww_elig_je_rec;
             --
             exit when ww_elig_je_rec.count =0;
                 --
                 if ww_elig_je_rec.count >0 then 
                  --
                    for idx in 1 .. ww_elig_je_rec.count loop 
                      --
                      -- transform incoming allocation_branch if we have a corresponding cross mapping
                        --ww_elig_je_rec(idx).char_60_4 :=
                         l_char_150_15 :=
                          transform_ww_branch 
                                 (
                                   p_project_code =>ww_elig_je_rec(idx).accrual_project
                                  ,p_branch =>ww_elig_je_rec(idx).accrual_branch
                                 );
                      --
                      -- When the branch override happens because of project code we default the cost center to 030 as well as the branch
                      --
                          if (l_char_150_15 != ww_elig_je_rec(idx).accrual_branch) then 
                           ww_elig_je_rec(idx).char_150_15 :=l_char_150_15;
                           ww_elig_je_rec(idx).char_150_16 :=g_WW_override_dept;           
                          else
                           null; --What this means is we will leave the collection ww_elig_je_rec(idx).char_60_5 as it is
                          end if;
                      --
                    end loop;
                      --
                      -- bulk insert into xxcus.xxcus_concur_ww_eligible_je table
                      --
                      begin
                       forall idx in 1 .. ww_elig_je_rec.count 
                        insert into xxcus.xxcus_concur_accruals_elig_je values ww_elig_je_rec(idx);
                      exception
                       when others then
                        print_log('Error in bulk insert of xxcus.xxcus_concur_accruals_elig_je, message =>'||sqlerrm);
                        raise program_error;
                      end;
                  --
                 end if;
                 --
            end loop;
            close ww_je_eligible_records;
            --
     -- @@@ 
  exception
   when program_error then
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end WW_EXTRACT_ELIGIBLE_GL_JE;        
  --
  procedure WW_OB_GL_JE  IS
   --
    l_sub_routine varchar2(30) :='ww_ob_gl_je';
    l_line_count number :=0;
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;      
    --
    l_utl_file_id   UTL_FILE.FILE_TYPE;
    l_line_buffer VARCHAR2(30000):= Null;
    l_file VARCHAR2(150) :=Null;
    --
    cursor c_summary_je  is
        select CHAR_150_8   je_entity          
                   ,CHAR_150_13   je_month
                   ,substr(CHAR_150_14, 3, 2)   je_year
                   ,CHAR_150_9   je_type
                   ,CHAR_150_10   je_seq 
                   ,CHAR_150_15   je_branch
                   ,CHAR_150_16   je_dept
                   ,CHAR_150_11   je_main_acct
                   ,CHAR_150_12  je_sub_acct
                   ,SUBSTR(EMP_FULL_NAME||' -'||char_150_6, 1, 25)   je_expense_description           
                   ,NUM_2              je_amount 
                   ,rowid                  je_rowid                                                                     
        from xxcus.xxcus_concur_accruals_elig_je
        where 1 =1
            and accrual_erp =g_WW_emp_grp
            and run_id =g_run_id
            and accrual_status =g_status_NEW
            
                   ;
    --
    type c_summary_je_type is table of c_summary_je%rowtype index by binary_integer;
    c_summary_rec  c_summary_je_type; 
    interco_liab_rec c_summary_je_type;
    -- 
    cursor c_WW_je_supporting_info  is
        select CHAR_150_8   je_entity          
                   ,CHAR_150_13   je_month
                   ,substr(CHAR_150_14, 3,2)   je_year
                   ,CHAR_150_9   je_type
                   ,CHAR_150_10   je_seq 
                   ,CHAR_150_15   je_branch
                   ,CHAR_150_16   je_dept
                   ,CHAR_150_11   je_main_acct
                   ,CHAR_150_12  je_sub_acct
                   ,SUBSTR(EMP_ID||', '||CHAR_150_5, 1, 25)   je_expense_description           
                   ,NUM_2              je_amount 
                   ,emp_id
                   ,emp_full_name
                   ,rpt_id
                   ,char_150_6 rpt_entry_vendor_desc
                   ,accrual_erp
                   ,accrual_company
                  ,accrual_branch
                  ,accrual_dept
                  ,accrual_account
                  ,accrual_project
                  ,run_id
                  ,char_150_1 payment_type
                  ,char_150_3 program_name 
                  ,char_150_5 rpt_name
                  ,date_1 date_first_submitted
                  ,date_1 posted_date
                  ,date_3 trans_date      
        from xxcus.xxcus_concur_accruals_elig_je
        where 1 =1
            and accrual_erp =g_WW_emp_grp
            and run_id =g_run_id
                   ;    
    --  
    type c_WW_JE_details_type is table of c_WW_je_supporting_info%rowtype index by binary_integer;
    c_WW_JE_details_rec  c_WW_JE_details_type; 
    -- 
    --     
  begin
                 --
                 g_outbound_loc :=GET_OUTBOUND_FILE_LOCATION ( v_operation =>'DBA_DIRECTORY', v_emp_group  =>g_WW_MINCRON);
                 --
                 print_log ('Current working DBA directory is '||g_outbound_loc);
                 --     
                 --
                 if g_database =g_prd_database then   
                   g_file_prefix :=g_WW_MINCRON;
                 else
                   g_file_prefix :=g_database||'_'||g_WW_MINCRON;
                 end if;
                 --
                 l_file :=g_file_prefix||'_JE_ACCRUALS_'||TO_CHAR(sysdate, 'MMDDYYYY')||'.txt';                 
                 --
                 print_log('Before file open for WW JE Accruals.');
                 --
                 l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
                 --
                 print_log ('File handle initiated for WW JE Accruals : '||l_file);
                 --                         
                 open c_summary_je;
                 --
                 loop
                  --
                  fetch c_summary_je bulk collect into c_summary_rec;
                  --
                  exit when c_summary_rec.count =0;
                  --
                      if c_summary_rec.count >0 then 
                         --
                         Null;
                         --
                         -- Begin copy the WW JE actuals header record to the file              
                         --
                         begin
                              --
                              l_line_buffer := 'ENTITY CODE'
                                  ||','
                                  ||''
                                  ||','          
                                  ||'MONTH'
                                  ||','
                                  ||''
                                  ||','            
                                  ||'YEAR'
                                  ||','
                                  ||''
                                  ||','           
                                  ||'JE TYPE'
                                  ||','         
                                  ||'JE #'
                                  ||','
                                  ||''
                                  ||','           
                                  ||'BRANCH'
                                  ||','
                                  ||''
                                  ||','           
                                  ||'DEPT'
                                  ||','
                                  ||''
                                  ||','
                                  ||'MINCRON GL MAIN'
                                  ||','
                                  ||''
                                  ||','           
                                  ||'MINCRON GL SUB'
                                  ||','
                                  ||''
                                  ||',' 
                                  ||'AMOUNT'
                                  ||','
                                  ||''
                                  ||','
                                  ||'EXPENSE DESCRIPTION'
                                  ||','
                                  ;                  
                              --
                              utl_file.put_line ( l_utl_file_id, l_line_buffer);
                              -- 
                              l_line_count:= l_line_count+1; --number of lines written
                              --
                          exception
                                 when no_data_found then
                                   exit;
                                 when others then
                                   print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                   print_log('@ Inside file loop operation, error : '||sqlerrm);
                                   exit;
                           end;              
                         --
                         --End copy the WW JE actuals header record to the file              
                         --
                         l_line_buffer :=Null;
                         --
                         --Begin copy the WW JE actuals detail records to the file              
                         --
                         for idx in 1 .. c_summary_rec.count loop 
                           --
                           if idx =1 then
                            interco_liab_rec(1) :=c_summary_rec(idx); --Just to get some common attributes so we can get the liability intercompany clearing records created later
                           end if;
                           --
                           l_line_buffer :=
                                                        c_summary_rec(idx).je_entity
                                                                  ||','
                                                                  ||''
                                                                  ||','          
                                                                  ||c_summary_rec(idx).je_month
                                                                  ||','
                                                                  ||''
                                                                  ||','            
                                                                  ||c_summary_rec(idx).je_year
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_summary_rec(idx).je_type
                                                                  ||','       
                                                                  ||c_summary_rec(idx).je_seq
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_summary_rec(idx).je_branch
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_summary_rec(idx).je_dept
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_summary_rec(idx).je_main_acct
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_summary_rec(idx).je_sub_acct
                                                                  ||','
                                                                  ||''
                                                                  ||',' 
                                                                  ||c_summary_rec(idx).je_amount
                                                                  ||','
                                                                  ||''
                                                                  ||','                    
                                                                  ||c_summary_rec(idx).je_expense_description
                                                                  ||','
                                                                  ;                
                           --
                                   begin
                                      --
                                      utl_file.put_line ( l_utl_file_id, l_line_buffer);
                                      -- 
                                      l_line_count:= l_line_count+1; --number of lines written
                                      --
                                      g_total := g_total + c_summary_rec(idx).je_amount;
                                      --
                                   exception
                                         when no_data_found then
                                           exit;
                                         when others then
                                           print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                           print_log('@ Inside file loop operation, error : '||sqlerrm);
                                           exit;
                                   end;                
                           --
                          end loop;
                         --
                         --End copy the WW JE actuals detail records to the file              
                         --
                      end if; 
                  --
                 end loop;
                 --
                 close c_summary_je;
                 --
                 -- Begin copy interco clearing rec [WW IC with GSC]
                 --
                 begin
                          --
                           l_line_buffer :=
                                                        g_WW_gl_prod_code --interco_liab_rec(1).je_entity --???? Check with Mike Macrae
                                                                  ||','
                                                                  ||''
                                                                  ||','          
                                                                  ||interco_liab_rec(1).je_month
                                                                  ||','
                                                                  ||''
                                                                  ||','            
                                                                  ||interco_liab_rec(1).je_year
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||interco_liab_rec(1).je_type
                                                                  ||','        
                                                                  ||interco_liab_rec(1).je_seq
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||'901' --interco_liab_rec(1).je_branch
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||'0' --interco_liab_rec(1).je_dept 
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||'4100' --interco_liab_rec(1).je_main_acct
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||'003' --interco_liab_rec(1).je_sub_acct 
                                                                  ||','
                                                                  ||''
                                                                  ||',' 
                                                                  ||( (-1) * g_total )
                                                                  ||','
                                                                  ||''
                                                                  ||','                    
                                                                  ||'WW IC with GSC' 
                                                                  ||','
                                                                  ;              
                          --
                          utl_file.put_line ( l_utl_file_id, l_line_buffer);
                          -- 
                          l_line_count:= l_line_count+1; --number of lines written
                          --
                          print_log('Copied interco clearing record [WW IC with GSC] to file '||l_file);
                          --
                 exception
                     when others then
                       print_log('@ Interco clearing, error =>'||sqlerrm||', line :'||l_line_buffer);
                 end;
                 --                         
                 -- End copy interco clearing rec [WW IC with GSC]
                 --          
                 print_log('Total records written into the file [Header and Lines] , l_line_count =>'||l_line_count);
                 --
                 -- Close file handle 
                 --
                 utl_file.fclose ( l_utl_file_id );
                 print_log ('After close for WW JE Accruals file : '||l_file);
                 --
                 -- Begin logging file details for each BU along with their concur batch id
                 begin 
                        --
                        -- setup the email body
                        --setup_email_body (p_text1 =>'BU CONCUR employee group : '||g_WW_emp_grp); --g_notif_email_body is assigned here
                        setup_email_body 
                          (
                             p_text1 =>'BU CONCUR employee group : '||g_WW_emp_grp
                            ,p_emp_group =>g_WW_emp_grp
                            ,p_type =>'OTHERS'
                          );
                        --
                        g_email_subject :=g_instance||' '||g_WW_emp_grp||' CONCUR accrual extracts to SharePoint - Status Update';
                        print_log('Email Subject :'||g_email_subject);                        
                        --
                        set_bu_sharepoint_email (p_emp_group =>g_WW_emp_grp, p_type=>g_control_type_AA); --g_sharepoint_email is assigned here
                        print_log('SharePoint Email  :'||g_sharepoint_email);                        
                        --
                        -- push the extract to sharepoint
                        print_log('Begin: Push WW JE Accruals to SharePoint.');                        
                        sharepoint_integration
                           (
                                p_emp_group =>g_WW_emp_grp,
                                p_file =>g_directory_path||'/'||l_file,
                                p_sharepoint_email =>g_sharepoint_email,
                                p_dummy_email_from =>g_notif_email_from,
                                p_email_subject =>g_email_subject
                           )
                           ;
                        print_log('End: Push WW JE Accruals to SharePoint.');                           
                        --                    
                 exception
                  when others then
                   print_log('@Failed to log file details for BU, @ emp_group '||g_WW_emp_grp||', message =>'||sqlerrm);
                   rollback to start_here;
                 end;
                 -- End logging file details for each BU along with their concur batch id
                 --                 
                 -- Begin WW Je with Supporting details
                 --
                 if g_database =g_prd_database then   
                   g_file_prefix :=g_WW_MINCRON;
                 else
                   g_file_prefix :=g_database||'_'||g_WW_MINCRON;
                 end if;
                 --                 
                 l_file :=g_file_prefix||'_JE_ACCRUAL_DETAILS_'||TO_CHAR(sysdate, 'MMDDYYYY')||'.txt';
                 --
                 print_log('Before file open for WW JE Accruals with supporting details.');
                 --
                 l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
                 --
                 print_log ('File handle initiated for WW JE Accruals with supporting details : '||l_file);
                 --                         
                 open c_WW_je_supporting_info;
                 --
                 loop
                  --
                  fetch c_WW_je_supporting_info bulk collect into c_WW_JE_details_rec;
                  --
                  exit when c_WW_JE_details_rec.count =0;
                  --
                      if c_WW_JE_details_rec.count >0 then 
                         --
                          begin
                              --
                              l_line_buffer := 'ENTITY CODE'
                                  ||','
                                  ||''
                                  ||','          
                                  ||'MONTH'
                                  ||','
                                  ||''
                                  ||','            
                                  ||'YEAR'
                                  ||','
                                  ||''
                                  ||','           
                                  ||'JE TYPE'
                                  ||','
                                  ||''
                                  ||','           
                                  ||'JE #'
                                  ||','
                                  ||''
                                  ||','           
                                  ||'JE BRANCH'
                                  ||','
                                  ||''
                                  ||','                                  
                                  ||'CONCUR_BRANCH'
                                  ||','   
                                  ||''
                                  ||','                                          
                                  ||'DEPT'
                                  ||','
                                  ||''
                                  ||','
                                  ||'MINCRON GL MAIN'
                                  ||','
                                  ||''
                                  ||','           
                                  ||'MINCRON GL SUB'
                                  ||','
                                  ||''
                                  ||',' 
                                  ||'AMOUNT'
                                  ||','
                                  ||''
                                  ||','
                                  ||'EXPENSE DESCRIPTION'
                                  ||','                    
                                  ||'EMP_ID'                                                      
                                  ||','                    
                                  ||'EMP_FULL_NAME'
                                  ||','                    
                                  ||'RPT_ID'
                                  ||','                    
                                  ||'ACCRUAL_ERP'
                                  ||','                    
                                  ||'ACCRUAL_COMPANY'    
                                  ||','                                      
                                  ||'ACCRUAL_DEPT'
                                  ||','                    
                                  ||'ACCRUAL_ACCOUNT'
                                  ||','                    
                                  ||'ACCRUAL_PROJECT'
                                  ||','                    
                                  ||'RUN_ID'  
                                  ||','                    
                                  ||'PAYMENT_TYPE'                                                      
                                  ||','                    
                                  ||'PROGRAM_NAME'
                                  ||','                    
                                  ||'RPT_NAME'
                                  ||','                    
                                  ||'RPT_ENTRY_VENDOR_DESC'
                                  ||','                    
                                  ||'DATE_FIRST_SUBMITTED'
                                  ||','                    
                                  ||'POSTED_DATE'                                                     
                                  ||','                    
                                  ||'TRANS_DATE'
                                  ||','             
                                  ;                               
                              --
                              utl_file.put_line ( l_utl_file_id, l_line_buffer);
                              -- 
                              l_line_count:= l_line_count+1; --number of lines written
                              --
                          exception
                                 when no_data_found then
                                   exit;
                                 when others then
                                   print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                   print_log('@ Inside file loop operation, error : '||sqlerrm);
                                   exit;
                           end;              
                         --
                         l_line_buffer :=Null;
                         --
                         for idx in 1 .. c_WW_JE_details_rec.count loop 
                           --
                           l_line_buffer :=
                                                        c_WW_JE_details_rec(idx).je_entity
                                                                  ||','
                                                                  ||''
                                                                  ||','          
                                                                  ||c_WW_JE_details_rec(idx).je_month
                                                                  ||','
                                                                  ||''
                                                                  ||','            
                                                                  ||c_WW_JE_details_rec(idx).je_year
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_WW_JE_details_rec(idx).je_type
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_WW_JE_details_rec(idx).je_seq
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_WW_JE_details_rec(idx).je_branch
                                                                  ||','
                                                                  ||''
                                                                  ||','                                  
                                                                  ||c_WW_JE_details_rec(idx).accrual_branch
                                                                  ||','   
                                                                  ||''
                                                                  ||','             
                                                                  ||c_WW_JE_details_rec(idx).je_dept
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_WW_JE_details_rec(idx).je_main_acct
                                                                  ||','
                                                                  ||''
                                                                  ||','           
                                                                  ||c_WW_JE_details_rec(idx).je_sub_acct
                                                                  ||','
                                                                  ||''
                                                                  ||',' 
                                                                  ||c_WW_JE_details_rec(idx).je_amount
                                                                  ||','
                                                                  ||''
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).je_expense_description
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).emp_id                                                      
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).emp_full_name
                                                                  ||','                    
                                                                  ||c_WW_JE_details_rec(idx).rpt_id
                                                                  ||','                    
                                                                  ||c_ww_je_details_rec(idx).accrual_erp
                                                                  ||','                    
                                                                  ||c_ww_je_details_rec(idx).accrual_company    
                                                                  ||','                              
                                                                  ||c_ww_je_details_rec(idx).accrual_dept
                                                                  ||','                    
                                                                  ||c_ww_je_details_rec(idx).accrual_account
                                                                  ||','                    
                                                                  ||c_ww_je_details_rec(idx).accrual_project
                                                                  ||','                    
                                                                  ||c_ww_je_details_rec(idx).run_id  
                                                                  ||','                    
                                                                  ||c_ww_je_details_rec(idx).payment_type                                                      
                                                                  ||','                    
                                                                  ||c_ww_je_details_rec(idx).program_name
                                                                  ||','                    
                                                                  ||c_ww_je_details_rec(idx).rpt_name
                                                                  ||','                    
                                                                  ||c_ww_je_details_rec(idx).rpt_entry_vendor_desc
                                                                  ||','                    
                                                                  ||c_ww_je_details_rec(idx).date_first_submitted
                                                                  ||','                    
                                                                  ||c_ww_je_details_rec(idx).posted_date                                                      
                                                                  ||','                    
                                                                  ||c_ww_je_details_rec(idx).trans_date
                                                                  ||','
                                                                  ;                                                           
                           --
                                   begin
                                      --
                                      utl_file.put_line ( l_utl_file_id, l_line_buffer);
                                      -- 
                                      l_line_count:= l_line_count+1; --number of lines written
                                      --
                                   exception
                                         when no_data_found then
                                           exit;
                                         when others then
                                           print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                           print_log('@ Inside file loop operation, error : '||sqlerrm);
                                           exit;
                                   end;                
                           --
                          end loop;
                         --    
                      end if; 
                  --
                 end loop;
                 --
                 close c_WW_je_supporting_info;
                 --             
                 print_log('Total records written into the file [Header and Lines] , l_line_count =>'||l_line_count);
                 --
                 utl_file.fclose ( l_utl_file_id );
                 print_log ('After close for WW JE Actuals file with supporting details : '||l_file);
                 --     
                 -- Close file handle      
                 -- End WW Je with Supporting details
                 -- Begin logging file details for each BU along with their concur batch id
                 begin 
                        --
                        -- setup the email body
                        --setup_email_body (p_text1 =>'BU CONCUR employee group : '||g_WW_emp_grp); --g_notif_email_body is assigned here
                        setup_email_body 
                          (
                             p_text1 =>'BU CONCUR employee group : '||g_WW_emp_grp
                            ,p_emp_group =>g_WW_emp_grp
                            ,p_type =>'OTHERS'
                          ); --g_notif_email_body is assigned here                           
                        --print_log('Email Body :'||g_notif_email_body);
                        --
                        g_email_subject :=g_instance||' '||g_WW_emp_grp||' CONCUR accrual extracts to SharePoint - Status Update';
                        print_log('Email Subject :'||g_email_subject);                        
                        --
                        set_bu_sharepoint_email (p_emp_group =>g_WW_emp_grp, p_type =>g_control_type_AA); --g_sharepoint_email is assigned here
                        print_log('SharePoint Email  :'||g_sharepoint_email);                        
                        --
                        -- push the extract to sharepoint
                        print_log('Begin: Push WW JE Actuals with supporting details to SharePoint.');                        
                        sharepoint_integration
                           (
                                p_emp_group =>g_WW_emp_grp,
                                p_file =>g_directory_path||'/'||l_file,
                                p_sharepoint_email =>g_sharepoint_email,
                                p_dummy_email_from =>g_notif_email_from,
                                p_email_subject =>g_email_subject
                           )
                           ;
                        print_log('End: Push WW JE Actuals with supporting details to SharePoint.');                   
                 exception
                  when others then
                   print_log('@Failed to log file details for BU, @ emp_group '||g_WW_emp_grp||', message =>'||sqlerrm);
                   rollback to start_here;
                 end;
                 -- End logging file details for each BU along with their concur batch id     
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    raise program_error;
  end WW_OB_GL_JE;
  --  
  -- Begin CI Great Plains
  procedure CI_GP_EXTRACT_ELIGIBLE_GL_JE is 
    --extract Construction and Industrical [CI] Brafasco GL journal related info 
    --
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;  
    --
    l_prev_batch_date date :=Null;
    l_sub_routine varchar2(30) :='ci_gp_extract_eligible_gl_je';
    -- 
    cursor gp_je_eligible_records  is
    select 
             rpt_id
            ,emp_id
            ,emp_full_name
            ,accrual_erp
            ,segment1 accrual_company
            ,segment2 accrual_branch
            ,segment3 accrual_dept
            ,segment4 accrual_account
            ,segment5 accrual_project
            ,run_id
            ,accrual_period accrual_period
            ,reversal_accrual_period reversal_period
            ,accrual_fiscal_start
            ,accrual_fiscal_end
            ,accrual_status
            ,RPT_ENTRY_PMT_CODE_NAME char_150_1
            ,RPT_ENTRY_PMT_TYPE_CODE char_150_2
            ,PROGRAM_NAME char_150_3
            ,rpt_id char_150_4
            ,rpt_name char_150_5
            ,rpt_entry_vendor_name char_150_6
            ,reimbursement_currency char_150_7
            ,segment4||'-'||segment2 char_150_8
            ,Null char_150_9
            ,Null char_150_10
            ,regexp_substr(segment4,'[0-9]+',1,1) char_150_11 --je main account
            ,regexp_substr(segment4,'[0-9]+',1,2) char_150_12 -- je sub account
            ,null char_150_13 --WW je month 
            ,null char_150_14 --WW je year 
            ,Null char_150_15 --may get replaced because of routine transform_ww_branch
            ,Null char_150_16 --may get replaced because of the resulting value from routine transform_ww_branch
            ,lpad(to_char((accrual_period_num)), 2,'0') char_150_17
            ,null char_150_18
            ,null char_150_19
            ,null char_150_20
            ,ledger_id num_1
            ,posted_amount num_2
            ,rpt_key num_3
            ,null num_4
            ,null num_5
            ,date_first_submitted date_1
            ,posted_date date_2
            ,trans_date date_3
            ,null date_4
            ,null date_5
            ,oop_card
            ,expense_type
            ,expense_code            
    from xxcus.xxcus_concur_accruals_staging
    where 1 =1
    and accrual_erp =g_CI_GP_emp_grp
    and accrual_status =g_status_new
    and personal_flag =g_N
    and run_id =g_run_id    
    and validation_failed =g_N
    and exists
             (
                  select '1'
                  from    fnd_lookup_values
                  where 1 =1
                        and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
                        and nvl(enabled_flag, 'N') ='Y'
                        and lookup_code =accrual_erp
             )         
    ;
    --
    type gp_je_eligible_rec_type is table of xxcus.xxcus_concur_accruals_elig_je%rowtype index by binary_integer;
    gp_je_eligible_rec gp_je_eligible_rec_type;
    --
  begin
         --
--        delete xxcus.xxcus_concur_accruals_elig_je 
--        where 1 =1
--             and accrual_erp =g_CI_GP_emp_grp
--             and accrual_status =g_status_NEW 
--         ;     
         --
          print_log('@ '||l_sub_routine||' Flush any previous loaded eligible JE records for employee group '||g_CI_GP_emp_grp||', total deleted :'||sql%rowcount);
         --
        open gp_je_eligible_records;
        loop 
         --
         fetch gp_je_eligible_records bulk collect into gp_je_eligible_rec;
         --
         exit when gp_je_eligible_rec.count =0;
             --
             if gp_je_eligible_rec.count >0 then 
                  --
                  -- bulk insert into xxcus.xxcus_concur_ww_eligible_je table
                  --
                  begin
                   forall idx in 1 .. gp_je_eligible_rec.count 
                    insert into xxcus.xxcus_concur_accruals_elig_je values gp_je_eligible_rec(idx);
                  exception
                   when others then
                    print_log('Error in bulk insert of xxcus.xxcus_concur_accruals_elig_je, message =>'||sqlerrm);
                    raise program_error;
                  end;
              --
             end if;
             --
        end loop;
        close gp_je_eligible_records;
        --      

  exception
   when program_error then
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end CI_GP_EXTRACT_ELIGIBLE_GL_JE;        
  --  
  -- End CI Great Plains extract gl eligible je
  -- Begin CI Great Plains extract je files and generate Interco gl journal interface records 
  procedure CI_GP_OB_GL_JE is
   --
    l_sub_routine varchar2(30) :='ci_gp_ob_gl_je';
    l_line_count number :=0;
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;      
    --
    l_Interco_Acct_Date Date :=Trunc(Sysdate);--WW accounting date used for Intercompany journal entries
    l_Interco_Location              xxcus.xxcus_concur_gl_iface.segment2%type :='Z0436';
    l_Interco_Cost_Ctr              xxcus.xxcus_concur_gl_iface.segment3%type :='0000';
    l_Interco_OOP_Liability     xxcus.xxcus_concur_gl_iface.segment4%type :='211121';
    l_Interco_CARD_Liability   xxcus.xxcus_concur_gl_iface.segment4%type :='211126';    
    l_Interco_OOP_Clearing    xxcus.xxcus_concur_gl_iface.segment4%type :='910121';    
    l_Interco_Project                 xxcus.xxcus_concur_gl_iface.segment5%type :='00000';
    l_Brafasco_IC_GSC_Acct   xxcus.xxcus_concur_gl_iface.segment4%type :='000-21001';
    --
    l_Brafasco_IC_GSC_Desc  xxcus.xxcus_concur_gl_iface.reference10%type :='Brafasco IC with GSC';
    l_Interco_Clearing_Desc  xxcus.xxcus_concur_gl_iface.reference10%type :='GSC IC with Brafasco';
    --
    l_utl_file_id   UTL_FILE.FILE_TYPE;
    l_line_buffer VARCHAR2(32767):= Null;
    l_file VARCHAR2(150) :=Null;
    l_line_idx Number :=0;
    --
    cursor c_CI_GP_summary_je is
        select replace(upper(accrual_period), '-','')||' ACCRUAL BATCH ' batchid
                  ,to_char(sysdate, 'MM/DD/YYYY') asofdte
                  ,oop_card payment_type
                  ,char_150_8 --account_code and branch together
                  ,sum(num_2) total_amount
        from xxcus.xxcus_concur_accruals_elig_je
        where 1 =1
            and accrual_erp =g_CI_GP_emp_grp
            and accrual_status =g_status_new
            and run_id =g_run_id
        group by
                    replace(upper(accrual_period), '-','')||' ACCRUAL BATCH '
                  ,to_char(sysdate, 'MM/DD/YYYY')
                  ,oop_card
                  ,char_150_8
        ;
    --
    type c_summary_je_type is table of c_CI_GP_summary_je%rowtype index by binary_integer;
    c_summary_rec  c_summary_je_type; 
    --
    cursor c_CI_GP_IC  is --Used only for Brafasco IC with GSC
        select replace(upper(accrual_period), '-','')||' ACCRUAL BATCH ' BATCHID
                  ,to_char(sysdate, 'MM/DD/YYYY') ASOFDTE
                  ,oop_card payment_type
                  ,sum(num_2) brafasco_ic_amt
        from xxcus.xxcus_concur_accruals_elig_je
        where 1 =1
            and accrual_erp =g_CI_GP_emp_grp
            and accrual_status =g_status_new
            and run_id =g_run_id
        group by
                     replace(upper(accrual_period), '-','')||' ACCRUAL BATCH ' --BATCHID
                  ,to_char(sysdate, 'MM/DD/YYYY') --concur_batch_date
                  ,oop_card --payment_type
        ;
    --
    type c_CI_GP_IC_rec_type is table of c_CI_GP_IC%rowtype index by binary_integer;     
    c_CI_GP_IC_rec c_CI_GP_IC_rec_type;
    -- 
    cursor c_CI_GP_interco_je is --Used only for GSC IC with Brafasco
        select
                   to_char(sysdate, 'MM/DD/YYYY') concur_batch_date
                  ,oop_card payment_type
                  ,g_Interco_CA_Product product
                  ,l_Interco_Location  location
                  ,l_Interco_Cost_Ctr cost_ctr
                  ,case
                    when oop_card ='OOP' then l_Interco_OOP_Liability
                    when oop_card ='CARD' then l_Interco_CARD_Liability
                    else 'NONE'
                   end liability_account
                  ,l_Interco_Project project_code
                  ,g_future_use_1  fuse1
                  ,g_future_use_2  fuse2                   
                  ,l_Interco_OOP_Clearing interco_clearing 
                  ,case 
                     when oop_card ='OOP'  then 'Interco OOP'||', Accrual Batch '
                     when oop_card ='CARD'  then 'Interco Card'||', Accrual Batch '       
                     else 'Other than OOP/Card found'             
                   end reference4
                  ,case 
                     when oop_card ='OOP'  then 'Employee Group :'||g_CI_GP_emp_grp||', Accrual Batch '||', Interco for OOP'
                     when oop_card ='CARD'  then 'Employee Group :'||g_CI_GP_emp_grp||', Accrual Batch '||', Interco for CARD'                             
                     else 'Journal for other than OOP/Card'             
                   end reference5                   
                  --,'Rpt Key :'||num_3||', GSC IC with Brafasco' reference10                  
                  ,'GSC IC with Brafasco' reference10                  
                  ,sum(num_2) debit
                  ,sum(num_2) credit 
        from xxcus.xxcus_concur_accruals_elig_je
        where 1 =1
            and accrual_erp =g_CI_GP_emp_grp
            and accrual_status =g_status_new 
            and run_id =g_run_id 
        group by
                   to_char(sysdate, 'MM/DD/YYYY') 
                  --,num_3 --rpt_key
                  ,oop_card 
                  ,g_Interco_CA_Product 
                  ,l_Interco_Location  
                  ,l_Interco_Cost_Ctr 
                  ,case
                    when oop_card ='OOP' then l_Interco_OOP_Liability
                    when oop_card ='CARD' then l_Interco_CARD_Liability
                    else 'NONE'
                   end 
                  ,l_Interco_Project 
                  ,g_future_use_1  
                  ,g_future_use_2                     
                  ,l_Interco_OOP_Clearing  
                  ,case 
                     when oop_card ='OOP'  then 'Interco OOP'||', Accrual Batch '
                     when oop_card ='CARD'  then 'Interco Card'||', Accrual Batch '       
                     else 'Other than OOP/Card found'             
                   end 
                  ,case 
                     when oop_card ='OOP'  then 'Employee Group :'||g_CI_GP_emp_grp||', Accrual Batch '||', Interco for OOP'
                     when oop_card ='CARD'  then 'Employee Group :'||g_CI_GP_emp_grp||', Accrual Batch '||', Interco for CARD'                             
                     else 'Journal for other than OOP/Card'             
                   end                    
                  ,'GSC IC with Brafasco'        
        ;
    --
    type c_CI_GP_interco_je_type is table of c_CI_GP_interco_je%rowtype index by binary_integer;
    c_interco_rec  c_CI_GP_interco_je_type; 
    --    
    cursor c_ci_cad_gp_header is
    select
            'EMP_ID'
            ||'|'
            ||'EMP_FULL_NAME'
            ||'|'        
            ||'EXPENSE_TYPE'
            ||'|'    
            ||'EXPENSE_CODE'
            ||'|'    
            ||'RPT_ENTRY_PMT_CODE_NAME'
            ||'|'    
            ||'PROGRAM_NAME'
            ||'|'    
            ||'APPROVAL_STATUS'
            ||'|'    
            ||'RPT_ID'
            ||'|'    
            ||'RPT_NAME'
            ||'|'    
            ||'DATE_FIRST_SUBMITTED'
            ||'|'    
            ||'RPT_ENTRY_VENDOR_NAME'
            ||'|'    
            ||'POSTED_DATE'
            ||'|'    
            ||'TRANS_DATE'
            ||'|'    
            ||'COUNTRY'
            ||'|'    
            ||'REIMBURSEMENT_CURRENCY'
            ||'|'    
            ||'POSTED_AMOUNT'
            ||'|'    
            ||'ACCOUNT_CODE1'
            ||'|'    
            ||'ACCOUNT_CODE2'
            ||'|'    
            ||'RPT_ORG_UNIT_1'
            ||'|'    
            ||'RPT_ORG_UNIT_2'
            ||'|'    
            ||'RPT_ORG_UNIT_3'
            ||'|'    
            ||'RPT_ORG_UNIT_4'
            ||'|'    
            ||'RPT_ORG_UNIT_6'
            ||'|'    
            ||'PERSONAL_FLAG'
            ||'|'    
            ||'RPT_ENTRY_PMT_TYPE_CODE'
            ||'|'    
            ||'RPT_CUSTOM1'
            ||'|'    
            ||'RPT_CUSTOM2'
            ||'|'    
            ||'RPT_CUSTOM3'
            ||'|'    
            ||'RPT_CUSTOM4'
            ||'|'    
            ||'RPT_CUSTOM6'
            ||'|'    
            ||'EMP_ORG_UNIT1'
            ||'|'    
            ||'EMP_ORG_UNIT2'
            ||'|'    
            ||'EMP_ORG_UNIT3'
            ||'|'    
            ||'EMP_ORG_UNIT4'
            ||'|'    
            ||'EMP_ORG_UNIT6'
            ||'|'    
            ||'EMP_CUSTOM1'
            ||'|'    
            ||'EMP_CUSTOM2'
            ||'|'    
            ||'EMP_CUSTOM3'
            ||'|'    
            ||'EMP_CUSTOM4'
            ||'|'    
            ||'EMP_CUSTOM6'
            ||'|'    
            ||'ACCRUAL_PERIOD'
            ||'|'    
            ||'REVERSAL_ACCRUAL_PERIOD'
            ||'|'    
            ||'RUN_ID'
            ||'|'  
   from   dual; 
    --     
    cursor c_ci_cad_gp_lines is
    select
               EMP_ID
            ||'|'
            ||EMP_FULL_NAME
            ||'|'        
            ||EXPENSE_TYPE
            ||'|'    
            ||EXPENSE_CODE
            ||'|'    
            ||RPT_ENTRY_PMT_CODE_NAME
            ||'|'    
            ||PROGRAM_NAME
            ||'|'    
            ||APPROVAL_STATUS
            ||'|'    
            ||RPT_ID
            ||'|'    
            ||RPT_NAME
            ||'|'    
            ||DATE_FIRST_SUBMITTED
            ||'|'    
            ||RPT_ENTRY_VENDOR_NAME
            ||'|'    
            ||POSTED_DATE
            ||'|'    
            ||TRANS_DATE
            ||'|'    
            ||COUNTRY
            ||'|'    
            ||REIMBURSEMENT_CURRENCY
            ||'|'    
            ||POSTED_AMOUNT
            ||'|'    
            ||ACCOUNT_CODE1
            ||'|'    
            ||ACCOUNT_CODE2
            ||'|'    
            ||RPT_ORG_UNIT_1
            ||'|'    
            ||RPT_ORG_UNIT_2
            ||'|'    
            ||RPT_ORG_UNIT_3
            ||'|'    
            ||RPT_ORG_UNIT_4
            ||'|'    
            ||RPT_ORG_UNIT_6
            ||'|'    
            ||PERSONAL_FLAG
            ||'|'    
            ||RPT_ENTRY_PMT_TYPE_CODE
            ||'|'    
            ||RPT_CUSTOM1
            ||'|'    
            ||RPT_CUSTOM2
            ||'|'    
            ||RPT_CUSTOM3
            ||'|'    
            ||RPT_CUSTOM4
            ||'|'    
            ||RPT_CUSTOM6
            ||'|'    
            ||EMP_ORG_UNIT1
            ||'|'    
            ||EMP_ORG_UNIT2
            ||'|'    
            ||EMP_ORG_UNIT3
            ||'|'    
            ||EMP_ORG_UNIT4
            ||'|'    
            ||EMP_ORG_UNIT6
            ||'|'    
            ||EMP_CUSTOM1
            ||'|'    
            ||EMP_CUSTOM2
            ||'|'    
            ||EMP_CUSTOM3
            ||'|'    
            ||EMP_CUSTOM4
            ||'|'    
            ||EMP_CUSTOM6
            ||'|'    
            ||ACCRUAL_PERIOD
            ||'|'    
            ||REVERSAL_ACCRUAL_PERIOD
            ||'|'    
            ||RUN_ID
            ||'|'  rec_info
    from xxcus.xxcus_concur_accruals_staging
    where 1 =1
        and accrual_erp =g_CI_GP_emp_grp
        and run_id =g_run_id     
     ;  
   --
   type c_ci_cad_gp_type is table of c_ci_cad_gp_lines%rowtype index by binary_integer;
   c_ci_cad_gp_rec c_ci_cad_gp_type;
   --
   --  
  begin
     --
     g_outbound_loc :=GET_OUTBOUND_FILE_LOCATION ( v_operation =>'DBA_DIRECTORY', v_emp_group  =>g_CI_CAD_GP);
     --
     print_log ('Current working DBA directory is '||g_outbound_loc);
     --     
       g_concur_gl_intf_rec.delete;
       g_total :=0;
    --  
                 --
                 if g_database =g_prd_database then   
                   g_file_prefix :=g_CI_CAD_GP;
                 else
                   g_file_prefix :=g_database||'_'||g_CI_CAD_GP;
                 end if;
                 --          
                 l_file :=g_file_prefix||'_JE_ACCRUALS_'||TO_CHAR(SYSDATE, 'MMDDYYYY')||'.txt';
                 --
                 print_log('Before file open for CI CAD GP JE Accruals.');
                 --
                 l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
                 --
                 print_log ('File handle initiated for CI CAD GP JE Accruals : '||l_file);
                 --                         
                 open c_CI_GP_summary_je;
                 --
                 loop
                  --
                  fetch c_CI_GP_summary_je bulk collect into c_summary_rec;
                  --
                  exit when c_summary_rec.count =0;
                  --
                      if c_summary_rec.count >0 then 
                         --
                         Null;
                         --
                         -- Begin copy the WW JE actuals header record to the file              
                         --
                         begin
                              --
                              l_line_buffer := 'BATCHID'
                                  ||'|'
                                  ||'ASOFDTE'
                                  ||'|'    
                                  ||'TYPE [OOP OR CARD]'
                                  ||'|'
                                  ||'JOURNAL_ACCT_CODE'
                                  ||'|'
                                  ||'DEBIT'
                                  ||'|'       
                                  ||'CREDIT'
                                  ||'|'
                                  ;                  
                              --
                              utl_file.put_line ( l_utl_file_id, l_line_buffer);
                              -- 
                              l_line_count:= l_line_count+1; --number of lines written
                              --
                         exception
                                 when no_data_found then
                                   exit;
                                 when others then
                                   print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                   print_log('@ Inside file loop operation, error : '||sqlerrm);
                                   exit;
                         end;              
                         --
                         --End copy the WW JE actuals header record to the file              
                         --
                         l_line_buffer :=Null;
                         --
                         --Begin copy the WW JE actuals detail records to the file              
                         --
                         for idx in 1 .. c_summary_rec.count loop 
                           --
                           l_line_buffer :=
                                                        c_summary_rec(idx).batchid
                                                                  ||'|'          
                                                                  ||c_summary_rec(idx).asofdte
                                                                  ||'|'
                                                                  ||case
                                                                         when c_summary_rec(idx).payment_type ='OOP' then 'Out of Pocket'
                                                                         when c_summary_rec(idx).payment_type ='CARD' then 'Credit Card'
                                                                         else 'None'                                                        
                                                                       end
                                                                  ||'|'
                                                                  ||c_summary_rec(idx).char_150_8
                                                                  ||'|'           
                                                                  ||c_summary_rec(idx).total_amount
                                                                  ||'|'           
                                                                  ||g_zero
                                                                  ||'|' 
                                                                  ;                
                           --
                                   begin
                                      --
                                      utl_file.put_line ( l_utl_file_id, l_line_buffer);
                                      -- 
                                      l_line_count:= l_line_count+1; --number of lines written
                                      --
                                      g_total := g_total + c_summary_rec(idx).total_amount;
                                      --
                                   exception
                                         when no_data_found then
                                           exit;
                                         when others then
                                           print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                           print_log('@ Inside file loop operation, error : '||sqlerrm);
                                           exit;
                                   end;                
                           --
                          end loop;
                         --
                         --End copy the WW JE actuals detail records to the file              
                         --
                      end if; 
                  --
                 end loop;
                 --
                 close c_CI_GP_summary_je;
                 --
                 -- Begin copy interco clearing rec [CI Brafasco IC with GSC]
                 --
                 begin
                          --
                          open c_CI_GP_IC;
                          --
                          loop
                           --
                            fetch c_CI_GP_IC bulk collect into c_CI_GP_IC_rec;
                            exit when c_CI_GP_IC_rec.count =0;
                            --
                             if c_CI_GP_IC_rec.count >0 then
                               --
                               for idx in 1 .. c_CI_GP_IC_rec.count loop 
                                --
                                   l_line_buffer :=
                                          c_CI_GP_IC_rec(idx).batchid --Check with Mike Macrae
                                      ||'|'
                                      ||c_CI_GP_IC_rec(idx).asofdte --Check with Mike Macrae
                                      ||'|'          
                                      ||l_Brafasco_IC_GSC_Desc||' ['||c_CI_GP_IC_rec(idx).payment_type||']'
                                      ||'|'          
                                      ||l_Brafasco_IC_GSC_Acct
                                      ||'|'
                                      ||g_zero                                      
                                      ||'|'
                                      ||c_CI_GP_IC_rec(idx).brafasco_ic_amt
                                      ||'|'
                                      ;              
                                      --
                                      utl_file.put_line ( l_utl_file_id, l_line_buffer);
                                      -- 
                                      l_line_count:= l_line_count+1; --number of lines written
                                      --
                                      print_log('Copied interco clearing record [Brafasco IC with GSC] to file '||l_file);
                                      --                                    
                                --
                               end loop;                         
                               --
                             end if;
                           --
                          end loop;
                          --
                          close c_CI_GP_IC;
                          --
                 exception
                     when others then
                       print_log('@ Interco clearing, error =>'||sqlerrm||', line :'||l_line_buffer);
                 end;
                 --                         
                 -- End copy interco clearing rec [CI Brafasco IC with GSC]
                 --                 
                 --             
                 print_log('Total records written into the file [Header and Lines] , l_line_count =>'||l_line_count);
                 --
                 -- Close file handle 
                 --
                 utl_file.fclose ( l_utl_file_id );
                 print_log ('After close for CI CAD GP JE Accruals file : '||l_file);
                 --                             
                         begin 
                                -- setup the email body
                                --setup_email_body (p_text1 =>'BU CONCUR employee group : '||g_CI_GP_emp_grp); --g_notif_email_body is assigned here
                                 setup_email_body 
                                   (
                                      p_text1 =>'BU CONCUR employee group : '||g_CI_GP_emp_grp
                                     ,p_emp_group =>g_CI_GP_emp_grp
                                     ,p_type =>'OTHERS'
                                   ); --g_notif_email_body is assigned here                                
                                --print_log('Email Body :'||g_notif_email_body);
                                --
                                g_email_subject :=g_instance||' '||g_CI_GP_emp_grp||' CONCUR extracts to SharePoint - Status Update';
                                print_log('Email Subject :'||g_email_subject);                        
                                --
                                set_bu_sharepoint_email (p_emp_group =>g_CI_GP_emp_grp, p_type =>g_control_type_AA); --g_sharepoint_email is assigned here
                                print_log('SharePoint Email  :'||g_sharepoint_email);                        
                                --
                                -- push the extract to sharepoint
                                print_log('Begin: Push CI CAD Great Plains JE Actuals with supporting details to SharePoint.');                        
                                sharepoint_integration
                                   (
                                        p_emp_group =>g_CI_GP_emp_grp,
                                        p_file =>g_directory_path||'/'||l_file,
                                        p_sharepoint_email =>g_sharepoint_email,
                                        p_dummy_email_from =>g_notif_email_from,
                                        p_email_subject =>g_email_subject
                                   )
                                   ;
                                print_log('End: Push CI CAD Great Plains JE accruals with supporting details to SharePoint.');                           
                                -- email the teams about the extract
                                send_email_notif
                                   (
                                        p_email_from      =>g_notif_email_from,
                                        p_email_to           =>g_notif_email_to,
                                        p_email_cc           =>g_notif_email_cc,
                                        p_email_subject =>g_email_subject, 
                                        p_email_body     =>g_notif_email_body,
                                        p_mime_type      =>g_email_mime_type
                                   )
                                  ; 
                                print_log('After sending email notification about CI CAD Great Plains JE Actual and Details status');                                                                                                
                                --                                  
                         exception
                          when others then
                           print_log('@Failed to log file details for BU, @ emp_group '||g_WW_emp_grp||', message =>'||sqlerrm);
                           rollback to start_here;
                         end;
                         --     
                 -- 
                 -- %%%%
                         -- Begin copy liability rec [ Insert to concur gl interface table]
                         --
                         g_concur_gl_intf_rec.delete; --Cleanup plsql collection 
                         -- $$$$
                             --                       
                             open c_CI_GP_interco_je;
                             --
                             loop
                              --
                              fetch c_CI_GP_interco_je bulk collect into c_interco_rec;
                              --
                              exit when c_interco_rec.count =0;
                              --
                                  if c_interco_rec.count >0 then 
                                     --
                                     Null;
                                     --
                                     l_line_idx :=0;
                                     --
                                     --Begin copy the WW JE actuals detail records to the file              
                                     --
                                     for idx in 1 .. c_interco_rec.count loop          
                                       --
                                       l_line_idx :=l_line_idx+1;
                                       --
                                               begin
                                                  -- ++++
                                                     begin
                                                              -- Set set of books id
                                                              g_concur_gl_intf_rec(l_line_idx).set_of_books_id :=g_CAD_ledger_id;
                                                              -- Set accounting date
                                                              g_concur_gl_intf_rec(l_line_idx).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                                              -- Set currency          
                                                              g_concur_gl_intf_rec(l_line_idx).currency_code := Get_Currency (p_emp_group =>g_CI_GP_emp_grp); --Set currency based on concur emp group
                                                              -- Set Batch Name and description
                                                              g_concur_gl_intf_rec(l_line_idx).reference1 :='Accrual Batch ';                                                    
                                                              g_concur_gl_intf_rec(l_line_idx).reference2 :='Employee Group :'||g_CI_GP_emp_grp;                          
                                                              --
                                                              -- Set Journal entry name and description
                                                              g_concur_gl_intf_rec(l_line_idx).reference4 :=c_interco_rec(idx).reference4; --GL used first 25 characters to prepend the journal entry name                                                   
                                                              g_concur_gl_intf_rec(l_line_idx).reference5 :=c_interco_rec(idx).reference5;                          
                                                              --
                                                              -- Set Journal entry line description                                                                            
                                                              g_concur_gl_intf_rec(l_line_idx).reference10 :=c_interco_rec(idx).reference10;
                                                              --      
                                                              g_concur_gl_intf_rec(l_line_idx).group_id :=g_CI_GP_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                                              --
                                                              g_concur_gl_intf_rec(l_line_idx).reference21 :='Accrual Batch  '; --Concur batch id
                                                              g_concur_gl_intf_rec(l_line_idx).reference22 :='Accrual Date - '||to_char(sysdate, 'MM/DD/YYYY'); --Concur Batch Date
                                                              --
                                                              g_concur_gl_intf_rec(l_line_idx).reference25 :=Null;  
                                                              g_concur_gl_intf_rec(l_line_idx).reference26 :=Null;                          
                                                              g_concur_gl_intf_rec(l_line_idx).reference27 :=Null; 
                                                              g_concur_gl_intf_rec(l_line_idx).reference28 :=Null;
                                                              g_concur_gl_intf_rec(l_line_idx).reference29 :=g_run_id;
                                                              g_concur_gl_intf_rec(l_line_idx).reference30 :=Null;                                                                          
                                                              --                    
                                                              --Begin chart of accounts segments
                                                              --
                                                              -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                                              g_concur_gl_intf_rec(l_line_idx).segment1 := Get_Interco_GL_Product (p_emp_group =>g_CI_GP_emp_grp); --Set segment1 based on concur emp group
                                                              --
                                                              g_concur_gl_intf_rec(l_line_idx).segment2 :=c_interco_rec(idx).location;
                                                              g_concur_gl_intf_rec(l_line_idx).segment3 :=c_interco_rec(idx).cost_ctr;
                                                              g_concur_gl_intf_rec(l_line_idx).segment4 :=c_interco_rec(idx).liability_account;
                                                              --
                                                              g_concur_gl_intf_rec(l_line_idx).segment5 :=c_interco_rec(idx).project_code;
                                                              g_concur_gl_intf_rec(l_line_idx).segment6 :=c_interco_rec(idx).fuse1;
                                                              g_concur_gl_intf_rec(l_line_idx).segment7 :=c_interco_rec(idx).fuse2;
                                                              g_concur_gl_intf_rec(l_line_idx).attribute2 :=g_event_type_IC;                                                              
                                                              --
                                                              --End chart of accounts segments    
                                                              -- set Credit amount         
                                                              g_concur_gl_intf_rec(l_line_idx).entered_cr :=c_interco_rec(idx).credit;                          
                                                              --
                                                              print_log('Finished interco liability record');
                                                              --
                                                              l_line_idx :=l_line_idx+1;  --this is required bcoz we fetch only liability records for insert and have to insert interco clearing for every liability
                                                              --                                                          
                                                              -- ^^^^ Setup intercompany clearing debit journal 
                                                              -- Set set of books id
                                                              g_concur_gl_intf_rec(l_line_idx).set_of_books_id :=g_CAD_ledger_id;
                                                              -- Set accounting date
                                                              g_concur_gl_intf_rec(l_line_idx).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                                              -- Set currency          
                                                              g_concur_gl_intf_rec(l_line_idx).currency_code := Get_Currency (p_emp_group =>g_CI_GP_emp_grp); --Set currency based on concur emp group
                                                              -- Set Batch Name and description
                                                              g_concur_gl_intf_rec(l_line_idx).reference1 :='Accrual Batch ';                                               
                                                              g_concur_gl_intf_rec(l_line_idx).reference2 :='Employee Group :'||g_CI_GP_emp_grp;                          
                                                              --
                                                              --
                                                              -- Set Journal entry name and description
                                                              g_concur_gl_intf_rec(l_line_idx).reference4 :=c_interco_rec(idx).reference4; --GL used first 25 characters to prepend the journal entry name                                                   
                                                              g_concur_gl_intf_rec(l_line_idx).reference5 :=c_interco_rec(idx).reference5;                          
                                                              --
                                                              -- Set Journal entry line description                                                                            
                                                              g_concur_gl_intf_rec(l_line_idx).reference10 :=c_interco_rec(idx).reference10;
                                                              --    
                                                              g_concur_gl_intf_rec(l_line_idx).group_id :=g_CI_GP_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                                              --
                                                              g_concur_gl_intf_rec(l_line_idx).reference21 :='Accrual Batch  '; --Concur batch id
                                                              g_concur_gl_intf_rec(l_line_idx).reference22 :='Accrual Batch  - '||to_char(sysdate, 'MM/DD/YYYY'); --Concur Batch Date
                                                              --
                                                              g_concur_gl_intf_rec(l_line_idx).reference25 :=Null;  
                                                              g_concur_gl_intf_rec(l_line_idx).reference26 :=Null;                          
                                                              g_concur_gl_intf_rec(l_line_idx).reference27 :=Null; 
                                                              g_concur_gl_intf_rec(l_line_idx).reference28 :=Null;
                                                              g_concur_gl_intf_rec(l_line_idx).reference29 :=g_run_id;
                                                              g_concur_gl_intf_rec(l_line_idx).reference30 :=Null;                                                                               
                                                              --                    
                                                              --Begin chart of accounts segments
                                                              --
                                                              -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                                              g_concur_gl_intf_rec(l_line_idx).segment1 := Get_Interco_GL_Product (p_emp_group =>g_CI_GP_emp_grp); --Set segment1 based on concur emp group
                                                              --
                                                              g_concur_gl_intf_rec(l_line_idx).segment2 :=c_interco_rec(idx).location;
                                                              g_concur_gl_intf_rec(l_line_idx).segment3 :=c_interco_rec(idx).cost_ctr;
                                                              g_concur_gl_intf_rec(l_line_idx).segment4 :=c_interco_rec(idx).interco_clearing;
                                                              --
                                                              g_concur_gl_intf_rec(l_line_idx).segment5 :=c_interco_rec(idx).project_code;
                                                              g_concur_gl_intf_rec(l_line_idx).segment6 :=c_interco_rec(idx).fuse1;
                                                              g_concur_gl_intf_rec(l_line_idx).segment7 :=c_interco_rec(idx).fuse2;
                                                              g_concur_gl_intf_rec(l_line_idx).attribute2 :=g_event_type_IC;                                                              
                                                              --
                                                              --End chart of accounts segments    
                                                              -- set Debit amount         
                                                              g_concur_gl_intf_rec(l_line_idx).entered_dr :=c_interco_rec(idx).debit;                          
                                                              --
                                                              print_log('Finished interco clearing record');
                                                              --                                                    
                                                              -- ^^^^
                                                              -- Insert into xxcus_concur_gl_iface table
                                                              begin 
                                                                --
                                                                    flush_concur_gl_iface 
                                                                      (
                                                                         p_emp_group     =>g_CI_GP_emp_grp
                                                                        ,p_oop_or_card  =>Null --Not necessary
                                                                        ,p_group_id         =>g_CI_GP_grp_id
                                                                      );                                                                 
                                                                --
                                                                populate_concur_gl_iface 
                                                                    (
                                                                       p_concur_gl_intf_rec  =>g_concur_gl_intf_rec
                                                                      ,p_group_id                    =>g_CI_GP_grp_id
                                                                    );                                                                    
                                                              exception
                                                               when others then
                                                                print_log('@populate_concur_gl_iface, calling block error : '||sqlerrm);
                                                                raise program_error;
                                                              end;
                                                              --
                                                     exception
                                                         when others then
                                                            print_log('@Setup Interco Liability and Clearing, error =>'||sqlerrm||', line :'||l_line_buffer);
                                                     end;        
                                                     --                                               
                                                  -- ++++
                                               exception
                                                     when others then                         
                                                       print_log('@interco journal, Inside file loop operation, error : '||sqlerrm);
                                                       exit;
                                               end;
                                       --
                                     end loop;
                                     --
                                     --End copy the WW JE actuals detail records to the file              
                                     --
                                  end if; 
                              --
                             end loop;
                             --
                             close c_CI_GP_interco_je;
                             --    
                         -- $$$$     
                         -- End copy liability rec [ Insert to concur gl interface table]                                   
                 -- %%%%    
                 l_line_count :=0;
                 --  
                 l_file :=g_file_prefix||'_ACCRUAL_DETAILS_'||TO_CHAR(SYSDATE, 'MMDDYYYY')||'.txt';
                 --
                 print_log('Before file open for CI CAD GP JE Accrual Details.');
                 --
                 l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
                 --
                 print_log ('File handle initiated for CI CAD GP JE Accrual Details : '||l_file);
                 --    
                 OPEN c_ci_cad_gp_lines;
                 FETCH c_ci_cad_gp_lines BULK COLLECT INTO c_ci_cad_gp_rec;
                 if c_ci_cad_gp_rec.count >0 then
                   --
                   OPEN c_ci_cad_gp_header;
                   FETCH c_ci_cad_gp_header INTO l_line_buffer;
                   CLOSE c_ci_cad_gp_header;
                   --
                   utl_file.put_line ( l_utl_file_id, l_line_buffer);
                   -- 
                   l_line_count:= l_line_count+1; --number of lines written
                   --  
                   for idx in 1 .. c_ci_cad_gp_rec.count loop 
                        --
                        l_line_buffer :=c_ci_cad_gp_rec(idx).rec_info;
                        --
                       begin
                          --
                          utl_file.put_line ( l_utl_file_id, l_line_buffer);
                          -- 
                          l_line_count:= l_line_count+1; --number of lines written
                          --
                       exception
                             when no_data_found then
                               exit;
                             when others then
                               print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                               print_log('@ Inside file loop operation, error : '||sqlerrm);
                               exit;
                       end;                     
                    --
                   end loop; 
                   --                 
                 else
                  print_log(' ');
                  print_log(' No accrual details found for ERP :'||g_CI_GP_emp_grp);
                  print_log(' ');                                    
                 end if;
                 CLOSE c_ci_cad_gp_lines;
                 -- Close file handle 
                 --
                 utl_file.fclose ( l_utl_file_id );
                 print_log ('After close for CI CAD GP JE Accrual Details file : '||l_file);
                 --  
                         --
                         begin 
                                --
                                 setup_email_body 
                                   (
                                      p_text1 =>'BU CONCUR employee group : '||g_CI_GP_emp_grp
                                     ,p_emp_group =>g_CI_GP_emp_grp
                                     ,p_type =>'OTHERS'
                                   ); --g_notif_email_body is assigned here                                
                                --print_log('Email Body :'||g_notif_email_body);
                                --
                                g_email_subject :=g_instance||' '||g_CI_GP_emp_grp||' CONCUR extracts to SharePoint - Status Update';
                                print_log('Email Subject :'||g_email_subject);                        
                                --
                                set_bu_sharepoint_email (p_emp_group =>g_CI_GP_emp_grp, p_type =>g_control_type_AA); --g_sharepoint_email is assigned here
                                print_log('SharePoint Email  :'||g_sharepoint_email);                        
                                --
                                -- push the extract to sharepoint
                                print_log('Begin: Push CI CAD Great Plains JE accrual Detail file to SharePoint.');                        
                                sharepoint_integration
                                   (
                                        p_emp_group =>g_CI_GP_emp_grp,
                                        p_file =>g_directory_path||'/'||l_file,
                                        p_sharepoint_email =>g_sharepoint_email,
                                        p_dummy_email_from =>g_notif_email_from,
                                        p_email_subject =>g_email_subject
                                   )
                                   ;
                                print_log('End: Push CI CAD Great Plains JE accrual Detail file to SharePoint.');                           
                                --                                 
                         exception
                          when others then
                           print_log('@Failed to log file details for BU, @ emp_group '||g_WW_emp_grp||', message =>'||sqlerrm);
                           rollback to start_here;
                         end;
                         --                                                        
     --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    raise program_error;
  end CI_GP_OB_GL_JE;
  -- End CI Great Plains extract je files and generate Interco gl journal interface records   
  --     
  -- Begin CI US extract gl eligible je
  procedure CI_ORACLE_US_ELIGIBLE_GL_JE is 
    --extract Construction and Industrical [CI] US GL journal related info 
    --
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;  
    --
    l_prev_batch_date date :=Null;
    l_sub_routine varchar2(30) :='ci_oracle_us_eligible_gl_je';
    l_ww_je_seq varchar2(10) :=Null;
    --
    cursor ci_us_je_eligible_records is
    select 
             rpt_id
            ,emp_id
            ,emp_full_name
            ,accrual_erp
            ,segment1 accrual_company
            ,segment2 accrual_branch
            ,segment3 accrual_dept
            ,segment4 accrual_account
            ,segment5 accrual_project
            ,run_id
            ,accrual_period accrual_period
            ,reversal_accrual_period reversal_period
            ,accrual_fiscal_start
            ,accrual_fiscal_end
            ,accrual_status
            ,rpt_entry_pmt_code_name char_150_1
            ,rpt_entry_pmt_type_code char_150_2
            ,PROGRAM_NAME char_150_3
            ,rpt_id char_150_4
            ,rpt_name char_150_5
            ,rpt_entry_vendor_name char_150_6
            ,reimbursement_currency char_150_7
            ,segment4 char_150_8
            ,Null char_150_9
            ,Null char_150_10
            ,Null char_150_11 --je main account
            ,Null char_150_12 -- je sub account
            ,Null char_150_13 --WW je month 
            ,Null char_150_14 --WW je year 
            ,Null char_150_15 --may get replaced because of routine transform_ww_branch
            ,Null char_150_16 --may get replaced because of the resulting value from routine transform_ww_branch
            ,Null char_150_17
            ,Null char_150_18
            ,Null char_150_19
            ,Null char_150_20
            ,ledger_id num_1
            ,posted_amount num_2
            ,rpt_key num_3
            ,null num_4
            ,null num_5
            ,date_first_submitted date_1
            ,posted_date date_2
            ,trans_date date_3
            ,null date_4
            ,null date_5
            ,oop_card
            ,expense_type
            ,expense_code            
    from xxcus.xxcus_concur_accruals_staging
    where 1 =1
    and accrual_erp =g_CI_US_emp_grp
    and accrual_status =g_status_new
    and personal_flag =g_N
    and run_id =g_run_id    
    and validation_failed =g_N
    and exists
             (
                  select '1'
                  from    fnd_lookup_values
                  where 1 =1
                        and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
                        and nvl(enabled_flag, 'N') ='Y'
                        and lookup_code =accrual_erp
             )         
    ;
    --
    type gp_je_eligible_rec_type is table of xxcus.xxcus_concur_accruals_elig_je%rowtype index by binary_integer;
    gp_je_eligible_rec gp_je_eligible_rec_type;
    --
  begin
                 --
--                        delete xxcus.xxcus_concur_accruals_elig_je 
--                        where 1 =1
--                             and accrual_erp =g_CI_US_emp_grp
--                             and accrual_status =g_status_NEW
--                             ; 
                 --
                  print_log('@ '||l_sub_routine||' Flush any previous loaded eligible JE records for employee group '||g_CI_US_emp_grp||', total deleted :'||sql%rowcount);
                 --
                open ci_us_je_eligible_records;
                loop 
                 --
                 fetch ci_us_je_eligible_records bulk collect into gp_je_eligible_rec;
                 --
                 exit when gp_je_eligible_rec.count =0;
                     --
                     if gp_je_eligible_rec.count >0 then 
                          --
                          -- bulk insert into xxcus.xxcus_concur_accruals_elig_je table
                          --
                          begin
                           forall idx in 1 .. gp_je_eligible_rec.count 
                            insert into xxcus.xxcus_concur_accruals_elig_je values gp_je_eligible_rec(idx);
                            print_log('Total CI ORACLE US eligible je lines inserted : '||sql%rowcount);                            
                          exception
                           when others then
                            print_log('Error in bulk insert of xxcus.xxcus_concur_ww_eligible_je, message =>'||sqlerrm);
                            raise program_error;
                          end;
                         --
                     end if;
                     --
                end loop;
                close ci_us_je_eligible_records;
                --
  exception
   when program_error then
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end CI_ORACLE_US_ELIGIBLE_GL_JE;        
  --  
  -- End CI US extract gl eligible je  
  -- Begin CI US insert je actuals and generate Interco gl journal interface records
  procedure CI_US_GL_JE is
   --
    l_sub_routine varchar2(30) :='ci_us_gl_je'; --construction and industrial US gl journal entries
    l_line_count number :=0;
    --
    l_wc_oop_clearing_amt number :=0;
    l_wc_card_clearing_amt number :=0;    
    --
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;      
    --
    -- Begin variables used for GSC Interco with CI US    
    l_Interco_Acct_Date Date :=Trunc(Sysdate);
    I_CI_GSC_Interco_loc            xxcus.xxcus_concur_gl_iface.segment2%type  :='BW080';
    I_CI_GSC_Interco_Dept        xxcus.xxcus_concur_gl_iface.segment3%type :='0000';
    I_CI_GSC_Interco_Acct         xxcus.xxcus_concur_gl_iface.segment4%type  :='930118';    
    l_CI_US_IC_GSC_Desc  xxcus.xxcus_concur_gl_iface.reference10%type :='CI US IC with GSC';    
    -- End variables used for GSC Interco with CI US    
    --
    -- Begin variables are used for GSC Interco with CI US
    l_Interco_Location              xxcus.xxcus_concur_gl_iface.segment2%type :='Z0186';
    l_Interco_Cost_Ctr              xxcus.xxcus_concur_gl_iface.segment3%type :='0000';
    l_Interco_OOP_Liability     xxcus.xxcus_concur_gl_iface.segment4%type :='211121';
    l_Interco_CARD_Liability   xxcus.xxcus_concur_gl_iface.segment4%type :='211126'; 
    l_Interco_OOP_Clearing    xxcus.xxcus_concur_gl_iface.segment4%type :='910122';
    l_Interco_Project                 xxcus.xxcus_concur_gl_iface.segment5%type :='00000';
    l_Interco_Clearing_Desc  xxcus.xxcus_concur_gl_iface.reference10%type :='GSC IC with CI US';
    -- End variables are used for GSC Interco with CI US        
    --
    l_utl_file_id   UTL_FILE.FILE_TYPE;
    l_line_buffer VARCHAR2(30000):= Null;
    l_file VARCHAR2(150) :=Null;
    l_line_idx Number :=0;
    --
    cursor c_CI_US_summary_je  is
    select
                   oop_card payment_type
                  ,accrual_company  gl_product
                  ,accrual_branch  gl_location
                  ,accrual_dept cost_ctr
                  ,accrual_account charge_account
                  ,accrual_project project_code
                  ,g_future_use_1  fuse1
                  ,g_future_use_2  fuse2
                  ,Substr(emp_full_name||'- '||char_150_6||' : '||expense_type, 1, 240) line_description -- emp name - vendor : expense type
                  ,case 
                     when oop_card ='OOP'  then 'Interco OOP' 
                     when oop_card ='CARD'  then 'Interco Card'    
                     else 'Other than OOP/Card found'             
                   end reference4
                  ,case 
                     when oop_card ='OOP'  then 'Employee Group '||g_CI_US_emp_grp ||', Interco for OOP'
                     when oop_card ='CARD'  then 'Employee Group '||g_CI_US_emp_grp||', Interco for CARD'                             
                     else 'Journal for other than OOP/Card'             
                   end reference5                   
                  ,'CI US IC with GSC '||'Emp ID :'||emp_id||'Posted Date :'||to_char(date_2, 'MM/DD/YYYY')  reference10                  
                  ,sum(num_2) charge_amount 
from xxcus.xxcus_concur_accruals_elig_je
where 1 =1 
    and accrual_erp =g_CI_US_emp_grp
    and run_id =g_run_id
    and accrual_status =g_status_NEW  
group by         
                   to_char(date_2, 'MM/DD/YYYY') 
                  ,oop_card 
                  ,accrual_company  
                  ,accrual_branch  
                  ,accrual_dept 
                  ,accrual_account 
                  ,accrual_project 
                  ,g_future_use_1  
                  ,g_future_use_2  
                  ,Substr(emp_full_name||'- '||char_150_6||' : '||expense_type, 1, 240)  
                  ,case 
                     when oop_card ='OOP'  then 'Interco OOP' 
                     when oop_card ='CARD'  then 'Interco Card'    
                     else 'Other than OOP/Card found'             
                   end 
                  ,case 
                     when oop_card ='OOP'  then 'Employee Group '||g_CI_US_emp_grp ||', Interco for OOP'
                     when oop_card ='CARD'  then 'Employee Group '||g_CI_US_emp_grp||', Interco for CARD'                             
                     else 'Journal for other than OOP/Card'             
                   end                    
                  ,'CI US IC with GSC '||'Emp ID :'||emp_id||'Posted Date :'||to_char(date_2, 'MM/DD/YYYY') 
    ;     
    --
    type c_summary_je_type is table of c_CI_US_summary_je%rowtype index by binary_integer;
    c_summary_rec  c_summary_je_type; 
    --
    l_oop_interco_clearing c_summary_je_type;
    l_card_interco_clearing c_summary_je_type;
    -- 
    cursor c_ci_oracle_US_header is
    select
            'EMP_ID'
            ||'|'
            ||'EMP_FULL_NAME'
            ||'|'        
            ||'EXPENSE_TYPE'
            ||'|'    
            ||'EXPENSE_CODE'
            ||'|'    
            ||'RPT_ENTRY_PMT_CODE_NAME'
            ||'|'    
            ||'PROGRAM_NAME'
            ||'|'    
            ||'APPROVAL_STATUS'
            ||'|'    
            ||'RPT_ID'
            ||'|'    
            ||'RPT_NAME'
            ||'|'    
            ||'DATE_FIRST_SUBMITTED'
            ||'|'    
            ||'RPT_ENTRY_VENDOR_NAME'
            ||'|'    
            ||'POSTED_DATE'
            ||'|'    
            ||'TRANS_DATE'
            ||'|'    
            ||'COUNTRY'
            ||'|'    
            ||'REIMBURSEMENT_CURRENCY'
            ||'|'    
            ||'POSTED_AMOUNT'
            ||'|'    
            ||'ACCOUNT_CODE1'
            ||'|'    
            ||'ACCOUNT_CODE2'
            ||'|'    
            ||'RPT_ORG_UNIT_1'
            ||'|'    
            ||'RPT_ORG_UNIT_2'
            ||'|'    
            ||'RPT_ORG_UNIT_3'
            ||'|'    
            ||'RPT_ORG_UNIT_4'
            ||'|'    
            ||'RPT_ORG_UNIT_6'
            ||'|'    
            ||'PERSONAL_FLAG'
            ||'|'    
            ||'RPT_ENTRY_PMT_TYPE_CODE'
            ||'|'    
            ||'RPT_CUSTOM1'
            ||'|'    
            ||'RPT_CUSTOM2'
            ||'|'    
            ||'RPT_CUSTOM3'
            ||'|'    
            ||'RPT_CUSTOM4'
            ||'|'    
            ||'RPT_CUSTOM6'
            ||'|'    
            ||'EMP_ORG_UNIT1'
            ||'|'    
            ||'EMP_ORG_UNIT2'
            ||'|'    
            ||'EMP_ORG_UNIT3'
            ||'|'    
            ||'EMP_ORG_UNIT4'
            ||'|'    
            ||'EMP_ORG_UNIT6'
            ||'|'    
            ||'EMP_CUSTOM1'
            ||'|'    
            ||'EMP_CUSTOM2'
            ||'|'    
            ||'EMP_CUSTOM3'
            ||'|'    
            ||'EMP_CUSTOM4'
            ||'|'    
            ||'EMP_CUSTOM6'
            ||'|'    
            ||'ACCRUAL_PERIOD'
            ||'|'    
            ||'REVERSAL_ACCRUAL_PERIOD'
            ||'|'    
            ||'RUN_ID'
            ||'|'  
   from   dual; 
    --     
    cursor c_ci_oracle_US_lines is
    select
               EMP_ID
            ||'|'
            ||EMP_FULL_NAME
            ||'|'        
            ||EXPENSE_TYPE
            ||'|'    
            ||EXPENSE_CODE
            ||'|'    
            ||RPT_ENTRY_PMT_CODE_NAME
            ||'|'    
            ||PROGRAM_NAME
            ||'|'    
            ||APPROVAL_STATUS
            ||'|'    
            ||RPT_ID
            ||'|'    
            ||RPT_NAME
            ||'|'    
            ||DATE_FIRST_SUBMITTED
            ||'|'    
            ||RPT_ENTRY_VENDOR_NAME
            ||'|'    
            ||POSTED_DATE
            ||'|'    
            ||TRANS_DATE
            ||'|'    
            ||COUNTRY
            ||'|'    
            ||REIMBURSEMENT_CURRENCY
            ||'|'    
            ||POSTED_AMOUNT
            ||'|'    
            ||ACCOUNT_CODE1
            ||'|'    
            ||ACCOUNT_CODE2
            ||'|'    
            ||RPT_ORG_UNIT_1
            ||'|'    
            ||RPT_ORG_UNIT_2
            ||'|'    
            ||RPT_ORG_UNIT_3
            ||'|'    
            ||RPT_ORG_UNIT_4
            ||'|'    
            ||RPT_ORG_UNIT_6
            ||'|'    
            ||PERSONAL_FLAG
            ||'|'    
            ||RPT_ENTRY_PMT_TYPE_CODE
            ||'|'    
            ||RPT_CUSTOM1
            ||'|'    
            ||RPT_CUSTOM2
            ||'|'    
            ||RPT_CUSTOM3
            ||'|'    
            ||RPT_CUSTOM4
            ||'|'    
            ||RPT_CUSTOM6
            ||'|'    
            ||EMP_ORG_UNIT1
            ||'|'    
            ||EMP_ORG_UNIT2
            ||'|'    
            ||EMP_ORG_UNIT3
            ||'|'    
            ||EMP_ORG_UNIT4
            ||'|'    
            ||EMP_ORG_UNIT6
            ||'|'    
            ||EMP_CUSTOM1
            ||'|'    
            ||EMP_CUSTOM2
            ||'|'    
            ||EMP_CUSTOM3
            ||'|'    
            ||EMP_CUSTOM4
            ||'|'    
            ||EMP_CUSTOM6
            ||'|'    
            ||ACCRUAL_PERIOD
            ||'|'    
            ||REVERSAL_ACCRUAL_PERIOD
            ||'|'    
            ||RUN_ID
            ||'|'  rec_info
    from xxcus.xxcus_concur_accruals_staging
    where 1 =1
        and accrual_erp =g_CI_US_emp_grp
        and run_id =g_run_id     
     ;  
   --
   type c_ci_oracle_US_type is table of c_ci_oracle_US_lines%rowtype index by binary_integer;
   c_ci_oracle_US_rec c_ci_oracle_US_type;
   --       
  begin
     --
       g_concur_gl_intf_rec.delete;
       g_total :=0;
       l_wc_oop_clearing_amt  :=0;
       l_wc_card_clearing_amt :=0; 
     --
     g_outbound_loc :=GET_OUTBOUND_FILE_LOCATION ( v_operation =>'DBA_DIRECTORY', v_emp_group  =>g_CI_ORACLE);
     --
     print_log ('Current working DBA directory is '||g_outbound_loc);
     --                 
                 --                         
                 open c_CI_US_summary_je;
                 --
                 loop
                  --
                  fetch c_CI_US_summary_je bulk collect into c_summary_rec;
                  --
                  exit when c_summary_rec.count =0;
                  --
                      if c_summary_rec.count >0 then 
                         --
                         Null;
                         --
                         --Begin insert of actuals charges into concur gl interface for whitecap
                         --
                         for idx in 1 .. c_summary_rec.count loop         
                               --
                               if c_summary_rec(idx).payment_type ='OOP' then
                                  --
                                  if l_oop_interco_clearing.EXISTS(1) = FALSE then
                                      --
                                      l_oop_interco_clearing(1) :=c_summary_rec(idx); --need just one record of type OOP if found
                                      --                          
                                  end if;
                                  --
                                  l_wc_oop_clearing_amt :=l_wc_oop_clearing_amt + c_summary_rec(idx).charge_amount;
                                  -- 
                               elsif c_summary_rec(idx).payment_type ='CARD' then
                                  --
                                  if l_card_interco_clearing.EXISTS(1) = FALSE then
                                      --
                                      l_card_interco_clearing(1) :=c_summary_rec(idx); --need just one record of type CARD if found
                                      -- 
                                  end if;
                                  -- 
                                  l_wc_card_clearing_amt :=l_wc_card_clearing_amt + c_summary_rec(idx).charge_amount;
                                  --                         
                               else 
                                  --
                                  Null; 
                                  --                          
                               end if;
                               --
                               begin
                                  -- $$$$
                                    begin
                                              -- Set set of books id
                                              g_concur_gl_intf_rec(idx).set_of_books_id :=g_US_ledger_id;
                                              -- Set accounting date
                                              g_concur_gl_intf_rec(idx).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                              -- Set currency          
                                              g_concur_gl_intf_rec(idx).currency_code := Get_Currency (p_emp_group =>g_CI_US_emp_grp); --Set currency based on concur emp group
                                              -- Set Batch Name and description
                                              g_concur_gl_intf_rec(idx).reference1 :='Accrual Run ID :<'||g_run_id||'>';                                                    
                                              g_concur_gl_intf_rec(idx).reference2 :='Employee Group :'||g_CI_US_emp_grp;                          
                                              --
                                              -- Set Journal entry name and description
                                                    g_concur_gl_intf_rec(idx).reference4 :=c_summary_rec(idx).reference4;   
                                                    g_concur_gl_intf_rec(idx).reference5 :=c_summary_rec(idx).reference5;
                                                    -- Set Journal entry line description                                                                            
                                                    g_concur_gl_intf_rec(idx).reference10 :=c_summary_rec(idx).line_description;                   
                                              --      
                                              g_concur_gl_intf_rec(idx).group_id :=g_CI_US_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                              --
                                              g_concur_gl_intf_rec(idx).reference21 :='Accrual Run ID :<'||g_run_id||'>';
                                              g_concur_gl_intf_rec(idx).reference22 :='Employee Group :'||g_CI_US_emp_grp;
                                              g_concur_gl_intf_rec(idx).reference23 :=c_summary_rec(idx).payment_type;
                                              g_concur_gl_intf_rec(idx).reference24 :=c_summary_rec(idx).reference10;
                                              --
                                              g_concur_gl_intf_rec(idx).reference25 :=Null;  
                                              g_concur_gl_intf_rec(idx).reference26 :=Null;                          
                                              g_concur_gl_intf_rec(idx).reference27 :=Null; 
                                              g_concur_gl_intf_rec(idx).reference28 :=Null;
                                              g_concur_gl_intf_rec(idx).reference29 :=g_run_id;
                                              g_concur_gl_intf_rec(idx).reference30 :=Null;                                                                          
                                              --                    
                                              --Begin chart of accounts segments
                                              --
                                              -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                              g_concur_gl_intf_rec(idx).segment1 := c_summary_rec(idx).gl_product; 
                                              --
                                              g_concur_gl_intf_rec(idx).segment2 :=c_summary_rec(idx).gl_location;
                                              g_concur_gl_intf_rec(idx).segment3 :=c_summary_rec(idx).cost_ctr;
                                              g_concur_gl_intf_rec(idx).segment4 :=c_summary_rec(idx).charge_account;
                                              --
                                              g_concur_gl_intf_rec(idx).segment5 :=c_summary_rec(idx).project_code;
                                              g_concur_gl_intf_rec(idx).segment6 :=c_summary_rec(idx).fuse1;
                                              g_concur_gl_intf_rec(idx).segment7 :=c_summary_rec(idx).fuse2;
                                              g_concur_gl_intf_rec(idx).attribute2 :=g_event_type_EXP;
                                              --
                                              --End chart of accounts segments
                                              -- set Credit amount         
                                              g_concur_gl_intf_rec(idx).entered_dr :=c_summary_rec(idx).charge_amount;                          
                                              --
                                              --print_log('Finished accrual interco clearing [CI US IC WITH GSC] record.');
                                              --
                                    exception
                                         when others then
                                            print_log('@Setup Interco Liability and Clearing, error =>'||sqlerrm||', line :'||l_line_buffer);
                                    end;        
                                     --                                       
                                  -- $$$$
                                  l_line_count:= l_line_count+1; --number of lines written
                                  --
                               exception
                                     when no_data_found then
                                       exit;
                                     when others then  
                                       print_log ('Line Description : '||c_summary_rec(idx).line_description);                           
                                       print_log('@ Inside file loop operation, error : '||sqlerrm);
                                       exit;
                               end;                
                           --
                          end loop;
                         --
                         --End copy the CI US JE actuals detail records to the file              
                         --
                         -- ^^^^
                          -- Insert into xxcus_concur_gl_iface table
                          begin
                            --
                                flush_concur_gl_iface  --run only once
                                  (
                                     p_emp_group     =>g_CI_US_emp_grp
                                    ,p_oop_or_card  =>'OOP' --All gl interface records where reference23 is OOP, group id 66 with status NEW
                                    ,p_group_id         =>g_CI_US_grp_id
                                  ); 
                            -- 
                                flush_concur_gl_iface  --run only once
                                  (
                                     p_emp_group     =>g_CI_US_emp_grp
                                    ,p_oop_or_card  =>'CARD' --All gl interface records where reference23 is CARD, group id 66 with status NEW
                                    ,p_group_id         =>g_CI_US_grp_id
                                  );                                                                                                  
                            --        
                            populate_concur_gl_iface 
                                (
                                   p_concur_gl_intf_rec  =>g_concur_gl_intf_rec
                                  ,p_group_id                    =>g_CI_US_grp_id
                                );                                
                          exception
                           when others then
                            print_log('@populate_concur_gl_iface, calling block error : '||sqlerrm);
                            raise program_error;
                          end;
                         -- ^^^^                        
                      end if; 
                  --
                 end loop;
                 --
                 close c_CI_US_summary_je;
                 --
                 g_concur_gl_intf_rec.delete; --cleanup records from plsql collection table
                 --
                 l_line_count :=0;
                 --
                 -- Begin insert of CI US Interco clearing with GSC for Out of Pocket AND GSC IC WITH CI US
                 if l_oop_interco_clearing.count >0 then
                   --
                   for idx in 1 .. l_oop_interco_clearing.count loop
                        --
                        l_line_count := l_line_count + 1;
                        --
                        begin
                                  -- Set set of books id
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_CI_US_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Accrual Run ID :<'||g_run_id||'>';                                                 
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_CI_US_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_oop_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_oop_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_oop_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_CI_US_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Accrual Run ID :<'||g_run_id||'>'; 
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Employee Group :'||g_CI_US_emp_grp;
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_oop_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_oop_interco_clearing(idx).reference10; --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 := l_oop_interco_clearing(idx).gl_product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=I_CI_GSC_Interco_loc;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=I_CI_GSC_Interco_Dept;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=I_CI_GSC_Interco_Acct;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_oop_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_oop_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_EXP;
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_cr :=l_wc_oop_clearing_amt;                          
                                  --
                                  print_log('Finished interco clearing [CI US IC WITH GSC] record for '||'OOP');
                                  --
                        exception
                             when others then
                                print_log('@CI US to GSC: Setup Interco Clearing, error =>'||sqlerrm);
                        end;
                         --  
                         -- Begin GSC Interco with CI US                       
                        begin
                                   --
                                   l_line_count :=l_line_count +1; --increment plsql collection index so we can add record for GSC interco with CI US
                                   --                        
                                  -- Set set of books id
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_CI_US_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Accrual Run ID :<'||g_run_id||'>';                                                   
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_CI_US_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_oop_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_oop_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_Interco_Clearing_Desc; --l_oop_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_CI_US_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Accrual Run ID :<'||g_run_id||'>';
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Employee Group :'||g_CI_US_emp_grp;   
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_oop_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_oop_interco_clearing(idx).reference10; --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 :=g_Interco_US_Product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=l_Interco_Location;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=l_Interco_Cost_Ctr;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=l_Interco_OOP_Liability;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_oop_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_oop_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_IC;                                  
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_cr :=l_wc_oop_clearing_amt;                          
                                  --
                                  print_log('Finished OOP clearing [GSC Interco with CI US] liability record for '||'OOP');
                                  --
                                  l_line_count :=l_line_count +1; --increment plsql collection index so we can add record for GSC interco with CI US
                                  -- ~~~~~~
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_CI_US_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Accrual Run ID :<'||g_run_id||'>';                                     
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_CI_US_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_oop_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_oop_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_Interco_Clearing_Desc; --l_oop_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_CI_US_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Accrual Run ID :<'||g_run_id||'>';
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Employee Group :'||g_CI_US_emp_grp;
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_oop_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_oop_interco_clearing(idx).reference10; --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 :=g_Interco_US_Product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=l_Interco_Location;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=l_Interco_Cost_Ctr;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=l_Interco_OOP_Clearing;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_oop_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_oop_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_IC;                                  
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_dr :=l_wc_oop_clearing_amt;                          
                                  --
                                  --print_log('Finished Interco clearing [GSC Interco with CI US] record for '||'OOP');
                                  --                                                                    
                                  -- ~~~~~~
                                  -- 
                        exception
                             when others then
                                print_log('@CI US to GSC: Setup Interco Clearing, error =>'||sqlerrm);
                        end;
                         --                           
                         -- End GSC Interco with CI US
                         --   
                   end loop;
                    --
                         -- ^^^^
                          -- Insert into xxcus_concur_gl_iface table
                          begin
                             populate_concur_gl_iface 
                                ( 
                                   p_concur_gl_intf_rec =>g_concur_gl_intf_rec
                                  ,p_group_id                   =>g_CI_US_grp_id
                                );
                          exception
                           when others then
                            print_log('@populate_concur_gl_iface, calling block error : '||sqlerrm);
                            raise program_error;
                          end;
                         -- ^^^^                       
                 else
                 --
                 print_log ('No interco clearning record for CI US IC with GSC');
                 --
                 end if;                              
                 --    
                 -- End insert of CI US Interco clearing with GSC for Out of Pocket   
                 --
                 g_concur_gl_intf_rec.delete; --cleanup records from plsql collection table 
                 --
                 l_line_count :=0;
                 --
                 -- Begin insert of CI US Interco clearing with GSC for CARD
                 if l_card_interco_clearing.count >0 then
                   --
                   for idx in 1 .. l_card_interco_clearing.count loop
                        --
                        l_line_count := l_line_count + 1;                   
                        --
                        begin
                                  -- Set set of books id
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_CI_US_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Accrual Run ID :<'||g_run_id||'>';                                        
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_CI_US_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_card_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_card_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_card_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_CI_US_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Accrual Run ID :<'||g_run_id||'>'; 
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Employee Group :'||g_CI_US_emp_grp;
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_card_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_card_interco_clearing(idx).reference10;  --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 := l_card_interco_clearing(idx).gl_product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=I_CI_GSC_Interco_loc;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=I_CI_GSC_Interco_Dept;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=I_CI_GSC_Interco_Acct;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_card_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_card_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_EXP;                                  
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_cr :=l_wc_card_clearing_amt;                          
                                  --
                                  print_log('Finished interco clearing [CI US IC WITH GSC] record for '||'CARD.');
                                  --
                        exception
                             when others then
                                print_log('@CI US to GSC: Setup Interco Clearing, error =>'||sqlerrm);
                        end;
                         --  
                         -- Begin GSC Interco with CI US for CARD                      
                        begin
                                   --
                                   l_line_count :=l_line_count +1; --increment plsql collection index so we can add record for GSC interco with CI US
                                   --                        
                                  -- Set set of books id
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_CI_US_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Accrual Run ID :<'||g_run_id||'>';                                           
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_CI_US_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_card_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_card_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_Interco_Clearing_Desc; --l_oop_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_CI_US_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Accrual Run ID :<'||g_run_id||'>';
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Employee Group :'||g_CI_US_emp_grp;
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_card_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_card_interco_clearing(idx).reference10; --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 :=g_Interco_US_Product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=l_Interco_Location;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=l_Interco_Cost_Ctr;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=l_Interco_CARD_Liability;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_card_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_card_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_IC;                                  
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_cr :=l_wc_card_clearing_amt;                          
                                  --
                                  print_log('Finished  clearing [GSC Interco with CI US] liability record for '||'CARD');
                                  --
                                  l_line_count :=l_line_count +1; --increment plsql collection index so we can add record for GSC interco with CI US
                                  -- ~~~~~~
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_CI_US_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Accrual Run ID :<'||g_run_id||'>';                          
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_CI_US_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_card_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_card_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_Interco_Clearing_Desc; --l_oop_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_CI_US_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Accrual Run ID :<'||g_run_id||'>';
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Employee Group :'||g_CI_US_emp_grp;
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_card_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_card_interco_clearing(idx).reference10; --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 := g_Interco_US_Product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=l_Interco_Location;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=l_Interco_Cost_Ctr;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=l_Interco_OOP_Clearing;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_card_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_card_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_IC;                                  
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_dr :=l_wc_card_clearing_amt;                          
                                  --
                                  print_log('Finished Interco clearing [GSC Interco with CI US] record for '||'CARD.');
                                  --                                                                    
                                  -- ~~~~~~
                                  -- 
                        exception
                             when others then
                                print_log('@CI US to GSC: Setup Interco Clearing, error =>'||sqlerrm);
                        end;
                         --                           
                         -- End GSC Interco with CI US for CARD                       
                   end loop;
                    --
                     -- ^^^^
                     -- Insert into xxcus_concur_gl_iface table
                      begin
                         populate_concur_gl_iface 
                            ( 
                               p_concur_gl_intf_rec =>g_concur_gl_intf_rec
                              ,p_group_id                   =>g_CI_US_grp_id
                            );
                      exception
                       when others then
                        print_log('@populate_concur_gl_iface, calling block error : '||sqlerrm);
                        raise program_error;
                      end;
                     -- ^^^^                       
                 else
                 --
                 print_log ('Not able to derive interco clearning record for CI US IC with GSC');
                 --
                 end if;
                 --    
                 -- End insert of CI US Interco clearing with GSC for CARD
                 -- 
                 --
                 if g_database =g_prd_database then   
                   g_file_prefix :=g_CI_ORACLE;
                 else
                   g_file_prefix :=g_database||'_'||g_CI_ORACLE;
                 end if;
                 --                   
                 l_line_count :=0;
                 --  
                 l_file :=g_file_prefix||'_ACCRUAL_DETAILS_'||TO_CHAR(SYSDATE, 'MMDDYYYY')||'.txt';
                 --
                 print_log('Before file open for CI ORACLE US accrual details file.');
                 --
                 l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
                 --
                 print_log ('File handle initiated for CI ORACLE US accrual details : '||l_file);
                 --    
                 OPEN c_ci_oracle_US_lines;
                 FETCH c_ci_oracle_US_lines BULK COLLECT INTO c_ci_oracle_US_rec;
                 if c_ci_oracle_US_rec.count >0 then
                   --
                   OPEN c_ci_oracle_US_header;
                   FETCH c_ci_oracle_US_header INTO l_line_buffer;
                   CLOSE c_ci_oracle_US_header;
                   --
                   utl_file.put_line ( l_utl_file_id, l_line_buffer);
                   -- 
                   l_line_count:= l_line_count+1; --number of lines written
                   --  
                   for idx in 1 .. c_ci_oracle_US_rec.count loop 
                        --
                        l_line_buffer :=c_ci_oracle_US_rec(idx).rec_info;
                        --
                       begin
                          --
                          utl_file.put_line ( l_utl_file_id, l_line_buffer);
                          -- 
                          l_line_count:= l_line_count+1; --number of lines written
                          --
                       exception
                             when no_data_found then
                               exit;
                             when others then
                               print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                               print_log('@ Inside file loop operation, error : '||sqlerrm);
                               exit;
                       end;                     
                    --
                   end loop; 
                   --                 
                 else
                  print_log(' ');
                  print_log(' No accrual details found for ERP :'||g_CI_GP_emp_grp);
                  print_log(' ');                                    
                 end if;
                 CLOSE c_ci_oracle_US_lines;
                 -- Close file handle 
                 --
                 utl_file.fclose ( l_utl_file_id );
                 print_log ('After close for CI ORACLE US Accrual Details file : '||l_file);
                 --             
                 -- Begin logging file details for CI US detail file
                 begin 
                        --
                        setup_email_body 
                          (
                             p_text1 =>'BU CONCUR employee group : '||g_CI_US_emp_grp
                            ,p_emp_group =>g_CI_US_emp_grp
                            ,p_type =>'ORACLE_SOURCED'
                          ); --g_notif_email_body is assigned here                           
                        --print_log('Email Body :'||g_notif_email_body);
                        --
                        g_email_subject :=g_instance||' '||g_CI_US_emp_grp||' accrual details to SharePoint - Status Update';
                        print_log('Email Subject :'||g_email_subject);                        
                        --
                        set_bu_sharepoint_email (p_emp_group =>g_CI_US_emp_grp, p_type =>g_control_type_AA); --g_sharepoint_email is assigned here
                        print_log('SharePoint Email  :'||g_sharepoint_email);                        
                        --
                        -- push the extract to sharepoint
                        print_log('Begin: Push CI US Concur accrual details to SharePoint.'); 
                        --                       
                        sharepoint_integration
                           (
                                p_emp_group =>g_CI_US_emp_grp,
                                p_file =>g_directory_path||'/'||l_file,
                                p_sharepoint_email =>g_sharepoint_email,
                                p_dummy_email_from =>g_notif_email_from,
                                p_email_subject =>g_email_subject
                           )
                           ;
                        --
                        print_log('End: Push CI US Concur accrual details to SharePoint.');                           
                        --             
                 exception
                  when others then
                   print_log('@Failed to log file details for BU, @ emp_group '||g_CI_US_emp_grp||', message =>'||sqlerrm);
                 end;
                 -- End logging file details for CI US detail file                     
     --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    raise program_error;
  end CI_US_GL_JE;
  -- End CI US extract je files and generate Interco gl journal interface records   
  -- 
  -- Begin GSC ORACLE US extract gl eligible je
  procedure GSC_ORACLE_US_ELIGIBLE_GL_JE is 
    --extract Construction and Industrical [CI] US GL journal related info 
    --
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;  
    --
    l_prev_batch_date date :=Null;
    l_sub_routine varchar2(30) :='gsc_oracle_us_eligible_gl_je';
    l_ww_je_seq varchar2(10) :=Null;
    --
    cursor gsc_je_eligible_records is
    select 
             rpt_id
            ,emp_id
            ,emp_full_name
            ,accrual_erp
            ,segment1 accrual_company
            ,segment2 accrual_branch
            ,segment3 accrual_dept
            ,segment4 accrual_account
            ,segment5 accrual_project
            ,run_id
            ,accrual_period accrual_period
            ,reversal_accrual_period reversal_period
            ,accrual_fiscal_start
            ,accrual_fiscal_end
            ,accrual_status
            ,rpt_entry_pmt_code_name char_150_1
            ,rpt_entry_pmt_type_code char_150_2
            ,PROGRAM_NAME char_150_3
            ,rpt_id char_150_4
            ,rpt_name char_150_5
            ,rpt_entry_vendor_name char_150_6
            ,reimbursement_currency char_150_7
            ,segment4 char_150_8
            ,Null char_150_9
            ,Null char_150_10
            ,Null char_150_11 --je main account
            ,Null char_150_12 -- je sub account
            ,Null char_150_13 --WW je month 
            ,Null char_150_14 --WW je year 
            ,Null char_150_15 --may get replaced because of routine transform_ww_branch
            ,Null char_150_16 --may get replaced because of the resulting value from routine transform_ww_branch
            ,Null char_150_17
            ,Null char_150_18
            ,Null char_150_19
            ,Null char_150_20
            ,ledger_id num_1
            ,posted_amount num_2
            ,rpt_key num_3
            ,null num_4
            ,null num_5
            ,date_first_submitted date_1
            ,posted_date date_2
            ,trans_date date_3
            ,null date_4
            ,null date_5
            ,oop_card
            ,expense_type
            ,expense_code            
    from xxcus.xxcus_concur_accruals_staging
    where 1 =1
    and accrual_erp =g_GSC_emp_grp
    and accrual_status =g_status_new
    and personal_flag =g_N
    and run_id =g_run_id    
    and validation_failed =g_N
    and exists
             (
                  select '1'
                  from    fnd_lookup_values
                  where 1 =1
                        and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
                        and nvl(enabled_flag, 'N') ='Y'
                        and lookup_code =accrual_erp
             )         
    ;
    --
    type gp_je_eligible_rec_type is table of xxcus.xxcus_concur_accruals_elig_je%rowtype index by binary_integer;
    gp_je_eligible_rec gp_je_eligible_rec_type;
    --
  begin
                 --
                  print_log('@ '||l_sub_routine||' Flush any previous loaded eligible JE records for employee group '||g_CI_US_emp_grp||', total deleted :'||sql%rowcount);
                 --
                open gsc_je_eligible_records;
                loop 
                 --
                 fetch gsc_je_eligible_records bulk collect into gp_je_eligible_rec;
                 --
                 exit when gp_je_eligible_rec.count =0;
                     --
                     if gp_je_eligible_rec.count >0 then 
                          --
                          -- bulk insert into xxcus.xxcus_concur_accruals_elig_je table
                          --
                          begin
                           forall idx in 1 .. gp_je_eligible_rec.count 
                            insert into xxcus.xxcus_concur_accruals_elig_je values gp_je_eligible_rec(idx);
                            print_log('Total GSC ORACLE US eligible je lines inserted : '||sql%rowcount);                            
                          exception
                           when others then
                            print_log('Error in bulk insert of xxcus.xxcus_concur_ww_eligible_je, message =>'||sqlerrm);
                            raise program_error;
                          end;
                         --
                     end if;
                     --
                end loop;
                close gsc_je_eligible_records;
                --
  exception
   when program_error then
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end GSC_ORACLE_US_ELIGIBLE_GL_JE;        
  --  
  -- End GSC ORACLE US extract gl eligible je  
  -- Begin GSC US insert je actuals and generate Interco gl journal interface records
  
  -- Begin GSC ORACLE US insert je actuals and generate Interco gl journal interface records
  procedure GSC_US_GL_JE is
   --
   --
    l_sub_routine varchar2(30) :='gsc_us_gl_je'; --construction and industrial US gl journal entries
    l_line_count number :=0;
    --
    l_wc_oop_clearing_amt number :=0;
    l_wc_card_clearing_amt number :=0;    
    --
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;      
    --
    -- Begin variables used for GSC Interco with Self
    l_Interco_Acct_Date Date :=Trunc(Sysdate);
    I_CI_GSC_Interco_loc            xxcus.xxcus_concur_gl_iface.segment2%type  :='Z0186';
    I_CI_GSC_Interco_Dept        xxcus.xxcus_concur_gl_iface.segment3%type :='0000';
    I_CI_GSC_Interco_Acct         xxcus.xxcus_concur_gl_iface.segment4%type  :='930118';    
    l_CI_US_IC_GSC_Desc  xxcus.xxcus_concur_gl_iface.reference10%type :='IC with GSC';    
    -- End variables used for GSC Interco with CI US    
    --
    -- Begin variables are used for GSC Interco with Self
    l_Interco_Location              xxcus.xxcus_concur_gl_iface.segment2%type :='Z0186';
    l_Interco_Cost_Ctr              xxcus.xxcus_concur_gl_iface.segment3%type :='0000';
    l_Interco_OOP_Liability     xxcus.xxcus_concur_gl_iface.segment4%type :='211121';   --'211120'
    l_Interco_CARD_Liability   xxcus.xxcus_concur_gl_iface.segment4%type :='211126';   --'211125' 
    l_Interco_OOP_Clearing    xxcus.xxcus_concur_gl_iface.segment4%type :='910118';
    l_Interco_Project                 xxcus.xxcus_concur_gl_iface.segment5%type :='00000';
    l_Interco_Clearing_Desc  xxcus.xxcus_concur_gl_iface.reference10%type :='IC with GSC';
    -- End variables are used for GSC Interco with CI US        
    --
    l_utl_file_id   UTL_FILE.FILE_TYPE;
    l_line_buffer VARCHAR2(30000):= Null;
    l_file VARCHAR2(150) :=Null;
    l_line_idx Number :=0;
    --
    cursor c_GSC_summary_je  is
    select
                   oop_card payment_type
                  ,accrual_company  gl_product
                  ,accrual_branch  gl_location
                  ,accrual_dept cost_ctr
                  ,accrual_account charge_account
                  ,accrual_project project_code
                  ,g_future_use_1  fuse1
                  ,g_future_use_2  fuse2
                  ,Substr(emp_full_name||'- '||char_150_6||' : '||expense_type, 1, 240) line_description                  
                  ,case 
                     when oop_card ='OOP'  then 'Interco OOP' 
                     when oop_card ='CARD'  then 'Interco Card'    
                     else 'Other than OOP/Card found'             
                   end reference4
                  ,case 
                     when oop_card ='OOP'  then 'Employee Group '||g_GSC_emp_grp ||', Interco for OOP'
                     when oop_card ='CARD'  then 'Employee Group '||g_GSC_emp_grp||', Interco for CARD'                             
                     else 'Journal for other than OOP/Card'             
                   end reference5                   
                  ,'GSC IC with GSC '||'Emp ID :'||emp_id||'Posted Date :'||to_char(date_2, 'MM/DD/YYYY')  reference10                  
                  ,sum(num_2) charge_amount 
from xxcus.xxcus_concur_accruals_elig_je
where 1 =1 
    and accrual_erp =g_GSC_emp_grp
    and run_id =g_run_id
    and accrual_status =g_status_NEW  
group by         
                   to_char(date_2, 'MM/DD/YYYY') 
                  ,oop_card 
                  ,accrual_company  
                  ,accrual_branch  
                  ,accrual_dept 
                  ,accrual_account 
                  ,accrual_project 
                  ,g_future_use_1  
                  ,g_future_use_2  
                  ,Substr(emp_full_name||'- '||char_150_6||' : '||expense_type, 1, 240) 
                  ,case 
                     when oop_card ='OOP'  then 'Interco OOP' 
                     when oop_card ='CARD'  then 'Interco Card'    
                     else 'Other than OOP/Card found'             
                   end 
                  ,case 
                     when oop_card ='OOP'  then 'Employee Group '||g_GSC_emp_grp ||', Interco for OOP'
                     when oop_card ='CARD'  then 'Employee Group '||g_GSC_emp_grp||', Interco for CARD'                             
                     else 'Journal for other than OOP/Card'             
                   end                    
                  ,'GSC IC with GSC '||'Emp ID :'||emp_id||'Posted Date :'||to_char(date_2, 'MM/DD/YYYY') 
    ;     
    --
    type c_summary_je_type is table of c_GSC_summary_je%rowtype index by binary_integer;
    c_summary_rec  c_summary_je_type; 
    --
    l_oop_interco_clearing c_summary_je_type;
    l_card_interco_clearing c_summary_je_type;
    --    
    cursor c_GSC_US_header is
    select
            'EMP_ID'
            ||'|'
            ||'EMP_FULL_NAME'
            ||'|'        
            ||'EXPENSE_TYPE'
            ||'|'    
            ||'EXPENSE_CODE'
            ||'|'    
            ||'RPT_ENTRY_PMT_CODE_NAME'
            ||'|'    
            ||'PROGRAM_NAME'
            ||'|'    
            ||'APPROVAL_STATUS'
            ||'|'    
            ||'RPT_ID'
            ||'|'    
            ||'RPT_NAME'
            ||'|'    
            ||'DATE_FIRST_SUBMITTED'
            ||'|'    
            ||'RPT_ENTRY_VENDOR_NAME'
            ||'|'    
            ||'POSTED_DATE'
            ||'|'    
            ||'TRANS_DATE'
            ||'|'    
            ||'COUNTRY'
            ||'|'    
            ||'REIMBURSEMENT_CURRENCY'
            ||'|'    
            ||'POSTED_AMOUNT'
            ||'|'    
            ||'ACCOUNT_CODE1'
            ||'|'    
            ||'ACCOUNT_CODE2'
            ||'|'    
            ||'RPT_ORG_UNIT_1'
            ||'|'    
            ||'RPT_ORG_UNIT_2'
            ||'|'    
            ||'RPT_ORG_UNIT_3'
            ||'|'    
            ||'RPT_ORG_UNIT_4'
            ||'|'    
            ||'RPT_ORG_UNIT_6'
            ||'|'    
            ||'PERSONAL_FLAG'
            ||'|'    
            ||'RPT_ENTRY_PMT_TYPE_CODE'
            ||'|'    
            ||'RPT_CUSTOM1'
            ||'|'    
            ||'RPT_CUSTOM2'
            ||'|'    
            ||'RPT_CUSTOM3'
            ||'|'    
            ||'RPT_CUSTOM4'
            ||'|'    
            ||'RPT_CUSTOM6'
            ||'|'    
            ||'EMP_ORG_UNIT1'
            ||'|'    
            ||'EMP_ORG_UNIT2'
            ||'|'    
            ||'EMP_ORG_UNIT3'
            ||'|'    
            ||'EMP_ORG_UNIT4'
            ||'|'    
            ||'EMP_ORG_UNIT6'
            ||'|'    
            ||'EMP_CUSTOM1'
            ||'|'    
            ||'EMP_CUSTOM2'
            ||'|'    
            ||'EMP_CUSTOM3'
            ||'|'    
            ||'EMP_CUSTOM4'
            ||'|'    
            ||'EMP_CUSTOM6'
            ||'|'    
            ||'ACCRUAL_PERIOD'
            ||'|'    
            ||'REVERSAL_ACCRUAL_PERIOD'
            ||'|'    
            ||'RUN_ID'
            ||'|'  
   from   dual; 
    --     
    cursor c_GSC_US_lines is
    select
               EMP_ID
            ||'|'
            ||EMP_FULL_NAME
            ||'|'        
            ||EXPENSE_TYPE
            ||'|'    
            ||EXPENSE_CODE
            ||'|'    
            ||RPT_ENTRY_PMT_CODE_NAME
            ||'|'    
            ||PROGRAM_NAME
            ||'|'    
            ||APPROVAL_STATUS
            ||'|'    
            ||RPT_ID
            ||'|'    
            ||RPT_NAME
            ||'|'    
            ||DATE_FIRST_SUBMITTED
            ||'|'    
            ||RPT_ENTRY_VENDOR_NAME
            ||'|'    
            ||POSTED_DATE
            ||'|'    
            ||TRANS_DATE
            ||'|'    
            ||COUNTRY
            ||'|'    
            ||REIMBURSEMENT_CURRENCY
            ||'|'    
            ||POSTED_AMOUNT
            ||'|'    
            ||ACCOUNT_CODE1
            ||'|'    
            ||ACCOUNT_CODE2
            ||'|'    
            ||RPT_ORG_UNIT_1
            ||'|'    
            ||RPT_ORG_UNIT_2
            ||'|'    
            ||RPT_ORG_UNIT_3
            ||'|'    
            ||RPT_ORG_UNIT_4
            ||'|'    
            ||RPT_ORG_UNIT_6
            ||'|'    
            ||PERSONAL_FLAG
            ||'|'    
            ||RPT_ENTRY_PMT_TYPE_CODE
            ||'|'    
            ||RPT_CUSTOM1
            ||'|'    
            ||RPT_CUSTOM2
            ||'|'    
            ||RPT_CUSTOM3
            ||'|'    
            ||RPT_CUSTOM4
            ||'|'    
            ||RPT_CUSTOM6
            ||'|'    
            ||EMP_ORG_UNIT1
            ||'|'    
            ||EMP_ORG_UNIT2
            ||'|'    
            ||EMP_ORG_UNIT3
            ||'|'    
            ||EMP_ORG_UNIT4
            ||'|'    
            ||EMP_ORG_UNIT6
            ||'|'    
            ||EMP_CUSTOM1
            ||'|'    
            ||EMP_CUSTOM2
            ||'|'    
            ||EMP_CUSTOM3
            ||'|'    
            ||EMP_CUSTOM4
            ||'|'    
            ||EMP_CUSTOM6
            ||'|'    
            ||ACCRUAL_PERIOD
            ||'|'    
            ||REVERSAL_ACCRUAL_PERIOD
            ||'|'    
            ||RUN_ID
            ||'|'  rec_info
    from xxcus.xxcus_concur_accruals_staging
    where 1 =1
        and accrual_erp =g_GSC_emp_grp
        and run_id =g_run_id     
     ;  
   --
   type c_GSC_US_type is table of c_GSC_US_lines%rowtype index by binary_integer;
   c_GSC_US_rec c_GSC_US_type;
   --       
  begin
     --
       g_concur_gl_intf_rec.delete;
       g_total :=0;
       l_wc_oop_clearing_amt  :=0;
       l_wc_card_clearing_amt :=0; 
     --
     g_outbound_loc :=GET_OUTBOUND_FILE_LOCATION ( v_operation =>'DBA_DIRECTORY', v_emp_group  =>g_GSC_ORACLE);
     --
     print_log ('Current working DBA directory is '||g_outbound_loc);
     --                 
                 --                         
                 open c_GSC_summary_je;
                 --
                 loop
                  --
                  fetch c_GSC_summary_je bulk collect into c_summary_rec;
                  --
                  exit when c_summary_rec.count =0;
                  --
                      if c_summary_rec.count >0 then 
                         --
                         Null;
                         --
                         --Begin insert of actuals charges into concur gl interface for whitecap
                         --
                         for idx in 1 .. c_summary_rec.count loop         
                               --
                               if c_summary_rec(idx).payment_type ='OOP' then
                                  --
                                  if l_oop_interco_clearing.EXISTS(1) = FALSE then
                                      --
                                      l_oop_interco_clearing(1) :=c_summary_rec(idx); --need just one record of type OOP if found
                                      --                          
                                  end if;
                                  --
                                  l_wc_oop_clearing_amt :=l_wc_oop_clearing_amt + c_summary_rec(idx).charge_amount;
                                  -- 
                               elsif c_summary_rec(idx).payment_type ='CARD' then
                                  --
                                  if l_card_interco_clearing.EXISTS(1) = FALSE then
                                      --
                                      l_card_interco_clearing(1) :=c_summary_rec(idx); --need just one record of type CARD if found
                                      -- 
                                  end if;
                                  -- 
                                  l_wc_card_clearing_amt :=l_wc_card_clearing_amt + c_summary_rec(idx).charge_amount;
                                  --                         
                               else 
                                  --
                                  Null; 
                                  --                          
                               end if;
                               --
                               begin
                                  -- $$$$
                                    begin
                                              -- Set set of books id
                                              g_concur_gl_intf_rec(idx).set_of_books_id :=g_US_ledger_id;
                                              -- Set accounting date
                                              g_concur_gl_intf_rec(idx).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                              -- Set currency          
                                              g_concur_gl_intf_rec(idx).currency_code := Get_Currency (p_emp_group =>g_GSC_emp_grp); --Set currency based on concur emp group
                                              -- Set Batch Name and description
                                              g_concur_gl_intf_rec(idx).reference1 :='Accrual Run ID :<'||g_run_id||'>';                                                    
                                              g_concur_gl_intf_rec(idx).reference2 :='Employee Group :'||g_GSC_emp_grp;                          
                                              --
                                              -- Set Journal entry name and description
                                                    g_concur_gl_intf_rec(idx).reference4 :=c_summary_rec(idx).reference4;   
                                                    g_concur_gl_intf_rec(idx).reference5 :=c_summary_rec(idx).reference5;
                                                    -- Set Journal entry line description                                                                            
                                                    g_concur_gl_intf_rec(idx).reference10 :=c_summary_rec(idx).line_description;                   
                                              --      
                                              g_concur_gl_intf_rec(idx).group_id :=g_GSC_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                              --
                                              g_concur_gl_intf_rec(idx).reference21 :='Accrual Run ID :<'||g_run_id||'>';
                                              g_concur_gl_intf_rec(idx).reference22 :='Employee Group :'||g_GSC_emp_grp;
                                              g_concur_gl_intf_rec(idx).reference23 :=c_summary_rec(idx).payment_type;
                                              g_concur_gl_intf_rec(idx).reference24 :=c_summary_rec(idx).reference10;
                                              --
                                              g_concur_gl_intf_rec(idx).reference25 :=Null;  
                                              g_concur_gl_intf_rec(idx).reference26 :=Null;                          
                                              g_concur_gl_intf_rec(idx).reference27 :=Null; 
                                              g_concur_gl_intf_rec(idx).reference28 :=Null;
                                              g_concur_gl_intf_rec(idx).reference29 :=g_run_id;
                                              g_concur_gl_intf_rec(idx).reference30 :=Null;                                                                          
                                              --                    
                                              --Begin chart of accounts segments
                                              --
                                              -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                              g_concur_gl_intf_rec(idx).segment1 := c_summary_rec(idx).gl_product; 
                                              --
                                              g_concur_gl_intf_rec(idx).segment2 :=c_summary_rec(idx).gl_location;
                                              g_concur_gl_intf_rec(idx).segment3 :=c_summary_rec(idx).cost_ctr;
                                              g_concur_gl_intf_rec(idx).segment4 :=c_summary_rec(idx).charge_account;
                                              --
                                              g_concur_gl_intf_rec(idx).segment5 :=c_summary_rec(idx).project_code;
                                              g_concur_gl_intf_rec(idx).segment6 :=c_summary_rec(idx).fuse1;
                                              g_concur_gl_intf_rec(idx).segment7 :=c_summary_rec(idx).fuse2;
                                              g_concur_gl_intf_rec(idx).attribute2 :=g_event_type_EXP;
                                              --
                                              --End chart of accounts segments
                                              -- set Credit amount         
                                              g_concur_gl_intf_rec(idx).entered_dr :=c_summary_rec(idx).charge_amount;                          
                                              --
                                              --print_log('Finished accrual interco clearing [CI US IC WITH GSC] record.');
                                              --
                                    exception
                                         when others then
                                            print_log('@Setup Interco Liability and Clearing, error =>'||sqlerrm||', line :'||l_line_buffer);
                                    end;        
                                     --                                       
                                  -- $$$$
                                  l_line_count:= l_line_count+1; --number of lines written
                                  --
                               exception
                                     when no_data_found then
                                       exit;
                                     when others then  
                                       print_log ('Line Description : '||c_summary_rec(idx).line_description);                           
                                       print_log('@ Inside file loop operation, error : '||sqlerrm);
                                       exit;
                               end;                
                           --
                          end loop;
                         --
                         --End copy the CI US JE actuals detail records to the file              
                         --
                         -- ^^^^
                          -- Insert into xxcus_concur_gl_iface table
                          begin
                            --
                                flush_concur_gl_iface  --run only once
                                  (
                                     p_emp_group     =>g_GSC_emp_grp
                                    ,p_oop_or_card  =>'OOP' --All gl interface records where reference23 is OOP, group id 66 with status NEW
                                    ,p_group_id         =>g_GSC_grp_id
                                  ); 
                            -- 
                                flush_concur_gl_iface  --run only once
                                  (
                                     p_emp_group     =>g_GSC_emp_grp
                                    ,p_oop_or_card  =>'CARD' --All gl interface records where reference23 is CARD, group id 66 with status NEW
                                    ,p_group_id         =>g_GSC_grp_id
                                  );                                                                                                  
                            --        
                            populate_concur_gl_iface 
                                (
                                   p_concur_gl_intf_rec  =>g_concur_gl_intf_rec
                                  ,p_group_id                    =>g_GSC_grp_id
                                );                                
                          exception
                           when others then
                            print_log('@populate_concur_gl_iface, calling block error : '||sqlerrm);
                            raise program_error;
                          end;
                         -- ^^^^                        
                      end if; 
                  --
                 end loop;
                 --
                 close c_GSC_summary_je;
                 --
                 g_concur_gl_intf_rec.delete; --cleanup records from plsql collection table
                 --
                 l_line_count :=0;
                 --
                 -- Begin insert of CI US Interco clearing with GSC for Out of Pocket AND GSC IC WITH CI US
                 if l_oop_interco_clearing.count >0 then
                   --
                   for idx in 1 .. l_oop_interco_clearing.count loop
                        --
                        l_line_count := l_line_count + 1;
                        --
                        begin
                                  -- Set set of books id
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_GSC_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Accrual Run ID :<'||g_run_id||'>';                                                 
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_GSC_emp_grp;      --g_GSC_emp_grp                    
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_oop_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_oop_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_oop_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_GSC_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Accrual Run ID :<'||g_run_id||'>'; 
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Employee Group :'||g_GSC_emp_grp;
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_oop_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_oop_interco_clearing(idx).reference10; --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 := l_oop_interco_clearing(idx).gl_product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=I_CI_GSC_Interco_loc;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=I_CI_GSC_Interco_Dept;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=I_CI_GSC_Interco_Acct;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_oop_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_oop_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_EXP;
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_cr :=l_wc_oop_clearing_amt;                          
                                  --
                                  print_log('Finished interco clearing [CI US IC WITH GSC] record for '||'OOP');
                                  --
                        exception
                             when others then
                                print_log('@CI US to GSC: Setup Interco Clearing, error =>'||sqlerrm);
                        end;
                         --  
                         -- Begin GSC Interco with CI US                       
                        begin
                                   --
                                   l_line_count :=l_line_count +1; --increment plsql collection index so we can add record for GSC interco with CI US
                                   --                        
                                  -- Set set of books id
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_GSC_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Accrual Run ID :<'||g_run_id||'>';                                                   
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_GSC_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_oop_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_oop_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_Interco_Clearing_Desc; --l_oop_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_GSC_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Accrual Run ID :<'||g_run_id||'>';
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Employee Group :'||g_GSC_emp_grp;   
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_oop_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_oop_interco_clearing(idx).reference10; --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 :=g_Interco_US_Product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=l_Interco_Location;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=l_Interco_Cost_Ctr;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=l_Interco_OOP_Liability;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_oop_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_oop_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_IC;                                  
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_cr :=l_wc_oop_clearing_amt;                          
                                  --
                                  print_log('Finished OOP clearing [GSC Interco with CI US] liability record for '||'OOP');
                                  --
                                  l_line_count :=l_line_count +1; --increment plsql collection index so we can add record for GSC interco with CI US
                                  -- ~~~~~~
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_GSC_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Accrual Run ID :<'||g_run_id||'>';                                     
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_GSC_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_oop_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_oop_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_Interco_Clearing_Desc; --l_oop_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_GSC_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Accrual Run ID :<'||g_run_id||'>';
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Employee Group :'||g_GSC_emp_grp;
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_oop_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_oop_interco_clearing(idx).reference10; --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 :=g_Interco_US_Product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=l_Interco_Location;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=l_Interco_Cost_Ctr;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=l_Interco_OOP_Clearing;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_oop_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_oop_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_IC;                                  
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_dr :=l_wc_oop_clearing_amt;                          
                                  --
                                  --print_log('Finished Interco clearing [GSC Interco with CI US] record for '||'OOP');
                                  --                                                                    
                                  -- ~~~~~~
                                  -- 
                        exception
                             when others then
                                print_log('@CI US to GSC: Setup Interco Clearing, error =>'||sqlerrm);
                        end;
                         --                           
                         -- End GSC Interco with CI US
                         --   
                   end loop;
                    --
                         -- ^^^^
                          -- Insert into xxcus_concur_gl_iface table
                          begin
                             populate_concur_gl_iface 
                                ( 
                                   p_concur_gl_intf_rec =>g_concur_gl_intf_rec
                                  ,p_group_id                   =>g_GSC_grp_id
                                );
                          exception
                           when others then
                            print_log('@populate_concur_gl_iface, calling block error : '||sqlerrm);
                            raise program_error;
                          end;
                         -- ^^^^                       
                 else
                 --
                 print_log ('No interco clearning record for CI US IC with GSC');
                 --
                 end if;                              
                 --    
                 -- End insert of CI US Interco clearing with GSC for Out of Pocket   
                 --
                 g_concur_gl_intf_rec.delete; --cleanup records from plsql collection table 
                 --
                 l_line_count :=0;
                 --
                 -- Begin insert of CI US Interco clearing with GSC for CARD
                 if l_card_interco_clearing.count >0 then
                   --
                   for idx in 1 .. l_card_interco_clearing.count loop
                        --
                        l_line_count := l_line_count + 1;                   
                        --
                        begin
                                  -- Set set of books id
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_GSC_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Accrual Run ID :<'||g_run_id||'>';                                        
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_GSC_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_card_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_card_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_card_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_GSC_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Accrual Run ID :<'||g_run_id||'>'; 
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Employee Group :'||g_GSC_emp_grp;
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_card_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_card_interco_clearing(idx).reference10;  --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 := l_card_interco_clearing(idx).gl_product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=I_CI_GSC_Interco_loc;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=I_CI_GSC_Interco_Dept;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=I_CI_GSC_Interco_Acct;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_card_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_card_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_EXP;                                  
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_cr :=l_wc_card_clearing_amt;                          
                                  --
                                  print_log('Finished interco clearing [CI US IC WITH GSC] record for '||'CARD.');
                                  --
                        exception
                             when others then
                                print_log('@CI US to GSC: Setup Interco Clearing, error =>'||sqlerrm);
                        end;
                         --  
                         -- Begin GSC Interco with CI US for CARD                      
                        begin
                                   --
                                   l_line_count :=l_line_count +1; --increment plsql collection index so we can add record for GSC interco with CI US
                                   --                        
                                  -- Set set of books id
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_GSC_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Accrual Run ID :<'||g_run_id||'>';                                           
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_GSC_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_card_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_card_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_Interco_Clearing_Desc; --l_oop_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_GSC_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Accrual Run ID :<'||g_run_id||'>';
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Employee Group :'||g_GSC_emp_grp;
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_card_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_card_interco_clearing(idx).reference10; --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 :=g_Interco_US_Product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=l_Interco_Location;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=l_Interco_Cost_Ctr;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=l_Interco_CARD_Liability;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_card_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_card_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_IC;                                  
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_cr :=l_wc_card_clearing_amt;                          
                                  --
                                  print_log('Finished  clearing [GSC Interco with CI US] liability record for '||'CARD');
                                  --
                                  l_line_count :=l_line_count +1; --increment plsql collection index so we can add record for GSC interco with CI US
                                  -- ~~~~~~
                                  g_concur_gl_intf_rec(l_line_count).set_of_books_id :=g_US_ledger_id;
                                  -- Set accounting date
                                  g_concur_gl_intf_rec(l_line_count).accounting_date :=g_batch_date; -- l_Interco_Acct_Date;
                                  -- Set currency          
                                  g_concur_gl_intf_rec(l_line_count).currency_code := Get_Currency (p_emp_group =>g_GSC_emp_grp); --Set currency based on concur emp group
                                  -- Set Batch Name and description
                                  g_concur_gl_intf_rec(l_line_count).reference1 :='Accrual Run ID :<'||g_run_id||'>';                          
                                  g_concur_gl_intf_rec(l_line_count).reference2 :='Employee Group :'||g_GSC_emp_grp;                          
                                  --
                                  -- Set Journal entry name and description
                                        g_concur_gl_intf_rec(l_line_count).reference4 :=l_card_interco_clearing(idx).reference4;   
                                        g_concur_gl_intf_rec(l_line_count).reference5 :=l_card_interco_clearing(idx).reference5;
                                        -- Set Journal entry line description                                                                            
                                        g_concur_gl_intf_rec(l_line_count).reference10 :=l_Interco_Clearing_Desc; --l_oop_interco_clearing(idx).reference10;                   
                                  --      
                                  g_concur_gl_intf_rec(l_line_count).group_id :=g_GSC_grp_id; --Unique per GL journal entry  source..In this case "Concur"...so we can run multiple journal imports per source if needed
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference21 :='Accrual Run ID :<'||g_run_id||'>';
                                  g_concur_gl_intf_rec(l_line_count).reference22 :='Employee Group :'||g_GSC_emp_grp;
                                  g_concur_gl_intf_rec(l_line_count).reference23 :=l_card_interco_clearing(idx).payment_type;
                                  g_concur_gl_intf_rec(l_line_count).reference24 :=l_card_interco_clearing(idx).reference10; --Null;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).reference25 :=Null;  
                                  g_concur_gl_intf_rec(l_line_count).reference26 :=Null;                          
                                  g_concur_gl_intf_rec(l_line_count).reference27 :=Null; 
                                  g_concur_gl_intf_rec(l_line_count).reference28 :=Null;
                                  g_concur_gl_intf_rec(l_line_count).reference29 :=g_run_id;
                                  g_concur_gl_intf_rec(l_line_count).reference30 :=Null;                                                                          
                                  --                    
                                  --Begin chart of accounts segments
                                  --
                                  -- Set GL Intercompany segments for the Interco OOP Clearing journal       
                                  g_concur_gl_intf_rec(l_line_count).segment1 := g_Interco_US_Product; 
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment2 :=l_Interco_Location;
                                  g_concur_gl_intf_rec(l_line_count).segment3 :=l_Interco_Cost_Ctr;
                                  g_concur_gl_intf_rec(l_line_count).segment4 :=l_Interco_OOP_Clearing;
                                  --
                                  g_concur_gl_intf_rec(l_line_count).segment5 :=l_Interco_Project;
                                  g_concur_gl_intf_rec(l_line_count).segment6 :=l_card_interco_clearing(idx).fuse1;
                                  g_concur_gl_intf_rec(l_line_count).segment7 :=l_card_interco_clearing(idx).fuse2;
                                  g_concur_gl_intf_rec(l_line_count).attribute2 :=g_event_type_IC;                                  
                                  --
                                  --End chart of accounts segments
                                  -- set Credit amount         
                                  g_concur_gl_intf_rec(l_line_count).entered_dr :=l_wc_card_clearing_amt;                          
                                  --
                                  print_log('Finished Interco clearing [GSC Interco with CI US] record for '||'CARD.');
                                  --                                                                    
                                  -- ~~~~~~
                                  -- 
                        exception
                             when others then
                                print_log('@CI US to GSC: Setup Interco Clearing, error =>'||sqlerrm);
                        end;
                         --                           
                         -- End GSC Interco with CI US for CARD                       
                   end loop;
                    --
                     -- ^^^^
                     -- Insert into xxcus_concur_gl_iface table
                      begin
                         populate_concur_gl_iface 
                            ( 
                               p_concur_gl_intf_rec =>g_concur_gl_intf_rec
                              ,p_group_id                   =>g_GSC_grp_id
                            );
                      exception
                       when others then
                        print_log('@populate_concur_gl_iface, calling block error : '||sqlerrm);
                        raise program_error;
                      end;
                     -- ^^^^                       
                 else
                 --
                 print_log ('Not able to derive interco clearning record for CI US IC with GSC');
                 --
                 end if;
                 --    
                 -- End insert of CI US Interco clearing with GSC for CARD
                 -- 
                 if g_database =g_prd_database then   
                   g_file_prefix :=g_GSC_ORACLE;
                 else
                   g_file_prefix :=g_database||'_'||g_GSC_ORACLE;
                 end if;
                 --                   
                 l_line_count :=0;
                 --  
                 l_file :=g_file_prefix||'_ACCRUAL_DETAILS_'||TO_CHAR(SYSDATE, 'MMDDYYYY')||'.txt';
                 --
                 print_log('Before file open for GSC ORACLE US accrual details file.');
                 --
                 l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
                 --
                 print_log ('File handle initiated for GSC ORACLE US accrual details : '||l_file);
                 --    
                 OPEN c_GSC_US_lines;
                 FETCH c_GSC_US_lines BULK COLLECT INTO c_GSC_US_rec;
                 if c_GSC_US_rec.count >0 then 
                   --
                   OPEN c_GSC_US_header;
                   FETCH c_GSC_US_header INTO l_line_buffer;
                   CLOSE c_GSC_US_header;
                   --
                   utl_file.put_line ( l_utl_file_id, l_line_buffer);
                   -- 
                   l_line_count:= l_line_count+1; --number of lines written
                   --  
                   for idx in 1 .. c_GSC_US_rec.count loop 
                        --
                        l_line_buffer :=c_GSC_US_rec(idx).rec_info;
                        --
                       begin
                          --
                          utl_file.put_line ( l_utl_file_id, l_line_buffer);
                          -- 
                          l_line_count:= l_line_count+1; --number of lines written
                          --
                       exception
                             when no_data_found then
                               exit;
                             when others then
                               print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                               print_log('@ Inside file loop operation, error : '||sqlerrm);
                               exit;
                       end;                     
                    --
                   end loop; 
                   --                 
                 else
                  print_log(' ');
                  print_log(' No accrual details found for ERP :'||g_CI_GP_emp_grp);
                  print_log(' ');                                    
                 end if;
                 CLOSE c_GSC_US_lines;
                 -- Close file handle 
                 --
                 utl_file.fclose ( l_utl_file_id );
                 print_log ('After close for GSC ORACLE US Accrual Details file : '||l_file);
                 --    
                             -- Begin logging file details for GSC US detail file
                             begin 
                                    --
                                    setup_email_body 
                                      (
                                         p_text1 =>'BU CONCUR employee group : '||g_GSC_emp_grp
                                        ,p_emp_group =>g_GSC_emp_grp
                                        ,p_type =>'ORACLE_SOURCED'
                                      ); --g_notif_email_body is assigned here
                                    --
                                    g_email_subject :=g_instance||' '||g_GSC_emp_grp||' detail extracts to SharePoint - Status Update';
                                    print_log('Email Subject :'||g_email_subject);                        
                                    --
                                    set_bu_sharepoint_email (p_emp_group =>g_GSC_emp_grp, p_type =>g_control_type_AA); --g_sharepoint_email is assigned here
                                    print_log('SharePoint Email  :'||g_sharepoint_email);                        
                                    --
                                    -- push the extract to sharepoint
                                    print_log('Begin: Push GSC Concur accrual details to SharePoint.'); 
                                    --                       
                                    sharepoint_integration
                                       (
                                            p_emp_group =>g_GSC_emp_grp,
                                            p_file =>g_directory_path||'/'||l_file,
                                            p_sharepoint_email =>g_sharepoint_email,
                                            p_dummy_email_from =>g_notif_email_from,
                                            p_email_subject =>g_email_subject
                                       )
                                       ;
                                    --
                                    print_log('End: Push GSC Concur accrual details to SharePoint.');                           
                                    --                    
                             exception
                              when others then
                               print_log('@Failed to log file details for BU, @ emp_group '||g_GSC_emp_grp||', message =>'||sqlerrm);
                             end;
                             -- End logging file details for GSC US detail file                                   
     --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    raise program_error;
  end GSC_US_GL_JE;
  -- End GSC ORACLE US extract je files and generate Interco gl journal interface records     
  -- 
  procedure EXTRACT_INACTIVE_BU  IS
   --
    l_sub_routine varchar2(30) :='extract_inactive_bu';
    l_line_count number :=0;
    l_request_id number :=fnd_global.conc_request_id;
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;      
    --
    l_utl_file_id   UTL_FILE.FILE_TYPE;
    l_line_buffer VARCHAR2(30000):= Null;
    l_file VARCHAR2(150) :=Null;
    --
    cursor c_uniq_concur_batches is
        select concur_batch_id
                   ,to_char(concur_batch_date, 'mm/dd/yyyy') concur_batch_date
                   ,nvl(allocation_custom1, allocation_custom7) allocation_erp 
                   ,replace(nvl(allocation_custom1, allocation_custom7),' ','_') transformed_erp
                   ,sum(journal_amt) total_journal_amt
                   ,count(concur_batch_date) total_journal_lines                                                                  
        from xxcus.xxcus_src_concur_sae_n
        where 1 =1
            and crt_btch_id =g_current_batch_id
            and exists
             (
                  select '1'
                  from    fnd_lookup_values
                  where 1 =1
                        and lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
                        and nvl(enabled_flag, 'N') ='N' --Pull only inactive business units that are pending implementation
                        and lookup_code =nvl(allocation_custom1, allocation_custom7)
             )            
        group by concur_batch_id 
                         ,to_char(concur_batch_date, 'mm/dd/yyyy')
                         ,nvl(allocation_custom1, allocation_custom7)
                         ,replace(nvl(allocation_custom1, allocation_custom7),' ','_')
        order by concur_batch_id asc  
        ;  
    --
    cursor c_inactive_bu (p_concur_batch_id in number, p_alloc_erp in varchar2) is
    select 
                 concur_batch_id
                ,to_char(concur_batch_date,'mm/dd/yyyy') concur_batch_date
                ,rpt_id
                ,rpt_key
                ,rpt_entry_key
                ,journal_rpj_key
                ,allocation_key
                ,line_number
                ,emp_id
                ,emp_last_name
                ,emp_first_name
                ,emp_mi
                ,emp_custom21 emp_group
                ,ledger_ledger_code ledger_code
                ,emp_currency_alpha_code
                ,to_char(rpt_submit_date,'mm/dd/yyyy') rpt_submit_date                
                ,to_char(rpt_user_defined_date,'mm/dd/yyyy') rpt_user_defined_date                
                ,to_char(rpt_pmt_processing_date,'mm/dd/yyyy') rpt_pmt_processing_date
                ,rpt_name
                ,rpt_entry_trans_type
                ,rpt_entry_expense_type_name
                ,rpt_entry_vendor_name
                ,rpt_entry_vendor_desc
                ,cc_trans_merchant_name                      merchant_name
                ,cc_trans_desc                                             merchant_description
                ,rpt_entry_location_city_name              rpt_entry_location_city
                ,cc_trans_merchant_city                          transaction_city
                ,to_char(rpt_entry_trans_date,'mm/dd/yyyy') rpt_entry_trans_date                
                ,to_char(cc_trans_trans_date,'mm/dd/yyyy') cc_trans_trans_date
                ,cc_trans_trans_id                                     cc_trans_trans_id
                ,cc_trans_merchant_state                      transaction_state
                ,rpt_entry_from_location
                ,rpt_entry_to_location
                ,car_log_entry_business_dist
                ,rpt_entry_ctry_sub_code
                ,rpt_entry_pmt_type_code
                ,rpt_entry_pmt_code_name
                ,journal_payer_pmt_type_name
                ,journal_payer_pmt_code_name
                ,journal_payee_pmt_type_name
                ,journal_payee_pmt_code_name
                ,journal_acct_code
                ,journal_debit_or_credit
                ,journal_amt
                ,allocation_pct
                ,nvl(allocation_custom1, allocation_custom7)allocation_erp
                ,nvl(allocation_custom2, allocation_custom8) allocation_company
                ,nvl(allocation_custom3, allocation_custom9) allocation_branch
                ,nvl(allocation_custom4, allocation_custom10) allocation_dept
                ,nvl(allocation_custom11, allocation_custom12) allocation_project
                ,crt_btch_id                                          
        from xxcus.xxcus_src_concur_sae_n
        where 1 =1
            and crt_btch_id =g_current_batch_id
            and concur_batch_id =p_concur_batch_id --Cursor parameter 
            and nvl(allocation_custom1, allocation_custom7) =p_alloc_erp                   
        order by rpt_key asc, line_number asc
                   ;    
    --  
    type c_inactive_bu_journals_type is table of c_inactive_bu%rowtype index by binary_integer;
    c_inactive_journals_rec  c_inactive_bu_journals_type; 
    -- 
    --     
  begin --All inactive files will be dropped into the Concur GSC ORACLE sharepoint folder
       -- 
       g_outbound_loc :=GET_OUTBOUND_FILE_LOCATION ( v_operation =>'DBA_DIRECTORY', v_emp_group  =>g_GSC_ORACLE);
       --
       print_log ('Current working DBA directory is '||g_outbound_loc);
       --       
     for rec in c_uniq_concur_batches loop
       --
       -- ### @ begin rec in c_uniq_concur_batches loop
       --         
       g_inactive_count   :=to_char(rec.total_journal_lines, 'FM999,990');
       g_inactive_dollars :=to_char(rec.total_journal_amt, 'FM999,999,990.90');
       --
                 --                        
                 open c_inactive_bu (p_concur_batch_id =>rec.concur_batch_id, p_alloc_erp =>rec.allocation_erp);
                 --
                 loop
                  --
                  fetch c_inactive_bu bulk collect into c_inactive_journals_rec;
                  --
                  print_log('Inactive ERP group allocated journals : '||c_inactive_journals_rec.count);
                  --
                  exit when c_inactive_journals_rec.count =0;
                  --
                      if c_inactive_journals_rec.count >0 then
                         --
                         if g_database =g_prd_database then   
                           g_file_prefix :=rec.transformed_erp||'_';
                         else
                           g_file_prefix :=g_database||'_'||rec.transformed_erp||'_';
                         end if;                    
                         --
                         l_file :=g_file_prefix||'CONCUR_INACTIVE_BU_BATCH_'||rec.concur_batch_id||'_'||TO_CHAR(g_batch_date, 'MMDDYYYY')||'.txt';
                         --
                         print_log('Before file open for inactive business unit journals, @ allocation_erp  ='||rec.allocation_erp||', concur_batch_id ='||rec.concur_batch_id);
                         --
                         l_utl_file_id  := utl_file.fopen ( g_outbound_loc, l_file, 'w',32767); 
                         --
                         print_log ('File handle initiated for inactive business unit journals : '||l_file);
                         -- 
                         -- Begin copy the WW JE actuals header record to the file              
                         --
                         begin
                              --                              
                              l_line_buffer := 
                                      'CONCUR_BATCH_ID'
                                  ||'|'
                                  ||'CONCUR_BATCH_DATE'
                                  ||'|'  
                                  ||'RPT_KEY'    
                                  ||'|'        
                                  ||'EMP_ID'                                                      
                                  ||'|'                    
                                  ||'EMP_LAST_NAME'
                                  ||'|'                    
                                  ||'EMP_FIRST_NAME'
                                  ||'|'                    
                                  ||'EMP_MI'
                                  ||'|'                                                                             
                                  ||'LINE_NUMBER'
                                  ||'|'
                                  ||'EMP_GROUP'
                                  ||'|'            
                                  ||'RPT_SUBMIT_DATE'
                                  ||'|'
                                  ||'RPT_ENTRY_EXPENSE_TYPE_NAME'
                                  ||'|'           
                                  ||'RPT_ENTRY_PMT_CODE_NAME'
                                  ||'|'
                                  ||'JOURNAL_ACCT_CODE'
                                  ||'|'           
                                  ||'JOURNAL_AMT'
                                  ||'|'
                                  ||'ALLOCATION_ERP'
                                  ||'|'           
                                  ||'ALLOCATION_COMPANY'
                                  ||'|'
                                  ||'ALLOCATION_BRANCH'
                                  ||'|'                                  
                                  ||'ALLOCATION_DEPT'
                                  ||'|'   
                                  ||'ALLOCATION_PROJECT'
                                  ||'|'
                                  ||'CRT_BTCH_ID'
                                  ||'|'
                                  ;                  
                              --
                              utl_file.put_line ( l_utl_file_id, l_line_buffer);
                              -- 
                              l_line_count:= l_line_count+1; --number of lines written including header
                              --
                         exception
                                 when no_data_found then
                                   exit;
                                 when others then
                                   print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                   print_log('@ Inside file loop operation, error : '||sqlerrm);
                                   exit;
                         end;              
                         --
                         --End copy the inactive business unit journals record to the file              
                         --
                         l_line_buffer :=Null;
                         --
                         --Begin copy inactive business unit journals records to the file              
                         --
                         for idx in 1 .. c_inactive_journals_rec.count loop 
                           --
                           l_line_buffer :=
                                      c_inactive_journals_rec(idx).concur_batch_id
                                  ||'|'
                                  ||c_inactive_journals_rec(idx).concur_batch_date
                                  ||'|'  
                                  ||c_inactive_journals_rec(idx).rpt_key   
                                  ||'|'        
                                  ||c_inactive_journals_rec(idx).emp_id                                                     
                                  ||'|'                    
                                  ||c_inactive_journals_rec(idx).emp_last_name
                                  ||'|'                    
                                  ||c_inactive_journals_rec(idx).emp_first_name
                                  ||'|'                    
                                  ||c_inactive_journals_rec(idx).emp_mi
                                  ||'|'                                                                             
                                  ||c_inactive_journals_rec(idx).line_number
                                  ||'|'
                                  ||c_inactive_journals_rec(idx).emp_group
                                  ||'|'            
                                  ||c_inactive_journals_rec(idx).rpt_submit_date
                                  ||'|'
                                  ||c_inactive_journals_rec(idx).rpt_entry_expense_type_name
                                  ||'|'           
                                  ||c_inactive_journals_rec(idx).rpt_entry_pmt_code_name
                                  ||'|'
                                  ||c_inactive_journals_rec(idx).journal_acct_code
                                  ||'|'           
                                  ||c_inactive_journals_rec(idx).journal_amt
                                  ||'|'
                                  ||c_inactive_journals_rec(idx).allocation_erp
                                  ||'|'           
                                  ||c_inactive_journals_rec(idx).allocation_company
                                  ||'|'
                                  ||c_inactive_journals_rec(idx).allocation_branch
                                  ||'|'                                  
                                  ||c_inactive_journals_rec(idx).allocation_dept
                                  ||'|'   
                                  ||c_inactive_journals_rec(idx).allocation_project
                                  ||'|'                                  
                                  ||c_inactive_journals_rec(idx).crt_btch_id
                                  ||'|'
                                  ;                
                               --
                               begin
                                  --
                                  utl_file.put_line ( l_utl_file_id, l_line_buffer);
                                  -- 
                                  l_line_count:= l_line_count+1; --number of lines written including header
                                  --
                               exception
                                     when no_data_found then
                                       exit;
                                     when others then
                                       print_log('Line# '||l_line_count||', Record: '||l_line_buffer);                               
                                       print_log('@ Inside file loop operation, error : '||sqlerrm);
                                       exit;
                               end;                
                               --
                         end loop;
                         --
                         --End copy inactive business unit journals records to the file              
                         --
                             print_log('Total records written into the file [Header and Lines] , l_line_count =>'||l_line_count);
                             --
                             -- Close file handle 
                             --
                             utl_file.fclose ( l_utl_file_id );
                             print_log ('After close for inactive business unit journals file : '||l_file);
                             -- 
                             -- Begin logging file details for each BU along with their concur batch id
                             begin 
                                    --
                                    insert into xxcus.xxcus_concur_ebs_bu_files
                                    (
                                     request_id
                                    ,concur_batch_id
                                    ,emp_group
                                    ,extracted_file_name
                                    ,extracted_file_folder
                                    ,created_by 
                                    ,creation_date
                                    ,crt_btch_id
                                    )
                                    values
                                    (
                                     l_request_id
                                    ,rec.concur_batch_id --c_uniq_batch_rec.batch_id
                                    ,rec.allocation_erp
                                    ,l_file
                                    ,g_directory_path
                                    ,l_created_by
                                    ,l_creation_date
                                    ,g_current_batch_id
                                    )
                                    ;
                                    --
                                    commit;
                                    --
                                    -- setup the email body
                                    --setup_email_body (p_text1 =>'BU CONCUR employee group : '||g_WW_emp_grp); --g_notif_email_body is assigned here
                                    setup_email_body 
                                      (
                                         p_text1 =>'BU CONCUR employee group : '||rec.allocation_erp
                                        ,p_emp_group =>rec.allocation_erp
                                        ,p_type =>'INACTIVE_BU'
                                      ); --g_notif_email_body is assigned here                           
                                    --print_log('Email Body :'||g_notif_email_body);
                                    --
                                    g_email_subject :=g_instance||' '||rec.allocation_erp||' Inactive journals extract to SharePoint - Status Update';
                                    print_log('Email Subject :'||g_email_subject);                        
                                    --
                                    set_bu_sharepoint_email (p_emp_group =>g_GSC_emp_grp, p_type =>g_control_type_AA); --g_sharepoint_email is assigned here
                                    print_log('SharePoint Email  :'||g_sharepoint_email);                        
                                    --
                                    -- push the extract to sharepoint
                                    print_log('Begin: Push inactive business unit journal details to GSC library in SharePoint.'); 
                                    --                       
                                    sharepoint_integration
                                       (
                                            p_emp_group =>rec.allocation_erp,
                                            p_file =>g_directory_path||'/'||l_file,
                                            p_sharepoint_email =>g_sharepoint_email,
                                            p_dummy_email_from =>g_notif_email_from,
                                            p_email_subject =>g_email_subject
                                       )
                                       ;
                                    --
                                    print_log('End: Push inactive business unit journal details to GSC library in SharePoint.');                           
                                    -- email the teams about the extract
                                    send_email_notif
                                       (
                                            p_email_from      =>g_notif_email_from,
                                            p_email_to           =>g_notif_email_to,
                                            p_email_cc           =>g_notif_email_cc,
                                            p_email_subject =>g_email_subject, 
                                            p_email_body     =>g_notif_email_body,
                                            p_mime_type      =>g_email_mime_type
                                       )
                                      ; 
                                    print_log('After sending email notification about inactive business unit journal details.');                                                                                                
                                    --                     
                             exception
                              when others then
                               print_log('@Failed to log file details for BU, @ emp_group '||rec.allocation_erp||', message =>'||sqlerrm);
                             end;
                             -- End logging file details for each BU along with their concur batch id
                      else
                       print_log('No inactive ERP group allocated journals found.');
                      end if; 
                  --
                 end loop;
                 --
                 close c_inactive_bu;
                 --    
       -- ### @ end of rec in c_uniq_concur_batches loop
     end loop;     
     --
  exception
   when others then
    print_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
    raise program_error;
  end EXTRACT_INACTIVE_BU;
  --    
  procedure EXTRACT_TIE_OUT_DETAILS is --main driver extract used for HDS Internal Audit Controls
   --
    l_sub_routine varchar2(30) :='extract_tie_out_details'; 
    l_request_id number :=fnd_global.conc_request_id;
    --
    l_created_by number :=fnd_global.user_id;
    l_creation_date date :=sysdate;
    l_count number :=0;
    l_run_id number :=0;
    l_seq number :=0;
    --
    l_utl_file_id   UTL_FILE.FILE_TYPE;
    l_line_buffer VARCHAR2(30000):= Null;
    l_file VARCHAR2(150) :=Null;        
    --
    cursor tie_out_hdr is 
    select 
                 'Accruals'                              je_type 
                ,sum(b.posted_amount)  header_total
                ,sum
                      (
                          case
                            when (b.accrual_erp is null) then -1*b.posted_amount
                            else 0
                          end 
                        )                                                                                           unknown_erp_amt
                ,sum
                      (
                          case
                            when (c.enabled_flag =g_N) then -1*b.posted_amount
                            else 0
                          end 
                        )                                                                                           inactive_erp_amt               
                ,sum
                      (
                          case
                            when (b.personal_flag =g_Y) then -1*b.posted_amount
                            else 0
                          end 
                        )                                                                                           personal_chgs               
    from  xxcus.xxcus_concur_accruals_staging b
               ,fnd_lookup_values c
    where 1 =1
         and c.lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
         and b.accrual_erp (+) = c.lookup_code    
         and run_id(+) =g_run_id
         and accrual_status(+) =g_status_new            
    ;
    --
    type tie_out_hdr_type is table of tie_out_hdr%rowtype index by binary_integer;
    tie_out_hdr_rec tie_out_hdr_type;            
    --
    cursor tie_out_interco is
    select b.employee_group allocated_group
               ,sum(a.entered_cr) allocated_amount 
               ,g_event_type_EXP which_set  
               ,1 pull_seq               
    from   apps.gl_interface a
               ,xxcus.xxcus_concur_extract_controls b
    where 1 =1
       and a.user_je_source_name =g_je_source
       and a.user_je_category_name =g_je_category
       and a.attribute2 =g_event_type_IC
       and a.status =g_status_new
       and a.reference29 =g_run_id
       and b.gl_group_id =a.group_id
       and b.type =g_control_type_AA --because GSC has multiple types in the extract control table, we just pull one record to get the group id
       and b.gl_group_id !=g_WW_grp_id -- Exclude WW because we do not interface to GL
    group by b.employee_group      
    union all   
    select  g_WW_emp_grp allocated_group
               ,sum(nvl(b.posted_amount, 0)) allocated_amount 
               ,g_event_type_EXP which_set
               ,2 pull_seq
    from   xxcus.xxcus_concur_accruals_staging b
    where 1 =1
         and b.accrual_erp =g_WW_emp_grp
         and b.run_id =g_run_id
         and b.personal_flag =g_N
         and accrual_status(+) =g_status_new             
    order by 4 asc 
    ;
    --
    cursor c_other_totals is
    select 
                case
                  when rpt_entry_pmt_code_name in ('Cash', 'Pending Card Transaction') then rpt_entry_pmt_code_name||'-'||accrual_erp
                  else rpt_entry_pmt_code_name
                end                                                     other_item
               ,sum
                 (
                    posted_amount
                 )                  other_amount
                ,1 pull_seq
    from   xxcus.xxcus_concur_accruals_staging b
               ,fnd_lookup_values c
    where 1 =1
         and c.lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
         and b.accrual_erp = c.lookup_code
         --and b.personal_flag =g_N  
         and c.enabled_flag =g_Y       
         and b.run_id            =g_run_id
         and b.accrual_status =g_status_new   
    group by 
                case
                  when rpt_entry_pmt_code_name in ('Cash', 'Pending Card Transaction') then rpt_entry_pmt_code_name||'-'||accrual_erp
                  else rpt_entry_pmt_code_name                  
                end 
    union all
    select 
                'Inactive Concur ERP''s' other_item
               ,sum (-1*posted_amount)   other_amount
                ,2 pull_seq
    from   xxcus.xxcus_concur_accruals_staging b
               ,fnd_lookup_values c
    where 1 =1
         and c.lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
         and b.accrual_erp = c.lookup_code 
         and c.enabled_flag =g_N       
         and b.run_id            =g_run_id
         and b.accrual_status =g_status_new     
    union all
    select 
                'Personal charges' other_item
               ,sum (-1*posted_amount)   other_amount
                ,3 pull_seq
    from   xxcus.xxcus_concur_accruals_staging b
               ,fnd_lookup_values c
    where 1 =1
         and c.lookup_type ='XXCUS_CONCUR_ACTIVE_BU'
         and b.accrual_erp = c.lookup_code 
         and b.personal_flag =g_Y      
         and b.run_id            =g_run_id
         and b.accrual_status =g_status_new                           
    order by 3 asc
    ;  
    --
  begin
    --
    delete xxcus.xxcus_concur_tie_out_accr_rpt;
    --
    open tie_out_hdr;
    --
    fetch tie_out_hdr bulk collect into tie_out_hdr_rec;
    --
    if tie_out_hdr_rec.count >0 then
         --        
           INS_AUDIT_TIE_OUT_RPT
           (
             p_report_type =>'Tie Out Batch'
            ,p_extract_type =>tie_out_hdr_rec(1).je_type
            ,p_seq =>1
            ,p_item_name =>'Header Total'
            ,p_item_amount =>tie_out_hdr_rec(1).header_total
            ,p_line_comments =>'Header total from Concur SAE extract'
            ,p_creation_date =>l_creation_date
            ,p_created_by =>l_created_by
            ,p_request_id =>l_request_id  
            ,p_run_id =>g_run_id 
           );
         --     
         l_count :=l_count + 1; --1'st record
         --
           INS_AUDIT_TIE_OUT_RPT
           (
             p_report_type =>'Tie Out Batch'
            ,p_extract_type =>tie_out_hdr_rec(1).je_type
            ,p_seq =>2
            ,p_item_name =>'Unknown Concur ERP'
            ,p_item_amount =>tie_out_hdr_rec(1).unknown_erp_amt
            ,p_line_comments =>'Unknown Concur ERP amount'
            ,p_creation_date =>l_creation_date
            ,p_created_by =>l_created_by
            ,p_request_id =>l_request_id
            ,p_run_id =>g_run_id               
           );
         --     
         l_count :=l_count + 1; --2'nd record
         --
           INS_AUDIT_TIE_OUT_RPT
           (
             p_report_type =>'Tie Out Batch'
            ,p_extract_type =>tie_out_hdr_rec(1).je_type
            ,p_seq =>3
            ,p_item_name =>'Inactive Concur ERP''s'
            ,p_item_amount =>tie_out_hdr_rec(1).inactive_erp_amt
            ,p_line_comments =>'Inactive ERP total journal amount'
            ,p_creation_date =>l_creation_date
            ,p_created_by =>l_created_by
            ,p_request_id =>l_request_id
            ,p_run_id =>g_run_id               
           );
         --     
         l_count :=l_count + 1; --3'rd record
         -- 
           INS_AUDIT_TIE_OUT_RPT
           (
             p_report_type =>'Tie Out Batch'
            ,p_extract_type =>tie_out_hdr_rec(1).je_type
            ,p_seq =>4
            ,p_item_name =>'Personal charges'
            ,p_item_amount =>tie_out_hdr_rec(1).personal_chgs
            ,p_line_comments =>'Personal charges on company card'
            ,p_creation_date =>l_creation_date
            ,p_created_by =>l_created_by
            ,p_request_id =>l_request_id
            ,p_run_id =>g_run_id                         
           );
         --     
         l_count :=l_count + 1; --4'th record
         --          
         print_log('@tie out report, total records inserted :'||l_count);
         --
    else
     print_log('No Tie out batch records found in collection tie_out_hdr_rec.count :'||tie_out_hdr_rec.count);
    end if;
    --
    close tie_out_hdr;
    --
    l_seq :=0;
    --
    for rec in tie_out_interco loop 
         --
         l_seq :=l_seq +1;
         --    
                   INS_AUDIT_TIE_OUT_RPT
                   (
                     p_report_type =>'Tie Out Interco'
                    ,p_extract_type =>'Accruals'
                    ,p_seq =>l_seq
                    ,p_item_name =>rec.allocated_group||' JE'
                    ,p_item_amount =>rec.allocated_amount
                    ,p_line_comments =>'GSC Interco for '||rec.allocated_group
                    ,p_creation_date =>l_creation_date
                    ,p_created_by =>l_created_by
                    ,p_request_id =>l_request_id
                    ,p_run_id =>g_run_id               
                   );
    end loop;
    --
    print_log('@tie out report interco, total records inserted :'||l_seq);  
    --
    l_seq :=0;
    --
    for rec in c_other_totals loop
       --
       l_seq :=l_seq +1;
       --     
       INS_AUDIT_TIE_OUT_RPT
       (
         p_report_type =>'Other Totals'
        ,p_extract_type =>'Accruals'
        ,p_seq =>l_seq
        ,p_item_name =>rec.other_item
        ,p_item_amount =>rec.other_amount
        ,p_line_comments =>'Other totals by cash or card type.'
        ,p_creation_date =>l_creation_date
        ,p_created_by =>l_created_by
        ,p_request_id =>l_request_id
        ,p_run_id =>g_run_id               
       );     
     --
    end loop;
    --
    print_log('@tie out report other totals, total records inserted :'||l_seq);  
    --    
    --
    commit;
    --  
  exception
   when program_error then
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
   when others then
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end EXTRACT_TIE_OUT_DETAILS;  
  --  
  procedure PROCESS_NEW_BATCH 
    (
        retcode out varchar2
       ,errbuf out varchar2
       ,p_non_prod_email in varchar2
       ,p_push_to_sharepoint in varchar2
       ,p_file_name in varchar2       
    ) IS
    --
    l_sub_routine varchar2(30) :='process_new_batch';
    l_ok boolean :=FALSE;
    --  
  begin
    --
    g_file_name :=p_file_name;
    --
    g_non_prod_email :=p_non_prod_email;
    --
    g_push_to_sharepoint :=p_push_to_sharepoint;
    --
    g_batch_date :=get_max_trans_date(); -- TMS: 20170530-00362 / ESMS 561767 
    --
    print_log('Enter: Get CI US Inventory Card Program Name.');    
    g_CI_US_INV_Program :=GET_CI_US_CARD_PROGRAM_NAME();
    print_log('Exit: Get CI US Inventory Card Program Name.');
    print_log(' ');
    --
    g_run_id :=get_tie_out_run_id(); --get run id 
    --    
    Is_This_Production;
    --
    --print_log('Enter: LOAD_ACCRUAL_DATA.');
    --LOAD_ACCRUAL_DATA;
    --print_log('Exit: LOAD_ACCRUAL_DATA');
    --print_log(' ');    
    --
    print_log('Enter: FETCH_AND_UPDATE_ATTRIBUTES.');
    FETCH_AND_UPDATE_ATTRIBUTES;
    print_log('Exit: FETCH_AND_UPDATE_ATTRIBUTES');
    print_log(' ');
    --
    print_log('Enter: WW_EXTRACT_ELIGIBLE_GL_JE --extract WaterWorks eligible accrual je records.');    
    WW_EXTRACT_ELIGIBLE_GL_JE;    
    print_log('Exit: WW_EXTRACT_ELIGIBLE_GL_JE');    
    print_log(' ');   
    --    
    print_log('Enter: WW_OB_GL_JE --extract WaterWorks JE accruals and supporting details flat files.');    
    WW_OB_GL_JE; --extracts all details required to generate GL journal entries for WaterWorks    
    print_log('Exit: WW_OB_GL_JE');    
    print_log(' ');
    --
    print_log('Enter: CI_GP_EXTRACT_ELIGIBLE_GL_JE --extract CI Brafasco eligible accrual je records.');    
    CI_GP_EXTRACT_ELIGIBLE_GL_JE;
    SET_SAE_HDR_STATUS (p_status =>'Flag CI Canada Brafasco GL JE eligible accrual je lines.');    
    print_log('Exit: CI_GP_EXTRACT_ELIGIBLE_GL_JE');    
    print_log(' ');    
    --
    print_log('Enter: CI_GP_OB_GL_JE --extract CI Brafasco Great Plains JE accruals, flat files and interco journals.');    
    CI_GP_OB_GL_JE;    
    print_log('Exit: CI_GP_OB_GL_JE');    
    print_log(' ');    
    -- 
    print_log('Enter: CI_ORACLE_US_ELIGIBLE_GL_JE --extract CI US JE accrual lines.');     
    CI_ORACLE_US_ELIGIBLE_GL_JE;
    print_log('Exit: CI_ORACLE_US_ELIGIBLE_GL_JE'); 
    print_log (' ');         
    --
    print_log('Enter: CI_US_GL_JE --extract CI US JE accruals, flat files and interco journals.');     
    CI_US_GL_JE;
    print_log('Exit: CI_US_GL_JE'); 
    print_log (' ');         
    --    
    print_log('Enter: GSC_ORACLE_US_ELIGIBLE_GL_JE --extract CI US JE accrual lines.');     
    GSC_ORACLE_US_ELIGIBLE_GL_JE;
    print_log('Exit: GSC_ORACLE_US_ELIGIBLE_GL_JE'); 
    print_log (' ');         
    --
    print_log('Enter: GSC_US_GL_JE --extract CI US JE accruals, flat files and interco journals.');     
    GSC_US_GL_JE;
    print_log('Exit: GSC_US_GL_JE'); 
    print_log (' ');         
    --        
    -- Begin transfer gl journal interface records from concur gl interface to actual ebs gl interface 
    print_log('Enter: Transfer Concur GL JE interface journals to Oracle EBS.');     
    PUSH_CONCUR_JE_TO_EBS_GL;    
    print_log('Exit: Transfer Concur GL JE interface journals to Oracle EBS.');                    
    -- End transfer gl journal interface records from concur gl interface to actual ebs gl interface    
    --
    print_log(' ');
    print_log('Enter: Extract tie out information.');
    EXTRACT_TIE_OUT_DETAILS;    
    print_log('Exit: Extract tie out information.');  
    --
    commit;
    --
  exception
   when program_error then
    print_log('Issue in exception program_error @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);   
    raise;
   when others then
    print_log('Issue in exception others @'||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  end PROCESS_NEW_BATCH;        
  --
  procedure DELETE_REQUEST_SET_LOG  is
   l_sub_routine varchar2(30) :='delete_request_set_log';
   PRAGMA AUTONOMOUS_TRANSACTION;
  begin 
         --
        DELETE xxcus.xxcus_concur_accr_req_set_jobs;
         --
         print_log ('Deleted records from xxcus_concur_accr_req_set_jobs, total records :'||sql%rowcount);
         --
         commit;
         --
  exception
   when others then
   raise program_error;
   print_log('Failed to delete xxcus_concur_accr_req_set_jobs, message =>'||sqlerrm);
  end DELETE_REQUEST_SET_LOG;  
  --
  procedure INSERT_REQUEST_SET_LOG (p_set_req_id IN NUMBER, p_wrapper_req_id IN NUMBER) is
   l_sub_routine varchar2(30) :='insert_request_set_log';
   PRAGMA AUTONOMOUS_TRANSACTION;
  begin 
         --
         insert into xxcus.xxcus_concur_accr_req_set_jobs
          (
            report_set_request_id
           ,wrapper_req_id
          )
         values
          (
             p_set_req_id
            ,p_wrapper_req_id
          )
          ;
         --
         print_log ('Inserted into xxcus_concur_accr_req_set_jobs, total records :'||sql%rowcount);
         --
         commit;
         --
  exception
   when others then
   raise program_error;
   print_log('Failed to insert xxcus_concur_accr_req_set_jobs, message =>'||sqlerrm);
  end INSERT_REQUEST_SET_LOG;
  --
  procedure SUBMIT_REQUEST_SET
    (
        retcode out varchar2
       ,errbuf  out varchar2  
       ,p_non_prod_email in varchar2
       ,p_push_to_sharepoint in varchar2           
       ,p_file_name in varchar2                  
    ) is
    --
    l_request_id number :=0;
    l_req_id number :=0;  
    l_running_req_id number :=fnd_global.conc_request_id;  
    l_wrapper_req_id number :=fnd_global.conc_request_id;
    l_email_subject varchar2(80) :=Null;
    --
    l_non_prod_email  varchar2(240) :=Null;
    l_non_prod_cc_email  varchar2(240) :=Null;    
    l_job_code varchar(80) :='XXCUS_CONCUR_ACCRUAL_AUDIT';
    --
    l_current_db_name   VARCHAR2(20) :=Null;
    l_prod_db_name      VARCHAR2(20) := 'ebsprd';
    --
    l_zip_file varchar2(240) :=Null;
    --
    l_file_name varchar2(240) :=Null;    
    --    
    l_outbound_loc varchar2(240) :=Null;
    --
    l_max_files_allowed number :=1;
    --
    l_APPLCSF_OUT varchar2(60) :='/ebizlog/conc/out';
    --
    l_bool         BOOLEAN;
    l_iso_language VARCHAR2(30);
    --
    l_iso_territory VARCHAR2(30);
    --
    l_err_msg  VARCHAR2(3000);
    l_err_code NUMBER;
    l_sec      VARCHAR2(4000);

    l_ok                  BOOLEAN := TRUE;
    l_success             NUMBER := 0;
    l_user_id             NUMBER :=15837; --1229; --GLINTERFACE
    l_responsibility_id   NUMBER :=50661; --50761;  --50761 for HDS Cash Management Master User or 50661 for XXCUS_CON
    --
    l_resp_application_id NUMBER :=20003; -- 260; --260 for Cash Management OR 20003 for XXCUS_CON
     lc_request_data VARCHAR2(20) :='';
    l_sysdate             DATE;
    l_end_date            VARCHAR2(30);
    -- 
    srs_failed        EXCEPTION;
    submitprog_failed EXCEPTION;
    submitset_failed  EXCEPTION;    
    --
    l_err_callfrom  VARCHAR2(75) DEFAULT 'xxcus_concur_pkg.submit_request_set';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    --
   cursor c_xml_defn is
    select 
      a.default_language dflt_lang,
      a.default_territory dflt_territory,
      a.default_output_type dflt_output_type
    from xdo_templates_vl a
    where 1 =1
        and a.template_code =l_job_code
        and nvl(a.end_date,sysdate) >= sysdate
        and a.object_version_number = 
            (
               select max(xdt1.object_version_number)
               from xdo_templates_vl xdt1
               where 1 =1
                     and xdt1.template_code =a.template_code
                     and nvl(xdt1.end_date,sysdate) >= sysdate
            )
        and rownum < 2;
   --
   c_xml_defn_rec  c_xml_defn%rowtype :=Null;        
   --
   cursor get_audit_rpt_id  is
    select b.report_set_request_id report_set_request_id,
               a.request_id audit_rpt_req_id, a.program job, a.phase_code, a.status_code
    from apps.fnd_amp_requests_v a
              ,xxcus.xxcus_concur_accr_req_set_jobs b
    where 1 =1
    and a.concurrent_program_id !=36034 --Exclude request set stages
    and a.concurrent_program_name =l_job_code
    --and b.wrapper_req_id =l_wrapper_req_id
    and a.phase_code ='C' --Complete
    connect by prior a.request_id  =a.parent_request_id
    start with a.request_id =b.report_set_request_id
    ;  
   get_audit_rpt_rec get_audit_rpt_id%rowtype :=Null;  
   --
   cursor c_new_file is
    select  'XXCUS_CONCUR_ACCRUAL_'||to_char(sysdate,'MMDDYYYY')||'.pdf' audit_rpt_file_name
    from apps.fnd_amp_requests_v a
              ,xxcus.xxcus_concur_accr_req_set_jobs b 
    where 1 =1
    and concurrent_program_id !=36034
    and concurrent_program_name =l_job_code
    --and b.wrapper_req_id =l_wrapper_req_id
    connect by prior a.request_id  =a.parent_request_id
    start with a.request_id =b.report_set_request_id
        ;   
   --
    l_new_file varchar2(240) :=Null;
   --
  BEGIN
   --
       lc_request_data :=fnd_conc_global.request_data;
   -- 
    if lc_request_data is null then     
       --
       lc_request_data :='1';
       -- 
       DELETE_REQUEST_SET_LOG; --delete xxcus.xxcus_concur_report_set_reqs;
       --
               print_log(' ');
               print_log('p_non_prod_email =>'||p_non_prod_email);
               -- 
               g_non_prod_email :=p_non_prod_email;      
               -- 
               print_log(' ');          
              -- 
              -- Begin Initialize the request set 
              --
                l_sec := 'Initializing Request Set - HDS CONCUR (Set): Process Accrual';
                --
                fnd_file.put_line(fnd_file.log, l_sec);
                --
                l_ok := fnd_submit.set_request_set
                                  (
                                      application =>'XXCUS'
                                     ,request_set =>'XXCUS_CONCUR_ACCRUAL_SET'
                                  );
                -- End Initialize the request set 
                --                 
                -- Submit each stage of the request set
                --
                -- Stage_100 : Import and process concur batch.
                --    
                IF (l_ok AND l_success = 0) THEN
                  --
                  l_sec := 'Stage_100 : HDS CONCUR: Import Accruals New Batch.';
                  --
                  fnd_file.put_line(fnd_file.log, l_sec);
                  --
                  l_ok := fnd_submit.submit_program
                                                  (
                                                        application => 'XXCUS'
                                                       ,program     => 'XXCUS_CONCUR_PROCESS_ACCRUALS'
                                                       ,stage       => 'STAGE_100'
                                                       ,argument1   =>p_non_prod_email
                                                       ,argument2   =>p_push_to_sharepoint
                                                       ,argument3   =>p_file_name --accrual file name..pass blank for now
                                                    );
                ELSE
                  l_success := -91;
                  print_log(l_sec || ':  Failed = ' || l_success);
                  RAISE submitprog_failed;
                END IF;
                -- 
                -- Stage_2 : Generate audit report.
                --    
                IF (l_ok AND l_success = 0) THEN
                  --
                  l_sec := 'Stage_200 : Set audit report template [RTF]';
                  --
                  fnd_file.put_line(fnd_file.log, l_sec);
                  --
                     begin 
                      --
                      open c_xml_defn;
                      fetch c_xml_defn into c_xml_defn_rec.dflt_lang, c_xml_defn_rec.dflt_territory, c_xml_defn_rec.dflt_output_type ;
                      close c_xml_defn;
                      --
                     exception
                      when others then
                       c_xml_defn_rec.dflt_lang :='en';
                       c_xml_defn_rec.dflt_territory :='00';
                       c_xml_defn_rec.dflt_output_type :='PDF';              
                     end;
                      --                  
                      l_ok :=fnd_submit.add_layout
                                    (
                                      template_appl_name =>'XXCUS',
                                      template_code =>l_job_code,
                                      template_language =>c_xml_defn_rec.dflt_lang,
                                      template_territory =>c_xml_defn_rec.dflt_territory,
                                      output_format =>c_xml_defn_rec.dflt_output_type
                                    );
                        -- 
                      l_sec := 'Stage_200 : HDS CONCUR : Print Accrual Tie Out Summary Report.';
                      --
                      fnd_file.put_line(fnd_file.log, l_sec);
                      --                                     
                      l_ok := fnd_submit.submit_program
                                                      (
                                                        application => 'XXCUS'
                                                       ,program     => 'XXCUS_CONCUR_ACCRUAL_AUDIT'
                                                       ,stage       => 'STAGE_200'                                        
                                                        );
                ELSE
                  l_success := -92;
                  print_log(l_sec || ':  Failed = ' || l_success);
                  RAISE submitprog_failed;
                END IF;
                --                                   
                -- At this moment we have submitted the individual requests for each stage. now submit the request set.
                --
                IF l_ok AND l_success = 0 THEN 
                     --
                     l_request_id := fnd_submit.submit_set(NULL, TRUE); --we use TRUE to let know that the current routine that submitted the set will pause until the request set is done
                     --
                    print_log ('Submitted request set HDS CONCUR (Set): Process Accrual, Request_id = ' || l_request_id);
                    --
                     INSERT_REQUEST_SET_LOG (p_set_req_id =>l_request_id, p_wrapper_req_id =>l_wrapper_req_id);
                     --                    
                    fnd_conc_global.set_req_globals(conc_status =>'PAUSED' ,request_data =>lc_request_data);
                    --                
                     lc_request_data :=to_char(to_number(lc_request_data)+1);
                     --                        
                ELSE
                    l_success := -1000;
                    --
                    print_log ( 'Failedto submit request set HDS CONCUR (Set): Process Accrual, message = ' || l_success||', msg =>'||sqlerrm);
                    --
                    RAISE submitset_failed;
                    --
                END IF;       
       --
    else     
          --   
          -- Email audit file.
          --    
          l_sec := 'Begin: Email audit file.';
          --
          fnd_file.put_line(fnd_file.log, l_sec);
          --
          begin  
               --
                select upper(name)
                into    g_database
                from  v$database
                where 1 =1
                ;
                --
                if (g_database ='EBSPRD') then  
                     -- 
                     g_instance :=Null;
                     l_non_prod_cc_email :=g_notif_email_cc;
                     --
                else 
                     --
                      g_instance :='( NON PROD - '||g_database||' ) ';
                      l_non_prod_cc_email :=p_non_prod_email;
                     --
                end if;
               --
               open get_audit_rpt_id;
               fetch get_audit_rpt_id into get_audit_rpt_rec;
               close get_audit_rpt_id;
                --
               if  get_audit_rpt_rec.audit_rpt_req_id is  null then
                print_log(
                                   'Not able to fetch details from cursor get_audit_rpt_id using wrapper id :'||l_wrapper_req_id
                                 );
                 raise program_error;
               end if; 
               --                 
                l_email_subject :=g_instance||'HDS CONCUR: Import Accruals - Status Update';
                --
                l_file_name :=l_job_code||'_'||get_audit_rpt_rec.audit_rpt_req_id||'_1.PDF';            
                --
                print_log('l_file_name ='||l_file_name);            
                --
               open c_new_file;
               fetch c_new_file into l_new_file;
               close c_new_file;
               --
               if  l_new_file is  null then
                print_log(
                                   'Not able to derive new file name for audit report using report set request id :'||get_audit_rpt_rec.audit_rpt_req_id
                                   ||', wrapper id :'||l_wrapper_req_id
                                 );
                 raise program_error;
               else
                print_log (' l_new_file =>'||l_new_file);
               end if; 
               --      
               if p_push_to_sharepoint =g_Y then
                     --
                     begin 
                            --
                            select sharepoint_email_address 
                            into     l_non_prod_email
                            from  xxcus.xxcus_concur_extract_controls
                            where 1 =1
                                  and employee_group =g_GSC_emp_grp
                                  and type                         =g_control_type_AUDIT
                            ; 
                            --
                     exception
                      when no_data_found then
                         l_non_prod_email :='hds_concur_audit@share.hdsupply.com';
                      when others then
                       print_log('Error in fetching sharepoint email address for employee group GSC ORACLE and type AUDIT');
                       l_non_prod_email :='hds_concur_audit@share.hdsupply.com';                                                      
                     end;
                     -- 
               else
                     l_non_prod_email :=p_non_prod_email;                              
               end if;                            
               --      
               l_req_id := fnd_request.submit_request
                 (
                   'XXCUS'
                  ,'XXCUS_CONCUR_EMAIL_RPT'
                  ,NULL
                  ,NULL
                  ,FALSE
                  ,l_file_name --absolute file path and name. Example /ebizlog/conc/out/XXCUS_CONCUR_AUDIT_18900888_1.PDF                  
                  ,l_non_prod_email -- non production recipient email id
                  ,g_notif_email_from --sender email id
                  ,l_email_subject --email subject  
                  ,l_new_file --new file name that goes to sharepoint folder Concur Audit Reports 
                  ,l_non_prod_cc_email               
                 );
              --
              l_sec := 'End: Email audit file, request_id ='||l_req_id;
              --
                  fnd_file.put_line(fnd_file.log, l_sec);
              --
          exception
           when no_data_found then
            print_log ('Failed to find audit report request id');
            raise program_error;
           when program_error then
            raise;
           when others then
            print_log ('when others - failed to find audit report request id');
            raise program_error;            
          end;
          --
        retcode :=0;
        errbuf :='Child request [ HDS CONCUR (Set) : Process New Batch completed. Exiting HDS CONCUR : Kickoff Actuals Import Wrapper ]';
        print_log(errbuf);
       -- end copy
    end if;
   --   
  EXCEPTION
    WHEN srs_failed THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := 'Call to set_request_set failed: ' || fnd_message.get ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN submitprog_failed THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Call to submit_program failed: ' || fnd_message.get ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN submitset_failed THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Call to submit_set failed: ' || fnd_message.get ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Request set submission failed - unknown error: ' ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END SUBMIT_REQUEST_SET;
  --       
end xxcus_concur_accruals_pkg;
/