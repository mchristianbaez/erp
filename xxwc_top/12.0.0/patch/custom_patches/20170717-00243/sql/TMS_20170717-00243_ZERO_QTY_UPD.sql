/*************************************************************************
  $Header TMS_20170717-00243_ZERO_QTY_UPD.sql $
  Module Name: 20170717-00243  DELETE the zero qty records 

  PURPOSE: Data fix  

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        24-JUL-2017  Pattabhi Avula        TMS# 20170717-00243 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170717-00243   , Before Update');

UPDATE po_requisition_lines_all
SET quantity = 0
WHERE requisition_line_id = 42939285;

   DBMS_OUTPUT.put_line (
         'TMS: 20170717-00243 Requisition Lines Updated and number of records (Expected:1): '
      || SQL%ROWCOUNT);
	  
UPDATE po_req_distributions_all
   SET REQ_LINE_QUANTITY = 0
 WHERE requisition_line_id = 42939285;
 
  DBMS_OUTPUT.put_line (
         'TMS: 20170717-00243 Requisition Lines Updated and number of records (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170717-00243   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170717-00243, Errors : ' || SQLERRM);
END;
/