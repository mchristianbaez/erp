/*************************************************************************
  $Header TMS_20180725-00035_CROSS_OVER_CUSTOMER_UPDATE.sql $
  Module Name: TMS_20180725-00035  CROSS OVER - CUSTOMER UPDATE SCRIPT

  PURPOSE: Data fix to update the CROSS OVER - CUSTOMER UPDATE SCRIPT
  
  ----------------------------------------------------------------
  CROSS OVER - CUSTOMER UPDATE SCRIPT
  ----------------------------------------------------------------

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        25-JUL-2018  Pattabhi Avula        TMS#20180725-00035 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

DECLARE
  CURSOR cur IS
    SELECT DISTINCT ahh_customer_number 
, ahh_customer_site_number
, ahh_OUTSIDE_SALES_REP_ID
, orcl_SALESREP_NUMBER
, orcl_salesrep_id
, cust_acct_site_id 
from (
    SELECT DISTINCT customer_number ahh_customer_number 
           ,customer_site_number ahh_customer_site_number
           ,OUTSIDE_SALES_REP_ID ahh_OUTSIDE_SALES_REP_ID
           ,a.SALESREP_NUMBER orcl_SALESREP_NUMBER
           ,a.salesrep_id orcl_salesrep_id
           ,hcas1.cust_acct_site_id
      FROM xxwc.xxwc_ar_conv_cust_acct_tbl_new stg
           ,RA_SALESREPS_ALL A
           ,XXWC.XXWC_AHH_AM_T B
           ,apps.hz_cust_acct_sites_all hcas1
           ,apps.hz_cust_site_uses_all hcsu1
     WHERE 1=1
       AND stg.customer_number IN (SELECT cust_num FROM XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T) 
       AND A.ORG_ID = 162
       AND UPPER (B.SLSREPOUT) = UPPER (stg.OUTSIDE_SALES_REP_ID)
       AND A.SALESREP_NUMBER = B.SALESREP_ID
       AND hcas1.attribute17 =  customer_number||'-'||customer_site_number
       AND TRUNC(hcas1.creation_date) >= TO_DATE('09-JUL-2018','DD-MON-YYYY')
       AND hcas1.cust_acct_Site_id= hcsu1.cust_acct_Site_id
       AND hcsu1.primary_salesrep_id != a.salesrep_id       
    UNION
        SELECT DISTINCT stg.ORIG_SYSTEM_REFERENCE ahh_customer_number 
           ,stg.ATTRIBUTE17 ahh_customer_site_number
           ,PRIMARY_SALESREP_NUMBER ahh_OUTSIDE_SALES_REP_ID
           ,a.SALESREP_NUMBER orcl_SALESREP_NUMBER
           ,a.salesrep_id orcl_salesrep_id
           ,hcas1.cust_acct_site_id
      FROM xxwc.XXWC_AR_CUST_SITE_IFACE_STG stg
           ,RA_SALESREPS_ALL A
           ,XXWC.XXWC_AHH_AM_T B
           ,apps.hz_cust_acct_sites_all hcas1
           ,apps.hz_cust_site_uses_all hcsu1 
     WHERE 1=1 
       AND stg.ORIG_SYSTEM_REFERENCE IN (SELECT cust_num FROM XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T) 
       AND A.ORG_ID = 162
       AND UPPER (B.SLSREPOUT) = UPPER (stg.PRIMARY_SALESREP_NUMBER)
       AND A.SALESREP_NUMBER = B.SALESREP_ID
       AND hcas1.attribute17 =  stg.ORIG_SYSTEM_REFERENCE||'-'||stg.ATTRIBUTE17
       AND hcas1.cust_acct_Site_id= hcsu1.cust_acct_Site_id
       AND hcsu1.primary_salesrep_id != a.salesrep_id    
    UNION
    SELECT DISTINCT customer_number ahh_customer_number 
           ,customer_site_number ahh_customer_site_number
           ,OUTSIDE_SALES_REP_ID ahh_OUTSIDE_SALES_REP_ID
           ,a.SALESREP_NUMBER orcl_SALESREP_NUMBER
           ,a.salesrep_id orcl_salesrep_id
           ,hcas1.cust_acct_site_id
      FROM xxwc.xxwc_ar_conv_cust_acct_tbl_bkp stg
           ,RA_SALESREPS_ALL A
           ,XXWC.XXWC_AHH_AM_T B
           ,apps.hz_cust_acct_sites_all hcas1
           ,apps.hz_cust_site_uses_all hcsu1
     WHERE 1=1
       AND stg.customer_number IN (SELECT cust_num FROM XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T) 
       AND A.ORG_ID = 162
       AND UPPER (B.SLSREPOUT) = UPPER (stg.OUTSIDE_SALES_REP_ID)
       AND A.SALESREP_NUMBER = B.SALESREP_ID
       AND hcas1.attribute17 =  customer_number||'-'||customer_site_number
       AND hcas1.cust_acct_Site_id= hcsu1.cust_acct_Site_id
       AND hcsu1.primary_salesrep_id != a.salesrep_id       
       AND TRUNC(hcas1.creation_date) >= TO_DATE('09-JUL-2018','DD-MON-YYYY'))    
     ORDER BY ahh_customer_number, ahh_customer_site_number;
BEGIN

DBMS_OUTPUT.put_line ('TMS: 20180725-00035    , Before Update');

--FOR rec IN cur LOOP
--UPDATE apps.hz_cust_site_uses_all hcsu
--   SET primary_salesrep_id = rec.orcl_salesrep_id
-- WHERE cust_acct_site_id = rec.cust_acct_site_id
--   AND apps.xxwc_om_force_ship_pkg.is_row_locked (hcsu.ROWID, 'HZ_CUST_SITE_USES_ALL')  = 'N';
--END LOOP;

--COMMIT;

UPDATE apps.hz_cust_site_uses_all hcsu
   SET primary_salesrep_id = 100415269
 WHERE cust_acct_site_id IN (1620342
, 1656019
, 1658887
, 1658888
, 1673660
, 1673661
, 1673979
, 1673980
, 1673981
, 1695448
, 1673978
, 1676875
, 1676876
, 1676877
, 1676878
, 1676879
, 1676874
, 1680951
, 1680952
, 1680953
, 1680954
, 1680955
, 1680956
, 1680957
, 1680958
, 1680959
, 1680960
, 1680961
, 1680962
, 1680963
, 1680964
, 1680965
, 1680966
, 1680967
, 1680968
, 1680969
, 1680970
, 1682980
, 1682981
, 1682982
, 1682983
, 1693236
, 1693237
, 1695350
, 1695351
, 1694840
, 1694841);

DBMS_OUTPUT.put_line (
         'TMS: 20180725-00035 - number of records updated: '
      || SQL%ROWCOUNT);

COMMIT;

UPDATE apps.hz_cust_site_uses_all hcsu
   SET primary_salesrep_id = 100058253
 WHERE cust_acct_site_id = 1696913
 ;

DBMS_OUTPUT.put_line (
         'TMS: 20180725-00035 - number of records updated: '
      || SQL%ROWCOUNT);

COMMIT;

UPDATE apps.hz_cust_site_uses_all hcsu
   SET primary_salesrep_id = 100408258
 WHERE cust_acct_site_id = 1645562
 ;

DBMS_OUTPUT.put_line (
         'TMS: 20180725-00035 - number of records updated: '
      || SQL%ROWCOUNT);

COMMIT;

UPDATE apps.hz_cust_site_uses_all hcsu
   SET primary_salesrep_id = 100414270
 WHERE cust_acct_site_id = 1702931
 ;

DBMS_OUTPUT.put_line (
         'TMS: 20180725-00035 - number of records updated: '
      || SQL%ROWCOUNT);

COMMIT;

UPDATE apps.hz_cust_site_uses_all hcsu
   SET primary_salesrep_id = 100415287
 WHERE cust_acct_site_id = 1700504
 ;

DBMS_OUTPUT.put_line (
         'TMS: 20180725-00035 - number of records updated: '
      || SQL%ROWCOUNT);

COMMIT;

DBMS_OUTPUT.put_line ('TMS: 20180725-00035   , End Update');
EXCEPTION
 WHEN OTHERS THEN
  DBMS_OUTPUT.put_line ('Unexpected error while executing the script: '||SQLERRM);
END; 
/ 