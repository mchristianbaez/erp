--//============================================================================
--//
--// Object Name         :: UPGRADE RESPONSIBILITY INFO  
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--//  1.0       16-Dec-2016      Siva   		TMS# 20161216-00026 
--//============================================================================
DECLARE
  CURSOR cur_user_list
  IS
    SELECT fu.user_id
    FROM fnd_user fu
    WHERE fu.user_name IN('10011289', '10012203', 'BK025113', 'BR058633', 'BS006141', 'CO008675', 'DE001783', 'DJ058912', 'EB053236', 'FT015774', 'GP050872', 'GR059241', 'ID020048', 'JG023358', 'JP016147', 'JS020126', 'KB047432', 'KG013546', 'KP059700', 'LA023190', 'LC039088', 'LC057799', 'MB001000', 'MC027824', 'MM027735', 'MM059989', 'NR059242', 'NS057223', 'PA022863', 'PN020488', 'PP018915', 'RG005654', 'RN020533', 'RR024931', 'RT008057', 'RV003897', 'SA016918', 'SA057257', 'SA059956', 'SK054540', 'SK054679', 'SR056655', 'SS084202', 'TD002849', 'VP038429', 'XXEIS_RS_ADMIN', 'XXWC_INT_FINANCE');
  l_success BOOLEAN;
BEGIN
  FOR rec_user IN cur_user_list
  LOOP
    l_success := FND_PROFILE.save ( x_name => 'XXEIS_RSC_REPORT_REQ_VIEW' ,
									x_value => 'ALL' ,
									x_level_name => 'USER' ,
									x_level_value => rec_user.user_id, 
									x_level_value_app_id => NULL ) ;
  END LOOP;
  COMMIT;
EXCEPTION
WHEN OTHERS THEN
  dbms_output.put_line('Exception in updating Profile Option:'||SUBSTR(SQLERRM,1,100));
END;
/
DECLARE
  l_old_resp_id NUMBER;
  l_new_resp_id NUMBER;
BEGIN
  SELECT responsibility_id
  INTO l_old_resp_id
  FROM fnd_responsibility_vl
  WHERE responsibility_name='XXEIS Reporting Administrator';
  
  SELECT responsibility_id
  INTO l_new_resp_id
  FROM fnd_responsibility_vl
  WHERE responsibility_name='XXEIS eXpress Administrator';
  
  UPDATE XXEIS.EIS_RS_REPORT_SECURITY
  SET responsibility_id  =l_new_resp_id
  WHERE responsibility_id=l_old_resp_id;
  
  COMMIT;
EXCEPTION
WHEN OTHERS THEN
  NULL;
END;
/
