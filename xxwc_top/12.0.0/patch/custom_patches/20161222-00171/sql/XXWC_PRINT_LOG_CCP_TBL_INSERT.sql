/*****************************************************************************************************************************************************
  $Header TMS_20161222-00171 

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        27-Mar-2017 Rakesh Patel            TMS#20161222-00171 - PRINT LOG FORM.
*****************************************************************************************************************************************************/
set serverout on

--'XXWC OM Packing Slip - Zero Copies' 
insert into XXWC.XXWC_PRINT_LOG_CCP_TBL
VALUES
   (20005
   ,(select CONCURRENT_PROGRAM_ID from apps.fnd_concurrent_programs_tl
    where user_concurrent_program_name ='XXWC OM Packing Slip - Zero Copies')
   ,2
   ,1
   ,'OE_ORDER_HEADERS_ALL');

COMMIT;