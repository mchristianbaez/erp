SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

declare
l_file_val       VARCHAR2(2000);
l_result_out     varchar2(30);
err_msg          VARCHAR2(240);
l_return_status  BOOLEAN := false;
v_org_id         NUMBER;
v_line_id        NUMBER := 63434812;

cursor pending_receipts is
       select rcv.transaction_id
       from   rcv_transactions rcv,
              po_line_locations_all poll,
              oe_drop_ship_sources oed,
              oe_order_lines_all oel
       where  oel.source_type_code = 'EXTERNAL'
       and    nvl(oel.shipped_quantity,0) = 0
       and    oed.line_id = oel.line_id
       and    oed.line_location_id is not null
       and    poll.line_location_id = oed.line_location_id
       and    rcv.po_line_location_id = poll.line_location_id
       and    oel.line_id = v_line_id
       and    rcv.TRANSACTION_TYPE = 'DELIVER';

cursor organization (v_order_line_id NUMBER) is select org_id
                                                from   oe_order_lines_all
                                                where  line_id = v_order_line_id;

BEGIN
    oe_debug_pub.debug_on;
    oe_debug_pub.initialize;
    l_file_val := OE_DEBUG_PUB.Set_Debug_Mode('FILE');
    oe_Debug_pub.setdebuglevel(5);
    dbms_output.put_line('Working for Line_Id : '||v_line_id||'       Pls  wait ...');
    oe_debug_pub.add( 'Working for Line_Id : '||v_line_id);
    
    oe_debug_pub.add( 'Updating the flow_status_code');
    
    UPDATE oe_order_lines_all oeol
       SET oeol.flow_status_code = 'AWAITING_RECEIPT'
     WHERE oeol.line_id = v_line_id
       AND oeol.flow_status_code <> 'AWAITING_RECEIPT';
     

    OPEN organization(v_line_id);
    FETCH organization INTO v_org_id;
    CLOSE organization;
    DBMS_OUTPUT.put_line('Organization ID set is : '||v_org_id);
    OE_DEBUG_PUB.ADD( 'Setting client info to '||v_org_id);
 mo_global.init('ONT');
 MO_GLOBAL.SET_POLICY_CONTEXT('S', v_org_id);
    --fnd_client_info.set_org_context(to_char(v_org_id));

    for all_lines in pending_receipts loop
        l_return_status := OE_DS_PVT.DROPSHIPRECEIVE(all_lines.transaction_id,'INV');
        if l_return_status = true then
            OE_DEBUG_PUB.ADD( 'l_return_status = TRUE' );
            DBMS_OUTPUT.PUT_LINE('  Script was successfully ..... !!!   ');
            OE_DEBUG_PUB.ADD('   Script was successfully .....  !!!! ');

        else
            OE_DEBUG_PUB.ADD( 'l_return_status = FALSE' );
            DBMS_OUTPUT.PUT_LINE('  Script was Un-successfully .....  !!!  ');
            OE_DEBUG_PUB.ADD('   Hard Luck !!! Script was Un-successfully ..... !!!  ');
        end if;
    end loop;
    COMMIT;

    dbms_output.put_line('Debug file name and path: '||OE_DEBUG_PUB.G_DIR||'/'||OE_DEBUG_PUB.G_FILE);
    DBMS_OUTPUT.PUT_LINE('.');
EXCEPTION
 WHEN OTHERS THEN
         ERR_MSG := 'ERROR :'||SQLERRM;
         DBMS_OUTPUT.PUT_LINE('ERROR: Pls Rollback.................');
         DBMS_OUTPUT.PUT_LINE(SQLERRM);
         OE_DEBUG_PUB.ADD(ERR_MSG);
END;
/
