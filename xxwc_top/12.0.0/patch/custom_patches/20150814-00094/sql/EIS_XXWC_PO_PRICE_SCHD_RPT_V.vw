CREATE OR REPLACE force VIEW XXEIS.EIS_XXWC_PO_PRICE_SCHD_RPT_V (BPA,
         type_lookup_code,
         vendor_name,
         supplier_number,
         site,
         approve_flag,
         IMPLEMENT_DATE,
         created,
         created_by,
         cnt_of_items_in_price_schedule)
  /* *************************************************************************************************************
  $Header XXEIS.EIS_XXWC_PO_PRICE_SCHD_RPT_V $
  Module Name: Purchasing
  PURPOSE: This view will give Price Schedule Report
  TMS Task Id : 20150814-00094 
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        09/18/2015  Mahender Reddy         Initial Version 
  **************************************************************************************************************/
AS
SELECT stg.po_number BPA,
         pha.type_lookup_code,
         asl.vendor_name,
         asl.segment1 supplier_number,
         assa.VENDOR_SITE_CODE site,
         DECODE(NVL(APPROVE_FLAG,'1'),'1','No','Yes') approve_flag,
         IMPLEMENT_DATE,
         TRUNC (stg.Creation_date) created,
         fu.description created_by,
         COUNT (1) cnt_of_items_in_price_schedule
    FROM XXWC.XXWC_BPA_PZ_STAGING_TBL stg,
         po_headers_all pha,
         ap_suppliers asl,
         fnd_user fu,
         ap_supplier_sites_all assa
   WHERE     stg.po_number = pha.segment1
         --and pha.type_lookup_code='BLANKET'
         AND pha.vendor_id = asl.vendor_id
         AND fu.user_id = stg.created_by
         AND assa.vendor_site_id = pha.vendor_site_id
GROUP BY stg.po_number,
         pha.type_lookup_code,
         asl.vendor_name,
         asl.segment1,
         assa.VENDOR_SITE_CODE,
         APPROVE_FLAG,
         IMPLEMENT_DATE,
         fu.description,
         TRUNC (stg.Creation_date)
order by  TRUNC (stg.Creation_date) desc
/