--TMS#20150814-00094 New report Initial Version
--Report Name            : Price Schedule Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Price Schedule Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_PO_PRICE_SCHD_RPT_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_PO_PRICE_SCHD_RPT_V',201,'','','','','MR020532','XXEIS','Eis Xxwc Po Price Schd Rpt V','EXPPSRV','','');
--Delete View Columns for EIS_XXWC_PO_PRICE_SCHD_RPT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_PO_PRICE_SCHD_RPT_V',201,FALSE);
--Inserting View Columns for EIS_XXWC_PO_PRICE_SCHD_RPT_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_PRICE_SCHD_RPT_V','CNT_OF_ITEMS_IN_PRICE_SCHEDULE',201,'Cnt Of Items In Price Schedule','CNT_OF_ITEMS_IN_PRICE_SCHEDULE','','','','MR020532','NUMBER','','','Cnt Of Items In Price Schedule','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_PRICE_SCHD_RPT_V','CREATED_BY',201,'Created By','CREATED_BY','','','','MR020532','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_PRICE_SCHD_RPT_V','CREATED',201,'Created','CREATED','','','','MR020532','DATE','','','Created','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_PRICE_SCHD_RPT_V','IMPLEMENT_DATE',201,'Implement Date','IMPLEMENT_DATE','','','','MR020532','DATE','','','Implement Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_PRICE_SCHD_RPT_V','APPROVE_FLAG',201,'Approve Flag','APPROVE_FLAG','','','','MR020532','VARCHAR2','','','Approve Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_PRICE_SCHD_RPT_V','SITE',201,'Site','SITE','','','','MR020532','VARCHAR2','','','Site','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_PRICE_SCHD_RPT_V','SUPPLIER_NUMBER',201,'Supplier Number','SUPPLIER_NUMBER','','','','MR020532','VARCHAR2','','','Supplier Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_PRICE_SCHD_RPT_V','VENDOR_NAME',201,'Vendor Name','VENDOR_NAME','','','','MR020532','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_PRICE_SCHD_RPT_V','TYPE_LOOKUP_CODE',201,'Type Lookup Code','TYPE_LOOKUP_CODE','','','','MR020532','VARCHAR2','','','Type Lookup Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_PRICE_SCHD_RPT_V','BPA',201,'Bpa','BPA','','','','MR020532','VARCHAR2','','','Bpa','','','');
--Inserting View Components for EIS_XXWC_PO_PRICE_SCHD_RPT_V
--Inserting View Component Joins for EIS_XXWC_PO_PRICE_SCHD_RPT_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Price Schedule Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Price Schedule Report - WC
xxeis.eis_rs_ins.lov( '','select distinct vendor_site_code
from ap_supplier_sites_all
order by vendor_site_code','','XXWC SUPPLIER SITE','XXWC SUPPLIER SITE','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','select aps.vendor_name,aps.segment1 Vendor_number
from ap_suppliers aps
order by aps.vendor_name','','XXWC SUPPLIER NAME','XXWC SUPPLIER NAME','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','select aps.segment1 Vendor_number,aps.vendor_name
from ap_suppliers aps
order by aps.segment1','','XXWC SUPPLIER NUMBER','XXWC SUPPLIER NUMBER','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','select distinct PO_NUMBER BPA_NUMBER
from XXWC.XXWC_BPA_PZ_STAGING_TBL','','XXWC BPA NUMBER','XXWC BPA NUMBER','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Price Schedule Report - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Price Schedule Report - WC
xxeis.eis_rs_utility.delete_report_rows( 'Price Schedule Report - WC' );
--Inserting Report - Price Schedule Report - WC
xxeis.eis_rs_ins.r( 201,'Price Schedule Report - WC','','Displays the total count of items by supplier that are pending approval/import on proposed blanket purchase agreement (BPA) pricing.
if they don''t address the pending approvals the field will not be seeing the most recent costs on BPAs and risk cost discrepancies/APINs
','','','','MR020532','EIS_XXWC_PO_PRICE_SCHD_RPT_V','Y','','','MR020532','','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - Price Schedule Report - WC
xxeis.eis_rs_ins.rc( 'Price Schedule Report - WC',201,'APPROVE_FLAG','Approved (Y/N)','Approve Flag','','','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_PRICE_SCHD_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Price Schedule Report - WC',201,'BPA','BPA Number','Bpa','','','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_PRICE_SCHD_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Price Schedule Report - WC',201,'CNT_OF_ITEMS_IN_PRICE_SCHEDULE','Count','Cnt Of Items In Price Schedule','','~~~','default','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_PRICE_SCHD_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Price Schedule Report - WC',201,'CREATED','Price Schedule Creation Date','Created','','','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_PRICE_SCHD_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Price Schedule Report - WC',201,'CREATED_BY','Created By','Created By','','','default','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_PRICE_SCHD_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Price Schedule Report - WC',201,'IMPLEMENT_DATE','Effective Date','Implement Date','','','default','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_PRICE_SCHD_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Price Schedule Report - WC',201,'SITE','Supplier Site','Site','','','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_PRICE_SCHD_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Price Schedule Report - WC',201,'SUPPLIER_NUMBER','Supplier Number','Supplier Number','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_PRICE_SCHD_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Price Schedule Report - WC',201,'VENDOR_NAME','Supplier Name','Vendor Name','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_PRICE_SCHD_RPT_V','','');
--Inserting Report Parameters - Price Schedule Report - WC
xxeis.eis_rs_ins.rp( 'Price Schedule Report - WC',201,'Supplier Number','Supplier Number','SUPPLIER_NUMBER','IN','XXWC SUPPLIER NUMBER','','NUMERIC','N','Y','1','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Price Schedule Report - WC',201,'Supplier Name','Supplier Name','VENDOR_NAME','IN','XXWC SUPPLIER NAME','','VARCHAR2','N','Y','2','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Price Schedule Report - WC',201,'Supplier Site','Supplier Site','SITE','=','XXWC SUPPLIER SITE','','VARCHAR2','N','Y','3','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Price Schedule Report - WC',201,'BPA Number','BPA Number','BPA','IN','XXWC BPA NUMBER','','VARCHAR2','N','Y','4','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Price Schedule Report - WC',201,'Effective Date From','Effective Date From','CREATED','>=','','','DATE','N','Y','5','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Price Schedule Report - WC',201,'Effective Date To','Effective Date To','CREATED','<=','','','DATE','N','Y','6','','Y','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - Price Schedule Report - WC
xxeis.eis_rs_ins.rcn( 'Price Schedule Report - WC',201,'SUPPLIER_NUMBER','IN',':Supplier Number','','','Y','1','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Price Schedule Report - WC',201,'VENDOR_NAME','IN',':Supplier Name','','','Y','2','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Price Schedule Report - WC',201,'SITE','=',':Supplier Site','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Price Schedule Report - WC',201,'CREATED','>=',':Effective Date From','','','Y','5','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Price Schedule Report - WC',201,'CREATED','<=',':Effective Date To','','','Y','6','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Price Schedule Report - WC',201,'BPA','IN',':BPA Number','','','Y','4','Y','MR020532');
--Inserting Report Sorts - Price Schedule Report - WC
--Inserting Report Triggers - Price Schedule Report - WC
--Inserting Report Templates - Price Schedule Report - WC
--Inserting Report Portals - Price Schedule Report - WC
--Inserting Report Dashboards - Price Schedule Report - WC
--Inserting Report Security - Price Schedule Report - WC
xxeis.eis_rs_ins.rsec( 'Price Schedule Report - WC','401','','50990',201,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Price Schedule Report - WC','201','','50983',201,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Price Schedule Report - WC','20005','','50900',201,'MR020532','','');
--Inserting Report Pivots - Price Schedule Report - WC
END;
/
set scan on define on
