--Report Name            : HDS Invoice Inquiry by Type - RENT
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_REAL_ESTATE_INV
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_REAL_ESTATE_INV
xxeis.eis_rsc_ins.v( 'XXEIS_REAL_ESTATE_INV',200,'Paste SQL View for Invoice Inquiry by Type','1.0','','','XXEIS_RS_ADMIN','XXEIS','XXEIS_REAL_ESTATE_INV','X4LV','','','VIEW','US','','','');
--Delete Object Columns for XXEIS_REAL_ESTATE_INV
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_REAL_ESTATE_INV',200,FALSE);
--Inserting Object Columns for XXEIS_REAL_ESTATE_INV
xxeis.eis_rsc_ins.vc( 'XXEIS_REAL_ESTATE_INV','VENDOR_NUMBER',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_REAL_ESTATE_INV','VENDOR_SITE_CODE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Site Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_REAL_ESTATE_INV','VENDOR_NAME',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_REAL_ESTATE_INV','INVOICE_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_REAL_ESTATE_INV','INVOICE_NUM',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_REAL_ESTATE_INV','YPAYABLE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ypayable','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_REAL_ESTATE_INV','PAYMENT_STATUS_FLAG',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Status Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_REAL_ESTATE_INV','TERMS_DESCRIPTION',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Terms Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_REAL_ESTATE_INV','INVOICE_AMOUNT',200,'','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_REAL_ESTATE_INV','BRANCH',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_REAL_ESTATE_INV','INVTYPE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invtype','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_REAL_ESTATE_INV','DESCRIPTION',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_REAL_ESTATE_INV','GL_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Gl Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_REAL_ESTATE_INV','INVOICE_CANCELLED_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Cancelled Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_REAL_ESTATE_INV','CHECK_NUMBER',200,'','','','','','XXEIS_RS_ADMIN','NUMBER','','','Check Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_REAL_ESTATE_INV','CHECK_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Check Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_REAL_ESTATE_INV','PAYMENT_AMOUNT',200,'','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Payment Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_REAL_ESTATE_INV','PAY_GROUP_LOOKUP_CODE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pay Group Lookup Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_REAL_ESTATE_INV','APPROVAL_STATUS',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Approval Status','','','','US','');
--Inserting Object Components for XXEIS_REAL_ESTATE_INV
xxeis.eis_rsc_ins.vcomp( 'XXEIS_REAL_ESTATE_INV','AP_INVOICES',200,'','ai','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','AP_INVOICES','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_REAL_ESTATE_INV','AP_CHECKS',200,'','ac','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','AP_CHECKS','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for XXEIS_REAL_ESTATE_INV
xxeis.eis_rsc_ins.vcj( 'XXEIS_REAL_ESTATE_INV','AP_INVOICES','ai',200,'X4LV.invoice_id','=','ai.invoice_id(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_REAL_ESTATE_INV','AP_CHECKS','ac',200,'X4LV.check_id','=','ac.check_id(+)','','','','','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report LOV Data for HDS Invoice Inquiry by Type - RENT
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS Invoice Inquiry by Type - RENT
xxeis.eis_rsc_ins.lov( 200,'select distinct ivc.pay_group_lookup_code from apps.XXCUSAP_INVOICES_ALL ivc order by 1','','XXWC Pay Group Lookup Code LOV','XXWC Pay Group Lookup Code LOV','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report Data for HDS Invoice Inquiry by Type - RENT
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS Invoice Inquiry by Type - RENT
xxeis.eis_rsc_utility.delete_report_rows( 'HDS Invoice Inquiry by Type - RENT',200 );
--Inserting Report - HDS Invoice Inquiry by Type - RENT
xxeis.eis_rsc_ins.r( 200,'HDS Invoice Inquiry by Type - RENT','','','','','','XXEIS_RS_ADMIN','XXEIS_REAL_ESTATE_INV','Y','','','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - HDS Invoice Inquiry by Type - RENT
xxeis.eis_rsc_ins.rc( 'HDS Invoice Inquiry by Type - RENT',200,'APPROVAL_STATUS','Approval Status','','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_REAL_ESTATE_INV','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS Invoice Inquiry by Type - RENT',200,'BRANCH','Branch','','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_REAL_ESTATE_INV','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS Invoice Inquiry by Type - RENT',200,'CHECK_DATE','Check Date','','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_REAL_ESTATE_INV','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS Invoice Inquiry by Type - RENT',200,'CHECK_NUMBER','Check Number','','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_REAL_ESTATE_INV','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS Invoice Inquiry by Type - RENT',200,'DESCRIPTION','Description','','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_REAL_ESTATE_INV','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS Invoice Inquiry by Type - RENT',200,'GL_DATE','Gl Date','','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_REAL_ESTATE_INV','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS Invoice Inquiry by Type - RENT',200,'INVOICE_AMOUNT','Invoice Amount','','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_REAL_ESTATE_INV','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS Invoice Inquiry by Type - RENT',200,'INVOICE_CANCELLED_DATE','Invoice Cancelled Date','','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_REAL_ESTATE_INV','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS Invoice Inquiry by Type - RENT',200,'INVOICE_DATE','Invoice Date','','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_REAL_ESTATE_INV','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS Invoice Inquiry by Type - RENT',200,'INVOICE_NUM','Invoice Num','','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_REAL_ESTATE_INV','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS Invoice Inquiry by Type - RENT',200,'INVTYPE','Invtype','','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_REAL_ESTATE_INV','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS Invoice Inquiry by Type - RENT',200,'PAYMENT_AMOUNT','Payment Amount','','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_REAL_ESTATE_INV','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS Invoice Inquiry by Type - RENT',200,'PAYMENT_STATUS_FLAG','Payment Status Flag','','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_REAL_ESTATE_INV','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS Invoice Inquiry by Type - RENT',200,'TERMS_DESCRIPTION','Terms Description','','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_REAL_ESTATE_INV','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS Invoice Inquiry by Type - RENT',200,'VENDOR_NAME','Vendor Name','','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_REAL_ESTATE_INV','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS Invoice Inquiry by Type - RENT',200,'VENDOR_NUMBER','Vendor Number','','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_REAL_ESTATE_INV','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS Invoice Inquiry by Type - RENT',200,'VENDOR_SITE_CODE','Vendor Site Code','','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_REAL_ESTATE_INV','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS Invoice Inquiry by Type - RENT',200,'YPAYABLE','Ypayable','','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_REAL_ESTATE_INV','','','GROUP_BY','US','','');
--Inserting Report Parameters - HDS Invoice Inquiry by Type - RENT
xxeis.eis_rsc_ins.rp( 'HDS Invoice Inquiry by Type - RENT',200,'Start_Date','Start_Date','GL_DATE','>=','','','DATE','N','Y','1','N','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','','XXEIS_REAL_ESTATE_INV','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Invoice Inquiry by Type - RENT',200,'End_Date','End_Date','GL_DATE','<=','','','DATE','N','Y','2','N','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','','XXEIS_REAL_ESTATE_INV','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Invoice Inquiry by Type - RENT',200,'Pay Group Type','Pay Group Type','PAY_GROUP_LOOKUP_CODE','IN','XXWC Pay Group Lookup Code LOV','''RENT''','VARCHAR2','N','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_REAL_ESTATE_INV','','','US','');
--Inserting Dependent Parameters - HDS Invoice Inquiry by Type - RENT
--Inserting Report Conditions - HDS Invoice Inquiry by Type - RENT
xxeis.eis_rsc_ins.rcnh( 'HDS Invoice Inquiry by Type - RENT',200,'TRUNC(X4LV.GL_DATE) >= Start_Date','ADVANCED','','  1#$# ','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','','','Start_Date','','','','','XXEIS_REAL_ESTATE_INV','','','','','','GREATER_THAN_EQUALS','Y','Y','TRUNC(X4LV.GL_DATE)','','','','1',200,'HDS Invoice Inquiry by Type - RENT','TRUNC(X4LV.GL_DATE) >= Start_Date');
xxeis.eis_rsc_ins.rcnh( 'HDS Invoice Inquiry by Type - RENT',200,'TRUNC(X4LV.GL_DATE) <= End_Date','ADVANCED','','  1#$# ','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','','','End_Date','','','','','XXEIS_REAL_ESTATE_INV','','','','','','LESS_THAN_EQUALS','Y','Y','TRUNC(X4LV.GL_DATE)','','','','1',200,'HDS Invoice Inquiry by Type - RENT','TRUNC(X4LV.GL_DATE) <= End_Date');
xxeis.eis_rsc_ins.rcnh( 'HDS Invoice Inquiry by Type - RENT',200,'X4LV.PAY_GROUP_LOOKUP_CODE IN Payment Type','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','PAY_GROUP_LOOKUP_CODE','','Pay Group Type','','','','','XXEIS_REAL_ESTATE_INV','','','','','','IN','Y','Y','','','','','1',200,'HDS Invoice Inquiry by Type - RENT','X4LV.PAY_GROUP_LOOKUP_CODE IN Payment Type');
--Inserting Report Sorts - HDS Invoice Inquiry by Type - RENT
--Inserting Report Triggers - HDS Invoice Inquiry by Type - RENT
--inserting report templates - HDS Invoice Inquiry by Type - RENT
--Inserting Report Portals - HDS Invoice Inquiry by Type - RENT
--inserting report dashboards - HDS Invoice Inquiry by Type - RENT
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS Invoice Inquiry by Type - RENT','200','XXEIS_REAL_ESTATE_INV','XXEIS_REAL_ESTATE_INV','N','');
--inserting report security - HDS Invoice Inquiry by Type - RENT
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','HDS_AP_ADMIN_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','HDS_AP_ADMIN_US_GSCIWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','HDS_AP_DISBURSEMTS_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','HDS_AP_INQUIRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','HDS_AP_MGR_NOSUP_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','HDS_AP_TRNS_ENTRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','HDS_OIE_USER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','PAYABLES_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','XXWC_PAY_NO_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','XXWC_PAY_W_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','XXWC_PAY_DISBURSE',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','XXWC_PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','XXWC_PAY_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','XXWC_PAY_VENDOR_MSTR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','HDS_AP_MGR_NOSUP_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','HDS_AP_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','HDS_PYABLS_MNGR_CAN',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','HDS_PYBLS_MNGR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','20005','','XXWC_VIEW_ALL_EIS_REPORTS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','HDS_PAYABLES_CLOSE_GSC_GLBL',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Invoice Inquiry by Type - RENT','200','','HDS_PAYABLES_CLOSE_GLBL',200,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS Invoice Inquiry by Type - RENT
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- HDS Invoice Inquiry by Type - RENT
xxeis.eis_rsc_ins.rv( 'HDS Invoice Inquiry by Type - RENT','','HDS Invoice Inquiry by Type - RENT','SA059956','21-MAY-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
