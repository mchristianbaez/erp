/*************************************************************************
  $Header TMS_20161110-00262_Header_stuck_in_Booked.sql $
  Module Name: 20161110-00262  Header stuck in Booked  

  PURPOSE: Data fix to Header stuck in Booked issue 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        21-DEC-2016  Pattabhi Avula         TMS# 20161110-00262 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20161110-00262   , Before Update');

   UPDATE apps.oe_order_headers_all
   SET    flow_status_code='CLOSED',
          open_flag='N'
   WHERE  header_id=23990941;

   DBMS_OUTPUT.put_line (
         'TMS: 20161110-00262 Order headers table Updated and number of records (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20161110-00262   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161110-00262, Errors : ' || SQLERRM);
END;
/