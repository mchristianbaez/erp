/******************************************************************************
   NAME   : Alter_Table_Script_TMS_20160407_00190.sql
   PURPOSE: Script to remove parallelism 
 
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        04/07/2016  Gopi Damuluri    TMS# 20160407-00190 Script to remove parallelism
******************************************************************************/
ALTER TABLE XXWC.XXWC_PURC_MASS_UPL_ARCH NOPARALLEL;
ALTER TABLE XXWC.XXWC_PURC_MASS_UPLOAD_P NOPARALLEL;
ALTER TABLE XXWC.XXWC_PURC_MASS_UPLOAD_T NOPARALLEL;
/
