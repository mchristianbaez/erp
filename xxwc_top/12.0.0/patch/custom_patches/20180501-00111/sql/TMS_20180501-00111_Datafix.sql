/***********************************************************************************************************************************************
   NAME:     TMS_20180501-00111_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        05/01/2018  Rakesh Patel     TMS#20180501-00111
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before backup table');
   
   CREATE TABLE xxcus.xxcus_errors_tbl_bk010518
      SELECT ert.* 
        FROM xxcus.xxcus_errors_tbl ert 
       WHERE CALLED_FROM = 'XXWC_OM_DMS2_OB_PKG.ShipmentSvc_delete_line';
	   
   DBMS_OUTPUT.put_line ('Before deleting records');  
	   
      DELETE FROM xxcus.xxcus_errors_tbl ert 
       WHERE CALLED_FROM = 'XXWC_OM_DMS2_OB_PKG.ShipmentSvc_delete_line';
	   
   DBMS_OUTPUT.put_line ('Records Deleted from xxcus_errors_tbl -' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to delete record ' || SQLERRM);
	  ROLLBACK;
END;
/