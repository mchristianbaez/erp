/*************************************************************************
  $Header TMS_20161027-00178_Data_Fix_Receiving_Rental_Items_into_General_Sub.sql $
  Module Name: TMS_20161027-00178  Data Fix script 

  PURPOSE: Data Fix script 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        20-DEC-2016  Pattabhi Avula        TMS#20161027-00178 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
  
   l_return_status_s2   VARCHAR2 (1);
   x_msg_count          NUMBER;
   x_msg_data           VARCHAR2 (2000);
   x_msg_index          NUMBER;

   l_msg_s2             VARCHAR2 (240);                       --VARCHAR2 (32000);
   l_date_s2            DATE := SYSDATE;
   i_s2                 NUMBER := 0;
   l_count_s2           NUMBER := NULL;


   l_msg_count_s2       NUMBER;
   l_msg_data_s2        VARCHAR2 (1000);
   l_msg_dummy_s2       VARCHAR2 (1000);
   l_output_s2          VARCHAR2 (1000);
 l_user_id          NUMBER                         := 16991;
 l_resp_id NUMBER;
    l_resp_appl_id NUMBER;
	
	CURSOR C2 
   IS
	SELECT ood.organization_name,
          msi.segment1,
          msi.description,
          MSI.INVENTORY_ITEM_STATUS_CODE,
        --  mic.CATEGORY_CONCAT_SEGS ITEM_CATEGORY,
          msi.inventory_item_id,
          msi.organization_id,
          2 default_type,
          'Rental' SUBINVENTORY_CODE,
          SYSDATE creation_date,
          16991 created_by,
          SYSDATE last_upd_date,
          16991 last_upd_by,
          'NO' RECORD_ALREADY_EXIST
    FROM --mtl_item_categories_v mic,
          mtl_system_items_b msi,
          org_organization_definitions ood
    WHERE     1 = 1
  --  AND mic.inventory_item_id = msi.inventory_item_id
   -- AND mic.organization_id = msi.organization_id
   -- AND mic.organization_id = ood.organization_id
   and  msi.organization_id=  ood.organization_id
    AND ood.organization_code NOT IN ('MST', 'WCC')
    AND ood.OPERATING_UNIT = '162'
    AND NVL (OOD.DISABLE_DATE, TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
  -- AND MSI.SEGMENT1='R191FM3125Y'
  -- and msi.organization_id=223
   -- AND mic.CATEGORY_CONCAT_SEGS IN ('99.99RT', '99.99RR')
	AND msi.Item_Type = 'RENTAL'
    AND  EXISTS
               (SELECT 'x'
                  FROM mtl_item_sub_defaults misd
                 WHERE     misd.inventory_item_id =
                              msi.inventory_item_id
                       AND MISD.ORGANIZATION_ID = MSI.ORGANIZATION_ID
                       AND misd.default_type=2 AND SUBINVENTORY_CODE = 'General'--and misd.default_type=2
                                                                     )
    --AND ROWNUM=1
    AND msi.segment1 NOT IN ('RE-RENTAL CHARGE','Rental Charge');

BEGIN

 BEGIN
            select RESPONSIBILITY_ID, APPLICATION_ID
            INTO l_resp_id, l_resp_appl_id
            from FND_RESPONSIBILITY_TL
           WHERE responsibility_name  =  'HDS Inventory Super User - WC';
       EXCEPTION
          WHEN NO_DATA_FOUND
          THEN
                  DBMS_OUTPUT.put_line(
                'Entered into Exception when getting the responsibiliy id and applicationid');
                   END;
     --  fnd_global.apps_initialize (l_user_id, l_resp_id, l_resp_appl_id);
  
 -- Script2 is starting from here
  -- Total PROD records count for script IS: 
   BEGIN
          fnd_global.apps_initialize (l_user_id, l_resp_id, l_resp_appl_id);
     FOR c2_rec IN c2
     LOOP
   
      l_msg_data_s2:=NULL;
      l_msg_count_s2:=NULL;
      l_msg_data_s2:=NULL;
      
         INV_ITEM_SUB_DEFAULT_PKG.INSERT_UPD_ITEM_SUB_DEFAULTS (
            x_return_status       => l_return_status_s2,
            x_msg_count           => l_msg_count_s2,
            x_msg_data            => l_msg_data_s2,
            p_organization_id     => c2_rec.organization_id,
            p_inventory_item_id   => c2_rec.inventory_item_id,
            p_subinventory_code   => c2_rec.subinventory_code,
            p_default_type        => c2_rec.default_type,
            p_creation_date       => c2_rec.creation_date,
            p_created_by          => c2_rec.created_by,
            p_last_update_date    => c2_rec.last_upd_date,
            p_last_updated_by     => c2_rec.last_upd_by,
            p_process_code        => 'SYNC',
            p_commit              => 'T');
      
         IF l_return_status_s2 = fnd_api.g_ret_sts_success
         THEN
	     
	     DBMS_OUTPUT.put_line('Script2 End date update IS successful for item-branch :'
                  || c2_rec.segment1
                  || '-'
                  || c2_rec.organization_id);
                        
      	       COMMIT;
              ELSE
               ROLLBACK;
                DBMS_OUTPUT.put_line('Script2 End date update NOT successful for item-branch :'
                  || c2_rec.segment1
                  || '-'
                  || c2_rec.organization_id
				  ||'Reason is'
				  || l_msg_data_s2);
      
         END IF;
     END LOOP;
   EXCEPTION
      WHEN OTHERS
        THEN
          ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161027-00178 Script2 , Errors : ' || SQLERRM); 
   END; 
	-- End the second script
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161027-00178 , Errors : ' || SQLERRM);
END;
/