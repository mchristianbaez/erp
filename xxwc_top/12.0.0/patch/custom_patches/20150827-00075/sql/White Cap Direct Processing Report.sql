--Report Name            : White Cap Direct Processing Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for White Cap Direct Processing Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_DIR_PRC_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_DIR_PRC_V',660,'','','','','MR020532','XXEIS','Eis Xxwc Om Dir Prc V','EXODPV','','');
--Delete View Columns for EIS_XXWC_OM_DIR_PRC_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_DIR_PRC_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_DIR_PRC_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_DIR_PRC_V','FROM_STORE',660,'From Store','FROM_STORE','','','','MR020532','VARCHAR2','','','From Store','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_DIR_PRC_V','TO_STORE',660,'To Store','TO_STORE','','','','MR020532','VARCHAR2','','','To Store','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_DIR_PRC_V','ACTUAL_SHIPMENT_DATE',660,'Actual Shipment Date','ACTUAL_SHIPMENT_DATE','','','','MR020532','DATE','','','Actual Shipment Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_DIR_PRC_V','SHIPPING_METHOD',660,'Shipping Method','SHIPPING_METHOD','','','','MR020532','VARCHAR2','','','Shipping Method','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_DIR_PRC_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','MR020532','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_DIR_PRC_V','ORDER_CHANNEL',660,'Order Channel','ORDER_CHANNEL','','','','MR020532','VARCHAR2','','','Order Channel','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_DIR_PRC_V','ITEM_DESC',660,'Item Desc','ITEM_DESC','','','','MR020532','VARCHAR2','','','Item Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_DIR_PRC_V','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','MR020532','VARCHAR2','','','Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_DIR_PRC_V','TAX_AMT',660,'Tax Amt','TAX_AMT','','','','MR020532','NUMBER','','','Tax Amt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_DIR_PRC_V','COST_AMT',660,'Cost Amt','COST_AMT','','','','MR020532','NUMBER','','','Cost Amt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_DIR_PRC_V','SALES_AMT',660,'Sales Amt','SALES_AMT','','','','MR020532','NUMBER','','','Sales Amt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_DIR_PRC_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','MR020532','NUMBER','','','Inventory Item Id','','','');
--Inserting View Components for EIS_XXWC_OM_DIR_PRC_V
--Inserting View Component Joins for EIS_XXWC_OM_DIR_PRC_V
END;
/
set scan on define on
prompt Creating Report LOV Data for White Cap Direct Processing Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - White Cap Direct Processing Report
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for White Cap Direct Processing Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - White Cap Direct Processing Report
xxeis.eis_rs_utility.delete_report_rows( 'White Cap Direct Processing Report' );
--Inserting Report - White Cap Direct Processing Report
xxeis.eis_rs_ins.r( 660,'White Cap Direct Processing Report','','White Cap Direct Processing Report','','','','MR020532','EIS_XXWC_OM_DIR_PRC_V','Y','','','MR020532','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - White Cap Direct Processing Report
xxeis.eis_rs_ins.rc( 'White Cap Direct Processing Report',660,'ACTUAL_SHIPMENT_DATE','Actual Shipment Date','Actual Shipment Date','','','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_DIR_PRC_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Direct Processing Report',660,'FROM_STORE','From Store','From Store','','','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_DIR_PRC_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Direct Processing Report',660,'ORDER_CHANNEL','Order Channel Type','Order Channel','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_DIR_PRC_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Direct Processing Report',660,'ORDER_NUMBER','Order Number','Order Number','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_DIR_PRC_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Direct Processing Report',660,'SHIPPING_METHOD','Shipping Method','Shipping Method','','','default','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_DIR_PRC_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Direct Processing Report',660,'TO_STORE','To Store','To Store','','','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_DIR_PRC_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Direct Processing Report',660,'ORDER_TOTAL','Order Total','To Store','NUMBER','~T~D~2','default','','5','Y','','','','','','','to_number(NVL(EXODPV.Sales_AMT,0) + NVL(EXODPV.TAX_AMT,0))','MR020532','N','N','','EIS_XXWC_OM_DIR_PRC_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Direct Processing Report',660,'GM','GM%','To Store','NUMBER','~T~D~2','default','','6','Y','','','','','','','case when (NVL(EXODPV.sales_amt,0) !=0  AND NVL(EXODPV.Cost_amt,0) !=0) then (((EXODPV.sales_amt-EXODPV.cost_amt)/(EXODPV.sales_amt))*100) WHEN NVL(EXODPV.cost_amt,0) =0  THEN 100 WHEN nvl(EXODPV.sales_amt,0) != 0 THEN (((EXODPV.sales_amt-EXODPV.cost_amt)/(EXODPV.sales_amt))*100) else 0 end','MR020532','N','N','','EIS_XXWC_OM_DIR_PRC_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Direct Processing Report',660,'ITEM_DESC','Item Desc','Item Desc','','','','','9','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_OM_DIR_PRC_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Direct Processing Report',660,'ITEM_NUMBER','Item Number','Item Number','','','','','10','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_OM_DIR_PRC_V','','');
--Inserting Report Parameters - White Cap Direct Processing Report
xxeis.eis_rs_ins.rp( 'White Cap Direct Processing Report',660,'Shipment Date From','Shipment Date From','ACTUAL_SHIPMENT_DATE','>=','','','DATE','N','Y','1','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap Direct Processing Report',660,'Shipment Date To','Shipment Date To','ACTUAL_SHIPMENT_DATE','<=','','','DATE','N','Y','2','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap Direct Processing Report',660,'Warehouse','Warehouse','FROM_STORE','IN','OM Warehouse All','','VARCHAR2','Y','Y','3','','N','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - White Cap Direct Processing Report
xxeis.eis_rs_ins.rcn( 'White Cap Direct Processing Report',660,'ACTUAL_SHIPMENT_DATE','>=',':Shipment Date From','','','Y','2','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'White Cap Direct Processing Report',660,'ACTUAL_SHIPMENT_DATE','<=',':Shipment Date To','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'White Cap Direct Processing Report',660,'','','','','AND ( ''All'' IN (:Warehouse) OR (FROM_STORE IN (:Warehouse)))','Y','1','','MR020532');
--Inserting Report Sorts - White Cap Direct Processing Report
xxeis.eis_rs_ins.rs( 'White Cap Direct Processing Report',660,'FROM_STORE','ASC','MR020532','','');
xxeis.eis_rs_ins.rs( 'White Cap Direct Processing Report',660,'ORDER_NUMBER','ASC','MR020532','','');
--Inserting Report Triggers - White Cap Direct Processing Report
xxeis.eis_rs_ins.rt( 'White Cap Direct Processing Report',660,'BEGIN
xxeis.eis_rs_xxwc_com_util_pkg.wcd_pre_trig (:Shipment Date From 
            , :Shipment Date To   
            , :Warehouse      );
END;','B','Y','MR020532');
--Inserting Report Templates - White Cap Direct Processing Report
--Inserting Report Portals - White Cap Direct Processing Report
--Inserting Report Dashboards - White Cap Direct Processing Report
--Inserting Report Security - White Cap Direct Processing Report
xxeis.eis_rs_ins.rsec( 'White Cap Direct Processing Report','660','','51044',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'White Cap Direct Processing Report','660','','51045',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'White Cap Direct Processing Report','660','','50901',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'White Cap Direct Processing Report','660','','51025',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'White Cap Direct Processing Report','660','','50860',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'White Cap Direct Processing Report','660','','50886',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'White Cap Direct Processing Report','660','','50859',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'White Cap Direct Processing Report','660','','50858',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'White Cap Direct Processing Report','660','','50857',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'White Cap Direct Processing Report','20005','','50900',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'White Cap Direct Processing Report','','TD002849','',660,'MR020532','','');
--Inserting Report Pivots - White Cap Direct Processing Report
END;
/
set scan on define on
