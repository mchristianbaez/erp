CREATE OR REPLACE FORCE VIEW XXEIS.EIS_XXWC_OM_DIR_PRC_V 
(ORDER_NUMBER, SALES_AMT, TAX_AMT, COST_AMT, ORDER_CHANNEL, FROM_STORE, TO_STORE,
 ACTUAL_SHIPMENT_DATE, SHIPPING_METHOD,INVENTORY_ITEM_ID,ITEM_NUMBER,ITEM_DESC
 ) AS 
  SELECT oh.order_number,
    SUM(DECODE(ol.line_category_code,'RETURN',(NVL(Ol.ordered_quantity,0)*-1),NVL(Ol.ordered_quantity,0)) * NVL(Ol.Unit_Selling_Price,0)) Sales_AMT,
    SUM(NVL(OL.TAX_VALUE,0)) Tax_AMT,
    SUM(DECODE(ol.line_category_code,'RETURN',(NVL(Ol.ordered_quantity,0)*-1),NVL(Ol.ordered_quantity,0)) * NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id) , 0)) Cost_amt,
    oh.attribute2 Order_Channel,
    ood.organization_code From_store,
    oodl.organization_code To_store,
    TRUNC(OL.ACTUAL_SHIPMENT_DATE) ACTUAL_SHIPMENT_DATE,
    FLV.MEANING SHIPPING_METHOD,
	ol.inventory_item_id inventory_item_id,  --Added by Mahender for TMS#20150827-00075 on 09/25/2015
	msi.segment1 item_number, --Added by Mahender for TMS#20150827-00075 on 09/25/2015
    msi.description item_desc --Added by Mahender for TMS#20150827-00075 on 09/25/2015
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM Org_Organization_Definitions Ood,
    xxwc.xxwc_oeh_wcd_gtt_tbl    Oh,  -- TMS# 20140228-00050
    xxwc.xxwc_oel_wcd_gtt_tbl    OL,  -- TMS# 20140228-00050
    Org_Organization_Definitions Oodl,
    FND_LOOKUP_VALUES_VL FLV,
	mtl_system_items_b msi  --Added by Mahender for TMS#20150827-00075 on 09/25/2015
  WHERE 1                  = 1
  AND ood.organization_id  = Oh.ship_from_org_id
  AND FLV.LOOKUP_TYPE      = 'SHIP_METHOD'
  AND flv.meaning like '%WCD%'
  AND flv.LOOKUP_CODE      = OL.SHIPPING_METHOD_CODE
  AND OL.HEADER_ID         = OH.HEADER_ID
  AND Oodl.organization_id = ol.Ship_From_Org_Id
  AND oh.Ship_From_Org_Id  <> ol.Ship_From_Org_Id  --Added by Mahender for TMS#20150827-00075 on 09/25/2015
  AND ol.Ship_From_Org_Id  = msi.organization_id   --Added by Mahender for TMS#20150827-00075 on 09/25/2015
  AND ol.inventory_item_id = msi.inventory_item_id  --Added by Mahender for TMS#20150827-00075 on 09/25/2015
  GROUP BY oh.order_number,
    oh.attribute2,
    ood.organization_code ,
    oodl.organization_code ,
    flv.meaning,
    TRUNC(ol.actual_shipment_date),
	ol.inventory_item_id,
	msi.segment1,
    MSI.DESCRIPTION
/