--Report Name            : Profile Options Modifications Listing - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_PROFILE_MODIFY_LIST_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_PROFILE_MODIFY_LIST_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_PROFILE_MODIFY_LIST_V',85000,'','','','','SA059956','XXEIS','Eis Xxwc Profile Modify List V','EXPMLV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_PROFILE_MODIFY_LIST_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_PROFILE_MODIFY_LIST_V',85000,FALSE);
--Inserting Object Columns for EIS_XXWC_PROFILE_MODIFY_LIST_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PROFILE_MODIFY_LIST_V','LAST_UPDATED_BY_NAME',85000,'Last Updated By Name','LAST_UPDATED_BY_NAME','','','','SA059956','VARCHAR2','','','Last Updated By Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PROFILE_MODIFY_LIST_V','LAST_UPDATED_BY',85000,'Last Updated By','LAST_UPDATED_BY','','','','SA059956','VARCHAR2','','','Last Updated By','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PROFILE_MODIFY_LIST_V','LAST_UPDATE_DATE',85000,'Last Update Date','LAST_UPDATE_DATE','','','','SA059956','DATE','','','Last Update Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PROFILE_MODIFY_LIST_V','PROFILE_OPTION_VALUE',85000,'Profile Option Value','PROFILE_OPTION_VALUE','','','','SA059956','VARCHAR2','','','Profile Option Value','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PROFILE_MODIFY_LIST_V','DETAILS',85000,'Details','DETAILS','','','','SA059956','VARCHAR2','','','Details','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PROFILE_MODIFY_LIST_V','LEVEL_VALUE',85000,'Level Value','LEVEL_VALUE','','','','SA059956','NUMBER','','','Level Value','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PROFILE_MODIFY_LIST_V','LEVEL_TYPE',85000,'Level Type','LEVEL_TYPE','','','','SA059956','VARCHAR2','','','Level Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PROFILE_MODIFY_LIST_V','USER_PROFILE_OPTION_NAME',85000,'User Profile Option Name','USER_PROFILE_OPTION_NAME','','','','SA059956','VARCHAR2','','','User Profile Option Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PROFILE_MODIFY_LIST_V','PROFILE_OPTION_NAME',85000,'Profile Option Name','PROFILE_OPTION_NAME','','','','SA059956','VARCHAR2','','','Profile Option Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PROFILE_MODIFY_LIST_V','APPLICATION_NAME',85000,'Application Name','APPLICATION_NAME','','','','SA059956','VARCHAR2','','','Application Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PROFILE_MODIFY_LIST_V','APPLICATION_SHORT_NAME',85000,'Application Short Name','APPLICATION_SHORT_NAME','','','','SA059956','VARCHAR2','','','Application Short Name','','','','US','');
--Inserting Object Components for EIS_XXWC_PROFILE_MODIFY_LIST_V
--Inserting Object Component Joins for EIS_XXWC_PROFILE_MODIFY_LIST_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report Profile Options Modifications Listing - WC
prompt Creating Report Data for Profile Options Modifications Listing - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Profile Options Modifications Listing - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Profile Options Modifications Listing - WC',85000 );
--Inserting Report - Profile Options Modifications Listing - WC
xxeis.eis_rsc_ins.r( 85000,'Profile Options Modifications Listing - WC','','Profile Options Modifications Listing - WC','','','','SA059956','EIS_XXWC_PROFILE_MODIFY_LIST_V','Y','','','SA059956','','N','WC Audit Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - Profile Options Modifications Listing - WC
xxeis.eis_rsc_ins.rc( 'Profile Options Modifications Listing - WC',85000,'APPLICATION_NAME','Application Name','Application Name','','','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PROFILE_MODIFY_LIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Profile Options Modifications Listing - WC',85000,'APPLICATION_SHORT_NAME','Application Short Name','Application Short Name','','','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PROFILE_MODIFY_LIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Profile Options Modifications Listing - WC',85000,'DETAILS','Details','Details','','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PROFILE_MODIFY_LIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Profile Options Modifications Listing - WC',85000,'LAST_UPDATE_DATE','Last Update Date','Last Update Date','','','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PROFILE_MODIFY_LIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Profile Options Modifications Listing - WC',85000,'LAST_UPDATED_BY','Last Updated By','Last Updated By','','','default','','10','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PROFILE_MODIFY_LIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Profile Options Modifications Listing - WC',85000,'LAST_UPDATED_BY_NAME','Last Updated By Name','Last Updated By Name','','','','','11','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PROFILE_MODIFY_LIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Profile Options Modifications Listing - WC',85000,'LEVEL_TYPE','Level Type','Level Type','','','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PROFILE_MODIFY_LIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Profile Options Modifications Listing - WC',85000,'LEVEL_VALUE','Level Value','Level Value','','~~~','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PROFILE_MODIFY_LIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Profile Options Modifications Listing - WC',85000,'PROFILE_OPTION_NAME','Profile Option Name','Profile Option Name','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PROFILE_MODIFY_LIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Profile Options Modifications Listing - WC',85000,'PROFILE_OPTION_VALUE','Profile Option Value','Profile Option Value','','','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PROFILE_MODIFY_LIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Profile Options Modifications Listing - WC',85000,'USER_PROFILE_OPTION_NAME','User Profile Option Name','User Profile Option Name','','','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PROFILE_MODIFY_LIST_V','','','','US','','');
--Inserting Report Parameters - Profile Options Modifications Listing - WC
--Inserting Dependent Parameters - Profile Options Modifications Listing - WC
--Inserting Report Conditions - Profile Options Modifications Listing - WC
--Inserting Report Sorts - Profile Options Modifications Listing - WC
xxeis.eis_rsc_ins.rs( 'Profile Options Modifications Listing - WC',85000,'USER_PROFILE_OPTION_NAME','ASC','SA059956','1','');
--Inserting Report Triggers - Profile Options Modifications Listing - WC
--inserting report templates - Profile Options Modifications Listing - WC
--Inserting Report Portals - Profile Options Modifications Listing - WC
--inserting report dashboards - Profile Options Modifications Listing - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Profile Options Modifications Listing - WC','85000','EIS_XXWC_PROFILE_MODIFY_LIST_V','EIS_XXWC_PROFILE_MODIFY_LIST_V','N','');
--inserting report security - Profile Options Modifications Listing - WC
xxeis.eis_rsc_ins.rsec( 'Profile Options Modifications Listing - WC','20005','','XXWC_IT_FUNC_CONFIGURATOR',85000,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Profile Options Modifications Listing - WC','20005','','XXWC_IT_OPERATIONS_ANALYST',85000,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Profile Options Modifications Listing - WC','20005','','XXWC_VIEW_ALL_EIS_REPORTS',85000,'SA059956','','','');
--Inserting Report Pivots - Profile Options Modifications Listing - WC
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Profile Options Modifications Listing - WC
xxeis.eis_rsc_ins.rv( 'Profile Options Modifications Listing - WC','','Profile Options Modifications Listing - WC','SA059956','08-AUG-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
