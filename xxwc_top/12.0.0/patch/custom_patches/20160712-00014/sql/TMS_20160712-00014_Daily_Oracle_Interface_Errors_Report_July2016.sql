/*************************************************************************
  $Header TMS_20160712-00014_Daily_Oracle_Interface_Errors_Report_July2016.sql.sql $
  Module Name: TMS_20160712-00014  Data Fix script  

  PURPOSE: Data Fix script 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        26-JUL-2016  Pattabhi Avula        TMS#20160712-00014

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160712-00014    , Before Update');

UPDATE apps.wsh_delivery_details
SET released_status = 'D',
src_requested_quantity = 0,
requested_quantity = 0,
shipped_quantity = 0,
cycle_count_quantity = 0,
cancelled_quantity = 0,
subinventory = null,
locator_id = null,
lot_number = null,
revision = null,
inv_interfaced_flag = 'X',
oe_interfaced_flag = 'X'
WHERE delivery_detail_id  in (16615420,16663771,16663772,16868323,16869038,16869039,16849492,16865891,16863104,16885561,16906567,16863104,16885561,16996366);

--14 row expected to be updated

update apps.wsh_delivery_assignments
set delivery_id = null,
parent_delivery_detail_id = null
where delivery_detail_id in (16615420,16663771,16663772,16868323,16869038,16869039,16849492,16865891,16863104,16885561,16906567,16863104,16885561,16996366);


   DBMS_OUTPUT.put_line (
         'TMS: 20160621-00014  Sales order lines updated (Expected:14): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160712-00014    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160712-00014 , Errors : ' || SQLERRM);
END;
/