---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_INV_WEBADI_UPLD_ERR_V $
  Module Name : INVENTORY
  PURPOSE	  : Upload Error Report-WC
  TMS Task Id : 20160419-00307   
  REVISIONS   :
  VERSION 	DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     26-May-2016        Siva   		 TMS#20160419-00307  
**************************************************************************************************************/
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_INV_WEBADI_UPLD_ERR_V (INVENTORY_ITEM_ID, ITEM_NUMBER, DESCRIPTION, REQUEST_ID, ORGANIZATION_ID, ORGANIZATION_CODE, ERROR_MESSAGE)
AS
  SELECT MSII.inventory_item_id,
    MSII.segment1 item_number,
    MSII.description,
    MIE.request_id,
    MIE.organization_id,
    MP.organization_code,
    MIE.error_message
  FROM apps.mtl_interface_errors MIE,
    apps.mtl_system_items_interface MSII,
    apps.mtl_parameters MP
  WHERE MIE.request_id    = MSII.request_id
  AND MIE.organization_id = MSII.organization_id
  AND MIE.ORGANIZATION_ID = MP.ORGANIZATION_ID
/
