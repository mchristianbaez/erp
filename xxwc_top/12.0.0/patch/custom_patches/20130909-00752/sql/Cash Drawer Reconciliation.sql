--Report Name            : Cash Drawer Reconciliation
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_XXWC_AR_CASH_DRWR_RECON_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_AR_CASH_DRWR_RECON_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_AR_CASH_DRWR_RECON_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Cash Drwr Recon V','EXACDRV','','Y','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_AR_CASH_DRWR_RECON_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_AR_CASH_DRWR_RECON_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_AR_CASH_DRWR_RECON_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CASH_DATE',660,'Cash Date','CASH_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Cash Date','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','INVOICE_DATE',660,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','INVOICE_NUMBER',660,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','BRANCH_NUMBER',660,'Branch Number','BRANCH_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Number','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CASH_AMOUNT',660,'Cash Amount','CASH_AMOUNT','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Cash Amount','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CARD_ISSUER_NAME',660,'Card Issuer Name','CARD_ISSUER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Issuer Name','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CASH_TYPE',660,'Cash Type','CASH_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cash Type','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PAYMENT_TYPE_CODE',660,'Payment Type Code','PAYMENT_TYPE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type Code','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','TAKEN_BY',660,'Taken By','TAKEN_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Taken By','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','SEGMENT_NUMBER',660,'Segment Number','SEGMENT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment Number','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CHK_CARDNO',660,'Chk Cardno','CHK_CARDNO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Chk Cardno','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','ORDER_AMOUNT',660,'Order Amount','ORDER_AMOUNT','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Order Amount','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','NAME',660,'Name','NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Name','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','ORDER_DATE',660,'Order Date','ORDER_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Order Date','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','ON_ACCOUNT',660,'On Account','ON_ACCOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','On Account','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PAYMENT_CHANNEL_NAME',660,'Payment Channel Name','PAYMENT_CHANNEL_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Channel Name','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CHECK_NUMBER',660,'Check Number','CHECK_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Check Number','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CARD_ISSUER_CODE',660,'Card Issuer Code','CARD_ISSUER_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Issuer Code','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CARD_NUMBER',660,'Card Number','CARD_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Number','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','ORG_ID',660,'Org Id','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PAYMENT_NUMBER',660,'Payment Number','PAYMENT_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Number','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CASH_RECEIPT_ID',660,'Cash Receipt Id','CASH_RECEIPT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cash Receipt Id','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PAYMENT_AMOUNT',660,'Payment Amount','PAYMENT_AMOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Amount','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PAYMENT_TYPE_CODE_NEW',660,'Payment Type Code New','PAYMENT_TYPE_CODE_NEW','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type Code New','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Type','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','TOTAL_ORDER_AMOUNT',660,'Total Order Amount','TOTAL_ORDER_AMOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Total Order Amount','','','Y','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','LAST_UPDATE_DATE',660,'Last Update Date','LAST_UPDATE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Last Update Date','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','LINE_LAST_UPDATE_DATE',660,'Line Last Update Date','LINE_LAST_UPDATE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Line Last Update Date','','','','');
--Inserting Object Components for EIS_XXWC_AR_CASH_DRWR_RECON_V
--Inserting Object Component Joins for EIS_XXWC_AR_CASH_DRWR_RECON_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Cash Drawer Reconciliation
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Cash Drawer Reconciliation
xxeis.eis_rsc_ins.lov( 660,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Cash Drawer Reconciliation
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Cash Drawer Reconciliation
xxeis.eis_rsc_utility.delete_report_rows( 'Cash Drawer Reconciliation' );
--Inserting Report - Cash Drawer Reconciliation
xxeis.eis_rsc_ins.r( 660,'Cash Drawer Reconciliation','','This report provides a total listing of all Cash, Check and Credit Card Sales by Branch (Customer, Taken By, Order Number, Cash Date, Cash Amount, Cash Type, Invoice Number, Invoice Date, Payment Type, Check/Credit Card No.)','','','','SA059956','EIS_XXWC_AR_CASH_DRWR_RECON_V','Y','','','SA059956','','Y','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N','','','','','','','APPS','US','','','','');
--Inserting Report Columns - Cash Drawer Reconciliation
xxeis.eis_rsc_ins.rc( 'Cash Drawer Reconciliation',660,'BRANCH_NUMBER','Branch Number','Branch Number','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Reconciliation',660,'CASH_AMOUNT','Cash Amount','Cash Amount','','~T~D~2','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Reconciliation',660,'CASH_DATE','Cash Date','Cash Date','','','default','','7','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Reconciliation',660,'CASH_TYPE','Cash Type','Cash Type','','','default','','15','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Reconciliation',660,'CHK_CARDNO','Check Card No','Chk Cardno','','','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Reconciliation',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Reconciliation',660,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','17','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Reconciliation',660,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','16','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Reconciliation',660,'NAME','Receipt Method','Name','','','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Reconciliation',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Reconciliation',660,'PAYMENT_TYPE_CODE','Payment Type','Payment Type Code','','','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Reconciliation',660,'SEGMENT_NUMBER','Segment Number','Segment Number','','','default','','18','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Reconciliation',660,'TAKEN_BY','Created By','Taken By','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Reconciliation',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Reconciliation',660,'CARD_ISSUER_NAME','Card Type','Card Issuer Name','','','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Reconciliation',660,'DIFFERENCE','DIFFERENCE','Card Issuer Name','NUMBER','~T~D~2','default','','10','Y','Y','','','','','','(CASE WHEN exacdrv.cash_type = ''CASH RETURN'' THEN ((exacdrv.total_order_amount + NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_order_freight(exacdrv.header_id),0))-cash_amount) ELSE (exacdrv.payment_amount-(exacdrv.total_order_amount + NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_order_freight(exacdrv.header_id),0))) END)','SA059956','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Reconciliation',660,'ORDER_DATE','Order Date','Order Date','','','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Reconciliation',660,'ON_ACCOUNT','On Account','On Account','','~~~','default','','19','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Cash Drawer Reconciliation',660,'ORDR_AMOUNT','Order Amount','Order Amount Calculation','NUMBER','~T~D~2','default','','8','Y','Y','','','','','','(CASE WHEN nvl(EXACDRV.order_amount,0) != 0 THEN (NVL(EXACDRV.order_amount,0) + NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_order_freight(EXACDRV.header_id),0)) ELSE 0 END)','SA059956','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','US','');
--Inserting Report Parameters - Cash Drawer Reconciliation
xxeis.eis_rsc_ins.rp( 'Cash Drawer Reconciliation',660,'Location','Branch Number','BRANCH_NUMBER','IN','AR Organizaion Code LOV','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cash Drawer Reconciliation',660,'From Date','From Date','','>=','','','DATE','Y','Y','2','Y','N','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cash Drawer Reconciliation',660,'To Date','To Date','','<=','','','DATE','Y','Y','3','Y','N','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','US','');
--Inserting Dependent Parameters - Cash Drawer Reconciliation
--Inserting Report Conditions - Cash Drawer Reconciliation
xxeis.eis_rsc_ins.rcnh( 'Cash Drawer Reconciliation',660,'BRANCH_NUMBER IN :Location ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BRANCH_NUMBER','','Location','','','','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','','','','','IN','Y','Y','','','','','1',660,'Cash Drawer Reconciliation','BRANCH_NUMBER IN :Location ');
xxeis.eis_rsc_ins.rcnh( 'Cash Drawer Reconciliation',660,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND ( (TRUNC(EXACDRV.ORDER_DATE) >= :From Date and TRUNC(EXACDRV.ORDER_DATE) <= :To Date )
OR (TRUNC(EXACDRV.CASH_DATE)   >= :From Date and TRUNC(EXACDRV.CASH_DATE) <= :To Date)
OR (TRUNC(EXACDRV.LINE_LAST_UPDATE_DATE)   >= :From Date and TRUNC(EXACDRV.LINE_LAST_UPDATE_DATE) <= :To Date)
OR ( EXACDRV.Order_type = ''STANDARD ORDER''
 AND ((nvl(EXACDRV.TOTAL_ORDER_AMOUNT,0) + NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_order_freight(EXACDRV.header_id),0)) - nvl(EXACDRV.payment_amount,0)) >0  
 AND Exists (SELECT 1  FROM OE_ORDER_LINES OL
        WHERE OL.HEADER_ID  =EXACDRV.HEADER_ID
        AND TRUNC(OL.ACTUAL_SHIPMENT_DATE) BETWEEN :From Date and :To Date )))
and  EXACDRV.CASH_AMOUNT is not null','1',660,'Cash Drawer Reconciliation','Free Text ');
--Inserting Report Sorts - Cash Drawer Reconciliation
xxeis.eis_rsc_ins.rs( 'Cash Drawer Reconciliation',660,'ORDER_NUMBER','ASC','SA059956','1','');
--Inserting Report Triggers - Cash Drawer Reconciliation
xxeis.eis_rsc_ins.rt( 'Cash Drawer Reconciliation',660,'declare
begin
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.G_CASH_FROM_DATE:= :From Date;
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.G_CASH_TO_DATE:= :To Date;
end;','B','Y','SA059956','AQ');
--inserting report templates - Cash Drawer Reconciliation
xxeis.eis_rsc_ins.r_tem( 'Cash Drawer Reconciliation','Cash Drawer Reconciliation','Cash Drawer Reconciliation','','','','','','','','','','','','SA059956','X','','','','','','');
--Inserting Report Portals - Cash Drawer Reconciliation
--inserting report dashboards - Cash Drawer Reconciliation
xxeis.eis_rsc_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 755','Dynamic 755','point','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','SA059956');
xxeis.eis_rsc_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 752','Dynamic 752','pie','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','SA059956');
xxeis.eis_rsc_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 753','Dynamic 753','vertical stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','SA059956');
xxeis.eis_rsc_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 759','Dynamic 759','horizontal stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','SA059956');
xxeis.eis_rsc_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 758','Dynamic 758','vertical stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','SA059956');
xxeis.eis_rsc_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 756','Dynamic 756','stacked line','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','SA059956');
xxeis.eis_rsc_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 751','Dynamic 751','pie','large','Cash Amount','Cash Amount','Customer Name','Customer Name','Count','SA059956');
xxeis.eis_rsc_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 754','Dynamic 754','absolute line','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','SA059956');
xxeis.eis_rsc_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 757','Dynamic 757','scatter','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','SA059956');
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Cash Drawer Reconciliation','660','EIS_XXWC_AR_CASH_DRWR_RECON_V','EIS_XXWC_AR_CASH_DRWR_RECON_V','N','');
--inserting report security - Cash Drawer Reconciliation
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','','LB048272','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','101','','GENERAL_LEDGER_SUPER_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','101','','GENERAL_LEDGER_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','101','','XXCUS_GL_ACCOUNTANT_CAD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','101','','GNRL_LDGR_LTMR_ACCNTNT',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','101','','XXCUS_GL_ACCOUNTANT_USD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','101','','XXCUS_GL_INQUIRY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','101','','HDS GL INQUIRY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','101','','GNRL_LDGR_LTMR_NQR',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','101','','XXCUS_GL_MANAGER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','101','','HDS_CAD_MNTH_END_PROCS',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','101','','GNRL_LDGR_FSS',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','101','','GNRL_LDGR_LTMR_FSS',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','701','','CLN_OM_3A6_ADMINISTRATOR',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','660','','ORDER_MGMT_SUPER_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','401','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','660','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','401','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','20005','','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Cash Drawer Reconciliation','660','','XXWC_ORDER_MGMT_SUPER_USER',660,'SA059956','','','');
--Inserting Report Pivots - Cash Drawer Reconciliation
xxeis.eis_rsc_ins.rpivot( 'Cash Drawer Reconciliation',660,'Cash By Payment Type','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Cash By Payment Type
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment Type','CASH_TYPE','PAGE_FIELD','','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment Type','CUSTOMER_NUMBER','PAGE_FIELD','','','2','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment Type','CUSTOMER_NAME','PAGE_FIELD','','','3','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment Type','NAME','PAGE_FIELD','','','4','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment Type','SEGMENT_NUMBER','PAGE_FIELD','','','5','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment Type','CASH_AMOUNT','DATA_FIELD','SUM','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment Type','BRANCH_NUMBER','ROW_FIELD','','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment Type','PAYMENT_TYPE_CODE','ROW_FIELD','','','2','','');
--Inserting Report Summary Calculation Columns For Pivot- Cash By Payment Type
xxeis.eis_rsc_ins.rpivot( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','2','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Cash By Payment and Card Type
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','CASH_TYPE','PAGE_FIELD','','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','CUSTOMER_NUMBER','PAGE_FIELD','','','2','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','CUSTOMER_NAME','PAGE_FIELD','','','3','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','SEGMENT_NUMBER','PAGE_FIELD','','','4','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','CASH_AMOUNT','DATA_FIELD','SUM','','1','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','BRANCH_NUMBER','ROW_FIELD','','','1','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','PAYMENT_TYPE_CODE','ROW_FIELD','','','2','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','CARD_ISSUER_NAME','PAGE_FIELD','','','5','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','NAME','ROW_FIELD','','','3','','');
--Inserting Report Summary Calculation Columns For Pivot- Cash By Payment and Card Type
--Inserting Report   Version details- Cash Drawer Reconciliation
xxeis.eis_rsc_ins.rv( 'Cash Drawer Reconciliation','','Cash Drawer Reconciliation','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
