CREATE OR REPLACE PACKAGE BODY APPS.XXWC_RA_INTERFACE_FIXER_PKG
IS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_RA_INTERFACE_FIXER_PKG.pkb $
   *   Module Name: XXWC_RA_INTERFACE_FIXER_PKG.pkb
   *
   *   PURPOSE:   This package is a fixer package. And used to fix ra_interface_lines_error ERROR after Auto invoice.
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        04/20/2016  Neha Saini              Initial Version
                                                     TMS# 20150106-00014 AR - fixing interface lines errors before Auto invoice.
   *   1.1        07/1/2016   Neha Saini             TMS#Task ID: 20160701-00028  changing error message text.
   * **********************************************************************************************************************************/

   /*****************************
   -- GLOBAL VARIABLES
   ******************************/

   g_error_message   VARCHAR2 (32000);
   g_location        VARCHAR2 (5000);

   /*************************************************************************
   *   Procedure : LOG_MSG
   *   PURPOSE:   This procedure is the table handler called by the On-Insert Trigger
   *   Parameter:
   *          IN
   *              p_debug_level    -- Debug Level
   *              p_mod_name       -- Module Name
   *              p_debug_msg      -- Debug Message
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        05/03/2016  Neha Saini             Initial Version
   * ************************************************************************/
   -- add debug message to log table and concurrent log file
   PROCEDURE LOG_MSG (p_debug_level   IN NUMBER,
                      p_mod_name      IN VARCHAR2,
                      p_debug_msg     IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      FND_LOG.STRING (p_debug_level, p_mod_name, P_DEBUG_MSG);
      COMMIT;
   END LOG_MSG;

   /*************************************************************************
   Procedure : Write_Error
   PURPOSE:   This procedure logs error message
   Parameter:
         IN p_debug_msg      -- Debug Message
   REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        05/03/2016  Neha Saini             Initial Version
   ************************************************************************/

   --add message to concurrent output file
   PROCEDURE write_error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_RA_INTERFACE_FIXER_PKG';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running XXWC_RA_INTERFACE_FIXER_PKG with PROGRAM ERROR',
         p_distribution_list   => l_distro_list,
         p_module              => 'GL');
   END write_error;

   /*************************************************************************
   *   Procedure : Write_output
   *   PURPOSE:   This procedure is the table handler called by the On-Insert Trigger
   *   Parameter:
   *          IN
   *              p_debug_msg      -- Debug Message
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        05/03/2016  Neha Saini             Initial Version
   * ************************************************************************/

   --add message to concurrent output file
   PROCEDURE Write_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.output, p_debug_msg);
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      DBMS_OUTPUT.PUT_LINE (p_debug_msg);
   END Write_output;

   /*************************************************************************
     *   Function : get_column_value
     *   PURPOSE:   This procedure is to get the colkumn value where it is null
     *              for that order and trx combination.
     *   Parameter:
     *          IN
     *              p_column_name      -- column name
     *              p_order_number     -- order number
     *              p_trx_number       -- trx number
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
     *   1.0        05/03/2016  Neha Saini             Initial Version
     * ************************************************************************/

   FUNCTION get_column_value (p_column_name    IN VARCHAR2,
                              p_order_number   IN VARCHAR2,
                              p_trx_number     IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_column_value   VARCHAR2 (2000 BYTE);
      l_statement      VARCHAR2 (32000 BYTE);
   BEGIN
      g_location := 'starting get column value';
      write_output (g_location);

      l_statement :=
            ' SELECT '
         || p_column_name
         || ' FROM apps.ra_interface_lines_all 
             WHERE     INTERFACE_LINE_ATTRIBUTE1 = '''
         || p_order_number
         || ''' AND trx_number = '''
         || p_trx_number
         || ''' AND '
         || p_column_name
         || ' IS NOT NULL AND ROWNUM = 1 ';

      EXECUTE IMMEDIATE l_statement INTO l_column_value;

      g_location :=
            'ending get column value by returning value for '
         || p_column_name
         || ' : '
         || l_column_value;
      write_output (g_location);
      RETURN l_column_value;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_column_value;

       /*************************************************************************
       *   PROCEDURE : update_status
       *   PURPOSE:   This procedure is to update status=Y in temp table
       *   Parameter:
       *          IN  p_order_number     -- order number
       *              p_trx_number       -- trx number
       *   REVISIONS:
       *   Ver        Date        Author                     Description
       *    ---------  ----------  ---------------         -------------------------
       *   1.0        05/03/2016  Neha Saini             Initial Version
       * ************************************************************************/

   PROCEDURE update_status (p_order_number   IN VARCHAR2,
                            p_trx_number     IN VARCHAR2)
   IS
   BEGIN
      g_location := 'starting update_status';

      write_output (g_location);

      UPDATE XXWC.XXWC_RA_INTEFACE_LINES_TEMP
         SET status = 'Y'
       WHERE     INTERFACE_LINE_ATTRIBUTE1 = p_order_number
             AND trx_number = p_trx_number;

      g_location :=
            ' UPDATED '
         || SQL%ROWCOUNT
         || ' status = Y '
         || p_order_number
         || ' trx_number '
         || p_trx_number;

      write_output (g_location);
      COMMIT;
      write_output ('commit');
      g_location := 'ending update status  ';
      write_output (g_location);
   EXCEPTION
      WHEN OTHERS
      THEN
         g_location :=
               'ending in exception of update_status by returning value for status'
            || SQLERRM;
         write_output (g_location);
   END update_status;

   /**************************************************************************
   *   procedure Name: check_invoice_errors
   *
   *   PURPOSE:   This procedure is used to fix ra_interface_lines_error ERROR after Auto invoice.
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        05/03/2016  Neha Saini             Initial Version
   * ***************************************************************************/
   PROCEDURE check_invoice_errors (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      l_mod_name         VARCHAR2 (100)
                            := 'XXWC_RA_INTERFACE_FIXER_PKG.check_invoice_errors';
      ra_interface_rec   apps.ra_interface_lines_all%ROWTYPE;

      CURSOR C1
      IS
         SELECT * FROM XXWC.XXWC_RA_INTEFACE_LINES_TEMP;
   BEGIN
      g_error_message := NULL;
      g_location := 'Checking invoice error procedure started';
      write_output (g_location);

      LOG_MSG (g_LEVEL_STATEMENT,
               l_mod_name,
               'Checking invoice error procedsure started');

      g_location := 'Deleting data from temp table';
      write_output (g_location);

      DELETE FROM XXWC.XXWC_RA_INTEFACE_LINES_TEMP;

      COMMIT;

      g_location := 'Inserting data for error1 into temp table';
      write_output (g_location);

      INSERT INTO XXWC.XXWC_RA_INTEFACE_LINES_TEMP
         (SELECT INTERFACE_LINE_ID,
                 INTERFACE_LINE_ATTRIBUTE1,
                 TRX_NUMBER,
                 REASON_CODE,
                 PAYMENT_SET_ID,
                 TRX_DATE,
                 PRIMARY_SALESREP_ID,
                 ORIG_SYSTEM_BILL_ADDRESS_ID,
                 ORIG_SYSTEM_BILL_CONTACT_ID,
                 ORIG_SYSTEM_BILL_CUSTOMER_ID,
                 ORIG_SYSTEM_SOLD_CUSTOMER_ID,
                 ORIG_SYSTEM_SHIP_CONTACT_ID,
                 ORIG_SYSTEM_SHIP_ADDRESS_ID,
                 ORIG_SYSTEM_SHIP_CUSTOMER_ID,
                 '1',
                 SYSDATE,
                 FND_GLOBAL.USER_ID,
                 'N'
            FROM apps.ra_interface_lines_all
           WHERE interface_line_id IN (SELECT INTERFACE_LINE_ID
                                         FROM apps.RA_INTERFACE_ERRORS_ALL
                                        WHERE MESSAGE_TEXT LIKE
                                                 'You must supply the amount for this transaction'));

      COMMIT;

      g_location := 'Inserting data for Error 2 into temp table';
      write_output (g_location);

      INSERT INTO XXWC.XXWC_RA_INTEFACE_LINES_TEMP
         (SELECT INTERFACE_LINE_ID,
                 INTERFACE_LINE_ATTRIBUTE1,
                 TRX_NUMBER,
                 REASON_CODE,
                 PAYMENT_SET_ID,
                 TRX_DATE,
                 PRIMARY_SALESREP_ID,
                 ORIG_SYSTEM_BILL_ADDRESS_ID,
                 ORIG_SYSTEM_BILL_CONTACT_ID,
                 ORIG_SYSTEM_BILL_CUSTOMER_ID,
                 ORIG_SYSTEM_SOLD_CUSTOMER_ID,
                 ORIG_SYSTEM_SHIP_CONTACT_ID,
                 ORIG_SYSTEM_SHIP_ADDRESS_ID,
                 ORIG_SYSTEM_SHIP_CUSTOMER_ID,
                 '2',
                 SYSDATE,
                 FND_GLOBAL.USER_ID,
                 'N'
            FROM apps.ra_interface_lines_all
           WHERE interface_line_id IN (SELECT INTERFACE_LINE_ID
                                         FROM apps.RA_INTERFACE_ERRORS_ALL
                                        WHERE        1 = 1
                                                 AND MESSAGE_TEXT LIKE
                                                        'Your credit memo transaction can only credit an invoice or a debit memo%'
                                              OR MESSAGE_TEXT LIKE
                                                    '%exception occured in Tax Accounting routine during Adjustment/Application Accounting%'));--changed for ver1.1

      COMMIT;

      g_location := 'Inserting data for Error 3 into temp table';
      write_output (g_location);

      INSERT INTO XXWC.XXWC_RA_INTEFACE_LINES_TEMP
         (SELECT INTERFACE_LINE_ID,
                 INTERFACE_LINE_ATTRIBUTE1,
                 TRX_NUMBER,
                 REASON_CODE,
                 PAYMENT_SET_ID,
                 TRX_DATE,
                 PRIMARY_SALESREP_ID,
                 ORIG_SYSTEM_BILL_ADDRESS_ID,
                 ORIG_SYSTEM_BILL_CONTACT_ID,
                 ORIG_SYSTEM_BILL_CUSTOMER_ID,
                 ORIG_SYSTEM_SOLD_CUSTOMER_ID,
                 ORIG_SYSTEM_SHIP_CONTACT_ID,
                 ORIG_SYSTEM_SHIP_ADDRESS_ID,
                 ORIG_SYSTEM_SHIP_CUSTOMER_ID,
                 '3',
                 SYSDATE,
                 FND_GLOBAL.USER_ID,
                 'N'
            FROM apps.ra_interface_lines_all
           WHERE interface_line_id IN (SELECT INTERFACE_LINE_ID
                                         FROM apps.RA_INTERFACE_ERRORS_ALL
                                        WHERE     1 = 1
                                              AND MESSAGE_TEXT LIKE
                                                     'Duplicate invoice number'));

      COMMIT;

      g_location :=
         'UPDATE temp table for  error 1 in temp table to update ra_interface';

      write_output (g_location);

      UPDATE apps.ra_interface_lines_all
         SET AMOUNT = 0,
             UNIT_STANDARD_PRICE = 0,
             UNIT_SELLING_PRICE = 0,
             last_update_date = SYSDATE,
             last_updated_by = FND_GLOBAL.USER_ID
       WHERE     1 = 1
             AND interface_line_id IN (SELECT interface_line_id
                                         FROM XXWC.XXWC_RA_INTEFACE_LINES_TEMP
                                        WHERE ERROR = 1);

      g_location :=
         ' UPDATED ' || SQL%ROWCOUNT || ' for error 1 in ra_interface_lines ';
      write_output (g_location);
      COMMIT;
      g_location :=
         ' UPDATE temp table for error 2 to update ra interface lines ';
      write_output (g_location);

      UPDATE apps.ra_interface_lines_all
         SET REFERENCE_LINE_ID = NULL,
             last_update_date = SYSDATE,
             last_updated_by = FND_GLOBAL.USER_ID
       WHERE     1 = 1
             AND interface_line_id IN (SELECT INTERFACE_LINE_ID
                                         FROM XXWC.XXWC_RA_INTEFACE_LINES_TEMP
                                        WHERE ERROR = 2);

      g_location :=
         ' UPDATED ' || SQL%ROWCOUNT || ' for error 2 in ra_interface_lines ';
      write_output (g_location);

      COMMIT;
      /*******************ERROR 3 FIXING LOGIC STARTS**********************************************/

      g_location :=
         'UPDATE temp table for error 3 to update ra interface lines ';
      write_output (g_location);

      FOR rec IN (SELECT DISTINCT INTERFACE_LINE_ATTRIBUTE1, TRX_NUMBER
                    FROM XXWC.XXWC_RA_INTEFACE_LINES_TEMP
                   WHERE error = 3)
      LOOP
         ra_interface_rec.AGREEMENT_ID :=
            get_column_value ('AGREEMENT_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.APPLICATION_ID :=
            get_column_value ('APPLICATION_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.BILLING_DATE :=
            get_column_value ('BILLING_DATE',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.COMMENTS :=
            get_column_value ('COMMENTS',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.CONS_BILLING_NUMBER :=
            get_column_value ('CONS_BILLING_NUMBER',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.CONTRACT_ID :=
            get_column_value ('CONTRACT_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.CONVERSION_DATE :=
            get_column_value ('CONVERSION_DATE',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.CONVERSION_RATE :=
            get_column_value ('CONVERSION_RATE',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.CONVERSION_TYPE :=
            get_column_value ('CONVERSION_TYPE',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.CREDIT_METHOD_FOR_ACCT_RULE :=
            get_column_value ('CREDIT_METHOD_FOR_ACCT_RULE',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.CREDIT_METHOD_FOR_INSTALLMENTS :=
            get_column_value ('CREDIT_METHOD_FOR_INSTALLMENTS',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.CURRENCY_CODE :=
            get_column_value ('CURRENCY_CODE',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.CUSTOMER_BANK_ACCOUNT_ID :=
            get_column_value ('CUSTOMER_BANK_ACCOUNT_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.DEFAULT_TAXATION_COUNTRY :=
            get_column_value ('DEFAULT_TAXATION_COUNTRY',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.DOCUMENT_NUMBER :=
            get_column_value ('DOCUMENT_NUMBER',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.DOCUMENT_NUMBER_SEQUENCE_ID :=
            get_column_value ('DOCUMENT_NUMBER_SEQUENCE_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.DOCUMENT_SUB_TYPE :=
            get_column_value ('DOCUMENT_SUB_TYPE',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.GL_DATE :=
            get_column_value ('GL_DATE',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_ATTRIBUTE1 :=
            get_column_value ('HEADER_ATTRIBUTE1',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_ATTRIBUTE2 :=
            get_column_value ('HEADER_ATTRIBUTE2',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_ATTRIBUTE3 :=
            get_column_value ('HEADER_ATTRIBUTE3',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_ATTRIBUTE4 :=
            get_column_value ('HEADER_ATTRIBUTE4',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_ATTRIBUTE5 :=
            get_column_value ('HEADER_ATTRIBUTE5',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_ATTRIBUTE6 :=
            get_column_value ('HEADER_ATTRIBUTE6',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_ATTRIBUTE7 :=
            get_column_value ('HEADER_ATTRIBUTE7',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_ATTRIBUTE8 :=
            get_column_value ('HEADER_ATTRIBUTE8',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_ATTRIBUTE9 :=
            get_column_value ('HEADER_ATTRIBUTE9',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_ATTRIBUTE10 :=
            get_column_value ('HEADER_ATTRIBUTE10',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_ATTRIBUTE11 :=
            get_column_value ('HEADER_ATTRIBUTE11',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_ATTRIBUTE12 :=
            get_column_value ('HEADER_ATTRIBUTE12',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_ATTRIBUTE13 :=
            get_column_value ('HEADER_ATTRIBUTE13',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_ATTRIBUTE14 :=
            get_column_value ('HEADER_ATTRIBUTE14',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_ATTRIBUTE15 :=
            get_column_value ('HEADER_ATTRIBUTE15',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_ATTRIBUTE_CATEGORY :=
            get_column_value ('HEADER_ATTRIBUTE_CATEGORY',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE1 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE1',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE2 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE2',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE3 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE3',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE4 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE4',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE5 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE5',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE6 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE6',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE7 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE7',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE8 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE8',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE9 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE9',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE10 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE10',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE11 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE11',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE12 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE12',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE13 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE13',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE14 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE14',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE15 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE15',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE16 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE16',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE17 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE17',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE18 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE18',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE19 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE19',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE20 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE20',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE21 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE21',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE22 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE22',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE23 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE23',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE24 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE24',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE25 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE25',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE26 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE26',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE27 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE27',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE28 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE28',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE29 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE29',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTRIBUTE30 :=
            get_column_value ('HEADER_GDF_ATTRIBUTE30',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.HEADER_GDF_ATTR_CATEGORY :=
            get_column_value ('HEADER_GDF_ATTR_CATEGORY',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.INITIAL_CUSTOMER_TRX_ID :=
            get_column_value ('INITIAL_CUSTOMER_TRX_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.INTERNAL_NOTES :=
            get_column_value ('INTERNAL_NOTES',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.INVOICING_RULE_ID :=
            get_column_value ('INVOICING_RULE_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LEGAL_ENTITY_ID :=
            get_column_value ('LEGAL_ENTITY_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ORIG_SYSTEM_BILL_ADDRESS_ID :=
            get_column_value ('ORIG_SYSTEM_BILL_ADDRESS_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ORIG_SYSTEM_BILL_CONTACT_ID :=
            get_column_value ('ORIG_SYSTEM_BILL_CONTACT_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ORIG_SYSTEM_BILL_CUSTOMER_ID :=
            get_column_value ('ORIG_SYSTEM_BILL_CUSTOMER_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ORIG_SYSTEM_SOLD_CUSTOMER_ID :=
            get_column_value ('ORIG_SYSTEM_SOLD_CUSTOMER_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ORIG_SYSTEM_SHIP_ADDRESS_ID :=
            get_column_value ('ORIG_SYSTEM_SHIP_ADDRESS_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ORIG_SYSTEM_SHIP_CONTACT_ID :=
            get_column_value ('ORIG_SYSTEM_SHIP_CONTACT_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ORIG_SYSTEM_SHIP_CUSTOMER_ID :=
            get_column_value ('ORIG_SYSTEM_SHIP_CUSTOMER_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.PAYMENT_ATTRIBUTES :=
            get_column_value ('PAYMENT_ATTRIBUTES',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ORIG_SYSTEM_BATCH_NAME :=
            get_column_value ('ORIG_SYSTEM_BATCH_NAME',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.PAYMENT_SET_ID :=
            get_column_value ('PAYMENT_SET_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.PREVIOUS_CUSTOMER_TRX_ID :=
            get_column_value ('PREVIOUS_CUSTOMER_TRX_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.PRIMARY_SALESREP_ID :=
            get_column_value ('PRIMARY_SALESREP_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.PRINTING_OPTION :=
            get_column_value ('PRINTING_OPTION',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.PURCHASE_ORDER :=
            get_column_value ('PURCHASE_ORDER',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.PURCHASE_ORDER_DATE :=
            get_column_value ('PURCHASE_ORDER_DATE',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.PURCHASE_ORDER_REVISION :=
            get_column_value ('PURCHASE_ORDER_REVISION',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.REASON_CODE :=
            get_column_value ('REASON_CODE',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.RECEIPT_METHOD_ID :=
            get_column_value ('RECEIPT_METHOD_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.RELATED_CUSTOMER_TRX_ID :=
            get_column_value ('RELATED_CUSTOMER_TRX_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.SET_OF_BOOKS_ID :=
            get_column_value ('SET_OF_BOOKS_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.TERM_ID :=
            get_column_value ('TERM_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.TERRITORY_ID :=
            get_column_value ('TERRITORY_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.TRX_DATE :=
            get_column_value ('TRX_DATE',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ACCOUNTING_RULE_DURATION :=
            get_column_value ('ACCOUNTING_RULE_DURATION',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ACCOUNTING_RULE_ID :=
            get_column_value ('ACCOUNTING_RULE_ID',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ATTRIBUTE1 :=
            get_column_value ('ATTRIBUTE1',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ATTRIBUTE2 :=
            get_column_value ('ATTRIBUTE2',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ATTRIBUTE3 :=
            get_column_value ('ATTRIBUTE3',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ATTRIBUTE4 :=
            get_column_value ('ATTRIBUTE4',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ATTRIBUTE5 :=
            get_column_value ('ATTRIBUTE5',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ATTRIBUTE6 :=
            get_column_value ('ATTRIBUTE6',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ATTRIBUTE7 :=
            get_column_value ('ATTRIBUTE7',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ATTRIBUTE8 :=
            get_column_value ('ATTRIBUTE8',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ATTRIBUTE9 :=
            get_column_value ('ATTRIBUTE9',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ATTRIBUTE10 :=
            get_column_value ('ATTRIBUTE10',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ATTRIBUTE11 :=
            get_column_value ('ATTRIBUTE11',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ATTRIBUTE12 :=
            get_column_value ('ATTRIBUTE12',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ATTRIBUTE13 :=
            get_column_value ('ATTRIBUTE13',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ATTRIBUTE14 :=
            get_column_value ('ATTRIBUTE14',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ATTRIBUTE15 :=
            get_column_value ('ATTRIBUTE15',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.ATTRIBUTE_CATEGORY :=
            get_column_value ('ATTRIBUTE_CATEGORY',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.INTERFACE_LINE_CONTEXT :=
            get_column_value ('INTERFACE_LINE_CONTEXT',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE1 :=
            get_column_value ('LINE_GDF_ATTRIBUTE1',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE2 :=
            get_column_value ('LINE_GDF_ATTRIBUTE2',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE3 :=
            get_column_value ('LINE_GDF_ATTRIBUTE3',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE4 :=
            get_column_value ('LINE_GDF_ATTRIBUTE4',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE5 :=
            get_column_value ('LINE_GDF_ATTRIBUTE5',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE6 :=
            get_column_value ('LINE_GDF_ATTRIBUTE6',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE7 :=
            get_column_value ('LINE_GDF_ATTRIBUTE7',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE8 :=
            get_column_value ('LINE_GDF_ATTRIBUTE8',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE9 :=
            get_column_value ('LINE_GDF_ATTRIBUTE9',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE10 :=
            get_column_value ('LINE_GDF_ATTRIBUTE10',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE11 :=
            get_column_value ('LINE_GDF_ATTRIBUTE11',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE12 :=
            get_column_value ('LINE_GDF_ATTRIBUTE12',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE13 :=
            get_column_value ('LINE_GDF_ATTRIBUTE13',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE14 :=
            get_column_value ('LINE_GDF_ATTRIBUTE14',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE15 :=
            get_column_value ('LINE_GDF_ATTRIBUTE15',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE16 :=
            get_column_value ('LINE_GDF_ATTRIBUTE16',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE17 :=
            get_column_value ('LINE_GDF_ATTRIBUTE17',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE18 :=
            get_column_value ('LINE_GDF_ATTRIBUTE18',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE19 :=
            get_column_value ('LINE_GDF_ATTRIBUTE19',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTRIBUTE20 :=
            get_column_value ('LINE_GDF_ATTRIBUTE20',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.LINE_GDF_ATTR_CATEGORY :=
            get_column_value ('LINE_GDF_ATTR_CATEGORY',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.RULE_START_DATE :=
            get_column_value ('RULE_START_DATE',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.SALES_ORDER_REVISION :=
            get_column_value ('SALES_ORDER_REVISION',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);
         ra_interface_rec.SALES_ORDER_SOURCE :=
            get_column_value ('SALES_ORDER_SOURCE',
                              rec.INTERFACE_LINE_ATTRIBUTE1,
                              rec.trx_number);

         --------------------------------------------------------------------------------------
         --Now updating all records with same payment set id and reason code and trx_date
         ------------------------------------------------------------------------------------
         UPDATE apps.ra_interface_lines_all
            SET AGREEMENT_ID = ra_interface_rec.AGREEMENT_ID,
                APPLICATION_ID = ra_interface_rec.APPLICATION_ID,
                BILLING_DATE = ra_interface_rec.BILLING_DATE,
                COMMENTS = ra_interface_rec.COMMENTS,
                CONS_BILLING_NUMBER = ra_interface_rec.CONS_BILLING_NUMBER,
                CONTRACT_ID = ra_interface_rec.CONTRACT_ID,
                CONVERSION_DATE = ra_interface_rec.CONVERSION_DATE,
                CONVERSION_RATE = ra_interface_rec.CONVERSION_RATE,
                CONVERSION_TYPE = ra_interface_rec.CONVERSION_TYPE,
                CREDIT_METHOD_FOR_ACCT_RULE =
                   ra_interface_rec.CREDIT_METHOD_FOR_ACCT_RULE,
                CREDIT_METHOD_FOR_INSTALLMENTS =
                   ra_interface_rec.CREDIT_METHOD_FOR_INSTALLMENTS,
                CURRENCY_CODE = ra_interface_rec.CURRENCY_CODE,
                CUSTOMER_BANK_ACCOUNT_ID =
                   ra_interface_rec.CUSTOMER_BANK_ACCOUNT_ID,
                DEFAULT_TAXATION_COUNTRY =
                   ra_interface_rec.DEFAULT_TAXATION_COUNTRY,
                DOCUMENT_NUMBER = ra_interface_rec.DOCUMENT_NUMBER,
                DOCUMENT_NUMBER_SEQUENCE_ID =
                   ra_interface_rec.DOCUMENT_NUMBER_SEQUENCE_ID,
                DOCUMENT_SUB_TYPE = ra_interface_rec.DOCUMENT_SUB_TYPE,
                GL_DATE = ra_interface_rec.GL_DATE,
                HEADER_ATTRIBUTE1 = ra_interface_rec.HEADER_ATTRIBUTE1,
                HEADER_ATTRIBUTE2 = ra_interface_rec.HEADER_ATTRIBUTE2,
                HEADER_ATTRIBUTE3 = ra_interface_rec.HEADER_ATTRIBUTE3,
                HEADER_ATTRIBUTE4 = ra_interface_rec.HEADER_ATTRIBUTE4,
                HEADER_ATTRIBUTE5 = ra_interface_rec.HEADER_ATTRIBUTE5,
                HEADER_ATTRIBUTE6 = ra_interface_rec.HEADER_ATTRIBUTE6,
                HEADER_ATTRIBUTE7 = ra_interface_rec.HEADER_ATTRIBUTE7,
                HEADER_ATTRIBUTE8 = ra_interface_rec.HEADER_ATTRIBUTE8,
                HEADER_ATTRIBUTE9 = ra_interface_rec.HEADER_ATTRIBUTE9,
                HEADER_ATTRIBUTE10 = ra_interface_rec.HEADER_ATTRIBUTE10,
                HEADER_ATTRIBUTE11 = ra_interface_rec.HEADER_ATTRIBUTE11,
                HEADER_ATTRIBUTE12 = ra_interface_rec.HEADER_ATTRIBUTE12,
                HEADER_ATTRIBUTE13 = ra_interface_rec.HEADER_ATTRIBUTE13,
                HEADER_ATTRIBUTE14 = ra_interface_rec.HEADER_ATTRIBUTE14,
                HEADER_ATTRIBUTE15 = ra_interface_rec.HEADER_ATTRIBUTE15,
                HEADER_ATTRIBUTE_CATEGORY =
                   ra_interface_rec.HEADER_ATTRIBUTE_CATEGORY,
                HEADER_GDF_ATTRIBUTE1 = ra_interface_rec.HEADER_GDF_ATTRIBUTE1,
                HEADER_GDF_ATTRIBUTE2 = ra_interface_rec.HEADER_GDF_ATTRIBUTE2,
                HEADER_GDF_ATTRIBUTE3 = ra_interface_rec.HEADER_GDF_ATTRIBUTE3,
                HEADER_GDF_ATTRIBUTE4 = ra_interface_rec.HEADER_GDF_ATTRIBUTE4,
                HEADER_GDF_ATTRIBUTE5 = ra_interface_rec.HEADER_GDF_ATTRIBUTE5,
                HEADER_GDF_ATTRIBUTE6 = ra_interface_rec.HEADER_GDF_ATTRIBUTE6,
                HEADER_GDF_ATTRIBUTE7 = ra_interface_rec.HEADER_GDF_ATTRIBUTE7,
                HEADER_GDF_ATTRIBUTE8 = ra_interface_rec.HEADER_GDF_ATTRIBUTE8,
                HEADER_GDF_ATTRIBUTE9 = ra_interface_rec.HEADER_GDF_ATTRIBUTE9,
                HEADER_GDF_ATTRIBUTE10 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE10,
                HEADER_GDF_ATTRIBUTE11 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE11,
                HEADER_GDF_ATTRIBUTE12 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE12,
                HEADER_GDF_ATTRIBUTE13 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE13,
                HEADER_GDF_ATTRIBUTE14 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE14,
                HEADER_GDF_ATTRIBUTE15 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE15,
                HEADER_GDF_ATTRIBUTE16 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE16,
                HEADER_GDF_ATTRIBUTE17 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE17,
                HEADER_GDF_ATTRIBUTE18 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE18,
                HEADER_GDF_ATTRIBUTE19 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE19,
                HEADER_GDF_ATTRIBUTE20 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE20,
                HEADER_GDF_ATTRIBUTE21 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE21,
                HEADER_GDF_ATTRIBUTE22 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE22,
                HEADER_GDF_ATTRIBUTE23 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE23,
                HEADER_GDF_ATTRIBUTE24 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE24,
                HEADER_GDF_ATTRIBUTE25 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE25,
                HEADER_GDF_ATTRIBUTE26 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE26,
                HEADER_GDF_ATTRIBUTE27 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE27,
                HEADER_GDF_ATTRIBUTE28 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE28,
                HEADER_GDF_ATTRIBUTE29 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE29,
                HEADER_GDF_ATTRIBUTE30 =
                   ra_interface_rec.HEADER_GDF_ATTRIBUTE30,
                HEADER_GDF_ATTR_CATEGORY =
                   ra_interface_rec.HEADER_GDF_ATTR_CATEGORY,
                INITIAL_CUSTOMER_TRX_ID =
                   ra_interface_rec.INITIAL_CUSTOMER_TRX_ID,
                INTERNAL_NOTES = ra_interface_rec.INTERNAL_NOTES,
                INVOICING_RULE_ID = ra_interface_rec.INVOICING_RULE_ID,
                LEGAL_ENTITY_ID = ra_interface_rec.LEGAL_ENTITY_ID,
                ORIG_SYSTEM_BILL_ADDRESS_ID =
                   ra_interface_rec.ORIG_SYSTEM_BILL_ADDRESS_ID,
                ORIG_SYSTEM_BILL_CONTACT_ID =
                   ra_interface_rec.ORIG_SYSTEM_BILL_CONTACT_ID,
                ORIG_SYSTEM_BILL_CUSTOMER_ID =
                   ra_interface_rec.ORIG_SYSTEM_BILL_CUSTOMER_ID,
                ORIG_SYSTEM_SOLD_CUSTOMER_ID =
                   ra_interface_rec.ORIG_SYSTEM_SOLD_CUSTOMER_ID,
                ORIG_SYSTEM_SHIP_ADDRESS_ID =
                   ra_interface_rec.ORIG_SYSTEM_SHIP_ADDRESS_ID,
                ORIG_SYSTEM_SHIP_CONTACT_ID =
                   ra_interface_rec.ORIG_SYSTEM_SHIP_CONTACT_ID,
                ORIG_SYSTEM_SHIP_CUSTOMER_ID =
                   ra_interface_rec.ORIG_SYSTEM_SHIP_CUSTOMER_ID,
                PAYMENT_ATTRIBUTES = ra_interface_rec.PAYMENT_ATTRIBUTES,
                ORIG_SYSTEM_BATCH_NAME =
                   ra_interface_rec.ORIG_SYSTEM_BATCH_NAME,
                PAYMENT_SET_ID = ra_interface_rec.PAYMENT_SET_ID,
                PREVIOUS_CUSTOMER_TRX_ID =
                   ra_interface_rec.PREVIOUS_CUSTOMER_TRX_ID,
                PRIMARY_SALESREP_ID = ra_interface_rec.PRIMARY_SALESREP_ID,
                PRINTING_OPTION = ra_interface_rec.PRINTING_OPTION,
                PURCHASE_ORDER = ra_interface_rec.PURCHASE_ORDER,
                PURCHASE_ORDER_DATE = ra_interface_rec.PURCHASE_ORDER_DATE,
                PURCHASE_ORDER_REVISION =
                   ra_interface_rec.PURCHASE_ORDER_REVISION,
                REASON_CODE = ra_interface_rec.REASON_CODE,
                RECEIPT_METHOD_ID = ra_interface_rec.RECEIPT_METHOD_ID,
                RELATED_CUSTOMER_TRX_ID =
                   ra_interface_rec.RELATED_CUSTOMER_TRX_ID,
                SET_OF_BOOKS_ID = ra_interface_rec.SET_OF_BOOKS_ID,
                TERM_ID = ra_interface_rec.TERM_ID,
                TERRITORY_ID = ra_interface_rec.TERRITORY_ID,
                TRX_DATE = ra_interface_rec.TRX_DATE,
                ACCOUNTING_RULE_DURATION =
                   ra_interface_rec.ACCOUNTING_RULE_DURATION,
                ACCOUNTING_RULE_ID = ra_interface_rec.ACCOUNTING_RULE_ID,
                ATTRIBUTE1 = ra_interface_rec.ATTRIBUTE1,
                ATTRIBUTE2 = ra_interface_rec.ATTRIBUTE2,
                ATTRIBUTE3 = ra_interface_rec.ATTRIBUTE3,
                ATTRIBUTE4 = ra_interface_rec.ATTRIBUTE4,
                ATTRIBUTE5 = ra_interface_rec.ATTRIBUTE5,
                ATTRIBUTE6 = ra_interface_rec.ATTRIBUTE6,
                ATTRIBUTE7 = ra_interface_rec.ATTRIBUTE7,
                ATTRIBUTE8 = ra_interface_rec.ATTRIBUTE8,
                ATTRIBUTE9 = ra_interface_rec.ATTRIBUTE9,
                ATTRIBUTE10 = ra_interface_rec.ATTRIBUTE10,
                ATTRIBUTE11 = ra_interface_rec.ATTRIBUTE11,
                ATTRIBUTE12 = ra_interface_rec.ATTRIBUTE12,
                ATTRIBUTE13 = ra_interface_rec.ATTRIBUTE13,
                ATTRIBUTE14 = ra_interface_rec.ATTRIBUTE14,
                ATTRIBUTE15 = ra_interface_rec.ATTRIBUTE15,
                ATTRIBUTE_CATEGORY = ra_interface_rec.ATTRIBUTE_CATEGORY,
                INTERFACE_LINE_CONTEXT =
                   ra_interface_rec.INTERFACE_LINE_CONTEXT,
                LINE_GDF_ATTRIBUTE1 = ra_interface_rec.LINE_GDF_ATTRIBUTE1,
                LINE_GDF_ATTRIBUTE2 = ra_interface_rec.LINE_GDF_ATTRIBUTE2,
                LINE_GDF_ATTRIBUTE3 = ra_interface_rec.LINE_GDF_ATTRIBUTE3,
                LINE_GDF_ATTRIBUTE4 = ra_interface_rec.LINE_GDF_ATTRIBUTE4,
                LINE_GDF_ATTRIBUTE5 = ra_interface_rec.LINE_GDF_ATTRIBUTE5,
                LINE_GDF_ATTRIBUTE6 = ra_interface_rec.LINE_GDF_ATTRIBUTE6,
                LINE_GDF_ATTRIBUTE7 = ra_interface_rec.LINE_GDF_ATTRIBUTE7,
                LINE_GDF_ATTRIBUTE8 = ra_interface_rec.LINE_GDF_ATTRIBUTE8,
                LINE_GDF_ATTRIBUTE9 = ra_interface_rec.LINE_GDF_ATTRIBUTE9,
                LINE_GDF_ATTRIBUTE10 = ra_interface_rec.LINE_GDF_ATTRIBUTE10,
                LINE_GDF_ATTRIBUTE11 = ra_interface_rec.LINE_GDF_ATTRIBUTE11,
                LINE_GDF_ATTRIBUTE12 = ra_interface_rec.LINE_GDF_ATTRIBUTE12,
                LINE_GDF_ATTRIBUTE13 = ra_interface_rec.LINE_GDF_ATTRIBUTE13,
                LINE_GDF_ATTRIBUTE14 = ra_interface_rec.LINE_GDF_ATTRIBUTE14,
                LINE_GDF_ATTRIBUTE15 = ra_interface_rec.LINE_GDF_ATTRIBUTE15,
                LINE_GDF_ATTRIBUTE16 = ra_interface_rec.LINE_GDF_ATTRIBUTE16,
                LINE_GDF_ATTRIBUTE17 = ra_interface_rec.LINE_GDF_ATTRIBUTE17,
                LINE_GDF_ATTRIBUTE18 = ra_interface_rec.LINE_GDF_ATTRIBUTE18,
                LINE_GDF_ATTRIBUTE19 = ra_interface_rec.LINE_GDF_ATTRIBUTE19,
                LINE_GDF_ATTRIBUTE20 = ra_interface_rec.LINE_GDF_ATTRIBUTE20,
                LINE_GDF_ATTR_CATEGORY =
                   ra_interface_rec.LINE_GDF_ATTR_CATEGORY,
                RULE_START_DATE = ra_interface_rec.RULE_START_DATE,
                SALES_ORDER_REVISION = ra_interface_rec.SALES_ORDER_REVISION,
                SALES_ORDER_SOURCE = ra_interface_rec.SALES_ORDER_SOURCE,
                last_update_date = SYSDATE,
                last_updated_by = FND_GLOBAL.USER_ID
          WHERE     INTERFACE_LINE_ATTRIBUTE1 = rec.INTERFACE_LINE_ATTRIBUTE1
                AND trx_number = rec.trx_number;

         g_location :=
               ' UPDATED '
            || SQL%ROWCOUNT
            || ' for error 3 in ra_interface_lines for order '
            || rec.INTERFACE_LINE_ATTRIBUTE1
            || ' trx_number '
            || rec.trx_number;

         write_output (g_location);

         COMMIT;
         write_output ('calling update status');
         update_status (p_order_number   => rec.INTERFACE_LINE_ATTRIBUTE1,
                        p_trx_number     => rec.trx_number);
         COMMIT;
      END LOOP;

      /*******************ERROR 3 FIXING LOGIC ENDS**********************************************/
      g_location := ' check_invoice_errors procedure ended';
      write_output (g_location);
      errbuf := NULL;
      retcode := 0;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_error_message :=
               g_error_message
            || 'In  when others Error at '
            || g_location
            || ' Error: '
            || g_error_message
            || SQLERRM;
         errbuf := g_error_message;
         retcode := 2;
         write_output (g_error_message);
         write_error (g_error_message);
   END check_invoice_errors;
END XXWC_RA_INTERFACE_FIXER_PKG;
/