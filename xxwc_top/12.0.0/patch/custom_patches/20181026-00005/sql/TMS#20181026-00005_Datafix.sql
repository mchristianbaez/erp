/***********************************************************************************************************************************************
   NAME:     TMS#20181026-00005_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        10/26/2018  Rakesh Patel     TMS#20181026-00005- debit memos in apex that need to be resolved
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Insert');

   UPDATE xxwc.xxwcar_cash_rcpts_tbl  set STATUS='PROCESSED', COMMENTS=NULL 
    WHERE INVOICE_NBR in ( 
                          '4259012-01', 
                          '4516122-00', 
                          '4522122-01'
                         ) AND RECEIPT_TYPE LIKE 'DEBIT%';

   DBMS_OUTPUT.put_line ('Records Updated -' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
	  ROLLBACK;
END;
/