CREATE OR REPLACE PACKAGE XXWC_B2B_SO_IB_PKG
AS
   /*************************************************************************
   *   $Header XXWC_B2B_SO_IB_PKG.PKG $
   *   Module Name: XXWC_B2B_SO_IB_PKG.PKG
   *
   *   PURPOSE:   This package is used for B2B Customer SalesOrder Inbound Interface
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        04/09/2014  Gopi Damuluri           Initial Version
   *   1.6        06/11/2015  Manjula Chellappan      TMS# 20150615-00088 B2B for POA
   *                          Gopi Damuluri           Add Notification Email for B2B PO Import Success and Failures
   *                                                  Add functions for deriving SalesRep Information
   *   1.8        09/21/2015  Gopi Damuluri           TMS# 20151019-00065
   *                                                  Allow any user to setup B2B access when Customer has no Sales Rep associated.
   *   1.9        12/15/2015  Gopi Damuluri           TMS# 20160120-00169 - B2B POD Enhancement
   *   1.18       10/27/2016  Rakesh Patel            TMS#20161010-00242 --  Change SOA and ASN to reflect priced copy settings on B2B Setup page
   *   1.23    08/15/2017    Nancy Pahwa            TMS#20170220-00171 -- OSU Setup and Messaging Improvements
   * ***************************************************************************/
   PROCEDURE load_interface (p_errbuf                  OUT VARCHAR2,
                             p_retcode                 OUT NUMBER,
                             p_validate_only        IN     VARCHAR2,
                             p_notification_email   IN     VARCHAR2,
                             p_cust_po_number       IN     VARCHAR2,
                             p_apex_url             IN     VARCHAR2);

   PROCEDURE create_contact (p_customer_id       IN     NUMBER,
                             p_first_name        IN     VARCHAR2,
                             p_last_name         IN     VARCHAR2,
                             p_contact_num       IN     VARCHAR2,
                             p_email_id          IN     VARCHAR2,
                             p_fax_num           IN     VARCHAR2,
                             p_cust_contact_id      OUT NUMBER,
                             p_error_message        OUT VARCHAR2);

   PROCEDURE submit_job (p_errbuf                   OUT VARCHAR2,
                         p_retcode                  OUT NUMBER,
                         p_user_name             IN     VARCHAR2,
                         p_responsibility_name   IN     VARCHAR2,
                         p_process_type          IN     VARCHAR2);

   /*
      PROCEDURE load_staging(p_errbuf                      OUT VARCHAR2,
                             p_retcode                     OUT NUMBER);
   */

   PROCEDURE uc4_call (p_errbuf              OUT VARCHAR2,
                       p_retcode             OUT VARCHAR2,
                       p_conc_prog_name   IN     VARCHAR2,
                       p_user_name        IN     VARCHAR2,
                       p_resp_name        IN     VARCHAR2,
                       p_b2b_email        IN     VARCHAR2,
                       p_apex_url         IN     VARCHAR2);

   -- Version# 1.6 > Start
   PROCEDURE Generate_POA_KPT (errbuf              OUT VARCHAR2,
                               retcode             OUT VARCHAR2,
                               p_org_id           IN     NUMBER,
                               p_cust_po_number   IN     VARCHAR2,
                               p_resend_flag      IN     VARCHAR2);

   FUNCTION get_sales_rep (p_cust_account_id   IN NUMBER) RETURN VARCHAR2;
   function XXWC_CF_FND_B2B_TEXTFormula return Varchar2; --Version 1.23
   FUNCTION get_sales_mgr_email ( p_cust_account_id   IN NUMBER -- Version# 1.18
                                , p_user_name IN VARCHAR2) RETURN VARCHAR2; -- Version# 1.8

   FUNCTION get_sales_rep_email (p_cust_account_id   IN NUMBER -- Version# 1.18
                                ) RETURN VARCHAR2;

   FUNCTION is_sales_rep (p_cust_account_id   IN NUMBER -- Version# 1.18
                        , p_user_name  IN VARCHAR2) RETURN VARCHAR2;

   -- Version# 1.8 > Start
   FUNCTION get_sales_rep_name ( p_cust_account_id   IN NUMBER -- Version# 1.18
                              , p_user_name  IN VARCHAR2) RETURN VARCHAR2;
   -- Version# 1.8 < End

   FUNCTION get_apex_url RETURN VARCHAR2;

   -- Version# 1.6 < End

-- Version# 1.9 > Start

   PROCEDURE insert_pod_info (p_errbuf      OUT VARCHAR2,
                             p_retcode      OUT NUMBER);

   PROCEDURE CREATE_FILE (p_errbuf          OUT VARCHAR2
                         ,p_retcode         OUT NUMBER
                         ,p_directory_path  IN  VARCHAR2);
-- Version# 1.9 < End

END XXWC_B2B_SO_IB_PKG;
/