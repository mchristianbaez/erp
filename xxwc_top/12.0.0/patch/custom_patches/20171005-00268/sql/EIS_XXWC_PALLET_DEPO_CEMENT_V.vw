-----------------------------------------------------------------------------------------------
/* *****************************************************************************
  $Header XXEIS.EIS_XXWC_PALLET_DEPO_CEMENT_V.vw $
  Module Name: Order Management
  PURPOSE: 	Items Search - Available Quantity to Return WC
  TMS Task Id : 20171005-00268
  REVISIONS: Initial Version
  Ver        	Date        	Author                			Description
  ---------  ----------  ------------------    				----------------
  1.0        22/01/2018  		Prasanna       			Initial Version --20171005-00268 
************************************************************************************/
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_PALLET_DEPO_CEMENT_V (ORDER_NUMBER, PART_NUMBER, QTY_AVAILABLE_TO_RETURN, QUANTITY_PURCHASED, QTY_RETURN_ON_CREDIT, PURCHASED_PRICE, UNIT_SELLING_PRICE, RETURN_ORDER, INVOICED_DATE, BRANCH, CUSTOMER_NAME, CUSTOMER_NUMBER, CUSTOMER_JOB_NAME, OOHR_HEADER_ID, OOLR_LINE_ID, OTT_TRANSACTION_TYPE_ID, RCT_CUSTOMER_TRX_ID, RCTL_CUSTOMER_TRX_LINE_ID, OOD_ORGANIZATION_ID, HCA_CUST_ACCOUNT_ID, HP_PARTY_ID, HZCS_SHIP_TO_SITE_USE_ID, MSI_INVENTORY_ITEM_ID, MSI_ORGANIZATION_ID)
AS
  SELECT ooh.order_number,
    msi.segment1 part_number,
    SUM((ool.ordered_quantity-NVL(oolr.ordered_quantity,0)))qty_available_to_return,
    SUM(ool.ordered_quantity) quantity_purchased,
    DECODE(ott.NAME,'CREDIT ONLY',SUM(NVL(oolr.ordered_quantity,0)),0)qty_return_on_credit,
    SUM(ool.ordered_quantity*ool.unit_selling_price)purchased_price,
    ool.unit_selling_price unit_selling_price,
    oohr.order_number return_order,
    TRUNC(rct.trx_date) invoiced_date,
    ood.organization_code branch,
    hp.party_name customer_name,
    hca.account_number customer_number,
    HZCS_SHIP_TO.LOCATION customer_job_name,
    ---Primary Keys
    ooh.header_id oohr_header_id,
    ool.line_id oolr_line_id,
    ott.transaction_type_id ott_transaction_type_id,
    rct.customer_trx_id rct_customer_trx_id,
    rctl.customer_trx_line_id rctl_customer_trx_line_id,
    ood.organization_id ood_organization_id,
    hca.cust_account_id hca_cust_account_id,
    hp.party_id hp_party_id,
    HZCS_SHIP_TO.site_use_id HZCS_SHIP_TO_site_use_id,
    msi.inventory_item_id msi_inventory_item_id,
    msi.organization_id msi_organization_id
    --descr#flexfield#start
    --descr#flexfield#end
  FROM apps.oe_order_headers ooh,
    apps.oe_order_lines ool,
    apps.oe_order_lines oolr,
    apps.oe_order_headers oohr,
    apps.oe_transaction_types_tl ott,
    apps.ra_customer_trx rct,
    apps.ra_customer_trx_lines rctl,
    apps.mtl_parameters ood,
    apps.hz_cust_accounts hca,
    apps.hz_parties hp,
    APPS.HZ_CUST_SITE_USES HZCS_SHIP_TO,
    apps.mtl_system_items_b msi
  WHERE ooh.header_id                        =ool.header_id
  AND ool.line_id                            =oolr.reference_line_id(+)
  AND oolr.header_id                         =oohr.header_id(+)
  AND ool.line_type_id                       =ott.transaction_type_id
  AND rct.customer_trx_id                    =rctl.customer_trx_id
  AND rct.org_id                             =rctl.org_id
  AND TO_CHAR(rctl.interface_line_attribute6)=TO_CHAR(ool.line_id)
  AND ood.organization_id                    =ooh.ship_from_org_id
  AND hca.cust_account_id                    =ooh.sold_to_org_id
  AND hca.party_id                           =hp.party_id
  AND ooh.ship_to_org_id                     = hzcs_ship_to.site_use_id(+)
  AND ool.inventory_item_id                  =msi.inventory_item_id
  AND ooh.ship_from_org_id                   =msi.organization_id
  AND oolr.line_category_code(+)             ='RETURN'
  AND rctl.interface_line_context            ='ORDER ENTRY'
  AND ott.language                           ='US'
  GROUP BY ooh.order_number,
    ool.ordered_item,
    ool.unit_selling_price ,
    oohr.order_number ,
    TRUNC(rct.trx_date),
    ood.organization_code ,
    hp.party_name ,
    hca.account_number ,
    HZCS_SHIP_TO.LOCATION ,
    ott.NAME,
    msi.segment1,
    ooh.header_id ,
    ool.line_id ,
    ott.transaction_type_id ,
    rct.customer_trx_id ,
    rctl.customer_trx_line_id ,
    ood.organization_id ,
    hca.cust_account_id ,
    hp.party_id ,
    HZCS_SHIP_TO.site_use_id ,
    msi.inventory_item_id ,
    msi.organization_id
/
	