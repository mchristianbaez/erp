--Report Name            : Items Search - Available Quantity to Return WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_PALLET_DEPO_CEMENT_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_PALLET_DEPO_CEMENT_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_PALLET_DEPO_CEMENT_V',660,'','','','','PS066716','XXEIS','Eis Xxwc Pallet Depo Cement V','EXPDCV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_PALLET_DEPO_CEMENT_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_PALLET_DEPO_CEMENT_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_PALLET_DEPO_CEMENT_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','PS066716','NUMBER','','','Order Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','PART_NUMBER',660,'Part Number','PART_NUMBER','','','','PS066716','VARCHAR2','','','Part Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','QTY_AVAILABLE_TO_RETURN',660,'Qty Available To Return','QTY_AVAILABLE_TO_RETURN','','','','PS066716','NUMBER','','','Qty Available To Return','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','QUANTITY_PURCHASED',660,'Quantity Purchased','QUANTITY_PURCHASED','','','','PS066716','NUMBER','','','Quantity Purchased','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','QTY_RETURN_ON_CREDIT',660,'Qty Return On Credit','QTY_RETURN_ON_CREDIT','','','','PS066716','NUMBER','','','Qty Return On Credit','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','PURCHASED_PRICE',660,'Purchased Price','PURCHASED_PRICE','','','','PS066716','NUMBER','','','Purchased Price','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','INVOICED_DATE',660,'Invoiced Date','INVOICED_DATE','','','','PS066716','DATE','','','Invoiced Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','BRANCH',660,'Branch','BRANCH','','','','PS066716','VARCHAR2','','','Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','PS066716','VARCHAR2','','','Customer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','PS066716','VARCHAR2','','','Customer Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','CUSTOMER_JOB_NAME',660,'Customer Job Name','CUSTOMER_JOB_NAME','','','','PS066716','VARCHAR2','','','Customer Job Name','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','PS066716','NUMBER','','','Inventory Item Id','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','HCA_CUST_ACCOUNT_ID',660,'Hca Cust Account Id','HCA_CUST_ACCOUNT_ID','','','','PS066716','NUMBER','','','Hca Cust Account Id','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','HP_PARTY_ID',660,'Hp Party Id','HP_PARTY_ID','','','','PS066716','NUMBER','','','Hp Party Id','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','HZCS_SHIP_TO_SITE_USE_ID',660,'Hzcs Ship To Site Use Id','HZCS_SHIP_TO_SITE_USE_ID','','','','PS066716','NUMBER','','','Hzcs Ship To Site Use Id','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','MSI_INVENTORY_ITEM_ID',660,'Msi Inventory Item Id','MSI_INVENTORY_ITEM_ID','','','','PS066716','NUMBER','','','Msi Inventory Item Id','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','MSI_ORGANIZATION_ID',660,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','PS066716','NUMBER','','','Msi Organization Id','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','OOD_ORGANIZATION_ID',660,'Ood Organization Id','OOD_ORGANIZATION_ID','','','','PS066716','NUMBER','','','Ood Organization Id','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','OOHR_HEADER_ID',660,'Oohr Header Id','OOHR_HEADER_ID','','','','PS066716','NUMBER','','','Oohr Header Id','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','OOLR_LINE_ID',660,'Oolr Line Id','OOLR_LINE_ID','','','','PS066716','NUMBER','','','Oolr Line Id','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','OTT_TRANSACTION_TYPE_ID',660,'Ott Transaction Type Id','OTT_TRANSACTION_TYPE_ID','','','','PS066716','NUMBER','','','Ott Transaction Type Id','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','RCTL_CUSTOMER_TRX_LINE_ID',660,'Rctl Customer Trx Line Id','RCTL_CUSTOMER_TRX_LINE_ID','','','','PS066716','NUMBER','','','Rctl Customer Trx Line Id','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','RCT_CUSTOMER_TRX_ID',660,'Rct Customer Trx Id','RCT_CUSTOMER_TRX_ID','','','','PS066716','NUMBER','','','Rct Customer Trx Id','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','RETURN_ORDER',660,'Return Order','RETURN_ORDER','','','','PS066716','NUMBER','','','Return Order','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PALLET_DEPO_CEMENT_V','UNIT_SELLING_PRICE',660,'Unit Selling Price','UNIT_SELLING_PRICE','','','','PS066716','NUMBER','','','Unit Selling Price','','','','','');
--Inserting Object Components for EIS_XXWC_PALLET_DEPO_CEMENT_V
--Inserting Object Component Joins for EIS_XXWC_PALLET_DEPO_CEMENT_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Items Search - Available Quantity to Return WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Items Search - Available Quantity to Return WC
xxeis.eis_rsc_ins.lov( 660,'SELECT DISTINCT LOCATION FROM hz_cust_site_uses','','OM Customer Job Name LOV','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM Customer Name LOV','This LOV lists all Customer Names','XXEIS_RS_ADMIN',NULL,'N','','','Y','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select  cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','OM Customer Number LOV','This LOV lists all Customer Numbers','XXEIS_RS_ADMIN',NULL,'N','','','Y','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT DISTINCT segment1 Item 
FROM    mtl_system_items_b msi 
WHERE  NOT EXISTS (SELECT 1 
FROM    mtl_parameters mp 
WHERE  organization_code IN (''CAN'',''HDS'',''US1'',''CN1'') 
AND      msi.organization_id = mp.organization_id ) order by segment1 ASC','','XXWC SKU Item Search','XXWC SKU Item Search','PS066716',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Items Search - Available Quantity to Return WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Items Search - Available Quantity to Return WC
xxeis.eis_rsc_utility.delete_report_rows( 'Items Search - Available Quantity to Return WC',660 );
--Inserting Report - Items Search - Available Quantity to Return WC
xxeis.eis_rsc_ins.r( 660,'Items Search - Available Quantity to Return WC','','Items Search - Available Quantity to Return report to see what order numbers in an account, for this part number,  still have quantity available for return for a customer','','','','PS066716','EIS_XXWC_PALLET_DEPO_CEMENT_V','Y','','','PS066716','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - Items Search - Available Quantity to Return WC
xxeis.eis_rsc_ins.rc( 'Items Search - Available Quantity to Return WC',660,'BRANCH','Branch','Branch','','','default','','10','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Items Search - Available Quantity to Return WC',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','11','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Items Search - Available Quantity to Return WC',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','12','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Items Search - Available Quantity to Return WC',660,'INVOICED_DATE','Invoiced Date','Invoiced Date','','','default','','9','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Items Search - Available Quantity to Return WC',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','1','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Items Search - Available Quantity to Return WC',660,'PART_NUMBER','Part Number','Part Number','','','default','','2','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Items Search - Available Quantity to Return WC',660,'PURCHASED_PRICE','Extended Selling Price','Purchased Price','','~T~D~2','default','','7','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Items Search - Available Quantity to Return WC',660,'QTY_AVAILABLE_TO_RETURN','Qty Available To Return','Qty Available To Return','','~~~','default','','3','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Items Search - Available Quantity to Return WC',660,'QTY_RETURN_ON_CREDIT','Qty Return On Credit Only','Qty Return On Credit','','~~~','default','','5','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Items Search - Available Quantity to Return WC',660,'QUANTITY_PURCHASED','Quantity Purchased','Quantity Purchased','','~~~','default','','4','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Items Search - Available Quantity to Return WC',660,'CUSTOMER_JOB_NAME','Customer Job Name','Customer Job Name','','','default','','13','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Items Search - Available Quantity to Return WC',660,'RETURN_ORDER','Return Order#','Return Order','','~~~','default','','8','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Items Search - Available Quantity to Return WC',660,'UNIT_SELLING_PRICE','Unit Selling Price','Unit Selling Price','','~T~D~5','default','','6','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','','US','','');
--Inserting Report Parameters - Items Search - Available Quantity to Return WC
xxeis.eis_rsc_ins.rp( 'Items Search - Available Quantity to Return WC',660,'Customer Name','Customer Name','CUSTOMER_NAME','IN','OM Customer Name LOV','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','PS066716','Y','N','','','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Items Search - Available Quantity to Return WC',660,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','OM Customer Number LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','PS066716','Y','N','','','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Items Search - Available Quantity to Return WC',660,'Part Number','Part Number','PART_NUMBER','IN','XXWC SKU Item Search','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','PS066716','Y','N','','','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Items Search - Available Quantity to Return WC',660,'Date From','Date From','INVOICED_DATE','>=','','','DATE','N','Y','5','Y','Y','CONSTANT','PS066716','Y','N','','','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Items Search - Available Quantity to Return WC',660,'Date To','Date To','INVOICED_DATE','<=','','','DATE','N','Y','6','Y','Y','CONSTANT','PS066716','Y','N','','','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Items Search - Available Quantity to Return WC',660,'Job','Job','CUSTOMER_JOB_NAME','IN','OM Customer Job Name LOV','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','PS066716','Y','N','','','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Items Search - Available Quantity to Return WC',660,'Branch','Branch','','','OM Warehouse All','','VARCHAR2','Y','Y','1','Y','N','CONSTANT','PS066716','Y','N','','','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','US','');
--Inserting Dependent Parameters - Items Search - Available Quantity to Return WC
xxeis.eis_rsc_ins.rdp( 'Items Search - Available Quantity to Return WC',660,'PARTY_NAME','Customer Number','Customer Name','IN','N','');
xxeis.eis_rsc_ins.rdp( 'Items Search - Available Quantity to Return WC',660,'ACCOUNT_NUMBER','Customer Name','Customer Number','IN','N','');
--Inserting Report Conditions - Items Search - Available Quantity to Return WC
xxeis.eis_rsc_ins.rcnh( 'Items Search - Available Quantity to Return WC',660,'FreeText','FREE_TEXT','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','Y','','','','AND ( ''All'' IN (:Branch) OR (EXPDCV.BRANCH IN (:Branch)))','1',660,'Items Search - Available Quantity to Return WC','FreeText');
xxeis.eis_rsc_ins.rcnh( 'Items Search - Available Quantity to Return WC',660,'EXPDCV.CUSTOMER_NAME IN Customer Name','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_NAME','','Customer Name','','','','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','','','','IN','Y','Y','','','','','1',660,'Items Search - Available Quantity to Return WC','EXPDCV.CUSTOMER_NAME IN Customer Name');
xxeis.eis_rsc_ins.rcnh( 'Items Search - Available Quantity to Return WC',660,'EXPDCV.CUSTOMER_NUMBER IN Customer Number','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_NUMBER','','Customer Number','','','','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','','','','IN','Y','Y','','','','','1',660,'Items Search - Available Quantity to Return WC','EXPDCV.CUSTOMER_NUMBER IN Customer Number');
xxeis.eis_rsc_ins.rcnh( 'Items Search - Available Quantity to Return WC',660,'EXPDCV.CUSTOMER_JOB_NAME IN Job','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_JOB_NAME','','Job','','','','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','','','','IN','Y','Y','','','','','1',660,'Items Search - Available Quantity to Return WC','EXPDCV.CUSTOMER_JOB_NAME IN Job');
xxeis.eis_rsc_ins.rcnh( 'Items Search - Available Quantity to Return WC',660,'EXPDCV.INVOICED_DATE >= Date From','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','INVOICED_DATE','','Date From','','','','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',660,'Items Search - Available Quantity to Return WC','EXPDCV.INVOICED_DATE >= Date From');
xxeis.eis_rsc_ins.rcnh( 'Items Search - Available Quantity to Return WC',660,'EXPDCV.INVOICED_DATE <= Date To','SIMPLE','','','Y','','6');
xxeis.eis_rsc_ins.rcnd( '','','INVOICED_DATE','','Date To','','','','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',660,'Items Search - Available Quantity to Return WC','EXPDCV.INVOICED_DATE <= Date To');
xxeis.eis_rsc_ins.rcnh( 'Items Search - Available Quantity to Return WC',660,'EXPDCV.PART_NUMBER IN Part Number','SIMPLE','','','Y','','7');
xxeis.eis_rsc_ins.rcnd( '','','PART_NUMBER','','Part Number','','','','','EIS_XXWC_PALLET_DEPO_CEMENT_V','','','','','','IN','Y','Y','','','','','1',660,'Items Search - Available Quantity to Return WC','EXPDCV.PART_NUMBER IN Part Number');
--Inserting Report Sorts - Items Search - Available Quantity to Return WC
xxeis.eis_rsc_ins.rs( 'Items Search - Available Quantity to Return WC',660,'ORDER_NUMBER','ASC','PS066716','1','');
--Inserting Report Triggers - Items Search - Available Quantity to Return WC
--inserting report templates - Items Search - Available Quantity to Return WC
--Inserting Report Portals - Items Search - Available Quantity to Return WC
--inserting report dashboards - Items Search - Available Quantity to Return WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Items Search - Available Quantity to Return WC','660','EIS_XXWC_PALLET_DEPO_CEMENT_V','EIS_XXWC_PALLET_DEPO_CEMENT_V','N','');
--inserting report security - Items Search - Available Quantity to Return WC
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','660','','XXWC_ORDER_MGMT_SUPER_USER',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','','KB047432','',660,'PS066716','','N','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','660','','ORDER_MGMT_SUPER_USER',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','201','','XXWC_SALES_PURCHASING_MGR',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','201','','XXWC_PUR_SUPER_USER',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','201','','HDS_PRCHSNG_SPR_USR_GSC',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','201','','HDS_PRCHSNG_SPR_USR',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','201','','XXWC_PURCHASING_SR_MRG_WC',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','201','','XXWC_PURCHASING_RPM',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','201','','XXWC_PURCHASING_MGR',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','201','','XXWC_PURCHASING_INQUIRY',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','201','','XXWC_PURCHASING_BUYER',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','201','','XXWC_EOM_PURCHASING',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'Items Search - Available Quantity to Return WC','222','','XXWC_EOM_ORDER_&_COST_MGMT',660,'PS066716','','','');
--Inserting Report Pivots - Items Search - Available Quantity to Return WC
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Items Search - Available Quantity to Return WC
xxeis.eis_rsc_ins.rv( 'Items Search - Available Quantity to Return WC','','Items Search - Available Quantity to Return WC','SA059956','23-FEB-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
