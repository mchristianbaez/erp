/*************************************************************************
  $Header TMS_20160302-00197_DMS.sql $
  Module Name: 20160302-00197  DELETE the UnProcessed DMS records 

  PURPOSE: Data fix to cancel Requisition 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        02-MAR-2016  Raghav Velichetti         TMS# 20160302-00197 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160302-00197   , Before Update');

update xxwc.XXWC_OM_DMS_SHIP_CONFIRM_TBL 
set ship_confirm_Status='NO'
where order_number is not null
and ship_confirm_Status is null;

   DBMS_OUTPUT.put_line (
         'TMS: 20160302-00197 Requisition Lines Updated and number of records (Expected:537): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160302-00197   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160302-00197, Errors : ' || SQLERRM);
END;
/
/