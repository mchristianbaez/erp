-- ******************************************************************************
-- *  Copyright (c) 2011 HDS Supply
-- *  All rights reserved.
-- ******************************************************************************
-- *   $Header XXWC.XXWC_AR_LOCKBX_AHH.ctl $
-- *   Module Name: XXWC.XXWC_AR_LOCKBX_AHH.ctl
-- *
-- *   PURPOSE:   This is the control file for loading the staging table XXWC.XXWCAR_CASH_RCPTS_TBL
-- *
-- *   REVISIONS:
-- *   Ver        Date        Author                     Description
-- *   ---------  ----------  ---------------         -------------------------
-- *   1.0        05/04/2018  P.Vamshidhar            TMS#20180503-00140 - Initial Version
-- *   1.1        07/18/2018  P.Vamshidhar            TMS#20180718-00038 - Ctl File Migration
-- * ****************************************************************************

LOAD DATA
INFILE *
INTO TABLE XXWC.XXWCAR_CASH_RCPTS_TBL
APPEND
FIELDS TERMINATED BY '|'
optionally enclosed by '"'
TRAILING NULLCOLS
(
CONTROL_NBR   "APPS.XXWC_AR_INV_STG_TBL_SEQ.NEXTVAL",
DEPOSIT_DATE DATE "MM/DD/RRRR",
OPERATOR_CODE,
CHECK_NBR,
CHECK_AMT,
CUSTOMER_NBR,
INVOICE_NBR,
FILL,
INVOICE_AMT,
DISC_AMT,
GL_ACCT_NBR,
SHORT_PAY_CODE  "RTRIM(:SHORT_PAY_CODE)",
CREATION_DATE	sysdate,
UPDATED_DATE	sysdate,
STATUS		CONSTANT 'NEW',
ABA_NUMBER,
BANK_ACCOUNT_NUM)