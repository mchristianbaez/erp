CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_REBATES_ARCHIVE
-- ESMS TICKET HISTORY
-- Scope: Oracle Trade Management / Rebates accruals and gl journals extract
-- Used by concurrent jobs HDS Rebates: Archive Partitioned Reporting Tables
-- Tables to be archived: XXCUS.XXCUS_OZF_XLA_ACCRUALS_B and XXCUS.XXCUS_OZF_GL_JOURNALS_B
-- Change History:
-- Ticket#               Version  Date          Comment
-- ====================  =======  ============  ============================================
-- TMS 20160316-00196    1.0      04/15/2016    Created.
as
  --
  ln_request_id NUMBER :=0;
  --
  procedure print_log(p_message in varchar2) is
  begin
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||sqlerrm);
  end print_log;
  --
  procedure tm_accruals
  (
    retcode                 out number
   ,errbuf                   out varchar2
   ,p_fiscal_year      in    number
  )  
  is
   --
   l_delete_sql  varchar2(4000) :=Null;
   l_insert_sql  varchar2(10000) :=Null;
   l_proceed boolean :=FALSE;
   --
   l_deleted_count number :=0;
   l_archived_count number :=0;
   --
   l_record_type varchar2(30) :='REBATE_ACCRUAL';
   --
   l_request_id number :=fnd_global.conc_request_id;
   l_user_id number :=fnd_global.user_id;
   --
    cursor fiscal_periods is
    select period_name                                         fiscal_period
               ,substr(upper(period_name), 1, 3) fiscal_month
    from gl_periods
    where 1 =1
    and period_set_name ='4-4-QTR'
    and period_year =p_fiscal_year
    and adjustment_period_flag ='N'
    order by period_year asc, period_num asc; 
   --
  begin
     --
     execute immediate 'ALTER SESSION FORCE PARALLEL DML';
     execute immediate 'ALTER TABLE xxcus.xxcus_ozf_xla_accruals_archive PARALLEL 8';
     execute immediate 'ALTER TABLE xxcus.xxcus_ozf_xla_accruals_b PARALLEL 8';
     --
     -- First insert partition...If everything is successful then delete the same set of data
     --
     for rec in fiscal_periods loop
     --
             begin 
              -- Insert sub set of data based on partition of the year to be archived
              l_insert_sql :='insert /*+ append parallel (INS 8) */ into xxcus.xxcus_ozf_xla_accruals_archive INS'||'
                (
                select /*+ parallel (tmsla 8) */ *
                from xxcus.xxcus_ozf_xla_accruals_b partition ('||rec.fiscal_month||') tmsla '||'
                '||'where 1 =1'||'
                '||'     and tmsla.xaeh_period_name ='||''''||rec.fiscal_period||''''||')';
              --
                   print_log('Insert SQL: ');
                   print_log('==========');   
                   print_log(l_insert_sql); 
                   print_log(' ');  
                  --
                   --
                   print_log ('Begin Delete 2: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                   execute immediate 'begin ' || l_insert_sql || '; :x := sql%rowcount; end;' using OUT l_archived_count;
                   print_log ('End Delete 2: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));     
                  --                  
                 l_proceed :=TRUE;
                 --
                 commit;
                 --
             exception
              when others then
               print_log('Error during insert for fiscal period '||rec.fiscal_period||', message ='||sqlerrm);
               l_proceed :=FALSE;
             end;
             -- Delete sub set of data based on partition of the year to be archived
             if (l_proceed) then 
                --
                 begin 
                    l_delete_sql := 'DELETE /*+ PARALLEL (D 2) */ XXCUS.XXCUS_OZF_XLA_ACCRUALS_B partition ('  --TMS 20160316-00196
                         ||rec.fiscal_month||') D '
                         ||'
                         '||' WHERE 1 =1 '
                           ||'
                           '||' AND D.XAEH_PERIOD_NAME ='||''''||rec.fiscal_period||'''';
                    --
                    --
                       print_log('Delete SQL: ');
                       print_log('==========');   
                       print_log(l_delete_sql); 
                       print_log(' ');  
                       --
                       print_log ('Begin Delete 2: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                       execute immediate 'begin ' || l_delete_sql || '; :x := sql%rowcount; end;' using OUT l_deleted_count;
                       print_log ('End Delete 2: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));     
                      --
                      commit;
                      --
                      begin
                            --
                           savepoint square2; 
                            --
                            insert into xxcus.xxcus_rebates_archive_log 
                            (
                              fiscal_period,
                              delete_sql,
                              insert_sql,
                              request_id,
                              created_by,
                              creation_date,
                              records_archived,                          
                              records_deleted,
                              record_type
                            )    
                           values
                           (
                              rec.fiscal_period,
                              l_delete_sql,
                              l_insert_sql,
                              l_request_id,
                              l_user_id,
                              sysdate,
                              l_archived_count,                          
                              l_deleted_count,
                              l_record_type
                           );           
                      exception
                       when others then
                        rollback to square2;
                        print_log('Error during insert into archive log table  xxcus.xxcus_rebates_archive_log  for fiscal period '||rec.fiscal_period||', message ='||sqlerrm);
                      end;                  
                      --        
                 exception
                   when others then
                      print_log('Error during delete for fiscal period '||rec.fiscal_period||', message ='||sqlerrm);
                  end;
                  --             
             else
              print_log('Error during insert for fiscal period '||rec.fiscal_period||', message ='||sqlerrm);
             end if;
              --         
     end loop;  -- end of cursor fiscal_periods
     --
  exception
   when others then
    print_log( 'Issue in tm_accruals routine ='||sqlerrm);
  end tm_accruals;  
  --
  procedure tm_journals
  (
    retcode                 out number
   ,errbuf                   out varchar2
   ,p_fiscal_year      in    number
  )  
  is
   --
   l_delete_sql  varchar2(4000) :=Null;
   l_insert_sql  varchar2(10000) :=Null;
   l_proceed boolean :=FALSE;
   --
   l_deleted_count number :=0;
   l_archived_count number :=0;
   --
   l_record_type varchar2(30) :='REBATE_JOURNAL';   
   --
   l_request_id number :=fnd_global.conc_request_id;
   l_user_id number :=fnd_global.user_id;
   --
    cursor fiscal_periods is
    select period_name                                         fiscal_period
               ,substr(upper(period_name), 1, 3) fiscal_month
    from gl_periods
    where 1 =1
    and period_set_name ='4-4-QTR'
    and period_year =p_fiscal_year
    and adjustment_period_flag ='N'
    order by period_year asc, period_num asc; 
   --
  begin
     --
     execute immediate 'ALTER SESSION FORCE PARALLEL DML';
     execute immediate 'ALTER TABLE xxcus.xxcus_ozf_gl_journals_archive PARALLEL 8';
     execute immediate 'ALTER TABLE xxcus.xxcus_ozf_gl_journals_b PARALLEL 8';
     --
     -- First insert partition...If everything is successful then delete the same set of data
     --
     for rec in fiscal_periods loop
     --
             begin 
              -- Insert sub set of data based on partition of the year to be archived
              l_insert_sql :='insert /*+ append parallel (INS 8) */ into xxcus.xxcus_ozf_gl_journals_archive INS'||'
                (
                select /*+ parallel (tmgl 8) */ *
                from xxcus.xxcus_ozf_gl_journals_b partition ('||rec.fiscal_month||') tmgl '||'
                '||'where 1 =1'||'
                '||'     and tmgl.gl_period ='||''''||rec.fiscal_period||''''||')';
              --
                   print_log('Insert SQL: ');
                   print_log('==========');   
                   print_log(l_insert_sql); 
                   print_log(' ');  
                  --
                   --
                   print_log ('Begin Insert : Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                   execute immediate 'begin ' || l_insert_sql || '; :x := sql%rowcount; end;' using OUT l_archived_count;
                   print_log ('End Insert : Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));     
                  --                  
                 l_proceed :=TRUE;
                 --
                 commit;
                 --
             exception
              when others then
               print_log('Error during insert for fiscal period '||rec.fiscal_period||', message ='||sqlerrm);
               l_proceed :=FALSE;
             end;
             -- Delete sub set of data based on partition of the year to be archived
             if (l_proceed) then 
                --
                 begin 
                    l_delete_sql := 'DELETE /*+ PARALLEL (D 2) */ xxcus.xxcus_ozf_gl_journals_b partition ('  --TMS 20160316-00196
                         ||rec.fiscal_month||') D '
                         ||'
                         '||' WHERE 1 =1 '
                           ||'
                           '||' AND D.GL_PERIOD ='||''''||rec.fiscal_period||'''';
                    --
                    --
                       print_log('Delete SQL: ');
                       print_log('==========');   
                       print_log(l_delete_sql); 
                       print_log(' ');  
                       --
                       print_log ('Begin Delete : Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                       execute immediate 'begin ' || l_delete_sql || '; :x := sql%rowcount; end;' using OUT l_deleted_count;
                       print_log ('End Delete : Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));     
                      --
                      commit;
                      --
                      begin
                            --
                           savepoint square2; 
                            --
                            insert into xxcus.xxcus_rebates_archive_log 
                            (
                              fiscal_period,
                              delete_sql,
                              insert_sql,
                              request_id,
                              created_by,
                              creation_date,
                              records_archived,                          
                              records_deleted,
                              record_type
                            )    
                           values
                           (
                              rec.fiscal_period,
                              l_delete_sql,
                              l_insert_sql,
                              l_request_id,
                              l_user_id,
                              sysdate,
                              l_archived_count,                          
                              l_deleted_count,
                              l_record_type                                             
                           );           
                      exception
                       when others then
                        rollback to square2;
                        print_log('Error during insert into archive log table  xxcus.xxcus_rebates_archive_log  for fiscal period '||rec.fiscal_period||', message ='||sqlerrm);
                      end;                  
                      --        
                 exception
                   when others then
                      print_log('Error during delete for fiscal period '||rec.fiscal_period||', message ='||sqlerrm);
                  end;
                  --             
             else
              print_log('Error during insert for fiscal period '||rec.fiscal_period||', message ='||sqlerrm);
             end if;
              --         
     end loop;  -- end of cursor fiscal_periods
     --
  exception
   when others then
    print_log( 'Issue in tm_journals routine ='||sqlerrm);
  end tm_journals;  
  --  
end XXCUS_REBATES_ARCHIVE;
/