/*
  @GSC Incident 615950 - Rebates archive table to store fiscal period gl journals data
  WC TMS: 20160316-00196 
  Date: 04/15/2016
*/
DROP TABLE XXCUS.XXCUS_OZF_GL_JOURNALS_ARCHIVE CASCADE CONSTRAINTS;
--
CREATE TABLE XXCUS.XXCUS_OZF_GL_JOURNALS_ARCHIVE
(
  GL_SOURCE                 VARCHAR2(25 BYTE),
  GL_CATEGORY               VARCHAR2(25 BYTE),
  FISCAL_MONTH              VARCHAR2(3 BYTE),
  GL_PERIOD                 VARCHAR2(15 BYTE),
  XLA_ACCRUAL_PERIOD        VARCHAR2(15 BYTE),
  XAEH_GL_XFER_STATUS_CODE  VARCHAR2(30 BYTE),
  GL_SEGMENTS               VARCHAR2(181 BYTE),
  GL_PRODUCT                VARCHAR2(25 BYTE),
  GL_LOCATION               VARCHAR2(25 BYTE),
  GL_ACCOUNT                VARCHAR2(25 BYTE),
  JL_ACCOUNTING_DATE        DATE,
  XAEH_ACCOUNTING_DATE      DATE,
  ACCOUNTING_CLASS_CODE     VARCHAR2(30 BYTE),
  SLA_EVENT_TYPE            VARCHAR2(30 BYTE),
  GL_BATCH_NAME             VARCHAR2(100 BYTE),
  GL_JOURNAL_NAME           VARCHAR2(100 BYTE),
  GL_LINE_NUM               NUMBER(15),
  AMOUNT                    NUMBER,
  SOURCE_DISTRIBUTION_TYPE  VARCHAR2(30 BYTE),
  UTILIZATION_ID            NUMBER(38),
  GL_CCID                   NUMBER(15),
  GL_SL_LINK_TABLE          VARCHAR2(30 BYTE),
  GL_SL_LINK_ID             NUMBER,
  GL_LEDGER_NAME            VARCHAR2(30 BYTE),
  GL_LEDGER_ID              NUMBER(15),
  EVENT_ID                  NUMBER(15),
  ENTITY_ID                 NUMBER(15),
  EVENT_TYPE_CODE           VARCHAR2(30 BYTE),
  AE_HEADER_ID              NUMBER,
  AE_LINE_NUM               NUMBER,
  OFU_MVID_NAME             VARCHAR2(150 BYTE),
  OFU_MVID                  VARCHAR2(150 BYTE),
  XLA_TRANSFER_MODE_CODE    VARCHAR2(1 BYTE),
  ADJUSTMENT_TYPE           VARCHAR2(30 BYTE),
  ADJUSTMENT_TYPE_ID        NUMBER,
  OBJECT_ID                 NUMBER,
  UTIL_ACCTD_AMOUNT         NUMBER,
  UTILIZATION_TYPE          VARCHAR2(30 BYTE),
  OFU_CUST_ACCOUNT_ID       NUMBER,
  QP_LIST_HEADER_ID         NUMBER,
  QP_GLOBAL_FLAG            VARCHAR2(1 BYTE),
  FUND_ID                   NUMBER,
  FUND_NAME                 VARCHAR2(150 BYTE),
  OFU_YEAR_ID               NUMBER,
  REFERENCE_ID              NUMBER,
  AMS_ACTIVITY_BUDGET_ID    NUMBER,
  OFFER_CODE                VARCHAR2(30 BYTE),
  OFFER_NAME                VARCHAR2(2000 BYTE),
  OFFER_TYPE                VARCHAR2(30 BYTE),
  OFFER_STATUS_CODE         VARCHAR2(30 BYTE),
  ACTIVITY_MEDIA_ID         NUMBER,
  ACTIVITY_MEDIA_NAME       VARCHAR2(120 BYTE),
  CALENDAR_YEAR             VARCHAR2(150 BYTE),
  BILL_TO_PARTY_NAME        VARCHAR2(150 BYTE),
  RESALE_DATE_CREATED       DATE,
  DATE_INVOICED             DATE,
  PO_NUMBER                 VARCHAR2(50 BYTE),
  DATE_ORDERED              DATE,
  SELLING_PRICE             NUMBER,
  QUANTITY                  NUMBER
)
NOCOMPRESS 
TABLESPACE XXCUS_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
PARTITION BY LIST (GL_PERIOD)
(  
  PARTITION JAN2012 VALUES ('Jan-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION FEB2012 VALUES ('FEB-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION MAR2012 VALUES ('Mar-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION APR2012 VALUES ('Apr-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION MAY2012 VALUES ('May-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION JUN2012 VALUES ('Jun-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION JUL2012 VALUES ('Jul-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION AUG2012 VALUES ('Aug-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION SEP2012 VALUES ('Sep-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION OCT2012 VALUES ('Oct-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION NOV2012 VALUES ('Nov-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION DEC2012 VALUES ('Dec-2012')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ), 
  PARTITION JAN2013 VALUES ('Jan-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION FEB2013 VALUES ('FEB-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION MAR2013 VALUES ('Mar-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION APR2013 VALUES ('Apr-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION MAY2013 VALUES ('May-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION JUN2013 VALUES ('Jun-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION JUL2013 VALUES ('Jul-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION AUG2013 VALUES ('Aug-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION SEP2013 VALUES ('Sep-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION OCT2013 VALUES ('Oct-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION NOV2013 VALUES ('Nov-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION DEC2013 VALUES ('Dec-2013')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION JAN2014 VALUES ('Jan-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION FEB2014 VALUES ('FEB-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION MAR2014 VALUES ('Mar-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION APR2014 VALUES ('Apr-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION MAY2014 VALUES ('May-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION JUN2014 VALUES ('Jun-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION JUL2014 VALUES ('Jul-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION AUG2014 VALUES ('Aug-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION SEP2014 VALUES ('Sep-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION OCT2014 VALUES ('Oct-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION NOV2014 VALUES ('Nov-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION DEC2014 VALUES ('Dec-2014')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),                               
  PARTITION NA VALUES (DEFAULT)
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE XXCUS_DATA
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
)
NOCACHE
MONITORING;
--
COMMENT ON TABLE XXCUS.XXCUS_OZF_GL_JOURNALS_ARCHIVE IS 'TMS 20160316-00196 / INCIDENT 615950';
-- 