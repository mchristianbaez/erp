CREATE OR REPLACE PACKAGE  BODY   XXEIS.EIS_XXWC_OM_ORDER_EXP_PKG as  
--//============================================================================
--//  
--// Change Request 			:: Performance Issue 
--//
--// Object Usage 				:: This Object Referred by "Orders Exception Report"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_OM_ORDER_EXP_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       Siva  	       05/06/2016   Initial Build   --20160503-00089 --Performance Tuning
--//============================================================================
  
procedure process_order_exception (p_process_id in number,
                                          p_organization in  varchar2,
                                          p_sales_person_name in varchar2
                                          ) as
--//============================================================================
--//
--// Object Name         :: process_order_exception  
--//
--// Object Usage 		 :: This Object Referred by "Orders Exception Report"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_OM_ORDER_EXP_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       05/06/2016   Initial Build   --20160503-00089 --Performance Tuning
--//============================================================================
										  
                                          
LC_WHERE_COND 	varchar2(32000);

type line_holds_sql_rec
IS
  RECORD
( header_id number,
  line_id number,
  exception_date date,
  exception_description VARCHAR2(2000)
);			  		  

type line_holds_sql_rec_tab is table of line_holds_sql_rec Index By Binary_Integer;

type l_order_holds_qry_rec_tab
is
  table of xxeis.EIS_XXWC_ORDER_EXP_TAB%rowtype index by BINARY_INTEGER;
  
type l_counter_order_sql_rec
IS
  RECORD
( process_id number,
  header_id number,
  LINE_ID NUMBER
);

type l_counter_order_sql_rec_tab is table of l_counter_order_sql_rec Index By Binary_Integer;


 ORDER_LINE_HOLDS_SQL_TAB 	      LINE_HOLDS_SQL_REC_TAB;
 L_ORDER_LINE_HOLDS_QRY_TAB       L_ORDER_HOLDS_QRY_REC_TAB;
 L_ORDER_HEADER_HOLDS_QRY_TAB     L_ORDER_HOLDS_QRY_REC_TAB;
 L_OUT_FOR_DELIVERY_QRY_TAB       L_ORDER_HOLDS_QRY_REC_TAB;
 L_OTHER_ORDER_HOLDS_QRY_TAB      L_ORDER_HOLDS_QRY_REC_TAB;
 L_COUNTER_ORDER_SQL_TAB          L_COUNTER_ORDER_SQL_REC_TAB;
 l_internal_order_sql_tab         l_counter_order_sql_rec_tab;
 
 
 
 L_REF_CURSOR1                CURSOR_TYPE4;
 L_LINE_HOLD_REF_CUR          CURSOR_TYPE4;
 L_HEADER_HOLD_REF_CUR        CURSOR_TYPE4;
 L_OUT_FOR_DELIVERY_REF_CUR   CURSOR_TYPE4;
 L_OTHER_ORDER_HOLDS_REF_CUR  CURSOR_TYPE4;
 L_COUNTER_ORDER_REF_CUR      CURSOR_TYPE4;
 L_INTERNAL_ORDER_REF_CUR     CURSOR_TYPE4;
 
 
 
 
 l_line_holds_sql varchar2(32000) :='select  /*+INDEX(ooh OE_ORDER_HOLDS_ALL_N1) INDEX(OHS OE_HOLD_SOURCES_U1,OE_HOLD_SOURCES_N1) USE_NL(ohs,ohd)*/
  ooh.header_id,ooh.line_id,ooh.creation_date exception_date,ohd.description exception_description
FROM apps.oe_order_holds_all ooh ,
  oe_hold_sources ohs ,
  oe_hold_definitions ohd
WHERE ooh.hold_source_id = ohs.hold_source_id
and ooh.hold_release_id is null
AND ohs.hold_id          = ohd.hold_id
and ohd.name not in (''Pricing Guardrail Hold'' ,''Pricing Guardrail Hold - Limited User'' ,''XXWC_PRICE_CHANGE_HOLD'')
';

  
  L_ORDER_LINE_HOLDS_QRY varchar2(32000):='SELECT  /*+ INDEX(oh XXWC_OE_ORDER_HDR_AL_N2) index(ol XXWC_OE_ORDER_LN_AL_N3) */
  '||p_process_id||' process_id,
  mp.organization_code loc ,
  ''Order On Hold'' exception_resaon ,
  oh.order_number order_number ,
  TRUNC (oh.ordered_date) order_date , --OHd.CREATION_DATE EXCEPTION_DATE,
  --  ooh.creation_date exception_date ,
  :exception_date exception_date,
  NVL (hca.account_name, hp.party_name) customer_name ,
  ottl.name order_type ,
  CASE
    WHEN xxeis.eis_rs_xxwc_com_util_pkg.get_line_id (ol.line_id) = 1
    THEN ROUND ( ( ROUND ( DECODE (ol.line_category_code, ''RETURN'', -1, 1) * NVL (ol.ordered_quantity, 0) * NVL (ol.unit_selling_price, 0) ,2) + NVL (ol.tax_value, 0)) ,2)
    ELSE NULL
  END ext_order_total ,
  DECODE (ol.line_category_code, ''RETURN'', -1, 1) * NVL (ol.ordered_quantity, 0) qty ,
  --  ohd.description exception_description ,
  :exception_description exception_description,
  rep.name sales_person_name ,
  xxeis.eis_rs_xxwc_com_util_pkg.get_empolyee_name (fu.user_id, oh.creation_date) created_by ,
  NVL (flv.meaning, ol.shipping_method_code) ship_method
FROM oe_order_headers oh ,
  oe_order_lines ol,
  -- ORG_ORGANIZATION_DEFINITIONS OOD,
  mtl_parameters mp ,
  hz_cust_accounts hca ,
  hz_parties hp ,
  oe_transaction_types_vl ottl,
  --         ,apps.oe_order_holds_all ooh
  --         ,oe_hold_sources ohs
  --         ,oe_hold_definitions ohd
  ra_salesreps rep,
  --  PER_PEOPLE_F PPF,
  fnd_user fu ,
  fnd_lookup_values flv
WHERE oh.header_id           = ol.header_id
AND ol.ship_from_org_id      = mp.organization_id
AND oh.order_type_id         = ottl.transaction_type_id
AND oh.salesrep_id           = rep.salesrep_id(+)
AND oh.org_id                = rep.org_id(+)
AND oh.sold_to_org_id        = hca.cust_account_id
AND ol.flow_status_code NOT IN (''CLOSED'', ''CANCELLED'')
--AND ohd.name NOT            IN (''Pricing Guardrail Hold'' ,''Pricing Guardrail Hold - Limited User'' ,''XXWC_PRICE_CHANGE_HOLD'')
and HCA.PARTY_ID             = HP.PARTY_ID
and OH.HEADER_ID = :header_id
and ol.line_id   = :line_id
  --          AND ooh.header_id = oh.header_id
  --          AND ol.line_id = ooh.line_id(+)
  --          AND ohs.hold_source_id = ooh.hold_source_id
  --          AND ooh.hold_release_id IS NULL
  --          AND ohs.hold_id = ohd.hold_id
AND fu.user_id         = oh.created_by
AND FLV.LOOKUP_TYPE(+) = ''SHIP_METHOD''
AND flv.lookup_code(+) = ol.shipping_method_code';


L_ORDER_HEADER_HOLDS_QRY varchar2(32000):='SELECT  /*+ INDEX(oh XXWC_OE_ORDER_HDR_AL_N2) index(ol XXWC_OE_ORDER_LN_AL_N3) */
  '||p_process_id||' process_id,
  mp.organization_code loc ,
  ''Order On Hold'' exception_resaon ,
  oh.order_number order_number ,
  TRUNC (oh.ordered_date) order_date , --OHd.CREATION_DATE EXCEPTION_DATE,
  --  ooh.creation_date exception_date ,
  :exception_date exception_date,
  NVL (hca.account_name, hp.party_name) customer_name ,
  ottl.name order_type ,
  CASE
    WHEN xxeis.eis_rs_xxwc_com_util_pkg.get_line_id (ol.line_id) = 1
    THEN ROUND ( ( ROUND ( DECODE (ol.line_category_code, ''RETURN'', -1, 1) * NVL (ol.ordered_quantity, 0) * NVL (ol.unit_selling_price, 0) ,2) + NVL (ol.tax_value, 0)) ,2)
    ELSE NULL
  END ext_order_total ,
  DECODE (ol.line_category_code, ''RETURN'', -1, 1) * NVL (ol.ordered_quantity, 0) qty ,
  --  ohd.description exception_description ,
  :exception_description exception_description,
  rep.name sales_person_name ,
  xxeis.eis_rs_xxwc_com_util_pkg.get_empolyee_name (fu.user_id, oh.creation_date) created_by ,
  NVL (flv.meaning, ol.shipping_method_code) ship_method
FROM oe_order_headers oh ,
  oe_order_lines ol , -- ORG_ORGANIZATION_DEFINITIONS OOD,
  mtl_parameters mp ,
  hz_cust_accounts hca ,
  hz_parties hp ,
  oe_transaction_types_vl ottl ,
--  apps.oe_order_holds_all ooh ,
--  oe_hold_sources ohs ,
--  oe_hold_definitions ohd ,
  ra_salesreps rep , --  PER_PEOPLE_F PPF,
  fnd_user fu ,
  fnd_lookup_values flv
WHERE oh.header_id           = ol.header_id
AND ol.ship_from_org_id      = mp.organization_id
AND oh.order_type_id         = ottl.transaction_type_id
AND oh.salesrep_id           = rep.salesrep_id(+)
AND oh.org_id                = rep.org_id(+)
AND oh.sold_to_org_id        = hca.cust_account_id
AND ol.flow_status_code NOT IN (''CLOSED'', ''CANCELLED'')
--AND ohd.name NOT            IN (''Pricing Guardrail Hold'' ,''Pricing Guardrail Hold - Limited User'' ,''XXWC_PRICE_CHANGE_HOLD'')
AND hca.party_id             = hp.party_id
--AND ooh.header_id          = oh.header_id
and oh.header_id			 = :header_id
--AND ooh.line_id             IS NULL
--AND ohs.hold_source_id       = ooh.hold_source_id
--AND ooh.hold_release_id     IS NULL
--AND ohs.hold_id              = ohd.hold_id
AND fu.user_id               = oh.created_by
AND flv.lookup_type(+)       = ''SHIP_METHOD''
AND flv.lookup_code(+)       = ol.shipping_method_code';
 
L_OUT_FOR_DELIVERY_QRY varchar2(32000) := 'SELECT  /*+ INDEX(OL XXWC_OE_ORDER_LN_AL_N15)*/
 '||p_process_id||' process_id,
  mp.organization_code loc ,
  ''Out for Delivery'' exception_resaon ,
  oh.order_number order_number ,
  TRUNC (OH.ORDERED_DATE) ORDER_DATE ,
  --xxeis.eis_rs_xxwc_com_util_pkg.yy_get_doc_date (
  -- oh.header_id,
  --  ol.line_id,
  ---  ol.inventory_item_id,
  --  ol.ship_from_org_id,
  --  oh.ship_from_org_id)
  (
  SELECT creation_date
  FROM apps.xxwc_wsh_shipping_stg
  WHERE header_id = oh.header_id
  AND LINE_ID     = OL.LINE_ID
  ) exception_date, --TMS#20140428-00163 by Mahender on 07-11-2014
  --wdd.creation_date exception_date
  -- XXEIS.eis_rs_xxwc_com_util_pkg.get_doc_date_modified ( oh.header_id, ol.line_id, ol.inventory_item_id, ol.ship_from_org_id, oh.ship_from_org_id) exception_date,
  NVL (hca.account_name, hp.party_name) customer_name ,
  ottl.name order_type ,
  ROUND ( ( ROUND ( DECODE (ol.line_category_code, ''RETURN'', -1, 1) * NVL (ol.ordered_quantity, 0) * NVL (ol.unit_selling_price, 0) ,2) + NVL (ol.tax_value, 0)) ,2) ext_order_total ,
  DECODE (ol.line_category_code, ''RETURN'',                   -1, 1) * NVL (ol.ordered_quantity, 0) qty ,
  ''Item Shipped but Not Confirmed'' exception_description ,
  rep.name sales_person_name ,
  xxeis.eis_rs_xxwc_com_util_pkg.get_empolyee_name (fu.user_id, oh.creation_date) created_by ,
  NVL (flv.meaning, ol.shipping_method_code) ship_method
FROM oe_order_headers oh ,
  oe_order_lines ol ,
  mtl_parameters mp ,
  hz_cust_accounts hca ,
  hz_parties hp ,
  oe_transaction_types_vl ottl ,
  RA_SALESREPS REP ,
  -- PER_PEOPLE_F PPF,
  fnd_user fu ,
  fnd_lookup_values flv ,
  wsh_delivery_details wdd ,
  wsh_delivery_assignments wda
WHERE oh.header_id           = ol.header_id
AND ol.ship_from_org_id      = mp.organization_id
AND oh.order_type_id         = ottl.transaction_type_id
AND OH.SALESREP_ID           = REP.SALESREP_ID(+)
AND oh.org_id                = rep.org_id(+)
AND oh.sold_to_org_id        = hca.cust_account_id
AND hca.party_id             = hp.party_id
AND fu.user_id               = oh.created_by
AND flv.lookup_type(+)       = ''SHIP_METHOD''
AND flv.lookup_code(+)       = ol.shipping_method_code
AND (ol.split_from_line_id  IS NULL
AND user_item_description   != ''BACKORDERED'')
AND ol.flow_status_code NOT IN (''CLOSED'', ''CANCELLED'')
AND ottl.name               IN (''STANDARD ORDER'', ''RETURN ORDER'', ''REPAIR ORDER'')
AND ol.user_item_description = ''OUT_FOR_DELIVERY''
AND wdd.source_header_id     = ol.header_id
AND wdd.source_line_id       = ol.line_id
AND wdd.inventory_item_id    = ol.inventory_item_id
AND wdd.organization_id      = ol.ship_from_org_id
AND wda.delivery_detail_id   = wdd.delivery_detail_id
AND ( (sysdate - WDD.CREATION_DATE) > 1)'; 

L_COUNTER_ORDER_SQL VARCHAR2(32000) :='SELECT
  '||p_process_id||' process_id,ohe.header_id,ole.line_id
  FROM oe_order_headers ohe ,
    oe_order_lines ole ,
    apps.oe_order_holds_all ooh ,
    oe_hold_sources ohs ,
    oe_hold_definitions ohd,
    oe_transaction_types_vl ottl
  WHERE ohe.header_id = ole.header_id
  AND ooh.header_id   = ohe.header_id
  AND ohs.hold_source_id   = ooh.hold_source_id
  AND ooh.hold_release_id IS NULL
  AND ohs.hold_id          = ohd.hold_id
  AND ohe.order_type_id    = ottl.transaction_type_id
  AND OTTL.NAME            = ''COUNTER ORDER''
  ';
L_internal_order_sql varchar2(32000) :='SELECT
'||p_process_id||' process_id,OHE.HEADER_ID,ole.line_id 
  FROM OE_ORDER_HEADERS OHE ,
       oe_order_lines ole ,
      APPS.OE_ORDER_HOLDS_ALL OOH ,
       OE_HOLD_SOURCES OHS ,
       OE_HOLD_DEFINITIONS OHD
  WHERE OHE.HEADER_ID      = OLE.HEADER_ID
  AND ooh.header_id        = ohe.header_id
  AND ohs.hold_source_id   = ooh.hold_source_id
  AND ooh.hold_release_id IS NULL
  AND ohs.hold_id          = ohd.hold_id
  AND OHD.NAME            IN (''Pricing Guardrail Hold'' ,''Pricing Guardrail Hold - Limited User'' ,''XXWC_PRICE_CHANGE_HOLD'')
  AND OLE.SOURCE_TYPE_CODE = ''INTERNAL''
  ';
  
L_OTHER_ORDER_HOLDS_QRY VARCHAR2(32000) :='SELECT 
           '||p_process_id||' process_id,
         mp.organization_code loc
         ,CASE
             WHEN ol.flow_status_code = ''INVOICE_HOLD''               /* Invoice on hold added on 03-oct-2013*/
             THEN
                ''Invoice on Hold''
             WHEN ottl.name = ''STANDARD ORDER'' AND ol.flow_status_code = ''AWAITING_RECEIPT''
             THEN
                ''Open Direct''
             WHEN ottl.name = ''RETURN ORDER'' AND ol.flow_status_code = ''AWAITING_RETURN''
             THEN
                ''Open RMA''
             WHEN ol.flow_status_code = ''PRE-BILLING_ACCEPTANCE''
             THEN
                ''Pending Pre Billing''
             WHEN (ottl.name = ''COUNTER ORDER'' AND ol.flow_status_code <> ''ENTERED'') -- THEN ''Booked Not Closed''
             THEN
                ''Counter Order > 1- Day''
             WHEN (ottl.name = ''COUNTER ORDER'' AND ol.flow_status_code = ''ENTERED'')
             THEN
                ''Entered Not Booked''
             ELSE
                NULL
          END
             exception_resaon
         ,oh.order_number order_number
         ,TRUNC (oh.ordered_date) order_date
         ,CASE
             WHEN ol.flow_status_code = ''INVOICE_HOLD''
             /* Invoice on hold added on 03-oct-2013*/
             THEN
                TRUNC (oh.ordered_date)
             WHEN ottl.name = ''STANDARD ORDER'' AND ol.flow_status_code = ''AWAITING_RECEIPT''
             THEN
                TRUNC (oh.ordered_date)
             WHEN ottl.name = ''RETURN ORDER'' AND ol.flow_status_code = ''AWAITING_RETURN''
             THEN
                TRUNC (oh.ordered_date)
             WHEN ol.flow_status_code = ''PRE-BILLING_ACCEPTANCE''
             THEN
                TRUNC (ol.actual_shipment_date)
             WHEN (ottl.name = ''COUNTER ORDER'' AND ol.flow_status_code <> ''ENTERED'')
             THEN
                TRUNC (oh.ordered_date) + 1
             WHEN (ottl.name = ''COUNTER ORDER'' AND ol.flow_status_code = ''ENTERED'')
             THEN
                TRUNC (oh.ordered_date)
             ELSE
                NULL
          END
             exception_date
         ,NVL (hca.account_name, hp.party_name) customer_name
         ,ottl.name order_type
         ,ROUND (
             (  ROUND (
                     DECODE (ol.line_category_code, ''RETURN'', -1, 1)
                   * NVL (ol.ordered_quantity, 0)
                   * NVL (ol.unit_selling_price, 0)
                  ,2)
              + NVL (ol.tax_value, 0))
            ,2)
             ext_order_total
         ,DECODE (ol.line_category_code, ''RETURN'', -1, 1) * NVL (ol.ordered_quantity, 0) qty
         ,CASE
             WHEN ol.flow_status_code = ''INVOICE_HOLD''
             /* Invoice on hold added on 03-oct-2013*/
             THEN
                ''Invoice Hold Applied - Review for Release''
             WHEN ottl.name = ''STANDARD ORDER'' AND ol.flow_status_code = ''AWAITING_RECEIPT''
             THEN
                ''Direct Ship waiting to be Received''
             WHEN ottl.name = ''RETURN ORDER'' AND ol.flow_status_code = ''AWAITING_RETURN''
             THEN
                ''RMA waiting to be Received''
             WHEN ol.flow_status_code = ''PRE-BILLING_ACCEPTANCE''
             THEN
                ''Item waiting on Fulfillment Acceptance''
             WHEN (ottl.name = ''COUNTER ORDER'' AND ol.flow_status_code <> ''ENTERED'')
             THEN
                ''Counter Order Needs Review''
             WHEN (ottl.name = ''COUNTER ORDER'' AND ol.flow_status_code = ''ENTERED'')
             THEN
                ''Needs to be Reviewed and Booked''
             ELSE
                NULL
          END
             exception_description
         ,rep.name sales_person_name
         ,xxeis.eis_rs_xxwc_com_util_pkg.get_empolyee_name (fu.user_id, oh.creation_date) created_by
         ,NVL (flv.meaning, ol.shipping_method_code) ship_method
     FROM oe_order_headers oh
         ,oe_order_lines ol
         ,mtl_parameters mp
         ,hz_cust_accounts hca
         ,hz_parties hp
         ,oe_transaction_types_vl ottl
         ,ra_salesreps rep
         ,                                                                                -- PER_PEOPLE_F PPF,
          fnd_user fu
         ,fnd_lookup_values flv
    WHERE     oh.header_id = ol.header_id
          AND ol.ship_from_org_id = mp.organization_id
          AND oh.order_type_id = ottl.transaction_type_id
          AND oh.salesrep_id = rep.salesrep_id(+)                                                           --(+)
          AND oh.org_id = rep.org_id(+)                                                                     --(+)
          AND oh.sold_to_org_id = hca.cust_account_id
          AND hca.party_id = hp.party_id
          AND fu.user_id = oh.created_by
          AND flv.lookup_type(+) = ''SHIP_METHOD''
          AND flv.lookup_code(+) = ol.shipping_method_code
          --and fu.employee_id      = ppf.person_id(+)
          -- AND TRUNC (oh.creation_date) BETWEEN NVL (ppf.effective_start_date, TRUNC (oh.creation_date) ) AND NVL (ppf.effective_end_date, TRUNC (oh.creation_date) )
          AND ol.flow_status_code NOT IN (''CLOSED'', ''CANCELLED'')
          AND ottl.name IN (''STANDARD ORDER''
                           ,''COUNTER ORDER''
                           ,''RETURN ORDER''
                           ,''REPAIR ORDER'')
          AND (   (    ottl.name != ''COUNTER ORDER''
                   AND ol.flow_status_code IN (''AWAITING_RECEIPT''
                                              ,''AWAITING_RETURN''
                                              ,''PRE-BILLING_ACCEPTANCE''
                                              ,''INVOICE_HOLD''))          ----added ''INVOICE_HOLD'' on 9/26/2013
               OR (ottl.name = ''COUNTER ORDER'' AND TRUNC (SYSDATE) > TRUNC (ordered_date)))
          and NOT EXISTS(select 1 from XXEIS.EIS_XXWC_CNTR_ORD_TAB ecot where
                            ecot.header_id = ol.header_id
                           AND ecot.line_id = ol.line_id
                           AND ecot.process_id = '||p_process_id||')
          and NOT EXISTS(select 1 from XXEIS.EIS_XXWC_INT_ORD_TAB eiot where
                            eiot.header_id = ol.header_id
                           AND eiot.line_id   = ol.line_id
                           AND eiot.process_id = '||p_process_id||')                          
          /*  Counter Orders that are not closed, due to a Hold, should only display once under Order On Hold*/
         /* AND NOT EXISTS
                     (SELECT DISTINCT ooh.header_id
                        FROM oe_order_headers ohe
                            ,oe_order_lines ole
                            ,apps.oe_order_holds_all ooh
                            ,oe_hold_sources ohs
                            ,oe_hold_definitions ohd
                            ,oe_transaction_types_vl ottl
                       WHERE     ohe.header_id = ole.header_id
                             AND ohe.header_id = oh.header_id
                             AND ole.line_id = ol.line_id
                             AND ooh.header_id = ohe.header_id
                             --and OLE.LINE_ID           = OOH.LINE_ID(+)
                             AND ohs.hold_source_id = ooh.hold_source_id
                             AND ooh.hold_release_id IS NULL
                             AND ohs.hold_id = ohd.hold_id
                             AND ohe.order_type_id = ottl.transaction_type_id
                             AND ottl.name = ''COUNTER ORDER'')
								--Start Ver 1.1
            AND NOT EXISTS
                     (SELECT DISTINCT ooh.header_id
                        FROM oe_order_headers ohe
                            ,oe_order_lines ole
                            ,apps.oe_order_holds_all ooh
                            ,oe_hold_sources ohs
                            ,OE_HOLD_DEFINITIONS OHD
                       WHERE     OHE.HEADER_ID = OLE.HEADER_ID
                             AND OHE.HEADER_ID = oh.header_id
                             AND ole.line_id = ol.line_id
                             AND ooh.header_id = ohe.header_id
                             AND ohs.hold_source_id = ooh.hold_source_id
                             AND ooh.hold_release_id IS NULL
                             AND ohs.hold_id = ohd.hold_id
                             AND ohd.name IN (''Pricing Guardrail Hold''
                              ,''Pricing Guardrail Hold - Limited User''
                              ,''XXWC_PRICE_CHANGE_HOLD'')
                             AND ole.SOURCE_TYPE_CODE = ''INTERNAL''   --Added for Ver 1.2
                              ) 								--End Ver 1.1		*/
			';
 
 begin


   IF P_ORGANIZATION      IS NOT NULL THEN
   IF P_ORGANIZATION='All'
   THEN
   LC_WHERE_COND:= LC_WHERE_COND||' and 1=1';
   else
    lc_where_cond:= lc_where_cond||' and mp.organization_code in ('||xxeis.eis_rs_utility.get_param_values(p_organization)||' )';
  END IF;
   END IF;
  
     IF p_sales_person_name      IS NOT NULL THEN
    lc_where_cond:= lc_where_cond||' and rep.name in ('||xxeis.eis_rs_utility.get_param_values(p_sales_person_name)||' )';
  END IF;
  
 order_line_holds_sql_tab.delete;
 L_ORDER_LINE_HOLDS_QRY_TAB.DELETE;
 L_ORDER_HEADER_HOLDS_QRY_TAB.DELETE;
 L_OUT_FOR_DELIVERY_QRY_TAB.DELETE;
 L_OTHER_ORDER_HOLDS_QRY_TAB.DELETE;
 l_internal_order_sql_tab.delete;
 L_COUNTER_ORDER_SQL_TAB.DELETE;
 
FND_FILE.PUT_LINE(FND_FILE.LOG,'l_line_holds_sql start'||To_Char(Sysdate,'DD-MON-YY HH24:MI:SS'));

  OPEN L_REF_CURSOR1 FOR l_line_holds_sql;
    FETCH L_REF_CURSOR1 BULK COLLECT INTO order_line_holds_sql_tab;
  CLOSE L_REF_CURSOR1;

FND_FILE.PUT_LINE(FND_FILE.LOG,'l_line_holds_sql END'||To_Char(Sysdate,'DD-MON-YY HH24:MI:SS'));  
 
 L_ORDER_LINE_HOLDS_QRY    := L_ORDER_LINE_HOLDS_QRY||' '||LC_WHERE_COND;  
 L_ORDER_HEADER_HOLDS_QRY  := L_ORDER_HEADER_HOLDS_QRY||' '||LC_WHERE_COND;
 L_OUT_FOR_DELIVERY_QRY    := L_OUT_FOR_DELIVERY_QRY||' '||LC_WHERE_COND;
 L_OTHER_ORDER_HOLDS_QRY   := L_OTHER_ORDER_HOLDS_QRY||' '||LC_WHERE_COND;
 

 
FND_FILE.PUT_LINE(FND_FILE.LOG,'l_order_line_holds_qry start'||To_Char(Sysdate,'DD-MON-YY HH24:MI:SS'));
 
IF order_line_holds_sql_tab.COUNT>0
  THEN
  FOR I IN 1..order_line_holds_sql_tab.COUNT
  LOOP
   OPEN L_line_hold_ref_cur FOR l_order_line_holds_qry USING 
   order_line_holds_sql_tab(i).exception_date,
   order_line_holds_sql_tab(i).exception_description,
   order_line_holds_sql_tab(i).header_id,
   ORDER_LINE_HOLDS_SQL_TAB(I).LINE_ID; 
   
   
  loop
  FETCH L_LINE_HOLD_REF_CUR bulk collect into L_ORDER_LINE_HOLDS_QRY_TAB limit 10000; 
  
  if L_ORDER_LINE_HOLDS_QRY_TAB.COUNT>0
   
	Then  
		FORALL J IN 1..l_order_line_holds_qry_tab.COUNT 
		Insert Into XXEIS.EIS_XXWC_ORDER_EXP_TAB Values l_order_line_holds_qry_tab(J);
  END IF;
		exit when L_line_hold_ref_cur%notfound;
  END LOOP;  -- l_order_line_holds_qry_tab
  commit;  
  close L_line_hold_ref_cur;
  END LOOP; -- order_line_holds_sql_tab
END IF; 
   
FND_FILE.PUT_LINE(FND_FILE.LOG,'l_order_line_holds_qry end'||To_Char(Sysdate,'DD-MON-YY HH24:MI:SS'));

FND_FILE.PUT_LINE(FND_FILE.LOG,'l_order_header_holds_qry  start'||To_Char(Sysdate,'DD-MON-YY HH24:MI:SS'));
  
IF order_line_holds_sql_tab.COUNT>0
  THEN
  FOR i IN 1..order_line_holds_sql_tab.COUNT
  LOOP
  if order_line_holds_sql_tab(i).line_id is null then
   OPEN L_header_hold_ref_cur FOR l_order_header_holds_qry USING 
   order_line_holds_sql_tab(i).exception_date,
   order_line_holds_sql_tab(I).EXCEPTION_DESCRIPTION,
   order_line_holds_sql_tab(i).header_id;
  loop
  FETCH L_HEADER_HOLD_REF_CUR bulk collect into L_ORDER_HEADER_HOLDS_QRY_TAB limit 10000;
   if l_order_header_holds_qry_tab.COUNT>0
	Then  
		FORALL J IN 1..l_order_header_holds_qry_tab.COUNT 
		Insert Into XXEIS.EIS_XXWC_ORDER_EXP_TAB Values l_order_header_holds_qry_tab(J);
  END IF;
		exit when L_header_hold_ref_cur%notfound;
  END LOOP;  -- l_order_header_holds_qry_tab
  commit;  
  close L_HEADER_HOLD_REF_CUR;
  end if;
  END LOOP; -- order_header_holds_sql_tab
END IF; 

FND_FILE.PUT_LINE(FND_FILE.LOG,'l_order_header_holds_qry end'||To_Char(Sysdate,'DD-MON-YY HH24:MI:SS'));

FND_FILE.PUT_LINE(FND_FILE.LOG,'L_OUT_FOR_DELIVERY_QRY Start'||To_Char(Sysdate,'DD-MON-YY HH24:MI:SS'));

open L_OUT_FOR_DELIVERY_REF_CUR for L_OUT_FOR_DELIVERY_QRY;
LOOP
  FETCH L_OUT_FOR_DELIVERY_REF_CUR bulk collect into L_OUT_FOR_DELIVERY_QRY_TAB limit 10000;
   if L_OUT_FOR_DELIVERY_QRY_TAB.COUNT>0
	Then  
		FORALL K in 1..L_OUT_FOR_DELIVERY_QRY_TAB.COUNT 
		insert into XXEIS.EIS_XXWC_ORDER_EXP_TAB values L_OUT_FOR_DELIVERY_QRY_TAB(k);
  commit;
  END IF;
IF L_OUT_FOR_DELIVERY_REF_CUR%NOTFOUND Then
  close L_OUT_FOR_DELIVERY_REF_CUR;
EXIT;
end if;
END LOOP;

FND_FILE.PUT_LINE(FND_FILE.LOG,'L_OUT_FOR_DELIVERY_QRY END'||To_Char(Sysdate,'DD-MON-YY HH24:MI:SS'));


FND_FILE.PUT_LINE(FND_FILE.LOG,'L_INTERNAL_ORDER_SQL Start'||To_Char(Sysdate,'DD-MON-YY HH24:MI:SS'));

OPEN L_INTERNAL_ORDER_REF_CUR FOR L_INTERNAL_ORDER_SQL;
loop
  FETCH L_INTERNAL_ORDER_REF_CUR bulk collect into l_internal_order_sql_tab limit 10000;
   if l_internal_order_sql_tab.COUNT>0
	Then  
		FORALL i in 1..l_internal_order_sql_tab.COUNT 
		insert into XXEIS.EIS_XXWC_INT_ORD_TAB values l_internal_order_sql_tab(i);
  commit;
  END IF;
IF L_INTERNAL_ORDER_REF_CUR%NOTFOUND Then    
CLOSE L_INTERNAL_ORDER_REF_CUR;
EXIT;
END IF;
end LOOP;
FND_FILE.PUT_LINE(FND_FILE.LOG,'L_INTERNAL_ORDER_SQL END'||To_Char(Sysdate,'DD-MON-YY HH24:MI:SS'));

FND_FILE.PUT_LINE(FND_FILE.LOG,'L_COUNTER_ORDER_SQL Start'||To_Char(Sysdate,'DD-MON-YY HH24:MI:SS'));

OPEN L_COUNTER_ORDER_REF_CUR FOR L_COUNTER_ORDER_SQL;
loop
  FETCH L_COUNTER_ORDER_REF_CUR bulk collect into L_COUNTER_ORDER_SQL_TAB limit 10000;
   if L_COUNTER_ORDER_SQL_TAB.COUNT>0
	Then  
		FORALL i in 1..L_COUNTER_ORDER_SQL_TAB.COUNT 
		insert into XXEIS.EIS_XXWC_CNTR_ORD_TAB values L_COUNTER_ORDER_SQL_TAB(i);
  commit;
  END IF;
IF L_COUNTER_ORDER_REF_CUR%NOTFOUND Then    
CLOSE L_COUNTER_ORDER_REF_CUR;
EXIT;
END IF;
END LOOP;
FND_FILE.PUT_LINE(FND_FILE.LOG,'L_COUNTER_ORDER_SQL End'||To_Char(Sysdate,'DD-MON-YY HH24:MI:SS'));

FND_FILE.PUT_LINE(FND_FILE.LOG,'L_OTHER_ORDER_HOLDS_QRY Start'||To_Char(Sysdate,'DD-MON-YY HH24:MI:SS'));

open L_OTHER_ORDER_HOLDS_REF_CUR for L_OTHER_ORDER_HOLDS_QRY;
LOOP
  FETCH L_OTHER_ORDER_HOLDS_REF_CUR bulk collect into L_OTHER_ORDER_HOLDS_QRY_TAB limit 10000;
   if L_OTHER_ORDER_HOLDS_QRY_TAB.COUNT>0
	Then  
		FORALL i in 1..L_OTHER_ORDER_HOLDS_QRY_TAB.COUNT 
		insert into XXEIS.EIS_XXWC_ORDER_EXP_TAB values L_OTHER_ORDER_HOLDS_QRY_TAB(i);
  commit;
  END IF;
IF L_OTHER_ORDER_HOLDS_REF_CUR%NOTFOUND Then
  CLOSE L_OTHER_ORDER_HOLDS_REF_CUR;
EXIT;
END IF;
end LOOP;

FND_FILE.PUT_LINE(FND_FILE.LOG,'L_OTHER_ORDER_HOLDS_QRY End'||To_Char(Sysdate,'DD-MON-YY HH24:MI:SS'));

EXCEPTION
      WHEN OTHERS
      THEN
     FND_FILE.PUT_LINE(FND_FILE.LOG,'Exception....'||SQLERRM||SQLCODE);
end;	 

PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER)
AS
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       05/06/2016   Initial Build   --20160503-00089 --Performance Tuning
--//============================================================================ 
  begin  
  delete from XXEIS.EIS_XXWC_ORDER_EXP_TAB where PROCESS_ID=P_PROCESS_ID;
  delete from XXEIS.EIS_XXWC_INT_ORD_TAB   where PROCESS_ID=P_PROCESS_ID;
  DELETE FROM XXEIS.EIS_XXWC_CNTR_ORD_TAB WHERE PROCESS_ID=P_PROCESS_ID;
  COMMIT;
END ;                                        
End;
/
