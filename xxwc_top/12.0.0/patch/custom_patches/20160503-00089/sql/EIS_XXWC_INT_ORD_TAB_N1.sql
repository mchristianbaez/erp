-----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header EIS_XXWC_INT_ORD_TAB_N1
  File Name: EIS_XXWC_INT_ORD_TAB_N1.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        06-MAY-2016  SIVA        TMS#20160503-00089 Orders Exception Report - Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX XXEIS.EIS_XXWC_INT_ORD_TAB_N1 ON XXEIS.EIS_XXWC_INT_ORD_TAB (PROCESS_ID,HEADER_ID) TABLESPACE APPS_TS_TX_DATA
/
