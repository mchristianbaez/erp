--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_ORDER_EXP_TAB
  Description: This table is used to get data from XXEIS.EIS_XXWC_OM_ORDER_EXP_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     06-May-2016        Siva   		TMS#20160503-00089  Performance Tuning
********************************************************************************/
 CREATE TABLE XXEIS.EIS_XXWC_ORDER_EXP_TAB 
   (PROCESS_ID NUMBER, 
	LOC VARCHAR2(3 BYTE), 
	EXCEPTION_RESAON VARCHAR2(22 BYTE), 
	ORDER_NUMBER NUMBER, 
	ORDER_DATE DATE, 
	EXCEPTION_DATE DATE, 
	CUSTOMER_NAME VARCHAR2(360 BYTE), 
	ORDER_TYPE VARCHAR2(30 BYTE), 
	EXT_ORDER_TOTAL NUMBER, 
	QTY NUMBER, 
	EXCEPTION_DESCRIPTION VARCHAR2(2000 BYTE), 
	SALES_PERSON_NAME VARCHAR2(360 BYTE), 
	CREATED_BY VARCHAR2(4000 BYTE), 
	SHIP_METHOD VARCHAR2(80 BYTE)
)
/
