CREATE OR REPLACE package   XXEIS.EIS_XXWC_OM_ORDER_EXP_PKG as 
--//============================================================================
--//  
--// Change Request 			:: Performance Issue 
--//
--// Object Usage 				:: This Object Referred by "Orders Exception Report"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_OM_ORDER_EXP_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       Siva  	       05/06/2016   Initial Build   --20160503-00089 --Performance Tuning
--//============================================================================
   
 type CURSOR_TYPE4 is ref cursor;
procedure process_order_exception (p_process_id in number,
                                          p_organization in  varchar2,
                                          p_sales_person_name in varchar2
                                          ); 
--//============================================================================
--//
--// Object Name         :: process_order_exception  
--//
--// Object Usage 		 :: This Object Referred by "Orders Exception Report"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_OM_ORDER_EXP_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       05/06/2016   Initial Build   --20160503-00089 --Performance Tuning
--//============================================================================
										  
procedure clear_temp_tables ( p_process_id in number);  
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       05/06/2016   Initial Build   --20160503-00089 --Performance Tuning
--//============================================================================ 
                                       
End;
/