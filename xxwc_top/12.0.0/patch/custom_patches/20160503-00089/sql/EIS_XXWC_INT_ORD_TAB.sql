--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_INT_ORD_TAB
  Description: This table is used to get data from XXEIS.EIS_XXWC_OM_ORDER_EXP_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     06-May-2016        SIVA   		TMS#20160503-00089   Performance Tuning
********************************************************************************/
CREATE TABLE XXEIS.EIS_XXWC_INT_ORD_TAB 
   (PROCESS_ID NUMBER, 
	HEADER_ID NUMBER, 
	LINE_ID NUMBER) 
/
