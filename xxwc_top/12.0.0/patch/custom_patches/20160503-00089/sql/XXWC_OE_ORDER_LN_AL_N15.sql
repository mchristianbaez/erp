-----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_OE_ORDER_LN_AL_N15
  File Name: XXWC_OE_ORDER_LN_AL_N15.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        06-MAY-2016  SIVA        TMS#20160503-00089 Orders Exception Report - Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX XXEIS.XXWC_OE_ORDER_LN_AL_N15 ON XXEIS.OE_ORDER_LINES_ALL (USER_ITEM_DESCRIPTION) TABLESPACE APPS_TS_TX_DATA
/
