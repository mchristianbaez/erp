------------------------------------------------------------------------------------
/******************************************************************************
   NAME       :  XXEIS.EIS_XXWC_OM_ORDER_EXP_TEST_V
   PURPOSE    :  Replaced the old view definition with new view.This view is created based on 
				 custom table XXEIS.EIS_XXWC_ORDER_EXP_TAB and this custom table data
				 gets populated by XXEIS.EIS_XXWC_OM_ORDER_EXP_PKG package.
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0		 06-May-2016   Siva  			--TMS#20160503-00089  Performance Tuning                                         
******************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_OM_ORDER_EXP_TEST_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_OM_ORDER_EXP_TEST_V (PROCESS_ID, LOC, EXCEPTION_RESAON, ORDER_NUMBER, ORDER_DATE, EXCEPTION_DATE, CUSTOMER_NAME, ORDER_TYPE, EXT_ORDER_TOTAL, QTY, EXCEPTION_DESCRIPTION, SALES_PERSON_NAME, CREATED_BY, SHIP_METHOD)
AS
  SELECT process_id,
    LOC,
    EXCEPTION_RESAON,
    ORDER_NUMBER,
    ORDER_DATE,
    EXCEPTION_DATE,
    CUSTOMER_NAME,
    ORDER_TYPE,
    EXT_ORDER_TOTAL,
    QTY,
    EXCEPTION_DESCRIPTION,
    SALES_PERSON_NAME,
    CREATED_BY,
    SHIP_METHOD
  FROM XXEIS.EIS_XXWC_ORDER_EXP_TAB
/
