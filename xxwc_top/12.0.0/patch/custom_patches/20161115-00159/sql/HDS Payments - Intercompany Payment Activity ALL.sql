--Report Name            : HDS Payments - Intercompany Payment Activity ALL
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_1011488_EIALOG_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1011488_EIALOG_V
xxeis.eis_rsc_ins.v( 'XXEIS_1011488_EIALOG_V',682,'Paste SQL View for HDS Payments - Intercompany Payment Activity ALL','1.0','','','DV003828','APPS','HDS Payments - Intercompany Payment Activity ALL View','X1EV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_1011488_EIALOG_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1011488_EIALOG_V',682,FALSE);
--Inserting Object Columns for XXEIS_1011488_EIALOG_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','DOCUMENT_TYPE',682,'','','','','','DV003828','VARCHAR2','','','Document Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','MVID',682,'','','','','','DV003828','VARCHAR2','','','Mvid','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','MVID_NAME',682,'','','','','','DV003828','VARCHAR2','','','Mvid Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','BUSINESS_UNIT',682,'','','','','','DV003828','VARCHAR2','','','Business Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','DOCUMENT_NUMBER',682,'','','','','','DV003828','VARCHAR2','','','Document Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','PAYMENT_TYPE',682,'','','','','','DV003828','VARCHAR2','','','Payment Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','DEPOSIT_DATE',682,'','','','','','DV003828','DATE','','','Deposit Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','DOCUMENT_DATE',682,'','','','','','DV003828','DATE','','','Document Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','AMOUNT',682,'','','','~T~D~2','','DV003828','NUMBER','','','Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','GL_DATE',682,'','','','','','DV003828','DATE','','','Gl Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','IMAGE_URL',682,'','','','','','DV003828','VARCHAR2','','','Image Url','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','TIMEFRAME',682,'','','','','','DV003828','VARCHAR2','','','Timeframe','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','CREATION_DATE',682,'','','','','','DV003828','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','USER_NAME',682,'','','','','','DV003828','VARCHAR2','','','User Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','LAST_UPDATE_DATE',682,'','','','','','DV003828','DATE','','','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','CURRENCY_CODE',682,'','','','','','DV003828','VARCHAR2','','','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','ORG_NAME',682,'','','','','','DV003828','VARCHAR2','','','Org Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','STATUS',682,'','','','','','DV003828','VARCHAR2','','','Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','REVERSAL_REASON',682,'','','','','','DV003828','VARCHAR2','','','Reversal Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','TRANSF_ADJ_TYPE',682,'','','','','','DV003828','VARCHAR2','','','Transf Adj Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1011488_EIALOG_V','REBATE_TYPE',682,'','','','','','DV003828','VARCHAR2','','','Rebate Type','','','','US');
--Inserting Object Components for XXEIS_1011488_EIALOG_V
--Inserting Object Component Joins for XXEIS_1011488_EIALOG_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report LOV Data for HDS Payments - Intercompany Payment Activity ALL
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS Payments - Intercompany Payment Activity ALL
xxeis.eis_rsc_ins.lov( 682,'SELECT distinct hp.party_name LOB
 FROM apps.hz_parties hp
WHERE  hp.attribute1=''HDS_BU''
and ( hp.party_name in (''ELECTRICAL'',''UTILISERV'')
      or hp.status =''A'')','','HDS BU','BU FROM apps.hz_parties it''s party_name = LOB BUSINESS UNIT','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report Data for HDS Payments - Intercompany Payment Activity ALL
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS Payments - Intercompany Payment Activity ALL
xxeis.eis_rsc_utility.delete_report_rows( 'HDS Payments - Intercompany Payment Activity ALL' );
--Inserting Report - HDS Payments - Intercompany Payment Activity ALL
xxeis.eis_rsc_ins.r( 682,'HDS Payments - Intercompany Payment Activity ALL','','Shows all Intercompany payments
','','','','KP059700','XXEIS_1011488_EIALOG_V','Y','','SELECT DISTINCT 
      decode(CRH.STATUS,''REVERSED'',''REVERSAL'',''PAYMENT'') Document_Type, 
      HCA.ATTRIBUTE2 MVID,
      HP.PARTY_NAME MVID_NAME,
      HP1.PARTY_NAME BUSINESS_UNIT,
      CR.RECEIPT_NUMBER DOCUMENT_NUMBER,
      RM.PRINTED_NAME PAYMENT_TYPE,
      null Transf_Adj_Type,
      CR.DEPOSIT_DATE,
      CR.RECEIPT_DATE DOCUMENT_DATE,
      DECODE(CRH.STATUS,''REVERSED'',CR.AMOUNT*-1,CR.AMOUNT) AMOUNT,
      TRUNC(DECODE(CR.STATUS,''REV'',CRH.GL_DATE,SCH.GL_DATE)) GL_DATE,
      CR.COMMENTS IMAGE_URL,
      CR.ATTRIBUTE2 TIMEFRAME,
      CR.ATTRIBUTE10 REBATE_TYPE,
      CR.CREATION_DATE CREATION_DATE,
      USR.USER_NAME USER_NAME,
      CR.LAST_UPDATE_DATE,
      CR.CURRENCY_CODE CURRENCY_CODE,
      HR.NAME ORG_NAME,
      CR.STATUS STATUS,
      CR.REVERSAL_COMMENTS REVERSAL_REASON
        
    
 FROM  
       APPS.AR_CASH_RECEIPTS_ALL CR ,
      AR.AR_CASH_RECEIPT_HISTORY_ALL CRH,
      AR.AR_RECEIVABLE_APPLICATIONS_ALL RA,
      APPS.AR_RECEIPT_METHODS RM,
      APPS.HZ_PARTIES HP,
      APPS.HZ_CUST_ACCOUNTS HCA,
      APPS.hz_parties hp1,
      APPS.HR_OPERATING_UNITS HR,
      APPS.AR_PAYMENT_SCHEDULES_ALL SCH,
      APPS.AR_RECEIVABLES_TRX_ALL         ART ,
      APPS.FND_USER USR
      
WHERE    1 = 1
      AND CR.ATTRIBUTE1                  = TO_CHAR(HP1.PARTY_ID)
      AND CR.RECEIPT_METHOD_ID           = RM.RECEIPT_METHOD_ID
      AND RA.CASH_RECEIPT_ID(+)          = CR.CASH_RECEIPT_ID
      AND HR.ORGANIZATION_ID             = CR.ORG_ID
      AND CRH.cash_receipt_id=CR.cash_receipt_id
      AND SCH.CASH_RECEIPT_ID= CR.CASH_RECEIPT_ID 
      AND HCA.CUST_ACCOUNT_ID            = CR.PAY_FROM_CUSTOMER
      AND HCA.PARTY_ID                   = HP.PARTY_ID
      AND USR.USER_ID                    = CR.CREATED_BY
      AND CR.ORG_ID                     IN (101, 102)
      and hp1.attribute1=''HDS_BU''
      and hp.attribute1=''HDS_MVID''
      
      UNION
      -----PAYMENT TRANSFERS REPORT---------------------------
  
SELECT
      ''TRANSFER/ADJUSTMENTS'' Document_Type,
      HCA.ATTRIBUTE2 MVID, 
      HP.PARTY_NAME MASTER_VENDOR,
      case when art.name like ''CB%'' then ''CB''
           when art.name like ''FT%'' then ''FASTENERS AND TOOLS''
           when art.name like ''CTI%'' then ''INTERIORS''
           when art.name like ''RR%'' then ''REPAIR_REMODEL''
           when art.name like ''GRA%'' then ''UTILITIES_CANADA10''
           when art.name like ''WW%'' then ''WATERWORKS''
           when art.name like ''WC%'' then ''WHITECAP CONSTRUCTION''
           when art.name like ''UTL%'' then ''UTILSERV''
           when art.name like ''ELE%'' then ''ELECTRICAL''
           when art.name like ''LIT%'' then ''LITEMOR CANADA''
           when art.name like ''CAN%'' then ''LITEMOR CANADA''
           when art.name like ''FM CAN%'' then ''FACILITIES MAINTENANCE CANADA''
           ELSE ''FACILITIES MAINTENANCE'' END   BUSINES_UNIT,
          CR.RECEIPT_NUMBER DOCUMENT_NUMBER,
      RM.PRINTED_NAME PAYMENT_TYPE, 
      ART.NAME Transf_Adj_Type,
      NULL DEPOSIT_DATE,
      APP.APPLY_DATE DOCUMENT_DATE,
      APP.AMOUNT_APPLIED*-1 AMOUNT,
      TRUNC(APP.GL_POSTED_DATE) GL_DATE,
      CR.COMMENTS IMAGE_URL,
      CR.ATTRIBUTE2 TIMEFREAME,
      CR.ATTRIBUTE10 REBATE_TYPE,
      TRUNC(APP.CREATION_DATE) REATION_DATE,
      USR.USER_NAME USER_NAME,
      APP.LAST_UPDATE_DATE LAST_UPDATE_DATE,        
      CR.CURRENCY_CODE CURRENCY_CODE, 
      HR.NAME ORG_NAME,
      CR.STATUS STATUS,
      NULL REVERSAL_REASON    
         
FROM 
      APPS.AR_CASH_RECEIPTS_ALL CR, 
      APPS.AR_RECEIPT_METHODS RM, 
      APPS.HZ_PARTIES HP, 
      APPS.HZ_CUST_ACCOUNTS HCA, 
      APPS.HZ_PARTIES HP1,       
      APPS.AR_PAYMENT_SCHEDULES_ALL SCH,  
      APPS.AR_RECEIVABLE_APPLICATIONS_ALL APP,
      APPS.HR_OPERATING_UNITS HR,
      APPS.AR_RECEIVABLES_TRX_ALL         ART,
      APPS.FND_USER USR
      
WHERE   1 =1 
    AND CR.ATTRIBUTE1        = TO_CHAR(HP1.PARTY_ID) 
    AND CR.STATUS NOT IN (''REV'') 
    AND CR.RECEIPT_METHOD_ID =RM.RECEIPT_METHOD_ID 
    AND HCA.CUST_ACCOUNT_ID  =CR.PAY_FROM_CUSTOMER 
    AND HCA.PARTY_ID         =HP.PARTY_ID
    AND HR.ORGANIZATION_ID             = CR.ORG_ID
    AND CR.ORG_ID           IN (101,102) 
    AND APP.ORG_ID          IN (101,102) 
    AND SCH.CASH_RECEIPT_ID= CR.CASH_RECEIPT_ID 
    AND USR.USER_ID                    = CR.CREATED_BY
    AND SCH.PAYMENT_SCHEDULE_ID = APP.PAYMENT_SCHEDULE_ID
    AND ART.RECEIVABLES_TRX_ID(+) = APP.RECEIVABLES_TRX_ID 
    AND APP.APPLIED_PAYMENT_SCHEDULE_ID=''-3'' 
    AND APP.DISPLAY =''Y''
','KP059700','','N','PAYMENTS ','','CSV,Pivot Excel,EXCEL,','','','','','','','','APPS','US','','','','');
--Inserting Report Columns - HDS Payments - Intercompany Payment Activity ALL
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'ORG_NAME','Org Name','','','','default','','18','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'MVID','Mvid','','','','default','','2','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'MVID_NAME','Vendor Name','','','','default','','3','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'DOCUMENT_DATE','Document Date','','','','default','','9','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'USER_NAME','User Name','','','','default','','15','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'BUSINESS_UNIT','Business Unit','','','','default','','4','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'DOCUMENT_NUMBER','Document Number','','','','default','','5','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'PAYMENT_TYPE','Payment Type','','','','default','','6','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'GL_DATE','Gl Date','','','','default','','11','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'DEPOSIT_DATE','Deposit Date','','','','default','','8','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'CURRENCY_CODE','Currency Code','','','','default','','17','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'LAST_UPDATE_DATE','Last Update Date','','','','default','','16','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'CREATION_DATE','Creation Date','','','','default','','14','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'AMOUNT','Amount','','','~,~.~2','default','','10','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'TIMEFRAME','Timeframe','','','','default','','13','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'IMAGE_URL','Image Url','','','','default','','12','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'REVERSAL_REASON','Reversal Reason','','','','default','','20','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'STATUS','Status','','','','default','','19','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'DOCUMENT_TYPE','Document Type','','','','default','','1','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'TRANSF_ADJ_TYPE','Transf Adj Type','','','','','','7','N','','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Intercompany Payment Activity ALL',682,'REBATE_TYPE','Rebate Type','','','','','','21','','Y','','','','','','','KP059700','N','N','','XXEIS_1011488_EIALOG_V','','','GROUP_BY','US','');
--Inserting Report Parameters - HDS Payments - Intercompany Payment Activity ALL
xxeis.eis_rsc_ins.rp( 'HDS Payments - Intercompany Payment Activity ALL',682,'Business Unit','','BUSINESS_UNIT','IN','HDS BU','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_1011488_EIALOG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payments - Intercompany Payment Activity ALL',682,'Gl Date From','','GL_DATE','>=','','','DATE','N','Y','1','','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_1011488_EIALOG_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payments - Intercompany Payment Activity ALL',682,'Gl Date To','','GL_DATE','<=','','','DATE','N','Y','2','','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_1011488_EIALOG_V','','','US','');
--Inserting Dependent Parameters - HDS Payments - Intercompany Payment Activity ALL
--Inserting Report Conditions - HDS Payments - Intercompany Payment Activity ALL
xxeis.eis_rsc_ins.rcnh( 'HDS Payments - Intercompany Payment Activity ALL',682,'BUSINESS_UNIT IN :Business Unit ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BUSINESS_UNIT','','Business Unit','','','','','XXEIS_1011488_EIALOG_V','','','','','','IN','Y','Y','','','','','1',682,'HDS Payments - Intercompany Payment Activity ALL','BUSINESS_UNIT IN :Business Unit ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payments - Intercompany Payment Activity ALL',682,'GL_DATE >= :Gl Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_DATE','','Gl Date From','','','','','XXEIS_1011488_EIALOG_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',682,'HDS Payments - Intercompany Payment Activity ALL','GL_DATE >= :Gl Date From ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payments - Intercompany Payment Activity ALL',682,'GL_DATE <= :Gl Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_DATE','','Gl Date To','','','','','XXEIS_1011488_EIALOG_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',682,'HDS Payments - Intercompany Payment Activity ALL','GL_DATE <= :Gl Date To ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payments - Intercompany Payment Activity ALL',682,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','and ( X1EV.PAYMENT_TYPE in (''REBATE_CREDIT'',''REBATE_DEDUCTION'')
or X1EV.document_type=''TRANSFER/ADJUSTMENTS'')','1',682,'HDS Payments - Intercompany Payment Activity ALL','Free Text ');
--Inserting Report Sorts - HDS Payments - Intercompany Payment Activity ALL
xxeis.eis_rsc_ins.rs( 'HDS Payments - Intercompany Payment Activity ALL',682,'DOCUMENT_TYPE','ASC','KP059700','2','');
xxeis.eis_rsc_ins.rs( 'HDS Payments - Intercompany Payment Activity ALL',682,'MVID','ASC','KP059700','3','');
xxeis.eis_rsc_ins.rs( 'HDS Payments - Intercompany Payment Activity ALL',682,'BUSINESS_UNIT','ASC','KP059700','1','');
xxeis.eis_rsc_ins.rs( 'HDS Payments - Intercompany Payment Activity ALL',682,'DOCUMENT_NUMBER','ASC','KP059700','4','');
--Inserting Report Triggers - HDS Payments - Intercompany Payment Activity ALL
--inserting report templates - HDS Payments - Intercompany Payment Activity ALL
--Inserting Report Portals - HDS Payments - Intercompany Payment Activity ALL
--inserting report dashboards - HDS Payments - Intercompany Payment Activity ALL
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS Payments - Intercompany Payment Activity ALL','682','XXEIS_1011488_EIALOG_V','XXEIS_1011488_EIALOG_V','N','');
--inserting report security - HDS Payments - Intercompany Payment Activity ALL
xxeis.eis_rsc_ins.rsec( 'HDS Payments - Intercompany Payment Activity ALL','682','','XXCUS_TM_USER',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payments - Intercompany Payment Activity ALL','682','','XXCUS_TM_ADMIN_USER',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payments - Intercompany Payment Activity ALL','682','','XXCUS_TM_FORMS_RESP',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payments - Intercompany Payment Activity ALL','682','','XXCUS_TM_ADM_FORMS_RESP',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payments - Intercompany Payment Activity ALL','20005','','XXWC_VIEW_ALL_EIS_REPORTS',682,'KP059700','','','');
--Inserting Report Pivots - HDS Payments - Intercompany Payment Activity ALL
--Inserting Report   Version details- HDS Payments - Intercompany Payment Activity ALL
xxeis.eis_rsc_ins.rv( 'HDS Payments - Intercompany Payment Activity ALL','','HDS Payments - Intercompany Payment Activity ALL','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
