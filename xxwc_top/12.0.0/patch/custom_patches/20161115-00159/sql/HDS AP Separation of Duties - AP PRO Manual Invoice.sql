--Report Name            : HDS AP Separation of Duties - AP PRO Manual Invoice
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_AP_PRO_MI
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_AP_PRO_MI
xxeis.eis_rsc_ins.v( 'EIS_AP_PRO_MI',200,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Ap Pro Mi1','EAPM1','','','VIEW','US','','');
--Delete Object Columns for EIS_AP_PRO_MI
xxeis.eis_rsc_utility.delete_view_rows('EIS_AP_PRO_MI',200,FALSE);
--Inserting Object Columns for EIS_AP_PRO_MI
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_MI','OPERATING_UNIT',200,'Operating Unit','OPERATING_UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_MI','ORG_ID',200,'Org Id','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_MI','INVOICE_SOURCE',200,'Invoice Source','INVOICE_SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Source','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_MI','BRANCH',200,'Branch','BRANCH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_MI','INVOICE_TYPE',200,'Invoice Type','INVOICE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_MI','INVOICE_AMOUNT',200,'Invoice Amount','INVOICE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_MI','INVOICE_DATE',200,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_MI','INVOICE_NUM',200,'Invoice Num','INVOICE_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_MI','VENDOR_SITE_CODE',200,'Vendor Site Code','VENDOR_SITE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Site Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_MI','VENDOR_NAME',200,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_MI','VENDOR_NUMBER',200,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_MI','CREATION_DATE',200,'Creation Date','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_MI','ENTERED_BY_NAME',200,'Entered By Name','ENTERED_BY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Entered By Name','','','','US');
--Inserting Object Components for EIS_AP_PRO_MI
--Inserting Object Component Joins for EIS_AP_PRO_MI
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report LOV Data for HDS AP Separation of Duties - AP PRO Manual Invoice
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS AP Separation of Duties - AP PRO Manual Invoice
xxeis.eis_rsc_ins.lov( 200,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 200,'select DISTINCT user_name || '' - '' || description entered_by_name
from applsys.FND_USER','','HDS_USER_NAME_DESCRIPTION','LOV from fnd user','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report Data for HDS AP Separation of Duties - AP PRO Manual Invoice
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS AP Separation of Duties - AP PRO Manual Invoice
xxeis.eis_rsc_utility.delete_report_rows( 'HDS AP Separation of Duties - AP PRO Manual Invoice' );
--Inserting Report - HDS AP Separation of Duties - AP PRO Manual Invoice
xxeis.eis_rsc_ins.r( 200,'HDS AP Separation of Duties - AP PRO Manual Invoice','','HDS AP Separation of Duties - AP PRO Manual Invoice','','','','XXEIS_RS_ADMIN','EIS_AP_PRO_MI','Y','','','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,EXCEL,Pivot Excel,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - HDS AP Separation of Duties - AP PRO Manual Invoice
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Manual Invoice',200,'BRANCH','Branch','Branch','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_MI','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Manual Invoice',200,'CREATION_DATE','Creation Date','Creation Date','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_MI','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Manual Invoice',200,'ENTERED_BY_NAME','Entered By Name','Entered By Name','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_MI','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Manual Invoice',200,'INVOICE_AMOUNT','Invoice Amount','Invoice Amount','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_MI','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Manual Invoice',200,'INVOICE_DATE','Invoice Date','Invoice Date','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_MI','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Manual Invoice',200,'INVOICE_NUM','Invoice Num','Invoice Num','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_MI','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Manual Invoice',200,'INVOICE_SOURCE','Invoice Source','Invoice Source','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_MI','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Manual Invoice',200,'INVOICE_TYPE','Invoice Type','Invoice Type','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_MI','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Manual Invoice',200,'VENDOR_NAME','Vendor Name','Vendor Name','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_MI','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Manual Invoice',200,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_MI','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Manual Invoice',200,'VENDOR_SITE_CODE','Vendor Site Code','Vendor Site Code','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_MI','','','GROUP_BY','US','');
--Inserting Report Parameters - HDS AP Separation of Duties - AP PRO Manual Invoice
xxeis.eis_rsc_ins.rp( 'HDS AP Separation of Duties - AP PRO Manual Invoice',200,'Start_Date','Beginning of range of dates','CREATION_DATE','>=','','','DATE','N','Y','1','N','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','','EIS_AP_PRO_MI','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP Separation of Duties - AP PRO Manual Invoice',200,'End_Date','End of range of dates','CREATION_DATE','<=','','','DATE','N','Y','2','N','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','','EIS_AP_PRO_MI','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP Separation of Duties - AP PRO Manual Invoice',200,'User Name','User Name','ENTERED_BY_NAME','IN','HDS_USER_NAME_DESCRIPTION','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AP_PRO_MI','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP Separation of Duties - AP PRO Manual Invoice',200,'Organization Name','Organization Name','OPERATING_UNIT','IN','EIS_MULTI_OPERATING_UNIT_LOV','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AP_PRO_MI','','','US','');
--Inserting Dependent Parameters - HDS AP Separation of Duties - AP PRO Manual Invoice
--Inserting Report Conditions - HDS AP Separation of Duties - AP PRO Manual Invoice
xxeis.eis_rsc_ins.rcnh( 'HDS AP Separation of Duties - AP PRO Manual Invoice',200,'EAPM1.CREATION_DATE >= Start_Date','ADVANCED','','  1#$# ','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Start_Date','','','','','EIS_AP_PRO_MI','','','','','','GREATER_THAN_EQUALS','Y','Y','TRUNC(EAPM1.CREATION_DATE)','','','','1',200,'HDS AP Separation of Duties - AP PRO Manual Invoice','EAPM1.CREATION_DATE >= Start_Date');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Separation of Duties - AP PRO Manual Invoice',200,'EAPM1.CREATION_DATE <= End_Date','ADVANCED','','  1#$# ','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','End_Date','','','','','EIS_AP_PRO_MI','','','','','','LESS_THAN_EQUALS','Y','Y','TRUNC(EAPM1.CREATION_DATE)','','','','1',200,'HDS AP Separation of Duties - AP PRO Manual Invoice','EAPM1.CREATION_DATE <= End_Date');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Separation of Duties - AP PRO Manual Invoice',200,'EAPM1.ENTERED_BY_NAME IN User Name','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ENTERED_BY_NAME','','User Name','','','','','EIS_AP_PRO_MI','','','','','','IN','Y','Y','','','','','1',200,'HDS AP Separation of Duties - AP PRO Manual Invoice','EAPM1.ENTERED_BY_NAME IN User Name');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Separation of Duties - AP PRO Manual Invoice',200,'EAPM1.OPERATING_UNIT IN Organization Name','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','OPERATING_UNIT','','Organization Name','','','','','EIS_AP_PRO_MI','','','','','','IN','Y','Y','','','','','1',200,'HDS AP Separation of Duties - AP PRO Manual Invoice','EAPM1.OPERATING_UNIT IN Organization Name');
--Inserting Report Sorts - HDS AP Separation of Duties - AP PRO Manual Invoice
--Inserting Report Triggers - HDS AP Separation of Duties - AP PRO Manual Invoice
--inserting report templates - HDS AP Separation of Duties - AP PRO Manual Invoice
--Inserting Report Portals - HDS AP Separation of Duties - AP PRO Manual Invoice
--inserting report dashboards - HDS AP Separation of Duties - AP PRO Manual Invoice
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS AP Separation of Duties - AP PRO Manual Invoice','200','EIS_AP_PRO_MI','EIS_AP_PRO_MI','N','');
--inserting report security - HDS AP Separation of Duties - AP PRO Manual Invoice
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Manual Invoice','200','','HDS_AP_ADMIN_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Manual Invoice','200','','HDS_AP_DISBURSEMTS_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Manual Invoice','200','','HDS_AP_INQUIRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Manual Invoice','200','','HDS_AP_MGR_NOSUP_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Manual Invoice','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Manual Invoice','200','','HDS_AP_TRNS_ENTRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Manual Invoice','200','','PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Manual Invoice','200','','PAYABLES_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Manual Invoice','20005','','XXWC_VIEW_ALL_EIS_REPORTS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Manual Invoice','200','','HDS_AP_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Manual Invoice','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Manual Invoice','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Manual Invoice','200','','HDS_AP_MGR_NOSUP_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Manual Invoice','200','','HDS_AP_DISBURSEMTS_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Manual Invoice','200','','HDS_AP_ADMIN_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Manual Invoice','200','','HDS_PAYABLES_CLOSE_GSC_GLBL',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Manual Invoice','200','','HDS_PAYABLES_CLOSE_GLBL',200,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS AP Separation of Duties - AP PRO Manual Invoice
--Inserting Report   Version details- HDS AP Separation of Duties - AP PRO Manual Invoice
xxeis.eis_rsc_ins.rv( 'HDS AP Separation of Duties - AP PRO Manual Invoice','','HDS AP Separation of Duties - AP PRO Manual Invoice','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
