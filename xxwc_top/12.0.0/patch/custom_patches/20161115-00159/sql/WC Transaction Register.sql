--Report Name            : WC Transaction Register
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data XXEIS_AR_TRANSACTIONS_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_AR_TRANSACTIONS_V
xxeis.eis_rsc_ins.v( 'XXEIS_AR_TRANSACTIONS_V',222,'This view shows details of receivable transactions including customer site information.','','','','XXEIS_RS_ADMIN','XXEIS','EIS AR Transactions','XEATV','','','VIEW','US','Y','');
--Delete Object Columns for XXEIS_AR_TRANSACTIONS_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_AR_TRANSACTIONS_V',222,FALSE);
--Inserting Object Columns for XXEIS_AR_TRANSACTIONS_V
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','BILL_TO_CUSTOMER_ID',222,'Customer identifier','BILL_TO_CUSTOMER_ID','','','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_ALL','BILL_TO_CUSTOMER_ID','Bill To Customer Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','BILL_TO_SITE_USE_ID',222,'Site use identifier billed','BILL_TO_SITE_USE_ID','','','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_ALL','BILL_TO_SITE_USE_ID','Bill To Site Use Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','SHIP_TO_CUSTOMER_ID',222,'Customer identifier','SHIP_TO_CUSTOMER_ID','','','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_ALL','SHIP_TO_CUSTOMER_ID','Ship To Customer Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','SHIP_TO_SITE_USE_ID',222,'Site identifier for the shipments','SHIP_TO_SITE_USE_ID','','','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_ALL','SHIP_TO_SITE_USE_ID','Ship To Site Use Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','COMMITMENT_NUMBER',222,'Transaction number','COMMITMENT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','TRX_NUMBER','Commitment Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','PREVIOUS_TRX_NUMBER',222,'Transaction number','PREVIOUS_TRX_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','TRX_NUMBER','Previous Trx Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','PREV_SOURCE_NAME',222,'Batch source name','PREV_SOURCE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_BATCH_SOURCES_ALL','NAME','Prev Source Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','RELATED_TRX_NUMBER',222,'Transaction number','RELATED_TRX_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','TRX_NUMBER','Related Trx Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','BILL_TO_CUSTOMER_NAME',222,'Name of this party','BILL_TO_CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','PARTY_NAME','Bill To Customer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','BILL_TO_CUSTOMER_NUM',222,'Account Number','BILL_TO_CUSTOMER_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ACCOUNT_NUMBER','Bill To Customer Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','BILL_TO_LOCATION',222,'Bill To Location','BILL_TO_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_SITE_USES_ALL','LOCCATION','Bill To Location','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','BILL_TO_ADDRESS1',222,'Bill To Address1','BILL_TO_ADDRESS1','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_LOCATIONS','ADDRESS1','Bill To Address1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','BILL_TO_ADDRESS2',222,'Bill To Address2','BILL_TO_ADDRESS2','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_LOCATIONS','ADDRESS2','Bill To Address2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','SHIP_TO_CUSTOMER_NAME',222,'Name of this party','SHIP_TO_CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','PARTY_NAME','Ship To Customer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','SHIP_TO_CUSTOMER_NUM',222,'Account Number','SHIP_TO_CUSTOMER_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ACCOUNT_NUMBER','Ship To Customer Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','SHIP_TO_LOCATION',222,'Site use identifier','SHIP_TO_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_SITE_USES_ALL','LOCATION','Ship To Location','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','SHIP_TO_ADDRESS1',222,'First line for address','SHIP_TO_ADDRESS1','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_LOCATIONS','ADDRESS1','Ship To Address1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','SHIP_TO_ADDRESS2',222,'Second line for address','SHIP_TO_ADDRESS2','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_LOCATIONS','ADDRESS2','Ship To Address2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','TRX_TYPE_NAME',222,'Transaction type name','TRX_TYPE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUST_TRX_TYPES_ALL','NAME','Trx Type Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','TRX_CLASS',222,'Transaction class, such as INV, CM, DM, GUAR, DEP, or CB','TRX_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUST_TRX_TYPES_ALL','TYPE','Trx Class','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','TRX_POST_TO_GL',222,'Trx Post To Gl','TRX_POST_TO_GL','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','POST_TO_GL','Trx Post To Gl','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','SHIP_VIA_NAME',222,'Freight code description','SHIP_VIA_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','ORG_FREIGHT_TL','DESCRIPTION','Ship Via Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','OF_ORGANIZATION_ID',222,'Organization identifier','OF_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','ORG_FREIGHT_TL','ORGANIZATION_ID','Of Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GL_DATE',222,'General Ledger date','GL_DATE','','','','XXEIS_RS_ADMIN','DATE','RA_CUST_TRX_LINE_GL_DIST_ALL','GL_DATE','Gl Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','ACCOUNTING_AFFECT_FLAG',222,'Indicates if transactions affect open receivable balances. Y for yes, N otherwise.','ACCOUNTING_AFFECT_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUST_TRX_TYPES_ALL','ACCOUNTING_AFFECT_FLAG','Accounting Affect Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','COMMIT_TRX_ID',222,'Invoice identifier','COMMIT_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_ALL','CUSTOMER_TRX_ID','Commit Trx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','RECEIVABLE_AMOUNT',222,'Receivable Amount','RECEIVABLE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','RA_CUST_TRX_LINE_GL_DIST_ALL','AMOUNT','Receivable Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','ACCOUNTED_AMOUNT',222,'Amount of this record in the functional currency','ACCOUNTED_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','RA_CUST_TRX_LINE_GL_DIST_ALL','ACCTD_AMOUNT','Accounted Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','TRX_REC_ACCOUNT',222,'Trx Rec Account','TRX_REC_ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','DERIVED COLUMN','DERIVED COLUMN','Trx Rec Account','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','PREV_TRX_ID',222,'Invoice identifier','PREV_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_ALL','CUSTOMER_TRX_ID','Prev Trx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','RAC_BILL_PARTY_ID',222,'Party identifier','RAC_BILL_PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_PARTIES','PARTY_ID','Rac Bill Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','RAC_SHIP_PARTY_ID',222,'Party identifier','RAC_SHIP_PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_PARTIES','PARTY_ID','Rac Ship Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','RAA_BILL_PS_SITE_ID',222,'Party site identifier','RAA_BILL_PS_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_PARTY_SITES','PARTY_SITE_ID','Raa Bill Ps Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','RAA_BILL_LOC_LOCATION_ID',222,'Unique identifier for this location','RAA_BILL_LOC_LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_LOCATIONS','LOCATION_ID','Raa Bill Loc Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','RAA_SHIP_LOC_LOCATION_ID',222,'Unique identifier for this location','RAA_SHIP_LOC_LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_LOCATIONS','LOCATION_ID','Raa Ship Loc Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','RAA_BILL_CUST_ACCT_SITE_ID',222,'Customer site identifier','RAA_BILL_CUST_ACCT_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_CUST_ACCT_SITES_ALL','CUST_ACCT_SITE_ID','Raa Bill Cust Acct Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','RAA_SHIP_CUST_ACCT_SITE_ID',222,'Customer site identifier','RAA_SHIP_CUST_ACCT_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_CUST_ACCT_SITES_ALL','CUST_ACCT_SITE_ID','Raa Ship Cust Acct Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','FT_BILL_TERRITORY_CODE',222,'The code for the territory','FT_BILL_TERRITORY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','FND_TERRITORIES','TERRITORY_CODE','Ft Bill Territory Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','FT_SHIP_TERRITORY_CODE',222,'The code for the territory','FT_SHIP_TERRITORY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','FND_TERRITORIES','TERRITORY_CODE','Ft Ship Territory Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','SU_BILL_SITE_USE_ID',222,'Site use identifier','SU_BILL_SITE_USE_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_CUST_SITE_USES_ALL','SITE_USE_ID','Su Bill Site Use Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','SU_SHIP_SITE_USE_ID',222,'Site use identifier','SU_SHIP_SITE_USE_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_CUST_SITE_USES_ALL','SITE_USE_ID','Su Ship Site Use Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','RAA_SHIP_PS_SITE_ID',222,'Party site identifier','RAA_SHIP_PS_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_PARTY_SITES','PARTY_SITE_ID','Raa Ship Ps Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','BATCH_SOURCE_ID',222,'Batch Source Id','BATCH_SOURCE_ID~BATCH_SOURCE_ID','','','','XXEIS_RS_ADMIN','NUMBER','RA_BATCH_SOURCES_ALL','BATCH_SOURCE_ID','Batch Source Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','BATCH_SOURCE_ID1',222,'Batch Source Id1','BATCH_SOURCE_ID1~BATCH_SOURCE_ID1','','','','XXEIS_RS_ADMIN','NUMBER','RA_BATCH_SOURCES_ALL','BATCH_SOURCE_ID','Batch Source Id1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','CODE_COMBINATION_ID',222,'Code Combination Id','CODE_COMBINATION_ID~CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_CODE_COMBINATIONS','CODE_COMBINATION_ID','Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','CUST_TRX_TYPE_ID',222,'Cust Trx Type Id','CUST_TRX_TYPE_ID~CUST_TRX_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','RA_CUST_TRX_TYPES_ALL','CUST_TRX_TYPE_ID','Cust Trx Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','FREIGHT_CODE',222,'Freight code','FREIGHT_CODE~FREIGHT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','ORG_FREIGHT_TL','FREIGHT_CODE','Freight Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','OPERATING_UNIT',222,'Operating Unit','OPERATING_UNIT~OPERATING_UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','HR_ALL_ORGANIZATION_UNITS_TL','NAME','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','ORGANIZATION_ID',222,'Organization Id','ORGANIZATION_ID~ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','HR_ALL_ORGANIZATION_UNITS','ORGANIZATION_ID','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','REL_TRX_ID',222,'Rel Trx Id','REL_TRX_ID~REL_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_ALL','CUSTOMER_TRX_ID','Rel Trx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','CUSTOMER_TRX_ID',222,'Customer Trx Id','CUSTOMER_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_ALL','CUSTOMER_TRX_ID','Customer Trx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','TRX_NUMBER',222,'Transaction number','TRX_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','TRX_NUMBER','Trx Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','ORG_ID',222,'Org Id','ORG_ID~ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','HR_ALL_ORGANIZATION_UNITS','ORGANIZATION_ID','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','TRX_DATE',222,'Invoice date','TRX_DATE','','','','XXEIS_RS_ADMIN','DATE','RA_CUSTOMER_TRX_ALL','TRX_DATE','Trx Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','TRX_COMMENTS',222,'Stores comments entered in the Transactions workbench. Value can be printed on an invoice using the Print Invoice view.','TRX_COMMENTS','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','COMMENTS','Trx Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','TRX_COMPLETED_STATUS',222,'Indicates if the invoice is complete. Y for yes, N otherwise.','TRX_COMPLETED_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','COMPLETE_FLAG','Trx Completed Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','PURCHASE_ORDER',222,'Purchase order','PURCHASE_ORDER','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','PURCHASE_ORDER','Purchase Order','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','INTERNAL_NOTES',222,'Stores the special instruction entered in the Transactions workbench. Value can be printed on an invoice with using the Print Invoice View','INTERNAL_NOTES','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','INTERNAL_NOTES','Internal Notes','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','SHIP_DATE_ACTUAL',222,'Ship date','SHIP_DATE_ACTUAL','','','','XXEIS_RS_ADMIN','DATE','RA_CUSTOMER_TRX_ALL','SHIP_DATE_ACTUAL','Ship Date Actual','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','WAYBILL_NUMBER',222,'Waybill number','WAYBILL_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','WAYBILL_NUMBER','Waybill Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','DOC_SEQUENCE_VALUE',222,'The value of the document sequence','DOC_SEQUENCE_VALUE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_ALL','DOC_SEQUENCE_VALUE','Doc Sequence Value','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','TRX_STATUS',222,'The status of the transaction','TRX_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','STATUS_TRX','Trx Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','INVOICE_CURRENCY_CODE',222,'Invoice currency','INVOICE_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','INVOICE_CURRENCY_CODE','Invoice Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','RAA_BILL_PS_PARTY_SITE_ID',222,'Raa Bill Ps Party Site Id','RAA_BILL_PS_PARTY_SITE_ID~RAA_BILL_PS_PARTY_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_PARTY_SITES','PARTY_SITE_ID','Raa Bill Ps Party Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','RAA_SHIP_PS_PARTY_SITE_ID',222,'Raa Ship Ps Party Site Id','RAA_SHIP_PS_PARTY_SITE_ID~RAA_SHIP_PS_PARTY_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_PARTY_SITES','PARTY_SITE_ID','Raa Ship Ps Party Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','RAC_BILL_CUST_ACCOUNT_ID',222,'Customer account identifier','RAC_BILL_CUST_ACCOUNT_ID~RAC_BILL_CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_CUST_ACCOUNTS','CUST_ACCOUNT_ID','Rac Bill Cust Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','RAC_BILL_PARTY_PARTY_ID',222,'Rac Bill Party Party Id','RAC_BILL_PARTY_PARTY_ID~RAC_BILL_PARTY_PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_PARTIES','PARTY_ID','Rac Bill Party Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','RAC_SHIP_CUST_ACCOUNT_ID',222,'Customer account identifier','RAC_SHIP_CUST_ACCOUNT_ID~RAC_SHIP_CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_CUST_ACCOUNTS','CUST_ACCOUNT_ID','Rac Ship Cust Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','RAC_SHIP_PARTY_PARTY_ID',222,'Rac Ship Party Party Id','RAC_SHIP_PARTY_PARTY_ID~RAC_SHIP_PARTY_PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_PARTIES','PARTY_ID','Rac Ship Party Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50328#PRODUCT',222,'Accounting Flexfield (KFF): Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GLCC#Product','50328','1014547','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50328#PRODUCT#DESCR',222,'Accounting Flexfield (KFF) : Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#PRODUCT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GLCC#Product Descr','50328','1014547','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50328#LOCATION',222,'Accounting Flexfield (KFF): Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GLCC#Location','50328','1014548','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50328#LOCATION#DESCR',222,'Accounting Flexfield (KFF) : Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#LOCATION#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GLCC#Location Descr','50328','1014548','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50328#COST_CENTER',222,'Accounting Flexfield (KFF): Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#COST_CENTER','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GLCC#Cost Center','50328','1014549','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50328#COST_CENTER#DESCR',222,'Accounting Flexfield (KFF) : Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#COST_CENTER#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GLCC#Cost Center Descr','50328','1014549','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50328#ACCOUNT',222,'Accounting Flexfield (KFF): Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GLCC#Account','50328','1014550','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50328#ACCOUNT#DESCR',222,'Accounting Flexfield (KFF) : Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#ACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GLCC#Account Descr','50328','1014550','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50328#PROJECT_CODE',222,'Accounting Flexfield (KFF): Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#PROJECT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GLCC#Project Code','50328','1014551','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50328#PROJECT_CODE#DESCR',222,'Accounting Flexfield (KFF) : Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#PROJECT_CODE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GLCC#Project Code Descr','50328','1014551','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50328#FUTURE_USE_2',222,'Accounting Flexfield (KFF): Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#FUTURE_USE_2','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GLCC#Future Use 2','50328','1014948','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50328#FUTURE_USE_2#DESCR',222,'Accounting Flexfield (KFF) : Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#FUTURE_USE_2#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GLCC#Future Use 2 Descr','50328','1014948','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50368#PRODUCT',222,'Accounting Flexfield (KFF): Segment ''Product'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GLCC#Product','50368','1014596','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50368#PRODUCT#DESCR',222,'Accounting Flexfield (KFF) : Segment ''Product'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#PRODUCT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GLCC#Product Descr','50368','1014596','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50368#DIVISION',222,'Accounting Flexfield (KFF): Segment ''Division'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#DIVISION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GLCC#Division','50368','1014597','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50368#DIVISION#DESCR',222,'Accounting Flexfield (KFF) : Segment ''Division'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#DIVISION#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GLCC#Division Descr','50368','1014597','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50368#DEPARTMENT',222,'Accounting Flexfield (KFF): Segment ''Department'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#DEPARTMENT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GLCC#Department','50368','1014598','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50368#DEPARTMENT#DESCR',222,'Accounting Flexfield (KFF) : Segment ''Department'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#DEPARTMENT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GLCC#Department Descr','50368','1014598','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50368#ACCOUNT',222,'Accounting Flexfield (KFF): Segment ''Account'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GLCC#Account','50368','1014599','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50368#ACCOUNT#DESCR',222,'Accounting Flexfield (KFF) : Segment ''Account'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#ACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GLCC#Account Descr','50368','1014599','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50368#SUBACCOUNT',222,'Accounting Flexfield (KFF): Segment ''SubAccount'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#SUBACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GLCC#SubAccount','50368','1014600','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50368#SUBACCOUNT#DESCR',222,'Accounting Flexfield (KFF) : Segment ''SubAccount'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#SUBACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GLCC#SubAccount Descr','50368','1014600','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50368#FUTURE_USE',222,'Accounting Flexfield (KFF): Segment ''Future Use'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#FUTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GLCC#Future Use','50368','1014601','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50368#FUTURE_USE#DESCR',222,'Accounting Flexfield (KFF) : Segment ''Future Use'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#FUTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GLCC#Future Use Descr','50368','1014601','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#Branch',222,'Descriptive flexfield (DFF): GL Accounts Column Name: Branch','GLCC#Branch','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','ATTRIBUTE1','Glcc#Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','BILL_TO_ACCOUNT_NAME',222,'Bill To Account Name','BILL_TO_ACCOUNT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','BILL_TO_PARTY_NAME',222,'Bill To Party Name','BILL_TO_PARTY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Party Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','BS_BATCH_SOURCE_ID',222,'Bs Batch Source Id','BS_BATCH_SOURCE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Bs Batch Source Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','BS_PREV_BATCH_SOURCE_ID',222,'Bs Prev Batch Source Id','BS_PREV_BATCH_SOURCE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Bs Prev Batch Source Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','CTT_CUST_TRX_TYPE_ID',222,'Ctt Cust Trx Type Id','CTT_CUST_TRX_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ctt Cust Trx Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','CT_REL_CUSTOMER_TRX_ID',222,'Ct Rel Customer Trx Id','CT_REL_CUSTOMER_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ct Rel Customer Trx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GD_CUST_TRX_LINE_GL_DIST_ID',222,'Gd Cust Trx Line Gl Dist Id','GD_CUST_TRX_LINE_GL_DIST_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Gd Cust Trx Line Gl Dist Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC_CODE_COMBINATION_ID',222,'Glcc Code Combination Id','GLCC_CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Glcc Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','SHIP_TO_ACCOUNT_NAME',222,'Ship To Account Name','SHIP_TO_ACCOUNT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','SHIP_TO_PARTY_NAME',222,'Ship To Party Name','SHIP_TO_PARTY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To Party Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','COPYRIGHT',222,'Copyright','COPYRIGHT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Copyright','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50328#FUTURE_USE',222,'Accounting Flexfield (KFF): Segment ''Future Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#FUTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GLCC#Future Use','50328','1014552','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','GLCC#50328#FUTURE_USE#DESCR',222,'Accounting Flexfield (KFF) : Segment ''Future Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#FUTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GLCC#Future Use Descr','50328','1014552','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','RAC_BILL_CUSTOMER_SOURCE',222,'Rac Bill Customer Source','RAC_BILL_CUSTOMER_SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Rac Bill Customer Source','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_AR_TRANSACTIONS_V','RAC_SHIP_CUSTOMER_SOURCE',222,'Rac Ship Customer Source','RAC_SHIP_CUSTOMER_SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Rac Ship Customer Source','','','','');
--Inserting Object Components for XXEIS_AR_TRANSACTIONS_V
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_TRANSACTIONS_V','HZ_PARTIES',222,'HZ_PARTIES','RAC_BILL_PARTY','RAC_BILL_PARTY','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Bill To Parties','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_TRANSACTIONS_V','HZ_CUST_ACCOUNTS',222,'HZ_CUST_ACCOUNTS','RAC_SHIP','RAC_SHIP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ship To Customer Accounts','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_TRANSACTIONS_V','HZ_PARTIES',222,'HZ_PARTIES','RAC_SHIP_PARTY','RAC_SHIP_PARTY','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ship To Parties','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_TRANSACTIONS_V','HZ_CUST_SITE_USES',222,'HZ_CUST_SITE_USES_ALL','SU_BILL','SU_BILL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Bill Customer Site Usages','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_TRANSACTIONS_V','HZ_CUST_SITE_USES',222,'HZ_CUST_SITE_USES_ALL','SU_SHIP','SU_SHIP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ship Customer Site Usages','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_TRANSACTIONS_V','HZ_CUST_ACCT_SITES',222,'HZ_CUST_ACCT_SITES_ALL','RAA_BILL','RAA_BILL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Billing Customer Account Sites','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_TRANSACTIONS_V','HZ_PARTY_SITES',222,'HZ_PARTY_SITES','RAA_BILL_PS','RAA_BILL_PS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Bill To Party Sites','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_TRANSACTIONS_V','HZ_LOCATIONS',222,'HZ_LOCATIONS','RAA_BILL_LOC','RAA_BILL_LOC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Bill To Addresses','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_TRANSACTIONS_V','HZ_CUST_ACCT_SITES',222,'HZ_CUST_ACCT_SITES_ALL','RAA_SHIP','RAA_SHIP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ship Customer Account Sites','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_TRANSACTIONS_V','HZ_PARTY_SITES',222,'HZ_PARTY_SITES','RAA_SHIP_PS','RAA_SHIP_PS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ship To Party Sites','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_TRANSACTIONS_V','HZ_LOCATIONS',222,'HZ_LOCATIONS','RAA_SHIP_LOC','RAA_SHIP_LOC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ship To Addresses','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_TRANSACTIONS_V','RA_CUST_TRX_TYPES',222,'RA_CUST_TRX_TYPES_ALL','CTT','CTT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Transaction Types','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_TRANSACTIONS_V','GL_CODE_COMBINATIONS',222,'GL_CODE_COMBINATIONS','GLCC','GLCC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Receivable Account','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_TRANSACTIONS_V','HR_ORGANIZATION_UNITS',222,'HR_ALL_ORGANIZATION_UNITS','HOU','HOU','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Organization Details','N','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_AR_TRANSACTIONS_V','HZ_CUST_ACCOUNTS',222,'HZ_CUST_ACCOUNTS','RAC_BILL','RAC_BILL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Bill To Customer Accounts','','','','','','Y',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for XXEIS_AR_TRANSACTIONS_V
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_TRANSACTIONS_V','HZ_CUST_ACCOUNTS','RAC_SHIP',222,'XEATV.RAC_SHIP_CUST_ACCOUNT_ID','=','RAC_SHIP.CUST_ACCOUNT_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_TRANSACTIONS_V','HZ_PARTIES','RAC_SHIP_PARTY',222,'XEATV.RAC_SHIP_PARTY_PARTY_ID','=','RAC_SHIP_PARTY.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_TRANSACTIONS_V','HZ_CUST_SITE_USES','SU_BILL',222,'XEATV.SU_BILL_SITE_USE_ID','=','SU_BILL.SITE_USE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_TRANSACTIONS_V','HZ_CUST_SITE_USES','SU_SHIP',222,'XEATV.SU_SHIP_SITE_USE_ID','=','SU_SHIP.SITE_USE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_TRANSACTIONS_V','HZ_CUST_ACCT_SITES','RAA_BILL',222,'XEATV.RAA_BILL_CUST_ACCT_SITE_ID','=','RAA_BILL.CUST_ACCT_SITE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_TRANSACTIONS_V','HZ_PARTY_SITES','RAA_BILL_PS',222,'XEATV.RAA_BILL_PS_PARTY_SITE_ID','=','RAA_BILL_PS.PARTY_SITE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_TRANSACTIONS_V','HZ_LOCATIONS','RAA_BILL_LOC',222,'XEATV.RAA_BILL_LOC_LOCATION_ID','=','RAA_BILL_LOC.LOCATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_TRANSACTIONS_V','HZ_CUST_ACCT_SITES','RAA_SHIP',222,'XEATV.RAA_SHIP_CUST_ACCT_SITE_ID','=','RAA_SHIP.CUST_ACCT_SITE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_TRANSACTIONS_V','HZ_PARTY_SITES','RAA_SHIP_PS',222,'XEATV.RAA_SHIP_PS_PARTY_SITE_ID','=','RAA_SHIP_PS.PARTY_SITE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_TRANSACTIONS_V','HZ_LOCATIONS','RAA_SHIP_LOC',222,'XEATV.RAA_SHIP_LOC_LOCATION_ID','=','RAA_SHIP_LOC.LOCATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_TRANSACTIONS_V','RA_CUST_TRX_TYPES','CTT',222,'XEATV.CUST_TRX_TYPE_ID','=','CTT.CUST_TRX_TYPE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_TRANSACTIONS_V','RA_CUST_TRX_TYPES','CTT',222,'XEATV.ORG_ID','=','CTT.ORG_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_TRANSACTIONS_V','GL_CODE_COMBINATIONS','GLCC',222,'XEATV.CODE_COMBINATION_ID','=','GLCC.CODE_COMBINATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_TRANSACTIONS_V','HR_ORGANIZATION_UNITS','HOU',222,'XEATV.ORGANIZATION_ID','=','HOU.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_TRANSACTIONS_V','HZ_CUST_ACCOUNTS','RAC_BILL',222,'XEATV.RAC_BILL_CUST_ACCOUNT_ID','=','RAC_BILL.CUST_ACCOUNT_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_AR_TRANSACTIONS_V','HZ_PARTIES','RAC_BILL_PARTY',222,'XEATV.RAC_BILL_PARTY_PARTY_ID','=','RAC_BILL_PARTY.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report LOV Data for WC Transaction Register
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - WC Transaction Register
xxeis.eis_rsc_ins.lov( 222,'select CURRENCY_CODE, NAME from
FND_CURRENCIES_VL
WHERE CURRENCY_FLAG = ''Y''
AND ENABLED_FLAG in (''Y'', ''N'')
AND CURRENCY_CODE NOT IN(''ANY'')
ORDER BY CURRENCY_CODE','','CURRENCY_CODE','CURRENCY_CODE','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT DISTINCT ct.trx_number 
FROM    ra_customer_trx ct 
ORDER BY ct.trx_number','','AR_TRANSACTION_NUMBER','AR TRANSACTION NUMBER','XXEIS_RS_ADMIN',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT name
FROM RA_CUST_TRX_TYPES
WHERE TYPE IN ( ''INV'', ''DM'', ''CM'')','','AR_Transaction_Type','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report Data for WC Transaction Register
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC Transaction Register
xxeis.eis_rsc_utility.delete_report_rows( 'WC Transaction Register' );
--Inserting Report - WC Transaction Register
xxeis.eis_rsc_ins.r( 222,'WC Transaction Register','','This report shows all the transactions.','','','','LA023190','XXEIS_AR_TRANSACTIONS_V','Y','','','LA023190','','N','Transactions','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - WC Transaction Register
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'ACCOUNTED_AMOUNT','Accounted Amount','Amount of this record in the functional currency','','','','','17','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'BILL_TO_ADDRESS1','Bill To Address1','Bill To Address1','','','','','33','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'BILL_TO_ADDRESS2','Bill To Address2','Bill To Address2','','','','','34','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'BILL_TO_CUSTOMER_NAME','Bill To Customer Name','Name of this party','','','','','1','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'BILL_TO_CUSTOMER_NUM','Bill To Customer Number','Account Number','','','','','2','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'BILL_TO_LOCATION','Bill To Location','Bill To Location','','','','','3','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'COMMITMENT_NUMBER','Commitment Number','Transaction number','','','','','18','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'DOC_SEQUENCE_VALUE','Doc Sequence Value','The value of the document sequence','','','','','19','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'FT_BILL_TERRITORY_CODE','Ft Bill Territory Code','The code for the territory','','','','','20','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'FT_SHIP_TERRITORY_CODE','Ft Ship Territory Code','The code for the territory','','','','','21','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'GL_DATE','Gl Date','General Ledger date','','','','','22','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'INTERNAL_NOTES','Internal Notes','Stores the special instruction entered in the Transactions workbench. Value can be printed on an invoice with using the Print Invoice View','','','','','23','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'INVOICE_CURRENCY_CODE','Invoice Currency Code','Invoice currency','','','','','15','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'PREVIOUS_TRX_NUMBER','Previous Trx Number','Transaction number','','','','','24','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'PURCHASE_ORDER','Purchase Order','Purchase order','','','','','25','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'RECEIVABLE_AMOUNT','Receivable Amount','Receivable Amount','','','','','26','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'RELATED_TRX_NUMBER','Related Trx Number','Transaction number','','','','','27','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'SHIP_DATE_ACTUAL','Ship Date Actual','Ship date','','','','','28','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'SHIP_TO_ADDRESS1','Ship To Address1','First line for address','','','','','29','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'SHIP_TO_ADDRESS2','Ship To Address2','Second line for address','','','','','30','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'SHIP_TO_CUSTOMER_NAME','Ship To Customer Name','Name of this party','','','','','4','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'SHIP_TO_CUSTOMER_NUM','Ship To Customer Num','Account Number','','','','','5','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'SHIP_TO_LOCATION','Ship To Location','Site use identifier','','','','','6','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'SHIP_VIA_NAME','Ship Via Name','Freight code description','','','','','31','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'TRX_CLASS','Trx Class','Transaction class, such as INV, CM, DM, GUAR, DEP, or CB','','','','','7','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'TRX_COMMENTS','Trx Comments','Stores comments entered in the Transactions workbench. Value can be printed on an invoice using the Print Invoice view.','','','','','13','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'TRX_COMPLETED_STATUS','Trx Completed Status','Indicates if the invoice is complete. Y for yes, N otherwise.','','','','','14','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'TRX_DATE','Trx Date','Invoice date','','','','','10','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'TRX_NUMBER','Trx Number','Transaction number','','','','','9','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'TRX_POST_TO_GL','Trx Post To Gl','Trx Post To Gl','','','','','11','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'TRX_REC_ACCOUNT','Trx Rec Account','Trx Rec Account','','','','','16','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'TRX_STATUS','Trx Status','The status of the transaction','','','','','12','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'TRX_TYPE_NAME','Trx Type Name','Transaction type name','','','','','8','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'WAYBILL_NUMBER','Waybill Number','Waybill number','','','','','32','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'OPERATING_UNIT','Operating Unit','Operating Unit','','','','','35','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'GLCC#50328#ACCOUNT','GLCC#Account','Accounting Flexfield (KFF): Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','','','','','38','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'GLCC#50328#COST_CENTER','GLCC#Cost Center','Accounting Flexfield (KFF): Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','','','','','39','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'GLCC#50328#LOCATION','GLCC#Location','Accounting Flexfield (KFF): Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','','','','','40','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'GLCC#50328#PRODUCT','GLCC#Product','Accounting Flexfield (KFF): Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','','','','','41','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'GLCC#50328#PROJECT_CODE','GLCC#Project Code','Accounting Flexfield (KFF): Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','','','','','42','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'GLCC#BRANCH','Glcc#Branch','Descriptive flexfield (DFF): GL Accounts Column Name: Branch','','','','','43','N','','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'RAC_BILL_CUSTOMER_SOURCE','Rac Bill Customer Source','Rac Bill Customer Source','','','','','36','','Y','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC Transaction Register',222,'RAC_SHIP_CUSTOMER_SOURCE','Rac Ship Customer Source','Rac Ship Customer Source','','','','','37','','Y','','','','','','','LA023190','N','N','','XXEIS_AR_TRANSACTIONS_V','','','','US','');
--Inserting Report Parameters - WC Transaction Register
xxeis.eis_rsc_ins.rp( 'WC Transaction Register',222,'Operating Unit','Operating Unit','OPERATING_UNIT','IN','EIS_MULTI_OPERATING_UNIT_LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_AR_TRANSACTIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Transaction Register',222,'Currency Code','Transaction Currency','INVOICE_CURRENCY_CODE','IN','CURRENCY_CODE','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_AR_TRANSACTIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Transaction Register',222,'Transaction Number','Transaction Number','TRX_NUMBER','IN','AR_TRANSACTION_NUMBER','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_AR_TRANSACTIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Transaction Register',222,'Transaction type','Transaction type','TRX_TYPE_NAME','IN','AR_Transaction_Type','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_AR_TRANSACTIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Transaction Register',222,'Transaction Date High','Transaction Date High','TRX_DATE','<=','','','DATE','N','Y','5','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_AR_TRANSACTIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Transaction Register',222,'Transaction Date Low','Transaction Date Low','TRX_DATE','>=','','','DATE','N','Y','4','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_AR_TRANSACTIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Transaction Register',222,'GL Date High','GL Date High','GL_DATE','<=','','','DATE','N','Y','3','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_AR_TRANSACTIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Transaction Register',222,'GL Date Low','GL Date Low','GL_DATE','>=','','','DATE','N','Y','2','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_AR_TRANSACTIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Transaction Register',222,'Rac Bill Customer Source','Rac Bill Customer Source','RAC_BILL_CUSTOMER_SOURCE','IN','','','VARCHAR2','N','Y','11','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_AR_TRANSACTIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Transaction Register',222,'Rac Ship Customer Source','Rac Ship Customer Source','RAC_SHIP_CUSTOMER_SOURCE','IN','','','VARCHAR2','N','Y','12','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_AR_TRANSACTIONS_V','','','US','');
--Inserting Dependent Parameters - WC Transaction Register
--Inserting Report Conditions - WC Transaction Register
xxeis.eis_rsc_ins.rcnh( 'WC Transaction Register',222,'GL_DATE <= :GL Date High ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_DATE','','GL Date High','','','','','XXEIS_AR_TRANSACTIONS_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',222,'WC Transaction Register','GL_DATE <= :GL Date High ');
xxeis.eis_rsc_ins.rcnh( 'WC Transaction Register',222,'GL_DATE >= :GL Date Low ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_DATE','','GL Date Low','','','','','XXEIS_AR_TRANSACTIONS_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',222,'WC Transaction Register','GL_DATE >= :GL Date Low ');
xxeis.eis_rsc_ins.rcnh( 'WC Transaction Register',222,'INVOICE_CURRENCY_CODE IN :Currency Code ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','INVOICE_CURRENCY_CODE','','Currency Code','','','','','XXEIS_AR_TRANSACTIONS_V','','','','','','IN','Y','Y','','','','','1',222,'WC Transaction Register','INVOICE_CURRENCY_CODE IN :Currency Code ');
xxeis.eis_rsc_ins.rcnh( 'WC Transaction Register',222,'OPERATING_UNIT IN :Operating Unit ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','OPERATING_UNIT','','Operating Unit','','','','','XXEIS_AR_TRANSACTIONS_V','','','','','','IN','Y','Y','','','','','1',222,'WC Transaction Register','OPERATING_UNIT IN :Operating Unit ');
xxeis.eis_rsc_ins.rcnh( 'WC Transaction Register',222,'TRX_DATE <= :Transaction Date High ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TRX_DATE','','Transaction Date High','','','','','XXEIS_AR_TRANSACTIONS_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',222,'WC Transaction Register','TRX_DATE <= :Transaction Date High ');
xxeis.eis_rsc_ins.rcnh( 'WC Transaction Register',222,'TRX_DATE >= :Transaction Date Low ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TRX_DATE','','Transaction Date Low','','','','','XXEIS_AR_TRANSACTIONS_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',222,'WC Transaction Register','TRX_DATE >= :Transaction Date Low ');
xxeis.eis_rsc_ins.rcnh( 'WC Transaction Register',222,'TRX_NUMBER IN :Transaction Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TRX_NUMBER','','Transaction Number','','','','','XXEIS_AR_TRANSACTIONS_V','','','','','','IN','Y','Y','','','','','1',222,'WC Transaction Register','TRX_NUMBER IN :Transaction Number ');
xxeis.eis_rsc_ins.rcnh( 'WC Transaction Register',222,'TRX_TYPE_NAME IN :Transaction type ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TRX_TYPE_NAME','','Transaction type','','','','','XXEIS_AR_TRANSACTIONS_V','','','','','','IN','Y','Y','','','','','1',222,'WC Transaction Register','TRX_TYPE_NAME IN :Transaction type ');
xxeis.eis_rsc_ins.rcnh( 'WC Transaction Register',222,'XEATV.RAC_BILL_CUSTOMER_SOURCE IN Rac Bill Customer Source','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','RAC_BILL_CUSTOMER_SOURCE','','Rac Bill Customer Source','','','','','XXEIS_AR_TRANSACTIONS_V','','','','','','IN','Y','Y','','','','','1',222,'WC Transaction Register','XEATV.RAC_BILL_CUSTOMER_SOURCE IN Rac Bill Customer Source');
xxeis.eis_rsc_ins.rcnh( 'WC Transaction Register',222,'XEATV.RAC_SHIP_CUSTOMER_SOURCE IN Rac Ship Customer Source','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','RAC_SHIP_CUSTOMER_SOURCE','','Rac Ship Customer Source','','','','','XXEIS_AR_TRANSACTIONS_V','','','','','','IN','Y','Y','','','','','1',222,'WC Transaction Register','XEATV.RAC_SHIP_CUSTOMER_SOURCE IN Rac Ship Customer Source');
--Inserting Report Sorts - WC Transaction Register
--Inserting Report Triggers - WC Transaction Register
--inserting report templates - WC Transaction Register
xxeis.eis_rsc_ins.r_tem( 'WC Transaction Register','WC Transaction Register','Seeded template for WC Transaction Register','','','','','','','','','','','WC Transaction Register.rtf','LA023190','X','','','Y','Y','','');
--Inserting Report Portals - WC Transaction Register
--inserting report dashboards - WC Transaction Register
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC Transaction Register','222','XXEIS_AR_TRANSACTIONS_V','XXEIS_AR_TRANSACTIONS_V','N','');
--inserting report security - WC Transaction Register
xxeis.eis_rsc_ins.rsec( 'WC Transaction Register','222','','XXWC_RECEIVABLES_INQUIRY_WC',222,'LA023190','','','');
--Inserting Report Pivots - WC Transaction Register
xxeis.eis_rsc_ins.rpivot( 'WC Transaction Register',222,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Transaction Register',222,'Pivot','ACCOUNTED_AMOUNT','DATA_FIELD','SUM','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Transaction Register',222,'Pivot','BILL_TO_CUSTOMER_NAME','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Transaction Register',222,'Pivot','BILL_TO_CUSTOMER_NUM','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Transaction Register',222,'Pivot','COMMITMENT_NUMBER','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Transaction Register',222,'Pivot','INVOICE_CURRENCY_CODE','ROW_FIELD','','','4','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Transaction Register',222,'Pivot','RECEIVABLE_AMOUNT','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Transaction Register',222,'Pivot','SHIP_TO_CUSTOMER_NAME','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Transaction Register',222,'Pivot','TRX_CLASS','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Transaction Register',222,'Pivot','TRX_DATE','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Transaction Register',222,'Pivot','TRX_NUMBER','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Transaction Register',222,'Pivot','TRX_REC_ACCOUNT','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Transaction Register',222,'Pivot','TRX_TYPE_NAME','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Transaction Register',222,'Pivot','OPERATING_UNIT','ROW_FIELD','','','1','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- WC Transaction Register
xxeis.eis_rsc_ins.rv( 'WC Transaction Register','','WC Transaction Register','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
