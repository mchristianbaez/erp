--Report Name            : HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_MILEAGE_DETAILS_ORG_163
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_MILEAGE_DETAILS_ORG_163
xxeis.eis_rsc_ins.v( 'EIS_MILEAGE_DETAILS_ORG_163',91000,'IEXPENSE_TRANSACTION_DETAILS_ORG_163','1.0','','','XXEIS_RS_ADMIN','APPS','IEXPENSE_TRANSACTION_DETAILS_ORG_163','ERDO8','','','VIEW','US','','');
--Delete Object Columns for EIS_MILEAGE_DETAILS_ORG_163
xxeis.eis_rsc_utility.delete_view_rows('EIS_MILEAGE_DETAILS_ORG_163',91000,FALSE);
--Inserting Object Columns for EIS_MILEAGE_DETAILS_ORG_163
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','PERIOD_NAME',91000,'Period Name','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ORACLE_PRODUCT',91000,'Oracle Product','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Product','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ORACLE_LOCATION',91000,'Oracle Location','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ORACLE_COST_CENTER',91000,'Oracle Cost Center','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','PRODUCT_DESCRIPTION',91000,'Product Description','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Product Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','LOCATION_DESCRIPTION',91000,'Location Description','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ACCOUNT_DESCRIPTION',91000,'Account Description','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Account Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','LINE_AMOUNT',91000,'Line Amount','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Line Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','INVOICE_NUM',91000,'Invoice Num','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','DESCRIPTION',91000,'Description','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','FULL_NAME',91000,'Full Name','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Full Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','EMP_BRANCH',91000,'Emp Branch','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Emp Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ITEM_DESCRIPTION',91000,'Item Description','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','TRIP_DISTANCE',91000,'Trip Distance','','','','','XXEIS_RS_ADMIN','NUMBER','','','Trip Distance','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','DISTANCE_UNIT_CODE',91000,'Distance Unit Code','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Distance Unit Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','DESTINATION_FROM',91000,'Destination From','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Destination From','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','DESTINATION_TO',91000,'Destination To','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Destination To','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','JUSTIFICATION',91000,'Justification','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Justification','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','START_EXPENSE_DATE',91000,'Start Expense Date','','','','','XXEIS_RS_ADMIN','DATE','','','Start Expense Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','END_EXPENSE_DATE',91000,'End Expense Date','','','','','XXEIS_RS_ADMIN','DATE','','','End Expense Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','CATEGORY_CODE',91000,'Category Code','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Category Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','DT_SENTTO_GL',91000,'Dt Sentto Gl','','','','','XXEIS_RS_ADMIN','DATE','','','Dt Sentto Gl','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','POSTED_TO_GL',91000,'Posted To Gl','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Posted To Gl','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','OVERRIDE_APPROVER_ID',91000,'Override Approver Id','','','','','XXEIS_RS_ADMIN','NUMBER','','','Override Approver Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','OVERRIDE_APPROVER_NAME',91000,'Override Approver Name','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Override Approver Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','REPORT_TYPE',91000,'Report Type','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Report Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','IEXP_ACCOUNT',91000,'Iexp Account','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Iexp Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ORACLE_ACCOUNT',91000,'Oracle Account','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ORACLE_ACCOUNTS',91000,'Oracle Accounts','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Accounts','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','COST_CENTER_DESCRIPTION',91000,'Cost Center Description','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cost Center Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','DISTRIBUTION_LINE_NUMBER',91000,'Distribution Line Number','','','','','XXEIS_RS_ADMIN','NUMBER','','','Distribution Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ACCOUNTING_DATE',91000,'Accounting Date','','','','','XXEIS_RS_ADMIN','DATE','','','Accounting Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','REPORT_TOTAL',91000,'Report Total','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Report Total','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','EMPLOYEE_NUMBER',91000,'Employee Number','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Employee Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','SEGMENT1',91000,'Segment1','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','SEGMENT2',91000,'Segment2','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','SEGMENT3',91000,'Segment3','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment3','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','CREATION_DATE',91000,'Creation Date','','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','REPORT_SUBMITTED_DATE',91000,'Report Submitted Date','','','','','XXEIS_RS_ADMIN','DATE','','','Report Submitted Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','MILES',91000,'Miles','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Miles','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','RATE_PER_MILE',91000,'Rate Per Mile','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Rate Per Mile','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','DAILY_DISTANCE',91000,'Daily Distance','','','','','XXEIS_RS_ADMIN','NUMBER','','','Daily Distance','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','AVG_MILEAGE_RATE',91000,'Avg Mileage Rate','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Avg Mileage Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','CREDIT_CARD_TRX_ID',91000,'Credit Card Trx Id','','','','','XXEIS_RS_ADMIN','NUMBER','','','Credit Card Trx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','REMARKS',91000,'Remarks','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Remarks','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ATTENDEES_CUST',91000,'Attendees Cust','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees Cust','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ATTENDEES_EMP',91000,'Attendees Emp','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees Emp','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','BUSINESS_PURPOSE',91000,'Business Purpose','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Business Purpose','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ATTRIBUTE_CATEGORY',91000,'Attribute Category','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ATTENDEES_TE',91000,'Attendees Te','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees Te','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ATTENDEES_CTI1',91000,'Attendees Cti1','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees Cti1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ATTENDEES_CTI2',91000,'Attendees Cti2','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees Cti2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ATTENDEES',91000,'Attendees','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','TRANSACTION_DATE',91000,'Transaction Date','','','','','XXEIS_RS_ADMIN','DATE','','','Transaction Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','MERCHANT_NAME1',91000,'Merchant Name1','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Merchant Name1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','MERCHANT_LOCATION',91000,'Merchant Location','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Merchant Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','TRANSACTION_AMOUNT',91000,'Transaction Amount','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Transaction Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','CURRENCY_CODE',91000,'Currency Code','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','RECEIPT_CURRENCY_AMOUNT',91000,'Receipt Currency Amount','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Receipt Currency Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','AMT_DUE_CCARD_COMPANY',91000,'Amt Due Ccard Company','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amt Due Ccard Company','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','AMT_DUE_EMPLOYEE',91000,'Amt Due Employee','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amt Due Employee','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','IMPORTED_TO_AP',91000,'Imported To Ap','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Imported To Ap','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','INVOICE_DISTRIBUTION_ID',91000,'Invoice Distribution Id','','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Distribution Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ACCOUNTING_EVENT_ID',91000,'Accounting Event Id','','','','','XXEIS_RS_ADMIN','NUMBER','','','Accounting Event Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','COMPANY_PREPAID_INVOICE_ID',91000,'Company Prepaid Invoice Id','','','','','XXEIS_RS_ADMIN','NUMBER','','','Company Prepaid Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','WORKFLOW_APPROVED_FLAG',91000,'Workflow Approved Flag','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Workflow Approved Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','EXPENSE_CURRENT_APPROVER_ID',91000,'Expense Current Approver Id','','','','','XXEIS_RS_ADMIN','NUMBER','','','Expense Current Approver Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','APP_FULL_NAME',91000,'App Full Name','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','App Full Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ADVANCE_INVOICE_TO_APPLY',91000,'Advance Invoice To Apply','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Advance Invoice To Apply','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ADVANCE_AMOUNT',91000,'Advance Amount','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Advance Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ADVANCE_DISTRIBUTION_NUMBER',91000,'Advance Distribution Number','','','','','XXEIS_RS_ADMIN','NUMBER','','','Advance Distribution Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ADVANCE_FLAG',91000,'Advance Flag','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Advance Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ADVANCE_GL_DATE',91000,'Advance Gl Date','','','','','XXEIS_RS_ADMIN','DATE','','','Advance Gl Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','ADVANCE_NUMBER',91000,'Advance Number','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Advance Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','MERCHANT_NAME',91000,'Merchant Name','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Merchant Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','REPORT_REJECT_CODE',91000,'Report Reject Code','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Report Reject Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','APP_POST_FLAG',91000,'App Post Flag','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','App Post Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','APPROVED_IN_GL',91000,'Approved In Gl','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Approved In Gl','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','VOUCHNO',91000,'Vouchno','','','','','XXEIS_RS_ADMIN','NUMBER','','','Vouchno','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','IEXP_PRODUCT',91000,'Iexp Product','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Iexp Product','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','IEXP_LOCATION',91000,'Iexp Location','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Iexp Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','IEXP_COST_CENTER',91000,'Iexp Cost Center','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Iexp Cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','IEXP_ACCOUNTS',91000,'Iexp Accounts','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Iexp Accounts','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','AP_PRODUCT',91000,'Ap Product','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ap Product','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','AP_LOCATION',91000,'Ap Location','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ap Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','AP_COST_CENTER',91000,'Ap Cost Center','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ap Cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','AP_ACCOUNT',91000,'Ap Account','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ap Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_MILEAGE_DETAILS_ORG_163','AP_ACCOUNTS',91000,'Ap Accounts','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ap Accounts','','','','US');
--Inserting Object Components for EIS_MILEAGE_DETAILS_ORG_163
--Inserting Object Component Joins for EIS_MILEAGE_DETAILS_ORG_163
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
prompt Creating Report LOV Data for HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB
xxeis.eis_rsc_ins.lov( '','SELECT FLEX_VALUE  FROM (select fvt.description, ffv.flex_value
  from apps.fnd_flex_values_tl fvt, apps.fnd_flex_values ffv
  where fvt.flex_value_id = ffv.flex_value_id
              and ffv.flex_value_set_id = 1014547
  union all
 select null,null from dual
   order by flex_value
) CUO318447 ','','XXCUS_ORACLE_PRODUCT FLEX VAL LOV','LOV Item Class "Flexfield Values Product LOV".','ID020048',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 91000,'select distinct period_name
from ap.ap_invoice_distributions_all','','LOV_Period_Name','Period Name from the ap.ap_invoice_distributions_all table','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
prompt Creating Report Data for HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB
xxeis.eis_rsc_utility.delete_report_rows( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB' );
--Inserting Report - HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB
xxeis.eis_rsc_ins.r( 91000,'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','','','','','','XXEIS_RS_ADMIN','EIS_MILEAGE_DETAILS_ORG_163','Y','','','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','Y','','','','','','','','US','','','','');
--Inserting Report Columns - HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'PERIOD_NAME','Period Name','Period Name','VARCHAR2','','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'ORACLE_PRODUCT','Oracle Product','Oracle Product','VARCHAR2','','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'PRODUCT_DESCRIPTION','Product Description','Product Description','VARCHAR2','','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'ORACLE_LOCATION','Oracle Location','Oracle Location','VARCHAR2','','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'LOCATION_DESCRIPTION','Location Description','Location Description','VARCHAR2','','default','','5','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'ORACLE_COST_CENTER','Oracle Cost Center','Oracle Cost Center','VARCHAR2','','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'IEXP_ACCOUNT','Oracle Account','Iexp Account','VARCHAR2','','default','','7','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'EMP_BRANCH','Emp Branch','Emp Branch','VARCHAR2','','default','','8','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'CATEGORY_CODE','Category Code','Category Code','VARCHAR2','','default','','9','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'ITEM_DESCRIPTION','Item Description','Item Description','VARCHAR2','','default','','10','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'INVOICE_NUM','Invoice Num','Invoice Num','VARCHAR2','','default','','11','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'FULL_NAME','Full Name','Full Name','VARCHAR2','','default','','12','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'TRIP_DISTANCE','Trip Distance','Trip Distance','NUMBER','~~~','default','','13','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'DISTANCE_UNIT_CODE','Distance Unit Code','Distance Unit Code','VARCHAR2','','default','','14','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'LINE_AMOUNT','Line Amount','Line Amount','NUMBER','~T~D~2','default','','15','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'MILEAGE_RATE','Mileage Rate','Line Amount','NUMBER','~~~','default','','16','Y','Y','','','','','','SUM(ERDO8.LINE_AMOUNT) / (ERDO8.TRIP_DISTANCE)','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'DESCRIPTION','Description','Description','VARCHAR2','','default','','17','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'ACCOUNT_DESCRIPTION','Account Description','Account Description','VARCHAR2','','default','','18','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'DESTINATION_FROM','Destination From','Destination From','VARCHAR2','','default','','19','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'DESTINATION_TO','Destination To','Destination To','VARCHAR2','','default','','20','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'JUSTIFICATION','Justification','Justification','VARCHAR2','','default','','21','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'START_EXPENSE_DATE','Start Expense Date','Start Expense Date','DATE','','default','','22','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'END_EXPENSE_DATE','End Expense Date','End Expense Date','DATE','','default','','23','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'DT_SENTTO_GL','Dt Sentto Gl','Dt Sentto Gl','DATE','','default','','24','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'POSTED_TO_GL','Posted To Gl','Posted To Gl','VARCHAR2','','default','','25','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'OVERRIDE_APPROVER_ID','Approver Id','Override Approver Id','NUMBER','~~~','default','','26','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'OVERRIDE_APPROVER_NAME','Approver Name','Override Approver Name','VARCHAR2','','default','','27','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'REPORT_TYPE','Report Type','Report Type','VARCHAR2','','default','','28','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_MILEAGE_DETAILS_ORG_163','','','GROUP_BY','US','');
--Inserting Report Parameters - HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB
xxeis.eis_rsc_ins.rp( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'GL Posting Period','Example, ''Oct-2007''','PERIOD_NAME','IN','LOV_Period_Name','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_MILEAGE_DETAILS_ORG_163','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'Oracle Product (LOB)','Example, ''06''','ORACLE_PRODUCT','IN','XXCUS_ORACLE_PRODUCT FLEX VAL LOV','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_MILEAGE_DETAILS_ORG_163','','','US','');
--Inserting Dependent Parameters - HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB
--Inserting Report Conditions - HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB
xxeis.eis_rsc_ins.rcnh( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'CATEGORY_CODE = ''MILEAGE'' ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CATEGORY_CODE','','','','','','','EIS_MILEAGE_DETAILS_ORG_163','','','','','','EQUALS','Y','N','','''MILEAGE''','','','1',91000,'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','CATEGORY_CODE = ''MILEAGE'' ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'ORACLE_PRODUCT IN :Oracle Product (LOB) ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORACLE_PRODUCT','','Oracle Product (LOB)','','','','','EIS_MILEAGE_DETAILS_ORG_163','','','','','','IN','Y','Y','','','','','1',91000,'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','ORACLE_PRODUCT IN :Oracle Product (LOB) ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'PERIOD_NAME IN :GL Posting Period ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PERIOD_NAME','','GL Posting Period','','','','','EIS_MILEAGE_DETAILS_ORG_163','','','','','','IN','Y','Y','','','','','1',91000,'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','PERIOD_NAME IN :GL Posting Period ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'TRIP_DISTANCE <> 0 ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TRIP_DISTANCE','','','','','','','EIS_MILEAGE_DETAILS_ORG_163','','','','','','NOTEQUALS','Y','N','','0','','','1',91000,'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','TRIP_DISTANCE <> 0 ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'Free Text ','FREE_TEXT','','','N','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','N','','','','','and group by
ERDO8.PERIOD_NAME,
       ERDO8.ORACLE_PRODUCT,
       ERDO8.PRODUCT_DESCRIPTION,
       ERDO8.ORACLE_LOCATION,
       ERDO8.LOCATION_DESCRIPTION,
       ERDO8.ORACLE_COST_CENTER,
       ERDO8.IEXP_ACCOUNT,
       ERDO8.EMP_BRANCH,
       ERDO8.CATEGORY_CODE,
       ERDO8.ITEM_DESCRIPTION,
       ERDO8.INVOICE_NUM,
       ERDO8.FULL_NAME,
       ERDO8.TRIP_DISTANCE,
       ERDO8.DISTANCE_UNIT_CODE,
       ERDO8.LINE_AMOUNT,
       ERDO8.LINE_AMOUNT,
       ERDO8.TRIP_DISTANCE,
       ERDO8.DESCRIPTION,
       ERDO8.ACCOUNT_DESCRIPTION,
       ERDO8.DESTINATION_FROM,
       ERDO8.DESTINATION_TO,
       ERDO8.JUSTIFICATION,
       TO_CHAR(ERDO8.START_EXPENSE_DATE, ''YYYY/MM/DD HH:MI:SS AM''),
       TO_CHAR(ERDO8.END_EXPENSE_DATE, ''YYYY/MM/DD HH:MI:SS AM''),
       TO_CHAR(ERDO8.DT_SENTTO_GL, ''YYYY/MM/DD HH:MI:SS AM''),
       ERDO8.POSTED_TO_GL,
       ERDO8.OVERRIDE_APPROVER_ID,
       ERDO8.OVERRIDE_APPROVER_NAME,
       ERDO8.REPORT_TYPE','1',91000,'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','Free Text ');
--Inserting Report Sorts - HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB
--Inserting Report Triggers - HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB
--inserting report templates - HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB
--Inserting Report Portals - HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB
--inserting report dashboards - HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','91000','EIS_MILEAGE_DETAILS_ORG_163','EIS_MILEAGE_DETAILS_ORG_163','N','');
--inserting report security - HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','HDS_AP_TRNS_ENTRY_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','HDS_AP_INQUIRY_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','HDS_AP_DISBURSEMTS_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','HDS_AP_ADMIN_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','PAYABLES_MANAGER',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','PAYABLES_INQUIRY',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','HDS_OIE_USER',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','HDS_AP_MGR_NOSUP_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','20005','','XXWC_VIEW_ALL_EIS_REPORTS',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','HDS_PYBLS_MNGR',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','HDS_PYABLS_MNGR_CAN',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','101','','XXCUS_GL_MANAGER_GLOBAL',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','101','','XXCUS_GL_ACCOUNTANT_GLOBAL',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','HDS_AP_MGR_NOSUP_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','HDS_AP_INQ_CANADA',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','HDS_AP_DISBURSEMTS_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','200','','HDS_AP_ADMIN_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB
xxeis.eis_rsc_ins.rpivot( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'Pivot','TRIP_DISTANCE','DATA_FIELD','SUM','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB',91000,'Pivot','LINE_AMOUNT','DATA_FIELD','SUM','','','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB
xxeis.eis_rsc_ins.rv( 'HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','','HDS AP MILEAGE_BY_LOB - IExpense Trxns Mileage by LOB','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
