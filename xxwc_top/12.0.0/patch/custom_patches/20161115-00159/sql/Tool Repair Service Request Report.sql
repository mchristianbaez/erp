--Report Name            : Tool Repair Service Request Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_OM_TOOLS_REPAIR_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_TOOLS_REPAIR_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_TOOLS_REPAIR_V',660,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Om Tools Repair V','EXOTRV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_OM_TOOLS_REPAIR_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_TOOLS_REPAIR_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_TOOLS_REPAIR_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','CUSTOMER_JOB_NAME',660,'Customer Job Name','CUSTOMER_JOB_NAME','','','','ANONYMOUS','VARCHAR2','','','Customer Job Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','TECHNICIAN',660,'Technician','TECHNICIAN','','','','ANONYMOUS','VARCHAR2','','','Technician','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','OWNER',660,'Owner','OWNER','','','','ANONYMOUS','VARCHAR2','','','Owner','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','STATUS',660,'Status','STATUS','','','','ANONYMOUS','VARCHAR2','','','Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','REQUEST_TYPE',660,'Request Type','REQUEST_TYPE','','','','ANONYMOUS','VARCHAR2','','','Request Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','ANONYMOUS','VARCHAR2','','','Customer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','ACCOUNT_NUMBER',660,'Account Number','ACCOUNT_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Account Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','REPAIR_NUMBER',660,'Repair Number','REPAIR_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Repair Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','DATE_OPENED',660,'Date Opened','DATE_OPENED','','','','ANONYMOUS','DATE','','','Date Opened','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','REQUEST_NUMBER',660,'Request Number','REQUEST_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Request Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','REPAIR_TYPE',660,'Repair Type','REPAIR_TYPE','','','','ANONYMOUS','VARCHAR2','','','Repair Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','ORGANIZATION_CODE',660,'Organization Code','ORGANIZATION_CODE','','','','ANONYMOUS','VARCHAR2','','','Organization Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','RESOURCE_ID',660,'Resource Id','RESOURCE_ID','','','','ANONYMOUS','NUMBER','','','Resource Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','REPAIR_LINE_ID',660,'Repair Line Id','REPAIR_LINE_ID','','','','ANONYMOUS','NUMBER','','','Repair Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','INCIDENT_TYPE_ID',660,'Incident Type Id','INCIDENT_TYPE_ID','','','','ANONYMOUS','NUMBER','','','Incident Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','ANONYMOUS','NUMBER','','','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','REPAIR_TYPE_ID',660,'Repair Type Id','REPAIR_TYPE_ID','','','','ANONYMOUS','NUMBER','','','Repair Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','EFFECTIVE_END_DATE',660,'Effective End Date','EFFECTIVE_END_DATE','','','','ANONYMOUS','DATE','','','Effective End Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','EFFECTIVE_START_DATE',660,'Effective Start Date','EFFECTIVE_START_DATE','','','','ANONYMOUS','DATE','','','Effective Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','PERSON_ID',660,'Person Id','PERSON_ID','','','','ANONYMOUS','NUMBER','','','Person Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','SITE_USE_ID',660,'Site Use Id','SITE_USE_ID','','','','ANONYMOUS','NUMBER','','','Site Use Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','ANONYMOUS','NUMBER','','','Cust Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','ANONYMOUS','NUMBER','','','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','INCIDENT_ID',660,'Incident Id','INCIDENT_ID','','','','ANONYMOUS','NUMBER','','','Incident Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','EMPLOYEE_NAME',660,'Employee Name','EMPLOYEE_NAME','','','','ANONYMOUS','VARCHAR2','','','Employee Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','ITEM',660,'Item','ITEM','','','','ANONYMOUS','VARCHAR2','','','Item','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_TOOLS_REPAIR_V','QTY',660,'Qty','QTY','','~T~D~2','','ANONYMOUS','NUMBER','','','Qty','','','','US');
--Inserting Object Components for EIS_XXWC_OM_TOOLS_REPAIR_V
--Inserting Object Component Joins for EIS_XXWC_OM_TOOLS_REPAIR_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Tool Repair Service Request Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Tool Repair Service Request Report
xxeis.eis_rsc_ins.lov( 660,'SELECT DISTINCT source_name Owner
FROM JTF_RS_RESOURCE_EXTNS jrre,
  CS_INCIDENTS_ALL_B sr
WHERE sr.incident_owner_id = jrre.resource_id','','XXWC CSD OWNER','CSD Owners','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT DISTINCT source_name Technician
FROM JTF_RS_RESOURCE_EXTNS jrre,
      CSD_REPAIRS csd
WHERE csd.resource_id = jrre.resource_id','','XXWC CSD Technician','XXWC CSD Technician','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT flow_status_meaning Repir_Status
  FROM CSD_REPAIR_FLOW_STATUSES_V','','XXWC CSD STATUS','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT DISTINCT NAME FROM CSD_REPAIR_TYPES_VL
WHERE  NAME NOT in (''Advance Exchange'',''Credit Only'',''Exchange'',''Loaner'',''Loaner, Repair and Return'',''Reburbishment'',''Repair and Return'',''Replacement'',''Standard'',''Third Party Repair and Return'',''Walk-In Repair'',''Walk-In Repair with Loaner'')
UNION  SELECT ''All'' from Dual','','XXWC Repair Type','This Lov Provides list of Repair types.','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Tool Repair Service Request Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Tool Repair Service Request Report
xxeis.eis_rsc_utility.delete_report_rows( 'Tool Repair Service Request Report' );
--Inserting Report - Tool Repair Service Request Report
xxeis.eis_rsc_ins.r( 660,'Tool Repair Service Request Report','','Tool Repair Service Request Report','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_TOOLS_REPAIR_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','','','','','','','','','US','','','','');
--Inserting Report Columns - Tool Repair Service Request Report
xxeis.eis_rsc_ins.rc( 'Tool Repair Service Request Report',660,'ACCOUNT_NUMBER','Customer Number','Account Number','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Tool Repair Service Request Report',660,'CUSTOMER_JOB_NAME','Customer Job Name','Customer Job Name','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Tool Repair Service Request Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Tool Repair Service Request Report',660,'DATE_OPENED','Date Opened','Date Opened','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Tool Repair Service Request Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Tool Repair Service Request Report',660,'OWNER','Owner','Owner','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Tool Repair Service Request Report',660,'REPAIR_NUMBER','Repair Number','Repair Number','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Tool Repair Service Request Report',660,'REQUEST_NUMBER','Request Num','Request Number','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Tool Repair Service Request Report',660,'REQUEST_TYPE','Order Type','Request Type','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Tool Repair Service Request Report',660,'STATUS','Status','Status','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Tool Repair Service Request Report',660,'TECHNICIAN','Technician','Technician','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Tool Repair Service Request Report',660,'REPAIR_TYPE','Repair Type','Repair Type','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','GROUP_BY','US','');
--Inserting Report Parameters - Tool Repair Service Request Report
xxeis.eis_rsc_ins.rp( 'Tool Repair Service Request Report',660,'Repair Type','','REPAIR_TYPE','IN','XXWC Repair Type','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Tool Repair Service Request Report',660,'Owner','','OWNER','IN','XXWC CSD OWNER','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Tool Repair Service Request Report',660,'Technician','','TECHNICIAN','IN','XXWC CSD Technician','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Tool Repair Service Request Report',660,'Status','','STATUS','IN','XXWC CSD STATUS','','VARCHAR2','Y','Y','4','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Tool Repair Service Request Report',660,'Start Date','','DATE_OPENED','>=','','','DATE','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Tool Repair Service Request Report',660,'End Date','','DATE_OPENED','<=','','','DATE','N','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','US','');
--Inserting Dependent Parameters - Tool Repair Service Request Report
--Inserting Report Conditions - Tool Repair Service Request Report
xxeis.eis_rsc_ins.rcnh( 'Tool Repair Service Request Report',660,'DATE_OPENED <= :End Date ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DATE_OPENED','','End Date','','','','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',660,'Tool Repair Service Request Report','DATE_OPENED <= :End Date ');
xxeis.eis_rsc_ins.rcnh( 'Tool Repair Service Request Report',660,'DATE_OPENED >= :Start Date ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DATE_OPENED','','Start Date','','','','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',660,'Tool Repair Service Request Report','DATE_OPENED >= :Start Date ');
xxeis.eis_rsc_ins.rcnh( 'Tool Repair Service Request Report',660,'OWNER IN :Owner ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','OWNER','','Owner','','','','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','','','','IN','Y','Y','','','','','1',660,'Tool Repair Service Request Report','OWNER IN :Owner ');
xxeis.eis_rsc_ins.rcnh( 'Tool Repair Service Request Report',660,'STATUS IN :Status ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','STATUS','','Status','','','','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','','','','IN','Y','Y','','','','','1',660,'Tool Repair Service Request Report','STATUS IN :Status ');
xxeis.eis_rsc_ins.rcnh( 'Tool Repair Service Request Report',660,'TECHNICIAN IN :Technician ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TECHNICIAN','','Technician','','','','','EIS_XXWC_OM_TOOLS_REPAIR_V','','','','','','IN','Y','Y','','','','','1',660,'Tool Repair Service Request Report','TECHNICIAN IN :Technician ');
xxeis.eis_rsc_ins.rcnh( 'Tool Repair Service Request Report',660,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND ( ''All'' IN (:Repair Type) OR (REPAIR_TYPE IN (:Repair Type)))
','1',660,'Tool Repair Service Request Report','Free Text ');
--Inserting Report Sorts - Tool Repair Service Request Report
xxeis.eis_rsc_ins.rs( 'Tool Repair Service Request Report',660,'REQUEST_NUMBER','ASC','XXEIS_RS_ADMIN','1','');
xxeis.eis_rsc_ins.rs( 'Tool Repair Service Request Report',660,'CUSTOMER_NAME','ASC','XXEIS_RS_ADMIN','2','');
xxeis.eis_rsc_ins.rs( 'Tool Repair Service Request Report',660,'REPAIR_NUMBER','ASC','XXEIS_RS_ADMIN','3','');
--Inserting Report Triggers - Tool Repair Service Request Report
--inserting report templates - Tool Repair Service Request Report
--Inserting Report Portals - Tool Repair Service Request Report
--inserting report dashboards - Tool Repair Service Request Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Tool Repair Service Request Report','660','EIS_XXWC_OM_TOOLS_REPAIR_V','EIS_XXWC_OM_TOOLS_REPAIR_V','N','');
--inserting report security - Tool Repair Service Request Report
xxeis.eis_rsc_ins.rsec( 'Tool Repair Service Request Report','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Tool Repair Service Request Report','660','','',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Tool Repair Service Request Report','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Tool Repair Service Request Report','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Tool Repair Service Request Report','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Tool Repair Service Request Report','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Tool Repair Service Request Report','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Tool Repair Service Request Report','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Tool Repair Service Request Report','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - Tool Repair Service Request Report
--Inserting Report   Version details- Tool Repair Service Request Report
xxeis.eis_rsc_ins.rv( 'Tool Repair Service Request Report','','Tool Repair Service Request Report','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
