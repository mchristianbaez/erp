--Report Name            : HDS Payment made to Vendor
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_VENDOR_PYMT
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_VENDOR_PYMT
xxeis.eis_rsc_ins.v( 'XXEIS_VENDOR_PYMT',200,'Paste SQL View for Payment made to Vendor','1.0','','','XXEIS_RS_ADMIN','XXEIS','XXEIS_VENDOR_PYMT','X4BV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_VENDOR_PYMT
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_VENDOR_PYMT',200,FALSE);
--Inserting Object Columns for XXEIS_VENDOR_PYMT
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_PYMT','VENDOR_NAME',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_PYMT','VENDOR_NUMBER',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_PYMT','VENDOR_SITE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Site','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_PYMT','TOTAL_DISBURSEMENT_AMOUNT',200,'','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Total Disbursement Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_PYMT','BANK_ACCOUNT_NAME',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bank Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_PYMT','CHECK_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Check Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_PYMT','ORG_ID',200,'','','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id','','','','US');
--Inserting Object Components for XXEIS_VENDOR_PYMT
xxeis.eis_rsc_ins.vcomp( 'XXEIS_VENDOR_PYMT','AP_INVOICES',200,'','ai','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','AP_INVOICES','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_VENDOR_PYMT','AP_CHECKS',200,'','ac','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','AP_CHECKS','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_VENDOR_PYMT','PO_VENDORS',200,'','po','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','PO_VENDORS','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_VENDOR_PYMT','PO_VENDOR_SITES_ALL',200,'','pos','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','PO_VENDOR_SITES_ALL','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_VENDOR_PYMT','AP_CHECKS_ALL',200,'','acl','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','AP_CHECKS_ALL','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_VENDOR_PYMT','AP_SUPPLIERS',200,'','po','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','AP_SUPPLIERS','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_VENDOR_PYMT','AP_SUPPLIER_SITES_ALL',200,'','pos','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','AP_SUPPLIER_SITES_ALL','','','','','ASSA','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for XXEIS_VENDOR_PYMT
xxeis.eis_rsc_ins.vcj( 'XXEIS_VENDOR_PYMT','PO_VENDORS','po',200,'BZPDGX.VENDOR_ID','=','po.vendor_id(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_VENDOR_PYMT','PO_VENDOR_SITES_ALL','pos',200,'BZPDGX.VENDOR_SITE_ID','=','pos.vendor_site_id(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_VENDOR_PYMT','AP_INVOICES','ai',200,'BZPDGX.INVOICE_ID','=','ai.invoice_id(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_VENDOR_PYMT','AP_CHECKS','ac',200,'BZPDGX.CHECK_ID','=','ac.check_id(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_VENDOR_PYMT','AP_SUPPLIERS','po',200,'BZPDGX.VENDOR_ID','=','po.vendor_id(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_VENDOR_PYMT','AP_SUPPLIER_SITES_ALL','pos',200,'BZPDGX.VENDOR_SITE_ID','=','pos.vendor_site_id(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXEIS_VENDOR_PYMT','AP_CHECKS_ALL','acl',200,'BZPDGX.VENDOR_SITE_ID','=','acl.vendor_site_id(+)','','','','','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report HDS Payment made to Vendor
prompt Creating Report Data for HDS Payment made to Vendor
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS Payment made to Vendor
xxeis.eis_rsc_utility.delete_report_rows( 'HDS Payment made to Vendor' );
--Inserting Report - HDS Payment made to Vendor
xxeis.eis_rsc_ins.r( 200,'HDS Payment made to Vendor','','Payment made to Vendor for analysis','','','','XXEIS_RS_ADMIN','XXEIS_VENDOR_PYMT','Y','','','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','Y','','','','','','','','US','','','','');
--Inserting Report Columns - HDS Payment made to Vendor
xxeis.eis_rsc_ins.rc( 'HDS Payment made to Vendor',200,'VENDOR_NAME','Vendor Name','','','','default','','5','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_PYMT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment made to Vendor',200,'VENDOR_NUMBER','Vendor Number','','','','default','','6','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_PYMT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment made to Vendor',200,'VENDOR_SITE','Vendor Site','','','','default','','7','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_PYMT','','','GROUP_BY','US','');
--Inserting Report Parameters - HDS Payment made to Vendor
xxeis.eis_rsc_ins.rp( 'HDS Payment made to Vendor',200,'Check Date From','Check Date From','','>=','','','DATE','N','Y','1','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','','XXEIS_VENDOR_PYMT','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payment made to Vendor',200,'Check Date To','Check Date To','','<=','','','DATE','N','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','','XXEIS_VENDOR_PYMT','','','US','');
--Inserting Dependent Parameters - HDS Payment made to Vendor
--Inserting Report Conditions - HDS Payment made to Vendor
xxeis.eis_rsc_ins.rcnh( 'HDS Payment made to Vendor',200,'CHECK_DATE BETWEEN :Check Date From  :Check Date To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECK_DATE','','Check Date From','','','Check Date To','','XXEIS_VENDOR_PYMT','','','','','','BETWEEN','Y','Y','','','','','1',200,'HDS Payment made to Vendor','CHECK_DATE BETWEEN :Check Date From  :Check Date To');
xxeis.eis_rsc_ins.rcnh( 'HDS Payment made to Vendor',200,'FreeText','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','Y','','','','and   X4BV.ORG_ID = 163
and   X4BV.BANK_ACCOUNT_NAME <> ''IEXPENSE PAYMENT CLEARING''
Having sum(X4BV.TOTAL_DISBURSEMENT_AMOUNT) > 600','1',200,'HDS Payment made to Vendor','FreeText');
--Inserting Report Sorts - HDS Payment made to Vendor
--Inserting Report Triggers - HDS Payment made to Vendor
--inserting report templates - HDS Payment made to Vendor
--Inserting Report Portals - HDS Payment made to Vendor
--inserting report dashboards - HDS Payment made to Vendor
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS Payment made to Vendor','200','XXEIS_VENDOR_PYMT','XXEIS_VENDOR_PYMT','N','');
--inserting report security - HDS Payment made to Vendor
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','HDS_AP_TRNS_ENTRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','HDS_AP_INQUIRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','HDS_AP_DISBURSEMTS_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','HDS_PYABLS_MNGR_CAN',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','HDS_PYBLS_MNGR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','HDS_AP_ADMIN_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','PAYABLES_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','HDS_OIE_USER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','HDS_AP_MGR_NOSUP_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','HDS_AP_ADMIN_US_GSCIWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','XXWC_PAY_NO_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','XXWC_PAY_W_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','XXWC_PAY_DISBURSE',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','XXWC_PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','XXWC_PAY_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','XXWC_PAY_VENDOR_MSTR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','HDS_AP_MGR_NOSUP_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','HDS_AP_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','20005','','XXWC_VIEW_ALL_EIS_REPORTS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','HDS_AP_MGR_NOSUP_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','HDS_AP_DISBURSEMTS_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','HDS_AP_ADMIN_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','HDS_PAYABLES_CLOSE_GSC_GLBL',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment made to Vendor','200','','HDS_PAYABLES_CLOSE_GLBL',200,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS Payment made to Vendor
--Inserting Report   Version details- HDS Payment made to Vendor
xxeis.eis_rsc_ins.rv( 'HDS Payment made to Vendor','','HDS Payment made to Vendor','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
