--Report Name            : WC - Payment Register Summary
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_AP_PAY_REGIS_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_AP_PAY_REGIS_V
xxeis.eis_rsc_ins.v( 'EIS_AP_PAY_REGIS_V',200,'This view shows details of payments made.','','','','XXEIS_RS_ADMIN','XXEIS','EIS AP Payments Registered','EAPRV1','','','VIEW','US','Y','');
--Delete Object Columns for EIS_AP_PAY_REGIS_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_AP_PAY_REGIS_V',200,FALSE);
--Inserting Object Columns for EIS_AP_PAY_REGIS_V
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','BANK_ACCOUNT_TYPE',200,'Bank account type code','BANK_ACCOUNT_TYPE~BANK_ACCOUNT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_BANK_ACCOUNTS_ALL','BANK_ACCOUNT_TYPE','Bank Account Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','ACCOUNT_CLASSIFICATION',200,'Account Classification (INTERNAL, EXTERNAL)','ACCOUNT_CLASSIFICATION~ACCOUNT_CLASSIFICATION','','','','XXEIS_RS_ADMIN','VARCHAR2','CE_BANK_ACCOUNTS','ACCOUNT_CLASSIFICATION','Account Classification','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','PAYMENT_DOCUMENT',200,'Payment Document Name','PAYMENT_DOCUMENT~PAYMENT_DOCUMENT','','','','XXEIS_RS_ADMIN','VARCHAR2','CE_PAYMENT_DOCUMENTS','PAYMENT_DOCUMENT_NAME','Payment Document','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','CHECK_ID',200,'Payment identifier','CHECK_ID~CHECK_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CHECKS_ALL','CHECK_ID','Check Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','ADDRESS_LINE1',200,'First address line of payment','ADDRESS_LINE1','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CHECKS_ALL','ADDRESS_LINE1','Address Line1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','ADDRESS_LINE2',200,'Second address line of payment','ADDRESS_LINE2','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CHECKS_ALL','ADDRESS_LINE2','Address Line2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','ADDRESS_LINE3',200,'Third address line of payment','ADDRESS_LINE3','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CHECKS_ALL','ADDRESS_LINE3','Address Line3','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','CITY',200,'City','CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CHECKS_ALL','CITY','City','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','STATE',200,'State','STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CHECKS_ALL','STATE','State','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','ZIP',200,'State or province postal code','ZIP','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CHECKS_ALL','ZIP','Zip','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','COUNTRY',200,'The short name that stands for the territory','COUNTRY','','','','XXEIS_RS_ADMIN','VARCHAR2','FND_TERRITORIES_TL','TERRITORY_SHORT_NAME','Country','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','CLEARED_DATE',200,'Payment cleared date','CLEARED_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CHECKS_ALL','CLEARED_DATE','Cleared Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','CLEARED_AMOUNT',200,'Payment cleared amount','CLEARED_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CHECKS_ALL','CLEARED_AMOUNT','Cleared Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','BANK',200,'Bank name','BANK','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_BANK_BRANCHES','BANK_NAME','Bank','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','BRANCH',200,'Bank branch name','BRANCH','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_BANK_BRANCHES','BANK_BRANCH_NAME','Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','ACCOUNT',200,'Bank account name','ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CHECKS_ALL','BANK_ACCOUNT_NAME','Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','COMPANY_NAME',200,'Company Name','COMPANY_NAME~COMPANY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Company Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','PAYMENT_TYPE',200,'QuickCode meaning','PAYMENT_TYPE~PAYMENT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','FND_LOOKUP_VALUES','MEANING','Payment Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','OPERATING_UNIT',200,'Translated name of the organization','OPERATING_UNIT~OPERATING_UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','HR_ALL_ORGANIZATION_UNITS_TL','NAME','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','BANK_ACCOUNT_CURRENCY_CODE',200,'Currency Code','BANK_ACCOUNT_CURRENCY_CODE~BANK_ACCOUNT_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','CE_BANK_ACCOUNTS','CURRENCY_CODE','Bank Account Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','PAYMENT_AMOUNT',200,'Payment amount','PAYMENT_AMOUNT~PAYMENT_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CHECKS_ALL','AMOUNT','Payment Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','PAYMENT_CURRENCY_CODE',200,'Currency code','PAYMENT_CURRENCY_CODE~PAYMENT_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CHECKS_ALL','CURRENCY_CODE','Payment Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','PAYMENT_DATE',200,'Payment date','PAYMENT_DATE~PAYMENT_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CHECKS_ALL','CHECK_DATE','Payment Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','PAYMENT_NUMBER',200,'Payment number','PAYMENT_NUMBER~PAYMENT_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','AP_CHECKS_ALL','CHECK_NUMBER','Payment Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','STATUS',200,'QuickCode meaning','STATUS~STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','FND_LOOKUP_VALUES','MEANING','Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','SUPPLIER_NAME',200,'Supplier name','SUPPLIER_NAME~SUPPLIER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CHECKS_ALL','VENDOR_NAME','Supplier Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','SUPPLIER_SITE',200,'Supplier site code','SUPPLIER_SITE~SUPPLIER_SITE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CHECKS_ALL','VENDOR_SITE_CODE','Supplier Site','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','RELEASED_AT',200,'No longer used','RELEASED_AT~RELEASED_AT','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CHECKS_ALL','RELEASED_AT','Released At','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','TERRITORY_CODE',200,'The code for the territory','TERRITORY_CODE~TERRITORY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','FND_TERRITORIES','TERRITORY_CODE','Territory Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','REQUEST_ID',200,'Request Id','REQUEST_ID~REQUEST_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_BANK_ACCOUNTS_ALL','AP_BANK_ACCOUNTS_ALL','Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','STOPPED_AT',200,'No longer used','STOPPED_AT~STOPPED_AT','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CHECKS_ALL','STOPPED_AT','Stopped At','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','STOPPED_BY',200,'User that recorded stop payment','STOPPED_BY~STOPPED_BY','','','','XXEIS_RS_ADMIN','NUMBER','AP_CHECKS_ALL','STOPPED_BY','Stopped By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','UNEDISC_RECEIVABLES_TRX_ID',200,'Unearned Discounts Activity','UNEDISC_RECEIVABLES_TRX_ID~UNEDISC_RECEIVABLES_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_BANK_ACCOUNTS_ALL','UNEDISC_RECEIVABLES_TRX_ID','Unedisc Receivables Trx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','ASSET_CODE_COMBINATION_ID',200,'Accounting Flexfield identifier for cash account associated with bank account','ASSET_CODE_COMBINATION_ID~ASSET_CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_BANK_ACCOUNTS_ALL','ASSET_CODE_COMBINATION_ID','Asset Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','BANK_BRANCH_NAME_ALT',200,'Alternate bank branch name','BANK_BRANCH_NAME_ALT~BANK_BRANCH_NAME_ALT','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_BANK_BRANCHES','BANK_BRANCH_NAME_ALT','Bank Branch Name Alt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','BANK_BRANCH_TYPE',200,'Branch type indicates which list the bank routing number is on. Valid types are ABA, CHIPS, SWIFT and OTHER','BANK_BRANCH_TYPE~BANK_BRANCH_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_BANK_BRANCHES','BANK_BRANCH_TYPE','Bank Branch Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','BANK_CHARGES_CCID',200,'Accounting Flexfield identifier for the Bank Charges account','BANK_CHARGES_CCID~BANK_CHARGES_CCID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CHECK_STOCKS_ALL','BANK_CHARGES_CCID','Bank Charges Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','BANK_ERRORS_CCID',200,'Accounting Flexfield identifier for the Bank Errors account','BANK_ERRORS_CCID~BANK_ERRORS_CCID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CHECK_STOCKS_ALL','BANK_ERRORS_CCID','Bank Errors Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','BANK_NAME_ALT',200,'Alternate bank name','BANK_NAME_ALT~BANK_NAME_ALT','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_BANK_BRANCHES','BANK_NAME_ALT','Bank Name Alt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','CHECK_DIGITS',200,'Holds any check digits that result from bank account number validation in FBS','CHECK_DIGITS~CHECK_DIGITS','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_BANK_ACCOUNTS_ALL','CHECK_DIGITS','Check Digits','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','DOC_CATEGORY_CODE',200,'Sequential Numbering (voucher number) document category for payment','DOC_CATEGORY_CODE~DOC_CATEGORY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CHECKS_ALL','DOC_CATEGORY_CODE','Doc Category Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','DOC_SEQUENCE_ID',200,'Sequential Numbering document sequence identifier','DOC_SEQUENCE_ID~DOC_SEQUENCE_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CHECKS_ALL','DOC_SEQUENCE_ID','Doc Sequence Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','DOC_SEQUENCE_VALUE',200,'Voucher number (sequential numbering) for payment','DOC_SEQUENCE_VALUE~DOC_SEQUENCE_VALUE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CHECKS_ALL','DOC_SEQUENCE_VALUE','Doc Sequence Value','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','EDISC_RECEIVABLES_TRX_ID',200,'Earned Discounts Activity','EDISC_RECEIVABLES_TRX_ID~EDISC_RECEIVABLES_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_BANK_ACCOUNTS_ALL','EDISC_RECEIVABLES_TRX_ID','Edisc Receivables Trx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','EFT_USER_NUM',200,'EFT_USER_NUM','EFT_USER_NUM~EFT_USER_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','CE_BANK_ACCOUNTS','EFT_USER_NUM','Eft User Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','MULTI_CURRENCY_ALLOWED_FLAG',200,'Flag to indicate if the multiple currency is allowed','MULTI_CURRENCY_ALLOWED_FLAG~MULTI_CURRENCY_ALLOWED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','CE_BANK_ACCOUNTS','MULTI_CURRENCY_ALLOWED_FLAG','Multi Currency Allowed Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','START_DATE',200,'The date that the class code applies to the organization.','START_DATE~START_DATE','','','','XXEIS_RS_ADMIN','DATE','HZ_CODE_ASSIGNMENTS','START_DATE_ACTIVE','Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','RELEASED_BY',200,'User that released stop payment','RELEASED_BY~RELEASED_BY','','','','XXEIS_RS_ADMIN','NUMBER','AP_CHECKS_ALL','RELEASED_BY','Released By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','BANK_ACCOUNT_ID',200,'Bank Account Identifier','BANK_ACCOUNT_ID~BANK_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','CE_BANK_ACCOUNTS','BANK_ACCOUNT_ID','Bank Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','ORGANIZATION_ID',200,'Organization Id','ORGANIZATION_ID~ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','HR_ALL_ORGANIZATION_UNITS','ORGANIZATION_ID','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','EDI_ID_NUMBER',200,'EDI trading partner number for the Bank Branch','EDI_ID_NUMBER~EDI_ID_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_BANK_BRANCHES','EDI_ID_NUMBER','Edi Id Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','EFT_SWIFT_CODE',200,'EFT swift code','EFT_SWIFT_CODE~EFT_SWIFT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_BANK_BRANCHES','EFT_SWIFT_CODE','Eft Swift Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','END_DATE',200,'End date','END_DATE~END_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_BANK_BRANCHES','END_DATE','End Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','IBAN_NUMBER',200,'International bank account number.  Used internationally to uniquely identify the account of a customer at a financial institution. During bank entry, the system validates the value to ensure that it is a valid IBAN.','IBAN_NUMBER~IBAN_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_BANK_ACCOUNTS_ALL','IBAN_NUMBER','Iban Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','INACTIVE_DATE',200,'Inactive date','INACTIVE_DATE~INACTIVE_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_BANK_ACCOUNTS_ALL','INACTIVE_DATE','Inactive Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','MAX_CHECK_AMOUNT',200,'Default maximum payment amount allowed for bank account in a payment batch','MAX_CHECK_AMOUNT~MAX_CHECK_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_BANK_ACCOUNTS_ALL','MAX_CHECK_AMOUNT','Max Check Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','MAX_OUTLAY',200,'Maximum payment batch outlay for bank account','MAX_OUTLAY~MAX_OUTLAY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_BANK_ACCOUNTS_ALL','MAX_OUTLAY','Max Outlay','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','MIN_CHECK_AMOUNT',200,'Default minimum payment amount allowed for bank account in a payment batch','MIN_CHECK_AMOUNT~MIN_CHECK_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_BANK_ACCOUNTS_ALL','MIN_CHECK_AMOUNT','Min Check Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','ACCOUNTID',200,'No longer used','ACCOUNTID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CHECKS_ALL','BANK_ACCOUNT_ID','Accountid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','BANK_BRANCH',200,'Bank branch identifier','BANK_BRANCH','','','','XXEIS_RS_ADMIN','NUMBER','AP_BANK_BRANCHES','BANK_BRANCH_ID','Bank Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','PAYMENT_TYPE_FLAG',200,'Flag that indicates the payment type','PAYMENT_TYPE_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CHECKS_ALL','PAYMENT_TYPE_FLAG','Payment Type Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','ZERO_AMOUNT_ALLOWED',200,'Indicator for allowing or disallowing zero amount','ZERO_AMOUNT_ALLOWED~ZERO_AMOUNT_ALLOWED','','','','XXEIS_RS_ADMIN','VARCHAR2','CE_BANK_ACCOUNTS','ZERO_AMOUNT_ALLOWED','Zero Amount Allowed','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','ACCOUNT_HOLDER_NAME',200,'Account holder name','ACCOUNT_HOLDER_NAME~ACCOUNT_HOLDER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_BANK_ACCOUNTS_ALL','ACCOUNT_HOLDER_NAME','Account Holder Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','ACCOUNT_HOLDER_NAME_ALT',200,'Alternate account holder name','ACCOUNT_HOLDER_NAME_ALT~ACCOUNT_HOLDER_NAME_ALT','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_BANK_ACCOUNTS_ALL','ACCOUNT_HOLDER_NAME_ALT','Account Holder Name Alt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','CHECK_STOCK_ID',200,'Payment document identifier','CHECK_STOCK_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CHECKS_ALL','CHECK_STOCK_ID','Check Stock Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','AGENCY_LOCATION_CODE',200,'Agency Location code assigned by US Treasury','AGENCY_LOCATION_CODE~AGENCY_LOCATION_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_BANK_ACCOUNTS_ALL','AGENCY_LOCATION_CODE','Agency Location Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','ORG_ID',200,'Organization identifier','ORG_ID~ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CHECK_STOCKS_ALL','ORG_ID','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','POOLED_FLAG',200,'Pooled or non-pooled account indicator','POOLED_FLAG~POOLED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_BANK_ACCOUNTS_ALL','POOLED_FLAG','Pooled Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','PROGRAM_APPLICATION_ID',200,'Concurrent Program who column - application id of the program that last updated this row (foreign key to FND_APPLICATION.APPLICATION_ID).','PROGRAM_APPLICATION_ID~PROGRAM_APPLICATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_BANK_ACCOUNTS_ALL','PROGRAM_APPLICATION_ID','Program Application Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','PROGRAM_ID',200,'Concurrent Program who column - program id of the program that last updated this row (foreign key to FND_CONCURRENT_PROGRAM.CONCURRENT_PROGRAM_ID).','PROGRAM_ID~PROGRAM_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_BANK_ACCOUNTS_ALL','PROGRAM_ID','Program Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','PROGRAM_UPDATE_DATE',200,'Concurrent Program who column - date when a program last updated this row).','PROGRAM_UPDATE_DATE~PROGRAM_UPDATE_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_BANK_ACCOUNTS_ALL','PROGRAM_UPDATE_DATE','Program Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','PROVINCE',200,'No longer used','PROVINCE~PROVINCE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CHECKS_ALL','PROVINCE','Province','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','RECEIPT_MULTI_CURRENCY_FLAG',200,'Receipt Multi Currency Flag','RECEIPT_MULTI_CURRENCY_FLAG~RECEIPT_MULTI_CURRENCY_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','RECEIPT_MULTI_CURRENCY_FLAG','AP_BANK_ACCOUNTS_ALL','Receipt Multi Currency Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAY_REGIS_V','COPYRIGHT',200,'Copyright','COPYRIGHT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Copyright','','','','US');
--Inserting Object Components for EIS_AP_PAY_REGIS_V
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAY_REGIS_V','GL_SETS_OF_BOOKS',200,'GL_LEDGERS','GSOB','GSOB','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ledger Definition','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAY_REGIS_V','AP_CHECKS',200,'AP_CHECKS_ALL','CH','CH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Supplier Payment Data','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAY_REGIS_V','CE_BANK_ACCOUNTS',200,'CE_BANK_ACCOUNTS','BA','BA','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Bank Accounts','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAY_REGIS_V','HR_ORGANIZATION_UNITS',200,'HR_ALL_ORGANIZATION_UNITS','HOU','HOU','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Organization Details','N','','','','','Y',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_AP_PAY_REGIS_V
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAY_REGIS_V','AP_CHECKS','CH',200,'EAPRV1.CHECK_ID','=','CH.CHECK_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAY_REGIS_V','CE_BANK_ACCOUNTS','BA',200,'EAPRV1.BANK_ACCOUNT_ID','=','BA.BANK_ACCOUNT_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAY_REGIS_V','GL_SETS_OF_BOOKS','GSOB',200,'EAPRV1.COMPANY_NAME','=','GSOB.NAME(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAY_REGIS_V','HR_ORGANIZATION_UNITS','HOU',200,'EAPRV1.ORGANIZATION_ID','=','HOU.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--Exporting View Component Data of the View -  EIS_AP_PAY_REGIS_V
prompt Creating Object Data AP_CHECKS
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object AP_CHECKS
xxeis.eis_rsc_ins.v( 'AP_CHECKS',200,'Supplier payment data','1.0','','','ANONYMOUS','APPS','Ap Checks','AC','','','SYNONYM','US','','');
--Delete Object Columns for AP_CHECKS
xxeis.eis_rsc_utility.delete_view_rows('AP_CHECKS',200,FALSE);
--Inserting Object Columns for AP_CHECKS
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ATTRIBUTE4',200,'Descriptive flexfield segment','ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ATTRIBUTE4','Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','EXCHANGE_RATE_TYPE',200,'This is the exchange rate type like Spot, Corporate etc used for foreign currency payments','EXCHANGE_RATE_TYPE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','EXCHANGE_RATE_TYPE','Exchange Rate Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE13',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE13','Global Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','IBAN_NUMBER',200,'International bank account number. Used internationally to uniquely identify the account of a customer at a financial institution. During bank entry, the system validates the value to ensure that it is a valid IBAN.','IBAN_NUMBER','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','IBAN_NUMBER','Iban Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CE_BANK_ACCT_USE_ID',200,'This is the Bank account identifier assigned by cash management to uniquely identify the bank accounts usage by various functions like Payables, Receivables etc.','CE_BANK_ACCT_USE_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','CE_BANK_ACCT_USE_ID','Ce Bank Acct Use Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','PAYMENT_METHOD_CODE',200,'This is the unique identifier assigned to the payment method selected for payment.','PAYMENT_METHOD_CODE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','PAYMENT_METHOD_CODE','Payment Method Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','PARTY_ID',200,'This is the party identifier maintained for the supplier. This is maintained by Trading Community Architecture.','PARTY_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','PARTY_ID','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','PARTY_SITE_ID',200,'This is the party site identifier maintained for the supplier. This is maintained by Trading Community Architecture.','PARTY_SITE_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','PARTY_SITE_ID','Party Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','PAYMENT_PROFILE_ID',200,'This is the Unique identifier assigned to the Payment Process Profile. This is maintained by Oracle Payments.','PAYMENT_PROFILE_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','PAYMENT_PROFILE_ID','Payment Profile Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','SETTLEMENT_PRIORITY',200,'The priority with which the financial institution or payment system should settle payment for this document. The available values for this column come from the FND lookup IBY_SETTLEMENT_PRIORITY''','SETTLEMENT_PRIORITY','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','SETTLEMENT_PRIORITY','Settlement Priority','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','BANK_CHARGE_BEARER',200,'Bearer of bank charge cost. Bank charge bearers are defined as the lookup IBY_BANK_CHARGE_BEARER.','BANK_CHARGE_BEARER','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','BANK_CHARGE_BEARER','Bank Charge Bearer','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','LEGAL_ENTITY_ID',200,'Legal entity identifier','LEGAL_ENTITY_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','LEGAL_ENTITY_ID','Legal Entity Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','PAYMENT_DOCUMENT_ID',200,'Payment document identifier','PAYMENT_DOCUMENT_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','PAYMENT_DOCUMENT_ID','Payment Document Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','COMPLETED_PMTS_GROUP_ID',200,'This is the unique identifier assigned to the completed payments. This is maintained by Oracle Payments.','COMPLETED_PMTS_GROUP_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','COMPLETED_PMTS_GROUP_ID','Completed Pmts Group Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','PAYMENT_ID',200,'This is the unique identifier assigned to the payments in Oracle Payments.','PAYMENT_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','PAYMENT_ID','Payment Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','PAYMENT_INSTRUCTION_ID',200,'This is the unique identifier assigned to the payment instructions in Oracle Payments.','PAYMENT_INSTRUCTION_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','PAYMENT_INSTRUCTION_ID','Payment Instruction Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','VOID_CHECK_NUMBER',200,'Voided check number','VOID_CHECK_NUMBER','','','','ANONYMOUS','NUMBER','AP_CHECKS','VOID_CHECK_NUMBER','Void Check Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','VOID_CHECK_ID',200,'Voided check unique identifier','VOID_CHECK_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','VOID_CHECK_ID','Void Check Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','REMIT_TO_SUPPLIER_NAME',200,'Name of the supplier to whom invoice amount will be remitted','REMIT_TO_SUPPLIER_NAME','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','REMIT_TO_SUPPLIER_NAME','Remit To Supplier Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','REMIT_TO_SUPPLIER_ID',200,'Unique supplier identifier to whom invoice amount will be remitted','REMIT_TO_SUPPLIER_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','REMIT_TO_SUPPLIER_ID','Remit To Supplier Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','REMIT_TO_SUPPLIER_SITE',200,'Name of the supplier site to whom invoice amount will be remitted','REMIT_TO_SUPPLIER_SITE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','REMIT_TO_SUPPLIER_SITE','Remit To Supplier Site','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','REMIT_TO_SUPPLIER_SITE_ID',200,'Unique supplier site identifier to whom invoice amount will be remitted','REMIT_TO_SUPPLIER_SITE_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','REMIT_TO_SUPPLIER_SITE_ID','Remit To Supplier Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','RELATIONSHIP_ID',200,'Unique identifier to third party relationships of a supplier with another supplier. This column will be populated if the payment is made to the third party.','RELATIONSHIP_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','RELATIONSHIP_ID','Relationship Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','PAYCARD_AUTHORIZATION_NUMBER',200,'This is the authorization number captured for the payments made using payment card.','PAYCARD_AUTHORIZATION_NUMBER','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','PAYCARD_AUTHORIZATION_NUMBER','Paycard Authorization Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','PAYCARD_REFERENCE_ID',200,'Unique identifier for the payment card used for the payment.','PAYCARD_REFERENCE_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','PAYCARD_REFERENCE_ID','Paycard Reference Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','AMOUNT',200,'Indicates payment amount in payment currency.','AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_CHECKS','AMOUNT','Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','BANK_ACCOUNT_ID',200,'No longer used','BANK_ACCOUNT_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','BANK_ACCOUNT_ID','Bank Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','BANK_ACCOUNT_NAME',200,'The bank account name from which payment is made.','BANK_ACCOUNT_NAME','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','BANK_ACCOUNT_NAME','Bank Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CHECK_DATE',200,'Payment date','CHECK_DATE','','','','ANONYMOUS','DATE','AP_CHECKS','CHECK_DATE','Check Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CHECK_ID',200,'Unique internal Identifier for this record. Generated using a database sequence.','CHECK_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','CHECK_ID','Check Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CHECK_NUMBER',200,'Payment number','CHECK_NUMBER','','','','ANONYMOUS','NUMBER','AP_CHECKS','CHECK_NUMBER','Check Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CURRENCY_CODE',200,'Indicates payment currency code in which payment is made.','CURRENCY_CODE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','CURRENCY_CODE','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','LAST_UPDATED_BY',200,'Standard who column - unique internal identifier user who last updated this row. Foreign key to the USER_ID column of the FND_USER table.','LAST_UPDATED_BY','','','','ANONYMOUS','NUMBER','AP_CHECKS','LAST_UPDATED_BY','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','LAST_UPDATE_DATE',200,'Standard Who column - date when a user last updated this row.','LAST_UPDATE_DATE','','','','ANONYMOUS','DATE','AP_CHECKS','LAST_UPDATE_DATE','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','PAYMENT_METHOD_LOOKUP_CODE',200,'Indicates the payment method.','PAYMENT_METHOD_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','PAYMENT_METHOD_LOOKUP_CODE','Payment Method Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','PAYMENT_TYPE_FLAG',200,'This flag indicates the payment type. The possible values are quick payments, manual payments, refunds etc.','PAYMENT_TYPE_FLAG','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','PAYMENT_TYPE_FLAG','Payment Type Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ADDRESS_LINE1',200,'First address line of payment','ADDRESS_LINE1','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ADDRESS_LINE1','Address Line1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ADDRESS_LINE2',200,'Second address line of payment','ADDRESS_LINE2','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ADDRESS_LINE2','Address Line2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ADDRESS_LINE3',200,'Third address line of payment','ADDRESS_LINE3','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ADDRESS_LINE3','Address Line3','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CHECKRUN_NAME',200,'This is the Payment Process Request Name in the payment manager.','CHECKRUN_NAME','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','CHECKRUN_NAME','Checkrun Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CHECK_FORMAT_ID',200,'This corresponds to the Payment Format attached to the payment. The payment format is attached to the payment documents.','CHECK_FORMAT_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','CHECK_FORMAT_ID','Check Format Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CHECK_STOCK_ID',200,'Unique identifier attached to the payment document','CHECK_STOCK_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','CHECK_STOCK_ID','Check Stock Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CITY',200,'City','CITY','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','CITY','City','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','COUNTRY',200,'Country','COUNTRY','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','COUNTRY','Country','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CREATED_BY',200,'Standard who column - unique internal identifier of user who created this row. Foreign key to the USER_ID column of the FND_USER table.','CREATED_BY','','','','ANONYMOUS','NUMBER','AP_CHECKS','CREATED_BY','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CREATION_DATE',200,'Standard who column - date when this row was created.','CREATION_DATE','','','','ANONYMOUS','DATE','AP_CHECKS','CREATION_DATE','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','LAST_UPDATE_LOGIN',200,'Standard who column - operating system login of user who last updated this row. Foreign key to the LOGIN_ID column of the FND_LOGINS table.','LAST_UPDATE_LOGIN','','','','ANONYMOUS','NUMBER','AP_CHECKS','LAST_UPDATE_LOGIN','Last Update Login','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','STATUS_LOOKUP_CODE',200,'Indicates the current status of payment. The possible payment status can be Negotiable, Set up, Spoiled, Cleared etc','STATUS_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','STATUS_LOOKUP_CODE','Status Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','VENDOR_NAME',200,'Supplier name','VENDOR_NAME','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','VENDOR_NAME','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','VENDOR_SITE_CODE',200,'The Supplier site code of payment.','VENDOR_SITE_CODE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','VENDOR_SITE_CODE','Vendor Site Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ZIP',200,'State or province postal code','ZIP','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ZIP','Zip','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','BANK_ACCOUNT_NUM',200,'Supplier''s bank account number for electronic payment purposes','BANK_ACCOUNT_NUM','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','BANK_ACCOUNT_NUM','Bank Account Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','BANK_ACCOUNT_TYPE',200,'Supplier''s bank account type code for electronic payment  purposes','BANK_ACCOUNT_TYPE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','BANK_ACCOUNT_TYPE','Bank Account Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','BANK_NUM',200,'Supplier''s bank number for electronic payment purposes','BANK_NUM','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','BANK_NUM','Bank Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CHECK_VOUCHER_NUM',200,'No Longer Used','CHECK_VOUCHER_NUM','','','','ANONYMOUS','NUMBER','AP_CHECKS','CHECK_VOUCHER_NUM','Check Voucher Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CLEARED_AMOUNT',200,'Amount disbursed by the bank as on the cleared date. This information is also stored in Cash management','CLEARED_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_CHECKS','CLEARED_AMOUNT','Cleared Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CLEARED_DATE',200,'Date when the bank disbursed the payment. This information is also stored in Cash management.','CLEARED_DATE','','','','ANONYMOUS','DATE','AP_CHECKS','CLEARED_DATE','Cleared Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','DOC_CATEGORY_CODE',200,'Sequential Numbering (voucher number) document category for payment','DOC_CATEGORY_CODE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','DOC_CATEGORY_CODE','Doc Category Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','DOC_SEQUENCE_ID',200,'Sequential Numbering document sequence identifier','DOC_SEQUENCE_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','DOC_SEQUENCE_ID','Doc Sequence Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','DOC_SEQUENCE_VALUE',200,'Voucher number (sequential numbering) for payment','DOC_SEQUENCE_VALUE','','~T~D~2','','ANONYMOUS','NUMBER','AP_CHECKS','DOC_SEQUENCE_VALUE','Doc Sequence Value','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','PROVINCE',200,'No longer used','PROVINCE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','PROVINCE','Province','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','RELEASED_AT',200,'No longer used','RELEASED_AT','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','RELEASED_AT','Released At','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','RELEASED_BY',200,'User that released stop payment','RELEASED_BY','','','','ANONYMOUS','NUMBER','AP_CHECKS','RELEASED_BY','Released By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','STATE',200,'State','STATE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','STATE','State','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','STOPPED_AT',200,'No longer used','STOPPED_AT','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','STOPPED_AT','Stopped At','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','STOPPED_BY',200,'User that recorded stop payment','STOPPED_BY','','','','ANONYMOUS','NUMBER','AP_CHECKS','STOPPED_BY','Stopped By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','VOID_DATE',200,'Payment void date','VOID_DATE','','','','ANONYMOUS','DATE','AP_CHECKS','VOID_DATE','Void Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ATTRIBUTE1',200,'Descriptive flexfield segment','ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ATTRIBUTE1','Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ATTRIBUTE10',200,'Descriptive flexfield segment','ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ATTRIBUTE10','Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ATTRIBUTE11',200,'Descriptive flexfield segment','ATTRIBUTE11','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ATTRIBUTE11','Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ATTRIBUTE12',200,'Descriptive flexfield segment','ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ATTRIBUTE12','Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ATTRIBUTE13',200,'Descriptive flexfield segment','ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ATTRIBUTE13','Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ATTRIBUTE14',200,'Descriptive flexfield segment','ATTRIBUTE14','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ATTRIBUTE14','Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ATTRIBUTE15',200,'Descriptive flexfield segment','ATTRIBUTE15','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ATTRIBUTE15','Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ATTRIBUTE2',200,'Descriptive flexfield segment','ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ATTRIBUTE2','Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ATTRIBUTE3',200,'Descriptive flexfield segment','ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ATTRIBUTE3','Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ATTRIBUTE5',200,'Descriptive flexfield segment','ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ATTRIBUTE5','Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ATTRIBUTE6',200,'Descriptive flexfield segment','ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ATTRIBUTE6','Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ATTRIBUTE7',200,'Descriptive flexfield segment','ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ATTRIBUTE7','Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ATTRIBUTE8',200,'Descriptive flexfield segment','ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ATTRIBUTE8','Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ATTRIBUTE9',200,'Descriptive flexfield segment','ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ATTRIBUTE9','Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ATTRIBUTE_CATEGORY',200,'Descriptive flexfield structure definition column.','ATTRIBUTE_CATEGORY','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ATTRIBUTE_CATEGORY','Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','FUTURE_PAY_DUE_DATE',200,'Negotiable date for future dated payment','FUTURE_PAY_DUE_DATE','','','','ANONYMOUS','DATE','AP_CHECKS','FUTURE_PAY_DUE_DATE','Future Pay Due Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','TREASURY_PAY_DATE',200,'No Longer Used','TREASURY_PAY_DATE','','','','ANONYMOUS','DATE','AP_CHECKS','TREASURY_PAY_DATE','Treasury Pay Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','TREASURY_PAY_NUMBER',200,'No Longer Used','TREASURY_PAY_NUMBER','','','','ANONYMOUS','NUMBER','AP_CHECKS','TREASURY_PAY_NUMBER','Treasury Pay Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','USSGL_TRANSACTION_CODE',200,'Transaction code for creating US Standard General Ledger journal entries','USSGL_TRANSACTION_CODE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','USSGL_TRANSACTION_CODE','Ussgl Transaction Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','USSGL_TRX_CODE_CONTEXT',200,'USSGL Transaction Code Descriptive Flexfield context column','USSGL_TRX_CODE_CONTEXT','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','USSGL_TRX_CODE_CONTEXT','Ussgl Trx Code Context','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','WITHHOLDING_STATUS_LOOKUP_CODE',200,'No longer used','WITHHOLDING_STATUS_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','WITHHOLDING_STATUS_LOOKUP_CODE','Withholding Status Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','RECONCILIATION_BATCH_ID',200,'No Longer Used','RECONCILIATION_BATCH_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','RECONCILIATION_BATCH_ID','Reconciliation Batch Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CLEARED_BASE_AMOUNT',200,'Payment cleared amount in functional currency','CLEARED_BASE_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_CHECKS','CLEARED_BASE_AMOUNT','Cleared Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CLEARED_EXCHANGE_RATE',200,'This is the exchange rate at which the payment is cleared. This is used only for foreign currency payments.','CLEARED_EXCHANGE_RATE','','~T~D~2','','ANONYMOUS','NUMBER','AP_CHECKS','CLEARED_EXCHANGE_RATE','Cleared Exchange Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CLEARED_EXCHANGE_DATE',200,'This is the date used for calculating the exchange rate for the payments disbursed. This is used only for foreign currency payments.','CLEARED_EXCHANGE_DATE','','','','ANONYMOUS','DATE','AP_CHECKS','CLEARED_EXCHANGE_DATE','Cleared Exchange Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CLEARED_EXCHANGE_RATE_TYPE',200,'This is the exchange rate type like Spot, Corporate etc taken for payment clearing. This is used only for foreign currency payments','CLEARED_EXCHANGE_RATE_TYPE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','CLEARED_EXCHANGE_RATE_TYPE','Cleared Exchange Rate Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ADDRESS_LINE4',200,'Fourth address line of payment.  Used for flexible address formatting','ADDRESS_LINE4','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ADDRESS_LINE4','Address Line4','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','COUNTY',200,'Used for flexible address formatting.  Also used for matching AP and AR addresses','COUNTY','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','COUNTY','County','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ADDRESS_STYLE',200,'Used as context value in FAF descr flexs.  Do not populate.  Instead, join to FND_TERRITORIES where territory_code=country','ADDRESS_STYLE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','ADDRESS_STYLE','Address Style','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ORG_ID',200,'Unique internal identifier of the Operating Unit to which the payment belongs','ORG_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','ORG_ID','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','VENDOR_ID',200,'This is the unique identifier of supplier. In Cash management, these fields are used to Find Transactions by Supplier so that these transactions can be cleared.','VENDOR_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','VENDOR_ID','Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','VENDOR_SITE_ID',200,'This is the unique identifier of supplier site. In Cash management, these fields are used to Find Transactions by Supplier so that these transactions can be cleared','VENDOR_SITE_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','VENDOR_SITE_ID','Vendor Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','EXCHANGE_RATE',200,'Exchange rate for foreign currency payment','EXCHANGE_RATE','','~T~D~2','','ANONYMOUS','NUMBER','AP_CHECKS','EXCHANGE_RATE','Exchange Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','EXCHANGE_DATE',200,'This is the date on which the exchange rate is effective. Applicable for foreign currency payments.','EXCHANGE_DATE','','','','ANONYMOUS','DATE','AP_CHECKS','EXCHANGE_DATE','Exchange Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','BASE_AMOUNT',200,'This is the Payment amount in functional currency. This is derived using the payment amount and the exchange rate','BASE_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_CHECKS','BASE_AMOUNT','Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CHECKRUN_ID',200,'This is the unique identifier assigned to the payment process request.','CHECKRUN_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','CHECKRUN_ID','Checkrun Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','REQUEST_ID',200,'Concurrent Program who column - concurrent request id of the program that last updated this row. Foreign key to FND_CONCURRENT_REQUESTS.REQUEST_ID.','REQUEST_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','REQUEST_ID','Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CLEARED_ERROR_AMOUNT',200,'This is the payment error amount calculated on clearing the payments. This is calculated in cash management.','CLEARED_ERROR_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_CHECKS','CLEARED_ERROR_AMOUNT','Cleared Error Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CLEARED_CHARGES_AMOUNT',200,'This is the bank charges amount calculated on clearing the payments. This is calculated in cash management.','CLEARED_CHARGES_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_CHECKS','CLEARED_CHARGES_AMOUNT','Cleared Charges Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CLEARED_ERROR_BASE_AMOUNT',200,'This is the payment error amount expressed in functional currency','CLEARED_ERROR_BASE_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_CHECKS','CLEARED_ERROR_BASE_AMOUNT','Cleared Error Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','CLEARED_CHARGES_BASE_AMOUNT',200,'This is the payment charges amount expressed in functional currency','CLEARED_CHARGES_BASE_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_CHECKS','CLEARED_CHARGES_BASE_AMOUNT','Cleared Charges Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','POSITIVE_PAY_STATUS_CODE',200,'This is the status set by and used by Positive Pay Report to select records.','POSITIVE_PAY_STATUS_CODE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','POSITIVE_PAY_STATUS_CODE','Positive Pay Status Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE_CATEGORY',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE_CATEGORY','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE_CATEGORY','Global Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE1',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE1','Global Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE2',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE2','Global Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE3',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE3','Global Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE4',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE4','Global Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE5',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE5','Global Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE6',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE6','Global Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE7',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE7','Global Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE8',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE8','Global Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE9',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE9','Global Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE10',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE10','Global Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE11',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE11','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE11','Global Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE12',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE12','Global Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE14',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE14','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE14','Global Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE15',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE15','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE15','Global Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE16',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE16','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE16','Global Attribute16','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE17',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE17','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE17','Global Attribute17','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE18',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE18','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE18','Global Attribute18','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE19',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE19','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE19','Global Attribute19','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','GLOBAL_ATTRIBUTE20',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE20','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','GLOBAL_ATTRIBUTE20','Global Attribute20','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','TRANSFER_PRIORITY',200,'This indicates the priority for transfer of electronic payments. Applicable for bank charge users only. Values are Any, Express or Normal.','TRANSFER_PRIORITY','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','TRANSFER_PRIORITY','Transfer Priority','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','EXTERNAL_BANK_ACCOUNT_ID',200,'Unique internal identifier for the supplier bank account associated with payment. Foreign key to the IBY_EXT_BANK_ACCOUNTS table','EXTERNAL_BANK_ACCOUNT_ID','','','','ANONYMOUS','NUMBER','AP_CHECKS','EXTERNAL_BANK_ACCOUNT_ID','External Bank Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','STAMP_DUTY_AMT',200,'Stamp duty tax amount for globalization','STAMP_DUTY_AMT','','~T~D~2','','ANONYMOUS','NUMBER','AP_CHECKS','STAMP_DUTY_AMT','Stamp Duty Amt','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','STAMP_DUTY_BASE_AMT',200,'Stamp duty tax amount in functional currency for globalization','STAMP_DUTY_BASE_AMT','','~T~D~2','','ANONYMOUS','NUMBER','AP_CHECKS','STAMP_DUTY_BASE_AMT','Stamp Duty Base Amt','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','MRC_CLEARED_BASE_AMOUNT',200,'No Longer Used','MRC_CLEARED_BASE_AMOUNT','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','MRC_CLEARED_BASE_AMOUNT','Mrc Cleared Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','MRC_CLEARED_EXCHANGE_RATE',200,'No Longer Used','MRC_CLEARED_EXCHANGE_RATE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','MRC_CLEARED_EXCHANGE_RATE','Mrc Cleared Exchange Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','MRC_CLEARED_EXCHANGE_DATE',200,'No Longer Used','MRC_CLEARED_EXCHANGE_DATE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','MRC_CLEARED_EXCHANGE_DATE','Mrc Cleared Exchange Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','MRC_CLEARED_EXCHANGE_RATE_TYPE',200,'No Longer Used','MRC_CLEARED_EXCHANGE_RATE_TYPE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','MRC_CLEARED_EXCHANGE_RATE_TYPE','Mrc Cleared Exchange Rate Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','MRC_EXCHANGE_RATE',200,'No Longer Used','MRC_EXCHANGE_RATE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','MRC_EXCHANGE_RATE','Mrc Exchange Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','MRC_EXCHANGE_DATE',200,'No Longer Used','MRC_EXCHANGE_DATE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','MRC_EXCHANGE_DATE','Mrc Exchange Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','MRC_EXCHANGE_RATE_TYPE',200,'No Longer Used','MRC_EXCHANGE_RATE_TYPE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','MRC_EXCHANGE_RATE_TYPE','Mrc Exchange Rate Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','MRC_BASE_AMOUNT',200,'No Longer Used','MRC_BASE_AMOUNT','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','MRC_BASE_AMOUNT','Mrc Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','MRC_CLEARED_ERROR_BASE_AMOUNT',200,'No Longer Used','MRC_CLEARED_ERROR_BASE_AMOUNT','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','MRC_CLEARED_ERROR_BASE_AMOUNT','Mrc Cleared Error Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','MRC_CLEARED_CHARGES_BASE_AMT',200,'No Longer Used','MRC_CLEARED_CHARGES_BASE_AMT','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','MRC_CLEARED_CHARGES_BASE_AMT','Mrc Cleared Charges Base Amt','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','MRC_STAMP_DUTY_BASE_AMT',200,'No Longer Used','MRC_STAMP_DUTY_BASE_AMT','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','MRC_STAMP_DUTY_BASE_AMT','Mrc Stamp Duty Base Amt','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','MATURITY_EXCHANGE_DATE',200,'This is the date used for identifying exchange rate on matured payments. This is usually the maturity date for the payment.','MATURITY_EXCHANGE_DATE','','','','ANONYMOUS','DATE','AP_CHECKS','MATURITY_EXCHANGE_DATE','Maturity Exchange Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','MATURITY_EXCHANGE_RATE_TYPE',200,'This is the exchange rate type at future dated payment maturity time. This is applicable for foreign currency payments only.','MATURITY_EXCHANGE_RATE_TYPE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','MATURITY_EXCHANGE_RATE_TYPE','Maturity Exchange Rate Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','MATURITY_EXCHANGE_RATE',200,'This is the exchange rate used at future dated payment maturity time. This is applicable for foreign currency payments only','MATURITY_EXCHANGE_RATE','','~T~D~2','','ANONYMOUS','NUMBER','AP_CHECKS','MATURITY_EXCHANGE_RATE','Maturity Exchange Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','DESCRIPTION',200,'This is the payment description.','DESCRIPTION','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','DESCRIPTION','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ACTUAL_VALUE_DATE',200,'Actual Value Date.  For use by Cash Management','ACTUAL_VALUE_DATE','','','','ANONYMOUS','DATE','AP_CHECKS','ACTUAL_VALUE_DATE','Actual Value Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','ANTICIPATED_VALUE_DATE',200,'Anticipated Value Date.  For use by Cash Management','ANTICIPATED_VALUE_DATE','','','','ANONYMOUS','DATE','AP_CHECKS','ANTICIPATED_VALUE_DATE','Anticipated Value Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','RELEASED_DATE',200,'Date and time user released stop payment','RELEASED_DATE','','','','ANONYMOUS','DATE','AP_CHECKS','RELEASED_DATE','Released Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','STOPPED_DATE',200,'Date and time user recorded stop payment','STOPPED_DATE','','','','ANONYMOUS','DATE','AP_CHECKS','STOPPED_DATE','Stopped Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','MRC_MATURITY_EXG_DATE',200,'No Longer Used','MRC_MATURITY_EXG_DATE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','MRC_MATURITY_EXG_DATE','Mrc Maturity Exg Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','MRC_MATURITY_EXG_RATE',200,'No Longer Used','MRC_MATURITY_EXG_RATE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','MRC_MATURITY_EXG_RATE','Mrc Maturity Exg Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CHECKS','MRC_MATURITY_EXG_RATE_TYPE',200,'No Longer Used','MRC_MATURITY_EXG_RATE_TYPE','','','','ANONYMOUS','VARCHAR2','AP_CHECKS','MRC_MATURITY_EXG_RATE_TYPE','Mrc Maturity Exg Rate Type','','','','US');
--Inserting Object Components for AP_CHECKS
--Inserting Object Component Joins for AP_CHECKS
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report LOV Data for WC - Payment Register Summary
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - WC - Payment Register Summary
xxeis.eis_rsc_ins.lov( 200,'SELECT displayed_field
FROM AP_LOOKUP_CODES
where lookup_type = ''PAYMENT TYPE''','','AP Payment Type','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 200,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report Data for WC - Payment Register Summary
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC - Payment Register Summary
xxeis.eis_rsc_utility.delete_report_rows( 'WC - Payment Register Summary' );
--Inserting Report - WC - Payment Register Summary
xxeis.eis_rsc_ins.r( 200,'WC - Payment Register Summary','','This report provides payment register details. It lists bank name, branch name, bank account, bank currency, supplier name, supplier site, payment amount, cleared amount etc., Additional parameters are effective start date, effective end date and payment type.','','','','10012196','EIS_AP_PAY_REGIS_V','Y','','','10012196','','N','Payments','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - WC - Payment Register Summary
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'STATE','State','State','','','','','22','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'ZIP','Zip','State or province postal code','','','','','23','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'COUNTRY','Country','The short name that stands for the territory','','','','','24','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'PAYMENT_TYPE','Payment Type','QuickCode meaning','','','','','7','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'BANK','Bank','Bank name','','','','','3','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'BRANCH','Branch','Bank branch name','','','','','4','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'ACCOUNT','Account','Bank account name','','','','','5','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'CLEARED_DATE','Cleared Date','Payment cleared date','','','','','16','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'CLEARED_AMOUNT','Cleared Amount','Payment cleared amount','','','','','15','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'ADDRESS_LINE1','Address Line1','First address line of payment','','','','','20','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'CITY','City','City','','','','','21','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'BANK_ACCOUNT_CURRENCY_CODE','Bank Account Currency Code','Currency Code','','','','','6','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'BANK_ACCOUNT_TYPE','Bank Account Type','Bank account type code','','','','','25','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'COMPANY_NAME','Company Name','Company Name','','','','','1','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'OPERATING_UNIT','Operating Unit','Translated name of the organization','','','','','2','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'PAYMENT_AMOUNT','Payment Amount','Payment amount','','','','','14','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'PAYMENT_CURRENCY_CODE','Payment Currency Code','Currency code','','','','','11','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'PAYMENT_DATE','Payment Date','Payment date','','','','','10','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'PAYMENT_NUMBER','Payment Number','Payment number','','','','','9','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'SUPPLIER_NAME','Supplier Name','Supplier name','','','','','12','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'SUPPLIER_SITE','Supplier Site','Supplier site code','','','','','13','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'STATUS','Status','QuickCode meaning','','','','','18','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'ACCOUNT_CLASSIFICATION','Account Classification','Account Classification (INTERNAL, EXTERNAL)','','','','','19','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'PAYMENT_DOCUMENT','Payment Document','Payment Document Name','','','','','8','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'ADDRESS_LINE2','Address Line2','Second address line of payment','','','','','26','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'ADDRESS_LINE3','Address Line3','Third address line of payment','','','','','27','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'VENDOR_NAME','Vendor Name','Supplier name','','','','','28','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','AP_CHECKS','CH','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'PAYMENT_INSTRUCTION_ID','Payment Instruction Id','This is the unique identifier assigned to the payment instructions in Oracle Payments.','','','','','29','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','AP_CHECKS','CH','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'VENDOR_ID','Vendor Id','This is the unique identifier of supplier. In Cash management, these fields are used to Find Transactions by Supplier so that these transactions can be cleared.','','','','','30','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','AP_CHECKS','CH','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'VOID_DATE','Void Date','Payment void date','','','','','17','N','','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','AP_CHECKS','CH','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - Payment Register Summary',200,'CHECKRUN_NAME','Checkrun Name','This is the Payment Process Request Name in the payment manager.','','','','','31','','Y','','','','','','','10012196','N','N','','EIS_AP_PAY_REGIS_V','AP_CHECKS','CH','GROUP_BY','US','');
--Inserting Report Parameters - WC - Payment Register Summary
xxeis.eis_rsc_ins.rp( 'WC - Payment Register Summary',200,'Effective Start Date','','PAYMENT_DATE','>=','','','DATE','N','Y','2','','Y','CONSTANT','10012196','Y','N','','','','EIS_AP_PAY_REGIS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - Payment Register Summary',200,'Effective End Date','','PAYMENT_DATE','<=','','','DATE','N','Y','3','','Y','CONSTANT','10012196','Y','N','','','','EIS_AP_PAY_REGIS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - Payment Register Summary',200,'Payment Type','','PAYMENT_TYPE','IN','AP Payment Type','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','10012196','Y','N','','','','EIS_AP_PAY_REGIS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - Payment Register Summary',200,'Operating Unit','','OPERATING_UNIT','IN','EIS_MULTI_OPERATING_UNIT_LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','10012196','Y','N','','','','EIS_AP_PAY_REGIS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - Payment Register Summary',200,'Bank','Bank name','BANK','IN','','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','10012196','Y','N','','','','EIS_AP_PAY_REGIS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - Payment Register Summary',200,'Cleared Date From','Payment cleared date','CLEARED_DATE','>=','','','DATE','N','Y','6','','Y','CONSTANT','10012196','Y','N','','','','EIS_AP_PAY_REGIS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - Payment Register Summary',200,'Cleared Date To','Payment cleared date','CLEARED_DATE','<=','','','DATE','N','Y','7','','Y','CONSTANT','10012196','Y','N','','','','EIS_AP_PAY_REGIS_V','','','US','');
--Inserting Dependent Parameters - WC - Payment Register Summary
--Inserting Report Conditions - WC - Payment Register Summary
xxeis.eis_rsc_ins.rcnh( 'WC - Payment Register Summary',200,'BANK IN :Bank ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BANK','','Bank','','','','','EIS_AP_PAY_REGIS_V','','','','','','IN','Y','Y','','','','','1',200,'WC - Payment Register Summary','BANK IN :Bank ');
xxeis.eis_rsc_ins.rcnh( 'WC - Payment Register Summary',200,'CLEARED_DATE >= :Cleared Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CLEARED_DATE','','Cleared Date From','','','','','EIS_AP_PAY_REGIS_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',200,'WC - Payment Register Summary','CLEARED_DATE >= :Cleared Date From ');
xxeis.eis_rsc_ins.rcnh( 'WC - Payment Register Summary',200,'CLEARED_DATE <= :Cleared Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CLEARED_DATE','','Cleared Date To','','','','','EIS_AP_PAY_REGIS_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',200,'WC - Payment Register Summary','CLEARED_DATE <= :Cleared Date To ');
xxeis.eis_rsc_ins.rcnh( 'WC - Payment Register Summary',200,'OPERATING_UNIT IN :Operating Unit ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','OPERATING_UNIT','','Operating Unit','','','','','EIS_AP_PAY_REGIS_V','','','','','','IN','Y','Y','','','','','1',200,'WC - Payment Register Summary','OPERATING_UNIT IN :Operating Unit ');
xxeis.eis_rsc_ins.rcnh( 'WC - Payment Register Summary',200,'PAYMENT_DATE <= :Effective End Date ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PAYMENT_DATE','','Effective End Date','','','','','EIS_AP_PAY_REGIS_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',200,'WC - Payment Register Summary','PAYMENT_DATE <= :Effective End Date ');
xxeis.eis_rsc_ins.rcnh( 'WC - Payment Register Summary',200,'PAYMENT_DATE >= :Effective Start Date ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PAYMENT_DATE','','Effective Start Date','','','','','EIS_AP_PAY_REGIS_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',200,'WC - Payment Register Summary','PAYMENT_DATE >= :Effective Start Date ');
xxeis.eis_rsc_ins.rcnh( 'WC - Payment Register Summary',200,'PAYMENT_TYPE IN :Payment Type ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PAYMENT_TYPE','','Payment Type','','','','','EIS_AP_PAY_REGIS_V','','','','','','IN','Y','Y','','','','','1',200,'WC - Payment Register Summary','PAYMENT_TYPE IN :Payment Type ');
--Inserting Report Sorts - WC - Payment Register Summary
xxeis.eis_rsc_ins.rs( 'WC - Payment Register Summary',200,'BANK','ASC','10012196','1','');
xxeis.eis_rsc_ins.rs( 'WC - Payment Register Summary',200,'BRANCH','ASC','10012196','2','');
xxeis.eis_rsc_ins.rs( 'WC - Payment Register Summary',200,'ACCOUNT','ASC','10012196','3','');
--Inserting Report Triggers - WC - Payment Register Summary
--inserting report templates - WC - Payment Register Summary
xxeis.eis_rsc_ins.r_tem( 'WC - Payment Register Summary','WC - Payment Register Summary','Seeded template for WC - Payment Register Summary','','','','','','','','','','','WC - Payment Register Summary.rtf','10012196','X','','','Y','Y','','');
--Inserting Report Portals - WC - Payment Register Summary
--inserting report dashboards - WC - Payment Register Summary
xxeis.eis_rsc_ins.R_dash( 'WC - Payment Register Summary','Payment Register Report','Payment Register Report','pie','large','Account','Account','','Amount','Sum','10012196');
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC - Payment Register Summary','200','EIS_AP_PAY_REGIS_V','EIS_AP_PAY_REGIS_V','N','');
xxeis.eis_rsc_ins.rviews( 'WC - Payment Register Summary','200','EIS_AP_PAY_REGIS_V','AP_CHECKS','N','CH');
--inserting report security - WC - Payment Register Summary
xxeis.eis_rsc_ins.rsec( 'WC - Payment Register Summary','','EH011559','',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Payment Register Summary','','LG045241','',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Payment Register Summary','','BG000020','',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Payment Register Summary','','NR054247','',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Payment Register Summary','','KM026443','',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Payment Register Summary','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Payment Register Summary','200','','XXWC_PAY_VENDOR_MSTR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Payment Register Summary','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Payment Register Summary','200','','XXWC_PAY_MANAGER',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Payment Register Summary','200','','XXWC_PAYABLES_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Payment Register Summary','200','','XXWC_PAY_DISBURSE',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Payment Register Summary','200','','XXWC_PAY_W_CALENDAR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Payment Register Summary','200','','XXWC_PAY_NO_CALENDAR',200,'10012196','','','');
--Inserting Report Pivots - WC - Payment Register Summary
xxeis.eis_rsc_ins.rpivot( 'WC - Payment Register Summary',200,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Payment Register Summary',200,'Pivot','BANK','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Payment Register Summary',200,'Pivot','BRANCH','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Payment Register Summary',200,'Pivot','ACCOUNT','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Payment Register Summary',200,'Pivot','CLEARED_DATE','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Payment Register Summary',200,'Pivot','CLEARED_AMOUNT','DATA_FIELD','SUM','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Payment Register Summary',200,'Pivot','BANK_ACCOUNT_CURRENCY_CODE','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Payment Register Summary',200,'Pivot','COMPANY_NAME','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Payment Register Summary',200,'Pivot','OPERATING_UNIT','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Payment Register Summary',200,'Pivot','PAYMENT_AMOUNT','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Payment Register Summary',200,'Pivot','PAYMENT_CURRENCY_CODE','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Payment Register Summary',200,'Pivot','PAYMENT_DATE','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Payment Register Summary',200,'Pivot','SUPPLIER_NAME','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Payment Register Summary',200,'Pivot','SUPPLIER_SITE','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC - Payment Register Summary',200,'Pivot','STATUS','PAGE_FIELD','','','','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- WC - Payment Register Summary
xxeis.eis_rsc_ins.rv( 'WC - Payment Register Summary','','WC - Payment Register Summary','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
