--Report Name            : WC Sales Journal By Customer - GL Range
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_AR_SALES_JOUR_BY_CUST_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_AR_SALES_JOUR_BY_CUST_V
xxeis.eis_rsc_ins.v( 'EIS_AR_SALES_JOUR_BY_CUST_V',222,'This view shows details of sales journal transaction lines including customer names.','','','','XXEIS_RS_ADMIN','XXEIS','EIS AR Sales Journal By Customers','EASJBCV','','','VIEW','US','Y','');
--Delete Object Columns for EIS_AR_SALES_JOUR_BY_CUST_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_AR_SALES_JOUR_BY_CUST_V',222,FALSE);
--Inserting Object Columns for EIS_AR_SALES_JOUR_BY_CUST_V
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','GL_ACCOUNT',222,'Gl Account','GL_ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','DERIVED COLUMN','DERIVED COLUMN','Gl Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','GL_DATE',222,'General Ledger date','GL_DATE','','','','XXEIS_RS_ADMIN','DATE','RA_CUST_TRX_LINE_GL_DIST_ALL','GL_DATE','Gl Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','CREDIT_AMOUNT',222,'Credit Amount','CREDIT_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Credit Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','DEBIT_AMOUNT',222,'Debit Amount','DEBIT_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Debit Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','GL_POSTED_DATE',222,'Gl Posted Date','GL_POSTED_DATE~GL_POSTED_DATE','','','','XXEIS_RS_ADMIN','DATE','RA_CUST_TRX_LINE_GL_DIST_ALL','GL_POSTED_DATE','Gl Posted Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','LINE_NUMBER',222,'Line Number','LINE_NUMBER~LINE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','DERIVED COLUMN','DERIVED COLUMN','Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','CUSTOMER_ACCOUNT_NAME',222,'Customer Account Name','CUSTOMER_ACCOUNT_NAME~CUSTOMER_ACCOUNT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ACCOUNT_NAME','Customer Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER~CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ACCOUNT_NUMBER','Customer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','DIST_ACCOUNT_CLASS',222,'Freight, Receivable, Revenue, AutoInvoice Clearing, Tax, Unbilled Receivable, Unearned Revenue, or Charges account type','DIST_ACCOUNT_CLASS~DIST_ACCOUNT_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUST_TRX_LINE_GL_DIST_ALL','ACCOUNT_CLASS','Dist Account Class','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','GL_TRANSFER_STATUS',222,'Gl Transfer Status','GL_TRANSFER_STATUS~GL_TRANSFER_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','DERIVED COLUMN','DERIVED COLUMN','Gl Transfer Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','LINK_LINE_DESCRIPTION',222,'Link Line Description','LINK_LINE_DESCRIPTION~LINK_LINE_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_LINES_ALL','DESCRIPTION','Link Line Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','LINK_LINE_NUMBER',222,'Link Line Number','LINK_LINE_NUMBER~LINK_LINE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_LINES_ALL','LINE_NUMBER','Link Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','LINK_LINE_TYPE',222,'Link Line Type','LINK_LINE_TYPE~LINK_LINE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_LINES_ALL','LINE_TYPE','Link Line Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','ORGANIZATION_ID',222,'Organization Id','ORGANIZATION_ID~ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','HR_ALL_ORGANIZATION_UNITS','NAME','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','PARTY_NAME',222,'Party Name','PARTY_NAME~PARTY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','PARTY_NAME','Party Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','PARTY_NUMBER',222,'Party Number','PARTY_NUMBER~PARTY_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','PARTY_NUMBER','Party Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','TRX_COMMENTS',222,'Stores comments entered in the Transactions workbench. Value can be printed on an invoice using the Print Invoice view.','TRX_COMMENTS~TRX_COMMENTS','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','COMMENTS','Trx Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','TRX_CURRENCY_CODE',222,'Trx Currency Code','TRX_CURRENCY_CODE~TRX_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','INVOICE_CURRENCY_CODE','Trx Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','TRX_DATE',222,'Trx Date','TRX_DATE~TRX_DATE','','','','XXEIS_RS_ADMIN','DATE','RA_CUSTOMER_TRX_ALL','TRX_DATE','Trx Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','TRX_LINE_AMOUNT',222,'Trx Line Amount','TRX_LINE_AMOUNT~TRX_LINE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_LINES_ALL','EXTENDED_AMOUNT','Trx Line Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','TRX_LINE_DESCRIPTION',222,'Trx Line Description','TRX_LINE_DESCRIPTION~TRX_LINE_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_LINES_ALL','DESCRIPTION','Trx Line Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','TRX_LINE_QUANTITY_INVOICED',222,'Trx Line Quantity Invoiced','TRX_LINE_QUANTITY_INVOICED~TRX_LINE_QUANTITY_INVOICED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_LINES_ALL','QUANTITY_INVOICED','Trx Line Quantity Invoiced','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','TRX_LINE_QUANTITY_ORDERED',222,'Trx Line Quantity Ordered','TRX_LINE_QUANTITY_ORDERED~TRX_LINE_QUANTITY_ORDERED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_LINES_ALL','QUANTITY_ORDERED','Trx Line Quantity Ordered','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','TRX_LINE_REVENUE_AMOUNT',222,'Trx Line Revenue Amount','TRX_LINE_REVENUE_AMOUNT~TRX_LINE_REVENUE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_LINES_ALL','REVENUE_AMOUNT','Trx Line Revenue Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','TRX_LINE_STANDARD_PRICE',222,'Trx Line Standard Price','TRX_LINE_STANDARD_PRICE~TRX_LINE_STANDARD_PRICE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_LINES_ALL','UNIT_STANDARD_PRICE','Trx Line Standard Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','TRX_LINE_TYPE',222,'Trx Line Type','TRX_LINE_TYPE~TRX_LINE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_LINES_ALL','LINE_TYPE','Trx Line Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','TRX_NUMBER',222,'Transaction number','TRX_NUMBER~TRX_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUSTOMER_TRX_ALL','TRX_NUMBER','Trx Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','TRX_TYPE_CLASS',222,'Trx Type Class','TRX_TYPE_CLASS~TRX_TYPE_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUST_TRX_TYPES_ALL','TYPE','Trx Type Class','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','TRX_TYPE_NAME',222,'Trx Type Name','TRX_TYPE_NAME~TRX_TYPE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','RA_CUST_TRX_TYPES_ALL','NAME','Trx Type Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','LEDGER_ID',222,'Ledger Id','LEDGER_ID~LEDGER_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_LEDGERS','LEDGER_ID','Ledger Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','OPERATING_UNIT',222,'Operating Unit','OPERATING_UNIT~OPERATING_UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','HR_ALL_ORGANIZATION_UNITS_TL','NAME','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','CODE_COMBINATION_ID',222,'Code Combination Id','CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_CODE_COMBINATIONS','CODE_COMBINATION_ID','Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','CUSTOMER_TRX_LINE_ID',222,'Customer Trx Line Id','CUSTOMER_TRX_LINE_ID~CUSTOMER_TRX_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_LINES_ALL','CUSTOMER_TRX_LINE_ID','Customer Trx Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','CUST_ACCOUNT_ID',222,'Customer account identifier','CUST_ACCOUNT_ID~CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_CUST_ACCOUNTS','CUST_ACCOUNT_ID','Cust Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','CUST_TRX_LINE_GL_DIST_ID',222,'Revenue distribution identifier','CUST_TRX_LINE_GL_DIST_ID~CUST_TRX_LINE_GL_DIST_ID','','','','XXEIS_RS_ADMIN','NUMBER','RA_CUST_TRX_LINE_GL_DIST_ALL','CUST_TRX_LINE_GL_DIST_ID','Cust Trx Line Gl Dist Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','CUST_TRX_TYPE_ID',222,'Transaction type identifier','CUST_TRX_TYPE_ID~CUST_TRX_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','RA_CUST_TRX_TYPES_ALL','CUST_TRX_TYPE_ID','Cust Trx Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','LINK_CUSTOMER_TRX_LINE_ID',222,'Invoice line identifier','LINK_CUSTOMER_TRX_LINE_ID~LINK_CUSTOMER_TRX_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_LINES_ALL','CUSTOMER_TRX_LINE_ID','Link Customer Trx Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','LINK_LINE_AMOUNT',222,'Link Line Amount','LINK_LINE_AMOUNT~LINK_LINE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_LINES_ALL','EXTENDED_AMOUNT','Link Line Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','LINK_LINE_QUANTITY_INVOICED',222,'Link Line Quantity Invoiced','LINK_LINE_QUANTITY_INVOICED~LINK_LINE_QUANTITY_INVOICED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_LINES_ALL','QUANTITY_INVOICED','Link Line Quantity Invoiced','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','LINK_LINE_QUANTITY_ORDERED',222,'Link Line Quantity Ordered','LINK_LINE_QUANTITY_ORDERED~LINK_LINE_QUANTITY_ORDERED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_LINES_ALL','QUANTITY_ORDERED','Link Line Quantity Ordered','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','LINK_LINE_REVENUE_AMOUNT',222,'Link Line Revenue Amount','LINK_LINE_REVENUE_AMOUNT~LINK_LINE_REVENUE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_LINES_ALL','REVENUE_AMOUNT','Link Line Revenue Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','LINK_LINE_STANDARD_PRICE',222,'Link Line Standard Price','LINK_LINE_STANDARD_PRICE~LINK_LINE_STANDARD_PRICE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_LINES_ALL','UNIT_STANDARD_PRICE','Link Line Standard Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','PARTY_ID',222,'Party Id','PARTY_ID~PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_PARTIES','PARTY_ID','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','SET_OF_BOOKS_CURRENCY',222,'Set Of Books Currency','SET_OF_BOOKS_CURRENCY~SET_OF_BOOKS_CURRENCY','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_SETS_OF_BOOKS','CURRENCY_CODE','Set Of Books Currency','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','SET_OF_BOOKS_NAME',222,'Set Of Books Name','SET_OF_BOOKS_NAME~SET_OF_BOOKS_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_SETS_OF_BOOKS','NAME','Set Of Books Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','TRX_LINE_NUMBER',222,'Trx Line Number','TRX_LINE_NUMBER~TRX_LINE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_LINES_ALL','LINE_NUMBER','Trx Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','CUSTOMER_TRX_ID',222,'Customer Trx Id','CUSTOMER_TRX_ID~CUSTOMER_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','RA_CUSTOMER_TRX_ALL','CUSTOMER_TRX_ID','Customer Trx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','ORG_ID',222,'Org Id','ORG_ID~ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','HR_ALL_ORGANIZATION_UNITS','ORGANIZATION_ID','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','TYPES_ORG_ID',222,'Types Org Id','TYPES_ORG_ID~TYPES_ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','RA_CUST_TRX_TYPES_ALL','TYPES_ORG_ID','Types Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','ACCOUNTED_CREDIT_AMOUNT',222,'Accounted Credit Amount','ACCOUNTED_CREDIT_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Accounted Credit Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_SALES_JOUR_BY_CUST_V','ACCOUNTED_DEBIT_AMOUNT',222,'Accounted Debit Amount','ACCOUNTED_DEBIT_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Accounted Debit Amount','','','','US');

--Inserting Object Components for EIS_AR_SALES_JOUR_BY_CUST_V
xxeis.eis_rsc_ins.vcomp( 'EIS_AR_SALES_JOUR_BY_CUST_V','HZ_PARTIES',222,'HZ_PARTIES','PARTY','PARTY','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Parties','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AR_SALES_JOUR_BY_CUST_V','GL_CODE_COMBINATIONS_KFV',222,'GL_CODE_COMBINATIONS','GL','GL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Distribution Account','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AR_SALES_JOUR_BY_CUST_V','GL_LEDGERS',222,'GL_LEDGERS','GLE','GLE','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ledger Definition','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AR_SALES_JOUR_BY_CUST_V','HR_ORGANIZATION_UNITS',222,'HR_ALL_ORGANIZATION_UNITS','HOU','HOU','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Organization Details','N','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AR_SALES_JOUR_BY_CUST_V','RA_CUST_TRX_TYPES',222,'RA_CUST_TRX_TYPES_ALL','TYPES','TYPES','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Transaction Types','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AR_SALES_JOUR_BY_CUST_V','HZ_CUST_ACCOUNTS',222,'HZ_CUST_ACCOUNTS','C','C','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Customer Accounts','','','','','','Y',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_AR_SALES_JOUR_BY_CUST_V
xxeis.eis_rsc_ins.vcj( 'EIS_AR_SALES_JOUR_BY_CUST_V','HZ_CUST_ACCOUNTS','C',222,'EASJBCV.CUST_ACCOUNT_ID','=','C.CUST_ACCOUNT_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AR_SALES_JOUR_BY_CUST_V','HZ_PARTIES','PARTY',222,'EASJBCV.PARTY_ID','=','PARTY.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AR_SALES_JOUR_BY_CUST_V','GL_CODE_COMBINATIONS_KFV','GL',222,'EASJBCV.CODE_COMBINATION_ID','=','GL.CODE_COMBINATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AR_SALES_JOUR_BY_CUST_V','GL_LEDGERS','GLE',222,'EASJBCV.SET_OF_BOOKS_NAME','=','GLE.NAME(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AR_SALES_JOUR_BY_CUST_V','HR_ORGANIZATION_UNITS','HOU',222,'EASJBCV.ORGANIZATION_ID','=','HOU.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AR_SALES_JOUR_BY_CUST_V','RA_CUST_TRX_TYPES','TYPES',222,'EASJBCV.CUST_TRX_TYPE_ID','=','TYPES.CUST_TRX_TYPE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AR_SALES_JOUR_BY_CUST_V','RA_CUST_TRX_TYPES','TYPES',222,'EASJBCV.TYPES_ORG_ID','=','TYPES.ORG_ID(+)','','','','Y','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report LOV Data for WC Sales Journal By Customer - GL Range
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - WC Sales Journal By Customer - GL Range
xxeis.eis_rsc_ins.lov( 222,'select distinct party_name from hz_parties p, hz_cust_accounts c
    where p.party_id = c.party_id','null','Customer Name','Displays List of Values for Customer Name ','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT  CONCATENATED_SEGMENTS
  FROM   gl_code_combinations_kfv glcc
 WHERE  chart_of_accounts_id IN (
                SELECT led.chart_of_accounts_id
                  from gl_ledgers led
                 where gl_security_pkg.validate_access(led.ledger_id) = ''TRUE'')
   and nvl(summary_flag,''N'') = upper(''N'')
order by CONCATENATED_SEGMENTS','','EIS_GLACCOUNT_CONCAT_LOV','EIS_GLACCOUNT_CONCAT_LOV','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select account_number from hz_cust_accounts','null','Customer Number','Displays List of Values for Customer Number','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select CURRENCY_CODE, NAME from
FND_CURRENCIES_VL
WHERE CURRENCY_FLAG = ''Y''
AND ENABLED_FLAG in (''Y'', ''N'')
AND CURRENCY_CODE NOT IN(''ANY'')
ORDER BY CURRENCY_CODE','','CURRENCY_CODE','CURRENCY_CODE','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT   lookup_code, meaning
    FROM ar_lookups
   WHERE lookup_type = ''AUTOGL_TYPE''
     AND lookup_code IN (''REC'', ''REV'', ''FREIGHT'', ''TAX'', ''UNEARN'')
ORDER BY meaning,lookup_code
','','GL_ACCOUNT_TYPE','GL_ACCOUNT_TYPE','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT lookup_code, meaning
  FROM ar_lookups
 WHERE lookup_type = ''POSTING_STATUS''
and lookup_code <> ''ALL''','','POSTING_STATUS','AR POSTING_STATUS','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select distinct type from ra_cust_trx_types','','Transaction Types','This LOV lists various receivable transactions types.','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT DISTINCT ct.trx_number 
FROM    ra_customer_trx ct 
ORDER BY ct.trx_number','','AR_TRANSACTION_NUMBER','AR TRANSACTION NUMBER','XXEIS_RS_ADMIN',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report Data for WC Sales Journal By Customer - GL Range
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC Sales Journal By Customer - GL Range
xxeis.eis_rsc_utility.delete_report_rows( 'WC Sales Journal By Customer - GL Range' );
--Inserting Report - WC Sales Journal By Customer - GL Range
xxeis.eis_rsc_ins.r( 222,'WC Sales Journal By Customer - GL Range','','This report displays all transactions with summary totals for the sales journal by posting status, company and transaction currency.','','','','XXEIS_RS_ADMIN','EIS_AR_SALES_JOUR_BY_CUST_V','Y','','','XXEIS_RS_ADMIN','','N','Audit and Setup','','CSV,Pivot Excel,EXCEL,','','','','','','','','','US','','','','');
--Inserting Report Columns - WC Sales Journal By Customer - GL Range
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'LEDGER_ID','Ledger Id','Ledger Id','','','','','30','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'OPERATING_UNIT','Operating Unit','Operating Unit','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'ORGANIZATION_ID','Organization Id','Organization Id','','','','','31','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'TRX_LINE_AMOUNT','Trx Line Amount','Trx Line Amount','','','','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'LINK_LINE_DESCRIPTION','Link Line Description','Link Line Description','','','','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'LINK_LINE_NUMBER','Link Line Number','Link Line Number','','','','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'TRX_LINE_DESCRIPTION','Trx Line Description','Trx Line Description','','','','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'LINK_LINE_TYPE','Link Line Type','Link Line Type','','','','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'PARTY_NAME','Customer Name','Party Name','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'PARTY_NUMBER','Party Number','Party Number','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'TRX_COMMENTS','Trx Comments','Stores comments entered in the Transactions workbench. Value can be printed on an invoice using the Print Invoice view.','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'TRX_CURRENCY_CODE','Trx Currency Code','Trx Currency Code','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'TRX_DATE','Trx Date','Trx Date','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'TRX_LINE_QUANTITY_INVOICED','Trx Line Quantity Invoiced','Trx Line Quantity Invoiced','','','','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'TRX_LINE_QUANTITY_ORDERED','Trx Line Quantity Ordered','Trx Line Quantity Ordered','','','','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'TRX_LINE_REVENUE_AMOUNT','Trx Line Revenue Amount','Trx Line Revenue Amount','','','','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'TRX_LINE_STANDARD_PRICE','Trx Line Standard Price','Trx Line Standard Price','','','','','28','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'TRX_LINE_TYPE','Trx Line Type','Trx Line Type','','','','','29','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'TRX_NUMBER','Trx Number','Transaction number','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'TRX_TYPE_CLASS','Trx Type Class','Trx Type Class','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'TRX_TYPE_NAME','Trx Type Name','Trx Type Name','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'GL_DATE','Gl Date','General Ledger date','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'GL_ACCOUNT','Gl Account','Gl Account','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'DEBIT_AMOUNT','Debit Amount','Debit Amount','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'CREDIT_AMOUNT','Credit Amount','Credit Amount','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'LINE_NUMBER','Line Number','Line Number','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'CUSTOMER_ACCOUNT_NAME','Customer Account Name','Customer Account Name','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'DIST_ACCOUNT_CLASS','Dist Account Class','Freight, Receivable, Revenue, AutoInvoice Clearing, Tax, Unbilled Receivable, Unearned Revenue, or Charges account type','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'GL_POSTED_DATE','Gl Posted Date','Gl Posted Date','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Sales Journal By Customer - GL Range',222,'GL_TRANSFER_STATUS','Gl Transfer Status','Gl Transfer Status','','','','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AR_SALES_JOUR_BY_CUST_V','','','GROUP_BY','US','');
--Inserting Report Parameters - WC Sales Journal By Customer - GL Range
xxeis.eis_rsc_ins.rp( 'WC Sales Journal By Customer - GL Range',222,'Operating Unit','Operating Unit','OPERATING_UNIT','IN','EIS_MULTI_OPERATING_UNIT_LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Sales Journal By Customer - GL Range',222,'Customer Name','','PARTY_NAME','IN','Customer Name','','VARCHAR2','N','Y','11','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Sales Journal By Customer - GL Range',222,'GL Account','','GL_ACCOUNT','IN','EIS_GLACCOUNT_CONCAT_LOV','','VARCHAR2','N','Y','13','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','Y','GL#SEGMENT','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Sales Journal By Customer - GL Range',222,'Customer Number','','CUSTOMER_NUMBER','IN','Customer Number','','VARCHAR2','N','Y','12','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Sales Journal By Customer - GL Range',222,'Currency','','TRX_CURRENCY_CODE','IN','CURRENCY_CODE','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Sales Journal By Customer - GL Range',222,'GL Account TYPE','','DIST_ACCOUNT_CLASS','IN','GL_ACCOUNT_TYPE','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Sales Journal By Customer - GL Range',222,'Posting Status','','GL_TRANSFER_STATUS','IN','POSTING_STATUS','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Sales Journal By Customer - GL Range',222,'Transaction Type','','TRX_TYPE_CLASS','IN','Transaction Types','','VARCHAR2','N','Y','9','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Sales Journal By Customer - GL Range',222,'Transaction Number','','TRX_NUMBER','IN','AR_TRANSACTION_NUMBER','','VARCHAR2','N','Y','10','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Sales Journal By Customer - GL Range',222,'Transaction Date High','','TRX_DATE','<=','','','DATE','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Sales Journal By Customer - GL Range',222,'Transaction Date Low','','TRX_DATE','>=','','','DATE','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Sales Journal By Customer - GL Range',222,'GL Date High','','GL_DATE','<=','','','DATE','Y','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Sales Journal By Customer - GL Range',222,'GL Date Low','','GL_DATE','>=','','','DATE','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','US','');
--Inserting Dependent Parameters - WC Sales Journal By Customer - GL Range
--Inserting Report Conditions - WC Sales Journal By Customer - GL Range
xxeis.eis_rsc_ins.rcnh( 'WC Sales Journal By Customer - GL Range',222,'CUSTOMER_NUMBER IN :Customer Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_NUMBER','','Customer Number','','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','','','','IN','Y','Y','','','','','1',222,'WC Sales Journal By Customer - GL Range','CUSTOMER_NUMBER IN :Customer Number ');
xxeis.eis_rsc_ins.rcnh( 'WC Sales Journal By Customer - GL Range',222,'DIST_ACCOUNT_CLASS IN :GL Account TYPE ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DIST_ACCOUNT_CLASS','','GL Account TYPE','','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','','','','IN','Y','Y','','','','','1',222,'WC Sales Journal By Customer - GL Range','DIST_ACCOUNT_CLASS IN :GL Account TYPE ');
xxeis.eis_rsc_ins.rcnh( 'WC Sales Journal By Customer - GL Range',222,'GL_ACCOUNT IN :GL Account ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_ACCOUNT','','GL Account','','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','','','','IN','Y','Y','','','','','1',222,'WC Sales Journal By Customer - GL Range','GL_ACCOUNT IN :GL Account ');
xxeis.eis_rsc_ins.rcnh( 'WC Sales Journal By Customer - GL Range',222,'GL_DATE <= :GL Date High ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_DATE','','GL Date High','','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',222,'WC Sales Journal By Customer - GL Range','GL_DATE <= :GL Date High ');
xxeis.eis_rsc_ins.rcnh( 'WC Sales Journal By Customer - GL Range',222,'GL_DATE >= :GL Date Low ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_DATE','','GL Date Low','','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',222,'WC Sales Journal By Customer - GL Range','GL_DATE >= :GL Date Low ');
xxeis.eis_rsc_ins.rcnh( 'WC Sales Journal By Customer - GL Range',222,'GL_TRANSFER_STATUS IN :Posting Status ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_TRANSFER_STATUS','','Posting Status','','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','','','','IN','Y','Y','','','','','1',222,'WC Sales Journal By Customer - GL Range','GL_TRANSFER_STATUS IN :Posting Status ');
xxeis.eis_rsc_ins.rcnh( 'WC Sales Journal By Customer - GL Range',222,'OPERATING_UNIT IN :Operating Unit ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','OPERATING_UNIT','','Operating Unit','','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','','','','IN','Y','Y','','','','','1',222,'WC Sales Journal By Customer - GL Range','OPERATING_UNIT IN :Operating Unit ');
xxeis.eis_rsc_ins.rcnh( 'WC Sales Journal By Customer - GL Range',222,'PARTY_NAME IN :Customer Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PARTY_NAME','','Customer Name','','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','','','','IN','Y','Y','','','','','1',222,'WC Sales Journal By Customer - GL Range','PARTY_NAME IN :Customer Name ');
xxeis.eis_rsc_ins.rcnh( 'WC Sales Journal By Customer - GL Range',222,'TRX_CURRENCY_CODE IN :Currency ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TRX_CURRENCY_CODE','','Currency','','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','','','','IN','Y','Y','','','','','1',222,'WC Sales Journal By Customer - GL Range','TRX_CURRENCY_CODE IN :Currency ');
xxeis.eis_rsc_ins.rcnh( 'WC Sales Journal By Customer - GL Range',222,'TRX_DATE <= :Transaction Date High ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TRX_DATE','','Transaction Date High','','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',222,'WC Sales Journal By Customer - GL Range','TRX_DATE <= :Transaction Date High ');
xxeis.eis_rsc_ins.rcnh( 'WC Sales Journal By Customer - GL Range',222,'TRX_DATE >= :Transaction Date Low ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TRX_DATE','','Transaction Date Low','','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',222,'WC Sales Journal By Customer - GL Range','TRX_DATE >= :Transaction Date Low ');
xxeis.eis_rsc_ins.rcnh( 'WC Sales Journal By Customer - GL Range',222,'TRX_NUMBER IN :Transaction Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TRX_NUMBER','','Transaction Number','','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','','','','IN','Y','Y','','','','','1',222,'WC Sales Journal By Customer - GL Range','TRX_NUMBER IN :Transaction Number ');
xxeis.eis_rsc_ins.rcnh( 'WC Sales Journal By Customer - GL Range',222,'TRX_TYPE_CLASS IN :Transaction Type ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TRX_TYPE_CLASS','','Transaction Type','','','','','EIS_AR_SALES_JOUR_BY_CUST_V','','','','','','IN','Y','Y','','','','','1',222,'WC Sales Journal By Customer - GL Range','TRX_TYPE_CLASS IN :Transaction Type ');
--Inserting Report Sorts - WC Sales Journal By Customer - GL Range
--Inserting Report Triggers - WC Sales Journal By Customer - GL Range
--inserting report templates - WC Sales Journal By Customer - GL Range
xxeis.eis_rsc_ins.r_tem( 'WC Sales Journal By Customer - GL Range','WC Sales Journal By Customer - GL Range','Seeded template for WC Sales Journal By Customer - GL Range','','','','','','','','','','','WC Sales Journal By Customer - GL Range.rtf','XXEIS_RS_ADMIN','X','','','Y','Y','','');
--Inserting Report Portals - WC Sales Journal By Customer - GL Range
--inserting report dashboards - WC Sales Journal By Customer - GL Range
xxeis.eis_rsc_ins.R_dash( 'WC Sales Journal By Customer - GL Range','AR Sales Journal By Customer','AR Sales Journal By Customer','pie','large','Invoice Type','Invoice Type','Debit Amount','Debit Amount','Sum','XXEIS_RS_ADMIN');
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC Sales Journal By Customer - GL Range','222','EIS_AR_SALES_JOUR_BY_CUST_V','EIS_AR_SALES_JOUR_BY_CUST_V','N','');
--inserting report security - WC Sales Journal By Customer - GL Range
xxeis.eis_rsc_ins.rsec( 'WC Sales Journal By Customer - GL Range','222','','XXWC_RECEIVABLES_INQUIRY_WC',222,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - WC Sales Journal By Customer - GL Range
xxeis.eis_rsc_ins.rpivot( 'WC Sales Journal By Customer - GL Range',222,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Sales Journal By Customer - GL Range',222,'Pivot','OPERATING_UNIT','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Sales Journal By Customer - GL Range',222,'Pivot','LINK_LINE_TYPE','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Sales Journal By Customer - GL Range',222,'Pivot','PARTY_NAME','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Sales Journal By Customer - GL Range',222,'Pivot','TRX_CURRENCY_CODE','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Sales Journal By Customer - GL Range',222,'Pivot','TRX_DATE','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Sales Journal By Customer - GL Range',222,'Pivot','TRX_LINE_TYPE','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Sales Journal By Customer - GL Range',222,'Pivot','TRX_NUMBER','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Sales Journal By Customer - GL Range',222,'Pivot','TRX_TYPE_CLASS','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Sales Journal By Customer - GL Range',222,'Pivot','TRX_TYPE_NAME','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Sales Journal By Customer - GL Range',222,'Pivot','GL_ACCOUNT','ROW_FIELD','','Gl Account','4','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Sales Journal By Customer - GL Range',222,'Pivot','DEBIT_AMOUNT','DATA_FIELD','SUM','Debit Amount','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Sales Journal By Customer - GL Range',222,'Pivot','CREDIT_AMOUNT','DATA_FIELD','SUM','Credit Amount','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Sales Journal By Customer - GL Range',222,'Pivot','LINE_NUMBER','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Sales Journal By Customer - GL Range',222,'Pivot','CUSTOMER_ACCOUNT_NAME','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Sales Journal By Customer - GL Range',222,'Pivot','CUSTOMER_NUMBER','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Sales Journal By Customer - GL Range',222,'Pivot','DIST_ACCOUNT_CLASS','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Sales Journal By Customer - GL Range',222,'Pivot','GL_TRANSFER_STATUS','PAGE_FIELD','','','','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- WC Sales Journal By Customer - GL Range
xxeis.eis_rsc_ins.rv( 'WC Sales Journal By Customer - GL Range','','WC Sales Journal By Customer - GL Range','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
