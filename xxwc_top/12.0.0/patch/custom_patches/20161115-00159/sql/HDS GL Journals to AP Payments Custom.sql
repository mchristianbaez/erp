--Report Name            : HDS GL Journals to AP Payments (Custom)
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data HDS_EIS_GL_JRNL_TO_AP_PMTS
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Object HDS_EIS_GL_JRNL_TO_AP_PMTS
xxeis.eis_rsc_ins.v( 'HDS_EIS_GL_JRNL_TO_AP_PMTS',101,'','','','','XXEIS_RS_ADMIN','XXEIS','Hds Eis Gl Jrnl To Ap Pmts','HEGJTAP','','','VIEW','US','','');
--Delete Object Columns for HDS_EIS_GL_JRNL_TO_AP_PMTS
xxeis.eis_rsc_utility.delete_view_rows('HDS_EIS_GL_JRNL_TO_AP_PMTS',101,FALSE);
--Inserting Object Columns for HDS_EIS_GL_JRNL_TO_AP_PMTS
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_PERIOD',101,'Je Period','JE_PERIOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Je Period','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_CATEGORY',101,'Je Category','JE_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Je Category','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_SOURCE',101,'Je Source','JE_SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Je Source','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','BATCH_NAME',101,'Batch Name','BATCH_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Batch Name','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC1#50328#ACCOUNT#DESCR',101,'Gcc1#50328#Account#Descr','GCC1#50328#ACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc1#50328#Account#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC1#50328#ACCOUNT',101,'Gcc1#50328#Account','GCC1#50328#ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc1#50328#Account','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','INOICE_DATE',101,'Inoice Date','INOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Inoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','INVOICE_NUM',101,'Invoice Num','INVOICE_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Num','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','SUPPLIER_NUMBER',101,'Supplier Number','SUPPLIER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Supplier Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','VENDOR_NAME',101,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GL_ACCOUNT_STRING',101,'Gl Account String','GL_ACCOUNT_STRING','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl Account String','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','CHECK_DATE',101,'Check Date','CHECK_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Check Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','CHECK_NUMBER',101,'Check Number','CHECK_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Check Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_POSTED_DATE',101,'Je Posted Date','JE_POSTED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Je Posted Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','SLA_ACCOUNT',101,'Sla Account','SLA_ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sla Account','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','INVOICE_AMOUNT',101,'Invoice Amount','INVOICE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','EMPLOYEE_NUMBER',101,'Employee Number','EMPLOYEE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Employee Number','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_STATUS',101,'Je Status','JE_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Je Status','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_CURRENCY',101,'Je Currency','JE_CURRENCY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Je Currency','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_DESCRIPTION',101,'Je Description','JE_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Je Description','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_NAME',101,'Je Name','JE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Je Name','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_CATEGORY_NAME',101,'Je Category Name','JE_CATEGORY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Je Category Name','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_SOURCE_NAME',101,'Je Source Name','JE_SOURCE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Je Source Name','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','LEDGER_NAME',101,'Ledger Name','LEDGER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ledger Name','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','LEDGER_ID',101,'Ledger Id','LEDGER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ledger Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_HEADER_ID',101,'Je Header Id','JE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Je Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','BATCH_DESCRIPTION',101,'Batch Description','BATCH_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Batch Description','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_BATCH_ID',101,'Je Batch Id','JE_BATCH_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Je Batch Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC1#50328#FUTURE_USE_2',101,'Gcc1#50328#Future Use 2','GCC1#50328#FUTURE_USE_2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc1#50328#Future Use 2','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC#50328#FURTURE_USE#DESCR',101,'Gcc#50328#Furture Use#Descr','GCC#50328#FURTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc#50328#Furture Use#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC#50328#FURTURE_USE',101,'Gcc#50328#Furture Use','GCC#50328#FURTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc#50328#Furture Use','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC1#50328#FURTURE_USE#DESCR',101,'Gcc1#50328#Furture Use#Descr','GCC1#50328#FURTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc1#50328#Furture Use#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC1#50328#FURTURE_USE',101,'Gcc1#50328#Furture Use','GCC1#50328#FURTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc1#50328#Furture Use','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC#50328#PROJECT_CODE#DESCR',101,'Gcc#50328#Project Code#Descr','GCC#50328#PROJECT_CODE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc#50328#Project Code#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC#50328#PROJECT_CODE',101,'Gcc#50328#Project Code','GCC#50328#PROJECT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc#50328#Project Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC1#50328#PROJECT_CODE#DESCR',101,'Gcc1#50328#Project Code#Descr','GCC1#50328#PROJECT_CODE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc1#50328#Project Code#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC1#50328#PROJECT_CODE',101,'Gcc1#50328#Project Code','GCC1#50328#PROJECT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc1#50328#Project Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC#50328#ACCOUNT#DESCR',101,'Gcc#50328#Account#Descr','GCC#50328#ACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc#50328#Account#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC#50328#ACCOUNT',101,'Gcc#50328#Account','GCC#50328#ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc#50328#Account','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC#50328#COST_CENTER#DESCR',101,'Gcc#50328#Cost Center#Descr','GCC#50328#COST_CENTER#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc#50328#Cost Center#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC#50328#COST_CENTER',101,'Gcc#50328#Cost Center','GCC#50328#COST_CENTER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc#50328#Cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC1#50328#COST_CENTER#DESCR',101,'Gcc1#50328#Cost Center#Descr','GCC1#50328#COST_CENTER#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc1#50328#Cost Center#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC1#50328#COST_CENTER',101,'Gcc1#50328#Cost Center','GCC1#50328#COST_CENTER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc1#50328#Cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC#50328#LOCATION#DESCR',101,'Gcc#50328#Location#Descr','GCC#50328#LOCATION#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc#50328#Location#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC#50328#LOCATION',101,'Gcc#50328#Location','GCC#50328#LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc#50328#Location','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC1#50328#LOCATION#DESCR',101,'Gcc1#50328#Location#Descr','GCC1#50328#LOCATION#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc1#50328#Location#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC1#50328#LOCATION',101,'Gcc1#50328#Location','GCC1#50328#LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc1#50328#Location','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC#50328#PRODUCT#DESCR',101,'Gcc#50328#Product#Descr','GCC#50328#PRODUCT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc#50328#Product#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC#50328#PRODUCT',101,'Gcc#50328#Product','GCC#50328#PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc#50328#Product','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC1#50328#PRODUCT#DESCR',101,'Gcc1#50328#Product#Descr','GCC1#50328#PRODUCT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc1#50328#Product#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC1#50328#PRODUCT',101,'Gcc1#50328#Product','GCC1#50328#PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc1#50328#Product','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','POV_VENDOR_ID',101,'Pov Vendor Id','POV_VENDOR_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Pov Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','INVOICE_PAYMENT_ID',101,'Invoice Payment Id','INVOICE_PAYMENT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Payment Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','PAYMENT_AMOUNT',101,'Payment Amount','PAYMENT_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Payment Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','PAYMENT_HIST_DIST_ID',101,'Payment Hist Dist Id','PAYMENT_HIST_DIST_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Hist Dist Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','TEMP_LINE_NUM',101,'Temp Line Num','TEMP_LINE_NUM','','','','XXEIS_RS_ADMIN','NUMBER','','','Temp Line Num','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','REF_AE_HEADER_ID',101,'Ref Ae Header Id','REF_AE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ref Ae Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','APPLICATION_ID',101,'Application Id','APPLICATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Application Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','AE_LINE_NUM',101,'Ae Line Num','AE_LINE_NUM','','','','XXEIS_RS_ADMIN','NUMBER','','','Ae Line Num','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','AE_HEADER_ID',101,'Ae Header Id','AE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ae Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','SLA_CODE_COMBINATION_ID',101,'Sla Code Combination Id','SLA_CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Sla Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','SLA_ACCOUNTING_DATE',101,'Sla Accounting Date','SLA_ACCOUNTING_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Sla Accounting Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','TRANSFER_DATE_FROM_SLA_TO_GL',101,'Transfer Date From Sla To Gl','TRANSFER_DATE_FROM_SLA_TO_GL','','','','XXEIS_RS_ADMIN','DATE','','','Transfer Date From Sla To Gl','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','SLA_EVENT_TYPE',101,'Sla Event Type','SLA_EVENT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sla Event Type','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','SLA_ACCOUNT_CLASS',101,'Sla Account Class','SLA_ACCOUNT_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sla Account Class','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GL_SL_LINK_TABLE',101,'Gl Sl Link Table','GL_SL_LINK_TABLE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl Sl Link Table','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GL_SL_LINK_ID',101,'Gl Sl Link Id','GL_SL_LINK_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Gl Sl Link Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','INVOICE_DOC_SEQ_VALUE',101,'Invoice Doc Seq Value','INVOICE_DOC_SEQ_VALUE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Doc Seq Value','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','INVOICE_CURRENCY_CODE',101,'Invoice Currency Code','INVOICE_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','INVOICE_ID',101,'Invoice Id','INVOICE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','VENDOR_SITE',101,'Vendor Site','VENDOR_SITE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Site','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','VENDOR_SITE_ID',101,'Vendor Site Id','VENDOR_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Vendor Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','VENDOR_ID',101,'Vendor Id','VENDOR_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','CODE_COMBINATION_ID',101,'Code Combination Id','CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','CHECK_DOC_SEQ_VALUE',101,'Check Doc Seq Value','CHECK_DOC_SEQ_VALUE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Check Doc Seq Value','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','CHECK_CLEARED_AMOUNT',101,'Check Cleared Amount','CHECK_CLEARED_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Check Cleared Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','CHECK_CLEARED_DATE',101,'Check Cleared Date','CHECK_CLEARED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Check Cleared Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','BANK_ACCOUNT_NUM',101,'Bank Account Num','BANK_ACCOUNT_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bank Account Num','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','CHECK_CURRENCY_CODE',101,'Check Currency Code','CHECK_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Check Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','CHECK_ID',101,'Check Id','CHECK_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Check Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','SLA_ACCOUNTED_NET',101,'Sla Accounted Net','SLA_ACCOUNTED_NET','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Sla Accounted Net','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','SLA_ACCOUNTED_CR',101,'Sla Accounted Cr','SLA_ACCOUNTED_CR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Sla Accounted Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','SLA_ACCOUNTED_DR',101,'Sla Accounted Dr','SLA_ACCOUNTED_DR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Sla Accounted Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','SLA_ENTERED_NET',101,'Sla Entered Net','SLA_ENTERED_NET','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Sla Entered Net','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','SLA_ENTERED_CR',101,'Sla Entered Cr','SLA_ENTERED_CR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Sla Entered Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','SLA_ENTERED_DR',101,'Sla Entered Dr','SLA_ENTERED_DR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Sla Entered Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_LINE_DESCRIPTION',101,'Je Line Description','JE_LINE_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Je Line Description','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_ACCOUNTED_CR',101,'Je Accounted Cr','JE_ACCOUNTED_CR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Je Accounted Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_ACCOUNTED_DR',101,'Je Accounted Dr','JE_ACCOUNTED_DR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Je Accounted Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_ENTERED_CR',101,'Je Entered Cr','JE_ENTERED_CR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Je Entered Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_ENTERED_DR',101,'Je Entered Dr','JE_ENTERED_DR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Je Entered Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_EFFECTIVE_DATE',101,'Je Effective Date','JE_EFFECTIVE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Je Effective Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_LINE_NUM',101,'Je Line Num','JE_LINE_NUM','','','','XXEIS_RS_ADMIN','NUMBER','','','Je Line Num','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','BUDGET_DESCRIPTION',101,'Budget Description','BUDGET_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Budget Description','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','BUDGET_STATUS',101,'Budget Status','BUDGET_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Budget Status','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','BUDGET_VERSION_ID',101,'Budget Version Id','BUDGET_VERSION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Budget Version Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','BUDGET_TYPE',101,'Budget Type','BUDGET_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Budget Type','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','BUDGET_NAME',101,'Budget Name','BUDGET_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Budget Name','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_EXTERNAL_REF',101,'Je External Ref','JE_EXTERNAL_REF','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Je External Ref','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_CURR_CONV_DATE',101,'Je Curr Conv Date','JE_CURR_CONV_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Je Curr Conv Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_CURR_CONV_TYPE',101,'Je Curr Conv Type','JE_CURR_CONV_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Je Curr Conv Type','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_CURR_CONV_RATE',101,'Je Curr Conv Rate','JE_CURR_CONV_RATE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Je Curr Conv Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','REVERSED_JE_HEADER_ID',101,'Reversed Je Header Id','REVERSED_JE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Reversed Je Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','ACCRUAL_REV_CHANGE_SIGN',101,'Accrual Rev Change Sign','ACCRUAL_REV_CHANGE_SIGN','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Accrual Rev Change Sign','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','ACCRUAL_REV_JE_HEADER_ID',101,'Accrual Rev Je Header Id','ACCRUAL_REV_JE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Accrual Rev Je Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','ACCRUAL_REV_STATUS',101,'Accrual Rev Status','ACCRUAL_REV_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Accrual Rev Status','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','ACCRUAL_REV_PERIOD_NAME',101,'Accrual Rev Period Name','ACCRUAL_REV_PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Accrual Rev Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','ACCRUAL_REV_EFFECTIVE_DATE',101,'Accrual Rev Effective Date','ACCRUAL_REV_EFFECTIVE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Accrual Rev Effective Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_MULTI_BAL_SEG',101,'Je Multi Bal Seg','JE_MULTI_BAL_SEG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Je Multi Bal Seg','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','ACCRUAL_REVERSAL',101,'Accrual Reversal','ACCRUAL_REVERSAL','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Accrual Reversal','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_BALANCED',101,'Je Balanced','JE_BALANCED','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Je Balanced','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_CREATED_BY',101,'Je Created By','JE_CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Je Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_CREATION_DATE',101,'Je Creation Date','JE_CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Je Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','JE_TYPE',101,'Je Type','JE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Je Type','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC#50328#FUTURE_USE_2#DESCR',101,'Gcc#50328#Future Use 2#Descr','GCC#50328#FUTURE_USE_2#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc#50328#Future Use 2#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC#50328#FUTURE_USE_2',101,'Gcc#50328#Future Use 2','GCC#50328#FUTURE_USE_2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc#50328#Future Use 2','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','GCC1#50328#FUTURE_USE_2#DESCR',101,'Gcc1#50328#Future Use 2#Descr','GCC1#50328#FUTURE_USE_2#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gcc1#50328#Future Use 2#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'HDS_EIS_GL_JRNL_TO_AP_PMTS','PERSON_ID',101,'Person Id','PERSON_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Person Id','','','','');
--Inserting Object Components for HDS_EIS_GL_JRNL_TO_AP_PMTS
--Inserting Object Component Joins for HDS_EIS_GL_JRNL_TO_AP_PMTS
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report LOV Data for HDS GL Journals to AP Payments (Custom)
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS GL Journals to AP Payments (Custom)
xxeis.eis_rsc_ins.lov( '','SELECT CONCATENATED_SEGMENTS Account_String,
SEGMENT1 PRODUCT,
SEGMENT2 LOCATION,
SEGMENT3 COST_CENTER,
SEGMENT4 ACCOUNT,
SEGMENT5 PROJECT_CODE,
SEGMENT6 FURTURE_USE,
SEGMENT7 FUTURE_USE_2
  FROM gl_code_combinations_kfv 
			 WHERE chart_of_accounts_id IN ( 
					SELECT chart_of_accounts_id 
					  FROM gl_sets_of_books 
					 WHERE gl_security_pkg.validate_access(set_of_books_id) = ''TRUE'') 
			   and gl_security_pkg.validate_access(null,code_combination_id) = ''TRUE''   
			   and nvl(summary_flag,''N'') = upper(''N'') 
			order by CONCATENATED_SEGMENTS','','GL_50328_ACCOUNT_LOV','GL Account LOV for the Chart Of Accounts XXCUS Accounting Flexfield from gl_code_combinations_kfv','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 101,'select distinct period_name
from gl_je_headers','','HDS_GL_PERIOD_NAME','GL Period Name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report Data for HDS GL Journals to AP Payments (Custom)
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS GL Journals to AP Payments (Custom)
xxeis.eis_rsc_utility.delete_report_rows( 'HDS GL Journals to AP Payments (Custom)' );
--Inserting Report - HDS GL Journals to AP Payments (Custom)
xxeis.eis_rsc_ins.r( 101,'HDS GL Journals to AP Payments (Custom)','','','','','','XXEIS_RS_ADMIN','HDS_EIS_GL_JRNL_TO_AP_PMTS','Y','','','XXEIS_RS_ADMIN','','Y','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','Y','','','','','','','','US','','','','');
--Inserting Report Columns - HDS GL Journals to AP Payments (Custom)
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments (Custom)',101,'BATCH_NAME','Batch Name','Batch Name','','','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','HDS_EIS_GL_JRNL_TO_AP_PMTS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments (Custom)',101,'CHECK_DATE','Check Date','Check Date','','','default','','7','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','HDS_EIS_GL_JRNL_TO_AP_PMTS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments (Custom)',101,'CHECK_NUMBER','Check Number','Check Number','','~~~','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','HDS_EIS_GL_JRNL_TO_AP_PMTS','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments (Custom)',101,'GCC1#50328#ACCOUNT','Gcc1#50328#Account','Gcc1#50328#Account','','','','','14','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','HDS_EIS_GL_JRNL_TO_AP_PMTS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments (Custom)',101,'INOICE_DATE','Inoice Date','Inoice Date','','','','','12','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','HDS_EIS_GL_JRNL_TO_AP_PMTS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments (Custom)',101,'INVOICE_NUM','Invoice Num','Invoice Num','','','','','11','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','HDS_EIS_GL_JRNL_TO_AP_PMTS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments (Custom)',101,'JE_CATEGORY','Je Category','Je Category','','','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','HDS_EIS_GL_JRNL_TO_AP_PMTS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments (Custom)',101,'JE_PERIOD','Je Period','Je Period','','','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','HDS_EIS_GL_JRNL_TO_AP_PMTS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments (Custom)',101,'JE_POSTED_DATE','Je Posted Date','Je Posted Date','','','default','','5','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','HDS_EIS_GL_JRNL_TO_AP_PMTS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments (Custom)',101,'JE_SOURCE','Je Source','Je Source','','','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','HDS_EIS_GL_JRNL_TO_AP_PMTS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments (Custom)',101,'SUPPLIER_NUMBER','Supplier Number','Supplier Number','','','default','','10','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','HDS_EIS_GL_JRNL_TO_AP_PMTS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments (Custom)',101,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','8','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','HDS_EIS_GL_JRNL_TO_AP_PMTS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments (Custom)',101,'GCC1#50328#ACCOUNT#DESCR','Gcc1#50328#Account#Descr','Gcc1#50328#Account#Descr','','','','','15','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','HDS_EIS_GL_JRNL_TO_AP_PMTS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments (Custom)',101,'EMPLOYEE_NUMBER','Employee Number','Employee Number','','','default','','9','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','HDS_EIS_GL_JRNL_TO_AP_PMTS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments (Custom)',101,'SLA_ACCOUNTED_NET','SLA_ACCOUNTED_NET','Sla Accounted Net','NUMBER','~T~D~2','','','13','Y','Y','','','','','','SUM(HEGJTAP.SLA_ACCOUNTED_NET)','XXEIS_RS_ADMIN','N','N','','','','','','US','');
--Inserting Report Parameters - HDS GL Journals to AP Payments (Custom)
xxeis.eis_rsc_ins.rp( 'HDS GL Journals to AP Payments (Custom)',101,'Period','GL Period','JE_PERIOD','IN','HDS_GL_PERIOD_NAME','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','HDS_EIS_GL_JRNL_TO_AP_PMTS','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GL Journals to AP Payments (Custom)',101,'Account','GL Account','GL_ACCOUNT_STRING','IN','GL_50328_ACCOUNT_LOV','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','HDS_EIS_GL_JRNL_TO_AP_PMTS','','','US','');
--Inserting Dependent Parameters - HDS GL Journals to AP Payments (Custom)
--Inserting Report Conditions - HDS GL Journals to AP Payments (Custom)
xxeis.eis_rsc_ins.rcnh( 'HDS GL Journals to AP Payments (Custom)',101,'GL_ACCOUNT_STRING IN :Account ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_ACCOUNT_STRING','','Account','','','','','HDS_EIS_GL_JRNL_TO_AP_PMTS','','','','','','IN','Y','Y','','','','','1',101,'HDS GL Journals to AP Payments (Custom)','GL_ACCOUNT_STRING IN :Account ');
xxeis.eis_rsc_ins.rcnh( 'HDS GL Journals to AP Payments (Custom)',101,'JE_PERIOD IN :Period ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','JE_PERIOD','','Period','','','','','HDS_EIS_GL_JRNL_TO_AP_PMTS','','','','','','IN','Y','Y','','','','','1',101,'HDS GL Journals to AP Payments (Custom)','JE_PERIOD IN :Period ');
--Inserting Report Sorts - HDS GL Journals to AP Payments (Custom)
--Inserting Report Triggers - HDS GL Journals to AP Payments (Custom)
--inserting report templates - HDS GL Journals to AP Payments (Custom)
--Inserting Report Portals - HDS GL Journals to AP Payments (Custom)
--inserting report dashboards - HDS GL Journals to AP Payments (Custom)
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS GL Journals to AP Payments (Custom)','101','HDS_EIS_GL_JRNL_TO_AP_PMTS','HDS_EIS_GL_JRNL_TO_AP_PMTS','N','');
--inserting report security - HDS GL Journals to AP Payments (Custom)
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','XXCUS_GL_ACCOUNTANT_CAD',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','XXCUS_GL_ACCOUNTANT_CAD_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','GNRL_LDGR_LTMR_ACCNTNT',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','XXCUS_GL_ACCOUNTANT_USD',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','XXCUS_GL_ACCOUNTANT_USD_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','XXCUS_GL_INQUIRY',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','HDS GL INQUIRY',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','GNRL_LDGR_LTMR_NQR',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','XXCUS_GL_INQUIRY_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','XXCUS_GL_MANAGER',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','XXCUS_GL_MANAGER_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','HDS_CAD_MNTH_END_PROCS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','GNRL_LDGR_FSS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','GNRL_LDGR_LTMR_FSS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','HDS_GNRL_LDGR_CAD',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','HDS_GNRL_LDGR_SPR_USR',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','20005','','XXWC_VIEW_ALL_EIS_REPORTS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','XXCUS_GL_ACCOUNTANT_GLOBAL',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','XXCUS_GL_MANAGER_GLOBAL',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','XXCUS_GL_MANAGER_PS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','XXCUS_GL_INQUIRY_PS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','XXCUS_GL_ACCOUNTANT_USD_PS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments (Custom)','101','','XXCUS_GL_ACCOUNTANT_CAD_PS',101,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS GL Journals to AP Payments (Custom)
--Inserting Report   Version details- HDS GL Journals to AP Payments (Custom)
xxeis.eis_rsc_ins.rv( 'HDS GL Journals to AP Payments (Custom)','','HDS GL Journals to AP Payments (Custom)','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
