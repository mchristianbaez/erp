--Report Name            : HDS AP Vendor File Maintenance
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_VENDOR_FILE_M
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_VENDOR_FILE_M
xxeis.eis_rsc_ins.v( 'XXEIS_VENDOR_FILE_M',200,'Paste SQL View for HDS AP Vendor File Maintenance','1.0','','','XXEIS_RS_ADMIN','APPS','XXEIS_VENDOR_FILE_M','X4BV1','','','VIEW','US','','');
--Delete Object Columns for XXEIS_VENDOR_FILE_M
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_VENDOR_FILE_M',200,FALSE);
--Inserting Object Columns for XXEIS_VENDOR_FILE_M
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','VENDOR_NAME',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','VENDOR_NUMBER',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','VENDOR_SITE_CODE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Site Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','TAX_ID',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Tax Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','ADDRESS_LINE1',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','ADDRESS_LINE2',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','CITY',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','City','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','STATE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','State','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','ZIP',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Zip','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','PAY_GROUP_LOOKUP_CODE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pay Group Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','PRIMARY_LOB',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Primary Lob','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','REMITTANCE_EMAIL',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Remittance Email','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','TYPE_1099',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Type 1099','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','VENDOR_TYPE_LOOKUP_CODE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Type Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','INACTIVE_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Inactive Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','INVOICE_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','FIRST_NAME',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','First Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','LAST_NAME',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Last Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','PHONE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Phone','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','ORG_ID',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Org Id','','','','US');
--Inserting Object Components for XXEIS_VENDOR_FILE_M
--Inserting Object Component Joins for XXEIS_VENDOR_FILE_M
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report LOV Data for HDS AP Vendor File Maintenance
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS AP Vendor File Maintenance
xxeis.eis_rsc_ins.lov( 200,'SELECT DISTINCT CASE org_id WHEN 162 THEN ''HDS White Cap - Org''
       WHEN 163 THEN ''HD Supply Corp USD - Org''
       WHEN 166 THEN ''HDS Garnishment - Org''
       WHEN 167 THEN ''HD Supply Corp CAD - Org'' END ORGANIZATION_ID
       FROM ap.ap_invoices_all','','HDS ORG ID','ORG ID FROM ap.ap_invoices_all','ID020048',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report Data for HDS AP Vendor File Maintenance
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS AP Vendor File Maintenance
xxeis.eis_rsc_utility.delete_report_rows( 'HDS AP Vendor File Maintenance' );
--Inserting Report - HDS AP Vendor File Maintenance
xxeis.eis_rsc_ins.r( 200,'HDS AP Vendor File Maintenance','','','','','','XXEIS_RS_ADMIN','XXEIS_VENDOR_FILE_M','Y','','SELECT
       pv.vendor_name Vendor_Name
     , pv.segment1 Vendor_Number
     , pvs.vendor_site_code Vendor_Site_Code
     , pvs.attribute1 Primary_Lob
     , pv.num_1099 Tax_ID
     , pvs.address_line1 Address_Line1
     , pvs.address_line2 Address_Line2
     , pvs.city City
     , pvs.state State
     , pvs.zip Zip
     , pvc.first_name First_Name
     , pvc.last_name Last_Name
     , pvc.phone Phone
     , pvs.remittance_email Remittance_Email
     , pv.type_1099 Type_1099
     , pvs.pay_group_lookup_code Pay_Group_Lookup_Code
     , pv.vendor_type_lookup_code Vendor_Type_Lookup_Code
     , ai.invoice_date Invoice_Date
     , CASE ai.org_id WHEN 162 THEN ''HDS White Cap - Org'' 
                      WHEN 163 THEN ''HD Supply Corp USD - Org''
                      WHEN 167 THEN ''HD Supply Corp CAD - Org''
                      END ORG_ID
     , pvs.inactive_date Inactive_Date
FROM ap.ap_suppliers pv
     , ap.ap_supplier_sites_all pvs
     , ap.ap_invoices_all ai
     , ap.ap_supplier_contacts pvc
WHERE pv.vendor_id = pvs.vendor_id
   AND pvc.vendor_site_id (+)  = pvs.vendor_site_id
   AND pvs.vendor_site_id = ai.vendor_site_id
','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','Y','','','','','','','APPS','US','','','','');
--Inserting Report Columns - HDS AP Vendor File Maintenance
xxeis.eis_rsc_ins.rc( 'HDS AP Vendor File Maintenance',200,'VENDOR_NAME','Vendor Name','','','','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Vendor File Maintenance',200,'VENDOR_NUMBER','Vendor Number','','','','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Vendor File Maintenance',200,'VENDOR_SITE_CODE','Vendor Site Code','','','','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Vendor File Maintenance',200,'ADDRESS_LINE1','Address Line1','','','','default','','5','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Vendor File Maintenance',200,'ADDRESS_LINE2','Address Line2','','','','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Vendor File Maintenance',200,'CITY','City','','','','default','','7','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Vendor File Maintenance',200,'STATE','State','','','','default','','8','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Vendor File Maintenance',200,'PAY_GROUP_LOOKUP_CODE','Pay Group Lookup Code','','','','default','','10','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Vendor File Maintenance',200,'ZIP','Zip','','','','default','','9','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Vendor File Maintenance',200,'TAX_ID','Tax Id','','','','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','');
--Inserting Report Parameters - HDS AP Vendor File Maintenance
xxeis.eis_rsc_ins.rp( 'HDS AP Vendor File Maintenance',200,'Organization Name','Organization Name','ORG_ID','IN','HDS ORG ID','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_VENDOR_FILE_M','','','US','');
--Inserting Dependent Parameters - HDS AP Vendor File Maintenance
--Inserting Report Conditions - HDS AP Vendor File Maintenance
xxeis.eis_rsc_ins.rcnh( 'HDS AP Vendor File Maintenance',200,'ORG_ID IN :Org Id ','ADVANCED','','1#$#','N','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','IN','Y','Y','ORG_ID',':Org Id','','','1',200,'HDS AP Vendor File Maintenance','ORG_ID IN :Org Id ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Vendor File Maintenance',200,'ORG_ID IN :Organization Name ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Organization Name','','','','','','','','','','','IN','Y','Y','ORG_ID','','','','1',200,'HDS AP Vendor File Maintenance','ORG_ID IN :Organization Name ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Vendor File Maintenance',200,'VENDOR_SITE_CODE NOT IN ''OFFICE'' ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_SITE_CODE','','','','','','','XXEIS_VENDOR_FILE_M','','','','','','NOTIN','Y','N','','''OFFICE''','','','1',200,'HDS AP Vendor File Maintenance','VENDOR_SITE_CODE NOT IN ''OFFICE'' ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Vendor File Maintenance',200,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND (X4BV1.INVOICE_DATE >= (SYSDATE - 546))
AND X4BV1.INACTIVE_DATE IS NULL','1',200,'HDS AP Vendor File Maintenance','Free Text ');
--Inserting Report Sorts - HDS AP Vendor File Maintenance
--Inserting Report Triggers - HDS AP Vendor File Maintenance
--inserting report templates - HDS AP Vendor File Maintenance
--Inserting Report Portals - HDS AP Vendor File Maintenance
--inserting report dashboards - HDS AP Vendor File Maintenance
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS AP Vendor File Maintenance','200','XXEIS_VENDOR_FILE_M','XXEIS_VENDOR_FILE_M','N','');
--inserting report security - HDS AP Vendor File Maintenance
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','XXWC_PAY_VENDOR_MSTR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_TRNS_ENTRY_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_TRNS_ENTRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_SUPPLIER_MAINT_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','PAYABLES_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','20005','','XXWC_VIEW_ALL_EIS_REPORTS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','XXWC_PAY_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_MGR_NOSUP_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_MGR_NOSUP_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_PYBLS_MNGR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_PYABLS_MNGR_CAN',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_MGR_NOSUP_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','XXWC_PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_INQUIRY_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_INQUIRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_INQ_CANADA',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','XXWC_PAY_DISBURSE',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_DISBUREMTS_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_DISBURSEMTS_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_DISBURSEMTS_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_PAYABLES_CLOSE_GSC_GLBL',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_PAYABLES_CLOSE_GLBL',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','XXWC_PAY_W_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','XXWC_PAY_NO_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_ADMIN_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_ADMIN_US_GSCIWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_ADMIN_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_AP_ADMIN_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Vendor File Maintenance','200','','HDS_OIE_USER',200,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS AP Vendor File Maintenance
--Inserting Report   Version details- HDS AP Vendor File Maintenance
xxeis.eis_rsc_ins.rv( 'HDS AP Vendor File Maintenance','','HDS AP Vendor File Maintenance','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
