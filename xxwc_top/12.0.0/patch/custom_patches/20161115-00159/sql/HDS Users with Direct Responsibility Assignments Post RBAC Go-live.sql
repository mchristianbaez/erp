--Report Name            : HDS Users with Direct Responsibility Assignments Post RBAC Go-live
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_1571491_QKAMBT_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1571491_QKAMBT_V
xxeis.eis_rsc_ins.v( 'XXEIS_1571491_QKAMBT_V',85000,'Paste SQL View for Users with Direct Responsibility Assignments Post RBAC Go-live','1.0','','','JB027320','APPS','Users with Direct Responsibility Assignments Post RBAC Go-live View','X1QV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_1571491_QKAMBT_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1571491_QKAMBT_V',85000,FALSE);
--Inserting Object Columns for XXEIS_1571491_QKAMBT_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1571491_QKAMBT_V','USER_NAME',85000,'','','','','','JB027320','VARCHAR2','','','User Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1571491_QKAMBT_V','FULL_NAME',85000,'','','','','','JB027320','VARCHAR2','','','Full Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1571491_QKAMBT_V','LOB',85000,'','','','','','JB027320','VARCHAR2','','','Lob','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1571491_QKAMBT_V','RESPONSIBILITY_ASSIGNED',85000,'','','','','','JB027320','VARCHAR2','','','Responsibility Assigned','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1571491_QKAMBT_V','DESCRIPTION',85000,'','','','','','JB027320','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1571491_QKAMBT_V','WHEN_GRANTED',85000,'','','','','','JB027320','DATE','','','When Granted','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1571491_QKAMBT_V','WHO_GRANTED',85000,'','','','','','JB027320','VARCHAR2','','','Who Granted','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1571491_QKAMBT_V','RESP_END_DATE',85000,'','','','','','JB027320','DATE','','','Resp End Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1571491_QKAMBT_V','RESP_START_DATE',85000,'','','','','','JB027320','DATE','','','Resp Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1571491_QKAMBT_V','USER_END_DATE',85000,'','','','','','JB027320','DATE','','','User End Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1571491_QKAMBT_V','USER_START_DATE',85000,'','','','','','JB027320','DATE','','','User Start Date','','','','US');
--Inserting Object Components for XXEIS_1571491_QKAMBT_V
--Inserting Object Component Joins for XXEIS_1571491_QKAMBT_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report HDS Users with Direct Responsibility Assignments Post RBAC Go-live
prompt Creating Report Data for HDS Users with Direct Responsibility Assignments Post RBAC Go-live
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS Users with Direct Responsibility Assignments Post RBAC Go-live
xxeis.eis_rsc_utility.delete_report_rows( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live' );
--Inserting Report - HDS Users with Direct Responsibility Assignments Post RBAC Go-live
xxeis.eis_rsc_ins.r( 85000,'HDS Users with Direct Responsibility Assignments Post RBAC Go-live','','Purpose:  The HDS Users with Direct Responsibility Assignments Post RBAC Go-live report displays users who have direct assigned responsibilities, and who provided that access, since the date entered in the date parameter.  This report is hard-coded to look no further back that December 20, 2014.

Created as part of project 20268.
','','','','JB027320','XXEIS_1571491_QKAMBT_V','Y','','SELECT DISTINCT fu.user_name,
                papf.full_name,
                fu.start_date user_start_date,
                fu.end_date user_end_date,
                cc.segment1           "LOB",
                r.responsibility_name responsibility_assigned,
                urg.description,
                urg.start_date resp_start_date,
                urg.END_DATE resp_end_date,
                urg.last_update_date  WHEN_GRANTED,
                papf2.full_name       WHO_GRANTED
  FROM apps.fnd_user fu,
       apps.fnd_user fu2,
       hr.per_all_people_f papf,
       hr.per_all_people_f papf2,
       apps.fnd_user_resp_groups_direct urg,
       apps.fnd_menus_vl f,
       apps.fnd_responsibility fr,
       apps.fnd_responsibility_tl r,
       apps.fnd_menus fm,
       apps.fnd_menu_entries fme,
       apps.fnd_menu_entries_tl me,
       apps.fnd_menus fm2,
       apps.fnd_menu_entries fme2,
       apps.fnd_menu_entries_tl metl,
       apps.per_employees_x e,
       gl.gl_code_combinations cc,
       (select distinct job_descr, nt_id from XXCUS.XXCUSHR_PS_EMP_ALL_TBL) pal,
       (select distinct hpap.full_name, hpap.person_id
          from apps.PER_EMPLOYEES_X pe, hr.per_all_people_f hpap
         where pe.SUPERVISOR_ID = hpap.person_id) a
 WHERE fr.menu_id = f.menu_id
   AND fu.employee_id = papf.person_id
   AND fu2.employee_id = papf2.person_id
   and fu.employee_id = e.employee_id
   AND fu.user_id = urg.user_id
   AND fu2.user_id = urg.last_updated_by
   AND fu.user_name = UPPER(pal.nt_id)
   AND a.person_id = e.SUPERVISOR_ID
   AND e.default_code_combination_id = cc.code_combination_id(+)
   AND (urg.responsibility_id = fr.responsibility_id AND
       urg.responsibility_application_id = fr.application_id)
   AND (fr.responsibility_id = r.responsibility_id AND
       fr.application_id = r.application_id)
   AND (fme.menu_id = me.menu_id AND fme.entry_sequence = me.entry_sequence)
   AND fr.menu_id = fm.menu_id
   AND fm.menu_id = fme.menu_id
   AND fme.sub_menu_id = fm2.menu_id
   AND fm2.menu_id = fme2.menu_id
   AND (fme2.menu_id = metl.menu_id AND
       fme2.entry_sequence = metl.entry_sequence)
   AND papf.effective_end_date = ''31-DEC-4712''
   AND fu.user_name not in
       (SELECT h.description
          FROM apps.fnd_lookup_values_vl h
         WHERE h.lookup_type = ''HDS_GRC_USER_EXCLUDES''
           AND h.lookup_code LIKE ''USER_%'')
   AND r.responsibility_name not in
       (SELECT h.description
          FROM apps.fnd_lookup_values_vl h
         WHERE h.lookup_type = ''HDS_GRC_RESP_EXCLUDES''
           AND h.lookup_code LIKE ''RESP_%'')
   and urg.last_update_date > ''20-DEC-2014''
','JB027320','','N','Audit Reports','','EXCEL,','N','','','','','','','APPS','US','','','','');
--Inserting Report Columns - HDS Users with Direct Responsibility Assignments Post RBAC Go-live
xxeis.eis_rsc_ins.rc( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live',85000,'RESPONSIBILITY_ASSIGNED','Responsibility Assigned','','','','default','','6','N','','','','','','','','JB027320','N','N','','XXEIS_1571491_QKAMBT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live',85000,'DESCRIPTION','Description','','','','default','','11','N','','','','','','','','JB027320','N','N','','XXEIS_1571491_QKAMBT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live',85000,'WHO_GRANTED','Last Updated By','','','','default','','10','N','','','','','','','','JB027320','N','N','','XXEIS_1571491_QKAMBT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live',85000,'WHEN_GRANTED','When Granted','','','','default','','9','N','','','','','','','','JB027320','N','N','','XXEIS_1571491_QKAMBT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live',85000,'USER_NAME','User Name','','','','default','','2','N','','','','','','','','JB027320','N','N','','XXEIS_1571491_QKAMBT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live',85000,'LOB','LOB','','','','default','','1','N','','','','','','','','JB027320','N','N','','XXEIS_1571491_QKAMBT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live',85000,'FULL_NAME','User Description','','','','default','','3','N','','','','','','','','JB027320','N','N','','XXEIS_1571491_QKAMBT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live',85000,'RESP_END_DATE','Resp End Date','','','','default','','8','N','','','','','','','','JB027320','N','N','','XXEIS_1571491_QKAMBT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live',85000,'RESP_START_DATE','Resp Start Date','','','','default','','7','N','','','','','','','','JB027320','N','N','','XXEIS_1571491_QKAMBT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live',85000,'USER_END_DATE','User End Date','','','','default','','5','N','','','','','','','','JB027320','N','N','','XXEIS_1571491_QKAMBT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live',85000,'USER_START_DATE','User Start Date','','','','default','','4','N','','','','','','','','JB027320','N','N','','XXEIS_1571491_QKAMBT_V','','','GROUP_BY','US','');
--Inserting Report Parameters - HDS Users with Direct Responsibility Assignments Post RBAC Go-live
xxeis.eis_rsc_ins.rp( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live',85000,'From Date (to present)','From Date','WHEN_GRANTED','>=','','20-DEC-2014','DATE','N','Y','1','Y','Y','CONSTANT','JB027320','Y','','','','','XXEIS_1571491_QKAMBT_V','','','US','');
--Inserting Dependent Parameters - HDS Users with Direct Responsibility Assignments Post RBAC Go-live
--Inserting Report Conditions - HDS Users with Direct Responsibility Assignments Post RBAC Go-live
xxeis.eis_rsc_ins.rcnh( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live',85000,'WHEN_GRANTED >= :As Of Date ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','WHEN_GRANTED','','','','','','','XXEIS_1571491_QKAMBT_V','','','','','','GREATER_THAN_EQUALS','Y','Y','',':As Of Date','','','1',85000,'HDS Users with Direct Responsibility Assignments Post RBAC Go-live','WHEN_GRANTED >= :As Of Date ');
xxeis.eis_rsc_ins.rcnh( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live',85000,'WHEN_GRANTED >= :From Date (to present) ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','WHEN_GRANTED','','From Date (to present)','','','','','XXEIS_1571491_QKAMBT_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',85000,'HDS Users with Direct Responsibility Assignments Post RBAC Go-live','WHEN_GRANTED >= :From Date (to present) ');
xxeis.eis_rsc_ins.rcnh( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live',85000,'WHEN_GRANTED >= :Look Back Date ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','WHEN_GRANTED','','','','','','','XXEIS_1571491_QKAMBT_V','','','','','','GREATER_THAN_EQUALS','Y','Y','',':Look Back Date','','','1',85000,'HDS Users with Direct Responsibility Assignments Post RBAC Go-live','WHEN_GRANTED >= :Look Back Date ');
--Inserting Report Sorts - HDS Users with Direct Responsibility Assignments Post RBAC Go-live
xxeis.eis_rsc_ins.rs( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live',85000,'WHEN_GRANTED','ASC','JB027320','1','');
--Inserting Report Triggers - HDS Users with Direct Responsibility Assignments Post RBAC Go-live
--inserting report templates - HDS Users with Direct Responsibility Assignments Post RBAC Go-live
--Inserting Report Portals - HDS Users with Direct Responsibility Assignments Post RBAC Go-live
--inserting report dashboards - HDS Users with Direct Responsibility Assignments Post RBAC Go-live
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live','85000','XXEIS_1571491_QKAMBT_V','XXEIS_1571491_QKAMBT_V','N','');
--inserting report security - HDS Users with Direct Responsibility Assignments Post RBAC Go-live
xxeis.eis_rsc_ins.rsec( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live','101','','XXCUS_GL_INQUIRY',85000,'JB027320','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live','20005','','XXWC_IT_SECURITY_ADMINISTRATOR',85000,'JB027320','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live','0','','XXWC_IT_ROLE_CONFIGURATION',85000,'JB027320','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live','20005','','XXWC_IT_OPERATIONS_ANALYST',85000,'JB027320','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live','20005','','XXWC_IT_FUNC_CONFIGURATOR',85000,'JB027320','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live','20005','','XXWC_VIEW_ALL_EIS_REPORTS',85000,'JB027320','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live','1','','SYSTEM_ADMINISTRATOR',85000,'JB027320','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live','1','','HDS_SYSTEM_ADMINISTRATOR',85000,'JB027320','','','');
--Inserting Report Pivots - HDS Users with Direct Responsibility Assignments Post RBAC Go-live
--Inserting Report   Version details- HDS Users with Direct Responsibility Assignments Post RBAC Go-live
xxeis.eis_rsc_ins.rv( 'HDS Users with Direct Responsibility Assignments Post RBAC Go-live','','HDS Users with Direct Responsibility Assignments Post RBAC Go-live','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
