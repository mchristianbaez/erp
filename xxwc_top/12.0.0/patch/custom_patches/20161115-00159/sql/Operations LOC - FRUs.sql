--Report Name            : Operations (LOC - FRUs)
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
CREATE OR REPLACE VIEW APPS.XXEIS_1321490_OBPRZL_V (LOC_FRU, LOC, LOC_TYPE, LOC_STATUS, EARLIEST_START, LATEST_END, ADDRESS, SUITE, COUNTY, CITY, ST_PRV, POSTAL_CODE, COUNTRY, BU, BRANCH, ORACLE_ID, FRU, FRU_DESCRIPTION, SECTION, OPS_TYPE, OPS_SF, SITE_TYPE, ASSIGNMENT_TYPE, OPS_START, OPS_END, OPS_STATUS, CUSTOMER_SITE, COMMENTS)
AS
  SELECT A.BL_LD_ID
    ||'-'
    ||C.LC_FRU LOC_FRU ,
    A.BL_LD_ID LOC ,
    A.LOCATION_TYPE LOC_TYPE ,
    A.HDS_BLDG_LAND_STATUS LOC_STATUS ,
    TO_CHAR(A.HDS_BLDG_LAND_MIN_START,'MM/DD/YYYY') EARLIEST_START ,
    TO_CHAR(A.HDS_BLDG_LAND_MAX_END,'MM/DD/YYYY') LATEST_END ,
    A.ADDRESS_LINE1 ADDRESS ,
    C.LOC_SUITE_NAME SUITE ,
    A.COUNTY COUNTY ,
    A.CITY CITY ,
    A.HDS_ST_PRV ST_PRV,
    A.POSTAL_CODE POSTAL_CODE ,
    A.COUNTRY_CODE COUNTRY,
    C.LOB_ABB BU ,
    C.LC_LOB_BRANCH BRANCH ,
    C.LC_ENTRP_LOC ORACLE_ID ,
    C.LC_FRU FRU ,
    C.LC_FRU_DESCRIPTION FRU_DESCRIPTION ,
    C.LOCATION_CODE SECTION ,
    C.SPACE_TYPE OPS_TYPE ,
    C.AREA_ALLOCATED OPS_SF ,
    C.SITE_TYPE SITE_TYPE,
    A.SC_YD_ASSIGNMENT_TYPE ASSIGNMENT_TYPE ,
    TO_CHAR(C.EMP_ASSIGN_START_DATE,'MM/DD/YYYY') OPS_START ,
    TO_CHAR(C.EMP_ASSIGN_END_DATE ,'MM/DD/YYYY') OPS_END,
    C.HDS_OPS_STATUS OPS_STATUS ,
    A.OFFICE_CUSTOMER_SITE CUSTOMER_SITE ,
    C.EMP_SPACE_COMMENTS COMMENTS
  FROM xxcus.xxcus_opn_properties_all a ,
    (SELECT OPN_ID ,
      LOCATION_CODE ,
      bldg_land_code ,
      LOC_ACTIVE_START_DATE ,
      loc_active_end_date
    FROM xxcus.xxcus_opn_fru_loc_all
    WHERE 1                          =1
    AND hds_fru_section_current_flag = 'Y'
    GROUP BY OPN_ID ,
      LOCATION_CODE ,
      BLDG_LAND_CODE ,
      LOC_ACTIVE_START_DATE ,
      loc_active_end_date
    ) b ,
    xxcus.xxcus_opn_fru_loc_all c
  WHERE 1                            =1
  AND a.opn_id                       =b.opn_id
  AND a.opn_type                     ='PROPERTY'
  AND a.location_code                =b.location_code
  AND a.loc_active_start_date        =b.loc_active_start_date
  AND c.location_code                =b.location_code
  AND c.loc_active_start_date        =b.loc_active_start_date
  AND c.loc_active_end_date          =b.loc_active_end_date
  AND c.bldg_land_code               =b.bldg_land_code
  AND c.hds_fru_section_current_flag ='Y';
/
prompt Creating Object Data XXEIS_1321490_OBPRZL_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(240);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1321490_OBPRZL_V
xxeis.eis_rsc_ins.v( 'XXEIS_1321490_OBPRZL_V',240,'Paste SQL View for 2 -Operations ( LOC - FRUs)','1.0','','','DV003828','APPS','2 -Operations ( LOC - FRUs) View','X1OV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_1321490_OBPRZL_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1321490_OBPRZL_V',240,FALSE);
--Inserting Object Columns for XXEIS_1321490_OBPRZL_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','ORACLE_ID',240,'','','','','','DV003828','VARCHAR2','','','Oracle Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','FRU',240,'','','','','','DV003828','VARCHAR2','','','Fru','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','FRU_DESCRIPTION',240,'','','','','','DV003828','VARCHAR2','','','Fru Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','SECTION',240,'','','','','','DV003828','VARCHAR2','','','Section','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','OPS_TYPE',240,'','','','','','DV003828','VARCHAR2','','','Ops Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','OPS_SF',240,'','','','','','DV003828','NUMBER','','','Ops Sf','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','SITE_TYPE',240,'','','','','','DV003828','VARCHAR2','','','Site Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','ASSIGNMENT_TYPE',240,'','','','','','DV003828','VARCHAR2','','','Assignment Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','OPS_START',240,'','','','','','DV003828','VARCHAR2','','','Ops Start','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','OPS_STATUS',240,'','','','','','DV003828','VARCHAR2','','','Ops Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','CUSTOMER_SITE',240,'','','','','','DV003828','VARCHAR2','','','Customer Site','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','COMMENTS',240,'','','','','','DV003828','VARCHAR2','','','Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','LOC_FRU',240,'','','','','','DV003828','VARCHAR2','','','Loc Fru','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','LOC',240,'','','','','','DV003828','VARCHAR2','','','Loc','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','LOC_TYPE',240,'','','','','','DV003828','VARCHAR2','','','Loc Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','LOC_STATUS',240,'','','','','','DV003828','VARCHAR2','','','Loc Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','ADDRESS',240,'','','','','','DV003828','VARCHAR2','','','Address','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','SUITE',240,'','','','','','DV003828','VARCHAR2','','','Suite','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','COUNTY',240,'','','','','','DV003828','VARCHAR2','','','County','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','CITY',240,'','','','','','DV003828','VARCHAR2','','','City','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','ST_PRV',240,'','','','','','DV003828','VARCHAR2','','','St Prv','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','POSTAL_CODE',240,'','','','','','DV003828','VARCHAR2','','','Postal Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','COUNTRY',240,'','','','','','DV003828','VARCHAR2','','','Country','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','BU',240,'','','','','','DV003828','VARCHAR2','','','Bu','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','BRANCH',240,'','','','','','DV003828','VARCHAR2','','','Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','EARLIEST_START',240,'','','','','','DV003828','VARCHAR2','','','Earliest Start','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','LATEST_END',240,'','','','','','DV003828','VARCHAR2','','','Latest End','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1321490_OBPRZL_V','OPS_END',240,'','','','','','DV003828','VARCHAR2','','','Ops End','','','','US');
--Inserting Object Components for XXEIS_1321490_OBPRZL_V
--Inserting Object Component Joins for XXEIS_1321490_OBPRZL_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 240');
END IF;
END;
/
prompt Creating Report LOV Data for Operations (LOC - FRUs)
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(240);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Operations (LOC - FRUs)
xxeis.eis_rsc_ins.lov( 240,'select distinct lob_abb
from 
XXCUS.XXCUS_OPN_FRU_LOC_ALL','','OPN_LOB_ABB (BU)','distinct LOB_ABB from XXCUS.XXCUS_OPN_FRU_LOC_ALL table','DV003828',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 240,'select distinct SPACE_TYPE OPS_TYPE
from 
XXCUS.XXCUS_OPN_FRU_LOC_ALL','','OPN_OPS_TYPE','SPACE_TYPE  from XXCUS.XXCUS_OPN_FRU_LOC_ALL table','DV003828',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 240,'select distinct SC_YD_SITE_TYPE_CODE  SITE_TYPE
  from   XXCUS.XXCUS_OPN_PROPERTIES_ALL','','OPN_SITE_TYPE','distinct SC_YD_SITE_TYPE_CODE   from XXCUS.XXCUS_OPN_PROPERTIES_ALL table','DV003828',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 240,'select distinct HDS_OPS_STATUS
from XXCUS.XXCUS_OPN_FRU_LOC_ALL','','OPN_OPS_STATUS','HDS_OPS_STATUS from XXCUS.XXCUS_OPN_FRU_LOC_ALL table','DV003828',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 240');
END IF;
END;
/
prompt Creating Report Data for Operations (LOC - FRUs)
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(240);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Operations (LOC - FRUs)
xxeis.eis_rsc_utility.delete_report_rows( 'Operations (LOC - FRUs)' );
--Inserting Report - Operations (LOC - FRUs)
xxeis.eis_rsc_ins.r( 240,'Operations (LOC - FRUs)','','List of all operations assigned to each LOC, including Ops Type, square footage, as well as ops status, start/end date and Section ID.  Administrative OPS Types are excluded from the report   
Unique Row: Section ID
','','','','AM033286','XXEIS_1321490_OBPRZL_V','Y','','SELECT 
  A.BL_LD_ID  ||''-''  ||C.LC_FRU           LOC_FRU ,
  A.BL_LD_ID                              LOC ,
  A.LOCATION_TYPE                         LOC_TYPE ,
  A.HDS_BLDG_LAND_STATUS                  LOC_STATUS ,
  TO_CHAR(A.HDS_BLDG_LAND_MIN_START,''MM/DD/YYYY'')      EARLIEST_START , 
  TO_CHAR(A.HDS_BLDG_LAND_MAX_END,''MM/DD/YYYY'') LATEST_END ,
  A.ADDRESS_LINE1                         ADDRESS ,
  C.LOC_SUITE_NAME                        SUITE ,
  A.COUNTY                                COUNTY ,
  A.CITY                                  CITY ,
  A.HDS_ST_PRV                             ST_PRV,
  A.POSTAL_CODE                           POSTAL_CODE ,
  A.COUNTRY_CODE                          COUNTRY,
  C.LOB_ABB                               BU ,
  C.LC_LOB_BRANCH                         BRANCH ,
  C.LC_ENTRP_LOC                          ORACLE_ID ,
  C.LC_FRU                                FRU ,
  C.LC_FRU_DESCRIPTION                    FRU_DESCRIPTION ,
  C.LOCATION_CODE                         SECTION ,
  C.SPACE_TYPE                            OPS_TYPE ,
  C.AREA_ALLOCATED                        OPS_SF ,
  C.SITE_TYPE                                 SITE_TYPE,
  A.SC_YD_ASSIGNMENT_TYPE                 ASSIGNMENT_TYPE ,  
  to_char(C.EMP_ASSIGN_START_DATE,''MM/DD/YYYY'')         OPS_START , 
  TO_CHAR(C.EMP_ASSIGN_END_DATE ,''MM/DD/YYYY'')  OPS_END,
  C.HDS_OPS_STATUS                        OPS_STATUS ,
  A.OFFICE_CUSTOMER_SITE                  CUSTOMER_SITE ,
  C.EMP_SPACE_COMMENTS                    COMMENTS 



 FROM  xxcus.xxcus_opn_properties_all a
       ,(
          SELECT 
            OPN_ID
            ,LOCATION_CODE
            ,bldg_land_code
            ,LOC_ACTIVE_START_DATE
            ,loc_active_end_date
          from   xxcus.xxcus_opn_fru_loc_all
          where  1 =1
            and  hds_fru_section_current_flag = ''Y''
          GROUP BY 
            OPN_ID
            ,LOCATION_CODE
            ,BLDG_LAND_CODE
            ,LOC_ACTIVE_START_DATE
            ,loc_active_end_date
        ) b
       ,xxcus.xxcus_opn_fru_loc_all c
    where 1 =1
     and a.opn_id                         =b.opn_id
     and a.opn_type                       =''PROPERTY''
     and a.location_code                  =b.location_code
     and a.loc_active_start_date          =b.loc_active_start_date
     and c.location_code                  =b.location_code
     and c.loc_active_start_date          =b.loc_active_start_date
     and c.loc_active_end_date            =b.loc_active_end_date
     and c.bldg_land_code                 =b.bldg_land_code
     and c.hds_fru_section_current_flag   =''Y''


','AM033286','','N','Locations and Operations','RTF,PDF,','CSV,HTML,EXCEL,','N','','','','','','','APPS','US','','','','');
--Inserting Report Columns - Operations (LOC - FRUs)
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'LOC','LOC','','','','default','','2','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'LOC_FRU','LOC-FRU','','','','default','','1','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'LOC_STATUS','LOC Status','','','','default','','4','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'LOC_TYPE','LOC Type','','','','default','','3','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'OPS_SF','OPS SF','','','~~~','default','','21','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'OPS_STATUS','Ops Status','','','','default','','26','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'OPS_TYPE','OPS Type','','','','default','','20','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'ORACLE_ID','Oracle ID','','','','default','','16','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'POSTAL_CODE','Postal Code','','','','default','','12','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'SECTION','Section','','','','default','','19','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'SITE_TYPE','Site Type','','','','default','','22','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'ST_PRV','ST/PRV','','','','default','','11','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'SUITE','Suite','','','','default','','8','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'EARLIEST_START','LOC Start','','','','default','','5','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'ADDRESS','Address','','','','default','','7','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'ASSIGNMENT_TYPE','Assignment Type','','','','default','','23','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'BRANCH','Branch','','','','default','','15','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'BU','BU','','','','default','','14','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'CITY','City','','','','default','','10','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'COMMENTS','Comments','','','','default','','28','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'COUNTRY','Country','','','','default','','13','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'COUNTY','County','','','','default','','9','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'CUSTOMER_SITE','Customer Site','','','','default','','27','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'FRU','FRU','','','','default','','17','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'FRU_DESCRIPTION','FRU Description','','','','default','','18','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'LATEST_END','LOC End','','','','default','','6','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'OPS_END','Ops End','','','','default','','25','N','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Operations (LOC - FRUs)',240,'OPS_START','Ops Start','','','','default','','24','','Y','','','','','','','AM033286','N','N','','XXEIS_1321490_OBPRZL_V','','','','US','');
--Inserting Report Parameters - Operations (LOC - FRUs)
xxeis.eis_rsc_ins.rp( 'Operations (LOC - FRUs)',240,'Ops Type','','OPS_TYPE','IN','OPN_OPS_TYPE','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','AM033286','Y','N','','','','XXEIS_1321490_OBPRZL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Operations (LOC - FRUs)',240,'Site Type','','SITE_TYPE','IN','OPN_SITE_TYPE','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','AM033286','Y','N','','','','XXEIS_1321490_OBPRZL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Operations (LOC - FRUs)',240,'BU','','BU','IN','OPN_LOB_ABB (BU)','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','AM033286','Y','N','','','','XXEIS_1321490_OBPRZL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Operations (LOC - FRUs)',240,'Ops Status','','OPS_STATUS','IN','OPN_OPS_STATUS','''Active''','VARCHAR2','N','Y','4','Y','Y','CONSTANT','AM033286','Y','N','','','','XXEIS_1321490_OBPRZL_V','','','US','');
--Inserting Dependent Parameters - Operations (LOC - FRUs)
--Inserting Report Conditions - Operations (LOC - FRUs)
xxeis.eis_rsc_ins.rcnh( 'Operations (LOC - FRUs)',240,'BU IN :BU ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BU','','BU','','','','','XXEIS_1321490_OBPRZL_V','','','','','','IN','Y','Y','','','','','1',240,'Operations (LOC - FRUs)','BU IN :BU ');
xxeis.eis_rsc_ins.rcnh( 'Operations (LOC - FRUs)',240,'OPS_STATUS IN :Ops Status ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','OPS_STATUS','','Ops Status','','','','','XXEIS_1321490_OBPRZL_V','','','','','','IN','Y','Y','','','','','1',240,'Operations (LOC - FRUs)','OPS_STATUS IN :Ops Status ');
xxeis.eis_rsc_ins.rcnh( 'Operations (LOC - FRUs)',240,'OPS_TYPE IN :Ops Type ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','OPS_TYPE','','Ops Type','','','','','XXEIS_1321490_OBPRZL_V','','','','','','IN','Y','Y','','','','','1',240,'Operations (LOC - FRUs)','OPS_TYPE IN :Ops Type ');
xxeis.eis_rsc_ins.rcnh( 'Operations (LOC - FRUs)',240,'SITE_TYPE IN :Site Type ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SITE_TYPE','','Site Type','','','','','XXEIS_1321490_OBPRZL_V','','','','','','IN','Y','Y','','','','','1',240,'Operations (LOC - FRUs)','SITE_TYPE IN :Site Type ');
xxeis.eis_rsc_ins.rcnh( 'Operations (LOC - FRUs)',240,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','and OPS_TYPE not in (''Administrative'')','1',240,'Operations (LOC - FRUs)','Free Text ');
--Inserting Report Sorts - Operations (LOC - FRUs)
xxeis.eis_rsc_ins.rs( 'Operations (LOC - FRUs)',240,'COUNTRY','ASC','AM033286','1','');
xxeis.eis_rsc_ins.rs( 'Operations (LOC - FRUs)',240,'ST_PRV','ASC','AM033286','2','');
xxeis.eis_rsc_ins.rs( 'Operations (LOC - FRUs)',240,'CITY','ASC','AM033286','3','');
xxeis.eis_rsc_ins.rs( 'Operations (LOC - FRUs)',240,'LOC_FRU','ASC','AM033286','4','');
--Inserting Report Triggers - Operations (LOC - FRUs)
--inserting report templates - Operations (LOC - FRUs)
--Inserting Report Portals - Operations (LOC - FRUs)
--inserting report dashboards - Operations (LOC - FRUs)
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Operations (LOC - FRUs)','240','XXEIS_1321490_OBPRZL_V','XXEIS_1321490_OBPRZL_V','N','');
--inserting report security - Operations (LOC - FRUs)
xxeis.eis_rsc_ins.rsec( 'Operations (LOC - FRUs)','20003','','XXCUS_OPN_PROP_MGR',240,'AM033286','','','');
xxeis.eis_rsc_ins.rsec( 'Operations (LOC - FRUs)','','AM033286','',240,'AM033286','','','');
xxeis.eis_rsc_ins.rsec( 'Operations (LOC - FRUs)','','AB063501','',240,'AM033286','','N','');
--Inserting Report Pivots - Operations (LOC - FRUs)
--Inserting Report   Version details- Operations (LOC - FRUs)
xxeis.eis_rsc_ins.rv( 'Operations (LOC - FRUs)','','Operations (LOC - FRUs)','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 240');
END IF;
END;
/
