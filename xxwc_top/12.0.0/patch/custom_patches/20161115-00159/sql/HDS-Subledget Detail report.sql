--Report Name            : HDS-Subledget Detail report
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_XXHDS_GL_SL_180_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXHDS_GL_SL_180_V
xxeis.eis_rsc_ins.v( 'EIS_XXHDS_GL_SL_180_V',101,'','','','','MT063505','XXEIS','Eis Xxhds Gl Sl 180 V','EXGS_V','','','VIEW','US','','');
--Delete Object Columns for EIS_XXHDS_GL_SL_180_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXHDS_GL_SL_180_V',101,FALSE);
--Inserting Object Columns for EIS_XXHDS_GL_SL_180_V
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#ACCOUNT#DESCR',101,'Gcc#50328#Account#Descr','GCC#50328#ACCOUNT#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50328#Account#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_LINE_ENTERED_NET',101,'Sla Line Entered Net','SLA_LINE_ENTERED_NET','','','','MT063505','NUMBER','','','Sla Line Entered Net','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_LINE_ACCOUNTED_NET',101,'Sla Line Accounted Net','SLA_LINE_ACCOUNTED_NET','','','','MT063505','NUMBER','','','Sla Line Accounted Net','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEGMENT5',101,'Segment5','SEGMENT5','','','','MT063505','VARCHAR2','','','Segment5','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEGMENT4',101,'Segment4','SEGMENT4','','','','MT063505','VARCHAR2','','','Segment4','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEGMENT3',101,'Segment3','SEGMENT3','','','','MT063505','VARCHAR2','','','Segment3','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEGMENT2',101,'Segment2','SEGMENT2','','','','MT063505','VARCHAR2','','','Segment2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEGMENT1',101,'Segment1','SEGMENT1','','','','MT063505','VARCHAR2','','','Segment1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','BATCH_NAME',101,'Batch Name','BATCH_NAME','','','','MT063505','VARCHAR2','','','Batch Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','NAME',101,'Name','NAME','','','','MT063505','VARCHAR2','','','Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','TYPE',101,'Type','TYPE','','','','MT063505','VARCHAR2','','','Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','PERIOD_NAME',101,'Period Name','PERIOD_NAME','','','','MT063505','VARCHAR2','','','Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','CURRENCY_CODE',101,'Currency Code','CURRENCY_CODE','','','','MT063505','VARCHAR2','','','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','TRANSACTION_NUM',101,'Transaction Num','TRANSACTION_NUM','','','','MT063505','VARCHAR2','','','Transaction Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_DIST_ACCOUNTED_NET',101,'Sla Dist Accounted Net','SLA_DIST_ACCOUNTED_NET','','','','MT063505','NUMBER','','','Sla Dist Accounted Net','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_DIST_ACCOUNTED_DR',101,'Sla Dist Accounted Dr','SLA_DIST_ACCOUNTED_DR','','','','MT063505','NUMBER','','','Sla Dist Accounted Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_DIST_ACCOUNTED_CR',101,'Sla Dist Accounted Cr','SLA_DIST_ACCOUNTED_CR','','','','MT063505','NUMBER','','','Sla Dist Accounted Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_DIST_ENTERED_NET',101,'Sla Dist Entered Net','SLA_DIST_ENTERED_NET','','','','MT063505','NUMBER','','','Sla Dist Entered Net','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ACCOUNTED_DR',101,'Accounted Dr','ACCOUNTED_DR','','','','MT063505','NUMBER','','','Accounted Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ACCOUNTED_CR',101,'Accounted Cr','ACCOUNTED_CR','','','','MT063505','NUMBER','','','Accounted Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','CUSTOMER_OR_VENDOR',101,'Customer Or Vendor','CUSTOMER_OR_VENDOR','','','','MT063505','VARCHAR2','','','Customer Or Vendor','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GL_SL_LINK_ID',101,'Gl Sl Link Id','GL_SL_LINK_ID','','','','MT063505','NUMBER','','','Gl Sl Link Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LINE_DESCR',101,'Line Descr','LINE_DESCR','','','','MT063505','VARCHAR2','','','Line Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ENTRY',101,'Entry','ENTRY','','','','MT063505','VARCHAR2','','','Entry','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ACC_DATE',101,'Acc Date','ACC_DATE','','','','MT063505','DATE','','','Acc Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','JE_LINE_NUM',101,'Je Line Num','JE_LINE_NUM','','','','MT063505','NUMBER','','','Je Line Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','JE_CATEGORY',101,'Je Category','JE_CATEGORY','','','','MT063505','VARCHAR2','','','Je Category','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SOURCE',101,'Source','SOURCE','','','','MT063505','VARCHAR2','','','Source','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#FUTURE_USE',101,'Gcc#50368#Future Use','GCC#50368#FUTURE_USE','','','','MT063505','VARCHAR2','','','Gcc#50368#Future Use','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#DIVISION#DESCR',101,'Gcc#50368#Division#Descr','GCC#50368#DIVISION#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50368#Division#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#DIVISION',101,'Gcc#50368#Division','GCC#50368#DIVISION','','','','MT063505','VARCHAR2','','','Gcc#50368#Division','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#DEPARTMENT#DESCR',101,'Gcc#50368#Department#Descr','GCC#50368#DEPARTMENT#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50368#Department#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#DEPARTMENT',101,'Gcc#50368#Department','GCC#50368#DEPARTMENT','','','','MT063505','VARCHAR2','','','Gcc#50368#Department','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#ACCOUNT#DESCR',101,'Gcc#50368#Account#Descr','GCC#50368#ACCOUNT#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50368#Account#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#ACCOUNT',101,'Gcc#50368#Account','GCC#50368#ACCOUNT','','','','MT063505','VARCHAR2','','','Gcc#50368#Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#PROJECT_CODE#DESCR',101,'Gcc#50328#Project Code#Descr','GCC#50328#PROJECT_CODE#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50328#Project Code#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#PROJECT_CODE',101,'Gcc#50328#Project Code','GCC#50328#PROJECT_CODE','','','','MT063505','VARCHAR2','','','Gcc#50328#Project Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#PRODUCT#DESCR',101,'Gcc#50328#Product#Descr','GCC#50328#PRODUCT#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50328#Product#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#PRODUCT',101,'Gcc#50328#Product','GCC#50328#PRODUCT','','','','MT063505','VARCHAR2','','','Gcc#50328#Product','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#LOCATION#DESCR',101,'Gcc#50328#Location#Descr','GCC#50328#LOCATION#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50328#Location#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#LOCATION',101,'Gcc#50328#Location','GCC#50328#LOCATION','','','','MT063505','VARCHAR2','','','Gcc#50328#Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#FUTURE_USE_2#DESCR',101,'Gcc#50328#Future Use 2#Descr','GCC#50328#FUTURE_USE_2#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50328#Future Use 2#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#FUTURE_USE_2',101,'Gcc#50328#Future Use 2','GCC#50328#FUTURE_USE_2','','','','MT063505','VARCHAR2','','','Gcc#50328#Future Use 2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#COST_CENTER#DESCR',101,'Gcc#50328#Cost Center#Descr','GCC#50328#COST_CENTER#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50328#Cost Center#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#COST_CENTER',101,'Gcc#50328#Cost Center','GCC#50328#COST_CENTER','','','','MT063505','VARCHAR2','','','Gcc#50328#Cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#ACCOUNT',101,'Gcc#50328#Account','GCC#50328#ACCOUNT','','','','MT063505','VARCHAR2','','','Gcc#50328#Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#BRANCH',101,'Gcc#Branch','GCC#BRANCH','','','','MT063505','VARCHAR2','','','Gcc#Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','CODE_COMBINATION_ID',101,'Code Combination Id','CODE_COMBINATION_ID','','','','MT063505','NUMBER','','','Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','EFFECTIVE_PERIOD_NUM',101,'Effective Period Num','EFFECTIVE_PERIOD_NUM','','','','MT063505','NUMBER','','','Effective Period Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GL_ACCOUNT_STRING',101,'Gl Account String','GL_ACCOUNT_STRING','','','','MT063505','VARCHAR2','','','Gl Account String','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_DIST_ENTERED_DR',101,'Sla Dist Entered Dr','SLA_DIST_ENTERED_DR','','','','MT063505','NUMBER','','','Sla Dist Entered Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_DIST_ENTERED_CR',101,'Sla Dist Entered Cr','SLA_DIST_ENTERED_CR','','','','MT063505','NUMBER','','','Sla Dist Entered Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_LINE_ENTERED_CR',101,'Sla Line Entered Cr','SLA_LINE_ENTERED_CR','','','','MT063505','NUMBER','','','Sla Line Entered Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_LINE_ENTERED_DR',101,'Sla Line Entered Dr','SLA_LINE_ENTERED_DR','','','','MT063505','NUMBER','','','Sla Line Entered Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_LINE_ACCOUNTED_CR',101,'Sla Line Accounted Cr','SLA_LINE_ACCOUNTED_CR','','','','MT063505','NUMBER','','','Sla Line Accounted Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_LINE_ACCOUNTED_DR',101,'Sla Line Accounted Dr','SLA_LINE_ACCOUNTED_DR','','','','MT063505','NUMBER','','','Sla Line Accounted Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ENTERED_CR',101,'Entered Cr','ENTERED_CR','','','','MT063505','NUMBER','','','Entered Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ENTERED_DR',101,'Entered Dr','ENTERED_DR','','','','MT063505','NUMBER','','','Entered Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','H_SEQ_ID',101,'H Seq Id','H_SEQ_ID','','','','MT063505','NUMBER','','','H Seq Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEQ_NUM',101,'Seq Num','SEQ_NUM','','','','MT063505','NUMBER','','','Seq Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEQ_ID',101,'Seq Id','SEQ_ID','','','','MT063505','NUMBER','','','Seq Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LINE_ENT_CR',101,'Line Ent Cr','LINE_ENT_CR','','','','MT063505','NUMBER','','','Line Ent Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LINE_ENT_DR',101,'Line Ent Dr','LINE_ENT_DR','','','','MT063505','NUMBER','','','Line Ent Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LINE_ACCTD_CR',101,'Line Acctd Cr','LINE_ACCTD_CR','','','','MT063505','NUMBER','','','Line Acctd Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LINE_ACCTD_DR',101,'Line Acctd Dr','LINE_ACCTD_DR','','','','MT063505','NUMBER','','','Line Acctd Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LLINE',101,'Lline','LLINE','','','','MT063505','NUMBER','','','Lline','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','HNUMBER',101,'Hnumber','HNUMBER','','','','MT063505','NUMBER','','','Hnumber','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LSEQUENCE',101,'Lsequence','LSEQUENCE','','','','MT063505','VARCHAR2','','','Lsequence','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','JE_HEADER_ID',101,'Je Header Id','JE_HEADER_ID','','','','MT063505','NUMBER','','','Je Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#SUBACCOUNT#DESCR',101,'Gcc#50368#Subaccount#Descr','GCC#50368#SUBACCOUNT#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50368#Subaccount#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#SUBACCOUNT',101,'Gcc#50368#Subaccount','GCC#50368#SUBACCOUNT','','','','MT063505','VARCHAR2','','','Gcc#50368#Subaccount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#PRODUCT#DESCR',101,'Gcc#50368#Product#Descr','GCC#50368#PRODUCT#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50368#Product#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#PRODUCT',101,'Gcc#50368#Product','GCC#50368#PRODUCT','','','','MT063505','VARCHAR2','','','Gcc#50368#Product','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#FUTURE_USE#DESCR',101,'Gcc#50368#Future Use#Descr','GCC#50368#FUTURE_USE#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50368#Future Use#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','BATCH',101,'Batch','BATCH','','','','MT063505','NUMBER','','','Batch','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#FURTURE_USE',101,'Gcc#50328#Furture Use','GCC#50328#FURTURE_USE','','','','MT063505','VARCHAR2','','','Gcc#50328#Furture Use','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#FURTURE_USE#DESCR',101,'Gcc#50328#Furture Use#Descr','GCC#50328#FURTURE_USE#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50328#Furture Use#Descr','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ASSOCIATE_NUM',101,'Associate Num','ASSOCIATE_NUM','','','','MT063505','VARCHAR2','','','Associate Num','','','','');
--Inserting Object Components for EIS_XXHDS_GL_SL_180_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_GL_SL_180_V','GL_JE_LINES',101,'GL_JE_LINES','GJL','GJL','MT063505','MT063505','-1','Journal Entry Lines','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_GL_SL_180_V','GL_JE_HEADERS',101,'GL_JE_HEADERS','GJH','GJH','MT063505','MT063505','-1','Journal Entry Headers','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_GL_SL_180_V','GL_CODE_COMBINATIONS',101,'GL_CODE_COMBINATIONS','GCC','GCC','MT063505','MT063505','140892295','Account Combinations','Y','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_GL_SL_180_V','GL_LEDGERS',101,'GL_LEDGERS','GL','GL','MT063505','MT063505','-1','Ledger Definition','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_GL_SL_180_V','GL_PERIOD_STATUSES',101,'GL_PERIOD_STATUSES','GPS','GPS','MT063505','MT063505','-1','Calendar Period Statuses','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXHDS_GL_SL_180_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_JE_LINES','GJL',101,'EXGS_V.JE_HEADER_ID','=','GJL.JE_HEADER_ID(+)','','','','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_JE_LINES','GJL',101,'EXGS_V.JE_LINE_NUM','=','GJL.JE_LINE_NUM(+)','','','','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_JE_HEADERS','GJH',101,'EXGS_V.JE_HEADER_ID','=','GJH.JE_HEADER_ID(+)','','','','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_CODE_COMBINATIONS','GCC',101,'EXGS_V.CODE_COMBINATION_ID','=','GCC.CODE_COMBINATION_ID(+)','','','1','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_LEDGERS','GL',101,'EXGS_V.NAME','=','GL.NAME(+)','','','','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_PERIOD_STATUSES','GPS',101,'EXGS_V.GPS_APPLICATION_ID','=','GPS.APPLICATION_ID(+)','','','','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_PERIOD_STATUSES','GPS',101,'EXGS_V.GPS_SET_OF_BOOKS_ID','=','GPS.SET_OF_BOOKS_ID(+)','','','','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_PERIOD_STATUSES','GPS',101,'EXGS_V.GPS_PERIOD_NAME','=','GPS.PERIOD_NAME(+)','','','','Y','MT063505');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report LOV Data for HDS-Subledget Detail report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS-Subledget Detail report
xxeis.eis_rsc_ins.lov( 101,'SELECT  CONCATENATED_SEGMENTS  
FROM   gl_code_combinations_kfv glcc 
WHERE  chart_of_accounts_id IN (                
SELECT led.chart_of_accounts_id                  
FROM gl_ledgers led                 
WHERE GL_SECURITY_PKG.VALIDATE_ACCESS(LED.LEDGER_ID) = ''TRUE'')   
and nvl(summary_flag,''N'') = upper(''N'')
order by CONCATENATED_SEGMENTS','','EIS_GLACCOUNT_CONCAT_LOV','EIS_GLACCOUNT_CONCAT_LOV','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 101,'SELECT distinct user_je_source_name FROM gl_je_sources','','EIS_GL_JE_SOURCES_LOV','LOV of all Available Journal Sources','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 101,'SELECT distinct user_je_category_name FROM gl_je_categories','','EIS_GL_JE_CATEGORY_LOV','LOV of all available Journal Categories','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 101,'SELECT LED.NAME ledger_name
from           
GL_ACCESS_SET_LEDGERS AC,           
GL_LEDGERS                      LED
WHERE AC.ACCESS_SET_ID          = FND_PROFILE.VALUE(''GL_ACCESS_SET_ID'')
AND LED.LEDGER_ID                     = AC.LEDGER_ID
AND LED.OBJECT_TYPE_CODE       = ''L''
UNION
SELECT LED.NAME
FROM   GL_LEDGERS LED
WHERE  gl_security_pkg.validate_access(LED.ledger_id) = ''TRUE'' ','','EIS_GL_LEDGERS','Ledger and ledger sets of an access set.','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 101,'select currency_code
from fnd_currencies
WHERE            
(CURRENCY_CODE in (SELECT CURRENCY_CODE                                 
FROM GL_LEDGERS                                
WHERE apps.GL_SECURITY_PKG.VALIDATE_ACCESS(LEDGER_ID) = ''TRUE''                               
)              
OR               
CURRENCY_CODE IN                              
(SELECT TARGET_CURRENCY                                 
FROM GL_TRANSLATION_TRACKING                                
WHERE apps.GL_SECURITY_PKG.VALIDATE_ACCESS(LEDGER_ID) = ''TRUE''                                   
OR CURRENCY_CODE = ''STAT''                               
)          
)
ORDER BY CURRENCY_CODE','','EIS_GL_CURRENCY_CODES_LOV','LEDGER CURRENCY, STAT, plus all currencies that have been used in translation','XXEIS_RS_ADMIN',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 101,'SELECT  GPS.PERIOD_NAME period_name, LED.NAME LEDGER_NAME, GPS.PERIOD_YEAR , GPS.PERIOD_NUM, GPS.START_DATE, GPS.END_DATE
FROM    GL_PERIOD_STATUSES GPS,             
GL_LEDGERS LED
WHERE  GPS.CLOSING_STATUS IN (''O'',''C'',''P'')
AND       GPS.APPLICATION_ID = 101
AND       LED.LEDGER_ID          = GPS.SET_OF_BOOKS_ID
AND       GL_SECURITY_PKG.VALIDATE_ACCESS(LED.LEDGER_ID) = ''TRUE''
ORDER BY PERIOD_YEAR DESC, PERIOD_NUM DESC','','EIS_GL_PERIOD_NAMES_LOV','Periods with closing_status ''O'',''P'' and ''C'' from gl_period_statuses','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report Data for HDS-Subledget Detail report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS-Subledget Detail report
xxeis.eis_rsc_utility.delete_report_rows( 'HDS-Subledget Detail report' );
--Inserting Report - HDS-Subledget Detail report
xxeis.eis_rsc_ins.r( 101,'HDS-Subledget Detail report','','Enables review of the details of subledger activity that has been posted to General Ledger
accounts. The report displays detail amounts for a specific journal source and category, in
functional currency or STAT.

This report prints the journal entry lines and beginning and ending balances of the accounts requested. For each journal entry line, the report prints the accounting date, category, journal batch name, header, sequence, number, line, description, and amount. For each journal entry line, the report also prints subledger details, including the vendor or customer name, transaction number, associated transaction, sequence, number, line, and transaction type.','','','','XXEIS_RS_ADMIN','EIS_XXHDS_GL_SL_180_V','Y','','','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,EXCEL,Pivot Excel,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - HDS-Subledget Detail report
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'BATCH','Batch','Batch','','~~~','default','','12','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'LINE_ACCTD_CR','Line Acctd Cr','Line Acctd Cr','','~~~','default','','15','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'CUSTOMER_OR_VENDOR','Customer Or Vendor','Customer Or Vendor','','','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'H_SEQ_ID','H Seq Id','H Seq Id','','~~~','default','','22','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'SLA_DIST_ACCOUNTED_NET','Sla Dist Accounted Net','Sla Dist Accounted Net','','~~~','default','','42','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'SLA_DIST_ENTERED_NET','Sla Dist Entered Net','Sla Dist Entered Net','','~~~','default','','43','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'SLA_LINE_ACCOUNTED_NET','Sla Line Accounted Net','Sla Line Accounted Net','','~~~','default','','44','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'SLA_LINE_ENTERED_NET','Sla Line Entered Net','Sla Line Entered Net','','~~~','default','','45','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'SLA_DIST_ACCOUNTED_CR','Sla Dist Accounted Cr','Sla Dist Accounted Cr','','~~~','default','','29','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'SLA_DIST_ACCOUNTED_DR','Sla Dist Accounted Dr','Sla Dist Accounted Dr','','~~~','default','','30','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'SLA_DIST_ENTERED_CR','Sla Dist Entered Cr','Sla Dist Entered Cr','','~~~','default','','31','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'SLA_DIST_ENTERED_DR','Sla Dist Entered Dr','Sla Dist Entered Dr','','~~~','default','','32','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'SLA_LINE_ACCOUNTED_CR','Sla Line Accounted Cr','Sla Line Accounted Cr','','~~~','default','','33','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'SLA_LINE_ACCOUNTED_DR','Sla Line Accounted Dr','Sla Line Accounted Dr','','~~~','default','','34','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'SLA_LINE_ENTERED_CR','Sla Line Entered Cr','Sla Line Entered Cr','','~~~','default','','35','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'SLA_LINE_ENTERED_DR','Sla Line Entered Dr','Sla Line Entered Dr','','~~~','default','','36','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'ACCOUNTED_CR','Accounted Cr','Accounted Cr','','~~~','default','','37','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'ACCOUNTED_DR','Accounted Dr','Accounted Dr','','~~~','default','','38','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'ENTERED_CR','Entered Cr','Entered Cr','','~~~','default','','39','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'ENTERED_DR','Entered Dr','Entered Dr','','~~~','default','','40','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'JE_HEADER_ID','Je Header Id','Je Header Id','','~~~','default','','23','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'JE_LINE_NUM','Je Line Num','Je Line Num','','~~~','default','','24','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'LLINE','Lline','Lline','','~~~','default','','25','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'LSEQUENCE','Lsequence','Lsequence','','','default','','26','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'SEQ_ID','Seq Id','Seq Id','','~~~','default','','27','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'SEQ_NUM','Seq Num','Seq Num','','~~~','default','','28','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'SOURCE','Source','Source','','','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'TRANSACTION_NUM','Transaction Num','Transaction Num','','','default','','7','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'TYPE','Type','Type','','','default','','9','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'GL_ACCOUNT_STRING','Gl Account String','Gl Account String','','','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'NAME','Name','Name','','','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'LINE_ACCTD_DR','Line Acctd Dr','Line Acctd Dr','','~~~','default','','16','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'LINE_ENT_CR','Line Ent Cr','Line Ent Cr','','~~~','default','','17','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'LINE_ENT_DR','Line Ent Dr','Line Ent Dr','','~~~','default','','18','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'GL_SL_LINK_ID','Gl Sl Link Id','Gl Sl Link Id','','~~~','default','','20','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'HNUMBER','Hnumber','Hnumber','','~~~','default','','21','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'BATCH_NAME','Batch Name','Batch Name','','','default','','41','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'JE_CATEGORY','Je Category','Je Category','','','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'PERIOD_NAME','Period Name','Period Name','','','default','','5','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'CURRENCY_CODE','Currency Code','Currency Code','','','default','','10','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'EFFECTIVE_PERIOD_NUM','Effective Period Num','Effective Period Num','','~~~','default','','14','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'ENTRY','Entry','Entry','','','default','','19','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'LINE_DESCR','Line Descr','Line Descr','','','default','','13','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'ACC_DATE','Acc Date','Acc Date','','','default','','11','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'ASSOCIATE_NUM','Associate Num','Associate Num','','','default','','8','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'GCC#50328#ACCOUNT','GCC#Account','Gcc#50328#Account','','','default','','46','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'GCC#50328#COST_CENTER','GCC#Cost Center','Gcc#50328#Cost Center','','','default','','47','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'GCC#50328#FURTURE_USE','GCC#Furture Use','Gcc#50328#Furture Use','','','default','','48','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'GCC#50328#FUTURE_USE_2','GCC#Future Use 2','Gcc#50328#Future Use 2','','','default','','49','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'GCC#50328#LOCATION','GCC#Location','Gcc#50328#Location','','','default','','50','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'GCC#50328#PRODUCT','GCC#Product','Gcc#50328#Product','','','default','','51','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Subledget Detail report',101,'GCC#50328#PROJECT_CODE','GCC#Project Code','Gcc#50328#Project Code','','','default','','52','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','');
--Inserting Report Parameters - HDS-Subledget Detail report
xxeis.eis_rsc_ins.rp( 'HDS-Subledget Detail report',101,'Currency','Currency','CURRENCY_CODE','IN','EIS_GL_CURRENCY_CODES_LOV','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Subledget Detail report',101,'Starting Period','Starting Period','EFFECTIVE_PERIOD_NUM','>=','EIS_GL_PERIOD_NAMES_LOV','','VARCHAR2','Y','Y','3','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Subledget Detail report',101,'Ending Period','Ending Period','EFFECTIVE_PERIOD_NUM','<=','EIS_GL_PERIOD_NAMES_LOV','','VARCHAR2','Y','Y','4','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Subledget Detail report',101,'Journal Source','Journal Source','SOURCE','IN','EIS_GL_JE_SOURCES_LOV','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Subledget Detail report',101,'Journal Category','Journal Category','JE_CATEGORY','IN','EIS_GL_JE_CATEGORY_LOV','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Subledget Detail report',101,'GL account string','GL account string','GL_ACCOUNT_STRING','IN','EIS_GLACCOUNT_CONCAT_LOV','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Subledget Detail report',101,'Ledger name','ledger name','NAME','IN','EIS_GL_LEDGERS','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Subledget Detail report',101,'GCC#Account','Accounting Flexfield : Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#ACCOUNT','IN','','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Subledget Detail report',101,'GCC#Cost Center','Accounting Flexfield : Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#COST_CENTER','IN','','','VARCHAR2','N','Y','9','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Subledget Detail report',101,'GCC#Furture Use','Accounting Flexfield : Segment ''Furture Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FURTURE_USE','IN','','','VARCHAR2','N','Y','10','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Subledget Detail report',101,'GCC#Future Use 2','Accounting Flexfield : Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE_2','IN','','','VARCHAR2','N','Y','11','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Subledget Detail report',101,'GCC#Location','Accounting Flexfield : Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#LOCATION','IN','','','VARCHAR2','N','Y','12','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Subledget Detail report',101,'GCC#Product','Accounting Flexfield : Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PRODUCT','IN','','','VARCHAR2','N','Y','13','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Subledget Detail report',101,'GCC#Project Code','Accounting Flexfield : Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PROJECT_CODE','IN','','','VARCHAR2','N','Y','14','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
--Inserting Dependent Parameters - HDS-Subledget Detail report
--Inserting Report Conditions - HDS-Subledget Detail report
xxeis.eis_rsc_ins.rcnh( 'HDS-Subledget Detail report',101,'CURRENCY_CODE IN :Currency ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CURRENCY_CODE','','Currency','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Subledget Detail report','CURRENCY_CODE IN :Currency ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Subledget Detail report',101,'EFFECTIVE_PERIOD_NUM <= xxeis.EIS_RS_GL_FIN_COM_UTIL_PKG.eis_get_period_num(:Ending Period) ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','EFFECTIVE_PERIOD_NUM','','','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','LESS_THAN_EQUALS','Y','Y','','xxeis.EIS_RS_GL_FIN_COM_UTIL_PKG.eis_get_period_num(:Ending Period)','','','1',101,'HDS-Subledget Detail report','EFFECTIVE_PERIOD_NUM <= xxeis.EIS_RS_GL_FIN_COM_UTIL_PKG.eis_get_period_num(:Ending Period) ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Subledget Detail report',101,'EFFECTIVE_PERIOD_NUM >= xxeis.EIS_RS_GL_FIN_COM_UTIL_PKG.eis_get_period_num(:Starting Period) ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','EFFECTIVE_PERIOD_NUM','','','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','xxeis.EIS_RS_GL_FIN_COM_UTIL_PKG.eis_get_period_num(:Starting Period)','','','1',101,'HDS-Subledget Detail report','EFFECTIVE_PERIOD_NUM >= xxeis.EIS_RS_GL_FIN_COM_UTIL_PKG.eis_get_period_num(:Starting Period) ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Subledget Detail report',101,'GCC#50328#COST_CENTER IN :GCC#Cost Center ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GCC#50328#COST_CENTER','','GCC#Cost Center','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Subledget Detail report','GCC#50328#COST_CENTER IN :GCC#Cost Center ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Subledget Detail report',101,'GCC#50328#FURTURE_USE IN :GCC#Furture Use ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GCC#50328#FURTURE_USE','','GCC#Furture Use','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Subledget Detail report','GCC#50328#FURTURE_USE IN :GCC#Furture Use ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Subledget Detail report',101,'GCC#50328#FUTURE_USE_2 IN :GCC#Future Use 2 ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GCC#50328#FUTURE_USE_2','','GCC#Future Use 2','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Subledget Detail report','GCC#50328#FUTURE_USE_2 IN :GCC#Future Use 2 ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Subledget Detail report',101,'GCC#50328#LOCATION IN :GCC#Location ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GCC#50328#LOCATION','','GCC#Location','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Subledget Detail report','GCC#50328#LOCATION IN :GCC#Location ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Subledget Detail report',101,'GCC#50328#PRODUCT IN :GCC#Product ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GCC#50328#PRODUCT','','GCC#Product','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Subledget Detail report','GCC#50328#PRODUCT IN :GCC#Product ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Subledget Detail report',101,'GCC#50328#PROJECT_CODE IN :GCC#Project Code ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GCC#50328#PROJECT_CODE','','GCC#Project Code','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Subledget Detail report','GCC#50328#PROJECT_CODE IN :GCC#Project Code ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Subledget Detail report',101,'GL_ACCOUNT_STRING IN :GL account string ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_ACCOUNT_STRING','','GL account string','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Subledget Detail report','GL_ACCOUNT_STRING IN :GL account string ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Subledget Detail report',101,'JE_CATEGORY IN :Journal Category ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','JE_CATEGORY','','Journal Category','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Subledget Detail report','JE_CATEGORY IN :Journal Category ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Subledget Detail report',101,'NAME IN :Ledger name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','NAME','','Ledger name','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Subledget Detail report','NAME IN :Ledger name ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Subledget Detail report',101,'SOURCE IN :Journal Source ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SOURCE','','Journal Source','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Subledget Detail report','SOURCE IN :Journal Source ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Subledget Detail report',101,'EGS1V.GCC#50328#ACCOUNT IN GCC#Account','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GCC#50328#ACCOUNT','','GCC#Account','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Subledget Detail report','EGS1V.GCC#50328#ACCOUNT IN GCC#Account');
--Inserting Report Sorts - HDS-Subledget Detail report
--Inserting Report Triggers - HDS-Subledget Detail report
--inserting report templates - HDS-Subledget Detail report
xxeis.eis_rsc_ins.r_tem( 'HDS-Subledget Detail report','HDS-Subledget Detail report','Seeded template for HDS-Subledget Detail report','','','','','','','','','','','HDS-Subledget Detail report.rtf','XXEIS_RS_ADMIN','X','','','Y','Y','','');
--Inserting Report Portals - HDS-Subledget Detail report
--inserting report dashboards - HDS-Subledget Detail report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS-Subledget Detail report','101','EIS_XXHDS_GL_SL_180_V','EIS_XXHDS_GL_SL_180_V','N','');
--inserting report security - HDS-Subledget Detail report
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','XXCUS_GL_ACCOUNTANT_CAD',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','XXCUS_GL_ACCOUNTANT_CAD_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','XXCUS_GL_ACCOUNTANT_GLOBAL',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','GNRL_LDGR_LTMR_ACCNTNT',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','XXCUS_GL_ACCOUNTANT_USD',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','XXCUS_GL_ACCOUNTANT_USD_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','XXCUS_GL_INQUIRY',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','HDS GL INQUIRY',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','GNRL_LDGR_LTMR_NQR',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','XXCUS_GL_INQUIRY_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','XXCUS_GL_MANAGER',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','XXCUS_GL_MANAGER_GLOBAL',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','XXCUS_GL_MANAGER_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','HDS_CAD_MNTH_END_PROCS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','GNRL_LDGR_FSS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','GNRL_LDGR_LTMR_FSS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','HDS_GNRL_LDGR_CAD',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','HDS_GNRL_LDGR_SPR_USR',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','20005','','XXWC_VIEW_ALL_EIS_REPORTS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','XXCUS_GL_MANAGER_PS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','XXCUS_GL_INQUIRY_PS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','XXCUS_GL_ACCOUNTANT_USD_PS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Subledget Detail report','101','','XXCUS_GL_ACCOUNTANT_CAD_PS',101,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS-Subledget Detail report
xxeis.eis_rsc_ins.rpivot( 'HDS-Subledget Detail report',101,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS-Subledget Detail report',101,'Pivot','SLA_DIST_ACCOUNTED_NET','DATA_FIELD','SUM','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS-Subledget Detail report',101,'Pivot','SLA_DIST_ENTERED_NET','DATA_FIELD','SUM','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS-Subledget Detail report',101,'Pivot','SLA_DIST_ACCOUNTED_CR','DATA_FIELD','SUM','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS-Subledget Detail report',101,'Pivot','SLA_DIST_ACCOUNTED_DR','DATA_FIELD','SUM','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS-Subledget Detail report',101,'Pivot','SLA_DIST_ENTERED_CR','DATA_FIELD','SUM','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS-Subledget Detail report',101,'Pivot','SLA_DIST_ENTERED_DR','DATA_FIELD','SUM','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS-Subledget Detail report',101,'Pivot','GL_ACCOUNT_STRING','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS-Subledget Detail report',101,'Pivot','NAME','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS-Subledget Detail report',101,'Pivot','BATCH_NAME','ROW_FIELD','','','4','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS-Subledget Detail report',101,'Pivot','PERIOD_NAME','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS-Subledget Detail report',101,'Pivot','CURRENCY_CODE','ROW_FIELD','','','5','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- HDS-Subledget Detail report
xxeis.eis_rsc_ins.rv( 'HDS-Subledget Detail report','','HDS-Subledget Detail report','MT063505');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
