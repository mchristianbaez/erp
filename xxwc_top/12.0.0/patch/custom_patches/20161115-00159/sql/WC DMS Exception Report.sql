--Report Name            : WC DMS Exception Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_DMS_SHIP_CONFIRM_VW
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_DMS_SHIP_CONFIRM_VW
xxeis.eis_rsc_ins.v( 'EIS_XXWC_DMS_SHIP_CONFIRM_VW',660,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Dms Ship Confirm Vw','EXDSCV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_DMS_SHIP_CONFIRM_VW
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_DMS_SHIP_CONFIRM_VW',660,FALSE);
--Inserting Object Columns for EIS_XXWC_DMS_SHIP_CONFIRM_VW
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_SHIP_CONFIRM_VW','CREATION_DATE',660,'Creation Date','CREATION_DATE','','','','ANONYMOUS','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_SHIP_CONFIRM_VW','SHIP_CONFIRM_EXCEPTION',660,'Ship Confirm Exception','SHIP_CONFIRM_EXCEPTION','','','','ANONYMOUS','VARCHAR2','','','Ship Confirm Exception','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_SHIP_CONFIRM_VW','SHIP_CONFIRM_STATUS',660,'Ship Confirm Status','SHIP_CONFIRM_STATUS','','','','ANONYMOUS','VARCHAR2','','','Ship Confirm Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_SHIP_CONFIRM_VW','SIGNED_BY',660,'Signed By','SIGNED_BY','','','','ANONYMOUS','VARCHAR2','','','Signed By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_SHIP_CONFIRM_VW','BRANCH',660,'Branch','BRANCH','','','','ANONYMOUS','VARCHAR2','','','Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_SHIP_CONFIRM_VW','DELIVERY_ID',660,'Delivery Id','DELIVERY_ID','','','','ANONYMOUS','NUMBER','','','Delivery Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_SHIP_CONFIRM_VW','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','ANONYMOUS','NUMBER','','','Order Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_SHIP_CONFIRM_VW','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','ANONYMOUS','VARCHAR2','','','Customer Name','','','','US');
--Inserting Object Components for EIS_XXWC_DMS_SHIP_CONFIRM_VW
--Inserting Object Component Joins for EIS_XXWC_DMS_SHIP_CONFIRM_VW
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for WC DMS Exception Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - WC DMS Exception Report
xxeis.eis_rsc_ins.lov( '','SELECT organization_code warehouse, organization_name
  FROM org_organization_definitions ood
 WHERE    1=1
 AND SYSDATE < NVL (ood.disable_date, SYSDATE + 1)
       AND EXISTS
              (SELECT 1
                 FROM xxeis.eis_org_access_v
                WHERE organization_id = ood.organization_id)
union
SELECT ''All Organizations'' organization_code, ''All'' organization_name  FROM dual','','EIS XXWC ORG CODE','To get Organization Code','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for WC DMS Exception Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC DMS Exception Report
xxeis.eis_rsc_utility.delete_report_rows( 'WC DMS Exception Report' );
--Inserting Report - WC DMS Exception Report
xxeis.eis_rsc_ins.r( 660,'WC DMS Exception Report','','','','','','MR020532','EIS_XXWC_DMS_SHIP_CONFIRM_VW','Y','','','MR020532','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - WC DMS Exception Report
xxeis.eis_rsc_ins.rc( 'WC DMS Exception Report',660,'BRANCH','Branch','Branch','','','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DMS_SHIP_CONFIRM_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC DMS Exception Report',660,'CREATION_DATE','Creation Date','Creation Date','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DMS_SHIP_CONFIRM_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC DMS Exception Report',660,'DELIVERY_ID','Delivery Id','Delivery Id','','~T~D~0','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DMS_SHIP_CONFIRM_VW','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC DMS Exception Report',660,'ORDER_NUMBER','Order Number','Order Number','','~T~D~0','default','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DMS_SHIP_CONFIRM_VW','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC DMS Exception Report',660,'SHIP_CONFIRM_EXCEPTION','Ship Confirm Exception','Ship Confirm Exception','','','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DMS_SHIP_CONFIRM_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC DMS Exception Report',660,'SHIP_CONFIRM_STATUS','Ship Confirm Status','Ship Confirm Status','','','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DMS_SHIP_CONFIRM_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC DMS Exception Report',660,'SIGNED_BY','Signed By','Signed By','','','default','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DMS_SHIP_CONFIRM_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC DMS Exception Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DMS_SHIP_CONFIRM_VW','','','GROUP_BY','US','');
--Inserting Report Parameters - WC DMS Exception Report
xxeis.eis_rsc_ins.rp( 'WC DMS Exception Report',660,'Branch','Branch Name','BRANCH','IN','EIS XXWC ORG CODE','','VARCHAR2','N','Y','1','N','N','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_DMS_SHIP_CONFIRM_VW','','','US','');
xxeis.eis_rsc_ins.rp( 'WC DMS Exception Report',660,'Date From','Date From','CREATION_DATE','>=','','','DATE','Y','Y','2','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_DMS_SHIP_CONFIRM_VW','','','US','');
xxeis.eis_rsc_ins.rp( 'WC DMS Exception Report',660,'Date To','Date To','CREATION_DATE','<=','','','DATE','Y','Y','3','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_DMS_SHIP_CONFIRM_VW','','','US','');
--Inserting Dependent Parameters - WC DMS Exception Report
--Inserting Report Conditions - WC DMS Exception Report
xxeis.eis_rsc_ins.rcnh( 'WC DMS Exception Report',660,'CREATION_DATE >= :Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Date From','','','','','EIS_XXWC_DMS_SHIP_CONFIRM_VW','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',660,'WC DMS Exception Report','CREATION_DATE >= :Date From ');
xxeis.eis_rsc_ins.rcnh( 'WC DMS Exception Report',660,'CREATION_DATE <= :Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Date To','','','','','EIS_XXWC_DMS_SHIP_CONFIRM_VW','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',660,'WC DMS Exception Report','CREATION_DATE <= :Date To ');
xxeis.eis_rsc_ins.rcnh( 'WC DMS Exception Report',660,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','and ( ''All Organizations'' in NVL((:Branch), ''All Organizations'' ) or (Branch in (:Branch)))','1',660,'WC DMS Exception Report','Free Text ');
--Inserting Report Sorts - WC DMS Exception Report
--Inserting Report Triggers - WC DMS Exception Report
--inserting report templates - WC DMS Exception Report
--Inserting Report Portals - WC DMS Exception Report
--inserting report dashboards - WC DMS Exception Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC DMS Exception Report','660','EIS_XXWC_DMS_SHIP_CONFIRM_VW','EIS_XXWC_DMS_SHIP_CONFIRM_VW','N','');
--inserting report security - WC DMS Exception Report
xxeis.eis_rsc_ins.rsec( 'WC DMS Exception Report','660','','',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'WC DMS Exception Report','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'WC DMS Exception Report','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'WC DMS Exception Report','660','','XXWC_ORDER_MGMT_SUPER_USER',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'WC DMS Exception Report','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'WC DMS Exception Report','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'WC DMS Exception Report','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'WC DMS Exception Report','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'WC DMS Exception Report','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'WC DMS Exception Report','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'WC DMS Exception Report','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'MR020532','','','');
--Inserting Report Pivots - WC DMS Exception Report
--Inserting Report   Version details- WC DMS Exception Report
xxeis.eis_rsc_ins.rv( 'WC DMS Exception Report','','WC DMS Exception Report','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
