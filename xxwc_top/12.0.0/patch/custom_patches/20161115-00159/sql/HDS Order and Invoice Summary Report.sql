--Report Name            : HDS Order and Invoice Summary Report
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_XXWC_OM_INV_ORDER_DETAIL_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_INV_ORDER_DETAIL_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V',660,'','','','','MT063505','XXEIS','Eis Xxwc Om Inv Order Detail V','EXOIODV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_OM_INV_ORDER_DETAIL_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_INV_ORDER_DETAIL_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_INV_ORDER_DETAIL_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','AGREEMENT_ID',660,'Agreement Id','AGREEMENT_ID','','','','MT063505','NUMBER','','','Agreement Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIPMENT_NUMBER',660,'Shipment Number','SHIPMENT_NUMBER','','','','MT063505','NUMBER','','','Shipment Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','PRICE_LIST_ID',660,'Price List Id','PRICE_LIST_ID','','','','MT063505','NUMBER','','','Price List Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SOURCE_TYPE_CODE',660,'Source Type Code','SOURCE_TYPE_CODE','','','','MT063505','VARCHAR2','','','Source Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','DELIVER_TO_ORG_ID',660,'Deliver To Org Id','DELIVER_TO_ORG_ID','','','','MT063505','NUMBER','','','Deliver To Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIP_TO_ORG_ID',660,'Ship To Org Id','SHIP_TO_ORG_ID','','','','MT063505','NUMBER','','','Ship To Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIP_FROM_ORG_ID',660,'Ship From Org Id','SHIP_FROM_ORG_ID','','','','MT063505','NUMBER','','','Ship From Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SOLD_TO_ORG_ID',660,'Sold To Org Id','SOLD_TO_ORG_ID','','','','MT063505','NUMBER','','','Sold To Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIPPING_QUANTITY_UOM',660,'Shipping Quantity Uom','SHIPPING_QUANTITY_UOM','','','','MT063505','VARCHAR2','','','Shipping Quantity Uom','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIPPING_QUANTITY',660,'Shipping Quantity','SHIPPING_QUANTITY','','','','MT063505','NUMBER','','','Shipping Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','FULFILLED_QUANTITY',660,'Fulfilled Quantity','FULFILLED_QUANTITY','','','','MT063505','NUMBER','','','Fulfilled Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ORDERED_QUANTITY',660,'Ordered Quantity','ORDERED_QUANTITY','','','','MT063505','NUMBER','','','Ordered Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIPPED_QUANTITY',660,'Shipped Quantity','SHIPPED_QUANTITY','','','','MT063505','NUMBER','','','Shipped Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','CANCELLED_QUANTITY',660,'Cancelled Quantity','CANCELLED_QUANTITY','','','','MT063505','NUMBER','','','Cancelled Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ORDER_QUANTITY_UOM',660,'Order Quantity Uom','ORDER_QUANTITY_UOM','','','','MT063505','VARCHAR2','','','Order Quantity Uom','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ORDERED_ITEM',660,'Ordered Item','ORDERED_ITEM','','','','MT063505','VARCHAR2','','','Ordered Item','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','INVOICE_CREATION_DATE',660,'Invoice Creation Date','INVOICE_CREATION_DATE','','','','MT063505','DATE','','','Invoice Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ORDER_LINE_CHARGE_COST',660,'Order Line Charge Cost','ORDER_LINE_CHARGE_COST','','','','MT063505','NUMBER','','','Order Line Charge Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ORDER_LINE_CHARGE',660,'Order Line Charge','ORDER_LINE_CHARGE','','','','MT063505','VARCHAR2','','','Order Line Charge','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','INVOICE_LINE_TAX_REV_AMNT',660,'Invoice Line Tax Rev Amnt','INVOICE_LINE_TAX_REV_AMNT','','','','MT063505','NUMBER','','','Invoice Line Tax Rev Amnt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','INVOICE_LINE_TAX_EXT_AMNT',660,'Invoice Line Tax Ext Amnt','INVOICE_LINE_TAX_EXT_AMNT','','','','MT063505','NUMBER','','','Invoice Line Tax Ext Amnt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','INVOICE_LINE_REVENUE_AMOUNT',660,'Invoice Line Revenue Amount','INVOICE_LINE_REVENUE_AMOUNT','','','','MT063505','NUMBER','','','Invoice Line Revenue Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','INVOICE_LINE_EXTENDED_AMOUNT',660,'Invoice Line Extended Amount','INVOICE_LINE_EXTENDED_AMOUNT','','','','MT063505','NUMBER','','','Invoice Line Extended Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','INTERFACE_LINE_CONTEXT',660,'Interface Line Context','INTERFACE_LINE_CONTEXT','','','','MT063505','VARCHAR2','','','Interface Line Context','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','RECEIVABLE_LINE_TYPE',660,'Receivable Line Type','RECEIVABLE_LINE_TYPE','','','','MT063505','VARCHAR2','','','Receivable Line Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','RECEIVABLE_INV_QUANTITY',660,'Receivable Inv Quantity','RECEIVABLE_INV_QUANTITY','','','','MT063505','NUMBER','','','Receivable Inv Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','QUANTITY_CREDITED',660,'Quantity Credited','QUANTITY_CREDITED','','','','MT063505','NUMBER','','','Quantity Credited','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','QUANTITY_ORDERED',660,'Quantity Ordered','QUANTITY_ORDERED','','','','MT063505','NUMBER','','','Quantity Ordered','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','REJECT_REASON_CODE',660,'Reject Reason Code','REJECT_REASON_CODE','','','','MT063505','VARCHAR2','','','Reject Reason Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','OPEN_FLAG',660,'Open Flag','OPEN_FLAG','','','','MT063505','VARCHAR2','','','Open Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SALES_ORDER',660,'Sales Order','SALES_ORDER','','','','MT063505','VARCHAR2','','','Sales Order','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','TRANSACTION_DESCRIPTION',660,'Transaction Description','TRANSACTION_DESCRIPTION','','','','MT063505','VARCHAR2','','','Transaction Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','TRANSACTION_NAME',660,'Transaction Name','TRANSACTION_NAME','','','','MT063505','VARCHAR2','','','Transaction Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','CREDIT_INVOICE_NUMBER',660,'Credit Invoice Number','CREDIT_INVOICE_NUMBER','','','','MT063505','VARCHAR2','','','Credit Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','EXTENDED_PRICE',660,'Extended Price','EXTENDED_PRICE','','','','MT063505','NUMBER','','','Extended Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','MT063505','VARCHAR2','','','Customer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','TRX_LINE_NUMBER',660,'Trx Line Number','TRX_LINE_NUMBER','','','','MT063505','NUMBER','','','Trx Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','MT063505','DATE','','','Ordered Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','MT063505','VARCHAR2','','','Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','MT063505','NUMBER','','','Order Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIP_SITE_USE_ID',660,'Ship Site Use Id','SHIP_SITE_USE_ID','','','','MT063505','NUMBER','','','Ship Site Use Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIPPING_ORG_ID',660,'Shipping Org Id','SHIPPING_ORG_ID','','','','MT063505','NUMBER','','','Shipping Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','OPERATING_UNIT',660,'Operating Unit','OPERATING_UNIT','','','','MT063505','VARCHAR2','','','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','MT063505','NUMBER','','','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','RETURN_REASON_CODE',660,'Return Reason Code','RETURN_REASON_CODE','','','','MT063505','VARCHAR2','','','Return Reason Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','UNIT_SELLING_PRICE_PER_PQTY',660,'Unit Selling Price Per Pqty','UNIT_SELLING_PRICE_PER_PQTY','','','','MT063505','NUMBER','','','Unit Selling Price Per Pqty','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','UPGRADED_FLAG',660,'Upgraded Flag','UPGRADED_FLAG','','','','MT063505','VARCHAR2','','','Upgraded Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','FULFILLED_QUANTITY2',660,'Fulfilled Quantity2','FULFILLED_QUANTITY2','','','','MT063505','NUMBER','','','Fulfilled Quantity2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIPPING_QUANTITY_UOM2',660,'Shipping Quantity Uom2','SHIPPING_QUANTITY_UOM2','','','','MT063505','VARCHAR2','','','Shipping Quantity Uom2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIPPING_QUANTITY2',660,'Shipping Quantity2','SHIPPING_QUANTITY2','','','','MT063505','NUMBER','','','Shipping Quantity2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','CANCELLED_QUANTITY2',660,'Cancelled Quantity2','CANCELLED_QUANTITY2','','','','MT063505','NUMBER','','','Cancelled Quantity2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIPPED_QUANTITY2',660,'Shipped Quantity2','SHIPPED_QUANTITY2','','','','MT063505','NUMBER','','','Shipped Quantity2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ORDERED_QUANTITY_UOM2',660,'Ordered Quantity Uom2','ORDERED_QUANTITY_UOM2','','','','MT063505','VARCHAR2','','','Ordered Quantity Uom2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ORDERED_QUANTITY2',660,'Ordered Quantity2','ORDERED_QUANTITY2','','','','MT063505','NUMBER','','','Ordered Quantity2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','PREFERRED_GRADE',660,'Preferred Grade','PREFERRED_GRADE','','','','MT063505','VARCHAR2','','','Preferred Grade','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','FULFILLMENT_DATE',660,'Fulfillment Date','FULFILLMENT_DATE','','','','MT063505','DATE','','','Fulfillment Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ORDER_HEADER_STATUS',660,'Order Header Status','ORDER_HEADER_STATUS','','','','MT063505','VARCHAR2','','','Order Header Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ORDER_LINE_STATUS',660,'Order Line Status','ORDER_LINE_STATUS','','','','MT063505','VARCHAR2','','','Order Line Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','INVOICED_QUANTITY',660,'Invoiced Quantity','INVOICED_QUANTITY','','','','MT063505','NUMBER','','','Invoiced Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','BILL_TO_CONTACT',660,'Bill To Contact','BILL_TO_CONTACT','','','','MT063505','VARCHAR2','','','Bill To Contact','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIP_TO_CONTACT',660,'Ship To Contact','SHIP_TO_CONTACT','','','','MT063505','VARCHAR2','','','Ship To Contact','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','BILL_TO_ADDRESS5',660,'Bill To Address5','BILL_TO_ADDRESS5','','','','MT063505','VARCHAR2','','','Bill To Address5','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','BILL_TO_ADDRESS4',660,'Bill To Address4','BILL_TO_ADDRESS4','','','','MT063505','VARCHAR2','','','Bill To Address4','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','BILL_TO_ADDRESS3',660,'Bill To Address3','BILL_TO_ADDRESS3','','','','MT063505','VARCHAR2','','','Bill To Address3','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','BILL_TO_ADDRESS2',660,'Bill To Address2','BILL_TO_ADDRESS2','','','','MT063505','VARCHAR2','','','Bill To Address2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','BILL_TO_ADDRESS1',660,'Bill To Address1','BILL_TO_ADDRESS1','','','','MT063505','VARCHAR2','','','Bill To Address1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','BILL_TO_LOCATION',660,'Bill To Location','BILL_TO_LOCATION','','','','MT063505','VARCHAR2','','','Bill To Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','BILL_TO',660,'Bill To','BILL_TO','','','','MT063505','VARCHAR2','','','Bill To','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIP_TO_ADDRESS5',660,'Ship To Address5','SHIP_TO_ADDRESS5','','','','MT063505','VARCHAR2','','','Ship To Address5','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIP_TO_ADDRESS4',660,'Ship To Address4','SHIP_TO_ADDRESS4','','','','MT063505','VARCHAR2','','','Ship To Address4','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIP_TO_ADDRESS3',660,'Ship To Address3','SHIP_TO_ADDRESS3','','','','MT063505','VARCHAR2','','','Ship To Address3','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIP_TO_ADDRESS2',660,'Ship To Address2','SHIP_TO_ADDRESS2','','','','MT063505','VARCHAR2','','','Ship To Address2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIP_TO_ADDRESS1',660,'Ship To Address1','SHIP_TO_ADDRESS1','','','','MT063505','VARCHAR2','','','Ship To Address1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIP_TO_LOCATION',660,'Ship To Location','SHIP_TO_LOCATION','','','','MT063505','VARCHAR2','','','Ship To Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIP_TO',660,'Ship To','SHIP_TO','','','','MT063505','VARCHAR2','','','Ship To','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SUBINVENTORY',660,'Subinventory','SUBINVENTORY','','','','MT063505','VARCHAR2','','','Subinventory','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIP_FROM',660,'Ship From','SHIP_FROM','','','','MT063505','VARCHAR2','','','Ship From','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','MT063505','VARCHAR2','','','Customer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SOLD_TO',660,'Sold To','SOLD_TO','','','','MT063505','VARCHAR2','','','Sold To','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','TERMS',660,'Terms','TERMS','','','','MT063505','VARCHAR2','','','Terms','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','INVOICING_RULE',660,'Invoicing Rule','INVOICING_RULE','','','','MT063505','VARCHAR2','','','Invoicing Rule','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ACCOUNTING_RULE',660,'Accounting Rule','ACCOUNTING_RULE','','','','MT063505','VARCHAR2','','','Accounting Rule','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ROUNDING_FACTOR',660,'Rounding Factor','ROUNDING_FACTOR','','','','MT063505','NUMBER','','','Rounding Factor','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','PRICE_LIST',660,'Price List','PRICE_LIST','','','','MT063505','VARCHAR2','','','Price List','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','LINE_TYPE',660,'Line Type','LINE_TYPE','','','','MT063505','VARCHAR2','','','Line Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','LATEST_ACCEPTABLE_DATE',660,'Latest Acceptable Date','LATEST_ACCEPTABLE_DATE','','','','MT063505','DATE','','','Latest Acceptable Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','EARLIEST_ACCEPTABLE_DATE',660,'Earliest Acceptable Date','EARLIEST_ACCEPTABLE_DATE','','','','MT063505','DATE','','','Earliest Acceptable Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ACTUAL_SHIPMENT_DATE',660,'Actual Shipment Date','ACTUAL_SHIPMENT_DATE','','','','MT063505','DATE','','','Actual Shipment Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ACTUAL_ARRIVAL_DATE',660,'Actual Arrival Date','ACTUAL_ARRIVAL_DATE','','','','MT063505','DATE','','','Actual Arrival Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','VISIBLE_DEMAND_FLAG',660,'Visible Demand Flag','VISIBLE_DEMAND_FLAG','','','','MT063505','VARCHAR2','','','Visible Demand Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','DEP_PLAN_REQUIRED_FLAG',660,'Dep Plan Required Flag','DEP_PLAN_REQUIRED_FLAG','','','','MT063505','VARCHAR2','','','Dep Plan Required Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','OPTION_FLAG',660,'Option Flag','OPTION_FLAG','','','','MT063505','VARCHAR2','','','Option Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','MODEL_GROUP_NUMBER',660,'Model Group Number','MODEL_GROUP_NUMBER','','','','MT063505','NUMBER','','','Model Group Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ORDER_TAX_VALUE',660,'Order Tax Value','ORDER_TAX_VALUE','','','','MT063505','NUMBER','','','Order Tax Value','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','UNIT_LIST_PRICE',660,'Unit List Price','UNIT_LIST_PRICE','','','','MT063505','NUMBER','','','Unit List Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','UNIT_SELLING_PRICE',660,'Unit Selling Price','UNIT_SELLING_PRICE','','','','MT063505','NUMBER','','','Unit Selling Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','REFERENCE_CUSTOMER_TRX_LINE_ID',660,'Reference Customer Trx Line Id','REFERENCE_CUSTOMER_TRX_LINE_ID','','','','MT063505','NUMBER','','','Reference Customer Trx Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','CUSTOMER_TRX_LINE_ID',660,'Customer Trx Line Id','CUSTOMER_TRX_LINE_ID','','','','MT063505','NUMBER','','','Customer Trx Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','LINE_CATEGORY_CODE',660,'Line Category Code','LINE_CATEGORY_CODE','','','','MT063505','VARCHAR2','','','Line Category Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ITEM_REVISION',660,'Item Revision','ITEM_REVISION','','','','MT063505','VARCHAR2','','','Item Revision','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','REFERENCE_HEADER_ID',660,'Reference Header Id','REFERENCE_HEADER_ID','','','','MT063505','NUMBER','','','Reference Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','REFERENCE_TYPE',660,'Reference Type','REFERENCE_TYPE','','','','MT063505','VARCHAR2','','','Reference Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','REFERENCE_LINE_ID',660,'Reference Line Id','REFERENCE_LINE_ID','','','','MT063505','NUMBER','','','Reference Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SOURCE_DOCUMENT_LINE_ID',660,'Source Document Line Id','SOURCE_DOCUMENT_LINE_ID','','','','MT063505','NUMBER','','','Source Document Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ORIG_SYS_LINE_RE',660,'Orig Sys Line Re','ORIG_SYS_LINE_RE','','','','MT063505','VARCHAR2','','','Orig Sys Line Re','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SOURCE_DOCUMENT_ID',660,'Source Document Id','SOURCE_DOCUMENT_ID','','','','MT063505','NUMBER','','','Source Document Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ORIG_SYS_DOCUMENT_REF',660,'Orig Sys Document Ref','ORIG_SYS_DOCUMENT_REF','','','','MT063505','VARCHAR2','','','Orig Sys Document Ref','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SOURCE_DOCUMENT_TYPE_ID',660,'Source Document Type Id','SOURCE_DOCUMENT_TYPE_ID','','','','MT063505','NUMBER','','','Source Document Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ACCOUNTING_RULE_DURATION',660,'Accounting Rule Duration','ACCOUNTING_RULE_DURATION','','','','MT063505','NUMBER','','','Accounting Rule Duration','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ACCOUNTING_RULE_ID',660,'Accounting Rule Id','ACCOUNTING_RULE_ID','','','','MT063505','NUMBER','','','Accounting Rule Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','INVOICING_RULE_ID',660,'Invoicing Rule Id','INVOICING_RULE_ID','','','','MT063505','NUMBER','','','Invoicing Rule Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','PAYMENT_TERM_ID',660,'Payment Term Id','PAYMENT_TERM_ID','','','','MT063505','NUMBER','','','Payment Term Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','TAX_POINT_CODE',660,'Tax Point Code','TAX_POINT_CODE','','','','MT063505','VARCHAR2','','','Tax Point Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','FOB_POINT_CODE',660,'Fob Point Code','FOB_POINT_CODE','','','','MT063505','VARCHAR2','','','Fob Point Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','FREIGHT_CARRIER_CODE',660,'Freight Carrier Code','FREIGHT_CARRIER_CODE','','','','MT063505','VARCHAR2','','','Freight Carrier Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','FREIGHT_TERMS_CODE',660,'Freight Terms Code','FREIGHT_TERMS_CODE','','','','MT063505','VARCHAR2','','','Freight Terms Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIPPING_METHOD_CODE',660,'Shipping Method Code','SHIPPING_METHOD_CODE','','','','MT063505','VARCHAR2','','','Shipping Method Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIPMENT_PRIORITY_CODE',660,'Shipment Priority Code','SHIPMENT_PRIORITY_CODE','','','','MT063505','VARCHAR2','','','Shipment Priority Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','OCL_CHARGE_ID',660,'Ocl Charge Id','OCL_CHARGE_ID','','','','MT063505','NUMBER','','','Ocl Charge Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','RCTT_ORG_ID',660,'Rctt Org Id','RCTT_ORG_ID','','','','MT063505','NUMBER','','','Rctt Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','RCTT_CUST_TRX_TYPE_ID',660,'Rctt Cust Trx Type Id','RCTT_CUST_TRX_TYPE_ID','','','','MT063505','NUMBER','','','Rctt Cust Trx Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','RCT_CUSTOMER_TRX_ID',660,'Rct Customer Trx Id','RCT_CUSTOMER_TRX_ID','','','','MT063505','NUMBER','','','Rct Customer Trx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','RCT_TX_CSTMR_TRX_LINE_ID',660,'Rct Tx Cstmr Trx Line Id','RCT_TX_CSTMR_TRX_LINE_ID','','','','MT063505','NUMBER','','','Rct Tx Cstmr Trx Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','RCTL_CUSTOMER_TRX_LINE_ID',660,'Rctl Customer Trx Line Id','RCTL_CUSTOMER_TRX_LINE_ID','','','','MT063505','NUMBER','','','Rctl Customer Trx Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','QLH_LIST_HEADER_ID',660,'Qlh List Header Id','QLH_LIST_HEADER_ID','','','','MT063505','NUMBER','','','Qlh List Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','INVOICE_REL_DIRCTNAL_FLAG',660,'Invoice Rel Dirctnal Flag','INVOICE_REL_DIRCTNAL_FLAG','','','','MT063505','VARCHAR2','','','Invoice Rel Dirctnal Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIP_REL_DIRECTIONAL_FLAG',660,'Ship Rel Directional Flag','SHIP_REL_DIRECTIONAL_FLAG','','','','MT063505','VARCHAR2','','','Ship Rel Directional Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','BILL_SU_SITE_USE_ID',660,'Bill Su Site Use Id','BILL_SU_SITE_USE_ID','','','','MT063505','NUMBER','','','Bill Su Site Use Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','MT063505','NUMBER','','','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','OPERATING_UNIT_ID',660,'Operating Unit Id','OPERATING_UNIT_ID','','','','MT063505','NUMBER','','','Operating Unit Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','CUST_TRX_TYPE_ID',660,'Cust Trx Type Id','CUST_TRX_TYPE_ID','','','','MT063505','NUMBER','','','Cust Trx Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','TRX_TYPE_TRANSACTION_TYPE_ID',660,'Trx Type Transaction Type Id','TRX_TYPE_TRANSACTION_TYPE_ID','','','','MT063505','NUMBER','','','Trx Type Transaction Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','TERM_TERM_ID',660,'Term Term Id','TERM_TERM_ID','','','','MT063505','NUMBER','','','Term Term Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','INVRULE_RULE_ID',660,'Invrule Rule Id','INVRULE_RULE_ID','','','','MT063505','NUMBER','','','Invrule Rule Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ACCRULE_RULE_ID',660,'Accrule Rule Id','ACCRULE_RULE_ID','','','','MT063505','NUMBER','','','Accrule Rule Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','PRICE_LIST_LIST_HEADER_ID',660,'Price List List Header Id','PRICE_LIST_LIST_HEADER_ID','','','','MT063505','NUMBER','','','Price List List Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ORDER_LINE_LINE_ID',660,'Order Line Line Id','ORDER_LINE_LINE_ID','','','','MT063505','NUMBER','','','Order Line Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','ORDER_HEADER_HEADER_ID',660,'Order Header Header Id','ORDER_HEADER_HEADER_ID','','','','MT063505','NUMBER','','','Order Header Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','INVOICE_ACCT_CUST_ACCOUNT_ID',660,'Invoice Acct Cust Account Id','INVOICE_ACCT_CUST_ACCOUNT_ID','','','','MT063505','NUMBER','','','Invoice Acct Cust Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','INVOICE_REL_RELATIONSHIP_ID',660,'Invoice Rel Relationship Id','INVOICE_REL_RELATIONSHIP_ID','','','','MT063505','NUMBER','','','Invoice Rel Relationship Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','INVOICE_PARTY_PARTY_ID',660,'Invoice Party Party Id','INVOICE_PARTY_PARTY_ID','','','','MT063505','NUMBER','','','Invoice Party Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','IROLES_CUST_ACCOUNT_ROLE_ID',660,'Iroles Cust Account Role Id','IROLES_CUST_ACCOUNT_ROLE_ID','','','','MT063505','NUMBER','','','Iroles Cust Account Role Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIP_ACCT_CUST_ACCOUNT_ID',660,'Ship Acct Cust Account Id','SHIP_ACCT_CUST_ACCOUNT_ID','','','','MT063505','NUMBER','','','Ship Acct Cust Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIP_REL_RELATIONSHIP_ID',660,'Ship Rel Relationship Id','SHIP_REL_RELATIONSHIP_ID','','','','MT063505','NUMBER','','','Ship Rel Relationship Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SROLES_CUST_ACCOUNT_ROLE_ID',660,'Sroles Cust Account Role Id','SROLES_CUST_ACCOUNT_ROLE_ID','','','','MT063505','NUMBER','','','Sroles Cust Account Role Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','CUST_ACCT_CUST_ACCOUNT_ID',660,'Cust Acct Cust Account Id','CUST_ACCT_CUST_ACCOUNT_ID','','','','MT063505','NUMBER','','','Cust Acct Cust Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIP_PARTY_PARTY_ID',660,'Ship Party Party Id','SHIP_PARTY_PARTY_ID','','','','MT063505','NUMBER','','','Ship Party Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','BILL_CAS_CUST_ACCT_SITE_ID',660,'Bill Cas Cust Acct Site Id','BILL_CAS_CUST_ACCT_SITE_ID','','','','MT063505','NUMBER','','','Bill Cas Cust Acct Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','BILL_PS_PARTY_SITE_ID',660,'Bill Ps Party Site Id','BILL_PS_PARTY_SITE_ID','','','','MT063505','NUMBER','','','Bill Ps Party Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','BILL_LOC_LOCATION_ID',660,'Bill Loc Location Id','BILL_LOC_LOCATION_ID','','','','MT063505','NUMBER','','','Bill Loc Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIP_CAS_CUST_ACCT_SITE_ID',660,'Ship Cas Cust Acct Site Id','SHIP_CAS_CUST_ACCT_SITE_ID','','','','MT063505','NUMBER','','','Ship Cas Cust Acct Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIP_LOC_LOCATION_ID',660,'Ship Loc Location Id','SHIP_LOC_LOCATION_ID','','','','MT063505','NUMBER','','','Ship Loc Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','SHIP_PS_PARTY_SITE_ID',660,'Ship Ps Party Site Id','SHIP_PS_PARTY_SITE_ID','','','','MT063505','NUMBER','','','Ship Ps Party Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_INV_ORDER_DETAIL_V','RERENTAL_BILLING_TERMS',660,'Rerental Billing Terms','RERENTAL_BILLING_TERMS','','','','MT063505','VARCHAR2','','','Rerental Billing Terms','','','','');
--Inserting Object Components for EIS_XXWC_OM_INV_ORDER_DETAIL_V
--Inserting Object Component Joins for EIS_XXWC_OM_INV_ORDER_DETAIL_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for HDS Order and Invoice Summary Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS Order and Invoice Summary Report
xxeis.eis_rsc_ins.lov( 660,' SELECT OH.ORDER_NUMBER      ORDER_NUMBER,
        NVL(CUST.ACCOUNT_NUMBER, PARTY.PARTY_NUMBER)     CUSTOMER_NUMBER,
        NVL(CUST.ACCOUNT_NAME, PARTY.PARTY_NAME)     CUSTOMER_NAME,
        OH.FLOW_STATUS_CODE  STATUS,
        OH.ORDERED_DATE      ORDERED_DATE
FROM   OE_ORDER_HEADERS OH,
       HZ_PARTIES        PARTY,
       HZ_CUST_ACCOUNTS  CUST
WHERE PARTY.PARTY_ID=CUST.PARTY_ID
AND   CUST.CUST_ACCOUNT_ID =OH.SOLD_TO_ORG_ID','','OM ORDER NUMBER','This gives the Order Number','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select CONCATENATED_SEGMENTS ITEM_NAME,DESCRIPTION ,ood.ORGANIZATION_NAME FROM  MTL_SYSTEM_ITEMS_KFV MSI ,org_organization_definitions ood WHERE  OOD.ORGANIZATION_ID=MSI.ORGANIZATION_ID and  exists (Select 1 from XXEIS.EIS_ORG_ACCESS_V where organization_id = msi.organization_id)
','','OM ITEM NAME','This gives the item name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select  cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','OM CUSTOMER NUMBER','This gives the Customer Number','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT C.LOCATION  SHIP_TO_LOCATION ,A.ACCOUNT_NUMBER
CUSTOMER_NUMBER,A.ACCOUNT_NAME CUSTOMER_NAME ,HOU.NAME OPERATING_UNIT
FROM HZ_CUST_ACCOUNTS A,HZ_CUST_ACCT_SITES B,
HZ_CUST_SITE_USES C,
HR_OPERATING_UNITS HOU
 WHERE A.CUST_ACCOUNT_ID=B.CUST_ACCOUNT_ID
 AND B.CUST_ACCT_SITE_ID=C.CUST_ACCT_SITE_ID
 AND SITE_USE_CODE=''SHIP_TO''
 AND B.ORG_ID=HOU.ORGANIZATION_ID
','','OM SHIP TO LOCATION','This gives ship to locations','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT C.LOCATION  BILL_TO_LOCATION ,A.ACCOUNT_NUMBER
CUSTOMER_NUMBER,A.ACCOUNT_NAME CUSTOMER_NAME ,HOU.NAME OPERATING_UNIT
FROM HZ_CUST_ACCOUNTS A,HZ_CUST_ACCT_SITES B,
HZ_CUST_SITE_USES C,
HR_OPERATING_UNITS HOU
 WHERE A.CUST_ACCOUNT_ID=B.CUST_ACCOUNT_ID
 AND B.CUST_ACCT_SITE_ID=C.CUST_ACCT_SITE_ID
 AND SITE_USE_CODE=''BILL_TO''
 AND B.ORG_ID=HOU.ORGANIZATION_ID','','OM BILL TO LOCATION','This gives bill to locations','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct  trx_number Invoice_Number  from ra_customer_trx','','OM Invoice Number','This Lov pick Transactions Numer .','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for HDS Order and Invoice Summary Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS Order and Invoice Summary Report
xxeis.eis_rsc_utility.delete_report_rows( 'HDS Order and Invoice Summary Report' );
--Inserting Report - HDS Order and Invoice Summary Report
xxeis.eis_rsc_ins.r( 660,'HDS Order and Invoice Summary Report','','The Report reviews summary invoice information about orders that have invoiced, including ordered amount, invoiced amount etc.','','','','LA023190','EIS_XXWC_OM_INV_ORDER_DETAIL_V','Y','','','LA023190','','N','Orders','','CSV,EXCEL,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - HDS Order and Invoice Summary Report
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'ORDER_QUANTITY_UOM','Order Quantity Uom','Order Quantity Uom','','','default','','34','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'PRICE_LIST','Price List','Price List','','','default','','5','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'INVOICED_QUANTITY','Invoiced Quantity','Invoiced Quantity','','~T~D~2','default','','30','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'INVOICING_RULE','Invoicing Rule','Invoicing Rule','','','default','','31','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'LINE_CATEGORY_CODE','Line Category Code','Line Category Code','','','default','','32','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'LINE_TYPE','Line Type','Line Type','','','default','','23','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'OPEN_FLAG','Open Flag','Open Flag','','','default','','33','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'ORDERED_ITEM','Ordered Item','Ordered Item','','','default','','20','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'ORDERED_QUANTITY','Ordered Quantity','Ordered Quantity','','~T~D~2','default','','21','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','1','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'QUANTITY_CREDITED','Quantity Credited','Quantity Credited','','~T~D~2','default','','35','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'QUANTITY_ORDERED','Quantity Ordered','Quantity Ordered','','~T~D~2','default','','36','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'RECEIVABLE_INV_QUANTITY','Receivable Inv Quantity','Receivable Inv Quantity','','~T~D~2','default','','37','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'REFERENCE_TYPE','Reference Type','Reference Type','','','default','','38','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'REJECT_REASON_CODE','Reject Reason Code','Reject Reason Code','','','default','','39','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'RETURN_REASON_CODE','Return Reason Code','Return Reason Code','','','default','','40','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'ROUNDING_FACTOR','Rounding Factor','Rounding Factor','','~~~','default','','41','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'SHIPMENT_NUMBER','Shipment Number','Shipment Number','','~~~','default','','42','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'SHIPPED_QUANTITY','Shipped Quantity','Shipped Quantity','','~T~D~2','default','','43','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'SHIPPING_QUANTITY','Shipping Quantity','Shipping Quantity','','~T~D~2','default','','44','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'SHIP_FROM','Ship From','Ship From','','','default','','45','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'SHIP_TO','Ship To','Ship To','','','default','','6','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'SHIP_TO_ADDRESS1','Ship To Address1','Ship To Address1','','','default','','7','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'SHIP_TO_ADDRESS2','Ship To Address2','Ship To Address2','','','default','','8','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'SUBINVENTORY','Subinventory','Subinventory','','','default','','46','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'TRANSACTION_NAME','Transaction Name','Transaction Name','','','default','','47','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'ACTUAL_ARRIVAL_DATE','Actual Arrival Date','Actual Arrival Date','','','default','','24','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'ACTUAL_SHIPMENT_DATE','Actual Shipment Date','Actual Shipment Date','','','default','','25','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'BILL_TO','Bill To','Bill To','','','default','','12','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'BILL_TO_ADDRESS1','Bill To Address1','Bill To Address1','','','default','','13','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'BILL_TO_ADDRESS2','Bill To Address2','Bill To Address2','','','default','','14','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'BILL_TO_LOCATION','Bill To Location','Bill To Location','','','default','','49','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','3','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'FOB_POINT_CODE','Fob Point Code','Fob Point Code','','','default','','26','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'FREIGHT_CARRIER_CODE','Freight Carrier Code','Freight Carrier Code','','','default','','27','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'FULFILLED_QUANTITY','Fulfilled Quantity','Fulfilled Quantity','','~T~D~2','default','','28','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'FULFILLMENT_DATE','Fulfillment Date','Fulfillment Date','','','default','','29','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'SOLD_TO','Sold To','Sold To','','','default','','18','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'SOURCE_TYPE_CODE','Source Type Code','Source Type Code','','','default','','51','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'TRANSACTION_DESCRIPTION','Transaction Description','Transaction Description','','','default','','52','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'UNIT_LIST_PRICE','Unit List Price','Unit List Price','','~T~D~2','default','','53','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'UNIT_SELLING_PRICE','Unit Selling Price','Unit Selling Price','','~T~D~2','default','','54','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'BILL_TO_ADDRESS3','Bill To Address3','Bill To Address3','','','default','','15','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'BILL_TO_ADDRESS4','Bill To Address4','Bill To Address4','','','default','','16','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'BILL_TO_ADDRESS5','Bill To Address5','Bill To Address5','','','default','','17','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'BILL_TO_CONTACT','Bill To Contact','Bill To Contact','','','default','','48','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'SHIP_TO_ADDRESS3','Ship To Address3','Ship To Address3','','','default','','9','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'SHIP_TO_ADDRESS4','Ship To Address4','Ship To Address4','','','default','','10','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'SHIP_TO_ADDRESS5','Ship To Address5','Ship To Address5','','','default','','11','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'LINE_NUMBER','Line Number','Line Number','','','default','','19','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'TRX_LINE_NUMBER','Trx Line Number','Trx Line Number','','~~~','default','','22','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','4','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','default','','2','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'INVOICE_CREATION_DATE','Invoice Creation Date','Invoice Creation Date','','','default','','50','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'INVOICE_LINE_EXTENDED_AMOUNT','Invoice Line Extended Amount','Invoice Line Extended Amount','','~T~D~2','default','','55','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'INVOICE_LINE_REVENUE_AMOUNT','Invoice Line Revenue Amount','Invoice Line Revenue Amount','','~T~D~2','default','','56','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'INVOICE_LINE_TAX_EXT_AMNT','Invoice Line Tax Ext Amnt','Invoice Line Tax Ext Amnt','','~T~D~2','default','','57','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'INVOICE_LINE_TAX_REV_AMNT','Invoice Line Tax Rev Amnt','Invoice Line Tax Rev Amnt','','~T~D~2','default','','58','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'OPERATING_UNIT','Operating Unit','Operating Unit','','','default','','59','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'ORDER_LINE_CHARGE','Order Line Charge','Order Line Charge','','','default','','60','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'ORDER_LINE_CHARGE_COST','Order Line Charge Cost','Order Line Charge Cost','','~T~D~2','default','','61','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'ORDER_LINE_STATUS','Order Line Status','Order Line Status','','','default','','62','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'ORDER_TAX_VALUE','Order Tax Value','Order Tax Value','','~T~D~2','default','','64','N','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Order and Invoice Summary Report',660,'RERENTAL_BILLING_TERMS','Rerental Billing Terms','Rerental Billing Terms','','','default','','63','','Y','','','','','','','LA023190','N','N','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','US','');
--Inserting Report Parameters - HDS Order and Invoice Summary Report
xxeis.eis_rsc_ins.rp( 'HDS Order and Invoice Summary Report',660,'Invoice Number To','Invoice Number To','CREDIT_INVOICE_NUMBER','<=','OM Invoice Number','','VARCHAR2','N','Y','7','N','Y','CONSTANT','LA023190','Y','N','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Order and Invoice Summary Report',660,'Order  Number To','Order Number To','ORDER_NUMBER','<=','OM ORDER NUMBER','','VARCHAR2','N','Y','4','N','Y','CONSTANT','LA023190','Y','N','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Order and Invoice Summary Report',660,'Ordered Date From','Ordered Date From','ORDERED_DATE','>=','','','DATE','N','Y','1','Y','N','CONSTANT','LA023190','Y','N','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Order and Invoice Summary Report',660,'Ordered Date To','Ordered Date To','ORDERED_DATE','<=','','','DATE','N','Y','2','Y','N','CONSTANT','LA023190','Y','N','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Order and Invoice Summary Report',660,'Order Number from','Order Number from','ORDER_NUMBER','>=','OM ORDER NUMBER','','VARCHAR2','N','Y','3','N','Y','CONSTANT','LA023190','Y','N','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Order and Invoice Summary Report',660,'Ordered Item','Ordered Item','ORDERED_ITEM','IN','OM ITEM NAME','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','LA023190','Y','N','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Order and Invoice Summary Report',660,'Invoice Number From','Invoice Number From','CREDIT_INVOICE_NUMBER','>=','OM Invoice Number','','VARCHAR2','N','Y','6','N','Y','CONSTANT','LA023190','Y','N','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Order and Invoice Summary Report',660,'Invoice Date From','Invoice Date From','INVOICE_CREATION_DATE','>=','','','DATE','N','Y','8','Y','N','CONSTANT','LA023190','Y','N','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Order and Invoice Summary Report',660,'Invoice Date To','Invoice Date To','INVOICE_CREATION_DATE','<=','','','DATE','N','Y','9','Y','N','CONSTANT','LA023190','Y','N','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Order and Invoice Summary Report',660,'Bill To','Bill To','BILL_TO','IN','OM BILL TO LOCATION','','VARCHAR2','N','Y','11','Y','Y','CONSTANT','LA023190','Y','N','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Order and Invoice Summary Report',660,'Ship To','Ship To','SHIP_TO','IN','OM SHIP TO LOCATION','','VARCHAR2','N','Y','12','Y','Y','CONSTANT','LA023190','Y','N','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Order and Invoice Summary Report',660,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','OM CUSTOMER NUMBER','','VARCHAR2','N','Y','10','Y','Y','CONSTANT','LA023190','Y','N','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Order and Invoice Summary Report',660,'Source Type Code','Source Type Code','SOURCE_TYPE_CODE','IN','','','VARCHAR2','N','Y','13','Y','Y','CONSTANT','LA023190','Y','N','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','US','');
--Inserting Dependent Parameters - HDS Order and Invoice Summary Report
--Inserting Report Conditions - HDS Order and Invoice Summary Report
xxeis.eis_rsc_ins.rcnh( 'HDS Order and Invoice Summary Report',660,'BILL_TO IN :Bill To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BILL_TO','','Bill To','','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','','','IN','Y','Y','','','','','1',660,'HDS Order and Invoice Summary Report','BILL_TO IN :Bill To ');
xxeis.eis_rsc_ins.rcnh( 'HDS Order and Invoice Summary Report',660,'CREDIT_INVOICE_NUMBER >= :Invoice Number From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREDIT_INVOICE_NUMBER','','Invoice Number From','','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',660,'HDS Order and Invoice Summary Report','CREDIT_INVOICE_NUMBER >= :Invoice Number From ');
xxeis.eis_rsc_ins.rcnh( 'HDS Order and Invoice Summary Report',660,'CREDIT_INVOICE_NUMBER <= :Invoice Number To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREDIT_INVOICE_NUMBER','','Invoice Number To','','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',660,'HDS Order and Invoice Summary Report','CREDIT_INVOICE_NUMBER <= :Invoice Number To ');
xxeis.eis_rsc_ins.rcnh( 'HDS Order and Invoice Summary Report',660,'CUSTOMER_NUMBER IN :Customer Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_NUMBER','','Customer Number','','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','','','IN','Y','Y','','','','','1',660,'HDS Order and Invoice Summary Report','CUSTOMER_NUMBER IN :Customer Number ');
xxeis.eis_rsc_ins.rcnh( 'HDS Order and Invoice Summary Report',660,'ORDERED_ITEM IN :Ordered Item ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORDERED_ITEM','','Ordered Item','','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','','','IN','Y','Y','','','','','1',660,'HDS Order and Invoice Summary Report','ORDERED_ITEM IN :Ordered Item ');
xxeis.eis_rsc_ins.rcnh( 'HDS Order and Invoice Summary Report',660,'ORDER_NUMBER <= :Order  Number To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORDER_NUMBER','','Order  Number To','','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',660,'HDS Order and Invoice Summary Report','ORDER_NUMBER <= :Order  Number To ');
xxeis.eis_rsc_ins.rcnh( 'HDS Order and Invoice Summary Report',660,'ORDER_NUMBER >= :Order Number from ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORDER_NUMBER','','Order Number from','','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',660,'HDS Order and Invoice Summary Report','ORDER_NUMBER >= :Order Number from ');
xxeis.eis_rsc_ins.rcnh( 'HDS Order and Invoice Summary Report',660,'SHIP_TO IN :Ship To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SHIP_TO','','Ship To','','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','','','IN','Y','Y','','','','','1',660,'HDS Order and Invoice Summary Report','SHIP_TO IN :Ship To ');
xxeis.eis_rsc_ins.rcnh( 'HDS Order and Invoice Summary Report',660,'SOURCE_TYPE_CODE IN :Source Type Code ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SOURCE_TYPE_CODE','','Source Type Code','','','','','EIS_XXWC_OM_INV_ORDER_DETAIL_V','','','','','','IN','Y','Y','','','','','1',660,'HDS Order and Invoice Summary Report','SOURCE_TYPE_CODE IN :Source Type Code ');
xxeis.eis_rsc_ins.rcnh( 'HDS Order and Invoice Summary Report',660,'trunc(INVOICE_CREATION_DATE) >= :Invoice Date From ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Invoice Date From','','','','','','','','','','','GREATER_THAN_EQUALS','Y','Y','trunc(INVOICE_CREATION_DATE)','','','','1',660,'HDS Order and Invoice Summary Report','trunc(INVOICE_CREATION_DATE) >= :Invoice Date From ');
xxeis.eis_rsc_ins.rcnh( 'HDS Order and Invoice Summary Report',660,'trunc(INVOICE_CREATION_DATE) <= :Invoice Date To ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Invoice Date To','','','','','','','','','','','LESS_THAN_EQUALS','Y','Y','trunc(INVOICE_CREATION_DATE)','','','','1',660,'HDS Order and Invoice Summary Report','trunc(INVOICE_CREATION_DATE) <= :Invoice Date To ');
xxeis.eis_rsc_ins.rcnh( 'HDS Order and Invoice Summary Report',660,'trunc(ORDERED_DATE) >= :Ordered Date From ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Ordered Date From','','','','','','','','','','','GREATER_THAN_EQUALS','Y','Y','trunc(ORDERED_DATE)','','','','1',660,'HDS Order and Invoice Summary Report','trunc(ORDERED_DATE) >= :Ordered Date From ');
xxeis.eis_rsc_ins.rcnh( 'HDS Order and Invoice Summary Report',660,'trunc(ORDERED_DATE) <= :Ordered Date To ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Ordered Date To','','','','','','','','','','','LESS_THAN_EQUALS','Y','Y','trunc(ORDERED_DATE)','','','','1',660,'HDS Order and Invoice Summary Report','trunc(ORDERED_DATE) <= :Ordered Date To ');
--Inserting Report Sorts - HDS Order and Invoice Summary Report
xxeis.eis_rsc_ins.rs( 'HDS Order and Invoice Summary Report',660,'INVOICE_LINE_TAX_EXT_AMNT','ASC','LA023190','1','');
--Inserting Report Triggers - HDS Order and Invoice Summary Report
--inserting report templates - HDS Order and Invoice Summary Report
xxeis.eis_rsc_ins.r_tem( 'HDS Order and Invoice Summary Report','HDS Order and Invoice Summary Report','Seeded template for HDS Order and Invoice Summary Report','','','','','','','','','','','HDS Order and Invoice Summary Report.rtf','LA023190','X','','','Y','Y','','');
--Inserting Report Portals - HDS Order and Invoice Summary Report
--inserting report dashboards - HDS Order and Invoice Summary Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS Order and Invoice Summary Report','660','EIS_XXWC_OM_INV_ORDER_DETAIL_V','EIS_XXWC_OM_INV_ORDER_DETAIL_V','N','');
--inserting report security - HDS Order and Invoice Summary Report
xxeis.eis_rsc_ins.rsec( 'HDS Order and Invoice Summary Report','222','','XXWC_CRE_ASSOC_CASH_APP_MGR',660,'LA023190','','','');
--Inserting Report Pivots - HDS Order and Invoice Summary Report
xxeis.eis_rsc_ins.rpivot( 'HDS Order and Invoice Summary Report',660,'Pivot','1','1,0|1,2,1','0,1,0,0|None|1');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Order and Invoice Summary Report',660,'Pivot','ORDER_NUMBER','ROW_FIELD','','','1','2','');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Order and Invoice Summary Report',660,'Pivot','INVOICE_LINE_TAX_EXT_AMNT','DATA_FIELD','SUM','','1','','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- HDS Order and Invoice Summary Report
xxeis.eis_rsc_ins.rv( 'HDS Order and Invoice Summary Report','','HDS Order and Invoice Summary Report','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
