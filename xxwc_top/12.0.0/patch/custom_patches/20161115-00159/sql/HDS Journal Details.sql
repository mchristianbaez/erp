--Report Name            : HDS Journal Details
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_1801492_ZEQWMS_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1801492_ZEQWMS_V
xxeis.eis_rsc_ins.v( 'XXEIS_1801492_ZEQWMS_V',85000,'Paste SQL View for HDS Journal Details','1.0','','','ID020048','APPS','HDS Journal Details View','X1ZV123','','','VIEW','US','','');
--Delete Object Columns for XXEIS_1801492_ZEQWMS_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1801492_ZEQWMS_V',85000,FALSE);
--Inserting Object Columns for XXEIS_1801492_ZEQWMS_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','CLOSE_ACCT_SEQ_VERSION_ID',85000,'','','','','','ID020048','NUMBER','','','Close Acct Seq Version Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','CLOSE_ACCT_SEQ_ASSIGN_ID',85000,'','','','','','ID020048','NUMBER','','','Close Acct Seq Assign Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','CLOSE_ACCT_SEQ_VALUE',85000,'','','','~T~D~2','','ID020048','NUMBER','','','Close Acct Seq Value','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','FULL_NAME',85000,'','','','','','ID020048','VARCHAR2','','','Full Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','JE_HEADER_ID',85000,'','','','','','ID020048','NUMBER','','','Je Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','LAST_UPDATE_DATE',85000,'','','','','','ID020048','DATE','','','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','LAST_UPDATED_BY',85000,'','','','','','ID020048','NUMBER','','','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','LEDGER_ID',85000,'','','','','','ID020048','NUMBER','','','Ledger Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','JE_CATEGORY',85000,'','','','','','ID020048','VARCHAR2','','','Je Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','JE_SOURCE',85000,'','','','','','ID020048','VARCHAR2','','','Je Source','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','PERIOD_NAME',85000,'','','','','','ID020048','VARCHAR2','','','Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','NAME',85000,'','','','','','ID020048','VARCHAR2','','','Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','CURRENCY_CODE',85000,'','','','','','ID020048','VARCHAR2','','','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','STATUS',85000,'','','','','','ID020048','VARCHAR2','','','Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','DATE_CREATED',85000,'','','','','','ID020048','DATE','','','Date Created','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','ACCRUAL_REV_FLAG',85000,'','','','','','ID020048','VARCHAR2','','','Accrual Rev Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','MULTI_BAL_SEG_FLAG',85000,'','','','','','ID020048','VARCHAR2','','','Multi Bal Seg Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','ACTUAL_FLAG',85000,'','','','','','ID020048','VARCHAR2','','','Actual Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','DEFAULT_EFFECTIVE_DATE',85000,'','','','','','ID020048','DATE','','','Default Effective Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','TAX_STATUS_CODE',85000,'','','','','','ID020048','VARCHAR2','','','Tax Status Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','CONVERSION_FLAG',85000,'','','','','','ID020048','VARCHAR2','','','Conversion Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','CREATION_DATE',85000,'','','','','','ID020048','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','CREATED_BY',85000,'','','','','','ID020048','NUMBER','','','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','LAST_UPDATE_LOGIN',85000,'','','','','','ID020048','NUMBER','','','Last Update Login','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','ENCUMBRANCE_TYPE_ID',85000,'','','','','','ID020048','NUMBER','','','Encumbrance Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','BUDGET_VERSION_ID',85000,'','','','','','ID020048','NUMBER','','','Budget Version Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','BALANCED_JE_FLAG',85000,'','','','','','ID020048','VARCHAR2','','','Balanced Je Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','BALANCING_SEGMENT_VALUE',85000,'','','','','','ID020048','VARCHAR2','','','Balancing Segment Value','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','JE_BATCH_ID',85000,'','','','','','ID020048','NUMBER','','','Je Batch Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','FROM_RECURRING_HEADER_ID',85000,'','','','','','ID020048','NUMBER','','','From Recurring Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','UNIQUE_DATE',85000,'','','','','','ID020048','VARCHAR2','','','Unique Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','EARLIEST_POSTABLE_DATE',85000,'','','','','','ID020048','DATE','','','Earliest Postable Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','POSTED_DATE',85000,'','','','','','ID020048','DATE','','','Posted Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','ACCRUAL_REV_EFFECTIVE_DATE',85000,'','','','','','ID020048','DATE','','','Accrual Rev Effective Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','ACCRUAL_REV_PERIOD_NAME',85000,'','','','','','ID020048','VARCHAR2','','','Accrual Rev Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','ACCRUAL_REV_STATUS',85000,'','','','','','ID020048','VARCHAR2','','','Accrual Rev Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','ACCRUAL_REV_JE_HEADER_ID',85000,'','','','','','ID020048','NUMBER','','','Accrual Rev Je Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','ACCRUAL_REV_CHANGE_SIGN_FLAG',85000,'','','','','','ID020048','VARCHAR2','','','Accrual Rev Change Sign Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','DESCRIPTION',85000,'','','','','','ID020048','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','CONTROL_TOTAL',85000,'','','','~T~D~2','','ID020048','NUMBER','','','Control Total','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','RUNNING_TOTAL_DR',85000,'','','','~T~D~2','','ID020048','NUMBER','','','Running Total Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','RUNNING_TOTAL_CR',85000,'','','','~T~D~2','','ID020048','NUMBER','','','Running Total Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','RUNNING_TOTAL_ACCOUNTED_DR',85000,'','','','~T~D~2','','ID020048','NUMBER','','','Running Total Accounted Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','RUNNING_TOTAL_ACCOUNTED_CR',85000,'','','','~T~D~2','','ID020048','NUMBER','','','Running Total Accounted Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','CURRENCY_CONVERSION_RATE',85000,'','','','~T~D~2','','ID020048','NUMBER','','','Currency Conversion Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','CURRENCY_CONVERSION_TYPE',85000,'','','','','','ID020048','VARCHAR2','','','Currency Conversion Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','CURRENCY_CONVERSION_DATE',85000,'','','','','','ID020048','DATE','','','Currency Conversion Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','EXTERNAL_REFERENCE',85000,'','','','','','ID020048','VARCHAR2','','','External Reference','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','PARENT_JE_HEADER_ID',85000,'','','','','','ID020048','NUMBER','','','Parent Je Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','REVERSED_JE_HEADER_ID',85000,'','','','','','ID020048','NUMBER','','','Reversed Je Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','ORIGINATING_BAL_SEG_VALUE',85000,'','','','','','ID020048','VARCHAR2','','','Originating Bal Seg Value','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','INTERCOMPANY_MODE',85000,'','','','','','ID020048','NUMBER','','','Intercompany Mode','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','DR_BAL_SEG_VALUE',85000,'','','','','','ID020048','VARCHAR2','','','Dr Bal Seg Value','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','CR_BAL_SEG_VALUE',85000,'','','','','','ID020048','VARCHAR2','','','Cr Bal Seg Value','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','ATTRIBUTE1',85000,'','','','','','ID020048','VARCHAR2','','','Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','ATTRIBUTE2',85000,'','','','','','ID020048','VARCHAR2','','','Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','ATTRIBUTE3',85000,'','','','','','ID020048','VARCHAR2','','','Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','ATTRIBUTE4',85000,'','','','','','ID020048','VARCHAR2','','','Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','ATTRIBUTE5',85000,'','','','','','ID020048','VARCHAR2','','','Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','ATTRIBUTE6',85000,'','','','','','ID020048','VARCHAR2','','','Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','ATTRIBUTE7',85000,'','','','','','ID020048','VARCHAR2','','','Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','ATTRIBUTE8',85000,'','','','','','ID020048','VARCHAR2','','','Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','ATTRIBUTE9',85000,'','','','','','ID020048','VARCHAR2','','','Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','ATTRIBUTE10',85000,'','','','','','ID020048','VARCHAR2','','','Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','CONTEXT',85000,'','','','','','ID020048','VARCHAR2','','','Context','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','GLOBAL_ATTRIBUTE_CATEGORY',85000,'','','','','','ID020048','VARCHAR2','','','Global Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','GLOBAL_ATTRIBUTE1',85000,'','','','','','ID020048','VARCHAR2','','','Global Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','GLOBAL_ATTRIBUTE2',85000,'','','','','','ID020048','VARCHAR2','','','Global Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','GLOBAL_ATTRIBUTE3',85000,'','','','','','ID020048','VARCHAR2','','','Global Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','GLOBAL_ATTRIBUTE4',85000,'','','','','','ID020048','VARCHAR2','','','Global Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','GLOBAL_ATTRIBUTE5',85000,'','','','','','ID020048','VARCHAR2','','','Global Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','GLOBAL_ATTRIBUTE6',85000,'','','','','','ID020048','VARCHAR2','','','Global Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','GLOBAL_ATTRIBUTE7',85000,'','','','','','ID020048','VARCHAR2','','','Global Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','GLOBAL_ATTRIBUTE8',85000,'','','','','','ID020048','VARCHAR2','','','Global Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','GLOBAL_ATTRIBUTE9',85000,'','','','','','ID020048','VARCHAR2','','','Global Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','GLOBAL_ATTRIBUTE10',85000,'','','','','','ID020048','VARCHAR2','','','Global Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','USSGL_TRANSACTION_CODE',85000,'','','','','','ID020048','VARCHAR2','','','Ussgl Transaction Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','CONTEXT2',85000,'','','','','','ID020048','VARCHAR2','','','Context2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','DOC_SEQUENCE_ID',85000,'','','','','','ID020048','NUMBER','','','Doc Sequence Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','DOC_SEQUENCE_VALUE',85000,'','','','~T~D~2','','ID020048','NUMBER','','','Doc Sequence Value','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','JGZZ_RECON_CONTEXT',85000,'','','','','','ID020048','VARCHAR2','','','Jgzz Recon Context','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','JGZZ_RECON_REF',85000,'','','','','','ID020048','VARCHAR2','','','Jgzz Recon Ref','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','REFERENCE_DATE',85000,'','','','','','ID020048','DATE','','','Reference Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','LOCAL_DOC_SEQUENCE_ID',85000,'','','','','','ID020048','NUMBER','','','Local Doc Sequence Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','LOCAL_DOC_SEQUENCE_VALUE',85000,'','','','~T~D~2','','ID020048','NUMBER','','','Local Doc Sequence Value','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','DISPLAY_ALC_JOURNAL_FLAG',85000,'','','','','','ID020048','VARCHAR2','','','Display Alc Journal Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','JE_FROM_SLA_FLAG',85000,'','','','','','ID020048','VARCHAR2','','','Je From Sla Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','POSTING_ACCT_SEQ_VERSION_ID',85000,'','','','','','ID020048','NUMBER','','','Posting Acct Seq Version Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','POSTING_ACCT_SEQ_ASSIGN_ID',85000,'','','','','','ID020048','NUMBER','','','Posting Acct Seq Assign Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801492_ZEQWMS_V','POSTING_ACCT_SEQ_VALUE',85000,'','','','~T~D~2','','ID020048','NUMBER','','','Posting Acct Seq Value','','','','US');
--Inserting Object Components for XXEIS_1801492_ZEQWMS_V
xxeis.eis_rsc_ins.vcomp( 'XXEIS_1801492_ZEQWMS_V','GL_JE_HEADERS',85000,'GL_JE_HEADERS','GJ','GJ','ID020048','ID020048','-1','Journal Entry Headers','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for XXEIS_1801492_ZEQWMS_V
xxeis.eis_rsc_ins.vcj( 'XXEIS_1801492_ZEQWMS_V','GL_JE_HEADERS','GJ',85000,'X1ZV123.JE_HEADER_ID','=','GJ.JE_HEADER_ID(+)','','','','Y','ID020048');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report HDS Journal Details
prompt Creating Report Data for HDS Journal Details
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS Journal Details
xxeis.eis_rsc_utility.delete_report_rows( 'HDS Journal Details' );
--Inserting Report - HDS Journal Details
xxeis.eis_rsc_ins.r( 85000,'HDS Journal Details','','This report was created for User Audit review. This report provides a list of specific users from this list (''Bozik, John R'',''MacRae, Michael S'',''MacRae, Diane K'',''De Jesus, Ivelice'',''Duquette, Glenn'',''Hill, Felechia M'',''Velimirovic, Dragan'') who have created any GL Journal Entries.','','','','XXEIS_RS_ADMIN','XXEIS_1801492_ZEQWMS_V','Y','','SELECT papf.full_name
     , gj.*
  FROM gl.gl_je_headers gj
     , applsys.fnd_user u
     , hr.per_all_people_f papf
WHERE gj.created_by = u.user_id
   AND u.employee_id = papf.person_id
   AND papf.effective_end_date = ''31-DEC-4712''
   order by full_name asc
','XXEIS_RS_ADMIN','','N','Audit Reports','','CSV,EXCEL,','N','','','','','','','APPS','US','','','','');
--Inserting Report Columns - HDS Journal Details
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'JE_SOURCE','Je Source','','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'JE_FROM_SLA_FLAG','Je From Sla Flag','','','','','','34','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'BALANCED_JE_FLAG','Balanced Je Flag','','','','','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'RUNNING_TOTAL_DR','Running Total Dr','','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'NAME','Name','','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'LAST_UPDATE_DATE','Last Update Date','','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'RUNNING_TOTAL_ACCOUNTED_CR','Running Total Accounted Cr','','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'CREATION_DATE','Creation Date','','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'CURRENCY_CONVERSION_RATE','Currency Conversion Rate','','','','','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'ACTUAL_FLAG','Actual Flag','','','','','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'RUNNING_TOTAL_ACCOUNTED_DR','Running Total Accounted Dr','','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'DEFAULT_EFFECTIVE_DATE','Default Effective Date','','','','','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'MULTI_BAL_SEG_FLAG','Multi Bal Seg Flag','','','','','','28','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'LAST_UPDATED_BY','Last Updated By','','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'LAST_UPDATE_LOGIN','Last Update Login','','','','','','29','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'ACCRUAL_REV_CHANGE_SIGN_FLAG','Accrual Rev Change Sign Flag','','','','','','30','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'STATUS','Status','','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'LEDGER_ID','Ledger Id','','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'JE_HEADER_ID','Je Header Id','','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'POSTED_DATE','Posted Date','','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'CURRENCY_CONVERSION_TYPE','Currency Conversion Type','','','','','','31','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'ACCRUAL_REV_FLAG','Accrual Rev Flag','','','','','','32','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'RUNNING_TOTAL_CR','Running Total Cr','','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'CREATED_BY','Created By','','','','','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'FULL_NAME','Full Name','','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'JE_BATCH_ID','Je Batch Id','','','','','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'DESCRIPTION','Description','','','','','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'PERIOD_NAME','Period Name','','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'DATE_CREATED','Date Created','','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'JE_CATEGORY','Je Category','','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'REVERSED_JE_HEADER_ID','Reversed Je Header Id','','','','','','33','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'EXTERNAL_REFERENCE','External Reference','','','','','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'CURRENCY_CODE','Currency Code','','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Details',85000,'TAX_STATUS_CODE','Tax Status Code','','','','','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801492_ZEQWMS_V','','','GROUP_BY','US','');
--Inserting Report Parameters - HDS Journal Details
xxeis.eis_rsc_ins.rp( 'HDS Journal Details',85000,'Creation_Date_From','Creation_Date_From','CREATION_DATE','>=','','','DATE','N','Y','1','','N','CONSTANT','XXEIS_RS_ADMIN','Y','','','','','XXEIS_1801492_ZEQWMS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Journal Details',85000,'Creation_Date_To','Creation_Date_To','CREATION_DATE','<=','','','DATE','N','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','','','','','XXEIS_1801492_ZEQWMS_V','','','US','');
--Inserting Dependent Parameters - HDS Journal Details
--Inserting Report Conditions - HDS Journal Details
xxeis.eis_rsc_ins.rcnh( 'HDS Journal Details',85000,'CREATION_DATE BETWEEN :Creation_Date_From  :Creation_Date_To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Creation_Date_From','','','Creation_Date_To','','XXEIS_1801492_ZEQWMS_V','','','','','','BETWEEN','Y','Y','','','','','1',85000,'HDS Journal Details','CREATION_DATE BETWEEN :Creation_Date_From  :Creation_Date_To');
xxeis.eis_rsc_ins.rcnh( 'HDS Journal Details',85000,'X1ZV123.FULL_NAME IN ''Bozik, John R'',''MacRae, Michael S'',''MacRae, Diane K'',''De Jesus, Ivelice'',''Duquette, Glenn'',''Hill,  ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','IN','Y','N','X1ZV123.FULL_NAME','''Bozik, John R'',''MacRae, Michael S'',''MacRae, Diane K'',''De Jesus, Ivelice'',''Duquette, Glenn'',''Hill, Felechia M'',''Velimirovic, Dragan'',''Driggers, Michelle I'',''Cimini, Lisa A'',''Adeboye, Soji''','','','1',85000,'HDS Journal Details','X1ZV123.FULL_NAME IN ''Bozik, John R'',''MacRae, Michael S'',''MacRae, Diane K'',''De Jesus, Ivelice'',''Duquette, Glenn'',''Hill,  ');
--Inserting Report Sorts - HDS Journal Details
--Inserting Report Triggers - HDS Journal Details
--inserting report templates - HDS Journal Details
--Inserting Report Portals - HDS Journal Details
--inserting report dashboards - HDS Journal Details
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS Journal Details','85000','XXEIS_1801492_ZEQWMS_V','XXEIS_1801492_ZEQWMS_V','N','');
--inserting report security - HDS Journal Details
xxeis.eis_rsc_ins.rsec( 'HDS Journal Details','1','','HDS_SYSTEM_ADMINISTRATOR',85000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Journal Details','1','','SYSTEM_ADMINISTRATOR',85000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Journal Details','101','','XXCUS_GL_INQUIRY',85000,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS Journal Details
--Inserting Report   Version details- HDS Journal Details
xxeis.eis_rsc_ins.rv( 'HDS Journal Details','','HDS Journal Details','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
