--Report Name            : WC User Roles and Responsibilities
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXWC_USER_ROLES_VW
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXWC_USER_ROLES_VW
xxeis.eis_rsc_ins.v( 'XXWC_USER_ROLES_VW',85000,'','','','','10011289','XXEIS','Xxwc User Roles Vw','XURV','','','VIEW','US','','');
--Delete Object Columns for XXWC_USER_ROLES_VW
xxeis.eis_rsc_utility.delete_view_rows('XXWC_USER_ROLES_VW',85000,FALSE);
--Inserting Object Columns for XXWC_USER_ROLES_VW
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','EFFECTIVE_END_DATE',85000,'Effective End Date','EFFECTIVE_END_DATE','','','','10011289','DATE','','','Effective End Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','EFFECTIVE_START_DATE',85000,'Effective Start Date','EFFECTIVE_START_DATE','','','','10011289','DATE','','','Effective Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','LAST_ASSIGNED_BY',85000,'Last Assigned By','LAST_ASSIGNED_BY','','','','10011289','VARCHAR2','','','Last Assigned By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','SUPER_NAME',85000,'Super Name','SUPER_NAME','','','','10011289','VARCHAR2','','','Super Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','SUPER_CODE',85000,'Super Code','SUPER_CODE','','','','10011289','VARCHAR2','','','Super Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','SUB_NAME',85000,'Sub Name','SUB_NAME','','','','10011289','VARCHAR2','','','Sub Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','SUB_CODE',85000,'Sub Code','SUB_CODE','','','','10011289','VARCHAR2','','','Sub Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','FULL_NAME',85000,'Full Name','FULL_NAME','','','','10011289','VARCHAR2','','','Full Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','USER_NAME',85000,'User Name','USER_NAME','','','','10011289','VARCHAR2','','','User Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','EMAIL_ADDRESS',85000,'Email Address','EMAIL_ADDRESS','','','','10011289','VARCHAR2','','','Email Address','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','SUPERVISOR_EMAIL',85000,'Supervisor Email','SUPERVISOR_EMAIL','','','','10011289','VARCHAR2','','','Supervisor Email','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','SUPERVISOR_NAME',85000,'Supervisor Name','SUPERVISOR_NAME','','','','10011289','VARCHAR2','','','Supervisor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','EMPLOYEE_NUMBER',85000,'Employee Number','EMPLOYEE_NUMBER','','','','10011289','VARCHAR2','','','Employee Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','IS_BUYER',85000,'Is Buyer','IS_BUYER','','','','10011289','CHAR','','','Is Buyer','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','BUYER_JOB',85000,'Buyer Job','BUYER_JOB','','','','10011289','VARCHAR2','','','Buyer Job','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','BUYER_POSITION',85000,'Buyer Position','BUYER_POSITION','','','','10011289','VARCHAR2','','','Buyer Position','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','ASSIGNMENT_REASON',85000,'Assignment Reason','ASSIGNMENT_REASON','','','','10011289','VARCHAR2','','','Assignment Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','GL_SEGMENT',85000,'Gl Segment','GL_SEGMENT','','','','10011289','VARCHAR2','','','Gl Segment','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','SCOPE',85000,'Scope','SCOPE','','','','10011289','VARCHAR2','','','Scope','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','BRANCH',85000,'Branch','BRANCH','','','','10011289','VARCHAR2','','','Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','REGION',85000,'Region','REGION','','','','10011289','VARCHAR2','','','Region','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','FRU',85000,'Fru','FRU','','','','10011289','VARCHAR2','','','Fru','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','LOB',85000,'Lob','LOB','','','','10011289','VARCHAR2','','','Lob','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','FRU_DESCRIPTION',85000,'Fru Description','FRU_DESCRIPTION','','','','10011289','VARCHAR2','','','Fru Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','JOB_DESCRIPTION',85000,'Job Description','JOB_DESCRIPTION','','','','10011289','VARCHAR2','','','Job Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','LAST_UPDATE_DATE',85000,'Last Update Date','LAST_UPDATE_DATE','','','','10011289','DATE','','','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','BUYER_EMPLOYEE_ID',85000,'Buyer Employee Id','BUYER_EMPLOYEE_ID','','','','10011289','NUMBER','','','Buyer Employee Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_USER_ROLES_VW','ROLE_ORIG_SYSTEM',85000,'Role Orig System','ROLE_ORIG_SYSTEM','','','','10011289','VARCHAR2','','','Role Orig System','','','','US');
--Inserting Object Components for XXWC_USER_ROLES_VW
--Inserting Object Component Joins for XXWC_USER_ROLES_VW
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report WC User Roles and Responsibilities
prompt Creating Report Data for WC User Roles and Responsibilities
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC User Roles and Responsibilities
xxeis.eis_rsc_utility.delete_report_rows( 'WC User Roles and Responsibilities' );
--Inserting Report - WC User Roles and Responsibilities
xxeis.eis_rsc_ins.r( 85000,'WC User Roles and Responsibilities','','Create User Role Responsibilities view for reporting.This view shows all responsibilities and roles for all WC users, or users who have been assigned a WC role or Responsibility.
The Parent column shows the role that provides access to the roles/responsibilities shown in the Child column. For responsibilities directly assigned the parent will be the same as the child.
In cases where there is a hierarchy (parent role has a sub-role, and the sub-role has children) it is flattened in this report. All items are included, but shown as rolling up under the top-level parent. Intermediate roles are also shown as rolling up to the parent.','','','','MR020532','XXWC_USER_ROLES_VW','Y','','','MR020532','','N','WC Audit Reports','','CSV,HTML,EXCEL,','','','','','','','','','US','','','','');
--Inserting Report Columns - WC User Roles and Responsibilities
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'EFFECTIVE_END_DATE','Effective End Date','Effective End Date','','','','','9','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'EFFECTIVE_START_DATE','Effective Start Date','Effective Start Date','','','','','8','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'FULL_NAME','Full Name','Full Name','','','','','3','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'LAST_ASSIGNED_BY','Last Assigned By','Last Assigned By','','','','','10','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'SUB_CODE','Child Code','Sub Code','','','','','13','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'SUB_NAME','Child Role/Resp','Sub Name','','','','','6','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'SUPER_CODE','Parent Code','Super Code','','','','','12','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'SUPER_NAME','Parent Role/Resp','Super Name','','','','','5','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'USER_NAME','User Name','User Name','','','','','1','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'EMAIL_ADDRESS','Email Address','Email Address','','','','','4','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'SUPERVISOR_EMAIL','Supervisor Email','Supervisor Email','','','','','15','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'SUPERVISOR_NAME','Supervisor Name','Supervisor Name','','','','','14','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'EMPLOYEE_NUMBER','Employee Number','Employee Number','','','','','2','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'IS_BUYER','Is Buyer','Is Buyer','','','','','18','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'BUYER_JOB','Buyer Job','Buyer Job','','','','','16','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'BUYER_POSITION','Buyer Position','Buyer Position','','','','','17','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'ASSIGNMENT_REASON','Assignment Reason','Assignment Reason','','','','','11','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'GL_SEGMENT','Gl Segment','Gl Segment','','','','','19','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'SCOPE','Scope','Scope','','','','','20','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'BRANCH','Branch','Branch','','','','','24','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'REGION','Region','Region','','','','','25','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'FRU','Fru','Fru','','','','','22','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'LOB','Lob','Lob','','','','','26','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'FRU_DESCRIPTION','Fru Description','Fru Description','','','','','23','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'JOB_DESCRIPTION','Job Description','Job Description','','','','','21','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC User Roles and Responsibilities',85000,'LAST_UPDATE_DATE','Last Update Date','Last Update Date','','','','','7','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','','GROUP_BY','US','');
--Inserting Report Parameters - WC User Roles and Responsibilities
xxeis.eis_rsc_ins.rp( 'WC User Roles and Responsibilities',85000,'Specific User NT ID','User''s login name, aka NT ID','USER_NAME','LIKE','','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','MR020532','Y','','','','','XXWC_USER_ROLES_VW','','','US','');
xxeis.eis_rsc_ins.rp( 'WC User Roles and Responsibilities',85000,'User Full Name','User''s full name','FULL_NAME','LIKE','','%','VARCHAR2','N','Y','2','Y','Y','CONSTANT','MR020532','Y','','','','','XXWC_USER_ROLES_VW','','','US','');
xxeis.eis_rsc_ins.rp( 'WC User Roles and Responsibilities',85000,'Parent Role/Resp Name','The parent role or responsiiblity','SUPER_NAME','LIKE','','%','VARCHAR2','N','Y','3','Y','Y','CONSTANT','MR020532','Y','','','','','XXWC_USER_ROLES_VW','','','US','');
xxeis.eis_rsc_ins.rp( 'WC User Roles and Responsibilities',85000,'Child Role/Resp Name','The child role or responsibility','SUB_NAME','LIKE','','%','VARCHAR2','N','Y','4','Y','Y','CONSTANT','MR020532','Y','','','','','XXWC_USER_ROLES_VW','','','US','');
xxeis.eis_rsc_ins.rp( 'WC User Roles and Responsibilities',85000,'Employee Number','HR Employee ID','EMPLOYEE_NUMBER','LIKE','','%','VARCHAR2','N','Y','6','Y','Y','CONSTANT','MR020532','Y','','','','','XXWC_USER_ROLES_VW','','','US','');
xxeis.eis_rsc_ins.rp( 'WC User Roles and Responsibilities',85000,'Is Buyer (Y/N)','','IS_BUYER','LIKE','','%','VARCHAR2','N','Y','5','Y','Y','CONSTANT','MR020532','Y','','','','','XXWC_USER_ROLES_VW','','','US','');
xxeis.eis_rsc_ins.rp( 'WC User Roles and Responsibilities',85000,'GL Segment','Indicates the LOB','GL_SEGMENT','LIKE','','%','VARCHAR2','N','Y','7','Y','Y','CONSTANT','MR020532','Y','N','','','','XXWC_USER_ROLES_VW','','','US','');
xxeis.eis_rsc_ins.rp( 'WC User Roles and Responsibilities',85000,'Scope','Auditing scope, either WC or GSC','SCOPE','LIKE','','WC','VARCHAR2','N','Y','8','Y','Y','CONSTANT','MR020532','Y','N','','','','XXWC_USER_ROLES_VW','','','US','');
--Inserting Dependent Parameters - WC User Roles and Responsibilities
--Inserting Report Conditions - WC User Roles and Responsibilities
xxeis.eis_rsc_ins.rcnh( 'WC User Roles and Responsibilities',85000,'EMPLOYEE_NUMBER LIKE :Employee Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','EMPLOYEE_NUMBER','','Employee Number','','','','','XXWC_USER_ROLES_VW','','','','','','LIKE','Y','Y','','','','','1',85000,'WC User Roles and Responsibilities','EMPLOYEE_NUMBER LIKE :Employee Number ');
xxeis.eis_rsc_ins.rcnh( 'WC User Roles and Responsibilities',85000,'FULL_NAME LIKE :User Full Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','FULL_NAME','','User Full Name','','','','','XXWC_USER_ROLES_VW','','','','','','LIKE','Y','Y','','','','','1',85000,'WC User Roles and Responsibilities','FULL_NAME LIKE :User Full Name ');
xxeis.eis_rsc_ins.rcnh( 'WC User Roles and Responsibilities',85000,'GL_SEGMENT LIKE :GL Segment ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_SEGMENT','','GL Segment','','','','','XXWC_USER_ROLES_VW','','','','','','LIKE','Y','Y','','','','','1',85000,'WC User Roles and Responsibilities','GL_SEGMENT LIKE :GL Segment ');
xxeis.eis_rsc_ins.rcnh( 'WC User Roles and Responsibilities',85000,'IS_BUYER LIKE :Is Buyer (Y/N) ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','IS_BUYER','','Is Buyer (Y/N)','','','','','XXWC_USER_ROLES_VW','','','','','','LIKE','Y','Y','','','','','1',85000,'WC User Roles and Responsibilities','IS_BUYER LIKE :Is Buyer (Y/N) ');
xxeis.eis_rsc_ins.rcnh( 'WC User Roles and Responsibilities',85000,'SCOPE LIKE :Scope ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SCOPE','','Scope','','','','','XXWC_USER_ROLES_VW','','','','','','LIKE','Y','Y','','','','','1',85000,'WC User Roles and Responsibilities','SCOPE LIKE :Scope ');
xxeis.eis_rsc_ins.rcnh( 'WC User Roles and Responsibilities',85000,'SUB_NAME LIKE :Child Role/Resp Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SUB_NAME','','Child Role/Resp Name','','','','','XXWC_USER_ROLES_VW','','','','','','LIKE','Y','Y','','','','','1',85000,'WC User Roles and Responsibilities','SUB_NAME LIKE :Child Role/Resp Name ');
xxeis.eis_rsc_ins.rcnh( 'WC User Roles and Responsibilities',85000,'SUPER_NAME LIKE :Parent Role/Resp Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SUPER_NAME','','Parent Role/Resp Name','','','','','XXWC_USER_ROLES_VW','','','','','','LIKE','Y','Y','','','','','1',85000,'WC User Roles and Responsibilities','SUPER_NAME LIKE :Parent Role/Resp Name ');
xxeis.eis_rsc_ins.rcnh( 'WC User Roles and Responsibilities',85000,'USER_NAME LIKE :Specific User NT ID ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','USER_NAME','','Specific User NT ID','','','','','XXWC_USER_ROLES_VW','','','','','','LIKE','Y','Y','','','','','1',85000,'WC User Roles and Responsibilities','USER_NAME LIKE :Specific User NT ID ');
--Inserting Report Sorts - WC User Roles and Responsibilities
--Inserting Report Triggers - WC User Roles and Responsibilities
--inserting report templates - WC User Roles and Responsibilities
--Inserting Report Portals - WC User Roles and Responsibilities
--inserting report dashboards - WC User Roles and Responsibilities
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC User Roles and Responsibilities','85000','XXWC_USER_ROLES_VW','XXWC_USER_ROLES_VW','N','');
--inserting report security - WC User Roles and Responsibilities
xxeis.eis_rsc_ins.rsec( 'WC User Roles and Responsibilities','20005','','XXWC_VIEW_ALL_EIS_REPORTS',85000,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'WC User Roles and Responsibilities','20005','','XXWC_IT_FUNC_CONFIGURATOR',85000,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'WC User Roles and Responsibilities','20005','','',85000,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'WC User Roles and Responsibilities','20005','','',85000,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'WC User Roles and Responsibilities','20005','','XXWC_IT_SECURITY_ADMINISTRATOR',85000,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'WC User Roles and Responsibilities','20005','','XXWC_IT_OPERATIONS_ANALYST',85000,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'WC User Roles and Responsibilities','1','','SYSTEM_ADMINISTRATOR',85000,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'WC User Roles and Responsibilities','1','','HDS_SYSTEM_ADMINISTRATOR',85000,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'WC User Roles and Responsibilities','101','','XXCUS_GL_INQUIRY',85000,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'WC User Roles and Responsibilities','','SS084202','',85000,'MR020532','','','');
--Inserting Report Pivots - WC User Roles and Responsibilities
xxeis.eis_rsc_ins.rpivot( 'WC User Roles and Responsibilities',85000,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'WC User Roles and Responsibilities',85000,'Pivot','FULL_NAME','ROW_FIELD','','Full Name','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC User Roles and Responsibilities',85000,'Pivot','SUB_NAME','ROW_FIELD','','Child Role/Resp','5','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC User Roles and Responsibilities',85000,'Pivot','SUPER_NAME','ROW_FIELD','','Parent Role/Resp','4','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC User Roles and Responsibilities',85000,'Pivot','USER_NAME','ROW_FIELD','','NT ID','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC User Roles and Responsibilities',85000,'Pivot','IS_BUYER','ROW_FIELD','','','3','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- WC User Roles and Responsibilities
xxeis.eis_rsc_ins.rv( 'WC User Roles and Responsibilities','','WC User Roles and Responsibilities','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
