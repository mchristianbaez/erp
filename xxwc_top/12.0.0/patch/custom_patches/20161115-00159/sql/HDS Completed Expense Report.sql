--Report Name            : HDS Completed Expense Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXCUS_BULLET_IEXP_TBL
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXCUS_BULLET_IEXP_TBL
xxeis.eis_rsc_ins.v( 'XXCUS_BULLET_IEXP_TBL',91000,'Paste SQL View for iExpense Bullet Train Table','','','','XXEIS_RS_ADMIN','XXEIS','XXCUS_BULLET_IEXP_TBL','XBIT','','','VIEW','US','','');
--Delete Object Columns for XXCUS_BULLET_IEXP_TBL
xxeis.eis_rsc_utility.delete_view_rows('XXCUS_BULLET_IEXP_TBL',91000,FALSE);
--Inserting Object Columns for XXCUS_BULLET_IEXP_TBL
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','PERIOD_NAME',91000,'','PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CC_FISCAL_PERIOD',91000,'','CC_FISCAL_PERIOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cc Fiscal Period','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_PRODUCT',91000,'','ORACLE_PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Product','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_LOCATION',91000,'','ORACLE_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Location','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_COST_CENTER',91000,'','ORACLE_COST_CENTER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_ACCOUNT',91000,'','ORACLE_ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Account','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ITEM_DESCRIPTION',91000,'','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','LINE_AMOUNT',91000,'','LINE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Line Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DISTRIBUTION_LINE_NUMBER',91000,'','DISTRIBUTION_LINE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Distribution Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','FULL_NAME',91000,'','FULL_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Full Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EMPLOYEE_NUMBER',91000,'','EMPLOYEE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Employee Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EMP_DEFAULT_PROD',91000,'','EMP_DEFAULT_PROD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Emp Default Prod','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EMP_DEFAULT_LOC',91000,'','EMP_DEFAULT_LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Emp Default Loc','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EMP_DEFAULT_COSTCTR',91000,'','EMP_DEFAULT_COSTCTR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Emp Default Costctr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CREATION_DATE',91000,'','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','REPORT_SUBMITTED_DATE',91000,'','REPORT_SUBMITTED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Report Submitted Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','MERCHANT_NAME',91000,'','MERCHANT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Merchant Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','BUSINESS_PURPOSE',91000,'','BUSINESS_PURPOSE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Business Purpose','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','JUSTIFICATION',91000,'','JUSTIFICATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Justification','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','START_EXPENSE_DATE',91000,'','START_EXPENSE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Start Expense Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','END_EXPENSE_DATE',91000,'','END_EXPENSE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','End Expense Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','REMARKS',91000,'','REMARKS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Remarks','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ATTENDEES',91000,'','ATTENDEES','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CREDIT_CARD_TRX_ID',91000,'','CREDIT_CARD_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Credit Card Trx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','TRANSACTION_DATE',91000,'','TRANSACTION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Transaction Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','MERCHANT_LOCATION',91000,'','MERCHANT_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Merchant Location','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DESTINATION_FROM',91000,'','DESTINATION_FROM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Destination From','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DESTINATION_TO',91000,'','DESTINATION_TO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Destination To','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DAILY_DISTANCE',91000,'','DAILY_DISTANCE','','','','XXEIS_RS_ADMIN','NUMBER','','','Daily Distance','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','TRIP_DISTANCE',91000,'','TRIP_DISTANCE','','','','XXEIS_RS_ADMIN','NUMBER','','','Trip Distance','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DT_SENTTO_GL',91000,'','DT_SENTTO_GL','','','','XXEIS_RS_ADMIN','DATE','','','Dt Sentto Gl','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EXPENSE_REPORT_NUMBER',91000,'','EXPENSE_REPORT_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Expense Report Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CARD_PROGRAM_NAME',91000,'','CARD_PROGRAM_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Program Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','REPORT_TOTAL',91000,'','REPORT_TOTAL','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Report Total','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','TRANSACTION_AMOUNT',91000,'','TRANSACTION_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Transaction Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','APPROVER_ID',91000,'','APPROVER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Approver Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','APPROVER_NAME',91000,'','APPROVER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Approver Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','QUERY_DESCR',91000,'','QUERY_DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Query Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','POSTED_DATE',91000,'','POSTED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Posted Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','MCC_CODE_NO',91000,'','MCC_CODE_NO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mcc Code No','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','COST_CENTER_DESCR',91000,'','COST_CENTER_DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cost Center Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','FRU',91000,'','FRU','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Fru','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CARDMEMBER_NAME',91000,'','CARDMEMBER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cardmember Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','MCC_CODE',91000,'','MCC_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mcc Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','IMPORTED_TO_GL',91000,'','IMPORTED_TO_GL','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Imported To Gl','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','QUERY_NUM',91000,'','QUERY_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Query Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EXPENSE_REPORT_STATUS',91000,'','EXPENSE_REPORT_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Expense Report Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','REFERENCE_NUMBER',91000,'','REFERENCE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','PRODUCT_DESCR',91000,'','PRODUCT_DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Product Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','LOCATION_DESCR',91000,'','LOCATION_DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ACCOUNT_DESCR',91000,'','ACCOUNT_DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Account Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_ACCOUNTS',91000,'','ORACLE_ACCOUNTS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Accounts','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CATEGORY',91000,'','CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','SUB_CATEGORY',91000,'','SUB_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sub Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','INVOICE_NUMBER',91000,'','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DESCRIPTION',91000,'','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','AVG_MILEAGE_RATE',91000,'','AVG_MILEAGE_RATE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Avg Mileage Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ACCOUNTING_DATE',91000,'','ACCOUNTING_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Accounting Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','AMT_DUE_CCARD_COMPANY',91000,'','AMT_DUE_CCARD_COMPANY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amt Due Ccard Company','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CC_TRNS_CATEGORY',91000,'','CC_TRNS_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cc Trns Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','WORKFLOW_APPROVED_FLAG',91000,'','WORKFLOW_APPROVED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Workflow Approved Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EXPENSE_CURRENT_APPROVER_ID',91000,'','EXPENSE_CURRENT_APPROVER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Expense Current Approver Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','REPORT_TYPE',91000,'','REPORT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Report Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ADVANCE_INVOICE_TO_APPLY',91000,'','ADVANCE_INVOICE_TO_APPLY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Advance Invoice To Apply','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ADVANCE_DISTRIBUTION_NUMBER',91000,'','ADVANCE_DISTRIBUTION_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Advance Distribution Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ADVANCE_FLAG',91000,'','ADVANCE_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Advance Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ADVANCE_GL_DATE',91000,'','ADVANCE_GL_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Advance Gl Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ADVANCE_NUMBER',91000,'','ADVANCE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Advance Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','REPORT_REJECT_CODE',91000,'','REPORT_REJECT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Report Reject Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','APP_POST_FLAG',91000,'','APP_POST_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','App Post Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','APPROVED_IN_GL',91000,'','APPROVED_IN_GL','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Approved In Gl','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','VOUCHNO',91000,'','VOUCHNO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vouchno','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','PROJECT_NUMBER',91000,'','PROJECT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Project Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','PROJECT_NAME',91000,'','PROJECT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Project Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CURRENCY_CODE',91000,'','CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','VENDOR_NAME',91000,'','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ATTENDEES_EMP',91000,'','ATTENDEES_EMP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees Emp','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ATTENDEES_TE',91000,'','ATTENDEES_TE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees Te','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ATTENDEES_CTI1',91000,'','ATTENDEES_CTI1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees Cti1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ATTENDEES_CTI2',91000,'','ATTENDEES_CTI2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees Cti2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DISTANCE_UNIT_CODE',91000,'','DISTANCE_UNIT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Distance Unit Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CARD_PROGRAM_ID',91000,'','CARD_PROGRAM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Card Program Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','MILES',91000,'','MILES','','','','XXEIS_RS_ADMIN','NUMBER','','','Miles','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','RATE_PER_MILE',91000,'','RATE_PER_MILE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Rate Per Mile','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ATTRIBUTE_CATEGORY',91000,'','ATTRIBUTE_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CATEGORY_CODE',91000,'','CATEGORY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Category Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','RECEIPT_CURRENCY_AMOUNT',91000,'','RECEIPT_CURRENCY_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Receipt Currency Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','AMT_DUE_EMPLOYEE',91000,'','AMT_DUE_EMPLOYEE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amt Due Employee','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','IMPORTED_TO_AP',91000,'','IMPORTED_TO_AP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Imported To Ap','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','GST_AMOUNT',91000,'Gst Amount','GST_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Gst Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','HST_AMOUNT',91000,'Hst Amount','HST_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Hst Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_SEGMENT6',91000,'Oracle Segment6','ORACLE_SEGMENT6','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Segment6','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_SEGMENT7',91000,'Oracle Segment7','ORACLE_SEGMENT7','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Segment7','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORG_ID',91000,'Org Id','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','PST_AMOUNT',91000,'Pst Amount','PST_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Pst Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','QST_AMOUNT',91000,'Qst Amount','QST_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Qst Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','TAX_PROVINCE',91000,'Tax Province','TAX_PROVINCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Tax Province','','','','US');
--Inserting Object Components for XXCUS_BULLET_IEXP_TBL
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXCUS_NATURAL_ACCT_TBL',91000,'','XNAT','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXCUS_NATURAL_ACCT_TBL','','','','','XNAT','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXCUS_LOCATION_CODE_TBL',91000,'','XLC','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXCUS_LOCATION_CODE_TBL','','','','','XLCT','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','AP_CREDIT_CARD_TRXNS_ALL',91000,'','ACCTA','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','AP_CREDIT_CARD_TRXNS_ALL','','','','','ACCTA','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXHDS_APPR_ACT_TERM_DATE',91000,'','XAATD','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXHDS_APPR_ACT_TERM_DATE','','','','','XAATD','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXHDS_MCC_CODE_ACCT_INFO',91000,'','XMCAI','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXHDS_MCC_CODE_ACCT_INFO','','','','','XMCAI','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXHDS_CC_HOLDER_INFO',91000,'','XCHI','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXHDS_CC_HOLDER_INFO','','','','','XCHI','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXEIS_LOB_NAMES_V',91000,'','XLNV','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXEIS_LOB_NAMES_V','','','','','X1HV','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXCUSHR_PS_EMP_ALL_TBL',91000,'','XPEAT','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXCUSHR_PS_EMP_ALL_TBL','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for XXCUS_BULLET_IEXP_TBL
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXEIS_LOB_NAMES_V','XLNV',91000,'XBIT.ORACLE_PRODUCT','=','XLNV.ORACLE_PRODUCT(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','AP_CREDIT_CARD_TRXNS_ALL','ACCTA',91000,'XBIT.CREDIT_CARD_TRX_ID','=','ACCTA.TRX_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXCUS_LOCATION_CODE_TBL','XLC',91000,'XBIT.ORACLE_LOCATION','=','XLC.ENTRP_LOC(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXCUS_NATURAL_ACCT_TBL','XNAT',91000,'XBIT.ORACLE_ACCOUNT','=','XNAT.Z_OLD_VALUE(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXHDS_APPR_ACT_TERM_DATE','XAATD',91000,'XBIT.APPROVER_ID','=','XAATD.APPROVER_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXHDS_CC_HOLDER_INFO','XCHI',91000,'XBIT.EMPLOYEE_NUMBER','=','XCHI.EMPLOYEE_NUMBER','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXHDS_MCC_CODE_ACCT_INFO','XMCAI',91000,'XBIT.MCC_CODE_NO','=','XMCAI.MCC_CODE_N0(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXCUSHR_PS_EMP_ALL_TBL','XPEAT',91000,'XBIT.EMPLOYEE_NUMBER','=','XPEAT.EMPLOYEE_NUMBER','','','','','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
prompt Creating Report LOV Data for HDS Completed Expense Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS Completed Expense Report
xxeis.eis_rsc_ins.lov( 91000,'select DISTINCT PERIOD_NAME from XXCUS.XXCUS_BULLET_IEXP_TBL','','XXCUS_PERIOD_NAME T&E','Oracle Period Date from the bullet train table','ID020048',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 91000,'select DISTINCT ORACLE_PRODUCT from XXCUS.XXCUS_BULLET_IEXP_TBL','','XXCUS_ORACLE_PRODUCT T&E','Oracle product number','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
prompt Creating Report Data for HDS Completed Expense Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS Completed Expense Report
xxeis.eis_rsc_utility.delete_report_rows( 'HDS Completed Expense Report' );
--Inserting Report - HDS Completed Expense Report
xxeis.eis_rsc_ins.r( 91000,'HDS Completed Expense Report','','This report is to provide total completed expense report by LOB by Fiscal Period','','','','XXEIS_RS_ADMIN','XXCUS_BULLET_IEXP_TBL','Y','','','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','Y','','','','','','','','US','','','','');
--Inserting Report Columns - HDS Completed Expense Report
xxeis.eis_rsc_ins.rc( 'HDS Completed Expense Report',91000,'ORACLE_PRODUCT','Oracle Product','','','','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Completed Expense Report',91000,'PERIOD_NAME','Period Name','','','','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Completed Expense Report',91000,'Total_of_Expense_Reports','Total of Expense Reports','','NUMBER','~~~','default','','2','Y','Y','','','','','','(COUNT(distinct(expense_report_number)))','XXEIS_RS_ADMIN','N','N','','','','','','US','');
--Inserting Report Parameters - HDS Completed Expense Report
xxeis.eis_rsc_ins.rp( 'HDS Completed Expense Report',91000,'Fiscal Period','Fiscal Period','PERIOD_NAME','IN','XXCUS_PERIOD_NAME T&E','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXCUS_BULLET_IEXP_TBL','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Completed Expense Report',91000,'LOB','LOB','ORACLE_PRODUCT','IN','XXCUS_ORACLE_PRODUCT T&E','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXCUS_BULLET_IEXP_TBL','','','US','');
--Inserting Dependent Parameters - HDS Completed Expense Report
--Inserting Report Conditions - HDS Completed Expense Report
xxeis.eis_rsc_ins.rcnh( 'HDS Completed Expense Report',91000,'ORACLE_PRODUCT IN :LOB ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORACLE_PRODUCT','','LOB','','','','','XXCUS_BULLET_IEXP_TBL','','','','','','IN','Y','Y','','','','','1',91000,'HDS Completed Expense Report','ORACLE_PRODUCT IN :LOB ');
xxeis.eis_rsc_ins.rcnh( 'HDS Completed Expense Report',91000,'PERIOD_NAME IN :Fiscal Period ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PERIOD_NAME','','Fiscal Period','','','','','XXCUS_BULLET_IEXP_TBL','','','','','','IN','Y','Y','','','','','1',91000,'HDS Completed Expense Report','PERIOD_NAME IN :Fiscal Period ');
xxeis.eis_rsc_ins.rcnh( 'HDS Completed Expense Report',91000,'XBIT.QUERY_NUM = ''1'' ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','EQUALS','Y','N','XBIT.QUERY_NUM','''1''','','','1',91000,'HDS Completed Expense Report','XBIT.QUERY_NUM = ''1'' ');
--Inserting Report Sorts - HDS Completed Expense Report
--Inserting Report Triggers - HDS Completed Expense Report
--inserting report templates - HDS Completed Expense Report
--Inserting Report Portals - HDS Completed Expense Report
--inserting report dashboards - HDS Completed Expense Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS Completed Expense Report','91000','XXCUS_BULLET_IEXP_TBL','XXCUS_BULLET_IEXP_TBL','N','');
--inserting report security - HDS Completed Expense Report
xxeis.eis_rsc_ins.rsec( 'HDS Completed Expense Report','200','','HDS_AP_MANAGER',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Completed Expense Report','200','','HDS_AP_MGR_NOSUP_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Completed Expense Report','200','','HDS_AP_INQUIRY_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Completed Expense Report','200','','PAYABLES_MANAGER',91000,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS Completed Expense Report
--Inserting Report   Version details- HDS Completed Expense Report
xxeis.eis_rsc_ins.rv( 'HDS Completed Expense Report','','HDS Completed Expense Report','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
