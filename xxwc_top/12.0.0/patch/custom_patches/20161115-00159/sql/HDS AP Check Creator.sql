--Report Name            : HDS AP Check Creator
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_1801494_NBJRIL_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1801494_NBJRIL_V
xxeis.eis_rsc_ins.v( 'XXEIS_1801494_NBJRIL_V',85000,'Paste SQL View for HDS AP Check Creator','1.0','','','ID020048','APPS','HDS AP Check Creator View','X1NV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_1801494_NBJRIL_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1801494_NBJRIL_V',85000,FALSE);
--Inserting Object Columns for XXEIS_1801494_NBJRIL_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1801494_NBJRIL_V','CREATED_BY',85000,'','','','','','ID020048','NUMBER','','','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801494_NBJRIL_V','FULL_NAME',85000,'','','','','','ID020048','VARCHAR2','','','Full Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801494_NBJRIL_V','CREATION_DATE',85000,'','','','','','ID020048','DATE','','','Creation Date','','','','US');
--Inserting Object Components for XXEIS_1801494_NBJRIL_V
--Inserting Object Component Joins for XXEIS_1801494_NBJRIL_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report HDS AP Check Creator
prompt Creating Report Data for HDS AP Check Creator
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS AP Check Creator
xxeis.eis_rsc_utility.delete_report_rows( 'HDS AP Check Creator' );
--Inserting Report - HDS AP Check Creator
xxeis.eis_rsc_ins.r( 85000,'HDS AP Check Creator','','This report was created for User Audit review. This report provides a list of users that have created any AP Checks.','','','','XXEIS_RS_ADMIN','XXEIS_1801494_NBJRIL_V','Y','','SELECT DISTINCT ac.created_by
     , papf.full_name
      ,ac.creation_date
  FROM ap.ap_checks_all ac
     , applsys.fnd_user u
     , hr.per_all_people_f papf
WHERE ac.created_by = u.user_id
   AND u.employee_id = papf.person_id
   AND papf.effective_end_date = ''31-DEC-4712''
   and ac.org_id <> 162
','XXEIS_RS_ADMIN','','Y','Audit Reports','','CSV,EXCEL,','','','','','','','','APPS','US','','','','');
--Inserting Report Columns - HDS AP Check Creator
xxeis.eis_rsc_ins.rc( 'HDS AP Check Creator',85000,'FULL_NAME','Full Name','','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801494_NBJRIL_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Check Creator',85000,'CREATED_BY','Created By','','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801494_NBJRIL_V','','','SUM','US','');
--Inserting Report Parameters - HDS AP Check Creator
xxeis.eis_rsc_ins.rp( 'HDS AP Check Creator',85000,'Creation_Date_From','Creation_Date_From','CREATION_DATE','>=','','','DATE','N','Y','1','','N','CONSTANT','XXEIS_RS_ADMIN','Y','','','','','XXEIS_1801494_NBJRIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP Check Creator',85000,'Creation_Date_To','Creation_Date_To','CREATION_DATE','<=','','','DATE','N','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','','','','','XXEIS_1801494_NBJRIL_V','','','US','');
--Inserting Dependent Parameters - HDS AP Check Creator
--Inserting Report Conditions - HDS AP Check Creator
xxeis.eis_rsc_ins.rcnh( 'HDS AP Check Creator',85000,'Creation_Date BETWEEN :Creation_Date_From  :Creation_Date_To','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Creation_Date_From','','','Creation_Date_To','','','','','','','','BETWEEN','Y','Y','Creation_Date','','','','1',85000,'HDS AP Check Creator','Creation_Date BETWEEN :Creation_Date_From  :Creation_Date_To');
--Inserting Report Sorts - HDS AP Check Creator
--Inserting Report Triggers - HDS AP Check Creator
--inserting report templates - HDS AP Check Creator
--Inserting Report Portals - HDS AP Check Creator
--inserting report dashboards - HDS AP Check Creator
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS AP Check Creator','85000','XXEIS_1801494_NBJRIL_V','XXEIS_1801494_NBJRIL_V','N','');
--inserting report security - HDS AP Check Creator
xxeis.eis_rsc_ins.rsec( 'HDS AP Check Creator','1','','HDS_SYSTEM_ADMINISTRATOR',85000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Check Creator','1','','SYSTEM_ADMINISTRATOR',85000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Check Creator','101','','XXCUS_GL_INQUIRY',85000,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS AP Check Creator
--Inserting Report   Version details- HDS AP Check Creator
xxeis.eis_rsc_ins.rv( 'HDS AP Check Creator','','HDS AP Check Creator','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
