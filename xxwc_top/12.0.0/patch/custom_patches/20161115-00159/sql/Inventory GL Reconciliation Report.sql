--Report Name            : Inventory GL Reconciliation Report
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Report Data for Inventory GL Reconciliation Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Inventory GL Reconciliation Report
xxeis.eis_rsc_utility.delete_report_rows( 'Inventory GL Reconciliation Report' );
--Inserting Report - Inventory GL Reconciliation Report
xxeis.eis_rsc_ins.r( 401,'Inventory GL Reconciliation Report','','This report provides Material Transaction and general ledger journal information . This report can be used to Reconcile Inventory  and its corresponding journal information in   detail  for a given GL Period, material Transaction date,Distribution Account','802','','','XXEIS_RS_ADMIN','EIS_INV_GL_V','Y','','','XXEIS_RS_ADMIN','','N','Transaction Reports','RTF,PDF,','HTML,Html Summary,CSV,XML,EXCEL,Pivot Excel,','N','','','Y','','','','APPS','US','','','','');
--Inserting Report Columns - Inventory GL Reconciliation Report
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'UNROUNDED_ACCOUNTED_DR','Unrounded Accounted Dr','Unrounded Accounted Dr','','~T~D~0','default','','23','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'UNROUNDED_ENTERED_CR','Unrounded Entered Cr','Unrounded Entered Credit Amount for the journal line','','~T~D~0','default','','24','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'UNROUNDED_ENTERED_DR','Unrounded Entered Dr','Unrounded Entered Debit Amount for the journal line','','~T~D~0','default','','25','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'GL_ACCOUNT_STRING','GL Account String','Gl Account String','','','default','','14','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'JE_LINE_NUM','Journal Entry Line Number','Journal entry line number','','~T~D~0','default','','13','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'EVENT_CLASS_CODE','Event Class Code','Event class code','','','default','','19','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'EVENT_TYPE_CODE','Event Type Code','Event type code','','','default','','20','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'AE_LINE_NUM','AE Line Number','Ae Line Num','','~T~D~0','default','','21','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'ACCOUNTED_CR','Accounted Cr','Journal entry line credit amount in base currency','','~T~D~0','default','','15','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'ACCOUNTED_DR','Accounted Dr','Journal entry line debit amount in base currency','','~T~D~0','default','','16','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'ACCOUNTING_LINE_CODE','Accounting Line Code','Accounting line type code','','','default','','26','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'ACCOUNTING_LINE_TYPE_CODE','Accounting Line Type Code','Accounting Line Type Code','','','default','','27','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'CURRENCY_CODE','Currency Code','Currency code used in the transaction','','','default','','28','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'CURRENCY_CONVERSION_RATE','Currency Conversion Rate','Currency conversion rate used for the transaction','','~T~D~0','default','','29','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'ENTERED_CR','Entered Cr','Journal entry line credit amount in entered currency','','~T~D~0','default','','17','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'ENTERED_DR','Entered Dr','Journal entry line debit amount in entered currency','','~T~D~0','default','','18','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'GL_SL_LINK_TABLE','GL SL Link Table','Gl Sl Link Table','','','default','','30','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'ORGANIZATION_NAME','Organization Name','Translated name of the organization','','','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'SUBINVENTORY_CODE','Subinventory Code','Transaction secondary inventory','','','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'PERIOD_NAME','Period Name','Accounting period','','','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'TRANSACTION_DATE','Transaction Date','Transaction date','','','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'ITEM','Item','Key flexfield segment','','','default','','5','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'PRIMARY_QUANTITY','Primary Quantity','Transaction quantity in terms of primary uom of the item','','~T~D~0','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'ACTUAL_COST','Actual Cost','Actual cost','','~T~D~0','default','','7','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'BASE_TRANSACTION_VALUE','Base Transaction Value','Accounting value in the base currency','','~T~D~0','default','','8','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'JE_SOURCE','Journal Entry Source','Journal entry source','','','default','','9','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'JE_CATEGORY','Journal Entry Category','Journal entry category','','','default','','10','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'BATCH_NAME','Batch Name','Name of journal entry batch','','','default','','11','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'LINE_DEFINITION_CODE','Line Definition Code','Journal lines definition code','','','default','','31','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'ORGANIZATION_CODE','Organization Code','Organization code','','','default','','32','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'POSTED_DATE','Posted Date','Date journal entry header was posted','','','default','','33','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'RATE_OR_AMOUNT','Rate/Amount','Material or material overhead rate/amount (depending on cost element identifier)','','~T~D~0','default','','34','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'ROUNDING_CLASS_CODE','Rounding Class Code','The accounting class associated to the journal line. It is used as the grouping criteria for rounding. Accounting Program uses this field and the document_rounding_level together to group lines together to perform rounding.','','','default','','35','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'SOURCE_CODE','Source Code','User- entered source code for outside transactions','','','default','','36','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'SOURCE_DISTRIBUTION_TYPE','Source Distribution Type','Source Distribution Type','','','default','','37','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'TRANSACTION_QUANTITY','Transaction Quantity','Transaction quantity','','~T~D~0','default','','38','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'TRANSACTION_UOM','Transaction UOM','Transaction unit of measure','','','default','','39','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'UNROUNDED_ACCOUNTED_CR','Unrounded Accounted Cr','Unrounded Accounted Cr','','~T~D~0','default','','22','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','N','EIS_INV_GL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Inventory GL Reconciliation Report',401,'LEDGER_NAME','Ledger Name','Ledger Name','','','','','12','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_INV_GL_V','','','','US','');
--Inserting Report Parameters - Inventory GL Reconciliation Report
xxeis.eis_rsc_ins.rp( 'Inventory GL Reconciliation Report',401,'Organization','','ORGANIZATION_NAME','IN','INV Organization Names LOV','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_INV_GL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory GL Reconciliation Report',401,'Subinventory','','SUBINVENTORY_CODE','IN','INV Item Sub Inventory Names LOV','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_INV_GL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory GL Reconciliation Report',401,'Period Name','','PERIOD_NAME','IN','INV General Ledger Period Names LOV','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_INV_GL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory GL Reconciliation Report',401,'GL Account String','','GL_ACCOUNT_STRING','IN','INV General Ledger Concatenated Segments LOV','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','Y','','','','EIS_INV_GL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory GL Reconciliation Report',401,'Transaction Date From','','TRANSACTION_DATE','>=','','','DATE','Y','Y','5','N','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','','EIS_INV_GL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory GL Reconciliation Report',401,'Transaction Date To','','TRANSACTION_DATE','<=','','','DATE','Y','Y','6','N','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','','EIS_INV_GL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory GL Reconciliation Report',401,'Item','','ITEM','IN','INV Items LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_INV_GL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Inventory GL Reconciliation Report',401,'Ledger Name','Ledger Name','LEDGER_NAME','IN','INV General Ledger Name LOV','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_INV_GL_V','','','US','');
--Inserting Dependent Parameters - Inventory GL Reconciliation Report
--Inserting Report Conditions - Inventory GL Reconciliation Report
xxeis.eis_rsc_ins.rcnh( 'Inventory GL Reconciliation Report',401,'EIGV.LEDGER_NAME IN Ledger Name','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','LEDGER_NAME','','Ledger Name','','','','','EIS_INV_GL_V','','','','','','IN','Y','Y','','','','','1',401,'Inventory GL Reconciliation Report','EIGV.LEDGER_NAME IN Ledger Name');
xxeis.eis_rsc_ins.rcnh( 'Inventory GL Reconciliation Report',401,'GL_ACCOUNT_STRING IN :GL Account String ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_ACCOUNT_STRING','','GL Account String','','','','','EIS_INV_GL_V','','','','','','IN','Y','Y','','','','','1',401,'Inventory GL Reconciliation Report','GL_ACCOUNT_STRING IN :GL Account String ');
xxeis.eis_rsc_ins.rcnh( 'Inventory GL Reconciliation Report',401,'ITEM IN :Item ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ITEM','','Item','','','','','EIS_INV_GL_V','','','','','','IN','Y','Y','','','','','1',401,'Inventory GL Reconciliation Report','ITEM IN :Item ');
xxeis.eis_rsc_ins.rcnh( 'Inventory GL Reconciliation Report',401,'ORGANIZATION_NAME IN :Organization ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORGANIZATION_NAME','','Organization','','','','','EIS_INV_GL_V','','','','','','IN','Y','Y','','','','','1',401,'Inventory GL Reconciliation Report','ORGANIZATION_NAME IN :Organization ');
xxeis.eis_rsc_ins.rcnh( 'Inventory GL Reconciliation Report',401,'PERIOD_NAME IN :Period Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PERIOD_NAME','','Period Name','','','','','EIS_INV_GL_V','','','','','','IN','Y','Y','','','','','1',401,'Inventory GL Reconciliation Report','PERIOD_NAME IN :Period Name ');
xxeis.eis_rsc_ins.rcnh( 'Inventory GL Reconciliation Report',401,'SUBINVENTORY_CODE IN :Subinventory ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SUBINVENTORY_CODE','','Subinventory','','','','','EIS_INV_GL_V','','','','','','IN','Y','Y','','','','','1',401,'Inventory GL Reconciliation Report','SUBINVENTORY_CODE IN :Subinventory ');
xxeis.eis_rsc_ins.rcnh( 'Inventory GL Reconciliation Report',401,'TRANSACTION_DATE >= :Transaction Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TRANSACTION_DATE','','Transaction Date From','','','','','EIS_INV_GL_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',401,'Inventory GL Reconciliation Report','TRANSACTION_DATE >= :Transaction Date From ');
xxeis.eis_rsc_ins.rcnh( 'Inventory GL Reconciliation Report',401,'TRANSACTION_DATE <= :Transaction Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TRANSACTION_DATE','','Transaction Date To','','','','','EIS_INV_GL_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',401,'Inventory GL Reconciliation Report','TRANSACTION_DATE <= :Transaction Date To ');
--Inserting Report Sorts - Inventory GL Reconciliation Report
--Inserting Report Triggers - Inventory GL Reconciliation Report
--inserting report templates - Inventory GL Reconciliation Report
xxeis.eis_rsc_ins.r_tem( 'Inventory GL Reconciliation Report','Inventory GL Reconciliation Report','Inventory GL Reconciliation Report','','','','','','','','','','','Inventory GL Reconciliation Report.rtf','XXEIS_RS_ADMIN','X','','','Y','Y',' ',' ');
--Inserting Report Portals - Inventory GL Reconciliation Report
--inserting report dashboards - Inventory GL Reconciliation Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Inventory GL Reconciliation Report','401','EIS_INV_GL_V','EIS_INV_GL_V','N','');
--inserting report security - Inventory GL Reconciliation Report
xxeis.eis_rsc_ins.rsec( 'Inventory GL Reconciliation Report','401','','INVENTORY',401,'XXEIS_RS_ADMIN','','N','');
xxeis.eis_rsc_ins.rsec( 'Inventory GL Reconciliation Report','401','','XXWC_INVENTORY_CONTROL_INQUIRY',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory GL Reconciliation Report','401','','XXWC_AO_INV_ADJ_OEENTRY',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory GL Reconciliation Report','401','','XXWC_AO_BIN_MTN_CYCLE_REC',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory GL Reconciliation Report','401','','XXWC_AO_BIN_MTN_REC',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory GL Reconciliation Report','401','','XXWC_AO_REC',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory GL Reconciliation Report','660','','XXWC_AO_OEENTRY_REC',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory GL Reconciliation Report','401','','XXWC_DATA_MNGT_SC',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory GL Reconciliation Report','707','','XXWC_COST_MANAGEMENT_INQ',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory GL Reconciliation Report','401','','XXWC_INVENTORY_SUPER_USER',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory GL Reconciliation Report','401','','XXWC_INVENTORY_SPEC_SCC',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory GL Reconciliation Report','401','','XXWC_INV_PLANNER',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory GL Reconciliation Report','401','','XXWC_INVENTORY_CONTROL_SR_MGR',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory GL Reconciliation Report','401','','HDS_INVNTRY',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory GL Reconciliation Report','201','','XXWC_PURCHASING_INQUIRY',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory GL Reconciliation Report','401','','XXWC_RECEIVING_ASSOCIATE',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Inventory GL Reconciliation Report','20005','','XXWC_VIEW_ALL_EIS_REPORTS',401,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - Inventory GL Reconciliation Report
xxeis.eis_rsc_ins.rpivot( 'Inventory GL Reconciliation Report',401,'Pivot','1','1,0|1,2,1','1,1,0,0||2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory GL Reconciliation Report',401,'Pivot','UNROUNDED_ENTERED_CR','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory GL Reconciliation Report',401,'Pivot','UNROUNDED_ENTERED_DR','DATA_FIELD','SUM','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory GL Reconciliation Report',401,'Pivot','EVENT_CLASS_CODE','ROW_FIELD','','','8','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory GL Reconciliation Report',401,'Pivot','ACCOUNTING_LINE_CODE','ROW_FIELD','','','9','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory GL Reconciliation Report',401,'Pivot','ORGANIZATION_NAME','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory GL Reconciliation Report',401,'Pivot','SUBINVENTORY_CODE','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory GL Reconciliation Report',401,'Pivot','PERIOD_NAME','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory GL Reconciliation Report',401,'Pivot','ITEM','ROW_FIELD','','','5','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory GL Reconciliation Report',401,'Pivot','JE_CATEGORY','ROW_FIELD','','','4','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Inventory GL Reconciliation Report',401,'Pivot','BATCH_NAME','ROW_FIELD','','','6','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- Inventory GL Reconciliation Report
xxeis.eis_rsc_ins.rv( 'Inventory GL Reconciliation Report','802','','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
