--Report Name            : HDS GL TE ACCRUAL EMP
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_1071489_JCPCAD_V
prompt Table type Objects cannot be imported
set scan off define off
prompt Creating Object APPS.XXEIS_1071489_JCPCAD_V
set scan off define off
DECLARE
mod_exist varchar2(1);
stmt CLOB;
v_objlength INTEGER := 0;
v_offset INTEGER := 1;
v_sql_long DBMS_SQL.varchar2s;
v_sql_long_count INTEGER := 1;
c NUMBER;
r NUMBER;
l_stmt varchar2(32000);
l_view_cur pls_integer;
l_view_rows NUMBER;
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
dbms_lob.createtemporary(stmt,TRUE);
dbms_lob.write( lob_loc   => stmt,amount    => length('create or replace view APPS.XXEIS_1071489_JCPCAD_V
 ("LINE_AMOUNT","ORACLE_PRODUCT","ORACLE_LOCATION","ORACLE_COST_CENTER","ORACLE_ACCOUNT","PROJECT","FUTURE","FULL_NAME","MERCHANT_NAME","TRANSACTION_DATE","ITEM_DESCRIPTION","FRU","CARD_PROGRAM_NAME","MCC_CODE","START_EXPENSE_DATE","END_EXPENSE_DATE","EMPLOYEE_NUMBER"
) as '),offset    => 1, BUFFER    => 'create or replace view APPS.XXEIS_1071489_JCPCAD_V
 ("LINE_AMOUNT","ORACLE_PRODUCT","ORACLE_LOCATION","ORACLE_COST_CENTER","ORACLE_ACCOUNT","PROJECT","FUTURE","FULL_NAME","MERCHANT_NAME","TRANSACTION_DATE","ITEM_DESCRIPTION","FRU","CARD_PROGRAM_NAME","MCC_CODE","START_EXPENSE_DATE","END_EXPENSE_DATE","EMPLOYEE_NUMBER"
) as ');
l_stmt :=  'SELECT XXCUS_TE_ACCRUAL_HIST_TBL.LINE_AMOUNT,
NVL(XXCUS_TE_ACCRUAL_HIST_TBL.ORACLE_PRODUCT,''48'') ORACLE_PRODUCT,
NVL(XXCUS_TE_ACCRUAL_HIST_TBL.ORACLE_LOCATION,''Z0186'') ORACLE_LOCATION,
NVL(XXCUS_TE_ACCRUAL_HIST_TBL.ORACLE_COST_CENTER,''0000'') ORACLE_COST_CENTER,
NVL(XXCUS_TE_ACCRUAL_HIST_TBL.ORACLE_ACCOUNT,''658020'') ORACLE_ACCOUNT,
NVL(XXCUS_TE_ACCRUAL_HIST_TBL.SEGMENT5,''00000'') PROJECT,
NVL(XXCUS_TE_ACCRUAL_HIST_TBL.SEGMENT6,''00000'') FUTURE,
NVL(XXCUS_TE_ACCRUAL_HIST_TBL.FULL_NAME,''No Employee'') FULL_NAME,
XXCUS_TE_ACCRUAL_HIST_TBL.MERCHANT_NAME,
XXCUS_TE_ACCRUAL_HIST_TBL.TRANSACTION_DATE,
XXCUS_TE_ACCRUAL_HIST_TBL.ITEM_DESCRIPTION,
XXCUS_TE_ACCRUAL_HIST_TBL.FRU,
XXCUS_TE_ACCRUAL_HIST_TBL.CARD_PROGRAM_NAME,
XXCUS_TE_ACCRUAL_HIST_TBL.MCC_CODE,
XXCUS_TE_ACCRUAL_HIST_TBL.START_EXPENSE_DATE,
XXCUS_TE_ACCRUAL_HIST_TBL.END_EXPENSE_DATE,
XXCUS_TE_ACCRUAL_HIST_TBL.EMPLOYEE_NUMBER
FROM XXCUS.XXCUSGL_TE_ACCRUAL_TBL XXCUS_TE_ACCRUAL_HIST_TBL
WHERE XXCUS_TE_ACCRUAL_HIST_TBL.IMPORTED_TO_GL = ''N''
AND NVL(XXCUS_TE_ACCRUAL_HIST_TBL.ITEM_DESCRIPTION,''X'') <> ''Personal''
AND NVL(XXCUS_TE_ACCRUAL_HIST_TBL.CREDIT_CARD_TRX_ID,-1) <> 652460
AND NVL(XXCUS_TE_ACCRUAL_HIST_TBL.MERCHANT_NAME,''X'') <> ''CR BAL REFUND''
AND XXCUS_TE_ACCRUAL_HIST_TBL.CARD_PROGRAM_NAME <> ''Out of Pocket''
AND XXCUS_TE_ACCRUAL_HIST_TBL.CREDIT_CARD_TRX_ID IN (SELECT T.TRX_ID
FROM AP.AP_CREDIT_CARD_TRXNS_ALL T
WHERE T.TRX_ID = XXCUS_TE_ACCRUAL_HIST_TBL.CREDIT_CARD_TRX_ID
AND NVL(T.CATEGORY,''X'') <> ''PERSONAL'')
AND NVL(XXCUS_TE_ACCRUAL_HIST_TBL.MCC_CODE_NO,''X'') <> ''0'' OR XXCUS_TE_ACCRUAL_HIST_TBL.CARD_PROGRAM_NAME = ''Out of Pocket''

';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
v_objlength := DBMS_LOB.getlength (stmt);
IF v_objlength <= 32000 THEN 
EXECUTE IMMEDIATE to_char(stmt);
ELSE
v_offset := 1; 
WHILE v_offset <= v_objlength
LOOP
-- each record is 256 bytes from the LOB
v_sql_long (v_sql_long_count) := DBMS_LOB.SUBSTR (stmt, 256, v_offset);
-- increment the record count
v_sql_long_count := v_sql_long_count + 1;
-- figure out the new offset
v_offset := v_offset + 256;
END LOOP;
BEGIN
  -- open cursor
 c := DBMS_SQL.open_cursor;
 -- parse VARCHAR2S table
 DBMS_SQL.parse (c, v_sql_long, 1, v_sql_long_count - 1, FALSE, 1);
 -- Execute the cursor
r := DBMS_SQL.EXECUTE (c);
-- Close the cursor
 DBMS_SQL.close_cursor (c);
 END;
END IF;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
prompt Creating Object Data XXEIS_1071489_JCPCAD_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1071489_JCPCAD_V
xxeis.eis_rsc_ins.v( 'XXEIS_1071489_JCPCAD_V',91000,'Paste SQL View for HDS GL TE ACCRUAL EMP','1.0','','','ID020048','APPS','HDS GL TE ACCRUAL EMP View','X1JV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_1071489_JCPCAD_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1071489_JCPCAD_V',91000,FALSE);
--Inserting Object Columns for XXEIS_1071489_JCPCAD_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1071489_JCPCAD_V','LINE_AMOUNT',91000,'','','','~T~D~2','','ID020048','NUMBER','','','Line Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1071489_JCPCAD_V','ORACLE_PRODUCT',91000,'','','','','','ID020048','VARCHAR2','','','Oracle Product','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1071489_JCPCAD_V','ORACLE_LOCATION',91000,'','','','','','ID020048','VARCHAR2','','','Oracle Location','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1071489_JCPCAD_V','ORACLE_COST_CENTER',91000,'','','','','','ID020048','VARCHAR2','','','Oracle Cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1071489_JCPCAD_V','ORACLE_ACCOUNT',91000,'','','','','','ID020048','VARCHAR2','','','Oracle Account','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1071489_JCPCAD_V','PROJECT',91000,'','','','','','ID020048','VARCHAR2','','','Project','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1071489_JCPCAD_V','FUTURE',91000,'','','','','','ID020048','VARCHAR2','','','Future','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1071489_JCPCAD_V','FULL_NAME',91000,'','','','','','ID020048','VARCHAR2','','','Full Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1071489_JCPCAD_V','MERCHANT_NAME',91000,'','','','','','ID020048','VARCHAR2','','','Merchant Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1071489_JCPCAD_V','TRANSACTION_DATE',91000,'','','','','','ID020048','DATE','','','Transaction Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1071489_JCPCAD_V','ITEM_DESCRIPTION',91000,'','','','','','ID020048','VARCHAR2','','','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1071489_JCPCAD_V','FRU',91000,'','','','','','ID020048','VARCHAR2','','','Fru','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1071489_JCPCAD_V','CARD_PROGRAM_NAME',91000,'','','','','','ID020048','VARCHAR2','','','Card Program Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1071489_JCPCAD_V','MCC_CODE',91000,'','','','','','ID020048','VARCHAR2','','','Mcc Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1071489_JCPCAD_V','START_EXPENSE_DATE',91000,'','','','','','ID020048','DATE','','','Start Expense Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1071489_JCPCAD_V','END_EXPENSE_DATE',91000,'','','','','','ID020048','DATE','','','End Expense Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1071489_JCPCAD_V','EMPLOYEE_NUMBER',91000,'','','','','','ID020048','VARCHAR2','','','Employee Number','','','','US');
--Inserting Object Components for XXEIS_1071489_JCPCAD_V
--Inserting Object Component Joins for XXEIS_1071489_JCPCAD_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
prompt Creating Report LOV Data for HDS GL TE ACCRUAL EMP
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS GL TE ACCRUAL EMP
xxeis.eis_rsc_ins.lov( 91000,'select DISTINCT ORACLE_PRODUCT from XXCUS.XXCUS_BULLET_IEXP_TBL','','XXCUS_ORACLE_PRODUCT T&E','Oracle product number','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT DISTINCT CARD_PROGRAM_NAME  FROM  XXCUS.XXCUSGL_TE_ACCRUAL_TBL','','CARD_PROGRAM_NAME (Accrual Table)','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
prompt Creating Report Data for HDS GL TE ACCRUAL EMP
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS GL TE ACCRUAL EMP
xxeis.eis_rsc_utility.delete_report_rows( 'HDS GL TE ACCRUAL EMP' );
--Inserting Report - HDS GL TE ACCRUAL EMP
xxeis.eis_rsc_ins.r( 91000,'HDS GL TE ACCRUAL EMP','','','','','','XXEIS_RS_ADMIN','XXEIS_1071489_JCPCAD_V','Y','','SELECT XXCUS_TE_ACCRUAL_HIST_TBL.LINE_AMOUNT,
NVL(XXCUS_TE_ACCRUAL_HIST_TBL.ORACLE_PRODUCT,''48'') ORACLE_PRODUCT,
NVL(XXCUS_TE_ACCRUAL_HIST_TBL.ORACLE_LOCATION,''Z0186'') ORACLE_LOCATION,
NVL(XXCUS_TE_ACCRUAL_HIST_TBL.ORACLE_COST_CENTER,''0000'') ORACLE_COST_CENTER,
NVL(XXCUS_TE_ACCRUAL_HIST_TBL.ORACLE_ACCOUNT,''658020'') ORACLE_ACCOUNT,
NVL(XXCUS_TE_ACCRUAL_HIST_TBL.SEGMENT5,''00000'') PROJECT,
NVL(XXCUS_TE_ACCRUAL_HIST_TBL.SEGMENT6,''00000'') FUTURE,
NVL(XXCUS_TE_ACCRUAL_HIST_TBL.FULL_NAME,''No Employee'') FULL_NAME,
XXCUS_TE_ACCRUAL_HIST_TBL.MERCHANT_NAME,
XXCUS_TE_ACCRUAL_HIST_TBL.TRANSACTION_DATE,
XXCUS_TE_ACCRUAL_HIST_TBL.ITEM_DESCRIPTION,
XXCUS_TE_ACCRUAL_HIST_TBL.FRU,
XXCUS_TE_ACCRUAL_HIST_TBL.CARD_PROGRAM_NAME,
XXCUS_TE_ACCRUAL_HIST_TBL.MCC_CODE,
XXCUS_TE_ACCRUAL_HIST_TBL.START_EXPENSE_DATE,
XXCUS_TE_ACCRUAL_HIST_TBL.END_EXPENSE_DATE,
XXCUS_TE_ACCRUAL_HIST_TBL.EMPLOYEE_NUMBER
FROM XXCUS.XXCUSGL_TE_ACCRUAL_TBL XXCUS_TE_ACCRUAL_HIST_TBL
WHERE XXCUS_TE_ACCRUAL_HIST_TBL.IMPORTED_TO_GL = ''N''
AND NVL(XXCUS_TE_ACCRUAL_HIST_TBL.ITEM_DESCRIPTION,''X'') <> ''Personal''
AND NVL(XXCUS_TE_ACCRUAL_HIST_TBL.CREDIT_CARD_TRX_ID,-1) <> 652460
AND NVL(XXCUS_TE_ACCRUAL_HIST_TBL.MERCHANT_NAME,''X'') <> ''CR BAL REFUND''
AND XXCUS_TE_ACCRUAL_HIST_TBL.CARD_PROGRAM_NAME <> ''Out of Pocket''
AND XXCUS_TE_ACCRUAL_HIST_TBL.CREDIT_CARD_TRX_ID IN (SELECT T.TRX_ID
FROM AP.AP_CREDIT_CARD_TRXNS_ALL T
WHERE T.TRX_ID = XXCUS_TE_ACCRUAL_HIST_TBL.CREDIT_CARD_TRX_ID
AND NVL(T.CATEGORY,''X'') <> ''PERSONAL'')
AND NVL(XXCUS_TE_ACCRUAL_HIST_TBL.MCC_CODE_NO,''X'') <> ''0'' OR XXCUS_TE_ACCRUAL_HIST_TBL.CARD_PROGRAM_NAME = ''Out of Pocket''
','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','Y','','','','','','','APPS','US','','','','');
--Inserting Report Columns - HDS GL TE ACCRUAL EMP
xxeis.eis_rsc_ins.rc( 'HDS GL TE ACCRUAL EMP',91000,'FUTURE','Future','','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1071489_JCPCAD_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL TE ACCRUAL EMP',91000,'EMPLOYEE_NUMBER','Employee Number','','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1071489_JCPCAD_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL TE ACCRUAL EMP',91000,'START_EXPENSE_DATE','Start Expense Date','','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1071489_JCPCAD_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL TE ACCRUAL EMP',91000,'MCC_CODE','Mcc Code','','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1071489_JCPCAD_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL TE ACCRUAL EMP',91000,'END_EXPENSE_DATE','End Expense Date','','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1071489_JCPCAD_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL TE ACCRUAL EMP',91000,'ORACLE_ACCOUNT','Oracle Account','','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1071489_JCPCAD_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL TE ACCRUAL EMP',91000,'TRANSACTION_DATE','Transaction Date','','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1071489_JCPCAD_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL TE ACCRUAL EMP',91000,'ORACLE_LOCATION','Oracle Location','','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1071489_JCPCAD_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL TE ACCRUAL EMP',91000,'MERCHANT_NAME','Merchant Name','','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1071489_JCPCAD_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL TE ACCRUAL EMP',91000,'ITEM_DESCRIPTION','Item Description','','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1071489_JCPCAD_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL TE ACCRUAL EMP',91000,'ORACLE_COST_CENTER','Oracle Cost Center','','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1071489_JCPCAD_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL TE ACCRUAL EMP',91000,'PROJECT','Project','','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1071489_JCPCAD_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL TE ACCRUAL EMP',91000,'ORACLE_PRODUCT','Oracle Product','','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1071489_JCPCAD_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL TE ACCRUAL EMP',91000,'FRU','Fru','','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1071489_JCPCAD_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL TE ACCRUAL EMP',91000,'FULL_NAME','Full Name','','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1071489_JCPCAD_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL TE ACCRUAL EMP',91000,'CARD_PROGRAM_NAME','Card Program Name','','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1071489_JCPCAD_V','','','GROUP_BY','US','');
--Inserting Report Parameters - HDS GL TE ACCRUAL EMP
xxeis.eis_rsc_ins.rp( 'HDS GL TE ACCRUAL EMP',91000,'ORACLE PRODUCT','ORACLE PRODUCT','ORACLE_PRODUCT','IN','XXCUS_ORACLE_PRODUCT T&E','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_1071489_JCPCAD_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GL TE ACCRUAL EMP',91000,'CARD PROGRAM NAME','CARD PROGRAM NAME','CARD_PROGRAM_NAME','IN','CARD_PROGRAM_NAME (Accrual Table)','','VARCHAR2','N','N','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','N','N','','','','XXEIS_1071489_JCPCAD_V','','','US','');
--Inserting Dependent Parameters - HDS GL TE ACCRUAL EMP
--Inserting Report Conditions - HDS GL TE ACCRUAL EMP
xxeis.eis_rsc_ins.rcnh( 'HDS GL TE ACCRUAL EMP',91000,'CARD_PROGRAM_NAME IN :CARD PROGRAM NAME ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CARD_PROGRAM_NAME','','CARD PROGRAM NAME','','','','','XXEIS_1071489_JCPCAD_V','','','','','','IN','Y','Y','','','','','1',91000,'HDS GL TE ACCRUAL EMP','CARD_PROGRAM_NAME IN :CARD PROGRAM NAME ');
xxeis.eis_rsc_ins.rcnh( 'HDS GL TE ACCRUAL EMP',91000,'ORACLE_PRODUCT IN :ORACLE PRODUCT ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORACLE_PRODUCT','','ORACLE PRODUCT','','','','','XXEIS_1071489_JCPCAD_V','','','','','','IN','Y','Y','','','','','1',91000,'HDS GL TE ACCRUAL EMP','ORACLE_PRODUCT IN :ORACLE PRODUCT ');
--Inserting Report Sorts - HDS GL TE ACCRUAL EMP
--Inserting Report Triggers - HDS GL TE ACCRUAL EMP
--inserting report templates - HDS GL TE ACCRUAL EMP
--Inserting Report Portals - HDS GL TE ACCRUAL EMP
--inserting report dashboards - HDS GL TE ACCRUAL EMP
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS GL TE ACCRUAL EMP','91000','XXEIS_1071489_JCPCAD_V','XXEIS_1071489_JCPCAD_V','N','');
--inserting report security - HDS GL TE ACCRUAL EMP
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','200','','HDS_AP_TRNS_ENTRY_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','200','','HDS_AP_MANAGER',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','200','','HDS_AP_MGR_NOSUP_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','200','','HDS_AP_INQUIRY_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','200','','HDS_AP_DISBURSEMTS_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','200','','HDS_PAYABLES_CLOSE_GSC_GLBL',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','200','','HDS_PAYABLES_CLOSE_GLBL',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','200','','HDS_AP_ADMIN_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','101','','GNRL_LDGR_FSS',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','101','','XXCUS_GL_MANAGER_PVF',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','101','','XXCUS_GL_MANAGER_GLOBAL',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','101','','XXCUS_GL_MANAGER',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','101','','XXCUS_GL_INQUIRY_PVF',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','101','','XXCUS_GL_INQUIRY',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','101','','XXCUS_GL_ACCOUNTANT_USD_PVF',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','101','','XXCUS_GL_ACCOUNTANT_USD',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','101','','XXCUS_GL_ACCOUNTANT_GLOBAL',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','200','','HDS_AP_MGR_NOSUP_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','200','','HDS_AP_INQ_CANADA',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','200','','HDS_AP_DISBURSEMTS_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL TE ACCRUAL EMP','200','','HDS_AP_ADMIN_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS GL TE ACCRUAL EMP
--Inserting Report   Version details- HDS GL TE ACCRUAL EMP
xxeis.eis_rsc_ins.rv( 'HDS GL TE ACCRUAL EMP','','HDS GL TE ACCRUAL EMP','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
