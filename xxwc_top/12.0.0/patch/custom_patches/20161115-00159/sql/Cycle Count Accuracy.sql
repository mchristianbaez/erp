--Report Name            : Cycle Count Accuracy
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_CYCLE_ACC_COUNT_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_CYCLE_ACC_COUNT_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_CYCLE_ACC_COUNT_V',401,'','','','','MR020532','XXEIS','Eis Xxwc Cycle Acc Count V','EXCACV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_CYCLE_ACC_COUNT_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_CYCLE_ACC_COUNT_V',401,FALSE);
--Inserting Object Columns for EIS_XXWC_CYCLE_ACC_COUNT_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','VARQUANPER',401,'Varquanper','VARQUANPER','','','','MR020532','NUMBER','','','Varquanper','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','END_QUANTITY',401,'End Quantity','END_QUANTITY','','~T~D~2','','MR020532','NUMBER','','','End Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','STARTING_QUANTITY',401,'Starting Quantity','STARTING_QUANTITY','','~T~D~2','','MR020532','NUMBER','','','Starting Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','END_COUNT_DATE',401,'End Count Date','END_COUNT_DATE','','','','MR020532','DATE','','','End Count Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','BEGINING_COUNT_DATE',401,'Begining Count Date','BEGINING_COUNT_DATE','','','','MR020532','DATE','','','Begining Count Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','AVG_COST',401,'Avg Cost','AVG_COST','','~T~D~2','','MR020532','NUMBER','','','Avg Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','TS',401,'Ts','TS','','','','MR020532','CHAR','','','Ts','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','ITEM_DESCRIPTION',401,'Item Description','ITEM_DESCRIPTION','','','','MR020532','VARCHAR2','','','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','ITEM_NUMBER',401,'Item Number','ITEM_NUMBER','','','','MR020532','VARCHAR2','','','Item Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','PRIMARY_UOM_CODE',401,'Primary Uom Code','PRIMARY_UOM_CODE','','','','MR020532','VARCHAR2','','','Primary Uom Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','SV_CLASS',401,'Sv Class','SV_CLASS','','','','MR020532','VARCHAR2','','','Sv Class','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','CAT_CALSS',401,'Cat Calss','CAT_CALSS','','','','MR020532','VARCHAR2','','','Cat Calss','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','COUNT_DATE',401,'Count Date','COUNT_DATE','','','','MR020532','DATE','','','Count Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','ORG_CODE',401,'Org Code','ORG_CODE','','','','MR020532','VARCHAR2','','','Org Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','REGION',401,'Region','REGION','','','','MR020532','VARCHAR2','','','Region','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','DISTRICT',401,'District','DISTRICT','','','','MR020532','VARCHAR2','','','District','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','VAR_QUANTITY',401,'Var Quantity','VAR_QUANTITY','','~T~D~2','','MR020532','NUMBER','','','Var Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','COUNT_END_QUANTITY',401,'Count End Quantity','COUNT_END_QUANTITY','','~T~D~2','','MR020532','NUMBER','','','Count End Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','COUNT_VAR_QUANTITY',401,'Count Var Quantity','COUNT_VAR_QUANTITY','','~T~D~2','','MR020532','NUMBER','','','Count Var Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','TOTAL_VAR_COST',401,'Total Var Cost','TOTAL_VAR_COST','','~T~D~2','','MR020532','NUMBER','','','Total Var Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','VAR_COST',401,'Var Cost','VAR_COST','','~T~D~2','','MR020532','NUMBER','','','Var Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','ORG',401,'Org','ORG','','','','MR020532','VARCHAR2','','','Org','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','MR020532','NUMBER','','','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','PRIOR_COUNT_DATE',401,'Prior Count Date','PRIOR_COUNT_DATE','','','','MR020532','DATE','','','Prior Count Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','PRIOR_QUANTITY',401,'Prior Quantity','PRIOR_QUANTITY','','~T~D~2','','MR020532','NUMBER','','','Prior Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_ACC_COUNT_V','LAST_UPDATE_DATE',401,'Last Update Date','LAST_UPDATE_DATE','','','','MR020532','DATE','','','Last Update Date','','','','US');
--Inserting Object Components for EIS_XXWC_CYCLE_ACC_COUNT_V
--Inserting Object Component Joins for EIS_XXWC_CYCLE_ACC_COUNT_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report LOV Data for Cycle Count Accuracy
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Cycle Count Accuracy
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT concatenated_segments item, description FROM mtl_system_items_kfv msi,
                org_organization_definitions ood
          WHERE msi.organization_id = ood.organization_id
            AND ood.operating_unit = fnd_profile.VALUE (''ORG_ID'')
       ORDER BY concatenated_segments','','EIS_INV_ITEM_LOV','List of all inventory items.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','MR020532',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','MR020532',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','MR020532',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT organization_code code,organization_name name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_code','','XXWC INV ORGANIZATIONS LOV','List of All Inventory Orgs under a given operating unit.','MR020532',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','District Lov','MR020532',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Cat Class''  and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Cat Class List','','MR020532',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select distinct  MCV.SEGMENT1
        from  MTL_CATEGORIES_KFV    MCV,
                 MTL_CATEGORY_SETS     MCS,
                 Mtl_Item_Categories         Mic
          Where   Mcs.Category_Set_Name       =  ''Inventory Category''
          And Mcs.Structure_Id                         =  Mcv.Structure_Id
          and MIC.CATEGORY_SET_ID               =  MCS.CATEGORY_SET_ID
          And Mic.Category_Id                           =  Mcv.Category_Id','','XXWC Cat Class Lov','','MR020532',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report Data for Cycle Count Accuracy
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Cycle Count Accuracy
xxeis.eis_rsc_utility.delete_report_rows( 'Cycle Count Accuracy' );
--Inserting Report - Cycle Count Accuracy
xxeis.eis_rsc_ins.r( 401,'Cycle Count Accuracy','','','','','','MR020532','EIS_XXWC_CYCLE_ACC_COUNT_V','Y','','','MR020532','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Cycle Count Accuracy
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'AVG_COST','Avg Cost','Avg Cost','','~T~D~2','default','','16','N','','','','','','','','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'CAT_CALSS','Cat Class','Cat Calss','','','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'COUNT_DATE','Count Date','Count Date','','','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'DISTRICT','District','District','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'END_QUANTITY','Counted Qty','End Quantity','','~T~D~0','default','','13','N','','','','','','','','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'ITEM_NUMBER','Item','Item Number','','','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'PRIMARY_UOM_CODE','UOM','Primary Uom Code','','','default','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'REGION','Region','Region','','','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'STARTING_QUANTITY','Starting Quantity','Starting Quantity','','~T~D~0','default','','12','N','','','','','','','','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'SV_CLASS','SV Class','Sv Class','','','default','','10','N','','','','','','','','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'TS','TS','Ts','','','default','','11','N','','','','','','','','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'VARQUANPER','Variance%','Varquanper','','~T~D~2','default','','15','N','','','','','','','','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'VAR_QUANTITY','Variance Qty','Var Quantity','','~T~D~0','default','','14','N','','','','','','','','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'COUNT_END_QUANTITY','Lines Counted','Count End Quantity','','~T~D~0','default','','19','N','','','','','','','','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'COUNT_VAR_QUANTITY','Variance Line','Count Var Quantity','','~T~D~0','default','','20','N','','','','','','','','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'TOTAL','Total $$ Variance','Count Var Quantity','NUMBER','~T~D~2','default','','17','Y','','','','','','','EXCACV.VAR_QUANTITY*EXCACV.AVG_COST','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'TOTAL COST','Cost Variance %','Count Var Quantity','NUMBER','~T~D~2','default','','18','Y','','','','','','','100*(EXCACV.VAR_QUANTITY*EXCACV.AVG_COST)/decode(EXCACV.STARTING_QUANTITY*EXCACV.AVG_COST,0,1,EXCACV.STARTING_QUANTITY*EXCACV.AVG_COST)','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'ORG','Organization','Org','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Accuracy',401,'LAST_UPDATE_DATE','Last Posted Date','Last Update Date','','','default','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','GROUP_BY','US','');
--Inserting Report Parameters - Cycle Count Accuracy
xxeis.eis_rsc_ins.rp( 'Cycle Count Accuracy',401,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cycle Count Accuracy',401,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cycle Count Accuracy',401,'Org','Org','ORG_CODE','IN','XXWC INV ORGANIZATIONS LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cycle Count Accuracy',401,'OrgList','Org List','','IN','XXWC Org List','','VARCHAR2','N','Y','4','N','N','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cycle Count Accuracy',401,'Count Date Beginning','Count Date Beginning','LAST_UPDATE_DATE','>=','','','DATE','N','Y','5','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cycle Count Accuracy',401,'Count date ending','Count date ending','LAST_UPDATE_DATE','<=','','','DATE','N','Y','6','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cycle Count Accuracy',401,'Cat Class','Cat Class','CAT_CALSS','IN','XXWC Cat Class Lov','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cycle Count Accuracy',401,'Item','Item','ITEM_NUMBER','IN','EIS_INV_ITEM_LOV','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cycle Count Accuracy',401,'CatList','Cat Class List','','IN','XXWC Cat Class List','','VARCHAR2','N','Y','9','N','N','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cycle Count Accuracy',401,'ItemList','Item List','','IN','XXWC Item List','','VARCHAR2','N','Y','10','N','N','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','US','');
--Inserting Dependent Parameters - Cycle Count Accuracy
--Inserting Report Conditions - Cycle Count Accuracy
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Accuracy',401,'CAT_CALSS IN :Cat Class ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CAT_CALSS','','Cat Class','','','','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','','','','IN','Y','Y','','','','','1',401,'Cycle Count Accuracy','CAT_CALSS IN :Cat Class ');
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Accuracy',401,'DISTRICT IN :District ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DISTRICT','','District','','','','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','','','','IN','Y','Y','','','','','1',401,'Cycle Count Accuracy','DISTRICT IN :District ');
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Accuracy',401,'ITEM_NUMBER IN :Item ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ITEM_NUMBER','','Item','','','','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','','','','IN','Y','Y','','','','','1',401,'Cycle Count Accuracy','ITEM_NUMBER IN :Item ');
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Accuracy',401,'LAST_UPDATE_DATE >= :Count Date Beginning ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','LAST_UPDATE_DATE','','Count Date Beginning','','','','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',401,'Cycle Count Accuracy','LAST_UPDATE_DATE >= :Count Date Beginning ');
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Accuracy',401,'LAST_UPDATE_DATE <= :Count date ending ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','LAST_UPDATE_DATE','','Count date ending','','','','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',401,'Cycle Count Accuracy','LAST_UPDATE_DATE <= :Count date ending ');
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Accuracy',401,'ORG_CODE IN :Org ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORG_CODE','','Org','','','','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','','','','IN','Y','Y','','','','','1',401,'Cycle Count Accuracy','ORG_CODE IN :Org ');
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Accuracy',401,'REGION IN :Region ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','REGION','','Region','','','','','EIS_XXWC_CYCLE_ACC_COUNT_V','','','','','','IN','Y','Y','','','','','1',401,'Cycle Count Accuracy','REGION IN :Region ');
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Accuracy',401,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND (:OrgList is NULL  OR
          EXISTS  (SELECT 1
  from xxeis.eis_xxwc_param_parse_list x
  where x.list_name     =:OrgList
  and x.list_type     =''Org''
 and x.process_id    = :system.process_id
and  EXCACV.ORG_CODE =regexp_replace(x.list_value,''[^[a-z,A-Z,0-9]]*'')))
AND ( :ItemList is NULL  OR
          EXISTS  (SELECT 1
  from xxeis.eis_xxwc_param_parse_list x
  where x.list_name     =:ItemList
  and x.list_type     =''Item''
 and x.process_id    = :system.process_id
and  EXCACV.ITEM_NUMBER =regexp_replace(x.list_value,''[^[a-z,A-Z,0-9]]*'')))
AND ( :CatList is NULL  OR
          EXISTS  (SELECT 1
  from xxeis.eis_xxwc_param_parse_list x
  where x.list_name     =:CatList
  and x.list_type     =''Cat Class''
 and x.process_id    = :system.process_id
and  EXCACV.CAT_CALSS =regexp_replace(x.list_value,''[^[a-z,A-Z,0-9]]*'')))','1',401,'Cycle Count Accuracy','Free Text ');
--Inserting Report Sorts - Cycle Count Accuracy
--Inserting Report Triggers - Cycle Count Accuracy
xxeis.eis_rsc_ins.rt( 'Cycle Count Accuracy',401,'Declare
L_TEMP         varchar2(240);
L_ITEM_LIST     varchar2(240);
L_CAT_LIST     varchar2(240);
begin
L_TEMP         :=:OrgList ;
L_ITEM_LIST    :=:ItemList;
L_CAT_LIST    :=:CatList;
if l_temp is not null then
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID => :SYSTEM.PROCESS_ID,
                        P_LIST_NAME => L_TEMP ,
                        P_LIST_TYPE => ''Org''
                        );
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.g_flag_value:=''Y'';
else
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.g_flag_value:=''N'';
end if;
if L_ITEM_LIST is not null then
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID    => :SYSTEM.PROCESS_ID,
                        P_LIST_NAME    => L_ITEM_LIST,
                        P_LIST_TYPE    => ''Item''
                        );
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.G_ITEM_FLAG:=''Y'';
else
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.G_ITEM_FLAG:=''N'';
end if;
if L_CAT_LIST is not null then
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID    => :SYSTEM.PROCESS_ID,
                        P_LIST_NAME    => L_CAT_LIST,
                        P_LIST_TYPE    => ''Cat Class'');
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.g_cat_flag:=''Y'';
else
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.g_cat_flag:=''N'';
end if;
end;','B','Y','MR020532','');
xxeis.eis_rsc_ins.rt( 'Cycle Count Accuracy',401,'BEGIN
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.parse_cleanup_table(P_PROCESS_ID    => :SYSTEM.PROCESS_ID);
END;','A','Y','MR020532','');
--inserting report templates - Cycle Count Accuracy
--Inserting Report Portals - Cycle Count Accuracy
--inserting report dashboards - Cycle Count Accuracy
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Cycle Count Accuracy','401','EIS_XXWC_CYCLE_ACC_COUNT_V','EIS_XXWC_CYCLE_ACC_COUNT_V','N','');
--inserting report security - Cycle Count Accuracy
xxeis.eis_rsc_ins.rsec( 'Cycle Count Accuracy','401','','XXWC_INVENTORY_SUPER_USER',401,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Accuracy','401','','XXWC_INVENTORY_SPEC_SCC',401,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Accuracy','401','','XXWC_INV_PLANNER',401,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Accuracy','401','','XXWC_INVENTORY_CONTROL_INQUIRY',401,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Accuracy','401','','XXWC_INVENTORY_CONTROL_SR_MGR',401,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Accuracy','401','','XXWC_INV_ACCOUNTANT',401,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Accuracy','401','','HDS_INVNTRY',401,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Accuracy','201','','XXWC_PURCHASING_INQUIRY',401,'MR020532','','','');
--Inserting Report Pivots - Cycle Count Accuracy
xxeis.eis_rsc_ins.rpivot( 'Cycle Count Accuracy',401,'Summary By Line','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Summary By Line
xxeis.eis_rsc_ins.rpivot_dtls( 'Cycle Count Accuracy',401,'Summary By Line','COUNT_END_QUANTITY','DATA_FIELD','SUM','Lines Counted','1','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cycle Count Accuracy',401,'Summary By Line','COUNT_VAR_QUANTITY','DATA_FIELD','SUM','Variance Line','2','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cycle Count Accuracy',401,'Summary By Line','REGION','ROW_FIELD','','','2','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cycle Count Accuracy',401,'Summary By Line','DISTRICT','ROW_FIELD','','','3','1','');
--Inserting Report Summary Calculation Columns For Pivot- Summary By Line
xxeis.eis_rsc_ins.rpivot_sum_cal( 'Cycle Count Accuracy',401,'Summary By Line','100*<Variance Line>/<Lines Counted>','Variance','1');
xxeis.eis_rsc_ins.rpivot( 'Cycle Count Accuracy',401,'Summary By Region, by District','2','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Summary By Region, by District
xxeis.eis_rsc_ins.rpivot_dtls( 'Cycle Count Accuracy',401,'Summary By Region, by District','VAR_QUANTITY','DATA_FIELD','SUM','Variance Count','2','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cycle Count Accuracy',401,'Summary By Region, by District','DISTRICT','ROW_FIELD','','','2','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cycle Count Accuracy',401,'Summary By Region, by District','REGION','ROW_FIELD','','','1','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cycle Count Accuracy',401,'Summary By Region, by District','STARTING_QUANTITY','DATA_FIELD','SUM','Total Item Count','1','','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Summary By Region, by District
xxeis.eis_rsc_ins.rpivot_sum_cal( 'Cycle Count Accuracy',401,'Summary By Region, by District','100*<Variance Count>/<Total Item Count>','Varaiance%','1');
--Inserting Report   Version details- Cycle Count Accuracy
xxeis.eis_rsc_ins.rv( 'Cycle Count Accuracy','','Cycle Count Accuracy','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
