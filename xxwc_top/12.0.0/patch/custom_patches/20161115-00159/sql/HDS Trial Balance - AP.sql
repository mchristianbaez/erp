--Report Name            : HDS Trial Balance - AP
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_AP_TRIAL_BALANCE_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_AP_TRIAL_BALANCE_V
xxeis.eis_rsc_ins.v( 'EIS_AP_TRIAL_BALANCE_V',200,'This view displays information about the trial balances.','','','','XXEIS_RS_ADMIN','XXEIS','EIS AP Trial Balances','EATBV','','','VIEW','US','Y','');
--Delete Object Columns for EIS_AP_TRIAL_BALANCE_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_AP_TRIAL_BALANCE_V',200,FALSE);
--Inserting Object Columns for EIS_AP_TRIAL_BALANCE_V
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','OPERATING_UNIT',200,'Operating Unit','OPERATING_UNIT~OPERATING_UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','COMMON OUTPUTS','COMMON OUTPUTS','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','LEDGER_CURRENCY_CODE',200,'Ledger Currency Code','LEDGER_CURRENCY_CODE~LEDGER_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','COMMON OUTPUTS','COMMON OUTPUTS','Ledger Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','LEDGER_NAME',200,'Ledger Name','LEDGER_NAME~LEDGER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','COMMON OUTPUTS','COMMON OUTPUTS','Ledger Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','ACCOUNT',200,'Account','ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','COMMON OUTPUTS','COMMON OUTPUTS','Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','THIRD_PARTY_NAME',200,'Third Party Name','THIRD_PARTY_NAME~THIRD_PARTY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','COMMON OUTPUTS','COMMON OUTPUTS','Third Party Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','SRC_ACCTD_ROUNDED_REM_AMT',200,'Src Acctd Rounded Rem Amt','SRC_ACCTD_ROUNDED_REM_AMT~SRC_ACCTD_ROUNDED_REM_AMT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','COMMON OUTPUTS','COMMON OUTPUTS','Src Acctd Rounded Rem Amt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','SOURCE_TRX_CURR',200,'Source Trx Curr','SOURCE_TRX_CURR~SOURCE_TRX_CURR','','','','XXEIS_RS_ADMIN','VARCHAR2','COMMON OUTPUTS','COMMON OUTPUTS','Source Trx Curr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','LEDGER_SHORT_NAME',200,'Ledger Short Name','LEDGER_SHORT_NAME~LEDGER_SHORT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','COMMON OUTPUTS','COMMON OUTPUTS','Ledger Short Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','SOURCE_TRX_TYPE',200,'Source Trx Type','SOURCE_TRX_TYPE~SOURCE_TRX_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','COMMON OUTPUTS','COMMON OUTPUTS','Source Trx Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','SRC_ACCTD_ROUNDED_ORIG_AMT',200,'Src Acctd Rounded Orig Amt','SRC_ACCTD_ROUNDED_ORIG_AMT~SRC_ACCTD_ROUNDED_ORIG_AMT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','COMMON OUTPUTS','COMMON OUTPUTS','Src Acctd Rounded Orig Amt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','SOURCE_TRX_GL_DATE',200,'Source Trx Gl Date','SOURCE_TRX_GL_DATE~SOURCE_TRX_GL_DATE','','','','XXEIS_RS_ADMIN','VARCHAR2','COMMON OUTPUTS','COMMON OUTPUTS','Source Trx Gl Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','SOURCE_TRX_NUMBER',200,'Source Trx Number','SOURCE_TRX_NUMBER~SOURCE_TRX_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','COMMON OUTPUTS','COMMON OUTPUTS','Source Trx Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','INVOICE_DATE',200,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','VARCHAR2','Common Outputs','Common Outputs','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','INVOICE_TERMS',200,'Invoice Terms','INVOICE_TERMS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Terms','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','SUPPLIER_NUMBER',200,'Supplier Number','SUPPLIER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Supplier Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','THIRD_PARTY_SITE_NAME',200,'Third Party Site Name','THIRD_PARTY_SITE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Third Party Site Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','PROCESS_ID',200,'Process Id','PROCESS_ID','','','','XXEIS_RS_ADMIN','NUMBER','COMMON OUTPUTS','COMMON OUTPUTS','Process Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','CANCELLED_DATE',200,'Cancelled Date','CANCELLED_DATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cancelled Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','THIRD_PARTY_NUMBER',200,'Third Party Number','THIRD_PARTY_NUMBER~THIRD_PARTY_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','COMMON OUTPUTS','COMMON OUTPUTS','Third Party Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','COMMON_OUTPUT_ID',200,'Common Output Id','COMMON_OUTPUT_ID','','','','XXEIS_RS_ADMIN','NUMBER','COMMON OUTPUTS','COMMON OUTPUTS','Common Output Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','INVOICE_ID',200,'Invoice Id','INVOICE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','CODE_COMBINATION_ID',200,'Code Combination Id','CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','CONCATENATED_SEGMENTS',200,'Concatenated Segments','CONCATENATED_SEGMENTS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Concatenated Segments','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_TRIAL_BALANCE_V','COPYRIGHT',200,'Copyright','COPYRIGHT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Copyright','','','','US');
--Inserting Object Components for EIS_AP_TRIAL_BALANCE_V
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_TRIAL_BALANCE_V','AP_INVOICES',200,'AP_INVOICES_ALL','AI','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ap Invoices','N','Detailed invoice records','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_TRIAL_BALANCE_V','GL_CODE_COMBINATIONS_KFV',200,'GL_CODE_COMBINATIONS','GCCK','GCCK','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Gl Code Combinations Kfv','N','Account combinations','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_AP_TRIAL_BALANCE_V
xxeis.eis_rsc_ins.vcj( 'EIS_AP_TRIAL_BALANCE_V','AP_INVOICES','AI',200,'EATBV.INVOICE_ID','=','AI.INVOICE_ID(+)','AI.','','1','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_TRIAL_BALANCE_V','GL_CODE_COMBINATIONS_KFV','GCCK',200,'EATBV.CODE_COMBINATION_ID','=','GCCK.CODE_COMBINATION_ID(+)','GCCK.','','1','Y','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report LOV Data for HDS Trial Balance - AP
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS Trial Balance - AP
xxeis.eis_rsc_ins.lov( 200,'SELECT MEANING
FROM FND_LOOKUPS
WHERE lookup_type = ''YES_NO''','','AP Yes No Lov','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 200,'select xl.name  from XLA_TB_DEFINITIONS_vl XL, XXEIS.EIS_MO_ORG_TMP_V SOB where xl.ledger_id=sob.set_of_books_id','','EIS_AP_REPORT_DEFINITION','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 200,'select meaning from XLA_LOOKUPS WHERE lookup_type = ''XLA_TB_ACCT_BALANCE''','','EIS_AP_ACCOUNT_BALANCE','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 200,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','select vendor_name,vendor_name_alt,vendor_type_lookup_code,vendor_id from po_vendors','','Supplier Name','vendor name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report Data for HDS Trial Balance - AP
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS Trial Balance - AP
xxeis.eis_rsc_utility.delete_report_rows( 'HDS Trial Balance - AP' );
--Inserting Report - HDS Trial Balance - AP
xxeis.eis_rsc_ins.r( 200,'HDS Trial Balance - AP','','This report will give invoice balance, remaining balance for each liability account for the invoices whose accounting date is less than or equal to selected accounting date.  If the from date is entered then the invoices after that date will be excluded.  If the negative balances only option is selected then only negative balances will be shown.  The report can be used to reconcile with General Ledger Balances.
','','','','XXEIS_RS_ADMIN','EIS_AP_TRIAL_BALANCE_V','Y','','','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','Y','','','','','','','','US','','','','');
--Inserting Report Columns - HDS Trial Balance - AP
xxeis.eis_rsc_ins.rc( 'HDS Trial Balance - AP',200,'OPERATING_UNIT','Operating Unit','Operating Unit','','','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_TRIAL_BALANCE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Trial Balance - AP',200,'LEDGER_CURRENCY_CODE','Ledger Currency Code','Ledger Currency Code','','','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_TRIAL_BALANCE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Trial Balance - AP',200,'LEDGER_SHORT_NAME','Ledger Short Name','Ledger Short Name','','','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_TRIAL_BALANCE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Trial Balance - AP',200,'ACCOUNT','Account','Account','','','default','','8','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_TRIAL_BALANCE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Trial Balance - AP',200,'LEDGER_NAME','Ledger Name','Ledger Name','','','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_TRIAL_BALANCE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Trial Balance - AP',200,'SOURCE_TRX_CURR','Currency','Source Trx Curr','','','default','','14','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_TRIAL_BALANCE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Trial Balance - AP',200,'SOURCE_TRX_GL_DATE','Gl Date','Source Trx Gl Date','','','default','','13','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_TRIAL_BALANCE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Trial Balance - AP',200,'SOURCE_TRX_NUMBER','Transaction Number','Source Trx Number','','','default','','10','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_TRIAL_BALANCE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Trial Balance - AP',200,'SOURCE_TRX_TYPE','Transaction Type','Source Trx Type','','','default','','9','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_TRIAL_BALANCE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Trial Balance - AP',200,'SRC_ACCTD_ROUNDED_ORIG_AMT','Original Amount','Src Acctd Rounded Orig Amt','','~T~D~2','default','','15','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_TRIAL_BALANCE_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Trial Balance - AP',200,'THIRD_PARTY_NAME','Supplier Name','Third Party Name','','','default','','5','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_TRIAL_BALANCE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Trial Balance - AP',200,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','11','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_TRIAL_BALANCE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Trial Balance - AP',200,'INVOICE_TERMS','Invoice Terms','Invoice Terms','','','default','','12','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_TRIAL_BALANCE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Trial Balance - AP',200,'SUPPLIER_NUMBER','Supplier Number','Supplier Number','','','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_TRIAL_BALANCE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Trial Balance - AP',200,'THIRD_PARTY_SITE_NAME','Vendor Site','Third Party Site Name','','','default','','7','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_TRIAL_BALANCE_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Trial Balance - AP',200,'Remaining_Amount','Remaining Amount','Remaining Amount','NUMBER','~~~','default','','16','Y','Y','','','','','','(SUM(SRC_ACCTD_ROUNDED_REM_AMT))','XXEIS_RS_ADMIN','N','N','','EIS_AP_TRIAL_BALANCE_V','','','','US','');
--Inserting Report Parameters - HDS Trial Balance - AP
xxeis.eis_rsc_ins.rp( 'HDS Trial Balance - AP',200,'Operating Unit','Operating Unit','','IN','EIS_MULTI_OPERATING_UNIT_LOV','','VARCHAR2','N','Y','1','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AP_TRIAL_BALANCE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Trial Balance - AP',200,'Supplier Name','Supplier Name','','IN','Supplier Name','','VARCHAR2','N','Y','4','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AP_TRIAL_BALANCE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Trial Balance - AP',200,'Include Write Offs','Include Write Offs','','IN','AP Yes No Lov','''No''','VARCHAR2','Y','Y','5','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AP_TRIAL_BALANCE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Trial Balance - AP',200,'Report Definition','Report Definition','','IN','EIS_AP_REPORT_DEFINITION','','VARCHAR2','Y','Y','2','N','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AP_TRIAL_BALANCE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Trial Balance - AP',200,'Account Balance','Account Balance','','IN','EIS_AP_ACCOUNT_BALANCE','''Year to Date''','VARCHAR2','Y','Y','6','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AP_TRIAL_BALANCE_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Trial Balance - AP',200,'As of Date','As of Date','','IN','','','DATE','N','Y','3','Y','N','CURRENT_DATE','XXEIS_RS_ADMIN','Y','N','','End Date','','EIS_AP_TRIAL_BALANCE_V','','','US','');
--Inserting Dependent Parameters - HDS Trial Balance - AP
--Inserting Report Conditions - HDS Trial Balance - AP
xxeis.eis_rsc_ins.rcnh( 'HDS Trial Balance - AP',200,'PROCESS_ID = :SYSTEM.PROCESS_ID ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PROCESS_ID','','','','','','','EIS_AP_TRIAL_BALANCE_V','','','','','','EQUALS','Y','N','',':SYSTEM.PROCESS_ID','','','1',200,'HDS Trial Balance - AP','PROCESS_ID = :SYSTEM.PROCESS_ID ');
xxeis.eis_rsc_ins.rcnh( 'HDS Trial Balance - AP',200,'SRC_ACCTD_ROUNDED_REM_AMT <> ''0'' ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SRC_ACCTD_ROUNDED_REM_AMT','','','','','','','EIS_AP_TRIAL_BALANCE_V','','','','','','NOTEQUALS','Y','N','','''0''','','','1',200,'HDS Trial Balance - AP','SRC_ACCTD_ROUNDED_REM_AMT <> ''0'' ');
--Inserting Report Sorts - HDS Trial Balance - AP
xxeis.eis_rsc_ins.rs( 'HDS Trial Balance - AP',200,'SOURCE_TRX_NUMBER','ASC','XXEIS_RS_ADMIN','1','');
xxeis.eis_rsc_ins.rs( 'HDS Trial Balance - AP',200,'THIRD_PARTY_NAME','ASC','XXEIS_RS_ADMIN','2','');
--Inserting Report Triggers - HDS Trial Balance - AP
xxeis.eis_rsc_ins.rt( 'HDS Trial Balance - AP',200,'BEGIN



   xxeis.EIS_AP_TRIAL_BALANCE_PKG.GET_TRIAL_BAL_DATA



              ( p_process_id            => :SYSTEM.PROCESS_ID,

                p_operating_unit   => :Operating Unit,

                p_report_definition_dsp => :Report Definition,

                p_as_of_date    =>  :As of Date,

                p_supplier_name => :Supplier Name,

                p_incl_write_off_dsp => :Include Write Offs,

                p_acct_balance_dsp => :Account Balance );



END;','B','Y','XXEIS_RS_ADMIN','AQ');
--inserting report templates - HDS Trial Balance - AP
xxeis.eis_rsc_ins.r_tem( 'HDS Trial Balance - AP','HDS Trial Balance - AP','Seeded template for HDS Trial Balance - AP','','','','','','','','','','','HDS Trial Balance - AP.rtf','XXEIS_RS_ADMIN','X','','','Y','Y','','');
--Inserting Report Portals - HDS Trial Balance - AP
--inserting report dashboards - HDS Trial Balance - AP
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS Trial Balance - AP','200','EIS_AP_TRIAL_BALANCE_V','EIS_AP_TRIAL_BALANCE_V','N','');
--inserting report security - HDS Trial Balance - AP
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','HDS_AP_ADMIN_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','HDS_AP_ADMIN_US_GSCIWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','HDS_AP_DISBURSEMTS_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','HDS_AP_INQUIRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','HDS_AP_MGR_NOSUP_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','HDS_AP_TRNS_ENTRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','PAYABLES_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','XXWC_PAY_NO_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','XXWC_PAY_W_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','XXWC_PAY_DISBURSE',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','XXWC_PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','XXWC_PAY_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','XXWC_PAY_VENDOR_MSTR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','HDS_AP_MGR_NOSUP_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','HDS_AP_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','HDS_PYABLS_MNGR_CAN',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','HDS_PYBLS_MNGR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','20005','','XXWC_VIEW_ALL_EIS_REPORTS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','HDS_AP_MGR_NOSUP_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','HDS_AP_DISBURSEMTS_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','HDS_AP_ADMIN_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','HDS_PAYABLES_CLOSE_GSC_GLBL',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Trial Balance - AP','200','','HDS_PAYABLES_CLOSE_GLBL',200,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS Trial Balance - AP
xxeis.eis_rsc_ins.rpivot( 'HDS Trial Balance - AP',200,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Trial Balance - AP',200,'Pivot','OPERATING_UNIT','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Trial Balance - AP',200,'Pivot','ACCOUNT','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Trial Balance - AP',200,'Pivot','LEDGER_NAME','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Trial Balance - AP',200,'Pivot','SOURCE_TRX_CURR','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Trial Balance - AP',200,'Pivot','SOURCE_TRX_NUMBER','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Trial Balance - AP',200,'Pivot','SOURCE_TRX_TYPE','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Trial Balance - AP',200,'Pivot','SRC_ACCTD_ROUNDED_ORIG_AMT','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS Trial Balance - AP',200,'Pivot','THIRD_PARTY_NAME','ROW_FIELD','','','2','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- HDS Trial Balance - AP
xxeis.eis_rsc_ins.rv( 'HDS Trial Balance - AP','','HDS Trial Balance - AP','MT063505');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
