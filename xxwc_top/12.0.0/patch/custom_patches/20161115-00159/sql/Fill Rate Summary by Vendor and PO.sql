--Report Name            : Fill Rate Summary by Vendor and PO
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_1006488_AAJVIR_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1006488_AAJVIR_V
xxeis.eis_rsc_ins.v( 'XXEIS_1006488_AAJVIR_V',201,'Paste SQL View for Fill Rate Summary by Vendor and PO','1.0','','','SG019472','APPS','Fill Rate Summary by Vendor and PO View','X1AV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_1006488_AAJVIR_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1006488_AAJVIR_V',201,FALSE);
--Inserting Object Columns for XXEIS_1006488_AAJVIR_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1006488_AAJVIR_V','VENDOR_NUM',201,'','','','','','SG019472','VARCHAR2','','','Vendor Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1006488_AAJVIR_V','VENDOR_NAME',201,'','','','','','SG019472','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1006488_AAJVIR_V','PO_NUMBER',201,'','','','','','SG019472','VARCHAR2','','','Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1006488_AAJVIR_V','PO_CREATION_DATE',201,'','','','','','SG019472','DATE','','','Po Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1006488_AAJVIR_V','TOTAL_NUM_OF_PO_LINES',201,'','','','~T~D~2','','SG019472','NUMBER','','','Total Num Of Po Lines','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1006488_AAJVIR_V','TOTAL_AMT_OF_PO_LINES',201,'','','','~T~D~2','','SG019472','NUMBER','','','Total Amt Of Po Lines','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1006488_AAJVIR_V','BUYER_NAME',201,'','','','','','SG019472','VARCHAR2','','','Buyer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1006488_AAJVIR_V','BRANCH_NAME',201,'','','','','','SG019472','VARCHAR2','','','Branch Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1006488_AAJVIR_V','NUM_OF_LINES_ON_FIRST_RCPT',201,'','','','','','SG019472','NUMBER','','','Num Of Lines On First Rcpt','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1006488_AAJVIR_V','AMT_OF_LINES_ON_FIRST_RCPT',201,'','','','','','SG019472','NUMBER','','','Amt Of Lines On First Rcpt','','','','US');
--Inserting Object Components for XXEIS_1006488_AAJVIR_V
--Inserting Object Component Joins for XXEIS_1006488_AAJVIR_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report LOV Data for Fill Rate Summary by Vendor and PO
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Fill Rate Summary by Vendor and PO
xxeis.eis_rsc_ins.lov( 201,'select vendor_name from po_vendors','','EIS_PO_SUPPLIER_LOV','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report Data for Fill Rate Summary by Vendor and PO
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Fill Rate Summary by Vendor and PO
xxeis.eis_rsc_utility.delete_report_rows( 'Fill Rate Summary by Vendor and PO' );
--Inserting Report - Fill Rate Summary by Vendor and PO
xxeis.eis_rsc_ins.r( 201,'Fill Rate Summary by Vendor and PO','','','','','','SG019472','XXEIS_1006488_AAJVIR_V','Y','','select 
pov.segment1 vendor_num,
pov.vendor_name vendor_name,
pha.segment1 po_number,
pha.creation_date po_creation_date,
(CASE
WHEN pha.po_header_id is not null 
THEN 1
ELSE 0
END) total_num_of_PO_Lines,
mmt.actual_cost*mmt.primary_quantity Total_Amt_of_PO_Lines,
pav.agent_name buyer_name,
haou.name branch_name,
(CASE
WHEN trunc(mmt.transaction_date) = trunc(pha1.min_date)
THEN 1
ELSE 0
END  
) num_of_lines_on_first_rcpt,
(CASE
WHEN trunc(mmt.transaction_date) = trunc(pha1.min_date)
THEN (mmt.actual_cost*mmt.primary_quantity)
ELSE 0
END
) amt_of_lines_on_first_rcpt
from
(select 
pha.po_header_id,
min(mmt.transaction_date) min_date
from
apps.po_headers_all pha,
apps.po_lines_all pla,
apps.rcv_transactions rt,
apps.rcv_shipment_headers rsh,
apps.mtl_material_transactions mmt
where
pha.po_header_id = pla.po_header_id
and pla.po_line_id = rt.po_line_id
and rt.transaction_id = mmt.rcv_transaction_id
and rt.shipment_header_id = rsh.shipment_header_id
group by pha.po_header_id) pha1,
apps.po_vendors pov,
apps.po_headers_all pha,
apps.po_lines_all pla,
apps.rcv_transactions rt,
apps.rcv_shipment_headers rsh,
apps.mtl_material_transactions mmt,
apps.mtl_system_items_b msib,
apps.hr_all_organization_units haou,
apps.po_agents_v pav,
apps.mtl_transaction_types mtt,
apps.po_line_locations_all plla,
apps.org_organization_definitions ood
where
ood.organization_id = haou.organization_id
and pha.po_header_id = pha1.po_header_id
and pov.vendor_id = pha.vendor_id
and pha.po_header_id = pla.po_header_id
and pla.po_line_id = rt.po_line_id
and rt.shipment_header_id = rsh.shipment_header_id
and rt.transaction_id = mmt.rcv_transaction_id
and mmt.transaction_source_type_id=''1''
and mmt.organization_id =haou.organization_id
and msib.organization_id = mmt.organization_id
and msib.inventory_item_id = mmt.inventory_item_id
and pha.agent_id = pav.agent_id
and mmt.transaction_type_id = mtt.transaction_type_id
and pla.po_line_id = plla.po_line_id
AND NVL (pha.closed_code, ''OPEN'')  IN (''FINALLY CLOSED'', ''CLOSED'')
AND NVL (pha.cancel_flag, ''N'')= ''N''
and mtt.transaction_type_id = ''18'' --transaction type of ''PO Receipt'' only





','SG019472','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','','','','','','','','APPS','US','','','','');
--Inserting Report Columns - Fill Rate Summary by Vendor and PO
xxeis.eis_rsc_ins.rc( 'Fill Rate Summary by Vendor and PO',201,'PO_NUMBER','Po Number','','','','default','','3','N','','','','','','','','SG019472','N','N','','XXEIS_1006488_AAJVIR_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Fill Rate Summary by Vendor and PO',201,'PO_CREATION_DATE','Po Creation Date','','','DD-MON-RRRR','default','','4','N','','','','','','','','SG019472','N','N','','XXEIS_1006488_AAJVIR_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Fill Rate Summary by Vendor and PO',201,'BRANCH_NAME','Branch Name','','','','default','','6','N','','','','','','','','SG019472','N','N','','XXEIS_1006488_AAJVIR_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Fill Rate Summary by Vendor and PO',201,'VENDOR_NUM','Vendor Num','','','','default','','1','N','','','','','','','','SG019472','N','N','','XXEIS_1006488_AAJVIR_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Fill Rate Summary by Vendor and PO',201,'TOTAL_AMT_OF_PO_LINES','Total Amt Of Po Lines','','','$~,~.~2','default','','10','N','','','','','','','','SG019472','N','N','','XXEIS_1006488_AAJVIR_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Fill Rate Summary by Vendor and PO',201,'BUYER_NAME','Buyer Name','','','','default','','5','N','','','','','','','','SG019472','N','N','','XXEIS_1006488_AAJVIR_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Fill Rate Summary by Vendor and PO',201,'AMT_OF_LINES_ON_FIRST_RCPT','Amt Of Lines On First Rcpt','','','~,~.~2','default','','8','N','','','','','','','','SG019472','N','N','','XXEIS_1006488_AAJVIR_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Fill Rate Summary by Vendor and PO',201,'TOTAL_NUM_OF_PO_LINES','Total Num Of Po Lines','','','~,~.~0','default','','9','N','','','','','','','','SG019472','N','N','','XXEIS_1006488_AAJVIR_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Fill Rate Summary by Vendor and PO',201,'VENDOR_NAME','Vendor Name','','','','default','','2','N','','','','','','','','SG019472','N','N','','XXEIS_1006488_AAJVIR_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Fill Rate Summary by Vendor and PO',201,'NUM_OF_LINES_ON_FIRST_RCPT','Num Of Lines On First Rcpt','','','~,~.~0','default','','7','N','','','','','','','','SG019472','N','N','','XXEIS_1006488_AAJVIR_V','','','SUM','US','');
--Inserting Report Parameters - Fill Rate Summary by Vendor and PO
xxeis.eis_rsc_ins.rp( 'Fill Rate Summary by Vendor and PO',201,'Supplier','','VENDOR_NAME','IN','EIS_PO_SUPPLIER_LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','SG019472','Y','N','','','','XXEIS_1006488_AAJVIR_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Fill Rate Summary by Vendor and PO',201,'Start Date','','PO_CREATION_DATE','>=','','','DATE','N','Y','2','','Y','CONSTANT','SG019472','Y','N','','','','XXEIS_1006488_AAJVIR_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Fill Rate Summary by Vendor and PO',201,'End Date','','PO_CREATION_DATE','<=','','','DATE','N','Y','3','','Y','CONSTANT','SG019472','Y','N','','','','XXEIS_1006488_AAJVIR_V','','','US','');
--Inserting Dependent Parameters - Fill Rate Summary by Vendor and PO
--Inserting Report Conditions - Fill Rate Summary by Vendor and PO
xxeis.eis_rsc_ins.rcnh( 'Fill Rate Summary by Vendor and PO',201,'PO_CREATION_DATE <= :End Date ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PO_CREATION_DATE','','End Date','','','','','XXEIS_1006488_AAJVIR_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',201,'Fill Rate Summary by Vendor and PO','PO_CREATION_DATE <= :End Date ');
xxeis.eis_rsc_ins.rcnh( 'Fill Rate Summary by Vendor and PO',201,'PO_CREATION_DATE >= :Start Date ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PO_CREATION_DATE','','Start Date','','','','','XXEIS_1006488_AAJVIR_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',201,'Fill Rate Summary by Vendor and PO','PO_CREATION_DATE >= :Start Date ');
xxeis.eis_rsc_ins.rcnh( 'Fill Rate Summary by Vendor and PO',201,'VENDOR_NAME IN :Supplier ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NAME','','Supplier','','','','','XXEIS_1006488_AAJVIR_V','','','','','','IN','Y','Y','','','','','1',201,'Fill Rate Summary by Vendor and PO','VENDOR_NAME IN :Supplier ');
--Inserting Report Sorts - Fill Rate Summary by Vendor and PO
xxeis.eis_rsc_ins.rs( 'Fill Rate Summary by Vendor and PO',201,'VENDOR_NAME','','SG019472','1','');
xxeis.eis_rsc_ins.rs( 'Fill Rate Summary by Vendor and PO',201,'BRANCH_NAME','','SG019472','2','');
xxeis.eis_rsc_ins.rs( 'Fill Rate Summary by Vendor and PO',201,'BUYER_NAME','','SG019472','3','');
xxeis.eis_rsc_ins.rs( 'Fill Rate Summary by Vendor and PO',201,'PO_CREATION_DATE','','SG019472','4','');
xxeis.eis_rsc_ins.rs( 'Fill Rate Summary by Vendor and PO',201,'PO_NUMBER','','SG019472','5','');
--Inserting Report Triggers - Fill Rate Summary by Vendor and PO
--inserting report templates - Fill Rate Summary by Vendor and PO
--Inserting Report Portals - Fill Rate Summary by Vendor and PO
--inserting report dashboards - Fill Rate Summary by Vendor and PO
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Fill Rate Summary by Vendor and PO','201','XXEIS_1006488_AAJVIR_V','XXEIS_1006488_AAJVIR_V','N','');
--inserting report security - Fill Rate Summary by Vendor and PO
xxeis.eis_rsc_ins.rsec( 'Fill Rate Summary by Vendor and PO','201','','XXWC_PURCHASING_MGR',201,'SG019472','','','');
xxeis.eis_rsc_ins.rsec( 'Fill Rate Summary by Vendor and PO','201','','XXWC_PURCHASING_SR_MRG_WC',201,'SG019472','','','');
xxeis.eis_rsc_ins.rsec( 'Fill Rate Summary by Vendor and PO','201','','XXWC_PURCHASING_RPM',201,'SG019472','','','');
xxeis.eis_rsc_ins.rsec( 'Fill Rate Summary by Vendor and PO','201','','XXWC_PURCHASING_INQUIRY',201,'SG019472','','','');
xxeis.eis_rsc_ins.rsec( 'Fill Rate Summary by Vendor and PO','','LN053976','',201,'SG019472','','','');
xxeis.eis_rsc_ins.rsec( 'Fill Rate Summary by Vendor and PO','','TP022026','',201,'SG019472','','','');
xxeis.eis_rsc_ins.rsec( 'Fill Rate Summary by Vendor and PO','','10022763','',201,'SG019472','','','');
--Inserting Report Pivots - Fill Rate Summary by Vendor and PO
xxeis.eis_rsc_ins.rpivot( 'Fill Rate Summary by Vendor and PO',201,'Fill Rate by Vendor','1','0,0|1,1,0','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Fill Rate by Vendor
xxeis.eis_rsc_ins.rpivot_dtls( 'Fill Rate Summary by Vendor and PO',201,'Fill Rate by Vendor','BRANCH_NAME','PAGE_FIELD','','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Fill Rate Summary by Vendor and PO',201,'Fill Rate by Vendor','NUM_OF_LINES_ON_FIRST_RCPT','DATA_FIELD','SUM','','1','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Fill Rate Summary by Vendor and PO',201,'Fill Rate by Vendor','AMT_OF_LINES_ON_FIRST_RCPT','DATA_FIELD','SUM','','2','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Fill Rate Summary by Vendor and PO',201,'Fill Rate by Vendor','TOTAL_NUM_OF_PO_LINES','DATA_FIELD','SUM','','3','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Fill Rate Summary by Vendor and PO',201,'Fill Rate by Vendor','TOTAL_AMT_OF_PO_LINES','DATA_FIELD','SUM','','4','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Fill Rate Summary by Vendor and PO',201,'Fill Rate by Vendor','VENDOR_NUM','ROW_FIELD','','','1','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Fill Rate Summary by Vendor and PO',201,'Fill Rate by Vendor','VENDOR_NAME','ROW_FIELD','','','2','1','');
--Inserting Report Summary Calculation Columns For Pivot- Fill Rate by Vendor
xxeis.eis_rsc_ins.rpivot_sum_cal( 'Fill Rate Summary by Vendor and PO',201,'Fill Rate by Vendor','<X1AV.AMT_OF_LINES_ON_FIRST_RCPT>/<X1AV.TOTAL_AMT_OF_PO_LINES>','% of Line $ on First Rcpt','0');
xxeis.eis_rsc_ins.rpivot_sum_cal( 'Fill Rate Summary by Vendor and PO',201,'Fill Rate by Vendor','<X1AV.NUM_OF_LINES_ON_FIRST_RCPT>/<X1AV.TOTAL_NUM_OF_PO_LINES>','% of Lines on First Rcpt','0');
--Inserting Report   Version details- Fill Rate Summary by Vendor and PO
xxeis.eis_rsc_ins.rv( 'Fill Rate Summary by Vendor and PO','','Fill Rate Summary by Vendor and PO','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
