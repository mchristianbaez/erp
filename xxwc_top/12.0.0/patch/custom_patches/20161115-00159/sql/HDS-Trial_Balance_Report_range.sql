--Report Name            : HDS-Trial_Balance_Report_range
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_GL_TRIAL_BALANCE_ACTUAL_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_GL_TRIAL_BALANCE_ACTUAL_V
xxeis.eis_rsc_ins.v( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V',101,'This view shows details of trail balance actuals activity.','','','','XXEIS_RS_ADMIN','XXEIS','EIS GL Trial Balance Actual','EGTBAV','','','VIEW','US','Y','');
--Delete Object Columns for EIS_GL_TRIAL_BALANCE_ACTUAL_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_GL_TRIAL_BALANCE_ACTUAL_V',101,FALSE);
--Inserting Object Columns for EIS_GL_TRIAL_BALANCE_ACTUAL_V
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GL_ACCOUNT_STRING',101,'Gl Account String','GL_ACCOUNT_STRING~GL_ACCOUNT_STRING','','','GL Accounts LOV','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Gl Account String','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','PERIOD_BEGIN_BALANCE',101,'Period Begin Balance','PERIOD_BEGIN_BALANCE~PERIOD_BEGIN_BALANCE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Period Begin Balance','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','PERIOD_END_BALANCE',101,'Period End Balance','PERIOD_END_BALANCE~PERIOD_END_BALANCE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Period End Balance','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','PERIOD_END_DATE',101,'Date on which accounting period ends','PERIOD_END_DATE~PERIOD_END_DATE','','','','XXEIS_RS_ADMIN','DATE','GL_PERIODS','END_DATE','Period End Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','PERIOD_NAME',101,'Accounting period','PERIOD_NAME~PERIOD_NAME','','','GL Period Names LOV','XXEIS_RS_ADMIN','VARCHAR2','GL_BALANCES','PERIOD_NAME','Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','PERIOD_START_DATE',101,'Date on which accounting period begins','PERIOD_START_DATE~PERIOD_START_DATE','','','','XXEIS_RS_ADMIN','DATE','GL_PERIODS','START_DATE','Period Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','YEAR_ACTIVITY',101,'Year Activity','YEAR_ACTIVITY~YEAR_ACTIVITY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Year Activity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','YEAR_BEGIN_BALANCE',101,'Year Begin Balance','YEAR_BEGIN_BALANCE~YEAR_BEGIN_BALANCE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Year Begin Balance','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','YEAR_END_BALANCE',101,'Year End Balance','YEAR_END_BALANCE~YEAR_END_BALANCE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Year End Balance','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','YEAR_STARTING_PERIOD',101,'System generated accounting period name','YEAR_STARTING_PERIOD~YEAR_STARTING_PERIOD','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_PERIODS','PERIOD_NAME','Year Starting Period','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','LEDGER_CURRENCY',101,'Currency','LEDGER_CURRENCY~LEDGER_CURRENCY','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_LEDGERS','CURRENCY_CODE','Ledger Currency','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','LEDGER_NAME',101,'Ledger name','LEDGER_NAME~LEDGER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_LEDGERS','NAME','Ledger Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','CURRENCY_CODE',101,'Currency','CURRENCY_CODE','','','GL Currency Codes LOV','XXEIS_RS_ADMIN','VARCHAR2','GL_BALANCES','CURRENCY_CODE','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','PERIOD_ACTIVITY',101,'Period Activity','PERIOD_ACTIVITY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Period Activity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50328#PRODUCT',101,'Accounting Flexfield (KFF): Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GLCC#Product','50328','1014547','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50328#LOCATION',101,'Accounting Flexfield (KFF): Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GLCC#Location','50328','1014548','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50328#PROJECT_CODE',101,'Accounting Flexfield (KFF): Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#PROJECT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GLCC#Project Code','50328','1014551','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50328#COST_CENTER',101,'Accounting Flexfield (KFF): Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#COST_CENTER','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GLCC#Cost Center','50328','1014549','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50328#ACCOUNT',101,'Accounting Flexfield (KFF): Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GLCC#Account','50328','1014550','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','CHART_OF_ACCOUNTS_ID',101,'Key flexfield structure defining column','CHART_OF_ACCOUNTS_ID~CHART_OF_ACCOUNTS_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_CODE_COMBINATIONS','CHART_OF_ACCOUNTS_ID','Chart Of Accounts Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','PERIOD_SET_NAME',101,'Accounting calendar name','PERIOD_SET_NAME~PERIOD_SET_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_PERIODS','PERIOD_SET_NAME','Period Set Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','LEDGER_ID',101,'Ledger defining column','LEDGER_ID~LEDGER_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_LEDGERS','LEDGER_ID','Ledger Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','CODE_COMBINATION_ID',101,'Key flexfield combination defining column','CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_CODE_COMBINATIONS','CODE_COMBINATION_ID','Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','ACCTD_PERIOD_ACTIVITY',101,'Acctd Period Activity','ACCTD_PERIOD_ACTIVITY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Acctd Period Activity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','ACCTD_PERIOD_BEGIN_BALANCE',101,'Acctd Period Begin Balance','ACCTD_PERIOD_BEGIN_BALANCE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Acctd Period Begin Balance','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','ACCTD_YEAR_END_BALANCE',101,'Acctd Year End Balance','ACCTD_YEAR_END_BALANCE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Acctd Year End Balance','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','FUNCTIONAL_CURRENCY_CODE',101,'Functional Currency Code','FUNCTIONAL_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Functional Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLBY_ACTUAL_FLAG',101,'Glby Actual Flag','GLBY_ACTUAL_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Glby Actual Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLBY_CODE_COMBINATION_ID',101,'Glby Code Combination Id','GLBY_CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Glby Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLBY_CURRENCY_CODE',101,'Glby Currency Code','GLBY_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Glby Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLBY_PERIOD_NAME',101,'Glby Period Name','GLBY_PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Glby Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLBY_SET_OF_BOOKS_ID',101,'Glby Set Of Books Id','GLBY_SET_OF_BOOKS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Glby Set Of Books Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLBY_TRANSLATED_FLAG',101,'Glby Translated Flag','GLBY_TRANSLATED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Glby Translated Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLB_ACTUAL_FLAG',101,'Glb Actual Flag','GLB_ACTUAL_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Glb Actual Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLB_CODE_COMBINATION_ID',101,'Glb Code Combination Id','GLB_CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Glb Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLB_SET_OF_BOOKS_ID',101,'Glb Set Of Books Id','GLB_SET_OF_BOOKS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Glb Set Of Books Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLB_TRANSLATED_FLAG',101,'Glb Translated Flag','GLB_TRANSLATED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Glb Translated Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GPC_PERIOD_NAME',101,'Gpc Period Name','GPC_PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gpc Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GPC_PERIOD_SET_NAME',101,'Gpc Period Set Name','GPC_PERIOD_SET_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gpc Period Set Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GPO_PERIOD_SET_NAME',101,'Gpo Period Set Name','GPO_PERIOD_SET_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gpo Period Set Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GPY_PERIOD_SET_NAME',101,'Gpy Period Set Name','GPY_PERIOD_SET_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gpy Period Set Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','PERIOD_NET',101,'Period Net','PERIOD_NET','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Period Net','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','PERIOD_NUM',101,'Period Num','PERIOD_NUM','','','','XXEIS_RS_ADMIN','NUMBER','','','Period Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','PERIOD_OFFSET',101,'Period Offset','PERIOD_OFFSET','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Offset','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','PERIOD_TYPE',101,'Period Type','PERIOD_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','PERIOD_YEAR',101,'Period Year','PERIOD_YEAR','','','','XXEIS_RS_ADMIN','NUMBER','','','Period Year','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','PROJECT_TO_DATE',101,'Project To Date','PROJECT_TO_DATE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Project To Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','PROJECT_TO_DATE_NET',101,'Project To Date Net','PROJECT_TO_DATE_NET','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Project To Date Net','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','QUARTER_NUM',101,'Quarter Num','QUARTER_NUM','','','','XXEIS_RS_ADMIN','NUMBER','','','Quarter Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','QUARTER_OFFSET',101,'Quarter Offset','QUARTER_OFFSET','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Quarter Offset','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','QUARTER_TO_DATE',101,'Quarter To Date','QUARTER_TO_DATE','','','','XXEIS_RS_ADMIN','NUMBER','','','Quarter To Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','QUARTER_TO_DATE_NET',101,'Quarter To Date Net','QUARTER_TO_DATE_NET','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Quarter To Date Net','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','SET_OF_BOOKS_ID',101,'Set Of Books Id','SET_OF_BOOKS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Set Of Books Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','SET_OF_BOOKS_NAME',101,'Set Of Books Name','SET_OF_BOOKS_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Set Of Books Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','SET_OF_BOOK_CURRENCY',101,'Set Of Book Currency','SET_OF_BOOK_CURRENCY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Set Of Book Currency','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','COPYRIGHT',101,'Copyright','COPYRIGHT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Copyright','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#Branch',101,'Descriptive flexfield (DFF): GL Accounts Column Name: Branch','GLCC#Branch','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','ATTRIBUTE1','Glcc#Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50328#ACCOUNT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#ACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GLCC#Account Descr','50328','1014550','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50328#COST_CENTER#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#COST_CENTER#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GLCC#Cost Center Descr','50328','1014549','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50328#FUTURE_USE',101,'Accounting Flexfield (KFF): Segment ''Future Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#FUTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GLCC#Future Use','50328','1014552','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50328#FUTURE_USE#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Future Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#FUTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GLCC#Future Use Descr','50328','1014552','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50328#FUTURE_USE_2',101,'Accounting Flexfield (KFF): Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#FUTURE_USE_2','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GLCC#Future Use 2','50328','1014948','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50328#FUTURE_USE_2#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#FUTURE_USE_2#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GLCC#Future Use 2 Descr','50328','1014948','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50328#LOCATION#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#LOCATION#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GLCC#Location Descr','50328','1014548','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50328#PRODUCT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#PRODUCT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GLCC#Product Descr','50328','1014547','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50328#PROJECT_CODE#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GLCC#50328#PROJECT_CODE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GLCC#Project Code Descr','50328','1014551','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50368#ACCOUNT',101,'Accounting Flexfield (KFF): Segment ''Account'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GLCC#Account','50368','1014599','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50368#ACCOUNT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Account'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#ACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GLCC#Account Descr','50368','1014599','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50368#DEPARTMENT',101,'Accounting Flexfield (KFF): Segment ''Department'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#DEPARTMENT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GLCC#Department','50368','1014598','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50368#DEPARTMENT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Department'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#DEPARTMENT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GLCC#Department Descr','50368','1014598','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50368#DIVISION',101,'Accounting Flexfield (KFF): Segment ''Division'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#DIVISION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GLCC#Division','50368','1014597','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50368#DIVISION#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Division'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#DIVISION#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GLCC#Division Descr','50368','1014597','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50368#FUTURE_USE',101,'Accounting Flexfield (KFF): Segment ''Future Use'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#FUTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GLCC#Future Use','50368','1014601','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50368#FUTURE_USE#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Future Use'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#FUTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GLCC#Future Use Descr','50368','1014601','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50368#PRODUCT',101,'Accounting Flexfield (KFF): Segment ''Product'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GLCC#Product','50368','1014596','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50368#PRODUCT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Product'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#PRODUCT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GLCC#Product Descr','50368','1014596','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50368#SUBACCOUNT',101,'Accounting Flexfield (KFF): Segment ''SubAccount'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#SUBACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GLCC#SubAccount','50368','1014600','','US');
xxeis.eis_rsc_ins.vc( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GLCC#50368#SUBACCOUNT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''SubAccount'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GLCC#50368#SUBACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GLCC#SubAccount Descr','50368','1014600','','US');
--Inserting Object Components for EIS_GL_TRIAL_BALANCE_ACTUAL_V
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GL_LEDGERS',101,'GL_LEDGERS','LED','LED','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ledger Definition','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GL_CODE_COMBINATIONS_KFV',101,'GL_CODE_COMBINATIONS','GLCC','GLCC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','GL Balance Account','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GL_PERIODS',101,'GL_PERIODS','GPC','GPC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Calendar Period Definitions','','','','','','Y',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_GL_TRIAL_BALANCE_ACTUAL_V
xxeis.eis_rsc_ins.vcj( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GL_PERIODS','GPC',101,'EGTBAV.PERIOD_NAME','=','GPC.PERIOD_NAME(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GL_PERIODS','GPC',101,'EGTBAV.PERIOD_SET_NAME','=','GPC.PERIOD_SET_NAME(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GL_LEDGERS','LED',101,'EGTBAV.LEDGER_NAME','=','LED.NAME(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_TRIAL_BALANCE_ACTUAL_V','GL_CODE_COMBINATIONS_KFV','GLCC',101,'EGTBAV.CODE_COMBINATION_ID','=','GLCC.CODE_COMBINATION_ID(+)','','','','','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report LOV Data for HDS-Trial_Balance_Report_range
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS-Trial_Balance_Report_range
xxeis.eis_rsc_ins.lov( 101,'select  per.period_name , led.name ledger_name, per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     GL_SECURITY_PKG.VALIDATE_ACCESS(led.ledger_ID) = ''TRUE''
and     led.accounted_period_type = per.period_type','null','EIS_GL_PERIOD_LOV','Derives GL Periods based on the corresponding setups in the sets of books','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					apps.fnd_flex_value_sets ffvs , 
					apps.fnd_flex_values ffv, 
					apps.fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_ACCOUNT'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
	 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT4'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value','','XXCUS_GL_ACCOUNT','XXCUS_GL_ACCOUNT','MM050208',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report Data for HDS-Trial_Balance_Report_range
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS-Trial_Balance_Report_range
xxeis.eis_rsc_utility.delete_report_rows( 'HDS-Trial_Balance_Report_range' );
--Inserting Report - HDS-Trial_Balance_Report_range
xxeis.eis_rsc_ins.r( 101,'HDS-Trial_Balance_Report_range','','','','','','KP012542','EIS_GL_TRIAL_BALANCE_ACTUAL_V','Y','','','KP012542','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - HDS-Trial_Balance_Report_range
xxeis.eis_rsc_ins.rc( 'HDS-Trial_Balance_Report_range',101,'GLCC#50328#LOCATION','GLCC#Location','Accounting Flexfield (KFF): Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','','','default','','2','N','Y','','','','','','','KP012542','N','N','','EIS_GL_TRIAL_BALANCE_ACTUAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Trial_Balance_Report_range',101,'GLCC#50328#PRODUCT','GLCC#Product','Accounting Flexfield (KFF): Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','','','default','','1','N','Y','','','','','','','KP012542','N','N','','EIS_GL_TRIAL_BALANCE_ACTUAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Trial_Balance_Report_range',101,'GLCC#50328#PROJECT_CODE','GLCC#Project Code','Accounting Flexfield (KFF): Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','','','default','','5','N','Y','','','','','','','KP012542','N','N','','EIS_GL_TRIAL_BALANCE_ACTUAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Trial_Balance_Report_range',101,'PERIOD_NAME','Period Name','Accounting period','','','default','','6','N','Y','','','','','','','KP012542','N','N','','EIS_GL_TRIAL_BALANCE_ACTUAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Trial_Balance_Report_range',101,'YEAR_END_BALANCE','Year End Balance','Year End Balance','','~T~D~2','default','','7','N','Y','','','','','','','KP012542','N','N','','EIS_GL_TRIAL_BALANCE_ACTUAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Trial_Balance_Report_range',101,'LEDGER_CURRENCY','Ledger Currency','Currency','','','default','','8','N','Y','','','','','','','KP012542','N','N','','EIS_GL_TRIAL_BALANCE_ACTUAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Trial_Balance_Report_range',101,'GLCC#50328#ACCOUNT','GLCC#Account','Accounting Flexfield (KFF): Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','','','default','','4','N','Y','','','','','','','KP012542','N','N','','EIS_GL_TRIAL_BALANCE_ACTUAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Trial_Balance_Report_range',101,'GLCC#50328#COST_CENTER','GLCC#Cost Center','Accounting Flexfield (KFF): Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','','','default','','3','N','Y','','','','','','','KP012542','N','N','','EIS_GL_TRIAL_BALANCE_ACTUAL_V','','','','US','');
--Inserting Report Parameters - HDS-Trial_Balance_Report_range
xxeis.eis_rsc_ins.rp( 'HDS-Trial_Balance_Report_range',101,'Period Name','Period Name','PERIOD_NAME','IN','EIS_GL_PERIOD_LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','KP012542','Y','N','','','','EIS_GL_TRIAL_BALANCE_ACTUAL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Trial_Balance_Report_range',101,'Account To','Account To','GLCC#50328#ACCOUNT','<=','XXCUS_GL_ACCOUNT','','VARCHAR2','N','Y','3','N','Y','CONSTANT','KP012542','Y','N','','','','EIS_GL_TRIAL_BALANCE_ACTUAL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Trial_Balance_Report_range',101,'Account From','Account From','GLCC#50328#ACCOUNT','>=','XXCUS_GL_ACCOUNT','','VARCHAR2','N','Y','2','N','Y','CONSTANT','KP012542','Y','N','','','','EIS_GL_TRIAL_BALANCE_ACTUAL_V','','','US','');
--Inserting Dependent Parameters - HDS-Trial_Balance_Report_range
--Inserting Report Conditions - HDS-Trial_Balance_Report_range
xxeis.eis_rsc_ins.rcnh( 'HDS-Trial_Balance_Report_range',101,'EGTBAV.GLCC#50328#ACCOUNT <= :Account From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GLCC#50328#ACCOUNT','','Account From','','','','','EIS_GL_TRIAL_BALANCE_ACTUAL_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',101,'HDS-Trial_Balance_Report_range','EGTBAV.GLCC#50328#ACCOUNT <= :Account From ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Trial_Balance_Report_range',101,'PERIOD_NAME IN :Period Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PERIOD_NAME','','Period Name','','','','','EIS_GL_TRIAL_BALANCE_ACTUAL_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Trial_Balance_Report_range','PERIOD_NAME IN :Period Name ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Trial_Balance_Report_range',101,'YEAR_END_BALANCE <> 0 ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','YEAR_END_BALANCE','','','','','','','EIS_GL_TRIAL_BALANCE_ACTUAL_V','','','','','','NOTEQUALS','Y','N','','0','','','1',101,'HDS-Trial_Balance_Report_range','YEAR_END_BALANCE <> 0 ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Trial_Balance_Report_range',101,'EGTBAV.GLCC#50328#ACCOUNT >= Account To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GLCC#50328#ACCOUNT','','Account To','','','','','EIS_GL_TRIAL_BALANCE_ACTUAL_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',101,'HDS-Trial_Balance_Report_range','EGTBAV.GLCC#50328#ACCOUNT >= Account To');
--Inserting Report Sorts - HDS-Trial_Balance_Report_range
--Inserting Report Triggers - HDS-Trial_Balance_Report_range
--inserting report templates - HDS-Trial_Balance_Report_range
--Inserting Report Portals - HDS-Trial_Balance_Report_range
--inserting report dashboards - HDS-Trial_Balance_Report_range
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS-Trial_Balance_Report_range','101','EIS_GL_TRIAL_BALANCE_ACTUAL_V','EIS_GL_TRIAL_BALANCE_ACTUAL_V','N','');
--inserting report security - HDS-Trial_Balance_Report_range
xxeis.eis_rsc_ins.rsec( 'HDS-Trial_Balance_Report_range','101','','XXCUS_GL_ACCOUNTANT_CAD',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Trial_Balance_Report_range','101','','XXCUS_GL_ACCOUNTANT_CAD_PVF',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Trial_Balance_Report_range','101','','XXCUS_GL_ACCOUNTANT_GLOBAL',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Trial_Balance_Report_range','101','','GNRL_LDGR_LTMR_ACCNTNT',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Trial_Balance_Report_range','101','','XXCUS_GL_ACCOUNTANT_USD',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Trial_Balance_Report_range','101','','XXCUS_GL_ACCOUNTANT_USD_PVF',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Trial_Balance_Report_range','101','','XXCUS_GL_INQUIRY',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Trial_Balance_Report_range','101','','HDS GL INQUIRY',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Trial_Balance_Report_range','101','','GNRL_LDGR_LTMR_NQR',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Trial_Balance_Report_range','101','','XXCUS_GL_INQUIRY_PVF',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Trial_Balance_Report_range','101','','XXCUS_GL_MANAGER',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Trial_Balance_Report_range','101','','XXCUS_GL_MANAGER_GLOBAL',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Trial_Balance_Report_range','101','','XXCUS_GL_MANAGER_PVF',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Trial_Balance_Report_range','101','','HDS_CAD_MNTH_END_PROCS',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Trial_Balance_Report_range','101','','GNRL_LDGR_FSS',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Trial_Balance_Report_range','101','','GNRL_LDGR_LTMR_FSS',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Trial_Balance_Report_range','101','','HDS_GNRL_LDGR_CAD',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Trial_Balance_Report_range','101','','HDS_GNRL_LDGR_SPR_USR',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Trial_Balance_Report_range','20005','','XXWC_VIEW_ALL_EIS_REPORTS',101,'KP012542','','','');
--Inserting Report Pivots - HDS-Trial_Balance_Report_range
--Inserting Report   Version details- HDS-Trial_Balance_Report_range
xxeis.eis_rsc_ins.rv( 'HDS-Trial_Balance_Report_range','','HDS-Trial_Balance_Report_range','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
