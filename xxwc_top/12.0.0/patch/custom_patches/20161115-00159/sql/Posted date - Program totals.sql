--Report Name            : Posted date - Program totals
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_1491489_GHPFSA_V
prompt Table type Objects cannot be imported
set scan off define off
prompt Creating Object APPS.XXEIS_1491489_GHPFSA_V
set scan off define off
DECLARE
mod_exist varchar2(1);
stmt CLOB;
v_objlength INTEGER := 0;
v_offset INTEGER := 1;
v_sql_long DBMS_SQL.varchar2s;
v_sql_long_count INTEGER := 1;
c NUMBER;
r NUMBER;
l_stmt varchar2(32000);
l_view_cur pls_integer;
l_view_rows NUMBER;
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
dbms_lob.createtemporary(stmt,TRUE);
dbms_lob.write( lob_loc   => stmt,amount    => length('create or replace view APPS.XXEIS_1491489_GHPFSA_V
 ("CARD_PROGRAM_ID","POSTED_DATE","BILLED_AMOUNT","TRANSACTION_TYPE"
) as '),offset    => 1, BUFFER    => 'create or replace view APPS.XXEIS_1491489_GHPFSA_V
 ("CARD_PROGRAM_ID","POSTED_DATE","BILLED_AMOUNT","TRANSACTION_TYPE"
) as ');
l_stmt :=  'select card_program_id
         ,posted_date
         ,billed_amount
         ,transaction_type
from ap.ap_credit_card_trxns_all

';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
v_objlength := DBMS_LOB.getlength (stmt);
IF v_objlength <= 32000 THEN 
EXECUTE IMMEDIATE to_char(stmt);
ELSE
v_offset := 1; 
WHILE v_offset <= v_objlength
LOOP
-- each record is 256 bytes from the LOB
v_sql_long (v_sql_long_count) := DBMS_LOB.SUBSTR (stmt, 256, v_offset);
-- increment the record count
v_sql_long_count := v_sql_long_count + 1;
-- figure out the new offset
v_offset := v_offset + 256;
END LOOP;
BEGIN
  -- open cursor
 c := DBMS_SQL.open_cursor;
 -- parse VARCHAR2S table
 DBMS_SQL.parse (c, v_sql_long, 1, v_sql_long_count - 1, FALSE, 1);
 -- Execute the cursor
r := DBMS_SQL.EXECUTE (c);
-- Close the cursor
 DBMS_SQL.close_cursor (c);
 END;
END IF;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
prompt Creating Object Data XXEIS_1491489_GHPFSA_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1491489_GHPFSA_V
xxeis.eis_rsc_ins.v( 'XXEIS_1491489_GHPFSA_V',91000,'Paste SQL View for Posted date – Program totals','1.0','','','ID020048','APPS','Posted date – Program totals View','X1GV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_1491489_GHPFSA_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1491489_GHPFSA_V',91000,FALSE);
--Inserting Object Columns for XXEIS_1491489_GHPFSA_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1491489_GHPFSA_V','POSTED_DATE',91000,'','','','','','ID020048','DATE','','','Posted Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1491489_GHPFSA_V','BILLED_AMOUNT',91000,'','','','~T~D~2','','ID020048','NUMBER','','','Billed Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1491489_GHPFSA_V','CARD_PROGRAM_ID',91000,'','','','','','ID020048','NUMBER','','','Card Program Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1491489_GHPFSA_V','TRANSACTION_TYPE',91000,'','','','','','ID020048','VARCHAR2','','','Transaction Type','','','','US');
--Inserting Object Components for XXEIS_1491489_GHPFSA_V
--Inserting Object Component Joins for XXEIS_1491489_GHPFSA_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report Posted date - Program totals
prompt Creating Report Data for Posted date - Program totals
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Posted date - Program totals
xxeis.eis_rsc_utility.delete_report_rows( 'Posted date - Program totals' );
--Inserting Report - Posted date - Program totals
xxeis.eis_rsc_ins.r( 91000,'Posted date - Program totals','','This Report is copied from Posted date – Program totals.This report provides the total per card program per post date.','','','','AB063501','XXEIS_1491489_GHPFSA_V','Y','','','AB063501','','N','HDS Standard Reports','','CSV,EXCEL,Pivot Excel,','Y','','','','','','N','','US','','Posted date – Program totals','','');
--Inserting Report Columns - Posted date - Program totals
xxeis.eis_rsc_ins.rc( 'Posted date - Program totals',91000,'POSTED_DATE','Posted Date','','','','default','','1','N','Y','','','','','','','AB063501','N','N','','XXEIS_1491489_GHPFSA_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Posted date - Program totals',91000,'Card_Program_Name','Card Program Name','','VARCHAR2','','default','','2','Y','Y','','','','','','(case when card_program_id = ''10000'' then ''HDS Company Pay Program'' when card_program_id = ''10001'' then ''WC PNC Inventory Card Program'' when card_program_id = ''10002'' then ''FM PNC Employee-Pay Card Program'' when card_program_id = ''10003'' then ''CB PNC Employee-Pay Card Program'' when card_program_id = ''10004'' then ''WW PNC Employee-Pay Card Program'' end)','AB063501','N','N','','','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Posted date - Program totals',91000,'Amount','Amount','','NUMBER','~~~','default','','3','Y','Y','','','','','','(sum(billed_amount))','AB063501','N','N','','','','','','US','');
--Inserting Report Parameters - Posted date - Program totals
xxeis.eis_rsc_ins.rp( 'Posted date - Program totals',91000,'Post Date','','POSTED_DATE','IN','','','DATE','N','Y','1','Y','Y','CONSTANT','AB063501','Y','N','','','','XXEIS_1491489_GHPFSA_V','','','US','');
--Inserting Dependent Parameters - Posted date - Program totals
--Inserting Report Conditions - Posted date - Program totals
xxeis.eis_rsc_ins.rcnh( 'Posted date - Program totals',91000,'X1GV.TRANSACTION_TYPE NOT IN ''31'', ''30'', ''62'' ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','NOTIN','Y','N','X1GV.TRANSACTION_TYPE','''31'', ''30'', ''62''','','','1',91000,'Posted date - Program totals','X1GV.TRANSACTION_TYPE NOT IN ''31'', ''30'', ''62'' ');
xxeis.eis_rsc_ins.rcnh( 'Posted date - Program totals',91000,'Free Text ','FREE_TEXT','','','N','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','group by card_program_id, posted_date
order by posted_date','1',91000,'Posted date - Program totals','Free Text ');
xxeis.eis_rsc_ins.rcnh( 'Posted date - Program totals',91000,'X1GV.POSTED_DATE IN Post Date','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','POSTED_DATE','','Post Date','','','','','XXEIS_1491489_GHPFSA_V','','','','','','IN','Y','Y','','','','','1',91000,'Posted date - Program totals','X1GV.POSTED_DATE IN Post Date');
--Inserting Report Sorts - Posted date - Program totals
--Inserting Report Triggers - Posted date - Program totals
--inserting report templates - Posted date - Program totals
--Inserting Report Portals - Posted date - Program totals
--inserting report dashboards - Posted date - Program totals
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Posted date - Program totals','91000','XXEIS_1491489_GHPFSA_V','XXEIS_1491489_GHPFSA_V','N','');
--inserting report security - Posted date - Program totals
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','HDS_OIE_USER',91000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','HDS_AP_DISBURSEMTS_US_GSC',91000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','HDS_AP_DISBURSEMTS_CAD_GSC',91000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','HDS_PAYABLES_CLOSE_GLBL',91000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','XXWC_PAY_W_CALENDAR',91000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','XXWC_PAY_NO_CALENDAR',91000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','HDS_AP_ADMIN_US_IWO',91000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','HDS_AP_ADMIN_US_GSC',91000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','HDS_AP_ADMIN_CAD_GSC',91000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','XXWC_PAY_DISBURSE',91000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',91000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','XXWC_PAY_VENDOR_MSTR',91000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','XXWC_PAY_VENDOR_BANK_DETAILS',91000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','HDS_AP_TRNS_ENTRY_US_GSC',91000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',91000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','XXWC_PAY_MANAGER',91000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','HDS_AP_MGR_NOSUP_US_GSC',91000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','HDS_AP_MANAGER',91000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','XXWC_PAYABLES_INQUIRY',91000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'Posted date - Program totals','200','','HDS_AP_INQUIRY_US_GSC',91000,'AB063501','','','');
--Inserting Report Pivots - Posted date - Program totals
--Inserting Report   Version details- Posted date - Program totals
xxeis.eis_rsc_ins.rv( 'Posted date - Program totals','','Posted date – Program totals','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
