--Report Name            : Transfer Detail Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_PO_TRANSFER_DTL_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_PO_TRANSFER_DTL_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_PO_TRANSFER_DTL_V',201,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Po Transfer Dtl V','EXPTDV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_PO_TRANSFER_DTL_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_PO_TRANSFER_DTL_V',201,FALSE);
--Inserting Object Columns for EIS_XXWC_PO_TRANSFER_DTL_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','EXT$',201,'Ext$','EXT$','','','','ANONYMOUS','NUMBER','','','Ext$','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','ITEM_COST',201,'Item Cost','ITEM_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Item Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','CAT_CLASS_DESC',201,'Cat Class Desc','CAT_CLASS_DESC','','','','ANONYMOUS','VARCHAR2','','','Cat Class Desc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','CAT_CLASS',201,'Cat Class','CAT_CLASS','','','','ANONYMOUS','VARCHAR2','','','Cat Class','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_METHOD',201,'Ship Method','SHIP_METHOD','','','','ANONYMOUS','VARCHAR2','','','Ship Method','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','ORDER_STATUS',201,'Order Status','ORDER_STATUS','','','','ANONYMOUS','VARCHAR2','','','Order Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_TO_STATE',201,'Ship To State','SHIP_TO_STATE','','','','ANONYMOUS','VARCHAR2','','','Ship To State','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_TO_CITY',201,'Ship To City','SHIP_TO_CITY','','','','ANONYMOUS','VARCHAR2','','','Ship To City','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_FROM_STATE',201,'Ship From State','SHIP_FROM_STATE','','','','ANONYMOUS','VARCHAR2','','','Ship From State','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_FROM_CITY',201,'Ship From City','SHIP_FROM_CITY','','','','ANONYMOUS','VARCHAR2','','','Ship From City','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_DATE',201,'Ship Date','SHIP_DATE','','','','ANONYMOUS','DATE','','','Ship Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','ORDERED_DATE',201,'Ordered Date','ORDERED_DATE','','','','ANONYMOUS','DATE','','','Ordered Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','UOM',201,'Uom','UOM','','','','ANONYMOUS','VARCHAR2','','','Uom','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','QTY_DELIVERED',201,'Qty Delivered','QTY_DELIVERED','','~T~D~2','','ANONYMOUS','NUMBER','','','Qty Delivered','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','ORD_QTY',201,'Ord Qty','ORD_QTY','','~T~D~2','','ANONYMOUS','NUMBER','','','Ord Qty','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SOURCE',201,'Source','SOURCE','','','','ANONYMOUS','VARCHAR2','','','Source','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','VENDOR',201,'Vendor','VENDOR','','','','ANONYMOUS','VARCHAR2','','','Vendor','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','VENDOR_NUMBER',201,'Vendor Number','VENDOR_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SUPPLIER_ITEM_NUMBER',201,'Supplier Item Number','SUPPLIER_ITEM_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Supplier Item Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','ITEM_DESCRIPTION',201,'Item Description','ITEM_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','ITEM',201,'Item','ITEM','','','','ANONYMOUS','VARCHAR2','','','Item','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','BUYER',201,'Buyer','BUYER','','','','ANONYMOUS','VARCHAR2','','','Buyer','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_FROM_NAME',201,'Ship From Name','SHIP_FROM_NAME','','','','ANONYMOUS','VARCHAR2','','','Ship From Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_FROM_ORG',201,'Ship From Org','SHIP_FROM_ORG','','','','ANONYMOUS','VARCHAR2','','','Ship From Org','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_TO_ORG',201,'Ship To Org','SHIP_TO_ORG','','','','ANONYMOUS','VARCHAR2','','','Ship To Org','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','ORDER_LINE',201,'Order Line','ORDER_LINE','','','','ANONYMOUS','VARCHAR2','','','Order Line','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','ISO',201,'Iso','ISO','','','','ANONYMOUS','NUMBER','','','Iso','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_FROM_DISTRICT',201,'Ship From District','SHIP_FROM_DISTRICT','','','','ANONYMOUS','VARCHAR2','','','Ship From District','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_FROM_REGION',201,'Ship From Region','SHIP_FROM_REGION','','','','ANONYMOUS','VARCHAR2','','','Ship From Region','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_TO_DISTRICT',201,'Ship To District','SHIP_TO_DISTRICT','','','','ANONYMOUS','VARCHAR2','','','Ship To District','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_TO_REGION',201,'Ship To Region','SHIP_TO_REGION','','','','ANONYMOUS','VARCHAR2','','','Ship To Region','','','','US');
--Inserting Object Components for EIS_XXWC_PO_TRANSFER_DTL_V
--Inserting Object Component Joins for EIS_XXWC_PO_TRANSFER_DTL_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report LOV Data for Transfer Detail Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Transfer Detail Report
xxeis.eis_rsc_ins.lov( 201,'select distinct PAPF.FULL_NAME,HOU.name OPERATING_UNIT  from PO_VENDORS POV,
       po_vendor_sites_all pvs,
       PER_ALL_PEOPLE_F PAPF,
       po_headers_all poh,HR_OPERATING_UNITS HOU
 WHERE poh.vendor_id = pov.vendor_id
   AND poh.vendor_site_id = pvs.vendor_site_id
   and POH.TYPE_LOOKUP_CODE in (''STANDARD'', ''BLANKET'', ''PLANNED'')
   and POH.AGENT_ID = PAPF.PERSON_ID
   and PVS.ORG_ID=HOU.ORGANIZATION_ID
   and exists(select 1 from XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=pvs.ORG_ID)','','BUYER_NAME','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Supplier'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Supplier List','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Cat Class''  and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Cat Class List','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT POV.SEGMENT1 VENDOR_NUMBER,
  POV.VENDOR_NAME
FROM APPS.PO_VENDORS POV
WHERE EXISTS
  (SELECT /*+ INDEX(MSA XXWC_MRP_SR_ASSIGNMENTS_N7)*/
	1
  FROM APPS.MRP_SR_SOURCE_ORG MSSO ,
    APPS.MRP_SR_RECEIPT_ORG MSRO ,
    APPS.MRP_SOURCING_RULES MSR ,
    APPS.MRP_SR_ASSIGNMENTS MSA ,
    APPS.MTL_SYSTEM_ITEMS_B MSI
  WHERE 1                   =1
  AND msro.sr_receipt_id    = msso.sr_receipt_id
  AND MSR.SOURCING_RULE_ID  = MSRO.SOURCING_RULE_ID
  AND MSSO.VENDOR_ID        = POV.VENDOR_ID
  AND MSA.SOURCING_RULE_ID  = MSRO.SOURCING_RULE_ID
  AND MSA.INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
  AND MSA.ORGANIZATION_ID   = MSI.ORGANIZATION_ID
  AND MSI.SOURCE_TYPE       = 2
  )
ORDER BY LPAD(POV.SEGMENT1,10)','','XXWC Vendors','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT ood.organization_code organization_code,
  ood.organization_name organization_name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE organization_code NOT IN(''CAN'',''HDS'',''US1'',''CN1'')
ORDER BY organization_code','','XXWC Org Lov','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select distinct pov.vendor_name , pov.segment1 vendor_number from po_vendors pov, mtl_system_items_b msi, mrp_sr_assignments msa,
    mrp_sr_receipt_org msro,
    mrp_sr_source_org msso,
    mrp_sourcing_rules msr
WHERE msi.inventory_item_id         = msa.inventory_item_id
  AND msi.organization_id           = msa.organization_id
  AND msa.sourcing_rule_id          = msro.sourcing_rule_id
  AND msa.sourcing_rule_id          = msr.sourcing_rule_id
  AND msro.sr_receipt_id            = msso.sr_receipt_id
  AND msso.vendor_id                = pov.vendor_id
  AND msi.source_type               = 2','','XXWC Vendor Name','','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report Data for Transfer Detail Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Transfer Detail Report
xxeis.eis_rsc_utility.delete_report_rows( 'Transfer Detail Report' );
--Inserting Report - Transfer Detail Report
xxeis.eis_rsc_ins.r( 201,'Transfer Detail Report','','Report to pull detailed transfer history between orgs and DC/HUBs','','','','10011985','EIS_XXWC_PO_TRANSFER_DTL_V','Y','','','10011985','','N','White Cap Reports','','EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Transfer Detail Report
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'BUYER','Buyer','Buyer','','','default','','4','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'CAT_CLASS','Cat Class','Cat Class','','','default','','24','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'CAT_CLASS_DESC','Cat Class Description','Cat Class Desc','','','default','','25','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'EXT$','Ext$','Ext$','','~T~D~2','default','','29','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'ITEM','Item','Item','','','default','','5','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'ITEM_COST','Item Cost','Item Cost','','~T~D~2','default','','28','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','6','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'ORDERED_DATE','Order Date','Ordered Date','','','default','','14','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'ORDER_STATUS','Order Line Status','Order Status','','','default','','22','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'ORD_QTY','Ord Qty','Ord Qty','','~T~D~0','default','','11','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'QTY_DELIVERED','Qty Delivered','Qty Delivered','','~T~D~0','default','','12','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'SHIP_DATE','Ship Date','Ship Date','','','default','','15','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'SHIP_FROM_CITY','Ship From City','Ship From City','','','default','','18','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'SHIP_FROM_NAME','Ship From Name','Ship From Name','','','default','','3','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'SHIP_FROM_ORG','Ship From Org','Ship From Org','','','default','','2','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'SHIP_FROM_STATE','Ship From State','Ship From State','','','default','','19','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'SHIP_METHOD','Ship Method','Ship Method','','','default','','23','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'SHIP_TO_CITY','Ship to City','Ship To City','','','default','','20','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'SHIP_TO_ORG','Ship To Org','Ship To Org','','','default','','1','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'SHIP_TO_STATE','Ship to State','Ship To State','','','default','','21','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'SOURCE','Source','Source','','','default','','10','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'SUPPLIER_ITEM_NUMBER','Supplier Item Number','Supplier Item Number','','','default','','7','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'UOM','UOM','Uom','','','default','','13','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'VENDOR','Vendor','Vendor','','','default','','9','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','default','','8','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'SHIP_TO_DISTRICT','District','Ship To District','','','default','','27','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'SHIP_TO_REGION','Region','Ship To Region','','','default','','26','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'ISO','ISO','Iso','','~~~','default','','16','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Transfer Detail Report',201,'ORDER_LINE','Order Line#','Order Line','','','default','','17','N','Y','','','','','','','10011985','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','US','');
--Inserting Report Parameters - Transfer Detail Report
xxeis.eis_rsc_ins.rp( 'Transfer Detail Report',201,'Ship to Region','Ship to Region','SHIP_TO_REGION','IN','Region Lov','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','10011985','Y','N','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Transfer Detail Report',201,'Ship to District','Ship to District','SHIP_TO_DISTRICT','IN','District Lov','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','10011985','Y','N','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Transfer Detail Report',201,'Ship to Location','Ship to Location','SHIP_TO_ORG','IN','XXWC Org Lov','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','10011985','Y','N','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Transfer Detail Report',201,'Ship to Location List','Ship to Location List','','IN','XXWC Org List','','VARCHAR2','N','Y','4','','N','CONSTANT','10011985','Y','N','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Transfer Detail Report',201,'Ship From Region','Ship From Region','SHIP_FROM_REGION','IN','Region Lov','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','10011985','Y','N','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Transfer Detail Report',201,'Ship From District','Ship From District','SHIP_FROM_DISTRICT','IN','District Lov','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','10011985','Y','N','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Transfer Detail Report',201,'Ship From Location','Ship From Location','SHIP_FROM_ORG','IN','XXWC Org Lov','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','10011985','Y','N','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Transfer Detail Report',201,'Ship From Location List','Ship From Location List','','IN','XXWC Org List','','VARCHAR2','N','Y','8','','N','CONSTANT','10011985','Y','N','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Transfer Detail Report',201,'Vendor Name','Vendor Name','VENDOR','IN','XXWC Vendor Name','','VARCHAR2','N','Y','9','Y','Y','CONSTANT','10011985','Y','N','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Transfer Detail Report',201,'Vendor Number','Vendor Number','VENDOR_NUMBER','IN','XXWC Vendors','','VARCHAR2','N','Y','10','Y','Y','CONSTANT','10011985','Y','N','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Transfer Detail Report',201,'Vendor List','Vendor List','','IN','XXWC Supplier List','','VARCHAR2','N','Y','11','','N','CONSTANT','10011985','Y','N','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Transfer Detail Report',201,'Parts List','Parts List','','IN','XXWC Item List','','VARCHAR2','N','Y','12','','N','CONSTANT','10011985','Y','N','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Transfer Detail Report',201,'CatClass List','CatClass List','','IN','XXWC Cat Class List','','VARCHAR2','N','Y','13','','N','CONSTANT','10011985','Y','N','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Transfer Detail Report',201,'Order Date From','Order Date From','ORDERED_DATE','>=','','','DATE','N','Y','14','','Y','CONSTANT','10011985','Y','N','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Transfer Detail Report',201,'Order Date To','Order Date To','ORDERED_DATE','<=','','','DATE','N','Y','15','','Y','CONSTANT','10011985','Y','N','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Transfer Detail Report',201,'Actual Ship Date From','Actual Ship Date From','SHIP_DATE','>=','','','DATE','N','Y','16','','Y','CONSTANT','10011985','Y','N','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Transfer Detail Report',201,'Actual Ship Date To','Actual Ship Date To','SHIP_DATE','<=','','','DATE','N','Y','17','','Y','CONSTANT','10011985','Y','N','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Transfer Detail Report',201,'Buyer Name','Buyer Name','BUYER','IN','BUYER_NAME','','VARCHAR2','N','N','18','','N','CONSTANT','10011985','N','N','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','US','');
--Inserting Dependent Parameters - Transfer Detail Report
--Inserting Report Conditions - Transfer Detail Report
xxeis.eis_rsc_ins.rcnh( 'Transfer Detail Report',201,'BUYER IN :Buyer Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BUYER','','Buyer Name','','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','','','IN','Y','Y','','','','','1',201,'Transfer Detail Report','BUYER IN :Buyer Name ');
xxeis.eis_rsc_ins.rcnh( 'Transfer Detail Report',201,'ORDERED_DATE >= :Order Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORDERED_DATE','','Order Date From','','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',201,'Transfer Detail Report','ORDERED_DATE >= :Order Date From ');
xxeis.eis_rsc_ins.rcnh( 'Transfer Detail Report',201,'ORDERED_DATE <= :Order Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORDERED_DATE','','Order Date To','','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',201,'Transfer Detail Report','ORDERED_DATE <= :Order Date To ');
xxeis.eis_rsc_ins.rcnh( 'Transfer Detail Report',201,'SHIP_DATE >= :Actual Ship Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SHIP_DATE','','Actual Ship Date From','','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',201,'Transfer Detail Report','SHIP_DATE >= :Actual Ship Date From ');
xxeis.eis_rsc_ins.rcnh( 'Transfer Detail Report',201,'SHIP_DATE <= :Actual Ship Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SHIP_DATE','','Actual Ship Date To','','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',201,'Transfer Detail Report','SHIP_DATE <= :Actual Ship Date To ');
xxeis.eis_rsc_ins.rcnh( 'Transfer Detail Report',201,'SHIP_FROM_DISTRICT IN :Ship From District ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SHIP_FROM_DISTRICT','','Ship From District','','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','','','IN','Y','Y','','','','','1',201,'Transfer Detail Report','SHIP_FROM_DISTRICT IN :Ship From District ');
xxeis.eis_rsc_ins.rcnh( 'Transfer Detail Report',201,'SHIP_FROM_ORG IN :Ship From Location ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SHIP_FROM_ORG','','Ship From Location','','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','','','IN','Y','Y','','','','','1',201,'Transfer Detail Report','SHIP_FROM_ORG IN :Ship From Location ');
xxeis.eis_rsc_ins.rcnh( 'Transfer Detail Report',201,'SHIP_FROM_REGION IN :Ship From Region ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SHIP_FROM_REGION','','Ship From Region','','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','','','IN','Y','Y','','','','','1',201,'Transfer Detail Report','SHIP_FROM_REGION IN :Ship From Region ');
xxeis.eis_rsc_ins.rcnh( 'Transfer Detail Report',201,'SHIP_TO_DISTRICT IN :Ship to District ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SHIP_TO_DISTRICT','','Ship to District','','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','','','IN','Y','Y','','','','','1',201,'Transfer Detail Report','SHIP_TO_DISTRICT IN :Ship to District ');
xxeis.eis_rsc_ins.rcnh( 'Transfer Detail Report',201,'SHIP_TO_ORG IN :Ship to Location ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SHIP_TO_ORG','','Ship to Location','','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','','','IN','Y','Y','','','','','1',201,'Transfer Detail Report','SHIP_TO_ORG IN :Ship to Location ');
xxeis.eis_rsc_ins.rcnh( 'Transfer Detail Report',201,'SHIP_TO_REGION IN :Ship to Region ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SHIP_TO_REGION','','Ship to Region','','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','','','IN','Y','Y','','','','','1',201,'Transfer Detail Report','SHIP_TO_REGION IN :Ship to Region ');
xxeis.eis_rsc_ins.rcnh( 'Transfer Detail Report',201,'VENDOR IN :Vendor Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR','','Vendor Name','','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','','','IN','Y','Y','','','','','1',201,'Transfer Detail Report','VENDOR IN :Vendor Name ');
xxeis.eis_rsc_ins.rcnh( 'Transfer Detail Report',201,'VENDOR_NUMBER IN :Vendor Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NUMBER','','Vendor Number','','','','','EIS_XXWC_PO_TRANSFER_DTL_V','','','','','','IN','Y','Y','','','','','1',201,'Transfer Detail Report','VENDOR_NUMBER IN :Vendor Number ');
xxeis.eis_rsc_ins.rcnh( 'Transfer Detail Report',201,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND (:Parts List is null or EXISTS (SELECT 1  FROM 
                         xxeis.EIS_XXWC_PARAM_PARSE_LIST
                         WHERE LIST_NAME= :Parts List
                            AND LIST_TYPE =''Item''
                           AND process_id = :SYSTEM.PROCESS_ID
                           AND list_value= EXPTDV.ITEM) 
     )
AND (:Vendor List is null or EXISTS (SELECT 1  FROM 
                         xxeis.EIS_XXWC_PARAM_PARSE_LIST
                         WHERE LIST_NAME= :Vendor List
                            AND LIST_TYPE =''Supplier''
                            AND process_id = :SYSTEM.PROCESS_ID
                            AND EXPTDV.VENDOR_NUMBER= list_value) 
     )
AND (:CatClass List is null or EXISTS (SELECT 1  FROM 
                        xxeis.EIS_XXWC_PARAM_PARSE_LIST
                         WHERE LIST_NAME= :CatClass List
                            AND LIST_TYPE =''Cat Class''
                            AND process_id = :SYSTEM.PROCESS_ID
                            AND EXPTDV.CAT_CLASS = list_value) 
     )
AND  (:Ship to Location List IS NULL OR EXISTS (SELECT 1  FROM 
                        xxeis.EIS_XXWC_PARAM_PARSE_LIST
                         WHERE LIST_NAME= :Ship to Location List                            
                           AND LIST_TYPE =''Org''
                           AND process_id = :SYSTEM.PROCESS_ID     
                           AND LPAD(EXPTDV.ship_to_org,3,''0'')= list_value)
     )
AND  (:Ship From Location List IS NULL OR EXISTS (SELECT 1  FROM 
                         xxeis.EIS_XXWC_PARAM_PARSE_LIST
                         WHERE LIST_NAME= :Ship From Location List                            
                           AND LIST_TYPE =''Org''
                           AND process_id = :SYSTEM.PROCESS_ID     
                           AND LPAD(EXPTDV.ship_from_org,3,''0'')= list_value)
     )

        
        ','1',201,'Transfer Detail Report','Free Text ');
--Inserting Report Sorts - Transfer Detail Report
--Inserting Report Triggers - Transfer Detail Report
xxeis.eis_rsc_ins.rt( 'Transfer Detail Report',201,'BEGIN

 IF :Vendor List is NOT NULL THEN
     XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(:SYSTEM.PROCESS_ID,:Vendor List,''Supplier'');
 END IF;

 IF :Parts List is NOT NULL THEN
   XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(:SYSTEM.PROCESS_ID,:Parts List,''Item'');  
 END IF;

 IF :Ship to Location List is NOT NULL THEN
     XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(:SYSTEM.PROCESS_ID,:Ship to Location List,''Org'');
 END IF;

 IF :Ship From Location List is NOT NULL THEN
   XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(:SYSTEM.PROCESS_ID,:Ship From Location List,''Org'');
 END IF;

 IF :CatClass List is NOT NULL THEN
   XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(:SYSTEM.PROCESS_ID,:CatClass List,''Cat Class'');  
 END IF;

END;','B','Y','10011985','');
xxeis.eis_rsc_ins.rt( 'Transfer Detail Report',201,'begin
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.parse_cleanup_table(:SYSTEM.PROCESS_ID);
end;','A','Y','10011985','');
--inserting report templates - Transfer Detail Report
--Inserting Report Portals - Transfer Detail Report
xxeis.eis_rsc_ins.r_port( 'Transfer Detail Report','XXWC_PUR_TOP_RPTS','201','Transfer Detail Report','PO and Transfer Detail report','OA.jsp?page=/eis/oracle/apps/xxeis/central/reporting/webui/EISRSCLaunchPG&EisProduct=Reporting&Portal=Yes&mod=Purchasing','','EXCEL,','CONC','N','10011985');
--inserting report dashboards - Transfer Detail Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Transfer Detail Report','201','EIS_XXWC_PO_TRANSFER_DTL_V','EIS_XXWC_PO_TRANSFER_DTL_V','N','');
--inserting report security - Transfer Detail Report
xxeis.eis_rsc_ins.rsec( 'Transfer Detail Report','201','','XXWC_PUR_SUPER_USER',201,'10011985','','','');
xxeis.eis_rsc_ins.rsec( 'Transfer Detail Report','201','','HDS_PRCHSNG_SPR_USR',201,'10011985','','','');
xxeis.eis_rsc_ins.rsec( 'Transfer Detail Report','201','','XXWC_PURCHASING_SR_MRG_WC',201,'10011985','','','');
xxeis.eis_rsc_ins.rsec( 'Transfer Detail Report','201','','XXWC_PURCHASING_RPM',201,'10011985','','','');
xxeis.eis_rsc_ins.rsec( 'Transfer Detail Report','201','','XXWC_PURCHASING_MGR',201,'10011985','','','');
xxeis.eis_rsc_ins.rsec( 'Transfer Detail Report','201','','XXWC_PURCHASING_INQUIRY',201,'10011985','','','');
xxeis.eis_rsc_ins.rsec( 'Transfer Detail Report','201','','XXWC_PURCHASING_BUYER',201,'10011985','','','');
xxeis.eis_rsc_ins.rsec( 'Transfer Detail Report','201','','XXWC_EOM_PURCHASING',201,'10011985','','','');
--Inserting Report Pivots - Transfer Detail Report
--Inserting Report   Version details- Transfer Detail Report
xxeis.eis_rsc_ins.rv( 'Transfer Detail Report','','Transfer Detail Report','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
