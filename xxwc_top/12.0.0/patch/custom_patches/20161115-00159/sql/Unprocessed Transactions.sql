--Report Name            : Unprocessed Transactions
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_UNPROCESSED_XACTION_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_UNPROCESSED_XACTION_V
xxeis.eis_rsc_ins.v( 'XXEIS_UNPROCESSED_XACTION_V',91000,'Paste SQL View for Outstanding Transactions','1.0','','','JR006127','APPS','Outstanding Transactions View','X9KV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_UNPROCESSED_XACTION_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_UNPROCESSED_XACTION_V',91000,FALSE);
--Inserting Object Columns for XXEIS_UNPROCESSED_XACTION_V
xxeis.eis_rsc_ins.vc( 'XXEIS_UNPROCESSED_XACTION_V','CARD_PROGRAM_NAME',91000,'','','','','','JR006127','VARCHAR2','','','Card Program Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_UNPROCESSED_XACTION_V','FULL_NAME',91000,'','','','','','JR006127','VARCHAR2','','','Full Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_UNPROCESSED_XACTION_V','TOTAL_OUTSTANDING_AMOUNT',91000,'','','','~T~D~2','','JR006127','NUMBER','','','Total Outstanding Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_UNPROCESSED_XACTION_V','NUM_OF_OUTSTANDING_TRANS',91000,'','','','','','JR006127','NUMBER','','','Num Of Outstanding Trans','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_UNPROCESSED_XACTION_V','ORACLE_PRODUCT',91000,'','','','','','JR006127','VARCHAR2','','','Oracle Product','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_UNPROCESSED_XACTION_V','EMPLOYEE_NUMBER',91000,'','','','','','JR006127','VARCHAR2','','','Employee Number','','','','US');
--Inserting Object Components for XXEIS_UNPROCESSED_XACTION_V
xxeis.eis_rsc_ins.vcomp( 'XXEIS_UNPROCESSED_XACTION_V','XXEIS_LOB_NAMES_V',91000,'','XLNV','','JR006127','JR006127','1934651','XXEIS_LOB_NAMES_V','','','','','X1HV','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_UNPROCESSED_XACTION_V','XXCUSHR_PS_EMP_ALL_TBL',91000,'','XPEAT','','JR006127','JR006127','3847553','XXCUSHR_PS_EMP_ALL_TBL','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for XXEIS_UNPROCESSED_XACTION_V
xxeis.eis_rsc_ins.vcj( 'XXEIS_UNPROCESSED_XACTION_V','XXEIS_LOB_NAMES_V','XLNV',91000,'X9KV.ORACLE_PRODUCT','=','XLNV.ORACLE_PRODUCT(+)','','','','','JR006127');
xxeis.eis_rsc_ins.vcj( 'XXEIS_UNPROCESSED_XACTION_V','XXCUSHR_PS_EMP_ALL_TBL','XPEAT',91000,'X9KV.EMPLOYEE_NUMBER','=','XPEAT.EMPLOYEE_NUMBER(+)','','','','','JR006127');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
prompt Creating Report LOV Data for Unprocessed Transactions
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Unprocessed Transactions
xxeis.eis_rsc_ins.lov( 91000,'select DISTINCT ORACLE_PRODUCT from XXCUS.XXCUS_BULLET_IEXP_TBL','','XXCUS_ORACLE_PRODUCT T&E','Oracle product number','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
prompt Creating Report Data for Unprocessed Transactions
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Unprocessed Transactions
xxeis.eis_rsc_utility.delete_report_rows( 'Unprocessed Transactions' );
--Inserting Report - Unprocessed Transactions
xxeis.eis_rsc_ins.r( 91000,'Unprocessed Transactions','','Displays all transactions that are still unprocessed','','','','ID020048','XXEIS_UNPROCESSED_XACTION_V','Y','','--Company Pay cards--
SELECT oracle_product, card_program_name, Full_name, employee_number, sum(Line_Amount) AS Total_Outstanding_Amount, count(*) AS Num_Of_Outstanding_Trans
FROM XXEIS.XXCUS_BULLET_IEXP_TBL XBIT
WHERE query_num IN (''2'', ''3'', ''5'')
      AND (card_program_name LIKE (''WC PNC Inventory Card Program'')
           OR card_program_name LIKE (''HDS Company Pay Program''))
      AND (trunc(sysdate, ''J'') - trunc(posted_date, ''J'') > 60)
GROUP BY oracle_product, Card_Program_Name, Full_Name, employee_number

UNION

--Employee Pay Cards--      
SELECT oracle_product, card_program_name, Full_name, employee_number, sum(Line_Amount) AS Total_Outstanding_Amount, count(*) AS Num_Of_Outstanding_Trans
FROM XXEIS.XXCUS_BULLET_IEXP_TBL XBIT
WHERE query_num IN (''2'', ''3'', ''5'')
      AND (card_program_name LIKE (''CB PNC Employee-Pay Card Program'')
           OR card_program_name LIKE (''FM PNC Employee-Pay Card Program'')
           OR card_program_name LIKE (''WW PNC Employee-Pay Card Program''))
      AND (trunc(sysdate, ''J'') - trunc(posted_date, ''J'') > 90)
GROUP BY oracle_product, Card_Program_Name, Full_Name, employee_number
','ID020048','','N','User Modified Reports','','CSV,Pivot Excel,EXCEL,','','','','','','','','APPS','US','','','','');
--Inserting Report Columns - Unprocessed Transactions
xxeis.eis_rsc_ins.rc( 'Unprocessed Transactions',91000,'CARD_PROGRAM_NAME','Card Program Name','','','','default','','2','N','','','','','','','','ID020048','N','N','','XXEIS_UNPROCESSED_XACTION_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Unprocessed Transactions',91000,'FULL_NAME','Full Name','','','','default','','3','N','','','','','','','','ID020048','N','N','','XXEIS_UNPROCESSED_XACTION_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Unprocessed Transactions',91000,'TOTAL_OUTSTANDING_AMOUNT','Total Outstanding Amount','','','$~,~.~2','default','','6','N','','','','','','','','ID020048','N','N','','XXEIS_UNPROCESSED_XACTION_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Unprocessed Transactions',91000,'NUM_OF_OUTSTANDING_TRANS','Num Of Outstanding Trans','','','~T~D~0','default','','5','N','','','','','','','','ID020048','N','N','','XXEIS_UNPROCESSED_XACTION_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Unprocessed Transactions',91000,'EMPLOYEE_NUMBER','Employee Number','','','','default','','4','N','','','','','','','','ID020048','N','N','','XXEIS_UNPROCESSED_XACTION_V','','','GROUP_BY','US','');
--Inserting Report Parameters - Unprocessed Transactions
xxeis.eis_rsc_ins.rp( 'Unprocessed Transactions',91000,'oracle_product','','ORACLE_PRODUCT','IN','XXCUS_ORACLE_PRODUCT T&E','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','ID020048','Y','N','','','','XXEIS_UNPROCESSED_XACTION_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Unprocessed Transactions',91000,'LOB','','','IN','','','VARCHAR2','N','N','2','Y','N','CONSTANT','ID020048','N','N','','','','XXEIS_UNPROCESSED_XACTION_V','','','US','');
--Inserting Dependent Parameters - Unprocessed Transactions
--Inserting Report Conditions - Unprocessed Transactions
xxeis.eis_rsc_ins.rcnh( 'Unprocessed Transactions',91000,'ORACLE_PRODUCT IN :oracle_product ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORACLE_PRODUCT','','oracle_product','','','','','XXEIS_UNPROCESSED_XACTION_V','','','','','','IN','Y','Y','','','','','1',91000,'Unprocessed Transactions','ORACLE_PRODUCT IN :oracle_product ');
xxeis.eis_rsc_ins.rcnh( 'Unprocessed Transactions',91000,'XLNV.LOB_NAME IN :LOB ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','LOB','','','','','','','','','','','IN','Y','Y','XLNV.LOB_NAME','','','','1',91000,'Unprocessed Transactions','XLNV.LOB_NAME IN :LOB ');
--Inserting Report Sorts - Unprocessed Transactions
xxeis.eis_rsc_ins.rs( 'Unprocessed Transactions',91000,'','ASC','ID020048','1','');
xxeis.eis_rsc_ins.rs( 'Unprocessed Transactions',91000,'NUM_OF_OUTSTANDING_TRANS','DESC','ID020048','2','');
--Inserting Report Triggers - Unprocessed Transactions
--inserting report templates - Unprocessed Transactions
--Inserting Report Portals - Unprocessed Transactions
--inserting report dashboards - Unprocessed Transactions
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Unprocessed Transactions','91000','XXEIS_UNPROCESSED_XACTION_V','XXEIS_UNPROCESSED_XACTION_V','N','');
--inserting report security - Unprocessed Transactions
xxeis.eis_rsc_ins.rsec( 'Unprocessed Transactions','','ID020048','',91000,'ID020048','','','');
xxeis.eis_rsc_ins.rsec( 'Unprocessed Transactions','','JG023358','',91000,'ID020048','','','');
--Inserting Report Pivots - Unprocessed Transactions
--Inserting Report   Version details- Unprocessed Transactions
xxeis.eis_rsc_ins.rv( 'Unprocessed Transactions','','Unprocessed Transactions','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
