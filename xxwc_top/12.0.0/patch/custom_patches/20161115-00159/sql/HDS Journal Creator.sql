--Report Name            : HDS Journal Creator
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_1801493_QDTZGW_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1801493_QDTZGW_V
xxeis.eis_rsc_ins.v( 'XXEIS_1801493_QDTZGW_V',85000,'Paste SQL View for HDS Journal Creator','1.0','','','XXEIS_RS_ADMIN','APPS','HDS Journal Creator View','X1QV12','','Y','VIEW','US','','');
--Delete Object Columns for XXEIS_1801493_QDTZGW_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1801493_QDTZGW_V',85000,FALSE);
--Inserting Object Columns for XXEIS_1801493_QDTZGW_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1801493_QDTZGW_V','CREATED_BY',85000,'','','','','','XXEIS_RS_ADMIN','NUMBER','','','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801493_QDTZGW_V','FULL_NAME',85000,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Full Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801493_QDTZGW_V','ENTRP_ENTITY',85000,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Entrp Entity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1801493_QDTZGW_V','CREATION_DATE',85000,'','','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date','','','','US');
--Inserting Object Components for XXEIS_1801493_QDTZGW_V
--Inserting Object Component Joins for XXEIS_1801493_QDTZGW_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report HDS Journal Creator
prompt Creating Report Data for HDS Journal Creator
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS Journal Creator
xxeis.eis_rsc_utility.delete_report_rows( 'HDS Journal Creator' );
--Inserting Report - HDS Journal Creator
xxeis.eis_rsc_ins.r( 85000,'HDS Journal Creator','','This report was created for User Audit review. This report provides a list of users that have created any GL Journals.','','','','XXEIS_RS_ADMIN','XXEIS_1801493_QDTZGW_V','Y','','SELECT DISTINCT gj.created_by
     , papf.full_name
     , loc.entrp_entity
     ,gj.creation_date
  FROM gl.gl_je_headers gj
     , applsys.fnd_user u
     , hr.per_all_people_f papf
     , xxcus.xxcus_location_code_tbl loc
WHERE gj.created_by = u.user_id
   AND u.employee_id = papf.person_id
   and papf.attribute1 = loc.fru
   AND papf.effective_end_date = ''31-DEC-4712''   
   order by full_name asc
','XXEIS_RS_ADMIN','','Y','Audit Reports','','CSV,EXCEL,','N','','','','','','N','APPS','US','','','','');
--Inserting Report Columns - HDS Journal Creator
xxeis.eis_rsc_ins.rc( 'HDS Journal Creator',85000,'ENTRP_ENTITY','Entrp Entity','','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801493_QDTZGW_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Creator',85000,'FULL_NAME','Full Name','','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801493_QDTZGW_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Journal Creator',85000,'CREATED_BY','Created By','','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1801493_QDTZGW_V','','','SUM','US','');
--Inserting Report Parameters - HDS Journal Creator
xxeis.eis_rsc_ins.rp( 'HDS Journal Creator',85000,'Creation_Date_From','Creation_Date_From','CREATION_DATE','>=','','','DATE','N','Y','1','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_1801493_QDTZGW_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Journal Creator',85000,'Creation_Date_To','Creation_Date_To','CREATION_DATE','<=','','','DATE','N','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','','','','','XXEIS_1801493_QDTZGW_V','','','US','');
--Inserting Dependent Parameters - HDS Journal Creator
--Inserting Report Conditions - HDS Journal Creator
xxeis.eis_rsc_ins.rcnh( 'HDS Journal Creator',85000,'CREATION_DATE BETWEEN :Creation_Date_From  :Creation_Date_To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Creation_Date_From','','','Creation_Date_To','','XXEIS_1801493_QDTZGW_V','','','','','','BETWEEN','Y','Y','','','','','1',85000,'HDS Journal Creator','CREATION_DATE BETWEEN :Creation_Date_From  :Creation_Date_To');
--Inserting Report Sorts - HDS Journal Creator
--Inserting Report Triggers - HDS Journal Creator
--inserting report templates - HDS Journal Creator
--Inserting Report Portals - HDS Journal Creator
--inserting report dashboards - HDS Journal Creator
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS Journal Creator','85000','XXEIS_1801493_QDTZGW_V','XXEIS_1801493_QDTZGW_V','N','');
--inserting report security - HDS Journal Creator
xxeis.eis_rsc_ins.rsec( 'HDS Journal Creator','1','','HDS_SYSTEM_ADMINISTRATOR',85000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Journal Creator','1','','SYSTEM_ADMINISTRATOR',85000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Journal Creator','101','','XXCUS_GL_INQUIRY',85000,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS Journal Creator
--Inserting Report   Version details- HDS Journal Creator
xxeis.eis_rsc_ins.rv( 'HDS Journal Creator','','HDS Journal Creator','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
