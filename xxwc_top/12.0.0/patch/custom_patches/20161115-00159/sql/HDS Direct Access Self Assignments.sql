--Report Name            : HDS Direct Access Self Assignments
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_1796492_IPTZMJ_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1796492_IPTZMJ_V
xxeis.eis_rsc_ins.v( 'XXEIS_1796492_IPTZMJ_V',85000,'Paste SQL View for HDS Direct Access Self Assignments','1.0','','','MM027735','APPS','HDS Direct Access Self Assignments View','X1IV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_1796492_IPTZMJ_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1796492_IPTZMJ_V',85000,FALSE);
--Inserting Object Columns for XXEIS_1796492_IPTZMJ_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1796492_IPTZMJ_V','GRANTED_TO_ID',85000,'','','','','','MM027735','NUMBER','','','Granted To Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1796492_IPTZMJ_V','USER_NAME',85000,'','','','','','MM027735','VARCHAR2','','','User Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1796492_IPTZMJ_V','GRANTED_TO_NAME',85000,'','','','','','MM027735','VARCHAR2','','','Granted To Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1796492_IPTZMJ_V','RESPONSIBILITY_NAME',85000,'','','','','','MM027735','VARCHAR2','','','Responsibility Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1796492_IPTZMJ_V','DESCRIPTION',85000,'','','','','','MM027735','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1796492_IPTZMJ_V','GREANTED_BY_ID',85000,'','','','','','MM027735','NUMBER','','','Greanted By Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1796492_IPTZMJ_V','GRANTED_BY_NAME',85000,'','','','','','MM027735','VARCHAR2','','','Granted By Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1796492_IPTZMJ_V','DATE_GRANTED',85000,'','','','','','MM027735','DATE','','','Date Granted','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1796492_IPTZMJ_V','END_DATE',85000,'','','','','','MM027735','DATE','','','End Date','','','','US');
--Inserting Object Components for XXEIS_1796492_IPTZMJ_V
--Inserting Object Component Joins for XXEIS_1796492_IPTZMJ_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report HDS Direct Access Self Assignments
prompt Creating Report Data for HDS Direct Access Self Assignments
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS Direct Access Self Assignments
xxeis.eis_rsc_utility.delete_report_rows( 'HDS Direct Access Self Assignments' );
--Inserting Report - HDS Direct Access Self Assignments
xxeis.eis_rsc_ins.r( 85000,'HDS Direct Access Self Assignments','','HDS Direct Access Self Assignments - report provides a list of all access directly assigned to any user where it was self provisioned.

Created per ESMS 294511.','','','','MM027735','XXEIS_1796492_IPTZMJ_V','Y','','SELECT furgd.USER_ID  GRANTED_TO_ID
,      fu.USER_NAME
,      papf.FULL_NAME GRANTED_TO_NAME
,      frtl.RESPONSIBILITY_NAME
,      furgd.DESCRIPTION
,      furgd.CREATED_BY GREANTED_BY_ID
,      papf2.FULL_NAME GRANTED_BY_NAME
,      furgd.CREATION_DATE DATE_GRANTED
,      furgd.END_DATE
FROM   apps.fnd_user_resp_groups_direct furgd
,      apps.fnd_user fu
,      apps.fnd_user fu2
,      hr.per_all_people_f papf
,      hr.per_all_people_f papf2
,      apps.fnd_responsibility_tl frtl
WHERE  1 = 1
--AND    furgd.USER_ID = ''1219'' --for testing
AND    furgd.USER_ID = furgd.CREATED_BY
-- AND    furgd.creation_date BETWEEN TO_DATE (''2014/02/01'', ''yyyy/mm/dd'') AND TO_DATE (''2015/02/01'', ''yyyy/mm/dd'')
AND    fu.user_id = furgd.user_id
AND    fu2.user_id = furgd.created_by
AND    fu.employee_id = papf.person_id  (+) -- if no papf record
AND    fu2.employee_id = papf2.person_id  (+) --if no papf2 record
AND    furgd.responsibility_application_id = frtl.application_id
AND    furgd.responsibility_id = frtl.responsibility_id
AND    (papf.effective_end_date = ''31-DEC-4712'' OR papf.effective_end_date is NULL) -- if no papf record
AND    (papf2.effective_end_date = ''31-DEC-4712'' OR papf2.effective_end_date is NULL) --if no papf2 record
ORDER BY furgd.CREATION_DATE, furgd.USER_ID
','MM027735','','N','Audit Reports','','EXCEL,','','','','','','','','APPS','US','','','','');
--Inserting Report Columns - HDS Direct Access Self Assignments
xxeis.eis_rsc_ins.rc( 'HDS Direct Access Self Assignments',85000,'GRANTED_BY_NAME','Granted By Name','','','','','','','','Y','','','','','','','MM027735','','','','XXEIS_1796492_IPTZMJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Direct Access Self Assignments',85000,'DATE_GRANTED','Date Granted','','','','','','','','Y','','','','','','','MM027735','','','','XXEIS_1796492_IPTZMJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Direct Access Self Assignments',85000,'RESPONSIBILITY_NAME','Responsibility Name','','','','','','','','Y','','','','','','','MM027735','','','','XXEIS_1796492_IPTZMJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Direct Access Self Assignments',85000,'DESCRIPTION','Description','','','','','','','','Y','','','','','','','MM027735','','','','XXEIS_1796492_IPTZMJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Direct Access Self Assignments',85000,'GRANTED_TO_ID','Granted To Id','','','','','','','','Y','','','','','','','MM027735','','','','XXEIS_1796492_IPTZMJ_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Direct Access Self Assignments',85000,'GREANTED_BY_ID','Greanted By Id','','','','','','','','Y','','','','','','','MM027735','','','','XXEIS_1796492_IPTZMJ_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Direct Access Self Assignments',85000,'USER_NAME','User Name','','','','','','','','Y','','','','','','','MM027735','','','','XXEIS_1796492_IPTZMJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Direct Access Self Assignments',85000,'GRANTED_TO_NAME','Granted To Name','','','','','','','','Y','','','','','','','MM027735','','','','XXEIS_1796492_IPTZMJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Direct Access Self Assignments',85000,'END_DATE','End Date','','','','','','','','Y','','','','','','','MM027735','','','','XXEIS_1796492_IPTZMJ_V','','','GROUP_BY','US','');
--Inserting Report Parameters - HDS Direct Access Self Assignments
xxeis.eis_rsc_ins.rp( 'HDS Direct Access Self Assignments',85000,'Date Granted Start','','DATE_GRANTED','>=','','','DATE','N','Y','1','','Y','CURRENT_DATE','MM027735','Y','N','','Start Date','','XXEIS_1796492_IPTZMJ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Direct Access Self Assignments',85000,'Date Granted End','','DATE_GRANTED','<=','','','DATE','N','Y','2','','Y','CURRENT_DATE','MM027735','Y','','','End Date','','XXEIS_1796492_IPTZMJ_V','','','US','');
--Inserting Dependent Parameters - HDS Direct Access Self Assignments
--Inserting Report Conditions - HDS Direct Access Self Assignments
xxeis.eis_rsc_ins.rcnh( 'HDS Direct Access Self Assignments',85000,'DATE_GRANTED IN :Date Granted ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DATE_GRANTED','','','','','','','XXEIS_1796492_IPTZMJ_V','','','','','','IN','Y','Y','',':Date Granted','','','1',85000,'HDS Direct Access Self Assignments','DATE_GRANTED IN :Date Granted ');
xxeis.eis_rsc_ins.rcnh( 'HDS Direct Access Self Assignments',85000,'DATE_GRANTED <= :Date Granted End ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DATE_GRANTED','','Date Granted End','','','','','XXEIS_1796492_IPTZMJ_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',85000,'HDS Direct Access Self Assignments','DATE_GRANTED <= :Date Granted End ');
xxeis.eis_rsc_ins.rcnh( 'HDS Direct Access Self Assignments',85000,'DATE_GRANTED >= :Date Granted Start ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DATE_GRANTED','','Date Granted Start','','','','','XXEIS_1796492_IPTZMJ_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',85000,'HDS Direct Access Self Assignments','DATE_GRANTED >= :Date Granted Start ');
--Inserting Report Sorts - HDS Direct Access Self Assignments
--Inserting Report Triggers - HDS Direct Access Self Assignments
--inserting report templates - HDS Direct Access Self Assignments
--Inserting Report Portals - HDS Direct Access Self Assignments
--inserting report dashboards - HDS Direct Access Self Assignments
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS Direct Access Self Assignments','85000','XXEIS_1796492_IPTZMJ_V','XXEIS_1796492_IPTZMJ_V','N','');
--inserting report security - HDS Direct Access Self Assignments
xxeis.eis_rsc_ins.rsec( 'HDS Direct Access Self Assignments','101','','XXCUS_GL_INQUIRY',85000,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Direct Access Self Assignments','1','','SYSTEM_ADMINISTRATOR',85000,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Direct Access Self Assignments','1','','HDS_SYSTEM_ADMINISTRATOR',85000,'MM027735','','','');
--Inserting Report Pivots - HDS Direct Access Self Assignments
--Inserting Report   Version details- HDS Direct Access Self Assignments
xxeis.eis_rsc_ins.rv( 'HDS Direct Access Self Assignments','','HDS Direct Access Self Assignments','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
