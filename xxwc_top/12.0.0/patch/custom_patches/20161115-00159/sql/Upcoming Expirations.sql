--Report Name            : Upcoming Expirations
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_296491_AYNPDA_V
prompt Table type Objects cannot be imported
set scan off define off
prompt Creating Object APPS.XXEIS_296491_AYNPDA_V
set scan off define off
DECLARE
mod_exist varchar2(1);
stmt CLOB;
v_objlength INTEGER := 0;
v_offset INTEGER := 1;
v_sql_long DBMS_SQL.varchar2s;
v_sql_long_count INTEGER := 1;
c NUMBER;
r NUMBER;
l_stmt varchar2(32000);
l_view_cur pls_integer;
l_view_rows NUMBER;
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(240);
IF mod_exist = 'Y' THEN 
dbms_lob.createtemporary(stmt,TRUE);
dbms_lob.write( lob_loc   => stmt,amount    => length('create or replace view APPS.XXEIS_296491_AYNPDA_V
 ("BU","BRANCH_#","FRU","RER","RER_TYPE","RER_STATUS","PRIMARY_LOC","PRIMARY_ADDRESS","CITY","ST_PRV","POSTAL","COUNTRY","COMMENCEMENT_DATE","EXPIRATION","MASTER_RER","REM_NAME") as '),offset    => 1, BUFFER    => 'create or replace view APPS.XXEIS_296491_AYNPDA_V
 ("BU","BRANCH_#","FRU","RER","RER_TYPE","RER_STATUS","PRIMARY_LOC","PRIMARY_ADDRESS","CITY","ST_PRV","POSTAL","COUNTRY","COMMENCEMENT_DATE","EXPIRATION","MASTER_RER","REM_NAME") as ');
l_stmt :=  'SELECT CASE gcc.segment1
         WHEN ''06'' THEN
          ''PLB''
         WHEN ''07'' THEN
          ''PLB''
         ELSE
          llr.bu_id
       END bu
      ,lc.lob_branch branch_#
      ,lc.fru
      ,le.lease_num rer
      ,flv.meaning rer_type
      ,flv2.meaning rer_status
      ,loc_bld.location_code primary_loc
      ,ad.address_line1 || '' '' || ad.address_line2 primary_address
      ,ad.city
      ,(CASE ad.country
         WHEN ''US'' THEN
          ad.state
         ELSE
          ad.province
       END) st_prv
      ,ad.zip_code postal
      ,ad.country
      ,ld.lease_commencement_date commencement_date
      ,ld.lease_termination_date expiration
      ,mle.lease_num master_rer
      ,fu.description rem_name
  FROM pn.pn_leases_all              le
      ,pn.pn_leases_all              mle
      ,pn.pn_lease_details_all       ld
      ,pn.pn_locations_all           loc_bld
      ,pn.pn_addresses_all           ad
      ,gl.gl_code_combinations       gcc
      ,apps.xxcuspn_ld_lob_rollup_vw llr
      ,apps.fnd_lookups              flv
      ,apps.fnd_lookups              flv2
      ,applsys.fnd_user              fu
      ,xxcus.xxcus_location_code_tbl lc
 WHERE ad.address_id = loc_bld.address_id(+)
   AND loc_bld.location_id = le.location_id
   AND le.lease_id = ld.lease_id
   AND le.parent_lease_id = mle.lease_id(+)
   AND ld.responsible_user = fu.user_id(+)
   AND ld.expense_account_id = gcc.code_combination_id(+)
   AND gcc.segment1 = llr.fps_id(+)
   AND gcc.segment2 = lc.entrp_loc(+)
   AND le.lease_type_code = flv.lookup_code
   AND flv.lookup_type = ''PN_LEASE_TYPE''
   AND le.lease_status = flv2.lookup_code
   AND flv2.lookup_type = ''PN_LEASESTATUS_TYPE''
   AND loc_bld.active_end_date =
       (SELECT MAX(active_end_date)
          FROM pn.pn_locations_all
         WHERE location_id = loc_bld.location_id)
   AND flv2.meaning NOT IN (''Completed'', ''Pending Completion'')
   AND flv.meaning NOT IN (''Owned'', ''Non Leased'')

UNION -- Owned and Non Leased if expiration date is not 4712

SELECT CASE gcc.segment1
         WHEN ''06'' THEN
          ''PLB''
         WHEN ''07'' THEN
          ''PLB''
         ELSE
          llr.bu_id
       END bu
      ,lc.lob_branch branch_#
      ,lc.fru
      ,le.lease_num rer
      ,flv.meaning rer_type
      ,flv2.meaning rer_status
      ,loc_bld.location_code primary_loc
      ,ad.address_line1 || '' '' || ad.address_line2 primary_address
      ,ad.city
      ,(CASE ad.country
         WHEN ''US'' THEN
          ad.state
         ELSE
          ad.province
       END) st_prv
      ,ad.zip_code postal
      ,ad.country
      ,ld.lease_commencement_date commencement_date
      ,ld.lease_termination_date expiration
      ,mle.lease_num master_rer
      ,fu.description rem_name
  FROM pn.pn_leases_all              le
      ,pn.pn_leases_all              mle
      ,pn.pn_lease_details_all       ld
      ,pn.pn_locations_all           loc_bld
      ,pn.pn_addresses_all           ad
      ,gl.gl_code_combinations       gcc
      ,apps.xxcuspn_ld_lob_rollup_vw llr
      ,apps.fnd_lookups              flv
      ,apps.fnd_lookups              flv2
      ,applsys.fnd_user              fu
      ,xxcus.xxcus_location_code_tbl lc
 WHERE ad.address_id = loc_bld.address_id(+)
   AND loc_bld.location_id = le.location_id
   AND le.lease_id = ld.lease_id
   AND le.parent_lease_id = mle.lease_id(+)
   AND ld.responsible_user = fu.user_id(+)
   AND ld.expense_account_id = gcc.code_combination_id(+)
   AND gcc.segment1 = llr.fps_id(+)
   AND gcc.segment2 = lc.entrp_loc(+)
   AND le.lease_type_code = flv.lookup_code
   AND flv.lookup_type = ''PN_LEASE_TYPE''
   AND le.lease_status = flv2.lookup_code
   AND flv2.lookup_type = ''PN_LEASESTATUS_TYPE''
   AND loc_bld.active_end_date =
       (SELECT MAX(active_end_date)
          FROM pn.pn_locations_all
         WHERE location_id = loc_bld.location_id)
   AND flv2.meaning = ''Active''
   AND flv2.meaning NOT IN (''Completed'', ''Pending Completion'')
   AND flv.meaning IN (''Owned'', ''Non Leased'')
   AND ld.lease_termination_date <> ''31-Dec-4712''
 ORDER BY bu
         ,rem_name
         ,st_prv
         ,city
         ,rer

';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
v_objlength := DBMS_LOB.getlength (stmt);
IF v_objlength <= 32000 THEN 
EXECUTE IMMEDIATE to_char(stmt);
ELSE
v_offset := 1; 
WHILE v_offset <= v_objlength
LOOP
-- each record is 256 bytes from the LOB
v_sql_long (v_sql_long_count) := DBMS_LOB.SUBSTR (stmt, 256, v_offset);
-- increment the record count
v_sql_long_count := v_sql_long_count + 1;
-- figure out the new offset
v_offset := v_offset + 256;
END LOOP;
BEGIN
  -- open cursor
 c := DBMS_SQL.open_cursor;
 -- parse VARCHAR2S table
 DBMS_SQL.parse (c, v_sql_long, 1, v_sql_long_count - 1, FALSE, 1);
 -- Execute the cursor
r := DBMS_SQL.EXECUTE (c);
-- Close the cursor
 DBMS_SQL.close_cursor (c);
 END;
END IF;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 240');
END IF;
END;
/
prompt Creating Object Data XXEIS_296491_AYNPDA_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(240);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_296491_AYNPDA_V
xxeis.eis_rsc_ins.v( 'XXEIS_296491_AYNPDA_V',240,'Paste SQL View for Upcoming Expirations','1.0','','','ANONYMOUS','APPS','Upcoming Expirations View','X2AV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_296491_AYNPDA_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_296491_AYNPDA_V',240,FALSE);
--Inserting Object Columns for XXEIS_296491_AYNPDA_V
xxeis.eis_rsc_ins.vc( 'XXEIS_296491_AYNPDA_V','BRANCH_#',240,'','','','','','ANONYMOUS','VARCHAR2','','','Branch #','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_296491_AYNPDA_V','FRU',240,'','','','','','ANONYMOUS','VARCHAR2','','','Fru','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_296491_AYNPDA_V','RER',240,'','','','','','ANONYMOUS','VARCHAR2','','','Rer','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_296491_AYNPDA_V','RER_TYPE',240,'','','','','','ANONYMOUS','VARCHAR2','','','Rer Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_296491_AYNPDA_V','RER_STATUS',240,'','','','','','ANONYMOUS','VARCHAR2','','','Rer Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_296491_AYNPDA_V','PRIMARY_LOC',240,'','','','','','ANONYMOUS','VARCHAR2','','','Primary Loc','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_296491_AYNPDA_V','PRIMARY_ADDRESS',240,'','','','','','ANONYMOUS','VARCHAR2','','','Primary Address','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_296491_AYNPDA_V','CITY',240,'','','','','','ANONYMOUS','VARCHAR2','','','City','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_296491_AYNPDA_V','ST_PRV',240,'','','','','','ANONYMOUS','VARCHAR2','','','St Prv','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_296491_AYNPDA_V','COMMENCEMENT_DATE',240,'','','','','','ANONYMOUS','DATE','','','Commencement Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_296491_AYNPDA_V','EXPIRATION',240,'','','','','','ANONYMOUS','DATE','','','Expiration','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_296491_AYNPDA_V','MASTER_RER',240,'','','','','','ANONYMOUS','VARCHAR2','','','Master Rer','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_296491_AYNPDA_V','REM_NAME',240,'','','','','','ANONYMOUS','VARCHAR2','','','Rem Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_296491_AYNPDA_V','COUNTRY',240,'','','','','','ANONYMOUS','VARCHAR2','','','Country','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_296491_AYNPDA_V','POSTAL',240,'','','','','','ANONYMOUS','VARCHAR2','','','Postal','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_296491_AYNPDA_V','BU',240,'','','','','','ANONYMOUS','VARCHAR2','','','Bu','','','','US');
--Inserting Object Components for XXEIS_296491_AYNPDA_V
--Inserting Object Component Joins for XXEIS_296491_AYNPDA_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 240');
END IF;
END;
/
prompt Creating Report LOV Data for Upcoming Expirations
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(240);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Upcoming Expirations
xxeis.eis_rsc_ins.lov( '','select calendar_type,description,period_suffix_type,number_per_fiscal_year from fa_calendar_types',',','Calendar_type','Calendar_type LOV','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 240,'SELECT distinct BU_ID
FROM apps.XXCUSPN_LD_LOB_ROLLUP_VW
WHERE BU_ID IS NOT NULL','','XXEIS LOB ID','SELECT distinct BU_ID
FROM apps.XXCUSPN_LD_LOB_ROLLUP_VW
WHERE BU_ID IS NOT NULL','ANONYMOUS',NULL,'Y','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 240');
END IF;
END;
/
prompt Creating Report Data for Upcoming Expirations
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(240);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Upcoming Expirations
xxeis.eis_rsc_utility.delete_report_rows( 'Upcoming Expirations' );
--Inserting Report - Upcoming Expirations
xxeis.eis_rsc_ins.r( 240,'Upcoming Expirations','','','','','','AM033286','XXEIS_296491_AYNPDA_V','Y','','SELECT CASE gcc.segment1
         WHEN ''06'' THEN
          ''PLB''
         WHEN ''07'' THEN
          ''PLB''
         ELSE
          llr.bu_id
       END bu
      ,lc.lob_branch branch_#
      ,lc.fru
      ,le.lease_num rer
      ,flv.meaning rer_type
      ,flv2.meaning rer_status
      ,loc_bld.location_code primary_loc
      ,ad.address_line1 || '' '' || ad.address_line2 primary_address
      ,ad.city
      ,(CASE ad.country
         WHEN ''US'' THEN
          ad.state
         ELSE
          ad.province
       END) st_prv
      ,ad.zip_code postal
      ,ad.country
      ,ld.lease_commencement_date commencement_date
      ,ld.lease_termination_date expiration
      ,mle.lease_num master_rer
      ,fu.description rem_name
  FROM pn.pn_leases_all              le
      ,pn.pn_leases_all              mle
      ,pn.pn_lease_details_all       ld
      ,pn.pn_locations_all           loc_bld
      ,pn.pn_addresses_all           ad
      ,gl.gl_code_combinations       gcc
      ,apps.xxcuspn_ld_lob_rollup_vw llr
      ,apps.fnd_lookups              flv
      ,apps.fnd_lookups              flv2
      ,applsys.fnd_user              fu
      ,xxcus.xxcus_location_code_tbl lc
 WHERE ad.address_id = loc_bld.address_id(+)
   AND loc_bld.location_id = le.location_id
   AND le.lease_id = ld.lease_id
   AND le.parent_lease_id = mle.lease_id(+)
   AND ld.responsible_user = fu.user_id(+)
   AND ld.expense_account_id = gcc.code_combination_id(+)
   AND gcc.segment1 = llr.fps_id(+)
   AND gcc.segment2 = lc.entrp_loc(+)
   AND le.lease_type_code = flv.lookup_code
   AND flv.lookup_type = ''PN_LEASE_TYPE''
   AND le.lease_status = flv2.lookup_code
   AND flv2.lookup_type = ''PN_LEASESTATUS_TYPE''
   AND loc_bld.active_end_date =
       (SELECT MAX(active_end_date)
          FROM pn.pn_locations_all
         WHERE location_id = loc_bld.location_id)
   AND flv2.meaning NOT IN (''Completed'', ''Pending Completion'')
   AND flv.meaning NOT IN (''Owned'', ''Non Leased'')

UNION -- Owned and Non Leased if expiration date is not 4712

SELECT CASE gcc.segment1
         WHEN ''06'' THEN
          ''PLB''
         WHEN ''07'' THEN
          ''PLB''
         ELSE
          llr.bu_id
       END bu
      ,lc.lob_branch branch_#
      ,lc.fru
      ,le.lease_num rer
      ,flv.meaning rer_type
      ,flv2.meaning rer_status
      ,loc_bld.location_code primary_loc
      ,ad.address_line1 || '' '' || ad.address_line2 primary_address
      ,ad.city
      ,(CASE ad.country
         WHEN ''US'' THEN
          ad.state
         ELSE
          ad.province
       END) st_prv
      ,ad.zip_code postal
      ,ad.country
      ,ld.lease_commencement_date commencement_date
      ,ld.lease_termination_date expiration
      ,mle.lease_num master_rer
      ,fu.description rem_name
  FROM pn.pn_leases_all              le
      ,pn.pn_leases_all              mle
      ,pn.pn_lease_details_all       ld
      ,pn.pn_locations_all           loc_bld
      ,pn.pn_addresses_all           ad
      ,gl.gl_code_combinations       gcc
      ,apps.xxcuspn_ld_lob_rollup_vw llr
      ,apps.fnd_lookups              flv
      ,apps.fnd_lookups              flv2
      ,applsys.fnd_user              fu
      ,xxcus.xxcus_location_code_tbl lc
 WHERE ad.address_id = loc_bld.address_id(+)
   AND loc_bld.location_id = le.location_id
   AND le.lease_id = ld.lease_id
   AND le.parent_lease_id = mle.lease_id(+)
   AND ld.responsible_user = fu.user_id(+)
   AND ld.expense_account_id = gcc.code_combination_id(+)
   AND gcc.segment1 = llr.fps_id(+)
   AND gcc.segment2 = lc.entrp_loc(+)
   AND le.lease_type_code = flv.lookup_code
   AND flv.lookup_type = ''PN_LEASE_TYPE''
   AND le.lease_status = flv2.lookup_code
   AND flv2.lookup_type = ''PN_LEASESTATUS_TYPE''
   AND loc_bld.active_end_date =
       (SELECT MAX(active_end_date)
          FROM pn.pn_locations_all
         WHERE location_id = loc_bld.location_id)
   AND flv2.meaning = ''Active''
   AND flv2.meaning NOT IN (''Completed'', ''Pending Completion'')
   AND flv.meaning IN (''Owned'', ''Non Leased'')
   AND ld.lease_termination_date <> ''31-Dec-4712''
 ORDER BY bu
         ,rem_name
         ,st_prv
         ,city
         ,rer
','AM033286','','N','zzPeriodic Reporting Old','','CSV,Pivot Excel,EXCEL,','','','','','','','','APPS','US','','','','');
--Inserting Report Columns - Upcoming Expirations
xxeis.eis_rsc_ins.rc( 'Upcoming Expirations',240,'COMMENCEMENT_DATE','Commencement Date','','','','','','13','N','','','','','','','','AM033286','N','N','','XXEIS_296491_AYNPDA_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Upcoming Expirations',240,'BRANCH_#','Branch #','','','','','','2','N','','','','','','','','AM033286','N','N','','XXEIS_296491_AYNPDA_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Upcoming Expirations',240,'FRU','Fru','','','','','','3','N','','','','','','','','AM033286','N','N','','XXEIS_296491_AYNPDA_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Upcoming Expirations',240,'RER','Rer','','','','','','4','N','','','','','','','','AM033286','N','N','','XXEIS_296491_AYNPDA_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Upcoming Expirations',240,'RER_TYPE','Rer Type','','','','','','5','N','','','','','','','','AM033286','N','N','','XXEIS_296491_AYNPDA_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Upcoming Expirations',240,'RER_STATUS','Rer Status','','','','','','6','N','','','','','','','','AM033286','N','N','','XXEIS_296491_AYNPDA_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Upcoming Expirations',240,'PRIMARY_LOC','Primary Loc','','','','','','7','N','','','','','','','','AM033286','N','N','','XXEIS_296491_AYNPDA_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Upcoming Expirations',240,'PRIMARY_ADDRESS','Primary Address','','','','','','8','N','','','','','','','','AM033286','N','N','','XXEIS_296491_AYNPDA_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Upcoming Expirations',240,'CITY','City','','','','','','9','N','','','','','','','','AM033286','N','N','','XXEIS_296491_AYNPDA_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Upcoming Expirations',240,'ST_PRV','St Prv','','','','','','10','N','','','','','','','','AM033286','N','N','','XXEIS_296491_AYNPDA_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Upcoming Expirations',240,'EXPIRATION','Expiration','','','','','','14','N','','','','','','','','AM033286','N','N','','XXEIS_296491_AYNPDA_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Upcoming Expirations',240,'MASTER_RER','Master Rer','','','','','','15','N','','','','','','','','AM033286','N','N','','XXEIS_296491_AYNPDA_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Upcoming Expirations',240,'REM_NAME','Rem Name','','','','','','16','N','','','','','','','','AM033286','N','N','','XXEIS_296491_AYNPDA_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Upcoming Expirations',240,'COUNTRY','Country','','','','','','12','N','','','','','','','','AM033286','N','N','','XXEIS_296491_AYNPDA_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Upcoming Expirations',240,'POSTAL','Postal','','','','','','11','N','','','','','','','','AM033286','N','N','','XXEIS_296491_AYNPDA_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Upcoming Expirations',240,'BU','Bu','','','','','','1','N','','','','','','','','AM033286','N','N','','XXEIS_296491_AYNPDA_V','','','GROUP_BY','US','');
--Inserting Report Parameters - Upcoming Expirations
xxeis.eis_rsc_ins.rp( 'Upcoming Expirations',240,'Expiration Date - Start','','','IN','Calendar_type','01-JAN-2006','DATE','N','Y','1','','N','CONSTANT','AM033286','Y','N','','','','XXEIS_296491_AYNPDA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Upcoming Expirations',240,'Expiration Date - End','','','IN','Calendar_type','','DATE','N','Y','2','','N','CURRENT_DATE','AM033286','Y','N','','','','XXEIS_296491_AYNPDA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Upcoming Expirations',240,'BU','','','IN','XXEIS LOB ID','','VARCHAR2','N','Y','3','','N','CONSTANT','AM033286','Y','N','','','','XXEIS_296491_AYNPDA_V','','','US','');
--Inserting Dependent Parameters - Upcoming Expirations
--Inserting Report Conditions - Upcoming Expirations
xxeis.eis_rsc_ins.rcnh( 'Upcoming Expirations',240,'X2AV.BU IN :BU ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','BU','','','','','','','','','','','IN','Y','Y','X2AV.BU','','','','1',240,'Upcoming Expirations','X2AV.BU IN :BU ');
xxeis.eis_rsc_ins.rcnh( 'Upcoming Expirations',240,'X2AV.EXPIRATION BETWEEN :Expiration Date - Start  :Expiration Date - End','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Expiration Date - Start','','','Expiration Date - End','','','','','','','','BETWEEN','Y','Y','X2AV.EXPIRATION','','','','1',240,'Upcoming Expirations','X2AV.EXPIRATION BETWEEN :Expiration Date - Start  :Expiration Date - End');
--Inserting Report Sorts - Upcoming Expirations
xxeis.eis_rsc_ins.rs( 'Upcoming Expirations',240,'BU','ASC','AM033286','1','');
xxeis.eis_rsc_ins.rs( 'Upcoming Expirations',240,'REM_NAME','ASC','AM033286','2','');
xxeis.eis_rsc_ins.rs( 'Upcoming Expirations',240,'ST_PRV','ASC','AM033286','3','');
xxeis.eis_rsc_ins.rs( 'Upcoming Expirations',240,'CITY','ASC','AM033286','4','');
xxeis.eis_rsc_ins.rs( 'Upcoming Expirations',240,'RER','ASC','AM033286','5','');
--Inserting Report Triggers - Upcoming Expirations
--inserting report templates - Upcoming Expirations
--Inserting Report Portals - Upcoming Expirations
--inserting report dashboards - Upcoming Expirations
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Upcoming Expirations','240','XXEIS_296491_AYNPDA_V','XXEIS_296491_AYNPDA_V','N','');
--inserting report security - Upcoming Expirations
xxeis.eis_rsc_ins.rsec( 'Upcoming Expirations','20003','','XXCUS_OPN_RENT_ACCT',240,'AM033286','','','');
xxeis.eis_rsc_ins.rsec( 'Upcoming Expirations','20003','','XXCUS_OPN_PROP_MGR',240,'AM033286','','','');
xxeis.eis_rsc_ins.rsec( 'Upcoming Expirations','','AM033286','',240,'AM033286','','','');
xxeis.eis_rsc_ins.rsec( 'Upcoming Expirations','','JB010717','',240,'AM033286','','','');
xxeis.eis_rsc_ins.rsec( 'Upcoming Expirations','','JP006156','',240,'AM033286','','','');
xxeis.eis_rsc_ins.rsec( 'Upcoming Expirations','20003','','XXCUS_OPN_GENL_MOD',240,'AM033286','','','');
xxeis.eis_rsc_ins.rsec( 'Upcoming Expirations','','DV003828','',240,'AM033286','','','');
xxeis.eis_rsc_ins.rsec( 'Upcoming Expirations','','MC027824','',240,'AM033286','','','');
--Inserting Report Pivots - Upcoming Expirations
--Inserting Report   Version details- Upcoming Expirations
xxeis.eis_rsc_ins.rv( 'Upcoming Expirations','','Upcoming Expirations','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 240');
END IF;
END;
/
