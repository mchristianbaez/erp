--Report Name            : HDS AP Separation of Duties - AP PRO Vendor Action
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_AP_PRO_VA
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_AP_PRO_VA
xxeis.eis_rsc_ins.v( 'EIS_AP_PRO_VA',200,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Ap Pro Va1','EAPV1','','','VIEW','US','','');
--Delete Object Columns for EIS_AP_PRO_VA
xxeis.eis_rsc_utility.delete_view_rows('EIS_AP_PRO_VA',200,FALSE);
--Inserting Object Columns for EIS_AP_PRO_VA
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_VA','ATTACHMENT',200,'Attachment','ATTACHMENT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attachment','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_VA','OPERATING_UNIT',200,'Operating Unit','OPERATING_UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_VA','SITE_POS_ID',200,'Site Pos Id','SITE_POS_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Pos Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_VA','SITE_PAY_GROUP',200,'Site Pay Group','SITE_PAY_GROUP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Pay Group','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_VA','VENDOR_PAY_GROUP',200,'Vendor Pay Group','VENDOR_PAY_GROUP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Pay Group','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_VA','TAX_ID',200,'Tax Id','TAX_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Tax Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_VA','SITE_HOLD_REASON',200,'Site Hold Reason','SITE_HOLD_REASON','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Hold Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_VA','SITE_HOLD',200,'Site Hold','SITE_HOLD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Hold','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_VA','VENDOR_HOLD_REASON',200,'Vendor Hold Reason','VENDOR_HOLD_REASON','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Hold Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_VA','VENDOR_HOLD',200,'Vendor Hold','VENDOR_HOLD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Hold','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_VA','VENDOR_SITE_CODE',200,'Vendor Site Code','VENDOR_SITE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Site Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_VA','VENDOR_NAME',200,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_VA','VENDOR_ID',200,'Vendor Id','VENDOR_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_VA','NAME',200,'Name','NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_VA','USER_NAME',200,'User Name','USER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','User Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_VA','ACTION_DATE',200,'Action Date','ACTION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Action Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_VA','ACTION',200,'Action','ACTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Action','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PRO_VA','AUDIT_DATE',200,'Audit Date','AUDIT_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Audit Date','','','','US');
--Inserting Object Components for EIS_AP_PRO_VA
--Inserting Object Component Joins for EIS_AP_PRO_VA
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report LOV Data for HDS AP Separation of Duties - AP PRO Vendor Action
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS AP Separation of Duties - AP PRO Vendor Action
xxeis.eis_rsc_ins.lov( 200,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report Data for HDS AP Separation of Duties - AP PRO Vendor Action
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS AP Separation of Duties - AP PRO Vendor Action
xxeis.eis_rsc_utility.delete_report_rows( 'HDS AP Separation of Duties - AP PRO Vendor Action' );
--Inserting Report - HDS AP Separation of Duties - AP PRO Vendor Action
xxeis.eis_rsc_ins.r( 200,'HDS AP Separation of Duties - AP PRO Vendor Action','','','','','','XXEIS_RS_ADMIN','EIS_AP_PRO_VA','Y','','','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - HDS AP Separation of Duties - AP PRO Vendor Action
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'ACTION','Action','Action','','','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_VA','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'ACTION_DATE','Action Date','Action Date','','','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_VA','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'ATTACHMENT','Attachment','Attachment','','','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_VA','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'AUDIT_DATE','Audit Date','Audit Date','','','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_VA','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'NAME','Name','Name','','','default','','5','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_VA','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'SITE_HOLD','Site Hold','Site Hold','','','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_VA','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'SITE_HOLD_REASON','Site Hold Reason','Site Hold Reason','','','default','','7','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_VA','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'SITE_PAY_GROUP','Site Pay Group','Site Pay Group','','','default','','8','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_VA','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'SITE_POS_ID','Site Pos Id','Site Pos Id','','','default','','9','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_VA','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'TAX_ID','Tax Id','Tax Id','','','default','','10','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_VA','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'USER_NAME','User Name','User Name','','','default','','11','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_VA','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'VENDOR_HOLD','Vendor Hold','Vendor Hold','','','default','','12','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_VA','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'VENDOR_HOLD_REASON','Vendor Hold Reason','Vendor Hold Reason','','','default','','13','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_VA','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'VENDOR_ID','Vendor Id','Vendor Id','','','default','','14','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_VA','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','15','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_VA','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'VENDOR_PAY_GROUP','Vendor Pay Group','Vendor Pay Group','','','default','','16','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_VA','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'VENDOR_SITE_CODE','Vendor Site Code','Vendor Site Code','','','default','','17','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PRO_VA','','','','US','');
--Inserting Report Parameters - HDS AP Separation of Duties - AP PRO Vendor Action
xxeis.eis_rsc_ins.rp( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'Start_Date','Beginning of range of dates','','IN','','','DATE','N','Y','1','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','','EIS_AP_PRO_VA','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'End_Date','End of range of dates','','IN','','','DATE','N','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','','EIS_AP_PRO_VA','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'Organization Name','Organization Name','OPERATING_UNIT','IN','EIS_MULTI_OPERATING_UNIT_LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AP_PRO_VA','','','US','');
--Inserting Dependent Parameters - HDS AP Separation of Duties - AP PRO Vendor Action
--Inserting Report Conditions - HDS AP Separation of Duties - AP PRO Vendor Action
xxeis.eis_rsc_ins.rcnh( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'ACTION <> ''VENDOR_CREATION'' ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ACTION','','','','','','','EIS_AP_PRO_VA','','','','','','NOTEQUALS','Y','N','','''VENDOR_CREATION''','','','1',200,'HDS AP Separation of Duties - AP PRO Vendor Action','ACTION <> ''VENDOR_CREATION'' ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'ACTION_DATE BETWEEN :Start_Date  :End_Date','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ACTION_DATE','','Start_Date','','','End_Date','','EIS_AP_PRO_VA','','','','','','BETWEEN','Y','N','','','','','1',200,'HDS AP Separation of Duties - AP PRO Vendor Action','ACTION_DATE BETWEEN :Start_Date  :End_Date');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'OPERATING_UNIT IN :Organization Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','OPERATING_UNIT','','Organization Name','','','','','EIS_AP_PRO_VA','','','','','','IN','Y','Y','','','','','1',200,'HDS AP Separation of Duties - AP PRO Vendor Action','OPERATING_UNIT IN :Organization Name ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Separation of Duties - AP PRO Vendor Action',200,'USER_NAME <> ''LM027941'' ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','USER_NAME','','','','','','','EIS_AP_PRO_VA','','','','','','NOTEQUALS','Y','N','','''LM027941''','','','1',200,'HDS AP Separation of Duties - AP PRO Vendor Action','USER_NAME <> ''LM027941'' ');
--Inserting Report Sorts - HDS AP Separation of Duties - AP PRO Vendor Action
--Inserting Report Triggers - HDS AP Separation of Duties - AP PRO Vendor Action
--inserting report templates - HDS AP Separation of Duties - AP PRO Vendor Action
--Inserting Report Portals - HDS AP Separation of Duties - AP PRO Vendor Action
--inserting report dashboards - HDS AP Separation of Duties - AP PRO Vendor Action
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS AP Separation of Duties - AP PRO Vendor Action','200','EIS_AP_PRO_VA','EIS_AP_PRO_VA','N','');
--inserting report security - HDS AP Separation of Duties - AP PRO Vendor Action
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Vendor Action','200','','HDS_AP_ADMIN_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Vendor Action','200','','HDS_AP_DISBURSEMTS_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Vendor Action','200','','HDS_AP_INQUIRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Vendor Action','200','','HDS_AP_MGR_NOSUP_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Vendor Action','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Vendor Action','200','','HDS_AP_TRNS_ENTRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Vendor Action','200','','PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Vendor Action','200','','PAYABLES_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Vendor Action','20005','','XXWC_VIEW_ALL_EIS_REPORTS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Vendor Action','200','','HDS_AP_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Vendor Action','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Vendor Action','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Vendor Action','200','','HDS_AP_MGR_NOSUP_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Vendor Action','200','','HDS_AP_DISBURSEMTS_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Vendor Action','200','','HDS_AP_ADMIN_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Vendor Action','200','','HDS_PAYABLES_CLOSE_GSC_GLBL',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Separation of Duties - AP PRO Vendor Action','200','','HDS_PAYABLES_CLOSE_GLBL',200,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS AP Separation of Duties - AP PRO Vendor Action
--Inserting Report   Version details- HDS AP Separation of Duties - AP PRO Vendor Action
xxeis.eis_rsc_ins.rv( 'HDS AP Separation of Duties - AP PRO Vendor Action','','HDS AP Separation of Duties - AP PRO Vendor Action','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
