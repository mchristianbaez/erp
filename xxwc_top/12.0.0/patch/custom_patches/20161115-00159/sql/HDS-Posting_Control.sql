--Report Name            : HDS-Posting_Control
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_486_AZZRIW_V
prompt Table type Objects cannot be imported
set scan off define off
prompt Creating Object APPS.XXEIS_486_AZZRIW_V
set scan off define off
DECLARE
mod_exist varchar2(1);
stmt CLOB;
v_objlength INTEGER := 0;
v_offset INTEGER := 1;
v_sql_long DBMS_SQL.varchar2s;
v_sql_long_count INTEGER := 1;
c NUMBER;
r NUMBER;
l_stmt varchar2(32000);
l_view_cur pls_integer;
l_view_rows NUMBER;
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
dbms_lob.createtemporary(stmt,TRUE);
dbms_lob.write( lob_loc   => stmt,amount    => length('create or replace view APPS.XXEIS_486_AZZRIW_V
 ("CREATED_BY","DEFAULT_EFFECTIVE_DATE","DEFAULT_PERIOD_NAME","DESCRIPTION","NAME","POSTED_BY","POSTED_DATE","RUNNING_TOTAL_CR","RUNNING_TOTAL_DR","USER_NAME","EMAIL_ADDRESS"
) as '),offset    => 1, BUFFER    => 'create or replace view APPS.XXEIS_486_AZZRIW_V
 ("CREATED_BY","DEFAULT_EFFECTIVE_DATE","DEFAULT_PERIOD_NAME","DESCRIPTION","NAME","POSTED_BY","POSTED_DATE","RUNNING_TOTAL_CR","RUNNING_TOTAL_DR","USER_NAME","EMAIL_ADDRESS"
) as ');
l_stmt :=  'select b.CREATED_BY,
       b.DEFAULT_EFFECTIVE_DATE,
       b.DEFAULT_PERIOD_NAME,
       b.DESCRIPTION,
       b.NAME,
       b.POSTED_BY,
       b.POSTED_DATE,
       b.RUNNING_TOTAL_CR,
       b.RUNNING_TOTAL_DR,
       c.USER_NAME,
       C.EMAIL_ADDRESS
  FROM GL.GL_JE_BATCHES b, apps.fnd_user c
 WHERE b.CREATED_BY = b.POSTED_BY
 and b.posted_by = c.user_id
 and b.CREATED_BY <> 1229

';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
v_objlength := DBMS_LOB.getlength (stmt);
IF v_objlength <= 32000 THEN 
EXECUTE IMMEDIATE to_char(stmt);
ELSE
v_offset := 1; 
WHILE v_offset <= v_objlength
LOOP
-- each record is 256 bytes from the LOB
v_sql_long (v_sql_long_count) := DBMS_LOB.SUBSTR (stmt, 256, v_offset);
-- increment the record count
v_sql_long_count := v_sql_long_count + 1;
-- figure out the new offset
v_offset := v_offset + 256;
END LOOP;
BEGIN
  -- open cursor
 c := DBMS_SQL.open_cursor;
 -- parse VARCHAR2S table
 DBMS_SQL.parse (c, v_sql_long, 1, v_sql_long_count - 1, FALSE, 1);
 -- Execute the cursor
r := DBMS_SQL.EXECUTE (c);
-- Close the cursor
 DBMS_SQL.close_cursor (c);
 END;
END IF;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Object Data XXEIS_486_AZZRIW_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_486_AZZRIW_V
xxeis.eis_rsc_ins.v( 'XXEIS_486_AZZRIW_V',101,'Paste SQL View for HDS-Posting_Control','1.0','','','KP012542','APPS','HDS-Posting_Control View','X4AV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_486_AZZRIW_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_486_AZZRIW_V',101,FALSE);
--Inserting Object Columns for XXEIS_486_AZZRIW_V
xxeis.eis_rsc_ins.vc( 'XXEIS_486_AZZRIW_V','CREATED_BY',101,'','','','','','KP012542','NUMBER','','','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_486_AZZRIW_V','DEFAULT_EFFECTIVE_DATE',101,'','','','','','KP012542','DATE','','','Default Effective Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_486_AZZRIW_V','DEFAULT_PERIOD_NAME',101,'','','','','','KP012542','VARCHAR2','','','Default Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_486_AZZRIW_V','DESCRIPTION',101,'','','','','','KP012542','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_486_AZZRIW_V','NAME',101,'','','','','','KP012542','VARCHAR2','','','Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_486_AZZRIW_V','POSTED_BY',101,'','','','','','KP012542','NUMBER','','','Posted By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_486_AZZRIW_V','POSTED_DATE',101,'','','','','','KP012542','DATE','','','Posted Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_486_AZZRIW_V','RUNNING_TOTAL_CR',101,'','','','~T~D~2','','KP012542','NUMBER','','','Running Total Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_486_AZZRIW_V','RUNNING_TOTAL_DR',101,'','','','~T~D~2','','KP012542','NUMBER','','','Running Total Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_486_AZZRIW_V','USER_NAME',101,'','','','','','KP012542','VARCHAR2','','','User Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_486_AZZRIW_V','EMAIL_ADDRESS',101,'','','','','','KP012542','VARCHAR2','','','Email Address','','','','US');
--Inserting Object Components for XXEIS_486_AZZRIW_V
--Inserting Object Component Joins for XXEIS_486_AZZRIW_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report HDS-Posting_Control
prompt Creating Report Data for HDS-Posting_Control
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS-Posting_Control
xxeis.eis_rsc_utility.delete_report_rows( 'HDS-Posting_Control' );
--Inserting Report - HDS-Posting_Control
xxeis.eis_rsc_ins.r( 101,'HDS-Posting_Control','','Report to identify journals imported and posted by the same user','','','','XXEIS_RS_ADMIN','XXEIS_486_AZZRIW_V','Y','','  select b.CREATED_BY,
       b.DEFAULT_EFFECTIVE_DATE,
       b.DEFAULT_PERIOD_NAME,
       b.DESCRIPTION,
       b.NAME,
       b.POSTED_BY,
       b.POSTED_DATE,
       b.RUNNING_TOTAL_CR,
       b.RUNNING_TOTAL_DR,
       c.USER_NAME,
       C.EMAIL_ADDRESS
  FROM GL.GL_JE_BATCHES b, apps.fnd_user c
 WHERE b.CREATED_BY = b.POSTED_BY
 and b.posted_by = c.user_id
 and b.CREATED_BY <> 1229
','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','APPS','US','','','','');
--Inserting Report Columns - HDS-Posting_Control
xxeis.eis_rsc_ins.rc( 'HDS-Posting_Control',101,'CREATED_BY','Created By','','','~~~','default','','','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_486_AZZRIW_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Posting_Control',101,'DEFAULT_EFFECTIVE_DATE','Default Effective Date','','','','default','','','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_486_AZZRIW_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Posting_Control',101,'DEFAULT_PERIOD_NAME','Default Period Name','','','','default','','','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_486_AZZRIW_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Posting_Control',101,'DESCRIPTION','Description','','','','default','','','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_486_AZZRIW_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Posting_Control',101,'NAME','Name','','','','default','','','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_486_AZZRIW_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Posting_Control',101,'POSTED_BY','Posted By','','','~~~','default','','','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_486_AZZRIW_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Posting_Control',101,'POSTED_DATE','Posted Date','','','','default','','','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_486_AZZRIW_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Posting_Control',101,'RUNNING_TOTAL_CR','Running Total Cr','','','~T~D~2','default','','','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_486_AZZRIW_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Posting_Control',101,'RUNNING_TOTAL_DR','Running Total Dr','','','~T~D~2','default','','','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_486_AZZRIW_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Posting_Control',101,'USER_NAME','User Name','','','','default','','','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_486_AZZRIW_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Posting_Control',101,'EMAIL_ADDRESS','Email Address','','','','default','','','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_486_AZZRIW_V','','','','US','');
--Inserting Report Parameters - HDS-Posting_Control
xxeis.eis_rsc_ins.rp( 'HDS-Posting_Control',101,'Posted Date From','','POSTED_DATE','IN','','','DATE','N','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','','XXEIS_486_AZZRIW_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Posting_Control',101,'Posted Date To','','POSTED_DATE','IN','','','DATE','N','Y','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','','XXEIS_486_AZZRIW_V','','','US','');
--Inserting Dependent Parameters - HDS-Posting_Control
--Inserting Report Conditions - HDS-Posting_Control
xxeis.eis_rsc_ins.rcnh( 'HDS-Posting_Control',101,'POSTED_DATE BETWEEN :Posted Date From  :Posted Date To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','POSTED_DATE','','Posted Date From','','','Posted Date To','','XXEIS_486_AZZRIW_V','','','','','','BETWEEN','Y','Y','','','','','1',101,'HDS-Posting_Control','POSTED_DATE BETWEEN :Posted Date From  :Posted Date To');
--Inserting Report Sorts - HDS-Posting_Control
--Inserting Report Triggers - HDS-Posting_Control
--inserting report templates - HDS-Posting_Control
--Inserting Report Portals - HDS-Posting_Control
--inserting report dashboards - HDS-Posting_Control
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS-Posting_Control','101','XXEIS_486_AZZRIW_V','XXEIS_486_AZZRIW_V','N','');
--inserting report security - HDS-Posting_Control
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','XXCUS_GL_ACCOUNTANT_CAD',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','XXCUS_GL_ACCOUNTANT_USD',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','XXCUS_GL_INQUIRY',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','XXCUS_GL_MANAGER',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','GNRL_LDGR_FSS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','GNRL_LDGR_LTMR_ACCNTNT',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','HDS GL INQUIRY',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','GNRL_LDGR_LTMR_NQR',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','HDS_CAD_MNTH_END_PROCS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','GNRL_LDGR_LTMR_FSS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','HDS_GNRL_LDGR_CAD',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','HDS_GNRL_LDGR_SPR_USR',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','XXCUS_GL_ACCOUNTANT_CAD_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','XXCUS_GL_ACCOUNTANT_GLOBAL',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','XXCUS_GL_ACCOUNTANT_USD_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','XXCUS_GL_INQUIRY_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','XXCUS_GL_MANAGER_GLOBAL',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','XXCUS_GL_MANAGER_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','20005','','XXWC_VIEW_ALL_EIS_REPORTS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','XXCUS_GL_MANAGER_PS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','XXCUS_GL_INQUIRY_PS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','XXCUS_GL_ACCOUNTANT_USD_PS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Posting_Control','101','','XXCUS_GL_ACCOUNTANT_CAD_PS',101,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS-Posting_Control
--Inserting Report   Version details- HDS-Posting_Control
xxeis.eis_rsc_ins.rv( 'HDS-Posting_Control','','HDS-Posting_Control','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
