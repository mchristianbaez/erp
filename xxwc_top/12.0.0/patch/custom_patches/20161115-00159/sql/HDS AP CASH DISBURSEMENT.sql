--Report Name            : HDS AP CASH DISBURSEMENT
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_CASH_DISBURSMENT
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_CASH_DISBURSMENT
xxeis.eis_rsc_ins.v( 'XXEIS_CASH_DISBURSMENT',200,'Paste SQL View for HDS AP CASH DISBURSEMENT','1.0','','','XXEIS_RS_ADMIN','XXEIS','XXEIS_CASH_DISBURSMENT','X4TV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_CASH_DISBURSMENT
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_CASH_DISBURSMENT',200,FALSE);
--Inserting Object Columns for XXEIS_CASH_DISBURSMENT
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','BANK_ACCOUNT_NAME',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bank Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','CHECK_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Check Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','CHECK_NUMBER',200,'','','','','','XXEIS_RS_ADMIN','NUMBER','','','Check Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','PAYMENT_STATUS_DESCR',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Status Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','VOID_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Void Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','STOP_PAYMENT_RECORDED_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Stop Payment Recorded Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','VENDOR_NAME',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','ADDRESS_LINE1',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','ADDRESS_LINE2',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','ADDRESS_LINE3',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line3','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','VENDOR_CITY',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor City','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','VENDOR_POSTAL_CODE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Postal Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','VENDOR_STATE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor State','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','VENDOR_SITE_CODE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Site Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','PAYMENT_TYPE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','PAY_GROUP',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pay Group','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','SUPPLIER_NUMBER',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Supplier Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','CLEARED_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Cleared Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','AMOUNT',200,'','','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','PAYMENT_METHOD_CODE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Method Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_CASH_DISBURSMENT','PAYMENT_PRIORITY',200,'','','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Priority','','','','US');
--Inserting Object Components for XXEIS_CASH_DISBURSMENT
--Inserting Object Component Joins for XXEIS_CASH_DISBURSMENT
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report LOV Data for HDS AP CASH DISBURSEMENT
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS AP CASH DISBURSEMENT
xxeis.eis_rsc_ins.lov( 200,'SELECT DISTINCT BANK_ACCOUNT_NAME
  FROM AP.AP_CHECKS_ALL','','XXCUS_BANK_ACCOUNT','AP Bank Account Name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 200,'select distinct payment_method_code
FROM ap.ap_checks_all','','HDS_Payment_Method_Code','Payment Method Code from ap.ap_checks_all','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report Data for HDS AP CASH DISBURSEMENT
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS AP CASH DISBURSEMENT
xxeis.eis_rsc_utility.delete_report_rows( 'HDS AP CASH DISBURSEMENT' );
--Inserting Report - HDS AP CASH DISBURSEMENT
xxeis.eis_rsc_ins.r( 200,'HDS AP CASH DISBURSEMENT','','','','','','XXEIS_RS_ADMIN','XXEIS_CASH_DISBURSMENT','Y','','','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','Y','','','','','','','','US','','','','');
--Inserting Report Columns - HDS AP CASH DISBURSEMENT
xxeis.eis_rsc_ins.rc( 'HDS AP CASH DISBURSEMENT',200,'ADDRESS_LINE1','Address Line1','','','','default','','9','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP CASH DISBURSEMENT',200,'ADDRESS_LINE2','Address Line2','','','','default','','10','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP CASH DISBURSEMENT',200,'ADDRESS_LINE3','Address Line3','','','','','','11','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP CASH DISBURSEMENT',200,'AMOUNT','Amount','','','~T~D~2','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP CASH DISBURSEMENT',200,'BANK_ACCOUNT_NAME','Bank Account Name','','','','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP CASH DISBURSEMENT',200,'CHECK_DATE','Check Date','','','','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP CASH DISBURSEMENT',200,'CHECK_NUMBER','Check Number','','','~~~','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP CASH DISBURSEMENT',200,'PAYMENT_STATUS_DESCR','Payment Status Descr','','','','default','','5','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP CASH DISBURSEMENT',200,'PAYMENT_TYPE','Payment Type','','','','','','16','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP CASH DISBURSEMENT',200,'PAY_GROUP','Pay Group','','','','','','17','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP CASH DISBURSEMENT',200,'STOP_PAYMENT_RECORDED_DATE','Stop Payment Recorded Date','','','','default','','7','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP CASH DISBURSEMENT',200,'SUPPLIER_NUMBER','Supplier Number','','','','','','18','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP CASH DISBURSEMENT',200,'VENDOR_CITY','Vendor City','','','','','','12','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP CASH DISBURSEMENT',200,'VENDOR_NAME','Vendor Name','','','','default','','8','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP CASH DISBURSEMENT',200,'VENDOR_POSTAL_CODE','Vendor Postal Code','','','','','','13','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP CASH DISBURSEMENT',200,'VENDOR_SITE_CODE','Vendor Site Code','','','','','','15','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP CASH DISBURSEMENT',200,'VENDOR_STATE','Vendor State','','','','','','14','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP CASH DISBURSEMENT',200,'VOID_DATE','Void Date','','','','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP CASH DISBURSEMENT',200,'PAYMENT_METHOD_CODE','Payment Method Code','','','','','','19','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_CASH_DISBURSMENT','','','GROUP_BY','US','');
--Inserting Report Parameters - HDS AP CASH DISBURSEMENT
xxeis.eis_rsc_ins.rp( 'HDS AP CASH DISBURSEMENT',200,'Check Date From','Check Date From','CHECK_DATE','IN','','01-JAN-2005','DATE','N','Y','1','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','','XXEIS_CASH_DISBURSMENT','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP CASH DISBURSEMENT',200,'Check Date To','Check Date To','CHECK_DATE','IN','','','DATE','N','Y','2','','N','CURRENT_DATE','XXEIS_RS_ADMIN','Y','N','','End Date','','XXEIS_CASH_DISBURSMENT','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP CASH DISBURSEMENT',200,'Void Date','Void Date','VOID_DATE','>','','','DATE','N','Y','3','','N','CURRENT_DATE','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_CASH_DISBURSMENT','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP CASH DISBURSEMENT',200,'Cleared Date','Cleared Date','CLEARED_DATE','>','','','DATE','N','Y','4','','N','CURRENT_DATE','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_CASH_DISBURSMENT','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP CASH DISBURSEMENT',200,'Bank Account','Bank Account','BANK_ACCOUNT_NAME','IN','XXCUS_BANK_ACCOUNT','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_CASH_DISBURSMENT','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS AP CASH DISBURSEMENT',200,'Payment Method','Payment Method','PAYMENT_METHOD_CODE','IN','HDS_Payment_Method_Code','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_CASH_DISBURSMENT','','','US','');
--Inserting Dependent Parameters - HDS AP CASH DISBURSEMENT
--Inserting Report Conditions - HDS AP CASH DISBURSEMENT
xxeis.eis_rsc_ins.rcnh( 'HDS AP CASH DISBURSEMENT',200,'BANK_ACCOUNT_NAME IN :Bank Account ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BANK_ACCOUNT_NAME','','Bank Account','','','','','XXEIS_CASH_DISBURSMENT','','','','','','IN','Y','Y','','','','','1',200,'HDS AP CASH DISBURSEMENT','BANK_ACCOUNT_NAME IN :Bank Account ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP CASH DISBURSEMENT',200,'PAYMENT_METHOD_CODE IN :Payment Method ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PAYMENT_METHOD_CODE','','Payment Method','','','','','XXEIS_CASH_DISBURSMENT','','','','','','IN','Y','Y','','','','','1',200,'HDS AP CASH DISBURSEMENT','PAYMENT_METHOD_CODE IN :Payment Method ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP CASH DISBURSEMENT',200,'Free Text ','FREE_TEXT','','','N','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND    (((X4TV.void_date IS NULL OR X4TV.void_date > :Void Date)
AND    X4TV.CHECK_DATE BETWEEN :Check Date From AND :Check Date To
AND    (X4TV.CLEARED_DATE > :Cleared Date OR X4TV.CLEARED_DATE IS NULL)))
group by
         X4TV.bank_account_name,
          X4TV.check_date,
          X4TV.check_number,
          X4TV.PAYMENT_STATUS_DESCR,
          X4TV.VOID_DATE,
          X4TV.STOP_PAYMENT_RECORDED_DATE,
          X4TV.VENDOR_NAME,
          X4TV.ADDRESS_LINE1,
          X4TV.ADDRESS_LINE2,
          X4TV.ADDRESS_LINE3,
          X4TV.VENDOR_CITY,
          X4TV.VENDOR_POSTAL_CODE,
          X4TV.VENDOR_STATE,
          X4TV.VENDOR_SITE_CODE,
          X4TV.PAYMENT_TYPE,
          X4TV.PAY_GROUP,
          X4TV.SUPPLIER_NUMBER,
          X4TV.cleared_date,
          X4TV.payment_priority,
          X4TV.AMOUNT,
          X4TV.PAYMENT_METHOD_CODE','1',200,'HDS AP CASH DISBURSEMENT','Free Text ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP CASH DISBURSEMENT',200,'Free text Corrected','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','Y','','','','AND    (((X4TV.void_date IS NULL OR X4TV.void_date > :Void Date)
AND    X4TV.CHECK_DATE BETWEEN :Check Date From AND :Check Date To
AND    (X4TV.CLEARED_DATE > :Cleared Date OR X4TV.CLEARED_DATE IS NULL)))','1',200,'HDS AP CASH DISBURSEMENT','Free text Corrected');
--Inserting Report Sorts - HDS AP CASH DISBURSEMENT
--Inserting Report Triggers - HDS AP CASH DISBURSEMENT
--inserting report templates - HDS AP CASH DISBURSEMENT
--Inserting Report Portals - HDS AP CASH DISBURSEMENT
--inserting report dashboards - HDS AP CASH DISBURSEMENT
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS AP CASH DISBURSEMENT','200','XXEIS_CASH_DISBURSMENT','XXEIS_CASH_DISBURSMENT','N','');
--inserting report security - HDS AP CASH DISBURSEMENT
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_OIE_USER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_AP_ADMIN_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_AP_DISBURSEMTS_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_AP_INQUIRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_AP_MGR_NOSUP_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_AP_TRNS_ENTRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_AP_ADMIN_US_GSCIWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_AP_ADMIN_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_AP_DISBUREMTS_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_AP_INQUIRY_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_AP_TRNS_ENTRY_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','PAYABLES_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','XXWC_PAY_NO_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','XXWC_PAY_W_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','XXWC_PAY_DISBURSE',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','XXWC_PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','XXWC_PAY_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','XXWC_PAY_VENDOR_MSTR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_AP_MGR_NOSUP_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_AP_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_PYABLS_MNGR_CAN',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_PYBLS_MNGR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','20005','','XXWC_VIEW_ALL_EIS_REPORTS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_AP_MGR_NOSUP_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_AP_DISBURSEMTS_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_AP_ADMIN_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_PAYABLES_CLOSE_GSC_GLBL',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP CASH DISBURSEMENT','200','','HDS_PAYABLES_CLOSE_GLBL',200,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS AP CASH DISBURSEMENT
--Inserting Report   Version details- HDS AP CASH DISBURSEMENT
xxeis.eis_rsc_ins.rv( 'HDS AP CASH DISBURSEMENT','','HDS AP CASH DISBURSEMENT','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
