--Report Name            : Agreement Detail Validation - Step 2
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_911488_HHKGGJ_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_911488_HHKGGJ_V
xxeis.eis_rsc_ins.v( 'XXEIS_911488_HHKGGJ_V',682,'Paste SQL View for Agreement Data Validation - Step 2','1.0','','','DV003828','APPS','Agreement Data Validation - Step 2 View','X9HV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_911488_HHKGGJ_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_911488_HHKGGJ_V',682,FALSE);
--Inserting Object Columns for XXEIS_911488_HHKGGJ_V
xxeis.eis_rsc_ins.vc( 'XXEIS_911488_HHKGGJ_V','COMMENTS',682,'','','','','','DV003828','VARCHAR2','','','Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_911488_HHKGGJ_V','MVID',682,'','','','','','DV003828','VARCHAR2','','','Mvid','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_911488_HHKGGJ_V','MV_NAME',682,'','','','','','DV003828','VARCHAR2','','','Mv Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_911488_HHKGGJ_V','AGREEMENT_NAME',682,'','','','','','DV003828','VARCHAR2','','','Agreement Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_911488_HHKGGJ_V','AGREEMENT_YEAR',682,'','','','','','DV003828','VARCHAR2','','','Agreement Year','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_911488_HHKGGJ_V','AGREEMENT_STATUS',682,'','','','','','DV003828','VARCHAR2','','','Agreement Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_911488_HHKGGJ_V','ACTIVITY',682,'','','','','','DV003828','VARCHAR2','','','Activity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_911488_HHKGGJ_V','AGREEMENT_START_DATE',682,'','','','','','DV003828','DATE','','','Agreement Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_911488_HHKGGJ_V','AGREEMENT_END_DATE',682,'','','','','','DV003828','DATE','','','Agreement End Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_911488_HHKGGJ_V','STOCK_RECEIPT_START_DATE',682,'','','','','','DV003828','DATE','','','Stock Receipt Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_911488_HHKGGJ_V','STOCK_RECEIPT_END_DATE',682,'','','','','','DV003828','DATE','','','Stock Receipt End Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_911488_HHKGGJ_V','PROGRAM_NAME',682,'','','','','','DV003828','VARCHAR2','','','Program Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_911488_HHKGGJ_V','PROGRAM_START_DATE',682,'','','','','','DV003828','DATE','','','Program Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_911488_HHKGGJ_V','CURRENCY_CODE',682,'','','','','','DV003828','VARCHAR2','','','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_911488_HHKGGJ_V','ORG',682,'','','','','','DV003828','VARCHAR2','','','Org','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_911488_HHKGGJ_V','PROGRAM_END_DATE',682,'','','','','','DV003828','DATE','','','Program End Date','','','','US');
--Inserting Object Components for XXEIS_911488_HHKGGJ_V
--Inserting Object Component Joins for XXEIS_911488_HHKGGJ_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report LOV Data for Agreement Detail Validation - Step 2
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Agreement Detail Validation - Step 2
xxeis.eis_rsc_ins.lov( 682,'select distinct status_code
from ozf.OZF_OFFERS','','LOV AGREEMENT_STATUS','LOV STATUS CODE FROM ozf.OZF_OFFERS  TABLE IS THE AGREEMENT STATUS','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'select distinct nvl(attribute7, ''NULL'') agreement_year
from apps.QP_LIST_HEADERS_VL','','LOV AGREEMENT_YEAR','LOV attribute7 from apps.QP_LIST_HEADERS_VL  table is the agreement year','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'select distinct party_name
from ar.HZ_PARTIES
where ATTRIBUTE1 = ''HDS_MVID''','','LOV VENDOR_NAME','VENDOR NAME FROM ar.HZ_PARTIES
','ID020048',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report Data for Agreement Detail Validation - Step 2
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Agreement Detail Validation - Step 2
xxeis.eis_rsc_utility.delete_report_rows( 'Agreement Detail Validation - Step 2' );
--Inserting Report - Agreement Detail Validation - Step 2
xxeis.eis_rsc_ins.r( 682,'Agreement Detail Validation - Step 2','','Second Report used to confirm that new agreement setup is accurate (all criteria types could not be built into a single report due to differences in logic).  Inaccurate setup can lead to incorrect accruals.  Confirm that new agreement setup does not show up on the report.  If so, make necessary corrections and rerun the report','','','','MC027824','XXEIS_911488_HHKGGJ_V','Y','','---1  Rebate Agreement Validation - CAN Program_Currency
SELECT ''CAD program - program descr not like FY''||AGREEMENT_YEAR||''-CAD%'' COMMENTS
,MVID
,MV_NAME
, AGREEMENt_NAME
,AGREEMENT_YEAR
,AGREEMENT_STATUS
, ACTIVITY
,AGREEMENT_START_DATE
,AGREEMENT_END_DATE
, STOCK_RECEIPT_START_DATE
, STOCK_RECEIPT_END_DATE
,PROGRAM_NAME
, PROGRAM_START_DATE
,PROGRAM_END_DATE
,ORG
,currency_code

From apps.XXEIS_906488_KHATPX_V r
WHERE 1=1

AND GLOBAL_FLAG=''N''
AND ORG = ''HDS Rebates CAN - Org''
AND PROGRAM_NAME  NOT LIKE ''%FY''||agreement_year||''-CAD%''
and agreement_year>2012

union

--- 2   Rebate Agreement Validation - US Program_Currency
SELECT ''USD program - program descr not like FY''||agreement_year||''-USD%'' Comments
,MVID
,MV_NAME
, AGREEMENt_NAME
,AGREEMENT_YEAR
,AGREEMENT_STATUS
, ACTIVITY
,AGREEMENT_START_DATE
,AGREEMENT_END_DATE
, STOCK_RECEIPT_START_DATE
, STOCK_RECEIPT_END_DATE
,PROGRAM_NAME
, PROGRAM_START_DATE
,PROGRAM_END_DATE
,ORG
,currency_code
From apps.XXEIS_906488_KHATPX_V r
Where 1=1
And agreement_year>2012
AND GLOBAL_FLAG=''N''
AND ORG = ''HDS Rebates USA - Org''
AND PROGRAM_NAME  NOT LIKE ''FY''||agreement_year||''-USD%''

union

--- 3  Rebate Agreement Validation - Different Currency Than CAD ORG
SELECT ''Different currency than CAD ORG'' Comments
,MVID
,MV_NAME
, AGREEMENt_NAME
,AGREEMENT_YEAR
,AGREEMENT_STATUS
, ACTIVITY
,AGREEMENT_START_DATE
,AGREEMENT_END_DATE
, STOCK_RECEIPT_START_DATE
, STOCK_RECEIPT_END_DATE
,PROGRAM_NAME
, PROGRAM_START_DATE
,PROGRAM_END_DATE
,ORG
,currency_code
From apps.XXEIS_906488_KHATPX_V r
WHERE 1=1
And agreement_year>2012
AND GLOBAL_FLAG=''N''
AND ORG = ''HDS Rebates CAN - Org''
AND CURRENCY_CODE not in (''CAD'')

UNION

--- 4  Rebate Agreement Validation - Different Currency Than US ORG
SELECT ''Different currency than US ORG'' COMMENTS
,MVID
,MV_NAME
, AGREEMENt_NAME
,AGREEMENT_YEAR
,AGREEMENT_STATUS
, ACTIVITY
,AGREEMENT_START_DATE
,AGREEMENT_END_DATE
, STOCK_RECEIPT_START_DATE
, STOCK_RECEIPT_END_DATE
,PROGRAM_NAME
, PROGRAM_START_DATE
,PROGRAM_END_DATE
,ORG
,currency_code
 From apps.XXEIS_906488_KHATPX_V r
WHERE 1=1
And agreement_year>2012
AND GLOBAL_FLAG=''N''
AND ORG = ''HDS Rebates USA - Org''
AND CURRENCY_CODE NOT IN(''USD'')

UNION

----5 Invalid Program Start Date
SELECT ''Invalid Program Start Date'' COMMENTS
,MVID
,MV_NAME
, AGREEMENt_NAME
,AGREEMENT_YEAR
,AGREEMENT_STATUS
, ACTIVITY
,AGREEMENT_START_DATE
,AGREEMENT_END_DATE
, STOCK_RECEIPT_START_DATE
, STOCK_RECEIPT_END_DATE
,PROGRAM_NAME
, PROGRAM_START_DATE
,PROGRAM_END_DATE
,ORG
,currency_code
 From apps.XXEIS_906488_KHATPX_V r
WHERE 1=1
and program_start_date>trunc(TO_DATE(agreement_year,''YYYY''),''YEAR'')
And agreement_year>2012

UNION

----6 Invalid Program End Date
SELECT ''Invalid Program End Date'' COMMENTS
,MVID
,MV_NAME
, AGREEMENt_NAME
,AGREEMENT_YEAR
,AGREEMENT_STATUS
, ACTIVITY
,AGREEMENT_START_DATE
,AGREEMENT_END_DATE
, STOCK_RECEIPT_START_DATE
, STOCK_RECEIPT_END_DATE
,PROGRAM_NAME
, PROGRAM_START_DATE
,PROGRAM_END_DATE
,ORG
,currency_code
 From apps.XXEIS_906488_KHATPX_V r
WHERE 1=1
AND PROGRAM_END_DATE<trunc(TO_DATE(agreement_year,''YYYY''),''YEAR'')
And agreement_year>2012

union
----7 Invalid agreement name

SELECT ''Invalid Agreement Name'' COMMENTS
,MVID
,MV_NAME
, AGREEMENt_NAME
,AGREEMENT_YEAR
,AGREEMENT_STATUS
, ACTIVITY
,AGREEMENT_START_DATE
,AGREEMENT_END_DATE
, STOCK_RECEIPT_START_DATE
, STOCK_RECEIPT_END_DATE
,PROGRAM_NAME
, PROGRAM_START_DATE
,PROGRAM_END_DATE
,ORG
,currency_code
 From apps.XXEIS_906488_KHATPX_V r
WHERE 1=1
AND AGREEMENT_NAME NOT LIKE ''%FY''||AGREEMENT_YEAR||''-%''
and agreement_year>2012

 UNION
 --8 --Invalid Stock Receipt Start Date
SELECT ''Invalid Stock Receipt Start Date'' COMMENTS
,MVID
,MV_NAME
, AGREEMENt_NAME
,AGREEMENT_YEAR
,AGREEMENT_STATUS
, ACTIVITY
,AGREEMENT_START_DATE
,AGREEMENT_END_DATE
, STOCK_RECEIPT_START_DATE
, STOCK_RECEIPT_END_DATE
,PROGRAM_NAME
, PROGRAM_START_DATE
,PROGRAM_END_DATE
,ORG
,currency_code
 From apps.XXEIS_906488_KHATPX_V r
WHERE 1=1
and agreement_year>2012
AND STOCK_RECEIPT_START_DATE<>trunc(TO_DATE(agreement_year,''YYYY''),''YEAR'')
AND stock_receipt_END_DATE<>add_months(trunc(TO_DATE(agreement_year,''YYYY''),''YEAR''),12)-1
and  STOCK_RECEIPT_END_DATE - STOCK_RECEIPT_START_DATE not in (30,89,90,91,364,365,366,180,181,179)

UNION
--9--Invalid Stock Receipt End Date
SELECT ''Invalid Stock Receipt End Date'' COMMENTS
,MVID
,MV_NAME
, AGREEMENt_NAME
,AGREEMENT_YEAR
,AGREEMENT_STATUS
, ACTIVITY
,AGREEMENT_START_DATE
,AGREEMENT_END_DATE
, STOCK_RECEIPT_START_DATE
, STOCK_RECEIPT_END_DATE
,PROGRAM_NAME
, PROGRAM_START_DATE
,PROGRAM_END_DATE
,ORG
,currency_code
 From apps.XXEIS_906488_KHATPX_V r
WHERE 1=1
AND stock_receipt_END_DATE<>add_months(trunc(TO_DATE(agreement_year,''YYYY''),''YEAR''),12)-1
and agreement_year >2012
and  STOCK_RECEIPT_END_DATE - STOCK_RECEIPT_START_DATE not in (30,89,90,91,364,365,366,180,181,179)
','MC027824','','N','VALIDATION - AGREEMENT SETUP','','CSV,Pivot Excel,EXCEL,','','','','','','','','APPS','US','','','','');
--Inserting Report Columns - Agreement Detail Validation - Step 2
xxeis.eis_rsc_ins.rc( 'Agreement Detail Validation - Step 2',682,'AGREEMENT_END_DATE','Agreement End Date','','','','default','','9','N','','','','','','','','MC027824','N','N','','XXEIS_911488_HHKGGJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Agreement Detail Validation - Step 2',682,'AGREEMENT_STATUS','Agreement Status','','','','default','','7','N','','','','','','','','MC027824','N','N','','XXEIS_911488_HHKGGJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Agreement Detail Validation - Step 2',682,'AGREEMENT_YEAR','Agreement Year','','','','default','','5','N','','','','','','','','MC027824','N','N','','XXEIS_911488_HHKGGJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Agreement Detail Validation - Step 2',682,'MVID','Mvid','','','','default','','2','N','','','','','','','','MC027824','N','N','','XXEIS_911488_HHKGGJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Agreement Detail Validation - Step 2',682,'PROGRAM_START_DATE','Program Start Date','','','','default','','13','N','','','','','','','','MC027824','N','N','','XXEIS_911488_HHKGGJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Agreement Detail Validation - Step 2',682,'ACTIVITY','Activity','','','','default','','6','N','','','','','','','','MC027824','N','N','','XXEIS_911488_HHKGGJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Agreement Detail Validation - Step 2',682,'PROGRAM_NAME','Program Name','','','','default','','12','N','','','','','','','','MC027824','N','N','','XXEIS_911488_HHKGGJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Agreement Detail Validation - Step 2',682,'STOCK_RECEIPT_START_DATE','Stock Receipt Start Date','','','','default','','10','N','','','','','','','','MC027824','N','N','','XXEIS_911488_HHKGGJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Agreement Detail Validation - Step 2',682,'COMMENTS','Comments','','','','default','','1','N','','','','','','','','MC027824','N','N','','XXEIS_911488_HHKGGJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Agreement Detail Validation - Step 2',682,'CURRENCY_CODE','Currency Code','','','','default','','16','N','','','','','','','','MC027824','N','N','','XXEIS_911488_HHKGGJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Agreement Detail Validation - Step 2',682,'MV_NAME','Vendor','','','','default','','3','N','','','','','','','','MC027824','N','N','','XXEIS_911488_HHKGGJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Agreement Detail Validation - Step 2',682,'STOCK_RECEIPT_END_DATE','Stock Receipt End Date','','','','default','','11','N','','','','','','','','MC027824','N','N','','XXEIS_911488_HHKGGJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Agreement Detail Validation - Step 2',682,'AGREEMENT_NAME','Agreement Name','','','','default','','4','N','','','','','','','','MC027824','N','N','','XXEIS_911488_HHKGGJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Agreement Detail Validation - Step 2',682,'AGREEMENT_START_DATE','Agreement Start Date','','','','default','','8','N','','','','','','','','MC027824','N','N','','XXEIS_911488_HHKGGJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Agreement Detail Validation - Step 2',682,'ORG','Org','','','','default','','15','N','','','','','','','','MC027824','N','N','','XXEIS_911488_HHKGGJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Agreement Detail Validation - Step 2',682,'PROGRAM_END_DATE','Program End Date','','','','default','','14','N','','','','','','','','MC027824','N','N','','XXEIS_911488_HHKGGJ_V','','','GROUP_BY','US','');
--Inserting Report Parameters - Agreement Detail Validation - Step 2
xxeis.eis_rsc_ins.rp( 'Agreement Detail Validation - Step 2',682,'Agreement Status','','AGREEMENT_STATUS','IN','LOV AGREEMENT_STATUS','ACTIVE','VARCHAR2','N','Y','2','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_911488_HHKGGJ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Agreement Detail Validation - Step 2',682,'Agreement Year','','AGREEMENT_YEAR','IN','LOV AGREEMENT_YEAR','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_911488_HHKGGJ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Agreement Detail Validation - Step 2',682,'Mv Name','','MV_NAME','IN','LOV VENDOR_NAME','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_911488_HHKGGJ_V','','','US','');
--Inserting Dependent Parameters - Agreement Detail Validation - Step 2
--Inserting Report Conditions - Agreement Detail Validation - Step 2
xxeis.eis_rsc_ins.rcnh( 'Agreement Detail Validation - Step 2',682,'AGREEMENT_STATUS IN :Agreement Status ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','AGREEMENT_STATUS','','Agreement Status','','','','','XXEIS_911488_HHKGGJ_V','','','','','','IN','Y','Y','','','','','1',682,'Agreement Detail Validation - Step 2','AGREEMENT_STATUS IN :Agreement Status ');
xxeis.eis_rsc_ins.rcnh( 'Agreement Detail Validation - Step 2',682,'AGREEMENT_YEAR IN :Agreement Year ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','AGREEMENT_YEAR','','Agreement Year','','','','','XXEIS_911488_HHKGGJ_V','','','','','','IN','Y','Y','','','','','1',682,'Agreement Detail Validation - Step 2','AGREEMENT_YEAR IN :Agreement Year ');
xxeis.eis_rsc_ins.rcnh( 'Agreement Detail Validation - Step 2',682,'MV_NAME IN :Mv Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','MV_NAME','','Mv Name','','','','','XXEIS_911488_HHKGGJ_V','','','','','','IN','Y','Y','','','','','1',682,'Agreement Detail Validation - Step 2','MV_NAME IN :Mv Name ');
--Inserting Report Sorts - Agreement Detail Validation - Step 2
--Inserting Report Triggers - Agreement Detail Validation - Step 2
--inserting report templates - Agreement Detail Validation - Step 2
--Inserting Report Portals - Agreement Detail Validation - Step 2
--inserting report dashboards - Agreement Detail Validation - Step 2
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Agreement Detail Validation - Step 2','682','XXEIS_911488_HHKGGJ_V','XXEIS_911488_HHKGGJ_V','N','');
--inserting report security - Agreement Detail Validation - Step 2
xxeis.eis_rsc_ins.rsec( 'Agreement Detail Validation - Step 2','682','','XXCUS_TM_USER',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'Agreement Detail Validation - Step 2','682','','XXCUS_TM_ADMIN_USER',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'Agreement Detail Validation - Step 2','682','','XXCUS_TM_FORMS_RESP',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'Agreement Detail Validation - Step 2','682','','XXCUS_TM_ADM_FORMS_RESP',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'Agreement Detail Validation - Step 2','','AB063501','',682,'MC027824','','N','');
--Inserting Report Pivots - Agreement Detail Validation - Step 2
--Inserting Report   Version details- Agreement Detail Validation - Step 2
xxeis.eis_rsc_ins.rv( 'Agreement Detail Validation - Step 2','','Agreement Detail Validation - Step 2','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
