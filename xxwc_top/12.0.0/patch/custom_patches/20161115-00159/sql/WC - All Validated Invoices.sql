--Report Name            : WC - All Validated Invoices
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_91454_FEVHMB_V
CREATE OR REPLACE VIEW APPS.XXEIS_91454_FEVHMB_V (BATCH_NAME, INVOICE_ID, LAST_UPDATE_DATE, LAST_UPDATED_BY, VENDOR_ID, INVOICE_NUM, SET_OF_BOOKS_ID, INVOICE_CURRENCY_CODE, PAYMENT_CURRENCY_CODE, PAYMENT_CROSS_RATE, INVOICE_AMOUNT, VENDOR_SITE_ID, AMOUNT_PAID, DISCOUNT_AMOUNT_TAKEN, INVOICE_DATE, SOURCE, INVOICE_TYPE_LOOKUP_CODE, DESCRIPTION, BATCH_ID, AMOUNT_APPLICABLE_TO_DISCOUNT, TAX_AMOUNT, TERMS_ID, TERMS_DATE, PAYMENT_METHOD_LOOKUP_CODE, PAY_GROUP_LOOKUP_CODE, ACCTS_PAY_CODE_COMBINATION_ID, PAYMENT_STATUS_FLAG, CREATION_DATE, CREATED_BY, BASE_AMOUNT, VAT_CODE, LAST_UPDATE_LOGIN, EXCLUSIVE_PAYMENT_FLAG, PO_HEADER_ID, FREIGHT_AMOUNT, GOODS_RECEIVED_DATE, INVOICE_RECEIVED_DATE, VOUCHER_NUM, APPROVED_AMOUNT, RECURRING_PAYMENT_ID, EXCHANGE_RATE, EXCHANGE_RATE_TYPE, EXCHANGE_DATE, EARLIEST_SETTLEMENT_DATE, ORIGINAL_PREPAYMENT_AMOUNT, DOC_SEQUENCE_ID, DOC_SEQUENCE_VALUE, DOC_CATEGORY_CODE, ATTRIBUTE1, ATTRIBUTE2, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7,
  ATTRIBUTE8, ATTRIBUTE9, ATTRIBUTE10, ATTRIBUTE11, ATTRIBUTE12, ATTRIBUTE13, ATTRIBUTE14, ATTRIBUTE15, ATTRIBUTE_CATEGORY, APPROVAL_STATUS, APPROVAL_DESCRIPTION, INVOICE_DISTRIBUTION_TOTAL, POSTING_STATUS, PREPAY_FLAG, AUTHORIZED_BY, CANCELLED_DATE, CANCELLED_BY, CANCELLED_AMOUNT, TEMP_CANCELLED_AMOUNT, PROJECT_ACCOUNTING_CONTEXT, USSGL_TRANSACTION_CODE, USSGL_TRX_CODE_CONTEXT, PROJECT_ID, TASK_ID, EXPENDITURE_TYPE, EXPENDITURE_ITEM_DATE, PA_QUANTITY, EXPENDITURE_ORGANIZATION_ID, PA_DEFAULT_DIST_CCID, VENDOR_PREPAY_AMOUNT, PAYMENT_AMOUNT_TOTAL, AWT_FLAG, AWT_GROUP_ID, REFERENCE_1, REFERENCE_2, ORG_ID, PRE_WITHHOLDING_AMOUNT, GLOBAL_ATTRIBUTE_CATEGORY, GLOBAL_ATTRIBUTE1, GLOBAL_ATTRIBUTE2, GLOBAL_ATTRIBUTE3, GLOBAL_ATTRIBUTE4, GLOBAL_ATTRIBUTE5, GLOBAL_ATTRIBUTE6, GLOBAL_ATTRIBUTE7, GLOBAL_ATTRIBUTE8, GLOBAL_ATTRIBUTE9, GLOBAL_ATTRIBUTE10, GLOBAL_ATTRIBUTE11, GLOBAL_ATTRIBUTE12, GLOBAL_ATTRIBUTE13, GLOBAL_ATTRIBUTE14, GLOBAL_ATTRIBUTE15, GLOBAL_ATTRIBUTE16, GLOBAL_ATTRIBUTE17,
  GLOBAL_ATTRIBUTE18, GLOBAL_ATTRIBUTE19, GLOBAL_ATTRIBUTE20, AUTO_TAX_CALC_FLAG, PAYMENT_CROSS_RATE_TYPE, PAYMENT_CROSS_RATE_DATE, PAY_CURR_INVOICE_AMOUNT, MRC_BASE_AMOUNT, MRC_EXCHANGE_RATE, MRC_EXCHANGE_RATE_TYPE, MRC_EXCHANGE_DATE, GL_DATE, AWARD_ID, PAID_ON_BEHALF_EMPLOYEE_ID, AMT_DUE_CCARD_COMPANY, AMT_DUE_EMPLOYEE, APPROVAL_READY_FLAG, APPROVAL_ITERATION, WFAPPROVAL_STATUS, REQUESTER_ID, VALIDATION_REQUEST_ID, VALIDATED_TAX_AMOUNT, QUICK_CREDIT, CREDITED_INVOICE_ID, DISTRIBUTION_SET_ID, APPLICATION_ID, PRODUCT_TABLE, REFERENCE_KEY1, REFERENCE_KEY2, REFERENCE_KEY3, REFERENCE_KEY4, REFERENCE_KEY5, TOTAL_TAX_AMOUNT, SELF_ASSESSED_TAX_AMOUNT, TAX_RELATED_INVOICE_ID, TRX_BUSINESS_CATEGORY, USER_DEFINED_FISC_CLASS, TAXATION_COUNTRY, DOCUMENT_SUB_TYPE, SUPPLIER_TAX_INVOICE_NUMBER, SUPPLIER_TAX_INVOICE_DATE, SUPPLIER_TAX_EXCHANGE_RATE, TAX_INVOICE_RECORDING_DATE, TAX_INVOICE_INTERNAL_SEQ, LEGAL_ENTITY_ID, HISTORICAL_FLAG, FORCE_REVALIDATION_FLAG, BANK_CHARGE_BEARER,
  REMITTANCE_MESSAGE1, REMITTANCE_MESSAGE2, REMITTANCE_MESSAGE3, UNIQUE_REMITTANCE_IDENTIFIER, URI_CHECK_DIGIT, SETTLEMENT_PRIORITY, PAYMENT_REASON_CODE, PAYMENT_REASON_COMMENTS, PAYMENT_METHOD_CODE, DELIVERY_CHANNEL_CODE, QUICK_PO_HEADER_ID, NET_OF_RETAINAGE_FLAG, RELEASE_AMOUNT_NET_OF_TAX, CONTROL_AMOUNT, PARTY_ID, PARTY_SITE_ID, PAY_PROC_TRXN_TYPE_CODE, PAYMENT_FUNCTION, CUST_REGISTRATION_CODE, CUST_REGISTRATION_NUMBER, PORT_OF_ENTRY_CODE, EXTERNAL_BANK_ACCOUNT_ID, VENDOR_CONTACT_ID, INTERNAL_CONTACT_EMAIL, DISC_IS_INV_LESS_TAX_FLAG, EXCLUDE_FREIGHT_FROM_DISCOUNT, PAY_AWT_GROUP_ID, ORIGINAL_INVOICE_AMOUNT, DISPUTE_REASON, REMIT_TO_SUPPLIER_NAME, REMIT_TO_SUPPLIER_ID, REMIT_TO_SUPPLIER_SITE, REMIT_TO_SUPPLIER_SITE_ID, RELATIONSHIP_ID, SEGMENT1, VENDOR_NAME, INVOICE_STATUS)
AS
  SELECT APBA.BATCH_NAME,
    API.INVOICE_ID,
    API.LAST_UPDATE_DATE,
    API.LAST_UPDATED_BY,
    API.VENDOR_ID,
    API.INVOICE_NUM,
    API.SET_OF_BOOKS_ID,
    API.INVOICE_CURRENCY_CODE,
    API.PAYMENT_CURRENCY_CODE,
    API.PAYMENT_CROSS_RATE,
    API.INVOICE_AMOUNT,
    API.VENDOR_SITE_ID,
    API.AMOUNT_PAID,
    API.DISCOUNT_AMOUNT_TAKEN,
    API.INVOICE_DATE,
    API.SOURCE,
    API.INVOICE_TYPE_LOOKUP_CODE,
    API.DESCRIPTION,
    API.BATCH_ID,
    API.AMOUNT_APPLICABLE_TO_DISCOUNT,
    API.TAX_AMOUNT,
    API.TERMS_ID,
    API.TERMS_DATE,
    API.PAYMENT_METHOD_LOOKUP_CODE,
    API.PAY_GROUP_LOOKUP_CODE,
    API.ACCTS_PAY_CODE_COMBINATION_ID,
    API.PAYMENT_STATUS_FLAG,
    API.CREATION_DATE,
    API.CREATED_BY,
    API.BASE_AMOUNT,
    API.VAT_CODE,
    API.LAST_UPDATE_LOGIN,
    API.EXCLUSIVE_PAYMENT_FLAG,
    API.PO_HEADER_ID,
    API.FREIGHT_AMOUNT,
    API.GOODS_RECEIVED_DATE,
    API.INVOICE_RECEIVED_DATE,
    API.VOUCHER_NUM,
    API.APPROVED_AMOUNT,
    API.RECURRING_PAYMENT_ID,
    API.EXCHANGE_RATE,
    API.EXCHANGE_RATE_TYPE,
    API.EXCHANGE_DATE,
    API.EARLIEST_SETTLEMENT_DATE,
    API.ORIGINAL_PREPAYMENT_AMOUNT,
    API.DOC_SEQUENCE_ID,
    API.DOC_SEQUENCE_VALUE,
    API.DOC_CATEGORY_CODE,
    API.ATTRIBUTE1,
    API.ATTRIBUTE2,
    API.ATTRIBUTE3,
    API.ATTRIBUTE4,
    API.ATTRIBUTE5,
    API.ATTRIBUTE6,
    API.ATTRIBUTE7,
    API.ATTRIBUTE8,
    API.ATTRIBUTE9,
    API.ATTRIBUTE10,
    API.ATTRIBUTE11,
    API.ATTRIBUTE12,
    API.ATTRIBUTE13,
    API.ATTRIBUTE14,
    API.ATTRIBUTE15,
    API.ATTRIBUTE_CATEGORY,
    API.APPROVAL_STATUS,
    API.APPROVAL_DESCRIPTION,
    API.INVOICE_DISTRIBUTION_TOTAL,
    API.POSTING_STATUS,
    API.PREPAY_FLAG,
    API.AUTHORIZED_BY,
    API.CANCELLED_DATE,
    API.CANCELLED_BY,
    API.CANCELLED_AMOUNT,
    API.TEMP_CANCELLED_AMOUNT,
    API.PROJECT_ACCOUNTING_CONTEXT,
    API.USSGL_TRANSACTION_CODE,
    API.USSGL_TRX_CODE_CONTEXT,
    API.PROJECT_ID,
    API.TASK_ID,
    API.EXPENDITURE_TYPE,
    API.EXPENDITURE_ITEM_DATE,
    API.PA_QUANTITY,
    API.EXPENDITURE_ORGANIZATION_ID,
    API.PA_DEFAULT_DIST_CCID,
    API.VENDOR_PREPAY_AMOUNT,
    API.PAYMENT_AMOUNT_TOTAL,
    API.AWT_FLAG,
    API.AWT_GROUP_ID,
    API.REFERENCE_1,
    API.REFERENCE_2,
    API.ORG_ID,
    API.PRE_WITHHOLDING_AMOUNT,
    API.GLOBAL_ATTRIBUTE_CATEGORY,
    API.GLOBAL_ATTRIBUTE1,
    API.GLOBAL_ATTRIBUTE2,
    API.GLOBAL_ATTRIBUTE3,
    API.GLOBAL_ATTRIBUTE4,
    API.GLOBAL_ATTRIBUTE5,
    API.GLOBAL_ATTRIBUTE6,
    API.GLOBAL_ATTRIBUTE7,
    API.GLOBAL_ATTRIBUTE8,
    API.GLOBAL_ATTRIBUTE9,
    API.GLOBAL_ATTRIBUTE10,
    API.GLOBAL_ATTRIBUTE11,
    API.GLOBAL_ATTRIBUTE12,
    API.GLOBAL_ATTRIBUTE13,
    API.GLOBAL_ATTRIBUTE14,
    API.GLOBAL_ATTRIBUTE15,
    API.GLOBAL_ATTRIBUTE16,
    API.GLOBAL_ATTRIBUTE17,
    API.GLOBAL_ATTRIBUTE18,
    API.GLOBAL_ATTRIBUTE19,
    API.GLOBAL_ATTRIBUTE20,
    API.AUTO_TAX_CALC_FLAG,
    API.PAYMENT_CROSS_RATE_TYPE,
    API.PAYMENT_CROSS_RATE_DATE,
    API.PAY_CURR_INVOICE_AMOUNT,
    API.MRC_BASE_AMOUNT,
    API.MRC_EXCHANGE_RATE,
    API.MRC_EXCHANGE_RATE_TYPE,
    API.MRC_EXCHANGE_DATE,
    API.GL_DATE,
    API.AWARD_ID,
    API.PAID_ON_BEHALF_EMPLOYEE_ID,
    API.AMT_DUE_CCARD_COMPANY,
    API.AMT_DUE_EMPLOYEE,
    API.APPROVAL_READY_FLAG,
    API.APPROVAL_ITERATION,
    API.WFAPPROVAL_STATUS,
    API.REQUESTER_ID,
    API.VALIDATION_REQUEST_ID,
    API.VALIDATED_TAX_AMOUNT,
    API.QUICK_CREDIT,
    API.CREDITED_INVOICE_ID,
    API.DISTRIBUTION_SET_ID,
    API.APPLICATION_ID,
    API.PRODUCT_TABLE,
    API.REFERENCE_KEY1,
    API.REFERENCE_KEY2,
    API.REFERENCE_KEY3,
    API.REFERENCE_KEY4,
    API.REFERENCE_KEY5,
    API.TOTAL_TAX_AMOUNT,
    API.SELF_ASSESSED_TAX_AMOUNT,
    API.TAX_RELATED_INVOICE_ID,
    API.TRX_BUSINESS_CATEGORY,
    API.USER_DEFINED_FISC_CLASS,
    API.TAXATION_COUNTRY,
    API.DOCUMENT_SUB_TYPE,
    API.SUPPLIER_TAX_INVOICE_NUMBER,
    API.SUPPLIER_TAX_INVOICE_DATE,
    API.SUPPLIER_TAX_EXCHANGE_RATE,
    API.TAX_INVOICE_RECORDING_DATE,
    API.TAX_INVOICE_INTERNAL_SEQ,
    API.LEGAL_ENTITY_ID,
    API.HISTORICAL_FLAG,
    API.FORCE_REVALIDATION_FLAG,
    API.BANK_CHARGE_BEARER,
    API.REMITTANCE_MESSAGE1,
    API.REMITTANCE_MESSAGE2,
    API.REMITTANCE_MESSAGE3,
    API.UNIQUE_REMITTANCE_IDENTIFIER,
    API.URI_CHECK_DIGIT,
    API.SETTLEMENT_PRIORITY,
    API.PAYMENT_REASON_CODE,
    API.PAYMENT_REASON_COMMENTS,
    API.PAYMENT_METHOD_CODE,
    API.DELIVERY_CHANNEL_CODE,
    API.QUICK_PO_HEADER_ID,
    API.NET_OF_RETAINAGE_FLAG,
    API.RELEASE_AMOUNT_NET_OF_TAX,
    API.CONTROL_AMOUNT,
    API.PARTY_ID,
    API.PARTY_SITE_ID,
    API.PAY_PROC_TRXN_TYPE_CODE,
    API.PAYMENT_FUNCTION,
    API.CUST_REGISTRATION_CODE,
    API.CUST_REGISTRATION_NUMBER,
    API.PORT_OF_ENTRY_CODE,
    API.EXTERNAL_BANK_ACCOUNT_ID,
    API.VENDOR_CONTACT_ID,
    API.INTERNAL_CONTACT_EMAIL,
    API.DISC_IS_INV_LESS_TAX_FLAG,
    API.EXCLUDE_FREIGHT_FROM_DISCOUNT,
    API.PAY_AWT_GROUP_ID,
    API.ORIGINAL_INVOICE_AMOUNT,
    API.DISPUTE_REASON,
    API.REMIT_TO_SUPPLIER_NAME,
    API.REMIT_TO_SUPPLIER_ID,
    API.REMIT_TO_SUPPLIER_SITE,
    API.REMIT_TO_SUPPLIER_SITE_ID,
    API.RELATIONSHIP_ID,
    ASS.SEGMENT1,
    ASS.VENDOR_NAME ,
    DECODE (APPS.AP_INVOICES_PKG.GET_APPROVAL_STATUS (API.INVOICE_ID, API.INVOICE_AMOUNT, API.PAYMENT_STATUS_FLAG, API.INVOICE_TYPE_LOOKUP_CODE ), 'APPROVED', 'Validated', 'NEVER APPROVED', 'Never Validated', 'NEEDS REAPPROVAL', 'Needs Revalidation' ) INVOICE_STATUS
  FROM AP.AP_INVOICES_ALL API,
    AP.AP_BATCHES_ALL APBA,
    AP.AP_SUPPLIERS ASS
  WHERE API.ORG_ID                                                                                                                                                                                                                                           = 162
  AND API.BATCH_ID                                                                                                                                                                                                                                           = APBA.BATCH_ID (+)
  AND API.VENDOR_ID                                                                                                                                                                                                                                          = ASS.VENDOR_ID
  AND DECODE (APPS.AP_INVOICES_PKG.GET_APPROVAL_STATUS (API.INVOICE_ID, API.INVOICE_AMOUNT, API.PAYMENT_STATUS_FLAG, API.INVOICE_TYPE_LOOKUP_CODE ), 'APPROVED', 'Validated', 'NEVER APPROVED', 'Never Validated', 'NEEDS REAPPROVAL', 'Needs Revalidation' )='Validated'
/
prompt Creating Object Data XXEIS_91454_FEVHMB_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_91454_FEVHMB_V
xxeis.eis_rsc_ins.v( 'XXEIS_91454_FEVHMB_V',200,'Paste SQL View for WC - All Validated Invoices','1.0','','','10012196','APPS','WC - All Validated Invoices View','X9FV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_91454_FEVHMB_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_91454_FEVHMB_V',200,FALSE);
--Inserting Object Columns for XXEIS_91454_FEVHMB_V
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE16',200,'','','','','','10012196','VARCHAR2','','','Global Attribute16','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE17',200,'','','','','','10012196','VARCHAR2','','','Global Attribute17','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE18',200,'','','','','','10012196','VARCHAR2','','','Global Attribute18','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE19',200,'','','','','','10012196','VARCHAR2','','','Global Attribute19','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE20',200,'','','','','','10012196','VARCHAR2','','','Global Attribute20','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','AUTO_TAX_CALC_FLAG',200,'','','','','','10012196','VARCHAR2','','','Auto Tax Calc Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PAYMENT_CROSS_RATE_TYPE',200,'','','','','','10012196','VARCHAR2','','','Payment Cross Rate Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PAYMENT_CROSS_RATE_DATE',200,'','','','','','10012196','DATE','','','Payment Cross Rate Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PAY_CURR_INVOICE_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Pay Curr Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','MRC_BASE_AMOUNT',200,'','','','','','10012196','VARCHAR2','','','Mrc Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','MRC_EXCHANGE_RATE',200,'','','','','','10012196','VARCHAR2','','','Mrc Exchange Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','MRC_EXCHANGE_RATE_TYPE',200,'','','','','','10012196','VARCHAR2','','','Mrc Exchange Rate Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','MRC_EXCHANGE_DATE',200,'','','','','','10012196','VARCHAR2','','','Mrc Exchange Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GL_DATE',200,'','','','','','10012196','DATE','','','Gl Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','AWARD_ID',200,'','','','','','10012196','NUMBER','','','Award Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PAID_ON_BEHALF_EMPLOYEE_ID',200,'','','','','','10012196','NUMBER','','','Paid On Behalf Employee Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','AMT_DUE_CCARD_COMPANY',200,'','','','~T~D~2','','10012196','NUMBER','','','Amt Due Ccard Company','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','AMT_DUE_EMPLOYEE',200,'','','','~T~D~2','','10012196','NUMBER','','','Amt Due Employee','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','APPROVAL_READY_FLAG',200,'','','','','','10012196','VARCHAR2','','','Approval Ready Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','APPROVAL_ITERATION',200,'','','','','','10012196','NUMBER','','','Approval Iteration','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','WFAPPROVAL_STATUS',200,'','','','','','10012196','VARCHAR2','','','Wfapproval Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','REQUESTER_ID',200,'','','','','','10012196','NUMBER','','','Requester Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','VALIDATION_REQUEST_ID',200,'','','','','','10012196','NUMBER','','','Validation Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','VALIDATED_TAX_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Validated Tax Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','QUICK_CREDIT',200,'','','','','','10012196','VARCHAR2','','','Quick Credit','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','CREDITED_INVOICE_ID',200,'','','','','','10012196','NUMBER','','','Credited Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','DISTRIBUTION_SET_ID',200,'','','','','','10012196','NUMBER','','','Distribution Set Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','APPLICATION_ID',200,'','','','','','10012196','NUMBER','','','Application Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PRODUCT_TABLE',200,'','','','','','10012196','VARCHAR2','','','Product Table','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','REFERENCE_KEY1',200,'','','','','','10012196','VARCHAR2','','','Reference Key1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','REFERENCE_KEY2',200,'','','','','','10012196','VARCHAR2','','','Reference Key2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','REFERENCE_KEY3',200,'','','','','','10012196','VARCHAR2','','','Reference Key3','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','REFERENCE_KEY4',200,'','','','','','10012196','VARCHAR2','','','Reference Key4','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','REFERENCE_KEY5',200,'','','','','','10012196','VARCHAR2','','','Reference Key5','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','TOTAL_TAX_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Total Tax Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','SELF_ASSESSED_TAX_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Self Assessed Tax Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','TAX_RELATED_INVOICE_ID',200,'','','','','','10012196','NUMBER','','','Tax Related Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','TRX_BUSINESS_CATEGORY',200,'','','','','','10012196','VARCHAR2','','','Trx Business Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','USER_DEFINED_FISC_CLASS',200,'','','','','','10012196','VARCHAR2','','','User Defined Fisc Class','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','TAXATION_COUNTRY',200,'','','','','','10012196','VARCHAR2','','','Taxation Country','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','DOCUMENT_SUB_TYPE',200,'','','','','','10012196','VARCHAR2','','','Document Sub Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','SUPPLIER_TAX_INVOICE_NUMBER',200,'','','','','','10012196','VARCHAR2','','','Supplier Tax Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','SUPPLIER_TAX_INVOICE_DATE',200,'','','','','','10012196','DATE','','','Supplier Tax Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','SUPPLIER_TAX_EXCHANGE_RATE',200,'','','','~T~D~2','','10012196','NUMBER','','','Supplier Tax Exchange Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','TAX_INVOICE_RECORDING_DATE',200,'','','','','','10012196','DATE','','','Tax Invoice Recording Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','TAX_INVOICE_INTERNAL_SEQ',200,'','','','','','10012196','VARCHAR2','','','Tax Invoice Internal Seq','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','LEGAL_ENTITY_ID',200,'','','','','','10012196','NUMBER','','','Legal Entity Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','HISTORICAL_FLAG',200,'','','','','','10012196','VARCHAR2','','','Historical Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','FORCE_REVALIDATION_FLAG',200,'','','','','','10012196','VARCHAR2','','','Force Revalidation Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','BANK_CHARGE_BEARER',200,'','','','','','10012196','VARCHAR2','','','Bank Charge Bearer','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','REMITTANCE_MESSAGE1',200,'','','','','','10012196','VARCHAR2','','','Remittance Message1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','REMITTANCE_MESSAGE2',200,'','','','','','10012196','VARCHAR2','','','Remittance Message2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','REMITTANCE_MESSAGE3',200,'','','','','','10012196','VARCHAR2','','','Remittance Message3','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','UNIQUE_REMITTANCE_IDENTIFIER',200,'','','','','','10012196','VARCHAR2','','','Unique Remittance Identifier','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','URI_CHECK_DIGIT',200,'','','','','','10012196','VARCHAR2','','','Uri Check Digit','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','SETTLEMENT_PRIORITY',200,'','','','','','10012196','VARCHAR2','','','Settlement Priority','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PAYMENT_REASON_CODE',200,'','','','','','10012196','VARCHAR2','','','Payment Reason Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PAYMENT_REASON_COMMENTS',200,'','','','','','10012196','VARCHAR2','','','Payment Reason Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PAYMENT_METHOD_CODE',200,'','','','','','10012196','VARCHAR2','','','Payment Method Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','DELIVERY_CHANNEL_CODE',200,'','','','','','10012196','VARCHAR2','','','Delivery Channel Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','QUICK_PO_HEADER_ID',200,'','','','','','10012196','NUMBER','','','Quick Po Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','NET_OF_RETAINAGE_FLAG',200,'','','','','','10012196','VARCHAR2','','','Net Of Retainage Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','RELEASE_AMOUNT_NET_OF_TAX',200,'','','','~T~D~2','','10012196','NUMBER','','','Release Amount Net Of Tax','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','CONTROL_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Control Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PARTY_ID',200,'','','','','','10012196','NUMBER','','','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PARTY_SITE_ID',200,'','','','','','10012196','NUMBER','','','Party Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PAY_PROC_TRXN_TYPE_CODE',200,'','','','','','10012196','VARCHAR2','','','Pay Proc Trxn Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PAYMENT_FUNCTION',200,'','','','','','10012196','VARCHAR2','','','Payment Function','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','CUST_REGISTRATION_CODE',200,'','','','','','10012196','VARCHAR2','','','Cust Registration Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','CUST_REGISTRATION_NUMBER',200,'','','','','','10012196','VARCHAR2','','','Cust Registration Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PORT_OF_ENTRY_CODE',200,'','','','','','10012196','VARCHAR2','','','Port Of Entry Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','EXTERNAL_BANK_ACCOUNT_ID',200,'','','','','','10012196','NUMBER','','','External Bank Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','VENDOR_CONTACT_ID',200,'','','','','','10012196','NUMBER','','','Vendor Contact Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','INTERNAL_CONTACT_EMAIL',200,'','','','','','10012196','VARCHAR2','','','Internal Contact Email','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','DISC_IS_INV_LESS_TAX_FLAG',200,'','','','','','10012196','VARCHAR2','','','Disc Is Inv Less Tax Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','EXCLUDE_FREIGHT_FROM_DISCOUNT',200,'','','','','','10012196','VARCHAR2','','','Exclude Freight From Discount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PAY_AWT_GROUP_ID',200,'','','','','','10012196','NUMBER','','','Pay Awt Group Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ORIGINAL_INVOICE_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Original Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','DISPUTE_REASON',200,'','','','','','10012196','VARCHAR2','','','Dispute Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','REMIT_TO_SUPPLIER_NAME',200,'','','','','','10012196','VARCHAR2','','','Remit To Supplier Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','REMIT_TO_SUPPLIER_ID',200,'','','','','','10012196','NUMBER','','','Remit To Supplier Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','REMIT_TO_SUPPLIER_SITE',200,'','','','','','10012196','VARCHAR2','','','Remit To Supplier Site','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','REMIT_TO_SUPPLIER_SITE_ID',200,'','','','','','10012196','NUMBER','','','Remit To Supplier Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','RELATIONSHIP_ID',200,'','','','','','10012196','NUMBER','','','Relationship Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','SEGMENT1',200,'','','','','','10012196','VARCHAR2','','','Segment1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','VENDOR_NAME',200,'','','','','','10012196','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','INVOICE_STATUS',200,'','','','','','10012196','VARCHAR2','','','Invoice Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','BATCH_NAME',200,'','','','','','10012196','VARCHAR2','','','Batch Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','INVOICE_ID',200,'','','','','','10012196','NUMBER','','','Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','LAST_UPDATE_DATE',200,'','','','','','10012196','DATE','','','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','LAST_UPDATED_BY',200,'','','','','','10012196','NUMBER','','','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','VENDOR_ID',200,'','','','','','10012196','NUMBER','','','Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','INVOICE_NUM',200,'','','','','','10012196','VARCHAR2','','','Invoice Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','SET_OF_BOOKS_ID',200,'','','','','','10012196','NUMBER','','','Set Of Books Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','INVOICE_CURRENCY_CODE',200,'','','','','','10012196','VARCHAR2','','','Invoice Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PAYMENT_CURRENCY_CODE',200,'','','','','','10012196','VARCHAR2','','','Payment Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PAYMENT_CROSS_RATE',200,'','','','~T~D~2','','10012196','NUMBER','','','Payment Cross Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','INVOICE_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','VENDOR_SITE_ID',200,'','','','','','10012196','NUMBER','','','Vendor Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','AMOUNT_PAID',200,'','','','~T~D~2','','10012196','NUMBER','','','Amount Paid','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','DISCOUNT_AMOUNT_TAKEN',200,'','','','~T~D~2','','10012196','NUMBER','','','Discount Amount Taken','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','INVOICE_DATE',200,'','','','','','10012196','DATE','','','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','SOURCE',200,'','','','','','10012196','VARCHAR2','','','Source','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','INVOICE_TYPE_LOOKUP_CODE',200,'','','','','','10012196','VARCHAR2','','','Invoice Type Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','DESCRIPTION',200,'','','','','','10012196','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','BATCH_ID',200,'','','','','','10012196','NUMBER','','','Batch Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','AMOUNT_APPLICABLE_TO_DISCOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Amount Applicable To Discount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','TAX_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Tax Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','TERMS_ID',200,'','','','','','10012196','NUMBER','','','Terms Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','TERMS_DATE',200,'','','','','','10012196','DATE','','','Terms Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PAYMENT_METHOD_LOOKUP_CODE',200,'','','','','','10012196','VARCHAR2','','','Payment Method Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PAY_GROUP_LOOKUP_CODE',200,'','','','','','10012196','VARCHAR2','','','Pay Group Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ACCTS_PAY_CODE_COMBINATION_ID',200,'','','','~T~D~2','','10012196','NUMBER','','','Accts Pay Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PAYMENT_STATUS_FLAG',200,'','','','','','10012196','VARCHAR2','','','Payment Status Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','CREATION_DATE',200,'','','','','','10012196','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','CREATED_BY',200,'','','','','','10012196','NUMBER','','','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','BASE_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','VAT_CODE',200,'','','','','','10012196','VARCHAR2','','','Vat Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','LAST_UPDATE_LOGIN',200,'','','','','','10012196','NUMBER','','','Last Update Login','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','EXCLUSIVE_PAYMENT_FLAG',200,'','','','','','10012196','VARCHAR2','','','Exclusive Payment Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PO_HEADER_ID',200,'','','','','','10012196','NUMBER','','','Po Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','FREIGHT_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Freight Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GOODS_RECEIVED_DATE',200,'','','','','','10012196','DATE','','','Goods Received Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','INVOICE_RECEIVED_DATE',200,'','','','','','10012196','DATE','','','Invoice Received Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','VOUCHER_NUM',200,'','','','','','10012196','VARCHAR2','','','Voucher Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','APPROVED_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Approved Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','RECURRING_PAYMENT_ID',200,'','','','','','10012196','NUMBER','','','Recurring Payment Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','EXCHANGE_RATE',200,'','','','~T~D~2','','10012196','NUMBER','','','Exchange Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','EXCHANGE_RATE_TYPE',200,'','','','','','10012196','VARCHAR2','','','Exchange Rate Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','EXCHANGE_DATE',200,'','','','','','10012196','DATE','','','Exchange Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','EARLIEST_SETTLEMENT_DATE',200,'','','','','','10012196','DATE','','','Earliest Settlement Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ORIGINAL_PREPAYMENT_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Original Prepayment Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','DOC_SEQUENCE_ID',200,'','','','','','10012196','NUMBER','','','Doc Sequence Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','DOC_SEQUENCE_VALUE',200,'','','','~T~D~2','','10012196','NUMBER','','','Doc Sequence Value','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','DOC_CATEGORY_CODE',200,'','','','','','10012196','VARCHAR2','','','Doc Category Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ATTRIBUTE1',200,'','','','','','10012196','VARCHAR2','','','Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ATTRIBUTE2',200,'','','','','','10012196','VARCHAR2','','','Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ATTRIBUTE3',200,'','','','','','10012196','VARCHAR2','','','Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ATTRIBUTE4',200,'','','','','','10012196','VARCHAR2','','','Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ATTRIBUTE5',200,'','','','','','10012196','VARCHAR2','','','Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ATTRIBUTE6',200,'','','','','','10012196','VARCHAR2','','','Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ATTRIBUTE7',200,'','','','','','10012196','VARCHAR2','','','Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ATTRIBUTE8',200,'','','','','','10012196','VARCHAR2','','','Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ATTRIBUTE9',200,'','','','','','10012196','VARCHAR2','','','Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ATTRIBUTE10',200,'','','','','','10012196','VARCHAR2','','','Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ATTRIBUTE11',200,'','','','','','10012196','VARCHAR2','','','Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ATTRIBUTE12',200,'','','','','','10012196','VARCHAR2','','','Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ATTRIBUTE13',200,'','','','','','10012196','VARCHAR2','','','Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ATTRIBUTE14',200,'','','','','','10012196','VARCHAR2','','','Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ATTRIBUTE15',200,'','','','','','10012196','VARCHAR2','','','Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ATTRIBUTE_CATEGORY',200,'','','','','','10012196','VARCHAR2','','','Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','APPROVAL_STATUS',200,'','','','','','10012196','VARCHAR2','','','Approval Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','APPROVAL_DESCRIPTION',200,'','','','','','10012196','VARCHAR2','','','Approval Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','INVOICE_DISTRIBUTION_TOTAL',200,'','','','~T~D~2','','10012196','NUMBER','','','Invoice Distribution Total','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','POSTING_STATUS',200,'','','','','','10012196','VARCHAR2','','','Posting Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PREPAY_FLAG',200,'','','','','','10012196','VARCHAR2','','','Prepay Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','AUTHORIZED_BY',200,'','','','','','10012196','VARCHAR2','','','Authorized By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','CANCELLED_DATE',200,'','','','','','10012196','DATE','','','Cancelled Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','CANCELLED_BY',200,'','','','','','10012196','NUMBER','','','Cancelled By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','CANCELLED_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Cancelled Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','TEMP_CANCELLED_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Temp Cancelled Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PROJECT_ACCOUNTING_CONTEXT',200,'','','','','','10012196','VARCHAR2','','','Project Accounting Context','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','USSGL_TRANSACTION_CODE',200,'','','','','','10012196','VARCHAR2','','','Ussgl Transaction Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','USSGL_TRX_CODE_CONTEXT',200,'','','','','','10012196','VARCHAR2','','','Ussgl Trx Code Context','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PROJECT_ID',200,'','','','','','10012196','NUMBER','','','Project Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','TASK_ID',200,'','','','','','10012196','NUMBER','','','Task Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','EXPENDITURE_TYPE',200,'','','','','','10012196','VARCHAR2','','','Expenditure Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','EXPENDITURE_ITEM_DATE',200,'','','','','','10012196','DATE','','','Expenditure Item Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PA_QUANTITY',200,'','','','~T~D~2','','10012196','NUMBER','','','Pa Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','EXPENDITURE_ORGANIZATION_ID',200,'','','','','','10012196','NUMBER','','','Expenditure Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PA_DEFAULT_DIST_CCID',200,'','','','','','10012196','NUMBER','','','Pa Default Dist Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','VENDOR_PREPAY_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Vendor Prepay Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PAYMENT_AMOUNT_TOTAL',200,'','','','~T~D~2','','10012196','NUMBER','','','Payment Amount Total','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','AWT_FLAG',200,'','','','','','10012196','VARCHAR2','','','Awt Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','AWT_GROUP_ID',200,'','','','','','10012196','NUMBER','','','Awt Group Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','REFERENCE_1',200,'','','','','','10012196','VARCHAR2','','','Reference 1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','REFERENCE_2',200,'','','','','','10012196','VARCHAR2','','','Reference 2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','ORG_ID',200,'','','','','','10012196','NUMBER','','','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','PRE_WITHHOLDING_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Pre Withholding Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE_CATEGORY',200,'','','','','','10012196','VARCHAR2','','','Global Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE1',200,'','','','','','10012196','VARCHAR2','','','Global Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE2',200,'','','','','','10012196','VARCHAR2','','','Global Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE3',200,'','','','','','10012196','VARCHAR2','','','Global Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE4',200,'','','','','','10012196','VARCHAR2','','','Global Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE5',200,'','','','','','10012196','VARCHAR2','','','Global Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE6',200,'','','','','','10012196','VARCHAR2','','','Global Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE7',200,'','','','','','10012196','VARCHAR2','','','Global Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE8',200,'','','','','','10012196','VARCHAR2','','','Global Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE9',200,'','','','','','10012196','VARCHAR2','','','Global Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE10',200,'','','','','','10012196','VARCHAR2','','','Global Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE11',200,'','','','','','10012196','VARCHAR2','','','Global Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE12',200,'','','','','','10012196','VARCHAR2','','','Global Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE13',200,'','','','','','10012196','VARCHAR2','','','Global Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE14',200,'','','','','','10012196','VARCHAR2','','','Global Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_91454_FEVHMB_V','GLOBAL_ATTRIBUTE15',200,'','','','','','10012196','VARCHAR2','','','Global Attribute15','','','','US');
--Inserting Object Components for XXEIS_91454_FEVHMB_V
--Inserting Object Component Joins for XXEIS_91454_FEVHMB_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report WC - All Validated Invoices
prompt Creating Report Data for WC - All Validated Invoices
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC - All Validated Invoices
xxeis.eis_rsc_utility.delete_report_rows( 'WC - All Validated Invoices' );
--Inserting Report - WC - All Validated Invoices
xxeis.eis_rsc_ins.r( 200,'WC - All Validated Invoices','','','','','','10012196','XXEIS_91454_FEVHMB_V','Y','','SELECT APBA.BATCH_NAME, API.*, ASS.SEGMENT1, ASS.VENDOR_NAME
,DECODE (APPS.AP_INVOICES_PKG.GET_APPROVAL_STATUS (API.INVOICE_ID, API.INVOICE_AMOUNT, API.PAYMENT_STATUS_FLAG, API.INVOICE_TYPE_LOOKUP_CODE ), ''APPROVED'', ''Validated'', ''NEVER APPROVED'', ''Never Validated'', ''NEEDS REAPPROVAL'', ''Needs Revalidation'' ) INVOICE_STATUS
FROM AP.AP_INVOICES_ALL API, AP.AP_BATCHES_ALL APBA, AP.AP_SUPPLIERS ASS
WHERE API.ORG_ID = 162 AND API.BATCH_ID = APBA.BATCH_ID (+) AND API.VENDOR_ID = ASS.VENDOR_ID AND DECODE (APPS.AP_INVOICES_PKG.GET_APPROVAL_STATUS (API.INVOICE_ID, API.INVOICE_AMOUNT, API.PAYMENT_STATUS_FLAG, API.INVOICE_TYPE_LOOKUP_CODE ), ''APPROVED'', ''Validated'', ''NEVER APPROVED'', ''Never Validated'', ''NEEDS REAPPROVAL'', ''Needs Revalidation'' )=''Validated''
','10012196','','N','Invoices','','CSV,Pivot Excel,EXCEL,','','','','','','','','APPS','US','','','','');
--Inserting Report Columns - WC - All Validated Invoices
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE17','Global Attribute17','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE18','Global Attribute18','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE19','Global Attribute19','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE20','Global Attribute20','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'AUTO_TAX_CALC_FLAG','Auto Tax Calc Flag','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PAYMENT_CROSS_RATE_TYPE','Payment Cross Rate Type','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PAYMENT_CROSS_RATE_DATE','Payment Cross Rate Date','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PAY_CURR_INVOICE_AMOUNT','Pay Curr Invoice Amount','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'MRC_BASE_AMOUNT','Mrc Base Amount','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'MRC_EXCHANGE_RATE','Mrc Exchange Rate','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'MRC_EXCHANGE_RATE_TYPE','Mrc Exchange Rate Type','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'MRC_EXCHANGE_DATE','Mrc Exchange Date','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GL_DATE','Gl Date','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'AWARD_ID','Award Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PAID_ON_BEHALF_EMPLOYEE_ID','Paid On Behalf Employee Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'AMT_DUE_CCARD_COMPANY','Amt Due Ccard Company','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'AMT_DUE_EMPLOYEE','Amt Due Employee','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'APPROVAL_READY_FLAG','Approval Ready Flag','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'APPROVAL_ITERATION','Approval Iteration','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'WFAPPROVAL_STATUS','Wfapproval Status','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'REQUESTER_ID','Requester Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'VALIDATION_REQUEST_ID','Validation Request Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'VALIDATED_TAX_AMOUNT','Validated Tax Amount','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'QUICK_CREDIT','Quick Credit','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'CREDITED_INVOICE_ID','Credited Invoice Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE16','Global Attribute16','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'DISTRIBUTION_SET_ID','Distribution Set Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'APPLICATION_ID','Application Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PRODUCT_TABLE','Product Table','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'REFERENCE_KEY1','Reference Key1','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'REFERENCE_KEY2','Reference Key2','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'REFERENCE_KEY3','Reference Key3','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'REFERENCE_KEY4','Reference Key4','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'REFERENCE_KEY5','Reference Key5','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'TOTAL_TAX_AMOUNT','Total Tax Amount','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'SELF_ASSESSED_TAX_AMOUNT','Self Assessed Tax Amount','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'TAX_RELATED_INVOICE_ID','Tax Related Invoice Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'TRX_BUSINESS_CATEGORY','Trx Business Category','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'USER_DEFINED_FISC_CLASS','User Defined Fisc Class','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'TAXATION_COUNTRY','Taxation Country','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'DOCUMENT_SUB_TYPE','Document Sub Type','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'SUPPLIER_TAX_INVOICE_NUMBER','Supplier Tax Invoice Number','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'SUPPLIER_TAX_INVOICE_DATE','Supplier Tax Invoice Date','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'SUPPLIER_TAX_EXCHANGE_RATE','Supplier Tax Exchange Rate','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'TAX_INVOICE_RECORDING_DATE','Tax Invoice Recording Date','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'TAX_INVOICE_INTERNAL_SEQ','Tax Invoice Internal Seq','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'LEGAL_ENTITY_ID','Legal Entity Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'HISTORICAL_FLAG','Historical Flag','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'FORCE_REVALIDATION_FLAG','Force Revalidation Flag','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'BANK_CHARGE_BEARER','Bank Charge Bearer','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'REMITTANCE_MESSAGE1','Remittance Message1','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'REMITTANCE_MESSAGE2','Remittance Message2','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'REMITTANCE_MESSAGE3','Remittance Message3','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'UNIQUE_REMITTANCE_IDENTIFIER','Unique Remittance Identifier','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'URI_CHECK_DIGIT','Uri Check Digit','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PAYMENT_REASON_CODE','Payment Reason Code','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PAYMENT_REASON_COMMENTS','Payment Reason Comments','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PAYMENT_METHOD_CODE','Payment Method Code','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'DELIVERY_CHANNEL_CODE','Delivery Channel Code','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'QUICK_PO_HEADER_ID','Quick Po Header Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'NET_OF_RETAINAGE_FLAG','Net Of Retainage Flag','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'RELEASE_AMOUNT_NET_OF_TAX','Release Amount Net Of Tax','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'CONTROL_AMOUNT','Control Amount','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PARTY_ID','Party Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PARTY_SITE_ID','Party Site Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PAY_PROC_TRXN_TYPE_CODE','Pay Proc Trxn Type Code','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PAYMENT_FUNCTION','Payment Function','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'CUST_REGISTRATION_CODE','Cust Registration Code','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'CUST_REGISTRATION_NUMBER','Cust Registration Number','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PORT_OF_ENTRY_CODE','Port Of Entry Code','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'EXTERNAL_BANK_ACCOUNT_ID','External Bank Account Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'VENDOR_CONTACT_ID','Vendor Contact Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'INTERNAL_CONTACT_EMAIL','Internal Contact Email','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'DISC_IS_INV_LESS_TAX_FLAG','Disc Is Inv Less Tax Flag','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'EXCLUDE_FREIGHT_FROM_DISCOUNT','Exclude Freight From Discount','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PAY_AWT_GROUP_ID','Pay Awt Group Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ORIGINAL_INVOICE_AMOUNT','Original Invoice Amount','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'DISPUTE_REASON','Dispute Reason','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'REMIT_TO_SUPPLIER_NAME','Remit To Supplier Name','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'REMIT_TO_SUPPLIER_ID','Remit To Supplier Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'REMIT_TO_SUPPLIER_SITE','Remit To Supplier Site','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'REMIT_TO_SUPPLIER_SITE_ID','Remit To Supplier Site Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'RELATIONSHIP_ID','Relationship Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'SEGMENT1','Segment1','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'VENDOR_NAME','Vendor Name','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'INVOICE_STATUS','Invoice Status','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'BATCH_NAME','Batch Name','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'INVOICE_ID','Invoice Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'SETTLEMENT_PRIORITY','Settlement Priority','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'LAST_UPDATE_DATE','Last Update Date','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'LAST_UPDATED_BY','Last Updated By','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'VENDOR_ID','Vendor Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'INVOICE_NUM','Invoice Num','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'SET_OF_BOOKS_ID','Set Of Books Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'INVOICE_CURRENCY_CODE','Invoice Currency Code','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PAYMENT_CURRENCY_CODE','Payment Currency Code','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PAYMENT_CROSS_RATE','Payment Cross Rate','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'INVOICE_AMOUNT','Invoice Amount','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'VENDOR_SITE_ID','Vendor Site Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'AMOUNT_PAID','Amount Paid','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'DISCOUNT_AMOUNT_TAKEN','Discount Amount Taken','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'INVOICE_DATE','Invoice Date','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'SOURCE','Source','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'INVOICE_TYPE_LOOKUP_CODE','Invoice Type Lookup Code','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'DESCRIPTION','Description','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'BATCH_ID','Batch Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'AMOUNT_APPLICABLE_TO_DISCOUNT','Amount Applicable To Discount','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'TAX_AMOUNT','Tax Amount','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'TERMS_ID','Terms Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'TERMS_DATE','Terms Date','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PAYMENT_METHOD_LOOKUP_CODE','Payment Method Lookup Code','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PAY_GROUP_LOOKUP_CODE','Pay Group Lookup Code','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ACCTS_PAY_CODE_COMBINATION_ID','Accts Pay Code Combination Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PAYMENT_STATUS_FLAG','Payment Status Flag','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'CREATION_DATE','Creation Date','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'CREATED_BY','Created By','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'BASE_AMOUNT','Base Amount','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'VAT_CODE','Vat Code','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'LAST_UPDATE_LOGIN','Last Update Login','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'EXCLUSIVE_PAYMENT_FLAG','Exclusive Payment Flag','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PO_HEADER_ID','Po Header Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'FREIGHT_AMOUNT','Freight Amount','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GOODS_RECEIVED_DATE','Goods Received Date','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'INVOICE_RECEIVED_DATE','Invoice Received Date','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'VOUCHER_NUM','Voucher Num','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'RECURRING_PAYMENT_ID','Recurring Payment Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'EXCHANGE_RATE','Exchange Rate','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'EXCHANGE_RATE_TYPE','Exchange Rate Type','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'EXCHANGE_DATE','Exchange Date','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'EARLIEST_SETTLEMENT_DATE','Earliest Settlement Date','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ORIGINAL_PREPAYMENT_AMOUNT','Original Prepayment Amount','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'DOC_SEQUENCE_ID','Doc Sequence Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'DOC_SEQUENCE_VALUE','Doc Sequence Value','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'DOC_CATEGORY_CODE','Doc Category Code','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ATTRIBUTE1','Attribute1','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ATTRIBUTE2','Attribute2','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ATTRIBUTE3','Attribute3','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ATTRIBUTE4','Attribute4','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ATTRIBUTE5','Attribute5','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ATTRIBUTE6','Attribute6','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ATTRIBUTE7','Attribute7','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ATTRIBUTE8','Attribute8','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ATTRIBUTE9','Attribute9','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ATTRIBUTE10','Attribute10','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ATTRIBUTE11','Attribute11','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ATTRIBUTE12','Attribute12','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ATTRIBUTE13','Attribute13','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ATTRIBUTE14','Attribute14','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ATTRIBUTE15','Attribute15','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'APPROVED_AMOUNT','Approved Amount','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ATTRIBUTE_CATEGORY','Attribute Category','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'APPROVAL_STATUS','Approval Status','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'APPROVAL_DESCRIPTION','Approval Description','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'INVOICE_DISTRIBUTION_TOTAL','Invoice Distribution Total','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'POSTING_STATUS','Posting Status','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PREPAY_FLAG','Prepay Flag','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'AUTHORIZED_BY','Authorized By','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'CANCELLED_DATE','Cancelled Date','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'CANCELLED_BY','Cancelled By','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'CANCELLED_AMOUNT','Cancelled Amount','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'TEMP_CANCELLED_AMOUNT','Temp Cancelled Amount','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PROJECT_ACCOUNTING_CONTEXT','Project Accounting Context','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'USSGL_TRANSACTION_CODE','Ussgl Transaction Code','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'USSGL_TRX_CODE_CONTEXT','Ussgl Trx Code Context','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PROJECT_ID','Project Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'TASK_ID','Task Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'EXPENDITURE_TYPE','Expenditure Type','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'EXPENDITURE_ITEM_DATE','Expenditure Item Date','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PA_QUANTITY','Pa Quantity','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'EXPENDITURE_ORGANIZATION_ID','Expenditure Organization Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PA_DEFAULT_DIST_CCID','Pa Default Dist Ccid','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'VENDOR_PREPAY_AMOUNT','Vendor Prepay Amount','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PAYMENT_AMOUNT_TOTAL','Payment Amount Total','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'AWT_FLAG','Awt Flag','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'AWT_GROUP_ID','Awt Group Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'REFERENCE_1','Reference 1','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'REFERENCE_2','Reference 2','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'ORG_ID','Org Id','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'PRE_WITHHOLDING_AMOUNT','Pre Withholding Amount','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE_CATEGORY','Global Attribute Category','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE1','Global Attribute1','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE2','Global Attribute2','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE3','Global Attribute3','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE4','Global Attribute4','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE5','Global Attribute5','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE6','Global Attribute6','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE7','Global Attribute7','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE8','Global Attribute8','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE9','Global Attribute9','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE10','Global Attribute10','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE11','Global Attribute11','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE12','Global Attribute12','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE13','Global Attribute13','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE14','Global Attribute14','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - All Validated Invoices',200,'GLOBAL_ATTRIBUTE15','Global Attribute15','','','','','','','','Y','','','','','','','10012196','','','','XXEIS_91454_FEVHMB_V','','','GROUP_BY','US','');
--Inserting Report Parameters - WC - All Validated Invoices
xxeis.eis_rsc_ins.rp( 'WC - All Validated Invoices',200,'Creation Date From','','CREATION_DATE','IN','','','DATE','N','Y','1','Y','Y','CONSTANT','10012196','Y','N','','','','XXEIS_91454_FEVHMB_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - All Validated Invoices',200,'Gl Date From','','GL_DATE','IN','','','DATE','N','Y','3','Y','Y','CONSTANT','10012196','Y','N','','','','XXEIS_91454_FEVHMB_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - All Validated Invoices',200,'Supplier Number','','SEGMENT1','IN','','','NUMERIC','N','Y','5','Y','Y','CONSTANT','10012196','Y','N','','','','XXEIS_91454_FEVHMB_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - All Validated Invoices',200,'Creation Date To','','CREATION_DATE','IN','','','DATE','N','Y','2','Y','Y','CONSTANT','10012196','Y','N','','','','XXEIS_91454_FEVHMB_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - All Validated Invoices',200,'Gl Date To','','GL_DATE','IN','','','DATE','N','Y','4','Y','Y','CONSTANT','10012196','Y','N','','','','XXEIS_91454_FEVHMB_V','','','US','');
--Inserting Dependent Parameters - WC - All Validated Invoices
--Inserting Report Conditions - WC - All Validated Invoices
xxeis.eis_rsc_ins.rcnh( 'WC - All Validated Invoices',200,'CREATION_DATE >= :Creation Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Creation Date From','','','','','XXEIS_91454_FEVHMB_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',200,'WC - All Validated Invoices','CREATION_DATE >= :Creation Date From ');
xxeis.eis_rsc_ins.rcnh( 'WC - All Validated Invoices',200,'CREATION_DATE <= :Creation Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Creation Date To','','','','','XXEIS_91454_FEVHMB_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',200,'WC - All Validated Invoices','CREATION_DATE <= :Creation Date To ');
xxeis.eis_rsc_ins.rcnh( 'WC - All Validated Invoices',200,'GL_DATE >= :Gl Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_DATE','','Gl Date From','','','','','XXEIS_91454_FEVHMB_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',200,'WC - All Validated Invoices','GL_DATE >= :Gl Date From ');
xxeis.eis_rsc_ins.rcnh( 'WC - All Validated Invoices',200,'GL_DATE <= :Gl Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_DATE','','Gl Date To','','','','','XXEIS_91454_FEVHMB_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',200,'WC - All Validated Invoices','GL_DATE <= :Gl Date To ');
xxeis.eis_rsc_ins.rcnh( 'WC - All Validated Invoices',200,'SEGMENT1 IN :Supplier Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SEGMENT1','','Supplier Number','','','','','XXEIS_91454_FEVHMB_V','','','','','','IN','Y','Y','','','','','1',200,'WC - All Validated Invoices','SEGMENT1 IN :Supplier Number ');
--Inserting Report Sorts - WC - All Validated Invoices
--Inserting Report Triggers - WC - All Validated Invoices
--inserting report templates - WC - All Validated Invoices
--Inserting Report Portals - WC - All Validated Invoices
--inserting report dashboards - WC - All Validated Invoices
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC - All Validated Invoices','200','XXEIS_91454_FEVHMB_V','XXEIS_91454_FEVHMB_V','N','');
--inserting report security - WC - All Validated Invoices
xxeis.eis_rsc_ins.rsec( 'WC - All Validated Invoices','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - All Validated Invoices','200','','XXWC_PAY_VENDOR_MSTR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - All Validated Invoices','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - All Validated Invoices','200','','XXWC_PAY_MANAGER',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - All Validated Invoices','200','','XXWC_PAYABLES_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - All Validated Invoices','200','','XXWC_PAY_DISBURSE',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - All Validated Invoices','200','','XXWC_PAY_W_CALENDAR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - All Validated Invoices','200','','XXWC_PAY_NO_CALENDAR',200,'10012196','','','');
--Inserting Report Pivots - WC - All Validated Invoices
--Inserting Report   Version details- WC - All Validated Invoices
xxeis.eis_rsc_ins.rv( 'WC - All Validated Invoices','','WC - All Validated Invoices','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
