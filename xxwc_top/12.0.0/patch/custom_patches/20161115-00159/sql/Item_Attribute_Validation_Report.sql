--Report Name            : Item Attribute Validation Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_PO_ISR_RPT_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_PO_ISR_RPT_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_PO_ISR_RPT_V',201,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Po Isr V','EXPIV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_PO_ISR_RPT_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_PO_ISR_RPT_V',201,FALSE);
--Inserting Object Columns for EIS_XXWC_PO_ISR_RPT_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','BPA',201,'Bpa','BPA','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bpa','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ITEM_COST',201,'Item Cost','ITEM_COST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Item Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','AVER_COST',201,'Aver Cost','AVER_COST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Aver Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','AMU',201,'Amu','AMU','','~T~D~0','','XXEIS_RS_ADMIN','NUMBER','','','Amu','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','PM',201,'Pm','PM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pm','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','STK_FLAG',201,'Stk Flag','STK_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Stk Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','CL',201,'Cl','CL','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cl','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','UOM',201,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Uom','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','CAT',201,'Cat','CAT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','DESCRIPTION',201,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ST',201,'St','ST','','','','XXEIS_RS_ADMIN','VARCHAR2','','','St','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SOURCE',201,'Source','SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Source','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','VENDOR_NAME',201,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','VENDOR_NUM',201,'Vendor Num','VENDOR_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ITEM_NUMBER',201,'Item Number','ITEM_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','PRE',201,'Pre','PRE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pre','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ORG',201,'Org','ORG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Org','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','BUYER',201,'Buyer','BUYER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Buyer','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','TURNS',201,'Turns','TURNS','','','','XXEIS_RS_ADMIN','NUMBER','','','Turns','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','THIRTEEN_WK_AN_COGS',201,'Thirteen Wk An Cogs','THIRTEEN_WK_AN_COGS','','','','XXEIS_RS_ADMIN','NUMBER','','','Thirteen Wk An Cogs','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','THIRTEEN_WK_AVG_INV',201,'Thirteen Wk Avg Inv','THIRTEEN_WK_AVG_INV','','','','XXEIS_RS_ADMIN','NUMBER','','','Thirteen Wk Avg Inv','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','RES',201,'Res','RES','','~T~D~0','','XXEIS_RS_ADMIN','NUMBER','','','Res','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','FREEZE_DATE',201,'Freeze Date','FREEZE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Freeze Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','FI_FLAG',201,'Fi Flag','FI_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Fi Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MC',201,'Mc','MC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','BIN_LOC',201,'Bin Loc','BIN_LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bin Loc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','AVAILABLEDOLLAR',201,'Availabledollar','AVAILABLEDOLLAR','','','','XXEIS_RS_ADMIN','NUMBER','','','Availabledollar','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','AVAILABLE',201,'Available','AVAILABLE','','','','XXEIS_RS_ADMIN','NUMBER','','','Available','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','QOH',201,'Qoh','QOH','','','','XXEIS_RS_ADMIN','NUMBER','','','Qoh','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','APR_SALES',201,'Apr Sales','APR_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Apr Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','AUG_SALES',201,'Aug Sales','AUG_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Aug Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','DEC_SALES',201,'Dec Sales','DEC_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Dec Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','FEB_SALES',201,'Feb Sales','FEB_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Feb Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','HIT4_SALES',201,'Hit4 Sales','HIT4_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Hit4 Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','HIT6_SALES',201,'Hit6 Sales','HIT6_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Hit6 Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','JAN_SALES',201,'Jan Sales','JAN_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Jan Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','JUL_SALES',201,'Jul Sales','JUL_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Jul Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','JUNE_SALES',201,'June Sales','JUNE_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','June Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MAR_SALES',201,'Mar Sales','MAR_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Mar Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MAY_SALES',201,'May Sales','MAY_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','May Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','NOV_SALES',201,'Nov Sales','NOV_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Nov Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','OCT_SALES',201,'Oct Sales','OCT_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Oct Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SEP_SALES',201,'Sep Sales','SEP_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Sep Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ONE_SALES',201,'One Sales','ONE_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','One Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SIX_SALES',201,'Six Sales','SIX_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Six Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','TWELVE_SALES',201,'Twelve Sales','TWELVE_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Twelve Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','DISTRICT',201,'District','DISTRICT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','District','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','REGION',201,'Region','REGION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Region','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MAXN',201,'Maxn','MAXN','','~T~D~0','','XXEIS_RS_ADMIN','NUMBER','','','Maxn','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MINN',201,'Minn','MINN','','~T~D~0','','XXEIS_RS_ADMIN','NUMBER','','','Minn','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','PLT',201,'Plt','PLT','','~T~D~0','','XXEIS_RS_ADMIN','NUMBER','','','Plt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','PPLT',201,'Pplt','PPLT','','~T~D~0','','XXEIS_RS_ADMIN','NUMBER','','','Pplt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','TS',201,'Ts','TS','','~T~D~0','','XXEIS_RS_ADMIN','NUMBER','','','Ts','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','BPA_COST',201,'Bpa Cost','BPA_COST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Bpa Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ON_ORD',201,'On Ord','ON_ORD','','','','XXEIS_RS_ADMIN','NUMBER','','','On Ord','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','FML',201,'Fml','FML','','','','XXEIS_RS_ADMIN','NUMBER','','','Fml','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','OPEN_REQ',201,'Open Req','OPEN_REQ','','','','XXEIS_RS_ADMIN','NUMBER','','','Open Req','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','WT',201,'Wt','WT','','','','XXEIS_RS_ADMIN','NUMBER','','','Wt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SO',201,'So','SO','','','','XXEIS_RS_ADMIN','NUMBER','','','So','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SOURCING_RULE',201,'Sourcing Rule','SOURCING_RULE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sourcing Rule','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SS',201,'Ss','SS','','','','XXEIS_RS_ADMIN','NUMBER','','','Ss','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','CLT',201,'Clt','CLT','','','','XXEIS_RS_ADMIN','NUMBER','','','Clt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','AVAIL2',201,'Avail2','AVAIL2','','','','XXEIS_RS_ADMIN','NUMBER','','','Avail2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','INT_REQ',201,'Int Req','INT_REQ','','','','XXEIS_RS_ADMIN','NUMBER','','','Int Req','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','DIR_REQ',201,'Dir Req','DIR_REQ','','','','XXEIS_RS_ADMIN','NUMBER','','','Dir Req','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SITE_VENDOR_NUM',201,'Site Vendor Num','SITE_VENDOR_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Vendor Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','VENDOR_SITE',201,'Vendor Site','VENDOR_SITE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Site','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ORG_NAME',201,'Org Name','ORG_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Org Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MF_FLAG',201,'Mf Flag','MF_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mf Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SET_OF_BOOKS_ID',201,'Set Of Books Id','SET_OF_BOOKS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Set Of Books Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ORGANIZATION_ID',201,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','INVENTORY_ITEM_ID',201,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','COMMON_OUTPUT_ID',201,'Common Output Id','COMMON_OUTPUT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Common Output Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','PROCESS_ID',201,'Process Id','PROCESS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Process Id','','','','US');
--Inserting Object Components for EIS_XXWC_PO_ISR_RPT_V
--Inserting Object Component Joins for EIS_XXWC_PO_ISR_RPT_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report LOV Data for Item Attribute Validation Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Item Attribute Validation Report
xxeis.eis_rsc_ins.lov( 201,'select distinct segment1 bin_loc from Mtl_Item_Locations','','Bin Location Lov','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'    SELECT distinct Mcv.SEGMENT2 cat_class
        from  Mtl_Categories_Kfv Mcv,
            MTL_CATEGORY_SETS MCS,
            MTL_ITEM_CATEGORIES MIC,
            mtl_system_items_kfv msi
            Where   Mcs.Category_Set_Name      = ''Inventory Category''
          AND MCS.STRUCTURE_ID                 = MCV.STRUCTURE_ID
          AND MIC.INVENTORY_ITEM_ID            = MSI.INVENTORY_ITEM_ID
          And Mic.Organization_Id              = msi.Organization_Id
          AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
          And Mic.Category_Id                  = Mcv.Category_Id
order by Mcv.SEGMENT2','','Catclass Lov','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'','Exclude,Include,Tool Repair Only','Tool Repair','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'','  ,Vendor Number(%),3 Digit Prefix,Item number,Source,2 Digit Cat,4 Digit Cat Class,Default Buyer(%)','Report Criteria','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'','Active small,All items,All items on hand, Stock items only,Non stock only,Active large, Non-stock on hand,Stock items with 0/0 min/max','EIS PO XXWC ISR REPORT COND','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'','Include,Time Sensitive Only','EIS PO XXWC Time Sensitive','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'','Yes,No','EIS PO XXWC DC Mode','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Supplier'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Supplier List','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Source'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Source List','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Cat Class''  and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Cat Class List','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT DISTINCT segment1 Item
FROM mtl_system_items_b msi
WHERE NOT EXISTS
  (SELECT 1
  FROM mtl_parameters mp
  WHERE organization_code IN (''CAN'',''HDS'',''US1'',''CN1'')
  AND msi.organization_id  = mp.organization_id
  )
order by segment1 ASC','','XXWC Item','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT POV.SEGMENT1 VENDOR_NUMBER,
  POV.VENDOR_NAME
FROM APPS.PO_VENDORS POV
WHERE EXISTS
  (SELECT /*+ INDEX(MSA XXWC_MRP_SR_ASSIGNMENTS_N7)*/
	1
  FROM APPS.MRP_SR_SOURCE_ORG MSSO ,
    APPS.MRP_SR_RECEIPT_ORG MSRO ,
    APPS.MRP_SOURCING_RULES MSR ,
    APPS.MRP_SR_ASSIGNMENTS MSA ,
    APPS.MTL_SYSTEM_ITEMS_B MSI
  WHERE 1                   =1
  AND msro.sr_receipt_id    = msso.sr_receipt_id
  AND MSR.SOURCING_RULE_ID  = MSRO.SOURCING_RULE_ID
  AND MSSO.VENDOR_ID        = POV.VENDOR_ID
  AND MSA.SOURCING_RULE_ID  = MSRO.SOURCING_RULE_ID
  AND MSA.INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
  AND MSA.ORGANIZATION_ID   = MSI.ORGANIZATION_ID
  AND MSI.SOURCE_TYPE       = 2
  )
ORDER BY LPAD(POV.SEGMENT1,10)','','XXWC Vendors','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT ood.organization_code organization_code,
  ood.organization_name organization_name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE organization_code NOT IN(''CAN'',''HDS'',''US1'',''CN1'')
ORDER BY organization_code','','XXWC Org Lov','','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report Data for Item Attribute Validation Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Item Attribute Validation Report
xxeis.eis_rsc_utility.delete_report_rows( 'Item Attribute Validation Report' );
--Inserting Report - Item Attribute Validation Report
xxeis.eis_rsc_ins.r( 201,'Item Attribute Validation Report','','This report displays info needed to make inventory replenishment decisions ( by vendor, part number) at selected locations.','','','','PA022863','EIS_XXWC_PO_ISR_RPT_V','Y','','','PA022863','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Item Attribute Validation Report
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'AVER_COST','Aver Cost','Aver Cost','','~T~D~2','default','','27','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'BIN_LOC','Bin Loc','Bin Loc','','','default','','40','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'BPA','Bpa#','Bpa','','','default','','30','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'BUYER','Buyer','Buyer','','','default','','48','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'CAT','Cat/Cl','Cat','','','default','','10','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'CL','Cl','Cl','','','default','','19','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'DEC_SALES','Dec','Dec Sales','','~T~D~0','default','','61','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'DESCRIPTION','Description','Description','','','default','','9','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'FEB_SALES','Feb','Feb Sales','','~T~D~0','default','','51','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'FI_FLAG','FI','Fi Flag','','','default','','42','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'FREEZE_DATE','Freeze Date','Freeze Date','','','default','','43','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'HIT4_SALES','Hit4','Hit4 Sales','','~T~D~0','default','','26','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'HIT6_SALES','Hit6','Hit6 Sales','','~T~D~0','default','','25','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'ITEM_COST','Item Cost','Item Cost','','~T~D~2','default','','28','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'ITEM_NUMBER','Item Number','Item Number','','','default','','3','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'JAN_SALES','Jan','Jan Sales','','~T~D~0','default','','50','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'AUG_SALES','Aug','Aug Sales','','~T~D~0','default','','57','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'AVAILABLE','Avail','Available','','~T~D~0','default','','34','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'AVAILABLEDOLLAR','Ext$','Availabledollar','','~T~D~0','default','','36','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'JUL_SALES','Jul','Jul Sales','','~T~D~0','default','','56','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'JUNE_SALES','June','June Sales','','~T~D~0','default','','55','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'MAR_SALES','Mar','Mar Sales','','~T~D~0','default','','52','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'MAXN','Max','Maxn','NUMBER','~T~D~0','default','','23','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'MAY_SALES','May','May Sales','','~T~D~0','default','','54','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'MC','MC','Mc','','','default','','41','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'QOH','QOH','Qoh','','~T~D~0','default','','31','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'RES','Res','Res','NUMBER','~T~D~0','default','','44','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'SEP_SALES','Sep','Sep Sales','','~T~D~0','default','','58','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'SIX_SALES','6','Six Sales','','~T~D~0','default','','38','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'SOURCE','Source','Source','','','default','','7','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'ST','ST','St','','','default','','8','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'STK_FLAG','Stk','Stk Flag','','','default','','20','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'THIRTEEN_WK_AN_COGS','13 Wk An COGS $','Thirteen Wk An Cogs','','~T~D~0','default','','46','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'THIRTEEN_WK_AVG_INV','13 Wk Av Inv $','Thirteen Wk Avg Inv','','~T~D~0','default','','45','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'TS','TS','Ts','NUMBER','~T~D~0','default','','49','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'TURNS','Turns','Turns','','~T~D~0','default','','47','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'TWELVE_SALES','12','Twelve Sales','','~T~D~0','default','','39','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'UOM','UOM','Uom','','','default','','15','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'VENDOR_NAME','Vendor','Vendor Name','','','default','','5','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'MINN','Min','Minn','NUMBER','~T~D~0','default','','22','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'VENDOR_NUM','Vend #','Vendor Num','','','default','','4','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'AMU','AMU','Amu','NUMBER','~T~D~0','default','','24','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'FML','FLM','Fml','','~T~D~0','default','','17','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'OPEN_REQ','Req','Open Req','','~T~D~0','default','','32','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'WT','Wt','Wt','','~T~D~0','default','','16','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'SO','So','So','','~T~D~0','default','','18','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'SOURCING_RULE','Sourcing Rule','Sourcing Rule','','','default','','6','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'SS','Ss','Ss','','~T~D~0','default','','14','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'CLT','CLT','Clt','','~T~D~0','default','','13','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'AVAIL2','Avail2','Avail2','','~T~D~0','default','','35','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'BPA_COST','Bpa Cost','Bpa Cost','','~T~D~2','default','','29','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'ON_ORD','On Ord','On Ord','','~T~D~0','default','','33','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'APR_SALES','Apr','Apr Sales','','~T~D~0','default','','53','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'INT_REQ','Int Req','Int Req','','~T~D~0','default','','62','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'DIR_REQ','Dir Req','Dir Req','','~T~D~0','default','','63','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'NOV_SALES','Nov','Nov Sales','','~T~D~0','default','','60','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'OCT_SALES','Oct','Oct Sales','','~T~D~0','default','','59','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'ONE_SALES','1','One Sales','','~T~D~0','default','','37','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'ORG','Org','Org','','','default','','1','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'PLT','PLT','Plt','','~T~D~0','default','','12','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'PM','PM','Pm','','','default','','21','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'PPLT','PPLT','Pplt','','~T~D~0','default','','11','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'PRE','Pre','Pre','','','default','','2','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'SITE_VENDOR_NUM','Supplier Number','Site Vendor Num','','','default','','64','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Item Attribute Validation Report',201,'VENDOR_SITE','Supplier Site','Vendor Site','','','default','','65','N','Y','','','','','','','PA022863','N','N','','EIS_XXWC_PO_ISR_RPT_V','','','','US','');
--Inserting Report Parameters - Item Attribute Validation Report
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'Items w/ 4 mon sales hits','Stock items w/ 4 mon sales hits > x','','IN','','','NUMERIC','N','Y','9','','N','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'Report Criteria','Report Criteria','','IN','Report Criteria','','VARCHAR2','N','Y','10','','N','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'Report Criteria Value','Report Criteria Value','','IN','','','VARCHAR2','N','Y','11','','N','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'DC Mode','DC Mode','','IN','EIS PO XXWC DC Mode','''No''','VARCHAR2','N','Y','5','','N','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'Starting Bin','Starting Bin','BIN_LOC','>=','Bin Location Lov','','VARCHAR2','N','Y','12','','Y','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'Ending Bin','Ending Bin','BIN_LOC','<=','Bin Location Lov','','VARCHAR2','N','Y','13','','Y','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'Tool Repair','Tool Repair','','IN','Tool Repair','''Include''','VARCHAR2','N','Y','6','','N','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'Time Sensitive','Time Sensitive','','IN','EIS PO XXWC Time Sensitive','','VARCHAR2','N','Y','7','','N','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'Vendor','Vendor','VENDOR_NUM','IN','XXWC Vendors','','VARCHAR2','N','Y','14','Y','Y','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'Item','Item','ITEM_NUMBER','IN','XXWC Item','','VARCHAR2','N','Y','15','Y','Y','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'Cat Class','Cat Class','CAT','IN','Catclass Lov','','VARCHAR2','N','Y','16','Y','Y','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'Report Filter','Report Condition','','IN','EIS PO XXWC ISR REPORT COND','','VARCHAR2','N','Y','8','','N','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'Item List','Item List','','IN','XXWC Item List','','VARCHAR2','N','Y','17','','N','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'Vendor List','Vendor List','','IN','XXWC Supplier List','','VARCHAR2','N','Y','18','','N','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'Cat Class List','Cat Class List','','IN','XXWC Cat Class List','','VARCHAR2','N','Y','19','','N','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'Source List','Source List','','IN','XXWC Source List','','VARCHAR2','N','Y','20','','N','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'Organization List','Organization List','','IN','XXWC Org List','','VARCHAR2','N','Y','4','','N','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Item Attribute Validation Report',201,'Organization','Organization','ORG','IN','XXWC Org Lov','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','PA022863','Y','N','','','','EIS_XXWC_PO_ISR_RPT_V','','','US','');
--Inserting Dependent Parameters - Item Attribute Validation Report
--Inserting Report Conditions - Item Attribute Validation Report
xxeis.eis_rsc_ins.rcnh( 'Item Attribute Validation Report',201,'BIN_LOC <= :Ending Bin ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BIN_LOC','','Ending Bin','','','','','EIS_XXWC_PO_ISR_RPT_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',201,'Item Attribute Validation Report','BIN_LOC <= :Ending Bin ');
xxeis.eis_rsc_ins.rcnh( 'Item Attribute Validation Report',201,'BIN_LOC >= :Starting Bin ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BIN_LOC','','Starting Bin','','','','','EIS_XXWC_PO_ISR_RPT_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',201,'Item Attribute Validation Report','BIN_LOC >= :Starting Bin ');
xxeis.eis_rsc_ins.rcnh( 'Item Attribute Validation Report',201,'CAT IN :Cat Class ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CAT','','Cat Class','','','','','EIS_XXWC_PO_ISR_RPT_V','','','','','','IN','Y','Y','','','','','1',201,'Item Attribute Validation Report','CAT IN :Cat Class ');
xxeis.eis_rsc_ins.rcnh( 'Item Attribute Validation Report',201,'DISTRICT IN :District ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DISTRICT','','District','','','','','EIS_XXWC_PO_ISR_RPT_V','','','','','','IN','Y','Y','','','','','1',201,'Item Attribute Validation Report','DISTRICT IN :District ');
xxeis.eis_rsc_ins.rcnh( 'Item Attribute Validation Report',201,'ITEM_NUMBER IN :Item ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ITEM_NUMBER','','Item','','','','','EIS_XXWC_PO_ISR_RPT_V','','','','','','IN','Y','Y','','','','','1',201,'Item Attribute Validation Report','ITEM_NUMBER IN :Item ');
xxeis.eis_rsc_ins.rcnh( 'Item Attribute Validation Report',201,'ORG IN :Organization ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORG','','Organization','','','','','EIS_XXWC_PO_ISR_RPT_V','','','','','','IN','Y','Y','','','','','1',201,'Item Attribute Validation Report','ORG IN :Organization ');
xxeis.eis_rsc_ins.rcnh( 'Item Attribute Validation Report',201,'PROCESS_ID IN :SYSTEM.PROCESS_ID ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PROCESS_ID','','','','','','','EIS_XXWC_PO_ISR_RPT_V','','','','','','IN','Y','N','',':SYSTEM.PROCESS_ID','','','1',201,'Item Attribute Validation Report','PROCESS_ID IN :SYSTEM.PROCESS_ID ');
xxeis.eis_rsc_ins.rcnh( 'Item Attribute Validation Report',201,'REGION IN :Region ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','REGION','','Region','','','','','EIS_XXWC_PO_ISR_RPT_V','','','','','','IN','Y','Y','','','','','1',201,'Item Attribute Validation Report','REGION IN :Region ');
xxeis.eis_rsc_ins.rcnh( 'Item Attribute Validation Report',201,'VENDOR_NUM IN :Vendor ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NUM','','Vendor','','','','','EIS_XXWC_PO_ISR_RPT_V','','','','','','IN','Y','Y','','','','','1',201,'Item Attribute Validation Report','VENDOR_NUM IN :Vendor ');
--Inserting Report Sorts - Item Attribute Validation Report
--Inserting Report Triggers - Item Attribute Validation Report
xxeis.eis_rsc_ins.rt( 'Item Attribute Validation Report',201,'begin
xxeis.eis_po_xxwc_isr_util_pkg.G_isr_rpt_dc_mod_sub:=:DC Mode;
xxeis.eis_po_xxwc_isr_pkg.Isr_live_Rpt_Proc(
P_process_id => :SYSTEM.PROCESS_ID,
P_Region =>:Region,
P_District =>:District,
P_Location =>:Organization,
P_Dc_Mode =>:DC Mode,
P_Tool_Repair =>:Tool Repair,
P_Time_Sensitive =>:Time Sensitive,                            
P_Stk_Items_With_Hit4 =>:Items w/ 4 mon sales hits,
p_Report_Condition =>:Report Filter,
P_Report_Criteria            =>:Report Criteria,
P_Report_Criteria_val            =>:Report Criteria Value,
p_start_bin_loc =>:Starting Bin,
p_end_bin_loc =>:Ending Bin,
p_vendor =>:Vendor,
p_item =>:Item,
p_cat_class =>:Cat Class,
p_org_list =>:Organization List,
P_ITEM_LIST =>:Item List,
P_SUPPLIER_LIST =>:Vendor List,
P_CAT_CLASS_LIST =>:Cat Class List,
P_SOURCE_LIST =>:Source List
);
end;','B','Y','PA022863','');
--inserting report templates - Item Attribute Validation Report
--Inserting Report Portals - Item Attribute Validation Report
--inserting report dashboards - Item Attribute Validation Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Item Attribute Validation Report','201','EIS_XXWC_PO_ISR_RPT_V','EIS_XXWC_PO_ISR_RPT_V','N','');
--inserting report security - Item Attribute Validation Report
xxeis.eis_rsc_ins.rsec( 'Item Attribute Validation Report','','AB063501','',201,'PA022863','','N','');
--Inserting Report Pivots - Item Attribute Validation Report
--Inserting Report   Version details- Item Attribute Validation Report
xxeis.eis_rsc_ins.rv( 'Item Attribute Validation Report','','Item Attribute Validation Report','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
