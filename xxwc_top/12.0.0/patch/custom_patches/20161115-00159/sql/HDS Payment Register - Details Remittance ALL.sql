--Report Name            : HDS Payment Register - Details Remittance ALL
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_AP_PAYMENT_REG_REMIT
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_AP_PAYMENT_REG_REMIT
xxeis.eis_rsc_ins.v( 'EIS_AP_PAYMENT_REG_REMIT',200,'Paste SQL View for HDS Payment Register - Details Remittance ALL','1.0','','','ID020048','APPS','HDS Payment Register - Details Remittance ALL View','X1OV12','','','VIEW','US','','');
--Delete Object Columns for EIS_AP_PAYMENT_REG_REMIT
xxeis.eis_rsc_utility.delete_view_rows('EIS_AP_PAYMENT_REG_REMIT',200,FALSE);
--Inserting Object Columns for EIS_AP_PAYMENT_REG_REMIT
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','CHECK_NUMBER',200,'','','','','','ID020048','NUMBER','','','Check Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','PAYMENT_DATE',200,'','','','','','ID020048','DATE','','','Payment Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','CHECK_AMOUNT',200,'','','','~T~D~2','','ID020048','NUMBER','','','Check Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','VENDOR_NAME',200,'','','','','','ID020048','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','SUPPLIER_NUMBER',200,'','','','','','ID020048','VARCHAR2','','','Supplier Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','INVOICE_NUMBER',200,'','','','','','ID020048','VARCHAR2','','','Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','INVOICE_AMOUNT',200,'','','','~T~D~2','','ID020048','NUMBER','','','Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','DISTRIBUTION_AMOUNT',200,'','','','~T~D~2','','ID020048','NUMBER','','','Distribution Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','EXT_BANK_ACCOUNT_NAME',200,'','','','','','ID020048','VARCHAR2','','','Ext Bank Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','EXT_BANK_ACCOUNT_NUMBER',200,'','','','','','ID020048','VARCHAR2','','','Ext Bank Account Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','INT_BANK_NAME',200,'','','','','','ID020048','VARCHAR2','','','Int Bank Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','PAYMENT_METHOD_CODE',200,'','','','','','ID020048','VARCHAR2','','','Payment Method Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','EXT_BANK_ACCOUNT_IBAN_NUMBER',200,'','','','','','ID020048','VARCHAR2','','','Ext Bank Account Iban Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','INT_BANK_BRANCH_NAME',200,'','','','','','ID020048','VARCHAR2','','','Int Bank Branch Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','INT_BANK_ACCOUNT_NAME',200,'','','','','','ID020048','VARCHAR2','','','Int Bank Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','INT_BANK_ACCOUNT_NUMBER',200,'','','','','','ID020048','VARCHAR2','','','Int Bank Account Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','DISCOUNT_TAKEN',200,'','','','~T~D~2','','ID020048','NUMBER','','','Discount Taken','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','AMOUNT_PAID',200,'','','','~T~D~2','','ID020048','NUMBER','','','Amount Paid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','CHECKRUN_NAME',200,'','','','','','ID020048','VARCHAR2','','','Checkrun Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','ADDRESS_LINE1',200,'','','','','','ID020048','VARCHAR2','','','Address Line1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','ADDRESS_LINE2',200,'','','','','','ID020048','VARCHAR2','','','Address Line2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','CITY',200,'','','','','','ID020048','VARCHAR2','','','City','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','STATE',200,'','','','','','ID020048','VARCHAR2','','','State','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','ZIP',200,'','','','','','ID020048','VARCHAR2','','','Zip','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','INVOICE_DATE',200,'','','','','','ID020048','DATE','','','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','CHECK_CREATED_BY',200,'','','','','','ID020048','VARCHAR2','','','Check Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','ORGANIZATION_NAME',200,'','','','','','ID020048','VARCHAR2','','','Organization Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMIT','EXT_BRANCH_NUMBER',200,'','','','','','ID020048','VARCHAR2','','','Ext Branch Number','','','','US');
--Inserting Object Components for EIS_AP_PAYMENT_REG_REMIT
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMIT','AP_INVOICE_PAYMENTS_ALL',200,'AP_INVOICE_PAYMENTS_ALL','AIP','AIP','ID020048','ID020048','49049140','Invoice Payment Records','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMIT','AP_CHECKS_ALL',200,'AP_CHECKS_ALL','CH','CH','ID020048','ID020048','49049140','Supplier Payment Data','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMIT','AP_INVOICES_ALL',200,'AP_INVOICES_ALL','API','API','ID020048','ID020048','49049140','Detailed Invoice Records','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMIT','AP_INVOICE_DISTRIBUTIONS_ALL',200,'AP_INVOICE_DISTRIBUTIONS_ALL','APID','APID','ID020048','ID020048','49049140','Invoice Distribution Line Information','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMIT','PO_DISTRIBUTIONS_ALL',200,'PO_DISTRIBUTIONS_ALL','POD','POD','ID020048','ID020048','49049140','Purchase Order Distributions','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMIT','PO_LINE_LOCATIONS_ALL',200,'PO_LINE_LOCATIONS_ALL','POLL','POLL','ID020048','ID020048','49049140','Document Shipment Schedules (For Purchase Orders, ','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMIT','PO_LINES_ALL',200,'PO_LINES_ALL','POL','POL','ID020048','ID020048','49049140','Purchase Document Lines (For Purchase Orders, Purc','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMIT','PO_HEADERS_ALL',200,'PO_HEADERS_ALL','POH','POH','ID020048','ID020048','49049140','Document Headers (For Purchase Orders, Purchase Ag','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMIT','AP_SUPPLIERS',200,'AP_SUPPLIERS','PV','PV','ID020048','ID020048','49049140','Ap Suppliers Stores Information About Your Supplie','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMIT','AP_SUPPLIER_SITES_ALL',200,'AP_SUPPLIER_SITES_ALL','PVS','PVS','ID020048','ID020048','49049140','Ap Supplier Sites All Stores Information About You','','','','','ASSA','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMIT','HR_ORGANIZATION_UNITS',200,'HR_ALL_ORGANIZATION_UNITS','HOU','HOU','ID020048','ID020048','-1','HR_ALL_ORGANIZATION_UNITS','N','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_AP_PAYMENT_REG_REMIT
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMIT','AP_INVOICE_PAYMENTS_ALL','AIP',200,'X1OV12.INVOICE_PAYMENT_ID','=','AIP.INVOICE_PAYMENT_ID(+)','','','','Y','ID020048');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMIT','AP_CHECKS_ALL','CH',200,'X1OV12.CHECK_ID','=','CH.CHECK_ID(+)','','','','Y','ID020048');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMIT','AP_INVOICES_ALL','API',200,'X1OV12.INVOICE_ID','=','API.INVOICE_ID(+)','','','','Y','ID020048');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMIT','AP_INVOICE_DISTRIBUTIONS_ALL','APID',200,'X1OV12.INVOICE_ID','=','APID.INVOICE_ID(+)','','','','Y','ID020048');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMIT','AP_INVOICE_DISTRIBUTIONS_ALL','APID',200,'X1OV12.INVOICE_LINE_NUMBER','=','APID.INVOICE_LINE_NUMBER(+)','','','','Y','ID020048');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMIT','AP_INVOICE_DISTRIBUTIONS_ALL','APID',200,'X1OV12.DISTRIBUTION_LINE_NUMBER','=','APID.DISTRIBUTION_LINE_NUMBER(+)','','','','Y','ID020048');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMIT','PO_DISTRIBUTIONS_ALL','POD',200,'X1OV12.PO_DISTRIBUTION_ID','=','POD.PO_DISTRIBUTION_ID(+)','','','','Y','ID020048');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMIT','PO_LINE_LOCATIONS_ALL','POLL',200,'X1OV12.LINE_LOCATION_ID','=','POLL.LINE_LOCATION_ID(+)','','','','Y','ID020048');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMIT','PO_LINES_ALL','POL',200,'X1OV12.PO_LINE_ID','=','POL.PO_LINE_ID(+)','','','','Y','ID020048');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMIT','PO_HEADERS_ALL','POH',200,'X1OV12.PO_HEADER_ID','=','POH.PO_HEADER_ID(+)','','','','Y','ID020048');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMIT','AP_SUPPLIERS','PV',200,'X1OV12.VENDOR_ID','=','PV.VENDOR_ID(+)','','','','Y','ID020048');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMIT','AP_SUPPLIER_SITES_ALL','PVS',200,'X1OV12.VENDOR_SITE_ID','=','PVS.VENDOR_SITE_ID(+)','','','','Y','ID020048');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMIT','HR_ORGANIZATION_UNITS','HOU',200,'X1OV12.ORGANIZATION_ID','=','HOU.ORGANIZATION_ID(+)','','','','Y','ID020048');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report LOV Data for HDS Payment Register - Details Remittance ALL
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS Payment Register - Details Remittance ALL
xxeis.eis_rsc_ins.lov( 200,'select distinct CHECKRUN_NAME from AP_CHECKS','','EIS_AP_CHECKRUN_NAME_LOV','List of Values for Checkrun Name','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 200,'select CHECK_NUMBER from AP_CHECKS','','EIS_AP_CHECK_NUMBER_LOV','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 200,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 200,'select distinct payment_method_code from iby.IBY_PAYMENTS_ALL','','EIS_AP_Payment_Method_Code','List of Values for Payment Method Code','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report Data for HDS Payment Register - Details Remittance ALL
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS Payment Register - Details Remittance ALL
xxeis.eis_rsc_utility.delete_report_rows( 'HDS Payment Register - Details Remittance ALL' );
--Inserting Report - HDS Payment Register - Details Remittance ALL
xxeis.eis_rsc_ins.r( 200,'HDS Payment Register - Details Remittance ALL','','','','','','XXEIS_RS_ADMIN','EIS_AP_PAYMENT_REG_REMIT','Y','','select   pv.segment1 Supplier_Number,
         ch.vendor_name,
         ch.address_line1,
         ch.address_line2,
         ch.city,
         ch.state,
         ch.zip,
         ipa.ext_bank_account_name,
         ipa.ext_bank_account_number,
         ipa.ext_branch_number,
         ipa.ext_bank_account_iban_number,
         ch.checkrun_name,
         ch.amount check_amount,
         ch.check_number,
         apid.amount amount_paid,
         aip.discount_taken,
         apid.amount distribution_amount,
         api.invoice_num invoice_number,
         api.invoice_date,
         api.invoice_amount invoice_amount,
         ipa.int_bank_account_name,
         ipa.int_bank_account_number,
         ipa.int_bank_branch_name,
         ipa.int_bank_name,
         trunc(ch.check_date) payment_date,
         ipa.payment_method_code,
         fu.user_name check_created_by,
         hou.name organization_name

from   ap.ap_invoice_payments_all aip,
       ap.ap_checks_all ch,
       ap.ap_invoices_all api,
       ap.ap_invoice_distributions_all apid,
       apps.fnd_user fu,
       iby.IBY_PAYMENTS_ALL ipa,
       ap.ap_suppliers pv,
       ap.ap_supplier_sites_all pvs,
       hr_operating_units hou
       
WHERE aip.check_id = ch.check_id
  AND aip.invoice_id = api.invoice_id
  AND api.invoice_id = apid.invoice_id
  AND api.vendor_id = ch.vendor_id
  AND pvs.vendor_site_id = ch.vendor_site_id
  AND pvs.vendor_id = pv.vendor_id
  AND ipa.payment_process_request_name = ch.checkrun_name
  AND ch.party_id = ipa.payee_party_id
  AND ch.created_by = fu.user_id
  AND aip.org_id = hou.organization_id
  AND ch.created_by = fu.user_id
','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','APPS','US','','','','');
--Inserting Report Columns - HDS Payment Register - Details Remittance ALL
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'ADDRESS_LINE1','Address Line1','','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'ADDRESS_LINE2','Address Line2','','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'AMOUNT_PAID','Amount Paid','','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'CHECKRUN_NAME','Checkrun Name','','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'CHECK_AMOUNT','Check Amount','','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'CHECK_NUMBER','Check Number','','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'CITY','City','','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'DISCOUNT_TAKEN','Discount Taken','','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'DISTRIBUTION_AMOUNT','Distribution Amount','','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'INVOICE_DATE','Invoice Date','','','','','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'INVOICE_NUMBER','Invoice Number','','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'PAYMENT_DATE','Payment Date','','','','','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'STATE','State','','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'VENDOR_NAME','Vendor Name','','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'ZIP','Zip','','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'SUPPLIER_NUMBER','Supplier Number','','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'EXT_BANK_ACCOUNT_NAME','Ext Bank Account Name','','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'EXT_BANK_ACCOUNT_NUMBER','Ext Bank Account Number','','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'EXT_BANK_ACCOUNT_IBAN_NUMBER','Ext Bank Account Iban Number','','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'INVOICE_AMOUNT','Invoice Amount','','','','','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'INT_BANK_ACCOUNT_NAME','Int Bank Account Name','','','','','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'INT_BANK_ACCOUNT_NUMBER','Int Bank Account Number','','','','','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'INT_BANK_BRANCH_NAME','Int Bank Branch Name','','','','','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'INT_BANK_NAME','Int Bank Name','','','','','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'PAYMENT_METHOD_CODE','Payment Method Code','','','','','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'CHECK_CREATED_BY','Check Created By','','','','','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance ALL',200,'EXT_BRANCH_NUMBER','Ext Branch Number','','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMIT','','','GROUP_BY','US','');
--Inserting Report Parameters - HDS Payment Register - Details Remittance ALL
xxeis.eis_rsc_ins.rp( 'HDS Payment Register - Details Remittance ALL',200,'Payment Date From','Payment Date From','PAYMENT_DATE','>=','','','DATE','N','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','','EIS_AP_PAYMENT_REG_REMIT','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payment Register - Details Remittance ALL',200,'Payment Date To','Payment Date To','PAYMENT_DATE','<=','','','DATE','N','Y','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','','EIS_AP_PAYMENT_REG_REMIT','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payment Register - Details Remittance ALL',200,'Check Number','Check Number','CHECK_NUMBER','IN','EIS_AP_CHECK_NUMBER_LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AP_PAYMENT_REG_REMIT','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payment Register - Details Remittance ALL',200,'Checkrun Name','Checkrun Name','CHECKRUN_NAME','IN','EIS_AP_CHECKRUN_NAME_LOV','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AP_PAYMENT_REG_REMIT','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payment Register - Details Remittance ALL',200,'Payment Type','Payment Type','PAYMENT_METHOD_CODE','IN','EIS_AP_Payment_Method_Code','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AP_PAYMENT_REG_REMIT','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payment Register - Details Remittance ALL',200,'Organization Name','Organization Name','ORGANIZATION_NAME','IN','EIS_MULTI_OPERATING_UNIT_LOV','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AP_PAYMENT_REG_REMIT','','','US','');
--Inserting Dependent Parameters - HDS Payment Register - Details Remittance ALL
--Inserting Report Conditions - HDS Payment Register - Details Remittance ALL
xxeis.eis_rsc_ins.rcnh( 'HDS Payment Register - Details Remittance ALL',200,'CHECKRUN_NAME IN :Checkrun Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECKRUN_NAME','','Checkrun Name','','','','','EIS_AP_PAYMENT_REG_REMIT','','','','','','IN','Y','Y','','','','','1',200,'HDS Payment Register - Details Remittance ALL','CHECKRUN_NAME IN :Checkrun Name ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payment Register - Details Remittance ALL',200,'CHECK_NUMBER IN :Check Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECK_NUMBER','','Check Number','','','','','EIS_AP_PAYMENT_REG_REMIT','','','','','','IN','Y','Y','','','','','1',200,'HDS Payment Register - Details Remittance ALL','CHECK_NUMBER IN :Check Number ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payment Register - Details Remittance ALL',200,'ORGANIZATION_NAME IN :Organization Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORGANIZATION_NAME','','Organization Name','','','','','EIS_AP_PAYMENT_REG_REMIT','','','','','','IN','Y','Y','','','','','1',200,'HDS Payment Register - Details Remittance ALL','ORGANIZATION_NAME IN :Organization Name ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payment Register - Details Remittance ALL',200,'PAYMENT_METHOD_CODE = ''WIRE'' ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PAYMENT_METHOD_CODE','','','','','','','EIS_AP_PAYMENT_REG_REMIT','','','','','','EQUALS','Y','N','','''WIRE''','','','1',200,'HDS Payment Register - Details Remittance ALL','PAYMENT_METHOD_CODE = ''WIRE'' ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payment Register - Details Remittance ALL',200,'PAYMENT_METHOD_CODE IN :Payment Type ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PAYMENT_METHOD_CODE','','Payment Type','','','','','EIS_AP_PAYMENT_REG_REMIT','','','','','','IN','Y','Y','','','','','1',200,'HDS Payment Register - Details Remittance ALL','PAYMENT_METHOD_CODE IN :Payment Type ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payment Register - Details Remittance ALL',200,'X1OV12.PAYMENT_DATE >= Payment Date From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PAYMENT_DATE','','Payment Date From','','','','','EIS_AP_PAYMENT_REG_REMIT','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',200,'HDS Payment Register - Details Remittance ALL','X1OV12.PAYMENT_DATE >= Payment Date From');
xxeis.eis_rsc_ins.rcnh( 'HDS Payment Register - Details Remittance ALL',200,'X1OV12.PAYMENT_DATE <= Payment Date To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PAYMENT_DATE','','Payment Date To','','','','','EIS_AP_PAYMENT_REG_REMIT','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',200,'HDS Payment Register - Details Remittance ALL','X1OV12.PAYMENT_DATE <= Payment Date To');
--Inserting Report Sorts - HDS Payment Register - Details Remittance ALL
--Inserting Report Triggers - HDS Payment Register - Details Remittance ALL
--inserting report templates - HDS Payment Register - Details Remittance ALL
--Inserting Report Portals - HDS Payment Register - Details Remittance ALL
--inserting report dashboards - HDS Payment Register - Details Remittance ALL
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS Payment Register - Details Remittance ALL','200','EIS_AP_PAYMENT_REG_REMIT','EIS_AP_PAYMENT_REG_REMIT','N','');
--inserting report security - HDS Payment Register - Details Remittance ALL
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','XXWC_PAY_VENDOR_MSTR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_TRNS_ENTRY_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_TRNS_ENTRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_SUPPLIER_MAINT_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','XXWC_PAY_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_MGR_NOSUP_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_MGR_NOSUP_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_PYBLS_MNGR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_PYABLS_MNGR_CAN',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_MGR_NOSUP_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','XXWC_PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_INQUIRY_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_INQUIRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_INQ_CANADA',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','XXWC_PAY_DISBURSE',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_DISBUREMTS_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_DISBURSEMTS_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_DISBURSEMTS_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_PAYABLES_CLOSE_GSC_GLBL',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_PAYABLES_CLOSE_GLBL',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','XXWC_PAY_W_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','XXWC_PAY_NO_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_ADMIN_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_ADMIN_US_GSCIWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_ADMIN_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_ADMIN_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance ALL','200','','HDS_AP_CAN_GSC',200,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS Payment Register - Details Remittance ALL
--Inserting Report   Version details- HDS Payment Register - Details Remittance ALL
xxeis.eis_rsc_ins.rv( 'HDS Payment Register - Details Remittance ALL','','HDS Payment Register - Details Remittance ALL','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
