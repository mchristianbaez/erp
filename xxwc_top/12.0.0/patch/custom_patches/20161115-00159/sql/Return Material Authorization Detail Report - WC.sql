--Report Name            : Return Material Authorization Detail Report - WC
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data XXEIS_WC_OM_RETURNS_RMA_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_WC_OM_RETURNS_RMA_V
xxeis.eis_rsc_ins.v( 'XXEIS_WC_OM_RETURNS_RMA_V',660,'','','','','ANONYMOUS','XXEIS','Eis Om Returns Rma V','EORRV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_WC_OM_RETURNS_RMA_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_WC_OM_RETURNS_RMA_V',660,FALSE);
--Inserting Object Columns for XXEIS_WC_OM_RETURNS_RMA_V
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CURRENCY_CODE',660,'Currency Code','CURRENCY_CODE~CURRENCY_CODE','','','','ANONYMOUS','VARCHAR2','','','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUSTOMER_STATUS',660,'Customer Status','CUSTOMER_STATUS~CUSTOMER_STATUS','','','','ANONYMOUS','VARCHAR2','','','Customer Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','INSPECTION_REQUIRED_FLAG',660,'Inspection Required Flag','INSPECTION_REQUIRED_FLAG~INSPECTION_REQUIRED_FLAG','','','','ANONYMOUS','VARCHAR2','','','Inspection Required Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION~ITEM_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ORIGINAL_SYSTEM_LINE_REFERENCE',660,'Original System Line Reference','ORIGINAL_SYSTEM_LINE_REFERENCE~ORIGINAL_SYSTEM_LINE_REFERENCE','','','','ANONYMOUS','VARCHAR2','','','Original System Line Reference','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ORIGINAL_SYSTEM_REFERENCE',660,'Original System Reference','ORIGINAL_SYSTEM_REFERENCE~ORIGINAL_SYSTEM_REFERENCE','','','','ANONYMOUS','VARCHAR2','','','Original System Reference','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SCHEDULE_STATUS_CODE',660,'Schedule Status Code','SCHEDULE_STATUS_CODE~SCHEDULE_STATUS_CODE','','','','ANONYMOUS','VARCHAR2','','','Schedule Status Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SERVICE_COTERMINATE_FLAG',660,'Service Coterminate Flag','SERVICE_COTERMINATE_FLAG~SERVICE_COTERMINATE_FLAG','','','','ANONYMOUS','VARCHAR2','','','Service Coterminate Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SERVICE_DURATION',660,'Service Duration','SERVICE_DURATION~SERVICE_DURATION','','','','ANONYMOUS','NUMBER','','','Service Duration','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SERVICE_END_DATE',660,'Service End Date','SERVICE_END_DATE~SERVICE_END_DATE','','','','ANONYMOUS','DATE','','','Service End Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SERVICE_NUMBER',660,'Service Number','SERVICE_NUMBER~SERVICE_NUMBER','','','','ANONYMOUS','NUMBER','','','Service Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SERVICE_PERIOD',660,'Service Period','SERVICE_PERIOD~SERVICE_PERIOD','','','','ANONYMOUS','VARCHAR2','','','Service Period','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SERVICE_REFERENCE_TYPE_CODE',660,'Service Reference Type Code','SERVICE_REFERENCE_TYPE_CODE~SERVICE_REFERENCE_TYPE_CODE','','','','ANONYMOUS','VARCHAR2','','','Service Reference Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SERVICE_START_DATE',660,'Service Start Date','SERVICE_START_DATE~SERVICE_START_DATE','','','','ANONYMOUS','DATE','','','Service Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SERVICE_TXN_COMMENTS',660,'Service Txn Comments','SERVICE_TXN_COMMENTS~SERVICE_TXN_COMMENTS','','','','ANONYMOUS','VARCHAR2','','','Service Txn Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SERVICE_TXN_REASON_CODE',660,'Service Txn Reason Code','SERVICE_TXN_REASON_CODE~SERVICE_TXN_REASON_CODE','','','','ANONYMOUS','VARCHAR2','','','Service Txn Reason Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIPPABLE_FLAG',660,'Shippable Flag','SHIPPABLE_FLAG~SHIPPABLE_FLAG','','','','ANONYMOUS','VARCHAR2','','','Shippable Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIPPING_INSTRUCTIONS',660,'Shipping Instructions','SHIPPING_INSTRUCTIONS~SHIPPING_INSTRUCTIONS','','','','ANONYMOUS','VARCHAR2','','','Shipping Instructions','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIPPING_INTERFACED_FLAG',660,'Shipping Interfaced Flag','SHIPPING_INTERFACED_FLAG~SHIPPING_INTERFACED_FLAG','','','','ANONYMOUS','VARCHAR2','','','Shipping Interfaced Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_MODEL_COMPLETE_FLAG',660,'Ship Model Complete Flag','SHIP_MODEL_COMPLETE_FLAG~SHIP_MODEL_COMPLETE_FLAG','','','','ANONYMOUS','VARCHAR2','','','Ship Model Complete Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TOLERANCE_ABOVE',660,'Ship Tolerance Above','SHIP_TOLERANCE_ABOVE~SHIP_TOLERANCE_ABOVE','','','','ANONYMOUS','NUMBER','','','Ship Tolerance Above','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TOLERANCE_BELOW',660,'Ship Tolerance Below','SHIP_TOLERANCE_BELOW~SHIP_TOLERANCE_BELOW','','','','ANONYMOUS','NUMBER','','','Ship Tolerance Below','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_CITY',660,'Ship To City','SHIP_TO_CITY~SHIP_TO_CITY','','','','ANONYMOUS','VARCHAR2','','','Ship To City','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_COUNTY',660,'Ship To County','SHIP_TO_COUNTY~SHIP_TO_COUNTY','','','','ANONYMOUS','VARCHAR2','','','Ship To County','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_POSTAL_CODE',660,'Ship To Postal Code','SHIP_TO_POSTAL_CODE~SHIP_TO_POSTAL_CODE','','','','ANONYMOUS','VARCHAR2','','','Ship To Postal Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_STATE',660,'Ship To State','SHIP_TO_STATE~SHIP_TO_STATE','','','','ANONYMOUS','VARCHAR2','','','Ship To State','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','TAX_CODE',660,'Tax Code','TAX_CODE~TAX_CODE','','','','ANONYMOUS','VARCHAR2','','','Tax Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','TAX_RATE',660,'Tax Rate','TAX_RATE~TAX_RATE','','~T~D~2','','ANONYMOUS','NUMBER','','','Tax Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE~ORDERED_DATE','','','','ANONYMOUS','DATE','','','Ordered Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','RECEIPT_DATE',660,'Receipt Date','RECEIPT_DATE~RECEIPT_DATE','','','','ANONYMOUS','DATE','','','Receipt Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','RECEIVING_QUANTITY',660,'Receiving Quantity','RECEIVING_QUANTITY~RECEIVING_QUANTITY','','~T~D~2','','ANONYMOUS','NUMBER','','','Receiving Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','RECEIVING_SUBINVNETORY',660,'Receiving Subinvnetory','RECEIVING_SUBINVNETORY~RECEIVING_SUBINVNETORY','','','','ANONYMOUS','VARCHAR2','','','Receiving Subinvnetory','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','RMA_RECEIPT_NUM',660,'Rma Receipt Num','RMA_RECEIPT_NUM~RMA_RECEIPT_NUM','','','','ANONYMOUS','VARCHAR2','','','Rma Receipt Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','EXTENDED_PRICE',660,'Extended Price','EXTENDED_PRICE~EXTENDED_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Extended Price','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE~ORDER_TYPE','','','','ANONYMOUS','VARCHAR2','','','Order Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO',660,'Bill To','BILL_TO~BILL_TO','','','','ANONYMOUS','VARCHAR2','','','Bill To','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO',660,'Ship To','SHIP_TO~SHIP_TO','','','','ANONYMOUS','VARCHAR2','','','Ship To','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','WAREHOUSE',660,'Warehouse','WAREHOUSE~WAREHOUSE','','','','ANONYMOUS','VARCHAR2','','','Warehouse','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OPERATING_UNIT',660,'Operating Unit','OPERATING_UNIT','','','','ANONYMOUS','VARCHAR2','','','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OPEN_FLAG',660,'Open Flag','OPEN_FLAG','','','','ANONYMOUS','VARCHAR2','','','Open Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','TRANSACTION_TYPE',660,'Transaction Type','TRANSACTION_TYPE','','','','ANONYMOUS','VARCHAR2','','','Transaction Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','RETURN_REASON_CODE',660,'Return Reason Code','RETURN_REASON_CODE','','','','ANONYMOUS','VARCHAR2','','','Return Reason Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','FULFILLED_QUANTITY2',660,'Fulfilled Quantity2','FULFILLED_QUANTITY2','','~T~D~2','','ANONYMOUS','NUMBER','','','Fulfilled Quantity2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIPPING_QUANTITY2',660,'Shipping Quantity2','SHIPPING_QUANTITY2','','~T~D~2','','ANONYMOUS','NUMBER','','','Shipping Quantity2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CANCELLED_QUANTITY2',660,'Cancelled Quantity2','CANCELLED_QUANTITY2','','~T~D~2','','ANONYMOUS','NUMBER','','','Cancelled Quantity2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIPPED_QUANTITY2',660,'Shipped Quantity2','SHIPPED_QUANTITY2','','~T~D~2','','ANONYMOUS','NUMBER','','','Shipped Quantity2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ORDERED_QUANTITY_UOM2',660,'Ordered Quantity Uom2','ORDERED_QUANTITY_UOM2','','','','ANONYMOUS','VARCHAR2','','','Ordered Quantity Uom2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ORDERED_QUANTITY2',660,'Ordered Quantity2','ORDERED_QUANTITY2','','~T~D~2','','ANONYMOUS','NUMBER','','','Ordered Quantity2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','PREFERRED_GRADE',660,'Preferred Grade','PREFERRED_GRADE','','','','ANONYMOUS','VARCHAR2','','','Preferred Grade','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','FULFILLMENT_DATE',660,'Fulfillment Date','FULFILLMENT_DATE','','','','ANONYMOUS','DATE','','','Fulfillment Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','REVENUE_AMOUNT',660,'Revenue Amount','REVENUE_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','','','Revenue Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','FLOW_STATUS_CODE',660,'Flow Status Code','FLOW_STATUS_CODE','','','','ANONYMOUS','VARCHAR2','','','Flow Status Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','INVOICED_QUANTITY',660,'Invoiced Quantity','INVOICED_QUANTITY','','~T~D~2','','ANONYMOUS','NUMBER','','','Invoiced Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_ADDRESS4',660,'Bill To Address4','BILL_TO_ADDRESS4','','','','ANONYMOUS','VARCHAR2','','','Bill To Address4','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_ADDRESS3',660,'Bill To Address3','BILL_TO_ADDRESS3','','','','ANONYMOUS','VARCHAR2','','','Bill To Address3','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_ADDRESS2',660,'Bill To Address2','BILL_TO_ADDRESS2','','','','ANONYMOUS','VARCHAR2','','','Bill To Address2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_ADDRESS1',660,'Bill To Address1','BILL_TO_ADDRESS1','','','','ANONYMOUS','VARCHAR2','','','Bill To Address1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_ADDRESS4',660,'Ship To Address4','SHIP_TO_ADDRESS4','','','','ANONYMOUS','VARCHAR2','','','Ship To Address4','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_ADDRESS3',660,'Ship To Address3','SHIP_TO_ADDRESS3','','','','ANONYMOUS','VARCHAR2','','','Ship To Address3','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_ADDRESS2',660,'Ship To Address2','SHIP_TO_ADDRESS2','','','','ANONYMOUS','VARCHAR2','','','Ship To Address2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_ADDRESS1',660,'Ship To Address1','SHIP_TO_ADDRESS1','','','','ANONYMOUS','VARCHAR2','','','Ship To Address1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SUBINVENTORY',660,'Subinventory','SUBINVENTORY','','','','ANONYMOUS','VARCHAR2','','','Subinventory','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Customer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','PRICE_LIST_NAME',660,'Price List Name','PRICE_LIST_NAME','','','','ANONYMOUS','VARCHAR2','','','Price List Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','LATEST_ACCEPTABLE_DATE',660,'Latest Acceptable Date','LATEST_ACCEPTABLE_DATE','','','','ANONYMOUS','DATE','','','Latest Acceptable Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','EARLIEST_ACCEPTABLE_DATE',660,'Earliest Acceptable Date','EARLIEST_ACCEPTABLE_DATE','','','','ANONYMOUS','DATE','','','Earliest Acceptable Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ACTUAL_SHIPMENT_DATE',660,'Actual Shipment Date','ACTUAL_SHIPMENT_DATE','','','','ANONYMOUS','DATE','','','Actual Shipment Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ACTUAL_ARRIVAL_DATE',660,'Actual Arrival Date','ACTUAL_ARRIVAL_DATE','','','','ANONYMOUS','DATE','','','Actual Arrival Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','DEP_PLAN_REQUIRED_FLAG',660,'Dep Plan Required Flag','DEP_PLAN_REQUIRED_FLAG','','','','ANONYMOUS','VARCHAR2','','','Dep Plan Required Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OPTION_FLAG',660,'Option Flag','OPTION_FLAG','','','','ANONYMOUS','VARCHAR2','','','Option Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','TAX_VALUE',660,'Tax Value','TAX_VALUE','','~T~D~2','','ANONYMOUS','NUMBER','','','Tax Value','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','UNIT_LIST_PRICE',660,'Unit List Price','UNIT_LIST_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Unit List Price','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','UNIT_SELLING_PRICE',660,'Unit Selling Price','UNIT_SELLING_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Unit Selling Price','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','LINE_CATEGORY_CODE',660,'Line Category Code','LINE_CATEGORY_CODE','','','','ANONYMOUS','VARCHAR2','','','Line Category Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ITEM_REVISION',660,'Item Revision','ITEM_REVISION','','','','ANONYMOUS','VARCHAR2','','','Item Revision','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','REFERENCE_TYPE',660,'Reference Type','REFERENCE_TYPE','','','','ANONYMOUS','VARCHAR2','','','Reference Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','TAX_POINT_CODE',660,'Tax Point Code','TAX_POINT_CODE','','','','ANONYMOUS','VARCHAR2','','','Tax Point Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','FOB_POINT_CODE',660,'Fob Point Code','FOB_POINT_CODE','','','','ANONYMOUS','VARCHAR2','','','Fob Point Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','FREIGHT_CARRIER_CODE',660,'Freight Carrier Code','FREIGHT_CARRIER_CODE','','','','ANONYMOUS','VARCHAR2','','','Freight Carrier Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','FREIGHT_TERMS_CODE',660,'Freight Terms Code','FREIGHT_TERMS_CODE','','','','ANONYMOUS','VARCHAR2','','','Freight Terms Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIPPING_METHOD_CODE',660,'Shipping Method Code','SHIPPING_METHOD_CODE','','','','ANONYMOUS','VARCHAR2','','','Shipping Method Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIPMENT_NUMBER',660,'Shipment Number','SHIPMENT_NUMBER','','','','ANONYMOUS','NUMBER','','','Shipment Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SOURCE_TYPE_CODE',660,'Source Type Code','SOURCE_TYPE_CODE','','','','ANONYMOUS','VARCHAR2','','','Source Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIPPING_QUANTITY_UOM',660,'Shipping Quantity Uom','SHIPPING_QUANTITY_UOM','','','','ANONYMOUS','VARCHAR2','','','Shipping Quantity Uom','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIPPING_QUANTITY',660,'Shipping Quantity','SHIPPING_QUANTITY','','~T~D~2','','ANONYMOUS','NUMBER','','','Shipping Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','FULFILLED_QUANTITY',660,'Fulfilled Quantity','FULFILLED_QUANTITY','','~T~D~2','','ANONYMOUS','NUMBER','','','Fulfilled Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ORDERED_QUANTITY',660,'Ordered Quantity','ORDERED_QUANTITY','','~T~D~2','','ANONYMOUS','NUMBER','','','Ordered Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIPPED_QUANTITY',660,'Shipped Quantity','SHIPPED_QUANTITY','','~T~D~2','','ANONYMOUS','NUMBER','','','Shipped Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CANCELLED_QUANTITY',660,'Cancelled Quantity','CANCELLED_QUANTITY','','~T~D~2','','ANONYMOUS','NUMBER','','','Cancelled Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ORDER_QUANTITY_UOM',660,'Order Quantity Uom','ORDER_QUANTITY_UOM','','','','ANONYMOUS','VARCHAR2','','','Order Quantity Uom','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ORDERED_ITEM',660,'Ordered Item','ORDERED_ITEM','','','','ANONYMOUS','VARCHAR2','','','Ordered Item','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','ANONYMOUS','NUMBER','','','Order Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ACCEPTED_BY',660,'Accepted By','ACCEPTED_BY~ACCEPTED_BY','','','','ANONYMOUS','NUMBER','','','Accepted By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ACCEPTED_QUANTITY',660,'Accepted Quantity','ACCEPTED_QUANTITY~ACCEPTED_QUANTITY','','~T~D~2','','ANONYMOUS','NUMBER','','','Accepted Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ACTUAL_FULFILLMENT_DATE',660,'Actual Fulfillment Date','ACTUAL_FULFILLMENT_DATE~ACTUAL_FULFILLMENT_DATE','','','','ANONYMOUS','DATE','','','Actual Fulfillment Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','AGREEMENT_NAME',660,'Agreement Name','AGREEMENT_NAME~AGREEMENT_NAME','','','','ANONYMOUS','VARCHAR2','','','Agreement Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_CITY',660,'Bill To City','BILL_TO_CITY~BILL_TO_CITY','','','','ANONYMOUS','VARCHAR2','','','Bill To City','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_COUNTY',660,'Bill To County','BILL_TO_COUNTY~BILL_TO_COUNTY','','','','ANONYMOUS','VARCHAR2','','','Bill To County','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_POSTAL_CODE',660,'Bill To Postal Code','BILL_TO_POSTAL_CODE~BILL_TO_POSTAL_CODE','','','','ANONYMOUS','VARCHAR2','','','Bill To Postal Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_STATE',660,'Bill To State','BILL_TO_STATE~BILL_TO_STATE','','','','ANONYMOUS','VARCHAR2','','','Bill To State','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BOOKED_FLAG',660,'Booked Flag','BOOKED_FLAG~BOOKED_FLAG','','','','ANONYMOUS','VARCHAR2','','','Booked Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CALCULATE_PRICE_FLAG',660,'Calculate Price Flag','CALCULATE_PRICE_FLAG~CALCULATE_PRICE_FLAG','','','','ANONYMOUS','VARCHAR2','','','Calculate Price Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CANCELLED_FLAG',660,'Cancelled Flag','CANCELLED_FLAG~CANCELLED_FLAG','','','','ANONYMOUS','VARCHAR2','','','Cancelled Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CHANGE_SEQUENCE',660,'Change Sequence','CHANGE_SEQUENCE~CHANGE_SEQUENCE','','','','ANONYMOUS','VARCHAR2','','','Change Sequence','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CHARGE_PERIODICITY_CODE',660,'Charge Periodicity Code','CHARGE_PERIODICITY_CODE~CHARGE_PERIODICITY_CODE','','','','ANONYMOUS','VARCHAR2','','','Charge Periodicity Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','COMPONENT_NUMBER',660,'Component Number','COMPONENT_NUMBER~COMPONENT_NUMBER','','','','ANONYMOUS','NUMBER','','','Component Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CONCATENATED_SEGMENTS',660,'Concatenated Segments','CONCATENATED_SEGMENTS~CONCATENATED_SEGMENTS','','','','ANONYMOUS','VARCHAR2','','','Concatenated Segments','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CREATION_DATE',660,'Creation Date','CREATION_DATE~CREATION_DATE','','','','ANONYMOUS','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUSTOMER',660,'Customer','CUSTOMER~CUSTOMER','','','','ANONYMOUS','VARCHAR2','','','Customer','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUSTOMER_DOCK_CODE',660,'Customer Dock Code','CUSTOMER_DOCK_CODE~CUSTOMER_DOCK_CODE','','','','ANONYMOUS','VARCHAR2','','','Customer Dock Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUSTOMER_ITEM_NET_PRICE',660,'Customer Item Net Price','CUSTOMER_ITEM_NET_PRICE~CUSTOMER_ITEM_NET_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Customer Item Net Price','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUSTOMER_JOB',660,'Customer Job','CUSTOMER_JOB~CUSTOMER_JOB','','','','ANONYMOUS','VARCHAR2','','','Customer Job','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUSTOMER_LINE_NUMBER',660,'Customer Line Number','CUSTOMER_LINE_NUMBER~CUSTOMER_LINE_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Customer Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUSTOMER_PRODUCTION_LINE',660,'Customer Production Line','CUSTOMER_PRODUCTION_LINE~CUSTOMER_PRODUCTION_LINE','','','','ANONYMOUS','VARCHAR2','','','Customer Production Line','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUSTOMER_SHIPMENT_NUMBER',660,'Customer Shipment Number','CUSTOMER_SHIPMENT_NUMBER~CUSTOMER_SHIPMENT_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Customer Shipment Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUST_MODEL_SERIAL_NUMBER',660,'Cust Model Serial Number','CUST_MODEL_SERIAL_NUMBER~CUST_MODEL_SERIAL_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Cust Model Serial Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUST_PO_NUMBER',660,'Cust Po Number','CUST_PO_NUMBER~CUST_PO_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Cust Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUST_PRODUCTION_SEQ_NUM',660,'Cust Production Seq Num','CUST_PRODUCTION_SEQ_NUM~CUST_PRODUCTION_SEQ_NUM','','','','ANONYMOUS','VARCHAR2','','','Cust Production Seq Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','DEMAND_BUCKET_TYPE_CODE',660,'Demand Bucket Type Code','DEMAND_BUCKET_TYPE_CODE~DEMAND_BUCKET_TYPE_CODE','','','','ANONYMOUS','VARCHAR2','','','Demand Bucket Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','DEMAND_CLASS_CODE',660,'Demand Class Code','DEMAND_CLASS_CODE~DEMAND_CLASS_CODE','','','','ANONYMOUS','VARCHAR2','','','Demand Class Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','END_ITEM_UNIT_NUMBER',660,'End Item Unit Number','END_ITEM_UNIT_NUMBER~END_ITEM_UNIT_NUMBER','','','','ANONYMOUS','VARCHAR2','','','End Item Unit Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','EXPLOSION_DATE',660,'Explosion Date','EXPLOSION_DATE~EXPLOSION_DATE','','','','ANONYMOUS','DATE','','','Explosion Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','FIRST_ACK_CODE',660,'First Ack Code','FIRST_ACK_CODE~FIRST_ACK_CODE','','','','ANONYMOUS','VARCHAR2','','','First Ack Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','FIRST_ACK_DATE',660,'First Ack Date','FIRST_ACK_DATE~FIRST_ACK_DATE','','','','ANONYMOUS','DATE','','','First Ack Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','FULFILLED_FLAG',660,'Fulfilled Flag','FULFILLED_FLAG~FULFILLED_FLAG','','','','ANONYMOUS','VARCHAR2','','','Fulfilled Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','FULFILLMENT_METHOD_CODE',660,'Fulfillment Method Code','FULFILLMENT_METHOD_CODE~FULFILLMENT_METHOD_CODE','','','','ANONYMOUS','VARCHAR2','','','Fulfillment Method Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','INVOICE_INTERFACE_STATUS_CODE',660,'Invoice Interface Status Code','INVOICE_INTERFACE_STATUS_CODE~INVOICE_INTERFACE_STATUS_CODE','','','','ANONYMOUS','VARCHAR2','','','Invoice Interface Status Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ITEM_IDENTIFIER_TYPE',660,'Item Identifier Type','ITEM_IDENTIFIER_TYPE~ITEM_IDENTIFIER_TYPE','','','','ANONYMOUS','VARCHAR2','','','Item Identifier Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ITEM_SUBSTITUTION_TYPE_CODE',660,'Item Substitution Type Code','ITEM_SUBSTITUTION_TYPE_CODE~ITEM_SUBSTITUTION_TYPE_CODE','','','','ANONYMOUS','VARCHAR2','','','Item Substitution Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ITEM_TYPE_CODE',660,'Item Type Code','ITEM_TYPE_CODE~ITEM_TYPE_CODE','','','','ANONYMOUS','VARCHAR2','','','Item Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','LAST_ACK_CODE',660,'Last Ack Code','LAST_ACK_CODE~LAST_ACK_CODE','','','','ANONYMOUS','VARCHAR2','','','Last Ack Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','LAST_ACK_DATE',660,'Last Ack Date','LAST_ACK_DATE~LAST_ACK_DATE','','','','ANONYMOUS','DATE','','','Last Ack Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','LATE_DEMAND_PENALTY_FACTOR',660,'Late Demand Penalty Factor','LATE_DEMAND_PENALTY_FACTOR~LATE_DEMAND_PENALTY_FACTOR','','','','ANONYMOUS','NUMBER','','','Late Demand Penalty Factor','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER~LINE_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MFG_LEAD_TIME',660,'Mfg Lead Time','MFG_LEAD_TIME~MFG_LEAD_TIME','','','','ANONYMOUS','NUMBER','','','Mfg Lead Time','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OPTION_NUMBER',660,'Option Number','OPTION_NUMBER~OPTION_NUMBER','','','','ANONYMOUS','NUMBER','','','Option Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ORDER_FIRMED_DATE',660,'Order Firmed Date','ORDER_FIRMED_DATE~ORDER_FIRMED_DATE','','','','ANONYMOUS','DATE','','','Order Firmed Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ORIGINAL_ITEM_IDENTIFIER_TYPE',660,'Original Item Identifier Type','ORIGINAL_ITEM_IDENTIFIER_TYPE~ORIGINAL_ITEM_IDENTIFIER_TYPE','','','','ANONYMOUS','VARCHAR2','','','Original Item Identifier Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ORIGINAL_LIST_PRICE',660,'Original List Price','ORIGINAL_LIST_PRICE~ORIGINAL_LIST_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Original List Price','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ORIGINAL_ORDERED_ITEM',660,'Original Ordered Item','ORIGINAL_ORDERED_ITEM~ORIGINAL_ORDERED_ITEM','','','','ANONYMOUS','VARCHAR2','','','Original Ordered Item','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ORIG_SYS_SHIPMENT_REF',660,'Orig Sys Shipment Ref','ORIG_SYS_SHIPMENT_REF~ORIG_SYS_SHIPMENT_REF','','','','ANONYMOUS','VARCHAR2','','','Orig Sys Shipment Ref','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OVERRIDE_ATP_DATE_CODE',660,'Override Atp Date Code','OVERRIDE_ATP_DATE_CODE~OVERRIDE_ATP_DATE_CODE','','','','ANONYMOUS','VARCHAR2','','','Override Atp Date Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OVER_SHIP_REASON_CODE',660,'Over Ship Reason Code','OVER_SHIP_REASON_CODE~OVER_SHIP_REASON_CODE','','','','ANONYMOUS','VARCHAR2','','','Over Ship Reason Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OVER_SHIP_RESOLVED_FLAG',660,'Over Ship Resolved Flag','OVER_SHIP_RESOLVED_FLAG~OVER_SHIP_RESOLVED_FLAG','','','','ANONYMOUS','VARCHAR2','','','Over Ship Resolved Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','PACKING_INSTRUCTIONS',660,'Packing Instructions','PACKING_INSTRUCTIONS~PACKING_INSTRUCTIONS','','','','ANONYMOUS','VARCHAR2','','','Packing Instructions','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','PLANNING_PRIORITY',660,'Planning Priority','PLANNING_PRIORITY~PLANNING_PRIORITY','','','','ANONYMOUS','NUMBER','','','Planning Priority','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','PRICE_REQUEST_CODE',660,'Price Request Code','PRICE_REQUEST_CODE~PRICE_REQUEST_CODE','','','','ANONYMOUS','VARCHAR2','','','Price Request Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','PRICING_DATE',660,'Pricing Date','PRICING_DATE~PRICING_DATE','','','','ANONYMOUS','DATE','','','Pricing Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','PRICING_QUANTITY',660,'Pricing Quantity','PRICING_QUANTITY~PRICING_QUANTITY','','~T~D~2','','ANONYMOUS','NUMBER','','','Pricing Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','PRICING_QUANTITY_UOM',660,'Pricing Quantity Uom','PRICING_QUANTITY_UOM~PRICING_QUANTITY_UOM','','','','ANONYMOUS','VARCHAR2','','','Pricing Quantity Uom','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','PROMISE_DATE',660,'Promise Date','PROMISE_DATE~PROMISE_DATE','','','','ANONYMOUS','DATE','','','Promise Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','REQUEST_DATE',660,'Request Date','REQUEST_DATE~REQUEST_DATE','','','','ANONYMOUS','DATE','','','Request Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','RETURN_ATTRIBUTE1',660,'Return Attribute1','RETURN_ATTRIBUTE1~RETURN_ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','','','Return Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','RETURN_ATTRIBUTE2',660,'Return Attribute2','RETURN_ATTRIBUTE2~RETURN_ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','','','Return Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','RETURN_CONTEXT',660,'Return Context','RETURN_CONTEXT~RETURN_CONTEXT','','','','ANONYMOUS','VARCHAR2','','','Return Context','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','REVREC_COMMENTS',660,'Revrec Comments','REVREC_COMMENTS~REVREC_COMMENTS','','','','ANONYMOUS','VARCHAR2','','','Revrec Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','REVREC_IMPLICIT_FLAG',660,'Revrec Implicit Flag','REVREC_IMPLICIT_FLAG~REVREC_IMPLICIT_FLAG','','','','ANONYMOUS','VARCHAR2','','','Revrec Implicit Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','REVREC_REFERENCE_DOCUMENT',660,'Revrec Reference Document','REVREC_REFERENCE_DOCUMENT~REVREC_REFERENCE_DOCUMENT','','','','ANONYMOUS','VARCHAR2','','','Revrec Reference Document','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','REVREC_SIGNATURE',660,'Revrec Signature','REVREC_SIGNATURE~REVREC_SIGNATURE','','','','ANONYMOUS','VARCHAR2','','','Revrec Signature','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','REVREC_SIGNATURE_DATE',660,'Revrec Signature Date','REVREC_SIGNATURE_DATE~REVREC_SIGNATURE_DATE','','','','ANONYMOUS','DATE','','','Revrec Signature Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','RE_SOURCE_FLAG',660,'Re Source Flag','RE_SOURCE_FLAG~RE_SOURCE_FLAG','','','','ANONYMOUS','VARCHAR2','','','Re Source Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','RLA_SCHEDULE_TYPE_CODE',660,'Rla Schedule Type Code','RLA_SCHEDULE_TYPE_CODE~RLA_SCHEDULE_TYPE_CODE','','','','ANONYMOUS','VARCHAR2','','','Rla Schedule Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SALESREP_NAME',660,'Salesrep Name','SALESREP_NAME~SALESREP_NAME','','','','ANONYMOUS','VARCHAR2','','','Salesrep Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SCHEDULE_ARRIVAL_DATE',660,'Schedule Arrival Date','SCHEDULE_ARRIVAL_DATE~SCHEDULE_ARRIVAL_DATE','','','','ANONYMOUS','DATE','','','Schedule Arrival Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SCHEDULE_SHIP_DATE',660,'Schedule Ship Date','SCHEDULE_SHIP_DATE~SCHEDULE_SHIP_DATE','','','','ANONYMOUS','DATE','','','Schedule Ship Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_SET_ID',660,'Ship Set Id','SHIP_SET_ID~SHIP_SET_ID','','','','ANONYMOUS','NUMBER','','','Ship Set Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_CONTACT_ID',660,'Ship To Contact Id','SHIP_TO_CONTACT_ID~SHIP_TO_CONTACT_ID','','','','ANONYMOUS','NUMBER','','','Ship To Contact Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SOLD_FROM_ORG_ID',660,'Sold From Org Id','SOLD_FROM_ORG_ID~SOLD_FROM_ORG_ID','','','','ANONYMOUS','NUMBER','','','Sold From Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SORT_ORDER',660,'Sort Order','SORT_ORDER~SORT_ORDER','','','','ANONYMOUS','VARCHAR2','','','Sort Order','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SPLIT_BY',660,'Split By','SPLIT_BY~SPLIT_BY','','','','ANONYMOUS','VARCHAR2','','','Split By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SPLIT_FROM_LINE_ID',660,'Split From Line Id','SPLIT_FROM_LINE_ID~SPLIT_FROM_LINE_ID','','','','ANONYMOUS','NUMBER','','','Split From Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','TASK_ID',660,'Task Id','TASK_ID~TASK_ID','','','','ANONYMOUS','NUMBER','','','Task Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','TAX_DATE',660,'Tax Date','TAX_DATE~TAX_DATE','','','','ANONYMOUS','DATE','','','Tax Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','TAX_EXEMPT_FLAG',660,'Tax Exempt Flag','TAX_EXEMPT_FLAG~TAX_EXEMPT_FLAG','','','','ANONYMOUS','VARCHAR2','','','Tax Exempt Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','TAX_EXEMPT_NUMBER',660,'Tax Exempt Number','TAX_EXEMPT_NUMBER~TAX_EXEMPT_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Tax Exempt Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','TAX_EXEMPT_REASON_CODE',660,'Tax Exempt Reason Code','TAX_EXEMPT_REASON_CODE~TAX_EXEMPT_REASON_CODE','','','','ANONYMOUS','VARCHAR2','','','Tax Exempt Reason Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','TRANSACTION_PHASE_CODE',660,'Transaction Phase Code','TRANSACTION_PHASE_CODE~TRANSACTION_PHASE_CODE','','','','ANONYMOUS','VARCHAR2','','','Transaction Phase Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','TRANSACTION_TYPE_CODE',660,'Transaction Type Code','TRANSACTION_TYPE_CODE~TRANSACTION_TYPE_CODE','','','','ANONYMOUS','VARCHAR2','','','Transaction Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','UNIT_LIST_PERCENT',660,'Unit List Percent','UNIT_LIST_PERCENT~UNIT_LIST_PERCENT','','~T~D~2','','ANONYMOUS','NUMBER','','','Unit List Percent','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','UNIT_PERCENT_BASE_PRICE',660,'Unit Percent Base Price','UNIT_PERCENT_BASE_PRICE~UNIT_PERCENT_BASE_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Unit Percent Base Price','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','UNIT_SELLING_PERCENT',660,'Unit Selling Percent','UNIT_SELLING_PERCENT~UNIT_SELLING_PERCENT','','~T~D~2','','ANONYMOUS','NUMBER','','','Unit Selling Percent','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','VEH_CUS_ITEM_CUM_KEY_ID',660,'Veh Cus Item Cum Key Id','VEH_CUS_ITEM_CUM_KEY_ID~VEH_CUS_ITEM_CUM_KEY_ID','','','','ANONYMOUS','NUMBER','','','Veh Cus Item Cum Key Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIPMENT_HEADER_ID',660,'Shipment Header Id','SHIPMENT_HEADER_ID~SHIPMENT_HEADER_ID','','','','ANONYMOUS','NUMBER','','','Shipment Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','TRANSACTION_ID',660,'Transaction Id','TRANSACTION_ID~TRANSACTION_ID','','','','ANONYMOUS','NUMBER','','','Transaction Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','UOM_CODE',660,'Uom Code','UOM_CODE~UOM_CODE','','','','ANONYMOUS','VARCHAR2','','','Uom Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIPPING_ORG_ID',660,'Shipping Org Id','SHIPPING_ORG_ID~SHIPPING_ORG_ID','','','','ANONYMOUS','NUMBER','','','Shipping Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','ANONYMOUS','NUMBER','','','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','UNIT_SELLING_PRICE_PER_PQTY',660,'Unit Selling Price Per Pqty','UNIT_SELLING_PRICE_PER_PQTY','','~T~D~2','','ANONYMOUS','NUMBER','','','Unit Selling Price Per Pqty','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','UPGRADED_FLAG',660,'Upgraded Flag','UPGRADED_FLAG','','','','ANONYMOUS','VARCHAR2','','','Upgraded Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','VISIBLE_DEMAND_FLAG',660,'Visible Demand Flag','VISIBLE_DEMAND_FLAG','','','','ANONYMOUS','VARCHAR2','','','Visible Demand Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','REFERENCE_CUSTOMER_TRX_LINE_ID',660,'Reference Customer Trx Line Id','REFERENCE_CUSTOMER_TRX_LINE_ID','','','','ANONYMOUS','NUMBER','','','Reference Customer Trx Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','REFERENCE_LINE_ID',660,'Reference Line Id','REFERENCE_LINE_ID','','','','ANONYMOUS','NUMBER','','','Reference Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SOURCE_DOCUMENT_LINE_ID',660,'Source Document Line Id','SOURCE_DOCUMENT_LINE_ID','','','','ANONYMOUS','NUMBER','','','Source Document Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SOURCE_DOCUMENT_ID',660,'Source Document Id','SOURCE_DOCUMENT_ID','','','','ANONYMOUS','NUMBER','','','Source Document Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SOURCE_DOCUMENT_TYPE_ID',660,'Source Document Type Id','SOURCE_DOCUMENT_TYPE_ID','','','','ANONYMOUS','NUMBER','','','Source Document Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ACCOUNTING_RULE_DURATION',660,'Accounting Rule Duration','ACCOUNTING_RULE_DURATION','','','','ANONYMOUS','NUMBER','','','Accounting Rule Duration','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIPMENT_PRIORITY_CODE',660,'Shipment Priority Code','SHIPMENT_PRIORITY_CODE','','','','ANONYMOUS','VARCHAR2','','','Shipment Priority Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','AGREEMENT_ID',660,'Agreement Id','AGREEMENT_ID','','','','ANONYMOUS','NUMBER','','','Agreement Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','DELIVER_TO_ORG_ID',660,'Deliver To Org Id','DELIVER_TO_ORG_ID','','','','ANONYMOUS','NUMBER','','','Deliver To Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_ORG_ID',660,'Ship To Org Id','SHIP_TO_ORG_ID','','','','ANONYMOUS','NUMBER','','','Ship To Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_FROM_ORG_ID',660,'Ship From Org Id','SHIP_FROM_ORG_ID','','','','ANONYMOUS','NUMBER','','','Ship From Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SOLD_TO_ORG_ID',660,'Sold To Org Id','SOLD_TO_ORG_ID','','','','ANONYMOUS','NUMBER','','','Sold To Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OPERATING_UNIT_ID',660,'Operating Unit Id','OPERATING_UNIT_ID','','~T~D~2','','ANONYMOUS','NUMBER','','','Operating Unit Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','TRANSACTION_TYPE_ID',660,'Transaction Type Id','TRANSACTION_TYPE_ID','','','','ANONYMOUS','NUMBER','','','Transaction Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CREATED_BY',660,'Created By','CREATED_BY~CREATED_BY','','','','ANONYMOUS','VARCHAR2','','','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','LAST_UPDATED_BY',660,'Last Updated By','LAST_UPDATED_BY~LAST_UPDATED_BY','','','','ANONYMOUS','VARCHAR2','','','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ACCOUNTING_RULE_ID',660,'Accounting Rule Id','ACCOUNTING_RULE_ID~ACCOUNTING_RULE_ID','','','','ANONYMOUS','NUMBER','','','Accounting Rule Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ARRIVAL_SET_ID',660,'Arrival Set Id','ARRIVAL_SET_ID~ARRIVAL_SET_ID','','','','ANONYMOUS','NUMBER','','','Arrival Set Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ATO_LINE_ID',660,'Ato Line Id','ATO_LINE_ID~ATO_LINE_ID','','','','ANONYMOUS','NUMBER','','','Ato Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','AUTHORIZED_TO_SHIP_FLAG',660,'Authorized To Ship Flag','AUTHORIZED_TO_SHIP_FLAG~AUTHORIZED_TO_SHIP_FLAG','','','','ANONYMOUS','VARCHAR2','','','Authorized To Ship Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','AUTO_SELECTED_QUANTITY',660,'Auto Selected Quantity','AUTO_SELECTED_QUANTITY~AUTO_SELECTED_QUANTITY','','~T~D~2','','ANONYMOUS','NUMBER','','','Auto Selected Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','COMMITMENT_ID',660,'Commitment Id','COMMITMENT_ID~COMMITMENT_ID','','','','ANONYMOUS','NUMBER','','','Commitment Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CREDIT_INVOICE_LINE_ID',660,'Credit Invoice Line Id','CREDIT_INVOICE_LINE_ID~CREDIT_INVOICE_LINE_ID','','','','ANONYMOUS','NUMBER','','','Credit Invoice Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUSTOMER_PAYMENT_TERM_ID',660,'Customer Payment Term Id','CUSTOMER_PAYMENT_TERM_ID~CUSTOMER_PAYMENT_TERM_ID','','','','ANONYMOUS','NUMBER','','','Customer Payment Term Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID~CUST_ACCOUNT_ID','','','','ANONYMOUS','NUMBER','','','Cust Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','DELIVERY_LEAD_TIME',660,'Delivery Lead Time','DELIVERY_LEAD_TIME~DELIVERY_LEAD_TIME','','','','ANONYMOUS','NUMBER','','','Delivery Lead Time','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','DELIVER_TO_CONTACT_ID',660,'Deliver To Contact Id','DELIVER_TO_CONTACT_ID~DELIVER_TO_CONTACT_ID','','','','ANONYMOUS','NUMBER','','','Deliver To Contact Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','HEADER_ID',660,'Header Id','HEADER_ID~HEADER_ID','','','','ANONYMOUS','NUMBER','','','Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','INTMED_SHIP_TO_CONTACT_ID',660,'Intmed Ship To Contact Id','INTMED_SHIP_TO_CONTACT_ID~INTMED_SHIP_TO_CONTACT_ID','','','','ANONYMOUS','NUMBER','','','Intmed Ship To Contact Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','INTMED_SHIP_TO_ORG_ID',660,'Intmed Ship To Org Id','INTMED_SHIP_TO_ORG_ID~INTMED_SHIP_TO_ORG_ID','','','','ANONYMOUS','NUMBER','','','Intmed Ship To Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','INVOICE_TO_CONTACT_ID',660,'Invoice To Contact Id','INVOICE_TO_CONTACT_ID~INVOICE_TO_CONTACT_ID','','','','ANONYMOUS','NUMBER','','','Invoice To Contact Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','INVOICE_TO_ORG_ID',660,'Invoice To Org Id','INVOICE_TO_ORG_ID~INVOICE_TO_ORG_ID','','','','ANONYMOUS','NUMBER','','','Invoice To Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','INVOICING_RULE_ID',660,'Invoicing Rule Id','INVOICING_RULE_ID~INVOICING_RULE_ID','','','','ANONYMOUS','NUMBER','','','Invoicing Rule Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','LAST_UPDATE_DATE',660,'Last Update Date','LAST_UPDATE_DATE~LAST_UPDATE_DATE','','','','ANONYMOUS','DATE','','','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','LINE_ID',660,'Line Id','LINE_ID~LINE_ID','','','','ANONYMOUS','NUMBER','','','Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','LINE_SET_ID',660,'Line Set Id','LINE_SET_ID~LINE_SET_ID','','','','ANONYMOUS','NUMBER','','','Line Set Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','LINE_TYPE_ID',660,'Line Type Id','LINE_TYPE_ID~LINE_TYPE_ID','','','','ANONYMOUS','NUMBER','','','Line Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','LIST_HEADER_ID',660,'List Header Id','LIST_HEADER_ID~LIST_HEADER_ID','','','','ANONYMOUS','NUMBER','','','List Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MARKETING_SOURCE_CODE_ID',660,'Marketing Source Code Id','MARKETING_SOURCE_CODE_ID~MARKETING_SOURCE_CODE_ID','','','','ANONYMOUS','NUMBER','','','Marketing Source Code Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MODEL_REMNANT_FLAG',660,'Model Remnant Flag','MODEL_REMNANT_FLAG~MODEL_REMNANT_FLAG','','','','ANONYMOUS','VARCHAR2','','','Model Remnant Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ORG_ID',660,'Org Id','ORG_ID~ORG_ID','','','','ANONYMOUS','NUMBER','','','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ORDER_SOURCE_ID',660,'Order Source Id','ORDER_SOURCE_ID~ORDER_SOURCE_ID','','','','ANONYMOUS','NUMBER','','','Order Source Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ORIGINAL_INVENTORY_ITEM_ID',660,'Original Inventory Item Id','ORIGINAL_INVENTORY_ITEM_ID~ORIGINAL_INVENTORY_ITEM_ID','','','','ANONYMOUS','NUMBER','','','Original Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','ORIGINAL_ORDERED_ITEM_ID',660,'Original Ordered Item Id','ORIGINAL_ORDERED_ITEM_ID~ORIGINAL_ORDERED_ITEM_ID','','','','ANONYMOUS','NUMBER','','','Original Ordered Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','PARTY_ID',660,'Party Id','PARTY_ID~PARTY_ID','','','','ANONYMOUS','NUMBER','','','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','PAYMENT_TERM_ID',660,'Payment Term Id','PAYMENT_TERM_ID~PAYMENT_TERM_ID','','','','ANONYMOUS','NUMBER','','','Payment Term Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','PRICE_LIST_ID',660,'Price List Id','PRICE_LIST_ID~PRICE_LIST_ID','','~T~D~2','','ANONYMOUS','NUMBER','','','Price List Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','PROJECT_ID',660,'Project Id','PROJECT_ID~PROJECT_ID','','','','ANONYMOUS','NUMBER','','','Project Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SALESREP_ID',660,'Salesrep Id','SALESREP_ID~SALESREP_ID','','','','ANONYMOUS','NUMBER','','','Salesrep Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','RS_SALESREP_ID',660,'Rs Salesrep Id','RS_SALESREP_ID','','','','ANONYMOUS','NUMBER','','','Rs Salesrep Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','RS_ORG_ID',660,'Rs Org Id','RS_ORG_ID','','','','ANONYMOUS','NUMBER','','','Rs Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','UPD_BY_USER_ID',660,'Upd By User Id','UPD_BY_USER_ID','','','','ANONYMOUS','NUMBER','','','Upd By User Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CREA_BY_USER_ID',660,'Crea By User Id','CREA_BY_USER_ID','','','','ANONYMOUS','NUMBER','','','Crea By User Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI_ORGANIZATION_ID',660,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Msi Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCSU_SITE_USE_ID',660,'Ship To Hcsu Site Use Id','SHIP_TO_HCSU_SITE_USE_ID','','','','ANONYMOUS','NUMBER','','','Ship To Hcsu Site Use Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCAS_SITE_ID',660,'Ship To Hcas Site Id','SHIP_TO_HCAS_SITE_ID','','','','ANONYMOUS','NUMBER','','','Ship To Hcas Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HPS_PRTY_STE_ID',660,'Ship To Hps Prty Ste Id','SHIP_TO_HPS_PRTY_STE_ID','','','','ANONYMOUS','NUMBER','','','Ship To Hps Prty Ste Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HL_LOCN_ID',660,'Ship To Hl Locn Id','SHIP_TO_HL_LOCN_ID','','','','ANONYMOUS','NUMBER','','','Ship To Hl Locn Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCSU_SITE_USE_ID',660,'Bill To Hcsu Site Use Id','BILL_TO_HCSU_SITE_USE_ID','','','','ANONYMOUS','NUMBER','','','Bill To Hcsu Site Use Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCAS_CUST_ID',660,'Bill To Hcas Cust Id','BILL_TO_HCAS_CUST_ID','','','','ANONYMOUS','NUMBER','','','Bill To Hcas Cust Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HPS_PRTY_STE_ID',660,'Bill To Hps Prty Ste Id','BILL_TO_HPS_PRTY_STE_ID','','','','ANONYMOUS','NUMBER','','','Bill To Hps Prty Ste Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HL_LOCN_ID',660,'Bill To Hl Locn Id','BILL_TO_HL_LOCN_ID','','','','ANONYMOUS','NUMBER','','','Bill To Hl Locn Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCAS#JOB_INFORMATION',660,'Bill To Hcas#Job Information','BILL_TO_HCAS#JOB_INFORMATION','','','','ANONYMOUS','VARCHAR2','','','Bill To Hcas#Job Information','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCAS#LIEN_RELEASE_DA',660,'Bill To Hcas#Lien Release Da','BILL_TO_HCAS#LIEN_RELEASE_DA','','','','ANONYMOUS','VARCHAR2','','','Bill To Hcas#Lien Release Da','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCAS#MANDATORY_PO_NU',660,'Bill To Hcas#Mandatory Po Nu','BILL_TO_HCAS#MANDATORY_PO_NU','','','','ANONYMOUS','VARCHAR2','','','Bill To Hcas#Mandatory Po Nu','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCAS#PRINT_PRICES_ON',660,'Bill To Hcas#Print Prices On','BILL_TO_HCAS#PRINT_PRICES_ON','','','','ANONYMOUS','VARCHAR2','','','Bill To Hcas#Print Prices On','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCAS#PRISM_NUMBER',660,'Bill To Hcas#Prism Number','BILL_TO_HCAS#PRISM_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Bill To Hcas#Prism Number','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCAS#TAX_EXEMPT1',660,'Bill To Hcas#Tax Exempt1','BILL_TO_HCAS#TAX_EXEMPT1','','','','ANONYMOUS','VARCHAR2','','','Bill To Hcas#Tax Exempt1','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCAS#TAX_EXEMPTION_T',660,'Bill To Hcas#Tax Exemption T','BILL_TO_HCAS#TAX_EXEMPTION_T','','','','ANONYMOUS','VARCHAR2','','','Bill To Hcas#Tax Exemption T','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCAS#YES#NOTICE_TO_O',660,'Bill To Hcas#Yes#Notice To O','BILL_TO_HCAS#YES#NOTICE_TO_O','','','','ANONYMOUS','VARCHAR2','','','Bill To Hcas#Yes#Notice To O','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCAS#YES#NOTICE_TO_O1',660,'Bill To Hcas#Yes#Notice To O1','BILL_TO_HCAS#YES#NOTICE_TO_O1','','','','ANONYMOUS','VARCHAR2','','','Bill To Hcas#Yes#Notice To O1','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCAS#YES#NOTICE_TO_O2',660,'Bill To Hcas#Yes#Notice To O2','BILL_TO_HCAS#YES#NOTICE_TO_O2','','','','ANONYMOUS','VARCHAR2','','','Bill To Hcas#Yes#Notice To O2','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCSU#CUSTOMER_SITE_C',660,'Bill To Hcsu#Customer Site C','BILL_TO_HCSU#CUSTOMER_SITE_C','','','','ANONYMOUS','VARCHAR2','','','Bill To Hcsu#Customer Site C','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCSU#DODGE_NUMBER',660,'Bill To Hcsu#Dodge Number','BILL_TO_HCSU#DODGE_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Bill To Hcsu#Dodge Number','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCSU#FUTURE_USE',660,'Bill To Hcsu#Future Use','BILL_TO_HCSU#FUTURE_USE','','','','ANONYMOUS','VARCHAR2','','','Bill To Hcsu#Future Use','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCSU#GOVERNMENT_FUND',660,'Bill To Hcsu#Government Fund','BILL_TO_HCSU#GOVERNMENT_FUND','','','','ANONYMOUS','VARCHAR2','','','Bill To Hcsu#Government Fund','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCSU#PERMIT_NUMBER',660,'Bill To Hcsu#Permit Number','BILL_TO_HCSU#PERMIT_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Bill To Hcsu#Permit Number','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCSU#SALESREP_#2',660,'Bill To Hcsu#Salesrep #2','BILL_TO_HCSU#SALESREP_#2','','','','ANONYMOUS','VARCHAR2','','','Bill To Hcsu#Salesrep #2','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCSU#SALESREP_SPILT_',660,'Bill To Hcsu#Salesrep Spilt ','BILL_TO_HCSU#SALESREP_SPILT_','','','','ANONYMOUS','VARCHAR2','','','Bill To Hcsu#Salesrep Spilt ','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCSU#SALESREP_SPILT_1',660,'Bill To Hcsu#Salesrep Spilt 1','BILL_TO_HCSU#SALESREP_SPILT_1','','','','ANONYMOUS','VARCHAR2','','','Bill To Hcsu#Salesrep Spilt 1','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HCSU#THOMAS_GUIDE_PA',660,'Bill To Hcsu#Thomas Guide Pa','BILL_TO_HCSU#THOMAS_GUIDE_PA','','','','ANONYMOUS','VARCHAR2','','','Bill To Hcsu#Thomas Guide Pa','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HL#LOB',660,'Bill To Hl#Lob','BILL_TO_HL#LOB','','','','ANONYMOUS','VARCHAR2','','','Bill To Hl#Lob','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HL#PAY_TO_VENDOR_COD',660,'Bill To Hl#Pay To Vendor Cod','BILL_TO_HL#PAY_TO_VENDOR_COD','','','','ANONYMOUS','VARCHAR2','','','Bill To Hl#Pay To Vendor Cod','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HL#PAY_TO_VENDOR_ID',660,'Bill To Hl#Pay To Vendor Id','BILL_TO_HL#PAY_TO_VENDOR_ID','','','','ANONYMOUS','VARCHAR2','','','Bill To Hl#Pay To Vendor Id','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HPS#REBT_LOB',660,'Bill To Hps#Rebt Lob','BILL_TO_HPS#REBT_LOB','','','','ANONYMOUS','VARCHAR2','','','Bill To Hps#Rebt Lob','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HPS#REBT_PAY_TO_VNDR',660,'Bill To Hps#Rebt Pay To Vndr','BILL_TO_HPS#REBT_PAY_TO_VNDR','','','','ANONYMOUS','VARCHAR2','','','Bill To Hps#Rebt Pay To Vndr','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HPS#REBT_VNDR_CODE',660,'Bill To Hps#Rebt Vndr Code','BILL_TO_HPS#REBT_VNDR_CODE','','','','ANONYMOUS','VARCHAR2','','','Bill To Hps#Rebt Vndr Code','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HPS#REBT_VNDR_FLAG',660,'Bill To Hps#Rebt Vndr Flag','BILL_TO_HPS#REBT_VNDR_FLAG','','','','ANONYMOUS','VARCHAR2','','','Bill To Hps#Rebt Vndr Flag','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','BILL_TO_HPS#REBT_VNDR_TAX_ID',660,'Bill To Hps#Rebt Vndr Tax Id','BILL_TO_HPS#REBT_VNDR_TAX_ID','','','','ANONYMOUS','VARCHAR2','','','Bill To Hps#Rebt Vndr Tax Id','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUST_ACCT#BRANCH_DESCRIPTION',660,'Cust Acct#Branch Description','CUST_ACCT#BRANCH_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Cust Acct#Branch Description','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUST_ACCT#CUSTOMER_SOURCE',660,'Cust Acct#Customer Source','CUST_ACCT#CUSTOMER_SOURCE','','','','ANONYMOUS','VARCHAR2','','','Cust Acct#Customer Source','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUST_ACCT#LEGAL_COLLECTION_I',660,'Cust Acct#Legal Collection I','CUST_ACCT#LEGAL_COLLECTION_I','','','','ANONYMOUS','VARCHAR2','','','Cust Acct#Legal Collection I','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUST_ACCT#PARTY_TYPE',660,'Cust Acct#Party Type','CUST_ACCT#PARTY_TYPE','','','','ANONYMOUS','VARCHAR2','','','Cust Acct#Party Type','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUST_ACCT#PRISM_NUMBER',660,'Cust Acct#Prism Number','CUST_ACCT#PRISM_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Cust Acct#Prism Number','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUST_ACCT#VNDR_CODE_AND_FRUL',660,'Cust Acct#Vndr Code And Frul','CUST_ACCT#VNDR_CODE_AND_FRUL','','','','ANONYMOUS','VARCHAR2','','','Cust Acct#Vndr Code And Frul','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUST_ACCT#YES#NOTE_LINE_#1',660,'Cust Acct#Yes#Note Line #1','CUST_ACCT#YES#NOTE_LINE_#1','','','','ANONYMOUS','VARCHAR2','','','Cust Acct#Yes#Note Line #1','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUST_ACCT#YES#NOTE_LINE_#2',660,'Cust Acct#Yes#Note Line #2','CUST_ACCT#YES#NOTE_LINE_#2','','','','ANONYMOUS','VARCHAR2','','','Cust Acct#Yes#Note Line #2','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUST_ACCT#YES#NOTE_LINE_#3',660,'Cust Acct#Yes#Note Line #3','CUST_ACCT#YES#NOTE_LINE_#3','','','','ANONYMOUS','VARCHAR2','','','Cust Acct#Yes#Note Line #3','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUST_ACCT#YES#NOTE_LINE_#4',660,'Cust Acct#Yes#Note Line #4','CUST_ACCT#YES#NOTE_LINE_#4','','','','ANONYMOUS','VARCHAR2','','','Cust Acct#Yes#Note Line #4','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','CUST_ACCT#YES#NOTE_LINE_#5',660,'Cust Acct#Yes#Note Line #5','CUST_ACCT#YES#NOTE_LINE_#5','','','','ANONYMOUS','VARCHAR2','','','Cust Acct#Yes#Note Line #5','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#HDS#DROP_SHIPMENT_ELIGAB',660,'Msi#Hds#Drop Shipment Eligab','MSI#HDS#DROP_SHIPMENT_ELIGAB','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Drop Shipment Eligab','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#HDS#INVOICE_UOM',660,'Msi#Hds#Invoice Uom','MSI#HDS#INVOICE_UOM','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Invoice Uom','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#HDS#LOB',660,'Msi#Hds#Lob','MSI#HDS#LOB','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Lob','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#HDS#PRODUCT_ID',660,'Msi#Hds#Product Id','MSI#HDS#PRODUCT_ID','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Product Id','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#HDS#SKU_DESCRIPTION',660,'Msi#Hds#Sku Description','MSI#HDS#SKU_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Sku Description','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#HDS#UNSPSC_CODE',660,'Msi#Hds#Unspsc Code','MSI#HDS#UNSPSC_CODE','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Unspsc Code','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#HDS#UPC_PRIMARY',660,'Msi#Hds#Upc Primary','MSI#HDS#UPC_PRIMARY','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Upc Primary','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#HDS#VENDOR_PART_NUMBER',660,'Msi#Hds#Vendor Part Number','MSI#HDS#VENDOR_PART_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Msi#Hds#Vendor Part Number','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#AMU',660,'Msi#Wc#Amu','MSI#WC#AMU','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Amu','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#AVERAGE_UNITS',660,'Msi#Wc#Average Units','MSI#WC#AVERAGE_UNITS','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Average Units','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#CALC_LEAD_TIME',660,'Msi#Wc#Calc Lead Time','MSI#WC#CALC_LEAD_TIME','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Calc Lead Time','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#CA_PROP_65',660,'Msi#Wc#Ca Prop 65','MSI#WC#CA_PROP_65','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Ca Prop 65','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#COUNTRY_OF_ORIGIN',660,'Msi#Wc#Country Of Origin','MSI#WC#COUNTRY_OF_ORIGIN','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Country Of Origin','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#DC_VELOCITY',660,'Msi#Wc#Dc Velocity','MSI#WC#DC_VELOCITY','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Dc Velocity','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#GTP_INDICATOR',660,'Msi#Wc#Gtp Indicator','MSI#WC#GTP_INDICATOR','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Gtp Indicator','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#HAZMAT_CONTAINER',660,'Msi#Wc#Hazmat Container','MSI#WC#HAZMAT_CONTAINER','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Hazmat Container','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#HAZMAT_DESCRIPTION',660,'Msi#Wc#Hazmat Description','MSI#WC#HAZMAT_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Hazmat Description','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#HAZMAT_PACKAGING_GROU',660,'Msi#Wc#Hazmat Packaging Grou','MSI#WC#HAZMAT_PACKAGING_GROU','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Hazmat Packaging Grou','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#IMPORT_DUTY_',660,'Msi#Wc#Import Duty ','MSI#WC#IMPORT_DUTY_','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Import Duty ','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#KEEP_ITEM_ACTIVE',660,'Msi#Wc#Keep Item Active','MSI#WC#KEEP_ITEM_ACTIVE','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Keep Item Active','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#LAST_LEAD_TIME',660,'Msi#Wc#Last Lead Time','MSI#WC#LAST_LEAD_TIME','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Last Lead Time','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#MSDS_#',660,'Msi#Wc#Msds #','MSI#WC#MSDS_#','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Msds #','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#ORM_D_FLAG',660,'Msi#Wc#Orm D Flag','MSI#WC#ORM_D_FLAG','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Orm D Flag','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#PESTICIDE_FLAG',660,'Msi#Wc#Pesticide Flag','MSI#WC#PESTICIDE_FLAG','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Pesticide Flag','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#PESTICIDE_FLAG_STATE',660,'Msi#Wc#Pesticide Flag State','MSI#WC#PESTICIDE_FLAG_STATE','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Pesticide Flag State','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#PRISM_PART_NUMBER',660,'Msi#Wc#Prism Part Number','MSI#WC#PRISM_PART_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Prism Part Number','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#PRODUCT_CODE',660,'Msi#Wc#Product Code','MSI#WC#PRODUCT_CODE','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Product Code','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#RESERVE_STOCK',660,'Msi#Wc#Reserve Stock','MSI#WC#RESERVE_STOCK','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Reserve Stock','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#STORE_VELOCITY',660,'Msi#Wc#Store Velocity','MSI#WC#STORE_VELOCITY','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Store Velocity','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#TAXWARE_CODE',660,'Msi#Wc#Taxware Code','MSI#WC#TAXWARE_CODE','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Taxware Code','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#VOC_CATEGORY',660,'Msi#Wc#Voc Category','MSI#WC#VOC_CATEGORY','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Voc Category','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#VOC_GL',660,'Msi#Wc#Voc Gl','MSI#WC#VOC_GL','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Voc Gl','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#VOC_SUB_CATEGORY',660,'Msi#Wc#Voc Sub Category','MSI#WC#VOC_SUB_CATEGORY','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Voc Sub Category','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#YEARLY_DC_VELOCITY',660,'Msi#Wc#Yearly Dc Velocity','MSI#WC#YEARLY_DC_VELOCITY','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Yearly Dc Velocity','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MSI#WC#YEARLY_STORE_VELOCITY',660,'Msi#Wc#Yearly Store Velocity','MSI#WC#YEARLY_STORE_VELOCITY','','','','ANONYMOUS','VARCHAR2','','','Msi#Wc#Yearly Store Velocity','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MTP#BRANCH_OPERATIONS_MANAGE',660,'Mtp#Branch Operations Manage','MTP#BRANCH_OPERATIONS_MANAGE','','','','ANONYMOUS','VARCHAR2','','','Mtp#Branch Operations Manage','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MTP#DELIVER_CHARGE',660,'Mtp#Deliver Charge','MTP#DELIVER_CHARGE','','','','ANONYMOUS','VARCHAR2','','','Mtp#Deliver Charge','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MTP#DISTRICT',660,'Mtp#District','MTP#DISTRICT','','','','ANONYMOUS','VARCHAR2','','','Mtp#District','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MTP#FACTORY_PLANNER_DATA_DIR',660,'Mtp#Factory Planner Data Dir','MTP#FACTORY_PLANNER_DATA_DIR','','','','ANONYMOUS','VARCHAR2','','','Mtp#Factory Planner Data Dir','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MTP#FACTORY_PLANNER_EXECUTAB',660,'Mtp#Factory Planner Executab','MTP#FACTORY_PLANNER_EXECUTAB','','','','ANONYMOUS','VARCHAR2','','','Mtp#Factory Planner Executab','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MTP#FACTORY_PLANNER_HOST',660,'Mtp#Factory Planner Host','MTP#FACTORY_PLANNER_HOST','','','','ANONYMOUS','VARCHAR2','','','Mtp#Factory Planner Host','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MTP#FACTORY_PLANNER_PORT_NUM',660,'Mtp#Factory Planner Port Num','MTP#FACTORY_PLANNER_PORT_NUM','','','','ANONYMOUS','VARCHAR2','','','Mtp#Factory Planner Port Num','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MTP#FACTORY_PLANNER_USER',660,'Mtp#Factory Planner User','MTP#FACTORY_PLANNER_USER','','','','ANONYMOUS','VARCHAR2','','','Mtp#Factory Planner User','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MTP#FRU',660,'Mtp#Fru','MTP#FRU','','','','ANONYMOUS','VARCHAR2','','','Mtp#Fru','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MTP#LOCATION_NUMBER',660,'Mtp#Location Number','MTP#LOCATION_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Mtp#Location Number','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MTP#ORG_TYPE',660,'Mtp#Org Type','MTP#ORG_TYPE','','','','ANONYMOUS','VARCHAR2','','','Mtp#Org Type','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MTP#PRICING_ZONE',660,'Mtp#Pricing Zone','MTP#PRICING_ZONE','','','','ANONYMOUS','VARCHAR2','','','Mtp#Pricing Zone','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','MTP#REGION',660,'Mtp#Region','MTP#REGION','','','','ANONYMOUS','VARCHAR2','','','Mtp#Region','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OH#DELIVERED_BY',660,'Oh#Delivered By','OH#DELIVERED_BY','','','','ANONYMOUS','VARCHAR2','','','Oh#Delivered By','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OH#LOAD_CHECKED_BY',660,'Oh#Load Checked By','OH#LOAD_CHECKED_BY','','','','ANONYMOUS','VARCHAR2','','','Oh#Load Checked By','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OH#ORDER_CHANNEL',660,'Oh#Order Channel','OH#ORDER_CHANNEL','','','','ANONYMOUS','VARCHAR2','','','Oh#Order Channel','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OH#PICKED_BY',660,'Oh#Picked By','OH#PICKED_BY','','','','ANONYMOUS','VARCHAR2','','','Oh#Picked By','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OH#RENTAL_TERM',660,'Oh#Rental Term','OH#RENTAL_TERM','','','','ANONYMOUS','VARCHAR2','','','Oh#Rental Term','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OL#APPLICATION_METHOD',660,'Ol#Application Method','OL#APPLICATION_METHOD','','','','ANONYMOUS','VARCHAR2','','','Ol#Application Method','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OL#ENGINEERING_COST',660,'Ol#Engineering Cost','OL#ENGINEERING_COST','','','','ANONYMOUS','VARCHAR2','','','Ol#Engineering Cost','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OL#ESTIMATED_RETURN_DATE',660,'Ol#Estimated Return Date','OL#ESTIMATED_RETURN_DATE','','','','ANONYMOUS','VARCHAR2','','','Ol#Estimated Return Date','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OL#FORCE_SHIP',660,'Ol#Force Ship','OL#FORCE_SHIP','','','','ANONYMOUS','VARCHAR2','','','Ol#Force Ship','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OL#ITEM_ON_BLOWOUT',660,'Ol#Item On Blowout','OL#ITEM_ON_BLOWOUT','','','','ANONYMOUS','VARCHAR2','','','Ol#Item On Blowout','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OL#POF_STD_LINE',660,'Ol#Pof Std Line','OL#POF_STD_LINE','','','','ANONYMOUS','VARCHAR2','','','Ol#Pof Std Line','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OL#PO_COST_FOR_VENDOR_QUOTE',660,'Ol#Po Cost For Vendor Quote','OL#PO_COST_FOR_VENDOR_QUOTE','','','','ANONYMOUS','VARCHAR2','','','Ol#Po Cost For Vendor Quote','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OL#PRICING_GUARDRAIL',660,'Ol#Pricing Guardrail','OL#PRICING_GUARDRAIL','','','','ANONYMOUS','VARCHAR2','','','Ol#Pricing Guardrail','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OL#PRINT_EXPIRED_PRODUCT_DIS',660,'Ol#Print Expired Product Dis','OL#PRINT_EXPIRED_PRODUCT_DIS','','','','ANONYMOUS','VARCHAR2','','','Ol#Print Expired Product Dis','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OL#RENTAL_CHARGE',660,'Ol#Rental Charge','OL#RENTAL_CHARGE','','','','ANONYMOUS','VARCHAR2','','','Ol#Rental Charge','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OL#RERENTAL_BILLING_TERMS',660,'Ol#Rerental Billing Terms','OL#RERENTAL_BILLING_TERMS','','','','ANONYMOUS','VARCHAR2','','','Ol#Rerental Billing Terms','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OL#RERENT_PO',660,'Ol#Rerent Po','OL#RERENT_PO','','','','ANONYMOUS','VARCHAR2','','','Ol#Rerent Po','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OL#SERIAL_NUMBER',660,'Ol#Serial Number','OL#SERIAL_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Ol#Serial Number','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','OL#VENDOR_QUOTE_COST',660,'Ol#Vendor Quote Cost','OL#VENDOR_QUOTE_COST','','','','ANONYMOUS','VARCHAR2','','','Ol#Vendor Quote Cost','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','PARTY#GVID_ID',660,'Party#Gvid Id','PARTY#GVID_ID','','','','ANONYMOUS','VARCHAR2','','','Party#Gvid Id','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','PARTY#PARTY_TYPE',660,'Party#Party Type','PARTY#PARTY_TYPE','','','','ANONYMOUS','VARCHAR2','','','Party#Party Type','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','QLHVL#AUTO_RENEWAL',660,'Qlhvl#Auto Renewal','QLHVL#AUTO_RENEWAL','','','','ANONYMOUS','VARCHAR2','','','Qlhvl#Auto Renewal','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','QLHVL#CONTRACT_NAME',660,'Qlhvl#Contract Name','QLHVL#CONTRACT_NAME','','','','ANONYMOUS','VARCHAR2','','','Qlhvl#Contract Name','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','QLHVL#UNTIL_YEAR',660,'Qlhvl#Until Year','QLHVL#UNTIL_YEAR','','','','ANONYMOUS','VARCHAR2','','','Qlhvl#Until Year','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_HEAD#PRINTED',660,'Ship Head#Printed','SHIP_HEAD#PRINTED','','','','ANONYMOUS','VARCHAR2','','','Ship Head#Printed','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCAS#JOB_INFORMATION',660,'Ship To Hcas#Job Information','SHIP_TO_HCAS#JOB_INFORMATION','','','','ANONYMOUS','VARCHAR2','','','Ship To Hcas#Job Information','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCAS#LIEN_RELEASE_DA',660,'Ship To Hcas#Lien Release Da','SHIP_TO_HCAS#LIEN_RELEASE_DA','','','','ANONYMOUS','VARCHAR2','','','Ship To Hcas#Lien Release Da','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCAS#MANDATORY_PO_NU',660,'Ship To Hcas#Mandatory Po Nu','SHIP_TO_HCAS#MANDATORY_PO_NU','','','','ANONYMOUS','VARCHAR2','','','Ship To Hcas#Mandatory Po Nu','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCAS#PRINT_PRICES_ON',660,'Ship To Hcas#Print Prices On','SHIP_TO_HCAS#PRINT_PRICES_ON','','','','ANONYMOUS','VARCHAR2','','','Ship To Hcas#Print Prices On','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCAS#PRISM_NUMBER',660,'Ship To Hcas#Prism Number','SHIP_TO_HCAS#PRISM_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Ship To Hcas#Prism Number','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCAS#TAX_EXEMPT1',660,'Ship To Hcas#Tax Exempt1','SHIP_TO_HCAS#TAX_EXEMPT1','','','','ANONYMOUS','VARCHAR2','','','Ship To Hcas#Tax Exempt1','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCAS#TAX_EXEMPTION_T',660,'Ship To Hcas#Tax Exemption T','SHIP_TO_HCAS#TAX_EXEMPTION_T','','','','ANONYMOUS','VARCHAR2','','','Ship To Hcas#Tax Exemption T','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCAS#YES#NOTICE_TO_O',660,'Ship To Hcas#Yes#Notice To O','SHIP_TO_HCAS#YES#NOTICE_TO_O','','','','ANONYMOUS','VARCHAR2','','','Ship To Hcas#Yes#Notice To O','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCAS#YES#NOTICE_TO_O1',660,'Ship To Hcas#Yes#Notice To O1','SHIP_TO_HCAS#YES#NOTICE_TO_O1','','','','ANONYMOUS','VARCHAR2','','','Ship To Hcas#Yes#Notice To O1','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCAS#YES#NOTICE_TO_O2',660,'Ship To Hcas#Yes#Notice To O2','SHIP_TO_HCAS#YES#NOTICE_TO_O2','','','','ANONYMOUS','VARCHAR2','','','Ship To Hcas#Yes#Notice To O2','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCSU#CUSTOMER_SITE_C',660,'Ship To Hcsu#Customer Site C','SHIP_TO_HCSU#CUSTOMER_SITE_C','','','','ANONYMOUS','VARCHAR2','','','Ship To Hcsu#Customer Site C','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCSU#DODGE_NUMBER',660,'Ship To Hcsu#Dodge Number','SHIP_TO_HCSU#DODGE_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Ship To Hcsu#Dodge Number','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCSU#FUTURE_USE',660,'Ship To Hcsu#Future Use','SHIP_TO_HCSU#FUTURE_USE','','','','ANONYMOUS','VARCHAR2','','','Ship To Hcsu#Future Use','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCSU#GOVERNMENT_FUND',660,'Ship To Hcsu#Government Fund','SHIP_TO_HCSU#GOVERNMENT_FUND','','','','ANONYMOUS','VARCHAR2','','','Ship To Hcsu#Government Fund','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCSU#PERMIT_NUMBER',660,'Ship To Hcsu#Permit Number','SHIP_TO_HCSU#PERMIT_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Ship To Hcsu#Permit Number','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCSU#SALESREP_#2',660,'Ship To Hcsu#Salesrep #2','SHIP_TO_HCSU#SALESREP_#2','','','','ANONYMOUS','VARCHAR2','','','Ship To Hcsu#Salesrep #2','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCSU#SALESREP_SPILT_',660,'Ship To Hcsu#Salesrep Spilt ','SHIP_TO_HCSU#SALESREP_SPILT_','','','','ANONYMOUS','VARCHAR2','','','Ship To Hcsu#Salesrep Spilt ','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCSU#SALESREP_SPILT_1',660,'Ship To Hcsu#Salesrep Spilt 1','SHIP_TO_HCSU#SALESREP_SPILT_1','','','','ANONYMOUS','VARCHAR2','','','Ship To Hcsu#Salesrep Spilt 1','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HCSU#THOMAS_GUIDE_PA',660,'Ship To Hcsu#Thomas Guide Pa','SHIP_TO_HCSU#THOMAS_GUIDE_PA','','','','ANONYMOUS','VARCHAR2','','','Ship To Hcsu#Thomas Guide Pa','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HL#LOB',660,'Ship To Hl#Lob','SHIP_TO_HL#LOB','','','','ANONYMOUS','VARCHAR2','','','Ship To Hl#Lob','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HL#PAY_TO_VENDOR_COD',660,'Ship To Hl#Pay To Vendor Cod','SHIP_TO_HL#PAY_TO_VENDOR_COD','','','','ANONYMOUS','VARCHAR2','','','Ship To Hl#Pay To Vendor Cod','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HL#PAY_TO_VENDOR_ID',660,'Ship To Hl#Pay To Vendor Id','SHIP_TO_HL#PAY_TO_VENDOR_ID','','','','ANONYMOUS','VARCHAR2','','','Ship To Hl#Pay To Vendor Id','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HPS#REBT_LOB',660,'Ship To Hps#Rebt Lob','SHIP_TO_HPS#REBT_LOB','','','','ANONYMOUS','VARCHAR2','','','Ship To Hps#Rebt Lob','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HPS#REBT_PAY_TO_VNDR',660,'Ship To Hps#Rebt Pay To Vndr','SHIP_TO_HPS#REBT_PAY_TO_VNDR','','','','ANONYMOUS','VARCHAR2','','','Ship To Hps#Rebt Pay To Vndr','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HPS#REBT_VNDR_CODE',660,'Ship To Hps#Rebt Vndr Code','SHIP_TO_HPS#REBT_VNDR_CODE','','','','ANONYMOUS','VARCHAR2','','','Ship To Hps#Rebt Vndr Code','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HPS#REBT_VNDR_FLAG',660,'Ship To Hps#Rebt Vndr Flag','SHIP_TO_HPS#REBT_VNDR_FLAG','','','','ANONYMOUS','VARCHAR2','','','Ship To Hps#Rebt Vndr Flag','','','','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_RETURNS_RMA_V','SHIP_TO_HPS#REBT_VNDR_TAX_ID',660,'Ship To Hps#Rebt Vndr Tax Id','SHIP_TO_HPS#REBT_VNDR_TAX_ID','','','','ANONYMOUS','VARCHAR2','','','Ship To Hps#Rebt Vndr Tax Id','','','','');
--Inserting Object Components for XXEIS_WC_OM_RETURNS_RMA_V
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','OE_AGREEMENTS',660,'OE_AGREEMENTS_B','OEA','OEA','ANONYMOUS','ANONYMOUS','-1','OE_AGREEMENTS','','','','','OA','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','RA_SALESREPS',660,'JTF_RS_SALESREPS','RS','RS','ANONYMOUS','ANONYMOUS','-1','RA_SALESREPS','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_PARTIES',660,'HZ_PARTIES','PARTY','PARTY','ANONYMOUS','ANONYMOUS','-1','Information About Parties Such As Organizations, P','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_CUST_ACCOUNTS',660,'HZ_CUST_ACCOUNTS','CUST_ACCT','CUST_ACCT','ANONYMOUS','ANONYMOUS','-1','Stores Information About Customer Accounts.','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','OE_ORDER_HEADERS',660,'OE_ORDER_HEADERS_ALL','OH','OH','ANONYMOUS','ANONYMOUS','-1','Oe Order Headers All Stores Header Information For','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','ANONYMOUS','ANONYMOUS','-1','Oe Order Lines All Stores Information For All Orde','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','QP_LIST_HEADERS_VL',660,'QP_LIST_HEADERS_B','QLHVL','QLHVL','ANONYMOUS','ANONYMOUS','-1','Qp List Headers Tl Stores The Translatable Columns','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','MTP','MTP','ANONYMOUS','ANONYMOUS','-1','Inventory Control Options And Defaults','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','MTL_SYSTEM_ITEMS',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','ANONYMOUS','ANONYMOUS','-1','Inventory Item Definitions','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','HR_ORGANIZATION_UNITS',660,'HR_ALL_ORGANIZATION_UNITS','HOU','HOU','ANONYMOUS','ANONYMOUS','-1','HR_ALL_ORGANIZATION_UNITS','N','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_CUST_SITE_USES',660,'HZ_CUST_SITE_USES_ALL','SHIP_TO_HCSU','SHIP_TO_HCSU','ANONYMOUS','ANONYMOUS','-1','Stores Business Purposes Assigned To Customer Acco','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_CUST_ACCT_SITES',660,'HZ_CUST_ACCT_SITES_ALL','SHIP_TO_HCAS','SHIP_TO_HCAS','ANONYMOUS','ANONYMOUS','-1','Stores All Customer Account Sites Across All Opera','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_PARTY_SITES',660,'HZ_PARTY_SITES','SHIP_TO_HPS','SHIP_TO_HPS','ANONYMOUS','ANONYMOUS','-1','Links Party To Physical Locations','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_LOCATIONS',660,'HZ_LOCATIONS','SHIP_TO_HL','SHIP_TO_HL','ANONYMOUS','ANONYMOUS','-1','Physical Addresses','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_CUST_SITE_USES',660,'HZ_CUST_SITE_USES_ALL','BILL_TO_HCSU','BILL_TO_HCSU','ANONYMOUS','ANONYMOUS','-1','Stores Business Purposes Assigned To Customer Acco','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_CUST_ACCT_SITES',660,'HZ_CUST_ACCT_SITES_ALL','BILL_TO_HCAS','BILL_TO_HCAS','ANONYMOUS','ANONYMOUS','-1','Stores All Customer Account Sites Across All Opera','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_PARTY_SITES',660,'HZ_PARTY_SITES','BILL_TO_HPS','BILL_TO_HPS','ANONYMOUS','ANONYMOUS','-1','Links Party To Physical Locations','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_LOCATIONS',660,'HZ_LOCATIONS','BILL_TO_HL','BILL_TO_HL','ANONYMOUS','ANONYMOUS','-1','Physical Addresses','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','RCV_TRANSACTIONS',660,'RCV_TRANSACTIONS','RCV','RCV','ANONYMOUS','ANONYMOUS','-1','Receiving Transactions','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','RCV_SHIPMENT_HEADERS',660,'RCV_SHIPMENT_HEADERS','SHIP_HEAD','SHIP_HEAD','ANONYMOUS','ANONYMOUS','-1','Shipment And Receipt Header Information','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','FND_USER',660,'FND_USER','CREA_BY','CREA_BY','ANONYMOUS','ANONYMOUS','-1','Application Users','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXEIS_WC_OM_RETURNS_RMA_V','FND_USER',660,'FND_USER','UPD_BY','UPD_BY','ANONYMOUS','ANONYMOUS','-1','Application Users','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for XXEIS_WC_OM_RETURNS_RMA_V
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','OE_AGREEMENTS','OEA',660,'EORRV.agreement_id','=','oea.agreement_id(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','RA_SALESREPS','RS',660,'EORRV.RS_ORG_ID','=','RS.ORG_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','RA_SALESREPS','RS',660,'EORRV.RS_SALESREP_ID','=','RS.SALESREP_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_PARTIES','PARTY',660,'EORRV.PARTY_ID','=','PARTY.PARTY_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_CUST_ACCOUNTS','CUST_ACCT',660,'EORRV.CUST_ACCOUNT_ID','=','CUST_ACCT.CUST_ACCOUNT_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','OE_ORDER_HEADERS','OH',660,'EORRV.HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','OE_ORDER_LINES','OL',660,'EORRV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','QP_LIST_HEADERS_VL','QLHVL',660,'EORRV.LIST_HEADER_ID','=','QLHVL.LIST_HEADER_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','MTL_PARAMETERS','MTP',660,'EORRV.shipping_org_id','=','MTP.ORGANIZATION_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','MTL_SYSTEM_ITEMS','MSI',660,'EORRV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','MTL_SYSTEM_ITEMS','MSI',660,'EORRV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','HR_ORGANIZATION_UNITS','HOU',660,'EORRV.operating_unit_id','=','HOU.ORGANIZATION_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_CUST_SITE_USES','SHIP_TO_HCSU',660,'EORRV.ship_to_hcsu_site_use_id','=','SHIP_TO_HCSU.SITE_USE_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_CUST_ACCT_SITES','SHIP_TO_HCAS',660,'EORRV.ship_to_hcas_site_id','=','SHIP_TO_HCAS.CUST_ACCT_SITE_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_PARTY_SITES','SHIP_TO_HPS',660,'EORRV.ship_to_hps_prty_ste_id','=','SHIP_TO_HPS.PARTY_SITE_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_LOCATIONS','SHIP_TO_HL',660,'EORRV.SHIP_TO_HL_LOCN_ID','=','SHIP_TO_HL.LOCATION_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_CUST_SITE_USES','BILL_TO_HCSU',660,'EORRV.BILL_TO_HCSU_SITE_USE_ID','=','BILL_TO_HCSU.SITE_USE_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_CUST_ACCT_SITES','BILL_TO_HCAS',660,'EORRV.BILL_TO_HCAS_CUST_ID','=','BILL_TO_HCAS.CUST_ACCT_SITE_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_PARTY_SITES','BILL_TO_HPS',660,'EORRV.BILL_TO_HPS_PRTY_STE_ID','=','BILL_TO_HPS.PARTY_SITE_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','HZ_LOCATIONS','BILL_TO_HL',660,'EORRV.BILL_TO_HL_LOCN_ID','=','BILL_TO_HL.LOCATION_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','RCV_TRANSACTIONS','RCV',660,'EORRV.TRANSACTION_ID','=','RCV.TRANSACTION_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','RCV_SHIPMENT_HEADERS','SHIP_HEAD',660,'EORRV.SHIPMENT_HEADER_ID','=','SHIP_HEAD.SHIPMENT_HEADER_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','FND_USER','CREA_BY',660,'EORRV.CREA_BY_USER_ID','=','CREA_BY.USER_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'XXEIS_WC_OM_RETURNS_RMA_V','FND_USER','UPD_BY',660,'EORRV.UPD_BY_USER_ID','=','UPD_BY.USER_ID(+)','','','','Y','ANONYMOUS');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Return Material Authorization Detail Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Return Material Authorization Detail Report - WC
xxeis.eis_rsc_ins.lov( 660,' SELECT OH.ORDER_NUMBER      ORDER_NUMBER,
        NVL(CUST.ACCOUNT_NUMBER, PARTY.PARTY_NUMBER)     CUSTOMER_NUMBER,
        NVL(CUST.ACCOUNT_NAME, PARTY.PARTY_NAME)     CUSTOMER_NAME,
        OH.FLOW_STATUS_CODE  STATUS,
        OH.ORDERED_DATE      ORDERED_DATE
FROM   OE_ORDER_HEADERS OH,
       HZ_PARTIES        PARTY,
       HZ_CUST_ACCOUNTS  CUST
WHERE PARTY.PARTY_ID=CUST.PARTY_ID
AND   CUST.CUST_ACCOUNT_ID =OH.SOLD_TO_ORG_ID','','OM ORDER NUMBER','This gives the Order Number','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct ott.name order_type,ott.description description,ott.transaction_type_id order_type_id from oe_transaction_types_tl ott','','OM ORDER TYPE','This gives the Order Type','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select CONCATENATED_SEGMENTS ITEM_NAME,DESCRIPTION ,ood.ORGANIZATION_NAME FROM  MTL_SYSTEM_ITEMS_KFV MSI ,org_organization_definitions ood WHERE  OOD.ORGANIZATION_ID=MSI.ORGANIZATION_ID and  exists (Select 1 from XXEIS.EIS_ORG_ACCESS_V where organization_id = msi.organization_id)
','','OM ITEM NAME','This gives the item name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select  cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','OM CUSTOMER NUMBER','This gives the Customer Number','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT C.LOCATION  SHIP_TO_LOCATION ,A.ACCOUNT_NUMBER
CUSTOMER_NUMBER,A.ACCOUNT_NAME CUSTOMER_NAME ,HOU.NAME OPERATING_UNIT
FROM HZ_CUST_ACCOUNTS A,HZ_CUST_ACCT_SITES B,
HZ_CUST_SITE_USES C,
HR_OPERATING_UNITS HOU
 WHERE A.CUST_ACCOUNT_ID=B.CUST_ACCOUNT_ID
 AND B.CUST_ACCT_SITE_ID=C.CUST_ACCT_SITE_ID
 AND SITE_USE_CODE=''SHIP_TO''
 AND B.ORG_ID=HOU.ORGANIZATION_ID
','','OM SHIP TO LOCATION','This gives ship to locations','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT C.LOCATION  BILL_TO_LOCATION ,A.ACCOUNT_NUMBER
CUSTOMER_NUMBER,A.ACCOUNT_NAME CUSTOMER_NAME ,HOU.NAME OPERATING_UNIT
FROM HZ_CUST_ACCOUNTS A,HZ_CUST_ACCT_SITES B,
HZ_CUST_SITE_USES C,
HR_OPERATING_UNITS HOU
 WHERE A.CUST_ACCOUNT_ID=B.CUST_ACCOUNT_ID
 AND B.CUST_ACCT_SITE_ID=C.CUST_ACCT_SITE_ID
 AND SITE_USE_CODE=''BILL_TO''
 AND B.ORG_ID=HOU.ORGANIZATION_ID','','OM BILL TO LOCATION','This gives bill to locations','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select  OT.Name,OT.DESCRIPTION,OT.ORDER_TYPE_ID   from  OE_ORDER_TYPES_V OT, HR_OPERATING_UNITS OU
WHERE OT.org_id = OU.organization_id
','','OM CREDIT ORDER TYPE','This gives credit order type','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct receipt_num from rcv_shipment_headers rsh,rcv_transactions rcv
where rsh.shipment_header_id=rcv.shipment_header_id
and rcv.source_document_code=''RMA''','','RMA Receipt Num','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Return Material Authorization Detail Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Return Material Authorization Detail Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Return Material Authorization Detail Report - WC' );
--Inserting Report - Return Material Authorization Detail Report - WC
xxeis.eis_rsc_ins.r( 660,'Return Material Authorization Detail Report - WC','','This report gives the information about the items (Sales order Lines) which are returned by the customer inlcuding Returned item,date and quantity etc.','','','','MR020532','XXEIS_WC_OM_RETURNS_RMA_V','Y','','','MR020532','','N','White Cap Reports','','CSV,EXCEL,Pivot Excel,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - Return Material Authorization Detail Report - WC
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'LINE_CATEGORY_CODE','Line Category Code','Line Category Code','','','default','','110','','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'OPTION_FLAG','Option Flag','Option Flag','','','default','','113','','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'ORDER_TYPE','Order Type','Order Type','','','default','','119','','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'RETURN_REASON_CODE','Return Reason Code','Return Reason Code','','','default','','136','','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'SALESREP_NAME','Salesrep Name','Salesrep Name','','','default','','4','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'EXTENDED_PRICE','Extended Price','Extended Price','','~T~D~2','default','','34','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'BILL_TO','Bill To','Bill To','','','default','','9','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'WAREHOUSE','Warehouse','Warehouse','','','default','','64','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'CUSTOMER','Customer','Customer','','','default','','6','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','7','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'CUST_PO_NUMBER','Cust Po Number','Cust Po Number','','','default','','8','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'FREIGHT_CARRIER_CODE','Freight Carrier Code','Freight Carrier Code','','','default','','45','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'INVOICED_QUANTITY','Invoiced Quantity','Invoiced Quantity','','~T~D~0','default','','49','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'OPERATING_UNIT','Operating Unit','Operating Unit','','','default','','1','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'ORDERED_ITEM','Ordered Item','Ordered Item','','','default','','28','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'ORDERED_QUANTITY','Ordered Quantity','Ordered Quantity','','~T~D~0','default','','29','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'ORDER_NUMBER','Order Number','Order Number','','~T~D~0','default','','2','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'SHIP_TO_CITY','Ship To City','Ship To City','','','default','','24','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'SHIP_TO_STATE','Ship To State','Ship To State','','','default','','26','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'TRANSACTION_TYPE','Transaction Type','Transaction Type','','','default','','62','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'UNIT_SELLING_PRICE','Unit Selling Price','Unit Selling Price','','~T~D~2','default','','31','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','default','','3','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'RECEIPT_DATE','Receipt Date','Receipt Date','','','default','','35','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Return Material Authorization Detail Report - WC',660,'RECEIVING_QUANTITY','Receiving Quantity','Receiving Quantity','','~T~D~0','default','','37','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_RETURNS_RMA_V','','','','US','');
--Inserting Report Parameters - Return Material Authorization Detail Report - WC
xxeis.eis_rsc_ins.rp( 'Return Material Authorization Detail Report - WC',660,'Bill To','Bill To','BILL_TO','IN','OM BILL TO LOCATION','','VARCHAR2','N','Y','10','Y','Y','CONSTANT','MR020532','Y','N','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Return Material Authorization Detail Report - WC',660,'Ship To','Ship To','SHIP_TO','IN','OM SHIP TO LOCATION','','VARCHAR2','N','Y','11','Y','Y','CONSTANT','MR020532','Y','N','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Return Material Authorization Detail Report - WC',660,'Transaction Type','Transaction Type','TRANSACTION_TYPE','IN','OM ORDER TYPE','BILL ONLY,CREDIT ONLY,RETURN WITH RECEIPT','VARCHAR2','N','Y','13','Y','Y','CONSTANT','MR020532','Y','N','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Return Material Authorization Detail Report - WC',660,'Warehouse','Warehouse','WAREHOUSE','IN','OM WAREHOUSE','','VARCHAR2','Y','Y','12','Y','Y','CONSTANT','MR020532','Y','N','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Return Material Authorization Detail Report - WC',660,'Order Number from','Order Number From','ORDER_NUMBER','>=','OM ORDER NUMBER','','NUMBER','N','Y','4','N','Y','CONSTANT','MR020532','Y','N','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Return Material Authorization Detail Report - WC',660,'Order Number To','Order Number To','ORDER_NUMBER','<=','OM ORDER NUMBER','','NUMBER','N','Y','5','N','Y','CONSTANT','MR020532','Y','N','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Return Material Authorization Detail Report - WC',660,'Operating unit','Operating unit','OPERATING_UNIT','IN','','HDS White Cap - Org','VARCHAR2','N','Y','1','Y','Y','CONSTANT','MR020532','Y','N','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Return Material Authorization Detail Report - WC',660,'Ordred Date From','Ordred Date From','ORDERED_DATE','>=','','','DATE','Y','Y','2','Y','Y','CONSTANT','MR020532','Y','N','','Start Date','','XXEIS_WC_OM_RETURNS_RMA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Return Material Authorization Detail Report - WC',660,'Ordred Date To','Ordred Date To','ORDERED_DATE','<=','','','DATE','Y','Y','3','Y','Y','CONSTANT','MR020532','Y','N','','End Date','','XXEIS_WC_OM_RETURNS_RMA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Return Material Authorization Detail Report - WC',660,'Ordered Item','Ordered Item','ORDERED_ITEM','IN','OM ITEM NAME','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','MR020532','Y','N','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Return Material Authorization Detail Report - WC',660,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','OM CUSTOMER NUMBER','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','MR020532','Y','N','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Return Material Authorization Detail Report - WC',660,'Customer Name','Customer Name','CUSTOMER','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','MR020532','Y','N','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Return Material Authorization Detail Report - WC',660,'RMA Receipt Num','RMA Receipt Num','RMA_RECEIPT_NUM','IN','RMA Receipt Num','','VARCHAR2','N','Y','9','Y','Y','CONSTANT','MR020532','Y','N','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Return Material Authorization Detail Report - WC',660,'Order Type','Order Type','ORDER_TYPE','IN','OM CREDIT ORDER TYPE','','VARCHAR2','N','Y','14','Y','Y','CONSTANT','MR020532','Y','N','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','US','');
--Inserting Dependent Parameters - Return Material Authorization Detail Report - WC
--Inserting Report Conditions - Return Material Authorization Detail Report - WC
xxeis.eis_rsc_ins.rcnh( 'Return Material Authorization Detail Report - WC',660,'BILL_TO IN :Bill To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BILL_TO','','Bill To','','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','','','','IN','Y','Y','','','','','1',660,'Return Material Authorization Detail Report - WC','BILL_TO IN :Bill To ');
xxeis.eis_rsc_ins.rcnh( 'Return Material Authorization Detail Report - WC',660,'CUSTOMER IN :Customer Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER','','Customer Name','','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','','','','IN','Y','Y','','','','','1',660,'Return Material Authorization Detail Report - WC','CUSTOMER IN :Customer Name ');
xxeis.eis_rsc_ins.rcnh( 'Return Material Authorization Detail Report - WC',660,'CUSTOMER_NUMBER IN :Customer Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_NUMBER','','Customer Number','','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','','','','IN','Y','Y','','','','','1',660,'Return Material Authorization Detail Report - WC','CUSTOMER_NUMBER IN :Customer Number ');
xxeis.eis_rsc_ins.rcnh( 'Return Material Authorization Detail Report - WC',660,'OPERATING_UNIT IN :Operating unit ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','OPERATING_UNIT','','Operating unit','','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','','','','IN','Y','Y','','','','','1',660,'Return Material Authorization Detail Report - WC','OPERATING_UNIT IN :Operating unit ');
xxeis.eis_rsc_ins.rcnh( 'Return Material Authorization Detail Report - WC',660,'ORDERED_DATE >= :Ordred Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORDERED_DATE','','Ordred Date From','','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',660,'Return Material Authorization Detail Report - WC','ORDERED_DATE >= :Ordred Date From ');
xxeis.eis_rsc_ins.rcnh( 'Return Material Authorization Detail Report - WC',660,'ORDERED_DATE <= :Ordred Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORDERED_DATE','','Ordred Date To','','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',660,'Return Material Authorization Detail Report - WC','ORDERED_DATE <= :Ordred Date To ');
xxeis.eis_rsc_ins.rcnh( 'Return Material Authorization Detail Report - WC',660,'ORDERED_ITEM IN :Ordered Item ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORDERED_ITEM','','Ordered Item','','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','','','','IN','Y','Y','','','','','1',660,'Return Material Authorization Detail Report - WC','ORDERED_ITEM IN :Ordered Item ');
xxeis.eis_rsc_ins.rcnh( 'Return Material Authorization Detail Report - WC',660,'ORDER_NUMBER <= :Order Number To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORDER_NUMBER','','Order Number To','','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',660,'Return Material Authorization Detail Report - WC','ORDER_NUMBER <= :Order Number To ');
xxeis.eis_rsc_ins.rcnh( 'Return Material Authorization Detail Report - WC',660,'ORDER_NUMBER >= :Order Number from ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORDER_NUMBER','','Order Number from','','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',660,'Return Material Authorization Detail Report - WC','ORDER_NUMBER >= :Order Number from ');
xxeis.eis_rsc_ins.rcnh( 'Return Material Authorization Detail Report - WC',660,'ORDER_TYPE IN :Order Type ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORDER_TYPE','','Order Type','','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','','','','IN','Y','Y','','','','','1',660,'Return Material Authorization Detail Report - WC','ORDER_TYPE IN :Order Type ');
xxeis.eis_rsc_ins.rcnh( 'Return Material Authorization Detail Report - WC',660,'RMA_RECEIPT_NUM IN :RMA Receipt Num ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','RMA_RECEIPT_NUM','','RMA Receipt Num','','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','','','','IN','Y','Y','','','','','1',660,'Return Material Authorization Detail Report - WC','RMA_RECEIPT_NUM IN :RMA Receipt Num ');
xxeis.eis_rsc_ins.rcnh( 'Return Material Authorization Detail Report - WC',660,'SHIP_TO IN :Ship To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SHIP_TO','','Ship To','','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','','','','IN','Y','Y','','','','','1',660,'Return Material Authorization Detail Report - WC','SHIP_TO IN :Ship To ');
xxeis.eis_rsc_ins.rcnh( 'Return Material Authorization Detail Report - WC',660,'TRANSACTION_TYPE IN :Transaction Type ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TRANSACTION_TYPE','','Transaction Type','','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','','','','IN','Y','Y','','','','','1',660,'Return Material Authorization Detail Report - WC','TRANSACTION_TYPE IN :Transaction Type ');
xxeis.eis_rsc_ins.rcnh( 'Return Material Authorization Detail Report - WC',660,'WAREHOUSE IN :Warehouse ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','WAREHOUSE','','Warehouse','','','','','XXEIS_WC_OM_RETURNS_RMA_V','','','','','','IN','Y','Y','','','','','1',660,'Return Material Authorization Detail Report - WC','WAREHOUSE IN :Warehouse ');
--Inserting Report Sorts - Return Material Authorization Detail Report - WC
--Inserting Report Triggers - Return Material Authorization Detail Report - WC
--inserting report templates - Return Material Authorization Detail Report - WC
xxeis.eis_rsc_ins.r_tem( 'Return Material Authorization Detail Report - WC','Return Material Authorization Detail Report - WC','Seeded template for Return Material Authorization Detail Report - WC','','','','','','','','','','','Return Material Authorization Detail Report - WC.rtf','MR020532','X','','','Y','Y','','');
--Inserting Report Portals - Return Material Authorization Detail Report - WC
--inserting report dashboards - Return Material Authorization Detail Report - WC
xxeis.eis_rsc_ins.R_dash( 'Return Material Authorization Detail Report - WC','Dynamic 1142','Dynamic 1142','vertical percent bar','large','Ship To','Ship To','Operating Unit','Operating Unit','Count','MR020532');
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Return Material Authorization Detail Report - WC','660','XXEIS_WC_OM_RETURNS_RMA_V','XXEIS_WC_OM_RETURNS_RMA_V','N','');
--inserting report security - Return Material Authorization Detail Report - WC
xxeis.eis_rsc_ins.rsec( 'Return Material Authorization Detail Report - WC','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return Material Authorization Detail Report - WC','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return Material Authorization Detail Report - WC','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return Material Authorization Detail Report - WC','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return Material Authorization Detail Report - WC','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return Material Authorization Detail Report - WC','660','','XXWC_AO_OEENTRY_PO_RPT',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return Material Authorization Detail Report - WC','660','','XXWC_AO_OEENTRY_REC',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return Material Authorization Detail Report - WC','660','','XXWC_AO_OEENTRY',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return Material Authorization Detail Report - WC','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return Material Authorization Detail Report - WC','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return Material Authorization Detail Report - WC','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return Material Authorization Detail Report - WC','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',660,'MR020532','','','');
--Inserting Report Pivots - Return Material Authorization Detail Report - WC
xxeis.eis_rsc_ins.rpivot( 'Return Material Authorization Detail Report - WC',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Return Material Authorization Detail Report - WC',660,'Pivot','CUSTOMER','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Return Material Authorization Detail Report - WC',660,'Pivot','CUSTOMER_NUMBER','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Return Material Authorization Detail Report - WC',660,'Pivot','OPERATING_UNIT','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Return Material Authorization Detail Report - WC',660,'Pivot','ORDERED_QUANTITY','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Return Material Authorization Detail Report - WC',660,'Pivot','ORDER_NUMBER','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Return Material Authorization Detail Report - WC',660,'Pivot','RECEIVING_QUANTITY','DATA_FIELD','SUM','','2','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- Return Material Authorization Detail Report - WC
xxeis.eis_rsc_ins.rv( 'Return Material Authorization Detail Report - WC','','Return Material Authorization Detail Report - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
