--Report Name            : OTM Step 1: Unposted SL Entries
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_756487_QRZOLB_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_756487_QRZOLB_V
xxeis.eis_rsc_ins.v( 'XXEIS_756487_QRZOLB_V',682,'Paste SQL View for Month End - TM to GL Summary Recon: Step 1','1.0','','','DV003828','APPS','Month End - TM to GL Summary Recon: Step 1 View','X7QV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_756487_QRZOLB_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_756487_QRZOLB_V',682,FALSE);
--Inserting Object Columns for XXEIS_756487_QRZOLB_V
xxeis.eis_rsc_ins.vc( 'XXEIS_756487_QRZOLB_V','ACCRUAL_AMOUNT',682,'','','','~T~D~2','','DV003828','NUMBER','','','Accrual Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_756487_QRZOLB_V','XLA_ORG',682,'','','','','','DV003828','NUMBER','','','Xla Org','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_756487_QRZOLB_V','LEDGER_NAME',682,'','','','','','DV003828','VARCHAR2','','','Ledger Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_756487_QRZOLB_V','EVENT_STATUS_CODE',682,'','','','','','DV003828','VARCHAR2','','','Event Status Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_756487_QRZOLB_V','PROCESS_STATUS_CODE',682,'','','','','','DV003828','VARCHAR2','','','Process Status Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_756487_QRZOLB_V','TRANSFER_FLAG',682,'','','','','','DV003828','VARCHAR2','','','Transfer Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_756487_QRZOLB_V','FISCAL_PERIOD',682,'','','','','','DV003828','VARCHAR2','','','Fiscal Period','','','','US');
--Inserting Object Components for XXEIS_756487_QRZOLB_V
--Inserting Object Component Joins for XXEIS_756487_QRZOLB_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report OTM Step 1: Unposted SL Entries
prompt Creating Report Data for OTM Step 1: Unposted SL Entries
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Deleting Report data - OTM Step 1: Unposted SL Entries
xxeis.eis_rsc_utility.delete_report_rows( 'OTM Step 1: Unposted SL Entries' );
--Inserting Report - OTM Step 1: Unposted SL Entries
xxeis.eis_rsc_ins.r( 682,'OTM Step 1: Unposted SL Entries','','Report used to identify TM accruals entries that have not had Subledger Accounting run and therefore have not been transferred to the GL.

If entries exist then Subledger Accounting program needs to be run in order to transfer these entries to the GL.','','','','KP059700','XXEIS_756487_QRZOLB_V','Y','','select  accrual_amount accrual_amount ,
          org_id xla_org,
          ledger_name ledger_name,
          event_status_code event_status_code,
          process_status_code process_status_code,
          gl_transfer_status_code transfer_flag,
          fiscal_period
  from xxcus.xxcus_ytd_income_b    
 WHERE 1 = 1 AND fiscal_period = ''NA''
','KP059700','','N','MONTH-END','','CSV,Pivot Excel,EXCEL,','Y','','','','','','','APPS','US','','','','');
--Inserting Report Columns - OTM Step 1: Unposted SL Entries
xxeis.eis_rsc_ins.rc( 'OTM Step 1: Unposted SL Entries',682,'EVENT_STATUS_CODE','Event Status Code','','','','default','','3','N','Y','','','','','','','KP059700','N','N','','XXEIS_756487_QRZOLB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'OTM Step 1: Unposted SL Entries',682,'PROCESS_STATUS_CODE','Process Status Code','','','','default','','4','N','Y','','','','','','','KP059700','N','N','','XXEIS_756487_QRZOLB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'OTM Step 1: Unposted SL Entries',682,'LEDGER_NAME','Ledger Name','','','','default','','1','N','Y','','','','','','','KP059700','N','N','','XXEIS_756487_QRZOLB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'OTM Step 1: Unposted SL Entries',682,'TRANSFER_FLAG','Transfer Flag','','','','default','','2','N','Y','','','','','','','KP059700','N','N','','XXEIS_756487_QRZOLB_V','','','GROUP_BY','US','');
--Inserting Report Parameters - OTM Step 1: Unposted SL Entries
xxeis.eis_rsc_ins.rp( 'OTM Step 1: Unposted SL Entries',682,'ORG','','XLA_ORG','IN','','101,102','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_756487_QRZOLB_V','','','US','');
--Inserting Dependent Parameters - OTM Step 1: Unposted SL Entries
--Inserting Report Conditions - OTM Step 1: Unposted SL Entries
xxeis.eis_rsc_ins.rcnh( 'OTM Step 1: Unposted SL Entries',682,'XLA_ORG IN :ORG ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','XLA_ORG','','ORG','','','','','XXEIS_756487_QRZOLB_V','','','','','','IN','Y','Y','','','','','1',682,'OTM Step 1: Unposted SL Entries','XLA_ORG IN :ORG ');
--Inserting Report Sorts - OTM Step 1: Unposted SL Entries
--Inserting Report Triggers - OTM Step 1: Unposted SL Entries
--inserting report templates - OTM Step 1: Unposted SL Entries
--Inserting Report Portals - OTM Step 1: Unposted SL Entries
--inserting report dashboards - OTM Step 1: Unposted SL Entries
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'OTM Step 1: Unposted SL Entries','682','XXEIS_756487_QRZOLB_V','XXEIS_756487_QRZOLB_V','N','');
--inserting report security - OTM Step 1: Unposted SL Entries
xxeis.eis_rsc_ins.rsec( 'OTM Step 1: Unposted SL Entries','682','','XXCUS_TM_USER',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'OTM Step 1: Unposted SL Entries','682','','XXCUS_TM_ADMIN_USER',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'OTM Step 1: Unposted SL Entries','682','','XXCUS_TM_FORMS_RESP',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'OTM Step 1: Unposted SL Entries','682','','XXCUS_TM_ADM_FORMS_RESP',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'OTM Step 1: Unposted SL Entries','222','','XXWC_RECEIVABLES_INQUIRY_WC',682,'KP059700','','','');
--Inserting Report Pivots - OTM Step 1: Unposted SL Entries
--Inserting Report   Version details- OTM Step 1: Unposted SL Entries
xxeis.eis_rsc_ins.rv( 'OTM Step 1: Unposted SL Entries','','OTM Step 1: Unposted SL Entries','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
