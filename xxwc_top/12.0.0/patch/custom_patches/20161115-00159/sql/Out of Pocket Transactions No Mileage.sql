--Report Name            : Out of Pocket Transactions (No Mileage)
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXCUS_BULLET_IEXP_TBL
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXCUS_BULLET_IEXP_TBL
xxeis.eis_rsc_ins.v( 'XXCUS_BULLET_IEXP_TBL',91000,'Paste SQL View for iExpense Bullet Train Table','','','','XXEIS_RS_ADMIN','XXEIS','XXCUS_BULLET_IEXP_TBL','XBIT','','','VIEW','US','','');
--Delete Object Columns for XXCUS_BULLET_IEXP_TBL
xxeis.eis_rsc_utility.delete_view_rows('XXCUS_BULLET_IEXP_TBL',91000,FALSE);
--Inserting Object Columns for XXCUS_BULLET_IEXP_TBL
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','PERIOD_NAME',91000,'','PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CC_FISCAL_PERIOD',91000,'','CC_FISCAL_PERIOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cc Fiscal Period','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_PRODUCT',91000,'','ORACLE_PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Product','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_LOCATION',91000,'','ORACLE_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Location','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_COST_CENTER',91000,'','ORACLE_COST_CENTER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_ACCOUNT',91000,'','ORACLE_ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Account','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ITEM_DESCRIPTION',91000,'','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','LINE_AMOUNT',91000,'','LINE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Line Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DISTRIBUTION_LINE_NUMBER',91000,'','DISTRIBUTION_LINE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Distribution Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','FULL_NAME',91000,'','FULL_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Full Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EMPLOYEE_NUMBER',91000,'','EMPLOYEE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Employee Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EMP_DEFAULT_PROD',91000,'','EMP_DEFAULT_PROD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Emp Default Prod','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EMP_DEFAULT_LOC',91000,'','EMP_DEFAULT_LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Emp Default Loc','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EMP_DEFAULT_COSTCTR',91000,'','EMP_DEFAULT_COSTCTR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Emp Default Costctr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CREATION_DATE',91000,'','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','REPORT_SUBMITTED_DATE',91000,'','REPORT_SUBMITTED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Report Submitted Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','MERCHANT_NAME',91000,'','MERCHANT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Merchant Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','BUSINESS_PURPOSE',91000,'','BUSINESS_PURPOSE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Business Purpose','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','JUSTIFICATION',91000,'','JUSTIFICATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Justification','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','START_EXPENSE_DATE',91000,'','START_EXPENSE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Start Expense Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','END_EXPENSE_DATE',91000,'','END_EXPENSE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','End Expense Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','REMARKS',91000,'','REMARKS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Remarks','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ATTENDEES',91000,'','ATTENDEES','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CREDIT_CARD_TRX_ID',91000,'','CREDIT_CARD_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Credit Card Trx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','TRANSACTION_DATE',91000,'','TRANSACTION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Transaction Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','MERCHANT_LOCATION',91000,'','MERCHANT_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Merchant Location','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DESTINATION_FROM',91000,'','DESTINATION_FROM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Destination From','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DESTINATION_TO',91000,'','DESTINATION_TO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Destination To','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DAILY_DISTANCE',91000,'','DAILY_DISTANCE','','','','XXEIS_RS_ADMIN','NUMBER','','','Daily Distance','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','TRIP_DISTANCE',91000,'','TRIP_DISTANCE','','','','XXEIS_RS_ADMIN','NUMBER','','','Trip Distance','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DT_SENTTO_GL',91000,'','DT_SENTTO_GL','','','','XXEIS_RS_ADMIN','DATE','','','Dt Sentto Gl','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EXPENSE_REPORT_NUMBER',91000,'','EXPENSE_REPORT_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Expense Report Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CARD_PROGRAM_NAME',91000,'','CARD_PROGRAM_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Program Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','REPORT_TOTAL',91000,'','REPORT_TOTAL','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Report Total','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','TRANSACTION_AMOUNT',91000,'','TRANSACTION_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Transaction Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','APPROVER_ID',91000,'','APPROVER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Approver Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','APPROVER_NAME',91000,'','APPROVER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Approver Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','QUERY_DESCR',91000,'','QUERY_DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Query Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','POSTED_DATE',91000,'','POSTED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Posted Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','MCC_CODE_NO',91000,'','MCC_CODE_NO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mcc Code No','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','COST_CENTER_DESCR',91000,'','COST_CENTER_DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cost Center Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','FRU',91000,'','FRU','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Fru','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CARDMEMBER_NAME',91000,'','CARDMEMBER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cardmember Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','MCC_CODE',91000,'','MCC_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mcc Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','IMPORTED_TO_GL',91000,'','IMPORTED_TO_GL','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Imported To Gl','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','QUERY_NUM',91000,'','QUERY_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Query Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EXPENSE_REPORT_STATUS',91000,'','EXPENSE_REPORT_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Expense Report Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','REFERENCE_NUMBER',91000,'','REFERENCE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','PRODUCT_DESCR',91000,'','PRODUCT_DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Product Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','LOCATION_DESCR',91000,'','LOCATION_DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ACCOUNT_DESCR',91000,'','ACCOUNT_DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Account Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_ACCOUNTS',91000,'','ORACLE_ACCOUNTS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Accounts','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CATEGORY',91000,'','CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','SUB_CATEGORY',91000,'','SUB_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sub Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','INVOICE_NUMBER',91000,'','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DESCRIPTION',91000,'','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','AVG_MILEAGE_RATE',91000,'','AVG_MILEAGE_RATE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Avg Mileage Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ACCOUNTING_DATE',91000,'','ACCOUNTING_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Accounting Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','AMT_DUE_CCARD_COMPANY',91000,'','AMT_DUE_CCARD_COMPANY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amt Due Ccard Company','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CC_TRNS_CATEGORY',91000,'','CC_TRNS_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cc Trns Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','WORKFLOW_APPROVED_FLAG',91000,'','WORKFLOW_APPROVED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Workflow Approved Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','EXPENSE_CURRENT_APPROVER_ID',91000,'','EXPENSE_CURRENT_APPROVER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Expense Current Approver Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','REPORT_TYPE',91000,'','REPORT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Report Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ADVANCE_INVOICE_TO_APPLY',91000,'','ADVANCE_INVOICE_TO_APPLY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Advance Invoice To Apply','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ADVANCE_DISTRIBUTION_NUMBER',91000,'','ADVANCE_DISTRIBUTION_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Advance Distribution Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ADVANCE_FLAG',91000,'','ADVANCE_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Advance Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ADVANCE_GL_DATE',91000,'','ADVANCE_GL_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Advance Gl Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ADVANCE_NUMBER',91000,'','ADVANCE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Advance Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','REPORT_REJECT_CODE',91000,'','REPORT_REJECT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Report Reject Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','APP_POST_FLAG',91000,'','APP_POST_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','App Post Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','APPROVED_IN_GL',91000,'','APPROVED_IN_GL','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Approved In Gl','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','VOUCHNO',91000,'','VOUCHNO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vouchno','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','PROJECT_NUMBER',91000,'','PROJECT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Project Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','PROJECT_NAME',91000,'','PROJECT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Project Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CURRENCY_CODE',91000,'','CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','VENDOR_NAME',91000,'','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ATTENDEES_EMP',91000,'','ATTENDEES_EMP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees Emp','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ATTENDEES_TE',91000,'','ATTENDEES_TE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees Te','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ATTENDEES_CTI1',91000,'','ATTENDEES_CTI1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees Cti1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ATTENDEES_CTI2',91000,'','ATTENDEES_CTI2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attendees Cti2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','DISTANCE_UNIT_CODE',91000,'','DISTANCE_UNIT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Distance Unit Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CARD_PROGRAM_ID',91000,'','CARD_PROGRAM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Card Program Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','MILES',91000,'','MILES','','','','XXEIS_RS_ADMIN','NUMBER','','','Miles','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','RATE_PER_MILE',91000,'','RATE_PER_MILE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Rate Per Mile','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ATTRIBUTE_CATEGORY',91000,'','ATTRIBUTE_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','CATEGORY_CODE',91000,'','CATEGORY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Category Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','RECEIPT_CURRENCY_AMOUNT',91000,'','RECEIPT_CURRENCY_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Receipt Currency Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','AMT_DUE_EMPLOYEE',91000,'','AMT_DUE_EMPLOYEE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amt Due Employee','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','IMPORTED_TO_AP',91000,'','IMPORTED_TO_AP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Imported To Ap','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','GST_AMOUNT',91000,'Gst Amount','GST_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Gst Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','HST_AMOUNT',91000,'Hst Amount','HST_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Hst Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_SEGMENT6',91000,'Oracle Segment6','ORACLE_SEGMENT6','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Segment6','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORACLE_SEGMENT7',91000,'Oracle Segment7','ORACLE_SEGMENT7','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Segment7','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','ORG_ID',91000,'Org Id','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','PST_AMOUNT',91000,'Pst Amount','PST_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Pst Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','QST_AMOUNT',91000,'Qst Amount','QST_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Qst Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUS_BULLET_IEXP_TBL','TAX_PROVINCE',91000,'Tax Province','TAX_PROVINCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Tax Province','','','','US');
--Inserting Object Components for XXCUS_BULLET_IEXP_TBL
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXCUS_NATURAL_ACCT_TBL',91000,'','XNAT','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXCUS_NATURAL_ACCT_TBL','','','','','XNAT','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXCUS_LOCATION_CODE_TBL',91000,'','XLC','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXCUS_LOCATION_CODE_TBL','','','','','XLCT','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','AP_CREDIT_CARD_TRXNS_ALL',91000,'','ACCTA','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','AP_CREDIT_CARD_TRXNS_ALL','','','','','ACCTA','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXHDS_APPR_ACT_TERM_DATE',91000,'','XAATD','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXHDS_APPR_ACT_TERM_DATE','','','','','XAATD','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXHDS_MCC_CODE_ACCT_INFO',91000,'','XMCAI','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXHDS_MCC_CODE_ACCT_INFO','','','','','XMCAI','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXHDS_CC_HOLDER_INFO',91000,'','XCHI','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXHDS_CC_HOLDER_INFO','','','','','XCHI','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXEIS_LOB_NAMES_V',91000,'','XLNV','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXEIS_LOB_NAMES_V','','','','','X1HV','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'XXCUS_BULLET_IEXP_TBL','XXCUSHR_PS_EMP_ALL_TBL',91000,'','XPEAT','','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','XXCUSHR_PS_EMP_ALL_TBL','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for XXCUS_BULLET_IEXP_TBL
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXEIS_LOB_NAMES_V','XLNV',91000,'XBIT.ORACLE_PRODUCT','=','XLNV.ORACLE_PRODUCT(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','AP_CREDIT_CARD_TRXNS_ALL','ACCTA',91000,'XBIT.CREDIT_CARD_TRX_ID','=','ACCTA.TRX_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXCUS_LOCATION_CODE_TBL','XLC',91000,'XBIT.ORACLE_LOCATION','=','XLC.ENTRP_LOC(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXCUS_NATURAL_ACCT_TBL','XNAT',91000,'XBIT.ORACLE_ACCOUNT','=','XNAT.Z_OLD_VALUE(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXHDS_APPR_ACT_TERM_DATE','XAATD',91000,'XBIT.APPROVER_ID','=','XAATD.APPROVER_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXHDS_CC_HOLDER_INFO','XCHI',91000,'XBIT.EMPLOYEE_NUMBER','=','XCHI.EMPLOYEE_NUMBER','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXHDS_MCC_CODE_ACCT_INFO','XMCAI',91000,'XBIT.MCC_CODE_NO','=','XMCAI.MCC_CODE_N0(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'XXCUS_BULLET_IEXP_TBL','XXCUSHR_PS_EMP_ALL_TBL','XPEAT',91000,'XBIT.EMPLOYEE_NUMBER','=','XPEAT.EMPLOYEE_NUMBER','','','','','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
--Exporting View Component Data of the View -  XXCUS_BULLET_IEXP_TBL
prompt Creating Object Data AP_CREDIT_CARD_TRXNS_ALL
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Object AP_CREDIT_CARD_TRXNS_ALL
xxeis.eis_rsc_ins.v( 'AP_CREDIT_CARD_TRXNS_ALL',91000,'Detailed information about the credit card transactions of your employees','1.0','','','XXEIS_RS_ADMIN','AP','Ap Credit Card Trxns All','ACCTA','','','TABLE','US','','');
--Delete Object Columns for AP_CREDIT_CARD_TRXNS_ALL
xxeis.eis_rsc_utility.delete_view_rows('AP_CREDIT_CARD_TRXNS_ALL',91000,FALSE);
--Inserting Object Columns for AP_CREDIT_CARD_TRXNS_ALL
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','INACTIVE_EMP_WF_ITEM_KEY',91000,'','INACTIVE_EMP_WF_ITEM_KEY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','INACTIVE_EMP_WF_ITEM_KEY','Inactive Emp Wf Item Key','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','LOCATION_ID',91000,'','LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','LOCATION_ID','Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','REQUEST_ID',91000,'','REQUEST_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','REQUEST_ID','Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_COUNTRY_CODE',91000,'','MERCHANT_COUNTRY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_COUNTRY_CODE','Merchant Country Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','DISPUTE_DATE',91000,'','DISPUTE_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','DISPUTE_DATE','Dispute Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','PAYMENT_DUE_FROM_CODE',91000,'','PAYMENT_DUE_FROM_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','PAYMENT_DUE_FROM_CODE','Payment Due From Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','TRX_AVAILABLE_DATE',91000,'','TRX_AVAILABLE_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','TRX_AVAILABLE_DATE','Trx Available Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CARD_ACCEPTOR_ID',91000,'','CARD_ACCEPTOR_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CARD_ACCEPTOR_ID','Card Acceptor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','TRXN_DETAIL_FLAG',91000,'','TRXN_DETAIL_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','TRXN_DETAIL_FLAG','Trxn Detail Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CARD_ID',91000,'','CARD_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CARD_ID','Card Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','DESCRIPTION',91000,'','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','DESCRIPTION','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','COMPANY_NUMBER',91000,'','COMPANY_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','COMPANY_NUMBER','Company Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MARKET_CODE',91000,'','MARKET_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MARKET_CODE','Market Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','VALIDATE_REQUEST_ID',91000,'','VALIDATE_REQUEST_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','VALIDATE_REQUEST_ID','Validate Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','TRX_ID',91000,'','TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','TRX_ID','Trx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','VALIDATE_CODE',91000,'','VALIDATE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','VALIDATE_CODE','Validate Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CARD_PROGRAM_ID',91000,'','CARD_PROGRAM_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CARD_PROGRAM_ID','Card Program Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','EXPENSED_AMOUNT',91000,'','EXPENSED_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','EXPENSED_AMOUNT','Expensed Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','REFERENCE_NUMBER',91000,'','REFERENCE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','REFERENCE_NUMBER','Reference Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','TRANSACTION_TYPE',91000,'','TRANSACTION_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','TRANSACTION_TYPE','Transaction Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','TRANSACTION_DATE',91000,'','TRANSACTION_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','TRANSACTION_DATE','Transaction Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','TRANSACTION_AMOUNT',91000,'','TRANSACTION_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','TRANSACTION_AMOUNT','Transaction Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','DEBIT_FLAG',91000,'','DEBIT_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','DEBIT_FLAG','Debit Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','BILLED_DATE',91000,'','BILLED_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','BILLED_DATE','Billed Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','BILLED_AMOUNT',91000,'','BILLED_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','BILLED_AMOUNT','Billed Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','BILLED_DECIMAL',91000,'','BILLED_DECIMAL','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','BILLED_DECIMAL','Billed Decimal','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','BILLED_CURRENCY_CODE',91000,'','BILLED_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','BILLED_CURRENCY_CODE','Billed Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','POSTED_DATE',91000,'','POSTED_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','POSTED_DATE','Posted Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','POSTED_AMOUNT',91000,'','POSTED_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','POSTED_AMOUNT','Posted Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','POSTED_DECIMAL',91000,'','POSTED_DECIMAL','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','POSTED_DECIMAL','Posted Decimal','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','POSTED_CURRENCY_CODE',91000,'','POSTED_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','POSTED_CURRENCY_CODE','Posted Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CURRENCY_CONVERSION_EXPONENT',91000,'','CURRENCY_CONVERSION_EXPONENT','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CURRENCY_CONVERSION_EXPONENT','Currency Conversion Exponent','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CURRENCY_CONVERSION_RATE',91000,'','CURRENCY_CONVERSION_RATE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CURRENCY_CONVERSION_RATE','Currency Conversion Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MIS_INDUSTRY_CODE',91000,'','MIS_INDUSTRY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MIS_INDUSTRY_CODE','Mis Industry Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','SIC_CODE',91000,'','SIC_CODE','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','SIC_CODE','Sic Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_TAX_ID',91000,'','MERCHANT_TAX_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_TAX_ID','Merchant Tax Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_REFERENCE',91000,'','MERCHANT_REFERENCE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_REFERENCE','Merchant Reference','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_NAME1',91000,'','MERCHANT_NAME1','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_NAME1','Merchant Name1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_NAME2',91000,'','MERCHANT_NAME2','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_NAME2','Merchant Name2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ADDRESS1',91000,'','MERCHANT_ADDRESS1','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ADDRESS1','Merchant Address1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ADDRESS2',91000,'','MERCHANT_ADDRESS2','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ADDRESS2','Merchant Address2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ADDRESS3',91000,'','MERCHANT_ADDRESS3','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ADDRESS3','Merchant Address3','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ADDRESS4',91000,'','MERCHANT_ADDRESS4','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ADDRESS4','Merchant Address4','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_CITY',91000,'','MERCHANT_CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_CITY','Merchant City','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_PROVINCE_STATE',91000,'','MERCHANT_PROVINCE_STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_PROVINCE_STATE','Merchant Province State','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CATEGORY',91000,'','CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CATEGORY','Category','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','REPORT_HEADER_ID',91000,'','REPORT_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','REPORT_HEADER_ID','Report Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','EXPENSE_STATUS',91000,'','EXPENSE_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','EXPENSE_STATUS','Expense Status','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','COMPANY_PREPAID_INVOICE_ID',91000,'','COMPANY_PREPAID_INVOICE_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','COMPANY_PREPAID_INVOICE_ID','Company Prepaid Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CARD_NUMBER',91000,'','CARD_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CARD_NUMBER','Card Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_POSTAL_CODE',91000,'','MERCHANT_POSTAL_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_POSTAL_CODE','Merchant Postal Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_COUNTRY',91000,'','MERCHANT_COUNTRY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_COUNTRY','Merchant Country','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','TOTAL_TAX',91000,'','TOTAL_TAX','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','TOTAL_TAX','Total Tax','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','LOCAL_TAX',91000,'','LOCAL_TAX','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','LOCAL_TAX','Local Tax','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','NATIONAL_TAX',91000,'','NATIONAL_TAX','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','NATIONAL_TAX','National Tax','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','OTHER_TAX',91000,'','OTHER_TAX','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','OTHER_TAX','Other Tax','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','ORG_ID',91000,'','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','ORG_ID','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','LAST_UPDATE_DATE',91000,'','LAST_UPDATE_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','LAST_UPDATE_DATE','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','LAST_UPDATED_BY',91000,'','LAST_UPDATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','LAST_UPDATED_BY','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','LAST_UPDATE_LOGIN',91000,'','LAST_UPDATE_LOGIN','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','LAST_UPDATE_LOGIN','Last Update Login','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CREATION_DATE',91000,'','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','CREATION_DATE','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CREATED_BY',91000,'','CREATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CREATED_BY','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','FOLIO_TYPE',91000,'','FOLIO_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','FOLIO_TYPE','Folio Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','ATM_CASH_ADVANCE',91000,'','ATM_CASH_ADVANCE','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','ATM_CASH_ADVANCE','Atm Cash Advance','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','ATM_TRANSACTION_DATE',91000,'','ATM_TRANSACTION_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','ATM_TRANSACTION_DATE','Atm Transaction Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','ATM_FEE_AMOUNT',91000,'','ATM_FEE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','ATM_FEE_AMOUNT','Atm Fee Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','ATM_TYPE',91000,'','ATM_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','ATM_TYPE','Atm Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','ATM_ID',91000,'','ATM_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','ATM_ID','Atm Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','ATM_NETWORK_ID',91000,'','ATM_NETWORK_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','ATM_NETWORK_ID','Atm Network Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','RESTAURANT_FOOD_AMOUNT',91000,'','RESTAURANT_FOOD_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','RESTAURANT_FOOD_AMOUNT','Restaurant Food Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','RESTAURANT_BEVERAGE_AMOUNT',91000,'','RESTAURANT_BEVERAGE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','RESTAURANT_BEVERAGE_AMOUNT','Restaurant Beverage Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','RESTAURANT_TIP_AMOUNT',91000,'','RESTAURANT_TIP_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','RESTAURANT_TIP_AMOUNT','Restaurant Tip Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_DATE',91000,'','CAR_RENTAL_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_DATE','Car Rental Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_RETURN_DATE',91000,'','CAR_RETURN_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','CAR_RETURN_DATE','Car Return Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_LOCATION',91000,'','CAR_RENTAL_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_LOCATION','Car Rental Location','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_STATE',91000,'','CAR_RENTAL_STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_STATE','Car Rental State','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_RETURN_LOCATION',91000,'','CAR_RETURN_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CAR_RETURN_LOCATION','Car Return Location','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_RETURN_STATE',91000,'','CAR_RETURN_STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CAR_RETURN_STATE','Car Return State','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTER_NAME',91000,'','CAR_RENTER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTER_NAME','Car Renter Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_DAYS',91000,'','CAR_RENTAL_DAYS','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_DAYS','Car Rental Days','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_AGREEMENT_NUMBER',91000,'','CAR_RENTAL_AGREEMENT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CAR_RENTAL_AGREEMENT_NUMBER','Car Rental Agreement Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_CLASS',91000,'','CAR_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','CAR_CLASS','Car Class','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_TOTAL_MILEAGE',91000,'','CAR_TOTAL_MILEAGE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CAR_TOTAL_MILEAGE','Car Total Mileage','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_GAS_AMOUNT',91000,'','CAR_GAS_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CAR_GAS_AMOUNT','Car Gas Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_INSURANCE_AMOUNT',91000,'','CAR_INSURANCE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CAR_INSURANCE_AMOUNT','Car Insurance Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_MILEAGE_AMOUNT',91000,'','CAR_MILEAGE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CAR_MILEAGE_AMOUNT','Car Mileage Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','CAR_DAILY_RATE',91000,'','CAR_DAILY_RATE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','CAR_DAILY_RATE','Car Daily Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ARRIVAL_DATE',91000,'','HOTEL_ARRIVAL_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ARRIVAL_DATE','Hotel Arrival Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_DEPART_DATE',91000,'','HOTEL_DEPART_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_DEPART_DATE','Hotel Depart Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_CHARGE_DESC',91000,'','HOTEL_CHARGE_DESC','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_CHARGE_DESC','Hotel Charge Desc','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_GUEST_NAME',91000,'','HOTEL_GUEST_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_GUEST_NAME','Hotel Guest Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_STAY_DURATION',91000,'','HOTEL_STAY_DURATION','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_STAY_DURATION','Hotel Stay Duration','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_RATE',91000,'','HOTEL_ROOM_RATE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_RATE','Hotel Room Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_NO_SHOW_FLAG',91000,'','HOTEL_NO_SHOW_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_NO_SHOW_FLAG','Hotel No Show Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_AMOUNT',91000,'','HOTEL_ROOM_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_AMOUNT','Hotel Room Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_TELEPHONE_AMOUNT',91000,'','HOTEL_TELEPHONE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_TELEPHONE_AMOUNT','Hotel Telephone Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_TAX',91000,'','HOTEL_ROOM_TAX','','','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_TAX','Hotel Room Tax','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_BAR_AMOUNT',91000,'','HOTEL_BAR_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_BAR_AMOUNT','Hotel Bar Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_MOVIE_AMOUNT',91000,'','HOTEL_MOVIE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_MOVIE_AMOUNT','Hotel Movie Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_GIFT_SHOP_AMOUNT',91000,'','HOTEL_GIFT_SHOP_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_GIFT_SHOP_AMOUNT','Hotel Gift Shop Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_LAUNDRY_AMOUNT',91000,'','HOTEL_LAUNDRY_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_LAUNDRY_AMOUNT','Hotel Laundry Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_HEALTH_AMOUNT',91000,'','HOTEL_HEALTH_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_HEALTH_AMOUNT','Hotel Health Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_RESTAURANT_AMOUNT',91000,'','HOTEL_RESTAURANT_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_RESTAURANT_AMOUNT','Hotel Restaurant Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_BUSINESS_AMOUNT',91000,'','HOTEL_BUSINESS_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_BUSINESS_AMOUNT','Hotel Business Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_PARKING_AMOUNT',91000,'','HOTEL_PARKING_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_PARKING_AMOUNT','Hotel Parking Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_SERVICE_AMOUNT',91000,'','HOTEL_ROOM_SERVICE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_SERVICE_AMOUNT','Hotel Room Service Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_TIP_AMOUNT',91000,'','HOTEL_TIP_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_TIP_AMOUNT','Hotel Tip Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_MISC_AMOUNT',91000,'','HOTEL_MISC_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_MISC_AMOUNT','Hotel Misc Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_CITY',91000,'','HOTEL_CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_CITY','Hotel City','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_STATE',91000,'','HOTEL_STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_STATE','Hotel State','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_FOLIO_NUMBER',91000,'','HOTEL_FOLIO_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_FOLIO_NUMBER','Hotel Folio Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_TYPE',91000,'','HOTEL_ROOM_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','HOTEL_ROOM_TYPE','Hotel Room Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_DEPARTURE_DATE',91000,'','AIR_DEPARTURE_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CREDIT_CARD_TRXNS_ALL','AIR_DEPARTURE_DATE','Air Departure Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_DEPARTURE_CITY',91000,'','AIR_DEPARTURE_CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_DEPARTURE_CITY','Air Departure City','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_ROUTING',91000,'','AIR_ROUTING','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_ROUTING','Air Routing','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_ARRIVAL_CITY',91000,'','AIR_ARRIVAL_CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_ARRIVAL_CITY','Air Arrival City','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_STOPOVER_FLAG',91000,'','AIR_STOPOVER_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_STOPOVER_FLAG','Air Stopover Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_BASE_FARE_AMOUNT',91000,'','AIR_BASE_FARE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CREDIT_CARD_TRXNS_ALL','AIR_BASE_FARE_AMOUNT','Air Base Fare Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_FARE_BASIS_CODE',91000,'','AIR_FARE_BASIS_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_FARE_BASIS_CODE','Air Fare Basis Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_SERVICE_CLASS',91000,'','AIR_SERVICE_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_SERVICE_CLASS','Air Service Class','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_CARRIER_ABBREVIATION',91000,'','AIR_CARRIER_ABBREVIATION','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_CARRIER_ABBREVIATION','Air Carrier Abbreviation','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_CARRIER_CODE',91000,'','AIR_CARRIER_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_CARRIER_CODE','Air Carrier Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_TICKET_ISSUER',91000,'','AIR_TICKET_ISSUER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_TICKET_ISSUER','Air Ticket Issuer','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_ISSUER_CITY',91000,'','AIR_ISSUER_CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_ISSUER_CITY','Air Issuer City','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_PASSENGER_NAME',91000,'','AIR_PASSENGER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_PASSENGER_NAME','Air Passenger Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_REFUND_TICKET_NUMBER',91000,'','AIR_REFUND_TICKET_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_REFUND_TICKET_NUMBER','Air Refund Ticket Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_EXCHANGED_TICKET_NUMBER',91000,'','AIR_EXCHANGED_TICKET_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_EXCHANGED_TICKET_NUMBER','Air Exchanged Ticket Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_AGENCY_NUMBER',91000,'','AIR_AGENCY_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_AGENCY_NUMBER','Air Agency Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','AIR_TICKET_NUMBER',91000,'','AIR_TICKET_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','AIR_TICKET_NUMBER','Air Ticket Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','FINANCIAL_CATEGORY',91000,'','FINANCIAL_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','FINANCIAL_CATEGORY','Financial Category','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','PAYMENT_FLAG',91000,'','PAYMENT_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','PAYMENT_FLAG','Payment Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','RECORD_TYPE',91000,'','RECORD_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','RECORD_TYPE','Record Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ACTIVITY',91000,'','MERCHANT_ACTIVITY','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CREDIT_CARD_TRXNS_ALL','MERCHANT_ACTIVITY','Merchant Activity','','','','US');
--Inserting Object Components for AP_CREDIT_CARD_TRXNS_ALL
--Inserting Object Component Joins for AP_CREDIT_CARD_TRXNS_ALL
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
--Exporting View Component Data of the View -  XXCUS_BULLET_IEXP_TBL
prompt Creating Object Data XXEIS_LOB_NAMES_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_LOB_NAMES_V
xxeis.eis_rsc_ins.v( 'XXEIS_LOB_NAMES_V',91000,'Paste SQL View for View Generation - LOB Names','1.0','','','JR006127','APPS','View Generation - LOB Names View','X1HV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_LOB_NAMES_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_LOB_NAMES_V',91000,FALSE);
--Inserting Object Columns for XXEIS_LOB_NAMES_V
xxeis.eis_rsc_ins.vc( 'XXEIS_LOB_NAMES_V','LOB_NAME',91000,'','','','','','JR006127','VARCHAR2','','','Lob Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_LOB_NAMES_V','ORACLE_PRODUCT',91000,'','','','','','JR006127','VARCHAR2','','','Oracle Product','','','','US');
--Inserting Object Components for XXEIS_LOB_NAMES_V
--Inserting Object Component Joins for XXEIS_LOB_NAMES_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
--Exporting View Component Data of the View -  XXCUS_BULLET_IEXP_TBL
prompt Creating Object Data XXCUSHR_PS_EMP_ALL_TBL
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXCUSHR_PS_EMP_ALL_TBL
xxeis.eis_rsc_ins.v( 'XXCUSHR_PS_EMP_ALL_TBL',91000,'','','','','ID020048','XXCUS','Xxcushr Ps Emp All Tbl','XPEAT','','','TABLE','US','','');
--Delete Object Columns for XXCUSHR_PS_EMP_ALL_TBL
xxeis.eis_rsc_utility.delete_view_rows('XXCUSHR_PS_EMP_ALL_TBL',91000,FALSE);
--Inserting Object Columns for XXCUSHR_PS_EMP_ALL_TBL
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','JOB_FAMILY_DESCR',91000,'Job Family Descr','JOB_FAMILY_DESCR','','','','ID020048','VARCHAR2','','','Job Family Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','JOB_FAMILY',91000,'Job Family','JOB_FAMILY','','','','ID020048','VARCHAR2','','','Job Family','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','JOB_DESCR',91000,'Job Descr','JOB_DESCR','','','','ID020048','VARCHAR2','','','Job Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','PROCESS_LEV',91000,'Process Lev','PROCESS_LEV','','','','ID020048','VARCHAR2','','','Process Lev','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','LAST_DAY_PAID',91000,'Last Day Paid','LAST_DAY_PAID','','','','ID020048','DATE','','','Last Day Paid','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','NT_ID',91000,'Nt Id','NT_ID','','','','ID020048','VARCHAR2','','','Nt Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','CODE_COMBINATION_ID',91000,'Code Combination Id','CODE_COMBINATION_ID','','','','ID020048','NUMBER','','','Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','JOB_CLASS_CODE',91000,'Job Class Code','JOB_CLASS_CODE','','','','ID020048','VARCHAR2','','','Job Class Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','JOB_CLASS',91000,'Job Class','JOB_CLASS','','','','ID020048','VARCHAR2','','','Job Class','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','SUPERVISOR_ID',91000,'Supervisor Id','SUPERVISOR_ID','','','','ID020048','VARCHAR2','','','Supervisor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','ERROR_COMMENTS',91000,'Error Comments','ERROR_COMMENTS','','','','ID020048','VARCHAR2','','','Error Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','SESSION_ID',91000,'Session Id','SESSION_ID','','','','ID020048','VARCHAR2','','','Session Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','MSG_ID',91000,'Msg Id','MSG_ID','','','','ID020048','VARCHAR2','','','Msg Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','LAST_UPDATE_LOGIN',91000,'Last Update Login','LAST_UPDATE_LOGIN','','','','ID020048','VARCHAR2','','','Last Update Login','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','LAST_UPDATE_DATE',91000,'Last Update Date','LAST_UPDATE_DATE','','','','ID020048','VARCHAR2','','','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','CREATED_BY',91000,'Created By','CREATED_BY','','','','ID020048','VARCHAR2','','','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','CREATION_DATE',91000,'Creation Date','CREATION_DATE','','','','ID020048','VARCHAR2','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','NUMBER_OF_RECORDS',91000,'Number Of Records','NUMBER_OF_RECORDS','','','','ID020048','VARCHAR2','','','Number Of Records','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','PROCESS_FLAG',91000,'Process Flag','PROCESS_FLAG','','','','ID020048','VARCHAR2','','','Process Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','ATTRIBUTE9',91000,'Attribute9','ATTRIBUTE9','','','','ID020048','VARCHAR2','','','Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','ATTRIBUTE8',91000,'Attribute8','ATTRIBUTE8','','','','ID020048','VARCHAR2','','','Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','ATTRIBUTE7',91000,'Attribute7','ATTRIBUTE7','','','','ID020048','VARCHAR2','','','Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','ATTRIBUTE6',91000,'Attribute6','ATTRIBUTE6','','','','ID020048','VARCHAR2','','','Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','ATTRIBUTE5',91000,'Attribute5','ATTRIBUTE5','','','','ID020048','VARCHAR2','','','Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','ATTRIBUTE4',91000,'Attribute4','ATTRIBUTE4','','','','ID020048','VARCHAR2','','','Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','ATTRIBUTE3',91000,'Attribute3','ATTRIBUTE3','','','','ID020048','NUMBER','','','Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','ATTRIBUTE2',91000,'Attribute2','ATTRIBUTE2','','','','ID020048','VARCHAR2','','','Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','ATTRIBUTE1',91000,'Attribute1','ATTRIBUTE1','','','','ID020048','VARCHAR2','','','Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','WORK_TELEPHONE',91000,'Work Telephone','WORK_TELEPHONE','','','','ID020048','VARCHAR2','','','Work Telephone','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','EMAIL_ADDRESS',91000,'Email Address','EMAIL_ADDRESS','','','','ID020048','VARCHAR2','','','Email Address','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','PERSON_TYPE',91000,'Person Type','PERSON_TYPE','','','','ID020048','VARCHAR2','','','Person Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','SEX',91000,'Sex','SEX','','','','ID020048','VARCHAR2','','','Sex','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','EFFECTIVE_START_DATE',91000,'Effective Start Date','EFFECTIVE_START_DATE','','','','ID020048','VARCHAR2','','','Effective Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','HIRE_DATE',91000,'Hire Date','HIRE_DATE','','','','ID020048','VARCHAR2','','','Hire Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','MIDDLE_NAMES',91000,'Middle Names','MIDDLE_NAMES','','','','ID020048','VARCHAR2','','','Middle Names','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','FIRST_NAME',91000,'First Name','FIRST_NAME','','','','ID020048','VARCHAR2','','','First Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','LAST_NAME',91000,'Last Name','LAST_NAME','','','','ID020048','VARCHAR2','','','Last Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','EMPLOYEE_NUMBER',91000,'Employee Number','EMPLOYEE_NUMBER','','','','ID020048','VARCHAR2','','','Employee Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','STATUS',91000,'Status','STATUS','','','','ID020048','VARCHAR2','','','Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXCUSHR_PS_EMP_ALL_TBL','TRX_ID',91000,'Trx Id','TRX_ID','','','','ID020048','VARCHAR2','','','Trx Id','','','','US');
--Inserting Object Components for XXCUSHR_PS_EMP_ALL_TBL
--Inserting Object Component Joins for XXCUSHR_PS_EMP_ALL_TBL
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
prompt Creating Report LOV Data for Out of Pocket Transactions (No Mileage)
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Out of Pocket Transactions (No Mileage)
xxeis.eis_rsc_ins.lov( 91000,'select DISTINCT PERIOD_NAME from XXCUS.XXCUS_BULLET_IEXP_TBL','','XXCUS_PERIOD_NAME T&E','Oracle Period Date from the bullet train table','ID020048',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 91000,'select DISTINCT ORACLE_PRODUCT from XXCUS.XXCUS_BULLET_IEXP_TBL','','XXCUS_ORACLE_PRODUCT T&E','Oracle product number','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
prompt Creating Report Data for Out of Pocket Transactions (No Mileage)
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Out of Pocket Transactions (No Mileage)
xxeis.eis_rsc_utility.delete_report_rows( 'Out of Pocket Transactions (No Mileage)' );
--Inserting Report - Out of Pocket Transactions (No Mileage)
xxeis.eis_rsc_ins.r( 91000,'Out of Pocket Transactions (No Mileage)','','Displays all out of pocket transactions, not including mileage reimbursements','','','','ID020048','XXCUS_BULLET_IEXP_TBL','Y','','','ID020048','','N','User Modified Reports','','CSV,Pivot Excel,EXCEL,','','','','','','','','','US','','','','');
--Inserting Report Columns - Out of Pocket Transactions (No Mileage)
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'ITEM_DESCRIPTION','Item Description','','','','default','','8','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'TRANSACTION_DATE','Transaction Date','','','','default','','5','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'FULL_NAME','Full Name','','','','default','','2','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'JUSTIFICATION','Justification','','','','default','','15','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'MERCHANT_NAME','Merchant Name','','','','default','','10','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'CATEGORY','Category','','','','default','','22','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','AP_CREDIT_CARD_TRXNS_ALL','ACCTA','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'MCC_CODE','Mcc Code','','','','default','','13','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'SUB_CATEGORY','Sub Category','','','','default','','9','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'MCC_CODE_NO','Mcc Code No','','','','default','','14','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'EXPENSE_REPORT_NUMBER','Expense Report Number','','','~T~D~0','default','','4','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'MERCHANT_LOCATION','Merchant Location','','','','default','','11','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'BUSINESS_PURPOSE','Business Purpose','','','','default','','16','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'REMARKS','Remarks','','','','default','','17','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'ATTENDEES','Attendees','','','','default','','18','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'APPROVER_NAME','Approver Name','','','','default','','19','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'QUERY_DESCR','Query Descr','','','','default','','20','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'CARDMEMBER_NAME','Cardmember Name','','','','default','','22','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'CARD_PROGRAM_NAME','Card Program Name','','','','default','','21','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'DOW','DOW','','VARCHAR2','','default','','6','Y','','','','','','','to_char(xbit.transaction_date, ''Day'')','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'LINE_AMOUNT','Amount Spent','','','$~T~D~2','default','','7','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'LOB_NAME','LOB Name','','','','default','','1','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','XXEIS_LOB_NAMES_V','XLNV','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'EMPLOYEE_NUMBER','Employee Number','','','','default','','3','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'JOB_DESCR','Job Descr','Job Descr','','','','','23','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','XXCUSHR_PS_EMP_ALL_TBL','XPEAT','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'JOB_FAMILY','Job Family','Job Family','','','','','24','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','XXCUSHR_PS_EMP_ALL_TBL','XPEAT','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'JOB_FAMILY_DESCR','Job Family Descr','Job Family Descr','','','','','25','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','XXCUSHR_PS_EMP_ALL_TBL','XPEAT','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Out of Pocket Transactions (No Mileage)',91000,'ORACLE_COST_CENTER','Oracle Cost Center','','','','','','12','N','','','','','','','','ID020048','N','N','','XXCUS_BULLET_IEXP_TBL','','','GROUP_BY','US','');
--Inserting Report Parameters - Out of Pocket Transactions (No Mileage)
xxeis.eis_rsc_ins.rp( 'Out of Pocket Transactions (No Mileage)',91000,'Fiscal_Period','','PERIOD_NAME','IN','XXCUS_PERIOD_NAME T&E','','VARCHAR2','N','Y','2','N','Y','CONSTANT','ID020048','Y','N','','','','XXCUS_BULLET_IEXP_TBL','','','US','');
xxeis.eis_rsc_ins.rp( 'Out of Pocket Transactions (No Mileage)',91000,'Oracle_Product','','ORACLE_PRODUCT','IN','XXCUS_ORACLE_PRODUCT T&E','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','ID020048','Y','N','','','','XXCUS_BULLET_IEXP_TBL','','','US','');
xxeis.eis_rsc_ins.rp( 'Out of Pocket Transactions (No Mileage)',91000,'LOB','','','IN','','','VARCHAR2','N','N','3','N','N','CONSTANT','ID020048','N','N','','','','XXCUS_BULLET_IEXP_TBL','','','US','');
--Inserting Dependent Parameters - Out of Pocket Transactions (No Mileage)
--Inserting Report Conditions - Out of Pocket Transactions (No Mileage)
xxeis.eis_rsc_ins.rcnh( 'Out of Pocket Transactions (No Mileage)',91000,'LINE_AMOUNT > 0 ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','LINE_AMOUNT','','','','','','','XXCUS_BULLET_IEXP_TBL','','','','','','GREATER_THAN','Y','N','','0','','','1',91000,'Out of Pocket Transactions (No Mileage)','LINE_AMOUNT > 0 ');
xxeis.eis_rsc_ins.rcnh( 'Out of Pocket Transactions (No Mileage)',91000,'ORACLE_PRODUCT IN :Oracle_Product ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORACLE_PRODUCT','','Oracle_Product','','','','','XXCUS_BULLET_IEXP_TBL','','','','','','IN','Y','Y','','','','','1',91000,'Out of Pocket Transactions (No Mileage)','ORACLE_PRODUCT IN :Oracle_Product ');
xxeis.eis_rsc_ins.rcnh( 'Out of Pocket Transactions (No Mileage)',91000,'ORACLE_PRODUCT IN :oracle_product ','ADVANCED','','1#$#','N','');
xxeis.eis_rsc_ins.rcnd( '','','ORACLE_PRODUCT','','','','','','','XXCUS_BULLET_IEXP_TBL','','','','','','IN','Y','Y','',':oracle_product','','','1',91000,'Out of Pocket Transactions (No Mileage)','ORACLE_PRODUCT IN :oracle_product ');
xxeis.eis_rsc_ins.rcnh( 'Out of Pocket Transactions (No Mileage)',91000,'PERIOD_NAME IN :Fiscal_Period ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PERIOD_NAME','','Fiscal_Period','','','','','XXCUS_BULLET_IEXP_TBL','','','','','','IN','Y','Y','','','','','1',91000,'Out of Pocket Transactions (No Mileage)','PERIOD_NAME IN :Fiscal_Period ');
xxeis.eis_rsc_ins.rcnh( 'Out of Pocket Transactions (No Mileage)',91000,'PERIOD_NAME IN :fiscal_period ','ADVANCED','','1#$#','N','');
xxeis.eis_rsc_ins.rcnd( '','','PERIOD_NAME','','','','','','','XXCUS_BULLET_IEXP_TBL','','','','','','IN','Y','Y','',':fiscal_period','','','1',91000,'Out of Pocket Transactions (No Mileage)','PERIOD_NAME IN :fiscal_period ');
xxeis.eis_rsc_ins.rcnh( 'Out of Pocket Transactions (No Mileage)',91000,'QUERY_NUM IN (''4'', ''6'') ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','QUERY_NUM','','','','','','','XXCUS_BULLET_IEXP_TBL','','','','','','IN','Y','N','','(''4'', ''6'')','','','1',91000,'Out of Pocket Transactions (No Mileage)','QUERY_NUM IN (''4'', ''6'') ');
xxeis.eis_rsc_ins.rcnh( 'Out of Pocket Transactions (No Mileage)',91000,'XLNV.LOB_NAME IN :LOB ','ADVANCED','','1#$#','N','');
xxeis.eis_rsc_ins.rcnd( '','','','','LOB','','','','','','','','','','','IN','Y','Y','XLNV.LOB_NAME','','','','1',91000,'Out of Pocket Transactions (No Mileage)','XLNV.LOB_NAME IN :LOB ');
xxeis.eis_rsc_ins.rcnh( 'Out of Pocket Transactions (No Mileage)',91000,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND (ACCTA.CATEGORY NOT IN (''personal'') OR ACCTA.CATEGORY IS NULL) 
AND (upper(xbit.sub_category) NOT LIKE (''%MILEAGE%''))
AND (upper(xbit.sub_category) NOT LIKE (''%TIPS%''))
AND (upper(xbit.sub_category) NOT LIKE (''%TOLLS%''))
AND (upper(xbit.sub_category) NOT LIKE (''%TELEPHONE%''))
AND (upper(xbit.sub_category) NOT LIKE (''%PARKING%''))
AND (upper(xbit.item_description) NOT LIKE (''%MILEAGE%''))
AND (upper(xbit.item_description) NOT LIKE (''%TIPS%''))
AND (upper(xbit.item_description) NOT LIKE (''%TOLLS%''))
AND (upper(xbit.item_description) NOT LIKE (''%TELEPHONE%''))
AND (upper(xbit.item_description) NOT LIKE (''%PARKING%''))','1',91000,'Out of Pocket Transactions (No Mileage)','Free Text ');
--Inserting Report Sorts - Out of Pocket Transactions (No Mileage)
xxeis.eis_rsc_ins.rs( 'Out of Pocket Transactions (No Mileage)',91000,'LOB_NAME','ASC','ID020048','1','');
xxeis.eis_rsc_ins.rs( 'Out of Pocket Transactions (No Mileage)',91000,'FULL_NAME','ASC','ID020048','2','');
xxeis.eis_rsc_ins.rs( 'Out of Pocket Transactions (No Mileage)',91000,'LINE_AMOUNT','DESC','ID020048','3','');
--Inserting Report Triggers - Out of Pocket Transactions (No Mileage)
--inserting report templates - Out of Pocket Transactions (No Mileage)
--Inserting Report Portals - Out of Pocket Transactions (No Mileage)
--inserting report dashboards - Out of Pocket Transactions (No Mileage)
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Out of Pocket Transactions (No Mileage)','91000','XXCUS_BULLET_IEXP_TBL','XXCUS_BULLET_IEXP_TBL','N','');
xxeis.eis_rsc_ins.rviews( 'Out of Pocket Transactions (No Mileage)','91000','XXCUS_BULLET_IEXP_TBL','AP_CREDIT_CARD_TRXNS_ALL','N','ACCTA');
xxeis.eis_rsc_ins.rviews( 'Out of Pocket Transactions (No Mileage)','91000','XXCUS_BULLET_IEXP_TBL','XXEIS_LOB_NAMES_V','N','XLNV');
xxeis.eis_rsc_ins.rviews( 'Out of Pocket Transactions (No Mileage)','91000','XXCUS_BULLET_IEXP_TBL','XXCUSHR_PS_EMP_ALL_TBL','N','XPEAT');
--inserting report security - Out of Pocket Transactions (No Mileage)
xxeis.eis_rsc_ins.rsec( 'Out of Pocket Transactions (No Mileage)','','ID020048','',91000,'ID020048','','','');
xxeis.eis_rsc_ins.rsec( 'Out of Pocket Transactions (No Mileage)','','JG023358','',91000,'ID020048','','','');
xxeis.eis_rsc_ins.rsec( 'Out of Pocket Transactions (No Mileage)','200','','XXWC_PAYABLES_INQUIRY',91000,'ID020048','','','');
--Inserting Report Pivots - Out of Pocket Transactions (No Mileage)
--Inserting Report   Version details- Out of Pocket Transactions (No Mileage)
xxeis.eis_rsc_ins.rv( 'Out of Pocket Transactions (No Mileage)','','Out of Pocket Transactions (No Mileage)','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
