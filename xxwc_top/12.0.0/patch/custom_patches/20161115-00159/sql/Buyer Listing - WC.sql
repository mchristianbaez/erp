--Report Name            : Buyer Listing - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_WC_PO_BUYER_LISTING_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_WC_PO_BUYER_LISTING_V
xxeis.eis_rsc_ins.v( 'XXEIS_WC_PO_BUYER_LISTING_V',201,'','','','','MR020532','XXEIS','Xxeis Wc Po Buyer Listing V','XWPBLV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_WC_PO_BUYER_LISTING_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_WC_PO_BUYER_LISTING_V',201,FALSE);
--Inserting Object Columns for XXEIS_WC_PO_BUYER_LISTING_V
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','C_NO',201,'C No','C_NO','','','','MR020532','VARCHAR2','','','C No','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','C_YES',201,'C Yes','C_YES','','','','MR020532','VARCHAR2','','','C Yes','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','C_CATEGORY_SET_ID',201,'C Category Set Id','C_CATEGORY_SET_ID','','','','MR020532','NUMBER','','','C Category Set Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','STRUCTURE_CAT',201,'Structure Cat','STRUCTURE_CAT','','','','MR020532','NUMBER','','','Structure Cat','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','STRUCTURE_ACC',201,'Structure Acc','STRUCTURE_ACC','','','','MR020532','NUMBER','','','Structure Acc','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','GL_CURRENCY',201,'Gl Currency','GL_CURRENCY','','','','MR020532','VARCHAR2','','','Gl Currency','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','C_ORGANIZATION_ID',201,'C Organization Id','C_ORGANIZATION_ID','','','','MR020532','NUMBER','','','C Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','C_COMPANY',201,'C Company','C_COMPANY','','','','MR020532','VARCHAR2','','','C Company','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','MCA_SEGMENT15',201,'Mca Segment15','MCA_SEGMENT15','','','','MR020532','VARCHAR2','','','Mca Segment15','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','MCA_SEGMENT14',201,'Mca Segment14','MCA_SEGMENT14','','','','MR020532','VARCHAR2','','','Mca Segment14','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','MCA_SEGMENT13',201,'Mca Segment13','MCA_SEGMENT13','','','','MR020532','VARCHAR2','','','Mca Segment13','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','MCA_SEGMENT12',201,'Mca Segment12','MCA_SEGMENT12','','','','MR020532','VARCHAR2','','','Mca Segment12','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','MCA_SEGMENT11',201,'Mca Segment11','MCA_SEGMENT11','','','','MR020532','VARCHAR2','','','Mca Segment11','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','MCA_SEGMENT10',201,'Mca Segment10','MCA_SEGMENT10','','','','MR020532','VARCHAR2','','','Mca Segment10','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','MCA_SEGMENT9',201,'Mca Segment9','MCA_SEGMENT9','','','','MR020532','VARCHAR2','','','Mca Segment9','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','MCA_SEGMENT8',201,'Mca Segment8','MCA_SEGMENT8','','','','MR020532','VARCHAR2','','','Mca Segment8','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','MCA_SEGMENT7',201,'Mca Segment7','MCA_SEGMENT7','','','','MR020532','VARCHAR2','','','Mca Segment7','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','MCA_SEGMENT6',201,'Mca Segment6','MCA_SEGMENT6','','','','MR020532','VARCHAR2','','','Mca Segment6','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','MCA_SEGMENT5',201,'Mca Segment5','MCA_SEGMENT5','','','','MR020532','VARCHAR2','','','Mca Segment5','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','MCA_SEGMENT4',201,'Mca Segment4','MCA_SEGMENT4','','','','MR020532','VARCHAR2','','','Mca Segment4','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','MCA_SEGMENT3',201,'Mca Segment3','MCA_SEGMENT3','','','','MR020532','VARCHAR2','','','Mca Segment3','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','MCA_SEGMENT2',201,'Mca Segment2','MCA_SEGMENT2','','','','MR020532','VARCHAR2','','','Mca Segment2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','MCA_SEGMENT1',201,'Mca Segment1','MCA_SEGMENT1','','','','MR020532','VARCHAR2','','','Mca Segment1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','MCA_STRUCTURE_ID',201,'Mca Structure Id','MCA_STRUCTURE_ID','','','','MR020532','NUMBER','','','Mca Structure Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','MCA_PADDED_CONCAT_SEGMENTS',201,'Mca Padded Concat Segments','MCA_PADDED_CONCAT_SEGMENTS','','','','MR020532','VARCHAR2','','','Mca Padded Concat Segments','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','C_FLEX_CAT',201,'C Flex Cat','C_FLEX_CAT','','','','MR020532','VARCHAR2','','','C Flex Cat','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','POA_END_DATE',201,'Poa End Date','POA_END_DATE','','','','MR020532','DATE','','','Poa End Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','POA_START_DATE',201,'Poa Start Date','POA_START_DATE','','','','MR020532','DATE','','','Poa Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','POA_PROGRAM_UPDATE_DATE',201,'Poa Program Update Date','POA_PROGRAM_UPDATE_DATE','','','','MR020532','DATE','','','Poa Program Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','POA_AUTHORIZATION_LIMIT',201,'Poa Authorization Limit','POA_AUTHORIZATION_LIMIT','','','','MR020532','NUMBER','','','Poa Authorization Limit','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','POA_AGENT_ID',201,'Poa Agent Id','POA_AGENT_ID','','','','MR020532','NUMBER','','','Poa Agent Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_TELEPHONE_NUMBER_2',201,'Location Telephone Number 2','LOCATION_TELEPHONE_NUMBER_2','','','','MR020532','VARCHAR2','','','Location Telephone Number 2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_TELEPHONE_NUMBER_1',201,'Location Telephone Number 1','LOCATION_TELEPHONE_NUMBER_1','','','','MR020532','VARCHAR2','','','Location Telephone Number 1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_REGION_3',201,'Location Region 3','LOCATION_REGION_3','','','','MR020532','VARCHAR2','','','Location Region 3','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_REGION_2',201,'Location Region 2','LOCATION_REGION_2','','','','MR020532','VARCHAR2','','','Location Region 2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_REGION_1',201,'Location Region 1','LOCATION_REGION_1','','','','MR020532','VARCHAR2','','','Location Region 1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_POSTAL_CODE',201,'Location Postal Code','LOCATION_POSTAL_CODE','','','','MR020532','VARCHAR2','','','Location Postal Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_COUNTRY',201,'Location Country','LOCATION_COUNTRY','','','','MR020532','VARCHAR2','','','Location Country','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_TOWN_OR_CITY',201,'Location Town Or City','LOCATION_TOWN_OR_CITY','','','','MR020532','VARCHAR2','','','Location Town Or City','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_ADDRESS_LINE_3',201,'Location Address Line 3','LOCATION_ADDRESS_LINE_3','','','','MR020532','VARCHAR2','','','Location Address Line 3','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_ADDRESS_LINE_2',201,'Location Address Line 2','LOCATION_ADDRESS_LINE_2','','','','MR020532','VARCHAR2','','','Location Address Line 2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_ADDRESS_LINE_1',201,'Location Address Line 1','LOCATION_ADDRESS_LINE_1','','','','MR020532','VARCHAR2','','','Location Address Line 1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_STYLE',201,'Location Style','LOCATION_STYLE','','','','MR020532','VARCHAR2','','','Location Style','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_OFFICE_SITE_FLAG',201,'Location Office Site Flag','LOCATION_OFFICE_SITE_FLAG','','','','MR020532','VARCHAR2','','','Location Office Site Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_IN_ORGANIZATION_FLAG',201,'Location In Organization Flag','LOCATION_IN_ORGANIZATION_FLAG','','','','MR020532','VARCHAR2','','','Location In Organization Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_BILL_TO_SITE_FLAG',201,'Location Bill To Site Flag','LOCATION_BILL_TO_SITE_FLAG','','','','MR020532','VARCHAR2','','','Location Bill To Site Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_RECEIVING_SITE_FLAG',201,'Location Receiving Site Flag','LOCATION_RECEIVING_SITE_FLAG','','','','MR020532','VARCHAR2','','','Location Receiving Site Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_SHIP_TO_SITE_FLAG',201,'Location Ship To Site Flag','LOCATION_SHIP_TO_SITE_FLAG','','','','MR020532','VARCHAR2','','','Location Ship To Site Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_DESCRIPTION',201,'Location Description','LOCATION_DESCRIPTION','','','','MR020532','VARCHAR2','','','Location Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_CODE',201,'Location Code','LOCATION_CODE','','','','MR020532','VARCHAR2','','','Location Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','LOCATION_ID',201,'Location Id','LOCATION_ID','','','','MR020532','NUMBER','','','Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','BUYER_EMAIL_ADDRESS',201,'Buyer Email Address','BUYER_EMAIL_ADDRESS','','','','MR020532','VARCHAR2','','','Buyer Email Address','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','BUYER_EXPENSE_CHECK_ADDR_FLAG',201,'Buyer Expense Check Addr Flag','BUYER_EXPENSE_CHECK_ADDR_FLAG','','','','MR020532','VARCHAR2','','','Buyer Expense Check Addr Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','BUYER_PREFIX',201,'Buyer Prefix','BUYER_PREFIX','','','','MR020532','VARCHAR2','','','Buyer Prefix','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','BUYER_LAST_NAME',201,'Buyer Last Name','BUYER_LAST_NAME','','','','MR020532','VARCHAR2','','','Buyer Last Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','BUYER_MIDDLE_NAME',201,'Buyer Middle Name','BUYER_MIDDLE_NAME','','','','MR020532','VARCHAR2','','','Buyer Middle Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','BUYER_FIRST_NAME',201,'Buyer First Name','BUYER_FIRST_NAME','','','','MR020532','VARCHAR2','','','Buyer First Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','BUYER_FULL_NAME',201,'Buyer Full Name','BUYER_FULL_NAME','','','','MR020532','VARCHAR2','','','Buyer Full Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','BUYER_EMPLOYEE_NUM',201,'Buyer Employee Num','BUYER_EMPLOYEE_NUM','','','','MR020532','VARCHAR2','','','Buyer Employee Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','BUYER_EMPLOYEE_ID',201,'Buyer Employee Id','BUYER_EMPLOYEE_ID','','','','MR020532','NUMBER','','','Buyer Employee Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','EMPLOYEE_TITLE',201,'Employee Title','EMPLOYEE_TITLE','','','','MR020532','VARCHAR2','','','Employee Title','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','STATUS',201,'Status','STATUS','','','','MR020532','CHAR','','','Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_PO_BUYER_LISTING_V','PO_APPROVAL_LEVEL',201,'Po Approval Level','PO_APPROVAL_LEVEL','','','','MR020532','VARCHAR2','','','Po Approval Level','','','','US');
--Inserting Object Components for XXEIS_WC_PO_BUYER_LISTING_V
--Inserting Object Component Joins for XXEIS_WC_PO_BUYER_LISTING_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report LOV Data for Buyer Listing - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Buyer Listing - WC
xxeis.eis_rsc_ins.lov( 201,'select distinct location_code from hr_locations_all','','EIS_LOCATIONS_LOV','This LOV lists all the locations','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT distinct concatenated_segments item_category from mtl_categories_kfv ORDER BY concatenated_segments','','EIS_PO_ITEM_CATEGORIES_LOV','Item categories for Purchasing User','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT DISPLAYED_FIELD FROM PO_LOOKUP_CODES WHERE LOOKUP_TYPE = ''ACTIVE_INACTIVE''
ORDER BY DISPLAYED_FIELD','','XXEIS_PO_ACT_INACTIVE_OPT_LOV','XXEIS_PO_ACT_INACTIVE_OPT_LOV','MR020532',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report Data for Buyer Listing - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Buyer Listing - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Buyer Listing - WC' );
--Inserting Report - Buyer Listing - WC
xxeis.eis_rsc_ins.r( 201,'Buyer Listing - WC','','Buyer Listing - WC','','','','MR020532','XXEIS_WC_PO_BUYER_LISTING_V','Y','','','MR020532','','N','White Cap Reports','','CSV,EXCEL,Pivot Excel,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - Buyer Listing - WC
xxeis.eis_rsc_ins.rc( 'Buyer Listing - WC',201,'BUYER_FULL_NAME','Buyer','Buyer Full Name','','','default','','2','N','','','','','','','','MR020532','N','N','','XXEIS_WC_PO_BUYER_LISTING_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Buyer Listing - WC',201,'C_COMPANY','Company Name','C Company','','','default','','1','N','','','','','','','','MR020532','N','N','','XXEIS_WC_PO_BUYER_LISTING_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Buyer Listing - WC',201,'C_FLEX_CAT','Category','C Flex Cat','','','default','','5','N','','','','','','','','MR020532','N','N','','XXEIS_WC_PO_BUYER_LISTING_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Buyer Listing - WC',201,'LOCATION_CODE','Ship To Location','Location Code','','','default','','6','N','','','','','','','','MR020532','N','N','','XXEIS_WC_PO_BUYER_LISTING_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Buyer Listing - WC',201,'POA_END_DATE','End Date','Poa End Date','','','default','','8','N','','','','','','','','MR020532','N','N','','XXEIS_WC_PO_BUYER_LISTING_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Buyer Listing - WC',201,'POA_START_DATE','Start Date','Poa Start Date','','','default','','7','N','','','','','','','','MR020532','N','N','','XXEIS_WC_PO_BUYER_LISTING_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Buyer Listing - WC',201,'EMPLOYEE_TITLE','Employee Title','Employee Title','','','','','3','N','','','','','','','','MR020532','N','N','','XXEIS_WC_PO_BUYER_LISTING_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Buyer Listing - WC',201,'STATUS','Status','Status','','','','','9','N','','','','','','','','MR020532','N','N','','XXEIS_WC_PO_BUYER_LISTING_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Buyer Listing - WC',201,'PO_APPROVAL_LEVEL','Po Approval Level','Po Approval Level','','','','','4','N','','','','','','','','MR020532','N','N','','XXEIS_WC_PO_BUYER_LISTING_V','','','GROUP_BY','US','');
--Inserting Report Parameters - Buyer Listing - WC
xxeis.eis_rsc_ins.rp( 'Buyer Listing - WC',201,'Active Status','','','IN','XXEIS_PO_ACT_INACTIVE_OPT_LOV','','VARCHAR2','Y','Y','1','','N','CONSTANT','MR020532','Y','N','','','','XXEIS_WC_PO_BUYER_LISTING_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Buyer Listing - WC',201,'Location Name','Name of Location','LOCATION_CODE','IN','EIS_LOCATIONS_LOV','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','MR020532','Y','N','','','','XXEIS_WC_PO_BUYER_LISTING_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Buyer Listing - WC',201,'Item Category','Item Category','MCA_SEGMENT1','IN','EIS_PO_ITEM_CATEGORIES_LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','MR020532','Y','N','','','','XXEIS_WC_PO_BUYER_LISTING_V','','','US','');
--Inserting Dependent Parameters - Buyer Listing - WC
--Inserting Report Conditions - Buyer Listing - WC
xxeis.eis_rsc_ins.rcnh( 'Buyer Listing - WC',201,'LOCATION_CODE IN :Location Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','LOCATION_CODE','','Location Name','','','','','XXEIS_WC_PO_BUYER_LISTING_V','','','','','','IN','Y','Y','','','','','1',201,'Buyer Listing - WC','LOCATION_CODE IN :Location Name ');
xxeis.eis_rsc_ins.rcnh( 'Buyer Listing - WC',201,'MCA_SEGMENT1 IN :Item Category ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','MCA_SEGMENT1','','Item Category','','','','','XXEIS_WC_PO_BUYER_LISTING_V','','','','','','IN','Y','Y','','','','','1',201,'Buyer Listing - WC','MCA_SEGMENT1 IN :Item Category ');
xxeis.eis_rsc_ins.rcnh( 'Buyer Listing - WC',201,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND      ((UPPER(:Active Status) = ''BOTH'')

OR       (UPPER(:Active Status) = ''ACTIVE''

            AND sysdate   >= nvl(XWPBLV.poa_start_date,sysdate - 1)

            AND sysdate   <  nvl(XWPBLV.poa_end_date,sysdate + 1))

OR       (UPPER(:Active Status) = ''INACTIVE''

            AND (sysdate  <  nvl(XWPBLV.poa_start_date,sysdate - 1)

            OR  sysdate   >= nvl(XWPBLV.poa_end_date,sysdate + 1))))','1',201,'Buyer Listing - WC','Free Text ');
--Inserting Report Sorts - Buyer Listing - WC
--Inserting Report Triggers - Buyer Listing - WC
--inserting report templates - Buyer Listing - WC
--Inserting Report Portals - Buyer Listing - WC
--inserting report dashboards - Buyer Listing - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Buyer Listing - WC','201','XXEIS_WC_PO_BUYER_LISTING_V','XXEIS_WC_PO_BUYER_LISTING_V','N','');
--inserting report security - Buyer Listing - WC
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','200','','XXWC_PAY_VENDOR_MSTR',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','200','','XXWC_PAY_VENDOR_BANK_DETAILS',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','200','','XXWC_PAY_MANAGER',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','200','','HDS_AP_MGR_NOSUP_US_IWO',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','200','','HDS_AP_MGR_NOSUP_US_GSC',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','200','','HDS_PYBLS_MNGR',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','200','','HDS_PYABLS_MNGR_CAN',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','200','','HDS_AP_MANAGER',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','200','','XXWC_PAYABLES_INQUIRY',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','200','','XXWC_PAY_DISBURSE',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','200','','HDS_PAYABLES_CLOSE_GLBL',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','200','','XXWC_PAY_W_CALENDAR',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','200','','XXWC_PAY_NO_CALENDAR',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','201','','XXWC_PUR_SUPER_USER',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','201','','HDS_PRCHSNG_SPR_USR',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','201','','XXWC_PURCHASING_SR_MRG_WC',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','201','','XXWC_PURCHASING_RPM',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','201','','XXWC_PURCHASING_MGR',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','201','','XXWC_PURCHASING_INQUIRY',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','201','','XXWC_PURCHASING_BUYER',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','20005','','XXWC_VIEW_ALL_EIS_REPORTS',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Buyer Listing - WC','201','','PURCHASING_SUPER_USER',201,'MR020532','','','');
--Inserting Report Pivots - Buyer Listing - WC
xxeis.eis_rsc_ins.rpivot( 'Buyer Listing - WC',201,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Buyer Listing - WC',201,'Pivot','LOCATION_CODE','PAGE_FIELD','','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Buyer Listing - WC',201,'Pivot','C_FLEX_CAT','PAGE_FIELD','','','2','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Buyer Listing - WC',201,'Pivot','BUYER_FULL_NAME','DATA_FIELD','COUNT','','1','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Buyer Listing - WC',201,'Pivot','C_COMPANY','ROW_FIELD','','','1','1','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- Buyer Listing - WC
xxeis.eis_rsc_ins.rv( 'Buyer Listing - WC','','Buyer Listing - WC','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
