---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.hds_eis_ap_invoice_details $
  REVISIONS   :
  VERSION 		DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.1     	   1-Dec-2016        	Siva   		 TMS#20161115-00159  -- Developed new view for EiS upgrade Project
**************************************************************************************************************/
CREATE OR REPLACE VIEW XXEIS.HDS_EIS_AP_INVOICE_DETAILS
(
   invoice_id
  ,invoice_creation_date
  ,invoice_last_update_date
  ,invoice_created_by
  ,invoice_updated_by
  ,ledger_name
  ,operating_unit
  ,supplier
  ,supplier_site
  ,invoice_batch_name
  ,invoice_num
  ,invoice_date
  ,invoice_gl_date
  ,invoice_source
  ,invoice_type
  ,invoice_description
  ,invoice_currency_code
  ,payment_currency_code
  ,payment_cross_rate
  ,invoice_amount
  ,base_amount
  ,invoice_amount_paid
  ,discount_amount_taken
  ,amount_applicable_to_discount
  ,tax_amount
  ,pay_curr_invoice_amount
  ,payment_cross_rate_date
  ,invoice_terms
  ,terms_date
  ,invoice_pay_method
  ,payment_method_code
  ,invoice_pay_group
  ,accts_pay_code_combination_id
  ,liability_account
  ,invoice_payment_status
  ,invoice_base_amount
  ,approved_amount
  ,invoice_exclusive_payment
  ,invoice_cancelled_date
  ,invoice_cancelled_amount
  ,invoice_cancelled_by
  ,invoice_temp_cancelled_amount
  ,dist_accounting_date
  ,dist_accrual_posted
  ,dist_assets_addition
  ,dist_assets_tracking
  ,dist_cash_posted
  ,dist_line_number
  ,dist_code_combination_id
  ,expense_account
  ,dist_line_type
  ,period_name
  ,dist_creation_date
  ,dist_last_update_date
  ,dist_created_by
  ,dist_updated_by
  ,dist_amount
  ,dist_base_amount
  ,original_amount
  ,dist_base_inv_price_variance
  ,dist_description
  ,dist_final_match
  ,dist_invoice_price_variance
  ,dist_match_status
  ,dist_posted
  ,po_distribution_id
  ,quantity_invoiced
  ,dist_reversal
  ,unit_price
  ,dist_price_adjustment
  ,price_var_code_combination_id
  ,price_variance_account
  ,pa_expenditure_item_date
  ,pa_expenditure_type
  ,expenditure_organization_id
  ,expenditure_organization
  ,pa_addition_flag
  ,project_accounting_context
  ,line_number
  ,line_type
  ,line_description
  ,line_amount
  ,line_base_amount
  ,project_id
  ,task_id
  ,project_number
  ,project_name
  ,project_description
  ,project_type
  ,project_status_code
  ,project_start_date
  ,project_completion_date
  ,task_number
  ,task_name
  ,task_description
  ,service_type_code
  ,task_start_date
  ,task_completion_date
  ,chargeable_task
  ,billable_task
  ,dist_amount_includes_tax
  ,dist_tax_calculated_flag
  ,invoice_distribution_id
  ,parent_reversal_id
  ,dist_match_type
  ,dist_tax_recovery_override
  ,dist_tax_recoverable
  ,dist_tax_code_override
  ,dist_tax_name
  ,vendor_id
  ,batch_id
  ,invoice_line_number
  ,distribution_line_number
  ,vendor_number    
  ,gcc1#50328#product
  ,gcc1#50328#product#descr
  ,gcc2#50328#product
  ,gcc2#50328#product#descr
  ,gcc3#50328#product
  ,gcc3#50328#product#descr
  ,gcc1#50328#location
  ,gcc1#50328#location#descr
  ,gcc2#50328#location
  ,gcc2#50328#location#descr
  ,gcc3#50328#location
  ,gcc3#50328#location#descr
  ,gcc1#50328#cost_center
  ,gcc1#50328#cost_center#descr
  ,gcc2#50328#cost_center
  ,gcc2#50328#cost_center#descr
  ,gcc3#50328#cost_center
  ,gcc3#50328#cost_center#descr
  ,gcc1#50328#account
  ,gcc1#50328#account#descr
  ,gcc2#50328#account
  ,gcc2#50328#account#descr
  ,gcc3#50328#account
  ,gcc3#50328#account#descr
  ,gcc1#50328#project_code
  ,gcc1#50328#project_code#descr
  ,gcc2#50328#project_code
  ,gcc2#50328#project_code#descr
  ,gcc3#50328#project_code
  ,gcc3#50328#project_code#descr
  ,gcc1#50328#furture_use
  ,gcc1#50328#furture_use#descr
  ,gcc2#50328#furture_use
  ,gcc2#50328#furture_use#descr
  ,gcc3#50328#furture_use
  ,gcc3#50328#furture_use#descr
  ,gcc1#50328#future_use_2
  ,gcc1#50328#future_use_2#descr
  ,gcc2#50328#future_use_2
  ,gcc2#50328#future_use_2#descr
  ,gcc3#50328#future_use_2
  ,gcc3#50328#future_use_2#descr
  ,gcc1#50368#account
  ,gcc1#50368#account#descr
  ,gcc1#50368#department
  ,gcc1#50368#department#descr
  ,gcc1#50368#division
  ,gcc1#50368#division#descr
  ,gcc1#50368#future_use
  ,gcc1#50368#future_use#descr
  ,gcc1#50368#product
  ,gcc1#50368#product#descr
  ,gcc1#50368#subaccount
  ,gcc1#50368#subaccount#descr
  ,gcc2#50368#account
  ,gcc2#50368#account#descr
  ,gcc2#50368#department
  ,gcc2#50368#department#descr
  ,gcc2#50368#division
  ,gcc2#50368#division#descr
  ,gcc2#50368#future_use
  ,gcc2#50368#future_use#descr
  ,gcc2#50368#product
  ,gcc2#50368#product#descr
  ,gcc2#50368#subaccount
  ,gcc2#50368#subaccount#descr
  ,gcc3#50368#account
  ,gcc3#50368#account#descr
  ,gcc3#50368#department
  ,gcc3#50368#department#descr
  ,gcc3#50368#division
  ,gcc3#50368#division#descr
  ,gcc3#50368#future_use
  ,gcc3#50368#future_use#descr
  ,gcc3#50368#product
  ,gcc3#50368#product#descr
  ,gcc3#50368#subaccount
  ,gcc3#50368#subaccount#descr
)
AS
   SELECT api.invoice_id invoice_id
         ,TRUNC (api.creation_date) invoice_creation_date
         ,api.last_update_date invoice_last_update_date
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_user_name (api.created_by)
             invoice_created_by
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_user_name (
             api.last_updated_by)
             invoice_updated_by
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_sob_name (
             api.set_of_books_id)
             ledger_name
         ,hou.name operating_unit
         ,pov.vendor_name supplier
         ,povs.vendor_site_code supplier_site
         ,apb.batch_name invoice_batch_name
         ,api.invoice_num
         ,api.invoice_date
         ,api.gl_date invoice_gl_date
         ,api.source invoice_source
         ,api.invoice_type_lookup_code invoice_type
         ,REPLACE (api.description, '~', '-') invoice_description
         ,api.invoice_currency_code
         ,api.payment_currency_code
         ,api.payment_cross_rate
         ,api.invoice_amount
         ,NVL (api.invoice_amount, api.base_amount) base_amount
         ,api.amount_paid invoice_amount_paid
         ,api.discount_amount_taken
         ,api.amount_applicable_to_discount
         ,api.tax_amount
         ,api.pay_curr_invoice_amount
         ,api.payment_cross_rate_date
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_ap_terms (api.terms_id)
             invoice_terms
         ,api.terms_date
         ,iby.payment_method_name invoice_pay_method
         ,api.payment_method_code payment_method_code
         ,api.pay_group_lookup_code invoice_pay_group
         ,api.accts_pay_code_combination_id
         , --         xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_gl_account(api.accts_pay_code_combination_id) invoice_payables_account,
          gcc1.concatenated_segments liability_account
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_yes_no (
             api.payment_status_flag)
             invoice_payment_status
         ,NVL (api.invoice_amount, api.base_amount) invoice_base_amount
         ,api.approved_amount
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_yes_no (
             api.exclusive_payment_flag)
             invoice_exclusive_payment
         ,api.cancelled_date invoice_cancelled_date
         ,api.cancelled_amount invoice_cancelled_amount
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_user_name (
             api.cancelled_by)
             invoice_cancelled_by
         ,api.temp_cancelled_amount invoice_temp_cancelled_amount
         ,                                           --api.auto_tax_calc_flag,
          apid.accounting_date dist_accounting_date
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_yes_no (
             apid.accrual_posted_flag)
             dist_accrual_posted
         ,apid.assets_addition_flag dist_assets_addition
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_yes_no (
             apid.assets_tracking_flag)
             dist_assets_tracking
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_yes_no (
             apid.cash_posted_flag)
             dist_cash_posted
         ,apid.distribution_line_number dist_line_number
         ,apid.dist_code_combination_id
         , --          xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_gl_account(apid.dist_code_combination_id) dist_payables_account,
          gcc2.concatenated_segments expense_account
         ,apid.line_type_lookup_code dist_line_type
         ,apid.period_name
         ,apid.creation_date dist_creation_date
         ,apid.last_update_date dist_last_update_date
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_user_name (
             apid.created_by)
             dist_created_by
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_user_name (
             apid.last_updated_by)
             dist_updated_by
         ,apid.amount dist_amount
         ,NVL (apid.amount, apid.base_amount) dist_base_amount
         ,NVL (NVL (apid.amount, apid.base_amount)
              ,NVL (apl.amount, apl.base_amount))
             original_amount
         ,apid.base_invoice_price_variance dist_base_inv_price_variance
         ,REPLACE (apid.description, '~', '-') dist_description
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_yes_no (
             apid.final_match_flag)
             dist_final_match
         ,apid.invoice_price_variance dist_invoice_price_variance
         ,apid.match_status_flag dist_match_status
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_yes_no (apid.posted_flag)
             dist_posted
         ,apid.po_distribution_id
         ,apid.quantity_invoiced
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_yes_no (
             apid.reversal_flag)
             dist_reversal
         ,apid.unit_price
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_yes_no (
             apid.price_adjustment_flag)
             dist_price_adjustment
         ,apid.price_var_code_combination_id
         , --          xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_gl_account(apid.price_var_code_combination_id) dist_price_variance_account,
          gcc3.concatenated_segments price_variance_account
         ,apid.expenditure_item_date pa_expenditure_item_date
         ,apid.expenditure_type pa_expenditure_type
         ,apid.expenditure_organization_id
         ,NVL (haou1.name, (NVL (haou3.name, haou2.name)))
             expenditure_organization
         ,apid.pa_addition_flag
         ,apid.project_accounting_context
         ,apl.line_number
         ,apl.line_type_lookup_code line_type
         ,REPLACE (apl.description, '~', '-') line_description
         ,apl.amount line_amount
         ,NVL (apl.amount, apl.base_amount) line_base_amount
         ,pap.project_id
         ,pat.task_id
         ,pap.segment1 project_number
         ,pap.name project_name
         ,pap.description project_description
         ,pap.project_type
         ,pap.project_status_code
         ,pap.start_date project_start_date
         ,pap.completion_date project_completion_date
         ,pat.task_number
         ,pat.task_name
         ,pat.description task_description
         ,pat.service_type_code
         ,pat.start_date task_start_date
         ,pat.completion_date task_completion_date
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_yes_no (
             pat.chargeable_flag)
             chargeable_task
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_yes_no (pat.billable_flag)
             billable_task
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_yes_no (
             apid.amount_includes_tax_flag)
             dist_amount_includes_tax
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_yes_no (
             apid.tax_calculated_flag)
             dist_tax_calculated_flag
         ,apid.invoice_distribution_id
         ,apid.parent_reversal_id
         ,apid.dist_match_type
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_yes_no (
             apid.tax_recovery_override_flag)
             dist_tax_recovery_override
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_yes_no (
             apid.tax_recoverable_flag)
             dist_tax_recoverable
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_yes_no (
             apid.tax_code_override_flag)
             dist_tax_code_override
         ,xxeis.eis_rs_ap_fin_com_util_pkg.eis_get_ap_tax_code (
             apid.tax_code_id)
             dist_tax_name
         ,pov.vendor_id
         ,apb.batch_id
         ,apid.invoice_line_number
         ,apid.distribution_line_number
		 ,pov.segment1 vendor_number  --added for version 1.1
         --descr#flexfield#start
         --descr#flexfield#end
         --gl#accountff#start
         ,gcc1.segment1 gcc1#50328#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment1
                                               ,'XXCUS_GL_PRODUCT')
             gcc1#50328#product#descr
         ,gcc2.segment1 gcc2#50328#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment1
                                               ,'XXCUS_GL_PRODUCT')
             gcc2#50328#product#descr
         ,gcc3.segment1 gcc3#50328#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc3.segment1
                                               ,'XXCUS_GL_PRODUCT')
             gcc3#50328#product#descr
         ,gcc1.segment2 gcc1#50328#location
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment2
                                               ,'XXCUS_GL_LOCATION')
             gcc1#50328#location#descr
         ,gcc2.segment2 gcc2#50328#location
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment2
                                               ,'XXCUS_GL_LOCATION')
             gcc2#50328#location#descr
         ,gcc3.segment2 gcc3#50328#location
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc3.segment2
                                               ,'XXCUS_GL_LOCATION')
             gcc3#50328#location#descr
         ,gcc1.segment3 gcc1#50328#cost_center
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment3
                                               ,'XXCUS_GL_COSTCENTER')
             gcc1#50328#cost_center#descr
         ,gcc2.segment3 gcc2#50328#cost_center
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment3
                                               ,'XXCUS_GL_COSTCENTER')
             gcc2#50328#cost_center#descr
         ,gcc3.segment3 gcc3#50328#cost_center
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc3.segment3
                                               ,'XXCUS_GL_COSTCENTER')
             gcc3#50328#cost_center#descr
         ,gcc1.segment4 gcc1#50328#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment4
                                               ,'XXCUS_GL_ACCOUNT')
             gcc1#50328#account#descr
         ,gcc2.segment4 gcc2#50328#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment4
                                               ,'XXCUS_GL_ACCOUNT')
             gcc2#50328#account#descr
         ,gcc3.segment4 gcc3#50328#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc3.segment4
                                               ,'XXCUS_GL_ACCOUNT')
             gcc3#50328#account#descr
         ,gcc1.segment5 gcc1#50328#project_code
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment5
                                               ,'XXCUS_GL_PROJECT')
             gcc1#50328#project_code#descr
         ,gcc2.segment5 gcc2#50328#project_code
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment5
                                               ,'XXCUS_GL_PROJECT')
             gcc2#50328#project_code#descr
         ,gcc3.segment5 gcc3#50328#project_code
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc3.segment5
                                               ,'XXCUS_GL_PROJECT')
             gcc3#50328#project_code#descr
         ,gcc1.segment6 gcc1#50328#furture_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment6
                                               ,'XXCUS_GL_FUTURE_USE1')
             gcc1#50328#furture_use#descr
         ,gcc2.segment6 gcc2#50328#furture_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment6
                                               ,'XXCUS_GL_FUTURE_USE1')
             gcc2#50328#furture_use#descr
         ,gcc3.segment6 gcc3#50328#furture_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc3.segment6
                                               ,'XXCUS_GL_FUTURE_USE1')
             gcc3#50328#furture_use#descr
         ,gcc1.segment7 gcc1#50328#future_use_2
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment7
                                               ,'XXCUS_GL_FUTURE_USE_2')
             gcc1#50328#future_use_2#descr
         ,gcc2.segment7 gcc2#50328#future_use_2
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment7
                                               ,'XXCUS_GL_FUTURE_USE_2')
             gcc2#50328#future_use_2#descr
         ,gcc3.segment7 gcc3#50328#future_use_2
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc3.segment7
                                               ,'XXCUS_GL_FUTURE_USE_2')
             gcc3#50328#future_use_2#descr
         ,gcc1.segment4 gcc1#50368#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment4
                                               ,'XXCUS_GL_ LTMR _ACCOUNT')
             gcc1#50368#account#descr
         ,gcc1.segment3 gcc1#50368#department
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment3
                                               ,'XXCUS_GL_ LTMR _DEPARTMENT')
             gcc1#50368#department#descr
         ,gcc1.segment2 gcc1#50368#division
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment2
                                               ,'XXCUS_GL_ LTMR _DIVISION')
             gcc1#50368#division#descr
         ,gcc1.segment6 gcc1#50368#future_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment6
                                               ,'XXCUS_GL_ LTMR _FUTUREUSE')
             gcc1#50368#future_use#descr
         ,gcc1.segment1 gcc1#50368#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment1
                                               ,'XXCUS_GL_LTMR_PRODUCT')
             gcc1#50368#product#descr
         ,gcc1.segment5 gcc1#50368#subaccount
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc1.segment5
                                               ,'XXCUS_GL_ LTMR _SUBACCOUNT')
             gcc1#50368#subaccount#descr
         ,gcc2.segment4 gcc2#50368#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment4
                                               ,'XXCUS_GL_ LTMR _ACCOUNT')
             gcc2#50368#account#descr
         ,gcc2.segment3 gcc2#50368#department
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment3
                                               ,'XXCUS_GL_ LTMR _DEPARTMENT')
             gcc2#50368#department#descr
         ,gcc2.segment2 gcc2#50368#division
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment2
                                               ,'XXCUS_GL_ LTMR _DIVISION')
             gcc2#50368#division#descr
         ,gcc2.segment6 gcc2#50368#future_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment6
                                               ,'XXCUS_GL_ LTMR _FUTUREUSE')
             gcc2#50368#future_use#descr
         ,gcc2.segment1 gcc2#50368#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment1
                                               ,'XXCUS_GL_LTMR_PRODUCT')
             gcc2#50368#product#descr
         ,gcc2.segment5 gcc2#50368#subaccount
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc2.segment5
                                               ,'XXCUS_GL_ LTMR _SUBACCOUNT')
             gcc2#50368#subaccount#descr
         ,gcc3.segment4 gcc3#50368#account
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc3.segment4
                                               ,'XXCUS_GL_ LTMR _ACCOUNT')
             gcc3#50368#account#descr
         ,gcc3.segment3 gcc3#50368#department
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc3.segment3
                                               ,'XXCUS_GL_ LTMR _DEPARTMENT')
             gcc3#50368#department#descr
         ,gcc3.segment2 gcc3#50368#division
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc3.segment2
                                               ,'XXCUS_GL_ LTMR _DIVISION')
             gcc3#50368#division#descr
         ,gcc3.segment6 gcc3#50368#future_use
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc3.segment6
                                               ,'XXCUS_GL_ LTMR _FUTUREUSE')
             gcc3#50368#future_use#descr
         ,gcc3.segment1 gcc3#50368#product
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc3.segment1
                                               ,'XXCUS_GL_LTMR_PRODUCT')
             gcc3#50368#product#descr
         ,gcc3.segment5 gcc3#50368#subaccount
         ,xxeis.eis_rs_fin_utility.decode_vset (gcc3.segment5
                                               ,'XXCUS_GL_ LTMR _SUBACCOUNT')
             gcc3#50368#subaccount#descr
     --gl#accountff#end
     FROM ap_invoices api
         ,ap_suppliers pov
         ,ap_supplier_sites_all povs
         ,ap_batches_all apb
         ,ap_invoice_distributions apid
         ,ap_invoice_lines apl
         ,pa_projects pap
         ,pa_tasks pat
         ,hr_operating_units hou
         ,gl_code_combinations_kfv gcc1
         ,gl_code_combinations_kfv gcc2
         ,gl_code_combinations_kfv gcc3
         --- For case 6123
         ,iby_payment_methods_vl iby
         ,hr_all_organization_units haou1
         ,hr_all_organization_units haou2
         ,hr_all_organization_units haou3
    WHERE     api.vendor_id = pov.vendor_id
          AND api.vendor_site_id = povs.vendor_site_id
          AND api.batch_id = apb.batch_id(+)
          AND api.invoice_id = apl.invoice_id(+)
          AND apl.invoice_id = apid.invoice_id(+)
          AND apid.invoice_line_number(+) = apl.line_number
          AND apid.project_id = pap.project_id(+)
          AND apid.task_id = pat.task_id(+)
          AND api.org_id = hou.organization_id
          AND api.accts_pay_code_combination_id = gcc1.code_combination_id(+)
          AND apid.dist_code_combination_id = gcc2.code_combination_id(+)
          AND apid.price_var_code_combination_id =
                 gcc3.code_combination_id(+)
          AND iby.payment_method_code = api.payment_method_code
          AND api.expenditure_organization_id = haou1.organization_id(+)
          AND apl.expenditure_organization_id = haou2.organization_id(+)
          AND apid.expenditure_organization_id = haou3.organization_id(+)
          AND api.cancelled_date IS NULL
/* AND xxeis.eis_gl_security_pkg.validate_access
                                       (api.set_of_books_id,
                                        api.accts_pay_code_combination_id
                                       ) = 'TRUE'
 AND xxeis.eis_gl_security_pkg.validate_access
                                           (api.set_of_books_id,
                                            apid.dist_code_combination_id
                                           ) = 'TRUE'
 AND xxeis.eis_gl_security_pkg.validate_access
                                      (api.set_of_books_id,
                                       apid.price_var_code_combination_id
                                      ) = 'TRUE' */
;
