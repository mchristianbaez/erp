--Report Name            : HDS GL Detail Invoices
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_1581489_VOHWKC_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1581489_VOHWKC_V
xxeis.eis_rsc_ins.v( 'XXEIS_1581489_VOHWKC_V',200,'Paste SQL View for HDS GL Detail Invoices','1.0','','','ID020048','APPS','HDS GL Detail Invoices View','X1VV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_1581489_VOHWKC_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1581489_VOHWKC_V',200,FALSE);
--Inserting Object Columns for XXEIS_1581489_VOHWKC_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1581489_VOHWKC_V','VENDOR_NAME',200,'','','','','','ID020048','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1581489_VOHWKC_V','INVOICE_NUM',200,'','','','','','ID020048','VARCHAR2','','','Invoice Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1581489_VOHWKC_V','INVOICE_AMOUNT',200,'','','','~T~D~2','','ID020048','NUMBER','','','Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1581489_VOHWKC_V','INVOICE_DATE',200,'','','','','','ID020048','DATE','','','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1581489_VOHWKC_V','CREATION_DATE',200,'','','','','','ID020048','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1581489_VOHWKC_V','INVOICE_TYPE_LOOKUP_CODE',200,'','','','','','ID020048','VARCHAR2','','','Invoice Type Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1581489_VOHWKC_V','PAY_GROUP_LOOKUP_CODE',200,'','','','','','ID020048','VARCHAR2','','','Pay Group Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1581489_VOHWKC_V','BRANCH',200,'','','','','','ID020048','VARCHAR2','','','Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1581489_VOHWKC_V','DISTRIBUTION_LINE_NUMBER',200,'','','','','','ID020048','NUMBER','','','Distribution Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1581489_VOHWKC_V','DIST_LINE_AMOUNT',200,'','','','~T~D~2','','ID020048','NUMBER','','','Dist Line Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1581489_VOHWKC_V','LINE_TYPE_LOOKUP_CODE',200,'','','','','','ID020048','VARCHAR2','','','Line Type Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1581489_VOHWKC_V','DIST_LINE_PRODUCT',200,'','','','','','ID020048','VARCHAR2','','','Dist Line Product','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1581489_VOHWKC_V','DIST_LINE_GL_ACCOUNT',200,'','','','','','ID020048','VARCHAR2','','','Dist Line Gl Account','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1581489_VOHWKC_V','SOURCE',200,'','','','','','ID020048','VARCHAR2','','','Source','','','','US');
--Inserting Object Components for XXEIS_1581489_VOHWKC_V
--Inserting Object Component Joins for XXEIS_1581489_VOHWKC_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report LOV Data for HDS GL Detail Invoices
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS GL Detail Invoices
xxeis.eis_rsc_ins.lov( 200,'select distinct vendor_name
from ap.ap_suppliers','','HDS_Vendor_Name','','ID020048',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report Data for HDS GL Detail Invoices
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS GL Detail Invoices
xxeis.eis_rsc_utility.delete_report_rows( 'HDS GL Detail Invoices' );
--Inserting Report - HDS GL Detail Invoices
xxeis.eis_rsc_ins.r( 200,'HDS GL Detail Invoices','','','','','','XXEIS_RS_ADMIN','XXEIS_1581489_VOHWKC_V','Y','','select v.vendor_name,
       A.INVOICE_NUM,
       A.INVOICE_AMOUNT,
       A.INVOICE_DATE,
       a.creation_date,
       a.invoice_type_lookup_code,
       a.pay_group_lookup_code,
       a.attribute2 BRANCH,
       d.distribution_line_number,
       d.amount DIST_LINE_AMOUNT,
       d.line_type_lookup_code,
       g.segment1 DIST_LINE_PRODUCT,
       g.segment4 DIST_LINE_GL_ACCOUNT,
       a.source
  from ap.ap_invoices_all              a,
       ap.ap_suppliers                 v,
       GL.GL_CODE_COMBINATIONS         g,
       ap.ap_invoice_distributions_all d
   where a.vendor_id = v.vendor_id
   and a.invoice_id = d.invoice_id
   and d.dist_code_combination_id = g.code_combination_id
   AND a.pay_group_lookup_code = ''FREIGHT''
   AND A.INVOICE_TYPE_LOOKUP_CODE <> ''EXPENSE REPORT''
   order by  v.vendor_name, a.invoice_num
','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','APPS','US','','','','');
--Inserting Report Columns - HDS GL Detail Invoices
xxeis.eis_rsc_ins.rc( 'HDS GL Detail Invoices',200,'PAY_GROUP_LOOKUP_CODE','Pay Group Lookup Code','','','','default','','7','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1581489_VOHWKC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Detail Invoices',200,'DIST_LINE_AMOUNT','Dist Line Amount','','','~T~D~2','default','','10','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1581489_VOHWKC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Detail Invoices',200,'INVOICE_AMOUNT','Invoice Amount','','','~T~D~2','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1581489_VOHWKC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Detail Invoices',200,'INVOICE_NUM','Invoice Num','','','','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1581489_VOHWKC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Detail Invoices',200,'INVOICE_TYPE_LOOKUP_CODE','Invoice Type Lookup Code','','','','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1581489_VOHWKC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Detail Invoices',200,'VENDOR_NAME','Vendor Name','','','','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1581489_VOHWKC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Detail Invoices',200,'LINE_TYPE_LOOKUP_CODE','Line Type Lookup Code','','','','','','11','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1581489_VOHWKC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Detail Invoices',200,'BRANCH','Branch','','','','default','','8','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1581489_VOHWKC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Detail Invoices',200,'INVOICE_DATE','Invoice Date','','','','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1581489_VOHWKC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Detail Invoices',200,'DIST_LINE_PRODUCT','Dist Line Product','','','','','','12','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1581489_VOHWKC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Detail Invoices',200,'DIST_LINE_GL_ACCOUNT','Dist Line Gl Account','','','','','','13','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1581489_VOHWKC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Detail Invoices',200,'CREATION_DATE','Creation Date','','','','default','','5','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1581489_VOHWKC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Detail Invoices',200,'SOURCE','Source','','','','','','14','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1581489_VOHWKC_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS GL Detail Invoices',200,'DISTRIBUTION_LINE_NUMBER','Distribution Line Number','','','~~~','default','','9','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1581489_VOHWKC_V','','','','US','');
--Inserting Report Parameters - HDS GL Detail Invoices
xxeis.eis_rsc_ins.rp( 'HDS GL Detail Invoices',200,'Creation Date From','','CREATION_DATE','>=','','','DATE','N','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_1581489_VOHWKC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GL Detail Invoices',200,'Creation Date To','','CREATION_DATE','<=','','','DATE','N','Y','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_1581489_VOHWKC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GL Detail Invoices',200,'Vendor Name','','VENDOR_NAME','IN','HDS_Vendor_Name','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_1581489_VOHWKC_V','','','US','');
--Inserting Dependent Parameters - HDS GL Detail Invoices
--Inserting Report Conditions - HDS GL Detail Invoices
xxeis.eis_rsc_ins.rcnh( 'HDS GL Detail Invoices',200,'X1VV.CREATION_DATE >= :Creation Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Creation Date From','','','','','XXEIS_1581489_VOHWKC_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',200,'HDS GL Detail Invoices','X1VV.CREATION_DATE >= :Creation Date From ');
xxeis.eis_rsc_ins.rcnh( 'HDS GL Detail Invoices',200,'VENDOR_NAME IN :Vendor Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NAME','','Vendor Name','','','','','XXEIS_1581489_VOHWKC_V','','','','','','IN','Y','Y','','','','','1',200,'HDS GL Detail Invoices','VENDOR_NAME IN :Vendor Name ');
xxeis.eis_rsc_ins.rcnh( 'HDS GL Detail Invoices',200,'X1VV.INVOICE_TYPE_LOOKUP_CODE <> ''EXPENSE REPORT'' ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','NOTEQUALS','Y','N','X1VV.INVOICE_TYPE_LOOKUP_CODE','''EXPENSE REPORT''','','','1',200,'HDS GL Detail Invoices','X1VV.INVOICE_TYPE_LOOKUP_CODE <> ''EXPENSE REPORT'' ');
xxeis.eis_rsc_ins.rcnh( 'HDS GL Detail Invoices',200,'X1VV.PAY_GROUP_LOOKUP_CODE = ''FREIGHT'' ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','EQUALS','Y','N','X1VV.PAY_GROUP_LOOKUP_CODE','''FREIGHT''','','','1',200,'HDS GL Detail Invoices','X1VV.PAY_GROUP_LOOKUP_CODE = ''FREIGHT'' ');
xxeis.eis_rsc_ins.rcnh( 'HDS GL Detail Invoices',200,'X1VV.CREATION_DATE <= Creation Date To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Creation Date To','','','','','XXEIS_1581489_VOHWKC_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',200,'HDS GL Detail Invoices','X1VV.CREATION_DATE <= Creation Date To');
--Inserting Report Sorts - HDS GL Detail Invoices
xxeis.eis_rsc_ins.rs( 'HDS GL Detail Invoices',200,'VENDOR_NAME','DESC','XXEIS_RS_ADMIN','1','');
xxeis.eis_rsc_ins.rs( 'HDS GL Detail Invoices',200,'INVOICE_NUM','DESC','XXEIS_RS_ADMIN','2','');
--Inserting Report Triggers - HDS GL Detail Invoices
--inserting report templates - HDS GL Detail Invoices
--Inserting Report Portals - HDS GL Detail Invoices
--inserting report dashboards - HDS GL Detail Invoices
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS GL Detail Invoices','200','XXEIS_1581489_VOHWKC_V','XXEIS_1581489_VOHWKC_V','N','');
--inserting report security - HDS GL Detail Invoices
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','PAYABLES_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','XXWC_PAY_VENDOR_MSTR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','HDS_AP_TRNS_ENTRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','XXWC_PAY_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','HDS_AP_MGR_NOSUP_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','HDS_AP_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','HDS_AP_MGR_NOSUP_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','XXWC_PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','HDS_AP_INQUIRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','HDS_AP_INQ_CANADA',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','XXWC_PAY_DISBURSE',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','HDS_AP_DISBURSEMTS_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','HDS_AP_DISBURSEMTS_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','HDS_PAYABLES_CLOSE_GLBL',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','XXWC_PAY_W_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','XXWC_PAY_NO_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','HDS_AP_ADMIN_US_GSCIWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','HDS_AP_ADMIN_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Detail Invoices','200','','HDS_AP_ADMIN_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS GL Detail Invoices
--Inserting Report   Version details- HDS GL Detail Invoices
xxeis.eis_rsc_ins.rv( 'HDS GL Detail Invoices','','HDS GL Detail Invoices','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
