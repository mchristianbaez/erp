--Report Name            : Unapplied Rec Register - Offshore
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_AR_UNAPPLIED_RECEIPTS_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_AR_UNAPPLIED_RECEIPTS_V
xxeis.eis_rsc_ins.v( 'EIS_AR_UNAPPLIED_RECEIPTS_V',222,'This view shows details of unapplied receipts including customer information.','','','','XXEIS_RS_ADMIN','XXEIS','EIS AR Unapplied Receipts','EAURV','','','VIEW','US','Y','');
--Delete Object Columns for EIS_AR_UNAPPLIED_RECEIPTS_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_AR_UNAPPLIED_RECEIPTS_V',222,FALSE);
--Inserting Object Columns for EIS_AR_UNAPPLIED_RECEIPTS_V
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','COMMENTS',222,'Comments about this receipt record','COMMENTS~COMMENTS','','','','XXEIS_RS_ADMIN','VARCHAR2','AR_CASH_RECEIPTS_ALL','COMMENTS','Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','MATURITY_DATE',222,'Due date of this installment','MATURITY_DATE~MATURITY_DATE','','','','XXEIS_RS_ADMIN','DATE','AR_PAYMENT_SCHEDULES_ALL','DUE_DATE','Maturity Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','CLAIM_AMT',222,'Claim Amt','CLAIM_AMT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Claim Amt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','UNID_FLAG',222,'Unid Flag','UNID_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Unid Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','CUSTOMER_CLASS_CODE',222,'Customer class indentifier','CUSTOMER_CLASS_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','CUSTOMER_CLASS_CODE','Customer Class Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','COMPANY_NAME',222,'Accounting books name','COMPANY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_SETS_OF_BOOKS','NAME','Company Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','FUNCTIONAL_CURRENCY',222,'Currency','FUNCTIONAL_CURRENCY','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_SETS_OF_BOOKS','CURRENCY_CODE','Functional Currency','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','RECEIPT_METHOD',222,'Name of the receipt method','RECEIPT_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','AR_RECEIPT_METHODS','NAME','Receipt Method','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','GL_DATE',222,'Gl Date','GL_DATE~GL_DATE','','','','XXEIS_RS_ADMIN','DATE','CALCULATION COLUMN','CALCULATION COLUMN','Gl Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','CURRENCY_CODE',222,'Currency code of the payment batch associated with this payment entry','CURRENCY_CODE~CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AR_CASH_RECEIPTS_ALL','CURRENCY_CODE','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','FUNCTIONAL_CURRENCY_PRECISION',222,'Maximum number of digits to the right of decimal point','FUNCTIONAL_CURRENCY_PRECISION','','','','XXEIS_RS_ADMIN','NUMBER','FND_CURRENCIES','PRECISION','Functional Currency Precision','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','CUSTOMER_NAME',222,'Name of this party','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','PARTY_NAME','Customer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','CUSTOMER_NUMBER',222,'Account Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ACCOUNT_NUMBER','Customer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','BATCH_SOURCE_NAME',222,'Name of the payment batch source','BATCH_SOURCE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','AR_BATCH_SOURCES_ALL','NAME','Batch Source Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','BATCH_NAME',222,'Name of the payment batch','BATCH_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','AR_BATCHES_ALL','NAME','Batch Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','RECEIPT_NUMBER',222,'Cash receipt number','RECEIPT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','AR_CASH_RECEIPTS_ALL','RECEIPT_NUMBER','Receipt Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','RECEIPT_DATE',222,'Date of the payment entry','RECEIPT_DATE','','','','XXEIS_RS_ADMIN','DATE','AR_CASH_RECEIPTS_ALL','RECEIPT_DATE','Receipt Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','ON_ACCOUNT_AMT',222,'On Account Amt','ON_ACCOUNT_AMT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','On Account Amt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','UNAPPLIED_AMT',222,'Unapplied Amt','UNAPPLIED_AMT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Unapplied Amt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','PARTY_NAME',222,'Party Name','PARTY_NAME~PARTY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','PARTY_NAME','Party Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','PARAM_ORG_ID',222,'Param Org Id','PARAM_ORG_ID~PARAM_ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','AR_SYSTEM_PARAMETERS_ALL','ORG_ID','Param Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','ORG_ID',222,'Org Id','ORG_ID~ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','PARTY_ID',222,'Party Id','PARTY_ID~PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_PARTIES','PARTY_ID','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','RECEIPT_METHOD_ID',222,'Receipt Method Id','RECEIPT_METHOD_ID~RECEIPT_METHOD_ID','','','','XXEIS_RS_ADMIN','NUMBER','AR_RECEIPT_METHODS','RECEIPT_METHOD_ID','Receipt Method Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','CUSTOMER_ID',222,'Customer account identifier','CUSTOMER_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_CUST_ACCOUNTS','CUST_ACCOUNT_ID','Customer Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','BATCH_ID',222,'Batch Id','BATCH_ID~BATCH_ID','','','','XXEIS_RS_ADMIN','NUMBER','AR_BATCHES_ALL','BATCH_ID','Batch Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','BATCH_SOURCE_ID',222,'Batch Source Id','BATCH_SOURCE_ID~BATCH_SOURCE_ID','','','','XXEIS_RS_ADMIN','NUMBER','RA_BATCH_SOURCES_ALL','BATCH_SOURCE_ID','Batch Source Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','CASH_RECEIPT_ID',222,'Cash Receipt Id','CASH_RECEIPT_ID~CASH_RECEIPT_ID','','','','XXEIS_RS_ADMIN','NUMBER','AR_CASH_RECEIPTS_ALL','CASH_RECEIPT_ID','Cash Receipt Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','CUR_CURRENCY_CODE',222,'Cur Currency Code','CUR_CURRENCY_CODE~CUR_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','FND_CURRENCIES','CURRENCY_CODE','Cur Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','CUST_ACCOUNT_ID',222,'Cust Account Id','CUST_ACCOUNT_ID~CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','HZ_CUST_ACCOUNTS','CUST_ACCOUNT_ID','Cust Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','LEDGER_ID',222,'Ledger Id','LEDGER_ID~LEDGER_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_LEDGERS','LEDGER_ID','Ledger Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','OPERATING_UNIT',222,'Operating Unit','OPERATING_UNIT~OPERATING_UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','HR_ALL_ORGANIZATION_UNITS_TL','NAME','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','ORGANIZATION_ID',222,'Organization Id','ORGANIZATION_ID~ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','HR_ALL_ORGANIZATION_UNITS','ORGANIZATION_ID','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','SET_OF_BOOKS_ID',222,'General Ledger Set of Books that is used, only one currently supported','SET_OF_BOOKS_ID','','','','XXEIS_RS_ADMIN','NUMBER','AR_SYSTEM_PARAMETERS_ALL','SET_OF_BOOKS_ID','Set Of Books Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','ACCOUNT_NAME',222,'Account Name','ACCOUNT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','DUE_DATE',222,'Due Date','DUE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Due Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AR_UNAPPLIED_RECEIPTS_V','COPYRIGHT',222,'Copyright','COPYRIGHT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Copyright','','','','US');
--Inserting Object Components for EIS_AR_UNAPPLIED_RECEIPTS_V
xxeis.eis_rsc_ins.vcomp( 'EIS_AR_UNAPPLIED_RECEIPTS_V','HZ_CUST_ACCOUNTS',222,'HZ_CUST_ACCOUNTS','CUST','CUST','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Bill To Customer Accounts','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AR_UNAPPLIED_RECEIPTS_V','HZ_PARTIES',222,'HZ_PARTIES','PARTY','PARTY','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Parties','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AR_UNAPPLIED_RECEIPTS_V','AR_RECEIPT_METHODS',222,'AR_RECEIPT_METHODS','RM','RM','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Receipts Methods','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AR_UNAPPLIED_RECEIPTS_V','AR_CASH_RECEIPTS',222,'AR_CASH_RECEIPTS_ALL','RCPT','RCPT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Detailed Receipt Information','','','','','ACR','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AR_UNAPPLIED_RECEIPTS_V','GL_LEDGERS',222,'GL_LEDGERS','GLE','GLE','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ledger Definition','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AR_UNAPPLIED_RECEIPTS_V','FND_CURRENCIES',222,'FND_CURRENCIES','CUR','CUR','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Currencies','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AR_UNAPPLIED_RECEIPTS_V','HR_ORGANIZATION_UNITS',222,'HR_ALL_ORGANIZATION_UNITS','HOU','HOU','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Organization Details','N','','','','','Y',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_AR_UNAPPLIED_RECEIPTS_V
xxeis.eis_rsc_ins.vcj( 'EIS_AR_UNAPPLIED_RECEIPTS_V','HZ_CUST_ACCOUNTS','CUST',222,'EAURV.CUST_ACCOUNT_ID','=','CUST.CUST_ACCOUNT_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AR_UNAPPLIED_RECEIPTS_V','HZ_PARTIES','PARTY',222,'EAURV.PARTY_ID','=','PARTY.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AR_UNAPPLIED_RECEIPTS_V','AR_RECEIPT_METHODS','RM',222,'EAURV.RECEIPT_METHOD_ID','=','RM.RECEIPT_METHOD_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AR_UNAPPLIED_RECEIPTS_V','AR_CASH_RECEIPTS','RCPT',222,'EAURV.CASH_RECEIPT_ID','=','RCPT.CASH_RECEIPT_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AR_UNAPPLIED_RECEIPTS_V','GL_LEDGERS','GLE',222,'EAURV.COMPANY_NAME','=','GLE.NAME(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AR_UNAPPLIED_RECEIPTS_V','FND_CURRENCIES','CUR',222,'EAURV.CUR_CURRENCY_CODE','=','CUR.CURRENCY_CODE(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AR_UNAPPLIED_RECEIPTS_V','HR_ORGANIZATION_UNITS','HOU',222,'EAURV.ORGANIZATION_ID','=','HOU.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
--Exporting View Component Data of the View -  EIS_AR_UNAPPLIED_RECEIPTS_V
prompt Creating Object Data AR_CASH_RECEIPTS
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object AR_CASH_RECEIPTS
xxeis.eis_rsc_ins.v( 'AR_CASH_RECEIPTS',222,'Detailed receipt information','1.0','','','ANONYMOUS','APPS','Ar Cash Receipts','ACR','','','SYNONYM','US','','');
--Delete Object Columns for AR_CASH_RECEIPTS
xxeis.eis_rsc_utility.delete_view_rows('AR_CASH_RECEIPTS',222,FALSE);
--Inserting Object Columns for AR_CASH_RECEIPTS
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','UNIQUE_REFERENCE',222,'Global unique Identifier for electronic receipts in Oracle Payments. This unique reference is generated using SYS_GUID() to make it unique within the instance.','UNIQUE_REFERENCE','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','UNIQUE_REFERENCE','Unique Reference','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','PROMISE_SOURCE',222,'Source that generated this promise to pay. Used by Advanced Collections.','PROMISE_SOURCE','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','PROMISE_SOURCE','Promise Source','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','REC_VERSION_NUMBER',222,'Used to control the updates when two concurrent users try to update the same receipt.','REC_VERSION_NUMBER','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','REC_VERSION_NUMBER','Rec Version Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','CC_ERROR_CODE',222,'Error code received from external payment system during processing of credit card receipts.','CC_ERROR_CODE','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','CC_ERROR_CODE','Cc Error Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','CC_ERROR_TEXT',222,'Error description provided by user for credit card error code','CC_ERROR_TEXT','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','CC_ERROR_TEXT','Cc Error Text','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','CC_ERROR_FLAG',222,'Flag to indicate whether receipt has credit card error. Y indicates a receipt had a credit card processing error','CC_ERROR_FLAG','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','CC_ERROR_FLAG','Cc Error Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','REMIT_BANK_ACCT_USE_ID',222,'The identifier of the remittance bank account to which the receipt will be deposited.','REMIT_BANK_ACCT_USE_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','REMIT_BANK_ACCT_USE_ID','Remit Bank Acct Use Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','OLD_CUSTOMER_BANK_BRANCH_ID',222,'Identifier of the old default remittance bank branch of the payment batch.','OLD_CUSTOMER_BANK_BRANCH_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','OLD_CUSTOMER_BANK_BRANCH_ID','Old Customer Bank Branch Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','OLD_ISSUER_BANK_BRANCH_ID',222,'Identifier of the old default remittance bank branch of the payment batch.','OLD_ISSUER_BANK_BRANCH_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','OLD_ISSUER_BANK_BRANCH_ID','Old Issuer Bank Branch Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','LEGAL_ENTITY_ID',222,'Identifies the legal entity to receive this receipt','LEGAL_ENTITY_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','LEGAL_ENTITY_ID','Legal Entity Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','PAYMENT_TRXN_EXTENSION_ID',222,'Payment extension identifier provided Oracle Payments for automatic receipts like credit card receipts and bank account transfer.','PAYMENT_TRXN_EXTENSION_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','PAYMENT_TRXN_EXTENSION_ID','Payment Trxn Extension Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','AX_ACCOUNTED_FLAG',222,'Upgrade flag to indicate the distributions have been upgraded by AX','AX_ACCOUNTED_FLAG','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','AX_ACCOUNTED_FLAG','Ax Accounted Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','OLD_CUSTOMER_BANK_ACCOUNT_ID',222,'Identifier of the old default remittance bank account of the payment batch','OLD_CUSTOMER_BANK_ACCOUNT_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','OLD_CUSTOMER_BANK_ACCOUNT_ID','Old Customer Bank Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','CASH_APPLN_OWNER_ID',222,'Identifier of the cash application owner to whom the receipt is assigned for application.','CASH_APPLN_OWNER_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','CASH_APPLN_OWNER_ID','Cash Appln Owner Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','WORK_ITEM_ASSIGNMENT_DATE',222,'Date on which the receipt is assigned to the cash application owner.','WORK_ITEM_ASSIGNMENT_DATE','','','','ANONYMOUS','DATE','AR_CASH_RECEIPTS','WORK_ITEM_ASSIGNMENT_DATE','Work Item Assignment Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','WORK_ITEM_REVIEW_DATE',222,'Review date assigned by the cash application owner.','WORK_ITEM_REVIEW_DATE','','','','ANONYMOUS','DATE','AR_CASH_RECEIPTS','WORK_ITEM_REVIEW_DATE','Work Item Review Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','WORK_ITEM_STATUS_CODE',222,'Status code of the receipt in cash application work queue.','WORK_ITEM_STATUS_CODE','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','WORK_ITEM_STATUS_CODE','Work Item Status Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','WORK_ITEM_REVIEW_NOTE',222,'Review notes entered by cash application owner','WORK_ITEM_REVIEW_NOTE','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','WORK_ITEM_REVIEW_NOTE','Work Item Review Note','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','PREV_PAY_FROM_CUSTOMER',222,'Identifier of the customer which was associated with this payment entry previously','PREV_PAY_FROM_CUSTOMER','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','PREV_PAY_FROM_CUSTOMER','Prev Pay From Customer','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','PREV_CUSTOMER_SITE_USE_ID',222,'Identifies the customer''s location which was associated with this payment entry previously','PREV_CUSTOMER_SITE_USE_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','PREV_CUSTOMER_SITE_USE_ID','Prev Customer Site Use Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','WORK_ITEM_EXCEPTION_REASON',222,'Reserved for future use','WORK_ITEM_EXCEPTION_REASON','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','WORK_ITEM_EXCEPTION_REASON','Work Item Exception Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','AUTOMATCH_SET_ID',222,'Reserved for future use','AUTOMATCH_SET_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','AUTOMATCH_SET_ID','Automatch Set Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','AUTOAPPLY_FLAG',222,'Reserved for future use','AUTOAPPLY_FLAG','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','AUTOAPPLY_FLAG','Autoapply Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','SEQ_TYPE_LAST',222,'','SEQ_TYPE_LAST','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','SEQ_TYPE_LAST','Seq Type Last','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','APPLICATION_NOTES',222,'Reference number that could not be matched to a transaction number during AutoLockbox validation.','APPLICATION_NOTES','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','APPLICATION_NOTES','Application Notes','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','CONFIRMED_FLAG',222,'Flag to indicate confirmation status of the receipt. This column will have null value if receipt does not require confirmation. It will have value ''Y'' when the receipt is confirmed and value ''N'' when the receipt is not confirmed but require','CONFIRMED_FLAG','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','CONFIRMED_FLAG','Confirmed Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','CUSTOMER_BANK_ACCOUNT_ID',222,'No longer used. In R12, Payment Transaction Extension Identifier in Oracle Payments links the customer bank details and transactions.','CUSTOMER_BANK_ACCOUNT_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','CUSTOMER_BANK_ACCOUNT_ID','Customer Bank Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','CUSTOMER_SITE_USE_ID',222,'Internal identifier for the customer site assigned to the receipt header.','CUSTOMER_SITE_USE_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','CUSTOMER_SITE_USE_ID','Customer Site Use Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','DEPOSIT_DATE',222,'Deposit date for the receipt.','DEPOSIT_DATE','','','','ANONYMOUS','DATE','AR_CASH_RECEIPTS','DEPOSIT_DATE','Deposit Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','PROGRAM_APPLICATION_ID',222,'Concurrent program who column. Application identifier of the program that last updated this row.','PROGRAM_APPLICATION_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','PROGRAM_APPLICATION_ID','Program Application Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','PROGRAM_ID',222,'Concurrent program who column. Identifier of the program that last updated this row.','PROGRAM_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','PROGRAM_ID','Program Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','PROGRAM_UPDATE_DATE',222,'Concurrent program who column. Date when a program last updated this row.','PROGRAM_UPDATE_DATE','','','','ANONYMOUS','DATE','AR_CASH_RECEIPTS','PROGRAM_UPDATE_DATE','Program Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','RECEIPT_METHOD_ID',222,'Identifier of the payment method assigned to the receipt','RECEIPT_METHOD_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','RECEIPT_METHOD_ID','Receipt Method Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','REQUEST_ID',222,'Concurrent program who column. Concurrent request identifier of the program that last updated this row.','REQUEST_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','REQUEST_ID','Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','SELECTED_FOR_FACTORING_FLAG',222,'Used by European and Asia Pacific localizations to flag receipt selected for factoring.','SELECTED_FOR_FACTORING_FLAG','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','SELECTED_FOR_FACTORING_FLAG','Selected For Factoring Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','SELECTED_REMITTANCE_BATCH_ID',222,'Identifier of the remittance batch that includes this receipt.','SELECTED_REMITTANCE_BATCH_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','SELECTED_REMITTANCE_BATCH_ID','Selected Remittance Batch Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','FACTOR_DISCOUNT_AMOUNT',222,'Bank charges amount for the receipt.','FACTOR_DISCOUNT_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','FACTOR_DISCOUNT_AMOUNT','Factor Discount Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','USSGL_TRANSACTION_CODE',222,'Code defined by public sector accounting','USSGL_TRANSACTION_CODE','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','USSGL_TRANSACTION_CODE','Ussgl Transaction Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','USSGL_TRANSACTION_CODE_CONTEXT',222,'Descriptive Flexfield structure defining column for public sector accounting','USSGL_TRANSACTION_CODE_CONTEXT','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','USSGL_TRANSACTION_CODE_CONTEXT','Ussgl Transaction Code Context','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','DOC_SEQUENCE_VALUE',222,'Document number assigned to the receipt.','DOC_SEQUENCE_VALUE','','~T~D~2','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','DOC_SEQUENCE_VALUE','Doc Sequence Value','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','DOC_SEQUENCE_ID',222,'Identifier of the sequence used to assign document number to the receipt','DOC_SEQUENCE_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','DOC_SEQUENCE_ID','Doc Sequence Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','VAT_TAX_ID',222,'Internal identifier of the tax rate. Applicable only for miscellaneous receipts.','VAT_TAX_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','VAT_TAX_ID','Vat Tax Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','REFERENCE_TYPE',222,'Reference type associated to the miscellaneous receipt. Reference types supported are PAYMENT_BATCH, PAYMENT, CREDIT_MEMO, RECEIPT and REMITTANCE.','REFERENCE_TYPE','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','REFERENCE_TYPE','Reference Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','REFERENCE_ID',222,'Identifier of the referred transaction for the miscellaneous receipt. Foreign key to the AR_BATCHES, AR_CASH_RECEIPTS, or AP_CHECKS tables based on the reference type selected.','REFERENCE_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','REFERENCE_ID','Reference Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','CUSTOMER_RECEIPT_REFERENCE',222,'Non-validated text field to store reference information entered by customer. This column corresponds to Reference field on the Receipt form.','CUSTOMER_RECEIPT_REFERENCE','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','CUSTOMER_RECEIPT_REFERENCE','Customer Receipt Reference','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','OVERRIDE_REMIT_ACCOUNT_FLAG',222,'Flag to indicate whether remittance bank can be overridden during remittance process. Values ''Y'' or null indicates yes and ''N'' indicates no.','OVERRIDE_REMIT_ACCOUNT_FLAG','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','OVERRIDE_REMIT_ACCOUNT_FLAG','Override Remit Account Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ORG_ID',222,'Operating unit identifier','ORG_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','ORG_ID','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ANTICIPATED_CLEARING_DATE',222,'Date the receipt is expected to clear the bank','ANTICIPATED_CLEARING_DATE','','','','ANONYMOUS','DATE','AR_CASH_RECEIPTS','ANTICIPATED_CLEARING_DATE','Anticipated Clearing Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE1',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE1','Global Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE2',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE2','Global Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE3',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE3','Global Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE4',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE4','Global Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE5',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE5','Global Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE6',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE6','Global Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE7',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE7','Global Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE8',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE8','Global Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE9',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE9','Global Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE10',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE10','Global Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE11',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE11','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE11','Global Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE12',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE12','Global Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE13',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE13','Global Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE14',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE14','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE14','Global Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE15',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE15','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE15','Global Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE16',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE16','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE16','Global Attribute16','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE17',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE17','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE17','Global Attribute17','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE18',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE18','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE18','Global Attribute18','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE19',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE19','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE19','Global Attribute19','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE20',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE20','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE20','Global Attribute20','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE_CATEGORY',222,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE_CATEGORY','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','GLOBAL_ATTRIBUTE_CATEGORY','Global Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ISSUER_NAME',222,'Issuer name of Notes Receivable','ISSUER_NAME','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','ISSUER_NAME','Issuer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ISSUE_DATE',222,'Date this Note Receivable was issued','ISSUE_DATE','','','','ANONYMOUS','DATE','AR_CASH_RECEIPTS','ISSUE_DATE','Issue Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ISSUER_BANK_BRANCH_ID',222,'Bank / Branch issuing the Note Receivable','ISSUER_BANK_BRANCH_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','ISSUER_BANK_BRANCH_ID','Issuer Bank Branch Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','CUSTOMER_BANK_BRANCH_ID',222,'No longer in use.','CUSTOMER_BANK_BRANCH_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','CUSTOMER_BANK_BRANCH_ID','Customer Bank Branch Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','MRC_EXCHANGE_RATE_TYPE',222,'No longer in use. Multiple Reporting Currencies columns are obsolete in R12. Accounting information for reporting currency is stored in Subledger Accounting tables in R12.','MRC_EXCHANGE_RATE_TYPE','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','MRC_EXCHANGE_RATE_TYPE','Mrc Exchange Rate Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','MRC_EXCHANGE_RATE',222,'No longer in use. Multiple Reporting Currencies columns are obsolete in R12. Accounting information for reporting currency is stored in Subledger Accounting tables in R12.','MRC_EXCHANGE_RATE','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','MRC_EXCHANGE_RATE','Mrc Exchange Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','MRC_EXCHANGE_DATE',222,'No longer in use. Multiple Reporting Currencies columns are obsolete in R12. Accounting information for reporting currency is stored in Subledger Accounting tables in R12.','MRC_EXCHANGE_DATE','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','MRC_EXCHANGE_DATE','Mrc Exchange Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','PAYMENT_SERVER_ORDER_NUM',222,'Internal identifier of credit card authorization in Oracle Payments','PAYMENT_SERVER_ORDER_NUM','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','PAYMENT_SERVER_ORDER_NUM','Payment Server Order Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','APPROVAL_CODE',222,'Credit card authorization approval code received from external payment systems','APPROVAL_CODE','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','APPROVAL_CODE','Approval Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ADDRESS_VERIFICATION_CODE',222,'No longer in use.','ADDRESS_VERIFICATION_CODE','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','ADDRESS_VERIFICATION_CODE','Address Verification Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','TAX_RATE',222,'The new tax rate entered when the default rate is overridden for an ad hoc tax code for miscellaneous receipts','TAX_RATE','','~T~D~2','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','TAX_RATE','Tax Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ACTUAL_VALUE_DATE',222,'The date cash is withdrawn or deposited in a bank account','ACTUAL_VALUE_DATE','','','','ANONYMOUS','DATE','AR_CASH_RECEIPTS','ACTUAL_VALUE_DATE','Actual Value Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','POSTMARK_DATE',222,'The postmark date of the Receipt. Used in Trade Accounting.','POSTMARK_DATE','','','','ANONYMOUS','DATE','AR_CASH_RECEIPTS','POSTMARK_DATE','Postmark Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','CASH_RECEIPT_ID',222,'Unique internal identifier of the cash receipt and primary key for this table.','CASH_RECEIPT_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','CASH_RECEIPT_ID','Cash Receipt Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','LAST_UPDATED_BY',222,'Standard who column. User who last updated this row.','LAST_UPDATED_BY','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','LAST_UPDATED_BY','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','LAST_UPDATE_DATE',222,'Standard Who column. Date when a user last updated this row.','LAST_UPDATE_DATE','','','','ANONYMOUS','DATE','AR_CASH_RECEIPTS','LAST_UPDATE_DATE','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','LAST_UPDATE_LOGIN',222,'Standard who column. Operating system login of user who last updated this row.','LAST_UPDATE_LOGIN','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','LAST_UPDATE_LOGIN','Last Update Login','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','CREATED_BY',222,'Standard who column. User who created this row.','CREATED_BY','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','CREATED_BY','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','CREATION_DATE',222,'Standard who column. Date when this row was created.','CREATION_DATE','','','','ANONYMOUS','DATE','AR_CASH_RECEIPTS','CREATION_DATE','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','AMOUNT',222,'Receipt amount in the currency of the receipt.','AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','AMOUNT','Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','SET_OF_BOOKS_ID',222,'Internal identifier of the ledger to which the accounting entries for this receipt will be posted.','SET_OF_BOOKS_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','SET_OF_BOOKS_ID','Set Of Books Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','CURRENCY_CODE',222,'Currency code of the receipt.','CURRENCY_CODE','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','CURRENCY_CODE','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','RECEIVABLES_TRX_ID',222,'Internal identifier of the transaction associated with the receipt on the header.','RECEIVABLES_TRX_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','RECEIVABLES_TRX_ID','Receivables Trx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','PAY_FROM_CUSTOMER',222,'Internal identifier of the customer making the payment.','PAY_FROM_CUSTOMER','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','PAY_FROM_CUSTOMER','Pay From Customer','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','STATUS',222,'Identifies the application status of the receipt. The application statuses are:APP - AppliedUNAPP - UnappliedUNID - Unidentified NSF - Non-sufficient fundsREV - Reversed receiptSTOP - Stop payment','STATUS','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','STATUS','Status','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','TYPE',222,'Identifies the category of the receipt. The column will have value ''CASH'' for standard receipts and ''MISC'' for miscellaneous receipts.','TYPE','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','TYPE','Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','RECEIPT_NUMBER',222,'Receipt number. System allows duplicate receipt numbers. Receipt number is not a primary key for records in this table.','RECEIPT_NUMBER','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','RECEIPT_NUMBER','Receipt Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','RECEIPT_DATE',222,'Receipt date.','RECEIPT_DATE','','','','ANONYMOUS','DATE','AR_CASH_RECEIPTS','RECEIPT_DATE','Receipt Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','MISC_PAYMENT_SOURCE',222,'Non-validated text field to store the source from which the miscellaneous payment is received. This column corresponds to the ''Paid By Name'' field on the Receipts form.','MISC_PAYMENT_SOURCE','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','MISC_PAYMENT_SOURCE','Misc Payment Source','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','COMMENTS',222,'Comments entered for the receipt.','COMMENTS','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','COMMENTS','Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','DISTRIBUTION_SET_ID',222,'Internal identifier of the distribution set associated with the miscellaneous receipt. Distribution set is derived from the receivable activity provided for the receipt.','DISTRIBUTION_SET_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','DISTRIBUTION_SET_ID','Distribution Set Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','REVERSAL_DATE',222,'Date on which the receipt is reversed.','REVERSAL_DATE','','','','ANONYMOUS','DATE','AR_CASH_RECEIPTS','REVERSAL_DATE','Reversal Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','REVERSAL_CATEGORY',222,'The reversal category code selected by the user when the receipt is reversed. Reversal category codes are stored in the system lookup REVERSAL_CATEGORY_TYPE.','REVERSAL_CATEGORY','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','REVERSAL_CATEGORY','Reversal Category','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','REVERSAL_REASON_CODE',222,'The reversal reason code selected by the user when the receipt is reversed. Reversal reason codes are stored in the extensible lookup CKAJST_REASON.','REVERSAL_REASON_CODE','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','REVERSAL_REASON_CODE','Reversal Reason Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','REVERSAL_COMMENTS',222,'Comments entered by the user when receipt is reversed.','REVERSAL_COMMENTS','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','REVERSAL_COMMENTS','Reversal Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','EXCHANGE_RATE_TYPE',222,'Currency exchange rate type provided for receipts in foreign currency.','EXCHANGE_RATE_TYPE','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','EXCHANGE_RATE_TYPE','Exchange Rate Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','EXCHANGE_RATE',222,'Currency exchange rate used to convert foreign currency receipt amount into functional currency','EXCHANGE_RATE','','~T~D~2','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','EXCHANGE_RATE','Exchange Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','EXCHANGE_DATE',222,'Date provided for deriving currency exchange rate.','EXCHANGE_DATE','','','','ANONYMOUS','DATE','AR_CASH_RECEIPTS','EXCHANGE_DATE','Exchange Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ATTRIBUTE_CATEGORY',222,'Descriptive flexfield context column.','ATTRIBUTE_CATEGORY','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','ATTRIBUTE_CATEGORY','Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ATTRIBUTE1',222,'Descriptive flexfield segment','ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','ATTRIBUTE1','Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ATTRIBUTE2',222,'Descriptive flexfield segment','ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','ATTRIBUTE2','Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ATTRIBUTE3',222,'Descriptive flexfield segment','ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','ATTRIBUTE3','Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ATTRIBUTE4',222,'Descriptive flexfield segment','ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','ATTRIBUTE4','Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ATTRIBUTE5',222,'Descriptive flexfield segment','ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','ATTRIBUTE5','Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ATTRIBUTE6',222,'Descriptive flexfield segment','ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','ATTRIBUTE6','Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ATTRIBUTE7',222,'Descriptive flexfield segment','ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','ATTRIBUTE7','Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ATTRIBUTE8',222,'Descriptive flexfield segment','ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','ATTRIBUTE8','Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ATTRIBUTE9',222,'Descriptive flexfield segment','ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','ATTRIBUTE9','Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ATTRIBUTE10',222,'Descriptive flexfield segment','ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','ATTRIBUTE10','Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','REMITTANCE_BANK_ACCOUNT_ID',222,'Internal identifier of the bank account to which the receipt will be remitted.','REMITTANCE_BANK_ACCOUNT_ID','','','','ANONYMOUS','NUMBER','AR_CASH_RECEIPTS','REMITTANCE_BANK_ACCOUNT_ID','Remittance Bank Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ATTRIBUTE11',222,'Descriptive flexfield segment','ATTRIBUTE11','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','ATTRIBUTE11','Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ATTRIBUTE12',222,'Descriptive flexfield segment','ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','ATTRIBUTE12','Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ATTRIBUTE13',222,'Descriptive flexfield segment','ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','ATTRIBUTE13','Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ATTRIBUTE14',222,'Descriptive flexfield segment','ATTRIBUTE14','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','ATTRIBUTE14','Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'AR_CASH_RECEIPTS','ATTRIBUTE15',222,'Descriptive flexfield segment','ATTRIBUTE15','','','','ANONYMOUS','VARCHAR2','AR_CASH_RECEIPTS','ATTRIBUTE15','Attribute15','','','','US');
--Inserting Object Components for AR_CASH_RECEIPTS
--Inserting Object Component Joins for AR_CASH_RECEIPTS
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report LOV Data for Unapplied Rec Register - Offshore
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Unapplied Rec Register - Offshore
xxeis.eis_rsc_ins.lov( 222,'select distinct party_name from hz_parties p, hz_cust_accounts c
    where p.party_id = c.party_id','null','Customer Name','Displays List of Values for Customer Name ','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select account_number from hz_cust_accounts','null','Customer Number','Displays List of Values for Customer Number','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select CURRENCY_CODE, NAME from
FND_CURRENCIES_VL
WHERE CURRENCY_FLAG = ''Y''
AND ENABLED_FLAG in (''Y'', ''N'')
AND CURRENCY_CODE NOT IN(''ANY'')
ORDER BY CURRENCY_CODE','','CURRENCY_CODE','CURRENCY_CODE','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select distinct name from ar_batches','','BATCH NAME','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT DISTINCT receipt_number FROM ar_cash_receipts','','RECEIPT NUMBER','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'Select name from ar_Receipt_methods','','Payment Method','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report Data for Unapplied Rec Register - Offshore
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Unapplied Rec Register - Offshore
xxeis.eis_rsc_utility.delete_report_rows( 'Unapplied Rec Register - Offshore' );
--Inserting Report - Unapplied Rec Register - Offshore
xxeis.eis_rsc_ins.r( 222,'Unapplied Rec Register - Offshore','','This report displays the Unapplied Receipts. The report has customer name and customer number as parameters. The report can be used to monitor receipts that have been unapplied to invoices and could work as a basis to receivables users applying receipts.','','','','LA023190','EIS_AR_UNAPPLIED_RECEIPTS_V','Y','','','LA023190','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Unapplied Rec Register - Offshore
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'BATCH_NAME','Batch Name','Name of the payment batch','','','','','3','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'BATCH_SOURCE_NAME','Batch Source Name','Name of the payment batch source','','','','','4','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'CLAIM_AMT','Claim Amt','Claim Amt','','','','','14','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'COMPANY_NAME','Company Name','Accounting books name','','','','','17','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'CUSTOMER_CLASS_CODE','Customer Class Code','Customer class indentifier','','','','','16','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'CUSTOMER_NAME','Customer Name','Name of this party','','','','','1','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'CUSTOMER_NUMBER','Customer Number','Account Number','','','','','2','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'FUNCTIONAL_CURRENCY','Functional Currency','Currency','','','','','9','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'FUNCTIONAL_CURRENCY_PRECISION','Functional Curr Precision','Maximum number of digits to the right of decimal point','','','','','10','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'ON_ACCOUNT_AMT','On Account Amt','On Account Amt','','','','','11','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'RECEIPT_DATE','Receipt Date','Date of the payment entry','','','','','7','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'RECEIPT_METHOD','Receipt Method','Name of the receipt method','','','','','6','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'RECEIPT_NUMBER','Receipt Number','Cash receipt number','','','','','5','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'UNAPPLIED_AMT','Unapplied Amt','Unapplied Amt','','','','','12','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'UNID_FLAG','Unid Flag','Unid Flag','','','','','15','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'GL_DATE','GL Date','Gl Date','','','','','19','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'CURRENCY_CODE','Entered Currency','Currency code of the payment batch associated with this payment entry','','','','','18','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'COMMENTS','Comments','Comments about this receipt record','','','','','8','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'MATURITY_DATE','Maturity Date','Due date of this installment','','','','','13','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'PARTY_NAME','Party Name','Party Name','','','','','20','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Unapplied Rec Register - Offshore',222,'CASH_APPLN_OWNER_ID','Cash Appln Owner Id','Identifier of the cash application owner to whom the receipt is assigned for application.','','','','','21','N','','','','','','','','LA023190','N','N','','EIS_AR_UNAPPLIED_RECEIPTS_V','AR_CASH_RECEIPTS','RCPT','SUM','US','');
--Inserting Report Parameters - Unapplied Rec Register - Offshore
xxeis.eis_rsc_ins.rp( 'Unapplied Rec Register - Offshore',222,'Customer Name','Customer Name','PARTY_NAME','IN','Customer Name','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','LA023190','Y','N','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Unapplied Rec Register - Offshore',222,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','Customer Number','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','LA023190','Y','N','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Unapplied Rec Register - Offshore',222,'GL Date Low','GL Date Low','GL_DATE','>=','','','DATE','N','Y','1','','Y','CONSTANT','LA023190','Y','N','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Unapplied Rec Register - Offshore',222,'Batch Source Name','Batch Source Name','BATCH_SOURCE_NAME','IN','','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','LA023190','Y','N','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Unapplied Rec Register - Offshore',222,'Batch Name','Batch Name','BATCH_NAME','IN','BATCH NAME','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','LA023190','Y','N','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Unapplied Rec Register - Offshore',222,'Receipt Number','Receipt Number','RECEIPT_NUMBER','IN','RECEIPT NUMBER','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','LA023190','Y','N','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Unapplied Rec Register - Offshore',222,'GL Date High','GL Date High','GL_DATE','<=','','','DATE','N','Y','2','','Y','CONSTANT','LA023190','Y','N','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Unapplied Rec Register - Offshore',222,'Entered Currency','Entered Currency','CURRENCY_CODE','IN','CURRENCY_CODE','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','LA023190','Y','N','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Unapplied Rec Register - Offshore',222,'Receipt Date Low','Receipt Date Low','RECEIPT_DATE','>=','','','DATE','N','Y','9','','Y','CONSTANT','LA023190','Y','N','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Unapplied Rec Register - Offshore',222,'Receipt Date High','Receipt Date High','RECEIPT_DATE','<=','','','DATE','N','Y','10','','Y','CONSTANT','LA023190','Y','N','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Unapplied Rec Register - Offshore',222,'Receipt Method Low','Receipt Method Low','RECEIPT_METHOD','>=','Payment Method','','VARCHAR2','N','Y','11','','Y','CONSTANT','LA023190','Y','N','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Unapplied Rec Register - Offshore',222,'Receipt Method High','Receipt Method High','RECEIPT_METHOD','<=','Payment Method','','VARCHAR2','N','Y','12','','Y','CONSTANT','LA023190','Y','N','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Unapplied Rec Register - Offshore',222,'On Account Amt','On Account Amt','ON_ACCOUNT_AMT','>','','','NUMERIC','N','Y','13','','Y','CONSTANT','LA023190','Y','N','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','US','');
--Inserting Dependent Parameters - Unapplied Rec Register - Offshore
--Inserting Report Conditions - Unapplied Rec Register - Offshore
xxeis.eis_rsc_ins.rcnh( 'Unapplied Rec Register - Offshore',222,'BATCH_NAME IN :Batch Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BATCH_NAME','','Batch Name','','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','','','','IN','Y','Y','','','','','1',222,'Unapplied Rec Register - Offshore','BATCH_NAME IN :Batch Name ');
xxeis.eis_rsc_ins.rcnh( 'Unapplied Rec Register - Offshore',222,'BATCH_SOURCE_NAME IN :Batch Source Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BATCH_SOURCE_NAME','','Batch Source Name','','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','','','','IN','Y','Y','','','','','1',222,'Unapplied Rec Register - Offshore','BATCH_SOURCE_NAME IN :Batch Source Name ');
xxeis.eis_rsc_ins.rcnh( 'Unapplied Rec Register - Offshore',222,'CURRENCY_CODE IN :Entered Currency ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CURRENCY_CODE','','Entered Currency','','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','','','','IN','Y','Y','','','','','1',222,'Unapplied Rec Register - Offshore','CURRENCY_CODE IN :Entered Currency ');
xxeis.eis_rsc_ins.rcnh( 'Unapplied Rec Register - Offshore',222,'CUSTOMER_NUMBER IN :Customer Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_NUMBER','','Customer Number','','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','','','','IN','Y','Y','','','','','1',222,'Unapplied Rec Register - Offshore','CUSTOMER_NUMBER IN :Customer Number ');
xxeis.eis_rsc_ins.rcnh( 'Unapplied Rec Register - Offshore',222,'GL_DATE <= :GL Date High ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_DATE','','GL Date High','','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',222,'Unapplied Rec Register - Offshore','GL_DATE <= :GL Date High ');
xxeis.eis_rsc_ins.rcnh( 'Unapplied Rec Register - Offshore',222,'GL_DATE >= :GL Date Low ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_DATE','','GL Date Low','','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',222,'Unapplied Rec Register - Offshore','GL_DATE >= :GL Date Low ');
xxeis.eis_rsc_ins.rcnh( 'Unapplied Rec Register - Offshore',222,'ON_ACCOUNT_AMT > :On Account Amt ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ON_ACCOUNT_AMT','','On Account Amt','','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','','','','GREATER_THAN','Y','Y','','','','','1',222,'Unapplied Rec Register - Offshore','ON_ACCOUNT_AMT > :On Account Amt ');
xxeis.eis_rsc_ins.rcnh( 'Unapplied Rec Register - Offshore',222,'PARTY_NAME IN :Customer Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PARTY_NAME','','Customer Name','','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','','','','IN','Y','Y','','','','','1',222,'Unapplied Rec Register - Offshore','PARTY_NAME IN :Customer Name ');
xxeis.eis_rsc_ins.rcnh( 'Unapplied Rec Register - Offshore',222,'RECEIPT_DATE <= :Receipt Date High ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','RECEIPT_DATE','','Receipt Date High','','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',222,'Unapplied Rec Register - Offshore','RECEIPT_DATE <= :Receipt Date High ');
xxeis.eis_rsc_ins.rcnh( 'Unapplied Rec Register - Offshore',222,'RECEIPT_DATE >= :Receipt Date Low ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','RECEIPT_DATE','','Receipt Date Low','','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',222,'Unapplied Rec Register - Offshore','RECEIPT_DATE >= :Receipt Date Low ');
xxeis.eis_rsc_ins.rcnh( 'Unapplied Rec Register - Offshore',222,'RECEIPT_METHOD <= :Receipt Method High ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','RECEIPT_METHOD','','Receipt Method High','','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',222,'Unapplied Rec Register - Offshore','RECEIPT_METHOD <= :Receipt Method High ');
xxeis.eis_rsc_ins.rcnh( 'Unapplied Rec Register - Offshore',222,'RECEIPT_METHOD >= :Receipt Method Low ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','RECEIPT_METHOD','','Receipt Method Low','','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',222,'Unapplied Rec Register - Offshore','RECEIPT_METHOD >= :Receipt Method Low ');
xxeis.eis_rsc_ins.rcnh( 'Unapplied Rec Register - Offshore',222,'RECEIPT_NUMBER IN :Receipt Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','RECEIPT_NUMBER','','Receipt Number','','','','','EIS_AR_UNAPPLIED_RECEIPTS_V','','','','','','IN','Y','Y','','','','','1',222,'Unapplied Rec Register - Offshore','RECEIPT_NUMBER IN :Receipt Number ');
--Inserting Report Sorts - Unapplied Rec Register - Offshore
xxeis.eis_rsc_ins.rs( 'Unapplied Rec Register - Offshore',222,'CUSTOMER_NUMBER','ASC','LA023190','1','');
xxeis.eis_rsc_ins.rs( 'Unapplied Rec Register - Offshore',222,'RECEIPT_NUMBER','ASC','LA023190','2','');
xxeis.eis_rsc_ins.rs( 'Unapplied Rec Register - Offshore',222,'PARTY_NAME','ASC','LA023190','3','');
--Inserting Report Triggers - Unapplied Rec Register - Offshore
--inserting report templates - Unapplied Rec Register - Offshore
xxeis.eis_rsc_ins.r_tem( 'Unapplied Rec Register - Offshore','Unapplied Rec Register - Offshore','Seeded template for Unapplied Rec Register - Offshore','','','','','','','','','','','Unapplied Rec Register - Offshore.rtf','LA023190','X','','','Y','Y','','');
--Inserting Report Portals - Unapplied Rec Register - Offshore
--inserting report dashboards - Unapplied Rec Register - Offshore
xxeis.eis_rsc_ins.R_dash( 'Unapplied Rec Register - Offshore','AR Unapplied Receipts Report','AR Unapplied Receipts Report','vertical clustered bar','large','Receipt Method','Receipt Method','Claim Amt','Claim Amt','Sum','LA023190');
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Unapplied Rec Register - Offshore','222','EIS_AR_UNAPPLIED_RECEIPTS_V','EIS_AR_UNAPPLIED_RECEIPTS_V','N','');
xxeis.eis_rsc_ins.rviews( 'Unapplied Rec Register - Offshore','222','EIS_AR_UNAPPLIED_RECEIPTS_V','AR_CASH_RECEIPTS','N','RCPT');
--inserting report security - Unapplied Rec Register - Offshore
xxeis.eis_rsc_ins.rsec( 'Unapplied Rec Register - Offshore','','AB063501','',222,'LA023190','','N','');
--Inserting Report Pivots - Unapplied Rec Register - Offshore
xxeis.eis_rsc_ins.rpivot( 'Unapplied Rec Register - Offshore',222,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Unapplied Rec Register - Offshore',222,'Pivot','BATCH_NAME','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Unapplied Rec Register - Offshore',222,'Pivot','BATCH_SOURCE_NAME','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Unapplied Rec Register - Offshore',222,'Pivot','CLAIM_AMT','DATA_FIELD','SUM','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Unapplied Rec Register - Offshore',222,'Pivot','COMPANY_NAME','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Unapplied Rec Register - Offshore',222,'Pivot','CUSTOMER_CLASS_CODE','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Unapplied Rec Register - Offshore',222,'Pivot','CUSTOMER_NAME','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Unapplied Rec Register - Offshore',222,'Pivot','CUSTOMER_NUMBER','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Unapplied Rec Register - Offshore',222,'Pivot','FUNCTIONAL_CURRENCY','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Unapplied Rec Register - Offshore',222,'Pivot','ON_ACCOUNT_AMT','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Unapplied Rec Register - Offshore',222,'Pivot','RECEIPT_DATE','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Unapplied Rec Register - Offshore',222,'Pivot','RECEIPT_METHOD','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Unapplied Rec Register - Offshore',222,'Pivot','RECEIPT_NUMBER','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Unapplied Rec Register - Offshore',222,'Pivot','UNAPPLIED_AMT','DATA_FIELD','SUM','','3','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Unapplied Rec Register - Offshore',222,'Pivot','PARTY_NAME','ROW_FIELD','','','2','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- Unapplied Rec Register - Offshore
xxeis.eis_rsc_ins.rv( 'Unapplied Rec Register - Offshore','','Unapplied Rec Register - Offshore','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
