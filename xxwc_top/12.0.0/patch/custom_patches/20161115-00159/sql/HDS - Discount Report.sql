--Report Name            : HDS - Discount Report 
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_GL_AR_180_DREAM_RPT_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_GL_AR_180_DREAM_RPT_V
xxeis.eis_rsc_ins.v( 'XXEIS_GL_AR_180_DREAM_RPT_V',101,'','','','','KP012542','APPS','Xxeis Gl Ar 180 Dream Rpt V','XGA1DRV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_GL_AR_180_DREAM_RPT_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_GL_AR_180_DREAM_RPT_V',101,FALSE);
--Inserting Object Columns for XXEIS_GL_AR_180_DREAM_RPT_V
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','REVERSAL_DATE',101,'Reversal Date','REVERSAL_DATE','','','','KP012542','DATE','','','Reversal Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','GCC#50368#PRODUCT',101,'Gcc#50368#Product','GCC#50368#PRODUCT','','','','KP012542','VARCHAR2','','','Gcc#50368#Product','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','GCC#50328#FURTURE_USE',101,'Gcc#50328#Furture Use','GCC#50328#FURTURE_USE','','','','KP012542','VARCHAR2','','','Gcc#50328#Furture Use','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','JE_CATEGORY',101,'Je Category','JE_CATEGORY','','','','KP012542','VARCHAR2','','','Je Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','GCC#50368#DIVISION',101,'Gcc#50368#Division','GCC#50368#DIVISION','','','','KP012542','VARCHAR2','','','Gcc#50368#Division','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','LINE_ENT_CR',101,'Line Ent Cr','LINE_ENT_CR','','~T~D~2','','KP012542','NUMBER','','','Line Ent Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','SLA_LINE_ENTERED_NET',101,'Sla Line Entered Net','SLA_LINE_ENTERED_NET','','~T~D~2','','KP012542','NUMBER','','','Sla Line Entered Net','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','RECEIPT_COMMENTS',101,'Receipt Comments','RECEIPT_COMMENTS','','','','KP012542','VARCHAR2','','','Receipt Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','RECEIPT_CLASS',101,'Receipt Class','RECEIPT_CLASS','','','','KP012542','VARCHAR2','','','Receipt Class','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','GCC#50368#ACCOUNT',101,'Gcc#50368#Account','GCC#50368#ACCOUNT','','','','KP012542','VARCHAR2','','','Gcc#50368#Account','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','CUSTOMER_NUMBER',101,'Customer Number','CUSTOMER_NUMBER','','','','KP012542','VARCHAR2','','','Customer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','BATCH_NAME',101,'Batch Name','BATCH_NAME','','','','KP012542','VARCHAR2','','','Batch Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','GCC#50328#PRODUCT',101,'Gcc#50328#Product','GCC#50328#PRODUCT','','','','KP012542','VARCHAR2','','','Gcc#50328#Product','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','GCC#50328#LOCATION',101,'Gcc#50328#Location','GCC#50328#LOCATION','','','','KP012542','VARCHAR2','','','Gcc#50328#Location','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','PAYMENT_METHOD',101,'Payment Method','PAYMENT_METHOD','','','','KP012542','VARCHAR2','','','Payment Method','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','REMIT_BANK_ACCOUNT',101,'Remit Bank Account','REMIT_BANK_ACCOUNT','','','','KP012542','VARCHAR2','','','Remit Bank Account','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','LINE_ACCTD_DR',101,'Line Acctd Dr','LINE_ACCTD_DR','','~T~D~2','','KP012542','NUMBER','','','Line Acctd Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','SLA_DIST_ACCOUNTED_DR',101,'Sla Dist Accounted Dr','SLA_DIST_ACCOUNTED_DR','','~T~D~2','','KP012542','NUMBER','','','Sla Dist Accounted Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','SLA_LINE_ENTERED_CR',101,'Sla Line Entered Cr','SLA_LINE_ENTERED_CR','','~T~D~2','','KP012542','NUMBER','','','Sla Line Entered Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','CUSTOMER_OR_VENDOR',101,'Customer Or Vendor','CUSTOMER_OR_VENDOR','','','','KP012542','VARCHAR2','','','Customer Or Vendor','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','ENTERED_CR',101,'Entered Cr','ENTERED_CR','','~T~D~2','','KP012542','NUMBER','','','Entered Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','CUSTOMER_RECEIPT_REF',101,'Customer Receipt Ref','CUSTOMER_RECEIPT_REF','','','','KP012542','VARCHAR2','','','Customer Receipt Ref','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','SLA_DIST_ENTERED_CR',101,'Sla Dist Entered Cr','SLA_DIST_ENTERED_CR','','~T~D~2','','KP012542','NUMBER','','','Sla Dist Entered Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','DTL_ID',101,'Dtl Id','DTL_ID','','','','KP012542','NUMBER','','','Dtl Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','SOURCE',101,'Source','SOURCE','','','','KP012542','VARCHAR2','','','Source','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','REVERSAL_REASON_CODE',101,'Reversal Reason Code','REVERSAL_REASON_CODE','','','','KP012542','VARCHAR2','','','Reversal Reason Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','SLA_LINE_ACCOUNTED_NET',101,'Sla Line Accounted Net','SLA_LINE_ACCOUNTED_NET','','~T~D~2','','KP012542','NUMBER','','','Sla Line Accounted Net','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','GCC#50328#FUTURE_USE_2',101,'Gcc#50328#Future Use 2','GCC#50328#FUTURE_USE_2','','','','KP012542','VARCHAR2','','','Gcc#50328#Future Use 2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','SLA_DIST_ACCOUNTED_CR',101,'Sla Dist Accounted Cr','SLA_DIST_ACCOUNTED_CR','','~T~D~2','','KP012542','NUMBER','','','Sla Dist Accounted Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','ACC_DATE',101,'Acc Date','ACC_DATE','','','','KP012542','DATE','','','Acc Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','GCC#50368#FUTURE_USE',101,'Gcc#50368#Future Use','GCC#50368#FUTURE_USE','','','','KP012542','VARCHAR2','','','Gcc#50368#Future Use','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','GCC#50328#COST_CENTER',101,'Gcc#50328#Cost Center','GCC#50328#COST_CENTER','','','','KP012542','VARCHAR2','','','Gcc#50328#Cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','GCC#50328#PROJECT_CODE',101,'Gcc#50328#Project Code','GCC#50328#PROJECT_CODE','','','','KP012542','VARCHAR2','','','Gcc#50328#Project Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','ACCOUNTED_DR',101,'Accounted Dr','ACCOUNTED_DR','','~T~D~2','','KP012542','NUMBER','','','Accounted Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','GL_ACCOUNT_STRING',101,'Gl Account String','GL_ACCOUNT_STRING','','','','KP012542','VARCHAR2','','','Gl Account String','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','RECEIPT_NUMBER',101,'Receipt Number','RECEIPT_NUMBER','','','','KP012542','VARCHAR2','','','Receipt Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','NAME',101,'Name','NAME','','','','KP012542','VARCHAR2','','','Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','SLA_LINE_ENTERED_DR',101,'Sla Line Entered Dr','SLA_LINE_ENTERED_DR','','~T~D~2','','KP012542','NUMBER','','','Sla Line Entered Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','PERIOD_NAME',101,'Period Name','PERIOD_NAME','','','','KP012542','VARCHAR2','','','Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','TRANSACTION_NUMBER',101,'Transaction Number','TRANSACTION_NUMBER','','','','KP012542','VARCHAR2','','','Transaction Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','RECEIPT_STATUS',101,'Receipt Status','RECEIPT_STATUS','','','','KP012542','VARCHAR2','','','Receipt Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','GCC#50328#ACCOUNT',101,'Gcc#50328#Account','GCC#50328#ACCOUNT','','','','KP012542','VARCHAR2','','','Gcc#50328#Account','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','REVERSAL_CATEGORY',101,'Reversal Category','REVERSAL_CATEGORY','','','','KP012542','VARCHAR2','','','Reversal Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','RECEIPT_DATE',101,'Receipt Date','RECEIPT_DATE','','','','KP012542','DATE','','','Receipt Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','ADJUSTMENT_NUMBER',101,'Adjustment Number','ADJUSTMENT_NUMBER','','','','KP012542','VARCHAR2','','','Adjustment Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','LINE_ACCTD_CR',101,'Line Acctd Cr','LINE_ACCTD_CR','','~T~D~2','','KP012542','NUMBER','','','Line Acctd Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','RECEIPT_STATE',101,'Receipt State','RECEIPT_STATE','','','','KP012542','VARCHAR2','','','Receipt State','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','DTL_ENTITY_ID',101,'Dtl Entity Id','DTL_ENTITY_ID','','','','KP012542','NUMBER','','','Dtl Entity Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','SLA_DIST_ENTERED_DR',101,'Sla Dist Entered Dr','SLA_DIST_ENTERED_DR','','~T~D~2','','KP012542','NUMBER','','','Sla Dist Entered Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','LINE_ENT_DR',101,'Line Ent Dr','LINE_ENT_DR','','~T~D~2','','KP012542','NUMBER','','','Line Ent Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','SLA_LINE_ACCOUNTED_DR',101,'Sla Line Accounted Dr','SLA_LINE_ACCOUNTED_DR','','~T~D~2','','KP012542','NUMBER','','','Sla Line Accounted Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','REVERSAL_REASON',101,'Reversal Reason','REVERSAL_REASON','','','','KP012542','VARCHAR2','','','Reversal Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','ADJUSMENT_NAME',101,'Adjusment Name','ADJUSMENT_NAME','','','','KP012542','VARCHAR2','','','Adjusment Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','LINE_DESCR',101,'Line Descr','LINE_DESCR','','','','KP012542','VARCHAR2','','','Line Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','TYPE',101,'Type','TYPE','','','','KP012542','VARCHAR2','','','Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','SLA_DIST_ENTERED_NET',101,'Sla Dist Entered Net','SLA_DIST_ENTERED_NET','','~T~D~2','','KP012542','NUMBER','','','Sla Dist Entered Net','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','ENTRY',101,'Entry','ENTRY','','','','KP012542','VARCHAR2','','','Entry','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','ACCOUNTED_CR',101,'Accounted Cr','ACCOUNTED_CR','','~T~D~2','','KP012542','NUMBER','','','Accounted Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','GCC#50368#SUBACCOUNT',101,'Gcc#50368#Subaccount','GCC#50368#SUBACCOUNT','','','','KP012542','VARCHAR2','','','Gcc#50368#Subaccount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','ENTERED_DR',101,'Entered Dr','ENTERED_DR','','~T~D~2','','KP012542','NUMBER','','','Entered Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','SLA_LINE_ACCOUNTED_CR',101,'Sla Line Accounted Cr','SLA_LINE_ACCOUNTED_CR','','~T~D~2','','KP012542','NUMBER','','','Sla Line Accounted Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','SLA_DIST_ACCOUNTED_NET',101,'Sla Dist Accounted Net','SLA_DIST_ACCOUNTED_NET','','~T~D~2','','KP012542','NUMBER','','','Sla Dist Accounted Net','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','REVERSAL_COMMENTS',101,'Reversal Comments','REVERSAL_COMMENTS','','','','KP012542','VARCHAR2','','','Reversal Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_GL_AR_180_DREAM_RPT_V','GCC#50368#DEPARTMENT',101,'Gcc#50368#Department','GCC#50368#DEPARTMENT','','','','KP012542','VARCHAR2','','','Gcc#50368#Department','','','','US');
--Inserting Object Components for XXEIS_GL_AR_180_DREAM_RPT_V
--Inserting Object Component Joins for XXEIS_GL_AR_180_DREAM_RPT_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report LOV Data for HDS - Discount Report 
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS - Discount Report 
xxeis.eis_rsc_ins.lov( 101,'select  per.period_name , led.name ledger_name, per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     GL_SECURITY_PKG.VALIDATE_ACCESS(led.ledger_ID) = ''TRUE''
and     led.accounted_period_type = per.period_type','null','EIS_GL_PERIOD_LOV','Derives GL Periods based on the corresponding setups in the sets of books','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 101,'SELECT distinct user_je_category_name FROM gl_je_categories','','EIS_GL_JE_CATEGORY_LOV','LOV of all available Journal Categories','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_PRODUCT'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT1'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_PRODUCT','XXCUS_GL_PRODUCT','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_LOCATION'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT2'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_LOCATION','XXCUS_GL_LOCATION','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					apps.fnd_flex_value_sets ffvs , 
					apps.fnd_flex_values ffv, 
					apps.fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_ACCOUNT'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT4'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value','','XXCUS_GL_ACCOUNT','XXCUS_GL_ACCOUNT','MM050208',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report Data for HDS - Discount Report 
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS - Discount Report 
xxeis.eis_rsc_utility.delete_report_rows( 'HDS - Discount Report ' );
--Inserting Report - HDS - Discount Report 
xxeis.eis_rsc_ins.r( 101,'HDS - Discount Report ','','','','','','LA023190','XXEIS_GL_AR_180_DREAM_RPT_V','Y','','','LA023190','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - HDS - Discount Report 
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'ADJUSMENT_NAME','Adjusment Name','Adjusment Name','','','default','','23','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','5','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'RECEIPT_COMMENTS','Receipt Comments','Receipt Comments','','','default','','24','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'REVERSAL_REASON','Reversal Reason','Reversal Reason','','','default','','28','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'SLA_DIST_ACCOUNTED_CR','Sla Dist Accounted Cr','Sla Dist Accounted Cr','','~T~D~0','default','','17','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'SLA_DIST_ACCOUNTED_DR','Sla Dist Accounted Dr','Sla Dist Accounted Dr','','~T~D~0','default','','16','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'SOURCE','Source','Source','','','default','','26','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'TRANSACTION_NUMBER','Transaction Number','Transaction Number','','','default','','4','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'TYPE','Type','Type','','','default','','21','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'RECEIPT_DATE','Receipt Date','Receipt Date','','','default','','10','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'RECEIPT_NUMBER','Receipt Number','Receipt Number','','','default','','9','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'RECEIPT_STATE','Receipt State','Receipt State','','','default','','12','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'RECEIPT_STATUS','Receipt Status','Receipt Status','','','default','','11','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'REMIT_BANK_ACCOUNT','Remit Bank Account','Remit Bank Account','','','default','','14','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'ACC_DATE','Acc Date','Acc Date','','','default','','1','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'BATCH_NAME','Batch Name','Batch Name','','','default','','22','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'CUSTOMER_OR_VENDOR','Customer Or Vendor','Customer Or Vendor','','','default','','6','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'CUSTOMER_RECEIPT_REF','Customer Receipt Ref','Customer Receipt Ref','','','default','','13','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'ENTRY','Entry','Entry','','','default','','2','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'GL_ACCOUNT_STRING','Gl Account String','Gl Account String','','','default','','15','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'JE_CATEGORY','Je Category','Je Category','','','default','','27','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'LINE_DESCR','Line Descr','Line Descr','','','default','','3','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'PAYMENT_METHOD','Payment Method','Payment Method','','','default','','8','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'PERIOD_NAME','Period Name','Period Name','','','default','','20','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'RECEIPT_CLASS','Receipt Class','Receipt Class','','','default','','7','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'SLA.DIST.ACCOUNTED.NET','Sla.Dist.Accounted.Net','','NUMBER','~T~D~0','default','','18','Y','','','','','','','XGA1DRV.SLA_DIST_ACCOUNTED_NET*-1','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'ABS SLA ACCOUNTED NET','ABS SLA Accounted Net','','NUMBER','~T~D~0','default','','19','Y','','','','','','','ABS(XGA1DRV.SLA_DIST_ACCOUNTED_NET)','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Discount Report ',101,'GCC#50328#ACCOUNT','Gcc#50328#Account','Gcc#50328#Account','','','default','','25','N','','','','','','','','LA023190','N','N','','XXEIS_GL_AR_180_DREAM_RPT_V','','','GROUP_BY','US','');
--Inserting Report Parameters - HDS - Discount Report 
xxeis.eis_rsc_ins.rp( 'HDS - Discount Report ',101,'Account To','Gcc#50328#Account','GCC#50328#ACCOUNT','<=','','405030','VARCHAR2','Y','Y','8','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_GL_AR_180_DREAM_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS - Discount Report ',101,'Acc Date From','Acc Date','ACC_DATE','>=','','','DATE','N','Y','6','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_GL_AR_180_DREAM_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS - Discount Report ',101,'Acc Date To','Acc Date','ACC_DATE','<=','','','DATE','N','Y','7','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_GL_AR_180_DREAM_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS - Discount Report ',101,'Period Name','Period Name','PERIOD_NAME','IN','EIS_GL_PERIOD_LOV','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_GL_AR_180_DREAM_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS - Discount Report ',101,'Account From','Gcc#50328#Account','GCC#50328#ACCOUNT','>=','XXCUS_GL_ACCOUNT','405010','VARCHAR2','Y','Y','4','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_GL_AR_180_DREAM_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS - Discount Report ',101,'Location','Gcc#50328#Location','GCC#50328#LOCATION','=','XXCUS_GL_LOCATION','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_GL_AR_180_DREAM_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS - Discount Report ',101,'Product','Gcc#50328#Product','GCC#50328#PRODUCT','IN','XXCUS_GL_PRODUCT','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_GL_AR_180_DREAM_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS - Discount Report ',101,'JE Category','JE Category','JE_CATEGORY','=','EIS_GL_JE_CATEGORY_LOV','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_GL_AR_180_DREAM_RPT_V','','','US','');
--Inserting Dependent Parameters - HDS - Discount Report 
--Inserting Report Conditions - HDS - Discount Report 
xxeis.eis_rsc_ins.rcnh( 'HDS - Discount Report ',101,'ACC_DATE >= :Acc Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ACC_DATE','','Acc Date From','','','','','XXEIS_GL_AR_180_DREAM_RPT_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',101,'HDS - Discount Report ','ACC_DATE >= :Acc Date From ');
xxeis.eis_rsc_ins.rcnh( 'HDS - Discount Report ',101,'ACC_DATE <= :Acc Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ACC_DATE','','Acc Date To','','','','','XXEIS_GL_AR_180_DREAM_RPT_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',101,'HDS - Discount Report ','ACC_DATE <= :Acc Date To ');
xxeis.eis_rsc_ins.rcnh( 'HDS - Discount Report ',101,'GCC#50328#ACCOUNT >= :Account From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GCC#50328#ACCOUNT','','Account From','','','','','XXEIS_GL_AR_180_DREAM_RPT_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',101,'HDS - Discount Report ','GCC#50328#ACCOUNT >= :Account From ');
xxeis.eis_rsc_ins.rcnh( 'HDS - Discount Report ',101,'GCC#50328#ACCOUNT <= :Account To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GCC#50328#ACCOUNT','','Account To','','','','','XXEIS_GL_AR_180_DREAM_RPT_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',101,'HDS - Discount Report ','GCC#50328#ACCOUNT <= :Account To ');
xxeis.eis_rsc_ins.rcnh( 'HDS - Discount Report ',101,'GCC#50328#LOCATION = :Location ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GCC#50328#LOCATION','','Location','','','','','XXEIS_GL_AR_180_DREAM_RPT_V','','','','','','EQUALS','Y','Y','','','','','1',101,'HDS - Discount Report ','GCC#50328#LOCATION = :Location ');
xxeis.eis_rsc_ins.rcnh( 'HDS - Discount Report ',101,'GCC#50328#PRODUCT IN :Product ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GCC#50328#PRODUCT','','Product','','','','','XXEIS_GL_AR_180_DREAM_RPT_V','','','','','','IN','Y','Y','','','','','1',101,'HDS - Discount Report ','GCC#50328#PRODUCT IN :Product ');
xxeis.eis_rsc_ins.rcnh( 'HDS - Discount Report ',101,'JE_CATEGORY = :JE Category ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','JE_CATEGORY','','JE Category','','','','','XXEIS_GL_AR_180_DREAM_RPT_V','','','','','','EQUALS','Y','Y','','','','','1',101,'HDS - Discount Report ','JE_CATEGORY = :JE Category ');
xxeis.eis_rsc_ins.rcnh( 'HDS - Discount Report ',101,'PERIOD_NAME IN :Period Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PERIOD_NAME','','Period Name','','','','','XXEIS_GL_AR_180_DREAM_RPT_V','','','','','','IN','Y','Y','','','','','1',101,'HDS - Discount Report ','PERIOD_NAME IN :Period Name ');
--Inserting Report Sorts - HDS - Discount Report 
--Inserting Report Triggers - HDS - Discount Report 
--inserting report templates - HDS - Discount Report 
--Inserting Report Portals - HDS - Discount Report 
--inserting report dashboards - HDS - Discount Report 
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS - Discount Report ','101','XXEIS_GL_AR_180_DREAM_RPT_V','XXEIS_GL_AR_180_DREAM_RPT_V','N','');
--inserting report security - HDS - Discount Report 
xxeis.eis_rsc_ins.rsec( 'HDS - Discount Report ','','LH082614','',101,'LA023190','','','');
xxeis.eis_rsc_ins.rsec( 'HDS - Discount Report ','101','','XXCUS_GL_INQUIRY',101,'LA023190','','','');
--Inserting Report Pivots - HDS - Discount Report 
--Inserting Report   Version details- HDS - Discount Report 
xxeis.eis_rsc_ins.rv( 'HDS - Discount Report ','','HDS - Discount Report ','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
