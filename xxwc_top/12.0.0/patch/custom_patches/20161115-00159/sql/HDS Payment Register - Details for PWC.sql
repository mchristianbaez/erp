--Report Name            : HDS Payment Register - Details for PWC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_AP_PAYMENT_REGISTER_HDS
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_AP_PAYMENT_REGISTER_HDS
xxeis.eis_rsc_ins.v( 'EIS_AP_PAYMENT_REGISTER_HDS',200,'','','','','XXEIS_RS_ADMIN','XXEIS','EIS_AP_PAYMENT_REGISTER_HDS','EAPRH','','','VIEW','US','','');
--Delete Object Columns for EIS_AP_PAYMENT_REGISTER_HDS
xxeis.eis_rsc_utility.delete_view_rows('EIS_AP_PAYMENT_REGISTER_HDS',200,FALSE);
--Inserting Object Columns for EIS_AP_PAYMENT_REGISTER_HDS
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','CHECK_VOUCHER_NUM',200,'Check Voucher Num','CHECK_VOUCHER_NUM','','','','XXEIS_RS_ADMIN','NUMBER','','','Check Voucher Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','STATUS_LOOKUP_CODE',200,'Status Lookup Code','STATUS_LOOKUP_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Status Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','ZIP',200,'Zip','ZIP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Zip','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','COUNTRY',200,'Country','COUNTRY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Country','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','STATE',200,'State','STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','State','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','COUNTY',200,'County','COUNTY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','County','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','CITY',200,'City','CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','City','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','ADDRESS_LINE4',200,'Address Line4','ADDRESS_LINE4','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line4','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','ADDRESS_LINE3',200,'Address Line3','ADDRESS_LINE3','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line3','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','ADDRESS_LINE2',200,'Address Line2','ADDRESS_LINE2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','ADDRESS_LINE1',200,'Address Line1','ADDRESS_LINE1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','CHECKRUN_NAME',200,'Checkrun Name','CHECKRUN_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Checkrun Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','PAYMENT_METHOD_LOOKUP_CODE',200,'Payment Method Lookup Code','PAYMENT_METHOD_LOOKUP_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Method Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','BANK_ACCOUNT_NAME',200,'Bank Account Name','BANK_ACCOUNT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bank Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','REVERSAL_INV_PMT_ID',200,'Reversal Inv Pmt Id','REVERSAL_INV_PMT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Reversal Inv Pmt Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','REVERSAL_FLAG',200,'Reversal Flag','REVERSAL_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reversal Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','INVOICE_PAYMENT_TYPE',200,'Invoice Payment Type','INVOICE_PAYMENT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Payment Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','ASSETS_ADDITION_FLAG',200,'Assets Addition Flag','ASSETS_ADDITION_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Assets Addition Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','PAYMENT_BASE_AMOUNT',200,'Payment Base Amount','PAYMENT_BASE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Payment Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','INVOICE_BASE_AMOUNT',200,'Invoice Base Amount','INVOICE_BASE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','AMOUNT_PAID',200,'Amount Paid','AMOUNT_PAID','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amount Paid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','DISCOUNT_TAKEN',200,'Discount Taken','DISCOUNT_TAKEN','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Discount Taken','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','DISCOUNT_LOST',200,'Discount Lost','DISCOUNT_LOST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Discount Lost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','BANK_NUM',200,'Bank Num','BANK_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bank Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','BANK_ACCOUNT_NUM',200,'Bank Account Num','BANK_ACCOUNT_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bank Account Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','POSTED_FLAG',200,'Posted Flag','POSTED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Posted Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','PERIOD_NAME',200,'Period Name','PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','PAYMENT_NUM',200,'Payment Num','PAYMENT_NUM','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','ACCOUNTING_DATE',200,'Accounting Date','ACCOUNTING_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Accounting Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','GL_ACCOUNT',200,'Gl Account','GL_ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','GL_CCID',200,'Gl Ccid','GL_CCID','','','','XXEIS_RS_ADMIN','NUMBER','','','Gl Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','INVOICE_DESCRIPTION',200,'Invoice Description','INVOICE_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','DISTRIBUTION_AMOUNT',200,'Distribution Amount','DISTRIBUTION_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Distribution Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','BASE_AMOUNT',200,'Base Amount','BASE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','DISTRIBUTION_LINE_NUMBER',200,'Distribution Line Number','DISTRIBUTION_LINE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Distribution Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','PAYMENT_CURRENCY_CODE',200,'Payment Currency Code','PAYMENT_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','INVOICE_CURRENCY_CODE',200,'Invoice Currency Code','INVOICE_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','INVOICE_NUMBER',200,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','SEGMENT1',200,'Segment1','SEGMENT1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','VENDOR_SITE_CODE',200,'Vendor Site Code','VENDOR_SITE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Site Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','VENDOR_NAME',200,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','CHECK_AMOUNT',200,'Check Amount','CHECK_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Check Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','PAYMENT_DATE',200,'Payment Date','PAYMENT_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Payment Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','OPERATING_UNIT',200,'Operating Unit','OPERATING_UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','LEDGER_NAME',200,'Ledger Name','LEDGER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ledger Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','PO_NUMBER',200,'Po Number','PO_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','CHECK_NUMBER',200,'Check Number','CHECK_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Check Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','VENDOR_SITE_ID',200,'Vendor Site Id','VENDOR_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Vendor Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','VENDOR_ID',200,'Vendor Id','VENDOR_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','ORGANIZATION_ID',200,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','PO_HEADER_ID',200,'Po Header Id','PO_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Po Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','PO_LINE_ID',200,'Po Line Id','PO_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Po Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','LINE_LOCATION_ID',200,'Line Location Id','LINE_LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','PO_DISTRIBUTION_ID',200,'Po Distribution Id','PO_DISTRIBUTION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Po Distribution Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','INVOICE_LINE_NUMBER',200,'Invoice Line Number','INVOICE_LINE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','INVOICE_ID',200,'Invoice Id','INVOICE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','CHECK_ID',200,'Check Id','CHECK_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Check Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','INVOICE_PAYMENT_ID',200,'Invoice Payment Id','INVOICE_PAYMENT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Payment Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','CHECK_CREATED_BY',200,'Check Created By','CHECK_CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Check Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','APPROVAL_DESCRIPTION',200,'Approval Description','APPROVAL_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Approval Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','APPROVAL_STATUS',200,'Approval Status','APPROVAL_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Approval Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','APPROVED_FLAG',200,'Approved Flag','APPROVED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Approved Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','PO_STATUS',200,'Po Status','PO_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Po Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','PO_DATE',200,'Po Date','PO_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Po Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','INVOICE_RECEIVED_DATE',200,'Invoice Received Date','INVOICE_RECEIVED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Received Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','GOODS_RECEIVED_DATE',200,'Goods Received Date','GOODS_RECEIVED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Goods Received Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','FREIGHT_AMOUNT',200,'Freight Amount','FREIGHT_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Freight Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','PAYMENT_STATUS_FLAG',200,'Payment Status Flag','PAYMENT_STATUS_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Status Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','PAY_GROUP_LOOKUP_CODE',200,'Pay Group Lookup Code','PAY_GROUP_LOOKUP_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pay Group Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','TAX_AMOUNT',200,'Tax Amount','TAX_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Tax Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','AMOUNT_APPLICABLE_TO_DISCOUNT',200,'Amount Applicable To Discount','AMOUNT_APPLICABLE_TO_DISCOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amount Applicable To Discount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','INVOICE_TYPE_LOOKUP_CODE',200,'Invoice Type Lookup Code','INVOICE_TYPE_LOOKUP_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Type Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','SOURCE',200,'Source','SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Source','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','INVOICE_DATE',200,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','PAYMENT_TYPE',200,'Payment Type','PAYMENT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','RELEASED_DATE',200,'Released Date','RELEASED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Released Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','STOPPED_DATE',200,'Stopped Date','STOPPED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Stopped Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','POSITIVE_PAY_STATUS_CODE',200,'Positive Pay Status Code','POSITIVE_PAY_STATUS_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Positive Pay Status Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','FUTURE_PAY_DUE_DATE',200,'Future Pay Due Date','FUTURE_PAY_DUE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Future Pay Due Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','VOID_DATE',200,'Void Date','VOID_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Void Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','CLEARED_DATE',200,'Cleared Date','CLEARED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Cleared Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','CLEARED_AMOUNT',200,'Cleared Amount','CLEARED_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Cleared Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','API#163#Invoice_Type',200,'Descriptive flexfield (DFF): Invoice Column Name: Invoice Type Context: 163','API#163#Invoice_Type','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE11','Api#163#Invoice Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','API#163#DCTM_Image_Link',200,'Descriptive flexfield (DFF): Invoice Column Name: DCTM Image Link Context: 163','API#163#DCTM_Image_Link','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE14','Api#163#Dctm Image Link','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','API#163#R11_Invoice_ID',200,'Descriptive flexfield (DFF): Invoice Column Name: R11 Invoice ID Context: 163','API#163#R11_Invoice_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE15','Api#163#R11 Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','API#166#Case_Identifier',200,'Descriptive flexfield (DFF): Invoice Column Name: Case Identifier Context: 166','API#166#Case_Identifier','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE1','Api#166#Case Identifier','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','API#166#Deduction_Code',200,'Descriptive flexfield (DFF): Invoice Column Name: Deduction Code Context: 166','API#166#Deduction_Code','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE2','Api#166#Deduction Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','API#166#Employee',200,'Descriptive flexfield (DFF): Invoice Column Name: Employee Context: 166','API#166#Employee','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE3','Api#166#Employee','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','API#163#BizTalk_ID_Frt_Bizta',200,'Descriptive flexfield (DFF): Invoice Column Name: BizTalk ID-Frt Biztalk Context: 163','API#163#BizTalk_ID_Frt_Bizta','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE1','Api#163#Biztalk Id-Frt Biztalk','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','API#163#Branch_Frt_Biztalk',200,'Descriptive flexfield (DFF): Invoice Column Name: Branch-Frt Biztalk Context: 163','API#163#Branch_Frt_Biztalk','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE2','Api#163#Branch-Frt Biztalk','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','API#163#POS_System_Code_Frt_',200,'Descriptive flexfield (DFF): Invoice Column Name: POS System Code-Frt Biztalk Context: 163','API#163#POS_System_Code_Frt_','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE8','Api#163#Pos System Code-Frt Biztalk','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','API#166#R11_Invoice_ID',200,'Descriptive flexfield (DFF): Invoice Column Name: R11 Invoice ID Context: 166','API#166#R11_Invoice_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE15','Api#166#R11 Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','API#166#Description',200,'Descriptive flexfield (DFF): Invoice Column Name: Description Context: 166','API#166#Description','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE4','Api#166#Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','APID#163#PO_Number_Frt_Bizta',200,'Descriptive flexfield (DFF): Invoice Distribution Column Name: PO Number-Frt Biztalk Context: 163','APID#163#PO_Number_Frt_Bizta','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICE_DISTRIBUTIONS_ALL','ATTRIBUTE1','Apid#163#Po Number-Frt Biztalk','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','APID#163#Branch_Frt_Biztalk',200,'Descriptive flexfield (DFF): Invoice Distribution Column Name: Branch-Frt Biztalk Context: 163','APID#163#Branch_Frt_Biztalk','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICE_DISTRIBUTIONS_ALL','ATTRIBUTE2','Apid#163#Branch-Frt Biztalk','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','APID#163#Bill_of_Lading_Frt_',200,'Descriptive flexfield (DFF): Invoice Distribution Column Name: Bill of Lading-Frt Biztalk Context: 163','APID#163#Bill_of_Lading_Frt_','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICE_DISTRIBUTIONS_ALL','ATTRIBUTE3','Apid#163#Bill Of Lading-Frt Biztalk','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','API#162#Documentum_PO_Number',200,'Descriptive flexfield (DFF): Invoice Column Name: Documentum PO Number Context: 162','API#162#Documentum_PO_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE2','Api#162#Documentum Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','POH#StandardP#Need_By_Date',200,'Descriptive flexfield (DFF): PO Headers Column Name: Need-By Date Context: Standard Purchase Order','POH#StandardP#Need_By_Date','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE1','Poh#Standard Purchase Order#Need-By Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','POH#STANDARDP#FREIGHT_TERMS_',200,'Descriptive flexfield: PO Headers Column Name: Freight Terms (TERMS Tab) Context: Standard Purchase Order','POH#StandardP#Freight_Terms_','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE2','Poh#Standard Purchase Order#Freight Terms Terms Tab','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','POH#STANDARDP#CARRIER_TERMS_',200,'Descriptive flexfield: PO Headers Column Name: Carrier (TERMS Tab) Context: Standard Purchase Order','POH#StandardP#Carrier_TERMS_','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE3','Poh#Standard Purchase Order#Carrier Terms Tab','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','POH#StandardP#FOB_TERMS_Tab',200,'Descriptive flexfield (DFF): PO Headers Column Name: FOB (TERMS Tab) Context: Standard Purchase Order','POH#StandardP#FOB_TERMS_Tab','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE4','Poh#Standard Purchase Order#Fob Terms Tab','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','API#163#SO_Number',200,'Descriptive flexfield (DFF): Invoice Column Name: SO Number Context: 163','API#163#SO_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE3','Api#163#So Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','API#163#PO_Number',200,'Descriptive flexfield (DFF): Invoice Column Name: PO Number Context: 163','API#163#PO_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE4','Api#163#Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','API#163#BOL',200,'Descriptive flexfield (DFF): Invoice Column Name: BOL Context: 163','API#163#BOL','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE6','Api#163#Bol','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','API#167#SO_Number',200,'Descriptive flexfield (DFF): Invoice Column Name: SO Number Context: 167','API#167#SO_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE3','Api#167#So Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','API#167#PO_Number',200,'Descriptive flexfield (DFF): Invoice Column Name: PO Number Context: 167','API#167#PO_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE4','Api#167#Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','API#167#Canada_Tax_Type',200,'Descriptive flexfield (DFF): Invoice Column Name: Canada Tax Type Context: 167','API#167#Canada_Tax_Type','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE5','Api#167#Canada Tax Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','API#167#BOL',200,'Descriptive flexfield (DFF): Invoice Column Name: BOL Context: 167','API#167#BOL','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE6','Api#167#Bol','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','POH#StandardP#Note_To_AP',200,'Descriptive flexfield (DFF): PO Headers Column Name: Note To AP Context: Standard Purchase Order','POH#StandardP#Note_To_AP','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE10','Poh#Standard Purchase Order#Note To Ap','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','POH#StandardP#Discount_Valid',200,'Descriptive flexfield (DFF): PO Headers Column Name: Discount Validated ? Context: Standard Purchase Order','POH#StandardP#Discount_Valid','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE11','Poh#Standard Purchase Order#Discount Validated ?','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','POH#StandardP#Requested_Deli',200,'Descriptive flexfield (DFF): PO Headers Column Name: Requested Delivery Date Context: Standard Purchase Order','POH#StandardP#Requested_Deli','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE12','Poh#Standard Purchase Order#Requested Delivery Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','POH#StandardP#Drop_Ship_EDI_',200,'Descriptive flexfield (DFF): PO Headers Column Name: Drop Ship EDI Inidicator Context: Standard Purchase Order','POH#StandardP#Drop_Ship_EDI_','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE2','Poh#Standard Purchase Order#Drop Ship Edi Inidicator','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','POH#StandardP#Drop_Ship_Cont',200,'Descriptive flexfield (DFF): PO Headers Column Name: Drop Ship Contact Phone Number Context: Standard Purchase Order','POH#StandardP#Drop_Ship_Cont','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE3','Poh#Standard Purchase Order#Drop Ship Contact Phone Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','POH#StandardP#Freight_Accoun',200,'Descriptive flexfield (DFF): PO Headers Column Name: Freight Account Number Context: Standard Purchase Order','POH#StandardP#Freight_Accoun','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE5','Poh#Standard Purchase Order#Freight Account Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','POH#StandardP#Promised_Date',200,'Descriptive flexfield (DFF): PO Headers Column Name: Promised Date Context: Standard Purchase Order','POH#StandardP#Promised_Date','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE6','Poh#Standard Purchase Order#Promised Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','POH#StandardP#Header_Discoun',200,'Descriptive flexfield (DFF): PO Headers Column Name: Header Discount Type Context: Standard Purchase Order','POH#StandardP#Header_Discoun','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE7','Poh#Standard Purchase Order#Header Discount Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','POH#StandardP#Header_Discoun1',200,'Descriptive flexfield (DFF): PO Headers Column Name: Header Discount Value Context: Standard Purchase Order','POH#StandardP#Header_Discoun1','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE8','Poh#Standard Purchase Order#Header Discount Value','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','POH#StandardP#Line_Discount_',200,'Descriptive flexfield (DFF): PO Headers Column Name: Line Discount Percent Context: Standard Purchase Order','POH#StandardP#Line_Discount_','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE9','Poh#Standard Purchase Order#Line Discount Percent','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','POL#Apply__Remove_Discount_',200,'Descriptive flexfield (DFF): PO Lines Column Name: Apply / Remove Discount ?','POL#Apply__Remove_Discount_','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINES_ALL','ATTRIBUTE1','Pol#Apply / Remove Discount ?','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','POL#Discount_Applied_Flag',200,'Descriptive flexfield (DFF): PO Lines Column Name: Discount Applied Flag','POL#Discount_Applied_Flag','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINES_ALL','ATTRIBUTE2','Pol#Discount Applied Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REGISTER_HDS','POL#Original_Line_Cost',200,'Descriptive flexfield (DFF): PO Lines Column Name: Original Line Cost','POL#Original_Line_Cost','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINES_ALL','ATTRIBUTE3','Pol#Original Line Cost','','','','US');
--Inserting Object Components for EIS_AP_PAYMENT_REGISTER_HDS
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REGISTER_HDS','AP_INVOICE_PAYMENTS',200,'AP_INVOICE_PAYMENTS_ALL','AIP','AIP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Invoice Payment Records','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REGISTER_HDS','AP_CHECKS',200,'AP_CHECKS_ALL','CH','CH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Supplier Payment Data','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REGISTER_HDS','AP_INVOICES',200,'AP_INVOICES_ALL','API','API','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Detailed Invoice Records','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REGISTER_HDS','AP_INVOICE_DISTRIBUTIONS',200,'AP_INVOICE_DISTRIBUTIONS_ALL','APID','APID','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Invoice Distribution Line Information','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REGISTER_HDS','PO_DISTRIBUTIONS',200,'PO_DISTRIBUTIONS_ALL','POD','POD','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Purchase Order Distributions','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REGISTER_HDS','PO_LINE_LOCATIONS',200,'PO_LINE_LOCATIONS_ALL','POLL','POLL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Document Shipment Schedules (For Purchase Orders, ','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REGISTER_HDS','PO_LINES',200,'PO_LINES_ALL','POL','POL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Purchase Document Lines (For Purchase Orders, Purc','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REGISTER_HDS','PO_HEADERS',200,'PO_HEADERS_ALL','POH','POH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Document Headers (For Purchase Orders, Purchase Ag','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REGISTER_HDS','AP_SUPPLIERS',200,'AP_SUPPLIERS','PV','PV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ap Suppliers Stores Information About Your Supplie','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REGISTER_HDS','AP_SUPPLIER_SITES_ALL',200,'AP_SUPPLIER_SITES_ALL','PVS','PVS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ap Supplier Sites All Stores Information About You','','','','','ASSA','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REGISTER_HDS','HR_ORGANIZATION_UNITS',200,'HR_ALL_ORGANIZATION_UNITS','HOU','HOU','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','HR_ALL_ORGANIZATION_UNITS','N','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_AP_PAYMENT_REGISTER_HDS
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REGISTER_HDS','AP_INVOICE_PAYMENTS','AIP',200,'EAPRH.INVOICE_PAYMENT_ID','=','AIP.INVOICE_PAYMENT_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REGISTER_HDS','AP_CHECKS','CH',200,'EAPRH.CHECK_ID','=','CH.CHECK_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REGISTER_HDS','AP_INVOICES','API',200,'EAPRH.INVOICE_ID','=','API.INVOICE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REGISTER_HDS','AP_INVOICE_DISTRIBUTIONS','APID',200,'EAPRH.INVOICE_ID','=','APID.INVOICE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REGISTER_HDS','AP_INVOICE_DISTRIBUTIONS','APID',200,'EAPRH.INVOICE_LINE_NUMBER','=','APID.INVOICE_LINE_NUMBER(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REGISTER_HDS','AP_INVOICE_DISTRIBUTIONS','APID',200,'EAPRH.DISTRIBUTION_LINE_NUMBER','=','APID.DISTRIBUTION_LINE_NUMBER(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REGISTER_HDS','PO_DISTRIBUTIONS','POD',200,'EAPRH.PO_DISTRIBUTION_ID','=','POD.PO_DISTRIBUTION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REGISTER_HDS','PO_LINE_LOCATIONS','POLL',200,'EAPRH.LINE_LOCATION_ID','=','POLL.LINE_LOCATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REGISTER_HDS','PO_LINES','POL',200,'EAPRH.PO_LINE_ID','=','POL.PO_LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REGISTER_HDS','PO_HEADERS','POH',200,'EAPRH.PO_HEADER_ID','=','POH.PO_HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REGISTER_HDS','AP_SUPPLIERS','PV',200,'EAPRH.VENDOR_ID','=','PV.VENDOR_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REGISTER_HDS','AP_SUPPLIER_SITES_ALL','PVS',200,'EAPRH.VENDOR_SITE_ID','=','PVS.VENDOR_SITE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REGISTER_HDS','HR_ORGANIZATION_UNITS','HOU',200,'EAPRH.ORGANIZATION_ID','=','HOU.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--Exporting View Component Data of the View -  EIS_AP_PAYMENT_REGISTER_HDS
prompt Creating Object Data AP_SUPPLIERS
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object AP_SUPPLIERS
xxeis.eis_rsc_ins.v( 'AP_SUPPLIERS',200,'AP_SUPPLIERS stores information about your supplier level attributes.','1.0','','','ANONYMOUS','APPS','Ap Suppliers','AS','','','SYNONYM','US','','');
--Delete Object Columns for AP_SUPPLIERS
xxeis.eis_rsc_utility.delete_view_rows('AP_SUPPLIERS',200,FALSE);
--Inserting Object Columns for AP_SUPPLIERS
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','INDIVIDUAL_1099',200,'','INDIVIDUAL_1099','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','INDIVIDUAL_1099','Individual 1099','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PAYMENT_METHOD_LOOKUP_CODE',200,'Default payment method type','PAYMENT_METHOD_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','PAYMENT_METHOD_LOOKUP_CODE','Payment Method Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ORGANIZATION_TYPE_LOOKUP_CODE',200,'IRS organization type','ORGANIZATION_TYPE_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ORGANIZATION_TYPE_LOOKUP_CODE','Organization Type Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SEGMENT1',200,'Supplier number','SEGMENT1','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SEGMENT1','Segment1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','VENDOR_NAME',200,'Supplier name','VENDOR_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','VENDOR_NAME','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE3',200,'Descriptive flexfield segment','ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE3','Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE2',200,'Descriptive flexfield segment','ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE2','Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE1',200,'Descriptive flexfield segment','ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE1','Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CIS_ENABLED_FLAG',200,'','CIS_ENABLED_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','CIS_ENABLED_FLAG','Cis Enabled Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PARTNERSHIP_NAME',200,'','PARTNERSHIP_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','PARTNERSHIP_NAME','Partnership Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PARTNERSHIP_UTR',200,'','PARTNERSHIP_UTR','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PARTNERSHIP_UTR','Partnership Utr','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','UNIQUE_TAX_REFERENCE_NUM',200,'','UNIQUE_TAX_REFERENCE_NUM','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','UNIQUE_TAX_REFERENCE_NUM','Unique Tax Reference Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BUS_CLASS_LAST_CERTIFIED_BY',200,'Last Certified By','BUS_CLASS_LAST_CERTIFIED_BY','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','BUS_CLASS_LAST_CERTIFIED_BY','Bus Class Last Certified By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CIS_VERIFICATION_DATE',200,'','CIS_VERIFICATION_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','CIS_VERIFICATION_DATE','Cis Verification Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','MATCH_STATUS_FLAG',200,'','MATCH_STATUS_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','MATCH_STATUS_FLAG','Match Status Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','VERIFICATION_REQUEST_ID',200,'','VERIFICATION_REQUEST_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','VERIFICATION_REQUEST_ID','Verification Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','VERIFICATION_NUMBER',200,'','VERIFICATION_NUMBER','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','VERIFICATION_NUMBER','Verification Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','NATIONAL_INSURANCE_NUMBER',200,'','NATIONAL_INSURANCE_NUMBER','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','NATIONAL_INSURANCE_NUMBER','National Insurance Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BUS_CLASS_LAST_CERTIFIED_DATE',200,'Last Certified Date','BUS_CLASS_LAST_CERTIFIED_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','BUS_CLASS_LAST_CERTIFIED_DATE','Bus Class Last Certified Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','COMPANY_REGISTRATION_NUMBER',200,'','COMPANY_REGISTRATION_NUMBER','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','COMPANY_REGISTRATION_NUMBER','Company Registration Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','WORK_REFERENCE',200,'','WORK_REFERENCE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','WORK_REFERENCE','Work Reference','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','TRADING_NAME',200,'','TRADING_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','TRADING_NAME','Trading Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SALUTATION',200,'','SALUTATION','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SALUTATION','Salutation','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','LAST_NAME',200,'','LAST_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','LAST_NAME','Last Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SECOND_NAME',200,'','SECOND_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SECOND_NAME','Second Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CIS_PARENT_VENDOR_ID',200,'CIS Parent Vendor ID','CIS_PARENT_VENDOR_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','CIS_PARENT_VENDOR_ID','Cis Parent Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PAY_AWT_GROUP_ID',200,'','PAY_AWT_GROUP_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PAY_AWT_GROUP_ID','Pay Awt Group Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','FIRST_NAME',200,'','FIRST_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','FIRST_NAME','First Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BANK_ACCOUNT_NUM',200,'No longer used','BANK_ACCOUNT_NUM','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','BANK_ACCOUNT_NUM','Bank Account Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BANK_ACCOUNT_NAME',200,'No longer used','BANK_ACCOUNT_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','BANK_ACCOUNT_NAME','Bank Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','MINORITY_GROUP_LOOKUP_CODE',200,'Type of minority-owned business','MINORITY_GROUP_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','MINORITY_GROUP_LOOKUP_CODE','Minority Group Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','END_DATE_ACTIVE',200,'Key flexfield end date','END_DATE_ACTIVE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','END_DATE_ACTIVE','End Date Active','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','START_DATE_ACTIVE',200,'Key flexfield start date','START_DATE_ACTIVE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','START_DATE_ACTIVE','Start Date Active','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','VAT_CODE',200,'VAT code','VAT_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','VAT_CODE','Vat Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','WITHHOLDING_START_DATE',200,'Withholding start date','WITHHOLDING_START_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','WITHHOLDING_START_DATE','Withholding Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','WITHHOLDING_STATUS_LOOKUP_CODE',200,'Withholding status type','WITHHOLDING_STATUS_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','WITHHOLDING_STATUS_LOOKUP_CODE','Withholding Status Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','TYPE_1099',200,'Type of 1099','TYPE_1099','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','TYPE_1099','Type 1099','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','NUM_1099',200,'Tax identification number','NUM_1099','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','NUM_1099','Num 1099','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PREPAY_CODE_COMBINATION_ID',200,'Unique identifier for the general ledger account for prepayment','PREPAY_CODE_COMBINATION_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PREPAY_CODE_COMBINATION_ID','Prepay Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EXPENSE_CODE_COMBINATION_ID',200,'Not used','EXPENSE_CODE_COMBINATION_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','EXPENSE_CODE_COMBINATION_ID','Expense Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','DISC_TAKEN_CODE_COMBINATION_ID',200,'No longer used','DISC_TAKEN_CODE_COMBINATION_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','DISC_TAKEN_CODE_COMBINATION_ID','Disc Taken Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','DISC_LOST_CODE_COMBINATION_ID',200,'No longer used','DISC_LOST_CODE_COMBINATION_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','DISC_LOST_CODE_COMBINATION_ID','Disc Lost Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ACCTS_PAY_CODE_COMBINATION_ID',200,'Unique identifier for the supplier liability account','ACCTS_PAY_CODE_COMBINATION_ID','','~T~D~2','','ANONYMOUS','NUMBER','AP_SUPPLIERS','ACCTS_PAY_CODE_COMBINATION_ID','Accts Pay Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','DISTRIBUTION_SET_ID',200,'Distribution set unique identifier','DISTRIBUTION_SET_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','DISTRIBUTION_SET_ID','Distribution Set Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','HOLD_REASON',200,'Reason for placing the supplier on payment hold','HOLD_REASON','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','HOLD_REASON','Hold Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','HOLD_FUTURE_PAYMENTS_FLAG',200,'Indicates whether Oracle Payables should place upapproved payments for this supplier on hold or not','HOLD_FUTURE_PAYMENTS_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','HOLD_FUTURE_PAYMENTS_FLAG','Hold Future Payments Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','HOLD_ALL_PAYMENTS_FLAG',200,'Indicates whether Oracle Payables should place all payments for this supplier on hold or not','HOLD_ALL_PAYMENTS_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','HOLD_ALL_PAYMENTS_FLAG','Hold All Payments Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EXCHANGE_DATE_LOOKUP_CODE',200,'No longer used','EXCHANGE_DATE_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','EXCHANGE_DATE_LOOKUP_CODE','Exchange Date Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','INVOICE_AMOUNT_LIMIT',200,'Maximum amount per invoice','INVOICE_AMOUNT_LIMIT','','~T~D~2','','ANONYMOUS','NUMBER','AP_SUPPLIERS','INVOICE_AMOUNT_LIMIT','Invoice Amount Limit','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PAYMENT_CURRENCY_CODE',200,'Default payment currency unique identifier','PAYMENT_CURRENCY_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','PAYMENT_CURRENCY_CODE','Payment Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','INVOICE_CURRENCY_CODE',200,'Default currency unique identifier','INVOICE_CURRENCY_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','INVOICE_CURRENCY_CODE','Invoice Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PAYMENT_PRIORITY',200,'Payment priority','PAYMENT_PRIORITY','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PAYMENT_PRIORITY','Payment Priority','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PAY_GROUP_LOOKUP_CODE',200,'Payment group type','PAY_GROUP_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','PAY_GROUP_LOOKUP_CODE','Pay Group Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PAY_DATE_BASIS_LOOKUP_CODE',200,'Type of payment date basis','PAY_DATE_BASIS_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','PAY_DATE_BASIS_LOOKUP_CODE','Pay Date Basis Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ALWAYS_TAKE_DISC_FLAG',200,'Indicator of whether Oracle Payables should always take a discount for the supplier','ALWAYS_TAKE_DISC_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ALWAYS_TAKE_DISC_FLAG','Always Take Disc Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CREDIT_LIMIT',200,'Not used','CREDIT_LIMIT','','~T~D~2','','ANONYMOUS','NUMBER','AP_SUPPLIERS','CREDIT_LIMIT','Credit Limit','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CREDIT_STATUS_LOOKUP_CODE',200,'No longer used','CREDIT_STATUS_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','CREDIT_STATUS_LOOKUP_CODE','Credit Status Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SET_OF_BOOKS_ID',200,'Set of Books unique identifier','SET_OF_BOOKS_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','SET_OF_BOOKS_ID','Set Of Books Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','TERMS_ID',200,'Payment terms unique identifier','TERMS_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','TERMS_ID','Terms Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','FOB_LOOKUP_CODE',200,'Default free-on-board type','FOB_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','FOB_LOOKUP_CODE','Fob Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','FREIGHT_TERMS_LOOKUP_CODE',200,'Default freight terms type','FREIGHT_TERMS_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','FREIGHT_TERMS_LOOKUP_CODE','Freight Terms Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SHIP_VIA_LOOKUP_CODE',200,'Default carrier type','SHIP_VIA_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SHIP_VIA_LOOKUP_CODE','Ship Via Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BILL_TO_LOCATION_ID',200,'Default bill-to location unique identifier','BILL_TO_LOCATION_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','BILL_TO_LOCATION_ID','Bill To Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SHIP_TO_LOCATION_ID',200,'Default ship-to location unique identifier','SHIP_TO_LOCATION_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','SHIP_TO_LOCATION_ID','Ship To Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','MIN_ORDER_AMOUNT',200,'Minimum purchase order amount','MIN_ORDER_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_SUPPLIERS','MIN_ORDER_AMOUNT','Min Order Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PARENT_VENDOR_ID',200,'Unique identifier of the parent supplier','PARENT_VENDOR_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PARENT_VENDOR_ID','Parent Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ONE_TIME_FLAG',200,'Indicates whether the supplier is a one-time supplier','ONE_TIME_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ONE_TIME_FLAG','One Time Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CUSTOMER_NUM',200,'Customer number with the supplier','CUSTOMER_NUM','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','CUSTOMER_NUM','Customer Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','VENDOR_TYPE_LOOKUP_CODE',200,'Supplier type','VENDOR_TYPE_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','VENDOR_TYPE_LOOKUP_CODE','Vendor Type Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EMPLOYEE_ID',200,'Employee unique identifier if the supplier is an employee','EMPLOYEE_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','EMPLOYEE_ID','Employee Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CREATED_BY',200,'Standard Who column','CREATED_BY','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','CREATED_BY','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CREATION_DATE',200,'Standard Who column','CREATION_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','CREATION_DATE','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','LAST_UPDATE_LOGIN',200,'Standard Who column','LAST_UPDATE_LOGIN','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','LAST_UPDATE_LOGIN','Last Update Login','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SEGMENT5',200,'Key flexfield summary flag','SEGMENT5','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SEGMENT5','Segment5','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SEGMENT4',200,'Key flexfield summary flag','SEGMENT4','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SEGMENT4','Segment4','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SEGMENT3',200,'Key flexfield summary flag','SEGMENT3','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SEGMENT3','Segment3','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SEGMENT2',200,'Key flexfield summary flag','SEGMENT2','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SEGMENT2','Segment2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ENABLED_FLAG',200,'Key flexfield summary flag','ENABLED_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ENABLED_FLAG','Enabled Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SUMMARY_FLAG',200,'Key flexfield summary flag','SUMMARY_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SUMMARY_FLAG','Summary Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','VENDOR_NAME_ALT',200,'Alternate supplier name for kana value','VENDOR_NAME_ALT','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','VENDOR_NAME_ALT','Vendor Name Alt','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','LAST_UPDATED_BY',200,'Standard Who column','LAST_UPDATED_BY','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','LAST_UPDATED_BY','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','LAST_UPDATE_DATE',200,'Standard Who column','LAST_UPDATE_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','LAST_UPDATE_DATE','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','VENDOR_ID',200,'Supplier unique identifier','VENDOR_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','VENDOR_ID','Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE14',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE14','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE14','Global Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE13',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE13','Global Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE12',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE12','Global Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE11',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE11','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE11','Global Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE10',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE10','Global Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE9',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE9','Global Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE8',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE8','Global Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE7',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE7','Global Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE6',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE6','Global Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE5',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE5','Global Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE4',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE4','Global Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE3',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE3','Global Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE2',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE2','Global Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE1',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE1','Global Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','AWT_GROUP_ID',200,'Unique identifier for the withholding tax group','AWT_GROUP_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','AWT_GROUP_ID','Awt Group Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ALLOW_AWT_FLAG',200,'Indicator of whether Allow Withholding Tax is enabled','ALLOW_AWT_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ALLOW_AWT_FLAG','Allow Awt Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BANK_NUMBER',200,'No longer used','BANK_NUMBER','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','BANK_NUMBER','Bank Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CHECK_DIGITS',200,'Check number used by Payables','CHECK_DIGITS','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','CHECK_DIGITS','Check Digits','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','TAX_REPORTING_NAME',200,'Tax reporting method name','TAX_REPORTING_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','TAX_REPORTING_NAME','Tax Reporting Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EXCLUDE_FREIGHT_FROM_DISCOUNT',200,'Exclude supplier freight from discount amount','EXCLUDE_FREIGHT_FROM_DISCOUNT','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','EXCLUDE_FREIGHT_FROM_DISCOUNT','Exclude Freight From Discount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','VALIDATION_NUMBER',200,'Validation number','VALIDATION_NUMBER','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','VALIDATION_NUMBER','Validation Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','AUTO_CALCULATE_INTEREST_FLAG',200,'Indicates whether interest is to be automatically calculated','AUTO_CALCULATE_INTEREST_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','AUTO_CALCULATE_INTEREST_FLAG','Auto Calculate Interest Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','VAT_REGISTRATION_NUM',200,'VAT registration number','VAT_REGISTRATION_NUM','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','VAT_REGISTRATION_NUM','Vat Registration Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','OFFSET_VAT_CODE',200,'No longer used','OFFSET_VAT_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','OFFSET_VAT_CODE','Offset Vat Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PROGRAM_UPDATE_DATE',200,'Standard Who column','PROGRAM_UPDATE_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','PROGRAM_UPDATE_DATE','Program Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PROGRAM_ID',200,'Standard Who column','PROGRAM_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PROGRAM_ID','Program Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PROGRAM_APPLICATION_ID',200,'Standard Who column','PROGRAM_APPLICATION_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PROGRAM_APPLICATION_ID','Program Application Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','REQUEST_ID',200,'Standard Who column','REQUEST_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','REQUEST_ID','Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE15',200,'Descriptive flexfield segment','ATTRIBUTE15','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE15','Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE14',200,'Descriptive flexfield segment','ATTRIBUTE14','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE14','Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE13',200,'Descriptive flexfield segment','ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE13','Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE12',200,'Descriptive flexfield segment','ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE12','Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE11',200,'Descriptive flexfield segment','ATTRIBUTE11','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE11','Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE10',200,'Descriptive flexfield segment','ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE10','Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE9',200,'Descriptive flexfield segment','ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE9','Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE8',200,'Descriptive flexfield segment','ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE8','Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE7',200,'Descriptive flexfield segment','ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE7','Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE6',200,'Descriptive flexfield segment','ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE6','Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE5',200,'Descriptive flexfield segment','ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE5','Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE4',200,'Descriptive flexfield segment','ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE4','Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ATTRIBUTE_CATEGORY',200,'Descriptive flexfield segment','ATTRIBUTE_CATEGORY','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ATTRIBUTE_CATEGORY','Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','FEDERAL_REPORTABLE_FLAG',200,'Federal reportable flag','FEDERAL_REPORTABLE_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','FEDERAL_REPORTABLE_FLAG','Federal Reportable Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','STATE_REPORTABLE_FLAG',200,'State reportable flag','STATE_REPORTABLE_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','STATE_REPORTABLE_FLAG','State Reportable Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','NAME_CONTROL',200,'Name control','NAME_CONTROL','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','NAME_CONTROL','Name Control','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','TAX_VERIFICATION_DATE',200,'Tax verification date','TAX_VERIFICATION_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','TAX_VERIFICATION_DATE','Tax Verification Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','AMOUNT_INCLUDES_TAX_FLAG',200,'Do amounts include tax from this supplier?','AMOUNT_INCLUDES_TAX_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','AMOUNT_INCLUDES_TAX_FLAG','Amount Includes Tax Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','AUTO_TAX_CALC_OVERRIDE',200,'Allows override of tax calculation at supplier site level','AUTO_TAX_CALC_OVERRIDE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','AUTO_TAX_CALC_OVERRIDE','Auto Tax Calc Override','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','AUTO_TAX_CALC_FLAG',200,'Supplier level where the tax calculation is done','AUTO_TAX_CALC_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','AUTO_TAX_CALC_FLAG','Auto Tax Calc Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','AP_TAX_ROUNDING_RULE',200,'No Longer Used','AP_TAX_ROUNDING_RULE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','AP_TAX_ROUNDING_RULE','Ap Tax Rounding Rule','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EXCLUSIVE_PAYMENT_FLAG',200,'Indicates exclusive payment','EXCLUSIVE_PAYMENT_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','EXCLUSIVE_PAYMENT_FLAG','Exclusive Payment Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','HOLD_UNMATCHED_INVOICES_FLAG',200,'Indicates whether unmatched invoices should be put on hold','HOLD_UNMATCHED_INVOICES_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','HOLD_UNMATCHED_INVOICES_FLAG','Hold Unmatched Invoices Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ALLOW_UNORDERED_RECEIPTS_FLAG',200,'Indicates whether unordered receipts are allowed','ALLOW_UNORDERED_RECEIPTS_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ALLOW_UNORDERED_RECEIPTS_FLAG','Allow Unordered Receipts Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ALLOW_SUBSTITUTE_RECEIPTS_FLAG',200,'Indicates whether substitute items can be received in place of the ordered items','ALLOW_SUBSTITUTE_RECEIPTS_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ALLOW_SUBSTITUTE_RECEIPTS_FLAG','Allow Substitute Receipts Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','RECEIVING_ROUTING_ID',200,'Receipt routing unique identifier','RECEIVING_ROUTING_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','RECEIVING_ROUTING_ID','Receiving Routing Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','RECEIPT_DAYS_EXCEPTION_CODE',200,'Action to be taken when items are received earlier or later than the allowed number of days specified','RECEIPT_DAYS_EXCEPTION_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','RECEIPT_DAYS_EXCEPTION_CODE','Receipt Days Exception Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','DAYS_LATE_RECEIPT_ALLOWED',200,'Maximum acceptable number of days items can be received late','DAYS_LATE_RECEIPT_ALLOWED','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','DAYS_LATE_RECEIPT_ALLOWED','Days Late Receipt Allowed','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','DAYS_EARLY_RECEIPT_ALLOWED',200,'Maximum acceptable number of days items can be received early','DAYS_EARLY_RECEIPT_ALLOWED','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','DAYS_EARLY_RECEIPT_ALLOWED','Days Early Receipt Allowed','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','ENFORCE_SHIP_TO_LOCATION_CODE',200,'Action to be taken when the receiving location differs from the ship-to location','ENFORCE_SHIP_TO_LOCATION_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','ENFORCE_SHIP_TO_LOCATION_CODE','Enforce Ship To Location Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','QTY_RCV_EXCEPTION_CODE',200,'Enforces, warns, or ignores quantity receiving tolerance','QTY_RCV_EXCEPTION_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','QTY_RCV_EXCEPTION_CODE','Qty Rcv Exception Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','QTY_RCV_TOLERANCE',200,'Quantity received tolerance percentage','QTY_RCV_TOLERANCE','','~T~D~2','','ANONYMOUS','NUMBER','AP_SUPPLIERS','QTY_RCV_TOLERANCE','Qty Rcv Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','RECEIPT_REQUIRED_FLAG',200,'Indicates whether shipment must be received before the invoice is paid','RECEIPT_REQUIRED_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','RECEIPT_REQUIRED_FLAG','Receipt Required Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','INSPECTION_REQUIRED_FLAG',200,'Indicates whether inspection is required or not','INSPECTION_REQUIRED_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','INSPECTION_REQUIRED_FLAG','Inspection Required Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PRICE_TOLERANCE',200,'Not used','PRICE_TOLERANCE','','~T~D~2','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PRICE_TOLERANCE','Price Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','TERMS_DATE_BASIS',200,'Type of invoice payment schedule basis','TERMS_DATE_BASIS','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','TERMS_DATE_BASIS','Terms Date Basis','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','HOLD_DATE',200,'Date the supplier was placed on purchasing hold','HOLD_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIERS','HOLD_DATE','Hold Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','HOLD_BY',200,'Unique identifier for the employee who placed the supplier on hold','HOLD_BY','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','HOLD_BY','Hold By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PURCHASING_HOLD_REASON',200,'Reason for placing the supplier on purchasing hold','PURCHASING_HOLD_REASON','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','PURCHASING_HOLD_REASON','Purchasing Hold Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','HOLD_FLAG',200,'Indicator of whether the supplier is on purchasing hold','HOLD_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','HOLD_FLAG','Hold Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','STANDARD_INDUSTRY_CLASS',200,'Standard industry classification number','STANDARD_INDUSTRY_CLASS','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','STANDARD_INDUSTRY_CLASS','Standard Industry Class','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','SMALL_BUSINESS_FLAG',200,'Indicates that the supplier is a small business','SMALL_BUSINESS_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','SMALL_BUSINESS_FLAG','Small Business Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','WOMEN_OWNED_FLAG',200,'Indicates whether the supplier is a woman-owned business','WOMEN_OWNED_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','WOMEN_OWNED_FLAG','Women Owned Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BANK_ACCOUNT_TYPE',200,'No longer used','BANK_ACCOUNT_TYPE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','BANK_ACCOUNT_TYPE','Bank Account Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BANK_NUM',200,'No longer used','BANK_NUM','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','BANK_NUM','Bank Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','TCA_SYNC_VAT_REG_NUM',200,'For internal use only','TCA_SYNC_VAT_REG_NUM','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','TCA_SYNC_VAT_REG_NUM','Tca Sync Vat Reg Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','TCA_SYNC_VENDOR_NAME',200,'For internal use only','TCA_SYNC_VENDOR_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','TCA_SYNC_VENDOR_NAME','Tca Sync Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','TCA_SYNC_NUM_1099',200,'For internal use only','TCA_SYNC_NUM_1099','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','TCA_SYNC_NUM_1099','Tca Sync Num 1099','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','NI_NUMBER',200,'National Insurance Number','NI_NUMBER','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','NI_NUMBER','Ni Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PARENT_PARTY_ID',200,'Parent Party Identifier','PARENT_PARTY_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PARENT_PARTY_ID','Parent Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','PARTY_ID',200,'Party Identifier','PARTY_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','PARTY_ID','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','OFFSET_TAX_FLAG',200,'Indicator of whether the supplier uses offset taxes','OFFSET_TAX_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','OFFSET_TAX_FLAG','Offset Tax Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','CREATE_DEBIT_MEMO_FLAG',200,'Indicator of whether a debit memo should be created','CREATE_DEBIT_MEMO_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','CREATE_DEBIT_MEMO_FLAG','Create Debit Memo Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','FUTURE_DATED_PAYMENT_CCID',200,'Accounting Flexfield identifier for the future dated payment account','FUTURE_DATED_PAYMENT_CCID','','','','ANONYMOUS','NUMBER','AP_SUPPLIERS','FUTURE_DATED_PAYMENT_CCID','Future Dated Payment Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','MATCH_OPTION',200,'Indicator of whether to match invoices to the purchase order or the receipt for this supplier','MATCH_OPTION','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','MATCH_OPTION','Match Option','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BANK_BRANCH_TYPE',200,'No longer used','BANK_BRANCH_TYPE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','BANK_BRANCH_TYPE','Bank Branch Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','BANK_CHARGE_BEARER',200,'Indicator of whether this supplier bears bank charges','BANK_CHARGE_BEARER','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','BANK_CHARGE_BEARER','Bank Charge Bearer','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EDI_REMITTANCE_INSTRUCTION',200,'No longer used','EDI_REMITTANCE_INSTRUCTION','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','EDI_REMITTANCE_INSTRUCTION','Edi Remittance Instruction','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EDI_REMITTANCE_METHOD',200,'No longer used','EDI_REMITTANCE_METHOD','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','EDI_REMITTANCE_METHOD','Edi Remittance Method','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EDI_PAYMENT_FORMAT',200,'No longer used','EDI_PAYMENT_FORMAT','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','EDI_PAYMENT_FORMAT','Edi Payment Format','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EDI_PAYMENT_METHOD',200,'No longer used','EDI_PAYMENT_METHOD','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','EDI_PAYMENT_METHOD','Edi Payment Method','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','EDI_TRANSACTION_HANDLING',200,'No longer used','EDI_TRANSACTION_HANDLING','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','EDI_TRANSACTION_HANDLING','Edi Transaction Handling','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE_CATEGORY',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE_CATEGORY','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE_CATEGORY','Global Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE20',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE20','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE20','Global Attribute20','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE19',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE19','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE19','Global Attribute19','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE18',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE18','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE18','Global Attribute18','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE17',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE17','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE17','Global Attribute17','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE16',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE16','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE16','Global Attribute16','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIERS','GLOBAL_ATTRIBUTE15',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE15','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIERS','GLOBAL_ATTRIBUTE15','Global Attribute15','','','','US');
--Inserting Object Components for AP_SUPPLIERS
--Inserting Object Component Joins for AP_SUPPLIERS
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--Exporting View Component Data of the View -  EIS_AP_PAYMENT_REGISTER_HDS
prompt Creating Object Data AP_SUPPLIER_SITES_ALL
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object AP_SUPPLIER_SITES_ALL
xxeis.eis_rsc_ins.v( 'AP_SUPPLIER_SITES_ALL',200,'','1.0','','','ANONYMOUS','APPS','Ap Supplier Sites All','ASSA','','','SYNONYM','US','','');
--Delete Object Columns for AP_SUPPLIER_SITES_ALL
xxeis.eis_rsc_utility.delete_view_rows('AP_SUPPLIER_SITES_ALL',200,FALSE);
--Inserting Object Columns for AP_SUPPLIER_SITES_ALL
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','DOING_BUS_AS_NAME',200,'','DOING_BUS_AS_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','DOING_BUS_AS_NAME','Doing Bus As Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','DIVISION_NAME',200,'','DIVISION_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','DIVISION_NAME','Division Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','SMALL_BUSINESS_CODE',200,'','SMALL_BUSINESS_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','SMALL_BUSINESS_CODE','Small Business Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','CCR_COMMENTS',200,'','CCR_COMMENTS','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','CCR_COMMENTS','Ccr Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','DEBARMENT_START_DATE',200,'','DEBARMENT_START_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIER_SITES_ALL','DEBARMENT_START_DATE','Debarment Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','DEBARMENT_END_DATE',200,'','DEBARMENT_END_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIER_SITES_ALL','DEBARMENT_END_DATE','Debarment End Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','VENDOR_SITE_ID',200,'','VENDOR_SITE_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','VENDOR_SITE_ID','Vendor Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','LAST_UPDATE_DATE',200,'','LAST_UPDATE_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIER_SITES_ALL','LAST_UPDATE_DATE','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','LAST_UPDATED_BY',200,'','LAST_UPDATED_BY','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','LAST_UPDATED_BY','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','VENDOR_ID',200,'','VENDOR_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','VENDOR_ID','Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','VENDOR_SITE_CODE',200,'','VENDOR_SITE_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','VENDOR_SITE_CODE','Vendor Site Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','VENDOR_SITE_CODE_ALT',200,'','VENDOR_SITE_CODE_ALT','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','VENDOR_SITE_CODE_ALT','Vendor Site Code Alt','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','LAST_UPDATE_LOGIN',200,'','LAST_UPDATE_LOGIN','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','LAST_UPDATE_LOGIN','Last Update Login','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','CREATION_DATE',200,'','CREATION_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIER_SITES_ALL','CREATION_DATE','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','CREATED_BY',200,'','CREATED_BY','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','CREATED_BY','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','PURCHASING_SITE_FLAG',200,'','PURCHASING_SITE_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','PURCHASING_SITE_FLAG','Purchasing Site Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','CAGE_CODE',200,'','CAGE_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','CAGE_CODE','Cage Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','LEGAL_BUSINESS_NAME',200,'','LEGAL_BUSINESS_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','LEGAL_BUSINESS_NAME','Legal Business Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','PAY_AWT_GROUP_ID',200,'','PAY_AWT_GROUP_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','PAY_AWT_GROUP_ID','Pay Awt Group Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','BANK_NUMBER',200,'','BANK_NUMBER','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','BANK_NUMBER','Bank Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ADDRESS_LINE4',200,'','ADDRESS_LINE4','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ADDRESS_LINE4','Address Line4','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','COUNTY',200,'','COUNTY','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','COUNTY','County','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ADDRESS_STYLE',200,'','ADDRESS_STYLE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ADDRESS_STYLE','Address Style','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','LANGUAGE',200,'','LANGUAGE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','LANGUAGE','Language','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ALLOW_AWT_FLAG',200,'','ALLOW_AWT_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ALLOW_AWT_FLAG','Allow Awt Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','AWT_GROUP_ID',200,'','AWT_GROUP_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','AWT_GROUP_ID','Awt Group Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE1',200,'','GLOBAL_ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE1','Global Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE2',200,'','GLOBAL_ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE2','Global Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE3',200,'','GLOBAL_ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE3','Global Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE4',200,'','GLOBAL_ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE4','Global Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE5',200,'','GLOBAL_ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE5','Global Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE6',200,'','GLOBAL_ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE6','Global Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE7',200,'','GLOBAL_ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE7','Global Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE8',200,'','GLOBAL_ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE8','Global Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE9',200,'','GLOBAL_ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE9','Global Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE10',200,'','GLOBAL_ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE10','Global Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE11',200,'','GLOBAL_ATTRIBUTE11','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE11','Global Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE12',200,'','GLOBAL_ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE12','Global Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE13',200,'','GLOBAL_ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE13','Global Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE14',200,'','GLOBAL_ATTRIBUTE14','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE14','Global Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE15',200,'','GLOBAL_ATTRIBUTE15','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE15','Global Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE16',200,'','GLOBAL_ATTRIBUTE16','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE16','Global Attribute16','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE17',200,'','GLOBAL_ATTRIBUTE17','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE17','Global Attribute17','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE18',200,'','GLOBAL_ATTRIBUTE18','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE18','Global Attribute18','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE19',200,'','GLOBAL_ATTRIBUTE19','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE19','Global Attribute19','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE20',200,'','GLOBAL_ATTRIBUTE20','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE20','Global Attribute20','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE_CATEGORY',200,'','GLOBAL_ATTRIBUTE_CATEGORY','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GLOBAL_ATTRIBUTE_CATEGORY','Global Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','EDI_TRANSACTION_HANDLING',200,'','EDI_TRANSACTION_HANDLING','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','EDI_TRANSACTION_HANDLING','Edi Transaction Handling','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','EDI_ID_NUMBER',200,'','EDI_ID_NUMBER','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','EDI_ID_NUMBER','Edi Id Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','EDI_PAYMENT_METHOD',200,'','EDI_PAYMENT_METHOD','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','EDI_PAYMENT_METHOD','Edi Payment Method','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','EDI_PAYMENT_FORMAT',200,'','EDI_PAYMENT_FORMAT','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','EDI_PAYMENT_FORMAT','Edi Payment Format','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','EDI_REMITTANCE_METHOD',200,'','EDI_REMITTANCE_METHOD','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','EDI_REMITTANCE_METHOD','Edi Remittance Method','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','BANK_CHARGE_BEARER',200,'','BANK_CHARGE_BEARER','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','BANK_CHARGE_BEARER','Bank Charge Bearer','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','EDI_REMITTANCE_INSTRUCTION',200,'','EDI_REMITTANCE_INSTRUCTION','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','EDI_REMITTANCE_INSTRUCTION','Edi Remittance Instruction','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','BANK_BRANCH_TYPE',200,'','BANK_BRANCH_TYPE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','BANK_BRANCH_TYPE','Bank Branch Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','PAY_ON_CODE',200,'','PAY_ON_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','PAY_ON_CODE','Pay On Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','DEFAULT_PAY_SITE_ID',200,'','DEFAULT_PAY_SITE_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','DEFAULT_PAY_SITE_ID','Default Pay Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','PAY_ON_RECEIPT_SUMMARY_CODE',200,'','PAY_ON_RECEIPT_SUMMARY_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','PAY_ON_RECEIPT_SUMMARY_CODE','Pay On Receipt Summary Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','TP_HEADER_ID',200,'','TP_HEADER_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','TP_HEADER_ID','Tp Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ECE_TP_LOCATION_CODE',200,'','ECE_TP_LOCATION_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ECE_TP_LOCATION_CODE','Ece Tp Location Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','PCARD_SITE_FLAG',200,'','PCARD_SITE_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','PCARD_SITE_FLAG','Pcard Site Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','MATCH_OPTION',200,'','MATCH_OPTION','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','MATCH_OPTION','Match Option','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','COUNTRY_OF_ORIGIN_CODE',200,'','COUNTRY_OF_ORIGIN_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','COUNTRY_OF_ORIGIN_CODE','Country Of Origin Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','FUTURE_DATED_PAYMENT_CCID',200,'','FUTURE_DATED_PAYMENT_CCID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','FUTURE_DATED_PAYMENT_CCID','Future Dated Payment Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','CREATE_DEBIT_MEMO_FLAG',200,'','CREATE_DEBIT_MEMO_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','CREATE_DEBIT_MEMO_FLAG','Create Debit Memo Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','OFFSET_TAX_FLAG',200,'','OFFSET_TAX_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','OFFSET_TAX_FLAG','Offset Tax Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','SUPPLIER_NOTIF_METHOD',200,'','SUPPLIER_NOTIF_METHOD','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','SUPPLIER_NOTIF_METHOD','Supplier Notif Method','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','EMAIL_ADDRESS',200,'','EMAIL_ADDRESS','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','EMAIL_ADDRESS','Email Address','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','REMITTANCE_EMAIL',200,'','REMITTANCE_EMAIL','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','REMITTANCE_EMAIL','Remittance Email','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','PRIMARY_PAY_SITE_FLAG',200,'','PRIMARY_PAY_SITE_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','PRIMARY_PAY_SITE_FLAG','Primary Pay Site Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','SHIPPING_CONTROL',200,'','SHIPPING_CONTROL','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','SHIPPING_CONTROL','Shipping Control','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','SELLING_COMPANY_IDENTIFIER',200,'','SELLING_COMPANY_IDENTIFIER','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','SELLING_COMPANY_IDENTIFIER','Selling Company Identifier','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','GAPLESS_INV_NUM_FLAG',200,'','GAPLESS_INV_NUM_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','GAPLESS_INV_NUM_FLAG','Gapless Inv Num Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','DUNS_NUMBER',200,'','DUNS_NUMBER','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','DUNS_NUMBER','Duns Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','TOLERANCE_ID',200,'','TOLERANCE_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','TOLERANCE_ID','Tolerance Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','LOCATION_ID',200,'','LOCATION_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','LOCATION_ID','Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','PARTY_SITE_ID',200,'','PARTY_SITE_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','PARTY_SITE_ID','Party Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','SERVICES_TOLERANCE_ID',200,'','SERVICES_TOLERANCE_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','SERVICES_TOLERANCE_ID','Services Tolerance Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','RETAINAGE_RATE',200,'','RETAINAGE_RATE','','~T~D~2','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','RETAINAGE_RATE','Retainage Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','TCA_SYNC_STATE',200,'','TCA_SYNC_STATE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','TCA_SYNC_STATE','Tca Sync State','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','TCA_SYNC_PROVINCE',200,'','TCA_SYNC_PROVINCE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','TCA_SYNC_PROVINCE','Tca Sync Province','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','TCA_SYNC_COUNTY',200,'','TCA_SYNC_COUNTY','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','TCA_SYNC_COUNTY','Tca Sync County','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','TCA_SYNC_CITY',200,'','TCA_SYNC_CITY','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','TCA_SYNC_CITY','Tca Sync City','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','TCA_SYNC_ZIP',200,'','TCA_SYNC_ZIP','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','TCA_SYNC_ZIP','Tca Sync Zip','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','TCA_SYNC_COUNTRY',200,'','TCA_SYNC_COUNTRY','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','TCA_SYNC_COUNTRY','Tca Sync Country','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','RFQ_ONLY_SITE_FLAG',200,'','RFQ_ONLY_SITE_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','RFQ_ONLY_SITE_FLAG','Rfq Only Site Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','PAY_SITE_FLAG',200,'','PAY_SITE_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','PAY_SITE_FLAG','Pay Site Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ATTENTION_AR_FLAG',200,'','ATTENTION_AR_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ATTENTION_AR_FLAG','Attention Ar Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ADDRESS_LINE1',200,'','ADDRESS_LINE1','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ADDRESS_LINE1','Address Line1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ADDRESS_LINES_ALT',200,'','ADDRESS_LINES_ALT','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ADDRESS_LINES_ALT','Address Lines Alt','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ADDRESS_LINE2',200,'','ADDRESS_LINE2','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ADDRESS_LINE2','Address Line2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ADDRESS_LINE3',200,'','ADDRESS_LINE3','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ADDRESS_LINE3','Address Line3','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','CITY',200,'','CITY','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','CITY','City','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','STATE',200,'','STATE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','STATE','State','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ZIP',200,'','ZIP','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ZIP','Zip','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','PROVINCE',200,'','PROVINCE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','PROVINCE','Province','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','COUNTRY',200,'','COUNTRY','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','COUNTRY','Country','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','AREA_CODE',200,'','AREA_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','AREA_CODE','Area Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','PHONE',200,'','PHONE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','PHONE','Phone','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','CUSTOMER_NUM',200,'','CUSTOMER_NUM','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','CUSTOMER_NUM','Customer Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','SHIP_TO_LOCATION_ID',200,'','SHIP_TO_LOCATION_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','SHIP_TO_LOCATION_ID','Ship To Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','BILL_TO_LOCATION_ID',200,'','BILL_TO_LOCATION_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','BILL_TO_LOCATION_ID','Bill To Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','SHIP_VIA_LOOKUP_CODE',200,'','SHIP_VIA_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','SHIP_VIA_LOOKUP_CODE','Ship Via Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','FREIGHT_TERMS_LOOKUP_CODE',200,'','FREIGHT_TERMS_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','FREIGHT_TERMS_LOOKUP_CODE','Freight Terms Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','FOB_LOOKUP_CODE',200,'','FOB_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','FOB_LOOKUP_CODE','Fob Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','INACTIVE_DATE',200,'','INACTIVE_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIER_SITES_ALL','INACTIVE_DATE','Inactive Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','FAX',200,'','FAX','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','FAX','Fax','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','FAX_AREA_CODE',200,'','FAX_AREA_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','FAX_AREA_CODE','Fax Area Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','TELEX',200,'','TELEX','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','TELEX','Telex','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','PAYMENT_METHOD_LOOKUP_CODE',200,'','PAYMENT_METHOD_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','PAYMENT_METHOD_LOOKUP_CODE','Payment Method Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','BANK_ACCOUNT_NAME',200,'','BANK_ACCOUNT_NAME','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','BANK_ACCOUNT_NAME','Bank Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','BANK_ACCOUNT_NUM',200,'','BANK_ACCOUNT_NUM','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','BANK_ACCOUNT_NUM','Bank Account Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','BANK_NUM',200,'','BANK_NUM','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','BANK_NUM','Bank Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','BANK_ACCOUNT_TYPE',200,'','BANK_ACCOUNT_TYPE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','BANK_ACCOUNT_TYPE','Bank Account Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','TERMS_DATE_BASIS',200,'','TERMS_DATE_BASIS','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','TERMS_DATE_BASIS','Terms Date Basis','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','CURRENT_CATALOG_NUM',200,'','CURRENT_CATALOG_NUM','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','CURRENT_CATALOG_NUM','Current Catalog Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','VAT_CODE',200,'','VAT_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','VAT_CODE','Vat Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','DISTRIBUTION_SET_ID',200,'','DISTRIBUTION_SET_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','DISTRIBUTION_SET_ID','Distribution Set Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ACCTS_PAY_CODE_COMBINATION_ID',200,'','ACCTS_PAY_CODE_COMBINATION_ID','','~T~D~2','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','ACCTS_PAY_CODE_COMBINATION_ID','Accts Pay Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','PREPAY_CODE_COMBINATION_ID',200,'','PREPAY_CODE_COMBINATION_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','PREPAY_CODE_COMBINATION_ID','Prepay Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','PAY_GROUP_LOOKUP_CODE',200,'','PAY_GROUP_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','PAY_GROUP_LOOKUP_CODE','Pay Group Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','PAYMENT_PRIORITY',200,'','PAYMENT_PRIORITY','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','PAYMENT_PRIORITY','Payment Priority','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','TERMS_ID',200,'','TERMS_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','TERMS_ID','Terms Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','INVOICE_AMOUNT_LIMIT',200,'','INVOICE_AMOUNT_LIMIT','','~T~D~2','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','INVOICE_AMOUNT_LIMIT','Invoice Amount Limit','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','PAY_DATE_BASIS_LOOKUP_CODE',200,'','PAY_DATE_BASIS_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','PAY_DATE_BASIS_LOOKUP_CODE','Pay Date Basis Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ALWAYS_TAKE_DISC_FLAG',200,'','ALWAYS_TAKE_DISC_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ALWAYS_TAKE_DISC_FLAG','Always Take Disc Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','INVOICE_CURRENCY_CODE',200,'','INVOICE_CURRENCY_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','INVOICE_CURRENCY_CODE','Invoice Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','PAYMENT_CURRENCY_CODE',200,'','PAYMENT_CURRENCY_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','PAYMENT_CURRENCY_CODE','Payment Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','HOLD_ALL_PAYMENTS_FLAG',200,'','HOLD_ALL_PAYMENTS_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','HOLD_ALL_PAYMENTS_FLAG','Hold All Payments Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','HOLD_FUTURE_PAYMENTS_FLAG',200,'','HOLD_FUTURE_PAYMENTS_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','HOLD_FUTURE_PAYMENTS_FLAG','Hold Future Payments Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','HOLD_REASON',200,'','HOLD_REASON','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','HOLD_REASON','Hold Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','HOLD_UNMATCHED_INVOICES_FLAG',200,'','HOLD_UNMATCHED_INVOICES_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','HOLD_UNMATCHED_INVOICES_FLAG','Hold Unmatched Invoices Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','AP_TAX_ROUNDING_RULE',200,'','AP_TAX_ROUNDING_RULE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','AP_TAX_ROUNDING_RULE','Ap Tax Rounding Rule','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','AUTO_TAX_CALC_FLAG',200,'','AUTO_TAX_CALC_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','AUTO_TAX_CALC_FLAG','Auto Tax Calc Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','AUTO_TAX_CALC_OVERRIDE',200,'','AUTO_TAX_CALC_OVERRIDE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','AUTO_TAX_CALC_OVERRIDE','Auto Tax Calc Override','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','AMOUNT_INCLUDES_TAX_FLAG',200,'','AMOUNT_INCLUDES_TAX_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','AMOUNT_INCLUDES_TAX_FLAG','Amount Includes Tax Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','EXCLUSIVE_PAYMENT_FLAG',200,'','EXCLUSIVE_PAYMENT_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','EXCLUSIVE_PAYMENT_FLAG','Exclusive Payment Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','TAX_REPORTING_SITE_FLAG',200,'','TAX_REPORTING_SITE_FLAG','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','TAX_REPORTING_SITE_FLAG','Tax Reporting Site Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ATTRIBUTE_CATEGORY',200,'','ATTRIBUTE_CATEGORY','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ATTRIBUTE_CATEGORY','Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ATTRIBUTE1',200,'','ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ATTRIBUTE1','Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ATTRIBUTE2',200,'','ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ATTRIBUTE2','Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ATTRIBUTE3',200,'','ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ATTRIBUTE3','Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ATTRIBUTE4',200,'','ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ATTRIBUTE4','Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ATTRIBUTE5',200,'','ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ATTRIBUTE5','Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ATTRIBUTE6',200,'','ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ATTRIBUTE6','Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ATTRIBUTE7',200,'','ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ATTRIBUTE7','Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ATTRIBUTE8',200,'','ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ATTRIBUTE8','Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ATTRIBUTE9',200,'','ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ATTRIBUTE9','Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ATTRIBUTE10',200,'','ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ATTRIBUTE10','Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ATTRIBUTE11',200,'','ATTRIBUTE11','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ATTRIBUTE11','Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ATTRIBUTE12',200,'','ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ATTRIBUTE12','Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ATTRIBUTE13',200,'','ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ATTRIBUTE13','Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ATTRIBUTE14',200,'','ATTRIBUTE14','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ATTRIBUTE14','Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ATTRIBUTE15',200,'','ATTRIBUTE15','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','ATTRIBUTE15','Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','REQUEST_ID',200,'','REQUEST_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','REQUEST_ID','Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','PROGRAM_APPLICATION_ID',200,'','PROGRAM_APPLICATION_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','PROGRAM_APPLICATION_ID','Program Application Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','PROGRAM_ID',200,'','PROGRAM_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','PROGRAM_ID','Program Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','PROGRAM_UPDATE_DATE',200,'','PROGRAM_UPDATE_DATE','','','','ANONYMOUS','DATE','AP_SUPPLIER_SITES_ALL','PROGRAM_UPDATE_DATE','Program Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','VALIDATION_NUMBER',200,'','VALIDATION_NUMBER','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','VALIDATION_NUMBER','Validation Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','EXCLUDE_FREIGHT_FROM_DISCOUNT',200,'','EXCLUDE_FREIGHT_FROM_DISCOUNT','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','EXCLUDE_FREIGHT_FROM_DISCOUNT','Exclude Freight From Discount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','VAT_REGISTRATION_NUM',200,'','VAT_REGISTRATION_NUM','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','VAT_REGISTRATION_NUM','Vat Registration Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','OFFSET_VAT_CODE',200,'','OFFSET_VAT_CODE','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','OFFSET_VAT_CODE','Offset Vat Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','ORG_ID',200,'','ORG_ID','','','','ANONYMOUS','NUMBER','AP_SUPPLIER_SITES_ALL','ORG_ID','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_SUPPLIER_SITES_ALL','CHECK_DIGITS',200,'','CHECK_DIGITS','','','','ANONYMOUS','VARCHAR2','AP_SUPPLIER_SITES_ALL','CHECK_DIGITS','Check Digits','','','','US');
--Inserting Object Components for AP_SUPPLIER_SITES_ALL
--Inserting Object Component Joins for AP_SUPPLIER_SITES_ALL
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--Exporting View Component Data of the View -  EIS_AP_PAYMENT_REGISTER_HDS
prompt Creating Object Data AP_INVOICES
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object AP_INVOICES
xxeis.eis_rsc_ins.v( 'AP_INVOICES',200,'Detailed invoice records','1.0','','','ANONYMOUS','APPS','Ap Invoices','AI','','','SYNONYM','US','','');
--Delete Object Columns for AP_INVOICES
xxeis.eis_rsc_utility.delete_view_rows('AP_INVOICES',200,FALSE);
--Inserting Object Columns for AP_INVOICES
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PAID_ON_BEHALF_EMPLOYEE_ID',200,'When an expense report gets split in Both Pay scenario, the new expense report''s paid_on_behalf_employee_id gets populated with the original expense report''s employee_id.','PAID_ON_BEHALF_EMPLOYEE_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','PAID_ON_BEHALF_EMPLOYEE_ID','Paid On Behalf Employee Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','AMT_DUE_CCARD_COMPANY',200,'Amount that is due to credit card company','AMT_DUE_CCARD_COMPANY','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','AMT_DUE_CCARD_COMPANY','Amt Due Ccard Company','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','AMT_DUE_EMPLOYEE',200,'Amount that is due to employee','AMT_DUE_EMPLOYEE','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','AMT_DUE_EMPLOYEE','Amt Due Employee','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','INVOICE_ID',200,'Unique internal Identifier for this record. Generated using a database sequence.','INVOICE_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','INVOICE_ID','Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','LAST_UPDATE_DATE',200,'Standard Who column - date when a user last updated this row.','LAST_UPDATE_DATE','','','','ANONYMOUS','DATE','AP_INVOICES','LAST_UPDATE_DATE','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','LAST_UPDATED_BY',200,'Standard who column - unique internal identifier user who last updated this row. Foreign key to the USER_ID column of the FND_USER table.','LAST_UPDATED_BY','','','','ANONYMOUS','NUMBER','AP_INVOICES','LAST_UPDATED_BY','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','VENDOR_ID',200,'The unique internal identifier of the supplier to which the invoice belongs. Foreign key to the AP_SUPPLIERS table.','VENDOR_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','VENDOR_ID','Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','INVOICE_NUM',200,'Invoice number for an invoice. This number entered will be unique per supplier and operating unit.','INVOICE_NUM','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','INVOICE_NUM','Invoice Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','SET_OF_BOOKS_ID',200,'Ledger unique identifier','SET_OF_BOOKS_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','SET_OF_BOOKS_ID','Set Of Books Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','INVOICE_CURRENCY_CODE',200,'Currency code of the invoice. Foreign key to the FND_CURRENCIES table.','INVOICE_CURRENCY_CODE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','INVOICE_CURRENCY_CODE','Invoice Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PAYMENT_CURRENCY_CODE',200,'Currency code of payment. If the Invoice Currency is not a fixed-rate currency, then the Payment Currency will be the same as the INVOICE_CURRENCY_CODE. For fixed-rate currency such as euro or another EMU currency it can be different to INV','PAYMENT_CURRENCY_CODE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','PAYMENT_CURRENCY_CODE','Payment Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PAYMENT_CROSS_RATE',200,'Exchange rate between invoice and payment. The value is always 1 unless they are associated fixed-rate currencies such as euro or another EMU currency.','PAYMENT_CROSS_RATE','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','PAYMENT_CROSS_RATE','Payment Cross Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','INVOICE_AMOUNT',200,'Total amount of the invoice. This amount can be negative for following types of invoices: Credit Memo, Debit Memo, Mixed, Automatic Withholding, Expense Reports','INVOICE_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','INVOICE_AMOUNT','Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','VENDOR_SITE_ID',200,'The unique internal identifier for the payment site of the supplier. Foreign key to the AP_SUPPLIER_SITES_ALL table.','VENDOR_SITE_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','VENDOR_SITE_ID','Vendor Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','AMOUNT_PAID',200,'Invoice amount paid','AMOUNT_PAID','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','AMOUNT_PAID','Amount Paid','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','DISCOUNT_AMOUNT_TAKEN',200,'This is the amount of discount taken for the invoice. This column gets updated during payment processing of this invoice and when discount is calculated.','DISCOUNT_AMOUNT_TAKEN','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','DISCOUNT_AMOUNT_TAKEN','Discount Amount Taken','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','INVOICE_DATE',200,'Invoice date','INVOICE_DATE','','','','ANONYMOUS','DATE','AP_INVOICES','INVOICE_DATE','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','SOURCE',200,'Defines the source of the invoice. Possible values for this column are derived from FND_LOOKUP_VALUES for lookup_type ''SOURCE''','SOURCE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','SOURCE','Source','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','INVOICE_TYPE_LOOKUP_CODE',200,'Defines the source of the invoice. Possible values for this column are derived from FND_LOOKUP_VALUES for lookup_type ''INVOICE TYPE''','INVOICE_TYPE_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','INVOICE_TYPE_LOOKUP_CODE','Invoice Type Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','DESCRIPTION',200,'Description of the invoice. This column is a free form text field in Invoice workbench.','DESCRIPTION','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','DESCRIPTION','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','BATCH_ID',200,'Invoice Batch Identifier. This column will be populated if Invoices are entered in batches. Foreign key to the AP_BATCHES_ALL table.','BATCH_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','BATCH_ID','Batch Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','AMOUNT_APPLICABLE_TO_DISCOUNT',200,'Amount of invoice applicable to a discount. This column gets calculated during scheduled payment creation based following :- Exclude Freight from discount option- Execlude Tax from the discount option- Prepayment applied with discount or wi','AMOUNT_APPLICABLE_TO_DISCOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','AMOUNT_APPLICABLE_TO_DISCOUNT','Amount Applicable To Discount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','TAX_AMOUNT',200,'No Longer Used','TAX_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','TAX_AMOUNT','Tax Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','TERMS_ID',200,'Payment terms identifier, this column corresponds to the ''Terms'' field in Invoice workbench. Foreign key to the TERMS_ID of the AP_TERMS_TL table.','TERMS_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','TERMS_ID','Terms Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','TERMS_DATE',200,'Date used with payment terms to calculate scheduled payment of an invoice. This date will be defaulted from one of the following Payables System Setup option defined for Term Date Basis:System Date, Goods Received Date, Invoice Date, Invoic','TERMS_DATE','','','','ANONYMOUS','DATE','AP_INVOICES','TERMS_DATE','Terms Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PAYMENT_METHOD_LOOKUP_CODE',200,'Name of the payment method used to pay the supplier site. Foreign key from the table IBY_PAYMENT_METHODS_B. This column is populated only for the invoices that are migrated from prior release.','PAYMENT_METHOD_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','PAYMENT_METHOD_LOOKUP_CODE','Payment Method Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PAY_GROUP_LOOKUP_CODE',200,'Name of the Paygroup defined to pay a category of suppliers or invoices in the same payment batch. Possible values for this column are derived from FND_LOOKUP_VALUES for lookup_type ''PAY GROUP''','PAY_GROUP_LOOKUP_CODE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','PAY_GROUP_LOOKUP_CODE','Pay Group Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ACCTS_PAY_CODE_COMBINATION_ID',200,'Accounting Flexfield identifier for accounts payable liability account','ACCTS_PAY_CODE_COMBINATION_ID','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','ACCTS_PAY_CODE_COMBINATION_ID','Accts Pay Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PAYMENT_STATUS_FLAG',200,'Flag that indicates if invoice has been paid or not. Possible Values: ''N'': Not paid, ''Y'': Paid, ''P'': Partially Paid','PAYMENT_STATUS_FLAG','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','PAYMENT_STATUS_FLAG','Payment Status Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','CREATION_DATE',200,'Standard who column - date when this row was created.','CREATION_DATE','','','','ANONYMOUS','DATE','AP_INVOICES','CREATION_DATE','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','CREATED_BY',200,'Standard who column - unique internal identifier of user who created this row. Foreign key to the USER_ID column of the FND_USER table.','CREATED_BY','','','','ANONYMOUS','NUMBER','AP_INVOICES','CREATED_BY','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','BASE_AMOUNT',200,'Invoice amount in functional currency. This column gets populated for the invoice entered in non-functional currency and this column corresponds to the ''Functional Curr Amount'' field in Invoice workbench.','BASE_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','BASE_AMOUNT','Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','VAT_CODE',200,'No Longer Used','VAT_CODE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','VAT_CODE','Vat Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','LAST_UPDATE_LOGIN',200,'Standard who column - operating system login of user who last updated this row. Foreign key to the LOGIN_ID column of the FND_LOGINS table.','LAST_UPDATE_LOGIN','','','','ANONYMOUS','NUMBER','AP_INVOICES','LAST_UPDATE_LOGIN','Last Update Login','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','EXCLUSIVE_PAYMENT_FLAG',200,'This flag indicates to pay this invoice using separate payment document.This columns corresponds to ''Pay alone'' checkbox of Invoice Workbench.','EXCLUSIVE_PAYMENT_FLAG','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','EXCLUSIVE_PAYMENT_FLAG','Exclusive Payment Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PO_HEADER_ID',200,'Purchase order identifier for invoices with QUICKMATCH and PO DEFAULT types, and prepayments associated with purchase order','PO_HEADER_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','PO_HEADER_ID','Po Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','FREIGHT_AMOUNT',200,'Freight amount used exclude when Payables calculates the default discountable amount for an invoice, if your Exclude Freight from Discount option is enabled for the supplier site.','FREIGHT_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','FREIGHT_AMOUNT','Freight Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GOODS_RECEIVED_DATE',200,'Date invoice items received','GOODS_RECEIVED_DATE','','','','ANONYMOUS','DATE','AP_INVOICES','GOODS_RECEIVED_DATE','Goods Received Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','INVOICE_RECEIVED_DATE',200,'Date invoice received','INVOICE_RECEIVED_DATE','','','','ANONYMOUS','DATE','AP_INVOICES','INVOICE_RECEIVED_DATE','Invoice Received Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','VOUCHER_NUM',200,'Voucher number assigned to an invoice for internal tracking purposes. Payables assigns a unique number if Sequential numbering is enabled and will be free form text field if sequential number is not enabled. It will be a non-editable field ','VOUCHER_NUM','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','VOUCHER_NUM','Voucher Num','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','APPROVED_AMOUNT',200,'Invoice amount approved through manual authorization for payment (used for reference purposes only).','APPROVED_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','APPROVED_AMOUNT','Approved Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','RECURRING_PAYMENT_ID',200,'Recurring invoice identifier. Foreign key to the RECURRING_PAYMENT_ID of the AP_RECURRING_PAYMENTS_ALL table.','RECURRING_PAYMENT_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','RECURRING_PAYMENT_ID','Recurring Payment Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','EXCHANGE_RATE',200,'Exchange rate for foreign currency invoice.','EXCHANGE_RATE','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','EXCHANGE_RATE','Exchange Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','EXCHANGE_RATE_TYPE',200,'Exchange rate type for foreign currency invoice. Payables uses following five types of exchange rates; User, Spot, Corporate, User-Defined, EMU Fixed','EXCHANGE_RATE_TYPE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','EXCHANGE_RATE_TYPE','Exchange Rate Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','EXCHANGE_DATE',200,'Date exchange rate is effective, usually accounting date of a transaction','EXCHANGE_DATE','','','','ANONYMOUS','DATE','AP_INVOICES','EXCHANGE_DATE','Exchange Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','EARLIEST_SETTLEMENT_DATE',200,'Date associated with a prepayment after which prepayment can be applied against invoices. Only used for temporary prepayments. Column is null for permanent prepayments and other invoice types.','EARLIEST_SETTLEMENT_DATE','','','','ANONYMOUS','DATE','AP_INVOICES','EARLIEST_SETTLEMENT_DATE','Earliest Settlement Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ORIGINAL_PREPAYMENT_AMOUNT',200,'No longer used','ORIGINAL_PREPAYMENT_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','ORIGINAL_PREPAYMENT_AMOUNT','Original Prepayment Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PARTY_SITE_ID',200,'Party Site identifier','PARTY_SITE_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','PARTY_SITE_ID','Party Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PAY_PROC_TRXN_TYPE_CODE',200,'Type of payment processing transaction or document. This derived from the PAYMENT_FUNCTION and passed to IBY to get IBY Defaults.','PAY_PROC_TRXN_TYPE_CODE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','PAY_PROC_TRXN_TYPE_CODE','Pay Proc Trxn Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PAYMENT_FUNCTION',200,'Function or purpose of the payment. Possible values are - PAYABLES_DISB, AR_CUSTOMER_REFUNDS, LOANS_PAYMENTS. Values are derived from FND_LOOKUP_VALUES for lookup_type IBY_PAYMENT_FUNCTIONS.','PAYMENT_FUNCTION','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','PAYMENT_FUNCTION','Payment Function','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','REQUESTER_ID',200,'Requester of invoice used by Invoice Workflow Approval process to generate list of approvers. Valid values from active HR employees. Validated against PER_ALL_PEOPLE_F.PERSON_ID.','REQUESTER_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','REQUESTER_ID','Requester Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','VALIDATION_REQUEST_ID',200,'Identifier for the Invoice Validation concurrent request that is currently processing the invoice. Foreign key to the REQUEST_ID column of the FND_CONCURRENT_REQUESTS table.','VALIDATION_REQUEST_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','VALIDATION_REQUEST_ID','Validation Request Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','VALIDATED_TAX_AMOUNT',200,'Total tax amount excluded from the discountable amount','VALIDATED_TAX_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','VALIDATED_TAX_AMOUNT','Validated Tax Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','CUST_REGISTRATION_CODE',200,'Customer legal registration code','CUST_REGISTRATION_CODE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','CUST_REGISTRATION_CODE','Cust Registration Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','CUST_REGISTRATION_NUMBER',200,'Customer legal registration number. This column corresponds to ''Customer Taxpayer ID'' of Invoice Workbench.','CUST_REGISTRATION_NUMBER','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','CUST_REGISTRATION_NUMBER','Cust Registration Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PORT_OF_ENTRY_CODE',200,'Customs location code.','PORT_OF_ENTRY_CODE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','PORT_OF_ENTRY_CODE','Port Of Entry Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','EXTERNAL_BANK_ACCOUNT_ID',200,'Unique internal identifier for the supplier bank account associated with Invoice. Foreign key to the IBY_EXT_BANK_ACCOUNTS table.','EXTERNAL_BANK_ACCOUNT_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','EXTERNAL_BANK_ACCOUNT_ID','External Bank Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','VENDOR_CONTACT_ID',200,'Foreign Key to PO_VENDOR_CONTACTS','VENDOR_CONTACT_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','VENDOR_CONTACT_ID','Vendor Contact Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','INTERNAL_CONTACT_EMAIL',200,'Email address of possible internal contact','INTERNAL_CONTACT_EMAIL','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','INTERNAL_CONTACT_EMAIL','Internal Contact Email','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','DISC_IS_INV_LESS_TAX_FLAG',200,'Flag that indicates if tax amount is excluded from the discountable amount when invoice amount applicable to discount is calculated (Y or N)','DISC_IS_INV_LESS_TAX_FLAG','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','DISC_IS_INV_LESS_TAX_FLAG','Disc Is Inv Less Tax Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','EXCLUDE_FREIGHT_FROM_DISCOUNT',200,'Flag that indicates whether the freight amount is excluded from the discountable amount when calculating the discount (Y or N)','EXCLUDE_FREIGHT_FROM_DISCOUNT','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','EXCLUDE_FREIGHT_FROM_DISCOUNT','Exclude Freight From Discount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','APPROVAL_READY_FLAG',200,'Indicates whether invoice is ready for the Invoice Approval Workflow process (Y or N)','APPROVAL_READY_FLAG','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','APPROVAL_READY_FLAG','Approval Ready Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','APPROVAL_ITERATION',200,'Count of times invoice has passed through the Invoice Approval Workflow process','APPROVAL_ITERATION','','','','ANONYMOUS','NUMBER','AP_INVOICES','APPROVAL_ITERATION','Approval Iteration','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','DOC_SEQUENCE_ID',200,'Unique Sequence identifier assigned to an invoice, based on the document category of the invoice. Foreign key to the DOC_SEQUENCE_ID of the AP_DOC_SEQUENCE_AUDIT table.','DOC_SEQUENCE_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','DOC_SEQUENCE_ID','Doc Sequence Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','DOC_SEQUENCE_VALUE',200,'Sequence name assigned to an invoice, based on the document category of the invoice. Foreign key to the DOC_SEQUENCE_VALUE of the AP_DOC_SEQUENCE_AUDIT table.','DOC_SEQUENCE_VALUE','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','DOC_SEQUENCE_VALUE','Doc Sequence Value','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','DOC_CATEGORY_CODE',200,'Document category for an invoice. Payables predefines a document category for each invoice type. If Sequential Numbering profile option is enabled, this document category defaults to an invoice during entry.','DOC_CATEGORY_CODE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','DOC_CATEGORY_CODE','Doc Category Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ATTRIBUTE7',200,'Descriptive flexfield segment','ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','ATTRIBUTE7','Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ATTRIBUTE9',200,'Descriptive flexfield segment','ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','ATTRIBUTE9','Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ATTRIBUTE10',200,'Descriptive flexfield segment','ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','ATTRIBUTE10','Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ATTRIBUTE12',200,'Descriptive flexfield segment','ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','ATTRIBUTE12','Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ATTRIBUTE13',200,'Descriptive flexfield segment','ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','ATTRIBUTE13','Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ATTRIBUTE_CATEGORY',200,'Descriptive flexfield structure definition column.','ATTRIBUTE_CATEGORY','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','ATTRIBUTE_CATEGORY','Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','APPROVAL_STATUS',200,'No Longer used','APPROVAL_STATUS','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','APPROVAL_STATUS','Approval Status','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','APPROVAL_DESCRIPTION',200,'Description of manual authorization invoice approval (used for reference purposes only).','APPROVAL_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','APPROVAL_DESCRIPTION','Approval Description','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','INVOICE_DISTRIBUTION_TOTAL',200,'No longer used','INVOICE_DISTRIBUTION_TOTAL','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','INVOICE_DISTRIBUTION_TOTAL','Invoice Distribution Total','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','POSTING_STATUS',200,'Status that indicates if invoice can be posted (either Available or N - column is populated, but not used)','POSTING_STATUS','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','POSTING_STATUS','Posting Status','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PREPAY_FLAG',200,'No longer used','PREPAY_FLAG','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','PREPAY_FLAG','Prepay Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','AUTHORIZED_BY',200,'No longer used.','AUTHORIZED_BY','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','AUTHORIZED_BY','Authorized By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','CANCELLED_DATE',200,'Date invoice cancelled','CANCELLED_DATE','','','','ANONYMOUS','DATE','AP_INVOICES','CANCELLED_DATE','Cancelled Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','CANCELLED_BY',200,'Unique internal identifier of user who cancelled an invoice. Foreign key to the USER_ID column of the FND_USER table.','CANCELLED_BY','','','','ANONYMOUS','NUMBER','AP_INVOICES','CANCELLED_BY','Cancelled By','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','CANCELLED_AMOUNT',200,'Original amount of cancelled invoice','CANCELLED_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','CANCELLED_AMOUNT','Cancelled Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','TEMP_CANCELLED_AMOUNT',200,'Column for recording the original amount of an invoice until cancellation completes successfully','TEMP_CANCELLED_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','TEMP_CANCELLED_AMOUNT','Temp Cancelled Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PROJECT_ACCOUNTING_CONTEXT',200,'No longer used','PROJECT_ACCOUNTING_CONTEXT','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','PROJECT_ACCOUNTING_CONTEXT','Project Accounting Context','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','USSGL_TRANSACTION_CODE',200,'Default transaction code for creating US Standard General Ledger journal entries (Oracle Public Sector Payables)','USSGL_TRANSACTION_CODE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','USSGL_TRANSACTION_CODE','Ussgl Transaction Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','USSGL_TRX_CODE_CONTEXT',200,'USSGL Transaction Code Descriptive Flexfield context column','USSGL_TRX_CODE_CONTEXT','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','USSGL_TRX_CODE_CONTEXT','Ussgl Trx Code Context','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PROJECT_ID',200,'Identifier for project used to build default Accounting Flexfield. Foreign key to the PROJECT_ID column of the PA_PROJECTS_ALL.','PROJECT_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','PROJECT_ID','Project Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','TASK_ID',200,'Identifier for project task used to build default Accounting Flexfield. Foreign key to the TASK_ID column of the TASK_ID.','TASK_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','TASK_ID','Task Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','EXPENDITURE_TYPE',200,'Project expenditure type used to build default Accounting Flexfield. Foreign key to the EXPENDITURE_TYPE column of the PA_EXPENDITURE_TYPES.','EXPENDITURE_TYPE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','EXPENDITURE_TYPE','Expenditure Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','EXPENDITURE_ITEM_DATE',200,'Project expenditure item date used to build default Accounting Flexfield','EXPENDITURE_ITEM_DATE','','','','ANONYMOUS','DATE','AP_INVOICES','EXPENDITURE_ITEM_DATE','Expenditure Item Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PA_QUANTITY',200,'Project item quantity used to build default Accounting Flexfield','PA_QUANTITY','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','PA_QUANTITY','Pa Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','EXPENDITURE_ORGANIZATION_ID',200,'Identifier for project organization used to build default Accounting Flexfield. Foreign key to the EXPENDITURE_ORGANIZATION_ID of the PA_EXPENDITURE_TYPES.','EXPENDITURE_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','EXPENDITURE_ORGANIZATION_ID','Expenditure Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PA_DEFAULT_DIST_CCID',200,'Identifier for project-related invoice default Accounting Flexfield (defaults to invoice distribution lines)','PA_DEFAULT_DIST_CCID','','','','ANONYMOUS','NUMBER','AP_INVOICES','PA_DEFAULT_DIST_CCID','Pa Default Dist Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','VENDOR_PREPAY_AMOUNT',200,'No longer used','VENDOR_PREPAY_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','VENDOR_PREPAY_AMOUNT','Vendor Prepay Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PAYMENT_AMOUNT_TOTAL',200,'Amount of invoice that has been paid','PAYMENT_AMOUNT_TOTAL','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','PAYMENT_AMOUNT_TOTAL','Payment Amount Total','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','AWT_FLAG',200,'Flag to indicate if Automatic Withholding Tax has been calculated automatically (A), or manually (M) ','AWT_FLAG','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','AWT_FLAG','Awt Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','AWT_GROUP_ID',200,'Withholding Tax group identifier at Invoice Validation time.','AWT_GROUP_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','AWT_GROUP_ID','Awt Group Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','REFERENCE_1',200,'Reference information from Oracle Projects expense reports','REFERENCE_1','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','REFERENCE_1','Reference 1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','REFERENCE_2',200,'Reference information from Oracle Projects expense reports','REFERENCE_2','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','REFERENCE_2','Reference 2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ORG_ID',200,'Unique internal identifier of the Operating Unit to which the invoice belongs.','ORG_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','ORG_ID','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PRE_WITHHOLDING_AMOUNT',200,'Reserved for future use','PRE_WITHHOLDING_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','PRE_WITHHOLDING_AMOUNT','Pre Withholding Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE_CATEGORY',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE_CATEGORY','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE_CATEGORY','Global Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE1',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE1','Global Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE2',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE2','Global Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE3',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE3','Global Attribute3','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE4',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE4','Global Attribute4','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE5',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE5','Global Attribute5','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE6',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE6','Global Attribute6','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE7',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE7','Global Attribute7','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE8',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE8','Global Attribute8','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE9',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE9','Global Attribute9','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE10',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE10','Global Attribute10','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE11',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE11','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE11','Global Attribute11','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE12',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE12','Global Attribute12','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE13',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE13','Global Attribute13','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE14',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE14','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE14','Global Attribute14','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE15',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE15','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE15','Global Attribute15','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE16',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE16','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE16','Global Attribute16','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE17',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE17','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE17','Global Attribute17','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE18',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE18','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE18','Global Attribute18','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE19',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE19','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE19','Global Attribute19','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GLOBAL_ATTRIBUTE20',200,'Reserved for country-specific functionality','GLOBAL_ATTRIBUTE20','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','GLOBAL_ATTRIBUTE20','Global Attribute20','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','AUTO_TAX_CALC_FLAG',200,'No Longer Used','AUTO_TAX_CALC_FLAG','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','AUTO_TAX_CALC_FLAG','Auto Tax Calc Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PAYMENT_CROSS_RATE_TYPE',200,'Cross currency payment rate type (only valid value in this release is EMU Fixed)','PAYMENT_CROSS_RATE_TYPE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','PAYMENT_CROSS_RATE_TYPE','Payment Cross Rate Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PAYMENT_CROSS_RATE_DATE',200,'Cross currency payment rate date','PAYMENT_CROSS_RATE_DATE','','','','ANONYMOUS','DATE','AP_INVOICES','PAYMENT_CROSS_RATE_DATE','Payment Cross Rate Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PAY_CURR_INVOICE_AMOUNT',200,'Invoice amount in the payment currency','PAY_CURR_INVOICE_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','PAY_CURR_INVOICE_AMOUNT','Pay Curr Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','MRC_BASE_AMOUNT',200,'No Longer used','MRC_BASE_AMOUNT','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','MRC_BASE_AMOUNT','Mrc Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','MRC_EXCHANGE_RATE',200,'No Longer used','MRC_EXCHANGE_RATE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','MRC_EXCHANGE_RATE','Mrc Exchange Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','MRC_EXCHANGE_RATE_TYPE',200,'No Longer used','MRC_EXCHANGE_RATE_TYPE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','MRC_EXCHANGE_RATE_TYPE','Mrc Exchange Rate Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','MRC_EXCHANGE_DATE',200,'No Longer used','MRC_EXCHANGE_DATE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','MRC_EXCHANGE_DATE','Mrc Exchange Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','GL_DATE',200,'Accounting date to default to invoice lines and distributions.','GL_DATE','','','','ANONYMOUS','DATE','AP_INVOICES','GL_DATE','Gl Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','AWARD_ID',200,'If Oracle Grants Accounting is installed, the award ID is used with data in project columns to track grant information','AWARD_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','AWARD_ID','Award Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','QUICK_CREDIT',200,'Indicates whether a Credit or Debit Memo was created as a Quick Credit (Y or N). Allowed to update only for Invoices of Credit or Debit Memo.','QUICK_CREDIT','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','QUICK_CREDIT','Quick Credit','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','CREDITED_INVOICE_ID',200,'Identifier for invoice to be fully reversed by a Credit or Debit Memo. This column corresponds to the ''Credited Invoice'' field of Invoice workbench. This is a mandatory if QUICK_CREDIT flag is set to ''Y''.','CREDITED_INVOICE_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','CREDITED_INVOICE_ID','Credited Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','DISTRIBUTION_SET_ID',200,'Distribution set identifier to be used to default on an invoice line and used in the generation of distributions for the line. Foreign key to the DISTRIBUTION_SET_ID of AP_DISTRIBUTION_SETS_ALL table.','DISTRIBUTION_SET_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','DISTRIBUTION_SET_ID','Distribution Set Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','APPLICATION_ID',200,'Unique internal application identifier. Foreign key to the APPLICATION_ID column of the FND_APPLICATION table.','APPLICATION_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','APPLICATION_ID','Application Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PRODUCT_TABLE',200,'Product source table name','PRODUCT_TABLE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','PRODUCT_TABLE','Product Table','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','REFERENCE_KEY1',200,'Primary key information that will uniquely identify a record in other products view','REFERENCE_KEY1','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','REFERENCE_KEY1','Reference Key1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','REFERENCE_KEY2',200,'Primary key information that will uniquely identify a record in other products view','REFERENCE_KEY2','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','REFERENCE_KEY2','Reference Key2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','REFERENCE_KEY3',200,'Primary key information that will uniquely identify a record in other products view','REFERENCE_KEY3','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','REFERENCE_KEY3','Reference Key3','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','REFERENCE_KEY4',200,'Primary key information that will uniquely identify a record in other products view','REFERENCE_KEY4','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','REFERENCE_KEY4','Reference Key4','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','REFERENCE_KEY5',200,'Primary key information that will uniquely identify a record in other products view','REFERENCE_KEY5','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','REFERENCE_KEY5','Reference Key5','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','TOTAL_TAX_AMOUNT',200,'The sum of the exclusive and inclusive tax amounts on an invoice.  The amount will be refreshed every time a change in the tax amounts happens (for example after recalculation).','TOTAL_TAX_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','TOTAL_TAX_AMOUNT','Total Tax Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','SELF_ASSESSED_TAX_AMOUNT',200,'Total self-assessed tax amount on the invoice.','SELF_ASSESSED_TAX_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','SELF_ASSESSED_TAX_AMOUNT','Self Assessed Tax Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','TAX_RELATED_INVOICE_ID',200,'Invoice Id from AP_INVOICES_ALL, used to link two invoices that were split by the supplier such as for shipping reasons, but that should be treated as one for tax purposes.','TAX_RELATED_INVOICE_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','TAX_RELATED_INVOICE_ID','Tax Related Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','TRX_BUSINESS_CATEGORY',200,'Transaction Business Category used to identify and classify your business transactions for tax calculation purpose. This column corresponds to ''Business Category'' field of Invoice Workbench.','TRX_BUSINESS_CATEGORY','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','TRX_BUSINESS_CATEGORY','Trx Business Category','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','USER_DEFINED_FISC_CLASS',200,'User Defined Fiscal Classification used to to classify any tax requirement that you cannot define using the seeded fiscal classification types. This column corresponds to ''Fiscal Classification'' of Invoice Workbench.','USER_DEFINED_FISC_CLASS','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','USER_DEFINED_FISC_CLASS','User Defined Fisc Class','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','TAXATION_COUNTRY',200,'The country in which the transaction is deemed to have taken place for taxation purposes. E-Business Tax will set the context of other tax drivers such as Product Fiscal Classification based on the value of this field.','TAXATION_COUNTRY','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','TAXATION_COUNTRY','Taxation Country','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','DOCUMENT_SUB_TYPE',200,'Document sub type is used for documentation classification within the application event class of the transaction. Used for certain countries, where tax or governmental authority defines and classifies document types for reporting purposes. ','DOCUMENT_SUB_TYPE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','DOCUMENT_SUB_TYPE','Document Sub Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','SUPPLIER_TAX_INVOICE_NUMBER',200,'The invoice number of the tax invoice issued by supplier. This field is a reporting tax driver used to satisfy reporting requirements for certain tax authorities like Thailand. This column replaces an existing GDFF.','SUPPLIER_TAX_INVOICE_NUMBER','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','SUPPLIER_TAX_INVOICE_NUMBER','Supplier Tax Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','SUPPLIER_TAX_INVOICE_DATE',200,'The date on the tax invoice issued by supplier. This field is a reporting tax driver for certain countries. This column replaces an existing GDFF.','SUPPLIER_TAX_INVOICE_DATE','','','','ANONYMOUS','DATE','AP_INVOICES','SUPPLIER_TAX_INVOICE_DATE','Supplier Tax Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PAY_AWT_GROUP_ID',200,'Withholding Tax group identifier at Payment time.','PAY_AWT_GROUP_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','PAY_AWT_GROUP_ID','Pay Awt Group Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ORIGINAL_INVOICE_AMOUNT',200,'Original Invoice Amount captured in case of a Dispute.','ORIGINAL_INVOICE_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','ORIGINAL_INVOICE_AMOUNT','Original Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','DISPUTE_REASON',200,'Dispute Reason','DISPUTE_REASON','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','DISPUTE_REASON','Dispute Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','REMIT_TO_SUPPLIER_NAME',200,'Name of the supplier to whom invoice amount can be remitted.','REMIT_TO_SUPPLIER_NAME','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','REMIT_TO_SUPPLIER_NAME','Remit To Supplier Name','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','REMIT_TO_SUPPLIER_ID',200,'Unique supplier identifier to whom invoice amount can be remitted.','REMIT_TO_SUPPLIER_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','REMIT_TO_SUPPLIER_ID','Remit To Supplier Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','REMIT_TO_SUPPLIER_SITE',200,'Name of the supplier site to whom invoice amount can be remitted.','REMIT_TO_SUPPLIER_SITE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','REMIT_TO_SUPPLIER_SITE','Remit To Supplier Site','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','REMIT_TO_SUPPLIER_SITE_ID',200,'Unique supplier site identifier to whom invoice amount can be remitted.','REMIT_TO_SUPPLIER_SITE_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','REMIT_TO_SUPPLIER_SITE_ID','Remit To Supplier Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','RELATIONSHIP_ID',200,'Unique identifier to third party relationships of a supplier with another supplier. This column will be populated if the payment is made to the third party.','RELATIONSHIP_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','RELATIONSHIP_ID','Relationship Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','TAX_INVOICE_INTERNAL_SEQ',200,'The internal sequential number that the deploying company uses to identify the supplier tax invoice when it receives it.  The column replaces an existing GDFF.','TAX_INVOICE_INTERNAL_SEQ','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','TAX_INVOICE_INTERNAL_SEQ','Tax Invoice Internal Seq','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','LEGAL_ENTITY_ID',200,'Legal Entity Id','LEGAL_ENTITY_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','LEGAL_ENTITY_ID','Legal Entity Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','HISTORICAL_FLAG',200,'Flag to represent whether the document / invoice has been upgraded from a prior release or whether it has been newly created.','HISTORICAL_FLAG','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','HISTORICAL_FLAG','Historical Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','FORCE_REVALIDATION_FLAG',200,'Force Revalidation Flag','FORCE_REVALIDATION_FLAG','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','FORCE_REVALIDATION_FLAG','Force Revalidation Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','BANK_CHARGE_BEARER',200,'Bearer of bank charge cost. Bank charge bearers are defined as the lookup IBY_BANK_CHARGE_BEARER','BANK_CHARGE_BEARER','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','BANK_CHARGE_BEARER','Bank Charge Bearer','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','REMITTANCE_MESSAGE1',200,'Remittance message for use in payment processing','REMITTANCE_MESSAGE1','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','REMITTANCE_MESSAGE1','Remittance Message1','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','REMITTANCE_MESSAGE2',200,'Remittance message for use in payment processing','REMITTANCE_MESSAGE2','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','REMITTANCE_MESSAGE2','Remittance Message2','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','REMITTANCE_MESSAGE3',200,'Remittance message for use in payment processing','REMITTANCE_MESSAGE3','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','REMITTANCE_MESSAGE3','Remittance Message3','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','UNIQUE_REMITTANCE_IDENTIFIER',200,'Unique remittance identifier provided by the payee','UNIQUE_REMITTANCE_IDENTIFIER','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','UNIQUE_REMITTANCE_IDENTIFIER','Unique Remittance Identifier','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','SUPPLIER_TAX_EXCHANGE_RATE',200,'The external exchange rate used to calculate the amount of the supplier-issued tax invoice in foreign currency. This column replaces an existing GDFF.','SUPPLIER_TAX_EXCHANGE_RATE','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','SUPPLIER_TAX_EXCHANGE_RATE','Supplier Tax Exchange Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','TAX_INVOICE_RECORDING_DATE',200,'The date the deploying company recorded the supplier-issued tax invoice in its internal records. This column is used to satisfy reporting requirements in certain countries.','TAX_INVOICE_RECORDING_DATE','','','','ANONYMOUS','DATE','AP_INVOICES','TAX_INVOICE_RECORDING_DATE','Tax Invoice Recording Date','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','URI_CHECK_DIGIT',200,'Unique remittance identifier check digit','URI_CHECK_DIGIT','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','URI_CHECK_DIGIT','Uri Check Digit','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','SETTLEMENT_PRIORITY',200,'The priority with which the financial institution or payment system should settle payment for this document. The available values for this column come from the FND lookup IBY_SETTLEMENT_PRIORITY','SETTLEMENT_PRIORITY','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','SETTLEMENT_PRIORITY','Settlement Priority','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','WFAPPROVAL_STATUS',200,'Status of invoice in the Invoice Approval Workflow process. Possible values for this column are derived from FND_LOOKUP_VALUES for lookup_type ''AP_WFAPPROVAL_STATUS''.','WFAPPROVAL_STATUS','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','WFAPPROVAL_STATUS','Wfapproval Status','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PAYMENT_REASON_CODE',200,'Payment reason code','PAYMENT_REASON_CODE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','PAYMENT_REASON_CODE','Payment Reason Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PAYMENT_REASON_COMMENTS',200,'Free text field available for entering a reason for the payment','PAYMENT_REASON_COMMENTS','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','PAYMENT_REASON_COMMENTS','Payment Reason Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PAYMENT_METHOD_CODE',200,'Name of the payment method used to pay the supplier site. Foreign key from the table IBY_PAYMENT_METHODS_B.','PAYMENT_METHOD_CODE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','PAYMENT_METHOD_CODE','Payment Method Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','DELIVERY_CHANNEL_CODE',200,'Delivery channel code','DELIVERY_CHANNEL_CODE','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','DELIVERY_CHANNEL_CODE','Delivery Channel Code','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','QUICK_PO_HEADER_ID',200,'PO Number ID is used for QuickMatch and to default values from the purchase order to the invoice header.','QUICK_PO_HEADER_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','QUICK_PO_HEADER_ID','Quick Po Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','NET_OF_RETAINAGE_FLAG',200,'Flag to indicate invoice amount is net of retainage','NET_OF_RETAINAGE_FLAG','','','','ANONYMOUS','VARCHAR2','AP_INVOICES','NET_OF_RETAINAGE_FLAG','Net Of Retainage Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','RELEASE_AMOUNT_NET_OF_TAX',200,'Invoice amount net of tax for retainage release invoices','RELEASE_AMOUNT_NET_OF_TAX','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','RELEASE_AMOUNT_NET_OF_TAX','Release Amount Net Of Tax','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','CONTROL_AMOUNT',200,'User-enterable value to ensure that the calculated tax will be the same as on the physical document.','CONTROL_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','AP_INVOICES','CONTROL_AMOUNT','Control Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','PARTY_ID',200,'Party identifier','PARTY_ID','','','','ANONYMOUS','NUMBER','AP_INVOICES','PARTY_ID','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ATTRIBUTE1',200,'DFF Context Code - 163','ATTRIBUTE1','','','','ANONYMOUS','','AP_INVOICES','ATTRIBUTE1','BizTalk ID-Frt Biztalk','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ATTRIBUTE11',200,'DFF Context Code - 163','ATTRIBUTE11','','','','ANONYMOUS','','AP_INVOICES','ATTRIBUTE11','Invoice Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ATTRIBUTE14',200,'DFF Context Code - 163','ATTRIBUTE14','','','','ANONYMOUS','','AP_INVOICES','ATTRIBUTE14','DCTM Image Link','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ATTRIBUTE15',200,'DFF Context Code - 163','ATTRIBUTE15','','','','ANONYMOUS','','AP_INVOICES','ATTRIBUTE15','R11 Invoice ID','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ATTRIBUTE2',200,'DFF Context Code - 162','ATTRIBUTE2','','','','ANONYMOUS','','AP_INVOICES','ATTRIBUTE2','Documentum PO Number','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ATTRIBUTE3',200,'DFF Context Code - 166','ATTRIBUTE3','','','','ANONYMOUS','','AP_INVOICES','ATTRIBUTE3','Employee','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ATTRIBUTE4',200,'DFF Context Code - 166','ATTRIBUTE4','','','','ANONYMOUS','','AP_INVOICES','ATTRIBUTE4','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ATTRIBUTE5',200,'DFF Context Code - 167','ATTRIBUTE5','','','','ANONYMOUS','','AP_INVOICES','ATTRIBUTE5','Canada Tax Type','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ATTRIBUTE6',200,'DFF Context Code - 163','ATTRIBUTE6','','','','ANONYMOUS','','AP_INVOICES','ATTRIBUTE6','BOL','','','','US');
xxeis.eis_rsc_ins.vc( 'AP_INVOICES','ATTRIBUTE8',200,'DFF Context Code - 163','ATTRIBUTE8','','','','ANONYMOUS','','AP_INVOICES','ATTRIBUTE8','POS System Code-Frt Biztalk','','','','US');
--Inserting Object Components for AP_INVOICES
--Inserting Object Component Joins for AP_INVOICES
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report LOV Data for HDS Payment Register - Details for PWC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS Payment Register - Details for PWC
xxeis.eis_rsc_ins.lov( 200,'select distinct CHECKRUN_NAME from AP_CHECKS','','EIS_AP_CHECKRUN_NAME_LOV','List of Values for Checkrun Name','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 200,'select CHECK_NUMBER from AP_CHECKS','','EIS_AP_CHECK_NUMBER_LOV','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 200,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 200,'SELECT lookup_code,displayed_field
FROM AP_LOOKUP_CODES
where lookup_type = ''PAYMENT TYPE''','','EIS AP Payment Code','This lov lists the different payment types','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report Data for HDS Payment Register - Details for PWC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS Payment Register - Details for PWC
xxeis.eis_rsc_utility.delete_report_rows( 'HDS Payment Register - Details for PWC' );
--Inserting Report - HDS Payment Register - Details for PWC
xxeis.eis_rsc_ins.r( 200,'HDS Payment Register - Details for PWC','','','','','','XXEIS_RS_ADMIN','EIS_AP_PAYMENT_REGISTER_HDS','Y','','','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - HDS Payment Register - Details for PWC
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'SEGMENT1','Segment1','Supplier number','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','AP_SUPPLIERS','PV','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'VENDOR_NAME','Vendor Name','Supplier name','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','AP_SUPPLIERS','PV','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'VENDOR_SITE_CODE','Vendor Site Code','','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','AP_SUPPLIER_SITES_ALL','PVS','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'ADDRESS_LINE1','Address Line1','Address Line1','','','','','31','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'ADDRESS_LINE2','Address Line2','Address Line2','','','','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'AMOUNT_PAID','Amount Paid','Amount Paid','','','','','32','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'APPROVAL_DESCRIPTION','Approval Description','Approval Description','','','','','34','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'APPROVAL_STATUS','Approval Status','Approval Status','','','','','36','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'BANK_ACCOUNT_NUM','Bank Account Num','Bank Account Num','','','','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'CHECKRUN_NAME','Checkrun Name','Checkrun Name','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'CHECK_AMOUNT','Check Amount','Check Amount','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'CHECK_CREATED_BY','Check Created By','Check Created By','','','','','37','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'CHECK_NUMBER','Check Number','Check Number','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'CITY','City','City','','','','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'CLEARED_AMOUNT','Cleared Amount','Cleared Amount','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'CLEARED_DATE','Cleared Date','Cleared Date','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'COUNTRY','Country','Country','','','','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'DISCOUNT_LOST','Discount Lost','Discount Lost','','','','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'DISCOUNT_TAKEN','Discount Taken','Discount Taken','','','','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'DISTRIBUTION_AMOUNT','Distribution Amount','Distribution Amount','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'DISTRIBUTION_LINE_NUMBER','Distribution Line Number','Distribution Line Number','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'GOODS_RECEIVED_DATE','Goods Received Date','Goods Received Date','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'INVOICE_DATE','Invoice Date','Invoice Date','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'INVOICE_DESCRIPTION','Invoice Description','Invoice Description','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'INVOICE_RECEIVED_DATE','Invoice Received Date','Invoice Received Date','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'PAYMENT_BASE_AMOUNT','Payment Base Amount','Payment Base Amount','','','','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'PAYMENT_DATE','Payment Date','Payment Date','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'PAYMENT_NUM','Payment Num','Payment Num','','','','','33','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'PAYMENT_TYPE','Payment Type','Payment Type','','','','','35','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'PAY_GROUP_LOOKUP_CODE','Pay Group Lookup Code','Pay Group Lookup Code','','','','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'PERIOD_NAME','Period Name','Period Name','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'POSTED_FLAG','Posted Flag','Posted Flag','','','','','29','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'PO_DATE','Po Date','Po Date','','','','','38','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'PO_STATUS','Po Status','Po Status','','','','','39','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'STATE','State','State','','','','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'VOID_DATE','Void Date','Void Date','','','','','30','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'ZIP','Zip','Zip','','','','','28','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details for PWC',200,'INVOICE_AMOUNT','Invoice Amount','Total amount of the invoice. This amount can be negative for following types of invoices: Credit Memo, Debit Memo, Mixed, Automatic Withholding, Expense Reports','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REGISTER_HDS','AP_INVOICES','API','SUM','US','');
--Inserting Report Parameters - HDS Payment Register - Details for PWC
xxeis.eis_rsc_ins.rp( 'HDS Payment Register - Details for PWC',200,'Payment Date From','Payment Date From','PAYMENT_DATE','>=','','','DATE','N','Y','1','N','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','','EIS_AP_PAYMENT_REGISTER_HDS','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payment Register - Details for PWC',200,'Payment Date To','Payment Date To','PAYMENT_DATE','<=','','','DATE','N','Y','2','N','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','','EIS_AP_PAYMENT_REGISTER_HDS','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payment Register - Details for PWC',200,'Check Number','Check Number','CHECK_NUMBER','IN','EIS_AP_CHECK_NUMBER_LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AP_PAYMENT_REGISTER_HDS','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payment Register - Details for PWC',200,'Checkrun Name','Checkrun Name','CHECKRUN_NAME','IN','EIS_AP_CHECKRUN_NAME_LOV','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AP_PAYMENT_REGISTER_HDS','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payment Register - Details for PWC',200,'Payment Type','Payment Type','PAYMENT_TYPE','IN','EIS AP Payment Code','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AP_PAYMENT_REGISTER_HDS','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payment Register - Details for PWC',200,'Operating Unit','Operating Unit','OPERATING_UNIT','IN','EIS_MULTI_OPERATING_UNIT_LOV','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AP_PAYMENT_REGISTER_HDS','','','US','');
--Inserting Dependent Parameters - HDS Payment Register - Details for PWC
--Inserting Report Conditions - HDS Payment Register - Details for PWC
xxeis.eis_rsc_ins.rcnh( 'HDS Payment Register - Details for PWC',200,'CHECKRUN_NAME IN :Checkrun Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECKRUN_NAME','','Checkrun Name','','','','','EIS_AP_PAYMENT_REGISTER_HDS','','','','','','IN','Y','Y','','','','','1',200,'HDS Payment Register - Details for PWC','CHECKRUN_NAME IN :Checkrun Name ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payment Register - Details for PWC',200,'CHECK_NUMBER IN :Check Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECK_NUMBER','','Check Number','','','','','EIS_AP_PAYMENT_REGISTER_HDS','','','','','','IN','Y','Y','','','','','1',200,'HDS Payment Register - Details for PWC','CHECK_NUMBER IN :Check Number ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payment Register - Details for PWC',200,'OPERATING_UNIT IN :Operating Unit ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','OPERATING_UNIT','','Operating Unit','','','','','EIS_AP_PAYMENT_REGISTER_HDS','','','','','','IN','Y','Y','','','','','1',200,'HDS Payment Register - Details for PWC','OPERATING_UNIT IN :Operating Unit ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payment Register - Details for PWC',200,'TRUNC(EAPRH.PAYMENT_DATE) >= :Payment Date From ','ADVANCED','','  1#$# ','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Payment Date From','','','','','EIS_AP_PAYMENT_REGISTER_HDS','','','','','','GREATER_THAN_EQUALS','Y','Y','TRUNC(EAPRH.PAYMENT_DATE)','','','','1',200,'HDS Payment Register - Details for PWC','TRUNC(EAPRH.PAYMENT_DATE) >= :Payment Date From ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payment Register - Details for PWC',200,'PAYMENT_TYPE IN :Payment Type ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PAYMENT_TYPE','','Payment Type','','','','','EIS_AP_PAYMENT_REGISTER_HDS','','','','','','IN','Y','Y','','','','','1',200,'HDS Payment Register - Details for PWC','PAYMENT_TYPE IN :Payment Type ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payment Register - Details for PWC',200,'TRUNC(EAPRH.PAYMENT_DATE) <= :Payment Date To ','ADVANCED','','  1#$# ','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','Payment Date To','','','','','EIS_AP_PAYMENT_REGISTER_HDS','','','','','','LESS_THAN_EQUALS','Y','Y','TRUNC(EAPRH.PAYMENT_DATE)','','','','1',200,'HDS Payment Register - Details for PWC','TRUNC(EAPRH.PAYMENT_DATE) <= :Payment Date To ');
--Inserting Report Sorts - HDS Payment Register - Details for PWC
--Inserting Report Triggers - HDS Payment Register - Details for PWC
--inserting report templates - HDS Payment Register - Details for PWC
--Inserting Report Portals - HDS Payment Register - Details for PWC
--inserting report dashboards - HDS Payment Register - Details for PWC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS Payment Register - Details for PWC','200','EIS_AP_PAYMENT_REGISTER_HDS','EIS_AP_PAYMENT_REGISTER_HDS','N','');
xxeis.eis_rsc_ins.rviews( 'HDS Payment Register - Details for PWC','200','EIS_AP_PAYMENT_REGISTER_HDS','AP_SUPPLIERS','N','PV');
xxeis.eis_rsc_ins.rviews( 'HDS Payment Register - Details for PWC','200','EIS_AP_PAYMENT_REGISTER_HDS','AP_SUPPLIER_SITES_ALL','N','PVS');
xxeis.eis_rsc_ins.rviews( 'HDS Payment Register - Details for PWC','200','EIS_AP_PAYMENT_REGISTER_HDS','AP_INVOICES','N','API');
--inserting report security - HDS Payment Register - Details for PWC
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','HDS_OIE_USER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','HDS_AP_ADMIN_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','HDS_AP_DISBURSEMTS_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','HDS_AP_INQUIRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','HDS_AP_MGR_NOSUP_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','HDS_AP_TRNS_ENTRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','HDS_AP_ADMIN_US_GSCIWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','PAYABLES_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','XXWC_PAY_NO_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','XXWC_PAY_W_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','XXWC_PAY_DISBURSE',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','XXWC_PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','XXWC_PAY_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','XXWC_PAY_VENDOR_MSTR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','HDS_AP_MGR_NOSUP_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','HDS_AP_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','HDS_PYABLS_MNGR_CAN',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','HDS_PYBLS_MNGR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','20005','','XXWC_VIEW_ALL_EIS_REPORTS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','HDS_AP_MGR_NOSUP_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','HDS_AP_DISBURSEMTS_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','HDS_AP_ADMIN_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','HDS_PAYABLES_CLOSE_GSC_GLBL',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details for PWC','200','','HDS_PAYABLES_CLOSE_GLBL',200,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS Payment Register - Details for PWC
--Inserting Report   Version details- HDS Payment Register - Details for PWC
xxeis.eis_rsc_ins.rv( 'HDS Payment Register - Details for PWC','','HDS Payment Register - Details for PWC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
