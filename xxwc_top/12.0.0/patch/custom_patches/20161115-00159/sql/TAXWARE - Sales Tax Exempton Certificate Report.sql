--Report Name            : TAXWARE - Sales Tax Exempton Certificate Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_36460_WZVDQT_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_36460_WZVDQT_V
xxeis.eis_rsc_ins.v( 'XXEIS_36460_WZVDQT_V',222,'Paste SQL View for TAXWARE - Sales Tax Exempton Certificate Report','1.0','','','CF029750','APPS','TAXWARE - Sales Tax Exempton Certificate Report View','X3WV','','Y','VIEW','US','','');
--Delete Object Columns for XXEIS_36460_WZVDQT_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_36460_WZVDQT_V',222,FALSE);
--Inserting Object Columns for XXEIS_36460_WZVDQT_V
xxeis.eis_rsc_ins.vc( 'XXEIS_36460_WZVDQT_V','K1',222,'','','','','','CF029750','VARCHAR2','','','K1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_36460_WZVDQT_V','K2',222,'','','','','','CF029750','VARCHAR2','','','K2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_36460_WZVDQT_V','ACTIVE_INACTIVE',222,'','','','','','CF029750','VARCHAR2','','','Active Inactive','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_36460_WZVDQT_V','CERTIFICATE_NUMBER',222,'','','','','','CF029750','VARCHAR2','','','Certificate Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_36460_WZVDQT_V','CUSTOMER_ADDRESS',222,'','','','','','CF029750','VARCHAR2','','','Customer Address','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_36460_WZVDQT_V','CUSTOMER_CITY',222,'','','','','','CF029750','VARCHAR2','','','Customer City','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_36460_WZVDQT_V','CUSTOMER_NAME',222,'','','','','','CF029750','VARCHAR2','','','Customer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_36460_WZVDQT_V','CUSTOMER_STATE',222,'','','','','','CF029750','VARCHAR2','','','Customer State','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_36460_WZVDQT_V','CUSTOMER_ZIPCODE',222,'','','','','','CF029750','VARCHAR2','','','Customer Zipcode','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_36460_WZVDQT_V','EFFECTIVE_DATE_FROM',222,'','','','','','CF029750','DATE','','','Effective Date From','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_36460_WZVDQT_V','EFFECTIVE_DATE_TO',222,'','','','','','CF029750','DATE','','','Effective Date To','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_36460_WZVDQT_V','JURISDICTION_LEVEL',222,'','','','','','CF029750','VARCHAR2','','','Jurisdiction Level','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_36460_WZVDQT_V','OCCUR_NUM',222,'','','','','','CF029750','NUMBER','','','Occur Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_36460_WZVDQT_V','REASON',222,'','','','','','CF029750','VARCHAR2','','','Reason','','','','US');
--Inserting Object Components for XXEIS_36460_WZVDQT_V
--Inserting Object Component Joins for XXEIS_36460_WZVDQT_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report LOV Data for TAXWARE - Sales Tax Exempton Certificate Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - TAXWARE - Sales Tax Exempton Certificate Report
xxeis.eis_rsc_ins.lov( '','select stalpha State from taxware.TAXSTINFO','','Taxware_States','Taxware States','CF029750',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report Data for TAXWARE - Sales Tax Exempton Certificate Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - TAXWARE - Sales Tax Exempton Certificate Report
xxeis.eis_rsc_utility.delete_report_rows( 'TAXWARE - Sales Tax Exempton Certificate Report' );
--Inserting Report - TAXWARE - Sales Tax Exempton Certificate Report
xxeis.eis_rsc_ins.r( 222,'TAXWARE - Sales Tax Exempton Certificate Report','','Sales Tax Exempton Certificate Report','','','','DM027741','XXEIS_36460_WZVDQT_V','Y','','select 
a.key_1  K1, 
a.key_2 K2,
a.cjurislevel  Jurisdiction_Level, 
a.skeyoccurnum OCCUR_Num, 
a.scustomername Customer_Name, 
a.scustomeraddr1 Customer_Address,
a.scustomerctyname Customer_City  ,
a.scustomerstatecode Customer_State, 
a.scustomerzipcode Customer_ZipCode,
a.staxcertifnum Certificate_Number, 
a.sreasoncode Reason, 
a.dateeffective Effective_Date_From, 
a.dateexpiration Effective_Date_To, 
a.cactivecompflag Active_Inactive
from taxware.steptec_tbl a
','DM027741','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','APPS','US','','','','');
--Inserting Report Columns - TAXWARE - Sales Tax Exempton Certificate Report
xxeis.eis_rsc_ins.rc( 'TAXWARE - Sales Tax Exempton Certificate Report',222,'JURISDICTION_LEVEL','Jurisdiction Level','','','','default','','1','N','Y','','','','','','','DM027741','N','N','','XXEIS_36460_WZVDQT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'TAXWARE - Sales Tax Exempton Certificate Report',222,'OCCUR_NUM','Occur Num','','','~~~','default','','2','N','Y','','','','','','','DM027741','N','N','','XXEIS_36460_WZVDQT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'TAXWARE - Sales Tax Exempton Certificate Report',222,'CUSTOMER_NAME','Customer Name','','','','default','','3','N','Y','','','','','','','DM027741','N','N','','XXEIS_36460_WZVDQT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'TAXWARE - Sales Tax Exempton Certificate Report',222,'CUSTOMER_ADDRESS','Customer Address','','','','default','','4','N','Y','','','','','','','DM027741','N','N','','XXEIS_36460_WZVDQT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'TAXWARE - Sales Tax Exempton Certificate Report',222,'CUSTOMER_CITY','Customer City','','','','default','','5','N','Y','','','','','','','DM027741','N','N','','XXEIS_36460_WZVDQT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'TAXWARE - Sales Tax Exempton Certificate Report',222,'CUSTOMER_STATE','Customer State','','','','default','','6','N','Y','','','','','','','DM027741','N','N','','XXEIS_36460_WZVDQT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'TAXWARE - Sales Tax Exempton Certificate Report',222,'CERTIFICATE_NUMBER','Certificate Number','','','','default','','12','N','Y','','','','','','','DM027741','N','N','','XXEIS_36460_WZVDQT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'TAXWARE - Sales Tax Exempton Certificate Report',222,'EFFECTIVE_DATE_FROM','Effective Date From','','','','default','','8','N','Y','','','','','','','DM027741','N','N','','XXEIS_36460_WZVDQT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'TAXWARE - Sales Tax Exempton Certificate Report',222,'EFFECTIVE_DATE_TO','Effective Date To','','','','default','','9','N','Y','','','','','','','DM027741','N','N','','XXEIS_36460_WZVDQT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'TAXWARE - Sales Tax Exempton Certificate Report',222,'ACTIVE_INACTIVE','Active Inactive','','','','default','','10','N','Y','','','','','','','DM027741','N','N','','XXEIS_36460_WZVDQT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'TAXWARE - Sales Tax Exempton Certificate Report',222,'K1','K1','','','','default','','11','N','Y','','','','','','','DM027741','N','N','','XXEIS_36460_WZVDQT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'TAXWARE - Sales Tax Exempton Certificate Report',222,'K2','K2','','','','default','','7','N','Y','','','','','','','DM027741','N','N','','XXEIS_36460_WZVDQT_V','','','','US','');
--Inserting Report Parameters - TAXWARE - Sales Tax Exempton Certificate Report
xxeis.eis_rsc_ins.rp( 'TAXWARE - Sales Tax Exempton Certificate Report',222,'Customer State','','CUSTOMER_STATE','IN','Taxware_States','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','DM027741','Y','N','','','','XXEIS_36460_WZVDQT_V','','','US','');
--Inserting Dependent Parameters - TAXWARE - Sales Tax Exempton Certificate Report
--Inserting Report Conditions - TAXWARE - Sales Tax Exempton Certificate Report
xxeis.eis_rsc_ins.rcnh( 'TAXWARE - Sales Tax Exempton Certificate Report',222,'X3WV.CUSTOMER_STATE IN Customer State','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_STATE','','Customer State','','','','','XXEIS_36460_WZVDQT_V','','','','','','IN','Y','Y','','','','','1',222,'TAXWARE - Sales Tax Exempton Certificate Report','X3WV.CUSTOMER_STATE IN Customer State');
--Inserting Report Sorts - TAXWARE - Sales Tax Exempton Certificate Report
--Inserting Report Triggers - TAXWARE - Sales Tax Exempton Certificate Report
--inserting report templates - TAXWARE - Sales Tax Exempton Certificate Report
--Inserting Report Portals - TAXWARE - Sales Tax Exempton Certificate Report
--inserting report dashboards - TAXWARE - Sales Tax Exempton Certificate Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'TAXWARE - Sales Tax Exempton Certificate Report','222','XXEIS_36460_WZVDQT_V','XXEIS_36460_WZVDQT_V','N','');
--inserting report security - TAXWARE - Sales Tax Exempton Certificate Report
xxeis.eis_rsc_ins.rsec( 'TAXWARE - Sales Tax Exempton Certificate Report','222','','XXWC_CRE_ASSOC_CASH_APP_MGR',222,'DM027741','','','');
xxeis.eis_rsc_ins.rsec( 'TAXWARE - Sales Tax Exempton Certificate Report','222','','XXWC_RECEIVABLES_INQUIRY_WC',222,'DM027741','','','');
--Inserting Report Pivots - TAXWARE - Sales Tax Exempton Certificate Report
--Inserting Report   Version details- TAXWARE - Sales Tax Exempton Certificate Report
xxeis.eis_rsc_ins.rv( 'TAXWARE - Sales Tax Exempton Certificate Report','','TAXWARE - Sales Tax Exempton Certificate Report','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
