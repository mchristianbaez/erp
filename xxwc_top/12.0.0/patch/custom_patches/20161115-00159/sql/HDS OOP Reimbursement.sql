--Report Name            : HDS OOP Reimbursement
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_1941495_YZMZFC_V
prompt Table type Objects cannot be imported
set scan off define off
prompt Creating Object APPS.XXEIS_1941495_YZMZFC_V
set scan off define off
DECLARE
mod_exist varchar2(1);
stmt CLOB;
v_objlength INTEGER := 0;
v_offset INTEGER := 1;
v_sql_long DBMS_SQL.varchar2s;
v_sql_long_count INTEGER := 1;
c NUMBER;
r NUMBER;
l_stmt varchar2(32000);
l_view_cur pls_integer;
l_view_rows NUMBER;
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
dbms_lob.createtemporary(stmt,TRUE);
dbms_lob.write( lob_loc   => stmt,amount    => length('create or replace view APPS.XXEIS_1941495_YZMZFC_V
 ("EMPLOYEE_ID","EMPLOYEE_NUM","NAME","VENDOR_ID","VENDOR_NUM","CHECK_NUMBER","REIMBURSEMENT_DATE","REIMBURSEMENT_AMOUNT","REIMBURSEMENT_AMOUNT_NUM","REIMBURSEMENT_CURRENCY","NUMBER_OF_DAYS","CHECK_ID"
) as '),offset    => 1, BUFFER    => 'create or replace view APPS.XXEIS_1941495_YZMZFC_V
 ("EMPLOYEE_ID","EMPLOYEE_NUM","NAME","VENDOR_ID","VENDOR_NUM","CHECK_NUMBER","REIMBURSEMENT_DATE","REIMBURSEMENT_AMOUNT","REIMBURSEMENT_AMOUNT_NUM","REIMBURSEMENT_CURRENCY","NUMBER_OF_DAYS","CHECK_ID"
) as ');
l_stmt :=  'SELECT     pv.employee_id employee_id,
                emp.employee_num,
                EMP.FULL_NAME NAME,
                AI.vendor_id vendor_id,
                pv.segment1 vendor_num,
                AC.check_number CHECK_NUMBER,
                trunc(AC.check_date) REIMBURSEMENT_DATE,
                to_char(AC.AMOUNT,
                        fnd_currency.get_format_mask(AC.currency_code, 30)) REIMBURSEMENT_AMOUNT,
                AC.AMOUNT REIMBURSEMENT_AMOUNT_NUM,
                AC.CURRENCY_CODE REIMBURSEMENT_CURRENCY,
                to_number(Trunc(SYSDATE - AC.check_date)) number_of_days,
                AC.check_id check_id
  FROM ap.AP_INVOICES_all          AI,
       ap.AP_INVOICE_PAYMENTS_all  AIP,
       ap.ap_suppliers           PV,
       ap.AP_CHECKS_all            AC,
       ap.AP_PAYMENT_SCHEDULES_all APS,
       apps.PER_EMPLOYEES_X      EMP
 WHERE AI.vendor_id = PV.vendor_id
   AND AIP.CHECK_ID = AC.CHECK_ID
   AND AIP.INVOICE_ID = AI.INVOICE_ID
   AND AI.invoice_id = APS.invoice_id
   AND emp.employee_id = pv.employee_id
   AND AC.check_id = AIP.CHECK_ID
   AND AI.INVOICE_TYPE_LOOKUP_CODE <> ''PREPAYMENT''
UNION ALL
SELECT DISTINCT cwk.person_id employee_id,
                NULL,
                CWK.FULL_NAME NAME,
                AI.vendor_id vendor_id,
                NULL,
                AC.check_number CHECK_NUMBER,
                AC.check_date REIMBURSEMENT_DATE,
                to_char(AC.AMOUNT,
                        fnd_currency.get_format_mask(AC.currency_code, 30)) REIMBURSEMENT_AMOUNT,
                AC.AMOUNT REIMBURSEMENT_AMOUNT_NUM,
                AC.CURRENCY_CODE REIMBURSEMENT_CURRENCY,
                to_number(Trunc(SYSDATE - AC.check_date)) number_of_days,
                AC.check_id check_id
  FROM AP_INVOICES                AI,
       AP_INVOICE_PAYMENTS        AIP,
       PO_VENDORS                 PV,
       AP_CHECKS                  AC,
       AP_PAYMENT_SCHEDULES       APS,
       PER_CONT_WORKERS_CURRENT_X CWK
 WHERE AI.vendor_id = PV.vendor_id
   AND AIP.CHECK_ID = AC.CHECK_ID
   AND AIP.INVOICE_ID = AI.INVOICE_ID
   AND AI.invoice_id = APS.invoice_id
   AND cwk.vendor_id = pv.vendor_id
   AND AC.check_id = AIP.CHECK_ID
   AND AI.INVOICE_TYPE_LOOKUP_CODE <> ''PREPAYMENT''

';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
v_objlength := DBMS_LOB.getlength (stmt);
IF v_objlength <= 32000 THEN 
EXECUTE IMMEDIATE to_char(stmt);
ELSE
v_offset := 1; 
WHILE v_offset <= v_objlength
LOOP
-- each record is 256 bytes from the LOB
v_sql_long (v_sql_long_count) := DBMS_LOB.SUBSTR (stmt, 256, v_offset);
-- increment the record count
v_sql_long_count := v_sql_long_count + 1;
-- figure out the new offset
v_offset := v_offset + 256;
END LOOP;
BEGIN
  -- open cursor
 c := DBMS_SQL.open_cursor;
 -- parse VARCHAR2S table
 DBMS_SQL.parse (c, v_sql_long, 1, v_sql_long_count - 1, FALSE, 1);
 -- Execute the cursor
r := DBMS_SQL.EXECUTE (c);
-- Close the cursor
 DBMS_SQL.close_cursor (c);
 END;
END IF;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
prompt Creating Object Data XXEIS_1941495_YZMZFC_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1941495_YZMZFC_V
xxeis.eis_rsc_ins.v( 'XXEIS_1941495_YZMZFC_V',91000,'Paste SQL View for HDS OOP Reimbursement','1.0','','','ID020048','APPS','HDS OOP Reimbursement View','X1YV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_1941495_YZMZFC_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1941495_YZMZFC_V',91000,FALSE);
--Inserting Object Columns for XXEIS_1941495_YZMZFC_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1941495_YZMZFC_V','EMPLOYEE_ID',91000,'','','','','','ID020048','NUMBER','','','Employee Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1941495_YZMZFC_V','EMPLOYEE_NUM',91000,'','','','','','ID020048','VARCHAR2','','','Employee Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1941495_YZMZFC_V','NAME',91000,'','','','','','ID020048','VARCHAR2','','','Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1941495_YZMZFC_V','VENDOR_ID',91000,'','','','','','ID020048','NUMBER','','','Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1941495_YZMZFC_V','VENDOR_NUM',91000,'','','','','','ID020048','VARCHAR2','','','Vendor Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1941495_YZMZFC_V','CHECK_NUMBER',91000,'','','','','','ID020048','NUMBER','','','Check Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1941495_YZMZFC_V','REIMBURSEMENT_DATE',91000,'','','','','','ID020048','DATE','','','Reimbursement Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1941495_YZMZFC_V','REIMBURSEMENT_AMOUNT',91000,'','','','','','ID020048','VARCHAR2','','','Reimbursement Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1941495_YZMZFC_V','REIMBURSEMENT_AMOUNT_NUM',91000,'','','','~T~D~2','','ID020048','NUMBER','','','Reimbursement Amount Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1941495_YZMZFC_V','REIMBURSEMENT_CURRENCY',91000,'','','','','','ID020048','VARCHAR2','','','Reimbursement Currency','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1941495_YZMZFC_V','NUMBER_OF_DAYS',91000,'','','','','','ID020048','NUMBER','','','Number Of Days','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1941495_YZMZFC_V','CHECK_ID',91000,'','','','','','ID020048','NUMBER','','','Check Id','','','','US');
--Inserting Object Components for XXEIS_1941495_YZMZFC_V
--Inserting Object Component Joins for XXEIS_1941495_YZMZFC_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
prompt Creating Report LOV Data for HDS OOP Reimbursement
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS OOP Reimbursement
xxeis.eis_rsc_ins.lov( 91000,'select distinct full_name 
from apps.per_employees_x','','HDS Full Name','LOV from apps.per_employees_x','ID020048',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 91000,'select distinct employee_num
from apps.per_employees_x','','HDS Employee Number','LOV from apps.per_employees_x','ID020048',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
prompt Creating Report Data for HDS OOP Reimbursement
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS OOP Reimbursement
xxeis.eis_rsc_utility.delete_report_rows( 'HDS OOP Reimbursement' );
--Inserting Report - HDS OOP Reimbursement
xxeis.eis_rsc_ins.r( 91000,'HDS OOP Reimbursement','','This report provides the Employees OOP Reimbursement details.','','','','XXEIS_RS_ADMIN','XXEIS_1941495_YZMZFC_V','Y','','SELECT     pv.employee_id employee_id,
                emp.employee_num,
                EMP.FULL_NAME NAME,
                AI.vendor_id vendor_id,
                pv.segment1 vendor_num,
                AC.check_number CHECK_NUMBER,
                trunc(AC.check_date) REIMBURSEMENT_DATE,
                to_char(AC.AMOUNT,
                        fnd_currency.get_format_mask(AC.currency_code, 30)) REIMBURSEMENT_AMOUNT,
                AC.AMOUNT REIMBURSEMENT_AMOUNT_NUM,
                AC.CURRENCY_CODE REIMBURSEMENT_CURRENCY,
                to_number(Trunc(SYSDATE - AC.check_date)) number_of_days,
                AC.check_id check_id
  FROM ap.AP_INVOICES_all          AI,
       ap.AP_INVOICE_PAYMENTS_all  AIP,
       ap.ap_suppliers           PV,
       ap.AP_CHECKS_all            AC,
       ap.AP_PAYMENT_SCHEDULES_all APS,
       apps.PER_EMPLOYEES_X      EMP
 WHERE AI.vendor_id = PV.vendor_id
   AND AIP.CHECK_ID = AC.CHECK_ID
   AND AIP.INVOICE_ID = AI.INVOICE_ID
   AND AI.invoice_id = APS.invoice_id
   AND emp.employee_id = pv.employee_id
   AND AC.check_id = AIP.CHECK_ID
   AND AI.INVOICE_TYPE_LOOKUP_CODE <> ''PREPAYMENT''
UNION ALL
SELECT DISTINCT cwk.person_id employee_id,
                NULL,
                CWK.FULL_NAME NAME,
                AI.vendor_id vendor_id,
                NULL,
                AC.check_number CHECK_NUMBER,
                AC.check_date REIMBURSEMENT_DATE,
                to_char(AC.AMOUNT,
                        fnd_currency.get_format_mask(AC.currency_code, 30)) REIMBURSEMENT_AMOUNT,
                AC.AMOUNT REIMBURSEMENT_AMOUNT_NUM,
                AC.CURRENCY_CODE REIMBURSEMENT_CURRENCY,
                to_number(Trunc(SYSDATE - AC.check_date)) number_of_days,
                AC.check_id check_id
  FROM AP_INVOICES                AI,
       AP_INVOICE_PAYMENTS        AIP,
       PO_VENDORS                 PV,
       AP_CHECKS                  AC,
       AP_PAYMENT_SCHEDULES       APS,
       PER_CONT_WORKERS_CURRENT_X CWK
 WHERE AI.vendor_id = PV.vendor_id
   AND AIP.CHECK_ID = AC.CHECK_ID
   AND AIP.INVOICE_ID = AI.INVOICE_ID
   AND AI.invoice_id = APS.invoice_id
   AND cwk.vendor_id = pv.vendor_id
   AND AC.check_id = AIP.CHECK_ID
   AND AI.INVOICE_TYPE_LOOKUP_CODE <> ''PREPAYMENT''
','XXEIS_RS_ADMIN','','Y','HDS Standard Reports','','CSV,EXCEL,','','','','','','','','APPS','US','','','','');
--Inserting Report Columns - HDS OOP Reimbursement
xxeis.eis_rsc_ins.rc( 'HDS OOP Reimbursement',91000,'REIMBURSEMENT_DATE','Reimbursement Date','','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1941495_YZMZFC_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS OOP Reimbursement',91000,'NAME','Name','','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1941495_YZMZFC_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS OOP Reimbursement',91000,'CHECK_NUMBER','Check Number','','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1941495_YZMZFC_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS OOP Reimbursement',91000,'REIMBURSEMENT_AMOUNT','Reimbursement Amount','','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1941495_YZMZFC_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS OOP Reimbursement',91000,'VENDOR_NUM','Vendor Num','','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1941495_YZMZFC_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS OOP Reimbursement',91000,'NUMBER_OF_DAYS','Number Of Days','','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1941495_YZMZFC_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS OOP Reimbursement',91000,'EMPLOYEE_NUM','Employee Num','','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1941495_YZMZFC_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS OOP Reimbursement',91000,'REIMBURSEMENT_CURRENCY','Reimbursement Currency','','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_1941495_YZMZFC_V','','','GROUP_BY','US','');
--Inserting Report Parameters - HDS OOP Reimbursement
xxeis.eis_rsc_ins.rp( 'HDS OOP Reimbursement',91000,'Employee Name','','NAME','IN','HDS Full Name','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_1941495_YZMZFC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS OOP Reimbursement',91000,'Employee Number','','EMPLOYEE_NUM','IN','HDS Employee Number','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_1941495_YZMZFC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS OOP Reimbursement',91000,'Reimbursement Date From','','REIMBURSEMENT_DATE','>=','','','DATE','N','Y','1','N','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','','XXEIS_1941495_YZMZFC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS OOP Reimbursement',91000,'Reimbursement Date To','','REIMBURSEMENT_DATE','<=','','','DATE','N','Y','2','N','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','','XXEIS_1941495_YZMZFC_V','','','US','');
--Inserting Dependent Parameters - HDS OOP Reimbursement
--Inserting Report Conditions - HDS OOP Reimbursement
xxeis.eis_rsc_ins.rcnh( 'HDS OOP Reimbursement',91000,'EMPLOYEE_NUM IN :Employee Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','EMPLOYEE_NUM','','Employee Number','','','','','XXEIS_1941495_YZMZFC_V','','','','','','IN','Y','Y','','','','','1',91000,'HDS OOP Reimbursement','EMPLOYEE_NUM IN :Employee Number ');
xxeis.eis_rsc_ins.rcnh( 'HDS OOP Reimbursement',91000,'NAME IN :Employee Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','NAME','','Employee Name','','','','','XXEIS_1941495_YZMZFC_V','','','','','','IN','Y','Y','','','','','1',91000,'HDS OOP Reimbursement','NAME IN :Employee Name ');
xxeis.eis_rsc_ins.rcnh( 'HDS OOP Reimbursement',91000,'X1YV.REIMBURSEMENT_DATE >= Reimbursement Date From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','REIMBURSEMENT_DATE','','Reimbursement Date From','','','','','XXEIS_1941495_YZMZFC_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',91000,'HDS OOP Reimbursement','X1YV.REIMBURSEMENT_DATE >= Reimbursement Date From');
xxeis.eis_rsc_ins.rcnh( 'HDS OOP Reimbursement',91000,'X1YV.REIMBURSEMENT_DATE <= Reimbursement Date To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','REIMBURSEMENT_DATE','','Reimbursement Date To','','','','','XXEIS_1941495_YZMZFC_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',91000,'HDS OOP Reimbursement','X1YV.REIMBURSEMENT_DATE <= Reimbursement Date To');
--Inserting Report Sorts - HDS OOP Reimbursement
--Inserting Report Triggers - HDS OOP Reimbursement
--inserting report templates - HDS OOP Reimbursement
--Inserting Report Portals - HDS OOP Reimbursement
--inserting report dashboards - HDS OOP Reimbursement
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS OOP Reimbursement','91000','XXEIS_1941495_YZMZFC_V','XXEIS_1941495_YZMZFC_V','N','');
--inserting report security - HDS OOP Reimbursement
xxeis.eis_rsc_ins.rsec( 'HDS OOP Reimbursement','200','','HDS_AP_ADMIN_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS OOP Reimbursement','200','','XXWC_PAYABLES_INQUIRY',91000,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS OOP Reimbursement
--Inserting Report   Version details- HDS OOP Reimbursement
xxeis.eis_rsc_ins.rv( 'HDS OOP Reimbursement','','HDS OOP Reimbursement','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
