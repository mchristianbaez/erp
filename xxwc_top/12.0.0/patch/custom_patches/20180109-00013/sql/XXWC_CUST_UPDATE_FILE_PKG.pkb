CREATE OR REPLACE PACKAGE BODY APPS.XXWC_CUST_UPDATE_FILE_PKG
AS
   /**************************************************************************
      $Header XXWC_CUST_UPDATE_FILE_PKG $
      Module Name: XXWC_CUST_UPDATE_FILE_PKG.pks

      PURPOSE:   This package is called by the concurrent programs
                 XXWC Credit Customer Update File Generation
      REVISIONS:
      Ver        Date        Author             Description
      ---------  ----------  ---------------   -------------------------
       1.0       05/12/2016  Neha Saini         Initial Build - Task ID: 20131016-00419
	   1.1       11/01/2018  Ashwin Sridhar     TMS#20180109-00013-XXWC Credit Customer Update File Generation file enhancement
    /*************************************************************************/

   g_loc   VARCHAR2 (20000);

   /*************************************************************************
     Procedure : Write_Log

     PURPOSE:   This procedure logs debug message in Concurrent Log file
     Parameter:
            IN
                p_debug_msg      -- Debug Message
    REVISIONS:
       Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       05/12/2016  Neha Saini         Initial Build - Task ID: 20131016-00419
   ************************************************************************/
   PROCEDURE write_log (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      DBMS_OUTPUT.put_line (p_debug_msg);
   END write_log;

   /*************************************************************************
      Procedure : Write_Error

     PURPOSE:   This procedure logs error message
     Parameter:
            IN
                p_debug_msg      -- Debug Message
    REVISIONS:
       Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
     1.0       05/12/2016  Neha Saini         Initial Build - Task ID: 20131016-00419
   ************************************************************************/

   --add message to concurrent output file
   PROCEDURE write_error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_CUST_UPDATE_FILE_PKG';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running XXWC_CUST_UPDATE_FILE_PKG with PROGRAM ERROR',
         p_distribution_list   => l_distro_list,
         p_module              => 'AR');
   END write_error;

   /*************************************************************************
     Procedure : generate_file
     PARAMETERS:  IN - None
                  OUT - errbuf,retcode
     PURPOSE:  This procedure creates file for the ARS Outbound Open Balance
     REVISIONS:
       Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
     1.0       05/12/2016  Neha Saini         Initial Build - Task ID: 20131016-00419
	 1.1       11/01/2018  Ashwin Sridhar     TMS#20180109-00013-XXWC Credit Customer Update File Generation file enhancement
   ************************************************************************/
   PROCEDURE generate_file (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      CURSOR cur_open_bal
      IS
     SELECT DISTINCT 
       REPLACE (hca.account_name, ',', ' ') account_name,
       hca.account_number,
       REPLACE (ac.name, ',', ' ') collector_name,
       DECODE (hca.status,  'A', 'Active',  'I', 'InActive',  NULL) status,
       hcp.account_status,
       hl.address1,
       hl.address2,
       hl.city,
       hl.county,
       hl.state,
       hl.postal_code zip,
       hl.country,
       NVL (
          (SELECT PHONE_AREA_CODE || '-' || phone_number
             FROM apps.hz_contact_points
            WHERE     1 = 1
                  AND OWNER_TABLE_NAME = 'HZ_PARTIES'
                  AND OWNER_TABLE_ID = hca.party_id
                  AND CONTACT_POINT_TYPE = 'PHONE'
                  AND phone_line_type = 'GEN'
                  AND primary_flag = 'Y'
                  AND ROWNUM = 1),
          (SELECT PHONE_AREA_CODE || '-' || phone_number
             FROM apps.hz_contact_points
            WHERE     1 = 1
                  AND OWNER_TABLE_NAME = 'HZ_PARTY_SITES'
                  AND OWNER_TABLE_ID = hps.party_site_id
                  AND CONTACT_POINT_TYPE = 'PHONE'
                  AND phone_line_type = 'GEN'
                  AND primary_flag = 'Y'
                  AND ROWNUM = 1))
          Primary_Phone_number,
       (SELECT PHONE_AREA_CODE || '-' || phone_number
          FROM apps.hz_contact_points
         WHERE     1 = 1
               AND OWNER_TABLE_NAME = 'HZ_PARTIES'
               AND OWNER_TABLE_ID = hca.party_id
               AND CONTACT_POINT_TYPE = 'PHONE'
               AND phone_line_type = 'FAX'
               AND ROWNUM = 1)
          primary_fax,
       (SELECT email_address
          FROM apps.hz_contact_points
         WHERE     1 = 1
               AND OWNER_TABLE_NAME = 'HZ_PARTIES'
               AND OWNER_TABLE_ID = hca.party_id
               AND CONTACT_POINT_TYPE = 'EMAIL'
               AND ROWNUM = 1)
          email_address,
       NVL (
          (SELECT REPLACE (res.resource_name, ',', ' ') salesrep_name
             FROM apps.jtf_rs_salesreps rs, apps.JTF_RS_RESOURCE_EXTNS_VL RES
            WHERE     rs.resource_id = res.resource_id
                  AND org_id = 162
                  AND rs.salesrep_id = hcsu.primary_salesrep_id
                  AND ROWNUM = 1),
          'No Sales Credit')
          Account_manager,
       hca.attribute4 customer_source,
       (SELECT description
          FROM apps.ra_terms rt
         WHERE rt.term_id = hcp.standard_terms)
          payment_term,
       (SELECT al.meaning
          FROM apps.ar_lookups al
         WHERE     al.LOOKUP_TYPE LIKE 'CUSTOMER CLASS'
               AND al.enabled_flag = 'Y'
               AND al.lookup_code = hca.Customer_Class_Code)
          Classification,
       hca.attribute9 Predominant_Trade,
       TO_CHAR (HCA.account_established_date, 'MM/DD/RRRR')
          Customer_Since_Date,
       DECODE (hcp.credit_rating,
               'Y', 'Eligible',
               'N', 'Ineligible',
               '', 'Eligible')
          Credit_Rating,
       hcpc.name profile_class,
       hcsu.primary_salesrep_id acct_mgr_id,
       ac.resource_id coll_id,
	   hca.attribute6 SF_Unique_ID --Added by Ashwin.S on 11-Jan-2018 for TMS#20180109-00013 
  FROM ar.hz_cust_accounts hca,
       ar.hz_cust_acct_sites_all hcas,
       ar.hz_customer_profiles hcp,
       ar.hz_cust_profile_classes hcpc,
       ar.ar_collectors ac,
       ar.hz_party_sites hps,
       ar.hz_parties hp,
       ar.hz_locations hl,
       ar.hz_cust_site_uses_all hcsu
 WHERE     1 = 1
       AND hca.cust_account_id = hcas.cust_account_id
       AND hcas.org_id = 162
       AND hca.cust_account_id = hcp.cust_account_id
       AND hcp.collector_id = ac.collector_id
       AND hcp.site_use_id IS NULL
       AND hcp.profile_class_id = hcpc.profile_class_id
       AND hcas.party_site_id = hps.party_site_id
       AND hps.location_id = hl.location_id
       AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
       AND hcas.org_id = hcsu.org_id
       AND hcsu.site_use_code = 'BILL_TO'
       AND hcsu.primary_flag = 'Y'
       AND hp.party_id = hps.party_id
       AND hcpc.name not in ('WC Branches','Branch Cash Account','Intercompany Customers')
       AND hca.account_number NOT IN (SELECT account_number
                                        FROM apps.hz_cust_accounts_all
                                       WHERE LENGTH (
                                                TRIM (
                                                   TRANSLATE (account_number,
                                                              ' 0123456789',
                                                              ' '))) > 0);
                                                              
                                                              
                                                           
      l_filename         VARCHAR2 (200);
      l_count            NUMBER;
      g_prog_exception   EXCEPTION;
      v_file             UTL_FILE.file_type;
      l_err_msg          VARCHAR2 (5000);
      l_directory_name   VARCHAR2 (5000) := 'XXWC_CREDIT_CUST_UPD_FILE';
   BEGIN
      -- printing in Parameters
      g_loc := 'Begining sample file Extract  ';
      write_log (g_loc);
      write_log ('========================================================');


      --Initialize the Out Parameter
      errbuf := NULL;
      retcode := '0';



      -- Get the file name
      SELECT    'CustomerUpdate'
             || TO_CHAR (SYSDATE, 'MMDDRRRR')
             || '.csv'
        INTO l_filename
        FROM DUAL;

      g_loc := 'getting file name ';
      write_log (g_loc || 'l_filename:  ' || l_filename);

      --Open the file handler
      g_loc := 'Open File handler ';
      write_log (g_loc);
      v_file :=
         UTL_FILE.fopen (LOCATION       => l_directory_name,
                         filename       => l_filename,
                         open_mode      => 'w',
                         max_linesize   => 32767);

      g_loc := 'Writing to the file ... Opening Cursor ';
      write_log (g_loc);

      l_count := 0;
      g_loc := 'Resetting the counter and now writing in file first line  ';
      write_log (g_loc);

      UTL_FILE.put_line (
         v_file,
         UPPER (
               'ACCOUNT_NAME'
            || ','
            || 'ACCOUNT_NUMBER'
            || ','
            || 'COLLECTOR_NAME'
            || ','
            || 'STATUS'
            || ','
            || 'ACCOUNT_STATUS'
            || ','
            || 'ADDRESS1'
            || ','
            || 'ADDRESS2'
            || ','
            || 'CITY'
            || ','
            || 'COUNTY'
            || ','
            || 'STATE'
            || ','
            || 'ZIP'
            || ','
            || 'COUNTRY'
            || ','
            || 'PRIMARY_PHONE_NUMBER'
            || ','
            || 'PRIMARY_FAX'
            || ','
            || 'EMAIL_ADDRESS'
            || ','
            || 'ACCOUNT_MANAGER'
            || ','
            || 'CUSTOMER_SOURCE'
            || ','
            || 'PAYMENT_TERM'
            || ','
            || 'CLASSIFICATION'
            || ','
            || 'PREDOMINANT_TRADE'
            || ','
            || 'CUSTOMER_SINCE_DATE'
            || ','
            || 'CREDIT_RATING'
            || ','
            || 'PROFILE_CLASS'
            || ','
            || 'ACCT_MGR_ID'
            || ','
            || 'COLL_ID'
            || ','
			|| 'SF_UNIQUE_ID'  --Added by Ashwin.S on 11-Jan-2018 for TMS#20180109-00013
			|| ','
			));
			
      g_loc := 'now writing in file whole data ';
      write_log (g_loc);

      FOR cur_rec IN cur_open_bal
      LOOP
         EXIT WHEN cur_open_bal%NOTFOUND;

         BEGIN
            -- Writing File Lines

            UTL_FILE.put_line (
               v_file,
               UPPER (
                     TRIM (cur_rec.account_name)
                  || ','
                  || TRIM (cur_rec.account_number)
                  || ','
                  || TRIM (cur_rec.collector_name)
                  || ','
                  || TRIM (cur_rec.status)
                  || ','
                  || TRIM (cur_rec.account_status)
                  || ','
                  || REGEXP_REPLACE (
                        REGEXP_REPLACE (
                           REPLACE (
                              REPLACE (TRIM (cur_rec.address1), ',', ' '),
                              '"',
                              ' '),
                           '[^[:alnum:]|[:blank:]|[:punct:]]+',
                           NULL),
                        '[^' || CHR (32) || '-' || CHR (127) || ']',
                        ' ')
                  || ','
                  || REGEXP_REPLACE (
                        REGEXP_REPLACE (
                           REPLACE (
                              REPLACE (TRIM (cur_rec.address2), ',', ' '),
                              '"',
                              ' '),
                           '[^[:alnum:]|[:blank:]|[:punct:]]+',
                           NULL),
                        '[^' || CHR (32) || '-' || CHR (127) || ']',
                        ' ')
                  || ','
                  || REGEXP_REPLACE (
                        REGEXP_REPLACE (
                           REPLACE (REPLACE (TRIM (cur_rec.city), ',', ' '),
                                    '"',
                                    ' '),
                           '[^[:alnum:]|[:blank:]|[:punct:]]+',
                           NULL),
                        '[^' || CHR (32) || '-' || CHR (127) || ']',
                        ' ')
                  || ','
                  || TRIM (cur_rec.county)
                  || ','
                  || TRIM (cur_rec.state)
                  || ','
                  || TRIM (cur_rec.zip)
                  || ','
                  || TRIM (cur_rec.country)
                  || ','
                  || TRIM (cur_rec.primary_phone_number)
                  || ','
                  || TRIM (cur_rec.primary_fax)
                  || ','
                  || REGEXP_REPLACE (
                        REGEXP_REPLACE (
                           REPLACE (
                              REPLACE (TRIM (cur_rec.email_address),
                                       ',',
                                       ' '),
                              '"',
                              ' '),
                           '[^[:alnum:]|[:blank:]|[:punct:]]+',
                           NULL),
                        '[^' || CHR (32) || '-' || CHR (127) || ']',
                        ' ')
                  || ','
                  || TRIM (cur_rec.account_manager)
                  || ','
                  || REGEXP_REPLACE (
                        REGEXP_REPLACE (
                           REPLACE (
                              REPLACE (TRIM (cur_rec.customer_source),
                                       ',',
                                       ' '),
                              '"',
                              ' '),
                           '[^[:alnum:]|[:blank:]|[:punct:]]+',
                           NULL),
                        '[^' || CHR (32) || '-' || CHR (127) || ']',
                        ' ')
                  || ','
                  || TRIM (cur_rec.payment_term)
                  || ','
                  || TRIM (cur_rec.classification)
                  || ','
                  || REGEXP_REPLACE (
                        REGEXP_REPLACE (
                           REPLACE (
                              REPLACE (TRIM (cur_rec.predominant_trade),
                                       ',',
                                       ' '),
                              '"',
                              ' '),
                           '[^[:alnum:]|[:blank:]|[:punct:]]+',
                           NULL),
                        '[^' || CHR (32) || '-' || CHR (127) || ']',
                        ' ')
                  || ','
                  || TRIM (cur_rec.customer_since_date)
                  || ','
                  || TRIM (cur_rec.credit_rating)
                  || ','
                  || TRIM (cur_rec.profile_class)
                  || ','
                  || TRIM (cur_rec.acct_mgr_id)
                  || ','
                  || TRIM (cur_rec.coll_id)
                  || ','
				  || TRIM(cur_rec.SF_Unique_ID) --Added by Ashwin.S on 11-Jan-2018 for TMS#20180109-00013
				  || ','
				  )
				  );

            l_count := l_count + 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_err_msg :=
                     'Error at --'
                  || cur_rec.account_number
                  || '---- '
                  || SUBSTR (SQLERRM, 1, 2000);
               write_error (l_err_msg);
               write_log (l_err_msg);
         --   if one record has an issue whole program should not fail on capture error and send email
         END;
      END LOOP;

      write_log ('Wrote ' || l_count || ' records to file');
      write_log ('Closing File Handler');
      g_loc := 'now closing in file ';
      --Closing the file handler
      UTL_FILE.fclose (v_file);

      retcode := '0';
      write_log (
         'XXWC Credit Customer Update File Generation - Program Successfully completed');
   EXCEPTION
      WHEN UTL_FILE.invalid_path
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File Path is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_mode
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The open_mode parameter in FOPEN is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_filehandle
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File handle is invalid..';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_operation
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File could not be opened or operated on as requested';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.write_error
      THEN
         UTL_FILE.fclose (v_file);
         errbuf :=
            'Operating system error occurred during the write operation';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.internal_error
      THEN
         UTL_FILE.fclose (v_file);
         write_log ('Unspecified PL/SQL error.');
      WHEN UTL_FILE.file_open
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The requested operation failed because the file is open.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_filename
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The filename parameter is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.access_denied
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'Permission to access to the file location is denied.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN g_prog_exception
      THEN
         errbuf := l_err_msg;
         retcode := '2';
         write_error (l_err_msg);
         write_log (l_err_msg);
      WHEN OTHERS
      THEN
         -- UTL_FILE.fclose (v_file);
         l_err_msg := 'Error Msg :' || SQLERRM;
         errbuf := l_err_msg;
         retcode := '2';
         write_error (l_err_msg);
         write_log (l_err_msg);
   END generate_file;
END XXWC_CUST_UPDATE_FILE_PKG;
/