/****************************************************************************************************************************
    $Header XXWC_ISR_DETAILS_ALL.sql $
    Module Name: XXWC_ISR_DETAILS_ALL
    PURPOSE: Table to store EISR Final Data

    REVISIONS:
    Ver    Date         Author                Description
    ------ -----------  ------------------    -------------------------------------------------------------------------------
    1.0   16-Oct-2015  P.Vamshidhar           TMS#20151008-00138 - Renamed column name Item_cost --> List_price 
                                              renamed column name 
****************************************************************************************************************************/

ALTER TABLE XXWC.XXWC_ISR_DETAILS_ALL RENAME COLUMN ITEM_COST TO LIST_PRICE;
/
