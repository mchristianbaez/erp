/**********************************************************************************************************************
    $Header XXWC_ISR_DETAILS_ARCH_ALL.sql $
    Module Name: XXWC_ISR_DETAILS_ARCH_ALL
    PURPOSE: Table to archive EISR Data from Previous Run

    REVISIONS:
    Ver    Date         Author                Description
    ------ -----------  ------------------    ----------------
    1.0   16-Oct-2015  P.Vamshidhar           TMS#20151008-00138 - Renamed column name Item_cost --> List_price 
**********************************************************************************************************************/

ALTER TABLE XXWC.XXWC_ISR_DETAILS_ARCH_ALL RENAME COLUMN ITEM_COST TO LIST_PRICE;
/
