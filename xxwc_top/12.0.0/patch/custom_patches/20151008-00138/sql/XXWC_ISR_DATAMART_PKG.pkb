CREATE OR REPLACE PACKAGE BODY APPS.xxwc_isr_datamart_pkg
/************************************************************************************************************************************
    $Header XXWC_ISR_DATAMART_PKG.pkb $
    Module Name: XXWC_ISR_DATAMART_PKG
    PURPOSE: Package to load Inventory and Sales Reorder(ISR) Data
    TMS Task Id :  20141001-00063 - EISR Rearchitecure
                   20150317-00111 - EISR Enhancements for Purchasing
    REVISIONS:
    Ver    Date         Author                Description
    ------ -----------  ------------------    ----------------
    1.0    16-Oct-14    Manjula Chellappan    Initial Version
    2.0    09-Jun-15    Manjula chellappan    TMS# 20150609-00182 - Improve performance of load_sales_hit procedure
    3.0    30-Jul-15    M Hari Prasad         TMS# 20150717-00247 - EIS Report for new ISR errors when submitted for single item
    4.0    24-Sep-15    Manjula Chellappan    TMS# 20150910-00122 - Inventory Sales and Reorder Report - WC is currently
                                                                     includes direct order quantities for 1,6 and 12
    5.0    15-Oct-15    P.Vamshidhar          TMS# 20151008-00138  - Minor enhancements and Bug fixes for New ISR Data Mart and
                        Manjula Chellappan                           Inventory Sales and Reorder Report - WC.
***********************************************************************************************************************************/
AS
   g_start         NUMBER;
   g_errbuf        CLOB;
   g_retcode       NUMBER := 0;
   g_dflt_email    fnd_user.email_address%TYPE
                      := 'HDSOracleDevelopers@hdsupply.com';
   g_line_open     VARCHAR2 (100)
      := '/*************************************************************************';
   g_line_close    VARCHAR2 (100)
      := '**************************************************************************/';
   g_sec           VARCHAR2 (200);

   TYPE cursor_type IS REF CURSOR;

   TYPE view_tab IS TABLE OF XXEIS.EIS_XXWC_ISR_REPORT_V%ROWTYPE
      INDEX BY BINARY_INTEGER;

   g_view_tab      view_tab;
   g_view_record   apps.xxwc_isr_details_v%ROWTYPE;

   PROCEDURE write_log (p_log_msg VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.write_log $
     PURPOSE:     Procedure to write Log messages

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_log_msg   VARCHAR2 (2000);
   BEGIN
      IF p_log_msg = g_line_open OR p_log_msg = g_line_close
      THEN
         l_log_msg := p_log_msg;
      ELSE
         l_log_msg :=
            p_log_msg || ' : ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS');
         xxwc_common_tunning_helpers.elapsed_time ('ISR-MO',
                                                   p_log_msg,
                                                   g_start);
      END IF;

      fnd_file.put_line (fnd_file.LOG, l_log_msg);
   END write_log;


   PROCEDURE cleanup_log
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.cleanup_log $
     PURPOSE:     Procedure to cleanup the Log table

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_cleanup_before   NUMBER := 35;
   BEGIN
      DELETE FROM xxwc.xxwc_interfaces##log
            WHERE     interface_name = 'ISR-MO'
                  AND TRUNC (insert_date) <
                         TRUNC (SYSDATE) - l_cleanup_before;

      write_log ('xxwc.xxwc_interfaces##log cleanup completed');
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log (
               'xxwc.xxwc_interfaces##log cleanup failed '
            || SUBSTR (SQLERRM, 1, 1900));
   END cleanup_log;

   PROCEDURE drop_temp_table (p_owner VARCHAR2, p_table_name VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.drop_temp_table $
     PURPOSE:     Local Procedure to drop the temp tables

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_table_count       NUMBER := 0;
      l_drop_stmt         VARCHAR2 (2000);
      l_table_full_name   VARCHAR2 (60) := p_owner || '.' || p_table_name;
      l_success_msg       VARCHAR2 (200)
                             := l_table_full_name || ' Table Dropped ';
      l_error_msg         VARCHAR (200)
                             := l_table_full_name || ' Table Drop failed ';
   BEGIN
      g_sec := 'DROP TABLE ' || p_owner || '.' || p_table_name;


      SELECT COUNT (*)
        INTO l_table_count
        FROM all_tables
       WHERE     table_name = UPPER (p_table_name)
             AND owner = UPPER (p_owner)
             AND UPPER (p_owner) LIKE 'XX%';

      IF l_table_count > 0
      THEN
         l_drop_stmt := 'DROP TABLE ' || p_owner || '.' || p_table_name;

         EXECUTE IMMEDIATE l_drop_stmt;

         write_log (l_success_msg);
      ELSE
         write_log (l_table_full_name || ' Do not exist ');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
   END;

   FUNCTION check_temp_table (p_owner VARCHAR2, p_table_name VARCHAR2)
      RETURN BOOLEAN
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.check_temp_table $
     PURPOSE:     Local Procedure to check if the temp table exist

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_table_count       NUMBER := 0;
      l_drop_stmt         VARCHAR2 (2000);
      l_table_full_name   VARCHAR2 (60) := p_owner || '.' || p_table_name;
      l_success_msg       VARCHAR2 (200)
                             := l_table_full_name || ' Table Dropped ';
      l_error_msg         VARCHAR (200)
                             := l_table_full_name || ' Table Drop failed ';
   BEGIN
      g_sec := 'CHECK TABLE ' || p_owner || '.' || p_table_name;


      SELECT COUNT (*)
        INTO l_table_count
        FROM all_tables
       WHERE     table_name = UPPER (p_table_name)
             AND owner = UPPER (p_owner)
             AND UPPER (p_owner) LIKE 'XX%';

      IF l_table_count > 0
      THEN
         RETURN TRUE;
      ELSE
         RETURN FALSE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         RETURN FALSE;
   END check_temp_table;



   PROCEDURE copy_temp_table (p_owner         VARCHAR2,
                              p_from_table    VARCHAR2,
                              p_to_table      VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.copy_temp_table $
     PURPOSE:     Local Procedure to copy the temp table data

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_table_count            NUMBER := 0;
      l_drop_stmt              VARCHAR2 (2000);
      l_from_table_full_name   VARCHAR2 (60)
                                  := p_owner || '.' || p_from_table;
      l_to_table_full_name     VARCHAR2 (60) := p_owner || '.' || p_to_table;
      l_success_msg            VARCHAR2 (200)
         := l_to_table_full_name || ' Table Dropped ';
      l_error_msg              VARCHAR (200)
         := l_to_table_full_name || ' Table Drop failed ';

      l_copy_success_msg       VARCHAR2 (200)
         :=    l_from_table_full_name
            || ' Table Copied to '
            || l_to_table_full_name;
      l_copy_error_msg         VARCHAR2 (200)
         := l_from_table_full_name || ' Table Copy failed ';
   BEGIN
      g_sec :=
            'COPY TABLE '
         || l_from_table_full_name
         || ' To '
         || l_to_table_full_name;

      SELECT COUNT (*)
        INTO l_table_count
        FROM all_tables
       WHERE table_name = UPPER (p_to_table) AND owner = UPPER (p_owner);

      IF l_table_count > 0
      THEN
         l_drop_stmt := 'DROP TABLE ' || l_to_table_full_name;

         EXECUTE IMMEDIATE l_drop_stmt;

         write_log (l_success_msg);
      ELSE
         write_log (l_to_table_full_name || ' Do not exist ');
      END IF;

      l_success_msg := l_copy_success_msg;
      l_error_msg := l_copy_error_msg;

      EXECUTE IMMEDIATE
            'CREATE TABLE '
         || l_to_table_full_name
         || ' AS SELECT * FROM '
         || l_from_table_full_name;

      write_log (l_success_msg);
   EXCEPTION
      WHEN OTHERS
      THEN
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
   END copy_temp_table;



   PROCEDURE load_ou (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.Load_ou $
     PURPOSE:     1. Procedure to Load Operating Units list for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode             NUMBER := 0;
      l_owner               VARCHAR2 (10) := 'XXWC';
      l_table_name          VARCHAR2 (40) := 'XXWC_ISR_OU##';
      l_table_full_name     VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name          VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg   VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg     VARCHAR2 (200)
                               := l_table_full_name || ' Table Load failed ';
      l_success_msg         VARCHAR2 (200) := l_table_success_msg;
      l_error_msg           VARCHAR2 (200) := l_table_error_msg;
      l_index_success_msg   VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg     VARCHAR2 (200)
                               := l_index_name || ' Index Creation failed ';
      l_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt         VARCHAR2 (4000)
         := '  SELECT organization_id org_id, name operating_unit, set_of_books_id
                 FROM hr_operating_units
                WHERE short_code IN (SELECT lookup_code
                                       FROM fnd_lookup_values
                                      WHERE lookup_type = ''XXWC_REPORTING_OU_LIST'')';

      l_load_stmt           VARCHAR2 (4000)
                               := l_create_table_stmt || l_select_stmt;

      l_index_stmt          VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( org_id)';
   BEGIN
      g_sec := 'Process 1 : Load Operating Units ';
      write_log (g_line_open);
      write_log (g_sec);

      drop_temp_table (l_owner, l_table_name);

      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);
      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_ou;


   PROCEDURE load_inv_org (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.Load_inv_org $
     PURPOSE:     2. Procedure to Load inventory Organizations for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode             NUMBER := 0;
      l_owner               VARCHAR2 (10) := 'XXWC';
      l_table_name          VARCHAR2 (40) := 'XXWC_ISR_ORGANIZATIONS##';
      l_table_full_name     VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name          VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg   VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg     VARCHAR2 (200)
                               := l_table_full_name || ' Table Load failed ';
      l_success_msg         VARCHAR2 (200) := l_table_success_msg;
      l_error_msg           VARCHAR2 (200) := l_table_error_msg;
      l_index_success_msg   VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg     VARCHAR2 (200)
                               := l_index_name || ' Index Creation failed ';
      l_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt         VARCHAR2 (4000)
         := '    SELECT a.organization_id,
                        a.organization_code org,
                        organization_name org_name,
                        b.attribute8 district,
                        b.attribute9 region,
                        ou.org_id,
                        ou.operating_unit,
                        ou.set_of_books_id,
                        b.master_organization_id
                   FROM org_organization_definitions a,
                        mtl_parameters b,
                        xxwc.xxwc_isr_ou## ou
                  WHERE     a.organization_id = b.organization_id
                        AND a.operating_unit = ou.org_id
                        AND NVL (disable_date, TRUNC(SYSDATE) + 1) > TRUNC(SYSDATE)
                        AND NOT EXISTS
                                   (SELECT ''X''
                                      FROM fnd_lookup_values
                                     WHERE     lookup_type =
                                                  ''XXWC_ISR_EXCLUDED_INV_ORG_LIST''
                                           AND lookup_code = a.organization_code)';

      l_load_stmt           VARCHAR2 (4000)
                               := l_create_table_stmt || l_select_stmt;

      l_index_stmt          VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id)';
   BEGIN
      g_sec := 'Process 2 : Load Inventory Organizations ';
      write_log (g_line_open);
      write_log (g_sec);

      drop_temp_table (l_owner, l_table_name);

      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);
      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_inv_org;


   PROCEDURE load_items (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.Load_items $
     PURPOSE:     3. Procedure to Load Item Details for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_ITEMS##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200) := l_table_success_msg;
      l_error_msg                 VARCHAR2 (200) := l_table_error_msg;
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';

--<<Commented for Ver 5.0 Begin
/*		 
      l_select_stmt               VARCHAR2 (4000)
         := '     SELECT b.org_id,                  
                         b.operating_unit,
                         b.master_organization_id,
                         b.set_of_books_id,
                         b.org,
                         b.org_name,
                         b.district,
                         b.region,
                         inventory_item_id,
                         a.organization_id,
                         SUBSTR (segment1, 1, 3) pre,
                         segment1 item_number,
                         (SELECT DECODE (meaning,  ''Inventory'', ''I'',  ''Supplier'', ''S'',  NULL)
                            FROM mfg_lookups
                           WHERE     lookup_type = ''MTL_SOURCE_TYPES''
                                 AND lookup_code = a.source_type)
                            st,
                         (SELECT meaning
                            FROM mfg_lookups
                           WHERE     lookup_type = ''MTL_MATERIAL_PLANNING''
                                 AND lookup_code = a.inventory_planning_code)
                            pm,
                         description,
                         preprocessing_lead_time pplt,
                         full_lead_time plt,
                         primary_uom_code uom,
                         list_price_per_unit list_price,
                         primary_unit_of_measure primary_unit_of_measure,
                         min_minmax_quantity minn,
                         max_minmax_quantity maxn,
                         attribute21 res,
                         (SELECT full_name
                            FROM per_people_x
                           WHERE person_id = a.buyer_id)
                            buyer,
                         shelf_life_days ts,
                         a.inventory_item_status_code item_status_code,
                         a.unit_weight wt,
                         fixed_lot_multiplier fml,
                         attribute20 amu,
                         attribute30 clt,
                         source_organization_id,
                         (SELECT organization_code
                            FROM org_organization_definitions
                           WHERE organization_id = a.source_organization_id)
                            source_org_code,
                         (SELECT COUNT (*)
                            FROM mtl_system_items_b
                           WHERE inventory_item_id = a.inventory_item_id
                             AND source_organization_id = a.organization_id) SO, 
                           -1 thirteen_wk_avg_inv,
                           -1 thirteen_wk_an_cogs,
                           0 turns,
                           sysdate freeze_date,
                           ''XX'' mc,
                           ''XX'' mf_flag,
                           a.lot_control_code,
                           DECODE (a.planning_make_buy_code,  1, ''Make'',  2, ''Buy'') make_buy,
                           (SELECT inventory_item_status_code_tl 
                            FROM   mtl_item_status
                           WHERE   inventory_item_status_code =
                                    a.inventory_item_status_code) org_item_status,
                           (SELECT meaning            
                              FROM fnd_lookup_values            
                             WHERE lookup_type = ''ITEM_TYPE''            
                               AND lookup_code = a.item_type) org_user_item_type,
                           a.last_update_date item_last_update_date,                               
                           -1 common_output_id,
                           -1 process_id
                    FROM mtl_system_items_b a, xxwc.xxwc_isr_organizations## b
                   WHERE a.organization_id = b.organization_id ';
*/
-->>Commented for Ver 5.0 End
--<<Added for Ver 5.0 Begin
		 
      l_select_stmt               VARCHAR2 (4000)
         := '     SELECT b.org_id,                  
                         b.operating_unit,
                         b.master_organization_id,
                         b.set_of_books_id,
                         b.org,
                         b.org_name,
                         b.district,
                         b.region,
                         inventory_item_id,
                         a.organization_id,
                         SUBSTR (segment1, 1, 3) pre,
                         segment1 item_number,
                         NVL((SELECT DECODE (meaning,  ''Inventory'', ''I'',  ''Supplier'', ''S'',  NULL)
                            FROM mfg_lookups
                           WHERE     lookup_type = ''MTL_SOURCE_TYPES''
                                 AND lookup_code = a.source_type),''D'')
                            st,
                         (SELECT meaning
                            FROM mfg_lookups
                           WHERE     lookup_type = ''MTL_MATERIAL_PLANNING''
                                 AND lookup_code = a.inventory_planning_code)
                            pm,
                         description,
                         preprocessing_lead_time pplt,
                         full_lead_time plt,
                         primary_uom_code uom,
                         list_price_per_unit list_price,
                         primary_unit_of_measure primary_unit_of_measure,
                         min_minmax_quantity minn,
                         max_minmax_quantity maxn,
                         attribute21 res,
                         (SELECT full_name
                            FROM per_people_x
                           WHERE person_id = a.buyer_id)
                            buyer,
                         shelf_life_days ts,
                         a.inventory_item_status_code item_status_code,
                         a.unit_weight wt,
                         fixed_lot_multiplier fml,
                         attribute20 amu,
                         attribute30 clt,
                         source_organization_id,
                         (SELECT organization_code
                            FROM org_organization_definitions
                           WHERE organization_id = a.source_organization_id)
                            source_org_code,
                         (SELECT COUNT (*)
                            FROM mtl_system_items_b
                           WHERE inventory_item_id = a.inventory_item_id
                             AND source_organization_id = a.organization_id) SO, 
                           -1 thirteen_wk_avg_inv,
                           -1 thirteen_wk_an_cogs,
                           0 turns,
                           sysdate freeze_date,
                           ''XX'' mc,
                           ''XX'' mf_flag,
                           a.lot_control_code,
                           DECODE (a.planning_make_buy_code,  1, ''Make'',  2, ''Buy'') make_buy,
                           (SELECT inventory_item_status_code_tl 
                            FROM   mtl_item_status
                           WHERE   inventory_item_status_code =
                                    a.inventory_item_status_code) org_item_status,
                           (SELECT meaning            
                              FROM fnd_lookup_values            
                             WHERE lookup_type = ''ITEM_TYPE''            
                               AND lookup_code = a.item_type) org_user_item_type,
                           a.last_update_date item_last_update_date,                               
                           -1 common_output_id,
                           -1 process_id
                    FROM mtl_system_items_b a, xxwc.xxwc_isr_organizations## b
                   WHERE a.organization_id = b.organization_id ';

-->>Added for Ver 5.0 End
      l_load_stmt                 VARCHAR2 (4000)
                                     := l_create_table_stmt || l_select_stmt;

      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id)';



      l_table_name_incr           VARCHAR2 (40) := 'XXWC_ISR_ITEMS_INCR##';
      l_table_full_name_incr      VARCHAR2 (60)
                                     := l_owner || '.' || l_table_name_incr;
      l_index_name_incr           VARCHAR2 (60)
                                     := l_table_full_name_incr || '_N1';
      l_table_success_msg_incr    VARCHAR2 (200)
         := l_table_full_name_incr || ' Table Load completed ';
      l_table_error_msg_incr      VARCHAR2 (200)
         := l_table_full_name_incr || ' Table Load failed ';

      l_index_success_msg_incr    VARCHAR2 (200)
         := l_index_name_incr || ' Index Creation completed ';
      l_index_error_msg_incr      VARCHAR2 (200)
         := l_index_name_incr || ' Index Creation failed ';
	 		 
      l_create_table_stmt_incr    VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name_incr || ' AS ';
      l_select_stmt_incr VARCHAR2 (4000)   
         := ' SELECT b.org_id,                  
                         b.operating_unit,
                         b.master_organization_id,
                         b.set_of_books_id,
                         b.org,
                         b.org_name,
                         b.district,
                         b.region,
                         inventory_item_id,
                         a.organization_id,
                         SUBSTR (segment1, 1, 3) pre,
                         segment1 item_number,
                         (SELECT DECODE (meaning,  ''Inventory'', ''I'',  ''Supplier'', ''S'',  NULL)
                            FROM mfg_lookups
                           WHERE     lookup_type = ''MTL_SOURCE_TYPES''
                                 AND lookup_code = a.source_type) st,                            
                         (SELECT meaning
                            FROM mfg_lookups
                           WHERE     lookup_type = ''MTL_MATERIAL_PLANNING''
                                 AND lookup_code = a.inventory_planning_code) pm,                            
                         description,
                         preprocessing_lead_time pplt,
                         full_lead_time plt,
                         primary_uom_code uom,
                         list_price_per_unit list_price,
                         primary_unit_of_measure primary_unit_of_measure,
                         min_minmax_quantity minn,
                         max_minmax_quantity maxn,
                         attribute21 res,
                         (SELECT full_name
                            FROM per_people_x
                           WHERE person_id = a.buyer_id)
                            buyer,
                         shelf_life_days ts,
                         a.inventory_item_status_code item_status_code,
                         a.unit_weight wt,
                         fixed_lot_multiplier fml,
                         attribute20 amu,
                         attribute30 clt,
                         source_organization_id,
                         (SELECT organization_code
                            FROM org_organization_definitions
                           WHERE organization_id = a.source_organization_id)
                            source_org_code,
                         (SELECT COUNT (*)
                            FROM mtl_system_items_b
                           WHERE inventory_item_id = a.inventory_item_id
                             AND source_organization_id = a.organization_id) SO, 
                           -1 thirteen_wk_avg_inv,
                           -1 thirteen_wk_an_cogs,
                           0 turns,
                           sysdate freeze_date,
                           ''XX'' mc,
                           ''XX'' mf_flag,
                           a.lot_control_code,
                           DECODE (a.planning_make_buy_code,  1, ''Make'',  2, ''Buy'') make_buy,
                           (SELECT inventory_item_status_code_tl 
                            FROM   mtl_item_status
                           WHERE   inventory_item_status_code =
                                    a.inventory_item_status_code) org_item_status,
                           (SELECT meaning            
                              FROM fnd_lookup_values            
                             WHERE lookup_type = ''ITEM_TYPE''            
                               AND lookup_code = a.item_type) org_user_item_type,
                           a.last_update_date item_last_update_date, -1 common_output_id,  -1 process_id                                                                                  
                    FROM mtl_system_items_b a, xxwc.xxwc_isr_organizations## b
                   WHERE a.organization_id = b.organization_id 
                     AND a.last_update_date >= (SELECT MAX(item_last_update_date) FROM XXWC.XXWC_ISR_ITEMS##) ';


      l_load_stmt_incr            VARCHAR2 (4000)
         := l_create_table_stmt_incr || l_select_stmt_incr;

      l_delete_dup_stmt           VARCHAR2 (4000)
         := '    DELETE FROM XXWC.XXWC_ISR_ITEMS## a
                      WHERE EXISTS
                               (SELECT ''X''
                                  FROM XXWC.XXWC_ISR_ITEMS_INCR##
                                 WHERE     inventory_item_id = a.inventory_item_id
                                   AND organization_id = a.organization_id) ';

      l_delete_dup_success_msg    VARCHAR2 (200)
         := l_table_full_name || ' Duplicates Deleted ';

      l_delete_dup_error_msg      VARCHAR2 (200)
         := l_table_full_name || ' Duplicates Delete Failed ';


      l_insert_incr_stmt          VARCHAR2 (4000)
         := '      INSERT /*+ append */ INTO XXWC.XXWC_ISR_ITEMS##                        
                               (SELECT *
                                  FROM XXWC.XXWC_ISR_ITEMS_INCR## ) ';


      l_insert_incr_success_msg   VARCHAR2 (200)
         := l_table_full_name || ' Incremantal Load completed ';

      l_insert_incr_error_msg     VARCHAR2 (200)
         := l_table_full_name || ' Incremental Load Failed ';


		 
   BEGIN
      g_sec := 'Process 3 : Load Items ';
      write_log (g_line_open);
      write_log (g_sec);

--    IF NOT check_temp_table (l_owner, l_table_name) -- Commented for Ver 5.0 
--    THEN -- Commented for Ver 5.0 
         drop_temp_table (l_owner, l_table_name); -- Uncommented for Ver 5.0

         EXECUTE IMMEDIATE l_load_stmt;

         write_log (l_success_msg);

         l_success_msg := l_index_success_msg;
         l_error_msg := l_index_error_msg;

         EXECUTE IMMEDIATE l_index_stmt;

         write_log (l_success_msg);
		 
--<<Added for Ver 5.0 Begin	
	 
		 g_sec := 'Process 3a : Update Source type for Internal Items ';	
		 
		 l_success_msg := 'Update Source type for Internal Items completed';
         l_error_msg := 'Update Source type for Internal Items Failed';
		 
-- ST - D to be set to NULL later
		 
		 EXECUTE IMMEDIATE 
		      'UPDATE XXWC.XXWC_ISR_ITEMS## set st=''D'' 
				WHERE st = ''I'' 
				  AND ( source_organization_id IS NULL 
				        OR source_organization_id = organization_id) ' ;
				  
		 write_log (l_success_msg);				  
		
--<<Added for Ver 5.0 End		 

         write_log (g_line_close);		 
--<<Commented for Ver 5.0 Begin, Removed incremental Load
/*		 
      ELSE
         l_success_msg := l_table_success_msg_incr;
         l_error_msg := l_table_error_msg_incr;


         drop_temp_table (l_owner, l_table_name_incr);

         EXECUTE IMMEDIATE l_load_stmt_incr;

         write_log (l_success_msg);


         l_success_msg := l_delete_dup_success_msg;
         l_error_msg := l_delete_dup_error_msg;

         EXECUTE IMMEDIATE l_delete_dup_stmt;

         write_log (l_success_msg);

         l_success_msg := l_insert_incr_success_msg;
         l_error_msg := l_insert_incr_error_msg;


         EXECUTE IMMEDIATE l_insert_incr_stmt;

         write_log (l_success_msg);

         write_log (g_line_close);
      END IF;

      COMMIT;
*/
-->>Commented for Ver 5.0 End
	  
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_items;


   PROCEDURE load_category (p_retcode OUT VARCHAR2)
   /*********************************************************************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_category $
     PURPOSE:     4. Procedure to Load Item category for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     5.0    15-Oct-15    P.Vamshidhar          TMS#20151008-00138  - Minor enhancements and Bug fixes for New ISR Data Mart and
                         Manjula Chellappan                          Inventory Sales and Reorder Report - WC.
     *******************************************************************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';

      l_table_name_pre            VARCHAR2 (40) := 'XXWC_ISR_CATEGORY##';
      l_table_full_name_pre       VARCHAR2 (60)
                                     := l_owner || '.' || l_table_name_pre;
      l_index_name_pre            VARCHAR2 (60) := l_table_full_name_pre || '_N1';
      l_table_success_msg_pre     VARCHAR2 (200)
         := l_table_full_name_pre || ' Table Load completed ';
      l_table_error_msg_pre       VARCHAR2 (200)
         := l_table_full_name_pre || ' Table Load failed ';

      l_index_success_msg_pre     VARCHAR2 (200)
         := l_index_name_pre || ' Index Creation completed ';
      l_index_error_msg_pre       VARCHAR2 (200)
         := l_index_name_pre || ' Index Creation failed ';
      l_create_table_stmt_pre     VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name_pre || ' AS ';
      --  l_select_stmt       VARCHAR2(4000) := ' ';
      l_load_stmt_pre             VARCHAR2 (4000)
         :=    l_create_table_stmt_pre
            || '    SELECT mic.inventory_item_id,
                           mic.organization_id,
                           mcs.category_set_id,
                           mcs.structure_id,
                           mc.category_id,
                           mcs.category_set_name,
                           mc.segment1,
                           mc.segment2,
                           mic.last_update_date                           
                      FROM mtl_Category_sets mcs, mtl_categories_kfv mc, mtl_item_categories mic
                     WHERE     mcs.structure_id = mc.structure_id
                           AND mc.category_id = mic.category_id
                           AND mcs.category_set_name IN (''Inventory Category'',
                                                         ''Core Status'',
                                                         ''Purchase Flag'',
                                                         ''Sales Velocity'' ) ';

      l_index_stmt_pre            VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name_pre
            || ' ON '
            || l_table_full_name_pre
            || ' ( organization_id, inventory_item_id)';



      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_ITEM_CATEGORY##';
      l_table_full_name           VARCHAR2 (60)
                                     := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
         := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
         := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_load_stmt                 CLOB;

      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id)';

      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*,
                       b.cl,
                       b.last_sv_change_date,
                       b.fi_flag,
                       b.inv_cat_seg1,
                       b.cat,
                       b.core,
                       CASE 
                           WHEN (b.cl IN (''1'', ''2'', ''3'', ''4'', ''5'', ''6'', ''7'', ''8'', ''9'', ''C'', ''B'')) THEN ''Y''
                           WHEN (b.cl IN (''E'') AND (a.minn = 0 AND a.maxn = 0)) THEN ''N''
                           WHEN (b.cl IN (''E'') AND (a.minn > 0 AND a.maxn > 0)) THEN ''Y''
                           WHEN (b.cl IN (''N'', ''Z'')) THEN ''N''
                           ELSE ''N''
                       END stk_flag,
                       TO_DATE(NULL) flip_date                       
                  FROM xxwc.xxwc_isr_items## a, xxwc.xxwc_isr_item_category## b
                 WHERE a.organization_id = b.organization_id
                   AND a.inventory_item_id = b.inventory_item_id ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
--<<Commented for Ver 5.0 Begin
/*
      l_flip_date_update_stmt     VARCHAR2 (4000)
         := '   UPDATE XXWC.XXWC_ISR_MERGE## flip
                   SET flip_date =
                          (SELECT DECODE (flip.stk_flag,
                                          a.stk_flag, a.flip_date,
                                          flip.last_sv_change_date)
                             FROM xxwc.xxwc_isr_details_all a
                            WHERE     a.inventory_item_id = flip.inventory_item_id
                                  AND a.organization_id = flip.organization_id)    ';	
*/
-->>Commented for Ver 5.0 End
--<<Added for Ver 5.0 Begin		 
      l_flip_date_update_stmt     VARCHAR2 (4000)
         := '   UPDATE XXWC.XXWC_ISR_MERGE## flip
                   SET flip_date =
                          nvl((SELECT DECODE (flip.stk_flag,
                                          a.stk_flag, a.flip_date,
                                          flip.last_sv_change_date)    
                             FROM xxwc.xxwc_isr_details_all a
                            WHERE     a.inventory_item_id = flip.inventory_item_id
                                  AND a.organization_id = flip.organization_id),flip.last_sv_change_date)'; 
-->>Added for Ver 5.0 End

      l_flip_date_success_msg     VARCHAR2 (200)
                                     := ' Flip date Update completed ';
      l_flip_date_error_msg       VARCHAR2 (200)
                                     := ' Flip Date Update failed ';
   BEGIN
      g_sec := 'Process 4 : Load Category ';
      write_log (g_line_open);
      write_log (g_sec);

      g_sec := 'Process 4a : Pre Load Item Category ';

      write_log (g_sec);

      l_success_msg := l_table_success_msg_pre;
      l_error_msg := l_table_error_msg_pre;

      drop_temp_table (l_owner, l_table_name_pre);

      EXECUTE IMMEDIATE l_load_stmt_pre;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg_pre;
      l_error_msg := l_index_error_msg_pre;

      EXECUTE IMMEDIATE l_index_stmt_pre;

      write_log (l_success_msg);


      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);
      g_sec := 'Process 4b : Load Item Category ';

      write_log (g_sec);

      l_load_stmt :=
            l_create_table_stmt
         || '   SELECT inv.inventory_item_id,
                   inv.organization_id,
                   sv.segment1 cl,
                   sv.last_update_date last_sv_change_date,
                   pur.segment1 fi_flag,
                   inv.segment1 inv_cat_seg1,
                   inv.segment2 cat,
                   core.segment1 core
              FROM (SELECT *
                      FROM XXWC.XXWC_ISR_CATEGORY##
                     WHERE category_set_name = ''Inventory Category'') inv,
                   (SELECT *
                      FROM XXWC.XXWC_ISR_CATEGORY##
                     WHERE category_set_name = ''Core Status'') core,
                   (SELECT *
                      FROM XXWC.XXWC_ISR_CATEGORY##
                     WHERE category_set_name = ''Purchase Flag'') pur,
                   (SELECT *
                      FROM XXWC.XXWC_ISR_CATEGORY##
                     WHERE category_set_name = ''Sales Velocity'') sv
             WHERE     inv.inventory_item_id = core.inventory_item_id(+)
                   AND inv.organization_id = core.organization_id(+)
                   AND inv.inventory_item_id = pur.inventory_item_id(+)
                   AND inv.organization_id = pur.organization_id(+)
                   AND inv.inventory_item_id = sv.inventory_item_id(+)
                   AND inv.organization_id = sv.organization_id(+) ';

      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);

      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_flip_date_success_msg;
      l_error_msg := l_flip_date_error_msg;

      EXECUTE IMMEDIATE l_flip_date_update_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_category;



   PROCEDURE load_item_source (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.Load_item_source $
     PURPOSE:     5. Procedure to Load Item source Details for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_ITEM_SOURCE##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt               VARCHAR2 (4000)
         := '  SELECT msa.organization_id,                                   
                      msa.inventory_item_id,                    
                      msa.sourcing_rule_id,                    
                      msr.sourcing_rule_name sourcing_rule,                    
                      msro.sr_receipt_id,                    
                      msso.vendor_id,                    
                      aps.vendor_name vendor_name,
                      aps.attribute3 tier,
                      aps.attribute4 cat_sba_owner,                    
                      aps.segment1 vendor_num,                    
                      msso.vendor_site_id,                    
                      ass.vendor_site_code  vendor_site                  
                 FROM mrp_sr_assignments msa,                    
                      mrp_sr_receipt_org msro,                    
                      mrp_sr_source_org msso,                    
                      mrp_sourcing_rules msr,                    
                      ap_suppliers aps,                    
                      ap_supplier_sites_all ass                    
                WHERE     msa.sourcing_rule_id = msro.sourcing_rule_id(+)                    
                      AND msa.sourcing_rule_id = msr.sourcing_rule_id(+)                    
                      AND msro.sr_receipt_id = msso.sr_receipt_id(+)                    
                      AND msso.vendor_id = aps.vendor_id(+)                    
                      AND msso.vendor_site_id = ass.vendor_site_id(+)
                      AND msa.assignment_type=6                       
                      AND msa.assignment_set_id = 1
                      AND NVL(msso.source_type,3) = 3 ';
      l_load_stmt                 VARCHAR2 (4000)
                                     := l_create_table_stmt || l_select_stmt;

      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( vendor_id, vendor_site_id,inventory_item_id, organization_id)';


      l_table_name1               VARCHAR2 (40) := 'XXWC_ISR_ITEM_SOURCE_COM##';
      l_table_full_name1          VARCHAR2 (60) := l_owner || '.' || l_table_name1;
      l_index_name1               VARCHAR2 (60) := l_table_full_name1 || '_N1';
      l_table_success_msg1        VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load completed ';
      l_table_error_msg1          VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load failed ';
      l_success_msg1              VARCHAR2 (200);
      l_error_msg1                VARCHAR2 (200);
      l_index_success_msg1        VARCHAR2 (200)
         := l_index_name1 || ' Index Creation completed ';
      l_index_error_msg1          VARCHAR2 (200)
                                     := l_index_name1 || ' Index Creation failed ';
      l_create_table_stmt1        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name1 || ' AS ';
--Commented for Ver 5.0 Begin
/*
      l_select_stmt1              VARCHAR2 (4000)
         := '       SELECT *
                   FROM (  SELECT inventory_item_id,
                                  sourcing_rule_id,
                                  sourcing_rule,
                                  vendor_num,
                                  vendor_name,
                                  COUNT (*),
                                  ROW_NUMBER ()
                                  OVER (PARTITION BY inventory_item_id ORDER BY COUNT (*) DESC)
                                     rn
                             FROM xxwc.xxwc_isr_item_source##
                         GROUP BY inventory_item_id,
                                  sourcing_rule_id,
                                  sourcing_rule,
                                  vendor_num,
                                  vendor_name)
                  WHERE rn = 1 ';
*/
--Commented for Ver 5.0 End

      l_select_stmt1              VARCHAR2 (4000) ; -- Added for Ver 5.0
     				  		  
--Commented for Ver 5.0 Begin
/*	
     l_load_stmt1                VARCHAR2 (4000)
                                     := l_create_table_stmt1 || l_select_stmt1;
								 
      l_index_stmt1               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name1
            || ' ON '
            || l_table_full_name1
            || ' ( inventory_item_id)';
*/
--Commented for Ver 5.0 End

--Added for Ver 5.0 Begin
									 
      l_index_stmt1               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name1
            || ' ON '
            || l_table_full_name1
            || ' ( rule_type)';
--Added for Ver 5.0 End

      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';

--Commented for Ver 5.0 Begin
/*
      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*,
                       b.sourcing_rule,
                       b.vendor_name,
                       b.vendor_num,
                       b.vendor_num site_vendor_num,
                       b.vendor_site,
                       b.tier,
                       b.cat_sba_owner,
                       NVL(b.vendor_name, c.vendor_name) com_vendor_name,
                       NVL(b.vendor_num, c.vendor_num) com_vendor_num
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_item_source## b, xxwc.xxwc_isr_item_source_com## c 
                 WHERE a.organization_id = b.organization_id(+)
                   AND a.inventory_item_id = b.inventory_item_id(+)
                   AND a.inventory_item_id = c.inventory_item_id(+)    ';
*/
--Commented for Ver 5.0 End
--Added for Ver 5.0 Begin

      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*,
                       b.sourcing_rule,
                       b.vendor_name,
                       b.vendor_num,
                       b.vendor_num site_vendor_num,
                       b.vendor_site,
                       b.tier,
                       b.cat_sba_owner,
                       NVL(b.vendor_name, NVL(c.vendor_name,NVL(d.vendor_name,e.vendor_name))) com_vendor_name,
                       NVL(b.vendor_num, NVL(c.vendor_num,NVL(d.vendor_num,e.vendor_num))) com_vendor_num
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_item_source## b, 
                       (SELECT * 
                          FROM xxwc.xxwc_isr_item_source_com##
                         WHERE rule_type =''DISTRICT'') c,
                       (SELECT * 
                          FROM xxwc.xxwc_isr_item_source_com##
                         WHERE rule_type =''REGION'') d,
                       (SELECT * 
                          FROM xxwc.xxwc_isr_item_source_com##
                         WHERE rule_type =''WORLD'') e
                 WHERE a.organization_id = b.organization_id(+)
                   AND a.inventory_item_id = b.inventory_item_id(+)
                   AND a.inventory_item_id = c.inventory_item_id(+)
                   AND a.district = c.rule_value(+)
                   AND a.inventory_item_id = d.inventory_item_id(+)
                   AND a.district = d.rule_value(+)
                   AND a.inventory_item_id = e.inventory_item_id(+)  ';

--Added for Ver 5.0 End
      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec := 'Process 5 : Load Item source ';
      write_log (g_line_open);
      write_log (g_sec);

      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);

      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);


      g_sec := 'Process 5a : Load Item source common ';
      write_log (g_line_open);
      write_log (g_sec);

      l_success_msg := l_table_success_msg1;
      l_error_msg := l_table_error_msg1;

      drop_temp_table (l_owner, l_table_name1);

--    EXECUTE IMMEDIATE l_load_stmt1; -- Commented for Ver 5.0 
-- Added for ver 5.0 Begin
      EXECUTE IMMEDIATE l_create_table_stmt1 ||
	           ' SELECT *
                   FROM (  SELECT source.vendor_id,
				                  source.inventory_item_id,
                                  source.sourcing_rule_id,
                                  source.sourcing_rule,
                                  source.vendor_num,
                                  source.vendor_name,
                                  ''DISTRICT'' rule_type,
                                  item.district rule_value,
                                  COUNT (*) count,
                                  ROW_NUMBER ()
                                  OVER (PARTITION BY source.inventory_item_id, item.district ORDER BY COUNT (*) DESC ,source.vendor_id DESC)
                                     rn
                             FROM xxwc.xxwc_isr_item_source## source, xxwc.xxwc_isr_items## item
							              WHERE source.inventory_item_id = item.inventory_item_id
                             AND source.organization_id = item.organization_id
                             AND item.st = ''S''  
                             AND source.vendor_num NOT IN (''99999'',''88888'')							 
                         GROUP BY source.vendor_id,
                                  source.inventory_item_id,
                                  source.sourcing_rule_id,
                                  source.sourcing_rule,
                                  source.vendor_num,
                                  source.vendor_name,
                                  ''DISTRICT'',
                                  item.district   
                           UNION ALL                                  
                           SELECT source.vendor_id,
				                          source.inventory_item_id,
                                  source.sourcing_rule_id,
                                  source.sourcing_rule,
                                  source.vendor_num,
                                  source.vendor_name,
                                  ''REGION'' rule_type,
                                  item.region rule_value,
                                  COUNT (*) count,
                                  ROW_NUMBER ()
                                  OVER (PARTITION BY source.inventory_item_id, item.region ORDER BY COUNT (*) DESC ,source.vendor_id DESC)
                                     rn
                             FROM xxwc.xxwc_isr_item_source## source, xxwc.xxwc_isr_items## item
							              WHERE source.inventory_item_id = item.inventory_item_id
                             AND source.organization_id = item.organization_id
                             AND item.st = ''S''  
                             AND source.vendor_num NOT IN (''99999'',''88888'')							 
                         GROUP BY source.vendor_id,
                                  source.inventory_item_id,
                                  source.sourcing_rule_id,
                                  source.sourcing_rule,
                                  source.vendor_num,
                                  source.vendor_name,
                                  ''REGION'',
                                  item.region        
                           UNION ALL                                 
                           SELECT source.vendor_id,
				                          source.inventory_item_id,
                                  source.sourcing_rule_id,
                                  source.sourcing_rule,
                                  source.vendor_num,
                                  source.vendor_name,
                                  ''WORLD'' rule_type,
                                  NULL rule_value,
                                  COUNT (*) count,
                                  ROW_NUMBER ()
                                  OVER (PARTITION BY source.inventory_item_id ORDER BY COUNT (*) DESC ,source.vendor_id DESC)
                                     rn
                             FROM xxwc.xxwc_isr_item_source## source, xxwc.xxwc_isr_items## item
                            WHERE source.inventory_item_id = item.inventory_item_id
                             AND source.organization_id = item.organization_id
                             AND item.st = ''S''
							 AND source.vendor_num NOT IN (''99999'',''88888'')
                         GROUP BY source.vendor_id,
                                  source.inventory_item_id,
                                  source.sourcing_rule_id,
                                  source.sourcing_rule,
                                  source.vendor_num,
                                  source.vendor_name,
                                  ''WORLD'')
                  WHERE rn = 1 ' ;
-- Added for ver 5.0 End

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg1;
      l_error_msg := l_index_error_msg1;

      EXECUTE IMMEDIATE l_index_stmt1;

      write_log (l_success_msg);


      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_item_source;



   PROCEDURE load_sales_hit (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_sales_hit $
     PURPOSE:     6. Procedure to Load sales details for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     2.0    09-Jun-15    Manjula chellappan    TMS# 20150609-00182 - Improve performance
     4.0    24-Sep-15    Manjula chellappan    TMS# 20150910-00122 - Inventory Sales and Reorder Report - WC is currently
                                                                     includes direct order quantities for 1,6 and 12
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_SALES_TXN##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';

      l_load_stmt                 CLOB;

      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( line_category_code)';


      l_table_name_incr           VARCHAR2 (40) := 'XXWC_ISR_SALES_TXN_INCR##';
      l_table_full_name_incr      VARCHAR2 (60)
                                     := l_owner || '.' || l_table_name_incr;

      l_table_success_msg_incr    VARCHAR2 (200)
         := l_table_full_name_incr || ' Table Load completed ';
      l_table_error_msg_incr      VARCHAR2 (200)
         := l_table_full_name_incr || ' Table Load failed ';

      l_create_table_stmt_incr    VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name_incr || ' AS ';
      l_load_stmt_incr            CLOB;


      --<< Added for Rev 2.0 Begin

      l_table_name_stg            VARCHAR2 (40) := 'XXWC_ISR_SALES_TXN_STG##';
      l_table_full_name_stg       VARCHAR2 (60)
                                     := l_owner || '.' || l_table_name_stg;

      l_table_success_msg_stg     VARCHAR2 (200)
         := l_table_full_name_stg || ' Table Load completed ';
      l_table_error_msg_stg       VARCHAR2 (200)
         := l_table_full_name_stg || ' Table Load failed ';

      l_create_table_stmt_stg     VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name_stg;

      l_load_stmt_stg             VARCHAR2 (4000)
         :=    l_create_table_stmt_stg
            || ' AS SELECT * FROM '
            || l_table_full_name;

      l_insert_incr_stmt          VARCHAR2 (4000)
         := '   INSERT /*+ append */ INTO XXWC.XXWC_ISR_SALES_TXN## 
                    SELECT *
                      FROM XXWC.XXWC_ISR_SALES_TXN_STG## a
                     WHERE     NVL (action_date, TRUNC (SYSDATE - 364)) >= TRUNC (SYSDATE - 365)
                           AND NOT EXISTS
                                  (SELECT ''X''
                                     FROM XXWC.XXWC_ISR_SALES_TXN_INCR##
                                    WHERE line_id = a.line_id)               
                    UNION
                    SELECT * FROM XXWC.XXWC_ISR_SALES_TXN_INCR## ';

      -- Added for Rev 2.0 End >>

      --<< commented for Rev 2.0 Begin
      /*
            l_delete_dup_stmt           VARCHAR2 (4000)
               := '    DELETE FROM XXWC.XXWC_ISR_SALES_TXN## a
                            WHERE EXISTS
                                     (SELECT ''X''
                                        FROM XXWC.XXWC_ISR_SALES_TXN_INCR##
                                       WHERE     line_id = a.line_id )
                              OR NVL(action_date,TRUNC(SYSDATE-366)) < TRUNC(sysdate-365)         ';

            l_delete_dup_success_msg    VARCHAR2 (200)
               :=    l_table_full_name
                  || ' Duplicates and older than one year Deleted ';

            l_delete_dup_error_msg      VARCHAR2 (200)
               := l_table_full_name || ' Duplicates Delete Failed ';



      --     l_insert_incr_stmt          VARCHAR2 (4000)
      --        := '      INSERT /*+ append */ --INTO XXWC.XXWC_ISR_SALES_TXN##
      --                              (SELECT *
      --                                 FROM XXWC.XXWC_ISR_SALES_TXN_INCR## ) ';
      -- commented for Rev 2.0 End >>

      l_insert_incr_success_msg   VARCHAR2 (200)
         := l_table_full_name || ' Incremantal Load completed ';

      l_insert_incr_error_msg     VARCHAR2 (200)
         := l_table_full_name || ' Incremental Load Failed ';



      l_table_name1               VARCHAR2 (40) := 'XXWC_ISR_SALES_HIT_4_6##';
      l_table_full_name1          VARCHAR2 (60)
                                     := l_owner || '.' || l_table_name1;
      l_index_name1               VARCHAR2 (60)
                                     := l_table_full_name1 || '_N1';
      l_table_success_msg1        VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load completed ';
      l_table_error_msg1          VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load failed ';
      l_success_msg1              VARCHAR2 (200);
      l_error_msg1                VARCHAR2 (200);
      l_index_success_msg1        VARCHAR2 (200)
         := l_index_name1 || ' Index Creation completed ';
      l_index_error_msg1          VARCHAR2 (200)
         := l_index_name1 || ' Index Creation failed ';
      l_create_table_stmt1        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name1 || ' AS ';

      --<<Commented for Rev 4.0 Begin
      /*
            l_select_stmt1              VARCHAR2 (4000)
               := '  SELECT organization_id,
                            inventory_item_id,
                            org_id,
                            action_date,
                            CASE
                               WHEN sales.action_date > TRUNC (SYSDATE - 112)
                               THEN
                                  sales.header_id
                            END
                               hit4_store_sales,
                            CASE
                               WHEN sales.action_date > TRUNC (SYSDATE - 182)
                               THEN
                                  sales.header_id
                            END
                               hit6_store_sales
                       FROM xxwc.xxwc_isr_sales_txn## sales
                      WHERE     sales.line_category_code <> ''RETURN''
                            AND NOT EXISTS
                                       (SELECT 1
                                          FROM DUAL
                                         WHERE (DECODE (
                                                   sales.line_category_code,
                                                   ''RETURN'', (sales.ordered_quantity * -1),
                                                   sales.ordered_quantity)) =
                                                  (SELECT   -1
                                                          * SUM (
                                                               DECODE (
                                                                  olr.line_category_code,
                                                                  ''RETURN'', (  olr.ordered_quantity
                                                                             * -1),
                                                                  olr.ordered_quantity))
                                                     FROM oe_order_lines_all olr
                                                    WHERE     olr.reference_header_id =
                                                                 sales.header_id
                                                          AND olr.reference_line_id =
                                                                 sales.line_id
                                                          AND olr.line_category_code = ''RETURN''
                                                          AND olr.return_context = ''ORDER''))';
      */
      -->>Commented for Rev 4.0 End

      --<<Added for Rev 4.0 Begin
      l_select_stmt1              VARCHAR2 (4000)
         := '  SELECT organization_id,        
                      inventory_item_id,        
                      org_id,        
                      action_date,        
                      CASE        
                         WHEN sales.action_date > TRUNC (SYSDATE - 112)        
                         THEN        
                            sales.header_id        
                      END        
                         hit4_store_sales,        
                      CASE        
                         WHEN sales.action_date > TRUNC (SYSDATE - 182)        
                         THEN        
                            sales.header_id        
                      END        
                         hit6_store_sales        
                 FROM xxwc.xxwc_isr_sales_txn## sales        
                WHERE     sales.line_category_code <> ''RETURN''        
                      AND NOT EXISTS        
                                 (SELECT 1        
                                    FROM DUAL        
                                   WHERE (DECODE (        
                                             sales.line_category_code,        
                                             ''RETURN'', (sales.ordered_quantity * -1),        
                                             sales.ordered_quantity)) =        
                                            (SELECT   -1        
                                                    * SUM (        
                                                         DECODE (        
                                                            olr.line_category_code,        
                                                            ''RETURN'', (  olr.ordered_quantity        
                                                                       * -1),        
                                                            olr.ordered_quantity))        
                                               FROM oe_order_lines_all olr        
                                              WHERE     olr.reference_header_id =        
                                                           sales.header_id        
                                                    AND olr.reference_line_id =        
                                                           sales.line_id        
                                                    AND olr.line_category_code = ''RETURN''        
                                                    AND olr.return_context = ''ORDER''))
                      AND NOT EXISTS
                                 ( SELECT ''x'' 
                                   FROM oe_drop_ship_sources
                                  WHERE line_id = sales.line_id
                                  )';
      -->>Added for Rev 4.0 End

      l_load_stmt1                VARCHAR2 (4000)
         := l_create_table_stmt1 || l_select_stmt1;

      l_table_name2               VARCHAR2 (40) := 'XXWC_ISR_SALES_HIT##';
      l_table_full_name2          VARCHAR2 (60)
                                     := l_owner || '.' || l_table_name2;
      l_index_name2               VARCHAR2 (60)
                                     := l_table_full_name2 || '_N1';
      l_table_success_msg2        VARCHAR2 (200)
         := l_table_full_name2 || ' Table Load completed ';
      l_table_error_msg2          VARCHAR2 (200)
         := l_table_full_name2 || ' Table Load failed ';
      l_success_msg2              VARCHAR2 (200);
      l_error_msg2                VARCHAR2 (200);
      l_index_success_msg2        VARCHAR2 (200)
         := l_index_name2 || ' Index Creation completed ';
      l_index_error_msg2          VARCHAR2 (200)
         := l_index_name2 || ' Index Creation failed ';
      l_create_table_stmt2        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name2 || ' AS ';
      l_select_stmt2              VARCHAR2 (4000)
         := '                  SELECT organization_id,                    
                                    inventory_item_id,                    
                                    org_id,                    
                                    COUNT (DISTINCT hit4_store_sales) hit4_store_sales,                    
                                    COUNT (DISTINCT hit6_store_sales) hit6_store_sales                    
                               FROM xxwc.xxwc_isr_sales_hit_4_6##                    
                           GROUP BY organization_id, inventory_item_id, org_id ';
      l_load_stmt2                VARCHAR2 (4000)
         := l_create_table_stmt2 || l_select_stmt2;

      l_index_stmt2               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name2
            || ' ON '
            || l_table_full_name2
            || ' ( organization_id, inventory_item_id,org_id)';


      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*, b.hit4_store_sales, b.hit6_store_sales
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_sales_hit## b
                 WHERE     a.organization_id = b.organization_id(+)
                       AND a.inventory_item_id = b.inventory_item_id(+)
                       AND a.org_id = b.org_id(+) ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec := 'Process 6 : Load Sales Hit ';

      write_log (g_line_open);
      write_log (g_sec);

      g_sec := 'Process 6a : Load Sales Transactions ';
      write_log (g_sec);



      IF NOT check_temp_table (l_owner, l_table_name)
      THEN
         --  drop_temp_table (l_owner, l_table_name);

         l_load_stmt :=
               l_create_table_stmt
            || '   SELECT sales.organization_id,                    
                       sales.inventory_item_id,                    
                       sales.org_id,                    
                       sales.header_id,        
                       sales.line_id,        
                       sales.ordered_quantity,                    
                       sales.trunc_last_update_date,          
                       sales.line_category_code,                  
                       DECODE (sales.line_type,                    
                               ''COUNTER LINE'', TRUNC (sales.fulfillment_Date),                    
                               TRUNC (sales.actual_shipment_date))                    
                          action_date                    
                  FROM (SELECT oel.ship_from_org_id organization_id,                    
                               oel.inventory_item_id inventory_item_id,                    
                               oel.header_id,                    
                               oel.line_id,                    
                               oel.line_category_code,                    
                               oel.ordered_quantity,                    
                               oel.line_type_id,                    
                               (SELECT name                    
                                  FROM oe_transaction_types_tl                    
                                 WHERE transaction_type_id = oel.line_type_id)                    
                                  line_type,                    
                               oel.fulfillment_date,                    
                               oel.actual_shipment_date,                    
                               TRUNC (oel.LAST_UPDATE_DATE) trunc_last_update_Date,                    
                               oel.Flow_Status_Code,                    
                               oel.invoice_interface_status_code,                    
                               oel.Ship_to_Org_ID,                    
                               oel.org_id                    
                          FROM oe_order_lines_all oel                    
                         WHERE     flow_status_code = ''CLOSED''                    
                               AND invoice_interface_status_code = ''YES'' 
                               )  sales ';

         l_success_msg := l_table_success_msg;
         l_error_msg := l_table_error_msg;


         EXECUTE IMMEDIATE l_load_stmt;


         write_log (l_success_msg);


         l_success_msg := l_index_success_msg;
         l_error_msg := l_index_error_msg;

         EXECUTE IMMEDIATE l_index_stmt;

         write_log (l_success_msg);
         write_log (g_line_close);
      ELSE
         l_load_stmt_incr :=
               l_create_table_stmt_incr
            || '   SELECT sales.organization_id,                    
                       sales.inventory_item_id,                    
                       sales.org_id,                    
                       sales.header_id,        
                       sales.line_id,        
                       sales.ordered_quantity,                    
                       sales.trunc_last_update_date,          
                       sales.line_category_code,                  
                       DECODE (sales.line_type,                    
                               ''COUNTER LINE'', TRUNC (sales.fulfillment_Date),                    
                               TRUNC (sales.actual_shipment_date))                    
                          action_date                    
                  FROM (SELECT oel.ship_from_org_id organization_id,                    
                               oel.inventory_item_id inventory_item_id,                    
                               oel.header_id,                    
                               oel.line_id,                    
                               oel.line_category_code,                    
                               oel.ordered_quantity,                    
                               oel.line_type_id,                    
                               (SELECT name                    
                                  FROM oe_transaction_types_tl                    
                                 WHERE transaction_type_id = oel.line_type_id)                    
                                  line_type,                    
                               oel.fulfillment_date,                    
                               oel.actual_shipment_date,                    
                               TRUNC (oel.LAST_UPDATE_DATE) trunc_last_update_Date,                    
                               oel.Flow_Status_Code,                    
                               oel.invoice_interface_status_code,                    
                               oel.Ship_to_Org_ID,                    
                               oel.org_id                    
                          FROM oe_order_lines_all oel                    
                         WHERE     flow_status_code = ''CLOSED''                    
                               AND invoice_interface_status_code = ''YES'' 
                               AND TRUNC (oel.LAST_UPDATE_DATE) >= 
                                    ( SELECT MAX(trunc_last_update_Date) FROM XXWC.XXWC_ISR_SALES_TXN## ))  sales ';


         l_success_msg := l_table_success_msg_incr;
         l_error_msg := l_table_error_msg_incr;


         drop_temp_table (l_owner, l_table_name_incr);

         EXECUTE IMMEDIATE l_load_stmt_incr;

         write_log (l_success_msg);

         --<< commented for Rev 2.0 Begin
         /*
                  l_success_msg := l_delete_dup_success_msg;
                  l_error_msg := l_delete_dup_error_msg;

                  EXECUTE IMMEDIATE l_delete_dup_stmt;

                  write_log (l_success_msg);
         */
         --<< commented for Rev 2.0 End

         --<< Added for Rev 2.0 Begin

         drop_temp_table (l_owner, l_table_name_stg);

         l_success_msg := l_table_success_msg_stg;
         l_error_msg := l_table_error_msg_stg;

         EXECUTE IMMEDIATE l_load_stmt_stg;

         write_log (l_success_msg);

         l_success_msg := l_table_full_name || ' Truncated ';
         l_error_msg := l_table_full_name || ' Truncate Failed ';

         EXECUTE IMMEDIATE 'TRUNCATE TABLE ' || l_table_full_name;

         write_log (l_success_msg);

         --<< Added for Rev 2.0 End

         l_success_msg := l_insert_incr_success_msg;
         l_error_msg := l_insert_incr_error_msg;

         EXECUTE IMMEDIATE l_insert_incr_stmt;

         write_log (l_success_msg);

         write_log (g_line_close);
      END IF;

      COMMIT;

      ----------------------------------
      g_sec := 'Process 6b : Load Sales Hit 4 and 6 ';

      write_log (g_sec);

      l_success_msg := l_table_success_msg1;
      l_error_msg := l_table_error_msg1;

      drop_temp_table (l_owner, l_table_name1);

      EXECUTE IMMEDIATE l_load_stmt1;

      write_log (l_success_msg);

      -----------------------------------

      g_sec := 'Process 6c : Load Sales Hit ';
      write_log (g_sec);

      l_success_msg := l_table_success_msg2;
      l_error_msg := l_table_error_msg2;

      drop_temp_table (l_owner, l_table_name2);

      EXECUTE IMMEDIATE l_load_stmt2;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg2;
      l_error_msg := l_index_error_msg2;

      EXECUTE IMMEDIATE l_index_stmt2;

      write_log (l_success_msg);
      -----------------------------------

      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_sales_hit;


   PROCEDURE load_other_sales_hit (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_other_sales_hit $
     PURPOSE:     7. Procedure to Load ther Inventory inv org's Sales Hit for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_OSALES_HIT##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt               VARCHAR2 (4000)
         := '  SELECT source_organization_id organization_id,
                     inventory_item_id,
                     org_id,
                     SUM (hit4_other_inv_sales) hit4_other_inv_sales,
                     SUM (hit6_other_inv_sales) hit6_other_inv_sales
                FROM (  SELECT items.source_organization_id source_organization_id,
                               items.organization_id,
                               items.inventory_item_id,
                               items.org_id,
                               COUNT (DISTINCT hit4_store_sales) hit4_other_inv_sales,
                               COUNT (DISTINCT hit6_store_sales) hit6_other_inv_sales
                          FROM xxwc.xxwc_isr_sales_hit_4_6## sales,
                               xxwc.xxwc_isr_items## items
                         WHERE     sales.org_id = items.org_id
                               AND sales.organization_id = items.organization_id
                               AND sales.inventory_item_id = items.inventory_item_id
                               AND sales.organization_id <> items.source_organization_id
                      GROUP BY items.org_id,
                               items.source_organization_id,
                               items.organization_id,
                               items.inventory_item_id)               
            GROUP BY source_organization_id, inventory_item_id, org_id ';


      l_load_stmt                 VARCHAR2 (4000)
                                     := l_create_table_stmt || l_select_stmt;


      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id,org_id)';



      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*, b.hit4_other_inv_sales,    
                            b.hit6_other_inv_sales                                                        
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_osales_hit## b
                 WHERE     a.organization_id = b.organization_id(+)
                       AND a.inventory_item_id = b.inventory_item_id(+)
                       AND a.org_id = b.org_id(+) ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec :=
         'Process 7 : Load Item Sales Hit from Other Inventory Organizations';

      write_log (g_line_open);
      write_log (g_sec);

      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);


      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);


      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_other_sales_hit;


   PROCEDURE load_bpa_cost (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_bpa_cost $
     PURPOSE:     8. Procedure to Load BPA Cost for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_BPA_STG##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      --  l_select_stmt               VARCHAR2 (4000);
      l_load_stmt                 CLOB;



      l_table_name1               VARCHAR2 (40) := 'XXWC_ISR_BPA##';
      l_table_full_name1          VARCHAR2 (60) := l_owner || '.' || l_table_name1;
      l_index_name1               VARCHAR2 (60) := l_table_full_name1 || '_N1';
      l_table_success_msg1        VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load completed ';
      l_table_error_msg1          VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load failed ';
      l_success_msg1              VARCHAR2 (200);
      l_error_msg1                VARCHAR2 (200);
      l_index_success_msg1        VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg1          VARCHAR2 (200)
                                     := l_index_name1 || ' Index Creation failed ';
      l_create_table_stmt1        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name1 || ' AS ';
      l_select_stmt1              VARCHAR2 (4000)
         := '   SELECT bpa_stg.*
                  FROM xxwc.xxwc_isr_bpa_stg## bpa_stg
                 WHERE 1 =
                          (SELECT COUNT (*)
                             FROM po_lines_all
                            WHERE     po_header_id = bpa_stg.po_header_id
                                  AND item_id = bpa_stg.inventory_item_id
                                  AND (    NVL (closed_code, ''X'') = bpa_stg.l_closed_code
                                       AND NVL (cancel_flag, ''N'') = bpa_stg.l_cancel_flag)) ';
      l_load_stmt1                VARCHAR2 (4000)
                                     := l_create_table_stmt1 || l_select_stmt1;



      l_table_name2               VARCHAR2 (40) := 'XXWC_ISR_BPA_COST##';
      l_table_full_name2          VARCHAR2 (60) := l_owner || '.' || l_table_name2;
      l_index_name2               VARCHAR2 (60) := l_table_full_name2 || '_N1';
      l_table_success_msg2        VARCHAR2 (200)
         := l_table_full_name2 || ' Table Load completed ';
      l_table_error_msg2          VARCHAR2 (200)
         := l_table_full_name2 || ' Table Load failed ';
      l_success_msg2              VARCHAR2 (200);
      l_error_msg2                VARCHAR2 (200);
      l_index_success_msg2        VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg2          VARCHAR2 (200)
                                     := l_index_name2 || ' Index Creation failed ';
      l_create_table_stmt2        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name2 || ' AS ';
      l_select_stmt2              VARCHAR2 (4000)
         := '  SELECT bpa.*,                        
                      NVL (zone.price_zone, 0) price_zone,                        
                      (SELECT promo_price                        
                         FROM xxwc.xxwc_bpa_promo_tbl                        
                        WHERE     po_header_id = bpa.po_header_id                        
                              AND inventory_item_id = bpa.inventory_item_id                        
                              AND TRUNC(SYSDATE) BETWEEN NVL (promo_start_date,
                                                                   TRUNC(SYSDATE))
                                                          AND NVL (promo_end_date,
                                                                   TRUNC(SYSDATE)))                        
                         promo_price,                        
                      national.price_zone_price national_price,                        
                      zonal.price_zone_price                        
                 FROM xxwc.xxwc_isr_bpa## bpa,                        
                      xxwc.xxwc_vendor_price_zone_tbl zone,                        
                      (SELECT *                        
                         FROM xxwc.xxwc_bpa_price_zone_tbl                        
                        WHERE price_zone = 0) national,                        
                      (SELECT a.inventory_item_id,
                              a.org_id,
                              a.po_header_id,
                              a.price_zone_price,
                              c.organization_id                        
                         FROM xxwc.xxwc_bpa_price_zone_tbl a,                        
                              xxwc.xxwc_vendor_price_zone_tbl b,                        
                              xxwc.xxwc_isr_bpa## c                        
                        WHERE     a.price_zone <> 0                        
                              AND a.inventory_item_id = c.inventory_item_id                        
                              AND a.po_header_id = c.po_header_id                        
                              AND b.vendor_id = c.vendor_id                        
                              AND b.vendor_site_id = c.vendor_site_id                        
                              AND b.organization_id = c.organization_id                        
                              AND a.price_zone = b.price_zone) zonal                        
                WHERE     bpa.vendor_id = zone.vendor_id(+)                        
                      AND bpa.vendor_site_id = zone.vendor_site_id(+)                        
                      AND bpa.organization_id = zone.organization_id(+)                        
                      AND bpa.po_header_id = national.po_header_id(+)                        
                      AND bpa.inventory_item_id = national.inventory_item_id(+)                        
                      AND bpa.po_header_id = zonal.po_header_id(+)                        
                      AND bpa.inventory_item_id = zonal.inventory_item_id(+)                        
                      AND bpa.organization_id = zonal.organization_id(+)     ';
      l_load_stmt2                VARCHAR2 (4000)
                                     := l_create_table_stmt2 || l_select_stmt2;


      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id,po_header_id,vendor_id,vendor_site_id)';

      l_index_stmt1               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name1
            || ' ON '
            || l_table_full_name1
            || ' ( organization_id, inventory_item_id,po_header_id,vendor_id,vendor_site_id)';

      l_index_stmt2               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name2
            || ' ON '
            || l_table_full_name2
            || ' ( organization_id, inventory_item_id,org_id)';



      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*, b.bpa,  
                              NVL (
                              b.promo_price,
                              DECODE (
                                 b.vendor_site,
                                 ''0PURCHASING'', NVL (b.price_zone_price,
                                                     NVL (b.national_price, b.unit_price)),
                                 b.unit_price)) bpa_cost
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_bpa_cost## b
                 WHERE     a.organization_id = b.organization_id(+)
                       AND a.inventory_item_id = b.inventory_item_id(+)
                       AND a.org_id = b.org_id(+) ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec := 'Process 8 : Load Item BPA and BPA Cost ';

      write_log (g_line_open);
      write_log (g_sec);
      g_sec := 'Process 8a : Load Item Blanket Purchase Agreements ';

      write_log (g_sec);

      l_load_stmt :=
            l_create_table_stmt
         || ' SELECT *
              FROM (SELECT 
                   poh.org_id, 
                   pol.po_line_id po_line_id,
                   poh.po_header_id,
                   poh.segment1 bpa,
                   TRUNC (poh.creation_date) po_creation_date,
                   poh.vendor_id,
                   poh.vendor_site_id,
                   poh.global_agreement_flag,
                   z.vendor_num,
                   z.vendor_name,
                   z.vendor_site,
                   NVL (pol.closed_code, ''X'') l_closed_code,
                   NVL (poh.closed_code, ''X'') h_closed_code,
                   pol.item_id inventory_item_id,
                   z.organization_id,
                   NVL (pol.cancel_flag, ''N'') l_cancel_flag,
                   NVL (poh.cancel_flag, ''N'') h_cancel_flag,
                   pol.unit_price,
                   NVL (pol.quantity, 0) quantity,
                   ROW_NUMBER ()
                   OVER (PARTITION BY pol.item_id, z.organization_id, NVL (pol.cancel_flag, ''N'')
                     ORDER BY pol.po_line_id DESC)
                  AS rn
              FROM po_headers_all poh,
                   po_lines_all pol,
                   xxwc.xxwc_isr_item_source## z
             WHERE     poh.type_lookup_code = ''BLANKET''
                   AND poh.po_header_id = pol.po_header_id
                   AND NVL (pol.cancel_flag, ''N'') = ''N''
                   AND NVL (poh.cancel_flag, ''N'') = ''N''
                   AND pol.item_id = z.inventory_item_id
                   AND NVL (pol.closed_code, ''X'') <> ''CLOSED''
                   AND NVL (poh.closed_code, ''X'') <> ''CLOSED''
                   AND NVL (poh.authorization_status, ''X'') = ''APPROVED''
                   AND NVL (TRUNC(poh.end_date) , NVL (TRUNC (pol.expiration_date), TRUNC (SYSDATE) + 1)) >
                      TRUNC (SYSDATE)
                   AND poh.vendor_id = z.vendor_id
                   AND poh.vendor_site_id = z.vendor_site_id) l
         WHERE     l.rn = 1
               AND (   l.global_agreement_flag = ''Y''
                OR EXISTS
                  (SELECT ''x''
                     FROM po_line_locations_all
                    WHERE     po_line_id = l.po_line_id
                      AND ship_to_organization_id = l.organization_id)) ';



      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);


      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);

      ----------------------------------
      g_sec :=
         'Process 8b : Load Item Blanket Purchase Agreement lines without duplicate lines for same item';

      write_log (g_sec);

      l_success_msg := l_table_success_msg1;
      l_error_msg := l_table_error_msg1;

      drop_temp_table (l_owner, l_table_name1);

      EXECUTE IMMEDIATE l_load_stmt1;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg1;
      l_error_msg := l_index_error_msg1;

      EXECUTE IMMEDIATE l_index_stmt1;

      write_log (l_success_msg);


      write_log (l_success_msg);
      -----------------------------------

      g_sec := 'Process 8c : Load Item Blanket Purchase Agreement Cost';

      write_log (g_sec);

      l_success_msg := l_table_success_msg2;
      l_error_msg := l_table_error_msg2;

      drop_temp_table (l_owner, l_table_name2);

      EXECUTE IMMEDIATE l_load_stmt2;

      write_log (l_success_msg);
      -----------------------------------

      l_success_msg := l_index_success_msg2;
      l_error_msg := l_index_error_msg2;

      EXECUTE IMMEDIATE l_index_stmt2;

      write_log (l_success_msg);


      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_bpa_cost;



   PROCEDURE load_average_cost (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_average_cost $
     PURPOSE:     9. Procedure to Load Item Average Cost for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_AVERAGE_COST##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt               VARCHAR2 (4000)
         := '     SELECT a.item_cost aver_cost, a.inventory_item_id, a.organization_id                       
                     FROM cst_item_costs a,                            
                          cst_cost_types b                          
                    WHERE     1 = 1                            
                          AND a.cost_type_id = b.cost_type_id                            
                          AND b.COST_TYPE = ''Average''                            
                          AND a.organization_id = NVL (b.organization_id, a.organization_id)                            
                          AND TRUNC (NVL (b.DISABLE_DATE, SYSDATE + 1)) > TRUNC (SYSDATE) ';


      l_load_stmt                 VARCHAR2 (4000)
                                     := l_create_table_stmt || l_select_stmt;


      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id)';



      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*, b.aver_cost                              
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_average_cost## b
                 WHERE     a.organization_id = b.organization_id(+)
                       AND a.inventory_item_id = b.inventory_item_id(+)  ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec := 'Process 9 : Load Item Average Cost ';

      write_log (g_line_open);
      write_log (g_sec);

      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);


      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);


      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_average_cost;


   PROCEDURE load_primary_binloc (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_primary_binloc $
     PURPOSE:     10. Procedure to Load Item Primary Bin Location for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_BINLOC##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt               VARCHAR2 (4000)
         := '   SELECT mil.segment1 bin_loc,
                       msl.inventory_item_id,
                       msl.organization_id
                  FROM mtl_item_locations_kfv mil,
                       mtl_secondary_locators msl
                 WHERE     msl.secondary_locator = mil.inventory_location_id                       
                       AND mil.segment1 LIKE ''1%''
                       AND 1 =
                              (  SELECT COUNT (*)
                                   FROM mtl_item_locations_kfv a, mtl_secondary_locators b
                                  WHERE     b.secondary_locator = a.inventory_location_id
                                        AND b.organization_id = msl.organization_id
                                        AND b.inventory_item_id = msl.inventory_item_id
                                        AND a.segment1 LIKE ''1%''
                               GROUP BY b.inventory_item_id, b.organization_id)';


      l_load_stmt                 VARCHAR2 (4000)
                                     := l_create_table_stmt || l_select_stmt;


      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id)';


      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*, b.bin_loc                             
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_binloc## b
                 WHERE     a.organization_id = b.organization_id(+)
                       AND a.inventory_item_id = b.inventory_item_id(+) ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec := 'Process 10 : Load Item Primary Bin Location ';

      write_log (g_line_open);
      write_log (g_sec);

      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);


      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);


      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_primary_binloc;


   PROCEDURE load_sales_qty (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_sales_qty $
     PURPOSE:     11. Procedure to Load Item Sales Quantity for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     4.0    24-Sep-15    Manjula Chellappan    TMS# 20150910-00122 - Inventory Sales and Reorder Report - WC is currently
                                                                     includes direct order quantities for 1,6 and 12
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_SALES_QTY##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';

      --<<Commented for Ver 4.0 Begin
      /*
               l_select_stmt               VARCHAR2 (4000)
               := '        SELECT org_id, organization_id,
                                  inventory_item_id,
                                  SUM (twelve_store_sale) twelve_store_sale,
                                  SUM (six_store_sale) six_store_sale,
                                  SUM (one_store_sale) one_store_sale
                             FROM (SELECT org_id, organization_id,
                                          ol.inventory_item_id inventory_item_id,
                                          CASE
                                             WHEN ol.action_date > TRUNC (SYSDATE - 365)
                                             THEN
                                                DECODE (ol.line_category_code,
                                                        ''RETURN'', (ol.ordered_quantity * -1),
                                                        ol.ordered_quantity)
                                          END
                                             twelve_store_sale,
                                          CASE
                                             WHEN ol.action_date > TRUNC (SYSDATE - 182)
                                             THEN
                                                DECODE (ol.line_category_code,
                                                        ''RETURN'', (ol.ordered_quantity * -1),
                                                        ol.ordered_quantity)
                                          END
                                             six_store_sale,
                                          CASE
                                             WHEN ol.action_date > TRUNC (SYSDATE - 30)
                                             THEN
                                                DECODE (ol.line_category_code,
                                                        ''RETURN'', (ol.ordered_quantity * -1),
                                                        ol.ordered_quantity)
                                          END
                                             one_store_sale
                                     FROM xxwc.xxwc_isr_sales_txn## ol)
                         GROUP BY org_id, organization_id, inventory_item_id ';
      */
      -->>Commented for Ver 4.0 End

      --<<Added for Ver 4.0 Begin
      l_select_stmt               VARCHAR2 (4000)
         := '        SELECT org_id, organization_id,    
                            inventory_item_id,    
                            SUM (twelve_store_sale) twelve_store_sale,    
                            SUM (six_store_sale) six_store_sale,    
                            SUM (one_store_sale) one_store_sale    
                       FROM (SELECT org_id, organization_id,    
                                    ol.inventory_item_id inventory_item_id,    
                                    CASE    
                                       WHEN ol.action_date > TRUNC (SYSDATE - 365)    
                                       THEN    
                                          DECODE (ol.line_category_code,    
                                                  ''RETURN'', (ol.ordered_quantity * -1),    
                                                  ol.ordered_quantity)    
                                    END    
                                       twelve_store_sale,    
                                    CASE    
                                       WHEN ol.action_date > TRUNC (SYSDATE - 182)    
                                       THEN    
                                          DECODE (ol.line_category_code,    
                                                  ''RETURN'', (ol.ordered_quantity * -1),    
                                                  ol.ordered_quantity)    
                                    END    
                                       six_store_sale,    
                                    CASE    
                                       WHEN ol.action_date >  ( SELECT start_date 
                                                                  FROM gl_periods 
                                                                 WHERE trunc(SYSDATE) BETWEEN start_date and end_date
                                                                   AND period_set_name =''4-4-QTR'' )   
                                       THEN    
                                          DECODE (ol.line_category_code,    
                                                  ''RETURN'', (ol.ordered_quantity * -1),    
                                                  ol.ordered_quantity)    
                                    END    
                                       one_store_sale    
                               FROM xxwc.xxwc_isr_sales_txn## ol
                              WHERE NOT EXISTS
                               ( SELECT ''x'' 
                                   FROM oe_drop_ship_sources
                                  WHERE line_id = ol.line_id
                               ) )    
                   GROUP BY org_id, organization_id, inventory_item_id ';

      -->>Added for Ver 4.0 End

      l_load_stmt                 VARCHAR2 (4000)
                                     := l_create_table_stmt || l_select_stmt;


      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id,org_id)';



      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*, b.twelve_store_sale,    
                            b.six_store_sale,    
                            b.one_store_sale                                
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_sales_qty## b
                 WHERE     a.organization_id = b.organization_id(+)
                       AND a.inventory_item_id = b.inventory_item_id(+)
                       AND a.org_id = b.org_id(+) ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec := 'Process 11 : Load Item Sales Quantity ';

      write_log (g_line_open);
      write_log (g_sec);

      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);


      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);


      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_sales_qty;


   PROCEDURE load_other_sales_qty (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_other_sales_qty $
     PURPOSE:     12. Procedure to Load ther Inventory inv org's Sales Qty for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_OSALES_QTY##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt               VARCHAR2 (4000)
         := '    SELECT items.org_id, items.source_organization_id organization_id,        
                        items.inventory_item_id,        
                        SUM (twelve_store_sale) twelve_other_inv_sale,        
                        SUM (six_store_sale) six_other_inv_sale,        
                        SUM (one_store_Sale) one_other_inv_Sale        
                   FROM xxwc.xxwc_isr_sales_qty## sales, xxwc.xxwc_isr_items## items        
                  WHERE     sales.org_id = items.org_id
                        AND sales.organization_id = items.organization_id        
                        AND sales.inventory_item_id = items.inventory_item_id        
                        AND sales.organization_id <> items.source_organization_id        
               GROUP BY items.org_id, items.source_organization_id, items.inventory_item_id         ';

      l_load_stmt                 VARCHAR2 (4000)
                                     := l_create_table_stmt || l_select_stmt;


      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id,org_id)';



      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*, b.twelve_other_inv_sale,    
                            b.six_other_inv_sale,    
                            b.one_other_inv_sale                                
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_osales_qty## b
                 WHERE     a.organization_id = b.organization_id(+)
                       AND a.inventory_item_id = b.inventory_item_id(+)
                       AND a.org_id = b.org_id(+) ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec :=
         'Process 12 : Load Item Sales Quantity from Other Inventory Organizations';

      write_log (g_line_open);
      write_log (g_sec);

      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);


      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);


      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_other_sales_qty;


   PROCEDURE load_safety_stock (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_safety_stock $
     PURPOSE:     13. Procedure to Item safety stock for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_SAFETY_STOCK##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt               VARCHAR2 (4000)
         := '   SELECT mst.safety_stock_quantity ss,
                       mst.inventory_item_id,
                       mst.organization_id,
                       items.org_id,
                       mst.effectivity_date,
                       oap.acct_period_id,       
                       period_start_date,
                       period_close_date,
                       schedule_close_date
                  FROM mtl_safety_stocks_view mst,
                       org_acct_periods oap,
                       xxwc.xxwc_isr_items## items
                 WHERE     mst.organization_id = items.organization_id
                       AND mst.inventory_item_id = items.inventory_item_id
                       AND items.organization_id = oap.organization_id
                       AND TRUNC (SYSDATE) BETWEEN oap.period_start_date
                                               AND NVL (oap.period_close_date,
                                                        oap.schedule_close_date)
                       AND TRUNC (mst.effectivity_date) BETWEEN period_start_date
                                                            AND NVL (period_close_date,
                                                                     schedule_close_date)
                       AND TRUNC (SYSDATE) >= TRUNC (mst.effectivity_date)  ';

      l_load_stmt                 VARCHAR2 (4000)
                                     := l_create_table_stmt || l_select_stmt;


      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id,org_id)';



      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*, b.ss                                
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_safety_stock## b
                 WHERE     a.organization_id = b.organization_id(+)
                       AND a.inventory_item_id = b.inventory_item_id(+)
                       AND a.org_id = b.org_id(+) ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec := 'Process 13 : Load Item Safety Stock';

      write_log (g_line_open);
      write_log (g_sec);

      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);


      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);


      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_safety_stock;


   PROCEDURE load_gl_periods (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_gl_periods $
     PURPOSE:     14. Procedure to Load GL Periods for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode             NUMBER := 0;
      l_owner               VARCHAR2 (10) := 'XXWC';
      l_table_name          VARCHAR2 (40) := 'XXWC_ISR_PERIODS##';
      l_table_full_name     VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name          VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg   VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg     VARCHAR2 (200)
                               := l_table_full_name || ' Table Load failed ';
      l_success_msg         VARCHAR2 (200) := l_table_success_msg;
      l_error_msg           VARCHAR2 (200) := l_table_error_msg;
      l_index_success_msg   VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg     VARCHAR2 (200)
                               := l_index_name || ' Index Creation failed ';
      l_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt         VARCHAR2 (4000)
         := '     SELECT period_name,
                         period_year,
                         effective_period_num,
                         TO_DATE (period_name, ''Mon-YYYY'') period_date,
                         TO_NUMBER (TO_CHAR (TO_DATE (period_name, ''Mon-YYYY''), ''MM''))
                            month_num,
                         start_date,
                         end_date,
                         period_name hd_prname,
                         closing_status,
                         gps.set_of_books_id,
                         ou.org_id
                    FROM gl_period_statuses gps, xxwc.xxwc_isr_ou## ou
                   WHERE     end_date < TRUNC (SYSDATE)
                         AND start_date >=
                                (SELECT ADD_MONTHS (start_date, -12)
                                   FROM gl_period_statuses
                                  WHERE     TRUNC (SYSDATE) BETWEEN start_date AND end_date
                                        AND set_of_books_id = gps.set_of_books_id
                                        AND application_id = gps.application_id)
                         AND application_id = 101
                         AND adjustment_period_flag <> ''Y''
                         AND gps.set_of_books_id = ou.set_of_books_id
                         AND period_year BETWEEN TO_NUMBER (TO_CHAR (SYSDATE - 731, ''YYYY''))
                                             AND TO_NUMBER (TO_CHAR (SYSDATE + 731, ''YYYY''))
                ORDER BY start_date DESC ';

      l_load_stmt           VARCHAR2 (4000)
                               := l_create_table_stmt || l_select_stmt;

      l_index_stmt          VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( start_date,end_date,org_id)';
   BEGIN
      g_sec := 'Process 14 : Load GL Period Details ';
      write_log (g_line_open);
      write_log (g_sec);

      drop_temp_table (l_owner, l_table_name);

      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);
      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_gl_periods;


   PROCEDURE load_monthly_sales (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_monthly_sales $
     PURPOSE:     15. Procedure to Load monthly Sales information for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_MONTHLY_SALES##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt               VARCHAR2 (4000) := ' ';

      l_load_stmt                 CLOB;


      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id,org_id)';


      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*, 
                           b.jan_store_sale ,
                           b.feb_store_sale ,
                           b.mar_store_sale ,
                           b.apr_store_sale ,
                           b.may_store_sale ,
                           b.jun_store_sale ,
                           b.jul_store_sale ,
                           b.aug_store_sale ,
                           b.sep_store_sale ,
                           b.oct_store_sale ,
                           b.nov_store_sale ,
                           b.dec_store_sale                                 
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_monthly_sales## b
                 WHERE     a.organization_id = b.organization_id(+)
                       AND a.inventory_item_id = b.inventory_item_id(+)
                       AND a.org_id = b.org_id(+) ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec := 'Process 15 : Load Monthly Sales ';

      write_log (g_line_open);
      write_log (g_sec);

      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);

      l_load_stmt :=
            l_create_table_stmt
         || '       SELECT sales.inventory_item_id inventory_item_id,
                         sales.organization_id organization_id,
                         sales.org_id,
                         SUM (sales.jan1) jan_store_sale,
                         SUM (sales.feb1) feb_store_sale,
                         SUM (sales.mar1) mar_store_sale,
                         SUM (sales.apr1) apr_store_sale,
                         SUM (sales.may1) may_store_sale,
                         SUM (sales.june1) jun_store_sale,
                         SUM (sales.july1) jul_store_sale,
                         SUM (sales.aug1) aug_store_sale,
                         SUM (sales.sept1) sep_store_sale,
                         SUM (sales.oct1) oct_store_sale,
                         SUM (sales.nov1) nov_store_sale,
                         SUM (sales.dec1) dec_store_sale
                    FROM (SELECT gps.*,
                                 mdh.inventory_item_id,
                                 mdh.organization_id,
                                 mdh.sales_order_demand + NVL (std_wip_usage, 0),
                                 CASE
                                    WHEN gps.month_num = 1
                                    THEN
                                       mdh.sales_order_demand + NVL (std_wip_usage, 0)
                                 END
                                    jan1,
                                 CASE
                                    WHEN gps.month_num = 2
                                    THEN
                                       mdh.sales_order_demand + NVL (std_wip_usage, 0)
                                 END
                                    feb1,
                                 CASE
                                    WHEN gps.month_num = 3
                                    THEN
                                       mdh.sales_order_demand + NVL (std_wip_usage, 0)
                                 END
                                    mar1,
                                 CASE
                                    WHEN gps.month_num = 4
                                    THEN
                                       mdh.sales_order_demand + NVL (std_wip_usage, 0)
                                 END
                                    apr1,
                                 CASE
                                    WHEN gps.month_num = 5
                                    THEN
                                       mdh.sales_order_demand + NVL (std_wip_usage, 0)
                                 END
                                    may1,
                                 CASE
                                    WHEN gps.month_num = 6
                                    THEN
                                       mdh.sales_order_demand + NVL (std_wip_usage, 0)
                                 END
                                    june1,
                                 CASE
                                    WHEN gps.month_num = 7
                                    THEN
                                       mdh.sales_order_demand + NVL (std_wip_usage, 0)
                                 END
                                    july1,
                                 CASE
                                    WHEN gps.month_num = 8
                                    THEN
                                       mdh.sales_order_demand + NVL (std_wip_usage, 0)
                                 END
                                    aug1,
                                 CASE
                                    WHEN gps.month_num = 9
                                    THEN
                                       mdh.sales_order_demand + NVL (std_wip_usage, 0)
                                 END
                                    sept1,
                                 CASE
                                    WHEN gps.month_num = 10
                                    THEN
                                       mdh.sales_order_demand + NVL (std_wip_usage, 0)
                                 END
                                    oct1,
                                 CASE
                                    WHEN gps.month_num = 11
                                    THEN
                                       mdh.sales_order_demand + NVL (std_wip_usage, 0)
                                 END
                                    nov1,
                                 CASE
                                    WHEN gps.month_num = 12
                                    THEN
                                       mdh.sales_order_demand + NVL (std_wip_usage, 0)
                                 END
                                    dec1
                            FROM mtl_demand_histories mdh,
                                 xxwc.xxwc_isr_periods## gps,
                                 xxwc.xxwc_isr_organizations## org
                           WHERE     1 = 1
                                 AND mdh.period_type = 3
                                 AND mdh.period_start_date BETWEEN gps.start_date
                                                               AND gps.end_date
                                 AND mdh.organization_id = org.organization_id
                                 AND org.org_id = gps.org_id) sales
                GROUP BY sales.inventory_item_id, sales.organization_id, sales.org_id ';

      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);

      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_monthly_sales;



   PROCEDURE load_monthly_osales (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_monthly_osales $
     PURPOSE:     16. Procedure to Load monthly Sales for Other Inv orgs for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_MONTHLY_OSALES##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt               VARCHAR2 (4000) := ' ';

      l_load_stmt                 CLOB;


      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id,org_id)';


      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*, 
                           b.jan_other_inv_sale ,
                           b.feb_other_inv_sale ,
                           b.mar_other_inv_sale ,
                           b.apr_other_inv_sale ,
                           b.may_other_inv_sale ,
                           b.jun_other_inv_sale ,
                           b.jul_other_inv_sale ,
                           b.aug_other_inv_sale ,
                           b.sep_other_inv_sale ,
                           b.oct_other_inv_sale ,
                           b.nov_other_inv_sale ,
                           b.dec_other_inv_sale                                 
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_monthly_osales## b
                 WHERE     a.organization_id = b.organization_id(+)
                       AND a.inventory_item_id = b.inventory_item_id(+)
                       AND a.org_id = b.org_id(+) ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec := 'Process 16 : Load Monthly Sales from Other Inv Orgs';

      write_log (g_line_open);
      write_log (g_sec);

      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);

      l_load_stmt :=
            l_create_table_stmt
         || '            SELECT source.source_organization_id organization_id,        
                                sales.inventory_item_id, sales.org_id,        
                                SUM (sales.jan_store_sale) jan_other_inv_sale,        
                                SUM (sales.feb_store_sale) feb_other_inv_sale,        
                                SUM (sales.mar_store_sale) mar_other_inv_sale,        
                                SUM (sales.apr_store_sale) apr_other_inv_sale,        
                                SUM (sales.may_store_sale) may_other_inv_sale,        
                                SUM (sales.jun_store_sale) jun_other_inv_sale,        
                                SUM (sales.jul_store_sale) jul_other_inv_sale,        
                                SUM (sales.aug_store_sale) aug_other_inv_sale,        
                                SUM (sales.sep_store_sale) sep_other_inv_sale,        
                                SUM (sales.oct_store_sale) oct_other_inv_sale,        
                                SUM (sales.nov_store_sale) nov_other_inv_sale,        
                                SUM (sales.dec_store_sale) dec_other_inv_sale        
                           FROM xxwc.xxwc_isr_monthly_sales## sales, xxwc.xxwc_isr_items## source        
                          WHERE     sales.organization_id = source.organization_id        
                                AND sales.inventory_item_id = source.inventory_item_id        
                                AND source.source_organization_id IS NOT NULL        
                                AND sales.org_id = source.org_id        
                                AND source.organization_id <> source.source_organization_id        
                       GROUP BY source.source_organization_id, sales.inventory_item_id,sales.org_id     ';

      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);


      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_monthly_osales;


   PROCEDURE load_on_ord_qty (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_on_ord_qty $
     PURPOSE:     17. Procedure to Load On Order Quantity for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_ON_ORD##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      --  l_select_stmt               VARCHAR2 (4000);
      l_load_stmt                 CLOB;



      l_table_name1               VARCHAR2 (40) := 'XXWC_ISR_ON_ORD_QTY##';
      l_table_full_name1          VARCHAR2 (60) := l_owner || '.' || l_table_name1;
      l_index_name1               VARCHAR2 (60) := l_table_full_name1 || '_N1';
      l_table_success_msg1        VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load completed ';
      l_table_error_msg1          VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load failed ';
      l_success_msg1              VARCHAR2 (200);
      l_error_msg1                VARCHAR2 (200);
      l_index_success_msg1        VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg1          VARCHAR2 (200)
                                     := l_index_name1 || ' Index Creation failed ';
      l_create_table_stmt1        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name1 || ' AS ';
      l_select_stmt1              VARCHAR2 (4000)
         := '    SELECT org_id, inventory_item_id,
                        organization_id,
                        SUM (
                             (  quantity
                              - NVL (quantity_received, 0)
                              - NVL (quantity_cancelled, 0))
                           * po_uom_s.po_uom_convert (ordered_uom,
                                                      primary_uom,
                                                      inventory_item_id))
                           on_ord
                   FROM xxwc.xxwc_isr_on_ord##
               GROUP BY inventory_item_id, organization_id, org_id 
   ';
      l_load_stmt1                VARCHAR2 (4000)
                                     := l_create_table_stmt1 || l_select_stmt1;



      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id,org_id)';

      l_index_stmt1               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name1
            || ' ON '
            || l_table_full_name1
            || ' ( organization_id, inventory_item_id,org_id)';


      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*, b.on_ord
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_on_ord_qty## b
                 WHERE     a.organization_id = b.organization_id(+)
                       AND a.inventory_item_id = b.inventory_item_id(+)
                       AND a.org_id = b.org_id(+) ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec := 'Process 17 : Load On Order Qty ';
      write_log (g_line_open);
      write_log (g_sec);
      g_sec := 'Process 17a : Load On Order Transactions ';
      write_log (g_sec);

      l_load_stmt :=
            l_create_table_stmt
         || '  SELECT poh.org_id, pol.item_id inventory_item_id,        
                      poll.ship_to_organization_id organization_id,        
                      pol.unit_meas_lookup_code ordered_uom,        
                      msi.primary_unit_of_measure primary_uom,        
                      poll.quantity,        
                      poll.quantity_received,        
                      poll.quantity_cancelled        
                 FROM apps.po_headers_all poh,        
                      apps.po_line_locations_all poll,        
                      apps.po_lines_all pol,        
                      apps.po_releases_all por,        
                      apps.hr_locations_all_tl hl,        
                      xxwc.xxwc_isr_items## msi,        
                      apps.mtl_units_of_measure mum,        
                      apps.po_line_types_b polt,        
                      apps.gl_daily_conversion_types dct,        
                      apps.rcv_parameters rp        
                WHERE     NVL (poll.approved_flag, ''N'') = ''Y''        
                      AND NVL (poll.cancel_flag, ''N'') = ''N''        
                      AND NVL (pol.clm_info_flag, ''N'') = ''N''        
                      AND (   NVL (pol.clm_option_indicator, ''B'') <> ''O''        
                           OR NVL (pol.clm_exercised_flag, ''N'') = ''Y'')        
                      AND poll.closed_code IN (''OPEN'', ''CLOSED FOR INVOICE'')        
                      AND poll.shipment_type IN (''STANDARD'', ''BLANKET'', ''SCHEDULED'')        
                      AND poh.po_header_id = poll.po_header_id        
                      AND pol.po_line_id = poll.po_line_id        
                      AND poll.po_release_id = por.po_release_id(+)        
                      AND poll.ship_to_location_id = hl.location_id(+)        
                      AND pol.line_type_id = polt.line_type_id(+)        
                      AND mum.unit_of_measure(+) = pol.unit_meas_lookup_code        
                      AND NVL (msi.organization_id, poll.ship_to_organization_id) =        
                             poll.ship_to_organization_id        
                      AND msi.inventory_item_id(+) = pol.item_id        
                      AND dct.conversion_type(+) = poh.rate_type        
                      AND NVL (poh.consigned_consumption_flag, ''N'') = ''N''        
                      AND NVL (por.consigned_consumption_flag, ''N'') = ''N''        
                      AND NVL (poll.matching_basis, ''QUANTITY'') != ''AMOUNT''        
                      AND poll.payment_type IS NULL        
                      AND NVL (poll.drop_ship_flag, ''N'') = ''N''        
                      AND rp.organization_id = poll.ship_to_organization_id        
                      AND (   NVL (rp.pre_receive, ''N'') = ''N''        
                           OR (    NVL (rp.pre_receive, ''N'') = ''Y''        
                               AND NVL (poll.lcm_flag, ''N'') = ''N''))        
                      AND (  poll.quantity        
                           - NVL (poll.quantity_received, 0)        
                           - NVL (poll.quantity_cancelled, 0)) <> 0        
                      AND (   EXISTS        
                                 (SELECT ''Not associated to WIP Job''        
                                    FROM apps.po_distributions_all pod1,        
                                         apps.po_lines_all pltv        
                                   WHERE     pod1.po_line_id = pltv.po_line_id        
                                         AND pod1.line_location_id =        
                                                poll.line_location_id        
                                         AND pod1.wip_entity_id IS NULL)        
                           OR EXISTS        
                                 (SELECT ''Jobs not related to EAM WO or Closed WIP Jobs''        
                                    FROM apps.po_distributions_all pod1,        
                                         apps.po_lines_all pltv,        
                                         apps.wip_entities we        
                                   WHERE     pod1.po_line_id = pltv.po_line_id        
                                         AND pod1.line_location_id =        
                                                poll.line_location_id        
                                         AND pod1.wip_entity_id = we.wip_entity_id        
                                         AND we.entity_type NOT IN (6, 7, 3))        
                           OR EXISTS        
                                 (SELECT ''Open EAM WO Receipts''        
                                    FROM apps.po_distributions_all pod1,        
                                         apps.po_lines_all pltv,        
                                         apps.wip_entities we,        
                                         apps.wip_discrete_jobs wdj        
                                   WHERE     pod1.line_location_id =        
                                                poll.line_location_id        
                                         AND pod1.wip_entity_id = we.wip_entity_id        
                                         AND we.wip_entity_id = wdj.wip_entity_id        
                                         AND we.entity_type = 6        
                                         AND wdj.status_type IN (3, 4, 6)))  
UNION ALL            
               SELECT b.org_id, b.item_id inventory_item_id,
                      b.destination_organization_id organization_id ,
                      d.unit_of_measure ordered_uom,
                      c.primary_unit_of_measure primary_uom,
                      b.quantity,
                      b.quantity_received,
                      b.quantity_cancelled
                 FROM apps.po_requisition_lines_all b,
                      apps.po_requisition_headers_all a,
                      xxwc.xxwc_isr_items## c,
                      apps.mtl_units_of_measure d
                WHERE     b.source_type_code = ''INVENTORY''
                      AND NVL (b.cancel_flag, ''N'') = ''N''
                      AND a.type_lookup_code = ''INTERNAL''
                      AND a.authorization_status = ''APPROVED''
                      AND a.requisition_header_id = b.requisition_header_id
                      AND b.item_id = c.inventory_item_id
                      AND b.unit_meas_lookup_code = d.unit_of_measure
                      AND (  b.quantity
                           - NVL (b.quantity_received, 0)
                           - NVL (b.quantity_cancelled, 0)) <> 0
                      AND b.destination_organization_id = c.organization_id
                      AND NVL (b.drop_ship_flag, ''N'') = ''N''                                                
                ';



      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);


      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      ----------------------------------
      g_sec := 'Process 17b : Load On Order Quantity';

      write_log (g_sec);

      l_success_msg := l_table_success_msg1;
      l_error_msg := l_table_error_msg1;

      drop_temp_table (l_owner, l_table_name1);

      EXECUTE IMMEDIATE l_load_stmt1;

      write_log (l_success_msg);
      -----------------------------------

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);

      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_on_ord_qty;



   PROCEDURE load_int_req (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_int_req $
     PURPOSE:     18. Procedure to Load Internal Requisition Quantity for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_INT_REQ##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt               VARCHAR2 (4000) := ' ';

      l_load_stmt                 CLOB;


      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id,org_id)';


      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*, 
                           b.int_req                                 
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_int_req## b
                 WHERE     a.organization_id = b.organization_id(+)
                       AND a.inventory_item_id = b.inventory_item_id(+)
                       AND a.org_id = b.org_id(+) ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec := 'Process 18 : Load Internal Requisition Quantity';

      write_log (g_line_open);
      write_log (g_sec);

      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);

      l_load_stmt :=
            l_create_table_stmt
         || ' SELECT NVL (SUM (prl.quantity), 0) int_req,
                     item_id inventory_item_id,
                     destination_organization_id organization_id,
                     prl.org_id
                FROM po_requisition_headers_all prh, po_requisition_lines_all prl
               WHERE     prh.requisition_header_id = prl.requisition_header_id
                     AND NVL (prh.cancel_flag, ''N'') = ''N''
                     AND NVL (prl.cancel_flag, ''N'') = ''N''
                     AND NVL (prl.closed_code, ''OPEN'') = ''OPEN''
                     AND prh.authorization_status = ''INCOMPLETE''
                     AND source_type_code = ''INVENTORY''
                     AND NOT EXISTS
                                (SELECT 1
                                   FROM po_action_history pah
                                  WHERE     pah.object_id = prh.requisition_header_id
                                        AND pah.object_type_code = ''REQUISITION''
                                        AND pah.action_code = ''CANCEL'')
            GROUP BY item_id, destination_organization_id, prl.org_id';

      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);


      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_int_req;



   PROCEDURE load_po_req (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_po_req $
     PURPOSE:     19. Procedure to Load Direct and Open Requisition Quantities for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_VMI##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt               VARCHAR2 (4000)
         := '    SELECT DISTINCT po_line_location_id, req_line_id, NVL(b.vmi_flag,''N'') vmi_flag
                 FROM mtl_supply a, po_line_locations_all b
                WHERE a.po_line_location_id = b.line_location_id
                  AND NVL(b.vmi_flag,''N'') =''N''                
               UNION ALL
               SELECT DISTINCT po_line_location_id, req_line_id, NVL(b.vmi_flag,''N'') vmi_flag
                 FROM mtl_supply a, po_requisition_lines_all b, xxwc.xxwc_isr_organizations## c 
                WHERE a.req_line_id = requisition_line_id AND b.source_type_code = ''VENDOR''
                  AND  b.line_location_id is null
                  AND NVL(b.vmi_flag,''N'') =''N''
                  AND b.destination_organization_id <> c.master_organization_id                  ';


      l_load_stmt                 VARCHAR2 (4000)
                                     := l_create_table_stmt || l_select_stmt;
      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id)';



      l_table_name1               VARCHAR2 (40) := 'XXWC_ISR_SUPPLY##';
      l_table_full_name1          VARCHAR2 (60) := l_owner || '.' || l_table_name1;
      l_index_name1               VARCHAR2 (60) := l_table_full_name1 || '_N1';
      l_table_success_msg1        VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load completed ';
      l_table_error_msg1          VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load failed ';
      l_success_msg1              VARCHAR2 (200);
      l_error_msg1                VARCHAR2 (200);
      l_index_success_msg1        VARCHAR2 (200)
         := l_index_name1 || ' Index Creation completed ';
      l_index_error_msg1          VARCHAR2 (200)
                                     := l_index_name1 || ' Index Creation failed ';
      l_create_table_stmt1        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name1 || ' AS ';
      l_select_stmt1              VARCHAR2 (4000)
         := '   SELECT sup.*,
                       NVL (
                          (SELECT vmi_flag
                             FROM xxwc.xxwc_isr_vmi##
                            WHERE    req_line_id = sup.req_line_id
                                  OR po_line_location_id = sup.po_line_location_id),
                          ''Y'')
                          vmi_flag,
                       (SELECT COUNT (*)
                          FROM oe_drop_ship_sources
                         WHERE    requisition_line_id = sup.req_line_id
                               OR line_location_id = sup.po_line_location_id)
                          drop_ship
                  FROM mtl_supply sup';
      l_load_stmt1                VARCHAR2 (4000)
                                     := l_create_table_stmt1 || l_select_stmt1;


      l_table_name2               VARCHAR2 (40) := 'XXWC_ISR_REQ_QTY##';
      l_table_full_name2          VARCHAR2 (60) := l_owner || '.' || l_table_name2;
      l_index_name2               VARCHAR2 (60) := l_table_full_name2 || '_N1';
      l_table_success_msg2        VARCHAR2 (200)
         := l_table_full_name2 || ' Table Load completed ';
      l_table_error_msg2          VARCHAR2 (200)
         := l_table_full_name2 || ' Table Load failed ';
      l_success_msg2              VARCHAR2 (200);
      l_error_msg2                VARCHAR2 (200);
      l_index_success_msg2        VARCHAR2 (200)
         := l_index_name2 || ' Index Creation completed ';
      l_index_error_msg2          VARCHAR2 (200)
                                     := l_index_name2 || ' Index Creation failed ';
      l_create_table_stmt2        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name2 || ' AS ';
      l_select_stmt2              VARCHAR2 (4000)
         := '   SELECT prh.org_id, sup.to_organization_id organization_id
                    ,sup.item_id inventory_item_id                   
                    ,SUM (
                         CASE
                             WHEN     sup.vmi_flag = ''N''
                                  AND sup.drop_ship = 0
                             THEN
                                 NVL (to_org_primary_quantity, 0)
                             ELSE
                                 0
                         END)
                         open_req
                    ,SUM (
                         CASE
                             WHEN     sup.vmi_flag = ''N''
                                  AND sup.drop_ship > 0
                             THEN
                                 NVL (to_org_primary_quantity, 0)                                                    
                             ELSE
                                 0
                         END)
                         dir_req
                FROM xxwc.xxwc_isr_supply## sup, apps.po_requisition_headers_all prh,xxwc.xxwc_isr_items## items
               WHERE sup.supply_type_code IN (''REQ'')
                     AND sup.destination_type_code = ''INVENTORY''
                     AND sup.to_organization_id = items.organization_id
                     AND sup.item_id = items.inventory_item_id
                     AND prh.requisition_header_id = sup.req_header_id
                     AND (   NVL (sup.from_organization_id, -1) <> items.organization_id
                          OR (    sup.from_organization_id = items.organization_id
                              AND EXISTS
                                      (SELECT ''x''
                                         FROM apps.mtl_secondary_inventories sub1
                                        WHERE     sub1.organization_id = sup.from_organization_id
                                              AND sub1.secondary_inventory_name = sup.from_subinventory
                                              AND sub1.availability_type <> 1)))
                     AND (   sup.to_subinventory IS NULL
                          OR (EXISTS
                                  (SELECT ''x''
                                     FROM apps.mtl_secondary_inventories sub2
                                    WHERE     sub2.secondary_inventory_name = sup.to_subinventory
                                          AND sub2.organization_id = sup.to_organization_id
                                          AND sub2.availability_type = DECODE (2, 1, sub2.availability_type, 1))))
                     AND (   sup.po_line_location_id IS NULL
                          OR EXISTS
                                 (SELECT ''x''
                                    FROM apps.po_line_locations_all lilo
                                   WHERE     lilo.line_location_id = sup.po_line_location_id
                                         AND NVL (lilo.vmi_flag, ''N'') = ''N''))
            GROUP BY prh.org_id, sup.to_organization_id, sup.item_id ';
      l_load_stmt2                VARCHAR2 (4000)
                                     := l_create_table_stmt2 || l_select_stmt2;

      l_index_stmt2               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name2
            || ' ON '
            || l_table_full_name2
            || ' ( organization_id, inventory_item_id,org_id)';


      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*, b.dir_req, b.open_req
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_req_qty## b
                 WHERE     a.organization_id = b.organization_id(+)
                       AND a.inventory_item_id = b.inventory_item_id(+)
                       AND a.org_id = b.org_id(+) ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec := 'Process 19 : Load Vendor Requisition quantity  ';
      write_log (g_line_open);
      write_log (g_sec);
      g_sec := 'Process 19a : Load Vendor Managed Inventory Reqs ';
      write_log (g_sec);


      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);


      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      ----------------------------------
      g_sec := 'Process 19b : Load non VMI supply';

      write_log (g_sec);

      l_success_msg := l_table_success_msg1;
      l_error_msg := l_table_error_msg1;

      drop_temp_table (l_owner, l_table_name1);

      EXECUTE IMMEDIATE l_load_stmt1;

      write_log (l_success_msg);

      -----------------------------------

      g_sec := 'Process 19c : Load Open and Direct Requisitions ';
      write_log (g_sec);

      l_success_msg := l_table_success_msg2;
      l_error_msg := l_table_error_msg2;

      drop_temp_table (l_owner, l_table_name2);

      EXECUTE IMMEDIATE l_load_stmt2;

      write_log (l_success_msg);

      -----------------------------------

      l_success_msg := l_index_success_msg2;
      l_error_msg := l_index_error_msg2;

      EXECUTE IMMEDIATE l_index_stmt2;

      write_log (l_success_msg);


      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_po_req;


   PROCEDURE load_reserv_qty (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_reserv_qty $
     PURPOSE:     20. Procedure to Load reserved quantity for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode             NUMBER := 0;
      l_owner               VARCHAR2 (10) := 'XXWC';
      l_table_name          VARCHAR2 (40) := 'XXWC_ISR_RESERV_QTY##';
      l_table_full_name     VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name          VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg   VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg     VARCHAR2 (200)
                               := l_table_full_name || ' Table Load failed ';
      l_success_msg         VARCHAR2 (200);
      l_error_msg           VARCHAR2 (200);
      l_index_success_msg   VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg     VARCHAR2 (200)
                               := l_index_name || ' Index Creation failed ';
      l_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt         VARCHAR2 (4000)
         := '   SELECT NVL (SUM (mr.reservation_quantity), 0) reserv_qty,
                 mr.inventory_item_id,
                 mr.organization_id
            FROM mtl_reservations mr
        GROUP BY mr.inventory_item_id, mr.organization_id ';

      l_load_stmt           VARCHAR2 (4000)
                               := l_create_table_stmt || l_select_stmt;


      l_index_stmt          VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id)';
   BEGIN
      g_sec := 'Process 20 : Load Reserv Quantity';

      write_log (g_line_open);
      write_log (g_sec);

      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);

      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);


      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_reserv_qty;



   PROCEDURE load_qoh (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_qoh $
     PURPOSE:     21. Procedure to Load reserved quantity for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_MOQ_NET_QTY##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt               VARCHAR2 (4000)
         := '    SELECT mon.organization_id,                        
                        mon.inventory_item_id,                        
                        SUM (mon.primary_transaction_quantity) moq_qty                        
                   FROM apps.mtl_onhand_net mon, mtl_lot_numbers mln                        
                  WHERE     mon.organization_id =                        
                               NVL (mon.planning_organization_id, mon.organization_id)                        
                        AND mon.lot_number = mln.lot_number(+)                        
                        AND mon.organization_id = mln.organization_id(+)                        
                        AND mon.inventory_item_id = mln.inventory_item_id(+)                        
                        AND TRUNC (NVL (mln.expiration_date, SYSDATE + 1)) >                        
                               TRUNC (SYSDATE)                        
                        AND NVL (mon.planning_tp_type, 2) = 2                        
               GROUP BY mon.organization_id, mon.inventory_item_id     ';

      l_load_stmt                 VARCHAR2 (4000)
                                     := l_create_table_stmt || l_select_stmt;


      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id)';


      l_table_name1               VARCHAR2 (40) := 'XXWC_ISR_FULL_LOT_SRC_QTY##';
      l_table_full_name1          VARCHAR2 (60) := l_owner || '.' || l_table_name1;
      l_index_name1               VARCHAR2 (60) := l_table_full_name1 || '_N1';
      l_table_success_msg1        VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load completed ';
      l_table_error_msg1          VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load failed ';
      l_success_msg1              VARCHAR2 (200);
      l_error_msg1                VARCHAR2 (200);
      l_index_success_msg1        VARCHAR2 (200)
         := l_index_name1 || ' Index Creation completed ';
      l_index_error_msg1          VARCHAR2 (200)
                                     := l_index_name1 || ' Index Creation failed ';
      l_create_table_stmt1        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name1 || ' AS ';
      --  l_select_stmt1              VARCHAR2 (4000);


      l_load_stmt1                CLOB;


      l_index_stmt1               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name1
            || ' ON '
            || l_table_full_name1
            || ' ( organization_id, inventory_item_id)';



      l_table_name2               VARCHAR2 (40) := 'XXWC_ISR_FULL_LOT_DEST_QTY##';
      l_table_full_name2          VARCHAR2 (60) := l_owner || '.' || l_table_name2;
      l_index_name2               VARCHAR2 (60) := l_table_full_name2 || '_N1';
      l_table_success_msg2        VARCHAR2 (200)
         := l_table_full_name2 || ' Table Load completed ';
      l_table_error_msg2          VARCHAR2 (200)
         := l_table_full_name2 || ' Table Load failed ';
      l_success_msg2              VARCHAR2 (200);
      l_error_msg2                VARCHAR2 (200);
      l_index_success_msg2        VARCHAR2 (200)
         := l_index_name2 || ' Index Creation completed ';
      l_index_error_msg2          VARCHAR2 (200)
                                     := l_index_name2 || ' Index Creation failed ';
      l_create_table_stmt2        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name2 || ' AS ';



      l_index_stmt2               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name2
            || ' ON '
            || l_table_full_name2
            || ' ( organization_id, inventory_item_id)';


      l_table_name3               VARCHAR2 (40) := 'XXWC_ISR_NON_LOT_SRC_QTY##';
      l_table_full_name3          VARCHAR2 (60) := l_owner || '.' || l_table_name3;
      l_index_name3               VARCHAR2 (60) := l_table_full_name3 || '_N1';
      l_table_success_msg3        VARCHAR2 (300)
         := l_table_full_name3 || ' Table Load completed ';
      l_table_error_msg3          VARCHAR2 (200)
         := l_table_full_name3 || ' Table Load failed ';
      l_success_msg3              VARCHAR2 (200);
      l_error_msg3                VARCHAR2 (200);
      l_index_success_msg3        VARCHAR2 (200)
         := l_index_name3 || ' Index Creation completed ';
      l_index_error_msg3          VARCHAR2 (200)
                                     := l_index_name3 || ' Index Creation failed ';
      l_create_table_stmt3        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name3 || ' AS ';
      l_select_stmt3              VARCHAR2 (4000)
         := '    SELECT mmtt.organization_id,        
                        mmtt.inventory_item_id,        
                        SUM (        
                             DECODE (mmtt.transaction_action_id,        
                                     1, -1,        
                                     2, -1,        
                                     28, -1,        
                                     3, -1,        
                                     SIGN (mmtt.primary_quantity))        
                           * ABS (mmtt.primary_quantity))        
                           mmtt_qty_src        
                   FROM apps.mtl_material_transactions_temp mmtt        
                  WHERE     mmtt.posting_flag = ''Y''        
                        AND mmtt.subinventory_code IS NOT NULL        
                        AND NVL (mmtt.transaction_status, 0) <> 2        
                        AND mmtt.transaction_action_id NOT IN (24, 30)        
                        AND EXISTS        
                               (SELECT ''x''        
                                  FROM mtl_secondary_inventories msi        
                                 WHERE     msi.organization_id = mmtt.organization_id        
                                       AND msi.secondary_inventory_name =        
                                              mmtt.subinventory_code        
                                       AND msi.availability_type = 1)        
                        AND mmtt.planning_organization_id IS NULL        
                        AND (   mmtt.locator_id IS NULL        
                             OR (    mmtt.locator_id IS NOT NULL        
                                 AND EXISTS        
                                        (SELECT ''x''        
                                           FROM mtl_item_locations mil        
                                          WHERE     mmtt.organization_id =        
                                                       mil.organization_id        
                                                AND mmtt.locator_id =        
                                                       mil.inventory_location_id        
                                                AND mmtt.subinventory_code =        
                                                       mil.subinventory_code        
                                                AND mil.availability_type = 1)))        
                        AND NVL (mmtt.planning_tp_type, 2) = 2        
               GROUP BY mmtt.organization_id, mmtt.inventory_item_id ';

      l_load_stmt3                VARCHAR2 (4000)
                                     := l_create_table_stmt3 || l_select_stmt3;


      l_index_stmt3               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name3
            || ' ON '
            || l_table_full_name3
            || ' ( organization_id, inventory_item_id)';


      l_table_name4               VARCHAR2 (40) := 'XXWC_ISR_NON_LOT_DEST_QTY##';
      l_table_full_name4          VARCHAR2 (60) := l_owner || '.' || l_table_name4;
      l_index_name4               VARCHAR2 (60) := l_table_full_name4 || '_N1';
      l_table_success_msg4        VARCHAR2 (300)
         := l_table_full_name4 || ' Table Load completed ';
      l_table_error_msg4          VARCHAR2 (200)
         := l_table_full_name4 || ' Table Load failed ';
      l_success_msg4              VARCHAR2 (200);
      l_error_msg4                VARCHAR2 (200);
      l_index_success_msg4        VARCHAR2 (200)
         := l_index_name4 || ' Index Creation completed ';
      l_index_error_msg4          VARCHAR2 (200)
                                     := l_index_name4 || ' Index Creation failed ';
      l_create_table_stmt4        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name4 || ' AS ';
      l_select_stmt4              VARCHAR2 (4000)
         := '      SELECT DECODE (mmtt.transaction_action_id,        
                                3, mmtt.transfer_organization,        
                                mmtt.organization_id)        
                           organization_id,        
                        mmtt.inventory_item_id,        
                        SUM (ABS (mmtt.primary_quantity)) mmtt_qty_dest        
                   FROM apps.mtl_material_transactions_temp mmtt        
                  WHERE     mmtt.posting_flag = ''Y''        
                        AND NVL (mmtt.transaction_status, 0) <> 2        
                        AND mmtt.transaction_action_id IN (2, 28, 3)        
                        AND (   (mmtt.transfer_subinventory IS NULL)        
                             OR (    mmtt.transfer_subinventory IS NOT NULL        
                                 AND EXISTS        
                                        (SELECT ''x''        
                                           FROM mtl_secondary_inventories msi        
                                          WHERE     msi.organization_id =        
                                                       DECODE (        
                                                          mmtt.transaction_action_id,        
                                                          3, mmtt.transfer_organization,        
                                                          mmtt.organization_id)        
                                                AND msi.secondary_inventory_name =        
                                                       mmtt.transfer_subinventory        
                                                AND msi.availability_type = 1)))        
                        AND mmtt.planning_organization_id IS NULL        
                        AND (   mmtt.transfer_to_location IS NULL        
                             OR (    mmtt.transfer_to_location IS NOT NULL        
                                 AND EXISTS        
                                        (SELECT ''x''        
                                           FROM mtl_item_locations mil        
                                          WHERE     DECODE (mmtt.transaction_action_id,        
                                                            3, mmtt.transfer_organization,        
                                                            mmtt.organization_id) =        
                                                       mil.organization_id        
                                                AND mmtt.transfer_to_location =        
                                                       mil.inventory_location_id        
                                                AND mmtt.transfer_subinventory =        
                                                       mil.subinventory_code        
                                                AND mil.availability_type = 1)))        
                        AND NVL (mmtt.planning_tp_type, 2) = 2        
               GROUP BY DECODE (mmtt.transaction_action_id,        
                                3, mmtt.transfer_organization,        
                                mmtt.organization_id),        
                        mmtt.inventory_item_id        ';

      l_load_stmt4                VARCHAR2 (4000)
                                     := l_create_table_stmt4 || l_select_stmt4;


      l_index_stmt4               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name4
            || ' ON '
            || l_table_full_name4
            || ' ( organization_id, inventory_item_id)';



      l_table_name5               VARCHAR2 (40) := 'XXWC_ISR_LPN_QTY##';
      l_table_full_name5          VARCHAR2 (60) := l_owner || '.' || l_table_name5;
      l_index_name5               VARCHAR2 (60) := l_table_full_name5 || '_N1';
      l_table_success_msg5        VARCHAR2 (300)
         := l_table_full_name5 || ' Table Load completed ';
      l_table_error_msg5          VARCHAR2 (200)
         := l_table_full_name5 || ' Table Load failed ';
      l_success_msg5              VARCHAR2 (200);
      l_error_msg5                VARCHAR2 (200);
      l_index_success_msg5        VARCHAR2 (200)
         := l_index_name5 || ' Index Creation completed ';
      l_index_error_msg5          VARCHAR2 (200)
                                     := l_index_name5 || ' Index Creation failed ';
      l_create_table_stmt5        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name5 || ' AS ';
      l_select_stmt5              VARCHAR2 (4000)
         := '      SELECT mtrl.organization_id,                        
                        mtrl.inventory_item_id,                        
                        SUM (apps.inv_decimals_pub.get_primary_quantity (                        
                                mtrl.organization_id,                        
                                mtrl.inventory_item_id,                        
                                mtrl.uom_code,                        
                                mtrl.quantity - NVL (mtrl.quantity_delivered, 0)))                        
                           lpn_qty                        
                   FROM mtl_txn_request_lines mtrl,                        
                        mtl_txn_request_headers mtrh,                        
                        mtl_transaction_types mtt                        
                  WHERE     mtrl.header_id = mtrh.header_id                        
                        AND mtrh.move_order_type = 6                 -- Putaway Move Order                        
                        AND mtrl.transaction_source_type_id = 5                     -- Wip                        
                        AND mtt.transaction_action_id = 31      -- WIP Assembly Completion                        
                        AND mtt.transaction_type_id = mtrl.transaction_type_id                        
                        AND mtrl.line_status = 7                           -- Pre Approved                        
                        AND mtrl.lpn_id IS NOT NULL                        
               GROUP BY mtrl.organization_id, mtrl.inventory_item_id ';

      l_load_stmt5                VARCHAR2 (4000)
                                     := l_create_table_stmt5 || l_select_stmt5;


      l_index_stmt5               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name5
            || ' ON '
            || l_table_full_name5
            || ' ( organization_id, inventory_item_id)';



      l_table_name6               VARCHAR2 (40) := 'XXWC_ISR_QOH##';
      l_table_full_name6          VARCHAR2 (60) := l_owner || '.' || l_table_name6;
      l_index_name6               VARCHAR2 (60) := l_table_full_name6 || '_N1';
      l_table_success_msg6        VARCHAR2 (300)
         := l_table_full_name6 || ' Table Load completed ';
      l_table_error_msg6          VARCHAR2 (200)
         := l_table_full_name6 || ' Table Load failed ';
      l_success_msg6              VARCHAR2 (200);
      l_error_msg6                VARCHAR2 (200);
      l_index_success_msg6        VARCHAR2 (200)
         := l_index_name6 || ' Index Creation completed ';
      l_index_error_msg6          VARCHAR2 (200)
                                     := l_index_name6 || ' Index Creation failed ';
      l_create_table_stmt6        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name6 || ' AS ';
      l_select_stmt6              VARCHAR2 (4000)
         := '     SELECT items.org_id,
                       items.organization_id,
                       items.inventory_item_id,
                       1 lot_control_code,
                         NVL(moq.moq_qty,0)
                       + NVL (src.mmtt_qty_src, 0)
                       + NVL (dest.mmtt_qty_dest, 0)
                       + NVL (lpn.lpn_qty, 0)
                          qoh
                  FROM xxwc.xxwc_isr_items## items,
                       xxwc.xxwc_isr_moq_net_qty## moq,
                       xxwc.xxwc_isr_non_lot_src_qty## src,
                       xxwc.xxwc_isr_non_lot_dest_qty## dest,
                       xxwc.xxwc_isr_lpn_qty## lpn
                 WHERE     items.inventory_item_id = moq.inventory_item_id(+)
                       AND items.organization_id = moq.organization_id(+)
                       AND items.inventory_item_id = src.inventory_item_id(+)
                       AND items.organization_id = src.organization_id(+)
                       AND items.inventory_item_id = dest.inventory_item_id(+)
                       AND items.organization_id = dest.organization_id(+)
                       AND items.inventory_item_id = lpn.inventory_item_id(+)
                       AND items.organization_id = lpn.organization_id(+)
                UNION
                SELECT items.org_id,
                       items.organization_id,
                       items.inventory_item_id,
                       2 lot_control_code,
                         NVL(moq.moq_qty,0)
                       + NVL (src.mmtt_qty_src, 0)
                       + NVL (dest.mmtt_qty_dest, 0)
                       + NVL (lpn.lpn_qty, 0)
                          qoh
                  FROM xxwc.xxwc_isr_items## items,
                       xxwc.xxwc_isr_moq_net_qty## moq,
                       xxwc.xxwc_isr_full_lot_src_qty## src,
                       xxwc.xxwc_isr_full_lot_dest_qty## dest,
                       xxwc.xxwc_isr_lpn_qty## lpn
                 WHERE     items.inventory_item_id = moq.inventory_item_id(+)
                       AND items.organization_id = moq.organization_id(+)
                       AND items.inventory_item_id = src.inventory_item_id(+)
                       AND items.organization_id = src.organization_id(+)
                       AND items.inventory_item_id = dest.inventory_item_id(+)
                       AND items.organization_id = dest.organization_id(+)
                       AND items.inventory_item_id = lpn.inventory_item_id(+)
                       AND items.organization_id = lpn.organization_id(+)';

      l_load_stmt6                VARCHAR2 (4000)
                                     := l_create_table_stmt6 || l_select_stmt6;


      l_index_stmt6               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name6
            || ' ON '
            || l_table_full_name6
            || ' ( organization_id, inventory_item_id, lot_control_code)';



      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '      SELECT a.*, b.qoh, (B.qoh - NVL (C.reserv_qty, 0)) available
                  FROM xxwc.xxwc_isr_details## a,
                       xxwc.xxwc_isr_qoh## b,
                       xxwc.xxwc_isr_reserv_qty## c
                 WHERE     a.organization_id = b.organization_id(+)
                       AND a.inventory_item_id = b.inventory_item_id(+)
                       AND a.lot_control_code = b.lot_control_code(+)
                       AND a.org_id = b.org_id(+)
                       AND a.organization_id = c.organization_id(+)
                       AND a.inventory_item_id = c.inventory_item_id(+)   ';


      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec := 'Process 21 : Load QOH ';

      write_log (g_line_open);
      write_log (g_sec);

      g_sec := 'Process 21a : Load Nettable onhand Quantity ';

      write_log (g_sec);

      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);

      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      ----------------------------------------

      g_sec := 'Process 21b : Load Full lot control Source Qty ';

      write_log (g_sec);

      l_success_msg := l_table_success_msg1;
      l_error_msg := l_table_error_msg1;

      drop_temp_table (l_owner, l_table_name1);

      EXECUTE IMMEDIATE
            l_create_table_stmt1
         || '   SELECT mmtt.organization_id,                    
                        mmtt.inventory_item_id,                    
                        SUM (                    
                             DECODE (mmtt.transaction_action_id,                    
                                     1, -1,                    
                                     2, -1,                    
                                     28, -1,                    
                                     3, -1,                    
                                     SIGN (mmtt.primary_quantity))                    
                           * ABS (mmtt.primary_quantity))                    
                           mmtt_qty_src                    
                   FROM apps.mtl_material_transactions_temp mmtt                    
                  WHERE     mmtt.posting_flag = ''Y''                    
                        AND mmtt.subinventory_code IS NOT NULL                    
                        AND NVL (mmtt.transaction_status, 0) <> 2                    
                        AND mmtt.transaction_action_id NOT IN (24, 30)                    
                        AND EXISTS                    
                               (SELECT ''x''                    
                                  FROM mtl_secondary_inventories msi                    
                                 WHERE     msi.organization_id = mmtt.organization_id                    
                                       AND msi.secondary_inventory_name =                    
                                              mmtt.subinventory_code                    
                                       AND msi.availability_type = 1)                    
                        AND mmtt.planning_organization_id IS NULL                    
                        AND EXISTS                    
                               (SELECT ''x''                    
                                  FROM apps.mtl_transaction_lots_temp mtlt,                    
                                       mtl_lot_numbers mln                    
                                 WHERE     mtlt.transaction_temp_id =                    
                                              mmtt.transaction_temp_id                    
                                       AND mtlt.lot_number = mln.lot_number(+)                    
                                       AND mmtt.organization_id = mln.organization_id(+)                    
                                       AND mmtt.inventory_item_id =                    
                                              mln.inventory_item_id(+)                    
                                       AND NVL (mln.availability_type, 2) = 1                    
                                       AND TRUNC (                    
                                              NVL (                    
                                                 NVL (mtlt.lot_expiration_date,                    
                                                      mln.expiration_Date),                    
                                                 SYSDATE + 1)) > TRUNC (SYSDATE))                    
                        AND (   mmtt.locator_id IS NULL                    
                             OR (    mmtt.locator_id IS NOT NULL                    
                                 AND EXISTS                    
                                        (SELECT ''x''                    
                                           FROM mtl_item_locations mil                    
                                          WHERE     mmtt.organization_id =                    
                                                       mil.organization_id                    
                                                AND mmtt.locator_id =                    
                                                       mil.inventory_location_id                    
                                                AND mmtt.subinventory_code =                    
                                                       mil.subinventory_code                    
                                                AND mil.availability_type = 1)))                    
                        AND NVL (mmtt.planning_tp_type, 2) = 2                    
               GROUP BY mmtt.organization_id, mmtt.inventory_item_id';

      write_log (l_success_msg);


      -----------------------------------------

      g_sec := 'Process 21c : Load Full lot control Destination Qty ';

      write_log (g_sec);

      l_success_msg := l_table_success_msg2;
      l_error_msg := l_table_error_msg2;

      drop_temp_table (l_owner, l_table_name2);

      EXECUTE IMMEDIATE
            l_create_table_stmt2
         || '    SELECT DECODE (mmtt.transaction_action_id,                    
                                3, mmtt.transfer_organization,                    
                                mmtt.organization_id)                    
                           organization_id,                    
                        mmtt.inventory_item_id,                    
                        SUM (ABS (mmtt.primary_quantity)) mmtt_qty_dest                    
                   FROM apps.mtl_material_transactions_temp mmtt                    
                  WHERE     mmtt.posting_flag = ''Y''                    
                        AND NVL (mmtt.transaction_status, 0) <> 2                    
                        AND mmtt.transaction_action_id IN (2, 28, 3)                    
                        AND (   (mmtt.transfer_subinventory IS NULL)                    
                             OR (    mmtt.transfer_subinventory IS NOT NULL                    
                                 AND EXISTS                    
                                        (SELECT ''x''                    
                                           FROM mtl_secondary_inventories msi                    
                                          WHERE     msi.organization_id =                    
                                                       DECODE (                    
                                                          mmtt.transaction_action_id,                    
                                                          3, mmtt.transfer_organization,                    
                                                          mmtt.organization_id)                    
                                                AND msi.secondary_inventory_name =                    
                                                       mmtt.transfer_subinventory                    
                                                AND msi.availability_type = 1)))                    
                        AND mmtt.planning_organization_id IS NULL                    
                        AND EXISTS                    
                               (SELECT ''x''                    
                                  FROM apps.mtl_transaction_lots_temp mtlt,                    
                                       mtl_lot_numbers mln                    
                                 WHERE     mtlt.transaction_temp_id =                    
                                              mmtt.transaction_temp_id                    
                                       AND mtlt.lot_number = mln.lot_number(+)                    
                                       AND DECODE (mmtt.transaction_action_id,                    
                                                   3, mmtt.transfer_organization,                    
                                                   mmtt.organization_id) =                    
                                              mln.organization_id(+)                    
                                       AND mmtt.inventory_item_id =                    
                                              mln.inventory_item_id(+)                    
                                       AND NVL (mln.availability_type, 2) = 1                    
                                       AND TRUNC (                    
                                              NVL (                    
                                                 NVL (mtlt.lot_expiration_Date,                    
                                                      mln.expiration_date),                    
                                                 SYSDATE + 1)) > TRUNC (SYSDATE))                    
                        AND (   mmtt.transfer_to_location IS NULL                    
                             OR (    mmtt.transfer_to_location IS NOT NULL                    
                                 AND EXISTS                    
                                        (SELECT ''x''                    
                                           FROM mtl_item_locations mil                    
                                          WHERE     DECODE (mmtt.transaction_action_id,                    
                                                            3, mmtt.transfer_organization,                    
                                                            mmtt.organization_id) =                    
                                                       mil.organization_id                    
                                                AND mmtt.transfer_to_location =                    
                                                       mil.inventory_location_id                    
                                                AND mmtt.transfer_subinventory =                    
                                                       mil.subinventory_code                    
                                                AND mil.availability_type = 1)))                    
               GROUP BY DECODE (mmtt.transaction_action_id,                    
                                3, mmtt.transfer_organization,                    
                                mmtt.organization_id),                    
                        mmtt.inventory_item_id    ';

      write_log (l_success_msg);


      -----------------------------------------

      g_sec := 'Process 21d : Load Non lot control Source Qty ';

      write_log (g_sec);


      l_success_msg := l_table_success_msg3;
      l_error_msg := l_table_error_msg3;

      drop_temp_table (l_owner, l_table_name3);

      EXECUTE IMMEDIATE l_load_stmt3;

      write_log (l_success_msg);

      -----------------------------------------

      g_sec := 'Process 21e : Load Non lot control Destination Qty';

      write_log (g_sec);

      l_success_msg := l_table_success_msg4;
      l_error_msg := l_table_error_msg4;

      drop_temp_table (l_owner, l_table_name4);

      EXECUTE IMMEDIATE l_load_stmt4;

      write_log (l_success_msg);

      -----------------------------------------

      g_sec := 'Process 21f : Load LPN Qty ';

      write_log (g_sec);


      l_success_msg := l_table_success_msg5;
      l_error_msg := l_table_error_msg5;

      drop_temp_table (l_owner, l_table_name5);

      EXECUTE IMMEDIATE l_load_stmt5;

      write_log (l_success_msg);

      -----------------------------------------

      g_sec := 'Process 21g : Load QOH ';

      write_log (g_sec);


      l_success_msg := l_table_success_msg6;
      l_error_msg := l_table_error_msg6;

      drop_temp_table (l_owner, l_table_name6);

      EXECUTE IMMEDIATE l_load_stmt6;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);


      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_qoh;


   PROCEDURE load_demand (p_retcode OUT VARCHAR2)
   /**********************************************************************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_demand $
     PURPOSE:     22. Procedure to Load Demand Quantity for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    -----------------------------------------------------------------------------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     5.0    15-Oct-15    P.Vamshidhar          TMS#20151008-00138  - Minor enhancements and Bug fixes for New ISR Data Mart and
                         Manjula Chellappan                          Inventory Sales and Reorder Report - WC.

     ******************************************************************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_UNRES_DEM_QTY##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt               VARCHAR2 (4000)
         := '    SELECT organization_id,
                        inventory_item_id,
                        SUM (
                             primary_uom_quantity
                           - GREATEST (NVL (reservation_quantity, 0),
                                       NVL (completed_quantity, 0)))
                           unres_dem_qty
                   FROM mtl_demand md
                  WHERE     md.reservation_type = 1
                        AND md.parent_demand_id IS NULL
                        AND md.primary_uom_quantity >
                               GREATEST (NVL (md.reservation_quantity, 0),
                                         NVL (md.completed_quantity, 0))
                        AND md.demand_source_type NOT IN (2, 8, 12)
                        AND (   md.subinventory IS NULL
                             OR EXISTS
                                   (SELECT 1
                                      FROM mtl_secondary_inventories s
                                     WHERE     s.organization_id = md.organization_id
                                           AND s.secondary_inventory_name = md.subinventory
                                           AND s.availability_type = 1))
                        AND (   locator_id IS NULL
                             OR EXISTS
                                   (SELECT 1
                                      FROM mtl_item_locations mil
                                     WHERE     mil.organization_id = md.organization_id
                                           AND mil.inventory_location_id = md.locator_id
                                           AND mil.subinventory_code =
                                                  NVL (md.subinventory,
                                                       mil.subinventory_code)
                                           AND mil.availability_type = 1))
                        AND (   lot_number IS NULL
                             OR EXISTS
                                   (SELECT 1
                                      FROM mtl_lot_numbers mln
                                     WHERE     mln.organization_id = md.organization_id
                                           AND mln.lot_number = md.lot_number
                                           AND mln.inventory_item_id = md.inventory_item_id
                                           AND mln.availability_type = 1))
               GROUP BY organization_id, inventory_item_id ';

      l_load_stmt                 VARCHAR2 (4000)
                                     := l_create_table_stmt || l_select_stmt;


      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id)';


      l_table_name1               VARCHAR2 (40) := 'XXWC_ISR_RES_DEM_QTY##';
      l_table_full_name1          VARCHAR2 (60) := l_owner || '.' || l_table_name1;
      l_index_name1               VARCHAR2 (60) := l_table_full_name1 || '_N1';
      l_table_success_msg1        VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load completed ';
      l_table_error_msg1          VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load failed ';
      l_success_msg1              VARCHAR2 (200);
      l_error_msg1                VARCHAR2 (200);
      l_index_success_msg1        VARCHAR2 (200)
         := l_index_name1 || ' Index Creation completed ';
      l_index_error_msg1          VARCHAR2 (200)
                                     := l_index_name1 || ' Index Creation failed ';
      l_create_table_stmt1        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name1 || ' AS ';

      l_select_stmt1              VARCHAR2 (4000)
         := '    SELECT organization_id,
                        inventory_item_id,
                        SUM (primary_reservation_quantity) res_dem_qty
                   FROM mtl_reservations
                  WHERE     demand_source_type_id NOT IN (2, 8, 12)
                        AND (   subinventory_code IS NULL
                             OR EXISTS
                                   (SELECT 1
                                      FROM mtl_secondary_inventories s
                                     WHERE     s.organization_id = organization_id
                                           AND s.secondary_inventory_name =
                                                  subinventory_code
                                           AND s.availability_type = 1))
                        AND (   locator_id IS NULL
                             OR EXISTS
                                   (SELECT 1
                                      FROM mtl_item_locations mil
                                     WHERE     mil.organization_id = organization_id
                                           AND mil.inventory_location_id = locator_id
                                           AND mil.subinventory_code =
                                                  NVL (subinventory_code,
                                                       mil.subinventory_code)
                                           AND mil.availability_type = 1))
                        AND (   lot_number IS NULL
                             OR EXISTS
                                   (SELECT 1
                                      FROM mtl_lot_numbers mln
                                     WHERE     mln.organization_id = organization_id
                                           AND mln.lot_number = lot_number
                                           AND mln.inventory_item_id = inventory_item_id
                                           AND mln.availability_type = 1))
               GROUP BY organization_id, inventory_item_id ';


      l_load_stmt1                VARCHAR2 (4000)
                                     := l_create_table_stmt1 || l_select_stmt1;


      --  l_load_stmt1                CLOB;

      l_index_stmt1               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name1
            || ' ON '
            || l_table_full_name1
            || ' ( organization_id, inventory_item_id)';


      l_table_name2               VARCHAR2 (40) := 'XXWC_ISR_SO_DEM_QTY##';
      l_table_full_name2          VARCHAR2 (60) := l_owner || '.' || l_table_name2;
      l_index_name2               VARCHAR2 (60) := l_table_full_name2 || '_N1';
      l_table_success_msg2        VARCHAR2 (200)
         := l_table_full_name2 || ' Table Load completed ';
      l_table_error_msg2          VARCHAR2 (200)
         := l_table_full_name2 || ' Table Load failed ';
      l_success_msg2              VARCHAR2 (200);
      l_error_msg2                VARCHAR2 (200);
      l_index_success_msg2        VARCHAR2 (200)
         := l_index_name2 || ' Index Creation completed ';
      l_index_error_msg2          VARCHAR2 (200)
                                     := l_index_name2 || ' Index Creation failed ';
      l_create_table_stmt2        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name2 || ' AS ';
--<<Commented for Ver 5.0 Begin
/*
      l_select_stmt2              VARCHAR2 (4000)
         := '    SELECT ool.org_id,
                        ool.ship_from_org_id organization_id,
                        ool.inventory_item_id,
                        SUM (  (ordered_quantity)
                             )
                           so_dem_qty
                   FROM oe_order_lines_all ool
                  WHERE     1 = 1
                        AND open_flag = ''Y''
                        AND visible_demand_flag = ''Y''
                        AND shipped_quantity IS NULL
                        AND DECODE (ool.source_document_type_id,
                                    10, 8,
                                    DECODE (ool.line_category_code, ''ORDER'', 2, 12)) IN (2,
                                                                                         8,
                                                                                         12)
                        AND (   (    1 = 1
                                 AND DECODE (
                                        ool.source_document_type_id,
                                        10, 8,
                                        DECODE (ool.line_category_code, ''ORDER'', 2, 12)) <>
                                        8)
                             OR 1 = 1)
                        AND (   subinventory IS NULL
                             OR 2 = 2
                             OR EXISTS
                                   (SELECT 1
                                      FROM mtl_secondary_inventories s
                                     WHERE     s.organization_id = ship_from_org_id
                                           AND s.secondary_inventory_name = subinventory
                                           AND s.availability_type = 1))
               GROUP BY ool.org_id, ool.ship_from_org_id, ool.inventory_item_id ';
*/			   
-->>Commented for Ver 5.0 End
--<<Added for Ver 5.0 Begin, Removed condition 
     l_select_stmt2              VARCHAR2 (4000)
         := '    SELECT ool.org_id,
                        ool.ship_from_org_id organization_id,
                        ool.inventory_item_id,
                        SUM (  (ordered_quantity)
                             )
                           so_dem_qty
                   FROM oe_order_lines_all ool
                  WHERE     1 = 1
                        AND open_flag = ''Y''
                        AND visible_demand_flag = ''Y''
                        AND shipped_quantity IS NULL
                        AND DECODE (ool.source_document_type_id,
                                    10, 8,
                                    DECODE (ool.line_category_code, ''ORDER'', 2, 12)) IN (2,
                                                                                         12)                        
                        AND (   subinventory IS NULL
                             OR 2 = 2
                             OR EXISTS
                                   (SELECT 1
                                      FROM mtl_secondary_inventories s
                                     WHERE     s.organization_id = ship_from_org_id
                                           AND s.secondary_inventory_name = subinventory
                                           AND s.availability_type = 1))
               GROUP BY ool.org_id, ool.ship_from_org_id, ool.inventory_item_id ';	   
-->>Added for Ver 5.0 End
			   
      l_load_stmt2                VARCHAR2 (4000)
                                     := l_create_table_stmt2 || l_select_stmt2;


      l_index_stmt2               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name2
            || ' ON '
            || l_table_full_name2
            || ' ( organization_id, inventory_item_id)';
			
--Added for Ver 5.0 Begin
      l_table_name2a               VARCHAR2 (40) := 'XXWC_ISR_ISO_DEM_QTY##';
      l_table_full_name2a          VARCHAR2 (60) := l_owner || '.' || l_table_name2a;
      l_index_name2a               VARCHAR2 (60) := l_table_full_name2a || '_N1';
      l_table_success_msg2a        VARCHAR2 (200)
         := l_table_full_name2a || ' Table Load completed ';
      l_table_error_msg2a          VARCHAR2 (200)
         := l_table_full_name2a || ' Table Load failed ';
      l_success_msg2a              VARCHAR2 (200);
      l_error_msg2a                VARCHAR2 (200);
      l_index_success_msg2a        VARCHAR2 (200)
         := l_index_name2a || ' Index Creation completed ';
      l_index_error_msg2a          VARCHAR2 (200)
                                     := l_index_name2a || ' Index Creation failed ';
      l_create_table_stmt2a        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name2a || ' AS ';

     l_select_stmt2a              VARCHAR2 (4000)
         := '   SELECT  ool.org_id,
                        ool.ship_from_org_id organization_id,
                        ool.inventory_item_id,
                        SUM (  (ordered_quantity)
                             ) iso_dem_qty
                  FROM oe_order_lines_all ool,
                       po_requisition_lines_all pol
                 WHERE ool.source_document_id  = pol.requisition_header_id
                   AND ool.source_document_line_id = pol.requisition_line_id                 
                   AND (pol.destination_organization_id <> ool.ship_from_org_id or
                        (pol.destination_organization_id = ool.ship_from_org_id AND 
                          ( pol.destination_type_code = ''EXPENSE'' or  
                         (  pol.destination_type_code = ''INVENTORY''
                            AND pol.destination_subinventory is not null
                            AND exists (select 1 from
                                        mtl_secondary_inventories
                                                    WHERE secondary_inventory_name = pol.destination_subinventory
                                                    AND organization_id = pol.destination_organization_id
                                                    AND quantity_tracked = 2)
                          )
                          )
                        )
                      )                
                   AND ool.open_flag = ''Y''
                   AND ool.visible_demand_flag = ''Y''
                   AND shipped_quantity is null
                   AND DECODE(ool.source_document_type_id, 10, 8,DECODE(ool.line_category_code, ''ORDER'',2,12)) = 8
                   AND (subinventory IS NULL OR
                        EXISTS (select 1
                                  FROM mtl_secondary_inventories s
                                 WHERE s.organization_id = ool.ship_from_org_id
                                   AND s.secondary_inventory_name = subinventory
                                   AND s.availability_type = 1))
                                  group by ool.org_id, ool.ship_from_org_id, ool.inventory_item_id ';	   

			   
      l_load_stmt2a                VARCHAR2 (4000)
                                     := l_create_table_stmt2a || l_select_stmt2a;


      l_index_stmt2a               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name2a
            || ' ON '
            || l_table_full_name2a
            || ' ( organization_id, inventory_item_id)';

-- Added for Ver 5.0 End			


      l_table_name3               VARCHAR2 (40) := 'XXWC_ISR_OPM_DEM_QTY##';
      l_table_full_name3          VARCHAR2 (60) := l_owner || '.' || l_table_name3;
      l_index_name3               VARCHAR2 (60) := l_table_full_name3 || '_N1';
      l_table_success_msg3        VARCHAR2 (300)
         := l_table_full_name3 || ' Table Load completed ';
      l_table_error_msg3          VARCHAR2 (200)
         := l_table_full_name3 || ' Table Load failed ';
      l_success_msg3              VARCHAR2 (200);
      l_error_msg3                VARCHAR2 (200);
      l_index_success_msg3        VARCHAR2 (200)
         := l_index_name3 || ' Index Creation completed ';
      l_index_error_msg3          VARCHAR2 (200)
                                     := l_index_name3 || ' Index Creation failed ';
      l_create_table_stmt3        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name3 || ' AS ';
      l_select_stmt3              VARCHAR2 (4000)
         := '    SELECT d.organization_id,
                        d.inventory_item_id,
                        SUM (
                             NVL (NVL (d.wip_plan_qty, d.plan_qty) - d.actual_qty, 0)
                           - NVL (mtr.primary_reservation_quantity, 0))
                           opm_dem_qty
                   FROM gme_material_details d, gme_batch_header h, mtl_reservations mtr
                  WHERE     h.batch_type IN (0, 10)
                        AND h.batch_status IN (1, 2)
                        AND h.batch_id = d.batch_id
                        AND d.line_type = -1
                        AND d.batch_id = mtr.demand_source_header_id(+)
                        AND d.material_detail_id = mtr.demand_source_line_id(+)
                        AND d.inventory_item_id = mtr.inventory_item_id(+)
                        AND d.organization_id = mtr.organization_id(+)
                        AND   NVL (NVL (d.wip_plan_qty, d.plan_qty) - d.actual_qty, 0)
                            - NVL (mtr.primary_reservation_quantity, 0) > 0
                        AND NVL (mtr.demand_source_type_id, 5) = 5
                        AND (   mtr.subinventory_code IS NULL
                             OR EXISTS
                                   (SELECT 1
                                      FROM mtl_secondary_inventories s
                                     WHERE     s.organization_id = organization_id
                                           AND s.secondary_inventory_name =
                                                  mtr.subinventory_code
                                           AND s.availability_type = 1))
                        AND (   mtr.locator_id IS NULL
                             OR EXISTS
                                   (SELECT 1
                                      FROM mtl_item_locations mil
                                     WHERE     mil.organization_id = organization_id
                                           AND mil.inventory_location_id = mtr.locator_id
                                           AND mil.subinventory_code =
                                                  NVL (mtr.subinventory_code,
                                                       mil.subinventory_code)
                                           AND mil.availability_type = 1))
                        AND (   mtr.lot_number IS NULL
                             OR EXISTS
                                   (SELECT 1
                                      FROM mtl_lot_numbers mln
                                     WHERE     mln.organization_id = organization_id
                                           AND mln.lot_number = mtr.lot_number
                                           AND mln.inventory_item_id = inventory_item_id
                                           AND mln.availability_type = 1))
               GROUP BY d.organization_id, d.inventory_item_id ';

      l_load_stmt3                VARCHAR2 (4000)
                                     := l_create_table_stmt3 || l_select_stmt3;


      l_index_stmt3               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name3
            || ' ON '
            || l_table_full_name3
            || ' ( organization_id, inventory_item_id)';


      l_table_name4               VARCHAR2 (40) := 'XXWC_ISR_WIP_DIS_DEM_QTY##';
      l_table_full_name4          VARCHAR2 (60) := l_owner || '.' || l_table_name4;
      l_index_name4               VARCHAR2 (60) := l_table_full_name4 || '_N1';
      l_table_success_msg4        VARCHAR2 (300)
         := l_table_full_name4 || ' Table Load completed ';
      l_table_error_msg4          VARCHAR2 (200)
         := l_table_full_name4 || ' Table Load failed ';
      l_success_msg4              VARCHAR2 (200);
      l_error_msg4                VARCHAR2 (200);
      l_index_success_msg4        VARCHAR2 (200)
         := l_index_name4 || ' Index Creation completed ';
      l_index_error_msg4          VARCHAR2 (200)
                                     := l_index_name4 || ' Index Creation failed ';
      l_create_table_stmt4        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name4 || ' AS ';
      l_select_stmt4              VARCHAR2 (4000)
         := '    SELECT o.organization_id,
                        o.inventory_item_id,
                        SUM (o.required_quantity - o.quantity_issued) wip_dis_dem_qty
                   FROM wip_discrete_jobs d, wip_requirement_operations o
                  WHERE     o.wip_entity_id = d.wip_entity_id
                        AND o.organization_id = d.organization_id
                        AND o.required_quantity > 0
                        AND o.required_quantity > o.quantity_issued
                        AND o.operation_seq_num > 0
                        AND d.status_type IN (1,
                                              3,
                                              4,
                                              6)
                        AND o.wip_supply_type NOT IN (5, 6)
               GROUP BY o.organization_id, o.inventory_item_id ';

      l_load_stmt4                VARCHAR2 (4000)
                                     := l_create_table_stmt4 || l_select_stmt4;


      l_index_stmt4               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name4
            || ' ON '
            || l_table_full_name4
            || ' ( organization_id, inventory_item_id)';



      l_table_name5               VARCHAR2 (40) := 'XXWC_ISR_WIP_REP_DEM_QTY##';
      l_table_full_name5          VARCHAR2 (60) := l_owner || '.' || l_table_name5;
      l_index_name5               VARCHAR2 (60) := l_table_full_name5 || '_N1';
      l_table_success_msg5        VARCHAR2 (300)
         := l_table_full_name5 || ' Table Load completed ';
      l_table_error_msg5          VARCHAR2 (200)
         := l_table_full_name5 || ' Table Load failed ';
      l_success_msg5              VARCHAR2 (200);
      l_error_msg5                VARCHAR2 (200);
      l_index_success_msg5        VARCHAR2 (200)
         := l_index_name5 || ' Index Creation completed ';
      l_index_error_msg5          VARCHAR2 (200)
                                     := l_index_name5 || ' Index Creation failed ';
      l_create_table_stmt5        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name5 || ' AS ';
      l_select_stmt5              VARCHAR2 (4000)
         := '        SELECT o.organization_id,
                            o.inventory_item_id,
                            SUM (o.required_quantity - o.quantity_issued) wip_rep_dem_qty
                       FROM wip_repetitive_schedules r, wip_requirement_operations o
                      WHERE     o.wip_entity_id = r.wip_entity_id
                            AND o.repetitive_schedule_id = r.repetitive_schedule_id
                            AND o.organization_id = r.organization_id
                            AND o.required_quantity > 0
                            AND o.required_quantity > o.quantity_issued
                            AND o.operation_seq_num > 0
                            AND r.status_type IN (1,
                                                  3,
                                                  4,
                                                  6)
                            AND O.WIP_SUPPLY_TYPE NOT IN (5, 6)
                   GROUP BY o.organization_id, o.inventory_item_id ';

      l_load_stmt5                VARCHAR2 (4000)
                                     := l_create_table_stmt5 || l_select_stmt5;


      l_index_stmt5               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name5
            || ' ON '
            || l_table_full_name5
            || ' ( organization_id, inventory_item_id,org_id)';



      l_table_name6               VARCHAR2 (40) := 'XXWC_ISR_MOVE_ORD_DEM_QTY##';
      l_table_full_name6          VARCHAR2 (60) := l_owner || '.' || l_table_name6;
      l_index_name6               VARCHAR2 (60) := l_table_full_name6 || '_N1';
      l_table_success_msg6        VARCHAR2 (300)
         := l_table_full_name6 || ' Table Load completed ';
      l_table_error_msg6          VARCHAR2 (200)
         := l_table_full_name6 || ' Table Load failed ';
      l_success_msg6              VARCHAR2 (200);
      l_error_msg6                VARCHAR2 (200);
      l_index_success_msg6        VARCHAR2 (200)
         := l_index_name6 || ' Index Creation completed ';
      l_index_error_msg6          VARCHAR2 (200)
                                     := l_index_name6 || ' Index Creation failed ';
      l_create_table_stmt6        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name6 || ' AS ';
      l_select_stmt6              VARCHAR2 (4000)
         := '        SELECT mtrl.organization_id,
                            mtrl.inventory_item_id,
                            mtrl.from_subinventory_code subinventory,
                            NVL (SUM (apps.inv_decimals_pub.get_primary_quantity (
                                         mtrl.organization_id,
                                         mtrl.inventory_item_id,
                                         mtrl.uom_code,
                                         mtrl.quantity - NVL (mtrl.quantity_delivered, 0))),
                                 0)
                               move_ord_dem_qty
                       FROM mtl_txn_request_lines mtrl, mtl_transaction_types mtt
                      WHERE     mtt.transaction_type_id = mtrl.transaction_type_id
                            AND mtrl.transaction_type_id <> 35
                            AND mtrl.line_status IN (3, 7)
                            AND mtt.transaction_action_id = 1
                            AND (   mtrl.from_subinventory_code IS NULL
                                 OR 1 = 2
                                 OR EXISTS
                                       (SELECT 1
                                          FROM mtl_secondary_inventories s
                                         WHERE     s.organization_id = mtrl.organization_id
                                               AND s.secondary_inventory_name =
                                                      mtrl.from_subinventory_code
                                               AND s.availability_type = 1))
                            AND (   mtrl.from_locator_id IS NULL
                                 OR 1 = 2
                                 OR EXISTS
                                       (SELECT 1
                                          FROM mtl_item_locations mil
                                         WHERE     mil.organization_id = mtrl.organization_id
                                               AND mil.inventory_location_id =
                                                      mtrl.from_locator_id
                                               AND mil.subinventory_code =
                                                      NVL (mtrl.from_subinventory_code,
                                                           mil.subinventory_code)
                                               AND mil.availability_type = 1))
                            AND (   mtrl.lot_number IS NULL
                                 OR 1 = 2
                                 OR EXISTS
                                       (SELECT 1
                                          FROM mtl_lot_numbers mln
                                         WHERE     mln.organization_id = mtrl.organization_id
                                               AND mln.lot_number = mtrl.lot_number
                                               AND mln.inventory_item_id =
                                                      mtrl.inventory_item_id
                                               AND mln.availability_type = 1))
                   GROUP BY mtrl.organization_id,
                            mtrl.inventory_item_id,
                            mtrl.from_subinventory_code ';

      l_load_stmt6                VARCHAR2 (4000)
                                     := l_create_table_stmt6 || l_select_stmt6;


      l_index_stmt6               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name6
            || ' ON '
            || l_table_full_name6
            || ' ( organization_id, inventory_item_id,org_id)';



      l_table_name7               VARCHAR2 (40) := 'XXWC_ISR_DROP_SHIP_RES_QTY##';
      l_table_full_name7          VARCHAR2 (60) := l_owner || '.' || l_table_name7;
      l_index_name7               VARCHAR2 (60) := l_table_full_name7 || '_N1';
      l_table_success_msg7        VARCHAR2 (300)
         := l_table_full_name7 || ' Table Load completed ';
      l_table_error_msg7          VARCHAR2 (200)
         := l_table_full_name7 || ' Table Load failed ';
      l_success_msg7              VARCHAR2 (200);
      l_error_msg7                VARCHAR2 (200);
      l_index_success_msg7        VARCHAR2 (200)
         := l_index_name7 || ' Index Creation completed ';
      l_index_error_msg7          VARCHAR2 (200)
                                     := l_index_name7 || ' Index Creation failed ';
      l_create_table_stmt7        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name7 || ' AS ';
      l_select_stmt7              VARCHAR2 (4000)
         := '    SELECT organization_id,
                        inventory_item_id,
                        subinventory_code subininventory,
                        SUM (primary_reservation_quantity) drop_ship_res_qty
                   FROM mtl_reservations
                  WHERE     1 = 1
                        AND demand_source_type_id = 2
                        AND supply_source_type_id = 13
                        AND (   subinventory_code IS NULL
                             OR 1 = 2
                             OR EXISTS
                                   (SELECT 1
                                      FROM mtl_secondary_inventories s
                                     WHERE     s.organization_id = organization_id
                                           AND s.secondary_inventory_name =
                                                  subinventory_code
                                           AND s.availability_type = 1))
                        AND EXISTS
                               (SELECT 1
                                  FROM oe_drop_ship_sources odss
                                 WHERE odss.line_id = demand_source_line_id)
               GROUP BY organization_id, inventory_item_id, subinventory_code ';

      l_load_stmt7                VARCHAR2 (4000)
                                     := l_create_table_stmt7 || l_select_stmt7;


      l_index_stmt7               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name7
            || ' ON '
            || l_table_full_name7
            || ' ( organization_id, inventory_item_id,org_id)';



      l_table_name8               VARCHAR2 (40) := 'XXWC_ISR_DEMAND_QTY##';
      l_table_full_name8          VARCHAR2 (60) := l_owner || '.' || l_table_name8;
      l_index_name8               VARCHAR2 (60) := l_table_full_name8 || '_N1';
      l_table_success_msg8        VARCHAR2 (300)
         := l_table_full_name8 || ' Table Load completed ';
      l_table_error_msg8          VARCHAR2 (200)
         := l_table_full_name8 || ' Table Load failed ';
      l_success_msg8              VARCHAR2 (200);
      l_error_msg8                VARCHAR2 (200);
      l_index_success_msg8        VARCHAR2 (200)
         := l_index_name8 || ' Index Creation completed ';
      l_index_error_msg8          VARCHAR2 (200)
                                     := l_index_name8 || ' Index Creation failed ';
      l_create_table_stmt8        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name8 || ' AS ';
--Commented for Ver 5.0 Begin
/*		 
      l_select_stmt8              VARCHAR2 (4000)
         := '    SELECT items.org_id, items.organization_id,
                        items.inventory_item_id,
                        SUM (NVL (unres_dem_qty, 0)) unres_dem_qty,
                        SUM (NVL (res_dem_qty, 0)) res_dem_qty,
                        SUM (NVL (so_dem_qty, 0)) so_dem_qty,
                        SUM (NVL (opm_dem_qty, 0)) opm_dem_qty,
                        SUM (NVL (wip_dis_dem_qty, 0)) wip_dis_dem_qty,
                        SUM (NVL (wip_rep_dem_qty, 0)) wip_rep_dem_qty,
                        SUM (NVL (move_ord_dem_qty, 0)) move_ord_dem_qty,
                        SUM (NVL (drop_ship_res_qty, 0)) drop_ship_res_qty,
                          SUM (NVL (unres_dem_qty, 0))
                        + SUM (NVL (res_dem_qty, 0))
                        + SUM (NVL (so_dem_qty, 0))
                        + SUM (NVL (opm_dem_qty, 0))
                        + SUM (NVL (wip_dis_dem_qty, 0))
                        + SUM (NVL (wip_rep_dem_qty, 0))
                        + SUM (NVL (move_ord_dem_qty, 0))
                        + SUM (NVL (drop_ship_res_qty, 0))
                           demand
                   FROM xxwc.xxwc_isr_items## items,
                        xxwc.xxwc_isr_unres_dem_qty## a,
                        xxwc.xxwc_isr_res_dem_qty## b,
                        xxwc.xxwc_isr_so_dem_qty## c,
                        xxwc.xxwc_isr_opm_dem_qty## d,
                        xxwc.xxwc_isr_wip_dis_dem_qty## e,
                        xxwc.xxwc_isr_wip_rep_dem_qty## f,
                        xxwc.xxwc_isr_move_ord_dem_qty## g,
                        xxwc.xxwc_isr_drop_ship_res_qty## h
                  WHERE     items.inventory_item_id = a.inventory_item_id(+)
                        AND items.organization_id = a.organization_id(+)
                        AND items.inventory_item_id = b.inventory_item_id(+)
                        AND items.organization_id = b.organization_id(+)
                        AND items.inventory_item_id = c.inventory_item_id(+)
                        AND items.organization_id = c.organization_id(+)
                        AND items.inventory_item_id = d.inventory_item_id(+)
                        AND items.organization_id = d.organization_id(+)
                        AND items.inventory_item_id = e.inventory_item_id(+)
                        AND items.organization_id = e.organization_id(+)
                        AND items.inventory_item_id = f.inventory_item_id(+)
                        AND items.organization_id = f.organization_id(+)
                        AND items.inventory_item_id = g.inventory_item_id(+)
                        AND items.organization_id = g.organization_id(+)
                        AND items.inventory_item_id = h.inventory_item_id(+)
                        AND items.organization_id = h.organization_id(+)
               GROUP BY items.org_id, items.organization_id, items.inventory_item_id  ';
*/
--Commented for Ver 5.0 End			   

--Added for Ver 5.0 Begin	 
      l_select_stmt8              VARCHAR2 (4000)
         := '    SELECT items.org_id, items.organization_id,
                        items.inventory_item_id,
                        SUM (NVL (unres_dem_qty, 0)) unres_dem_qty,
                        SUM (NVL (res_dem_qty, 0)) res_dem_qty,
                        SUM (NVL (so_dem_qty, 0)) so_dem_qty,
                        SUM (NVL (opm_dem_qty, 0)) opm_dem_qty,
                        SUM (NVL (wip_dis_dem_qty, 0)) wip_dis_dem_qty,
                        SUM (NVL (wip_rep_dem_qty, 0)) wip_rep_dem_qty,
                        SUM (NVL (move_ord_dem_qty, 0)) move_ord_dem_qty,
                        SUM (NVL (drop_ship_res_qty, 0)) drop_ship_res_qty,
						SUM (NVL (iso_dem_qty, 0)) iso_dem_qty,
                          SUM (NVL (unres_dem_qty, 0))
                        + SUM (NVL (res_dem_qty, 0))
                        + SUM (NVL (so_dem_qty, 0))
                        + SUM (NVL (opm_dem_qty, 0))
                        + SUM (NVL (wip_dis_dem_qty, 0))
                        + SUM (NVL (wip_rep_dem_qty, 0))
                        + SUM (NVL (move_ord_dem_qty, 0))
                        + SUM (NVL (drop_ship_res_qty, 0))
						+ SUM (NVL (iso_dem_qty, 0))
                           demand
                   FROM xxwc.xxwc_isr_items## items,
                        xxwc.xxwc_isr_unres_dem_qty## a,
                        xxwc.xxwc_isr_res_dem_qty## b,
                        xxwc.xxwc_isr_so_dem_qty## c,
                        xxwc.xxwc_isr_opm_dem_qty## d,
                        xxwc.xxwc_isr_wip_dis_dem_qty## e,
                        xxwc.xxwc_isr_wip_rep_dem_qty## f,
                        xxwc.xxwc_isr_move_ord_dem_qty## g,
                        xxwc.xxwc_isr_drop_ship_res_qty## h,
						xxwc.xxwc_isr_iso_dem_qty## i
                  WHERE     items.inventory_item_id = a.inventory_item_id(+)
                        AND items.organization_id = a.organization_id(+)
                        AND items.inventory_item_id = b.inventory_item_id(+)
                        AND items.organization_id = b.organization_id(+)
                        AND items.inventory_item_id = c.inventory_item_id(+)
                        AND items.organization_id = c.organization_id(+)
                        AND items.inventory_item_id = d.inventory_item_id(+)
                        AND items.organization_id = d.organization_id(+)
                        AND items.inventory_item_id = e.inventory_item_id(+)
                        AND items.organization_id = e.organization_id(+)
                        AND items.inventory_item_id = f.inventory_item_id(+)
                        AND items.organization_id = f.organization_id(+)
                        AND items.inventory_item_id = g.inventory_item_id(+)
                        AND items.organization_id = g.organization_id(+)
                        AND items.inventory_item_id = h.inventory_item_id(+)
                        AND items.organization_id = h.organization_id(+)
						AND items.inventory_item_id = i.inventory_item_id(+)
                        AND items.organization_id = i.organization_id(+)
               GROUP BY items.org_id, items.organization_id, items.inventory_item_id  ';
--Added for Ver 5.0 End
      l_load_stmt8                VARCHAR2 (4000)
                                     := l_create_table_stmt8 || l_select_stmt8;


      l_index_stmt8               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name8
            || ' ON '
            || l_table_full_name8
            || ' ( organization_id, inventory_item_id,org_id)';



      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*,  b.demand 
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_demand_qty## b
                 WHERE     a.organization_id = b.organization_id(+)
                       AND a.inventory_item_id = b.inventory_item_id(+)                       
                       AND a.org_id = b.org_id(+) ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec := 'Process 22 : Load Demand ';
      write_log (g_line_open);
      write_log (g_sec);

      g_sec := 'Process 22a : Load Unreserved Demand ';
      write_log (g_sec);


      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);

      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      ----------------------------------------

      g_sec := 'Process 22b : Load Reserved Demand ';

      write_log (g_sec);

      l_success_msg := l_table_success_msg1;
      l_error_msg := l_table_error_msg1;

      drop_temp_table (l_owner, l_table_name1);

      EXECUTE IMMEDIATE l_load_stmt1;

      write_log (l_success_msg);


      -----------------------------------------

      g_sec := 'Process 22c : Load Sales Order Demand Qty ';

      write_log (g_sec);

      l_success_msg := l_table_success_msg2;
      l_error_msg := l_table_error_msg2;

      drop_temp_table (l_owner, l_table_name2);

      EXECUTE IMMEDIATE l_load_stmt2;

      write_log (l_success_msg);


      -----------------------------------------
--Added for ver 5.0 Begin	  
	  g_sec := 'Process 22c1 : Load Internal Sales Order Demand Qty ';

      write_log (g_sec);

      l_success_msg := l_table_success_msg2a;
      l_error_msg := l_table_error_msg2a;

      drop_temp_table (l_owner, l_table_name2a);

      EXECUTE IMMEDIATE l_load_stmt2a;

      write_log (l_success_msg);


      -----------------------------------------
--Added for ver 5.0 End	  

      g_sec := 'Process 22d : Load OPM Demand Qty ';

      write_log (g_sec);


      l_success_msg := l_table_success_msg3;
      l_error_msg := l_table_error_msg3;

      drop_temp_table (l_owner, l_table_name3);

      EXECUTE IMMEDIATE l_load_stmt3;

      write_log (l_success_msg);

      -----------------------------------------

      g_sec := 'Process 22e : Load WIP discrete Demand Qty';

      write_log (g_sec);

      l_success_msg := l_table_success_msg4;
      l_error_msg := l_table_error_msg4;

      drop_temp_table (l_owner, l_table_name4);

      EXECUTE IMMEDIATE l_load_stmt4;

      write_log (l_success_msg);

      -----------------------------------------



      g_sec := 'Process 22f: Load WIP Repetitive Demand Qty';

      write_log (g_sec);


      l_success_msg := l_table_success_msg5;
      l_error_msg := l_table_error_msg5;

      drop_temp_table (l_owner, l_table_name5);

      EXECUTE IMMEDIATE l_load_stmt5;

      write_log (l_success_msg);

      -----------------------------------------


      g_sec := 'Process 22g: Load Move Order Demand Qty';

      write_log (g_sec);


      l_success_msg := l_table_success_msg6;
      l_error_msg := l_table_error_msg6;

      drop_temp_table (l_owner, l_table_name6);

      EXECUTE IMMEDIATE l_load_stmt6;

      write_log (l_success_msg);

      -----------------------------------------

      g_sec := 'Process 22h: Load Drop ship reserved Demand Qty';

      write_log (g_sec);


      l_success_msg := l_table_success_msg7;
      l_error_msg := l_table_error_msg7;

      drop_temp_table (l_owner, l_table_name7);

      EXECUTE IMMEDIATE l_load_stmt7;

      write_log (l_success_msg);

      -----------------------------------------


      g_sec := 'Process 22i: Load consolidated Demand Qty';

      write_log (g_sec);


      l_success_msg := l_table_success_msg8;
      l_error_msg := l_table_error_msg8;

      drop_temp_table (l_owner, l_table_name8);

      EXECUTE IMMEDIATE l_load_stmt8;

      write_log (l_success_msg);

      -----------------------------------------

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);


      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_demand;


   PROCEDURE load_mfg_items (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_mfg_items $
     PURPOSE:     23. Procedure to Load Mfg Part Numbers for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    06-Apr-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';

      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_MFG_ITEMS_LIST##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt               VARCHAR2 (4000)
         := '      SELECT inventory_item_id,                                                                                    
                          cross_reference mfg_part_number ,                              
                          org_independent_flag,                    
                          ROW_NUMBER ()                    
                          OVER (PARTITION BY inventory_item_id, organization_id                    
                                ORDER BY cross_reference)                    
                             rn                    
                     FROM MTL_CROSS_REFERENCES                    
                    WHERE 1 = 1 AND cross_reference_type = ''VENDOR''                    
                    AND organization_id IS NULL     ';
      l_load_stmt                 VARCHAR2 (4000)
                                     := l_create_table_stmt || l_select_stmt;

      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( inventory_item_id,rn)';



      l_table_name1               VARCHAR2 (40) := 'XXWC_ISR_MFG_ITEMS##';
      l_table_full_name1          VARCHAR2 (60) := l_owner || '.' || l_table_name1;
      l_index_name1               VARCHAR2 (60) := l_table_full_name1 || '_N1';
      l_table_success_msg1        VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load completed ';
      l_table_error_msg1          VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load failed ';
      l_index_success_msg1        VARCHAR2 (200)
         := l_index_name1 || ' Index Creation completed ';
      l_index_error_msg1          VARCHAR2 (200)
                                     := l_index_name1 || ' Index Creation failed ';
      l_create_table_stmt1        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name1 || ' AS ';
      l_select_stmt1              VARCHAR2 (4000)
         := '      SELECT *        
                     FROM XXWC.XXWC_ISR_MFG_ITEMS_LIST##
                    WHERE rn = 1    ';
      l_load_stmt1                VARCHAR2 (4000)
                                     := l_create_table_stmt1 || l_select_stmt1;

      l_index_stmt1               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name1
            || ' ON '
            || l_table_full_name1
            || ' (inventory_item_id)';


      CUR_MFG_ITEM_RANK           SYS_REFCURSOR;


      L_MFG_ITEM_RANK_query       VARCHAR2 (500)
         := ' SELECT distinct rn 
                FROM     XXWC.XXWC_ISR_MFG_ITEMS_LIST## 
                WHERE rn>1
                ORDER BY 1    ';

      l_update_stmt               VARCHAR2 (4000)
         := '  UPDATE XXWC.XXWC_ISR_MFG_ITEMS## a                    
                   SET mfg_part_number =                    
                             mfg_part_number                                                              
                          || (SELECT '','' ||mfg_part_number                    
                                FROM XXWC.XXWC_ISR_MFG_ITEMS_LIST##                    
                               WHERE inventory_item_id = a.inventory_item_id AND rn = :rn)                    
                 WHERE a.inventory_item_id IN (SELECT inventory_item_id                    
                                                 FROM XXWC.XXWC_ISR_MFG_ITEMS_LIST##                     
                                                WHERE rn = a.rn)  ';

      l_rank                      NUMBER;



      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT     a.*,
                        b.mfg_part_number                          
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_mfg_items## b
                 WHERE a.inventory_item_id = b.inventory_item_id(+) ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec := 'Process 23a : Load MFG Part Number List ';
      write_log (g_line_open);
      write_log (g_sec);

      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);

      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);



      g_sec :=
         'Process 23b : Load MFG Part Number without Duplicate Item entries ';
      write_log (g_line_open);
      write_log (g_sec);

      l_success_msg := l_table_success_msg1;
      l_error_msg := l_table_error_msg1;

      drop_temp_table (l_owner, l_table_name1);

      EXECUTE IMMEDIATE l_load_stmt1;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg1;
      l_error_msg := l_index_error_msg1;

      EXECUTE IMMEDIATE l_index_stmt1;

      write_log (l_success_msg);


      g_sec := 'Process 23c : Concatenate mfg_part_number for items ';
      write_log (g_line_open);
      write_log (g_sec);


      l_success_msg := 'Mfg Part Number Update completed';



      OPEN CUR_MFG_ITEM_RANK FOR L_MFG_ITEM_RANK_QUERY;

      LOOP
         FETCH CUR_MFG_ITEM_RANK INTO l_rank;

         EXIT WHEN CUR_MFG_ITEM_RANK%NOTFOUND;

         l_error_msg := 'Rank ' || l_rank || ' Update failed ';

         EXECUTE IMMEDIATE l_update_stmt USING l_rank;
      END LOOP;


      write_log (l_success_msg);


      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;

      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_mfg_items;

   PROCEDURE load_mst_vendor (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.Load_mst_vendor $
     PURPOSE:     24. Procedure to Load Master Vendor Number for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    06-Apr-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_MST_VENDOR##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt               VARCHAR2 (4000)
         := '   SELECT DISTINCT inventory_item_id, organization_id, attribute_char_value mst_vendor_num    ,
                (SELECT vendor_name
                   FROM ap_suppliers
                  WHERE segment1 = a.attribute_char_value)
                   mst_vendor_name         
                  FROM ego_all_attr_base_v a                       
                 WHERE attribute_name = ''XXWC_VENDOR_NUMBER_ATTR''                        
                       AND attribute_group_name = ''XXWC_INTERNAL_ATTR_AG''     ';
      l_load_stmt                 VARCHAR2 (4000)
                                     := l_create_table_stmt || l_select_stmt;

      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( inventory_item_id, organization_id)';



      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*,
                       b.mst_vendor_num,
                       b.mst_vendor_name
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_mst_vendor## b
                 WHERE a.inventory_item_id = b.inventory_item_id(+) ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec := 'Process 24 : Load Master Vendor ';
      write_log (g_line_open);
      write_log (g_sec);

      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);

      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);

      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_mst_vendor;

   PROCEDURE load_last_receipt (p_retcode OUT VARCHAR2)
   /***************************************************************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_last_receipt $
     PURPOSE:     25. Procedure to Load Last Receipt Date for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    --------------------------------------------------------------------------------
     1.0    06-Apr-14    Manjula Chellappan    Initial Version
     5.0    15-Oct-15    P.Vamshidhar          TMS#20151008-00138  - Minor enhancements and Bug fixes for New ISR Data Mart and
                         Manjula Chellappan                          Inventory Sales and Reorder Report - WC.
 
     **************************************************************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_LAST_RECEIPT##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
--<<Commented for Ver 5.0 Begin 
/*		 
      l_select_stmt               VARCHAR2 (4000)
         := '   SELECT a.inventory_item_id,                    
                       a.organization_id,                    
                       a.transaction_date last_receipt_date                    
                  FROM (SELECT inventory_item_id,                    
                               organization_id,                    
                               transaction_date,                    
                               ROW_NUMBER ()                    
                               OVER (PARTITION BY inventory_item_id, organization_id                    
                                     ORDER BY transaction_date DESC)                    
                                  AS rn                    
                          FROM mtl_material_transactions                    
                         WHERE 1 = 1 AND transaction_type_id IN (18, 12)) a  						 
                 WHERE rn = 1     ';
*/				 
-->>Commented for Ver 5.0 End		
--<<Added for Ver 5.0 Begin, Added transaction_type_id 61 		 
      l_select_stmt               VARCHAR2 (4000)
         := '   SELECT a.inventory_item_id,                    
                       a.organization_id,                    
                       a.transaction_date last_receipt_date                    
                  FROM (SELECT inventory_item_id,                    
                               organization_id,                    
                               transaction_date,                    
                               ROW_NUMBER ()                    
                               OVER (PARTITION BY inventory_item_id, organization_id                    
                                     ORDER BY transaction_date DESC)                    
                                  AS rn                    
                          FROM mtl_material_transactions                    
                         WHERE 1 = 1 AND transaction_type_id IN (18, 12, 61)) a  						 
                 WHERE rn = 1     ';
-->>Added for Ver 5.0 End		 				             
				 
      l_load_stmt                 VARCHAR2 (4000)
                                     := l_create_table_stmt || l_select_stmt;

      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( inventory_item_id, organization_id)';



      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*,
                       b.last_receipt_date
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_last_receipt## b
                 WHERE a.organization_id = b.organization_id(+)
                   AND a.inventory_item_id = b.inventory_item_id(+) ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec := 'Process 25 : Load Last Receipt Date ';
      write_log (g_line_open);
      write_log (g_sec);

      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);

      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);

      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_last_receipt;


   PROCEDURE Load_oh_gt_270 (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.Load_oh_gt_270 $
     PURPOSE:     26. Procedure to Load Onhand Greater than 270 Days for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    06-Apr-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';
      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_OH_GT_270##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt               VARCHAR2 (4000)
         := '     SELECT inventory_item_id,                
                         organization_id,                
                         SUM (primary_transaction_quantity) onhand_gt_270                
                    FROM mtl_onhand_quantities_detail                
                   WHERE orig_date_received < SYSDATE - 270                
                GROUP BY inventory_item_id, organization_id ';
      l_load_stmt                 VARCHAR2 (4000)
                                     := l_create_table_stmt || l_select_stmt;

      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( inventory_item_id, organization_id)';



      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT a.*,
                       b.onhand_gt_270
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_oh_gt_270## b
                 WHERE a.organization_id = b.organization_id(+)
                   AND a.inventory_item_id = b.inventory_item_id(+) ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;
   BEGIN
      g_sec := 'Process 26 : Load Onhand Greater 270 Days ';
      write_log (g_line_open);
      write_log (g_sec);

      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);

      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);

      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;


      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END Load_oh_gt_270;


   PROCEDURE load_supersede_items (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.load_supersede_items $
     PURPOSE:     27. Procedure to Load Mfg Part Numbers for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    06-Apr-14    Manjula Chellappan    Initial Version
     **************************************************************************/
   IS
      l_retcode                   NUMBER := 0;
      l_owner                     VARCHAR2 (10) := 'XXWC';

      l_table_name                VARCHAR2 (40) := 'XXWC_ISR_SUPER_ITEMS_LIST##';
      l_table_full_name           VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name                VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg         VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg           VARCHAR2 (200)
                                     := l_table_full_name || ' Table Load failed ';
      l_success_msg               VARCHAR2 (200);
      l_error_msg                 VARCHAR2 (200);
      l_index_success_msg         VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg           VARCHAR2 (200)
                                     := l_index_name || ' Index Creation failed ';
      l_create_table_stmt         VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt               VARCHAR2 (4000)
         := '       SELECT s.*,
                           ROW_NUMBER ()
                           OVER (PARTITION BY inventory_item_id, organization_id
                                 ORDER BY supersede_item)
                              rn
                      FROM (SELECT inventory_item_id,
                                   organization_id, related_item_id,
                                   start_date,
                                   end_date,
                                   (SELECT segment1
                                      FROM mtl_system_items_b
                                     WHERE     inventory_item_id = a.related_item_id
                                           AND organization_id = a.organization_id)
                                      supersede_item
                              FROM MTL_RELATED_ITEMS_ALL_V a
                             WHERE     relationship_type_id = 8
                                   AND TRUNC (SYSDATE) BETWEEN TRUNC (NVL (start_date, SYSDATE))
                                                           AND TRUNC (
                                                                  NVL (end_date, SYSDATE + 1))) s    ';
      l_load_stmt                 VARCHAR2 (4000)
                                     := l_create_table_stmt || l_select_stmt;

      l_index_stmt                VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( inventory_item_id,organization_id, rn)';



      l_table_name1               VARCHAR2 (40) := 'XXWC_ISR_SUPER_ITEMS##';
      l_table_full_name1          VARCHAR2 (60) := l_owner || '.' || l_table_name1;
      l_index_name1               VARCHAR2 (60) := l_table_full_name1 || '_N1';
      l_table_success_msg1        VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load completed ';
      l_table_error_msg1          VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load failed ';
      l_index_success_msg1        VARCHAR2 (200)
         := l_index_name1 || ' Index Creation completed ';
      l_index_error_msg1          VARCHAR2 (200)
                                     := l_index_name1 || ' Index Creation failed ';
      l_create_table_stmt1        VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name1 || ' AS ';
      l_select_stmt1              VARCHAR2 (4000)
         := '      SELECT *        
                     FROM XXWC.XXWC_ISR_SUPER_ITEMS_LIST##
                    WHERE rn = 1    ';
      l_load_stmt1                VARCHAR2 (4000)
                                     := l_create_table_stmt1 || l_select_stmt1;

      l_index_stmt1               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name1
            || ' ON '
            || l_table_full_name1
            || ' (inventory_item_id,organization_id)';


      CUR_SUPER_ITEM_RANK         SYS_REFCURSOR;


      L_SUPER_ITEM_RANK_query     VARCHAR2 (500)
         := ' SELECT distinct rn 
                FROM     XXWC.XXWC_ISR_SUPER_ITEMS_LIST## 
                WHERE rn>1
                ORDER BY 1    ';

      l_update_stmt               VARCHAR2 (4000)
         := '  UPDATE XXWC.XXWC_ISR_SUPER_ITEMS## a                    
                   SET supersede_item =                    
                             supersede_item                                                              
                          || (SELECT '','' ||supersede_item                    
                                FROM XXWC.XXWC_ISR_SUPER_ITEMS_LIST##                    
                               WHERE inventory_item_id = a.inventory_item_id 
                                 AND organization_id = a.organization_id 
                                 AND rn = :rn)                    
                 WHERE a.inventory_item_id IN (SELECT inventory_item_id                    
                                                 FROM XXWC.XXWC_ISR_SUPER_ITEMS_LIST##                     
                                                WHERE organization_id = a.organization_id 
                                                  AND rn = a.rn)  ';

      l_rank                      NUMBER;



      l_merge_table_name          VARCHAR2 (40) := 'XXWC_ISR_MERGE##';
      l_merge_table_full_name     VARCHAR2 (60)
                                     := l_owner || '.' || l_merge_table_name;

      l_merge_success_msg         VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load completed ';
      l_merge_error_msg           VARCHAR2 (200)
         := l_merge_table_full_name || ' Table Load failed ';

      l_merge_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_merge_table_full_name || ' AS ';


      l_merge_select_stmt         VARCHAR2 (4000)
         := '   SELECT     a.*,
                        b.supersede_item                          
                  FROM xxwc.xxwc_isr_details## a, xxwc.xxwc_isr_super_items## b
                 WHERE a.inventory_item_id = b.inventory_item_id(+)
                   AND a.master_organization_id = b.organization_id(+)                 ';

      l_merge_load_stmt           VARCHAR2 (4000)
         := l_merge_create_table_stmt || l_merge_select_stmt;



      l_table_name2               VARCHAR2 (40) := 'XXWC_ISR_DETAILS##';
      l_table_full_name2          VARCHAR2 (60)
                                     := l_owner || '.' || l_table_name2;
      l_index_name2               VARCHAR2 (60)
                                     := l_table_full_name2 || '_N1';

      l_index_success_msg2        VARCHAR2 (200)
         := l_index_name2 || ' Index Creation completed ';
      l_index_error_msg2          VARCHAR2 (200)
         := l_index_name2 || ' Index Creation failed ';

      l_index_stmt2               VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name2
            || ' ON '
            || l_table_full_name2
            || ' ( organization_id, inventory_item_id)';
   BEGIN
      g_sec := 'Process 27a : Load Supersede Items List ';
      write_log (g_line_open);
      write_log (g_sec);

      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);

      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);



      g_sec :=
         'Process 27b : Load Supersede Items without Duplicate Item entries ';
      write_log (g_line_open);
      write_log (g_sec);

      l_success_msg := l_table_success_msg1;
      l_error_msg := l_table_error_msg1;

      drop_temp_table (l_owner, l_table_name1);

      EXECUTE IMMEDIATE l_load_stmt1;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg1;
      l_error_msg := l_index_error_msg1;

      EXECUTE IMMEDIATE l_index_stmt1;

      write_log (l_success_msg);


      g_sec := 'Process 27c : Concatenate Supersede Items for items ';
      write_log (g_line_open);
      write_log (g_sec);


      l_success_msg := 'Supersede Items Update completed';



      OPEN CUR_SUPER_ITEM_RANK FOR L_SUPER_ITEM_RANK_QUERY;

      LOOP
         FETCH CUR_SUPER_ITEM_RANK INTO l_rank;

         EXIT WHEN CUR_SUPER_ITEM_RANK%NOTFOUND;

         l_error_msg := 'Rank ' || l_rank || ' Update failed ';

         EXECUTE IMMEDIATE l_update_stmt USING l_rank;
      END LOOP;


      write_log (l_success_msg);


      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;

      drop_temp_table (l_owner, l_merge_table_name);

      EXECUTE IMMEDIATE l_merge_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner, 'XXWC_ISR_MERGE##', 'XXWC_ISR_DETAILS##');

      l_success_msg := l_merge_success_msg;
      l_error_msg := l_merge_error_msg;

      g_sec := 'Create index on XXWC.XXWC_ISR_DETAILS##';
      l_success_msg := l_index_success_msg2;
      l_error_msg := l_index_error_msg2;

      EXECUTE IMMEDIATE l_index_stmt2;


      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_supersede_items;



   PROCEDURE load_isr (p_retcode                OUT VARCHAR2,
                       p_load_old_isr_flag   IN     VARCHAR2) --Added new parameter p_load_old_isr_flag for Ver 4.0
   /*******************************************************************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.Load_isr $
     PURPOSE:    Final. Procedure to Load Item Details for ISR Datamart Refresh

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ---------------------------------------------------------------------------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     4.0    24-Sep-15    Manjula Chellappan    TMS# 20150910-00122 - Inventory Sales and Reorder Report - WC is currently
                                                                     includes direct order quantities for 1,6 and 12
     5.0    15-Oct-15    P.Vamshidhar          TMS#20151008-00138  - Minor enhancements and Bug fixes for New ISR Data Mart and
                         Manjula Chellappan                          Inventory Sales and Reorder Report - WC.
    ******************************************************************************************************************************/
   IS
      l_rec_count           NUMBER := 0; -- Added for Ver 5.0
      l_retcode             NUMBER := 0;
      l_owner               VARCHAR2 (10) := 'XXWC';
      l_table_name          VARCHAR2 (40) := 'XXWC_ISR_DETAILS_ALL##';
      l_table_full_name     VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name          VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg   VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg     VARCHAR2 (200)
                               := l_table_full_name || ' Table Load failed ';
      l_success_msg         VARCHAR2 (200) := l_table_success_msg;
      l_error_msg           VARCHAR2 (200) := l_table_error_msg;
      l_index_success_msg   VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg     VARCHAR2 (200)
                               := l_index_name || ' Index Creation failed ';
      l_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_select_stmt         CLOB := '   ';
      l_load_stmt           VARCHAR2 (4000)
                               := l_create_table_stmt || l_select_stmt;


      l_index_stmt          VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id)';
   BEGIN
      g_sec := 'Process Final : Load ISR ';
      write_log (g_line_open);
      write_log (g_sec);

      drop_temp_table (l_owner, l_table_name);

--Commented for Ver 5.0 Begin
/*	  
      EXECUTE IMMEDIATE
            l_create_table_stmt
         || ' SELECT isr.org org,                 
                       isr.pre,
                       isr.item_number,
                       DECODE(isr.st, ''I'', DECODE(isr.sourcing_rule,''88888'',isr.mst_vendor_num,''99999'', isr.mst_vendor_num, NULL, ''88888'',isr.vendor_num )
                          ,''S'', DECODE(isr.sourcing_rule,''88888'',isr.mst_vendor_num,''99999'', isr.mst_vendor_num, NULL, 
                          DECODE(isr.com_vendor_num,''88888'', isr.mst_vendor_num, ''99999'',isr.mst_vendor_num, isr.com_vendor_num),NVL(isr.vendor_num,''88888'')))    vendor_num,
                       DECODE(isr.st, ''I'', DECODE(isr.sourcing_rule,''88888'',isr.mst_vendor_name,''99999'', isr.mst_vendor_name, NULL, ''88888'',isr.vendor_name)
                         ,''S'', DECODE(isr.sourcing_rule,''88888'',isr.mst_vendor_name,''99999'', isr.mst_vendor_name, NULL, 
                          DECODE(isr.com_vendor_num,''88888'', isr.mst_vendor_name, ''99999'',isr.mst_vendor_name, isr.com_vendor_name),NVL(isr.vendor_name,
                          (SELECT vendor_name FROM AP_SUPPLIERS WHERE segment1 = ''88888'')))) vendor_name,
                       DECODE(isr.st,''S'',isr.vendor_num,''I'',isr.source_org_code) source,
                       isr.st st,
                       isr.description description,
                       isr.cat cat,
                       isr.pplt pplt,
                       isr.plt plt,
                       isr.uom uom,
                       isr.cl cl,
                       isr.stk_flag,
                       isr.flip_date,
                       isr.last_sv_change_date,
                       isr.pm pm,
                       isr.minn minn,
                       isr.maxn maxn,
                       isr.amu amu,  
                       isr.mf_flag mf_flag,
                       isr.hit6_store_sales hit6_store_sales,
                       isr.hit6_other_inv_sales hit6_other_inv_sales,
                       isr.aver_cost aver_cost,                       
                       NVL(isr.bpa_cost,isr.list_price) item_cost,                        
                       isr.bpa bpa,
                       NVL(isr.qoh,0) qoh,
                       isr.available available,
                       (isr.available*isr.aver_cost) availabledollar,
                       isr.one_store_sale one_store_sale,
                       isr.six_store_sale six_store_sale,
                       isr.twelve_store_sale twelve_store_sale,
                       isr.one_other_inv_sale one_other_inv_sale,
                       isr.six_other_inv_sale six_other_inv_sale,
                       isr.twelve_other_inv_sale twelve_other_inv_sale,
                       isr.bin_loc bin_loc,
                       isr.mc mc,
                       isr.fi_flag fi_flag,
                       isr.freeze_date freeze_date,
                       isr.res res,
                       isr.thirteen_wk_avg_inv thirteen_wk_avg_inv,
                       isr.thirteen_wk_an_cogs thirteen_wk_an_cogs,
                       isr.turns turns,
                       isr.buyer buyer,
                       isr.ts ts,
                       isr.jan_store_sale jan_store_sale,
                       isr.feb_store_sale feb_store_sale,
                       isr.mar_store_sale mar_store_sale,
                       isr.apr_store_sale apr_store_sale,
                       isr.may_store_sale may_store_sale,
                       isr.jun_store_sale jun_store_sale,
                       isr.jul_store_sale jul_store_sale,
                       isr.aug_store_sale aug_store_sale,
                       isr.sep_store_sale sep_store_sale,
                       isr.oct_store_sale oct_store_sale,
                       isr.nov_store_sale nov_store_sale,
                       isr.dec_store_sale dec_store_sale,
                       isr.jan_other_inv_sale jan_other_inv_sale,
                       isr.feb_other_inv_sale feb_other_inv_sale,
                       isr.mar_other_inv_sale mar_other_inv_sale,
                       isr.apr_other_inv_sale apr_other_inv_sale,
                       isr.may_other_inv_sale may_other_inv_sale,
                       isr.jun_other_inv_sale jun_other_inv_sale,
                       isr.jul_other_inv_sale jul_other_inv_sale,
                       isr.aug_other_inv_sale aug_other_inv_sale,
                       isr.sep_other_inv_sale sep_other_inv_sale,
                       isr.oct_other_inv_sale oct_other_inv_sale,
                       isr.nov_other_inv_sale nov_other_inv_sale,
                       isr.dec_other_inv_sale dec_other_inv_sale,
                       isr.hit4_store_sales hit4_store_sales,
                       isr.hit4_other_inv_sales hit4_other_inv_sales,
                       isr.inventory_item_id inventory_item_id,
                       isr.organization_id organization_id,
                       isr.source_organization_id source_organization_id,
                       isr.set_of_books_id, 
                       isr.org_name org_name,
                       isr.district district,
                       isr.region region,
                       isr.common_output_id common_output_id,
                       isr.process_id process_id,
                       isr.inv_cat_seg1 inv_cat_seg1,
                       NVL(isr.on_ord,0) on_ord,
                       isr.bpa_cost bpa_cost,
                       NVL(isr.open_req,0) open_req,
                       isr.wt wt,
                       isr.fml fml,
                       isr.sourcing_rule sourcing_rule,
                       isr.so so,
                       isr.ss ss,
                       isr.clt clt,
                       isr.qoh - isr.demand  avail2,
                       NVL(isr.int_req,0) int_req,
                       NVL(isr.dir_req,0) dir_req,
                       isr.demand demand,
                       isr.item_status_code item_status_code,
                       isr.site_vendor_num site_vendor_num,
                       isr.vendor_site vendor_site,
                       isr.core core,
                       isr.tier tier,                       
                       isr.cat_sba_owner cat_sba_owner,
                       isr.mfg_part_number,
                       isr.mst_vendor_num mst_vendor,
                       isr.make_buy,
                       isr.org_item_status,
                       isr.org_user_item_type,
                       (SELECT org_item_status
                          FROM xxwc.xxwc_isr_details## 
                         WHERE organization_id = isr.master_organization_id
                           AND inventory_item_id = isr.inventory_item_id) mst_item_status,
                       (SELECT org_user_item_type
                          FROM xxwc.xxwc_isr_details## 
                         WHERE organization_id = isr.master_organization_id
                           AND inventory_item_id = isr.inventory_item_id) mst_user_item_type,
                       isr.last_receipt_date,
                       NVL(isr.onhand_gt_270,0)* NVL(isr.aver_cost,0) onhand_gt_270,
                       isr.supersede_item,                       
                       isr.org_id org_id,
                       isr.operating_unit operating_unit
                  FROM xxwc.xxwc_isr_details## isr ';  
*/
-- Commented for Ver 5.0 End
-- Added for Ver 5.0 Begin
-- Changes to vendor_num, vendor_name, list_price, aver_cost
  
      EXECUTE IMMEDIATE
            l_create_table_stmt
         || ' SELECT isr.org org,                 
                       isr.pre,
                       isr.item_number,
                       DECODE(isr.st, ''I'', DECODE(isr.sourcing_rule,''88888'',NVL(isr.mst_vendor_num,''88888''),''99999'', NVL(isr.mst_vendor_num,''88888''), NULL, ''88888'',isr.vendor_num )
                          ,''S'', DECODE(isr.sourcing_rule,''88888'',NVL(isr.mst_vendor_num,''88888''),''99999'', NVL(isr.mst_vendor_num,''88888''), NULL, 
                          DECODE(isr.com_vendor_num,''88888'', NVL(isr.mst_vendor_num,''88888''), ''99999'',NVL(isr.mst_vendor_num,''88888''), NULL, NVL(isr.mst_vendor_num,''88888''), isr.com_vendor_num),NVL(isr.vendor_num,''88888''))
						  ,''D'', DECODE(isr.sourcing_rule,''88888'',NVL(isr.mst_vendor_num,''88888''),''99999'', NVL(isr.mst_vendor_num,''88888''), NULL, 
                          DECODE(isr.com_vendor_num,''88888'', NVL(isr.mst_vendor_num,''88888''), ''99999'',NVL(isr.mst_vendor_num,''88888''), NULL, NVL(isr.mst_vendor_num,''88888''), isr.com_vendor_num),NVL(isr.vendor_num,''88888''))) vendor_num,
                       DECODE(isr.st, ''I'', DECODE(isr.sourcing_rule,''88888'',NVL(isr.mst_vendor_name,(SELECT vendor_name FROM AP_SUPPLIERS WHERE segment1 = ''88888'')),''99999'', NVL(isr.mst_vendor_name,(SELECT vendor_name FROM AP_SUPPLIERS WHERE segment1 = ''88888'')), NULL, ''88888'',isr.vendor_name)
                         ,''S'', DECODE(isr.sourcing_rule,''88888'',NVL(isr.mst_vendor_name,(SELECT vendor_name FROM AP_SUPPLIERS WHERE segment1 = ''88888'')),''99999'', NVL(isr.mst_vendor_name,(SELECT vendor_name FROM AP_SUPPLIERS WHERE segment1 = ''88888'')), NULL, 
                          DECODE(isr.com_vendor_num,''88888'', NVL(isr.mst_vendor_name,(SELECT vendor_name FROM AP_SUPPLIERS WHERE segment1 = ''88888'')), ''99999'', NVL(isr.mst_vendor_name,(SELECT vendor_name FROM AP_SUPPLIERS WHERE segment1 = ''88888''))
						   , NULL, NVL(isr.mst_vendor_name,(SELECT vendor_name FROM AP_SUPPLIERS WHERE segment1 = ''88888'')), isr.com_vendor_name), NVL(isr.vendor_name,
                          NVL(isr.mst_vendor_name,(SELECT vendor_name FROM AP_SUPPLIERS WHERE segment1 = ''88888''))))
						 ,''D'', DECODE(isr.sourcing_rule,''88888'',NVL(isr.mst_vendor_name,(SELECT vendor_name FROM AP_SUPPLIERS WHERE segment1 = ''88888'')),''99999'', NVL(isr.mst_vendor_name,(SELECT vendor_name FROM AP_SUPPLIERS WHERE segment1 = ''88888'')), NULL, 
                          DECODE(isr.com_vendor_num,''88888'', NVL(isr.mst_vendor_name,(SELECT vendor_name FROM AP_SUPPLIERS WHERE segment1 = ''88888'')), ''99999'', NVL(isr.mst_vendor_name,(SELECT vendor_name FROM AP_SUPPLIERS WHERE segment1 = ''88888''))
						   , NULL, NVL(isr.mst_vendor_name,(SELECT vendor_name FROM AP_SUPPLIERS WHERE segment1 = ''88888'')), isr.com_vendor_name),NVL(isr.vendor_name,
                          NVL(isr.mst_vendor_name,(SELECT vendor_name FROM AP_SUPPLIERS WHERE segment1 = ''88888''))))) vendor_name,
                       DECODE(isr.st,''S'',DECODE(isr.sourcing_rule,''88888'',NVL(isr.mst_vendor_num,''88888''),''99999'', NVL(isr.mst_vendor_num,''88888''), NULL, 
                          DECODE(isr.com_vendor_num,''88888'', NVL(isr.mst_vendor_num,''88888''), ''99999'',NVL(isr.mst_vendor_num,''88888''), NULL, NVL(isr.mst_vendor_num,''88888''), isr.com_vendor_num),NVL(isr.vendor_num,''88888''))
						            ,''I'',isr.source_org_code) source,
                       DECODE(isr.st,''D'',NULL,isr.st) st,
                       isr.description description,
                       isr.cat cat,
                       isr.pplt pplt,
                       isr.plt plt,
                       isr.uom uom,
                       isr.cl cl,
                       isr.stk_flag,
                       isr.flip_date,
                       isr.last_sv_change_date,
                       isr.pm pm,
                       isr.minn minn,
                       isr.maxn maxn,
                       isr.amu amu,  
                       isr.mf_flag mf_flag,
                       isr.hit6_store_sales hit6_store_sales,
                       isr.hit6_other_inv_sales hit6_other_inv_sales,                       
                       NVL(isr.aver_cost,0) aver_cost, 
                       isr.List_Price list_price, 
                       isr.bpa bpa,
                       NVL(isr.qoh,0) qoh,
                       isr.available available,
                       (isr.available*isr.aver_cost) availabledollar,
                       isr.one_store_sale one_store_sale,
                       isr.six_store_sale six_store_sale,
                       isr.twelve_store_sale twelve_store_sale,
                       isr.one_other_inv_sale one_other_inv_sale,
                       isr.six_other_inv_sale six_other_inv_sale,
                       isr.twelve_other_inv_sale twelve_other_inv_sale,
                       isr.bin_loc bin_loc,
                       isr.mc mc,
                       isr.fi_flag fi_flag,
                       isr.freeze_date freeze_date,
                       isr.res res,
                       isr.thirteen_wk_avg_inv thirteen_wk_avg_inv,
                       isr.thirteen_wk_an_cogs thirteen_wk_an_cogs,
                       DECODE(isr.turns,0,NULL,isr.turns) turns,
                       isr.buyer buyer,
                       isr.ts ts,
                       isr.jan_store_sale jan_store_sale,
                       isr.feb_store_sale feb_store_sale,
                       isr.mar_store_sale mar_store_sale,
                       isr.apr_store_sale apr_store_sale,
                       isr.may_store_sale may_store_sale,
                       isr.jun_store_sale jun_store_sale,
                       isr.jul_store_sale jul_store_sale,
                       isr.aug_store_sale aug_store_sale,
                       isr.sep_store_sale sep_store_sale,
                       isr.oct_store_sale oct_store_sale,
                       isr.nov_store_sale nov_store_sale,
                       isr.dec_store_sale dec_store_sale,
                       isr.jan_other_inv_sale jan_other_inv_sale,
                       isr.feb_other_inv_sale feb_other_inv_sale,
                       isr.mar_other_inv_sale mar_other_inv_sale,
                       isr.apr_other_inv_sale apr_other_inv_sale,
                       isr.may_other_inv_sale may_other_inv_sale,
                       isr.jun_other_inv_sale jun_other_inv_sale,
                       isr.jul_other_inv_sale jul_other_inv_sale,
                       isr.aug_other_inv_sale aug_other_inv_sale,
                       isr.sep_other_inv_sale sep_other_inv_sale,
                       isr.oct_other_inv_sale oct_other_inv_sale,
                       isr.nov_other_inv_sale nov_other_inv_sale,
                       isr.dec_other_inv_sale dec_other_inv_sale,
                       isr.hit4_store_sales hit4_store_sales,
                       isr.hit4_other_inv_sales hit4_other_inv_sales,
                       isr.inventory_item_id inventory_item_id,
                       isr.organization_id organization_id,
                       isr.source_organization_id source_organization_id,
                       isr.set_of_books_id, 
                       isr.org_name org_name,
                       isr.district district,
                       isr.region region,
                       isr.common_output_id common_output_id,
                       isr.process_id process_id,
                       isr.inv_cat_seg1 inv_cat_seg1,
                       NVL(isr.on_ord,0) on_ord,
                       isr.bpa_cost bpa_cost,
                       NVL(isr.open_req,0) open_req,
                       isr.wt wt,
                       isr.fml fml,
                       isr.sourcing_rule sourcing_rule,
                       isr.so so,
                       isr.ss ss,
                       isr.clt clt,
                       isr.qoh - isr.demand  avail2,
                       NVL(isr.int_req,0) int_req,
                       NVL(isr.dir_req,0) dir_req,
                       isr.demand demand,
                       isr.item_status_code item_status_code,
                       isr.site_vendor_num site_vendor_num,
                       isr.vendor_site vendor_site,
                       isr.core core,
                       isr.tier tier,                       
                       isr.cat_sba_owner cat_sba_owner,
                       isr.mfg_part_number,
                       isr.mst_vendor_num mst_vendor,
                       isr.make_buy,
                       isr.org_item_status,
                       isr.org_user_item_type,
                       (SELECT org_item_status
                          FROM xxwc.xxwc_isr_details## 
                         WHERE organization_id = isr.master_organization_id
                           AND inventory_item_id = isr.inventory_item_id) mst_item_status,
                       (SELECT org_user_item_type
                          FROM xxwc.xxwc_isr_details## 
                         WHERE organization_id = isr.master_organization_id
                           AND inventory_item_id = isr.inventory_item_id) mst_user_item_type,
                       isr.last_receipt_date,
                       NVL(isr.onhand_gt_270,0)* NVL(isr.aver_cost,0) onhand_gt_270,
                       isr.supersede_item,                       
                       isr.org_id org_id,
                       isr.operating_unit operating_unit
                  FROM xxwc.xxwc_isr_details## isr '; 
--Added for Ver 5.0	End			  

      write_log (l_success_msg);

      -----------------------------------------
      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);


      g_sec := 'Process Final : Update Source for Internal Items ';

      l_success_msg := 'Update Vendor Num for Internal Items Completed ';
      l_error_msg := 'Update Vendor Num for Internal Items Failed';


      EXECUTE IMMEDIATE
         ' UPDATE xxwc.xxwc_isr_details_all## isr
           SET vendor_num =
                  (SELECT vendor_num
                     FROM xxwc.xxwc_isr_details_all##
                    WHERE     inventory_item_id = isr.inventory_item_id
                          AND organization_id = isr.source_organization_id)
         WHERE st = ''I''';

      write_log (l_success_msg);

      l_success_msg := 'Update Vendor Name for Internal Items Completed ';
      l_error_msg := 'Update Vendor Name for Internal Items Failed';

      EXECUTE IMMEDIATE
         ' UPDATE xxwc.xxwc_isr_details_all## isr
           SET vendor_name =
                  (SELECT vendor_name
                     FROM xxwc.xxwc_isr_details_all##
                    WHERE     inventory_item_id = isr.inventory_item_id
                          AND organization_id = isr.source_organization_id)
         WHERE st = ''I''';

      write_log (l_success_msg);

      g_sec :=
         'Process Finala : Archive Final table XXWC.XXWC_ISR_DETAILS_ALL';
      write_log (g_sec);
	  
--<<Added for Ver 5.0 Begin

      g_sec :=
         'Process Finala : Check if any data to archive from previos run';
      write_log (g_sec);	  

      SELECT COUNT(1) INTO l_rec_count from XXWC.XXWC_ISR_DETAILS_ALL;	

      IF NVL(l_rec_count,0)>0 THEN	  

-->>Added for Ver 5.0 End

      l_success_msg := 'XXWC.XXWC_ISR_DETAILS_ARCH_ALL Truncate Completed ';
      l_error_msg := 'XXWC.XXWC_ISR_DETAILS_ARCH_ALL Truncate Failed ';

      EXECUTE IMMEDIATE ' TRUNCATE TABLE XXWC.XXWC_ISR_DETAILS_ARCH_ALL ';

      write_log (l_success_msg);

      l_success_msg :=
         'Archive Table XXWC.XXWC_ISR_DETAILS_ARCH_ALL Load Completed ';
      l_error_msg :=
         'Archive  Table XXWC.XXWC_ISR_DETAILS_ARCH_ALL Load Failed ';

      EXECUTE IMMEDIATE
         ' INSERT /*+ append */ INTO  XXWC.XXWC_ISR_DETAILS_ARCH_ALL
                            (SELECT * FROM XXWC.XXWC_ISR_DETAILS_ALL)   ';

      write_log (l_success_msg);

--<<Added for Ver 5.0 Begin	  
     ELSE 

	 write_log ('Archive not done as there is no data to archive');
	 
	 END IF;
-->>Added for Ver 5.0 End	 	  

      g_sec := 'Process Finalb : Load Final table XXWC.XXWC_ISR_DETAILS_ALL';
      write_log (g_sec);

      l_success_msg := 'Final Table XXWC.XXWC_ISR_DETAILS_ALL Load Completed ';
      l_error_msg := 'Final  Table XXWC.XXWC_ISR_DETAILS_ALL Load Failed ';

      EXECUTE IMMEDIATE ' TRUNCATE TABLE XXWC.XXWC_ISR_DETAILS_ALL ';

      EXECUTE IMMEDIATE
         ' INSERT /*+ append */ INTO XXWC.XXWC_ISR_DETAILS_ALL 
                                      (ORG,
                                       PRE,
                                       ITEM_NUMBER,
                                       VENDOR_NUM,
                                       VENDOR_NAME,
                                       SOURCE,
                                       ST,
                                       DESCRIPTION,
                                       CAT,
                                       PPLT,
                                       PLT,
                                       UOM,
                                       CL,
                                       STK_FLAG,
                                       FLIP_DATE,
                                       LAST_SV_CHANGE_DATE,                                       
                                       PM,
                                       MINN,
                                       MAXN,
                                       AMU,
                                       MF_FLAG,
                                       HIT6_STORE_SALES,
                                       HIT6_OTHER_INV_SALES,
                                       AVER_COST,
                                       --ITEM_COST,  Commented for Ver 5.0
                                       LIST_PRICE, -- Added for Ver 5.0
                                       BPA,
                                       QOH,
                                       AVAILABLE,
                                       AVAILABLEDOLLAR,
                                       ONE_STORE_SALE,
                                       SIX_STORE_SALE,
                                       TWELVE_STORE_SALE,
                                       ONE_OTHER_INV_SALE,
                                       SIX_OTHER_INV_SALE,
                                       TWELVE_OTHER_INV_SALE,
                                       BIN_LOC,
                                       MC,
                                       FI_FLAG,
                                       FREEZE_DATE,
                                       RES,
                                       THIRTEEN_WK_AVG_INV,
                                       THIRTEEN_WK_AN_COGS,
                                       TURNS,
                                       BUYER,
                                       TS,
                                       JAN_STORE_SALE,
                                       FEB_STORE_SALE,
                                       MAR_STORE_SALE,
                                       APR_STORE_SALE,
                                       MAY_STORE_SALE,
                                       JUN_STORE_SALE,
                                       JUL_STORE_SALE,
                                       AUG_STORE_SALE,
                                       SEP_STORE_SALE,
                                       OCT_STORE_SALE,
                                       NOV_STORE_SALE,
                                       DEC_STORE_SALE,
                                       JAN_OTHER_INV_SALE,
                                       FEB_OTHER_INV_SALE,
                                       MAR_OTHER_INV_SALE,
                                       APR_OTHER_INV_SALE,
                                       MAY_OTHER_INV_SALE,
                                       JUN_OTHER_INV_SALE,
                                       JUL_OTHER_INV_SALE,
                                       AUG_OTHER_INV_SALE,
                                       SEP_OTHER_INV_SALE,
                                       OCT_OTHER_INV_SALE,
                                       NOV_OTHER_INV_SALE,
                                       DEC_OTHER_INV_SALE,
                                       HIT4_STORE_SALES,
                                       HIT4_OTHER_INV_SALES,
                                       INVENTORY_ITEM_ID,
                                       ORGANIZATION_ID,
                                       SET_OF_BOOKS_ID,
                                       ORG_NAME,
                                       DISTRICT,
                                       REGION,
                                       COMMON_OUTPUT_ID,
                                       PROCESS_ID,
                                       INV_CAT_SEG1,
                                       ON_ORD,
                                       BPA_COST,
                                       OPEN_REQ,
                                       WT,
                                       FML,
                                       SOURCING_RULE,
                                       SO,
                                       SS,
                                       CLT,
                                       AVAIL2,
                                       INT_REQ,
                                       DIR_REQ,
                                       DEMAND,
                                       ITEM_STATUS_CODE,
                                       SITE_VENDOR_NUM,
                                       VENDOR_SITE,
                                       CORE,                                        
                                       TIER,
                                       CAT_SBA_OWNER,    
                                       MFG_PART_NUMBER,
                                       MST_VENDOR,                                       
                                       MAKE_BUY,
                                       ORG_ITEM_STATUS,                                       
                                       ORG_USER_ITEM_TYPE,                                           
                                       MST_ITEM_STATUS,                                               
                                       MST_USER_ITEM_TYPE,
                                       LAST_RECEIPT_DATE,                                       
                                       ONHAND_GT_270,                                           
                                       SUPERSEDE_ITEM,                                           
                                       ORG_ID,
                                       OPERATING_UNIT
                                       )
   SELECT ORG,
          PRE,
          ITEM_NUMBER,
          VENDOR_NUM,
          VENDOR_NAME,
          SOURCE,
          ST,
          DESCRIPTION,
          CAT,
          PPLT,
          PLT,
          UOM,
          CL,
          STK_FLAG,
          FLIP_DATE,
          LAST_SV_CHANGE_DATE,                    
          PM,
          MINN,
          MAXN,
          AMU,
          NULL,
          HIT6_STORE_SALES,
          HIT6_OTHER_INV_SALES,
          AVER_COST,
          --ITEM_COST, Commented for Ver 5.0
          LIST_PRICE, -- Added for Ver 5.0
          BPA,
          QOH,
          AVAILABLE,
          AVAILABLEDOLLAR,
          ONE_STORE_SALE,
          SIX_STORE_SALE,
          TWELVE_STORE_SALE,
          ONE_OTHER_INV_SALE,
          SIX_OTHER_INV_SALE,
          TWELVE_OTHER_INV_SALE,
          BIN_LOC,
          NULL MC,
          FI_FLAG,
          NULL FREEZE_DATE,
          RES,
          NULL THIRTEEN_WK_AVG_INV,
          NULL THIRTEEN_WK_AN_COGS,
--        0 TURNS, -- Commented for Ver 5.0
          TURNS, -- Added for Ver 5.0
          BUYER,
          TS,
          JAN_STORE_SALE,
          FEB_STORE_SALE,
          MAR_STORE_SALE,
          APR_STORE_SALE,
          MAY_STORE_SALE,
          JUN_STORE_SALE,
          JUL_STORE_SALE,
          AUG_STORE_SALE,
          SEP_STORE_SALE,
          OCT_STORE_SALE,
          NOV_STORE_SALE,
          DEC_STORE_SALE,
          JAN_OTHER_INV_SALE,
          FEB_OTHER_INV_SALE,
          MAR_OTHER_INV_SALE,
          APR_OTHER_INV_SALE,
          MAY_OTHER_INV_SALE,
          JUN_OTHER_INV_SALE,
          JUL_OTHER_INV_SALE,
          AUG_OTHER_INV_SALE,
          SEP_OTHER_INV_SALE,
          OCT_OTHER_INV_SALE,
          NOV_OTHER_INV_SALE,
          DEC_OTHER_INV_SALE,
          HIT4_STORE_SALES,
          HIT4_OTHER_INV_SALES,
          INVENTORY_ITEM_ID,
          ORGANIZATION_ID,
          SET_OF_BOOKS_ID,
          ORG_NAME,
          DISTRICT,
          REGION,
          NULL COMMON_OUTPUT_ID,
          NULL PROCESS_ID,
          INV_CAT_SEG1,
          ON_ORD,
          BPA_COST,
          OPEN_REQ,
          WT,
          FML,
          SOURCING_RULE,
          SO,
          SS,
          CLT,
          AVAIL2,
          INT_REQ,
          DIR_REQ,
          DEMAND,
          ITEM_STATUS_CODE,
          SITE_VENDOR_NUM,
          VENDOR_SITE,
          CORE,                                        
          TIER,
          CAT_SBA_OWNER,    
          MFG_PART_NUMBER,
          MST_VENDOR,                                       
          MAKE_BUY,
          ORG_ITEM_STATUS,                                       
          ORG_USER_ITEM_TYPE,                                           
          MST_ITEM_STATUS,                                               
          MST_USER_ITEM_TYPE,
          LAST_RECEIPT_DATE,                                       
          ONHAND_GT_270,                                           
          SUPERSEDE_ITEM,                                           
          ORG_ID,
          OPERATING_UNIT
     FROM XXWC.XXWC_ISR_DETAILS_ALL## ';

      write_log (l_success_msg);

      --<<Added for Ver 4.0 Begin

      IF NVL (p_load_old_isr_flag, 'N') = 'Y'
      THEN
         g_sec :=
            'Process Finalc : Load Old ISR Table XXEIS.EIS_XXWC_PO_ISR_TAB';
         write_log (g_sec);

         l_success_msg :=
            'Old ISR Table XXEIS.EIS_XXWC_PO_ISR_TAB Load Completed ';
         l_error_msg := 'Old ISR Table XXEIS.EIS_XXWC_PO_ISR_TAB Load Failed ';

         EXECUTE IMMEDIATE ' TRUNCATE TABLE XXEIS.EIS_XXWC_PO_ISR_TAB ';

         EXECUTE IMMEDIATE
            ' INSERT /*+ append */ INTO XXEIS.EIS_XXWC_PO_ISR_TAB 
                                      (ORG,
                                       PRE,
                                       ITEM_NUMBER,
                                       VENDOR_NUM,
                                       VENDOR_NAME,
                                       SOURCE,
                                       ST,
                                       DESCRIPTION,
                                       CAT,
                                       PPLT,
                                       PLT,
                                       UOM,
                                       CL,
                                       STK_FLAG,                                    
                                       PM,
                                       MINN,
                                       MAXN,
                                       AMU,
                                       MF_FLAG,
                                       HIT6_STORE_SALES,
                                       HIT6_OTHER_INV_SALES,
                                       AVER_COST,
                                       ITEM_COST,
                                       BPA,
                                       QOH,
                                       AVAILABLE,
                                       AVAILABLEDOLLAR,
                                       ONE_STORE_SALE,
                                       SIX_STORE_SALE,
                                       TWELVE_STORE_SALE,
                                       ONE_OTHER_INV_SALE,
                                       SIX_OTHER_INV_SALE,
                                       TWELVE_OTHER_INV_SALE,
                                       BIN_LOC,
                                       MC,
                                       FI_FLAG,
                                       FREEZE_DATE,
                                       RES,
                                       THIRTEEN_WK_AVG_INV,
                                       THIRTEEN_WK_AN_COGS,
                                       TURNS,
                                       BUYER,
                                       TS,
                                       JAN_STORE_SALE,
                                       FEB_STORE_SALE,
                                       MAR_STORE_SALE,
                                       APR_STORE_SALE,
                                       MAY_STORE_SALE,
                                       JUN_STORE_SALE,
                                       JUL_STORE_SALE,
                                       AUG_STORE_SALE,
                                       SEP_STORE_SALE,
                                       OCT_STORE_SALE,
                                       NOV_STORE_SALE,
                                       DEC_STORE_SALE,
                                       JAN_OTHER_INV_SALE,
                                       FEB_OTHER_INV_SALE,
                                       MAR_OTHER_INV_SALE,
                                       APR_OTHER_INV_SALE,
                                       MAY_OTHER_INV_SALE,
                                       JUN_OTHER_INV_SALE,
                                       JUL_OTHER_INV_SALE,
                                       AUG_OTHER_INV_SALE,
                                       SEP_OTHER_INV_SALE,
                                       OCT_OTHER_INV_SALE,
                                       NOV_OTHER_INV_SALE,
                                       DEC_OTHER_INV_SALE,
                                       HIT4_STORE_SALES,
                                       HIT4_OTHER_INV_SALES,
                                       INVENTORY_ITEM_ID,
                                       ORGANIZATION_ID,
                                       SET_OF_BOOKS_ID,
                                       ORG_NAME,
                                       DISTRICT,
                                       REGION,
                                       COMMON_OUTPUT_ID,
                                       PROCESS_ID,
                                       INV_CAT_SEG1,
                                       ON_ORD,
                                       BPA_COST,
                                       OPEN_REQ,
                                       WT,
                                       FML,
                                       SOURCING_RULE,
                                       SO,
                                       SS,
                                       CLT,
                                       AVAIL2,
                                       INT_REQ,
                                       DIR_REQ,
                                       DEMAND,
                                       ITEM_STATUS_CODE,
                                       SITE_VENDOR_NUM,
                                       VENDOR_SITE                                       
                                       )
   SELECT ORG,
          PRE,
          ITEM_NUMBER,
          VENDOR_NUM,
          VENDOR_NAME,
          SOURCE,
          ST,
          DESCRIPTION,
          CAT,
          PPLT,
          PLT,
          UOM,
          CL,
          STK_FLAG,                   
          PM,
          MINN,
          MAXN,
          AMU,
          NULL,
          HIT6_STORE_SALES,
          HIT6_OTHER_INV_SALES,
          AVER_COST,
          ITEM_COST,            
          BPA,
          QOH,
          AVAILABLE,
          AVAILABLEDOLLAR,
          ONE_STORE_SALE,
          SIX_STORE_SALE,
          TWELVE_STORE_SALE,
          ONE_OTHER_INV_SALE,
          SIX_OTHER_INV_SALE,
          TWELVE_OTHER_INV_SALE,
          BIN_LOC,
          NULL MC,
          FI_FLAG,
          NULL FREEZE_DATE,
          RES,
          NULL THIRTEEN_WK_AVG_INV,
          NULL THIRTEEN_WK_AN_COGS,
--        0 TURNS, -- Commented for Ver 5.0
          TURNS, -- Added for Ver 5.0
          BUYER,
          TS,
          JAN_STORE_SALE,
          FEB_STORE_SALE,
          MAR_STORE_SALE,
          APR_STORE_SALE,
          MAY_STORE_SALE,
          JUN_STORE_SALE,
          JUL_STORE_SALE,
          AUG_STORE_SALE,
          SEP_STORE_SALE,
          OCT_STORE_SALE,
          NOV_STORE_SALE,
          DEC_STORE_SALE,
          JAN_OTHER_INV_SALE,
          FEB_OTHER_INV_SALE,
          MAR_OTHER_INV_SALE,
          APR_OTHER_INV_SALE,
          MAY_OTHER_INV_SALE,
          JUN_OTHER_INV_SALE,
          JUL_OTHER_INV_SALE,
          AUG_OTHER_INV_SALE,
          SEP_OTHER_INV_SALE,
          OCT_OTHER_INV_SALE,
          NOV_OTHER_INV_SALE,
          DEC_OTHER_INV_SALE,
          HIT4_STORE_SALES,
          HIT4_OTHER_INV_SALES,
          INVENTORY_ITEM_ID,
          ORGANIZATION_ID,
          SET_OF_BOOKS_ID,
          ORG_NAME,
          DISTRICT,
          REGION,
          NULL COMMON_OUTPUT_ID,
          NULL PROCESS_ID,
          INV_CAT_SEG1,
          ON_ORD,
          BPA_COST,
          OPEN_REQ,
          WT,
          FML,
          SOURCING_RULE,
          SO,
          SS,
          CLT,
          AVAIL2,
          INT_REQ,
          DIR_REQ,
          DEMAND,
          ITEM_STATUS_CODE,
          SITE_VENDOR_NUM,
          VENDOR_SITE
     FROM XXWC.XXWC_ISR_DETAILS_ALL## ';

         write_log (l_success_msg);
      END IF;

      -->>Added for Ver 4.0 End

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_isr;


   PROCEDURE load_isr_main (errbuf                   OUT VARCHAR2,
                            retcode                  OUT NUMBER,
                            p_load_old_isr_flag   IN     VARCHAR2) -- Added new Parameter for Ver 4.0
   /*************************************************************************
     $Header XXWC_ISR_DATAMART_PKG.Load_isr_main $
     PURPOSE:     Main. Procedure to Load ISR data from all the sub loads
     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    16-Oct-14    Manjula Chellappan    Initial Version
     4.0    24-Sep-15    Manjula Chellappan    TMS# 20150910-00122 - Inventory Sales and Reorder Report - WC is currently
                                                                     includes direct order quantities for 1,6 and 12
     **************************************************************************/
   IS
      l_retcode   NUMBER := 0;
   BEGIN
      g_sec := 'Process Main : Load ISR main ';
      write_log (g_line_open);
      write_log (g_sec);
      write_log (g_line_close);

      --Cleanup the log Table
      cleanup_log;

      -- Process 1
      load_ou (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 2
      load_inv_org (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 3
      load_items (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;


      -- Process 4

      load_category (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;


      -- Process 5

      load_item_source (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;


      -- Process 6
      load_sales_hit (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;


      -- Process 7
      load_other_sales_hit (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 8
      load_average_cost (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 9

      load_bpa_cost (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;


      -- Process 10
      load_primary_binloc (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 11
      load_sales_qty (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 12
      load_other_sales_qty (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 13
      load_safety_stock (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 14
      load_gl_periods (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 15
      load_monthly_sales (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 16
      load_monthly_osales (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 17
      load_on_ord_qty (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 18
      load_int_req (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 19
      load_po_req (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 20
      load_reserv_qty (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 21
      load_qoh (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 22
      load_demand (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 23
      load_mfg_items (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 24
      load_mst_vendor (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 25
      load_last_receipt (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;


      -- Process 26
      load_oh_gt_270 (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process 27
      load_supersede_items (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      -- Process Final
      load_isr (l_retcode, p_load_old_isr_flag); -- Added new Parameter p_load_old_isr_flag for Ver 4.0

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;


     <<laststep>>
      IF l_retcode = 2
      THEN
         retcode := 2;
         errbuf := g_errbuf;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_ISR_DATAMART_PKG.LOAD_ISR_MAIN',
            p_calling             => g_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (g_errbuf, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'PO');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_ISR_DATAMART_PKG.LOAD_ISR_MAIN',
            p_calling             => g_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (g_errbuf, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'PO');
   END load_isr_main;


   PROCEDURE report_isr /*************************************************************************
                         $Header XXWC_ISR_DATAMART_PKG.report_isr $
                         PURPOSE:     Report. Procedure to use for Reporting
                         REVISIONS:
                         Ver    Date         Author                Description
                         ------ -----------  ------------------    ----------------
                         1.0    16-Oct-14    Manjula Chellappan    Initial Version
                         3.0    30-Jul-15    M Hari Prasad          TMS# 20150717-00247 - EIS Report for new ISR errors when submitted for single item
                         **************************************************************************/
                        (p_process_id            IN NUMBER,
                         p_region                IN VARCHAR2,
                         p_district              IN VARCHAR2,
                         p_location              IN VARCHAR2,
                         p_dc_mode               IN VARCHAR2,
                         p_tool_repair           IN VARCHAR2,
                         p_time_sensitive        IN VARCHAR2,
                         p_stk_items_with_hit4   IN VARCHAR2,
                         p_report_condition      IN VARCHAR2,
                         p_report_criteria       IN VARCHAR2,
                         p_report_criteria_val   IN VARCHAR2,
                         p_start_bin_loc         IN VARCHAR2,
                         p_end_bin_loc           IN VARCHAR2,
                         p_vendor                IN VARCHAR2,
                         p_item                  IN VARCHAR2,
                         p_cat_class             IN VARCHAR2,
                         p_org_list              IN VARCHAR2,
                         p_item_list             IN VARCHAR2,
                         p_supplier_list         IN VARCHAR2,
                         p_cat_class_list        IN VARCHAR2,
                         p_source_list           IN VARCHAR2,
                         p_intangibles           IN VARCHAR2)
   IS
      l_query               VARCHAR2 (32000);
      l_condition_str       VARCHAR2 (32000);
      lv_program_location   VARCHAR2 (2000);
      l_ref_cursor          cursor_type;
      l_insert_recd_cnt     NUMBER;
      l_supplier_exists     VARCHAR2 (32767);
      l_org_exists          VARCHAR2 (32000);
      l_item_exists         VARCHAR2 (32000);
      l_catclass_exists     VARCHAR2 (32000);
      l_source_exists       VARCHAR2 (32000);
   BEGIN
      IF p_region IS NOT NULL
      THEN
         l_condition_str :=
               l_condition_str
            || 'and REGION in ('
            || xxeis.eis_rs_utility.get_param_values (p_region)
            || ')';
      END IF;

      IF p_district IS NOT NULL
      THEN
         l_condition_str :=
               l_condition_str
            || 'and DISTRICT in ('
            || xxeis.eis_rs_utility.get_param_values (p_district)
            || ')';
      END IF;

      IF p_location IS NOT NULL
      THEN
         l_condition_str :=
               l_condition_str
            || 'and ORG in ('
            || xxeis.eis_rs_utility.get_param_values (p_location)
            || ')';
      /*  l_location_exists:='And exists (select 1 from apps.XXWC_PARAM_LIST
                                  where list_name=''ORG''
                                   and list_values like '''||'%'||p_location||'%'||'''
                                   )';
    ELSE
     l_location_exists:='and 1=1';*/
      END IF;

      IF p_vendor IS NOT NULL
      THEN
         l_condition_str :=
               l_condition_str
            || 'and VENDOR_NUM in ('
            || xxeis.eis_rs_utility.get_param_values (p_vendor)
            || ')';
      /* l_vendor_exists :='And exists (select 1 from apps.XXWC_PARAM_LIST
                                    where list_name=''VENDOR''
                                     and list_values like '''||'%'||p_vendor||'%'||'''
                                     )';
        FND_FILE.PUT_LINE(FND_FILE.LOG,'l_vendor_exists is '||L_VENDOR_EXISTS);
        ELSE
        l_vendor_exists:='and 1=1';*/
      END IF;

      IF p_item IS NOT NULL
      THEN
         -- Commented for Rev 3.0 Begin <<
         /*l_condition_str :=
               l_condition_str
            || 'and msi.concatenated_segments in ('
            || xxeis.eis_rs_utility.get_param_values (p_item)
            || ')';*/
         -- Commented for Rev 3.0 Begin End >>
         -- Added for Rev 3.0 Begin <<
         l_condition_str :=
               l_condition_str
            || 'and item_number in ('
            || xxeis.eis_rs_utility.get_param_values (p_item)
            || ')';
      -- Added for Rev 3.0 End >>
      /* L_ITEM_EXISTS:='And exists (select 1 from apps.XXWC_PARAM_LIST
                                    where list_name=''ITEM''
                                     and list_values like '''||'%'||p_item||'%'||'''
                                     )';
       ELSE
        L_ITEM_EXISTS:='and 1=1';*/

      END IF;

      IF p_cat_class IS NOT NULL
      THEN
         l_condition_str :=
               l_condition_str
            || 'and CAT in ('
            || xxeis.eis_rs_utility.get_param_values (p_cat_class)
            || ')';
      END IF;

      /*  If P_Dc_Mode Is Not Null
        l_condition_str:= l_condition_str:=||'and ORG_NAME ='P_Location;
        Then
        End If;*/
      IF (p_intangibles = 'Exclude')
      THEN
         l_condition_str :=
               l_condition_str
            || 'and nvl(ITEM_STATUS_CODE,''Active'') <> ''Intangible''';
      END IF;

      IF (p_tool_repair = 'Tool Repair Only' OR p_tool_repair LIKE 'Tool%')
      THEN
         l_condition_str := l_condition_str || 'and CAT = ''TH01''';
      END IF;

      IF p_tool_repair = 'Exclude'
      THEN
         l_condition_str := l_condition_str || 'and CAT <> ''TH01''';
      END IF;

      IF p_report_condition = 'All items'
      THEN
         l_condition_str := l_condition_str || 'and 1=1 ';
      END IF;

      IF p_report_condition = 'All items on hand'
      THEN
         l_condition_str := l_condition_str || 'and QOH > 0 ';
      END IF;

      IF (   p_report_condition = 'Non-stock on hand'
          OR p_report_condition LIKE 'Non-%')
      THEN
         l_condition_str :=
            l_condition_str || 'and STK_FLAG  =''N''' || ' and QOH > 0 ';
      END IF;

      IF (   p_report_condition = 'Non stock only'
          OR (    p_report_condition LIKE 'Non%'
              AND p_report_condition NOT LIKE 'Non-%'))
      THEN
         l_condition_str := l_condition_str || 'and STK_FLAG =''N'' ';
      END IF;

      IF p_report_condition = 'Active large'
      THEN
         l_condition_str :=
               l_condition_str
            || 'and STK_FLAG in (''N'',''Y'') '
            || 'and (QOH > 0 or twelve_sales > 0 or on_ord > 0 or Available > 0 or Available < 0)';
      END IF;

      IF p_report_condition = 'Active small'
      THEN
         l_condition_str :=
               l_condition_str
            || 'AND (( STK_FLAG =''Y'' OR (STK_FLAG =''N'' and (QOH > 0 or on_ord > 0 or open_req > 0 or  int_req > 0 or demand > 0 or dir_req > 0 or Available > 0 )))'
            || 'AND ((NOT EXISTS (select 1 from dual where REGEXP_LIKE(substr(item_number,1,1),''[a-zA-Z]''))) or item_number like ''SP%''))';
      ---l_condition_str:= l_condition_str||'and STK_FLAG in (''N'',''Y'') '||' and (QOH > 0 or on_ord > 0 or Available > 0 or Available < 0)';
      END IF;

      IF p_report_condition = 'Stock items only'
      THEN
         l_condition_str := l_condition_str || 'and STK_FLAG =''Y''';
      END IF;

      IF p_report_condition = 'Stock items with 0/0 min/max'
      THEN
         l_condition_str :=
               l_condition_str
            || 'and STK_FLAG =''Y'''
            || 'and MINN=0 and MAXN=0';
      END IF;

      IF (   p_time_sensitive = 'Time Sensitive Only'
          OR p_time_sensitive LIKE 'Time%')
      THEN
         l_condition_str := l_condition_str || 'and TS > 0 and TS < 4000';
      END IF;

      /* Removing and STK_FLAG =''Y'''|| ' because this no longer filters for stock parts- 20130904-01108 */
      IF p_stk_items_with_hit4 IS NOT NULL
      THEN
         l_condition_str :=
            l_condition_str || 'and hit4_sales >= ' || p_stk_items_with_hit4;
      END IF;

      IF p_org_list IS NOT NULL
      THEN
         --insert into xxeis.log values ('Start P_ORG_LIST'); commit;
         xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list (p_process_id,
                                                          p_org_list,
                                                          'Org');
         l_org_exists :=
               'AND EXISTS (SELECT 1
                                        FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x
                                       WHERE x.LIST_NAME  = '''
            || REPLACE (p_org_list, '''', '')
            || '''
                                         AND x.LIST_TYPE     =''Org''
                                         AND x.process_id    = '
            || p_process_id
            || '
                                         AND TAB.ORG = X.list_value) ';
      ELSE
         l_org_exists := 'and 1=1 ';
      END IF;

      IF p_supplier_list IS NOT NULL
      THEN
         xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list (p_process_id,
                                                          p_supplier_list,
                                                          'Supplier');
         l_supplier_exists :=
               'AND EXISTS (SELECT 1
                                        FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x
                                       WHERE x.LIST_NAME    ='''
            || REPLACE (p_supplier_list, '''', '')
            || '''
                                         AND x.LIST_TYPE    =''Supplier''
                                         AND x.process_id   = '
            || p_process_id
            || '
                                         AND TAB.vendor_num = X.list_value) ';
      ELSE
         l_supplier_exists := 'and 1=1 ';
      END IF;

      IF p_item_list IS NOT NULL
      THEN
         xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list (p_process_id,
                                                          p_item_list,
                                                          'Item');
         l_item_exists :=
               'AND EXISTS (SELECT 1
                                        FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x
                                       WHERE x.LIST_NAME    ='''
            || REPLACE (p_item_list, '''', '')
            || '''
                                         AND x.LIST_TYPE    =''Item''
                                         AND x.process_id   = '
            || p_process_id
            || '
                                         AND TAB.item_number = X.list_value ) ';
      ELSE
         l_item_exists := 'and 1=1 ';
      END IF;

      IF p_cat_class_list IS NOT NULL
      THEN
         xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list (p_process_id,
                                                          p_cat_class_list,
                                                          'Cat Class');
         l_item_exists :=
               'AND EXISTS (SELECT 1
                                        FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x
                                       WHERE x.LIST_NAME    ='''
            || REPLACE (p_cat_class_list, '''', '')
            || '''
                                         AND x.LIST_TYPE    =''Cat Class''
                                         AND x.process_id   = '
            || p_process_id
            || '
                                         AND TAB.cat        = X.list_value) ';
      ELSE
         l_catclass_exists := 'and 1=1 ';
      END IF;

      IF p_source_list IS NOT NULL
      THEN
         xxeis.eis_rs_xxwc_com_util_pkg.parse_param_list (p_process_id,
                                                          p_source_list,
                                                          'Source');
         l_item_exists :=
               'AND EXISTS (SELECT 1
                                        FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x
                                       WHERE x.LIST_NAME    ='''
            || REPLACE (p_source_list, '''', '')
            || '''
                                         AND x.LIST_TYPE    =''Source''
                                         AND x.process_id   = '
            || p_process_id
            || '
                                         AND TAB.source     = X.list_value) ';
      ELSE
         l_source_exists := 'and 1=1 ';
      END IF;

      IF p_report_criteria IS NOT NULL AND p_report_criteria_val IS NOT NULL
      THEN
         IF (   p_report_criteria = 'Vendor Number(%)'
             OR p_report_criteria LIKE 'Vendor%')
         THEN
            l_condition_str :=
                  l_condition_str
               || 'and upper(VENDOR_NUM) like '''
               || UPPER (p_report_criteria_val)
               || '''';
            fnd_file.put_line (fnd_file.LOG, l_condition_str);
         END IF;

         IF (   p_report_criteria = '3 Digit Prefix'
             OR p_report_criteria LIKE '3%')
         THEN
            l_condition_str :=
                  l_condition_str
               || 'and upper(PRE) = '''
               || UPPER (p_report_criteria_val)
               || '''';
         END IF;

         IF (   p_report_criteria = 'Item number'
             OR p_report_criteria LIKE 'Item%')
         THEN
            l_condition_str :=
                  l_condition_str
               || 'and upper(ITEM_NUMBER) = '''
               || UPPER (p_report_criteria_val)
               || '''';
         END IF;

         IF     p_report_criteria = 'Source'
            AND p_report_criteria_val IS NOT NULL
         THEN
            l_condition_str :=
                  l_condition_str
               || 'and upper(SOURCE) = '''
               || UPPER (p_report_criteria_val)
               || '''';
            NULL;
         END IF;

         IF p_report_criteria = 'External source vendor'
         THEN
            -- L_Condition_Str:= L_Condition_Str||'and ITEM_NUMBER = '''||P_Report_Criteria_Val||'';
            NULL;
         END IF;

         IF (p_report_criteria = '2 Digit Cat' OR p_report_criteria LIKE '2%')
         THEN
            l_condition_str :=
                  l_condition_str
               || 'and upper(inv_cat_seg1) = '''
               || UPPER (p_report_criteria_val)
               || '''';
            NULL;
         END IF;

         IF (   p_report_criteria = '4 Digit Cat Class'
             OR p_report_criteria LIKE '4%')
         THEN
            l_condition_str :=
                  l_condition_str
               || 'and upper(CAT) = '''
               || UPPER (p_report_criteria_val)
               || '''';
         END IF;

         IF (   p_report_criteria = 'Default Buyer(%)'
             OR p_report_criteria LIKE 'Default%')
         THEN
            l_condition_str :=
                  l_condition_str
               || 'and upper(Buyer) like '''
               || UPPER (p_report_criteria_val)
               || '''';                                 --commented by santosh
         -- l_condition_str:= l_condition_str||'and upper(Buyer) like '''||upper(replace(p_report_criteria_val,''''))||''''; --commented by santosh
         --             L_CONDITION_STR:= L_CONDITION_STR||'and upper(Buyer) in ('''||UPPER(P_REPORT_CRITERIA_VAL)||''')'; -- added by santosh
         --       L_CONDITION_STR:= L_CONDITION_STR||'and upper(Buyer) in ('''||UPPER(replace(replace(P_REPORT_CRITERIA_VAL,'''',''),',',', '))||''')'; -- added by santosh

         --l_condition_str:= l_condition_str||'and upper(Buyer) like '''||upper(XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_report_criteria_val))||'''';
         END IF;
      END IF;

      IF p_start_bin_loc IS NOT NULL
      THEN
         l_condition_str :=
               l_condition_str
            || 'and upper(Bin_Loc) >= '''
            || UPPER (p_start_bin_loc)
            || '''';
      END IF;

      IF p_end_bin_loc IS NOT NULL
      THEN
         l_condition_str :=
               l_condition_str
            || 'and upper(Bin_Loc) <= '''
            || UPPER (p_end_bin_loc)
            || '''';
      END IF;

      /*   IF p_all_items ='Yes'   THEN
         l_condition_str:= 'and 1=1';
         END IF;
  */

      l_query :=
            'SELECT tab.*
             from
             XXWC_ISR_DETAILS_V tab
             where 1=1 '
         || l_condition_str
         || l_org_exists
         || l_supplier_exists
         || l_item_exists
         || l_catclass_exists
         || l_source_exists;
      lv_program_location := 'The main query is ' || l_query;
      apps.xxwc_common_tunning_helpers.write_log (lv_program_location);
      l_insert_recd_cnt := 1;

      OPEN l_ref_cursor FOR l_query;

      LOOP
         FETCH l_ref_cursor INTO g_view_record;

         EXIT WHEN l_ref_cursor%NOTFOUND;
         g_view_tab (l_insert_recd_cnt).org := g_view_record.org;
         g_view_tab (l_insert_recd_cnt).pre := g_view_record.pre;
         g_view_tab (l_insert_recd_cnt).item_number :=
            g_view_record.item_number;
         g_view_tab (l_insert_recd_cnt).vendor_num := g_view_record.vendor_num;
         g_view_tab (l_insert_recd_cnt).vendor_name :=
            g_view_record.vendor_name;
         g_view_tab (l_insert_recd_cnt).source := g_view_record.source;
         g_view_tab (l_insert_recd_cnt).st := g_view_record.st;
         g_view_tab (l_insert_recd_cnt).description :=
            g_view_record.description;
         g_view_tab (l_insert_recd_cnt).cat := g_view_record.cat;
         g_view_tab (l_insert_recd_cnt).pplt := g_view_record.pplt;
         g_view_tab (l_insert_recd_cnt).plt := g_view_record.plt;
         g_view_tab (l_insert_recd_cnt).uom := g_view_record.uom;
         g_view_tab (l_insert_recd_cnt).cl := g_view_record.cl;
         g_view_tab (l_insert_recd_cnt).stk_flag := g_view_record.stk_flag;
         g_view_tab (l_insert_recd_cnt).pm := g_view_record.pm;
         g_view_tab (l_insert_recd_cnt).minn := g_view_record.minn;
         g_view_tab (l_insert_recd_cnt).maxn := g_view_record.maxn;
         g_view_tab (l_insert_recd_cnt).amu := g_view_record.amu;
         g_view_tab (l_insert_recd_cnt).mf_flag := g_view_record.mf_flag;
         g_view_tab (l_insert_recd_cnt).hit6_sales := g_view_record.hit6_sales;
         g_view_tab (l_insert_recd_cnt).aver_cost := g_view_record.aver_cost;
--		 g_view_tab (l_insert_recd_cnt).item_cost := g_view_record.item_cost;  -- Commented for Ver 5.0 
         g_view_tab (l_insert_recd_cnt).list_price := g_view_record.list_price; -- Added for Ver 5.0 
         g_view_tab (l_insert_recd_cnt).bpa := g_view_record.bpa;
         g_view_tab (l_insert_recd_cnt).bpa_cost := g_view_record.bpa_cost;
         g_view_tab (l_insert_recd_cnt).qoh := g_view_record.qoh;
         g_view_tab (l_insert_recd_cnt).available := g_view_record.available;
         g_view_tab (l_insert_recd_cnt).availabledollar :=
            g_view_record.availabledollar;
         g_view_tab (l_insert_recd_cnt).jan_sales := g_view_record.jan_sales;
         g_view_tab (l_insert_recd_cnt).feb_sales := g_view_record.feb_sales;
         g_view_tab (l_insert_recd_cnt).mar_sales := g_view_record.mar_sales;
         g_view_tab (l_insert_recd_cnt).apr_sales := g_view_record.apr_sales;
         g_view_tab (l_insert_recd_cnt).may_sales := g_view_record.may_sales;
         g_view_tab (l_insert_recd_cnt).june_sales := g_view_record.june_sales;
         g_view_tab (l_insert_recd_cnt).jul_sales := g_view_record.jul_sales;
         g_view_tab (l_insert_recd_cnt).aug_sales := g_view_record.aug_sales;
         g_view_tab (l_insert_recd_cnt).sep_sales := g_view_record.sep_sales;
         g_view_tab (l_insert_recd_cnt).oct_sales := g_view_record.oct_sales;
         g_view_tab (l_insert_recd_cnt).nov_sales := g_view_record.nov_sales;
         g_view_tab (l_insert_recd_cnt).dec_sales := g_view_record.dec_sales;
         g_view_tab (l_insert_recd_cnt).hit4_sales := g_view_record.hit4_sales;
         g_view_tab (l_insert_recd_cnt).one_sales := g_view_record.one_sales;
         g_view_tab (l_insert_recd_cnt).six_sales := g_view_record.six_sales;
         g_view_tab (l_insert_recd_cnt).twelve_sales :=
            g_view_record.twelve_sales;
         g_view_tab (l_insert_recd_cnt).bin_loc := g_view_record.bin_loc;
         g_view_tab (l_insert_recd_cnt).mc := g_view_record.mc;
         g_view_tab (l_insert_recd_cnt).fi_flag := g_view_record.fi_flag;
         g_view_tab (l_insert_recd_cnt).freeze_date :=
            g_view_record.freeze_date;
         g_view_tab (l_insert_recd_cnt).res := g_view_record.res;
         g_view_tab (l_insert_recd_cnt).thirteen_wk_avg_inv :=
            g_view_record.thirteen_wk_avg_inv;
         g_view_tab (l_insert_recd_cnt).thirteen_wk_an_cogs :=
            g_view_record.thirteen_wk_an_cogs;
         g_view_tab (l_insert_recd_cnt).turns := g_view_record.turns;
         g_view_tab (l_insert_recd_cnt).buyer := g_view_record.buyer;
         g_view_tab (l_insert_recd_cnt).ts := g_view_record.ts;
         g_view_tab (l_insert_recd_cnt).so := g_view_record.so;
         g_view_tab (l_insert_recd_cnt).sourcing_rule :=
            g_view_record.sourcing_rule;
         g_view_tab (l_insert_recd_cnt).clt := g_view_record.clt;
         g_view_tab (l_insert_recd_cnt).inventory_item_id :=
            g_view_record.inventory_item_id;
         g_view_tab (l_insert_recd_cnt).organization_id :=
            g_view_record.organization_id;
         g_view_tab (l_insert_recd_cnt).set_of_books_id :=
            g_view_record.set_of_books_id;
         g_view_tab (l_insert_recd_cnt).on_ord := g_view_record.on_ord;
         g_view_tab (l_insert_recd_cnt).org_name := g_view_record.org_name;
         g_view_tab (l_insert_recd_cnt).district := g_view_record.district;
         g_view_tab (l_insert_recd_cnt).region := g_view_record.region;
         g_view_tab (l_insert_recd_cnt).wt := g_view_record.wt;
         g_view_tab (l_insert_recd_cnt).ss := g_view_record.ss;
         g_view_tab (l_insert_recd_cnt).fml := g_view_record.fml;
         g_view_tab (l_insert_recd_cnt).open_req := g_view_record.open_req;

         g_view_tab (l_insert_recd_cnt).common_output_id :=
            xxeis.eis_rs_common_outputs_s.NEXTVAL;
         g_view_tab (l_insert_recd_cnt).process_id := p_process_id;
         g_view_tab (l_insert_recd_cnt).avail2 := g_view_record.avail2;
         g_view_tab (l_insert_recd_cnt).int_req := g_view_record.int_req;
         g_view_tab (l_insert_recd_cnt).dir_req := g_view_record.dir_req;
         g_view_tab (l_insert_recd_cnt).demand := g_view_record.demand;

         g_view_tab (l_insert_recd_cnt).site_vendor_num :=
            g_view_record.site_vendor_num;
         g_view_tab (l_insert_recd_cnt).vendor_site :=
            g_view_record.vendor_site;


         g_view_tab (l_insert_recd_cnt).core := g_view_record.core;
         g_view_tab (l_insert_recd_cnt).tier := g_view_record.tier;
         g_view_tab (l_insert_recd_cnt).cat_sba_owner :=
            g_view_record.cat_sba_owner;
         g_view_tab (l_insert_recd_cnt).mfg_part_number :=
            g_view_record.mfg_part_number;
         g_view_tab (l_insert_recd_cnt).mst_vendor := g_view_record.mst_vendor;
         g_view_tab (l_insert_recd_cnt).make_buy := g_view_record.make_buy;
         g_view_tab (l_insert_recd_cnt).org_item_status :=
            g_view_record.org_item_status;
         g_view_tab (l_insert_recd_cnt).org_user_item_type :=
            g_view_record.org_user_item_type;
         g_view_tab (l_insert_recd_cnt).mst_item_status :=
            g_view_record.mst_item_status;
         g_view_tab (l_insert_recd_cnt).mst_user_item_type :=
            g_view_record.mst_user_item_type;
         g_view_tab (l_insert_recd_cnt).last_receipt_date :=
            g_view_record.last_receipt_date;
         g_view_tab (l_insert_recd_cnt).onhand_gt_270 :=
            g_view_record.onhand_gt_270;
         g_view_tab (l_insert_recd_cnt).supersede_item :=
            g_view_record.supersede_item;
         g_view_tab (l_insert_recd_cnt).flip_date := g_view_record.flip_date;
         g_view_tab (l_insert_recd_cnt).operating_unit :=
            g_view_record.operating_unit;

         l_insert_recd_cnt := l_insert_recd_cnt + 1;
      END LOOP;

      CLOSE l_ref_cursor;

      BEGIN
         fnd_file.put_line (fnd_file.LOG,
                            'l_insert_recd_cnt' || l_insert_recd_cnt);
         fnd_file.put_line (
            fnd_file.LOG,
            'first common_output_id' || g_view_tab (1).common_output_id);

         --    Fnd_File.Put_Line(Fnd_File.Log,'last common_output_id' ||G_View_Tab(L_Insert_Recd_Cnt).Common_Output_Id);
         IF l_insert_recd_cnt >= 1
         THEN
            FORALL i IN g_view_tab.FIRST .. g_view_tab.LAST
               INSERT INTO XXEIS.EIS_XXWC_ISR_REPORT_V
                    VALUES g_view_tab (i);
         END IF;

         COMMIT;

         g_view_tab.delete;
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_ISR_DATAMART_PKG.REPORT_ISR',
            p_calling             => g_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'PO');
   END report_isr;
END xxwc_isr_datamart_pkg;
/

GRANT EXECUTE ON APPS.XXWC_ISR_DATAMART_PKG TO XXEIS
/
GRANT EXECUTE ON APPS.XXWC_ISR_DATAMART_PKG TO INTERFACE_PRISM
/
