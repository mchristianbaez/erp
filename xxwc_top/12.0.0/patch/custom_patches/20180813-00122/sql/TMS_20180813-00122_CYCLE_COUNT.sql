/*************************************************************************
 $Header TMS_20180813-00122_CYCLE_COUNT.sql $
 Module Name: TMS_20180813-00122_CYCLE_COUNT.sql

 PURPOSE: Created backup table and deleted the error records from 
          RE: Cycle count process - Datafix

 REVISIONS:
 Ver        Date       Author           Description
 --------- ----------  ---------------  -------------------------
 1.0       08/23/2018  Pattabhi Avula   TMS#20180813-00122 - RE: Cycle count
 **************************************************************************/
  CREATE TABLE xxwc.xxwc_cc_entry_tbl_bkp as
              SELECT * 
			    FROM XXWC.XXWC_CC_ENTRY_TABLE 
			   WHERE organization_id = 335
                 AND MESSAGE <> 'Success';
/
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE

l_count        NUMBER:=0;

BEGIN
DBMS_OUTPUT.put_line('TMS: Datafix script    , Before Update');

DBMS_OUTPUT.put_line('Start Deleting the error records');
 DELETE 
   FROM XXWC.XXWC_CC_ENTRY_TABLE
  WHERE organization_id=335
    AND MESSAGE         <> 'Success';
 
 COMMIT;
l_count:=SQL%ROWCOUNT;
DBMS_OUTPUT.put_line ('Records deleted - ' || l_count);		   
COMMIT;

	  DBMS_OUTPUT.put_line ('TMS: 20180813-00122  , End Update');
EXCEPTION
 WHEN OTHERS THEN
 DBMS_OUTPUT.put_line ('TMS: 20180813-00122 , Errors : ' || SQLERRM);
END;
/