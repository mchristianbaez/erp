/*
TMS# 20170808-00193  - Data fix 
- Vamshi
*/
SET SERVEROUT ON

CREATE TABLE XXWC.XXWC_MTL_CST_DET_TMS_BKP
AS
   SELECT *
     FROM apps.mtl_cst_actual_cost_details
    WHERE transaction_id IN (616056480, 616054468)
/
CREATE TABLE XXWC.XXWC_MTL_MTL_TRANS_TMS_BKP
AS
   SELECT *
     FROM APPS.mtl_material_transactions
    WHERE     1 = 1
          AND transaction_id IN (SELECT transaction_id
                                   FROM APPS.mtl_material_transactions mmt
                                  WHERE     mmt.costed_flag IN ('N', 'E')
                                        AND (EXISTS
                                                (SELECT 1
                                                   FROM APPS.mtl_transaction_accounts mta
                                                  WHERE mmt.transaction_id =
                                                           mta.transaction_id)))
/
CREATE TABLE xxwc.xxwc_mtl_act_cost_sub_TMP_BKP
AS
   SELECT *
     FROM APPS.mtl_actual_cost_subelement
    WHERE transaction_id IN (616056480, 616054468)
/	
DECLARE
   ln_count   NUMBER := 0;
BEGIN
   SELECT COUNT (1) INTO ln_count FROM XXWC.XXWC_MTL_CST_DET_TMS_BKP;

   DBMS_OUTPUT.put_line ('XXWC.XXWC_MTL_CST_DET_TMS_BKP Count ' || ln_count);

   IF NVL (ln_count, 0) > 0
   THEN
      UPDATE mtl_cst_actual_cost_details
         SET transaction_id = -1
       WHERE transaction_id IN (616056480, 616054468);

      DBMS_OUTPUT.put_line (
         'mtl_cst_actual_cost_details Updated count ' || SQL%ROWCOUNT);
   END IF;

   ln_count := 0;

   SELECT COUNT (1) INTO ln_count FROM XXWC.XXWC_MTL_MTL_TRANS_TMS_BKP;

   DBMS_OUTPUT.put_line (
      'XXWC.XXWC_MTL_MTL_TRANS_TMS_BKP Count ' || ln_count);

   IF NVL (ln_count, 0) > 0
   THEN
      UPDATE mtl_material_transactions
         SET costed_flag = NULL, ERROR_CODE = NULL, error_explanation = NULL
       WHERE     1 = 1
             AND transaction_id IN (SELECT transaction_id
                                      FROM mtl_material_transactions mmt
                                     WHERE     mmt.costed_flag IN ('N', 'E')
                                           AND (EXISTS
                                                   (SELECT 1
                                                      FROM mtl_transaction_accounts mta
                                                     WHERE mmt.transaction_id =
                                                              mta.transaction_id)));

      DBMS_OUTPUT.put_line (
         'mtl_material_transactions Updated count ' || SQL%ROWCOUNT);
   END IF;

   ln_count := 0;

   SELECT COUNT (1) INTO ln_count FROM XXWC.xxwc_mtl_act_cost_sub_TMP_BKP;

   DBMS_OUTPUT.put_line (
      'XXWC.xxwc_mtl_act_cost_sub_TMP_BKP Count ' || ln_count);

   IF NVL (ln_count, 0) > 0
   THEN
      UPDATE mtl_actual_cost_subelement
         SET transaction_id = -1
       WHERE transaction_id IN (616056480, 616054468);

      DBMS_OUTPUT.put_line (
         'mtl_actual_cost_subelement Updated count ' || SQL%ROWCOUNT);
   END IF;

   UPDATE mtl_material_transactions MMT
      SET MMT.costed_flag = 'N',
          MMT.ERROR_CODE = NULL,
          MMT.error_explanation = NULL,
          MMT.transaction_group_id = NULL,
          MMT.transaction_set_id = NULL
    WHERE     1 = 1
          AND MMT.costed_flag IN ('N', 'E')
          AND NOT EXISTS
                 (SELECT 1
                    FROM MTL_TRANSACTION_ACCOUNTS MTA
                   WHERE MMT.TRANSACTION_ID = MTA.TRANSACTION_ID);

   DBMS_OUTPUT.put_line (
      'mtl_material_transactions (FINAL) Updated count ' || SQL%ROWCOUNT);


   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Error Occured ' || SUBSTR (SQLERRM, 1, 250));
END;
/			 