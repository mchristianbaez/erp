/*
 TMS: 20151020-00075 
 Date: 06/15/2016
 Notes:  @GSC ESMS 305338, Oracle FIXED ASSETS SR - Assets Not Operating as expected. 
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
   l_asset_hdr_rec   FA_API_TYPES.asset_hdr_rec_type;

   l_return_status   VARCHAR2 (1);
   l_mesg_count      NUMBER := 0;
   l_mesg_len        NUMBER;
   l_mesg            VARCHAR2 (4000);
   --
   CURSOR REMOVE_ASSETS IS -- HARD CODED ASSET ID'S ONLY
   SELECT 473502 ASSET_ID FROM DUAL
   UNION ALL
   SELECT 473472 FROM DUAL
   UNION ALL
   SELECT 473499 FROM DUAL; 
   --
BEGIN
   fnd_global.apps_initialize( user_id =>40816, resp_id =>50728, resp_appl_id =>140);
   DBMS_OUTPUT.enable (1000000);   
FOR REC IN REMOVE_ASSETS LOOP
   FA_SRVR_MSG.Init_Server_Message;

   -- asset header info
   l_asset_hdr_rec.asset_id := REC.ASSET_ID;
   l_asset_hdr_rec.book_type_code := 'HDS WC FEDERAL';
   --
   FA_DELETION_PUB.do_delete (
      p_api_version        => 1.0,
      p_init_msg_list      => FND_API.G_FALSE,
      p_commit             => FND_API.G_FALSE,
      p_validation_level   => FND_API.G_VALID_LEVEL_FULL,
      x_return_status      => l_return_status,
      x_msg_count          => l_mesg_count,
      x_msg_data           => l_mesg,
      p_calling_fn         => NULL,
      px_asset_hdr_rec     => l_asset_hdr_rec  
      );
     --
           l_mesg_count := fnd_msg_pub.count_msg;

           IF l_mesg_count > 0
           THEN
              l_mesg :=
                    CHR (10)
                 || SUBSTR (fnd_msg_pub.get (fnd_msg_pub.G_FIRST, fnd_api.G_FALSE),
                            1,
                            250);
              DBMS_OUTPUT.put_line (l_mesg);
              FOR i IN 1 .. (l_mesg_count - 1)
                  LOOP
                     l_mesg :=
                        SUBSTR (fnd_msg_pub.get (fnd_msg_pub.G_NEXT, fnd_api.G_FALSE),
                                1,
                                250);

                     DBMS_OUTPUT.put_line (l_mesg);
                  END LOOP;
              fnd_msg_pub.delete_msg ();
           END IF;


       IF (l_return_status <> FND_API.G_RET_STS_SUCCESS)
       THEN
          DBMS_OUTPUT.put_line ('FAILURE');
       ELSE
          DBMS_OUTPUT.put_line ('SUCCESS');
          DBMS_OUTPUT.put_line ('ASSET_ID' || TO_CHAR (l_asset_hdr_rec.asset_id));
          DBMS_OUTPUT.put_line ('BOOK: ' || l_asset_hdr_rec.book_type_code);
       END IF;
 END LOOP;
 --   
 COMMIT;
 --
EXCEPTION
 WHEN OTHERS THEN
   ROLLBACK;
   DBMS_OUTPUT.PUT_LINE('OUTER BLOCK, ERROR MESSAGE : '||SQLERRM);   
END;
/
