CREATE OR REPLACE PACKAGE BODY apps.xxwc_po_vendor_min_pkg
AS
    /************************************************************************************************************
      $Header xxwc_po_vendor_min_pkg $
      Module Name: xxwc_po_vendor_min_pkg.pks

      PURPOSE:   This package is used by the Vendor Minimum Form

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        10/30/2012  Consuelo Gonzalez      Initial Version
      1.2        02/06/2014 Rasikha Galimova        changed xxwc_populate_tmp, added populate_item_table
      1.3        11/06/2014 Maharajan Shunmugam     TMS#20141002-00050 Canada Multi org changes
	  1.4        01/11/2018	P.Vamshidhar            TMS#20180110-00287 - Oracle Vendor Meet Min Form slowness
    ***********************************************************************************************************/

    FUNCTION get_req_qty (supply_cutoff_date    DATE
                         ,org_id                NUMBER
                         ,current_item_id       NUMBER
                         ,include_po            NUMBER
                         ,include_nonnet        NUMBER
                         ,include_wip           NUMBER
                         ,include_if            NUMBER
                         ,subinv                CHAR)
        RETURN NUMBER
    IS
    /************************************************************************************************************
      Function: get_req_qty

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        10/30/2012  Consuelo Gonzalez      Initial Version
	  1.4        01/11/2018	P.Vamshidhar            TMS#20180110-00287 - Oracle Vendor Meet Min Form slowness
     ************************************************************************************************************/
	
        qty                 NUMBER;
        total               NUMBER;
        l_vmi_enabled       VARCHAR2 (1);
        l_stmt              CLOB;
        l_vmi_stmt          CLOB;
        scd                 VARCHAR2 (20);
        org_id1             NUMBER;

        p_char_qty          VARCHAR2 (200);

        /* nsinghi MIN-MAX INVCONV start */
        c_process_enabled   VARCHAR2 (5);
        v_errbuf            CLOB;
    /* nsinghi MIN-MAX INVCONV end */

    BEGIN
        -- The supply includes approved requisitions, approved purchase
        -- orders, intransit inventory, and wip jobs.
        -- PO Supply includes all the supplies from other organizations to the
        -- organization with no subinventory specified or to the nettable
        -- subinventories.  It also includes all the supplies from the same
        -- organization with nonnettable subinventories to the nettable subinventories.

        -- This function contains code to plan at the subinventory level but it has not
        -- been implemented because forecast demand cannot be easily determined at the
        -- subinventory level.  The reorder point report passes a null value in the subinv
        -- field so the supply is selected at the org level.

        total := 0;
        qty := 0;
        org_id1 := org_id;

        -- Find PO Supply

        /* Removing the subinventory conditions from the stmt until we implement subinventory logic later on */
        /*
         AND    (nvl(sup.to_subinventory,1) =
              DECODE(subinv,NULL,nvl(sup.to_subinventory,1),subinv)
              OR     (EXISTS (SELECT 'x'
                 FROM  mtl_secondary_inventories sub2
                 WHERE  sub2.secondary_inventory_name =
              DECODE(subinv,NULL,sup.to_subinventory,subinv)
                 AND    sub2.organization_id = sup.to_organization_id
                 AND    sub2.availability_type = decode(include_nonnet,1,sub2.availability_type,1))))
        */

        l_vmi_enabled := NVL (fnd_profile.VALUE ('PO_VMI_ENABLED'), 'N');
        scd := TO_CHAR (NVL (supply_cutoff_date, SYSDATE + 91), 'DD-MON-RRRR');
        -- DBMS_OUTPUT.put_line ('supply_cutoff_date=' || supply_cutoff_date);
        -- DBMS_OUTPUT.put_line ('scd=' || scd);
        -- INTO :parameter.char_qty
/* -- Commented in Rev 1.4 by Vamshi.
        l_stmt :=
               'SELECT to_char(nvl(sum(to_org_primary_quantity), 0))
      FROM   mtl_supply sup, mtl_system_items items
      WHERE  sup.supply_type_code = ''REQ''
      AND NOT EXISTS
               (SELECT 1
                  FROM po_requisition_headers
                 WHERE requisition_header_id = sup.req_header_id AND type_lookup_code = ''INTERNAL'')
      AND    sup.destination_type_code =''INVENTORY''
      AND    sup.to_organization_id ='
            || TO_CHAR (org_id)
            || ' AND    sup.item_id ='
            || TO_CHAR (current_item_id)
            || ' AND items.organization_id = sup.to_organization_id'
            || ' AND items.inventory_item_id = sup.item_id'
            || ' AND    TRUNC(DECODE(NVL(items.postprocessing_lead_time,0),0,MRP_CALENDAR.NEXT_WORK_DAY(items.organization_id,1,sup.need_by_date),'
            || ' MRP_CALENDAR.DATE_OFFSET(items.organization_id,1,sup.need_by_date,items.postprocessing_lead_time))) <=TO_DATE('''
            || scd
            || ''',''DD-MON-RRRR'')'
            || ' AND    (NVL(sup.FROM_organization_id,-1) !='
            || TO_CHAR (org_id)
            || ' OR     (sup.FROM_organization_id ='
            || TO_CHAR (org_id)
            || ' AND '
            || TO_CHAR (include_nonnet)
            || '= 2'
            || ' AND    EXISTS (SELECT ''x''
          FROM   mtl_secondary_inventories sub1
          WHERE  sub1.organization_id = sup.FROM_organization_id
           AND    sup.FROM_subinventory = sub1.secondary_inventory_name
           AND    sub1.availability_type != 1)))'
            || ' AND NOT EXISTS (select ''y''
              from oe_drop_ship_sources  odss
             where sup.po_header_id is null and sup.req_line_id = odss.requisition_line_id ) '
            || '  AND NOT EXISTS (select ''y''
            from  oe_drop_ship_sources odss
            where  sup.req_line_id is null and  sup.po_line_location_id = odss.line_location_id
*/
         -- Added below code in Rev1.4 Vamshi
        l_stmt :=
               'SELECT to_char(nvl(sum(to_org_primary_quantity), 0))
      FROM   mtl_supply sup, mtl_system_items items
      WHERE  sup.supply_type_code = ''REQ''
      AND NOT EXISTS
               (SELECT 1
                  FROM po_requisition_headers
                 WHERE requisition_header_id = sup.req_header_id AND type_lookup_code = ''INTERNAL'')
      AND    sup.destination_type_code =''INVENTORY''
      AND    sup.to_organization_id ='
            || ' TO_CHAR (:1)'
            || ' AND    sup.item_id ='
            || ' TO_CHAR (:2)'
            || ' AND items.organization_id = sup.to_organization_id'
            || ' AND items.inventory_item_id = sup.item_id'
            || ' AND    TRUNC(DECODE(NVL(items.postprocessing_lead_time,0),0,MRP_CALENDAR.NEXT_WORK_DAY(items.organization_id,1,sup.need_by_date),'
            || ' MRP_CALENDAR.DATE_OFFSET(items.organization_id,1,sup.need_by_date,items.postprocessing_lead_time))) <=TO_DATE('''
            || scd
            || ''',''DD-MON-RRRR'')'
            || ' AND    (NVL(sup.FROM_organization_id,-1) !='
            || ' TO_CHAR (:3) '
            || ' OR     (sup.FROM_organization_id ='
            || ' TO_CHAR (:4) '
            || ' AND '
            || ' TO_CHAR (:5) '
            || '= 2'
            || ' AND    EXISTS (SELECT ''x''
          FROM   mtl_secondary_inventories sub1
          WHERE  sub1.organization_id = sup.FROM_organization_id
           AND    sup.FROM_subinventory = sub1.secondary_inventory_name
           AND    sub1.availability_type != 1)))'
            || ' AND NOT EXISTS (select ''y''
              from oe_drop_ship_sources  odss
             where sup.po_header_id is null and sup.req_line_id = odss.requisition_line_id ) '
            || '  AND NOT EXISTS (select ''y''
            from  oe_drop_ship_sources odss
            where  sup.req_line_id is null and  sup.po_line_location_id = odss.line_location_id)';



			
        l_vmi_stmt := ' AND    (sup.po_line_location_id is NULL
                       OR EXISTS (SELECT ''x''
                                  FROM po_line_locations lilo
                                  WHERE lilo.line_location_id = sup.po_line_location_id
                                  AND NVL(lilo.vmi_flag,''N'') =''N''
                                 )
                  )
           AND    (sup.req_line_id IS NULL
                   OR EXISTS (SELECT ''x''
                              FROM po_requisition_lines prl
                              WHERE prl.requisition_line_id = sup.req_line_id
                              AND NVL(prl.vmi_flag,''N'') =''N''
                                ))';

        IF (include_po = 1)
        THEN
            IF l_vmi_enabled = 'Y'
            THEN
                l_stmt := l_stmt || l_vmi_stmt;
            END IF;

            -- srw.do_sql(l_stmt);

            p_char_qty := NULL;

            -- DBMS_OUTPUT.put_line (l_stmt);

            BEGIN
                --EXECUTE IMMEDIATE l_stmt INTO p_char_qty;
				  EXECUTE IMMEDIATE l_stmt into p_char_qty using org_id,current_item_id,org_id,org_id,include_nonnet;
            EXCEPTION
                WHEN OTHERS
                THEN
                    p_char_qty := NULL;
            END;

            qty := TO_NUMBER (p_char_qty);

            total := total + NVL (qty, 0);
        END IF;

        -- Find WIP Supply

        IF (include_wip = 1)
        THEN
            /* nsinghi MIN-MAX INVCONV start */

            /* Here we need to have query to select OPM batch data  */

            SELECT NVL (process_enabled_flag, 'N')
              INTO c_process_enabled
              FROM mtl_parameters
             WHERE organization_id = org_id;

            IF c_process_enabled = 'Y'
            THEN
                SELECT SUM (
                             NVL ( (NVL (d.wip_plan_qty, d.plan_qty) - d.actual_qty), 0)
                           * (original_primary_qty / original_qty))
                  INTO qty
                  FROM gme_material_details d, gme_batch_header h
                 WHERE     h.batch_type IN (0, 10)
                       AND h.batch_status IN (1, 2)
                       AND h.batch_id = d.batch_id
                       AND d.inventory_item_id = current_item_id
                       AND d.organization_id = org_id
                       AND d.material_requirement_date <= supply_cutoff_date
                       AND d.line_type > 0;

                total := total + NVL (qty, 0);
            ELSE
                /* nsinghi MIN-MAX INVCONV end */

                -- WIP discrete job
                SELECT SUM (NVL (start_quantity, 0) - NVL (quantity_completed, 0) - NVL (quantity_scrapped, 0))
                  INTO qty
                  FROM wip_discrete_jobs
                 WHERE     organization_id = org_id
                       AND primary_item_id = current_item_id
                       AND status_type IN (1, 3, 4, 6)
                       AND job_type IN (1, 3)
                       AND scheduled_completion_date <= TO_DATE (TO_CHAR (supply_cutoff_date), 'DD-MON-RR')
                       AND NVL (completion_subinventory, 1) =
                               DECODE (subinv, NULL, NVL (completion_subinventory, 1), subinv);

                total := total + NVL (qty, 0);

                -- WIP REPETITIVE JOB
                SELECT SUM (
                               daily_production_rate
                             * LEAST (0
                                     ,GREATEST (processing_work_days, supply_cutoff_date - first_unit_completion_date))
                           - quantity_completed)
                  INTO qty
                  FROM wip_repetitive_schedules wrs, wip_repetitive_items wri
                 WHERE     wrs.organization_id = org_id
                       AND wrs.status_type IN (1, 3, 4, 6)
                       AND wri.organization_id = org_id
                       AND wri.primary_item_id = current_item_id
                       AND wri.wip_entity_id = wrs.wip_entity_id
                       AND wri.line_id = wrs.line_id
                       AND NVL (wri.completion_subinventory, 1) =
                               DECODE (subinv, NULL, NVL (wri.completion_subinventory, 1), subinv);

                total := total + NVL (qty, 0);
            /* nsinghi MIN-MAX INVCONV start */
            END IF;                                                                            /* p_process_org = 'Y' */
        /* nsinghi MIN-MAX INVCONV end */

        END IF;

        -- Find interface supply
        IF (include_if = 1)
        THEN
            -- po_requisitions_interface_all
            SELECT SUM (quantity)
              INTO qty
              FROM po_requisitions_interface
             WHERE     item_id = current_item_id
                   AND destination_organization_id = org_id1
                   AND include_po = 1
                   AND (process_flag != 'ERROR' OR process_flag IS NULL)
                   AND need_by_date <= supply_cutoff_date
                   AND (   NVL (destination_subinventory, 1) =
                               DECODE (subinv, NULL, NVL (destination_subinventory, 1), subinv)
                        OR EXISTS
                               (SELECT 1
                                  FROM mtl_secondary_inventories sub2
                                 WHERE     secondary_inventory_name = destination_subinventory
                                       AND destination_subinventory = NVL (subinv, destination_subinventory)
                                       AND sub2.organization_id = org_id1
                                       AND sub2.availability_type =
                                               DECODE (include_nonnet, 1, sub2.availability_type, 1)));

            total := ROUND (total + NVL (qty, 0), 2);

            /* nsinghi MIN-MAX INVCONV start */
            IF c_process_enabled = 'N'
            THEN
                /* nsinghi MIN-MAX INVCONV end */

                SELECT SUM (start_quantity)
                  INTO qty
                  FROM wip_job_schedule_interface
                 WHERE     primary_item_id = current_item_id
                       AND organization_id = org_id
                       AND include_wip = 1
                       AND process_status != 3
                       AND last_unit_completion_date <= supply_cutoff_date;

                total := ROUND (total + NVL (qty, 0), 2);
            /* nsinghi MIN-MAX INVCONV start */
            END IF;                                                                            /* p_process_org = 'Y' */
        /* nsinghi MIN-MAX INVCONV end */

        END IF;

        RETURN (total);
    EXCEPTION
        WHEN OTHERS
        THEN
            v_errbuf :=
                   'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            RETURN 0;
    END get_req_qty;

    FUNCTION get_po_qty (supply_cutoff_date    DATE
                        ,org_id                NUMBER
                        ,current_item_id       NUMBER
                        ,include_po            NUMBER
                        ,include_nonnet        NUMBER
                        ,include_wip           NUMBER
                        ,include_if            NUMBER
                        ,subinv                CHAR)
        RETURN NUMBER
    IS
    /************************************************************************************************************
      Function: get_po_qty

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        10/30/2012  Consuelo Gonzalez      Initial Version
	  1.4        01/11/2018	P.Vamshidhar            TMS#20180110-00287 - Oracle Vendor Meet Min Form slowness
     ************************************************************************************************************/

     	qty                 NUMBER;
        total               NUMBER;
        l_vmi_enabled       VARCHAR2 (1);
        l_stmt              CLOB;
        l_vmi_stmt          CLOB;
        scd                 VARCHAR2 (20);
        org_id1             NUMBER;

        p_char_qty          VARCHAR2 (200);

        /* nsinghi MIN-MAX INVCONV start */
        c_process_enabled   VARCHAR2 (5);
        v_errbuf            CLOB;
    /* nsinghi MIN-MAX INVCONV end */
    BEGIN
        -- The supply includes approved requisitions, approved purchase
        -- orders, intransit inventory, and wip jobs.
        -- PO Supply includes all the supplies from other organizations to the
        -- organization with no subinventory specified or to the nettable
        -- subinventories.  It also includes all the supplies from the same
        -- organization with nonnettable subinventories to the nettable subinventories.

        -- This function contains code to plan at the subinventory level but it has not
        -- been implemented because forecast demand cannot be easily determined at the
        -- subinventory level.  The reorder point report passes a null value in the subinv
        -- field so the supply is selected at the org level.

        total := 0;
        qty := 0;
        org_id1 := org_id;

        -- Find PO Supply

        /* Removing the subinventory conditions from the stmt until we implement subinventory logic later on */
        /*
         AND    (nvl(sup.to_subinventory,1) =
              DECODE(subinv,NULL,nvl(sup.to_subinventory,1),subinv)
              OR     (EXISTS (SELECT 'x'
                 FROM  mtl_secondary_inventories sub2
                 WHERE  sub2.secondary_inventory_name =
              DECODE(subinv,NULL,sup.to_subinventory,subinv)
                 AND    sub2.organization_id = sup.to_organization_id
                 AND    sub2.availability_type = decode(include_nonnet,1,sub2.availability_type,1))))
        */

        l_vmi_enabled := NVL (fnd_profile.VALUE ('PO_VMI_ENABLED'), 'N');
        scd := TO_CHAR (NVL (supply_cutoff_date, SYSDATE + 91), 'DD-MON-RRRR');
        -- DBMS_OUTPUT.put_line ('supply_cutoff_date=' || supply_cutoff_date);
        -- DBMS_OUTPUT.put_line ('scd=' || scd);
        -- INTO :char_qty
        -- 12/12/12 CG: Added ASN, SHIPMENT, RECEIVING to the supply type filter
      /*  -- Commented below code in Rev 1.4 by Vamshi
        l_stmt :=
               'SELECT to_char(nvl(sum(to_org_primary_quantity), 0))
      FROM   mtl_supply sup, mtl_system_items items
      WHERE  sup.supply_type_code in (''PO'',''REQ'',''ASN'',''SHIPMENT'',''RECEIVING'')
      AND    sup.destination_type_code =''INVENTORY''
      AND    (SUP.req_header_id IS NULL or  EXISTS( SELECT 1 FROM oe_order_headers oeh
                             WHERE     oeh.source_document_id       =    SUP.req_header_id))
      AND    sup.to_organization_id ='
            || TO_CHAR (org_id)
            || ' AND    sup.item_id ='
            || TO_CHAR (current_item_id)
            || ' AND items.organization_id = sup.to_organization_id'
            || ' AND items.inventory_item_id = sup.item_id'
            || ' AND    TRUNC(DECODE(NVL(items.postprocessing_lead_time,0),0,MRP_CALENDAR.NEXT_WORK_DAY(items.organization_id,1,DECODE(sup.supply_type_code,''PO'',sup.need_by_date,''REQ'',sup.need_by_date,''ASN'',sup.need_by_date,''RECEIVING'',sup.receipt_date,''SHIPMENT'',sup.receipt_date)),'
            || ' MRP_CALENDAR.DATE_OFFSET(items.organization_id,1,DECODE(sup.supply_type_code,''PO'',sup.need_by_date,''REQ'',sup.need_by_date,''ASN'',sup.need_by_date,''RECEIVING'',sup.receipt_date,''SHIPMENT'',sup.receipt_date),items.postprocessing_lead_time))) <=TO_DATE('''
            || scd
            || ''',''DD-MON-RRRR'')'
            || ' AND    (NVL(sup.FROM_organization_id,-1) !='
            || TO_CHAR (org_id)
            || ' OR     (sup.FROM_organization_id ='
            || TO_CHAR (org_id)
            || ' AND '
            || TO_CHAR (include_nonnet)
            || '= 2'
            || ' AND    EXISTS (SELECT ''x''
          FROM   mtl_secondary_inventories sub1
          WHERE  sub1.organization_id = sup.FROM_organization_id
           AND    sup.FROM_subinventory = sub1.secondary_inventory_name
           AND    sub1.availability_type != 1)))'
            || ' AND NOT EXISTS (select ''y''
              from oe_drop_ship_sources  odss
             where sup.po_header_id is null and sup.req_line_id = odss.requisition_line_id ) '
            || '  AND NOT EXISTS (select ''y''
            from  oe_drop_ship_sources odss
            where  sup.req_line_id is null and  sup.po_line_location_id = odss.line_location_id)';
           */
         -- Added below code in Rev1.4 Vamshi
         
        l_stmt :=
               'SELECT to_char(nvl(sum(to_org_primary_quantity), 0))
      FROM   mtl_supply sup, mtl_system_items items
      WHERE  sup.supply_type_code in (''PO'',''REQ'',''ASN'',''SHIPMENT'',''RECEIVING'')
      AND    sup.destination_type_code =''INVENTORY''
      AND    (SUP.req_header_id IS NULL or  EXISTS( SELECT 1 FROM oe_order_headers oeh
                             WHERE     oeh.source_document_id       =    SUP.req_header_id))
      AND    sup.to_organization_id ='
            || ' TO_CHAR (:1) '
            || ' AND    sup.item_id ='
            || ' TO_CHAR (:2) '
            || ' AND items.organization_id = sup.to_organization_id'
            || ' AND items.inventory_item_id = sup.item_id'
            || ' AND    TRUNC(DECODE(NVL(items.postprocessing_lead_time,0),0,MRP_CALENDAR.NEXT_WORK_DAY(items.organization_id,1,DECODE(sup.supply_type_code,''PO'',sup.need_by_date,''REQ'',sup.need_by_date,''ASN'',sup.need_by_date,''RECEIVING'',sup.receipt_date,''SHIPMENT'',sup.receipt_date)),'
            || ' MRP_CALENDAR.DATE_OFFSET(items.organization_id,1,DECODE(sup.supply_type_code,''PO'',sup.need_by_date,''REQ'',sup.need_by_date,''ASN'',sup.need_by_date,''RECEIVING'',sup.receipt_date,''SHIPMENT'',sup.receipt_date),items.postprocessing_lead_time))) <=TO_DATE('''
            || scd
            || ''',''DD-MON-RRRR'')'
            || ' AND    (NVL(sup.FROM_organization_id,-1) !='
            || ' TO_CHAR (:3) '
            || ' OR     (sup.FROM_organization_id ='
            || ' TO_CHAR (:4) '
            || ' AND '
            || ' TO_CHAR (:5) '
            || '= 2'
            || ' AND    EXISTS (SELECT ''x''
          FROM   mtl_secondary_inventories sub1
          WHERE  sub1.organization_id = sup.FROM_organization_id
           AND    sup.FROM_subinventory = sub1.secondary_inventory_name
           AND    sub1.availability_type != 1)))'
            || ' AND NOT EXISTS (select ''y''
              from oe_drop_ship_sources  odss
             where sup.po_header_id is null and sup.req_line_id = odss.requisition_line_id ) '
            || '  AND NOT EXISTS (select ''y''
            from  oe_drop_ship_sources odss
            where  sup.req_line_id is null and  sup.po_line_location_id = odss.line_location_id)';



        l_vmi_stmt := ' AND    (sup.po_line_location_id is NULL
                       OR EXISTS (SELECT ''x''
                                  FROM po_line_locations lilo
                                  WHERE lilo.line_location_id = sup.po_line_location_id
                                  AND NVL(lilo.vmi_flag,''N'') =''N''
                                 )
                  )
           AND    (sup.req_line_id IS NULL
                   OR EXISTS (SELECT ''x''
                              FROM po_requisition_lines prl
                              WHERE prl.requisition_line_id = sup.req_line_id
                              AND NVL(prl.vmi_flag,''N'') =''N''
                              )
                  )';

        IF (include_po = 1)
        THEN
            IF l_vmi_enabled = 'Y'
            THEN
                l_stmt := l_stmt || l_vmi_stmt;
            END IF;

            -- srw.do_sql(l_stmt);

            -- DBMS_OUTPUT.put_line (l_stmt);

            BEGIN
                --EXECUTE IMMEDIATE l_stmt INTO p_char_qty;
				EXECUTE IMMEDIATE l_stmt INTO p_char_qty USING org_id,current_item_id,org_id,org_id,include_nonnet;
            EXCEPTION
                WHEN OTHERS
                THEN
                    p_char_qty := NULL;
            END;

            qty := TO_NUMBER (p_char_qty);

            total := total + NVL (qty, 0);
        END IF;

        -- Find WIP Supply

        IF (include_wip = 1)
        THEN
            /* nsinghi MIN-MAX INVCONV start */

            /* Here we need to have query to select OPM batch data  */

            SELECT NVL (process_enabled_flag, 'N')
              INTO c_process_enabled
              FROM mtl_parameters
             WHERE organization_id = org_id;

            IF c_process_enabled = 'Y'
            THEN
                SELECT SUM (
                             NVL ( (NVL (d.wip_plan_qty, d.plan_qty) - d.actual_qty), 0)
                           * (original_primary_qty / original_qty))
                  INTO qty
                  FROM gme_material_details d, gme_batch_header h
                 WHERE     h.batch_type IN (0, 10)
                       AND h.batch_status IN (1, 2)
                       AND h.batch_id = d.batch_id
                       AND d.inventory_item_id = current_item_id
                       AND d.organization_id = org_id
                       AND d.material_requirement_date <= supply_cutoff_date
                       AND d.line_type > 0;

                total := total + NVL (qty, 0);
            ELSE
                /* nsinghi MIN-MAX INVCONV end */

                -- WIP discrete job
                SELECT SUM (NVL (start_quantity, 0) - NVL (quantity_completed, 0) - NVL (quantity_scrapped, 0))
                  INTO qty
                  FROM wip_discrete_jobs
                 WHERE     organization_id = org_id
                       AND primary_item_id = current_item_id
                       AND status_type IN (1, 3, 4, 6)
                       AND job_type IN (1, 3)
                       AND scheduled_completion_date <= TO_DATE (TO_CHAR (supply_cutoff_date), 'DD-MON-RR')
                       AND NVL (completion_subinventory, 1) =
                               DECODE (subinv, NULL, NVL (completion_subinventory, 1), subinv);

                total := total + NVL (qty, 0);

                -- WIP REPETITIVE JOB
                SELECT SUM (
                               daily_production_rate
                             * LEAST (0
                                     ,GREATEST (processing_work_days, supply_cutoff_date - first_unit_completion_date))
                           - quantity_completed)
                  INTO qty
                  FROM wip_repetitive_schedules wrs, wip_repetitive_items wri
                 WHERE     wrs.organization_id = org_id
                       AND wrs.status_type IN (1, 3, 4, 6)
                       AND wri.organization_id = org_id
                       AND wri.primary_item_id = current_item_id
                       AND wri.wip_entity_id = wrs.wip_entity_id
                       AND wri.line_id = wrs.line_id
                       AND NVL (wri.completion_subinventory, 1) =
                               DECODE (subinv, NULL, NVL (wri.completion_subinventory, 1), subinv);

                total := total + NVL (qty, 0);
            /* nsinghi MIN-MAX INVCONV start */
            END IF;                                                                            /* p_process_org = 'Y' */
        /* nsinghi MIN-MAX INVCONV end */

        END IF;

        -- Find interface supply
        IF (include_if = 1)
        THEN
            -- po_requisitions_interface_all
            SELECT SUM (quantity)
              INTO qty
              FROM po_requisitions_interface
             WHERE     item_id = current_item_id
                   AND destination_organization_id = org_id1
                   AND include_po = 1
                   AND (process_flag != 'ERROR' OR process_flag IS NULL)
                   AND need_by_date <= supply_cutoff_date
                   AND (   NVL (destination_subinventory, 1) =
                               DECODE (subinv, NULL, NVL (destination_subinventory, 1), subinv)
                        OR EXISTS
                               (SELECT 1
                                  FROM mtl_secondary_inventories sub2
                                 WHERE     secondary_inventory_name = destination_subinventory
                                       AND destination_subinventory = NVL (subinv, destination_subinventory)
                                       AND sub2.organization_id = org_id1
                                       AND sub2.availability_type =
                                               DECODE (include_nonnet, 1, sub2.availability_type, 1)));

            total := ROUND (total + NVL (qty, 0), 2);

            /* nsinghi MIN-MAX INVCONV start */
            IF c_process_enabled = 'N'
            THEN
                /* nsinghi MIN-MAX INVCONV end */

                SELECT SUM (start_quantity)
                  INTO qty
                  FROM wip_job_schedule_interface
                 WHERE     primary_item_id = current_item_id
                       AND organization_id = org_id
                       AND include_wip = 1
                       AND process_status != 3
                       AND last_unit_completion_date <= supply_cutoff_date;

                total := ROUND (total + NVL (qty, 0), 2);
            /* nsinghi MIN-MAX INVCONV start */
            END IF;                                                                            /* p_process_org = 'Y' */
        /* nsinghi MIN-MAX INVCONV end */

        END IF;

        RETURN (total);
    EXCEPTION
        WHEN OTHERS
        THEN
            v_errbuf :=
                   'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            -- DBMS_OUTPUT.put_line (v_errbuf);
            RETURN 0;
    END get_po_qty;

    FUNCTION get_onhand_qty (item_id           NUMBER
                            ,lot_control       NUMBER
                            ,org_id            NUMBER
                            ,subinv            CHAR
                            ,include_nonnet    NUMBER)
        RETURN NUMBER
    IS
        l_is_lot_control      VARCHAR2 (20) := 'TRUE';
        x_return_status       VARCHAR2 (30);
        x_msg_count           NUMBER;
        x_msg_data            VARCHAR2 (1000);
        l_onhand_source       NUMBER := 3;
        l_subinventory_code   VARCHAR2 (30);
        l_sysdate             DATE;
        l_cursor_stmt         VARCHAR2 (1000);
        x_qoh                 NUMBER;
        x_rqoh                NUMBER;
        x_qr                  NUMBER;
        x_qs                  NUMBER;
        x_att                 NUMBER;
        x_atr                 NUMBER;
        x_voh                 NUMBER;
        x_vatt                NUMBER;
        l_qoh                 NUMBER;
    BEGIN
        /* Initialize date stuff */
        SELECT SYSDATE INTO l_sysdate FROM sys.DUAL;

        IF (include_nonnet = 1)
        THEN
            l_onhand_source := NULL;
        ELSE
            l_onhand_source := 2;
        END IF;

        -- Created a New Wrapper API MRP_GET_ONHAND to calulate Onhand qty

        mrp_get_onhand.get_oh_qty (item_id           => item_id
                                  ,org_id            => org_id
                                  ,include_nonnet    => include_nonnet
                                  ,x_qoh             => x_qoh
                                  ,x_return_status   => x_return_status
                                  ,x_msg_data        => x_msg_data);

        IF x_return_status = 'S'
        THEN
            RETURN (x_qoh);
        ELSE
            -- srw.message(92,'Error while calculating OnHandQty');
            RETURN (0);
        END IF;
    END get_onhand_qty;

    FUNCTION get_demand (item_id               NUMBER
                        ,org_id                NUMBER
                        ,demand_cutoff_date    DATE
                        ,net_rsv               NUMBER
                        ,include_nonnet        NUMBER
                        ,include_wip           NUMBER
                        ,net_unrsv             NUMBER
                        ,net_wip               NUMBER
                        ,subinv                CHAR)
        RETURN NUMBER
    IS
        qty                 NUMBER;
        total               NUMBER;
        lv_org_id           NUMBER;

        /* nsinghi MIN-MAX INVCONV start */
        c_process_enabled   VARCHAR2 (5);
        /* nsinghi MIN-MAX INVCONV end */

        p_level             NUMBER;
    BEGIN
        -- Demand includes unshipped sales orders and reservations.
        total := 0;
        lv_org_id := org_id;
        p_level := 1;

        -- Reservation type = 1 : Soft reservation
        --                    2 : Hard reservation
        --                    3 : Supply order reservation

        -- This function contains code to plan at the subinventory level but it has not
        -- been implemented because forecast demand cannot be easily determined at the
        -- subinventory level.  The reorder point report passes a null value in the subinv
        -- field so the demand is selected at the org level.

        /*
          Bug 4458234
          Ignore the Sales Order Reservations.
        */

        IF (net_rsv = 1)
        THEN
            SELECT SUM (primary_uom_quantity - GREATEST (NVL (reservation_quantity, 0), completed_quantity))
              INTO qty
              FROM mtl_demand
             WHERE     reservation_type = 2
                   AND demand_source_type NOT IN (2, 8, 12)
                   AND organization_id = org_id
                   AND inventory_item_id = item_id
                   AND primary_uom_quantity > GREATEST (NVL (reservation_quantity, 0), completed_quantity)
                   AND requirement_date <= demand_cutoff_date
                   AND (   NVL (subinventory, 'x') = DECODE (subinv, NULL, NVL (subinventory, 'x'), subinv) /* Bug 2844326 */
                        OR EXISTS
                               (SELECT 1
                                  FROM mtl_secondary_inventories s
                                 WHERE     s.organization_id = org_id
                                       AND s.secondary_inventory_name = NVL (subinv, subinventory)
                                       AND s.availability_type = DECODE (include_nonnet, 1, s.availability_type, 1)))
                   /* nsinghi MIN-MAX INVCONV start */
                   AND (   locator_id IS NULL
                        OR EXISTS
                               (SELECT 1
                                  FROM mtl_item_locations mil
                                 WHERE     mil.organization_id = org_id
                                       AND mil.inventory_location_id = locator_id
                                       AND mil.subinventory_code = NVL (subinventory, mil.subinventory_code)
                                       AND mil.availability_type = DECODE (include_nonnet, 1, mil.availability_type, 1)))
                   AND (   lot_number IS NULL
                        OR EXISTS
                               (SELECT 1
                                  FROM mtl_lot_numbers mln
                                 WHERE     mln.organization_id = org_id
                                       AND mln.lot_number = lot_number
                                       AND mln.inventory_item_id = item_id
                                       AND mln.availability_type = DECODE (include_nonnet, 1, mln.availability_type, 1)));

            /* nsinghi MIN-MAX INVCONV end */

            total := total + NVL (qty, 0);
        END IF;

        IF (net_unrsv = 1)
        THEN
            SELECT SUM (DECODE (ool.ordered_quantity
                               ,NULL, 0
                               ,inv_decimals_pub.get_primary_quantity (ool.ship_from_org_id
                                                                      ,ool.inventory_item_id
                                                                      ,ool.order_quantity_uom
                                                                      ,ool.ordered_quantity)))
              INTO qty
              FROM oe_order_lines ool
             WHERE     open_flag = 'Y'
                   AND visible_demand_flag = 'Y'
                   AND shipped_quantity IS NULL
                   AND ship_from_org_id = lv_org_id
                   AND inventory_item_id = item_id
                   AND schedule_ship_date <= demand_cutoff_date
                   AND (   NVL (subinventory, 1) = DECODE (subinv, NULL, NVL (subinventory, 1), subinv)
                        OR EXISTS
                               (SELECT 1
                                  FROM mtl_secondary_inventories s
                                 WHERE     s.organization_id = lv_org_id
                                       AND s.secondary_inventory_name = NVL (subinv, subinventory)
                                       AND s.availability_type = DECODE (include_nonnet, 1, s.availability_type, 1)));

            total := total + NVL (qty, 0);
        END IF;

        /*2447312 2608871 added the not exists and the index MTL_DEMAND_N12*/
        /* 4166079 - filter records with wip_supply_type = 5 (vendor) also */

        IF (net_wip = 1)
        THEN
            /* nsinghi MIN-MAX INVCONV start */

            /* Here we need to have query to select OPM batch data  */

            SELECT NVL (process_enabled_flag, 'N')
              INTO c_process_enabled
              FROM mtl_parameters
             WHERE organization_id = org_id;

            IF c_process_enabled = 'Y'
            THEN
                SELECT SUM (
                             (  NVL ( (NVL (d.wip_plan_qty, d.plan_qty) - d.actual_qty), 0)
                              * (d.original_primary_qty / d.original_qty))
                           - NVL (mtr.primary_reservation_quantity, 0))
                  INTO qty
                  FROM gme_material_details d, gme_batch_header h, mtl_reservations mtr
                 WHERE     h.batch_type IN (0, 10)
                       AND h.batch_status IN (1, 2)
                       AND h.batch_id = d.batch_id
                       AND d.line_type = -1
                       AND NVL (d.original_qty, 0) <> 0
                       AND d.organization_id = org_id
                       AND d.inventory_item_id = item_id
                       AND d.batch_id = mtr.demand_source_header_id(+)
                       AND d.material_detail_id = mtr.demand_source_line_id(+)
                       AND d.inventory_item_id = mtr.inventory_item_id(+)
                       AND d.organization_id = mtr.organization_id(+)
                       AND (  (  NVL ( (NVL (d.wip_plan_qty, d.plan_qty) - d.actual_qty), 0)
                               * (d.original_primary_qty / d.original_qty))
                            - NVL (mtr.primary_reservation_quantity, 0)) > 0
                       AND NVL (mtr.demand_source_type_id, 5) = 5
                       AND d.material_requirement_date <= demand_cutoff_date
                       AND (   mtr.subinventory_code IS NULL
                            OR EXISTS
                                   (SELECT 1
                                      FROM mtl_secondary_inventories s
                                     WHERE     s.organization_id = org_id
                                           AND s.secondary_inventory_name = mtr.subinventory_code
                                           AND s.availability_type = DECODE (include_nonnet, 1, s.availability_type, 1)))
                       AND (   mtr.locator_id IS NULL
                            OR EXISTS
                                   (SELECT 1
                                      FROM mtl_item_locations mil
                                     WHERE     mil.organization_id = org_id
                                           AND mil.inventory_location_id = mtr.locator_id
                                           AND mil.subinventory_code =
                                                   NVL (mtr.subinventory_code, mil.subinventory_code)
                                           AND mil.availability_type =
                                                   DECODE (include_nonnet, 1, mil.availability_type, 1)))
                       AND (   mtr.lot_number IS NULL
                            OR EXISTS
                                   (SELECT 1
                                      FROM mtl_lot_numbers mln
                                     WHERE     mln.organization_id = org_id
                                           AND mln.lot_number = mtr.lot_number
                                           AND mln.inventory_item_id = item_id
                                           AND mln.availability_type =
                                                   DECODE (include_nonnet, 1, mln.availability_type, 1)));

                total := total + NVL (qty, 0);
            ELSE
                /* nsinghi MIN-MAX INVCONV end */
                /* 4458234 - Select only open demand for WIP jobs, need to deduct the
                             quantity_issued */

                SELECT SUM (o.required_quantity - o.quantity_issued)
                  INTO qty
                  FROM wip_discrete_jobs d, wip_requirement_operations o
                 WHERE     o.wip_entity_id = d.wip_entity_id
                       AND o.organization_id = d.organization_id
                       AND d.organization_id = org_id
                       AND o.inventory_item_id = item_id
                       AND o.date_required <= demand_cutoff_date
                       AND o.required_quantity >= o.quantity_issued
                       AND o.operation_seq_num > 0
                       AND d.status_type IN (1, 3, 4, 6)
                       AND o.wip_supply_type NOT IN (5, 6)
                       AND NVL (o.supply_subinventory, 1) =
                               DECODE (subinv, NULL, NVL (o.supply_subinventory, 1), subinv)
                       AND NOT EXISTS
                                   (SELECT /*+ index(mtl MTL_DEMAND_N12)*/
                                          wip.wip_entity_id
                                      FROM wip_so_allocations wip, mtl_demand mtl
                                     WHERE     wip_entity_id = o.wip_entity_id
                                           AND wip.organization_id = org_id
                                           AND wip.organization_id = mtl.organization_id
                                           AND wip.demand_source_header_id = mtl.demand_source_header_id
                                           AND wip.demand_source_line = mtl.demand_source_line
                                           AND wip.demand_source_delivery = mtl.demand_source_delivery
                                           AND mtl.inventory_item_id = item_id);

                total := total + NVL (qty, 0);
            /* nsinghi MIN-MAX INVCONV start */
            END IF;                                                                            /* p_process_org = 'Y' */
        /* nsinghi MIN-MAX INVCONV end */

        END IF;

        -- Include move orders
        -- leave out the closed or cancelled lines
        -- select only the issue from stores for org level planning
        -- No lines for the sub level planning.

        SELECT SUM (mtrl.quantity - NVL (mtrl.quantity_delivered, 0))
          INTO qty
          FROM mtl_txn_request_lines mtrl, mtl_transaction_types mtt
         WHERE     mtt.transaction_type_id = mtrl.transaction_type_id
               AND mtrl.organization_id = org_id
               AND mtrl.inventory_item_id = item_id
               AND mtrl.line_status NOT IN (5, 6)
               AND mtt.transaction_action_id = 1
               AND (p_level = 1 OR mtrl.from_subinventory_code = subinv)
               AND (   mtrl.from_subinventory_code IS NULL
                    OR p_level = 2
                    OR EXISTS
                           (SELECT 1
                              FROM mtl_secondary_inventories s
                             WHERE     s.organization_id = org_id
                                   AND s.secondary_inventory_name = mtrl.from_subinventory_code
                                   AND s.availability_type = DECODE (include_nonnet, 1, s.availability_type, 1)))
               AND mtrl.date_required <= demand_cutoff_date
               /* nsinghi MIN-MAX INVCONV start */
               AND (   mtrl.from_locator_id IS NULL
                    OR EXISTS
                           (SELECT 1
                              FROM mtl_item_locations mil
                             WHERE     mil.organization_id = org_id
                                   AND mil.inventory_location_id = mtrl.from_locator_id
                                   AND mil.subinventory_code = NVL (mtrl.from_subinventory_code, mil.subinventory_code)
                                   AND mil.availability_type = DECODE (include_nonnet, 1, mil.availability_type, 1)))
               AND (   mtrl.lot_number IS NULL
                    OR EXISTS
                           (SELECT 1
                              FROM mtl_lot_numbers mln
                             WHERE     mln.organization_id = org_id
                                   AND mln.lot_number = mtrl.lot_number
                                   AND mln.inventory_item_id = item_id
                                   AND mln.availability_type = DECODE (include_nonnet, 1, mln.availability_type, 1)));

        /* nsinghi MIN-MAX INVCONV end */

        total := total + NVL (qty, 0);

        RETURN (ROUND (total, 2));
    EXCEPTION
        WHEN OTHERS
        THEN
            --  srw.message(91,'Error while calculating DemandQty');
            RETURN (0);
    END get_demand;

    -- 02/05/14 CG: TMS 20131120-00030: Updated to consider FLM in final number
    FUNCTION get_ord_qty (p_max           IN NUMBER
                         ,p_available     IN NUMBER
                         ,p_open_po       IN NUMBER
                         ,p_open_req      IN NUMBER
                         ,p_open_demand   IN NUMBER
                         ,p_flm           IN NUMBER)
        RETURN NUMBER
    IS
        l_ord_qty       NUMBER;

        l_flm_add_qty   NUMBER;
    BEGIN
        l_ord_qty := NULL;
        l_ord_qty := p_max - (p_available + p_open_po + p_open_req - p_open_demand);

        IF NVL (l_ord_qty, 0) <= 0
        THEN
            l_ord_qty := 0;
        END IF;

        -- 02/05/14 CG: TMS 20131120-00030: Updated to consider FLM in final number
        IF NVL (p_flm, 0) > 0 AND l_ord_qty > 0
        THEN
            l_flm_add_qty := 0;

            BEGIN
                SELECT MOD (l_ord_qty, p_flm) INTO l_flm_add_qty FROM DUAL;

                IF l_flm_add_qty > 0
                THEN
                    --l_ord_qty := l_ord_qty + (p_flm - l_flm_add_qty);
                    l_ord_qty := CEIL (l_ord_qty / p_flm) * p_flm;
                END IF;
            EXCEPTION
                WHEN OTHERS
                THEN
                    NULL;
            END;
        END IF;

        -- 02/05/14 CG: TMS 20131120-00030: Updated to consider FLM in final number

        RETURN l_ord_qty;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN 0;
    END get_ord_qty;

    FUNCTION get_status (p_min           IN NUMBER
                        ,p_max           IN NUMBER
                        ,p_available     IN NUMBER
                        ,p_open_po       IN NUMBER
                        ,p_open_req      IN NUMBER
                        ,p_open_demand   IN NUMBER)
        RETURN VARCHAR2
    IS
        l_status   VARCHAR2 (30);
    BEGIN
        l_status := NULL;

        IF ( (p_available + p_open_po + p_open_req - p_open_demand) < p_min)
        THEN
            l_status := 'Below Min';
        ELSIF ( (p_available + p_open_po + p_open_req - p_open_demand) = p_min AND p_min <> p_max)
        THEN
            l_status := 'At Min';
        ELSIF ( (p_available + p_open_po + p_open_req - p_open_demand) = p_max)
        THEN
            l_status := 'At Max';
        ELSIF ( (p_available + p_open_po + p_open_req - p_open_demand) > p_max)
        THEN
            l_status := 'Over Max';
        ELSE
            l_status := 'Below Max';
        END IF;

        RETURN l_status;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN ('Stat_Excep');
    END get_status;

    FUNCTION get_perc_spread (p_min           IN NUMBER
                             ,p_max           IN NUMBER
                             ,p_available     IN NUMBER
                             ,p_open_po       IN NUMBER
                             ,p_open_req      IN NUMBER
                             ,p_open_demand   IN NUMBER)
        RETURN NUMBER
    IS
        l_perc_spread   NUMBER;
        l_div           NUMBER;
    BEGIN
        l_perc_spread := NULL;
        l_div := p_max - p_min;

        IF NVL (l_div, 0) != 0
        THEN
            -- 01/23/13 CG: Corrected calculation
            l_perc_spread := ( ( (p_available - p_open_demand) + p_open_po + p_open_req) - p_min) / l_div;
        ELSE
            l_perc_spread := 0;
        END IF;

        RETURN (NVL (ROUND (100 * l_perc_spread, 2), 0));
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN (ROUND (0, 2));
    END get_perc_spread;

    FUNCTION get_period_sales (p_period_type IN VARCHAR2, p_inventory_item_id IN NUMBER, p_organization_id IN NUMBER)
        RETURN NUMBER
    IS
        l_start_date   DATE;
        l_end_date     DATE;
        l_sales        NUMBER;
    BEGIN
        IF p_period_type = 'M'
        THEN
            l_start_date := TRUNC (SYSDATE, 'MM');
            l_end_date := TRUNC (SYSDATE);
        ELSE
            l_start_date := TRUNC (SYSDATE, 'YYYY');
            l_end_date := TRUNC (SYSDATE);
        END IF;

        l_sales := NULL;

        SELECT SUM (sales_order_demand)
          INTO l_sales
          FROM mtl_demand_histories mdh
         WHERE     mdh.inventory_item_id = p_inventory_item_id
               AND mdh.organization_id = p_organization_id
               AND mdh.period_start_date BETWEEN l_start_date AND l_end_date;

        RETURN NVL (l_sales, 0);
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN 0;
    END get_period_sales;

    -- 12/12/13 CG:TMS 20131211-00210: Added to be able to pull with Price Breaks
    FUNCTION get_po_cost (i_inventory_item_id   IN NUMBER
                         ,i_organization_id     IN NUMBER
                         ,i_vendor_id           IN NUMBER
                         ,i_vendor_site_id      IN NUMBER
                         ,i_ord_qty             IN NUMBER)
        RETURN NUMBER
    --Used in XXWC_VENDOR_MINIMUM (Items to Meet Vendor Minimum Form, to recalc cost based on quantities)
    IS
        l_po_cost   NUMBER;
    BEGIN
        BEGIN
            -- 1. Check for Local BPA with PB
            SELECT NVL (pla.price_override, b.unit_price)
              INTO l_po_cost
              FROM po_headers a
                  ,po_lines b
                  ,po_ship_to_loc_org_v c
                  ,                 -- 12/12/2013 CG: TMS 20131211-00210: Added to check shipment for price break by org
                   po_line_locations pla
             WHERE     a.type_lookup_code = 'BLANKET'
                   AND a.enabled_flag = 'Y'
                   AND a.authorization_status = 'APPROVED'
                   AND a.approved_flag = 'Y'
                   AND a.po_header_id = b.po_header_id
                   AND NVL (b.cancel_flag, 'N') = 'N'
                   AND NVL (b.closed_code, 'x') <> 'CLOSED'
                   AND NVL (b.expiration_date, SYSDATE + 1) > SYSDATE
                   AND b.item_id = i_inventory_item_id
                   AND a.vendor_id = i_vendor_id
                   AND a.vendor_site_id = NVL (i_vendor_site_id, a.vendor_site_id)
                   AND a.ship_to_location_id = c.ship_to_location_id
                   AND c.inventory_organization_id = NVL (i_organization_id, c.inventory_organization_id)
                   AND NVL (b.cancel_flag, 'N') = 'N'
                   -- 04/25/2013 TMS 20130424-01825: Checking only local BPAs
                   AND NVL (a.global_agreement_flag, 'N') = 'N'
                   -- 12/12/2013 CG: TMS 20131211-00210: Added to check shipment for price break by org
                   AND b.po_header_id = pla.po_header_id
                   AND b.po_line_id = pla.po_line_id
                   AND NVL (pla.closed_code, 'Z') <> 'CLOSED'
                   AND pla.ship_to_organization_id = i_organization_id
                   AND NVL (pla.quantity, i_ord_qty) <= i_ord_qty
                   -- 12/12/2013 CG: TMS 20131211-00210:
                   AND b.creation_date =
                           (SELECT MAX (b.creation_date)
                              FROM po_headers a
                                  ,po_lines b
                                  ,po_ship_to_loc_org_v c
                                  -- 12/12/2013 CG: TMS 20131211-00210: Added to check shipment for price break by org
                                  ,po_line_locations pla
                             WHERE     a.type_lookup_code = 'BLANKET'
                                   AND a.enabled_flag = 'Y'
                                   AND a.authorization_status = 'APPROVED'
                                   AND a.approved_flag = 'Y'
                                   AND a.po_header_id = b.po_header_id
                                   AND NVL (b.cancel_flag, 'N') = 'N'
                                   AND NVL (b.closed_code, 'x') <> 'CLOSED'
                                   AND NVL (b.expiration_date, SYSDATE + 1) > SYSDATE
                                   AND b.item_id = i_inventory_item_id
                                   AND a.vendor_id = i_vendor_id
                                   AND a.vendor_site_id = NVL (i_vendor_site_id, a.vendor_site_id)
                                   AND a.ship_to_location_id = c.ship_to_location_id
                                   AND c.inventory_organization_id =
                                           NVL (i_organization_id, c.inventory_organization_id)
                                   AND NVL (b.cancel_flag, 'N') = 'N'
                                   -- 04/25/2013 TMS 20130424-01825: Checking only local BPAs
                                   AND NVL (a.global_agreement_flag, 'N') = 'N'
                                   -- 12/12/2013 CG: TMS 20131211-00210: Added to check shipment for price break by org
                                   AND b.po_header_id = pla.po_header_id
                                   AND b.po_line_id = pla.po_line_id
                                   AND NVL (pla.closed_code, 'Z') <> 'CLOSED'
                                   AND pla.ship_to_organization_id = i_organization_id
                                   AND NVL (pla.quantity, i_ord_qty) <= i_ord_qty  -- 12/12/2013 CG: TMS 20131211-00210:
                                                                                 )
                   AND ROWNUM = 1;
        EXCEPTION
            WHEN OTHERS
            THEN
                BEGIN
                    -- 1a. Check for Local BPA with/out PB
                    SELECT b.unit_price
                      INTO l_po_cost
                      FROM po_headers a, po_lines b, po_ship_to_loc_org_v c
                     WHERE     a.type_lookup_code = 'BLANKET'
                           AND a.enabled_flag = 'Y'
                           AND a.authorization_status = 'APPROVED'
                           AND a.approved_flag = 'Y'
                           AND a.po_header_id = b.po_header_id
                           AND NVL (b.cancel_flag, 'N') = 'N'
                           AND NVL (b.closed_code, 'x') <> 'CLOSED'
                           AND NVL (b.expiration_date, SYSDATE + 1) > SYSDATE
                           AND b.item_id = i_inventory_item_id
                           AND a.vendor_id = i_vendor_id
                           AND a.vendor_site_id = NVL (i_vendor_site_id, a.vendor_site_id)
                           AND a.ship_to_location_id = c.ship_to_location_id
                           AND c.inventory_organization_id = NVL (i_organization_id, c.inventory_organization_id)
                           AND NVL (b.cancel_flag, 'N') = 'N'
                           -- 04/25/2013 TMS 20130424-01825: Checking only local BPAs
                           AND NVL (a.global_agreement_flag, 'N') = 'N'
                           AND b.creation_date =
                                   (SELECT MAX (b.creation_date)
                                      FROM po_headers a, po_lines b, po_ship_to_loc_org_v c
                                     WHERE     a.type_lookup_code = 'BLANKET'
                                           AND a.enabled_flag = 'Y'
                                           AND a.authorization_status = 'APPROVED'
                                           AND a.approved_flag = 'Y'
                                           AND a.po_header_id = b.po_header_id
                                           AND NVL (b.cancel_flag, 'N') = 'N'
                                           AND NVL (b.closed_code, 'x') <> 'CLOSED'
                                           AND NVL (b.expiration_date, SYSDATE + 1) > SYSDATE
                                           AND b.item_id = i_inventory_item_id
                                           AND a.vendor_id = i_vendor_id
                                           AND a.vendor_site_id = NVL (i_vendor_site_id, a.vendor_site_id)
                                           AND a.ship_to_location_id = c.ship_to_location_id
                                           AND c.inventory_organization_id =
                                                   NVL (i_organization_id, c.inventory_organization_id)
                                           AND NVL (b.cancel_flag, 'N') = 'N'
                                           -- 04/25/2013 TMS 20130424-01825: Checking only local BPAs
                                           AND NVL (a.global_agreement_flag, 'N') = 'N')
                           AND ROWNUM = 1;
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        BEGIN
                            -- 2. Check for Global BPA with PB
                            SELECT NVL (pla.price_override, b.unit_price)
                              INTO l_po_cost
                              FROM po_headers a, po_lines b,                    -- 04/25/2013 TMS 20130424-01825
                                                                     -- , po_ship_to_loc_org_v c
                                                                     -- 12/12/2013 CG: TMS 20131211-00210: Added to check shipment for price break by org
                                                                     po_line_locations pla
                             WHERE     a.type_lookup_code = 'BLANKET'
                                   AND a.enabled_flag = 'Y'
                                   AND a.authorization_status = 'APPROVED'
                                   AND a.approved_flag = 'Y'
                                   AND a.po_header_id = b.po_header_id
                                   AND NVL (b.cancel_flag, 'N') = 'N'
                                   AND NVL (b.closed_code, 'x') <> 'CLOSED'
                                   AND NVL (b.expiration_date, SYSDATE + 1) > SYSDATE
                                   AND b.item_id = i_inventory_item_id
                                   AND a.vendor_id = i_vendor_id
                                   AND a.vendor_site_id = NVL (i_vendor_site_id, a.vendor_site_id)
                                   -- 04/25/2013 TMS 20130424-01825
                                   --AND a.ship_to_location_id = c.ship_to_location_id
                                   --AND c.inventory_organization_id = NVL (i_organization_id, c.inventory_organization_id)
                                   AND NVL (b.cancel_flag, 'N') = 'N'
                                   -- 04/25/2013 TMS 20130424-01825: Checking only Global BPAs
                                   AND NVL (a.global_agreement_flag, 'N') = 'Y'
                                   -- 12/12/2013 CG: TMS 20131211-00210: Added to check shipment for price break by org
                                   AND b.po_header_id = pla.po_header_id
                                   AND b.po_line_id = pla.po_line_id
                                   AND NVL (pla.closed_code, 'Z') <> 'CLOSED'
                                   AND pla.ship_to_organization_id = i_organization_id
                                   AND NVL (pla.quantity, i_ord_qty) <= i_ord_qty
                                   -- 12/12/2013 CG: TMS 20131211-00210:
                                   AND b.creation_date =
                                           (SELECT MAX (b.creation_date)
                                              FROM po_headers a, po_lines b     -- 04/25/2013 TMS 20130424-01825
                                                                                   --, po_ship_to_loc_org_v c
                                                                                   -- 12/12/2013 CG: TMS 20131211-00210: Added to check shipment for price break by org
                                                   , po_line_locations pla
                                             WHERE     a.type_lookup_code = 'BLANKET'
                                                   AND a.enabled_flag = 'Y'
                                                   AND a.authorization_status = 'APPROVED'
                                                   AND a.approved_flag = 'Y'
                                                   AND a.po_header_id = b.po_header_id
                                                   AND NVL (b.cancel_flag, 'N') = 'N'
                                                   AND NVL (b.closed_code, 'x') <> 'CLOSED'
                                                   AND NVL (b.expiration_date, SYSDATE + 1) > SYSDATE
                                                   AND b.item_id = i_inventory_item_id
                                                   AND a.vendor_id = i_vendor_id
                                                   AND a.vendor_site_id = NVL (i_vendor_site_id, a.vendor_site_id)
                                                   -- 04/25/2013 TMS 20130424-01825
                                                   --AND a.ship_to_location_id = c.ship_to_location_id
                                                   --AND c.inventory_organization_id = NVL (i_organization_id , c.inventory_organization_id)
                                                   AND NVL (b.cancel_flag, 'N') = 'N'
                                                   -- 04/25/2013 TMS 20130424-01825: Checking only local BPAs
                                                   AND NVL (a.global_agreement_flag, 'N') = 'Y'
                                                   -- 12/12/2013 CG: TMS 20131211-00210: Added to check shipment for price break by org
                                                   AND b.po_header_id = pla.po_header_id
                                                   AND b.po_line_id = pla.po_line_id
                                                   AND NVL (pla.closed_code, 'Z') <> 'CLOSED'
                                                   AND pla.ship_to_organization_id = i_organization_id
                                                   AND NVL (pla.quantity, i_ord_qty) <= i_ord_qty -- 12/12/2013 CG: TMS 20131211-00210:
                                                                                                 )
                                   AND ROWNUM = 1;
                        EXCEPTION
                            WHEN OTHERS
                            THEN
                                BEGIN
                                    -- 2a. Check for Global BPA without PB
                                    SELECT b.unit_price
                                      INTO l_po_cost
                                      FROM po_headers a, po_lines b
                                     -- 04/25/2013 TMS 20130424-01825
                                     -- , po_ship_to_loc_org_v c
                                     WHERE     a.type_lookup_code = 'BLANKET'
                                           AND a.enabled_flag = 'Y'
                                           AND a.authorization_status = 'APPROVED'
                                           AND a.approved_flag = 'Y'
                                           AND a.po_header_id = b.po_header_id
                                           AND NVL (b.cancel_flag, 'N') = 'N'
                                           AND NVL (b.closed_code, 'x') <> 'CLOSED'
                                           AND NVL (b.expiration_date, SYSDATE + 1) > SYSDATE
                                           AND b.item_id = i_inventory_item_id
                                           AND a.vendor_id = i_vendor_id
                                           AND a.vendor_site_id = NVL (i_vendor_site_id, a.vendor_site_id)
                                           -- 04/25/2013 TMS 20130424-01825
                                           --AND a.ship_to_location_id = c.ship_to_location_id
                                           --AND c.inventory_organization_id = NVL (i_organization_id, c.inventory_organization_id)
                                           AND NVL (b.cancel_flag, 'N') = 'N'
                                           -- 04/25/2013 TMS 20130424-01825: Checking only Global BPAs
                                           AND NVL (a.global_agreement_flag, 'N') = 'Y'
                                           AND b.creation_date =
                                                   (SELECT MAX (b.creation_date)
                                                      FROM po_headers a, po_lines b
                                                     -- 04/25/2013 TMS 20130424-01825
                                                     --, po_ship_to_loc_org_v c
                                                     WHERE     a.type_lookup_code = 'BLANKET'
                                                           AND a.enabled_flag = 'Y'
                                                           AND a.authorization_status = 'APPROVED'
                                                           AND a.approved_flag = 'Y'
                                                           AND a.po_header_id = b.po_header_id
                                                           AND NVL (b.cancel_flag, 'N') = 'N'
                                                           AND NVL (b.closed_code, 'x') <> 'CLOSED'
                                                           AND NVL (b.expiration_date, SYSDATE + 1) > SYSDATE
                                                           AND b.item_id = i_inventory_item_id
                                                           AND a.vendor_id = i_vendor_id
                                                           AND a.vendor_site_id =
                                                                   NVL (i_vendor_site_id, a.vendor_site_id)
                                                           -- 04/25/2013 TMS 20130424-01825
                                                           --AND a.ship_to_location_id = c.ship_to_location_id
                                                           --AND c.inventory_organization_id = NVL (i_organization_id , c.inventory_organization_id)
                                                           AND NVL (b.cancel_flag, 'N') = 'N'
                                                           -- 04/25/2013 TMS 20130424-01825: Checking only local BPAs
                                                           AND NVL (a.global_agreement_flag, 'N') = 'Y')
                                           AND ROWNUM = 1;
                                EXCEPTION
                                    WHEN OTHERS
                                    THEN
                                        BEGIN
                                            -- 3. Item List Price by Org
                                            SELECT list_price_per_unit
                                              INTO l_po_cost
                                              FROM mtl_system_items
                                             WHERE     inventory_item_id = i_inventory_item_id
                                                   AND organization_id = i_organization_id;

                                            RETURN (l_po_cost);
                                        EXCEPTION
                                            WHEN OTHERS
                                            THEN
                                                l_po_cost := 0;
                                        END;
                                END;
                        END;
                END;
        END;

        RETURN (l_po_cost);
    EXCEPTION
        WHEN OTHERS
        THEN
            -- Added 09-Apr-2012 Shankar Hariharan
            -- To return list price if no PO Cost is available
            BEGIN
                SELECT list_price_per_unit
                  INTO l_po_cost
                  FROM mtl_system_items
                 WHERE inventory_item_id = i_inventory_item_id AND organization_id = i_organization_id;

                RETURN (l_po_cost);
            EXCEPTION
                WHEN OTHERS
                THEN
                    RETURN (0);
            END;
    END get_po_cost;

    -- 01/16/14 CG: TMS 20131120-00030: Added to retrieve Incomplete Internal Req Qty's
    FUNCTION get_inc_int_req_qty (supply_cutoff_date   IN DATE
                                 ,dst_org_id           IN NUMBER
                                 ,current_item_id      IN NUMBER
                                 ,src_org_id           IN NUMBER)
        RETURN NUMBER
    IS
    /************************************************************************************************************
      Function: get_inc_int_req_qty

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        10/30/2012  Consuelo Gonzalez      Initial Version
	  1.4        01/11/2018	P.Vamshidhar            TMS#20180110-00287 - Oracle Vendor Meet Min Form slowness
     ************************************************************************************************************/	
        l_inc_int_req_qty   NUMBER;
        p_char_qty          VARCHAR2 (200);
        l_stmt              VARCHAR2 (4000);
        scd                 VARCHAR2 (20);
    BEGIN
        scd := TO_CHAR (supply_cutoff_date, 'DD-MON-RRRR');

        -- ' and     prl.destination_organization_id = NVL('||to_char(dst_org_id)||', prl.destination_organization_id)
/* -- Commented in 1.4 by Vamshi
        l_stmt :=
               'select  sum(nvl(prl.quantity,0) - nvl(prl.quantity_delivered,0) - nvl(prl.quantity_cancelled,0))
                    from    po_requisition_lines prl
                            , po_requisition_headers prh
                    where   prl.source_type_code = ''INVENTORY''
                    and     prl.item_id = '
            || TO_CHAR (current_item_id)
            || ' and     prl.destination_organization_id = '
            || TO_CHAR (dst_org_id)
            || ' and     prl.source_organization_id = '
            || TO_CHAR (src_org_id)
            || ' and     prl.need_by_date <= TO_DATE('''
            || scd
            || ''',''DD-MON-RRRR'')'
            || ' and     nvl(prl.cancel_flag, ''N'') = ''N''
                    and     prl.requisition_header_id = prh.requisition_header_id
                    and     prh.authorization_status = ''INCOMPLETE''
                    and exists (select 1 from fnd_user u where u.user_name =''XXWC_INT_SUPPLYCHAIN'' and u.user_id =prh.created_by)';
*/

     -- Added below code in 1.4 by Vamshi.
        l_stmt :=
               'select  sum(nvl(prl.quantity,0) - nvl(prl.quantity_delivered,0) - nvl(prl.quantity_cancelled,0))
                    from    po_requisition_lines prl
                            , po_requisition_headers prh
                    where   prl.source_type_code = ''INVENTORY''
                    and     prl.item_id = '
            || ' TO_CHAR (:1) '
            || ' and     prl.destination_organization_id = '
            || ' TO_CHAR (:2) '
            || ' and     prl.source_organization_id = '
            || ' TO_CHAR (:3) '
            || ' and     prl.need_by_date <= TO_DATE('''
            || scd
            || ''',''DD-MON-RRRR'')'
            || ' and     nvl(prl.cancel_flag, ''N'') = ''N''
                    and     prl.requisition_header_id = prh.requisition_header_id
                    and     prh.authorization_status = ''INCOMPLETE''
                    and exists (select 1 from fnd_user u where u.user_name =''XXWC_INT_SUPPLYCHAIN'' and u.user_id =prh.created_by)';

        p_char_qty := NULL;

        BEGIN		    
            --xxwc_exec_dynamic_sql (l_stmt, p_char_qty);
			EXECUTE IMMEDIATE l_stmt into p_char_qty using current_item_id,dst_org_id,src_org_id;
        EXCEPTION
            WHEN OTHERS
            THEN
                p_char_qty := NULL;
        END;

        l_inc_int_req_qty := TO_NUMBER (p_char_qty);

        RETURN (NVL (l_inc_int_req_qty, 0));
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN (0);
    END get_inc_int_req_qty;

    -- 01/21/14 CG: TMS 20131216-00029: New procedure based report to extract Vendor Min Data
    PROCEDURE xxwc_vendor_min_extract_rpt (errbuf             OUT VARCHAR2
                                          ,retcode            OUT NUMBER
                                          ,p_org_id        IN     NUMBER
                                          ,p_org_list_id   IN     NUMBER
                                          ,p_sup_list_id   IN     NUMBER)
    IS
        l_error_message2   CLOB;
        l_conc_req_id      NUMBER := fnd_global.conc_request_id;
        l_start_time       NUMBER;
        l_end_time         NUMBER;
        l_login_id         NUMBER := fnd_global.login_id;

        TYPE vendormintyp IS REF CURSOR;

        v_vendor_min       vendormintyp;
        v_stmt_str         VARCHAR2 (30000);
        v_vendor_min_dtl   VARCHAR2 (10000);
        l_count            NUMBER;

        v_list_values      xxwc_inv_prod_cloning.list_items;
        v_run_string       CLOB;
        v_param_count      NUMBER;
        n                  NUMBER := 0;
    BEGIN
        fnd_file.put_line (fnd_file.LOG, 'Starting XXWC PO Vendor Minimum Maintenance Extract');
        fnd_file.put_line (fnd_file.LOG, '=====================================================');
        fnd_file.put_line (fnd_file.LOG, ' ');

        IF p_org_id IS NOT NULL AND p_org_list_id IS NOT NULL
        THEN
            l_error_message2 := 'Both Organization and Org List parameters can be populate, use one or the other';

            fnd_file.put_line (fnd_file.LOG, l_error_message2);

            errbuf := l_error_message2;
            retcode := 2;
            RETURN;
        END IF;

        v_stmt_str :=
               'select   vendor_number||'
            || ''''
            || '|'
            || ''''
            || '
                                ||organization_code||'
            || ''''
            || '|'
            || ''''
            || '
                                ||vendor_min_dollar||'
            || ''''
            || '|'
            || ''''
            || '
                                ||freight_min_dollar||'
            || ''''
            || '|'
            || ''''
            || '
                                ||freight_min_uom||'
            || ''''
            || '|'
            || ''''
            || '||ppd_freight_units||'
            || ''''
            || '|'
            || ''''
            || '||REGEXP_REPLACE (notes, '
            || ''''
            || '[^[:alnum:]|[:blank:]|[:punct:]]+'
            || ''''
            || ', null)
                        from    apps.xxwc_po_vendor_minadi_vw ';

        IF p_org_id IS NOT NULL AND p_org_list_id IS NULL AND p_sup_list_id IS NULL
        THEN
            v_stmt_str := v_stmt_str || ' where organization_id = ' || p_org_id;
        ELSIF p_org_id IS NOT NULL AND p_org_list_id IS NULL AND p_sup_list_id IS NOT NULL
        THEN
            v_stmt_str :=
                   v_stmt_str
                || ' where organization_id = '
                || p_org_id
                || '
                                        and sup_number in (';

            n := 0;

            FOR r IN (SELECT list_id
                            ,list_name
                            ,REGEXP_REPLACE (LIST_VALUES, '[[:cntrl:]]', NULL) LIST_VALUES
                            ,list_type
                        FROM xxwc_param_list
                       WHERE list_id = p_sup_list_id)
            LOOP
                xxwc_inv_prod_cloning.read_coma_dlmtd_string (r.LIST_VALUES, v_list_values);
                v_param_count := v_list_values.COUNT;

                FOR k IN v_list_values.FIRST .. v_list_values.LAST
                LOOP
                    n := 1 + n;

                    IF v_list_values (k) IS NOT NULL
                    THEN
                        v_stmt_str := v_stmt_str || RTRIM (LTRIM (v_list_values (k), ' '), ' ');

                        IF n < v_param_count
                        THEN
                            v_stmt_str := v_stmt_str || ',';
                        END IF;
                    END IF;
                END LOOP;
            END LOOP;

            v_stmt_str := v_stmt_str || ') ';
        ELSIF p_org_id IS NULL AND p_org_list_id IS NOT NULL AND p_sup_list_id IS NULL
        THEN
            v_stmt_str := v_stmt_str || ' where organization_code in (';

            n := 0;

            FOR r IN (SELECT list_id
                            ,list_name
                            ,REGEXP_REPLACE (LIST_VALUES, '[[:cntrl:]]', NULL) LIST_VALUES
                            ,list_type
                        FROM xxwc_param_list
                       WHERE list_id = p_org_list_id)
            LOOP
                xxwc_inv_prod_cloning.read_coma_dlmtd_string (r.LIST_VALUES, v_list_values);
                v_param_count := v_list_values.COUNT;

                FOR k IN v_list_values.FIRST .. v_list_values.LAST
                LOOP
                    n := 1 + n;

                    IF v_list_values (k) IS NOT NULL
                    THEN
                        v_stmt_str := v_stmt_str || 'to_char(' || RTRIM (LTRIM (v_list_values (k), ' '), ' ') || ')';

                        IF n < v_param_count
                        THEN
                            v_stmt_str := v_stmt_str || ',';
                        END IF;
                    END IF;
                END LOOP;
            END LOOP;

            v_stmt_str := v_stmt_str || ') ';
        ELSIF p_org_id IS NULL AND p_org_list_id IS NOT NULL AND p_sup_list_id IS NOT NULL
        THEN
            v_stmt_str := v_stmt_str || ' where organization_code in (';

            n := 0;

            FOR r IN (SELECT list_id
                            ,list_name
                            ,REGEXP_REPLACE (LIST_VALUES, '[[:cntrl:]]', NULL) LIST_VALUES
                            ,list_type
                        FROM xxwc_param_list
                       WHERE list_id = p_org_list_id)
            LOOP
                xxwc_inv_prod_cloning.read_coma_dlmtd_string (r.LIST_VALUES, v_list_values);
                v_param_count := v_list_values.COUNT;

                FOR k IN v_list_values.FIRST .. v_list_values.LAST
                LOOP
                    n := 1 + n;

                    IF v_list_values (k) IS NOT NULL
                    THEN
                        v_stmt_str := v_stmt_str || 'to_char(' || RTRIM (LTRIM (v_list_values (k), ' '), ' ') || ')';

                        IF n < v_param_count
                        THEN
                            v_stmt_str := v_stmt_str || ',';
                        END IF;
                    END IF;
                END LOOP;
            END LOOP;

            v_stmt_str := v_stmt_str || ') and sup_number in (';

            n := 0;

            FOR r IN (SELECT list_id
                            ,list_name
                            ,REGEXP_REPLACE (LIST_VALUES, '[[:cntrl:]]', NULL) LIST_VALUES
                            ,list_type
                        FROM xxwc_param_list
                       WHERE list_id = p_sup_list_id)
            LOOP
                xxwc_inv_prod_cloning.read_coma_dlmtd_string (r.LIST_VALUES, v_list_values);
                v_param_count := v_list_values.COUNT;

                FOR k IN v_list_values.FIRST .. v_list_values.LAST
                LOOP
                    n := 1 + n;

                    IF v_list_values (k) IS NOT NULL
                    THEN
                        v_stmt_str := v_stmt_str || RTRIM (LTRIM (v_list_values (k), ' '), ' ');

                        IF n < v_param_count
                        THEN
                            v_stmt_str := v_stmt_str || ',';
                        END IF;
                    END IF;
                END LOOP;
            END LOOP;

            v_stmt_str := v_stmt_str || ') ';
        ELSIF p_org_id IS NULL AND p_org_list_id IS NULL AND p_sup_list_id IS NOT NULL
        THEN
            v_stmt_str := v_stmt_str || ' where sup_number in (';

            n := 0;

            FOR r IN (SELECT list_id
                            ,list_name
                            ,REGEXP_REPLACE (LIST_VALUES, '[[:cntrl:]]', NULL) LIST_VALUES
                            ,list_type
                        FROM xxwc_param_list
                       WHERE list_id = p_sup_list_id)
            LOOP
                xxwc_inv_prod_cloning.read_coma_dlmtd_string (r.LIST_VALUES, v_list_values);
                v_param_count := v_list_values.COUNT;

                FOR k IN v_list_values.FIRST .. v_list_values.LAST
                LOOP
                    n := 1 + n;

                    IF v_list_values (k) IS NOT NULL
                    THEN
                        v_stmt_str := v_stmt_str || RTRIM (LTRIM (v_list_values (k), ' '), ' ');

                        IF n < v_param_count
                        THEN
                            v_stmt_str := v_stmt_str || ',';
                        END IF;
                    END IF;
                END LOOP;
            END LOOP;

            v_stmt_str := v_stmt_str || ') ';
        ELSE
            -- p_org_id is null and p_org_list_id is null and p_sup_list_id is null
            fnd_file.put_line (fnd_file.LOG, 'No parameters passed, doing a full download');
        END IF;

        v_stmt_str := v_stmt_str || ' order by organization_code, vendor_number';

        fnd_file.put_line (fnd_file.LOG, 'Select statement: ');
        fnd_file.put_line (fnd_file.LOG, v_stmt_str);

        fnd_file.put_line (
            fnd_file.output
           ,   'Vendor-Site|Branch|Vendor Min('
            || CHR (36)
            || ')|Freight Min('
            || CHR (36)
            || ')|Freight Min (LBS)|PPD Freight Units|Notes');

        OPEN v_vendor_min FOR v_stmt_str;

        -- Fetch rows from result set one at a time:
        l_count := 0;

        LOOP
            FETCH v_vendor_min INTO v_vendor_min_dtl;

            EXIT WHEN v_vendor_min%NOTFOUND;

            fnd_file.put_line (fnd_file.output, v_vendor_min_dtl);
            l_count := l_count + 1;
        END LOOP;

        -- Close cursor:
        CLOSE v_vendor_min;

        fnd_file.put_line (fnd_file.LOG, ' ');
        fnd_file.put_line (fnd_file.LOG, '=====================================================');
        fnd_file.put_line (fnd_file.LOG, ' ');
        fnd_file.put_line (fnd_file.LOG, 'Extracted ' || l_count || ' lines');

        COMMIT;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_error_message2 :=
                   'xxwc_po_vendor_min_pkg.xxwc_vendor_min_extract_rpt '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => 'xxwc_po_vendor_min_pkg.xxwc_vendor_min_extract_rpt'
               ,p_calling             => 'xxwc_po_vendor_min_pkg.xxwc_vendor_min_extract_rpt'
               ,p_request_id          => l_conc_req_id
               ,p_ora_error_msg       => l_error_message2
               ,p_error_desc          => 'Error running XXWC PO Vendor Minimum Maintenance Extract'
               ,p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com'
               ,p_module              => 'PO');

            fnd_file.put_line (fnd_file.LOG, ' ');
            fnd_file.put_line (
                fnd_file.LOG
               ,'Error in main xxwc_po_vendor_min_pkg.xxwc_vendor_min_extract_rpt. Error: ' || l_error_message2);

            errbuf := l_error_message2;
            retcode := 2;
    END xxwc_vendor_min_extract_rpt;

    PROCEDURE xxwc_populate_temp (p_source_org_id         IN NUMBER
                                 ,p_inventory_item_id     IN NUMBER
                                 ,p_demand_cut_off_date   IN VARCHAR2)
    IS
        l_ref_cursor   cursor_type;

        l_main_sql     VARCHAR2 (32000);
        l_count        NUMBER;
    BEGIN
        DELETE FROM xxwc.xxwc_po_vendor_min_int_stg;

        l_main_sql :=
               ' select  msib.source_organization_id
                            , msib.organization_id
                            , msib.inventory_item_id
                            , mp_dst.organization_code mp_dst_org_code
                            , msib.segment1 item_number
                            , msib.description
                            , NVL (msib.attribute20, 0) amu
                            , NVL (msib.unit_weight, 0) unit_weight
                            , msib.weight_uom_code
                            , NVL (msib.fixed_lot_multiplier, 0) fixed_lot_multiplier
                            , NVL (ROUND (msib.min_minmax_quantity, 0), 0) min_minmax_quantity
                            , NVL (ROUND (msib.max_minmax_quantity, 0), 0) max_minmax_quantity
                            , msib.list_price_per_unit
                            , xxwc_ascp_scwb_pkg.get_on_hand (msib.inventory_item_id, mp_dst.organization_id, ''H'') on_hand_qty
                            , SUBSTR (xxwc_mv_routines_pkg.get_item_category (msib.inventory_item_id, mp_dst.organization_id, ''Sales Velocity'', 1), 1, 30) sales_velocity
                            , 0 open_demand
                            , 0 open_po
                            , 0 open_req
                            , 0 incomplete_reqs
                            , 0 potential_demand
                            , 0 perc_spread
                            , NULL status
                            , 0 ord_qty
                            , 0 avail2
                            , 0 sort_sequence
                            , 0 sort_sequence2
                            ,NULL status_short
                    from    mtl_system_items_b msib
                            , mtl_parameters mp_dst,xxwc.po_vendor_min_session_items si
                    where   msib.source_type = 1 -- Sourced from Inventory
                    and     msib.inventory_item_id = si.inventory_item_id
                    and     msib.source_organization_id=si.source_organization_id
                    and     msib.source_organization_id = '
            || p_source_org_id
            || '
                    and     (NVL (msib.min_minmax_quantity, 0) <> 0
                             OR NVL (msib.max_minmax_quantity, 0) <> 0) ';

        /*and     NVL(xxwc_mv_routines_pkg.get_item_category (msib.inventory_item_id, mp_dst.organization_id, ''Sales Velocity'', 1),''ZZ'') != ''N'' ';*/

        IF p_inventory_item_id IS NOT NULL
        THEN
            l_main_sql :=
                   l_main_sql
                || ' and     msib.inventory_item_id = '
                || p_inventory_item_id
                || '
                                        and     msib.organization_id = mp_dst.organization_id ';
        ELSE
            l_main_sql := l_main_sql || ' and     msib.organization_id = mp_dst.organization_id ';
        END IF;

        --DBMS_OUTPUT.put_line ('Step 1');

        OPEN l_ref_cursor FOR l_main_sql;

        LOOP
            --DBMS_OUTPUT.PUT_LINE('Step 2');

            FETCH l_ref_cursor
            BULK COLLECT INTO g_view_record
            LIMIT 10000;

            l_count := 0;

            FOR j IN 1 .. g_view_record.COUNT
            LOOP
                --DBMS_OUTPUT.PUT_LINE('Step 3');

                g_view_table (j).source_organization_id := g_view_record (j).source_organization_id;
                g_view_table (j).organization_id := g_view_record (j).organization_id;
                g_view_table (j).inventory_item_id := g_view_record (j).inventory_item_id;
                g_view_table (j).mp_dst_org_code := g_view_record (j).mp_dst_org_code;
                g_view_table (j).item_number := g_view_record (j).item_number;
                g_view_table (j).description := g_view_record (j).description;
                g_view_table (j).amu := g_view_record (j).amu;
                g_view_table (j).unit_weight := g_view_record (j).unit_weight;
                g_view_table (j).weight_uom_code := g_view_record (j).weight_uom_code;
                g_view_table (j).fixed_lot_multiplier := g_view_record (j).fixed_lot_multiplier;
                g_view_table (j).min_minmax_quantity := g_view_record (j).min_minmax_quantity;
                g_view_table (j).max_minmax_quantity := g_view_record (j).max_minmax_quantity;
                g_view_table (j).list_price_per_unit := g_view_record (j).list_price_per_unit;
                g_view_table (j).on_hand_qty := g_view_record (j).on_hand_qty;
                g_view_table (j).sales_velocity := g_view_record (j).sales_velocity;

                SELECT (xxwc_po_vendor_min_pkg.get_demand (g_view_record (j).inventory_item_id
                                                          ,g_view_record (j).organization_id
                                                          ,TO_DATE (p_demand_cut_off_date, 'DD-MON-YYYY')
                                                          ,1
                                                          ,2
                                                          ,2
                                                          ,1
                                                          ,1
                                                          ,NULL))
                      , (xxwc_po_vendor_min_pkg.get_po_qty (TO_DATE (p_demand_cut_off_date, 'DD-MON-YYYY')
                                                           ,g_view_record (j).organization_id
                                                           ,g_view_record (j).inventory_item_id
                                                           ,1
                                                           ,2
                                                           ,2
                                                           ,2
                                                           ,NULL))
                      , (xxwc_po_vendor_min_pkg.get_req_qty (TO_DATE (p_demand_cut_off_date, 'DD-MON-YYYY')
                                                            ,g_view_record (j).organization_id
                                                            ,g_view_record (j).inventory_item_id
                                                            ,1
                                                            ,2
                                                            ,2
                                                            ,2
                                                            ,NULL))
                      , (xxwc_po_vendor_min_pkg.get_inc_int_req_qty (TO_DATE (p_demand_cut_off_date, 'DD-MON-YYYY')
                                                                    ,g_view_record (j).organization_id
                                                                    ,g_view_record (j).inventory_item_id
                                                                    ,g_view_record (j).source_organization_id))
                  INTO g_view_table (j).open_demand
                      ,g_view_table (j).open_po
                      ,g_view_table (j).open_req
                      ,g_view_table (j).incomplete_reqs
                  FROM DUAL;

                SELECT xxwc_po_vendor_min_pkg.get_perc_spread (g_view_table (j).min_minmax_quantity
                                                              ,g_view_table (j).max_minmax_quantity
                                                              ,g_view_table (j).on_hand_qty
                                                              ,g_view_table (j).open_po
                                                              ,g_view_table (j).open_req
                                                              ,g_view_table (j).open_demand)
                      ,NVL (g_view_table (j).on_hand_qty, 0) - NVL (g_view_table (j).open_demand, 0)
                      ,xxwc_po_vendor_min_pkg.get_status (g_view_table (j).min_minmax_quantity
                                                         ,g_view_table (j).max_minmax_quantity
                                                         ,g_view_table (j).on_hand_qty
                                                         ,g_view_table (j).open_po
                                                         ,g_view_table (j).open_req
                                                         ,g_view_table (j).open_demand)
                  INTO g_view_table (j).perc_spread, g_view_table (j).avail2, g_view_table (j).status
                  FROM DUAL;

                g_view_table (j).ord_quantity := g_view_record (j).ord_quantity;

                g_view_table (j).potential_demand :=
                    CASE
                        WHEN g_view_table (j).status = 'Over Max'
                        THEN
                            (  NVL (g_view_table (j).max_minmax_quantity, 0)
                             - (  NVL (g_view_table (j).avail2, 0)
                                + NVL (g_view_table (j).open_po, 0)
                                + NVL (g_view_table (j).open_req, 0))
                             - g_view_table (j).incomplete_reqs) --  5/19/2014,   Include I req in p demand calculation.  Currently we do not consider an incomplete I req as supply.  This is creating a double counting of potential demand values for the sourcing orgs.
                        ELSE
                            (  NVL (g_view_table (j).max_minmax_quantity, 0)
                             - (  NVL (g_view_table (j).avail2, 0)
                                + NVL (g_view_table (j).open_po, 0)
                                + NVL (g_view_table (j).open_req, 0))
                             - g_view_table (j).incomplete_reqs) -- 5/19/2014,   Include I req in p demand calculation.  Currently we do not consider an incomplete I req as supply.  This is creating a double counting of potential demand values for the sourcing orgs.
                    END;

                g_view_table (j).sort_sequence :=
                    CASE
                        WHEN g_view_table (j).status = 'Over Max' THEN 1
                        WHEN g_view_table (j).status = 'Below Min' THEN 2
                        WHEN g_view_table (j).status = 'At Min' THEN 3
                        WHEN g_view_table (j).status = 'Below Max' THEN 4
                        WHEN g_view_table (j).status = 'At Max' THEN 5
                        ELSE 6
                    END;
                g_view_table (j).status_short :=
                    CASE
                        WHEN g_view_table (j).status = 'Over Max' THEN 'OMAX'
                        WHEN g_view_table (j).status = 'Below Min' THEN 'BMIN'
                        WHEN g_view_table (j).status = 'At Min' THEN 'AMIN'
                        WHEN g_view_table (j).status = 'Below Max' THEN 'BMAX'
                        WHEN g_view_table (j).status = 'At Max' THEN 'AMAX'
                        ELSE NULL
                    END;

                g_view_table (j).sort_sequence2 :=
                    CASE WHEN g_view_table (j).status = 'Over Max' THEN g_view_table (j).potential_demand ELSE NULL END;

                l_count := l_count + 1;
            END LOOP;

            --DBMS_OUTPUT.PUT_LINE('Step 4');
            IF l_count >= 1
            THEN
                --DBMS_OUTPUT.PUT_LINE('Step 5');
                FORALL j IN 1 .. g_view_table.COUNT
                    INSERT INTO xxwc.xxwc_po_vendor_min_int_stg
                         VALUES g_view_table (j);

                COMMIT;
            END IF;

            g_view_table.delete;
            g_view_record.delete;

            COMMIT;

            IF l_ref_cursor%NOTFOUND
            THEN
                CLOSE l_ref_cursor;

                EXIT;
            END IF;
        --DBMS_OUTPUT.put_line ('Step 7');
        END LOOP;
    EXCEPTION
        WHEN OTHERS
        THEN
            DBMS_OUTPUT.put_line ('ERROR: ' || SQLERRM);
            RAISE;
    END xxwc_populate_temp;

    --added by Rasikha to tune the form 2/6/2014
    PROCEDURE populate_items_table (p_organization_id NUMBER, p_vendor_id NUMBER, p_vendor_site_id NUMBER)
    IS
        v_count   NUMBER := 0;
    BEGIN
        EXECUTE IMMEDIATE 'truncate table xxwc.po_vendor_min_session_items';

        INSERT INTO xxwc.po_vendor_min_session_items (source_organization_id, inventory_item_id)
            SELECT organization_id, inventory_item_id
              FROM apps.xxwc_po_vendor_min_items
             WHERE     vendor_id = p_vendor_id
                   AND vendor_site_id = p_vendor_site_id
                   AND organization_id = p_organization_id;

        COMMIT;

        FOR r IN (SELECT COUNT (1) rec_count FROM xxwc.po_vendor_min_session_items)
        LOOP
            IF r.rec_count = 0
            THEN
                INSERT INTO xxwc.po_vendor_min_session_items (source_organization_id, inventory_item_id)
                    SELECT organization_id, inventory_item_id
                      FROM apps.xxwc_po_vendor_min_items
                     WHERE vendor_id = p_vendor_id AND vendor_site_id = p_vendor_site_id;

                COMMIT;
            END IF;
        END LOOP;
    END;
END xxwc_po_vendor_min_pkg;
/

-- End of DDL Script for Package Body APPS.XXWC_PO_VENDOR_MIN_PKG

