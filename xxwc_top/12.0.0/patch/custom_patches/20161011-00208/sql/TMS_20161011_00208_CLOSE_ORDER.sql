/*************************************************************************
  $Header TMS_20161011_00208_CLOSE_ORDER.sql $
  Module Name: TMS_20161011_00208  Data Fix script for 24257633

  PURPOSE: Data fix script for 24257633--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        21-DEC-2016  Niraj K Ranjan         TMS#20161011-00208 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20161011-00208    , Before Update');

   update apps.oe_order_lines_all
   set FLOW_STATUS_CODE='CANCELLED',
   CANCELLED_FLAG='Y'
   where line_id = 40237610
   and header_id= 24257633;

   DBMS_OUTPUT.put_line (
         'TMS: 20161011-00208  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20161011-00208    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161011-00208 , Errors : ' || SQLERRM);
END;
/
