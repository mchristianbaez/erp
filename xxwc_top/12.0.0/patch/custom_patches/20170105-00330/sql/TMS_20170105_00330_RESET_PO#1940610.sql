/*************************************************************************
  $Header datafix_TMS_20170105-00330 .sql $
  Module Name: TMS_20170105-00330 


  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
   1.0        13-JAN-2017  Niraj K Ranjan       TMS#20170105-00330   FW: Action Required: Document Manager Failed 
                                                                     With Error Number 3 while processing Standard 
																	 Purchase
**************************************************************************/ 
/*=======================================================================+
 	 |  Copyright (c) 2009 Oracle Corporation Redwood Shores, California, USA|
 	 |                            All rights reserved.                       |
 	 +=======================================================================*/

 /* $Header: poxrespo.sql 120.0.12010000.4 2010/06/09 00:12:31 vrecharl noship $ */
--SET SERVEROUTPUT ON
--SET VERIFY OFF;


/* PLEASE READ NOTE 390023.1 CAREFULLY BEFORE EXECUTING THIS SCRIPT.

 This script will:
* reset the document to incomplete/requires reapproval status.
* delete/update action history as desired (refere note 390023.1 for more details).
* abort all the related workflows 

* If there is a distribution with wrong encumbrance amount related to this PO, 
* it will: skip the reset action on the document.
*/

set serveroutput on size 100000
DECLARE
   /* select only the POs which are in preapproved, in process state and are not finally closed
      cancelled */

   CURSOR potoreset (
      po_number    VARCHAR2,
      x_org_id     NUMBER)
   IS
      SELECT wf_item_type,
             wf_item_key,
             po_header_id,
             segment1,
             revision_num,
             type_lookup_code,
             approved_date
        FROM po_headers_all
       WHERE     segment1 = po_number
             AND NVL (org_id, -99) = NVL (x_org_id, -99)
             -- bug 5015493: Need to allow reset of blankets and PPOs also.
             -- and type_lookup_code = 'STANDARD'
             AND authorization_status IN ('IN PROCESS', 'PRE-APPROVED')
             AND NVL (cancel_flag, 'N') = 'N'
             AND NVL (closed_code, 'OPEN') <> 'FINALLY_CLOSED';

   /* select the max sequence number with NULL action code */

   CURSOR maxseq (
      id         NUMBER,
      subtype    po_action_history.object_sub_type_code%TYPE)
   IS
      SELECT NVL (MAX (sequence_num), 0)
        FROM po_action_history
       WHERE     object_type_code IN ('PO', 'PA')
             AND object_sub_type_code = subtype
             AND object_id = id
             AND action_code IS NULL;

   /* select the max sequence number with submit action */

   CURSOR poaction (
      id         NUMBER,
      subtype    po_action_history.object_sub_type_code%TYPE)
   IS
      SELECT NVL (MAX (sequence_num), 0)
        FROM po_action_history
       WHERE     object_type_code IN ('PO', 'PA')
             AND object_sub_type_code = subtype
             AND object_id = id
             AND action_code = 'SUBMIT';

   CURSOR wfstoabort (
      st_item_type    VARCHAR2,
      st_item_key     VARCHAR2)
   IS
          SELECT LEVEL,
                 item_type,
                 item_key,
                 end_date
            FROM wf_items
      START WITH item_type = st_item_type AND item_key = st_item_key
      CONNECT BY     PRIOR item_type = parent_item_type
                 AND PRIOR item_key = parent_item_key
        ORDER BY LEVEL DESC;

   wf_rec                     wfstoabort%ROWTYPE;

   submitseq                  po_action_history.sequence_num%TYPE;
   nullseq                    po_action_history.sequence_num%TYPE;

   x_organization_id          NUMBER;
   x_po_number                VARCHAR2 (20);
   po_enc_flag                VARCHAR2 (1);
   x_open_notif_exist         VARCHAR2 (1);
   pos                        potoreset%ROWTYPE;

   x_progress                 VARCHAR2 (500);
   x_cont                     VARCHAR2 (10);
   x_active_wf_exists         VARCHAR2 (1);
   l_delete_act_hist          VARCHAR2 (1);
   l_change_req_exists        VARCHAR2 (1);
   l_res_seq                  po_action_history.sequence_num%TYPE;
   l_sub_res_seq              po_action_history.sequence_num%TYPE;
   l_res_act                  po_action_history.action_code%TYPE;
   l_del_res_hist             VARCHAR2 (1);


   /* For encumbrance actions */

   NAME_ALREADY_USED          EXCEPTION;
   PRAGMA EXCEPTION_INIT (NAME_ALREADY_USED, -955);
   X_STMT                     VARCHAR2 (2000);
   disallow_script            VARCHAR2 (1);

   TYPE enc_tbl_number IS TABLE OF NUMBER;

   TYPE enc_tbl_flag IS TABLE OF VARCHAR2 (1);

   l_dist_id                  enc_tbl_number;
   l_enc_flag                 enc_tbl_flag;
   l_enc_amount               enc_tbl_number;
   l_gl_amount                enc_tbl_number;
   l_manual_cand              enc_tbl_flag;
   l_req_dist_id              enc_tbl_number;
   l_req_enc_flag             enc_tbl_flag;
   l_req_enc_amount           enc_tbl_number;
   l_req_gl_amount            enc_tbl_number;
   l_req_qty_bill_del         enc_tbl_number;
   l_rate_table               enc_tbl_number;
   l_price_table              enc_tbl_number;
   l_qty_ordered_table        enc_tbl_number;
   l_req_price_table          enc_tbl_number;
   l_req_encumbrance_flag     VARCHAR2 (1);
   l_purch_encumbrance_flag   VARCHAR2 (1);
   l_remainder_qty            NUMBER;
   l_bill_del_amount          NUMBER;
   l_req_bill_del_amount      NUMBER;
   l_qty_bill_del             NUMBER;
   l_timestamp                DATE;
   l_eff_quantity             NUMBER;
   l_rate                     NUMBER;
   l_price                    NUMBER;
   l_ordered_quantity         NUMBER;
   l_tax                      NUMBER;
   l_amount                   NUMBER;
   l_precision                fnd_currencies.precision%TYPE;
   l_min_acc_unit             fnd_currencies.minimum_accountable_unit%TYPE;
   l_approved_flag            po_line_locations_all.approved_flag%TYPE;
   i                          NUMBER;
   j                          NUMBER;
   k                          NUMBER;
BEGIN
   SELECT 'Y' INTO l_delete_act_hist FROM DUAL;

   SELECT 162 INTO x_organization_id FROM DUAL;

   SELECT '1940610' INTO x_po_number FROM DUAL;

   dbms_output.put_line('Executing for PO# '||x_po_number);
   
   x_progress := '010: start';

   BEGIN
      SELECT 'Y'
        INTO x_open_notif_exist
        FROM DUAL
       WHERE EXISTS
                (SELECT 'open notifications'
                   FROM wf_item_activity_statuses wias,
                        wf_notifications wfn,
                        po_headers_all poh
                  WHERE     wias.notification_id IS NOT NULL
                        AND wias.notification_id = wfn.GROUP_ID
                        AND wfn.status = 'OPEN'
                        AND wias.item_type = 'POAPPRV'
                        AND wias.item_key = poh.wf_item_key
                        AND NVL (poh.org_id, -99) =
                               NVL (x_organization_id, -99)
                        AND poh.segment1 = x_po_number
                        AND poh.authorization_status IN ('IN PROCESS',
                                                         'PRE-APPROVED'));
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;
   END;

   x_progress := '020: selected open notif';

   IF (x_open_notif_exist = 'Y')
   THEN
      DBMS_OUTPUT.put_line ('  ');
      DBMS_OUTPUT.put_line (
         'An Open notification exists for this document, you may want to use the notification to process this document. Do not commit if you wish to use the notification');
   END IF;

   BEGIN
      SELECT 'Y'
        INTO l_change_req_exists
        FROM DUAL
       WHERE EXISTS
                (SELECT 'po with change request'
                   FROM po_headers_all h
                  WHERE     h.segment1 = x_po_number
                        AND NVL (h.org_id, -99) =
                               NVL (x_organization_id, -99)
                        AND h.change_requested_by IN ('REQUESTER', 'SUPPLIER'));
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;
   END;

   IF (l_change_req_exists = 'Y')
   THEN
      DBMS_OUTPUT.put_line ('  ');
      DBMS_OUTPUT.put_line (
         'ATTENTION !!! There is an open change request against this PO. You should respond to the notification for the same.');
      RETURN;
   --   dbms_output.put_line('If you are running this script unaware of the change request, Please ROLLBACK');
   END IF;

   OPEN potoreset (x_po_number, x_organization_id);

   FETCH potoreset INTO pos;

   IF potoreset%NOTFOUND
   THEN
      DBMS_OUTPUT.put_line (
            'No PO with PO Number '
         || x_po_number
         || ' exists in org '
         || TO_CHAR (x_organization_id)
         || ' which requires to be reset');
      RETURN;
   END IF;

   CLOSE potoreset;

   x_progress := '030 checking enc action ';

   -- If there exists any open shipment with one of its distributions reserved, then
   -- 1. For a Standard PO, check whether the present Encumbrance amount on the distribution
   --    is correct or not. If its not correct do not reset the document.
   -- 2. For a Blanket PO (irrespective of Encumbrance enabled or not), reset the document.
   -- 3. For a Planned PO, always do not reset the document.
   disallow_script := 'N';

   BEGIN
      SELECT 'Y'
        INTO disallow_script
        FROM DUAL
       WHERE EXISTS
                (SELECT 'Wrong Encumbrance Amount'
                   FROM po_headers_all h,
                        po_lines_all l,
                        po_line_locations_all s,
                        po_distributions_all d
                  WHERE     s.line_location_id = d.line_location_id
                        AND l.po_line_id = s.po_line_id
                        AND h.po_header_id = d.po_header_id
                        AND d.po_header_id = pos.po_header_id
                        AND l.matching_basis = 'QUANTITY'
                        AND NVL (d.encumbered_flag, 'N') = 'Y'
                        AND NVL (s.cancel_flag, 'N') = 'N'
                        AND NVL (s.closed_code, 'OPEN') <> 'FINALLY CLOSED'
                        AND NVL (d.prevent_encumbrance_flag, 'N') = 'N'
                        AND d.budget_account_id IS NOT NULL
                        AND NVL (s.shipment_type, 'BLANKET') = 'STANDARD'
                        AND (ROUND (NVL (d.encumbered_amount, 0), 2) <>
                                ROUND (
                                   (    s.price_override
                                      * d.quantity_ordered
                                      * NVL (d.rate, 1)
                                    +   NVL (d.nonrecoverable_tax, 0)
                                      * NVL (d.rate, 1)),
                                   2))
                 UNION
                 SELECT 'Wrong Encumbrance Amount'
                   FROM po_headers_all h,
                        po_lines_all l,
                        po_line_locations_all s,
                        po_distributions_all d
                  WHERE     s.line_location_id = d.line_location_id
                        AND l.po_line_id = s.po_line_id
                        AND h.po_header_id = d.po_header_id
                        AND d.po_header_id = pos.po_header_id
                        AND l.matching_basis = 'AMOUNT'
                        AND NVL (d.encumbered_flag, 'N') = 'Y'
                        AND NVL (s.cancel_flag, 'N') = 'N'
                        AND NVL (s.closed_code, 'OPEN') <> 'FINALLY CLOSED'
                        AND NVL (d.prevent_encumbrance_flag, 'N') = 'N'
                        AND d.budget_account_id IS NOT NULL
                        AND NVL (s.shipment_type, 'BLANKET') = 'STANDARD'
                        AND (ROUND (NVL (d.encumbered_amount, 0), 2) <>
                                ROUND (
                                     (  d.amount_ordered
                                      + NVL (d.nonrecoverable_tax, 0))
                                   * NVL (d.rate, 1),
                                   2))
                 UNION
                 SELECT 'Wrong Encumbrance Amount'
                   FROM po_headers_all h,
                        po_lines_all l,
                        po_line_locations_all s,
                        po_distributions_all d
                  WHERE     s.line_location_id = d.line_location_id
                        AND l.po_line_id = s.po_line_id
                        AND h.po_header_id = d.po_header_id
                        AND d.po_header_id = pos.po_header_id
                        AND NVL (d.encumbered_flag, 'N') = 'Y'
                        AND NVL (d.prevent_encumbrance_flag, 'N') = 'N'
                        AND d.budget_account_id IS NOT NULL
                        AND NVL (s.shipment_type, 'BLANKET') = 'PLANNED');
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         NULL;
   END;

   IF disallow_script = 'Y'
   THEN
      DBMS_OUTPUT.put_line (
         'This PO has at least one distribution with wrong Encumbrance amount.');
      DBMS_OUTPUT.put_line ('Hence this PO can not be reset.');
      RETURN;
   END IF;

   DBMS_OUTPUT.put_line (
      'Processing ' || pos.type_lookup_code || ' PO Number: ' || pos.segment1);
   DBMS_OUTPUT.put_line ('......................................');

   BEGIN
      SELECT 'Y'
        INTO x_active_wf_exists
        FROM wf_items wfi
       WHERE     wfi.item_type = pos.wf_item_type
             AND wfi.item_key = pos.wf_item_key
             AND wfi.end_date IS NULL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         x_active_wf_exists := 'N';
   END;

   IF (x_active_wf_exists = 'Y')
   THEN
      DBMS_OUTPUT.put_line ('Aborting Workflow...');

      OPEN wfstoabort (pos.wf_item_type, pos.wf_item_key);

      LOOP
         FETCH wfstoabort INTO wf_rec;

         IF wfstoabort%NOTFOUND
         THEN
            CLOSE wfstoabort;

            EXIT;
         END IF;

         IF (wf_rec.end_date IS NULL)
         THEN
            BEGIN
               APPS.WF_Engine.AbortProcess (wf_rec.item_type,
                                            wf_rec.item_key);
            EXCEPTION
               WHEN OTHERS
               THEN
                  DBMS_OUTPUT.put_line (
                        ' workflow not aborted :'
                     || wf_rec.item_type
                     || '-'
                     || wf_rec.item_key);
            END;
         END IF;
      END LOOP;
   END IF;

   DBMS_OUTPUT.put_line ('Updating PO Status..');

   UPDATE po_headers_all
      SET authorization_status =
             DECODE (pos.approved_date,
                     NULL, 'INCOMPLETE',
                     'REQUIRES REAPPROVAL'),
          wf_item_type = NULL,
          wf_item_key = NULL,
          approved_flag = DECODE (pos.approved_date, NULL, 'N', 'R')
    WHERE po_header_id = pos.po_header_id;

   OPEN maxseq (pos.po_header_id, pos.type_lookup_code);

   FETCH maxseq INTO nullseq;

   CLOSE maxseq;

   OPEN poaction (pos.po_header_id, pos.type_lookup_code);

   FETCH poaction INTO submitseq;

   CLOSE poaction;

   IF nullseq > submitseq
   THEN
      IF NVL (l_delete_act_hist, 'N') = 'N'
      THEN
         UPDATE po_action_history
            SET action_code = 'NO ACTION',
                action_date = TRUNC (SYSDATE),
                note =
                   'updated by reset script on ' || TO_CHAR (TRUNC (SYSDATE))
          WHERE     object_id = pos.po_header_id
                AND object_type_code =
                       DECODE (pos.type_lookup_code,
                               'STANDARD', 'PO',
                               'PLANNED', 'PO', --future plan to enhance for planned PO
                               'PA')
                AND object_sub_type_code = pos.type_lookup_code
                AND sequence_num = nullseq
                AND action_code IS NULL;
      ELSE
         DELETE po_action_history
          WHERE     object_id = pos.po_header_id
                AND object_type_code =
                       DECODE (pos.type_lookup_code,
                               'STANDARD', 'PO',
                               'PLANNED', 'PO', --future plan to enhance for planned PO
                               'PA')
                AND object_sub_type_code = pos.type_lookup_code
                AND sequence_num >= submitseq
                AND sequence_num <= nullseq;
      END IF;
   END IF;

   DBMS_OUTPUT.put_line ('Done Approval Processing.');

   SELECT NVL (purch_encumbrance_flag, 'N')
     INTO l_purch_encumbrance_flag
     FROM financials_system_params_all fspa
    WHERE NVL (fspa.org_id, -99) = NVL (x_organization_id, -99);

   IF (l_purch_encumbrance_flag = 'N') -- bug 5015493 : Need to allow reset for blankets also
      OR (pos.type_lookup_code = 'BLANKET')
   THEN
      IF (pos.type_lookup_code = 'BLANKET')
      THEN
         DBMS_OUTPUT.put_line ('document reset successfully');
         DBMS_OUTPUT.put_line (
            'If you are using Blanket encumbrance, Please ROLLBACK, else COMMIT');
      ELSE
         DBMS_OUTPUT.put_line ('document reset successfully');
         DBMS_OUTPUT.put_line ('please COMMIT data');
		 COMMIT;
      END IF;

      RETURN;
   END IF;

   -- reserve action history stuff
   -- check the action history and delete any reserve to submit actions if all the distributions
   -- are now unencumbered, this should happen only if we are deleting the action history

   IF l_delete_act_hist = 'Y'
   THEN
      -- first get the last sequence and action code from action history
      BEGIN
         SELECT sequence_num, action_code
           INTO l_res_seq, l_res_act
           FROM po_action_history pah
          WHERE     pah.object_id = pos.po_header_id
                AND pah.object_type_code =
                       DECODE (pos.type_lookup_code,
                               'STANDARD', 'PO',
                               'PLANNED', 'PO', --future plan to enhance for planned PO
                               'PA')
                AND pah.object_sub_type_code = pos.type_lookup_code
                AND sequence_num IN (SELECT MAX (sequence_num)
                                       FROM po_action_history pah1
                                      WHERE     pah1.object_id =
                                                   pah.object_id
                                            AND pah1.object_type_code =
                                                   pah.object_type_code
                                            AND pah1.object_sub_type_code =
                                                   pah.object_sub_type_code);
      EXCEPTION
         WHEN TOO_MANY_ROWS
         THEN
            DBMS_OUTPUT.put_line (
               'action history needs to be corrected separately ');
         WHEN NO_DATA_FOUND
         THEN
            NULL;
      END;

      -- now if the last action is reserve get the last submit action sequence

      IF (l_res_act = 'RESERVE')
      THEN
         BEGIN
            SELECT MAX (sequence_num)
              INTO l_sub_res_seq
              FROM po_action_history pah
             WHERE     action_code = 'SUBMIT'
                   AND pah.object_id = pos.po_header_id
                   AND pah.object_type_code =
                          DECODE (pos.type_lookup_code,
                                  'STANDARD', 'PO',
                                  'PLANNED', 'PO', --future plan to enhance for planned PO
                                  'PA')
                   AND pah.object_sub_type_code = pos.type_lookup_code;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               NULL;
         END;

         -- check if we need to delete the action history, ie. if all the distbributions
         -- are unreserved

         IF ( (l_sub_res_seq IS NOT NULL) AND (l_res_seq > l_sub_res_seq))
         THEN
            BEGIN
               SELECT 'Y'
                 INTO l_del_res_hist
                 FROM DUAL
                WHERE NOT EXISTS
                         (SELECT 'encumbered dist'
                            FROM po_distributions_all pod
                           WHERE     pod.po_header_id = pos.po_header_id
                                 AND NVL (pod.encumbered_flag, 'N') = 'Y'
                                 AND NVL (pod.prevent_encumbrance_flag, 'N') =
                                        'N');
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_del_res_hist := 'N';
            END;

            IF l_del_res_hist = 'Y'
            THEN
               DBMS_OUTPUT.put_line (
                  'deleting reservation action history ... ');

               DELETE po_action_history pah
                WHERE     pah.object_id = pos.po_header_id
                      AND pah.object_type_code =
                             DECODE (pos.type_lookup_code,
                                     'STANDARD', 'PO',
                                     'PLANNED', 'PO', --future plan to enhance for planned PO
                                     'PA')
                      AND pah.object_sub_type_code = pos.type_lookup_code
                      AND sequence_num >= l_sub_res_seq
                      AND sequence_num <= l_res_seq;
            END IF;
         END IF;                                  -- l_res_seq > l_sub_res_seq
      END IF;
   END IF;

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line (
            'some exception occured '
         || SQLERRM
         || ' rolling back'
         || x_progress);
      ROLLBACK;
END;
/
