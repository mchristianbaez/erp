   /*****************************************************************************************************************************************
   -- File Name: MTL_SYSTEM_ITEMS_INTERFACE_BKP_DROP.sql
   --
   -- PROGRAM TYPE: SQL Script to purge MTL_SYSTEM_ITEMS_INTERFACE 

   -- TMS#20160405-00170  - Dropping Backup Tables
   -- Created by : P.Vamshidhar - 05-Apr-2016
   ******************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

DROP TABLE XXWC.XXWC_MTL_SYSTEM_ITEMS_INTF_BKP;
/
DROP TABLE XXWC.XXWC_MTL_INTERFACE_ERRORS_BKP
/