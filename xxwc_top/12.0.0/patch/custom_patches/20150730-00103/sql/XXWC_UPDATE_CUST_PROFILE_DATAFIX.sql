/********************************************************************************
FILE NAME: XXWC_UPDATE_CUST_PROFILE_DATAFIX.sql
PROGRAM TYPE: sql

PURPOSE: to update org id 162 in XXWC_UPDATE_CUST_PROFILE_TBL 

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     08/04/2015   Maharajan Shunmugam    TMS#20150730-00103Initial version.
********************************************************************************/
UPDATE XXWC.XXWC_UPDATE_CUST_PROFILE_TBL 
SET ORG_ID = 162;
/
COMMIT;
/