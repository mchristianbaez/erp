--Report Name            : WC Tax Exemption Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_896489_FJXDXN_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_896489_FJXDXN_V
xxeis.eis_rsc_ins.v( 'XXEIS_896489_FJXDXN_V',222,'Paste SQL View for Tax Exemption','1.0','','','LA023190','APPS','Tax Exemption View','X8FV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_896489_FJXDXN_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_896489_FJXDXN_V',222,FALSE);
--Inserting Object Columns for XXEIS_896489_FJXDXN_V
xxeis.eis_rsc_ins.vc( 'XXEIS_896489_FJXDXN_V','CUST_ACCT_ID',222,'','','','','','LA023190','NUMBER','','','Cust Acct Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_896489_FJXDXN_V','ACCOUNT_NUMBER',222,'','','','','','LA023190','VARCHAR2','','','Account Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_896489_FJXDXN_V','ACCOUNT_NAME',222,'','','','','','LA023190','VARCHAR2','','','Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_896489_FJXDXN_V','SITE_NUMBER',222,'','','','','','LA023190','VARCHAR2','','','Site Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_896489_FJXDXN_V','SITE_ADDRESS',222,'','','','','','LA023190','VARCHAR2','','','Site Address','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_896489_FJXDXN_V','SITE_CITY',222,'','','','','','LA023190','VARCHAR2','','','Site City','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_896489_FJXDXN_V','SITE_STATE',222,'','','','','','LA023190','VARCHAR2','','','Site State','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_896489_FJXDXN_V','POSTAL_CODE',222,'','','','','','LA023190','VARCHAR2','','','Postal Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_896489_FJXDXN_V','TAX_EXEMPTION',222,'','','','','','LA023190','VARCHAR2','','','Tax Exemption','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_896489_FJXDXN_V','TAX_EXEMPTION_TYPE',222,'','','','','','LA023190','VARCHAR2','','','Tax Exemption Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_896489_FJXDXN_V','SITE_USE_CODE',222,'','','','','','LA023190','VARCHAR2','','','Site Use Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_896489_FJXDXN_V','PRIMARY_FLAG',222,'','','','','','LA023190','VARCHAR2','','','Primary Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_896489_FJXDXN_V','STATUS',222,'','','','','','LA023190','VARCHAR2','','','Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_896489_FJXDXN_V','LOCATION',222,'','','','','','LA023190','VARCHAR2','','','Location','','','','US');
--Inserting Object Components for XXEIS_896489_FJXDXN_V
--Inserting Object Component Joins for XXEIS_896489_FJXDXN_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report LOV Data for WC Tax Exemption Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - WC Tax Exemption Report
xxeis.eis_rsc_ins.lov( 222,'select distinct party_name from hz_parties p, hz_cust_accounts c
    where p.party_id = c.party_id','null','Customer Name','Displays List of Values for Customer Name ','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select account_number from hz_cust_accounts','null','Customer Number','Displays List of Values for Customer Number','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select distinct state from hz_locations','','Customer State','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select distinct status from hz_cust_accounts','','Customer Status','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report Data for WC Tax Exemption Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC Tax Exemption Report
xxeis.eis_rsc_utility.delete_report_rows( 'WC Tax Exemption Report' );
--Inserting Report - WC Tax Exemption Report
xxeis.eis_rsc_ins.r( 222,'WC Tax Exemption Report','','','','','','LA023190','XXEIS_896489_FJXDXN_V','Y','','select 
hca.cust_account_id Cust_Acct_ID,
hca.account_number Account_Number,
hca.account_name Account_Name,
hps.party_site_number Site_Number,
hcasa.status,
hl.address1 Site_Address,
hl.city Site_City,
hl.state Site_State,
hl.postal_code,
hcasa.attribute16 Tax_Exemption,
hcasa.attribute15 Tax_Exemption_Type,
hcsua.site_use_code,
hcsua.location,
hcsua.primary_flag
from 
ar.hz_cust_site_uses_all hcsua,
ar.hz_cust_acct_sites_all hcasa,
ar.hz_party_sites hps,
ar.hz_locations hl,
ar.hz_cust_accounts hca
where hcasa.cust_acct_site_id = hcsua.cust_acct_site_id
and hca.cust_account_id = hcasa.cust_account_id
and hcasa.party_site_id = hps.party_site_id
and hps.location_id = hl.location_id
','LA023190','','N','White Cap Tax Reporting','','CSV,EXCEL,','N','','','','','','','APPS','US','','','','');
--Inserting Report Columns - WC Tax Exemption Report
xxeis.eis_rsc_ins.rc( 'WC Tax Exemption Report',222,'STATUS','Status','','','','default','','8','N','','','','','','','','LA023190','N','N','','XXEIS_896489_FJXDXN_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Tax Exemption Report',222,'SITE_CITY','Site City','','','','default','','1','N','','','','','','','','LA023190','N','N','','XXEIS_896489_FJXDXN_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Tax Exemption Report',222,'ACCOUNT_NAME','Account Name','','','','default','','2','N','','','','','','','','LA023190','N','N','','XXEIS_896489_FJXDXN_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Tax Exemption Report',222,'TAX_EXEMPTION_TYPE','Tax Exemption Type','','','','default','','3','N','','','','','','','','LA023190','N','N','','XXEIS_896489_FJXDXN_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Tax Exemption Report',222,'POSTAL_CODE','Postal Code','','','','default','','4','N','','','','','','','','LA023190','N','N','','XXEIS_896489_FJXDXN_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Tax Exemption Report',222,'ACCOUNT_NUMBER','Account Number','','','','default','','5','N','','','','','','','','LA023190','N','N','','XXEIS_896489_FJXDXN_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Tax Exemption Report',222,'SITE_ADDRESS','Site Address','','','','default','','13','N','','','','','','','','LA023190','N','N','','XXEIS_896489_FJXDXN_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Tax Exemption Report',222,'SITE_NUMBER','Site Number','','','','default','','7','N','','','','','','','','LA023190','N','N','','XXEIS_896489_FJXDXN_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Tax Exemption Report',222,'TAX_EXEMPTION','Tax Exemption','','','','default','','9','N','','','','','','','','LA023190','N','N','','XXEIS_896489_FJXDXN_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Tax Exemption Report',222,'PRIMARY_FLAG','Primary Flag','','','','default','','10','N','','','','','','','','LA023190','N','N','','XXEIS_896489_FJXDXN_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Tax Exemption Report',222,'CUST_ACCT_ID','Cust Acct Id','','','~T~D~0','default','','11','N','','','','','','','','LA023190','N','N','','XXEIS_896489_FJXDXN_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC Tax Exemption Report',222,'SITE_STATE','Site State','','','','default','','12','N','','','','','','','','LA023190','N','N','','XXEIS_896489_FJXDXN_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Tax Exemption Report',222,'SITE_USE_CODE','Site Use Code','','','','default','','6','N','','','','','','','','LA023190','N','N','','XXEIS_896489_FJXDXN_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC Tax Exemption Report',222,'LOCATION','Location','','','','','','14','N','','','','','','','','LA023190','N','N','','XXEIS_896489_FJXDXN_V','','','GROUP_BY','US','');
--Inserting Report Parameters - WC Tax Exemption Report
xxeis.eis_rsc_ins.rp( 'WC Tax Exemption Report',222,'Status','','STATUS','IN','Customer Status','','VARCHAR2','N','Y','11','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_896489_FJXDXN_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Tax Exemption Report',222,'Account Name','','ACCOUNT_NAME','IN','Customer Name','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_896489_FJXDXN_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Tax Exemption Report',222,'Account Number','','ACCOUNT_NUMBER','IN','Customer Number','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_896489_FJXDXN_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Tax Exemption Report',222,'Primary Flag','','PRIMARY_FLAG','IN','','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_896489_FJXDXN_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Tax Exemption Report',222,'Site Address','','SITE_ADDRESS','IN','','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_896489_FJXDXN_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Tax Exemption Report',222,'Site City','','SITE_CITY','IN','','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_896489_FJXDXN_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Tax Exemption Report',222,'Site Number','','SITE_NUMBER','IN','','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_896489_FJXDXN_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Tax Exemption Report',222,'Site State','','SITE_STATE','IN','Customer State','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_896489_FJXDXN_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Tax Exemption Report',222,'Site Use Code','','SITE_USE_CODE','IN','','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_896489_FJXDXN_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Tax Exemption Report',222,'Tax Exemption','','TAX_EXEMPTION','IN','','','VARCHAR2','N','Y','9','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_896489_FJXDXN_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Tax Exemption Report',222,'TaxExemption Type','','TAX_EXEMPTION_TYPE','IN','','','VARCHAR2','N','Y','10','Y','Y','CONSTANT','LA023190','Y','N','','','','XXEIS_896489_FJXDXN_V','','','US','');
--Inserting Dependent Parameters - WC Tax Exemption Report
--Inserting Report Conditions - WC Tax Exemption Report
xxeis.eis_rsc_ins.rcnh( 'WC Tax Exemption Report',222,'ACCOUNT_NAME IN :Account Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ACCOUNT_NAME','','Account Name','','','','','XXEIS_896489_FJXDXN_V','','','','','','IN','Y','Y','','','','','1',222,'WC Tax Exemption Report','ACCOUNT_NAME IN :Account Name ');
xxeis.eis_rsc_ins.rcnh( 'WC Tax Exemption Report',222,'ACCOUNT_NUMBER IN :Account Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ACCOUNT_NUMBER','','Account Number','','','','','XXEIS_896489_FJXDXN_V','','','','','','IN','Y','Y','','','','','1',222,'WC Tax Exemption Report','ACCOUNT_NUMBER IN :Account Number ');
xxeis.eis_rsc_ins.rcnh( 'WC Tax Exemption Report',222,'PRIMARY_FLAG IN :Primary Flag ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PRIMARY_FLAG','','Primary Flag','','','','','XXEIS_896489_FJXDXN_V','','','','','','IN','Y','Y','','','','','1',222,'WC Tax Exemption Report','PRIMARY_FLAG IN :Primary Flag ');
xxeis.eis_rsc_ins.rcnh( 'WC Tax Exemption Report',222,'SITE_ADDRESS IN :Site Address ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SITE_ADDRESS','','Site Address','','','','','XXEIS_896489_FJXDXN_V','','','','','','IN','Y','Y','','','','','1',222,'WC Tax Exemption Report','SITE_ADDRESS IN :Site Address ');
xxeis.eis_rsc_ins.rcnh( 'WC Tax Exemption Report',222,'SITE_CITY IN :Site City ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SITE_CITY','','Site City','','','','','XXEIS_896489_FJXDXN_V','','','','','','IN','Y','Y','','','','','1',222,'WC Tax Exemption Report','SITE_CITY IN :Site City ');
xxeis.eis_rsc_ins.rcnh( 'WC Tax Exemption Report',222,'SITE_NUMBER IN :Site Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SITE_NUMBER','','Site Number','','','','','XXEIS_896489_FJXDXN_V','','','','','','IN','Y','Y','','','','','1',222,'WC Tax Exemption Report','SITE_NUMBER IN :Site Number ');
xxeis.eis_rsc_ins.rcnh( 'WC Tax Exemption Report',222,'SITE_STATE IN :Site State ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SITE_STATE','','Site State','','','','','XXEIS_896489_FJXDXN_V','','','','','','IN','Y','Y','','','','','1',222,'WC Tax Exemption Report','SITE_STATE IN :Site State ');
xxeis.eis_rsc_ins.rcnh( 'WC Tax Exemption Report',222,'SITE_USE_CODE IN :Site Use Code ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SITE_USE_CODE','','Site Use Code','','','','','XXEIS_896489_FJXDXN_V','','','','','','IN','Y','Y','','','','','1',222,'WC Tax Exemption Report','SITE_USE_CODE IN :Site Use Code ');
xxeis.eis_rsc_ins.rcnh( 'WC Tax Exemption Report',222,'STATUS IN :Status ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','STATUS','','Status','','','','','XXEIS_896489_FJXDXN_V','','','','','','IN','Y','Y','','','','','1',222,'WC Tax Exemption Report','STATUS IN :Status ');
xxeis.eis_rsc_ins.rcnh( 'WC Tax Exemption Report',222,'TAX_EXEMPTION IN :Tax Exemption ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TAX_EXEMPTION','','Tax Exemption','','','','','XXEIS_896489_FJXDXN_V','','','','','','IN','Y','Y','','','','','1',222,'WC Tax Exemption Report','TAX_EXEMPTION IN :Tax Exemption ');
xxeis.eis_rsc_ins.rcnh( 'WC Tax Exemption Report',222,'X8FV.TAX_EXEMPTION_TYPE IN :TaxExemption Type ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TAX_EXEMPTION_TYPE','','TaxExemption Type','','','','','XXEIS_896489_FJXDXN_V','','','','','','IN','Y','Y','','','','','1',222,'WC Tax Exemption Report','X8FV.TAX_EXEMPTION_TYPE IN :TaxExemption Type ');
--Inserting Report Sorts - WC Tax Exemption Report
--Inserting Report Triggers - WC Tax Exemption Report
--inserting report templates - WC Tax Exemption Report
--Inserting Report Portals - WC Tax Exemption Report
--inserting report dashboards - WC Tax Exemption Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC Tax Exemption Report','222','XXEIS_896489_FJXDXN_V','XXEIS_896489_FJXDXN_V','N','');
--inserting report security - WC Tax Exemption Report
xxeis.eis_rsc_ins.rsec( 'WC Tax Exemption Report','222','','XXWC_CRE_CREDIT_COLL_MGR_NOREC',222,'LA023190','','','');
xxeis.eis_rsc_ins.rsec( 'WC Tax Exemption Report','222','','XXWC_CRE_CREDIT_COLL_MGR',222,'LA023190','','','');
xxeis.eis_rsc_ins.rsec( 'WC Tax Exemption Report','222','','XXWC_CRE_ASSOC_CUST_MAINT',222,'LA023190','','','');
xxeis.eis_rsc_ins.rsec( 'WC Tax Exemption Report','222','','XXWC_CRE_ASSOC_COLLECTIONS',222,'LA023190','','','');
xxeis.eis_rsc_ins.rsec( 'WC Tax Exemption Report','222','','XXWC_CRE_ASSOC_CASH_APP',222,'LA023190','','','');
xxeis.eis_rsc_ins.rsec( 'WC Tax Exemption Report','222','','XXWC_CRE_ASSOC_CASH_APP_MGR',222,'LA023190','','','');
xxeis.eis_rsc_ins.rsec( 'WC Tax Exemption Report','','AB063501','',222,'LA023190','','N','');
--Inserting Report Pivots - WC Tax Exemption Report
--Inserting Report   Version details- WC Tax Exemption Report
xxeis.eis_rsc_ins.rv( 'WC Tax Exemption Report','','WC Tax Exemption Report','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
