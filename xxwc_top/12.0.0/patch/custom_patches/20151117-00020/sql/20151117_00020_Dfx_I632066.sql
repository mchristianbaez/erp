DECLARE
l_line_id    NUMBER :=53943231;
l_org_id     NUMBER;
l_count      NUMBER;
l_result     VARCHAR2(30);
l_file_val   VARCHAR2(60);

BEGIN

   oe_debug_pub.debug_on; 
   oe_debug_pub.initialize; 
   l_file_val := OE_DEBUG_PUB.Set_Debug_Mode('FILE'); 
   BEGIN
   SELECT ORG_ID
   INTO   l_org_id
   FROM   OE_ORDER_LINES_ALL
   WHERE  LINE_ID = l_line_id;
   EXCEPTION
   WHEN OTHERS THEN
   FND_FILE.PUT_LINE (FND_FILE.LOG,'Failed to fetch the org_id for given line id:'||l_line_id);
   END;
   BEGIN
   DELETE FROM oe_line_sets WHERE line_id = l_line_id;
   EXCEPTION
   WHEN OTHERS THEN
   FND_FILE.PUT_LINE (FND_FILE.LOG,'Failed to delete the record for given line id:'||l_line_id);
   END;
   COMMIT;
   
   BEGIN
   UPDATE OE_ORDER_LINES_ALL
   SET    INVOICE_INTERFACE_STATUS_CODE=NULL,
              fulfilled_quantity      = ordered_quantity,
              fulfilled_flag          = 'Y',
              actual_fulfillment_date = SYSDATE,
              flow_status_code        = 'FULFILLED'
   WHERE  LINE_ID = l_line_id;
   EXCEPTION
   WHEN OTHERS THEN
   FND_FILE.PUT_LINE (FND_FILE.LOG,'Failed to update the values for given line id:'||l_line_id);
   END;
  COMMIT;
  
   fnd_client_info.set_org_context(to_char(l_org_id));
  FND_FILE.PUT_LINE (FND_FILE.LOG,'Output Debug File is stored at : '||OE_DEBUG_PUB.G_DIR||'/'||OE_DEBUG_PUB.G_FILE);
   OE_DEBUG_PUB.SETDEBUGLEVEL(5);
   WF_ENGINE.HANDLEERROR('OEOL', to_char(l_line_id), 'INVOICE_INTERFACE', 'RETRY', NULL);

END;
/