CREATE OR REPLACE FORCE VIEW "XXEIS"."EIS_XXWC_INV_TIME_MGMT_V" ("INV_ORG_NAME", "ORGANIZATION_ID", "ORGANIZATION_CODE", "BUYER", "MASTER_ORG_CODE", "SUBINVENTORY_CODE", "LOCATOR_ID", "LOCATOR", "INVENTORY_ITEM_ID", "ITEM", "ITEM_DESCRIPTION", "REVISION", "UNIT_OF_MEASURE", "ON_HAND", "PRIMARY_BIN_LOC", "ALTERNATE_BIN_LOC", "OLDEST_BORN_DATE", "SHELF_LIFE_DAYS", "VENDOR_NAME", "VENDOR_NUMBER", "UNPACKED", "PACKED", "COST_GROUP_ID", "LOT_NUMBER", "SUBINVENTORY_STATUS_ID", "LOCATOR_STATUS_ID", "LOT_STATUS_ID", "PLANNING_TP_TYPE", "PLANNING_ORGANIZATION_ID", "PLANNING_ORGANIZATION", "OWNING_TP_TYPE", "OWNING_ORGANIZATION_ID", "OWNING_ORGANIZATION", "LOT_EXPIRY_DATE", "LPN", "LPN_ID", "STATUS_CODE", "UNIT_NUMBER", "SUBINVENTORY_TYPE", "SECONDARY_INVENTORY_NAME", "DATE_RECEIVED", "ONHAND_QUANTITIES_ID", "INVENTORY_LOCATION_ID", "LANGUAGE", "MIN_MINMAX_QUANTITY", "MAX_MINMAX_QUANTITY", "LIST_PRICE_PER_UNIT", "MP_ORGANIZATION_ID", "MIL_ORGANIZATION_ID", "MSI_ORGANIZATION_ID", "MSIV_INVENTORY_ITEM_ID", "MSIV_ORGANIZATION_ID", "MMS_STATUS_ID", "OOD_ORGANIZATION_ID", "OODW_ORGANIZATION_ID", "OODP_ORGANIZATION_ID", "HAOU_ORGANIZATION_ID", "MLN_INVENTORY_ITEM_ID", "MLN_ORGANIZATION_ID", "MLN_LOT_NUMBER", "REGION", "DAYS_AGED", "AT_RISK", "AVGCOST")
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_INV_TIME_MGMT_V.vw $
  Module Name: Inventory
  PURPOSE: Time Sensitive Mgmt WC
  TMS Task Id :
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        NA    				NA            	Initial Version
  1.1  		07/15/2014 	 Mahender Reddy         TMS#20150930-00054
  
  **************************************************************************************************************/
 AS 
  SELECT haou.name inv_org_name,
          moq.organization_id organization_id,
          mp.organization_code organization_code,
          ppf.full_name buyer,
          ood.organization_code master_org_code,
          moq.subinventory_code subinventory_code,
          moq.locator_id locator_id,
          mil.concatenated_segments LOCATOR,
          moq.inventory_item_id inventory_item_id,
          msiv.concatenated_segments item,
          MSIV.description item_description,
          moq.revision revision,
          muom.unit_of_measure,
          moq.primary_transaction_quantity on_hand,
          xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc (
             msiv.inventory_item_id,
             msiv.organization_id)
             primary_bin_loc,
          XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ALTERNATE_BIN_LOC (
             MSIv.INVENTORY_ITEM_ID,
             MSIv.ORGANIZATION_ID)
             ALTERNATE_BIN_LOC,
          moq.orig_date_received oldest_born_date,
          msiv.shelf_life_days shelf_life_days,
          xxeis.eis_po_xxwc_isr_util_qa_pkg.get_vendor_name (
             msiv.inventory_item_id,
             msiv.organization_id)
             vendor_name,
          xxeis.eis_po_xxwc_isr_util_qa_pkg.get_vendor_number (
             msiv.inventory_item_id,
             msiv.organization_id)
             vendor_number,
          DECODE (moq.containerized_flag,
                  1, 0,
                  moq.primary_transaction_quantity)
             unpacked,
          DECODE (moq.containerized_flag,
                  1, moq.primary_transaction_quantity,
                  0)
             packed,
          moq.cost_group_id cost_group_id,
          moq.lot_number lot_number,
          -- TO_CHAR (NULL) serial_number,
          --mil.project_id project_id,
          --mil.task_id task_id,
          msi.status_id subinventory_status_id,
          mil.status_id locator_status_id,
          mln.status_id lot_status_id,
          moq.planning_tp_type planning_tp_type,
          moq.planning_organization_id planning_organization_id,
          oodp.organization_name planning_organization,
          moq.owning_tp_type owning_tp_type,
          moq.owning_organization_id owning_organization_id,
          oodw.organization_name owning_organization,
          mln.expiration_date lot_expiry_date,
          mlv.lpn,
          moq.lpn_id,
          mms.status_code,
          pun.unit_number,
          msi.subinventory_type,
          msi.secondary_inventory_name,
          moq.date_received,
          moq.onhand_quantities_id,
          mil.inventory_location_id,
          muom.language,
          msiv.MIN_MINMAX_QUANTITY,
          msiv.MAX_MINMAX_QUANTITY,
          MSIV.LIST_PRICE_PER_UNIT,
          -- primary keys added for component_joins
          mp.organization_id mp_organization_id,
          mil.organization_id mil_organization_id,
          msi.organization_id msi_organization_id,
          msiv.inventory_item_id msiv_inventory_item_id,
          msiv.organization_id msiv_organization_id,
          mms.status_id mms_status_id,
          ood.organization_id ood_organization_id,
          oodw.organization_id oodw_organization_id,
          OODP.ORGANIZATION_ID OODP_ORGANIZATION_ID,
          HAOU.ORGANIZATION_ID HAOU_ORGANIZATION_ID,
          MLN.INVENTORY_ITEM_ID MLN_INVENTORY_ITEM_ID,
          MLN.ORGANIZATION_ID MLN_ORGANIZATION_ID,
          MLN.LOT_NUMBER MLN_LOT_NUMBER,
          mp.ATTRIBUTE9 REGION,
          ROUND (TRUNC (SYSDATE) - TRUNC (moq.orig_date_received)) Days_Aged,
          CASE
             WHEN (NVL (msiv.shelf_life_days, 0) * 0.75) >
                     ROUND (TRUNC (SYSDATE) - TRUNC (moq.orig_date_received))
             THEN
                'N'
             ELSE
                'Y'
          END
             At_risk,
          NVL (
             apps.cst_cost_api.get_item_cost (1,
                                              msiv.inventory_item_id,
                                              msiv.organization_id),
             0)
             avgcost
     --,msiv.SEGMENT1 item_num
     --descr#flexfield#start
     --descr#flexfield#end
     --kff#start
     --kff#end
     --gl#accountff#start
     --gl#accountff#end
     FROM mtl_parameters mp,
          mtl_item_locations_kfv mil,
          mtl_secondary_inventories msi,
          mtl_lot_numbers mln,
          mtl_system_items_kfv msiv,
          mtl_onhand_quantities_detail moq,
          mtl_units_of_measure_tl muom,
          mtl_onhand_lpn_v mlv,
          mtl_material_statuses_vl mms,
          pjm_unit_numbers pun,
          org_organization_definitions ood,
          org_organization_definitions oodw,
          org_organization_definitions oodp,
          hr_all_organization_units haou,
          -- per_people_f ppf -- Commented for Ver 1.1
		  (select ppf1.person_id person_id,ppf1.full_name full_name
				from per_people_f ppf1
					where ppf1.effective_start_date = 
					(Select MAX(ppf2.effective_start_date) 
					from per_people_f ppf2
						where ppf2.person_id = ppf1.person_id)) ppf -- Added for Ver 1.1
    WHERE     moq.organization_id = mp.organization_id
          AND moq.organization_id = mil.organization_id(+)
          AND moq.locator_id = mil.inventory_location_id(+)
          AND moq.organization_id = msi.organization_id
          AND moq.subinventory_code = msi.secondary_inventory_name
          AND moq.organization_id = mln.organization_id(+)
          AND moq.inventory_item_id = mln.inventory_item_id(+)
          AND moq.lot_number = mln.lot_number(+)
          AND msiv.buyer_id = ppf.person_id(+)
          /*AND ppf.effective_start_date =
                 (SELECT MAX (effective_start_date)
                    FROM per_people_f ppf1
                   WHERE ppf1.person_id = ppf.person_id)*/ -- Commented for Ver 1.1
          AND moq.organization_id = msiv.organization_id
          AND moq.inventory_item_id = msiv.inventory_item_id
          AND muom.uom_code = msiv.primary_uom_code
          AND muom.language = USERENV ('LANG')
          AND mlv.lpn_id(+) = moq.lpn_id
          AND mms.status_id(+) = mil.status_id
          AND pun.master_organization_id(+) = moq.organization_id
          AND mp.master_organization_id = ood.organization_id
          AND moq.planning_organization_id = oodp.organization_id
          AND moq.owning_organization_id = oodw.organization_id
          AND haou.organization_id = msi.organization_id
          AND (   (msiv.shelf_life_days > 0 AND msiv.shelf_life_days < 10000)
               OR moq.lot_number IS NOT NULL)
          AND EXISTS
                 (SELECT 1
                    FROM XXEIS.EIS_ORG_ACCESS_V
                   WHERE organization_id = msi.organization_id)
/