/*************************************************************************
  $Header TMS_20160307-00377 _POD.sql $
  Module Name: TMS_20160307-00377  Sales Order Header stuck in Booked Status

  PURPOSE: Data fix to cancel Requisition 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        07-MAR-2016  Gopi Damuluri         TMS#20160307-00377 

**************************************************************************/ 

SET VERIFY OFF;
WHENEVER SQLERROR EXIT FAILURE ROLLBACK;
WHENEVER OSERROR EXIT FAILURE ROLLBACK;


-- introduced the hint NO_UNNEST in the subquery, for the performance issue #5885900
-- change 1 of 3

SELECT COUNT(*) "Count before"
FROM   WF_ITEM_ACTIVITY_STATUSES WAS,
       WF_PROCESS_ACTIVITIES P,
       OE_ORDER_HEADERS_ALL H
WHERE  TO_NUMBER(WAS.ITEM_KEY) = H.HEADER_ID
AND    WAS.PROCESS_ACTIVITY = P.INSTANCE_ID
AND    P.ACTIVITY_ITEM_TYPE = 'OEOH'
AND    P.ACTIVITY_NAME = 'CLOSE_WAIT_FOR_L'
AND    WAS.ACTIVITY_STATUS = 'NOTIFIED'
AND    WAS.ITEM_TYPE = 'OEOH'
AND    NOT EXISTS ( SELECT /*+ NO_UNNEST */ 1
                    FROM   OE_ORDER_LINES_ALL
                    WHERE  HEADER_ID = TO_NUMBER(WAS.ITEM_KEY)
                    AND    OPEN_FLAG = 'Y');

DECLARE

l_header_id      VARCHAR2(100);
l_org_id         NUMBER;
l_count          NUMBER := 0;
l_cnt            NUMBER := 0;
l_result         VARCHAR2(30);
l_activity_label VARCHAR2(30);

-- introduced the hint NO_UNNEST in the subquery, for the performance issue #5885900
-- change 2 of 3
CURSOR TO_CLOSE IS SELECT P.INSTANCE_LABEL, WAS.ITEM_KEY, H.ORDER_NUMBER, H.ORG_ID
                   FROM   WF_ITEM_ACTIVITY_STATUSES WAS,
                          WF_PROCESS_ACTIVITIES P,
                          OE_ORDER_HEADERS_ALL H
                   WHERE TO_NUMBER(WAS.ITEM_KEY) = H.HEADER_ID
                   AND   WAS.PROCESS_ACTIVITY = P.INSTANCE_ID
                   AND   P.ACTIVITY_ITEM_TYPE = 'OEOH'
                   AND   P.ACTIVITY_NAME = 'CLOSE_WAIT_FOR_L'
                   AND   WAS.ACTIVITY_STATUS = 'NOTIFIED'
                   AND   WAS.ITEM_TYPE = 'OEOH'
				   AND   H.order_number = 18621942  
                   AND   NOT EXISTS ( SELECT /*+ NO_UNNEST */ 1
                                      FROM   OE_ORDER_LINES_ALL
                                      WHERE  HEADER_ID = TO_NUMBER(WAS.ITEM_KEY)
                                      AND    OPEN_FLAG = 'Y');

CURSOR WF_ERRORS(P_ERROR_TYPE VARCHAR2) IS SELECT I.ITEM_KEY, I.ITEM_TYPE
                                           FROM   WF_ITEMS I
                                           WHERE  I.ITEM_TYPE = P_ERROR_TYPE
                                           AND    I.PARENT_ITEM_TYPE = 'OEOH'
                                           AND    I.PARENT_ITEM_KEY = l_header_id
                                           AND    I.END_DATE IS NULL
                                           FOR    UPDATE NOWAIT;

BEGIN

  FOR C IN TO_CLOSE LOOP
      BEGIN
         l_header_id := c.item_key;
         FOR E IN WF_ERRORS('WFERROR') LOOP
             BEGIN
                 WF_ENGINE.ABORTPROCESS(ITEMTYPE =>E.ITEM_TYPE, ITEMKEY=>E.ITEM_KEY);
             EXCEPTION WHEN OTHERS THEN
                       UPDATE WF_ITEMS
                       SET    END_DATE = SYSDATE
                       WHERE  CURRENT OF WF_ERRORS;
             END;
             WF_PURGE.ITEMS(ITEMTYPE =>E.ITEM_TYPE, ITEMKEY=>E.ITEM_KEY, DOCOMMIT=>FALSE, FORCE=>TRUE);
         END LOOP;
         FOR E IN WF_ERRORS('OMERROR') LOOP
             BEGIN
                 WF_ENGINE.ABORTPROCESS(ITEMTYPE =>E.ITEM_TYPE, ITEMKEY=>E.ITEM_KEY);
             EXCEPTION WHEN OTHERS THEN
                 UPDATE WF_ITEMS
                 SET    END_DATE = SYSDATE
                 WHERE  CURRENT OF WF_ERRORS;
             END;
             WF_PURGE.ITEMS(ITEMTYPE =>E.ITEM_TYPE, ITEMKEY=>E.ITEM_KEY, DOCOMMIT=>FALSE, FORCE=>TRUE);
         END LOOP;

      BEGIN
         OE_Standard_WF.OEOH_SELECTOR
         (p_itemtype => 'OEOH'
         ,p_itemkey => c.item_key
         ,p_actid => 12345
         ,p_funcmode => 'SET_CTX'
         ,p_result => l_result
         );
      EXCEPTION WHEN NO_DATA_FOUND THEN
               FND_CLIENT_INFO.SET_ORG_CONTEXT(C.ORG_ID);
               FND_PROFILE.PUT('ORG_ID', TO_CHAR(C.ORG_ID));
      END;
      WF_ENGINE.HANDLEERROR('OEOH', C.ITEM_KEY, C.INSTANCE_LABEL, 'RETRY',NULL);
      l_count := l_count + 1;
      IF mod(l_count, 100) = 0 THEN
         COMMIT;
      END IF;
    EXCEPTION WHEN OTHERS THEN
              NULL;
    END;
    END LOOP;

END;
/

-- introduced the hint NO_UNNEST in the subquery, for the performance issue #5885900
-- change 3 of 3
SELECT COUNT(*) "Count after"
FROM   WF_ITEM_ACTIVITY_STATUSES WAS,
       WF_PROCESS_ACTIVITIES P,
       OE_ORDER_HEADERS_ALL H
WHERE TO_NUMBER(WAS.ITEM_KEY) = H.HEADER_ID
AND WAS.PROCESS_ACTIVITY = P.INSTANCE_ID
AND P.ACTIVITY_ITEM_TYPE = 'OEOH'
AND P.ACTIVITY_NAME = 'CLOSE_WAIT_FOR_L'
AND WAS.ACTIVITY_STATUS = 'NOTIFIED'
AND WAS.ITEM_TYPE = 'OEOH'
AND NOT EXISTS ( SELECT /*+ NO_UNNEST */ 1
                 FROM   OE_ORDER_LINES_ALL
                 WHERE  HEADER_ID = TO_NUMBER(was.item_key)
                 AND    OPEN_FLAG = 'Y');

--prompt If the "Count after" fails to drop to 0 after running this script ontd0060.sql, this may be because of orphan line workflows.
--Prompt We suggest running the script ont00165.sql once and then trying this one again. ont00165.sql is available with patch 17001353:R12.ONT.B.

COMMIT;
EXIT;
