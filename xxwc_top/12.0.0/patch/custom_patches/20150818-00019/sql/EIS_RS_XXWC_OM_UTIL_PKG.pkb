create or replace
PACKAGE BODY       XXEIS.EIS_RS_XXWC_OM_UTIL_PKG
AS
   /*************************************************************************
     $Header EIS_RS_XXWC_OM_UTIL_PKG $
     Module Name: EIS_RS_XXWC_OM_UTIL_PKG.pkb

     PURPOSE:   This package holds the utilities for EIS OrderManagement Reports

     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------    -------------------------
     1.0        03/26/2014  Gopi Damuluri      Initial Version
                                               TMS# 20140326-00106 Performance Tuning of Open SalesOrders Report
	1.1			09/25/2015  Mahender Reddy     TMS#20150818-00019 - Added WC Long Term Rental and WC Short Term Rental							   
   **************************************************************************/

   /********************************************************************************
   ProcedureName : open_sales_order_pre_trig
   Purpose       : Used by "White Cap Sales Order Report"

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     03/05/2014    Harsha Yedla   Initial Creation
   ********************************************************************************/

   PROCEDURE open_sales_order_pre_trig(p_org_id  varchar2, p_salesrep_name  varchar2, p_ordered_date_from  varchar2, p_ordered_date_to  varchar2)
   IS
      v_cnt             NUMBER;
      l_doc_date    DATE;
      V_ORG_ID     varchar2(2000);
      v_val             number;
      v_errbuf          VARCHAR2(2000);

-- Salesrep Variables -- Version# 1.0
      v_salesrep_cnt   NUMBER;
      v_salesrep_name  VARCHAR2(2000);

      CURSOR C1
      IS
         SELECT A.ROWID RID, A.*
           FROM XXEIS.XXWC_GTT_OPEN_ORDERS_TMP A;
   BEGIN
      DBMS_OUTPUT.put_line ('before inserts xxwc_open_sales_order_pre_trig');

-- Version# 1.0 > Start
BEGIN
   SELECT INSTR(p_salesrep_name,',') INTO v_salesrep_cnt FROM dual;

   IF p_salesrep_name   !='All' THEN
     IF v_salesrep_cnt  !=0 then
        v_salesrep_name := p_salesrep_name;
     ELSE
        v_salesrep_name := ''''||p_salesrep_name||'''';
     END IF;

     EXECUTE IMMEDIATE 'INSERT INTO xxeis.xxwc_gtt_salesreps_tbl (salesrep_id)(SELECT salesrep_id FROM ra_salesreps  WHERE org_id = 162 AND name IN ('||v_salesrep_name||'))';

   ELSE
      INSERT INTO xxeis.xxwc_gtt_salesreps_tbl (salesrep_id) (SELECT salesrep_id FROM ra_salesreps  WHERE 1=1 AND org_id = 162);
   END IF;
EXCEPTION
WHEN OTHERS THEN
  fnd_file.put_line (FND_FILE.LOG,'Error while populating XXWC_GTT_SALESREPS_TBL Global Temporary Table' || SQLERRM);
END;
-- Version# 1.0 < End

      --------------------------------------------------------------------------
      -- Insert into staging table XXEIS.XXWC_GTT_OE_ORDER_HEADERS
      --------------------------------------------------------------------------
      --------------added by harsha for performance issue TMS#20130920-00347 -----------

       BEGIN
              INSERT INTO XXEIS.XXWC_GTT_OE_ORDER_HEADERS (HEADER_ID
            ,ORG_ID
            ,ORDER_TYPE_ID
            ,ORDER_NUMBER
            ,ORDERED_DATE
            ,REQUEST_DATE
            ,CREATION_DATE
            ,CREATED_BY
            ,CREATED_BY_NAME
            ,QUOTE_NUMBER
            ,SOLD_TO_ORG_ID
            ,PARTY_ID
            ,CUST_ACCOUNT_ID
            ,CUSTOMER_NUMBER
            ,CUSTOMER_NAME
            ,CUSTOMER_JOB_NAME
            ,CUST_ACCT_SITE_ID
            ,SHIP_TO_CITY
            ,SHIP_TO_STE_ID
            ,SHP_TO_PRT_ID
            ,ZIP_CODE
            ,FLOW_STATUS_CODE
            ,TRANSACTION_PHASE_CODE
            ,PAYMENT_TERM_ID
            ,PAYMENT_TERMS
            ,SALESREP_ID
            ,ORG_SALESREP_ID
            ,SALES_PERSON_NAME
            ,SHIP_FROM_ORG_ID
            ,SHIP_TO_ORG_ID)
         (SELECT /*+rule*/
                Oh.Header_Id,
                 OH.ORG_ID,
                 OH.ORDER_TYPE_ID,
                 OH.ORDER_NUMBER,
                 OH.ORDERED_DATE,
                 OH.REQUEST_DATE,
                 oh.creation_date,
                 oh.CREATED_BY,
                 (SELECT ppf.full_name
                    FROM per_all_people_f ppf, fnd_user fu
                   WHERE     1 = 1
                         AND ppf.person_id = fu.employee_id
                         AND user_id = oh.CREATED_BY
                         AND (TRUNC (oh.creation_date) BETWEEN TRUNC (
                                                                  ppf.effective_start_date)
                                                           AND NVL (
                                                                  ppf.effective_end_date,
                                                                  oh.creation_date))) CREATED_BY_NAME,
                 OH.QUOTE_NUMBER,
                 OH.SOLD_TO_ORG_ID,
                 (SELECT HZP.PARTY_ID
                    FROM HZ_CUST_ACCOUNTS HCA, HZ_PARTIES HZP
                   WHERE     oh.sold_to_org_id = hca.cust_account_id
                         AND HCA.PARTY_ID = HZP.PARTY_ID),
                 (SELECT hca.cust_account_id
                    FROM HZ_CUST_ACCOUNTS HCA
                   WHERE oh.sold_to_org_id = hca.cust_account_id),
                 (SELECT HCA.ACCOUNT_NUMBER
                    FROM HZ_CUST_ACCOUNTS HCA
                   WHERE oh.sold_to_org_id = hca.cust_account_id),
                 (SELECT NVL (HCA.ACCOUNT_NAME, hzp.party_name)
                    FROM HZ_CUST_ACCOUNTS HCA, HZ_PARTIES HZP
                   WHERE     oh.sold_to_org_id = hca.cust_account_id
                         AND HCA.PARTY_ID = HZP.PARTY_ID),
                 (SELECT HZCS_SHIP_TO.LOCATION
                    FROM HZ_CUST_SITE_USES_ALL HZCS_SHIP_TO,
                         HZ_CUST_ACCT_SITES HCAS_SHIP_TO,
                         HZ_PARTY_SITES HZPS_SHIP_TO,
                         HZ_LOCATIONS HZL_SHIP_TO
                   WHERE     oh.ship_to_org_id = hzcs_ship_to.site_use_id
                         AND hzcs_ship_to.cust_acct_site_id =
                                hcas_ship_to.cust_acct_site_id
                         AND hcas_ship_to.party_site_id =
                                hzps_ship_to.party_site_id
                         AND hzl_ship_to.location_id =
                                hzps_ship_to.location_id),
                 (SELECT Hcas_Ship_To.Cust_Acct_Site_Id
                    FROM HZ_CUST_SITE_USES_ALL HZCS_SHIP_TO,
                         HZ_CUST_ACCT_SITES HCAS_SHIP_TO,
                         HZ_PARTY_SITES HZPS_SHIP_TO,
                         HZ_LOCATIONS HZL_SHIP_TO
                   WHERE     oh.ship_to_org_id = hzcs_ship_to.site_use_id
                         AND hzcs_ship_to.cust_acct_site_id =
                                hcas_ship_to.cust_acct_site_id
                         AND hcas_ship_to.party_site_id =
                                hzps_ship_to.party_site_id
                         AND hzl_ship_to.location_id =
                                hzps_ship_to.location_id),
                 (SELECT hzl_ship_to.city
                    FROM HZ_CUST_SITE_USES_ALL HZCS_SHIP_TO,
                         HZ_CUST_ACCT_SITES HCAS_SHIP_TO,
                         HZ_PARTY_SITES HZPS_SHIP_TO,
                         HZ_LOCATIONS HZL_SHIP_TO
                   WHERE     oh.ship_to_org_id = hzcs_ship_to.site_use_id
                         AND hzcs_ship_to.cust_acct_site_id =
                                hcas_ship_to.cust_acct_site_id
                         AND hcas_ship_to.party_site_id =
                                hzps_ship_to.party_site_id
                         AND hzl_ship_to.location_id =
                                hzps_ship_to.location_id),
                 (SELECT Hzcs_Ship_To.Site_Use_Id
                    FROM HZ_CUST_SITE_USES_ALL HZCS_SHIP_TO,
                         HZ_CUST_ACCT_SITES HCAS_SHIP_TO,
                         HZ_PARTY_SITES HZPS_SHIP_TO,
                         HZ_LOCATIONS HZL_SHIP_TO
                   WHERE     oh.ship_to_org_id = hzcs_ship_to.site_use_id
                         AND hzcs_ship_to.cust_acct_site_id =
                                hcas_ship_to.cust_acct_site_id
                         AND hcas_ship_to.party_site_id =
                                hzps_ship_to.party_site_id
                         AND hzl_ship_to.location_id =
                                hzps_ship_to.location_id),
                 (SELECT HZPS_SHIP_TO.PARTY_SITE_ID
                    FROM HZ_CUST_SITE_USES_ALL HZCS_SHIP_TO,
                         HZ_CUST_ACCT_SITES HCAS_SHIP_TO,
                         HZ_PARTY_SITES HZPS_SHIP_TO,
                         HZ_LOCATIONS HZL_SHIP_TO
                   WHERE     oh.ship_to_org_id = hzcs_ship_to.site_use_id
                         AND hzcs_ship_to.cust_acct_site_id =
                                hcas_ship_to.cust_acct_site_id
                         AND hcas_ship_to.party_site_id =
                                hzps_ship_to.party_site_id
                         AND hzl_ship_to.location_id =
                                hzps_ship_to.location_id),
                 (SELECT hzl_ship_to.postal_code
                    FROM HZ_CUST_SITE_USES_ALL HZCS_SHIP_TO,
                         HZ_CUST_ACCT_SITES HCAS_SHIP_TO,
                         HZ_PARTY_SITES HZPS_SHIP_TO,
                         HZ_LOCATIONS HZL_SHIP_TO
                   WHERE     oh.ship_to_org_id = hzcs_ship_to.site_use_id
                         AND hzcs_ship_to.cust_acct_site_id =
                                hcas_ship_to.cust_acct_site_id
                         AND hcas_ship_to.party_site_id =
                                hzps_ship_to.party_site_id
                         AND hzl_ship_to.location_id =
                                hzps_ship_to.location_id),
                 OH.FLOW_STATUS_CODE,
                 OH.TRANSACTION_PHASE_CODE,
                 OH.PAYMENT_TERM_ID,
                 (SELECT /*+index(RAB RA_TERMS_B_U1)*/
                         NAME
                    FROM apps.RA_TERMS_B RAB, apps.RA_TERMS_TL RAT
                   WHERE     RAB.TERM_ID = RAT.TERM_ID
                         AND RAB.TERM_ID = oh.payment_term_id),
                 OH.SALESREP_ID,
                 (SELECT Rep.Salesrep_Id
                    FROM ra_salesreps REP -- Version# 1.5
                   WHERE     OH.SALESREP_ID = REP.SALESREP_ID
                         AND OH.ORG_ID = REP.ORG_ID),
                 (SELECT REP.name
                    FROM ra_salesreps REP -- Version# 1.5
                   WHERE     OH.SALESREP_ID = REP.SALESREP_ID
                         AND OH.ORG_ID = REP.ORG_ID),
                 OH.SHIP_FROM_ORG_ID,
                 OH.SHIP_TO_ORG_ID
            FROM OE_ORDER_HEADERS_ALL OH
           WHERE 1= 1 
            AND (p_ordered_date_from IS NULL 
                 OR (TRUNC(NVL(oh.ordered_date,oh.creation_date)) BETWEEN to_date(p_ordered_date_from,'DD-MON-YYYY') AND to_date(p_ordered_date_to,'DD-MON-YYYY'))) -- Version# 1.0
             AND EXISTS (SELECT '1' -- Version# 1.0
                           FROM xxeis.xxwc_gtt_salesreps_tbl stg
                          WHERE oh.salesrep_id = stg.salesrep_id)
             AND OH.FLOW_STATUS_CODE IN ('BOOKED',
                                         'DRAFT',
                                         'ENTERED',
                                         'PENDING_CUSTOMER_ACCEPTANCE'));

EXCEPTION 
WHEN OTHERS THEN
  fnd_file.put_line (FND_FILE.LOG,'In others of xxwc_open_sales_order_pre_trig for headers' || SQLERRM);
End;

BEGIN
  SELECT instr(p_org_id,',') 
    INTO v_val
    FROM dual;

IF p_org_id !='All' THEN
   IF v_val!=0 THEN
      v_org_id:=p_org_id;
   ELSE
      v_org_id:= ''''||p_org_id||'''';
   END IF;

   EXECUTE IMMEDIATE 'insert into XXEIS.XXWC_GTT_ORGANIZATIONS_TEMP value (SELECT organization_id  FROM MTL_PARAMETERS  WHERE organization_code IN ('||v_org_id||'))';

ELSE
   INSERT INTO XXEIS.XXWC_GTT_ORGANIZATIONS_TEMP value (SELECT organization_id  FROM MTL_PARAMETERS  WHERE 1=1);
END IF;

EXCEPTION
WHEN OTHERS THEN
  fnd_file.put_line (FND_FILE.LOG,'Error while populating XXWC_GTT_ORGANIZATIONS_TEMP Global Temporary Table' || SQLERRM);
END;

--------------------------------------------------------------------------
-- Insert into staging table XXEIS.XXWC_GTT_OE_ORDER_LINES
--------------------------------------------------------------------------
BEGIN
   INSERT INTO XXEIS.XXWC_GTT_OE_ORDER_LINES  (LINE_ID
            ,HEADER_ID
            ,SOLD_FROM_ORG_ID
            ,SHIP_FROM_ORG_ID
            ,WAREHOUSE
            ,SHIPPING_METHOD_CODE
            ,ORDERED_QUANTITY
            ,CREATION_DATE
            ,FLOW_STATUS_CODE
            ,UNIT_SELLING_PRICE
            ,LINE_CATEGORY_CODE
            ,SCHEDULE_SHIP_DATE
            ,INVENTORY_ITEM_ID
            ,ITEM_NUMBER
            ,USER_ITEM_DESCRIPTION)
         (SELECT /*+rule*/
                OL.LINE_ID,
                 OL.HEADER_ID,
                 OL.SOLD_FROM_ORG_ID,
                 ol.ship_from_org_id,
                 (SELECT mtp.organization_code
                    FROM mtl_parameters mtp
                   WHERE mtp.organization_id = ol.ship_from_org_id),
                 OL.SHIPPING_METHOD_CODE,
                 OL.ORDERED_QUANTITY,
                 OL.CREATION_DATE,
                 OL.FLOW_STATUS_CODE,
                 OL.UNIT_SELLING_PRICE,
                 OL.LINE_CATEGORY_CODE,
                 OL.SCHEDULE_SHIP_DATE,
                 OL.INVENTORY_ITEM_ID,
                 (SELECT MSI.SEGMENT1
                    FROM MTL_SYSTEM_ITEMS_B MSI
                   WHERE     MSI.ORGANIZATION_ID = OL.SHIP_FROM_ORG_ID
                         AND MSI.INVENTORY_ITEM_ID = OL.INVENTORY_ITEM_ID),
                 (SELECT MSI.DESCRIPTION
                    FROM MTL_SYSTEM_ITEMS_B MSI
                   WHERE     MSI.ORGANIZATION_ID = OL.SHIP_FROM_ORG_ID
                         AND MSI.INVENTORY_ITEM_ID = OL.INVENTORY_ITEM_ID)
            FROM OE_ORDER_LINES_ALL OL, XXEIS.XXWC_GTT_OE_ORDER_HEADERS OH
           WHERE     OL.HEADER_ID = OH.HEADER_ID
                 AND exists (SELECT 1 FROM MTL_PARAMETERS mp,XXEIS.XXWC_GTT_ORGANIZATIONS_TEMP a  WHERE ol.ship_from_org_id =mp.organization_id and mp.organization_id=a.ORGANIZATION_ID )
                 AND OL.FLOW_STATUS_CODE NOT IN ('CANCELLED', 'CLOSED'));

EXCEPTION 
WHEN OTHERS THEN
 FND_FILE.put_line (FND_FILE.LOG,
            'In others of xxwc_open_sales_order_pre_trig for lines' || SQLERRM);
END;

      --------------------------------------------------------------------------
      -- Insert into staging table XXEIS.XXWC_GTT_WSH_SHIPPING_TEMP_STG
      --------------------------------------------------------------------------
    begin

              INSERT INTO XXEIS.XXWC_GTT_WSH_SHIPPING_TEMP_STG (
        HEADER_ID
        ,LINE_ID
        ,DELIVERY_ID
        ,DELIVERY_DETAIL_ID
        ,INVENTORY_ITEM_ID
        ,SHIP_FROM_ORG_ID
        ,ORDERED_QUANTITY
        ,FORCE_SHIP_QTY
        ,TRANSACTION_QTY
        ,LOT_NUMBER
        ,RESERVATION_ID
        ,STATUS
        ,CREATED_BY
        ,CREATION_DATE
        ,LAST_UPDATED_BY
        ,LAST_UPDATE_DATE
        ,LAST_UPDATE_LOGIN
        ,REQUEST_ID)
                 (SELECT /*+rule*/
                         XXWSS.HEADER_ID
        ,XXWSS.LINE_ID
        ,XXWSS.DELIVERY_ID
        ,XXWSS.DELIVERY_DETAIL_ID
        ,XXWSS.INVENTORY_ITEM_ID
        ,XXWSS.SHIP_FROM_ORG_ID
        ,XXWSS.ORDERED_QUANTITY
        ,XXWSS.FORCE_SHIP_QTY
        ,XXWSS.TRANSACTION_QTY
        ,XXWSS.LOT_NUMBER
        ,XXWSS.RESERVATION_ID
        ,XXWSS.STATUS
        ,XXWSS.CREATED_BY
        ,XXWSS.CREATION_DATE
        ,XXWSS.LAST_UPDATED_BY
        ,XXWSS.LAST_UPDATE_DATE
        ,XXWSS.LAST_UPDATE_LOGIN
        ,XXWSS.REQUEST_ID
            FROM xxeis.XXWC_GTT_OE_ORDER_LINES XXWCOL,
                 xxwc.xxwc_wsh_shipping_stg XXWSS
           WHERE     XXWCOL.HEADER_ID = XXWSS.HEADER_ID
                 AND XXWCOL.LINE_ID = XXWSS.LINE_ID
                 AND XXWCOL.INVENTORY_ITEM_ID = XXWSS.INVENTORY_ITEM_ID
                 AND XXWCOL.ship_from_org_id = XXWSS.ship_from_org_id);
          exception 
when others then
 FND_FILE.put_line (FND_FILE.LOG,
            'In others of xxwc_open_sales_order_pre_trig for SHIPPING_TEMP_STG' || SQLERRM);
 end;


      --------------------------------------------------------------------------
      -- Insert into staging table XXEIS.XXWC_GTT_PRINT_LOG_TBL
      --------------------------------------------------------------------------
 begin

              INSERT INTO XXEIS.XXWC_GTT_PRINT_LOG_TBL (REQUEST_ID
            ,CONCURRENT_PROGRAM_ID
            ,PROGRAM_APPLICATION_ID
            ,CONCURRENT_PROGRAM_NAME
            ,USER_CONCURRENT_PROGRAM_NAME
            ,PRINTER
            ,PRINTER_DESCRIPTION
            ,REQUESTED_BY
            ,USER_NAME
            ,USER_DESCRIPTION
            ,HEADER_ID
            ,ORGANIZATION_ID
            ,TABLE_NAME
            ,ACTUAL_COMPLETION_DATE
            ,PHASE_CODE
            ,STATUS_CODE
            ,NUMBER_OF_COPIES
            ,PHASE
            ,STATUS
            ,ARGUMENT1
            ,ARGUMENT2
            ,ARGUMENT3
            ,ARGUMENT4
            ,ARGUMENT5
            ,ARGUMENT6
            ,ARGUMENT7
            ,ARGUMENT8
            ,ARGUMENT9
            ,ARGUMENT10
            ,ARGUMENT11
            ,ARGUMENT12
            ,ARGUMENT13
            ,ARGUMENT14
            ,ARGUMENT15
            ,ARGUMENT16
            ,ARGUMENT17
            ,ARGUMENT18
            ,ARGUMENT19
            ,ARGUMENT20
            ,ARGUMENT21
            ,ARGUMENT22
            ,ARGUMENT23
            ,ARGUMENT24
            ,ARGUMENT25) 
                 (SELECT /*+rule*/
                         distinct xpl.REQUEST_ID
            ,xpl.CONCURRENT_PROGRAM_ID
            ,xpl.PROGRAM_APPLICATION_ID
            ,xpl.CONCURRENT_PROGRAM_NAME
            ,xpl.USER_CONCURRENT_PROGRAM_NAME
            ,xpl.PRINTER
            ,xpl.PRINTER_DESCRIPTION
            ,xpl.REQUESTED_BY
            ,xpl.USER_NAME
            ,xpl.USER_DESCRIPTION
            ,xpl.HEADER_ID
            ,xpl.ORGANIZATION_ID
            ,xpl.TABLE_NAME
            ,xpl.ACTUAL_COMPLETION_DATE
            ,xpl.PHASE_CODE
            ,xpl.STATUS_CODE
            ,xpl.NUMBER_OF_COPIES
            ,xpl.PHASE
            ,xpl.STATUS
            ,xpl.ARGUMENT1
            ,xpl.ARGUMENT2
            ,xpl.ARGUMENT3
            ,xpl.ARGUMENT4
            ,xpl.ARGUMENT5
            ,xpl.ARGUMENT6
            ,xpl.ARGUMENT7
            ,xpl.ARGUMENT8
            ,xpl.ARGUMENT9
            ,xpl.ARGUMENT10
            ,xpl.ARGUMENT11
            ,xpl.ARGUMENT12
            ,xpl.ARGUMENT13
            ,xpl.ARGUMENT14
            ,xpl.ARGUMENT15
            ,xpl.ARGUMENT16
            ,xpl.ARGUMENT17
            ,xpl.ARGUMENT18
            ,xpl.ARGUMENT19
            ,xpl.ARGUMENT20
            ,xpl.ARGUMENT21
            ,xpl.ARGUMENT22
            ,xpl.ARGUMENT23
            ,xpl.ARGUMENT24
            ,xpl.ARGUMENT25
            FROM xxwc.xxwc_print_log_tbl xpl,
                 XXEIS.XXWC_GTT_OE_ORDER_HEADERS oh
           WHERE     xpl.concurrent_program_id IN (70401, 75404) --packing slip and internal packslip
                 AND xpl.program_application_id = 20005
                 AND xpl.organization_id = oh.ship_from_org_id
                 AND xpl.header_id = oh.header_id);
exception 
when others then
FND_FILE.put_line (FND_FILE.LOG,'In others of xxwc_open_sales_order_pre_trig for log' || SQLERRM);
 end;

      --------------------------------------------------------------------------
      -- Insert into staging table XXEIS.XXWC_GTT_OPEN_ORDERS_TMP
      --------------------------------------------------------------------------
begin

                  INSERT INTO XXEIS.XXWC_GTT_OPEN_ORDERS_TMP (WAREHOUSE
                ,SALES_PERSON_NAME
                ,CREATED_BY
                ,ORDER_NUMBER
                ,ORDERED_DATE
                ,LINE_CREATION_DATE
                ,ORDER_TYPE
                ,QUOTE_NUMBER
                ,ORDER_LINE_STATUS
                ,ORDER_HEADER_STATUS
                ,CUSTOMER_NUMBER
                ,CUSTOMER_NAME
                ,CUSTOMER_JOB_NAME
                ,ORDER_AMOUNT
                ,SHIPPING_METHOD
                ,SHIP_TO_CITY
                ,ZIP_CODE
                ,SCHEDULE_SHIP_DATE
                ,HEADER_STATUS
                ,TRANSACTION_PHASE_CODE
                ,ITEM_NUMBER
                ,ITEM_DESCRIPTION
                ,QTY
                ,PAYMENT_TERMS
                ,REQUEST_DATE
                ,ORDER_HEADER_ID
                ,ORDER_LINE_ID
                ,CUST_ACCOUNT_ID
                ,CUST_ACCT_SITE_ID
                ,SALESREP_ID
                ,PARTY_ID
                ,TRANSACTION_TYPE_ID
                ,MTP_ORGANIZATION_ID
                ,HZCS_SHIP_TO_STE_ID
                ,HZPS_SHP_TO_PRT_ID
                ,USER_ITEM_DESCRIPTION
                ,INVENTORY_ITEM_ID
                ,HDR_SHIP_FROM_ORG_ID
                ,LIN_SHIP_FROM_ORG_ID
                ,HEADER_ID
                ,LINE_ID
                ,DELIVERY_DOC_DATE)
         (SELECT /*+rule*/
                OL.warehouse,
        --(SELECT organization_code FROM MTL_PARAMETERS  WHERE organization_id=OL.warehouse and rownum<2 ) warehouse,
                 OH.SALES_PERSON_NAME,
                 OH.CREATED_BY_NAME,
                 OH.ORDER_NUMBER,
                 TRUNC (OH.ORDERED_DATE),
                 TRUNC (ol.creation_date),
                 OTT.NAME,
                 OH.QUOTE_NUMBER,
                 apps.OE_LINE_STATUS_PUB.get_line_status (
                    ol.line_id,
                    ol.flow_status_code),
                 flv_order_status.meaning,
                 OH.CUSTOMER_NUMBER,
                 OH.customer_name,
                 OH.CUSTOMER_JOB_NAME,
                 ROUND (
                    (  OL.UNIT_SELLING_PRICE
                     * DECODE (OL.LINE_CATEGORY_CODE,
                               'RETURN', OL.ORDERED_QUANTITY * -1,
                               OL.ORDERED_QUANTITY)),
                    2),
                 flv_ship_mtd.meaning,
                 OH.ship_to_city,
                 OH.zip_code,
                 OL.SCHEDULE_SHIP_DATE,
                 OH.FLOW_STATUS_CODE,
                 OH.TRANSACTION_PHASE_CODE,
                 OL.Item_number,
                 OL.USER_ITEM_DESCRIPTION,
                 DECODE (ol.line_category_code,
                         'RETURN', (NVL (Ol.ordered_quantity, 0) * -1),
                         NVL (Ol.ordered_quantity, 0)),
                 OH.Payment_terms,
                 oh.request_date,
                 Oh.Header_Id,
                 Ol.Line_Id,
                 OH.cust_account_id,
                 OH.Cust_Acct_Site_Id,
                 OH.ORG_SALESREP_ID,
                 OH.PARTY_ID,
                 ott.transaction_type_id,
                 (SELECT Mtp.Organization_Id
                    FROM mtl_parameters mtp
                   WHERE mtp.organization_id = ol.ship_from_org_id),
                 OH.SHIP_TO_STE_ID,
                 OH.SHP_TO_PRT_ID,
                 OL.USER_ITEM_DESCRIPTION,
                 ol.INVENTORY_ITEM_ID,
                 oh.ship_from_org_id,
                 ol.ship_from_org_id,
                 oh.header_id,
                 ol.line_id,
                 NULL
            -- XXEIS.eis_rs_process_reports.GET_DOC_DATE (
            --    oh.header_id,
            --    ol.line_id,
            --    ol.INVENTORY_ITEM_ID,
            ----    ol.ship_from_org_id,
            --    oh.ship_from_org_id)
            FROM XXEIS.XXWC_GTT_OE_ORDER_HEADERS OH,
                 XXEIS.XXWC_GTT_OE_ORDER_LINES ol,
                 OE_TRANSACTION_TYPES_VL OTT,
                 FND_LOOKUP_VALUES_VL FLV_SHIP_MTD,
                 OE_LOOKUPS FLV_ORDER_STATUS
           WHERE     ol.header_id = oh.header_id
                 AND OH.ORDER_TYPE_ID = OTT.TRANSACTION_TYPE_ID
                 AND OL.FLOW_STATUS_CODE NOT IN ('CLOSED', 'CANCELLED')
                 AND FLV_SHIP_MTD.LOOKUP_TYPE(+) = 'SHIP_METHOD'
                 AND FLV_SHIP_MTD.LOOKUP_CODE(+) = OL.SHIPPING_METHOD_CODE
                 AND flv_order_status.lookup_type = 'FLOW_STATUS'
                 AND flv_order_status.lookup_code = Oh.flow_status_code
                 AND ott.name IN ('STANDARD ORDER',
                                  'COUNTER ORDER',
                                  'RETURN ORDER',
                                  'REPAIR ORDER'
                                  ,'WC LONG TERM RENTAL', --Added by Mahender Reddy	for TMS 20150818-00019 on 09/22/2015
                                  'WC SHORT TERM RENTAL' --Added by Mahender Reddy	for TMS 20150818-00019 on 09/22/2015
                                  )
                 AND (   (    ol.FLOW_STATUS_CODE = 'ENTERED'
                          AND TRUNC (oh.creation_date) < TRUNC (SYSDATE))
                      OR OL.FLOW_STATUS_CODE <> 'ENTERED'));

     exception 
   when others then
   FND_FILE.put_line (FND_FILE.LOG,'In others of xxwc_open_sales_order_pre_trig for tmp' || SQLERRM);
   end;
      


      FOR i IN c1
      LOOP

          begin
         SELECT MIN (xpl.actual_completion_date)
           INTO l_doc_date
           FROM XXEIS.XXWC_GTT_PRINT_LOG_TBL xpl
          WHERE     1 = 1
                AND xpl.header_id = i.header_id
                AND xpl.organization_id = i.HDR_SHIP_FROM_ORG_ID
                AND EXISTS
                       (SELECT distinct '1'
                          FROM XXEIS.XXWC_GTT_WSH_SHIPPING_TEMP_STG xws
                         WHERE     1 = 1
                               AND xws.header_id = i.header_id
                               AND xws.line_id = i.line_id
                             --AND xws.inventory_item_id = i.ITEM_NUMBER
                               AND xws.inventory_item_id = i.inventory_item_id  --TMS#20140317-00281 Error in Open Sales Orders Report
                               AND xws.ship_from_org_id =
                                      i.LIN_SHIP_FROM_ORG_ID
                               AND xpl.argument10 = TO_CHAR (xws.delivery_id));
            exception 
when others then
 FND_FILE.put_line (FND_FILE.LOG,'In others of xxwc_open_sales_order_pre_trig xpl.actual_completion_date i.header_id...i.line_id...i.ITEM_NUMBER...i.LIN_SHIP_FROM_ORG_ID...i.inventory_item_id..' || SQLERRM||i.header_id||'...'||i.line_id||'...'||i.ITEM_NUMBER||'...'||i.LIN_SHIP_FROM_ORG_ID||i.inventory_item_id);
 end;
      


         UPDATE XXEIS.XXWC_GTT_OPEN_ORDERS_TMP
            SET DELIVERY_DOC_DATE = l_doc_date
          WHERE ROWID = i.rid;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN


        v_errbuf :=
               'Error_Stack...'
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();

   END open_sales_order_pre_trig;
   -- Version# 1.3 < End



END EIS_RS_XXWC_OM_UTIL_PKG;
/