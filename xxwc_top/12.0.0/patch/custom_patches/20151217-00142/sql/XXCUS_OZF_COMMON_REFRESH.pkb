CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_OZF_COMMON_REFRESH
-- ESMS TICKET 228687
-- Date Modified: 04-NOV-2013
-- Scope: Oracle Trade Management / Rebates accruals and gl journals extract
-- Used by concurrent jobs HDS Rebates: Generate TM SLA Accruals Extract and HDS Rebates: Generate TM GL Journals Extract
-- Change History:
-- Ticket#                                Date                    Comment
-- =======                              =========          ============================================================================================
-- RFC 38762                         02-Dec-2013    Changes to the signature of the routines invoked 
-- RFC 38762 SR 231125   03-Dec-2013    Commented out the call to pop_category_set_details. This is not required as of now bcoz we are only using a specific category set id.
-- Child: TMS 20151217-00142 , Parent: TMS  20151008-00085 12/17/2015      ESMS 301618. USABB changes. Populate the custom table XXCUS.XXCUS_OZF_REBATE_DIVISIONS_ALL
--                                                                            which is going to store all HDS Rebate Divsions. This table will be used by the Rebate Market Eligibility screen setup
--                                                                            as well as the custom adjustment from "HDS Rebates: Accrual Adjustments".
-- 
as
  procedure print_log(p_message in varchar2) is
  begin  
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||sqlerrm);
  end print_log;
  --
  procedure extract_items
  (
    retcode               out number
   ,errbuf                out varchar2
  ) is
  --
   v_sql VARCHAR2(240) :=Null;
   N_Count NUMBER;
   b_proceed BOOLEAN;
   b_backup  BOOLEAN;
   v_loc     varchar2(3);
   v_cat_set_name inv.mtl_category_sets_tl.category_set_name%type :=Null;
   v_cat_set_desc inv.mtl_category_sets_tl.description%type :=Null;
   v_item_insert_sql varchar2(10000) :=Null;
   v_cr varchar2(1) :='
';
  --
   cursor rebuild_indexes (p_table in varchar2) is
    select 'ALTER INDEX '||owner||'.'||ui.index_name||' REBUILD' idx_sql
    from dba_indexes ui
    where 1 =1
    and ui.owner      ='XXCUS'
    and ui.table_name =p_table -- 'XXCUS_OZF_REBATE_ITEMS'
    order by ui.index_name;   
  --
  procedure pop_org_details is
  begin 
   --
   savepoint square1;
   --
    merge /*+ PARALLEL (AUTO) */ into xxcus.xxcus_ozf_rebate_items t
    using (
            select organization_id      inv_org_id
                  ,organization_code    inv_org_code
                  ,organization_name    inv_org_name
            from   org_organization_definitions
            where  1 =1  
              and  organization_id =84
           ) s
    on (t.organization_id = s.inv_org_id)
    when matched then update set t.organization_code =s.inv_org_code, t.organization_name =s.inv_org_name;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_org_details, message ='||sqlerrm);
  end pop_org_details;
  --
  procedure pop_category_details is
  begin 
   --
   savepoint square1;
   --
    merge /*+ PARALLEL (AUTO) */ into xxcus.xxcus_ozf_rebate_items t
    using (
            select segment1
                  ,segment2
                  ,description
                  ,structure_id
                  ,category_id
            from   inv.mtl_categories_b
            where  1 =1          
           ) s
    on (t.category_id = s.category_id)
    when matched then update set t.category_segment1 =s.segment1
                                ,t.category_segment2 =s.segment2
                                ,t.category_desc =s.description
                                ,t.cat_structure_id =s.structure_id;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_category_details, message ='||sqlerrm);
  end pop_category_details;
  --
  procedure pop_category_set_details is
  begin 
   --
   savepoint square1;
   --
    merge /*+ PARALLEL (AUTO) */ into xxcus.xxcus_ozf_rebate_items t
    using (
            select category_set_name  cat_set_name
                  ,description        cat_set_desc
                  ,category_set_id    cat_set_id
            from   inv.mtl_category_sets_tl
            where  1 =1          
           ) s
    on (t.category_set_id = s.cat_set_id)
    when matched then update set t.category_set_name =s.cat_set_name, t.category_set_desc =cat_set_desc;
   --
  exception
   when others then
    rollback to square1;
    print_log ('@pop_category_set_details, message ='||sqlerrm);
  end pop_category_set_details;   
  --  
  begin --Main Processing 
    --
    execute immediate 'ALTER SESSION FORCE PARALLEL DML';
    execute immediate 'ALTER TABLE XXCUS.XXCUS_OZF_REBATE_ITEMS PARALLEL';
    print_log ('');    
    print_log('Note: Current session is parallel enabled.');
    print_log ('Begin refresh of rebates item table XXCUS.XXCUS_OZF_REBATE_ITEMS');
   --
    begin
      --
      execute immediate 'TRUNCATE TABLE XXCUS.XXCUS_OZF_REBATE_ITEMS';
      --
      b_proceed :=TRUE;
      --
      print_log ('Successfully removed old data removed from table XXCUS.XXCUS_OZF_REBATE_ITEMS');
      --
    exception
     when others then
       b_proceed :=FALSE;
       print_log (''||sqlerrm);
    end;
    --
    begin 
     select category_set_name  
           ,description        
     into   v_cat_set_name
           ,v_cat_set_desc
     from   inv.mtl_category_sets_tl
     where  1 =1
       and  category_set_id =1100000041;
    exception
     when no_data_found then
      v_cat_set_name :=Null;
      v_cat_set_desc :=Null;     
     when others then
      print_log('Error in fetch of category set name and description');
      v_cat_set_name :=Null;
      v_cat_set_desc :=Null;
    end;
    --
    if (b_proceed) then
     -- 
     begin 
      --
      v_item_insert_sql :=
        'INSERT /*+ APPEND PARALLEL(AUTO) */ INTO XXCUS.XXCUS_OZF_REBATE_ITEMS 
        (
            SELECT  /*+ PARALLEL (AUTO) */
                  Null                     category_segment1
                 ,Null                     category_segment2
                 ,Null                     category_desc '||v_cr||
                 '                 ,'||''''||v_cat_set_name||''''||'  category_set_name '||v_cr||
                 '                 ,'||''''||v_cat_set_desc||''''||'  category_set_desc '||v_cr||                 
                 '                 ,inv.category_set_id      category_set_id     
                 ,inv.category_id          category_id
                 ,Null                     cat_structure_id    
                 ,msi.organization_id      organization_id
                 ,Null                     organization_code
                 ,Null                     organization_name
                 ,msi.inventory_item_id    inventory_item_id
                 ,msi.segment1             segment1
                 ,msi.description          description 
                 ,msi.attribute_category   attribute_category
                 ,msi.attribute1           attribute1       
                 ,msi.attribute2           attribute2
                 ,msi.attribute3           attribute3
                 ,msi.attribute4           attribute4
                 ,msi.attribute5           attribute5                 
                 ,msi.attribute6           attribute6
                 ,msi.attribute7           attribute7
                 ,msi.attribute8           attribute8
                 ,msi.attribute9           attribute9
                 ,msi.attribute10          attribute10
                 ,msi.attribute11          attribute11
                 ,msi.attribute12          attribute12
                 ,msi.attribute13          attribute13
                 ,msi.attribute14          attribute14
                 ,msi.attribute15          attribute15       
            from
             inv.mtl_system_items_b       msi
            ,inv.mtl_item_categories      inv
            where  1 =1
              and  msi.organization_id       =84 -- rebates inventory org items only
              and  inv.category_set_id       =1100000041 --HDS Supplier Category 
              and  inv.inventory_item_id(+)  =msi.inventory_item_id 
              and  inv.organization_id(+)    =msi.organization_id  
        )'; --End of insert statement       
      --
      print_log('');
      print_log('Dynamic SQL');
      print_log('===========');
      print_log(v_item_insert_sql);
      print_log('');      
      --
      execute immediate v_item_insert_sql;
      --
      print_log ('Total REBATE item records inserted =>'||SQL%ROWCOUNT); 
      --           
      Commit;
      --
      b_proceed :=TRUE;
      --
     exception
      when others then
       print_log('Error in insert of XXCUS.XXCUS_OZF_REBATE_ITEMS, Message =>'||sqlerrm);
       b_proceed :=FALSE;       
     end;
     --
    else
     print_log('Issue in truncating old data from XXCUS.XXCUS_OZF_REBATE_ITEMS, message =>'||sqlerrm);
    end if;
    -- 
    if (b_proceed) then
     -- 
        begin 
         --
         print_log ('Begin update of rebates item attributes.');     
         pop_org_details;
         Commit;
         v_loc :='101';
         pop_category_details;
         Commit;     
         v_loc :='102';
         -- Commented out the call to pop_category_set_details. This is not required as of now bcoz we are only using a specific category set id.
         --pop_category_set_details;
         --Commit;     
         --v_loc :='103';
         print_log ('End update of rebates item attributes.');                   
         --
        exception
         when others then
          print_log ('@Rebates item attributes update, last known location ='||v_loc||', Error =>'||sqlerrm);
        end;
     --
        begin 
         --
         print_log ('Begin rebuild of indexes...');
         --
         for rec in rebuild_indexes (p_table =>'XXCUS_OZF_REBATE_ITEMS') loop
          --
           v_sql :=rec.idx_sql; 
          --
            execute immediate rec.idx_sql;
          --
         end loop;
         --
         print_log ('End rebuild of indexes...');     
         --
        exception
         when others then
          print_log ('Issue in rebuild of index, current sql =>'||v_sql);
        end;
     --    
    end if;
   --
   print_log('Before gather table stats...');
   --
   begin
    dbms_stats.gather_table_stats
    (
        ownname     =>'XXCUS'
       ,tabname     =>'XXCUS_OZF_REBATE_ITEMS'
       ,method_opt  =>'for all columns size auto'
       ,degree      =>dbms_stats.auto_degree
    ); 
   exception
    when others then
     print_log('Error during of gather table stats, message: '||sqlerrm);
   end;
   --
   print_log('After gather table stats...');   
   -- 
  exception
   when others then
    print_log ('Issue in extract_items routine, message ='||sqlerrm);
    rollback;
  end extract_items;  
  --
  procedure extract_customers
  (
    retcode               out number
   ,errbuf                out varchar2
  ) is
  --
   v_sql VARCHAR2(240) :=Null;
   N_Count NUMBER;
   b_proceed BOOLEAN;
   b_backup  BOOLEAN;
   v_loc varchar2(3);
  --
   cursor rebuild_indexes (p_table in varchar2) is
    select 'ALTER INDEX '||owner||'.'||ui.index_name||' REBUILD' idx_sql
    from dba_indexes ui
    where 1 =1
    and ui.owner      ='XXCUS'
    and ui.table_name =p_table -- 'XXCUS_REBATE_CUSTOMERS'
    order by ui.index_name;    
  --    
  begin --Main Processing 
    --
    print_log('');    
    print_log ('Begin refresh of rebates customer table =>XXCUS.XXCUS_REBATE_CUSTOMERS');
    --
    begin 
      --
      execute immediate 'TRUNCATE TABLE XXCUS.XXCUS_REBATE_CUSTOMERS';
      --
      b_proceed :=TRUE;
      --
      print_log ('Successfully removed old data removed from table XXCUS.XXCUS_REBATE_CUSTOMERS');
      --
    exception
      when others then
       b_proceed :=FALSE;
     end;
    --
    if (b_proceed) then
     -- 
     begin 
      --
      INSERT INTO XXCUS.XXCUS_REBATE_CUSTOMERS
        (
        select hzca.account_number   customer_number
              ,hzca.attribute1       customer_attribute1
              ,hzca.attribute2       customer_attribute2
              ,hzca.attribute3       customer_attribute3      
              ,hzp.party_name        party_name
              ,hzp.attribute1        party_attribute1
              ,hzca.cust_account_id  customer_id
              ,hzp.party_id          party_id
              ,sysdate               extract_date
        from   hz_cust_accounts hzca
              ,hz_parties       hzp
        where  1 =1
          and  hzp.party_id =hzca.party_id
          and  (hzp.attribute1 IN ('HDS_MVID', 'HDS_BU', 'HDS_LOB') OR hzca.attribute1 IN ('HDS_MVID', 'HDS_LOB_BRANCH')) 
        );       
      --
      print_log ('Total records inserted =>'||SQL%ROWCOUNT); 
      --
     exception
      when others then
       print_log('Error in insert of XXCUS.XXCUS_REBATE_CUSTOMERS, Message =>'||sqlerrm);
       b_proceed :=FALSE;       
     end;
     --
    else
     print_log('Issue in truncating old data from XXCUS.XXCUS_REBATE_CUSTOMERS, message =>'||sqlerrm);
    end if;
    --  
    begin 
     --
     print_log ('Begin rebuild of indexes...');
     --
     for rec in rebuild_indexes (p_table =>'XXCUS_REBATE_CUSTOMERS') loop
      --
       v_sql :=rec.idx_sql; 
      --
        execute immediate rec.idx_sql;
      --
     end loop;
     --
     print_log ('End rebuild of indexes...');     
     --
    exception
     when others then
      print_log ('Issue in rebuild of index, current sql =>'||v_sql);
    end;
   --
    print_log ('End refresh of rebates customer table =>XXCUS.XXCUS_REBATE_CUSTOMERS');    
   --   
   -- Begin TMS 20151217-00142
   commit; --This is to safeguard all previous inserts.
   --
   print_log('Begin refresh of HDS Rebate BU custom table xxcus.xxcus_ozf_rebate_divisions_all');
   --
   begin
    --
    savepoint sqr1;
    --
    delete xxcus.xxcus_ozf_rebate_divisions_all;
    --
    insert into xxcus.xxcus_ozf_rebate_divisions_all 
     (
      party_id, 
      party_name, 
      party_number
     )
     (
       select party_id, 
                   party_name, 
                   party_number 
       from hz_parties
       where  1 =1
            and  attribute1 = 'HDS_BU'
            and  status ='A'
     );
    --
    print_log('Table xxcus.xxcus_ozf_rebate_divisions_all is refreshed, row count ='||sql%rowcount);
    --
    commit;
    --
   exception
    when others then
     print_log('Table is not refreshed, old data still exists. Error in insert of xxcus.xxcus_ozf_rebate_divisions_all, message ='||sqlerrm);
     rollback to sqr1;
   end;
   --
   print_log('End refresh of HDS Rebate BU custom table xxcus.xxcus_ozf_rebate_divisions_all');
   -- End TMS 20151217-00142     
  exception
   when others then
    print_log ('Issue in extract_customers routine, message ='||sqlerrm);
    rollback;
  end extract_customers;  
  -- 
end XXCUS_OZF_COMMON_REFRESH;
/
