   /* ************************************************************************
    HD Supply
    All rights reserved.
    Ticket                                         Ver          Date                 Author                                 Description
    ----------------------                  ----------   ----------          ------------------------           -------------------------
    ESMS 257200                         1.0                                                                                   Missing header 
    TMS 20151217-00142       1.1         11/19/2015   Balaguru Seshadri            EDIT SQL query to use a custom table instead of hz tables
   ************************************************************************ */ 
CREATE OR REPLACE FORCE VIEW APPS.XXCUSOZF_RBT_BRANCH_V
(
   BRANCH_ID,
   BRANCH_NAME,
   BRANCH_DESC,
   LOB_PARTY_NAME,
   LOB_PARTY_ID
)
AS
-- BEGIN Ver 1.1
select customer_id branch_id,
          customer_number branch_name,
          customer_attribute3 branch_desc,
          party_name lob_party_name,
          party_id lob_party_id
from    xxcus.xxcus_rebate_customers
where 1 =1
      and customer_attribute1 ='HDS_LOB_BRANCH'
      and party_attribute1 ='HDS_LOB'
order by party_id, customer_id;
-- END  Ver 1.1
/* -- Ver 1.1
   SELECT HZCA.CUST_ACCOUNT_ID BRANCH_ID,
          HZCA.ACCOUNT_NUMBER BRANCH_NAME,
          HZCA.ATTRIBUTE3 BRANCH_DESC,
          HZP.PARTY_NAME LOB_PARTY_NAME,
          HZP.PARTY_ID LOB_PARTY_ID
     FROM HZ_CUST_ACCOUNTS HZCA, HZ_PARTIES HZP
    WHERE     1 = 1
          AND HZCA.ATTRIBUTE1 = 'HDS_LOB_BRANCH'   --customer level attribute1
          AND HZP.ATTRIBUTE1 = 'HDS_LOB'              --party level attribute1
          AND HZP.PARTY_ID = HZCA.PARTY_ID;
*/  -- Ver 1.1
--
COMMENT ON TABLE APPS.XXCUSOZF_RBT_BRANCH_V IS 'ESMS 301618 / TMS 20151217-00142';
