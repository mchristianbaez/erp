CREATE OR REPLACE PACKAGE BODY APPS.xxcus_tm_misc_pkg IS
  -- ------------------------------------------------------------------------------
  -- Copyright 2010 HD Supply  Inc (Orlando, FL) - All rights reserved
  --
  --
  --    NAME:       XXCUS_TM_MISC_PKG.pck
  --
  --    REVISIONS:
  --    Ver        Date        Author           Description
  --    ---------  ----------  ---------------  -------------------------------
  --    1.0        01/18/2010  Chandra Gadge    1. Created this package.
  --    1.1        02/15/2010  Chandra Gadge    1. Created the Autoapproval For Programs
  --    1.2        02/25/2010  Chandra Gadge    1. Rebate Adjustments
  --                                            2. Rebate Mass Copy Changes
  --    1.3        03/18/2010  Chandra Gadge    1. Create Program Hierarchy
  --    1.4        06/02/2010  Robert Henderson 1. Program Validation
  --                                            2. Rebate Validation
  --    1.5        09/28/2010  Kathy Poling     Added wait to submit_fae so we could send 
  --                                            notification when process is complete 
  --    1.6        01/23/2012  Kathy Poling     Changes made end date of the program and
  --                                            offer to allow the programs to current day
  --                                            when working on previous years data.  Also
  --                                            adding order date and end date to allow 
  --                                            the offer to calculate for the correct period.
  --                                            RFC 32857     
  --    1.7        04/09/2012  Chandra Gadge    Added the Function to be used in Pricing Attribute
  --                                            Management for HDS Supplier Category 
  --    1.8        06/01/2012  Chandra Gadge    Added the Custom Function for State Qualifier
  --    1.9        06/07/2012  Chandra Gadge    Changes made for Currency Code Validation in case of Global Agreements
  --    1.10       06/27/2012  Chandra Gadge    1.Added the Validation for Beneficiary id of Type HDS_MVID
  --                                            2.Procedure to update MVID Collectors
  --    1.11       01/14/2014  Balaguru Seshadri   1.Add a new parameter calendar year to rebate_mass_copy [ESMS 235410 ]
  --    1.11       01/14/2014  Balaguru Seshadri   2.Added offer_id to update_rebate procedure [ESMS 235410 ]
  --    1.11       01/14/2014  Balaguru Seshadri   1.Added log messages to procedures rebate_mass_copy, update_rebate and start_copy [ESMS 235410 ]    
  --    2.0        07/22/2014  Kathy Poling     Corrected Canada parm for TM Recon JE RFC 41043 
  --                                            in procedures "monday_request_set" "saturday_request_set
  --                                            and "tues_fri_request_set"
  --    2.1        08/12/2014  Kathy Poling     SR 258560 RFC 41267 Change in procedure "monday_request_set" "saturday_request_set
  --                                            and "tues_fri_request_set"
  --    2.2        8/22/2014     Kathy Poling      RFC 41267 remove Subledger Accounting 
  --                                            Program from procedure monday_request_set   
  --    1.14       11/20/2014  Balaguru Seshadri ESMS 271028 -Add new routine fix_mass_copy_fund_issue to fix mass copy issue
  --    1.15       11/21/2014  Balaguru Seshadri ESMS 270170 -Update routine update_mvid_collectors to add an extra filter for site_use_id is null
  --                                                          as rebates collectors are looked up at the account level and not site level.    
  --   
  --    1.16      02/13/2015  RFC 42900 /ESMS 269624  Changes to the routines monday_request_set and tue_fri_request_set.
  --    1.17      03/04/2015  RFC 43009  ESMS 280366  Add YTD Income extract stage to tuesday, wed-fri and saturday request sets                                
  --    Notes:
  --
  --    Overview: This package contains procedure Rebate Mass Copy,Rebate Adjustments
  --              Creation of Programs, Program and Rebate Validations, Submit FAE, Submit resale interface purge,
  --    1.18      04/07/2015   Bala Seshadri    ESMS 283148. Add  stage EXTRACT_GL_SUMMARY [Sequence 68]   
  --              Submit budget update, submit tpa, submit Rebate request sets
  --   1.19      06/29/2015   ESMS 290257      Comment code for budget approval forward issue. 
  --   1.20      11/12/2015   TMS: 20151217-00142  Add function get_hds_rbt_div_id to be used as a rebateable qualifier for offers or otherwise by "Division"
  --                          Child: TMS 20151217-00142 , Parent: TMS  20151008-00085 
  -- ------------------------------------------------------------------------------
  PROCEDURE print_out(p_buffer VARCHAR2) IS
  BEGIN
    fnd_file.put_line(fnd_file.output, p_buffer);
    --  dbms_output.put_line(p_buffer);
  END print_out;
  --
  PROCEDURE print_log(p_buffer VARCHAR2) IS
  BEGIN
    fnd_file.put_line(fnd_file.log, p_buffer);
    --  dbms_output.put_line(p_buffer);
  END print_log;
  -- Update Existing Stock Receipt Lines with Product/Location Segments as one time data fix
  PROCEDURE update_all_seg(errbuf     IN OUT VARCHAR2
                          ,retcode    IN OUT VARCHAR2
                          ,p_lob_name IN VARCHAR2) AS
  
    CURSOR c_update_receipt(cv_lob_name IN VARCHAR2) IS
      SELECT orl.line_attribute2 bu_nm
            ,rct.shp_to_brnch_cd
            ,orl.resale_line_id
            ,rct.vndr_recpt_unq_nbr
        FROM ozf_resale_lines_all                orl
            ,xxcus.xxcus_rebate_receipt_curr_tbl rct
      --xxcus.xxcus_rebate_receipt_tbl rct
       WHERE rct.vndr_recpt_unq_nbr(+) = orl.line_attribute5
         AND rct.bu_nm(+) = orl.line_attribute2
         AND (orl.line_attribute11 IS NULL OR orl.line_attribute12 IS NULL)
         AND orl.line_attribute2 = cv_lob_name;
    --AND    rct.vndr_recpt_unq_nbr = 'PE04233091.8*5';
    --AND    rct.shp_to_brnch_cd = '71';
  
    l_cnt          NUMBER := 1;
    l_entrp_entity VARCHAR2(10);
    l_entrp_loc    VARCHAR2(10);
    l_prod_segment VARCHAR2(20);
    l_loc_segment  VARCHAR2(20);
    cv_lob_name    VARCHAR2(40);
  
  BEGIN
  
    print_log('p_lob_name' || p_lob_name);
  
    FOR i IN c_update_receipt(p_lob_name)
    LOOP
    
      --print_log('Before Call to get_gl_segments function');
    
      xxcus_tm_interface_pkg.get_gl_segments(i.bu_nm
                                            ,i.shp_to_brnch_cd
                                            ,l_prod_segment
                                            ,l_loc_segment);
    
      --print_log('End Call to get_gl_segments function');  
    
      --print_log('l_loc_segment'||l_loc_segment); 
      --print_log('l_prod_segment'||l_prod_segment); 
    
      UPDATE ozf_resale_lines_all orl
         SET line_attribute12 = l_loc_segment
            ,line_attribute11 = l_prod_segment
            ,line_attribute6  = i.shp_to_brnch_cd
       WHERE 1 = 1
            --AND   orl.line_attribute5 = i.vndr_recpt_unq_nbr
            --AND   orl.line_attribute2 = i.bu_nm
         AND orl.resale_line_id = i.resale_line_id;
      --print_log('After Update');  
    
      l_cnt := l_cnt + 1;
    
      IF l_cnt = 100000
      THEN
        COMMIT;
        l_cnt := 1;
      END IF;
    
    END LOOP;
  
  EXCEPTION
    WHEN OTHERS THEN
      ozf_utility_pvt.write_conc_log('Error is ' || SQLERRM);
      retcode := 2;
  END update_all_seg;

  PROCEDURE create_fund(p_fund_id     IN NUMBER
                       ,p_in_fund_id  IN NUMBER
                       ,p_out_fund_id OUT NUMBER) IS
  
    l_fund_rec_type  ozf_funds_pvt.fund_rec_type;
    l_fund_rec_type1 ozf_funds_pvt.fund_rec_type;
  
    l_modifier_list_rec ozf_offer_pub.modifier_list_rec_type;
    l_modifier_line_tbl ozf_offer_pub.modifier_line_tbl_type;
    l_vo_pbh_tbl        ozf_offer_pub.vo_disc_tbl_type;
    l_vo_dis_tbl        ozf_offer_pub.vo_disc_tbl_type;
    l_vo_prod_tbl       ozf_offer_pub.vo_prod_tbl_type;
    l_qualifier_tbl     ozf_offer_pub.qualifiers_tbl_type;
    l_vo_mo_tbl         ozf_offer_pub.vo_mo_tbl_type;
  
    x_return_status VARCHAR2(1);
    x_msg_count     NUMBER;
    x_msg_data      VARCHAR2(4000);
  
    x_fund_id NUMBER;
  
    l_start_date      DATE;
    l_end_date        DATE;
    l_fund_name       ozf_funds_all_tl.short_name%TYPE;
    l_currency_code   VARCHAR2(10);
    l_category_id     NUMBER;
    l_custom_setup_id NUMBER;
    l_ledger_id       NUMBER;
    l_owner_id        NUMBER;
    l_fund_id         NUMBER := p_fund_id;
    l_original_budget NUMBER;
    l_trans_in_amount NUMBER;
    l_obj_version     NUMBER;
    l_fund_type       VARCHAR2(50);
  
    CURSOR c_fund(p_fund_id IN NUMBER) IS
      SELECT ofa.fund_type
            ,ofa.start_date_active
            ,ofa.end_date_active
            ,oft.short_name
            ,ofa.category_id
            ,ofa.custom_setup_id
            ,ofa.ledger_id
            ,ofa.owner
            ,ofa.currency_code_tc
            ,ofa.original_budget
            ,ofa.transfered_in_amt
        FROM ozf_funds_all_b ofa, ozf_funds_all_tl oft
       WHERE ofa.fund_id = oft.fund_id
         AND ofa.fund_id = p_fund_id;
  
    CURSOR get_obj_ver(p_fund_id IN NUMBER) IS
      SELECT object_version_number
        FROM ozf_funds_all_b
       WHERE fund_id = p_fund_id;
  
  BEGIN
    fnd_msg_pub.initialize;
  
    --dbms_output.put_line('In Parent Fund Create');
  
    OPEN c_fund(l_fund_id);
    FETCH c_fund
      INTO l_fund_type
          ,l_start_date
          ,l_end_date
          ,l_fund_name
          ,l_category_id
          ,l_custom_setup_id
          ,l_ledger_id
          ,l_owner_id
          ,l_currency_code
          ,l_original_budget
          ,l_trans_in_amount;
    CLOSE c_fund;
  
    --dbms_output.put_line('Opened Cursor');
  
    l_fund_rec_type.fund_type  := l_fund_type;
    l_fund_rec_type.short_name := substr(l_fund_name, 1, 2) ||
                                  to_char(to_number(substr(l_fund_name
                                                          ,3
                                                          ,4)) + 1) || ' ' ||
                                  substr(l_fund_name, 7);
  
    IF l_start_date IS NOT NULL
    THEN
      l_fund_rec_type.start_date_active := add_months(l_start_date, 12);
    END IF;
  
    IF l_end_date IS NOT NULL
    THEN
      l_fund_rec_type.end_date_active := add_months(l_end_date, 12);
    END IF;
  
    l_fund_rec_type.category_id      := l_category_id;
    l_fund_rec_type.status_code      := 'DRAFT';
    l_fund_rec_type.user_status_id   := 2100;
    l_fund_rec_type.owner            := l_owner_id;
    l_fund_rec_type.currency_code_tc := l_currency_code;
    l_fund_rec_type.original_budget  := l_original_budget;
    l_fund_rec_type.ledger_id        := l_ledger_id;
    l_fund_rec_type.custom_setup_id  := l_custom_setup_id;
    l_fund_rec_type.attribute1       := l_fund_id;
  
    IF p_in_fund_id IS NOT NULL
    THEN
      l_fund_rec_type.parent_fund_id  := p_in_fund_id;
      l_fund_rec_type.original_budget := l_trans_in_amount;
    END IF;
  
    --dbms_output.put_line('Trans Amount'||l_fund_rec_type.original_budget);
  
    ozf_funds_pvt.create_fund(p_api_version      => 1.0
                             ,p_init_msg_list    => fnd_api.g_false
                             ,p_commit           => fnd_api.g_false
                             ,p_validation_level => fnd_api.g_valid_level_full
                             ,x_return_status    => x_return_status
                             ,x_msg_count        => x_msg_count
                             ,x_msg_data         => x_msg_data
                             ,p_fund_rec         => l_fund_rec_type
                             ,x_fund_id          => x_fund_id);
  
    IF x_return_status = fnd_api.g_ret_sts_error
    THEN
      RAISE fnd_api.g_exc_error;
    ELSIF x_return_status = fnd_api.g_ret_sts_unexp_error
    THEN
      RAISE fnd_api.g_exc_unexpected_error;
    END IF;
  
    OPEN get_obj_ver(x_fund_id);
    FETCH get_obj_ver
      INTO l_obj_version;
    CLOSE get_obj_ver;
  
    l_fund_rec_type1.fund_id               := x_fund_id;
    l_fund_rec_type1.object_version_number := l_obj_version;
    l_fund_rec_type1.user_status_id        := 2101;
    l_fund_rec_type1.status_code           := 'ACTIVE';
  
    ozf_funds_pvt.update_fund(p_api_version      => 1.0
                             ,p_init_msg_list    => fnd_api.g_false
                             ,p_commit           => fnd_api.g_false
                             ,p_validation_level => fnd_api.g_valid_level_full
                             ,x_return_status    => x_return_status
                             ,x_msg_count        => x_msg_count
                             ,x_msg_data         => x_msg_data
                             ,p_fund_rec         => l_fund_rec_type1
                             ,p_mode             => jtf_plsql_api.g_update);
  
    IF x_return_status = fnd_api.g_ret_sts_error
    THEN
      RAISE fnd_api.g_exc_error;
    ELSIF x_return_status = fnd_api.g_ret_sts_unexp_error
    THEN
      RAISE fnd_api.g_exc_unexpected_error;
    END IF;
  
    p_out_fund_id := x_fund_id;
  
  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);
      ozf_utility_pvt.write_conc_log;
      RAISE;
    
    WHEN fnd_api.g_exc_unexpected_error THEN
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);
      ozf_utility_pvt.write_conc_log;
      RAISE;
    
    WHEN OTHERS THEN
      dbms_output.put_line(substr(SQLERRM, 1, 80));
      ozf_utility_pvt.write_conc_log(substr(SQLERRM, 1, 80));
      RAISE;
    
  END create_fund;
  -- No Longer Used (Obsolete)
  PROCEDURE create_program_hierarchy(errbuf    IN OUT VARCHAR2
                                    ,retcode   IN OUT VARCHAR2
                                    ,p_fund_id IN NUMBER) IS
  
    l_fund_id      NUMBER;
    l_in_fund_id   NUMBER;
    l_out_fund_id  NUMBER;
    l_out_fund_id1 NUMBER;
  
    CURSOR c_child_programs(p_fund_id IN NUMBER) IS
      SELECT fund_id
        FROM ozf_funds_all_b
       WHERE parent_fund_id = p_fund_id
         AND status_code = 'ACTIVE';
  
    CURSOR c_child_programs1(p_fund_id IN NUMBER) IS
      SELECT fund_id
        FROM ozf_funds_all_b
       WHERE parent_fund_id = p_fund_id
         AND status_code = 'ACTIVE';
  
  BEGIN
    SAVEPOINT create_programs;
    retcode := 0;
  
    l_fund_id := p_fund_id;
  
    create_fund(l_fund_id, NULL, l_in_fund_id);
    dbms_output.put_line('Created Parent Fund');
  
    FOR i IN c_child_programs(l_fund_id)
    LOOP
      create_fund(i.fund_id, l_in_fund_id, l_out_fund_id);
      dbms_output.put_line('Created Child Fund' || i.fund_id);
      FOR j IN c_child_programs1(i.fund_id)
      LOOP
        create_fund(j.fund_id, l_out_fund_id, l_out_fund_id1);
        dbms_output.put_line('Created Child Of Child Fund' || j.fund_id);
      END LOOP;
    END LOOP;
  
    UPDATE ozf_funds_all_b SET attribute15 = 'Y' WHERE fund_id = l_fund_id;
  
  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      ROLLBACK TO create_programs;
      retcode := 2;
    WHEN fnd_api.g_exc_unexpected_error THEN
      ROLLBACK TO create_programs;
      retcode := 2;
    WHEN OTHERS THEN
      ROLLBACK TO create_programs;
      retcode := 2;
  END create_program_hierarchy;

  PROCEDURE copy_program(p_budget_id IN NUMBER, p_out_budget_id OUT NUMBER) IS
    l_in_prg_id      NUMBER;
    l_out_prg_id     NUMBER;
    l_program_name   ozf_funds_all_tl.short_name%TYPE;
    l_description    ozf_funds_all_tl.short_name%TYPE;
    l_new_start_date DATE;
    l_new_end_date   DATE;
    l_obj_version    NUMBER;
  
    x_return_status VARCHAR2(1);
    x_msg_count     NUMBER;
    x_msg_data      VARCHAR2(4000);
  
    x_new_object_id   NUMBER;
    x_custom_setup_id NUMBER;
  
    l_attributes_tbl   ams_cpyutility_pvt.copy_attributes_table_type;
    l_copy_columns_tbl ams_cpyutility_pvt.copy_columns_table_type;
    l_fund_rec         ozf_funds_pvt.fund_rec_type;
  
    CURSOR c_program_ref(p_program_id IN NUMBER) IS
      SELECT start_date_active, end_date_active, short_name
        FROM ozf_funds_all_vl
       WHERE fund_id = p_program_id;
  
    CURSOR get_obj_ver(p_fund_id IN NUMBER) IS
      SELECT object_version_number
        FROM ozf_funds_all_b
       WHERE fund_id = p_fund_id;
  
  BEGIN
    --
    l_program_name   := NULL;
    l_new_start_date := NULL;
    l_new_end_date   := NULL;
    --
    l_in_prg_id := p_budget_id;
    --
    OPEN c_program_ref(l_in_prg_id);
    FETCH c_program_ref
      INTO l_new_start_date, l_new_end_date, l_description;
    CLOSE c_program_ref;
    --
    print_log('');
    print_log('Inside copy program, old program =>' || l_description);
    print_log('Inside copy program, current new_start_date =>' ||
              l_new_start_date);
    print_log('Inside copy program, current new_end_date =>' ||
              l_new_end_date);
    --        
    l_program_name   := 'FY' ||
                        to_char(to_number(substr(l_description, 3, 4)) + 1) || '-' ||
                        substr(l_description
                              ,instr(l_description, '-', 4) + 1);
    l_new_start_date := add_months(l_new_start_date, 12);
    l_new_end_date   := add_months(l_new_end_date, 12);
    --
    print_log('Inside copy program, new program =>' || l_program_name);
    print_log('Inside copy program, added 12 months to new_start_date =>' ||
              l_new_start_date);
    print_log('Inside copy program, added 12 months to new_end_date =>' ||
              l_new_end_date);
    --
    IF (l_program_name IS NOT NULL AND l_new_start_date IS NOT NULL)
    THEN
      --Version 1.6         
      --
      IF l_new_end_date < trunc(SYSDATE)
      THEN
        l_new_end_date := trunc(SYSDATE);
      ELSE
        NULL;
      END IF;
      --      
      print_log('Before copy program, value of enddate modified after check =>' ||
                l_new_end_date);
      print_log('');
    
      l_copy_columns_tbl(1).column_name := 'newObjName';
      l_copy_columns_tbl(1).column_value := l_program_name;
    
      l_copy_columns_tbl(2).column_name := 'startDate';
      l_copy_columns_tbl(2).column_value := l_new_start_date;
    
      l_copy_columns_tbl(3).column_name := 'endDate';
      l_copy_columns_tbl(3).column_value := l_new_end_date;
    
      --l_copy_columns_tbl(5).column_name := 'ownerId';
      --l_copy_columns_tbl(5).column_value := l_owner_id;
    
      l_copy_columns_tbl(4).column_name := 'description';
      l_copy_columns_tbl(4).column_value := l_program_name;
    
      ozf_funds_pvt.copy_fund(p_api_version        => 1.0
                             ,p_init_msg_list      => fnd_api.g_false
                             ,p_commit             => fnd_api.g_false
                             ,p_validation_level   => fnd_api.g_valid_level_full
                             ,x_return_status      => x_return_status
                             ,x_msg_count          => x_msg_count
                             ,x_msg_data           => x_msg_data
                             ,p_source_object_id   => l_in_prg_id
                             ,p_attributes_table   => l_attributes_tbl
                             ,p_copy_columns_table => l_copy_columns_tbl
                             ,x_new_object_id      => x_new_object_id
                             ,x_custom_setup_id    => x_custom_setup_id);
    
      print_log('@ozf_funds_pvt.copy_fund, new_budget_id =' ||
                x_new_object_id);
    
      IF nvl(x_return_status, 'X') != 'S'
      THEN
        fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                                 ,p_count   => x_msg_count
                                 ,p_data    => x_msg_data);
        ozf_utility_pvt.write_conc_log;
        RETURN;
      ELSE
        OPEN get_obj_ver(x_new_object_id);
        FETCH get_obj_ver
          INTO l_obj_version;
        CLOSE get_obj_ver;
        --
        l_fund_rec.fund_id               := x_new_object_id;
        l_fund_rec.object_version_number := l_obj_version;
        l_fund_rec.user_status_id        := 2101;
        l_fund_rec.status_code           := 'ACTIVE';
        --
        l_fund_rec.end_date_active := l_new_end_date; --Bala...01/09/2014       
        --      
        ozf_funds_pvt.update_fund(p_api_version      => 1.0
                                 ,p_init_msg_list    => fnd_api.g_false
                                 ,p_commit           => fnd_api.g_false
                                 ,p_validation_level => fnd_api.g_valid_level_full
                                 ,x_return_status    => x_return_status
                                 ,x_msg_count        => x_msg_count
                                 ,x_msg_data         => x_msg_data
                                 ,p_fund_rec         => l_fund_rec
                                 ,p_mode             => jtf_plsql_api.g_update);
      
        IF nvl(x_return_status, 'X') != 'S'
        THEN
          fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                                   ,p_count   => x_msg_count
                                   ,p_data    => x_msg_data);
          ozf_utility_pvt.write_conc_log;
          RETURN;
        ELSE
          UPDATE ozf_funds_all_b
             SET attribute1 = l_in_prg_id
           WHERE fund_id = x_new_object_id;
        
          p_out_budget_id := x_new_object_id;
        END IF;
      END IF;
    END IF;
    --print_log('After copy program -' || l_description);      
    print_log('');
  EXCEPTION
    WHEN OTHERS THEN
      RAISE;
    
  END copy_program;
  --
  PROCEDURE update_rebate(p_rebate_id      IN NUMBER
                         ,p_from_rebate_id IN NUMBER
                         ,p_global_flag    IN VARCHAR2
                         ,p_org_id         IN NUMBER
                         ,p_offer_id       IN NUMBER
                         ,x_return_status  OUT VARCHAR2) IS
  
    l_return_status     VARCHAR2(100);
    l_msg_count         NUMBER;
    l_msg_data          VARCHAR2(100);
    l_source_object_id  NUMBER := p_rebate_id;
    l_new_object_id     NUMBER;
    l_custom_setup_id   NUMBER;
    l_qp_list_header_id NUMBER;
    l_error_location    VARCHAR2(2000);
  
    l_offer_type             VARCHAR2(30);
    l_modifier_list_rec_type ozf_offer_pub.modifier_list_rec_type;
    l_modifier_line_tbl_type ozf_offer_pub.modifier_line_tbl_type;
    l_qualifiers_tbl_type    ozf_offer_pub.qualifiers_tbl_type;
    l_budget_tbl_type        ozf_offer_pub.budget_tbl_type;
    l_act_product_tbl_type   ozf_offer_pub.act_product_tbl_type;
    l_discount_line_tbl_type ozf_offer_pub.discount_line_tbl_type;
    l_excl_rec_tbl_type      ozf_offer_pub.excl_rec_tbl_type;
    l_offer_tier_tbl_type    ozf_offer_pub.offer_tier_tbl_type;
    l_prod_rec_tbl_type      ozf_offer_pub.prod_rec_tbl_type;
    l_na_qualifier_tbl_type  ozf_offer_pub.na_qualifier_tbl_type;
  
    l_start_date_active_first DATE;
    l_end_date_active_first   DATE;
    l_calendar_year           NUMBER;
    l_auto_renew              VARCHAR2(150);
    l_until_year              VARCHAR2(150);
    l_pmt_method              VARCHAR2(150);
    l_pmt_frequency           VARCHAR2(150);
    l_back_to_dollar          VARCHAR2(150);
    l_request_id              NUMBER := nvl(fnd_global.conc_request_id, -1);
  
    CURSOR c_get_dates(p_list_header_id IN NUMBER) IS
      SELECT start_date_active_first
            ,end_date_active_first
            ,attribute7 --CAL YEAR
            ,attribute1 --AUTO RENEW
            ,attribute2 --UNTIL YEAR
            ,attribute5 --PAYMENT METHOD
            ,attribute6 --PAYMENT FREQUENCY
            ,attribute9 --BACK_TO_DOLLAR
        FROM qp_list_headers_vl
       WHERE list_header_id = p_list_header_id; --p_rebate_id; --01/07/2014
  
  BEGIN
    --     
    OPEN c_get_dates(p_list_header_id => p_from_rebate_id);
    --OPEN c_get_dates(l_source_object_id); --01/07/2014 -This is commented out bcoz it is using attributes from the new offer copied. not the source.
    --The new offer copied will not have all attributes at this time. So we are going to use the variable p_from_rebate_id
    FETCH c_get_dates
      INTO l_start_date_active_first
          ,l_end_date_active_first
          ,l_calendar_year
          ,l_auto_renew
          ,l_until_year
          ,l_pmt_method
          ,l_pmt_frequency
          ,l_back_to_dollar;
    CLOSE c_get_dates;
  
    l_modifier_list_rec_type.qp_list_header_id  := l_source_object_id;
    l_modifier_list_rec_type.offer_id           := p_offer_id;
    l_modifier_list_rec_type.offer_operation    := 'UPDATE';
    l_modifier_list_rec_type.modifier_operation := 'UPDATE';
    --  
    l_modifier_list_rec_type.start_date_active_first := add_months(l_start_date_active_first
                                                                  ,12);
    --                                                                  
    l_modifier_list_rec_type.end_date_active_first := add_months(l_end_date_active_first
                                                                ,12);
    --                                                                  
    l_modifier_list_rec_type.attribute1 := l_auto_renew;
    l_modifier_list_rec_type.attribute2 := l_until_year;
    l_modifier_list_rec_type.attribute5 := l_pmt_method;
    l_modifier_list_rec_type.attribute6 := l_pmt_frequency;
    l_modifier_list_rec_type.attribute7 := to_number(l_calendar_year) + 1;
    l_modifier_list_rec_type.attribute8 := p_from_rebate_id;
    l_modifier_list_rec_type.attribute9 := l_back_to_dollar;
    --l_modifier_list_rec_type.attribute15 :=l_request_id; --Store the concurrent request id that created the offer for future reference        
    print_log('');
    print_log('p_offer_id =' || p_offer_id);
    print_log('Before update of attributes for the copied list_header_id =' ||
              l_modifier_list_rec_type.qp_list_header_id);
    print_log('start_date_active_first =' ||
              l_modifier_list_rec_type.start_date_active_first);
    print_log('end_date_active_first =' ||
              l_modifier_list_rec_type.end_date_active_first);
    print_log('l_auto_renew =' || l_auto_renew);
    print_log('l_until_year =' || l_until_year);
    print_log('l_pmt_method =' || l_pmt_method);
    print_log('l_pmt_frequency =' || l_pmt_frequency);
    print_log('l_calendar_year =' || l_modifier_list_rec_type.attribute7);
    print_log('p_from_rebate_id =' || p_from_rebate_id);
    print_log('l_back_to_dollar =' || l_back_to_dollar);
    print_log('');
    ozf_offer_pub.process_modifiers(p_api_version       => 1.0
                                   ,p_init_msg_list     => fnd_api.g_false
                                   ,p_commit            => fnd_api.g_false
                                   ,x_return_status     => l_return_status
                                   ,x_msg_count         => l_msg_count
                                   ,x_msg_data          => l_msg_data
                                   ,p_offer_type        => l_offer_type
                                   ,p_modifier_list_rec => l_modifier_list_rec_type
                                   ,p_modifier_line_tbl => l_modifier_line_tbl_type
                                   ,p_qualifier_tbl     => l_qualifiers_tbl_type
                                   ,p_budget_tbl        => l_budget_tbl_type
                                   ,p_act_product_tbl   => l_act_product_tbl_type
                                   ,p_discount_tbl      => l_discount_line_tbl_type
                                   ,p_excl_tbl          => l_excl_rec_tbl_type
                                   ,p_offer_tier_tbl    => l_offer_tier_tbl_type
                                   ,p_prod_tbl          => l_prod_rec_tbl_type
                                   ,p_na_qualifier_tbl  => l_na_qualifier_tbl_type
                                   ,x_qp_list_header_id => l_qp_list_header_id
                                   ,x_error_location    => l_error_location);
    --                                     
    x_return_status := l_return_status;
    --
    print_log('After update of attributes for the copied offer, return_status =' ||
              l_return_status);
    --
    IF l_return_status <> 'S'
    THEN
      print_log('l_msg_count =' || l_msg_count || ', l_msg_data =' ||
                l_msg_data);
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => l_msg_count
                               ,p_data    => l_msg_data);
      FOR i IN 0 .. l_msg_count
      LOOP
        print_log('@Update offer attributes, ' ||
                  substr(fnd_msg_pub.get(p_msg_index => i
                                        ,p_encoded   => 'F')
                        ,1
                        ,254));
      END LOOP;
    END IF;
    --     
    print_log('');
  EXCEPTION
    WHEN OTHERS THEN
      print_log('@ozf_offer_pub.process_modifiers, p_rebate_id =' ||
                l_source_object_id);
      print_log('@ozf_offer_pub.process_modifiers, p_from_rebate_id =' ||
                p_from_rebate_id);
      print_log('@ozf_offer_pub.process_modifiers, Error' || SQLERRM);
      RETURN;
    
  END update_rebate;

  PROCEDURE start_copy(errbuf           IN OUT VARCHAR2
                      ,retcode          IN OUT VARCHAR2
                      ,p_list_header_id IN NUMBER
                      ,p_mode           IN VARCHAR2) IS
  
    l_list_header_id NUMBER;
    x_return_status  VARCHAR2(1);
    x_msg_count      NUMBER;
    x_msg_data       VARCHAR2(4000);
  
    l_attributes_tbl   ams_cpyutility_pvt.copy_attributes_table_type;
    l_copy_columns_tbl ams_cpyutility_pvt.copy_columns_table_type;
  
    l_act_budgets_rec ozf_actbudgets_pvt.act_budgets_rec_type;
  
    x_new_object_id   NUMBER;
    x_custom_setup_id NUMBER;
  
    l_offer_name VARCHAR2(100);
    l_cyear      VARCHAR2(4);
    l_nyear      VARCHAR2(4);
  
    l_new_start_date  DATE;
    l_new_end_date    DATE;
    l_new_start_first DATE; --Version 1.6
    l_new_end_first   DATE; --Version 1.6
  
    l_ren_year       VARCHAR2(4);
    l_budget_id      NUMBER;
    l_request_amount NUMBER;
  
    l_activity_budget_id NUMBER;
    x_status_code        VARCHAR2(50);
  
    l_start_date                  DATE;
    l_end_date                    DATE;
    l_start_date_first            DATE; --Version 1.6
    l_end_date_first              DATE; --Version 1.6
    l_owner_id                    NUMBER;
    l_year                        NUMBER;
    l_description                 VARCHAR2(100);
    l_auto_renew                  VARCHAR2(10);
    l_budget_amount               NUMBER;
    l_obj_version_number          NUMBER;
    l_budget_source_id            NUMBER;
    l_req_currency                VARCHAR2(10);
    l_new_budget_id               NUMBER;
    l_comments                    VARCHAR2(2000); --Version 1.6
    n_count                       NUMBER;
    l_amount                      NUMBER;
    l_cal_year                    NUMBER;
    l_global_flag                 VARCHAR2(1);
    l_cyear_global_processed_flag VARCHAR2(1) := NULL;
    l_auto_pay_enabled            VARCHAR2(1) := NULL;
    l_proceed                     BOOLEAN;
    l_org_id                      NUMBER;
    l_offer_id                    NUMBER;
  
    CURSOR c_rebates(p_list_header_id IN NUMBER) IS
      SELECT qlhv.list_header_id
            ,qlhv.attribute2
            ,qlhv.description
            ,qlhv.start_date_active
            ,qlhv.end_date_active
            ,oo.owner_id
            ,nvl(qlhv.attribute1, 'NA')
            ,oo.budget_amount_tc
            ,oo.budget_source_id
            ,nvl(qlhv.start_date_active_first, qlhv.start_date_active) start_date_active_first --Version 1.6
            ,nvl(qlhv.end_date_active_first, qlhv.end_date_active) end_date_active_first --Version 1.6
            ,qlhv.comments
            ,qlhv.attribute7
            ,qlhv.global_flag global_flag
            ,oo.org_id
        FROM qp_list_headers_vl qlhv, ozf_offers oo
       WHERE 1 = 1
         AND oo.qp_list_header_id = qlhv.list_header_id
         AND oo.qp_list_header_id = p_list_header_id;
  
    CURSOR c_budgets(p_offer_id IN NUMBER) IS
      SELECT budget_source_id
            ,request_amount
            ,activity_budget_id
            ,object_version_number
            ,request_currency
        FROM ozf_act_budgets
       WHERE act_budget_used_by_id = p_offer_id
         AND arc_act_budget_used_by = 'OFFR'
         AND budget_source_type = 'FUND'
         AND transfer_type = 'REQUEST'
         AND status_code = 'NEW'; --commented on 01/09/2014
  
    CURSOR c_budgets2(p_offer_id IN NUMBER) IS
      SELECT budget_source_id
            ,request_amount
            ,activity_budget_id
            ,object_version_number
            ,request_currency
        FROM ozf_act_budgets
       WHERE act_budget_used_by_id = p_offer_id
         AND arc_act_budget_used_by = 'OFFR'
         AND budget_source_type = 'FUND'
         AND transfer_type = 'REQUEST'
         AND status_code = 'APPROVED';
  
    CURSOR c_budgets3(p_offer_id IN NUMBER) IS
      SELECT budget_source_id
        FROM ozf_act_budgets
       WHERE act_budget_used_by_id = p_offer_id
         AND arc_act_budget_used_by = 'OFFR'
         AND budget_source_type = 'FUND'
         AND transfer_type = 'REQUEST'
         AND status_code = 'APPROVED';
  
    CURSOR c_new_budget(p_budget_id IN NUMBER) IS
      SELECT fund_id
        FROM ozf_funds_all_b
       WHERE 1 = 1
         AND to_number(attribute1) = p_budget_id;
  
  BEGIN
    print_log('Inside start_copy, org_id =' ||
              mo_global.get_current_org_id);
    print_log('Before cursor c_rebates, p_list_header_id =' ||
              p_list_header_id);
    OPEN c_rebates(p_list_header_id);
    FETCH c_rebates
      INTO l_list_header_id
          ,l_year
          ,l_description
          ,l_start_date
          ,l_end_date
          ,l_owner_id
          ,l_auto_renew
          ,l_budget_amount
          ,l_budget_source_id
          ,l_start_date_first --Version 1.6
          ,l_end_date_first --Version 1.6
          ,l_comments --Version 1.6
          ,l_cal_year
          ,l_global_flag
          ,l_org_id;
    CLOSE c_rebates;
    print_log('After cursor c_rebates, l_list_header_id =' ||
              l_list_header_id);
    l_offer_name      := NULL;
    l_new_start_date  := NULL;
    l_new_end_date    := NULL;
    l_new_start_first := NULL; --Version 1.6
    l_new_end_first   := NULL; --Version 1.6
  
    --l_cyear := to_char(to_date(l_start_date), 'YYYY');
    --l_nyear := to_char(to_date(l_start_date), 'YYYY') + 1;
  
    l_cyear := l_cal_year;
    l_nyear := l_cal_year + 1;
  
    IF p_mode = 'M'
    THEN
    
      print_log('Source List_Header_Id =' || l_list_header_id);
    
      IF (l_auto_renew = 'Y')
      THEN
        --Auto renew is YES.
        --
        IF (l_year IS NULL)
        THEN
          --These offers are Perpetual agreements. We will copy them every year until further notice.
          -- Just copy the offer with auto pay enabled
          -- 
          l_auto_pay_enabled := 'Y';
          l_proceed          := TRUE;
          --
        ELSE
          --(l_auto_renew ='Y') AND (L_YEAR IS NOT NULL) 
          --
          IF l_nyear <= to_number(l_year)
          THEN
            l_auto_pay_enabled := 'Y';
          ELSE
            l_auto_pay_enabled := 'N';
          END IF;
          --
          l_proceed := TRUE;
          --
        END IF;
        --
      ELSIF (l_auto_renew = 'N')
      THEN
        --Auto renew is NO. 
        --
        IF (l_year IS NULL)
        THEN
          --
          IF (l_global_flag = 'Y')
          THEN
            BEGIN
              SELECT COUNT(1)
                INTO n_count
                FROM qp_list_headers_vl
               WHERE 1 = 1
                 AND global_flag = 'Y'
                 AND attribute8 = l_list_header_id --check offers where from rebate =copied from agreement
                 AND attribute7 = l_nyear;
              IF n_count > 0
              THEN
                l_proceed                     := FALSE;
                l_cyear_global_processed_flag := 'Y';
              ELSE
                l_cyear_global_processed_flag := 'N';
                l_proceed                     := TRUE;
              END IF;
            EXCEPTION
              WHEN OTHERS THEN
                print_log('Issue in global copy, list_header_id =' ||
                          l_list_header_id || ', message =' || SQLERRM);
                l_proceed := FALSE;
            END;
          ELSE
            l_proceed := TRUE;
          END IF;
          --
        ELSE
          --
          l_proceed := TRUE;
          --
        END IF;
        -- Regardless of until year [attribute2] if the auto renew is N then autopay is disabled.
        l_auto_pay_enabled := 'N';
        --
      ELSIF (l_auto_renew = 'NA')
      THEN
        --Auto renew is BLANK.
        --
        l_auto_pay_enabled := 'N';
        l_proceed          := TRUE;
        --
      ELSE
        print_log('@ scenario 4');
        l_proceed := FALSE;
        --                    
      END IF;
      --       
      l_offer_name     := 'FY' || l_nyear || '-' ||
                          substr(l_description
                                ,instr(l_description, '-', 4) + 1);
      l_new_start_date := add_months(l_start_date, 12);
      l_new_end_date   := add_months(l_end_date, 12);
      --
      l_new_start_first := add_months(l_start_date_first, 12); --Version 1.6
      l_new_end_first   := add_months(l_end_date_first, 12); --Version 1.6
      --
      print_log('Before copy offer, l_org_id =>' || l_org_id);
      print_log('Before copy offer, l_offer_id =>' || l_offer_id);
      print_log('Before copy offer, l_start_date =>' ||
                to_char(l_start_date, 'DD-MON-YYYY'));
      print_log('Before copy offer, l_end_date =>' ||
                to_char(l_end_date, 'DD-MON-YYYY'));
      print_log('Before copy offer, l_new_start_date =>' ||
                to_char(l_new_start_date, 'DD-MON-YYYY'));
      print_log('Before copy offer, l_new_end_date =>' ||
                to_char(l_new_end_date, 'DD-MON-YYYY'));
      print_log('Before copy offer, l_new_start_first =>' ||
                to_char(l_new_start_first, 'DD-MON-YYYY'));
      print_log('Before copy offer, l_new_end_first =>' ||
                to_char(l_new_end_first, 'DD-MON-YYYY'));
    ELSIF p_mode = 'C'
    THEN
      print_log('HDS Mass Copy: Value p_mode =>C');
      l_offer_name      := l_description;
      l_new_start_date  := l_start_date;
      l_new_end_date    := l_end_date;
      l_new_start_first := l_start_date_first; --Version 1.6        
      l_new_end_first   := l_end_date_first; --Version 1.6
      l_proceed         := TRUE;
    ELSE
      print_log('HDS Mass Copy: Unknown mode. Current value is not a M or a C');
      l_proceed := FALSE;
    END IF;
  
    IF ((l_proceed) AND l_offer_name IS NOT NULL AND
       l_new_start_date IS NOT NULL AND l_new_end_date IS NOT NULL)
    THEN
      print_log('Variable L_PROCEED is TRUE, Inside copy offer logic....');
      --Version 1.6    
      IF l_new_end_date < trunc(SYSDATE)
      THEN
        l_new_end_date := trunc(SYSDATE);
      END IF;
      --
      l_copy_columns_tbl(1).column_name := 'newObjName';
      l_copy_columns_tbl(1).column_value := l_offer_name;
    
      l_copy_columns_tbl(2).column_name := 'offerCode';
      l_copy_columns_tbl(2).column_value := NULL;
    
      l_copy_columns_tbl(3).column_name := 'startDateActive';
      l_copy_columns_tbl(3).column_value := l_new_start_date;
    
      l_copy_columns_tbl(4).column_name := 'endDateActive';
      l_copy_columns_tbl(4).column_value := l_new_end_date;
    
      l_copy_columns_tbl(5).column_name := 'ownerId';
      l_copy_columns_tbl(5).column_value := l_owner_id;
    
      l_copy_columns_tbl(6).column_name := 'description';
      l_copy_columns_tbl(6).column_value := l_comments; --l_offer_name; --Version 1.6
    
      l_copy_columns_tbl(7).column_name := 'start_date_active_first'; --Version 1.6
      l_copy_columns_tbl(7).column_value := l_new_start_first; --Version 1.6
    
      l_copy_columns_tbl(8).column_name := 'end_date_active_first'; --Version 1.6
      l_copy_columns_tbl(8).column_value := l_new_end_first; --Version 1.6
    
      l_attributes_tbl(1) := 'ATCH';
      l_attributes_tbl(2) := 'ELIG';
      l_attributes_tbl(3) := 'MKT_OPT';
      l_attributes_tbl(4) := 'CPNT';
      l_attributes_tbl(5) := 'TEAM';
    
      ozf_copy_offer_pvt.copy_offer(p_api_version        => 1.0
                                   ,p_init_msg_list      => fnd_api.g_false
                                   ,p_commit             => fnd_api.g_false
                                   ,p_validation_level   => fnd_api.g_valid_level_full
                                   ,x_return_status      => x_return_status
                                   ,x_msg_count          => x_msg_count
                                   ,x_msg_data           => x_msg_data
                                   ,p_source_object_id   => l_list_header_id
                                   ,p_attributes_table   => l_attributes_tbl
                                   ,p_copy_columns_table => l_copy_columns_tbl
                                   ,x_new_object_id      => x_new_object_id
                                   ,x_custom_setup_id    => x_custom_setup_id);
    
      IF nvl(x_return_status, 'X') != 'S'
      THEN
        print_log('API Failed, Status Code =' || x_return_status);
        print_log('x_msg_count =' || x_msg_count || ', x_msg_data =' ||
                  x_msg_data);
        fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                                 ,p_count   => x_msg_count
                                 ,p_data    => x_msg_data);
        ozf_utility_pvt.write_conc_log;
        FOR i IN 0 .. x_msg_count
        LOOP
          print_log(substr(fnd_msg_pub.get(p_msg_index => i
                                          ,p_encoded   => 'F')
                          ,1
                          ,254));
        END LOOP;
        retcode := 2;
        RETURN;
      ELSE
        --
        print_log('API: ozf_copy_offer_pvt.copy_offer, Action: Success, List_header_id =>' ||
                  x_new_object_id);
        --       
        BEGIN
          --
          l_offer_id := 0;
          --
          SELECT offer_id
            INTO l_offer_id
            FROM ozf_offers
           WHERE 1 = 1
             AND qp_list_header_id = x_new_object_id;
          --
        EXCEPTION
          WHEN no_data_found THEN
            l_offer_id := 0;
          WHEN OTHERS THEN
            l_offer_id := 0;
        END;
        -- Moved update_rebate here instead of down the line.
        --
        BEGIN
          --  
          IF (x_new_object_id IS NOT NULL AND x_new_object_id > 0)
          THEN
            update_rebate(p_rebate_id      => x_new_object_id
                         ,p_from_rebate_id => l_list_header_id
                         ,p_global_flag    => l_global_flag
                         ,p_org_id         => l_org_id
                         ,p_offer_id       => l_offer_id
                         ,x_return_status  => x_return_status);
          ELSE
            print_log('Failed to invoke update_rebate routine because x_new_object_id is BLANK or ZERO');
          END IF;
          --                
        EXCEPTION
          WHEN OTHERS THEN
            print_log('@update_rebate, old list_header_id =' ||
                      l_list_header_id || ', new list_header_id =' ||
                      x_new_object_id);
            print_log('@update_rebate, error message: ' || SQLERRM);
        END;
        --       
        BEGIN
          OPEN c_budgets(p_offer_id => x_new_object_id);
          --OPEN c_budgets(x_new_object_id);
          --OPEN c_budgets(p_offer_id =>l_list_header_id);        
          FETCH c_budgets
            INTO l_budget_id
                ,l_request_amount
                ,l_activity_budget_id
                ,l_obj_version_number
                ,l_req_currency;
          CLOSE c_budgets;
          --       
        EXCEPTION
          WHEN no_data_found THEN
            l_budget_id          := NULL;
            l_request_amount     := NULL;
            l_activity_budget_id := NULL;
            l_obj_version_number := NULL;
            l_req_currency       := NULL;
          WHEN OTHERS THEN
            print_log('Cursor c_budgets, error found for act_budget_used_by_id =' ||
                      x_new_object_id || ', message =' || SQLERRM);
            print_log('Cursor c_budgets, error_message =' || SQLERRM);
            l_budget_id          := NULL;
            l_request_amount     := NULL;
            l_activity_budget_id := NULL;
            l_obj_version_number := NULL;
            l_req_currency       := NULL;
        END;
        --
        print_log('@Cursor c_budgets, l_budget_id   =>' || l_budget_id);
        print_log('@Cursor c_budgets, l_activity_budget_id =>' ||
                  l_activity_budget_id);
        --
        IF p_mode = 'M'
        THEN
          --
          l_new_budget_id := NULL;
          --
          print_log('@Before fetch of c_new_budget, l_budget_id =>' ||
                    l_budget_id);
          -- 
          BEGIN
            --
            OPEN c_new_budget(l_budget_id);
            FETCH c_new_budget
              INTO l_new_budget_id;
            CLOSE c_new_budget;
            --
          EXCEPTION
            WHEN no_data_found THEN
              l_new_budget_id := NULL;
            WHEN OTHERS THEN
              print_log('Cursor c_new_budget,WHEN OTHERS, error_message =' ||
                        SQLERRM);
              l_new_budget_id := NULL;
          END;
          print_log('@After fetch of c_new_budget, l_new_budget_id =>' ||
                    l_new_budget_id);
          --
          IF l_new_budget_id IS NULL
          THEN
            --
            print_log('Before copy_program');
            --
            copy_program(l_budget_id, l_new_budget_id);
            --
            print_log('@copy_program, l_budget_id =' || l_budget_id);
            print_log('@copy_program, l_new_budget_id =' ||
                      l_new_budget_id);
            print_log('After copy_program');
            -- 
          END IF;
          --          
          IF l_new_budget_id IS NULL
          THEN
          
            l_new_budget_id := l_budget_id;
          
            print_log('@second check, l_new_budget_id i still blank so use the old one, l_new_budget_id =' ||
                      l_new_budget_id);
          
          END IF;
          --
        ELSIF p_mode = 'C'
        THEN
          --
          OPEN c_budgets3(l_list_header_id);
          FETCH c_budgets3
            INTO l_budget_id;
          CLOSE c_budgets3;
          --
          l_new_budget_id := l_budget_id;
          --
        END IF;
        --
        IF l_activity_budget_id IS NOT NULL
        THEN
        
          l_act_budgets_rec.activity_budget_id     := l_activity_budget_id;
          l_act_budgets_rec.object_version_number  := l_obj_version_number;
          l_act_budgets_rec.act_budget_used_by_id  := x_new_object_id;
          l_act_budgets_rec.budget_source_id       := l_new_budget_id;
          l_act_budgets_rec.request_amount         := l_request_amount;
          l_act_budgets_rec.budget_source_type     := 'FUND';
          l_act_budgets_rec.transfer_type          := 'REQUEST';
          l_act_budgets_rec.arc_act_budget_used_by := 'OFFR';
          l_act_budgets_rec.request_currency       := l_req_currency;
          l_act_budgets_rec.approved_in_currency   := l_req_currency;
        
          ozf_actbudgets_pvt.update_act_budgets(p_api_version      => 1.0
                                               ,p_init_msg_list    => fnd_api.g_false
                                               ,p_commit           => fnd_api.g_false
                                               ,p_validation_level => fnd_api.g_valid_level_full
                                               ,x_return_status    => x_return_status
                                               ,x_msg_count        => x_msg_count
                                               ,x_msg_data         => x_msg_data
                                               ,p_act_budgets_rec  => l_act_budgets_rec);
        ELSE
          --dbms_output.put_line('activity id is  null');
        
          OPEN c_budgets2(l_list_header_id);
          FETCH c_budgets2
            INTO l_budget_id
                ,l_request_amount
                ,l_activity_budget_id
                ,l_obj_version_number
                ,l_req_currency;
          CLOSE c_budgets2;
        
          print_log('CHECK7 :' || l_budget_id);
          print_log('CHECK8 :' || l_activity_budget_id);
        
          IF p_mode = 'M'
          THEN
            OPEN c_new_budget(l_budget_id);
            FETCH c_new_budget
              INTO l_new_budget_id;
            CLOSE c_new_budget;
          
            print_log('CHECK9 :' || l_new_budget_id);
          
            IF l_new_budget_id IS NULL
            THEN
              print_log('CHECK10 =');
              copy_program(l_budget_id, l_new_budget_id);
              print_log('CHECK11 =');
            END IF;
          
            print_log('CHECK12 =' || l_new_budget_id);
          
            IF l_new_budget_id IS NULL
            THEN
              l_new_budget_id := l_budget_id;
            END IF;
          
          ELSIF p_mode = 'C'
          THEN
            l_new_budget_id := l_budget_id;
          END IF;
        
          l_act_budgets_rec.act_budget_used_by_id  := x_new_object_id;
          l_act_budgets_rec.budget_source_id       := l_new_budget_id;
          l_act_budgets_rec.request_amount         := l_request_amount;
          l_act_budgets_rec.budget_source_type     := 'FUND';
          l_act_budgets_rec.transfer_type          := 'REQUEST';
          l_act_budgets_rec.arc_act_budget_used_by := 'OFFR';
          l_act_budgets_rec.request_currency       := l_req_currency;
          l_act_budgets_rec.approved_in_currency   := l_req_currency;
          --
          print_log('Before calling ozf_actbudgets_pvt.create_act_budgets with the new list_header_id ' ||
                    x_new_object_id);
          --
          ozf_actbudgets_pvt.create_act_budgets(p_api_version      => 1.0
                                               ,p_init_msg_list    => fnd_api.g_false
                                               ,p_commit           => fnd_api.g_false
                                               ,p_validation_level => fnd_api.g_valid_level_full
                                               ,x_return_status    => x_return_status
                                               ,x_msg_count        => x_msg_count
                                               ,x_msg_data         => x_msg_data
                                               ,p_act_budgets_rec  => l_act_budgets_rec
                                               ,x_act_budget_id    => l_activity_budget_id);
          print_log('After calling ozf_actbudgets_pvt.create_act_budgets with the new list_header_id ' ||
                    x_new_object_id);
        END IF;
      
        IF nvl(x_return_status, 'X') != 'S'
        THEN
          fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                                   ,p_count   => x_msg_count
                                   ,p_data    => x_msg_data);
          ozf_utility_pvt.write_conc_log;
          print_log('1');
          FOR i IN 0 .. x_msg_count
          LOOP
            print_log('@Issue101, ' || substr(fnd_msg_pub.get(p_msg_index => i
                                                             ,p_encoded   => 'F')
                                             ,1
                                             ,254));
          END LOOP;
          retcode := 2;
          RETURN;
          --   KP 01-26-2012 Version
        ELSE
          print_log('before calling ozf_budgetapproval_pvt.budget_request_approval, x_new_object_id =' ||
                    x_new_object_id || ', x_status_code =' ||
                    x_status_code);
          ozf_budgetapproval_pvt.budget_request_approval(p_init_msg_list => fnd_api.g_false
                                                        ,p_api_version   => 1.0
                                                        ,p_commit        => fnd_api.g_false
                                                        ,x_return_status => x_return_status
                                                        ,x_msg_count     => x_msg_count
                                                        ,x_msg_data      => x_msg_data
                                                        ,p_object_type   => 'OFFR'
                                                        ,p_object_id     => x_new_object_id
                                                        ,x_status_code   => x_status_code);
        
          IF nvl(x_return_status, 'X') != 'S'
          THEN
            fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                                     ,p_count   => x_msg_count
                                     ,p_data    => x_msg_data);
            ozf_utility_pvt.write_conc_log;
            print_log('2');
            FOR i IN 0 .. x_msg_count
            LOOP
              print_log('@Issue102, ' || substr(fnd_msg_pub.get(p_msg_index => i
                                                               ,p_encoded   => 'F')
                                               ,1
                                               ,254));
            END LOOP;
            retcode := 2;
            RETURN;
          ELSE
            IF p_mode != 'C'
            THEN
              ozf_offer_pvt.activate_offer(x_return_status     => x_return_status
                                          ,x_msg_count         => x_msg_count
                                          ,x_msg_data          => x_msg_data
                                          ,p_qp_list_header_id => x_new_object_id
                                          ,p_new_status_id     => 1604);
            
              IF nvl(x_return_status, 'X') != 'S'
              THEN
                fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                                         ,p_count   => x_msg_count
                                         ,p_data    => x_msg_data);
                ozf_utility_pvt.write_conc_log;
                print_log('3');
                FOR i IN 0 .. x_msg_count
                LOOP
                  print_log('@Issue103, ' || substr(fnd_msg_pub.get(p_msg_index => i
                                                                   ,p_encoded   => 'F')
                                                   ,1
                                                   ,254));
                END LOOP;
                retcode := 2;
                RETURN;
              
              ELSE
                IF p_mode != 'C'
                THEN
                  --
                  /*
                  BEGIN 
                   update_rebate
                     (
                       p_rebate_id      => x_new_object_id
                      ,p_from_rebate_id => l_list_header_id
                      ,p_global_flag    => l_global_flag
                      ,p_org_id         => l_org_id
                      ,p_offer_id       => l_offer_id
                      ,x_return_status  => x_return_status
                     );                 
                  EXCEPTION
                   WHEN OTHERS THEN
                    PRINT_LOG('@update_rebate, copy from list_header_id ='||l_list_header_id||', copy to list_header_id ='||x_new_object_id);                  
                    PRINT_LOG('@update_rebate, error message: '||SQLERRM);
                  END; */
                  --
                  BEGIN
                    -- flag the source offer so we don't reuse them again                
                    UPDATE qp_list_headers_b
                       SET attribute3 = 'Y' --Set to Y to indicate we have copied the global offer once already
                     WHERE list_header_id = l_list_header_id; --flag the source offer to Y so we do not pick again 
                    --               
                  EXCEPTION
                    WHEN OTHERS THEN
                      print_log('@ stamp source offer, list_header_id =' ||
                                l_list_header_id);
                      print_log('@ stamp source offer, error message: ' ||
                                SQLERRM);
                  END;
                  -- Set auto pay flag for the new offer copied accordingly
                  BEGIN
                    --                 
                    UPDATE ozf_offers
                       SET autopay_flag = l_auto_pay_enabled
                     WHERE qp_list_header_id = x_new_object_id; --new copied offer is flagged appropriately for auto pay.
                    --               
                  EXCEPTION
                    WHEN OTHERS THEN
                      print_log('@set auto pay flag , list_header_id =' ||
                                x_new_object_id);
                      print_log('@set auto pay flag , error message: ' ||
                                SQLERRM);
                  END;
                  --
                  print_log('Successfully setup LIST_HEADER_ID ' ||
                            x_new_object_id);
                  print_log('********* end of new plan_id =' ||
                            x_new_object_id);
                  print_log('');
                  --
                END IF;
              END IF;
            END IF; -- End IF Activate Offer or Not         KP 01-26-2012 Version
          END IF; --  Activate Offer                        KP 01-26-2012 Version
        END IF; -- Budget Request Approval 
      END IF; -- Create Budget Request
    END IF; -- Copy Offer
  EXCEPTION
    WHEN OTHERS THEN
      print_log('Error' || SQLERRM);
      RETURN;
  END start_copy;

  PROCEDURE rebate_mass_copy(errbuf           IN OUT VARCHAR2
                            ,retcode          IN OUT VARCHAR2
                            ,p_year           IN VARCHAR2
                            ,p_list_header_id IN NUMBER
                            ,p_mode           IN VARCHAR2) IS
  
    l_list_header_id NUMBER;
    l_rebate_name    qp_list_headers_vl.description%TYPE;
  
    x_return_status     VARCHAR2(1);
    x_msg_count         NUMBER;
    x_msg_data          VARCHAR2(4000);
    l_offer_copy_status VARCHAR2(1);
    g_org_id            NUMBER;
  
    CURSOR c_rebates IS
      SELECT qlhv.list_header_id list_header_id
            ,qlhv.description
            ,nvl(qlhv.attribute3, 'N') offer_copy_status
        FROM qp_list_headers_vl qlhv, ozf_offers oo, ams_media_vl med
       WHERE 1 = 1
         AND oo.qp_list_header_id = qlhv.list_header_id
         AND qlhv.global_flag = 'N' --mass copy program is going to run only for non global offers
         AND qlhv.attribute7 = p_year --Which calendar year. Added on 01/06/2014
         AND oo.status_code = 'ACTIVE'
         AND oo.offer_type IN ('ACCRUAL', 'VOLUME_OFFER')
         AND qlhv.description LIKE 'FY%'
         AND med.media_id = oo.activity_media_id
         AND med.media_name NOT IN ('Payment Terms', 'Freight');
  
    CURSOR c_name(p_list_header_id IN NUMBER) IS
    --      SELECT qlhv.description
    --            ,qlhv.attribute3 offer_copy_status 
    --        FROM qp_list_headers_vl qlhv
    --       WHERE 1 =1
    --         AND list_header_id  =p_list_header_id
    --         AND qlhv.attribute7 =p_year --Which calendar year. Added on 01/06/2014
    --         AND qlhv.active_flag ='Y'
    --         AND qlhv.global_flag ='N'; --mass copy program is going to run only for non global offers
      SELECT qlhv.description, nvl(qlhv.attribute3, 'N') offer_copy_status
        FROM qp_list_headers_vl qlhv, ozf_offers oo, ams_media_vl med
       WHERE 1 = 1
         AND qlhv.list_header_id = p_list_header_id
         AND oo.qp_list_header_id = qlhv.list_header_id
         AND qlhv.global_flag = 'N' --mass copy program is going to run only for non global offers
         AND qlhv.attribute7 = p_year --Which calendar year. Added on 01/06/2014
         AND oo.status_code = 'ACTIVE'
         AND oo.offer_type IN ('ACCRUAL', 'VOLUME_OFFER')
         AND qlhv.description LIKE 'FY%'
         AND med.media_id = oo.activity_media_id
         AND med.media_name NOT IN ('Payment Terms', 'Freight');
  
    CURSOR c_name2(p_list_header_id IN NUMBER) IS
    --      SELECT qlhv.description
    --            ,qlhv.attribute3 offer_copy_status            
    --        FROM qp_list_headers_vl qlhv
    --       WHERE 1 =1
    --         AND list_header_id = p_list_header_id
    --         AND qlhv.attribute7 =p_year --Which calendar year. Added on 01/06/2014
    --         AND qlhv.active_flag = 'Y'
    --         AND qlhv.global_flag ='N'; --mass copy program is going to run only for non global offers
      SELECT qlhv.description, nvl(qlhv.attribute3, 'N') offer_copy_status
        FROM qp_list_headers_vl qlhv, ozf_offers oo, ams_media_vl med
       WHERE 1 = 1
         AND qlhv.list_header_id = p_list_header_id
         AND qlhv.attribute7 = p_year --Which calendar year. Added on 01/06/2014         
         AND oo.qp_list_header_id = qlhv.list_header_id
         AND qlhv.global_flag = 'N' --mass copy program is going to run only for non global offers
         AND oo.status_code = 'ACTIVE'
         AND oo.offer_type IN ('ACCRUAL', 'VOLUME_OFFER')
         AND qlhv.description LIKE 'FY%'
         AND med.media_id = oo.activity_media_id
         AND med.media_name NOT IN ('Payment Terms', 'Freight');
  
  BEGIN
    fnd_msg_pub.initialize;
    retcode := 0;
    print_log('');
    print_log('p_list_header_id =' || p_list_header_id);
    print_log('p_year =' || p_year);
    print_log('p_mode =' || p_mode);
    print_log('');
    --
    IF mo_global.get_current_org_id = 101
    THEN
      g_org_id := 101;
    ELSIF mo_global.get_current_org_id = 102
    THEN
      g_org_id := 102;
    ELSE
      g_org_id := 101;
    END IF;
    --
    --print_log('My current operating unit -0- is '||mo_global.get_current_org_id);
    --         
    print_log('');
    IF p_list_header_id IS NULL
    THEN
      print_log('Inside rebate_mass_copy, multiple_offers');
      FOR rebates_rec IN c_rebates
      LOOP
        --
        IF rebates_rec.offer_copy_status = 'Y'
        THEN
          print_log('');
          print_log('Offer: ' || rebates_rec.description);
          print_log('List_header_id: ' || rebates_rec.list_header_id);
          print_log('Action: Exit');
          print_log('Reason: Offer was already copied and flagged.');
          print_log('');
        ELSE
          --
          SAVEPOINT copy_rebate;
          --
          print_log('@Multiple offers, Before setting MO Operating unit');
          mo_global.set_policy_context('S', g_org_id);
          --
          print_log('@Multiple offers, After setting MO Operating unit to ' ||
                    mo_global.get_current_org_id);
          print_log('Ready to copy offer: -' || rebates_rec.description);
          --               
          start_copy(errbuf, retcode, rebates_rec.list_header_id, p_mode);
          --       
        END IF;
        --
      END LOOP;
      retcode := 0;
    ELSE
      --Single offer only
      --
      IF p_mode = 'M'
      THEN
        print_log('Inside rebate_mass_copy, single_offer, p_mode =M');
        OPEN c_name(p_list_header_id);
        FETCH c_name
          INTO l_rebate_name, l_offer_copy_status;
        CLOSE c_name;
      ELSIF p_mode = 'C'
      THEN
        print_log('Inside rebate_mass_copy, single_offer, p_mode =C');
        OPEN c_name2(p_list_header_id);
        FETCH c_name2
          INTO l_rebate_name, l_offer_copy_status;
        CLOSE c_name2;
      END IF;
      -- 
      print_log('l_rebate_name =' || l_rebate_name);
      -- 
      IF l_rebate_name IS NOT NULL
      THEN
        IF l_offer_copy_status = 'Y'
        THEN
          print_log('');
          print_log('Offer: ' || l_rebate_name);
          print_log('List_header_id: ' || p_list_header_id);
          print_log('Action: Exit');
          print_log('Reason: Offer was already copied and flagged.');
          print_log('*******************************************************');
          print_log('');
        ELSE
          --
          print_log('@Single offer, Before setting MO Operating unit');
          mo_global.set_policy_context('S', g_org_id);
          --
          print_log('@Single offer, After setting MO Operating unit to ' ||
                    mo_global.get_current_org_id);
          --         
          SAVEPOINT copy_rebate;
          --
          print_log('@2. Copying offer: -' || l_rebate_name);
          --
          start_copy(errbuf, retcode, p_list_header_id, p_mode);
        END IF;
        --SAVEPOINT copy_rebate;
        --print_log('Copying-' || l_rebate_name);
        --start_copy(errbuf, retcode, p_list_header_id, p_mode);
      ELSE
        OPEN c_name2(p_list_header_id);
        FETCH c_name2
          INTO l_rebate_name, l_offer_copy_status;
        CLOSE c_name2;
        print_log('Not eligible for copy -' || l_rebate_name);
      END IF;
      --
    END IF;
  
    print_log('RetCode is ' || retcode);
  
  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      ROLLBACK TO copy_rebate;
      ozf_utility_pvt.write_conc_log;
      errbuf  := x_msg_data;
      retcode := 2;
    WHEN fnd_api.g_exc_unexpected_error THEN
      ROLLBACK TO copy_rebate;
      ozf_utility_pvt.write_conc_log;
      errbuf  := x_msg_data;
      retcode := 2;
    WHEN OTHERS THEN
      ROLLBACK TO copy_rebate;
      ozf_utility_pvt.write_conc_log;
      errbuf  := substr(SQLERRM, 1, 80);
      retcode := 2;
  END rebate_mass_copy;

  FUNCTION get_lob_id(p_resale_line_tbl ozf_order_price_pvt.resale_line_tbl_type)
    RETURN NUMBER IS
    l_lob_id NUMBER;
  BEGIN
  
    IF p_resale_line_tbl.count > 0
    THEN
    
      l_lob_id := p_resale_line_tbl(1).line_attribute1;
    ELSE
      l_lob_id := NULL;
    END IF;
  
    RETURN l_lob_id;
  
  END get_lob_id;
  -- No Longer Needed(Obsolete)
  PROCEDURE get_child_budget_approver(itemtype        IN VARCHAR2
                                     ,itemkey         IN VARCHAR2
                                     ,x_approver_id   OUT NUMBER
                                     ,x_return_status OUT VARCHAR2) IS
  
    l_activity_id      NUMBER;
    l_approver_id      NUMBER;
    l_transaction_type VARCHAR2(50);
    l_transfer_type    VARCHAR2(50);
  
    l_requestor_id NUMBER;
  
  BEGIN
    x_return_status := fnd_api.g_ret_sts_success;
    l_activity_id   := wf_engine.getitemattrnumber(itemtype => itemtype
                                                  ,itemkey  => itemkey
                                                  ,aname    => 'AMS_ACTIVITY_ID');
  
    l_requestor_id := wf_engine.getitemattrnumber(itemtype => itemtype
                                                 ,itemkey  => itemkey
                                                 ,aname    => 'AMS_REQUESTER_ID');
  
    x_approver_id := NULL;
  
    IF x_return_status != fnd_api.g_ret_sts_error
    THEN
      IF x_approver_id IS NULL
      THEN
        x_approver_id := l_requestor_id;
      END IF;
    END IF;
  
  EXCEPTION
    WHEN OTHERS THEN
      x_return_status := fnd_api.g_ret_sts_error;
      RAISE;
  END get_child_budget_approver;

  PROCEDURE get_auto_budget_approver(itemtype        IN VARCHAR2
                                    ,itemkey         IN VARCHAR2
                                    ,x_approver_id   OUT NUMBER
                                    ,x_return_status OUT VARCHAR2) IS
    l_requestor_id NUMBER;
    l_activity_id  NUMBER;
  BEGIN
    x_return_status := fnd_api.g_ret_sts_success;
  
    l_activity_id := wf_engine.getitemattrnumber(itemtype => itemtype
                                                ,itemkey  => itemkey
                                                ,aname    => 'AMS_ACTIVITY_ID');
  
    l_requestor_id := wf_engine.getitemattrnumber(itemtype => itemtype
                                                 ,itemkey  => itemkey
                                                 ,aname    => 'AMS_REQUESTER_ID');
  
    x_approver_id := NULL;
  
    IF x_return_status != fnd_api.g_ret_sts_error
    THEN
      IF x_approver_id IS NULL
      THEN
        x_approver_id := l_requestor_id;
      END IF;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      x_return_status := fnd_api.g_ret_sts_error;
      RAISE;
  END get_auto_budget_approver;

  PROCEDURE offer_adjustment(errbuf              OUT NOCOPY VARCHAR2
                            ,retcode             OUT NOCOPY NUMBER
                            ,p_qp_list_header_id IN NUMBER) IS
  
    x_offer_adjustment_id   NUMBER;
    x_offer_adjst_tier_id   NUMBER;
    x_offer_adj_line_id     NUMBER;
    x_object_version_number NUMBER;
  
    x_qp_list_header_id NUMBER;
    x_err_location      NUMBER;
  
    l_offer_adj_rec        ozf_offer_adjustment_pvt.offer_adj_rec_type;
    l_offadj_tier_rec_type ozf_offer_adj_tier_pvt.offadj_tier_rec_type;
  
    l_offadj_line_rec_type ozf_offer_adj_line_pvt.offadj_line_rec_type;
  
    l_modifier_list_rec ozf_offer_pub.modifier_list_rec_type;
    l_modifier_line_tbl ozf_offer_pub.modifier_line_tbl_type;
    l_na_qualifier_tbl  ozf_offer_pub.na_qualifier_tbl_type;
    l_prod_rec_tbl      ozf_offer_pub.prod_rec_tbl_type;
    l_offer_tier_tbl    ozf_offer_pub.offer_tier_tbl_type;
    l_excl_rec_tbl      ozf_offer_pub.excl_rec_tbl_type;
    l_discount_line_tbl ozf_offer_pub.discount_line_tbl_type;
    l_act_product_tbl   ozf_offer_pub.act_product_tbl_type;
    l_vo_pbh_tbl        ozf_offer_pub.vo_disc_tbl_type;
    l_vo_dis_tbl        ozf_offer_pub.vo_disc_tbl_type;
    l_vo_prod_tbl       ozf_offer_pub.vo_prod_tbl_type;
    l_qualifier_tbl     ozf_offer_pub.qualifiers_tbl_type;
    l_vo_mo_tbl         ozf_offer_pub.vo_mo_tbl_type;
    l_budget_tbl        ozf_offer_pub.budget_tbl_type;
  
    l_object_version_number NUMBER;
    l_obj_version           NUMBER;
    l_start_date            DATE;
    l_offer_id              NUMBER;
    l_description           qp_list_headers_vl.description%TYPE;
    l_offer_type            VARCHAR2(50);
    l_user_status_id        NUMBER;
    l_cyear                 VARCHAR2(4);
    x_errbuf                VARCHAR2(2000);
    x_retcode               VARCHAR2(2000);
  
    x_return_status VARCHAR2(1);
    x_msg_count     NUMBER;
    x_msg_data      VARCHAR2(4000);
  
    CURSOR c1(p_offer_id IN NUMBER) IS
      SELECT offer_discount_line_id, discount
        FROM ozf_offer_discount_lines
       WHERE offer_id = p_offer_id
         AND volume_type IS NULL
         AND tier_type = 'DIS';
  
    CURSOR c2(p_offr_adj_id IN NUMBER) IS
      SELECT object_version_number
        FROM ozf_offer_adjustments_b
       WHERE offer_adjustment_id = p_offr_adj_id;
  
    CURSOR c_offer_details(p_list_header_id IN NUMBER) IS
      SELECT oo.offer_id
            ,oo.offer_type
            ,oo.object_version_number
            ,qlhv.description
            ,qlhv.start_date_active
        FROM ozf_offers oo, qp_list_headers_vl qlhv
       WHERE qp_list_header_id = p_list_header_id
         AND oo.qp_list_header_id = qlhv.list_header_id;
  
    CURSOR c5(p_list_header_id IN NUMBER) IS
      SELECT qll.list_line_id, qll.operand
        FROM qp_list_lines qll
       WHERE qll.list_header_id = p_list_header_id
         AND NOT EXISTS
       (SELECT 1
                FROM ozf_offer_adjustment_lines lin
               WHERE qll.list_header_id = lin.list_header_id
                 AND qll.list_line_id = lin.list_line_id);
  
    CURSOR c_user_status_id IS
      SELECT user_status_id
        FROM ams_user_statuses_vl
       WHERE system_status_type = 'OZF_OFFER_STATUS'
         AND system_status_code = 'TERMINATED'
         AND NAME LIKE '%Adjusted%';
  
  BEGIN
  
    SAVEPOINT offer_adjustment;
    retcode := 0;
  
    BEGIN
      SELECT attribute7
        INTO l_cyear
        FROM qp_list_headers_vl
       WHERE 1 = 1
         AND list_header_id = p_qp_list_header_id;
    EXCEPTION
      WHEN OTHERS THEN
        print_log('FAILED TO GET CALENDAR YEAR FOR LIST_HEADER_ID =' ||
                  p_qp_list_header_id || ', MESSAGE =' || SQLERRM);
        l_cyear := NULL;
    END;
  
    rebate_mass_copy(errbuf           => x_errbuf
                    ,retcode          => x_retcode
                    ,p_list_header_id => p_qp_list_header_id
                    ,p_mode           => 'C'
                    ,p_year           => l_cyear);
  
    IF x_retcode != 2
    THEN
    
      OPEN c_offer_details(p_qp_list_header_id);
      FETCH c_offer_details
        INTO l_offer_id
            ,l_offer_type
            ,l_obj_version
            ,l_description
            ,l_start_date;
      CLOSE c_offer_details;
    
      l_offer_adj_rec.effective_date  := l_start_date;
      l_offer_adj_rec.settlement_code := 'ACCRUE';
      l_offer_adj_rec.status_code     := 'DRAFT';
    
      l_offer_adj_rec.list_header_id        := p_qp_list_header_id;
      l_offer_adj_rec.offer_adjustment_name := 'Recalculate Accruals';
      l_offer_adj_rec.description           := 'Recalculate Accruals';
    
      ozf_offer_adjustment_pvt.create_offer_adjustment(p_api_version_number  => 1.0
                                                      ,p_init_msg_list       => fnd_api.g_false
                                                      ,p_commit              => fnd_api.g_false
                                                      ,p_validation_level    => fnd_api.g_valid_level_full
                                                      ,x_return_status       => x_return_status
                                                      ,x_msg_count           => x_msg_count
                                                      ,x_msg_data            => x_msg_data
                                                      ,p_offer_adj_rec       => l_offer_adj_rec
                                                      ,x_offer_adjustment_id => x_offer_adjustment_id);
    
      fnd_file.put_line(fnd_file.log
                       ,'Offer Adjustment ID' || x_offer_adjustment_id);
      dbms_output.put_line('Offer Adjustment ID' || x_offer_adjustment_id);
    
      IF x_return_status = fnd_api.g_ret_sts_error
      THEN
        RAISE fnd_api.g_exc_error;
      ELSIF x_return_status = fnd_api.g_ret_sts_unexp_error
      THEN
        RAISE fnd_api.g_exc_unexpected_error;
      END IF;
    
      IF l_offer_type = 'VOLUME_OFFER'
      THEN
      
        FOR c1_rec IN c1(l_offer_id)
        LOOP
        
          l_offadj_tier_rec_type.offer_adjustment_id := x_offer_adjustment_id;
          l_offadj_tier_rec_type.qp_list_header_id   := p_qp_list_header_id;
          l_offadj_tier_rec_type.original_discount   := c1_rec.discount;
          l_offadj_tier_rec_type.modified_discount   := 0;
        
          l_offadj_tier_rec_type.offer_discount_line_id := c1_rec.offer_discount_line_id;
        
          ozf_offer_adj_tier_pvt.create_offer_adj_tier(p_api_version_number  => 1.0
                                                      ,p_init_msg_list       => fnd_api.g_false
                                                      ,p_commit              => fnd_api.g_false
                                                      ,p_validation_level    => fnd_api.g_valid_level_full
                                                      ,x_return_status       => x_return_status
                                                      ,x_msg_count           => x_msg_count
                                                      ,x_msg_data            => x_msg_data
                                                      ,p_offadj_tier_rec     => l_offadj_tier_rec_type
                                                      ,x_offer_adjst_tier_id => x_offer_adjst_tier_id);
        END LOOP;
      
      ELSIF l_offer_type = 'ACCRUAL'
      THEN
      
        FOR c5_rec IN c5(p_qp_list_header_id)
        LOOP
        
          l_offadj_line_rec_type.offer_adjustment_id := x_offer_adjustment_id;
          l_offadj_line_rec_type.list_header_id      := p_qp_list_header_id;
          l_offadj_line_rec_type.list_line_id        := c5_rec.list_line_id;
          l_offadj_line_rec_type.original_discount   := c5_rec.operand;
          l_offadj_line_rec_type.modified_discount   := 0;
        
          ozf_offer_adj_line_pvt.create_offer_adj_line(p_api_version_number       => 1.0
                                                      ,p_init_msg_list            => fnd_api.g_false
                                                      ,p_commit                   => fnd_api.g_false
                                                      ,p_validation_level         => fnd_api.g_valid_level_full
                                                      ,x_return_status            => x_return_status
                                                      ,x_msg_count                => x_msg_count
                                                      ,x_msg_data                 => x_msg_data
                                                      ,p_offadj_line_rec          => l_offadj_line_rec_type
                                                      ,x_offer_adjustment_line_id => x_offer_adj_line_id);
        END LOOP;
      END IF;
    
      fnd_file.put_line(fnd_file.log
                       ,'Offer Adjustment Tier ID' ||
                        x_offer_adjst_tier_id);
      dbms_output.put_line('Offer Adjustment Tier ID' ||
                           x_offer_adjst_tier_id);
    
      IF x_return_status = fnd_api.g_ret_sts_error
      THEN
        RAISE fnd_api.g_exc_error;
      ELSIF x_return_status = fnd_api.g_ret_sts_unexp_error
      THEN
        RAISE fnd_api.g_exc_unexpected_error;
      END IF;
    
      l_offer_adj_rec := NULL;
    
      OPEN c2(x_offer_adjustment_id);
      FETCH c2
        INTO l_object_version_number;
      CLOSE c2;
    
      l_offer_adj_rec.offer_adjustment_id   := x_offer_adjustment_id;
      l_offer_adj_rec.status_code           := 'ACTIVE';
      l_offer_adj_rec.object_version_number := l_object_version_number;
    
      l_offer_adj_rec.approved_date := SYSDATE;
    
      ozf_offer_adjustment_pvt.update_offer_adjustment(p_api_version_number    => 1.0
                                                      ,p_init_msg_list         => fnd_api.g_false
                                                      ,p_commit                => fnd_api.g_false
                                                      ,p_validation_level      => fnd_api.g_valid_level_full
                                                      ,x_return_status         => x_return_status
                                                      ,x_msg_count             => x_msg_count
                                                      ,x_msg_data              => x_msg_data
                                                      ,p_offer_adj_rec         => l_offer_adj_rec
                                                      ,x_object_version_number => x_object_version_number);
    
      fnd_file.put_line(fnd_file.log
                       ,'Object Version Num' || x_object_version_number);
      dbms_output.put_line('Object Version Num' || x_object_version_number);
    
      IF x_return_status = fnd_api.g_ret_sts_error
      THEN
        RAISE fnd_api.g_exc_error;
      ELSIF x_return_status = fnd_api.g_ret_sts_unexp_error
      THEN
        RAISE fnd_api.g_exc_unexpected_error;
      END IF;
    
      OPEN c_user_status_id;
      FETCH c_user_status_id
        INTO l_user_status_id;
      CLOSE c_user_status_id;
    
      l_modifier_list_rec.qp_list_header_id     := p_qp_list_header_id;
      l_modifier_list_rec.description           := 'ADJ' || l_description;
      l_modifier_list_rec.object_version_number := l_obj_version;
      l_modifier_list_rec.status_code           := 'TERMINATED';
      l_modifier_list_rec.user_status_id        := nvl(l_user_status_id
                                                      ,1608);
      l_modifier_list_rec.modifier_operation    := 'UPDATE';
      l_modifier_list_rec.offer_operation       := 'UPDATE';
      l_modifier_list_rec.offer_id              := l_offer_id;
    
      ozf_offer_pub.process_modifiers(p_init_msg_list     => fnd_api.g_false
                                     ,p_api_version       => 1.0
                                     ,p_commit            => fnd_api.g_false
                                     ,x_return_status     => x_return_status
                                     ,x_msg_count         => x_msg_count
                                     ,x_msg_data          => x_msg_data
                                     ,p_offer_type        => l_offer_type
                                     ,p_modifier_list_rec => l_modifier_list_rec
                                     ,p_modifier_line_tbl => l_modifier_line_tbl
                                     ,p_qualifier_tbl     => l_qualifier_tbl
                                     ,p_budget_tbl        => l_budget_tbl
                                     ,p_act_product_tbl   => l_act_product_tbl
                                     ,p_discount_tbl      => l_discount_line_tbl
                                     ,p_excl_tbl          => l_excl_rec_tbl
                                     ,p_offer_tier_tbl    => l_offer_tier_tbl
                                     ,p_prod_tbl          => l_prod_rec_tbl
                                     ,p_na_qualifier_tbl  => l_na_qualifier_tbl
                                     ,x_qp_list_header_id => x_qp_list_header_id
                                     ,x_error_location    => x_err_location);
    
      IF x_return_status = fnd_api.g_ret_sts_error
      THEN
        RAISE fnd_api.g_exc_error;
      ELSIF x_return_status = fnd_api.g_ret_sts_unexp_error
      THEN
        RAISE fnd_api.g_exc_unexpected_error;
      END IF;
    ELSE
      ROLLBACK TO offer_adjustment;
      retcode := 2;
    END IF;
  
  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      --rollback to Offer_Adjustment;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);
      ozf_utility_pvt.write_conc_log;
      errbuf  := nvl(substr(x_msg_data, -1000), x_msg_data);
      retcode := 2;
    
    WHEN fnd_api.g_exc_unexpected_error THEN
      --rollback to Offer_Adjustment;
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);
      ozf_utility_pvt.write_conc_log;
      errbuf  := nvl(substr(x_msg_data, -1000), x_msg_data);
      retcode := 2;
    WHEN OTHERS THEN
      --rollback to Offer_Adjustment;
      ozf_utility_pvt.write_conc_log;
      errbuf  := substr(SQLERRM, 1, 80);
      retcode := 2;
  END offer_adjustment;

  FUNCTION validate_program(p_subscription_guid IN RAW
                           ,p_event             IN OUT NOCOPY wf_event_t)
    RETURN VARCHAR2 IS
  
    CURSOR c1(p_fund_id IN NUMBER) IS
      SELECT a.currency_code_tc
            ,a.ledger_id
            ,b.currency_code ledger_curr_code
            ,c.short_name
        FROM ozf_funds_all_b a, gl_ledgers b, ozf_funds_all_tl c
       WHERE a.ledger_id = b.ledger_id
         AND a.fund_id = c.fund_id
         AND a.fund_id = p_fund_id;
  
    l_parameter_list wf_parameter_list_t := wf_parameter_list_t();
    l_event_key      VARCHAR2(240);
    l_parameter_t    wf_parameter_t := wf_parameter_t(NULL, NULL);
    l_parameter_name l_parameter_t.name%TYPE;
    l_fund_id        NUMBER;
    i                PLS_INTEGER;
    upper_case_error EXCEPTION;
    format_error     EXCEPTION;
    curr_error       EXCEPTION;
  BEGIN
    SAVEPOINT validate_program;
  
    l_parameter_list := p_event.getparameterlist();
    l_event_key      := p_event.geteventkey();
  
    IF l_parameter_list IS NOT NULL
    THEN
      i := l_parameter_list.first;
    
      WHILE (i <= l_parameter_list.last)
      LOOP
        l_parameter_name := NULL;
        l_parameter_name := l_parameter_list(i).getname();
      
        IF l_parameter_name = 'P_FUND_ID'
        THEN
          l_fund_id := l_parameter_list(i).getvalue();
        
          FOR rec_1 IN c1(l_fund_id)
          LOOP
            IF upper(rec_1.short_name) != rec_1.short_name
            THEN
              RAISE upper_case_error;
            ELSIF substr(rec_1.short_name, 1, 2) != 'FY' OR
                  upper(substr(rec_1.short_name, 3, 4)) !=
                  lower(substr(rec_1.short_name, 3, 4))
            THEN
              RAISE format_error;
            ELSIF rec_1.currency_code_tc != rec_1.ledger_curr_code
            THEN
              RAISE curr_error;
            END IF;
          END LOOP;
        
        END IF;
      
        i := l_parameter_list.next(i);
      END LOOP;
    END IF;
  
    RETURN 'SUCCESS';
  
  EXCEPTION
    WHEN upper_case_error THEN
      fnd_message.set_name('OZF', 'OZF_API_DEBUG_MESSAGE');
      fnd_message.set_token('TEXT', 'Program Name must be in UPPER CASE');
      fnd_msg_pub.add;
    WHEN format_error THEN
      fnd_message.set_name('OZF', 'OZF_API_DEBUG_MESSAGE');
      fnd_message.set_token('TEXT', 'Program Name must began with FY####-');
      fnd_msg_pub.add;
    WHEN curr_error THEN
      fnd_message.set_name('OZF', 'OZF_API_DEBUG_MESSAGE');
      fnd_message.set_token('TEXT'
                           ,'Program Name currency not the same as ledger currency');
      fnd_msg_pub.add;
    WHEN OTHERS THEN
      fnd_message.set_name('OZF', 'OZF_API_DEBUG_MESSAGE');
      fnd_message.set_token('TEXT'
                           ,'Unknown error - please contact your system adminstrator');
      fnd_msg_pub.add;
  END validate_program;
-- ======================================================
-- PROCEDURE: program_request_auto_approve
-- ESMS     ver      Date         Note
--
-- 290257   1.19     01-JU1-2015  Add error email api 
-- ====================================================== 
  PROCEDURE program_request_auto_approve(itemtype        IN VARCHAR2
                                        ,itemkey         IN VARCHAR2
                                        ,x_approver_id   OUT NUMBER
                                        ,x_return_status OUT VARCHAR2) IS
  
    l_requestor_id   NUMBER;
    l_activity_id    NUMBER;
    l_currency_check VARCHAR2(1);
      -- Begin  ver 1.19
      l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_MISC_PKG';
      l_err_callpoint VARCHAR2(75) DEFAULT 'program_request_auto_approve';
      l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_err_msg       VARCHAR2(3000);
      l_err_code      NUMBER;
      l_sec           VARCHAR2(255);      
      -- End  ver  1.19     
  
    CURSOR c_fund_offer_currency_check(p_activity_id IN NUMBER) IS
      SELECT 'Y' --, oab.requester_id --Ver 1.19
        FROM ozf_offers oo, ozf_funds_all_b ofa, ozf_act_budgets oab
       WHERE ofa.fund_id = oab.budget_source_id
         AND oo.qp_list_header_id = oab.act_budget_used_by_id
         AND oab.budget_source_id = ofa.fund_id
            --AND ofa.currency_code_tc = oo.transaction_currency_code
         AND ((ofa.currency_code_tc = oo.transaction_currency_code) OR
             (oo.transaction_currency_code IS NULL AND oo.org_id IS NULL))
         AND oo.qp_list_header_id = p_activity_id;
  
  BEGIN
    x_return_status := fnd_api.g_ret_sts_success;
  
    l_activity_id := wf_engine.getitemattrnumber(itemtype => itemtype
                                                ,itemkey  => itemkey
                                                ,aname    => 'AMS_ACTIVITY_ID');
  
    l_requestor_id := wf_engine.getitemattrnumber(itemtype => itemtype
                                                 ,itemkey  => itemkey
                                                 ,aname    => 'AMS_REQUESTER_ID');
  
    x_approver_id := NULL;
  
    OPEN c_fund_offer_currency_check(l_activity_id);
    FETCH c_fund_offer_currency_check
      INTO l_currency_check; --, l_requestor_id; Ver 1.19
    CLOSE c_fund_offer_currency_check;
  
    IF l_currency_check IS NULL
    THEN
      fnd_message.set_name('OZF', 'OZF_API_DEBUG_MESSAGE');
      fnd_message.set_token('TEXT'
                           ,'Rebate Currency Not Matching with Program Currency');
      fnd_msg_pub.add;
    
      x_return_status := fnd_api.g_ret_sts_error;
    END IF;
  
    IF x_return_status != fnd_api.g_ret_sts_error
    THEN
      IF x_approver_id IS NULL
      THEN
        x_approver_id := l_requestor_id;
      END IF;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      x_return_status := fnd_api.g_ret_sts_error;
      l_err_msg  := ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();  
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'Rebates');      
      RAISE;
  END program_request_auto_approve;

  PROCEDURE validate_rebate(itemtype        IN VARCHAR2
                           ,itemkey         IN VARCHAR2
                           ,x_approver_id   OUT NUMBER
                           ,x_return_status OUT VARCHAR2) IS
    l_requestor_id  NUMBER;
    l_activity_id   NUMBER;
    l_offer_id      NUMBER;
    l_benf_party_id NUMBER;
    l_exists        VARCHAR2(1);
    l_offer_type    VARCHAR2(40);
    l_cust_type     VARCHAR2(50);
    l_vol_type      ozf_offers.volume_offer_type%TYPE;
    l_org_context   qp_list_headers_b.context%TYPE;
    l_currency      VARCHAR2(3);
    l_global_flag   qp_list_headers_b.global_flag%TYPE;
  
    CURSOR c_mkt_elig_exists(p_offer_id IN NUMBER) IS
      SELECT 'Y'
        FROM qp_qualifiers
       WHERE list_header_id = p_offer_id
         AND qualifier_attribute = 'QUALIFIER_ATTRIBUTE2'
         AND qualifier_context = 'SOLD_BY';
    --AND active_flag='Y';
  
    CURSOR c_offer_type(p_offer_id IN NUMBER) IS
      SELECT oo.offer_type
            ,oo.offer_id
            ,oo.autopay_party_id
            ,oo.volume_offer_type
            ,qlhv.context
            ,qlhv.global_flag
            ,oo.transaction_currency_code
        FROM ozf_offers oo, qp_list_headers_vl qlhv
       WHERE qp_list_header_id = p_offer_id
         AND qlhv.list_header_id = oo.qp_list_header_id;
  
    CURSOR c_benf_exists(p_offer_id IN NUMBER) IS
      SELECT beneficiary_party_id
        FROM ozf_offr_market_options
       WHERE offer_id = p_offer_id;
  
    CURSOR c_customer_type(p_cust_account_id IN NUMBER) IS
      SELECT attribute1
        FROM hz_cust_accounts hca
       WHERE cust_account_id = p_cust_account_id;
  
  BEGIN
    x_return_status := fnd_api.g_ret_sts_success;
  
    l_activity_id := wf_engine.getitemattrnumber(itemtype => itemtype
                                                ,itemkey  => itemkey
                                                ,aname    => 'AMS_ACTIVITY_ID');
  
    l_requestor_id := wf_engine.getitemattrnumber(itemtype => itemtype
                                                 ,itemkey  => itemkey
                                                 ,aname    => 'AMS_REQUESTER_ID');
  
    x_approver_id := NULL;
  
    OPEN c_offer_type(l_activity_id);
    FETCH c_offer_type
      INTO l_offer_type
          ,l_offer_id
          ,l_benf_party_id
          ,l_vol_type
          ,l_org_context
          ,l_global_flag
          ,l_currency;
    CLOSE c_offer_type;
  
    IF l_offer_type IN ('ACCRUAL', 'VOLUME_OFFER')
    THEN
    
      OPEN c_mkt_elig_exists(l_activity_id);
      FETCH c_mkt_elig_exists
        INTO l_exists;
      CLOSE c_mkt_elig_exists;
    
      IF nvl(l_exists, 'N') = 'N'
      THEN
        fnd_message.set_name('OZF', 'OZF_API_DEBUG_MESSAGE');
        fnd_message.set_token('TEXT'
                             ,'Please Enter the Market Eligibility');
        fnd_msg_pub.add;
      
        x_return_status := fnd_api.g_ret_sts_error;
        RETURN;
      END IF;
    
      IF l_offer_type = 'ACCRUAL'
      THEN
      
        IF l_benf_party_id IS NULL
        THEN
          fnd_message.set_name('OZF', 'OZF_API_DEBUG_MESSAGE');
          fnd_message.set_token('TEXT'
                               ,'Please Choose the Vendor in Advanced Options-AutoPay');
          fnd_msg_pub.add;
        
          x_return_status := fnd_api.g_ret_sts_error;
          RETURN;
        END IF;
      
      ELSIF l_offer_type = 'VOLUME_OFFER'
      THEN
      
        l_benf_party_id := NULL;
      
        OPEN c_benf_exists(l_offer_id);
        FETCH c_benf_exists
          INTO l_benf_party_id;
        CLOSE c_benf_exists;
      
        IF l_benf_party_id IS NULL
        THEN
          fnd_message.set_name('OZF', 'OZF_API_DEBUG_MESSAGE');
          fnd_message.set_token('TEXT'
                               ,'Please Choose the Vendor in Market Options -Alternate Beneficiary');
          fnd_msg_pub.add;
        
          x_return_status := fnd_api.g_ret_sts_error;
          RETURN;
        END IF;
      
        IF l_vol_type IS NULL OR l_vol_type != 'ACCRUAL'
        THEN
          fnd_message.set_name('OZF', 'OZF_API_DEBUG_MESSAGE');
          fnd_message.set_token('TEXT'
                               ,'Please Choose the Incentive as ACCRUAL');
          fnd_msg_pub.add;
        
          x_return_status := fnd_api.g_ret_sts_error;
          RETURN;
        END IF;
      
      END IF;
    
      OPEN c_customer_type(l_benf_party_id);
      FETCH c_customer_type
        INTO l_cust_type;
      CLOSE c_customer_type;
    
      IF l_cust_type != 'HDS_MVID'
      THEN
        fnd_message.set_name('OZF', 'OZF_API_DEBUG_MESSAGE');
        fnd_message.set_token('TEXT'
                             ,'Please Choose the Beneficiary of Type Master Vendor');
        fnd_msg_pub.add;
      
        x_return_status := fnd_api.g_ret_sts_error;
        RETURN;
      
      END IF;
    
      IF l_org_context IS NULL
      THEN
        fnd_message.set_name('OZF', 'OZF_API_DEBUG_MESSAGE');
        fnd_message.set_token('TEXT', 'Please Choose the Org Context');
        fnd_msg_pub.add;
      
        x_return_status := fnd_api.g_ret_sts_error;
        RETURN;
      
      END IF;
    
      IF l_global_flag = 'Y' AND l_currency IS NOT NULL
      THEN
        fnd_message.set_name('OZF', 'OZF_API_DEBUG_MESSAGE');
        fnd_message.set_token('TEXT'
                             ,'Currency Should be NULL for Global Agreements');
        fnd_msg_pub.add;
      
        x_return_status := fnd_api.g_ret_sts_error;
        RETURN;
      
      END IF;
    
    END IF;
  
    IF x_return_status != fnd_api.g_ret_sts_error
    THEN
      IF x_approver_id IS NULL
      THEN
        x_approver_id := l_requestor_id;
      END IF;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      x_return_status := fnd_api.g_ret_sts_error;
      RAISE;
  END validate_rebate;

  /*******************************************************************************
  * Procedure:   submit_fae
  * Description: This is called from XXCUS_TM_INTERFACE_PKG.LOAD_REBATE_RECEIPTS_GRP.  
  *              During month end processing after Third Party Accrual runs it will
  *              call this package to complete month end processing running the 
  *              Funds Accrual Engine by vendor
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/01/2010    Chandra Gadge   Initial creation of the procedure  
  1.5     09/28/2010    Kathy Poling    Added wait to submit_fae so we could send 
                                        notification when process is complete 
  1.6     01/29/2013    Kathy Poling    Added call to procedure submit_update_budget
                                        and Resale Batches Purge.  Added 2 more 
                                        conditions to the cursor and update to put
                                        status in the staging table.  Added BU 
                                        for parm 
                                        RFC 36582
  1.7    08/26/2013     Kathy Poling    Moving update budget to run before FAE is 
                                        submitted SR 207441 
  1.8    10/29/2013     Kathy Poling    Added offer type that will run FAE on volume offers  
                                        RFC # 38600  Service Number 228047  
  1.9    11/27/2013     Kathy Poling    Move call "submit_resale_purge" to submit_update_budget
                                        Adding lookup for concurrent time
                                        RFC 38762                                                                                                                                                               
  ********************************************************************************/

  PROCEDURE submit_fae(errbuf  OUT NOCOPY VARCHAR2
                      ,retcode OUT NOCOPY NUMBER
                      ,p_bu_nm IN VARCHAR2) IS
  
    --Version 1.5 added variables 
    --
    -- Package Variables
    --
    l_package    VARCHAR2(33) := 'XXCUS_TM_MISC_PKG.';
    l_dflt_email VARCHAR2(200) := 'hdsoracleapsupport@hdsupply.com'; -- email used for copy to AP Support
  
    l_errorstatus        NUMBER;
    l_req_id             NUMBER NULL;
    v_phase              VARCHAR2(50);
    v_status             VARCHAR2(50);
    v_dev_status         VARCHAR2(50);
    v_dev_phase          VARCHAR2(50);
    v_message            VARCHAR2(250);
    v_error_message      VARCHAR2(3000);
    l_interval           NUMBER; --Version 1.9
    l_max_time           NUMBER; --Version 1.9   
    l_message            VARCHAR2(150);
    l_globalset          VARCHAR2(100);
    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(255);
    l_statement          VARCHAR2(9000);
    l_can_submit_request BOOLEAN := TRUE;
    l_excptn_msg         VARCHAR2(15); --Version 1.8
    l_adj_offer          VARCHAR2(15); --Version 1.8
    l_vol_offer          VARCHAR2(15); --Version 1.8
    l_gl_post            VARCHAR2(15); --Version 1.8
    l_message_cnt        VARCHAR2(15); --Version 1.8
    l_debug              VARCHAR2(15); --Version 1.8
    l_responsibility     VARCHAR2(100); --Version 1.8
    l_user               VARCHAR2(100); --Version 1.8
  
    -- email
    l_primary_email VARCHAR2(250) := 'HDSRebateManagementSystemNotifications@hdsupply.com';
    l_subject       VARCHAR2(32767) DEFAULT NULL;
    l_body          VARCHAR2(32767) DEFAULT NULL;
    l_body_header   VARCHAR2(32767) DEFAULT NULL;
    l_body_detail   VARCHAR2(32767) DEFAULT NULL;
    l_body_footer   VARCHAR2(32767) DEFAULT NULL;
    l_sender        VARCHAR2(100);
    l_sid           VARCHAR2(8);
    l_errorstatus   NUMBER;
    l_host          VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
    l_hostport      VARCHAR2(20) := '25';
  
    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_MISC_PKG.SUBMIT_FAE';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  
    CURSOR c_vendors IS
      SELECT DISTINCT rebt_cust_acct_id
        FROM xxcus.xxcus_rebt_cust_tbl
       WHERE status_code = 'CLOSED'
         AND request_code = 'C'
         AND request_id IS NOT NULL
         AND status_flag IS NULL
         AND bu_nm = nvl(p_bu_nm, bu_nm) -- version 1.6
         AND offer_type = 'VOLUME_OFFER';
  
    --l_req_id     NUMBER;   version 1.5
  BEGIN
    --RETCODE := 0;          version 1.5
  
    --Setup parameters for running FND JOBS!
    --Version 1.8 11/7/13 so Rebate Team will be able to view request   
  
    l_sec := 'Setting global variables';
  
    SELECT responsibility_name
      INTO l_responsibility
      FROM fnd_responsibility_vl
     WHERE responsibility_id = fnd_global.resp_id();
  
    SELECT user_name
      INTO l_user
      FROM fnd_user
     WHERE user_id = fnd_global.user_id();
  
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user
                                                             ,l_responsibility);
    IF l_can_submit_request
    THEN
      l_sec := 'Global Variables are set User Name:  ' || l_user ||
               ' Resp Name:  ' || l_responsibility;
    
    ELSE
    
      l_sec := 'Global Variables are not set for User Name:  ' || l_user ||
               ' Resp Name:  ' || l_responsibility;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;
  
    dbms_output.enable(1000000);
  
    --Get lookups for parms used to run FAE
    l_sec := 'Getting lookups for parms used by HDS Funds Accrual Engine';
    BEGIN
      SELECT description
        INTO l_excptn_msg
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'XXCUS_REBATES_FAE_PARMS'
         AND lookup_code = 'PARM1'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find EXCEPTION MSG value : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
  
    BEGIN
      SELECT description
        INTO l_adj_offer
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'XXCUS_REBATES_FAE_PARMS'
         AND lookup_code = 'PARM3'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find ADJUST OFFER value : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
  
    BEGIN
      SELECT description
        INTO l_vol_offer
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'XXCUS_REBATES_FAE_PARMS'
         AND lookup_code = 'PARM4'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find VOLUME OFFER value : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
  
    BEGIN
      SELECT description
        INTO l_gl_post
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'XXCUS_REBATES_FAE_PARMS'
         AND lookup_code = 'PARM5'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find GL POSTING value : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
  
    BEGIN
      SELECT description
        INTO l_message_cnt
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'XXCUS_REBATES_FAE_PARMS'
         AND lookup_code = 'PARM6'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find MESSAGE CNT value : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
  
    BEGIN
      SELECT description
        INTO l_debug
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'XXCUS_REBATES_FAE_PARMS'
         AND lookup_code = 'PARM7'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find DEBUG value : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
  
    --lookup for interval time for concurrent request
    --Version 1.9
    BEGIN
      SELECT to_number(meaning)
        INTO l_interval
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'HDS_TM_RECEIPT_PARAMETERS'
         AND lookup_code = 'INTERVAL'
         AND view_application_id = 682 --Trade Management
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find value interval time for concurrent request: ' ||
                     SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
  
    --lookup for max time for concurrent request
    --Version 1.9
    BEGIN
      SELECT to_number(meaning)
        INTO l_max_time
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'HDS_TM_RECEIPT_PARAMETERS'
         AND lookup_code = 'MAX_TIME'
         AND view_application_id = 682 --Trade Management
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find value interval time for concurrent request: ' ||
                     SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
  
    SELECT lower(NAME) INTO l_sid FROM v$database;
    l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
  
    l_sec := 'Ready to submit OZF: Update Budgets from Interface Tables';
    --Version 1.7  08/26/2013  added call to submit OZF: Update Budgets from Interface Tables
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    submit_update_budget(l_err_code, retcode);
    -- 8/26/13  
  
    l_sec := 'Starting loop for running FAE by Customer';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
    FOR cust_rec IN c_vendors
    LOOP
    
      --set org
      --apps.Fnd_Request.set_org_id(c_batch.org_id);
    
      -- Call HDS Funds Accrual Engine
      BEGIN
      
        l_req_id := apps.fnd_request.submit_request('OZF'
                                                   ,'HDS_OZF_FUND_ACCRUAL'
                                                   ,NULL
                                                   ,NULL
                                                   ,FALSE
                                                   ,l_excptn_msg --'N' --Version 1.8
                                                   ,cust_rec.rebt_cust_acct_id
                                                   ,l_adj_offer --'N' --Version 1.8
                                                   ,l_vol_offer --'Y' --Version 1.8
                                                   ,l_gl_post --'N' --Version 1.8
                                                   ,l_message_cnt --0   --Version 1.8
                                                   ,l_debug --'N' --Version 1.8
                                                    );
        COMMIT;
      
        fnd_file.put_line(fnd_file.log
                         ,'Submitting HDS FAE For Vendor:  ' ||
                          cust_rec.rebt_cust_acct_id || 'Request ID:  ' ||
                          l_req_id);
      EXCEPTION
        WHEN OTHERS THEN
          l_statement := 'Error in Submitting HDS FAE For Vendor:  ' ||
                         cust_rec.rebt_cust_acct_id || ' BU Name:  ' ||
                         p_bu_nm;
          l_err_msg   := l_statement;
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
        
          UPDATE xxcus.xxcus_rebt_cust_tbl
             SET status_flag = 'E', created_date = SYSDATE
           WHERE rebt_cust_acct_id = cust_rec.rebt_cust_acct_id
             AND bu_nm = nvl(p_bu_nm, bu_nm);
        
      END;
    
      UPDATE xxcus.xxcus_rebt_cust_tbl
         SET status_flag = 'P', created_date = SYSDATE
       WHERE rebt_cust_acct_id = cust_rec.rebt_cust_acct_id
         AND bu_nm = nvl(p_bu_nm, bu_nm);
    
      COMMIT;
    END LOOP;
  
    l_sec := 'Wait for HDS Funds Accrual Engine to finish';
    --Version 1.5  wait for requests to finish
    IF (l_req_id != 0)
    THEN
      IF fnd_concurrent.wait_for_request(l_req_id
                                        ,l_interval
                                        ,l_max_time
                                        ,v_phase
                                        ,v_status
                                        ,v_dev_phase
                                        ,v_dev_status
                                        ,v_message)
      THEN
        v_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                           v_dev_phase || ' DStatus ' || v_dev_status ||
                           chr(10) || ' MSG - ' || v_message;
        -- Error Returned
        IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
        THEN
          l_statement := 'An error occured in the running of the XXCUS_TM_MISC_PKG submit_fae for BU:  ' ||
                         p_bu_nm || ' - ' || v_error_message || '.';
          l_err_msg   := l_statement;
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
        
          RAISE program_error;
        
        END IF;
        -- Then Success!
      
      ELSE
        l_statement := 'EBS Conc Program Wait timed out' || v_error_message;
        l_err_msg   := l_statement;
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        --RAISE PROGRAM_ERROR;
      END IF;
    
    ELSE
      l_statement := 'An error occured when trying to submitting the XXCUS_TM_MISC_PKG submit_fae for BU:  ' ||
                     p_bu_nm;
      l_err_msg   := l_statement;
      fnd_file.put_line(fnd_file.log, l_statement);
      fnd_file.put_line(fnd_file.output, l_statement);
      --RAISE PROGRAM_ERROR;
    END IF;
    -- end of waiting for request to finish
  
    --Version 1.6  01/29/2013 added call to submit OZF: Update Budgets from Interface Tables
    --submit_update_budget(l_err_code, retcode);    Version 1.7 8/26/13
  
    --l_sec := 'Ready to submit Resale Batches Purge';    --Version 1.9
    --Version 1.6  01/29/2013  added call to submit Resale Batches Purge 
    --submit_resale_purge(l_err_code, retcode);
  
    -- email to notify users the Funds Accrual finished
  
    l_subject     := l_sid || ' *** Funds Accrual Engine Submitted ***';
    l_body_header := '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
  
    l_body_detail := l_body_detail ||
                     '<BR>Funds Accrual Engine for month end processing was sumbitted for ' ||
                     p_bu_nm;
    l_body_footer := l_body_footer ||
                     '<BR>HDS Trade Management User-Rebates ';
  
    l_body := l_body_header || l_body_detail || l_body_footer;
  
    BEGIN
      l_err_callpoint := 'primary email - calling html_email program';
      xxcus_misc_pkg.html_email(p_to            => l_primary_email
                               ,p_from          => l_sender
                               ,p_text          => 'test'
                               ,p_subject       => l_subject
                               ,p_html          => l_body
                               ,p_smtp_hostname => l_host
                               ,p_smtp_portnum  => l_hostport);
    
      -- general email sent to the AP Support distribution group
      l_err_callpoint := 'default copy email - calling html_email program for cc email';
      xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                               ,p_from          => l_sender
                               ,p_text          => 'test'
                               ,p_subject       => l_subject
                               ,p_html          => l_body
                               ,p_smtp_hostname => l_host
                               ,p_smtp_portnum  => l_hostport);
    
    END;
  
  EXCEPTION
    /*   WHEN OTHERS THEN
         FND_FILE.PUT_LINE(FND_FILE.LOG, 'Error in Running FAE');
         RETCODE := 2;
         ERRBUF := sqlerrm;
    */ --Version 1.5
  
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(SQLERRM ||
                                                                         regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
    
      fnd_file.put_line(fnd_file.output, 'Fix the error!');
    
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
    
  END submit_fae;

  FUNCTION get_supplier_category(p_inventory_item_id IN NUMBER)
    RETURN qp_attr_mapping_pub.t_multirecord IS
    l_category_ids qp_attr_mapping_pub.t_multirecord;
  BEGIN
    l_category_ids.delete;
  
    SELECT category_id BULK COLLECT
      INTO l_category_ids
      FROM mtl_item_categories
     WHERE category_set_id = 1100000041
       AND organization_id = 84
       AND inventory_item_id = p_inventory_item_id;
  
    RETURN l_category_ids;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN l_category_ids;
  END get_supplier_category;

  FUNCTION get_state(p_cust_account_id IN NUMBER) RETURN VARCHAR2 IS
    l_state VARCHAR2(10);
  
  BEGIN
  
    SELECT loc.state
      INTO l_state
      FROM hz_locations           loc
          ,hz_party_sites         site
          ,hz_cust_acct_sites_all cust
     WHERE 1 = 1
       AND cust.cust_account_id = p_cust_account_id
       AND site.party_site_id = cust.party_site_id
       AND site.location_id = loc.location_id;
  
    -- l_state := 'CA';  
  
    RETURN l_state;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END get_state;

  PROCEDURE update_mvid_collectors(errbuf  IN OUT VARCHAR2
                                  ,retcode IN OUT VARCHAR2) IS
  
    l_customer_profile_rec_type hz_customer_profile_v2pub.customer_profile_rec_type;
    l_cust_account_profile_id   NUMBER;
    l_collector_id              NUMBER;
    l_object_version_number     NUMBER;
  
    x_return_status VARCHAR2(2000);
    x_msg_count     NUMBER;
    x_msg_data      VARCHAR2(2000);
  
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_MISC_PKG';
    l_err_callpoint VARCHAR2(75) DEFAULT 'Update MVID Collectors';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  
    v_message  VARCHAR2(250);
    l_message  VARCHAR2(150);
    l_err_msg  VARCHAR2(3000);
    l_err_code NUMBER;
    l_sec      VARCHAR2(500);
    l_req_id   NUMBER := fnd_global.conc_request_id;
  
    CURSOR c_collectors_id_csr(cv_mvid_id IN VARCHAR2) IS
      SELECT hp.attribute3
        FROM hz_parties hp, xxcusozf_mvid_bu_vw xmbu
       WHERE hp.attribute1 = 'HDS_LOB'
         AND hp.party_number = xmbu.lob_number
         AND xmbu.mvid = cv_mvid_id;
  
  BEGIN
  
    SAVEPOINT update_collectors;
  
    retcode := 0;
  
    ---Update the collector id based on MVID---
    FOR c_mvid_list IN (SELECT hca.account_number
                              ,hcp.cust_account_profile_id
                              ,hcp.object_version_number
                          FROM hz_customer_profiles hcp
                              ,hz_parties           hp
                              ,hz_cust_accounts     hca
                         WHERE hca.party_id = hp.party_id
                           AND hcp.party_id = hp.party_id
                           AND hcp.cust_account_id = hca.cust_account_id
                           AND hp.attribute1 = 'HDS_MVID'
                           AND hcp.site_use_id is null)
    LOOP
    
      OPEN c_collectors_id_csr(c_mvid_list.account_number);
      LOOP
        FETCH c_collectors_id_csr
          INTO l_collector_id;
      
        EXIT WHEN c_collectors_id_csr%NOTFOUND;
      
        l_customer_profile_rec_type.cust_account_profile_id := c_mvid_list.cust_account_profile_id;
        l_customer_profile_rec_type.collector_id            := l_collector_id;
        l_object_version_number                             := c_mvid_list.object_version_number;
      
        hz_customer_profile_v2pub.update_customer_profile('T'
                                                         ,l_customer_profile_rec_type
                                                         ,l_object_version_number
                                                         ,x_return_status
                                                         ,x_msg_count
                                                         ,x_msg_data);                                                        
      
        print_log('x_return_status = ' || substr(x_return_status, 1, 255));
        print_log('x_msg_count = ' || to_char(x_msg_count));
        print_log('x_msg_data = ' || substr(x_msg_data, 1, 255));
      
        IF x_return_status = fnd_api.g_ret_sts_error
        THEN
          print_log('Expected Error in Update MVID Collectors');
          RAISE fnd_api.g_exc_error;
        ELSIF x_return_status = fnd_api.g_ret_sts_unexp_error
        THEN
          print_log('Unexpected Error in Update MVID Collectors');
          RAISE fnd_api.g_exc_unexpected_error;
        END IF;
      
      END LOOP;
      CLOSE c_collectors_id_csr;
    END LOOP;
  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      x_return_status := fnd_api.g_ret_sts_error;
      -- Standard call to get message count and if count=1, get the message
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);
      FOR i IN 0 .. x_msg_count
      LOOP
        print_out(substr(fnd_msg_pub.get(p_msg_index => i
                                        ,p_encoded   => 'F')
                        ,1
                        ,254));
      END LOOP;
    
      retcode := 2;
      errbuf  := 'Update MVID Collectors Completed with Error';
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running XXCUS_TM_MISC_PKG.Update MVID Collectors package with PROGRAM ERROR'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
    
    WHEN fnd_api.g_exc_unexpected_error THEN
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      -- Standard call to get message count and if count=1, get the message
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);
      FOR i IN 0 .. x_msg_count
      LOOP
        print_out(substr(fnd_msg_pub.get(p_msg_index => i
                                        ,p_encoded   => 'F')
                        ,1
                        ,254));
      END LOOP;
    
      retcode := 2;
      errbuf  := 'Update MVID Collectors Completed with Error';
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running XXCUS_TM_MISC_PKG.Update MVID Collectors package with PROGRAM ERROR'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
    
    WHEN OTHERS THEN
      ROLLBACK TO update_collectors;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      print_out('Exception in Update MVID Collectors Procedure ' ||
                SQLERRM);
    
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);
      FOR i IN 0 .. x_msg_count
      LOOP
        print_out(substr(fnd_msg_pub.get(p_msg_index => i
                                        ,p_encoded   => 'F')
                        ,1
                        ,254));
      END LOOP;
    
      retcode := 2;
      errbuf  := 'Update MVID Collectors Completed with Error';
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running XXCUS_TM_MISC_PKG.Update MVID Collectors package with PROGRAM ERROR'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
    
  END update_mvid_collectors;
  -- One time fix to update BU's Party ID on existing Stock Receipt Lines
  PROCEDURE update_bu(errbuf IN OUT VARCHAR2, retcode IN OUT VARCHAR2) IS
    CURSOR c_receipts IS
      SELECT orl.resale_line_id
            ,orl.line_attribute2
            ,hp.party_name
            ,hp.party_id
        FROM ozf_resale_lines_all orl, hz_parties hp
       WHERE hp.party_name = orl.line_attribute2
         AND hp.attribute_category IN ('101', '102')
         AND hp.attribute1 = 'HDS_BU';
  
    CURSOR c_receipts1 IS
      SELECT orl.resale_line_int_id
            ,orl.line_attribute2
            ,hp.party_name
            ,hp.party_id
        FROM ozf_resale_lines_int_all orl, hz_parties hp
       WHERE hp.party_name = orl.line_attribute2
         AND hp.attribute_category IN ('101', '102')
         AND hp.attribute1 = 'HDS_BU';
  
    l_cnt NUMBER := 1;
  
  BEGIN
  
    retcode := 0;
  
    FOR i IN c_receipts1
    LOOP
      UPDATE ozf_resale_lines_int_all
         SET end_cust_party_id   = i.party_id
            ,end_cust_party_name = i.party_name --,
      --line_attribute14 = fnd_global.CONC_REQUEST_ID
       WHERE resale_line_int_id = i.resale_line_int_id;
    
      l_cnt := l_cnt + 1;
    
      IF l_cnt = 100000
      THEN
        COMMIT;
        l_cnt := 1;
      END IF;
    
    END LOOP;
  
    FOR i IN c_receipts
    LOOP
      UPDATE ozf_resale_lines_all
         SET end_cust_party_id   = i.party_id
            ,end_cust_party_name = i.party_name --,
      --line_attribute14 = fnd_global.CONC_REQUEST_ID
       WHERE resale_line_id = i.resale_line_id;
    
      l_cnt := l_cnt + 1;
    
      IF l_cnt = 100000
      THEN
        COMMIT;
        l_cnt := 1;
      END IF;
    
    END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      ozf_utility_pvt.write_conc_log('Error is ' || SQLERRM);
      retcode := 2;
    
  END update_bu;

  PROCEDURE refresh_mv(errbuf    IN OUT VARCHAR2
                      ,retcode   IN OUT VARCHAR2
                      ,p_mv_name IN VARCHAR2) IS
    v_loc VARCHAR2(2000);
  BEGIN
    EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DML';
    retcode := 0;
    v_loc   := '1000';
    --execute immediate 'TRUNCATE TABLE '||p_mv_name;
    --v_loc :='1001'; 
    --execute immediate 'alter table '||p_mv_name||' parallel 4';
    --v_loc :='1002';       
    dbms_mview.refresh(list           => p_mv_name
                      ,method         => 'C'
                      ,parallelism    => 4
                      ,atomic_refresh => FALSE);
    v_loc := '1003';
    ozf_utility_pvt.write_conc_log('Location =' || v_loc ||
                                   ', materialised view ' || p_mv_name ||
                                   ' refreshed successfully');
  EXCEPTION
    WHEN OTHERS THEN
      v_loc := '1004';
      ozf_utility_pvt.write_conc_log('Loc =' || v_loc ||
                                     'Error in Refreshing MV ' || SQLERRM);
      retcode := 2;
    
  END refresh_mv;

  ----------------------------
  -- No Longer Needed (Obsolete)
  PROCEDURE update_county(errbuf IN OUT VARCHAR2, retcode IN OUT VARCHAR2) IS
  
    p_location_rec          hz_location_v2pub.location_rec_type;
    l_object_version_number NUMBER;
  
    x_return_status VARCHAR2(2000);
    x_msg_count     NUMBER;
    x_msg_data      VARCHAR2(2000);
  
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_MISC_PKG';
    l_err_callpoint VARCHAR2(75) DEFAULT 'Update the MVID location';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  
    v_message  VARCHAR2(250);
    l_message  VARCHAR2(150);
    l_err_msg  VARCHAR2(3000);
    l_err_code NUMBER;
    l_sec      VARCHAR2(500);
    l_req_id   NUMBER := fnd_global.conc_request_id;
  
  BEGIN
  
    SAVEPOINT update_location;
  
    retcode := 0;
  
    ---Update the county based on the location id--
  
    FOR c_mvid_list IN (SELECT DISTINCT hl.location_id
                                       ,hl.object_version_number
                          FROM hz_locations     hl
                              ,hz_parties       hp
                              ,hz_party_sites   hps
                              ,hz_cust_accounts hca
                         WHERE hca.party_id = hp.party_id
                           AND hps.party_id = hp.party_id
                           AND hl.location_id = hps.location_id
                           AND (hl.county = 'X' OR hl.county IS NULL)
                           AND hp.attribute1 = 'HDS_MVID')
    LOOP
    
      p_location_rec := NULL;
    
      p_location_rec.location_id := c_mvid_list.location_id;
      p_location_rec.county      := NULL;
      l_object_version_number    := c_mvid_list.object_version_number;
    
      hz_location_v2pub.update_location('T'
                                       ,p_location_rec
                                       ,l_object_version_number
                                       ,x_return_status
                                       ,x_msg_count
                                       ,x_msg_data);
    
      --Print_Log('x_return_status = '||SUBSTR(x_return_status,1,255));
    
      IF x_return_status = fnd_api.g_ret_sts_error
      THEN
        RAISE fnd_api.g_exc_error;
      ELSIF x_return_status = fnd_api.g_ret_sts_unexp_error
      THEN
        RAISE fnd_api.g_exc_unexpected_error;
      END IF;
    
    --COMMIT WORK;
    END LOOP;
  
  EXCEPTION
    WHEN fnd_api.g_exc_error THEN
      x_return_status := fnd_api.g_ret_sts_error;
      -- Standard call to get message count and if count=1, get the message
      print_out('Expected Error in Updating Location - ' ||
                p_location_rec.location_id);
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);
      FOR i IN 0 .. x_msg_count
      LOOP
        print_out(substr(fnd_msg_pub.get(p_msg_index => i
                                        ,p_encoded   => 'F')
                        ,1
                        ,254));
      END LOOP;
    
      retcode := 2;
      errbuf  := 'Update Location';
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running XXCUS_TM_MISC_PKG.Update locations package with PROGRAM ERROR'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
    
    WHEN fnd_api.g_exc_unexpected_error THEN
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      -- Standard call to get message count and if count=1, get the message
      print_out('UnExpected Error in Updating Location - ' ||
                p_location_rec.location_id);
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);
      FOR i IN 0 .. x_msg_count
      LOOP
        print_out(substr(fnd_msg_pub.get(p_msg_index => i
                                        ,p_encoded   => 'F')
                        ,1
                        ,254));
      END LOOP;
    
      retcode := 2;
      errbuf  := 'Update location';
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running XXCUS_TM_MISC_PKG.Update locations package with PROGRAM ERROR'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
    
    WHEN OTHERS THEN
      ROLLBACK TO update_location;
      x_return_status := fnd_api.g_ret_sts_unexp_error;
      print_out('Exception in Update locations ' || SQLERRM);
    
      fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false
                               ,p_count   => x_msg_count
                               ,p_data    => x_msg_data);
      FOR i IN 0 .. x_msg_count
      LOOP
        print_out(substr(fnd_msg_pub.get(p_msg_index => i
                                        ,p_encoded   => 'F')
                        ,1
                        ,254));
      END LOOP;
    
      retcode := 2;
      errbuf  := 'Update location';
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running XXCUS_TM_MISC_PKG.Update locations package with PROGRAM ERROR'
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
    
  END update_county;

  /*******************************************************************************
  * Procedure:   SUBMIT_UPDATE_BUDGET
  * Description: This is for the concurrent request OZF: Update Budgets from 
  *              Interface Tables.  Process will update the UI with the correct
  *              amounts and close the batches
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     1/29/2013    Kathy Poling    Initial creation of the procedure
                                       RFC 36582
  1.1     5/28/2013    Kathy Poling    RFC
                                       Need to add a wait for FAE to complete before
                                       submitting update budget (can cause request to
                                       fail if FAE is still running) 
  1.2     8/26/2013    Kathy Poling    Removed the wait and changing to run prior to FAE 
                                       SR 207430  
  1.3     11/27/2013   Kathy Poling    Added call to submit_resale_purge  
                                       RFC 38762                                                                                                         
  ********************************************************************************/

  PROCEDURE submit_update_budget(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
    --
    -- Package Variables
    --
    l_req_id         NUMBER NULL;
    v_phase          VARCHAR2(50);
    v_status         VARCHAR2(50);
    v_dev_status     VARCHAR2(50);
    v_dev_phase      VARCHAR2(50);
    v_message        VARCHAR2(250);
    v_error_message  VARCHAR2(3000);
    v_interval       NUMBER := 30; -- In seconds
    v_max_time       NUMBER := 15000; -- In seconds
    l_err_msg        VARCHAR2(3000);
    l_err_code       NUMBER;
    l_sec            VARCHAR2(255);
    l_statement      VARCHAR2(9000);
    l_count          NUMBER;
    l_sleep          NUMBER := 180; --Version 1.1
    l_fae_program    NUMBER := 55330; --Version 1.1 HDS Funds Accrual Engine
    l_program_appl   NUMBER := 682; --Version 1.1 Trade Management
    l_responsibility NUMBER := 50661; --Version 1.1 XXCUS_CON
    l_resp_appl      NUMBER := 20003; --Version 1.1 XXCUS Application
    l_program        VARCHAR2(30) DEFAULT 'OZFCHBKBUDMAIN';
    l_batch_type     VARCHAR2(30) DEFAULT 'TP_ACCRUAL';
    l_batch_num      VARCHAR2(30) DEFAULT NULL;
  
    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.SUBMIT_UPDATE_BUDGET';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  
  BEGIN
  
    l_sec := 'Call to run concurrent request OZF: Update Budgets from Interface Tables.';
  
    l_req_id := fnd_request.submit_request('OZF'
                                          ,l_program
                                          ,NULL
                                          ,NULL
                                          ,FALSE
                                          ,l_batch_type
                                          ,l_batch_num);
    COMMIT;
  
    IF (l_req_id != 0)
    THEN
      IF fnd_concurrent.wait_for_request(l_req_id
                                        ,v_interval
                                        ,v_max_time
                                        ,v_phase
                                        ,v_status
                                        ,v_dev_phase
                                        ,v_dev_status
                                        ,v_message)
      THEN
        v_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                           v_dev_phase || ' DStatus ' || v_dev_status ||
                           chr(10) || ' MSG - ' || v_message;
        -- Error Returned
        IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
        THEN
          l_err_msg := 'An error occured in the running of the XXCUS_TM_MISC_PKG.SUBMIT_UPDDATE_BUDGET' ||
                       v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_err_msg);
          fnd_file.put_line(fnd_file.output, l_err_msg);
          RAISE program_error;
        
        END IF;
        -- Then Success!
        retcode := 0;
      ELSE
        l_err_msg := 'EBS timed out submitting the XXCUS_TM_MISC_PKG.SUBMIT_UPDDATE_BUDGET' ||
                     v_error_message || '.';
        fnd_file.put_line(fnd_file.log, l_err_msg);
        fnd_file.put_line(fnd_file.output, l_err_msg);
        RAISE program_error;
      END IF;
    
    ELSE
      l_err_msg := 'An error occured when trying to submitting the XXCUS_TM_MISC_PKG.SUBMIT_UPDDATE_BUDGET';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      RAISE program_error;
    END IF;
  
    --Version 1.3  11/27/2013  
    l_sec := 'Ready to submit Resale Batches Purge';
  
    submit_resale_purge(l_err_code, retcode);
  
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
  
  EXCEPTION
    WHEN program_error THEN
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
    
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
    
  END submit_update_budget;

  /*******************************************************************************
  * Procedure:   SUBMIT_RESALE_PURGE
  * Description: This is for the concurrent request OZF: Update Budgets from 
  *              Interface Tables.  Process will update the UI with the correct
  *              amounts and close the batches
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     1/29/2013    Kathy Poling    Initial creation of the procedure
                                       RFC 36582
  1.1     11/03/2013   Kathy Poling    Fixed message                              
  ********************************************************************************/

  PROCEDURE submit_resale_purge(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
    --
    -- Package Variables
    --
    l_req_id        NUMBER NULL;
    v_phase         VARCHAR2(50);
    v_status        VARCHAR2(50);
    v_dev_status    VARCHAR2(50);
    v_dev_phase     VARCHAR2(50);
    v_message       VARCHAR2(250);
    v_error_message VARCHAR2(3000);
    v_interval      NUMBER := 30; -- In seconds
    v_max_time      NUMBER := 15000; -- In seconds
    l_err_msg       VARCHAR2(3000);
    l_err_code      NUMBER;
    l_sec           VARCHAR2(255);
    l_statement     VARCHAR2(9000);
    l_program       VARCHAR2(30) DEFAULT 'OZFCHBPURG';
    l_data_source   VARCHAR2(30) DEFAULT 'ORCL_DW';
    l_batch_num     VARCHAR2(30) DEFAULT NULL;
  
    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.SUBMIT_RESALE_PURGE';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  
  BEGIN
  
    /*    SELECT user_id INTO l_user_id FROM fnd_user WHERE user_name = l_user;
      
        --  Setup parameters for running FND JOBS!
        l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user, l_responsibilty);
        IF l_can_submit_request
        THEN
          l_globalset := 'Global Variables are set.';
          dbms_output.put_line(l_globalset);
        ELSE
        
          l_globalset := 'Global Variables are not set.';
          l_sec       := 'Global Variables are not set for the ' || l_user || '.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
          dbms_output.put_line(l_sec);
          RAISE program_error;
        END IF;
    */
  
    dbms_output.put_line(l_program);
  
    l_sec := 'Call to run concurrent request Resale Batches Purge.';
  
    l_req_id := fnd_request.submit_request('OZF'
                                          ,l_program
                                          ,NULL
                                          ,NULL
                                          ,FALSE
                                          ,l_data_source
                                          ,l_batch_num);
    COMMIT;
  
    IF (l_req_id != 0)
    THEN
      IF fnd_concurrent.wait_for_request(l_req_id
                                        ,v_interval
                                        ,v_max_time
                                        ,v_phase
                                        ,v_status
                                        ,v_dev_phase
                                        ,v_dev_status
                                        ,v_message)
      THEN
        v_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                           v_dev_phase || ' DStatus ' || v_dev_status ||
                           chr(10) || ' MSG - ' || v_message;
        -- Error Returned
        IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
        THEN
          --Version 1.1  corrected message
          l_err_msg := 'An error occured in the running of the XXCUS_TM_MISC_PKG.SUBMIT_RESALE_PURGE' ||
                       v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_err_msg);
          fnd_file.put_line(fnd_file.output, l_err_msg);
          --RAISE program_error;   --Version 1.1  corrected message
        
        END IF;
        -- Then Success!
        retcode := 0;
      ELSE
        --Version 1.1  corrected message
        l_err_msg := 'EBS timed out submitting the XXCUS_TM_MISC_PKG.SUBMIT_RESALE_PURGE' ||
                     v_error_message || '.';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_err_msg);
        --RAISE program_error;    --Version 1.1  corrected message
      END IF;
    
    ELSE
      --Version 1.1  corrected message
      l_err_msg := 'An error occured when trying to submit the XXCUS_TM_MISC_PKG.SUBMIT_RESALE_PURGE';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      --RAISE program_error;    --Version 1.1  corrected message
    END IF;
  
    dbms_output.put_line(l_sec);
    dbms_output.put_line('Request ID:  ' || l_req_id);
  
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
    
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
    
  END submit_resale_purge;

  /*******************************************************************************
  * Procedure:   SUBMIT_TPA_BATCH
  * Description: This is for issue 250.8 - determin receipt batches loaded prior
  *              to Feb-2012 and execute TPA.  Need to ensure accruals are run for
  *              all existing data prior to turning on the accounting function.
  *              05-Oct-2010 is the minium report date (max report date 06-Deb-2012)
  *              these dates cover batches thru Jan-2012
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     6/05/2013     Kathy Poling    Initial creation of the procedure
                                        SR 207367
  1.1     8/07/2013     Kathy Poling    SR 207430 corrected if statement to look for 
                                        batch being null.  Date range is required 
                                        on request                                    
  ********************************************************************************/

  PROCEDURE submit_tpa_batch(errbuf       OUT VARCHAR2
                            ,retcode      OUT NUMBER
                            ,p_from_date  IN DATE
                            ,p_to_date    IN DATE
                            ,p_from_batch NUMBER
                            ,p_to_batch   NUMBER) IS
    --
    -- Package Variables
    --
    l_req_id         NUMBER NULL;
    v_phase          VARCHAR2(50);
    v_status         VARCHAR2(50);
    v_dev_status     VARCHAR2(50);
    v_dev_phase      VARCHAR2(50);
    v_message        VARCHAR2(250);
    v_error_message  VARCHAR2(3000);
    v_interval       NUMBER := 30; -- In seconds
    v_max_time       NUMBER := 15000; -- In seconds
    l_err_msg        VARCHAR2(3000);
    l_err_code       NUMBER;
    l_sec            VARCHAR2(255);
    l_statement      VARCHAR2(9000);
    l_program        VARCHAR2(30) DEFAULT 'OZFTPACCR';
    l_user_id        NUMBER := 1211; --REBTINTERFACE
    l_responsibility NUMBER := 50661; --XXCUS_CON
    l_application    NUMBER := 20003; --XXCUS
  
    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.SUBMIT_TPA_BATCH';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  
    CURSOR c_date IS
      SELECT resale_batch_id, status_code, partner_cust_account_id, org_id
        FROM ozf.ozf_resale_batches_all b
       WHERE status_code = 'CLOSED'
         AND report_date BETWEEN p_from_date AND p_to_date;
  
    CURSOR c_batch IS
      SELECT resale_batch_id, status_code, partner_cust_account_id, org_id
        FROM ozf.ozf_resale_batches_all b
       WHERE status_code = 'CLOSED'
         AND report_date BETWEEN p_from_date AND p_to_date
         AND resale_batch_id BETWEEN p_from_batch AND p_to_batch;
  
  BEGIN
  
    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
  
    mo_global.init('OZF');
    fnd_global.apps_initialize(l_user_id
                              ,l_responsibility --xxcus_con  responsibility HDS Trade Management Administrator - USD/CAD RBT
                              ,l_application); --20003 OZF  682
  
    IF p_from_batch IS NULL AND p_to_batch IS NULL --Version 1.1 
    THEN
      FOR c1 IN c_date
      LOOP
        BEGIN
          mo_global.set_policy_context('S', c1.org_id);
          fnd_request.set_org_id(c1.org_id);
        
          l_req_id := fnd_request.submit_request(application => 'OZF'
                                                ,program     => 'OZFTPACCR'
                                                ,description => NULL
                                                ,start_time  => SYSDATE
                                                ,sub_request => FALSE
                                                ,argument1   => c1.resale_batch_id
                                                ,argument2   => NULL
                                                ,argument3   => NULL
                                                ,argument4   => c1.partner_cust_account_id
                                                ,argument5   => 'Final');
        
          COMMIT;
        EXCEPTION
          WHEN OTHERS THEN
            fnd_file.put_line(fnd_file.log
                             ,'Error in Submitting TPA for Batch ID:  ' ||
                              c1.resale_batch_id || ' Cust Acct ID:  ' ||
                              c1.partner_cust_account_id);
        END;
      END LOOP;
    
    ELSE
      FOR c2 IN c_batch
      LOOP
      
        BEGIN
          mo_global.set_policy_context('S', c2.org_id);
          fnd_request.set_org_id(c2.org_id);
        
          l_req_id := fnd_request.submit_request(application => 'OZF'
                                                ,program     => 'OZFTPACCR'
                                                ,description => NULL
                                                ,start_time  => SYSDATE
                                                ,sub_request => FALSE
                                                ,argument1   => c2.resale_batch_id
                                                ,argument2   => NULL
                                                ,argument3   => NULL
                                                ,argument4   => c2.partner_cust_account_id
                                                ,argument5   => 'Final');
        
          COMMIT;
        EXCEPTION
          WHEN OTHERS THEN
            fnd_file.put_line(fnd_file.log
                             ,'Error in Submitting TPA for Batch ID:  ' ||
                              c2.resale_batch_id || ' Cust Acct ID:  ' ||
                              c2.partner_cust_account_id);
        END;
      END LOOP;
    
    END IF;
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      retcode    := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
    
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
  END submit_tpa_batch;

  /*******************************************************************************
  * Procedure:   MONDAY_REQUEST_SET
  * Description: This is for the concurrent request set XXCUS_TM_REBATE_MONDAY 
  *              Process will run the concurrent requests in request set 
  *              HDS Rebates Monday (includes FAE)
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)         DESCRIPTION
  ------- -----------   ---------------   -----------------------------------------
  1.0     04/15/2014    Kathy Poling      Initial creation of the procedure
                                          SR 244910  
  2.0     07/22/2014    Kathy Poling      Corrected Canada parm for TM Recon JE  
                                          RFC 41043 
  2.1     08/12/2014    Kathy Poling      SR 258560 Change to run Subledger Accounting 
                                          Program instead of Create Accounting 
                                          RFC 41267 
  2.2     8/22/2014     Kathy Poling      RFC 41267 remove Subledger Accounting 
                                          Program from Monday night request set
  2.3    1/13/2014     Balaguru Seshadri  Add new stages to the monday request set routine                                                                                                                                                                                                                                                                                                          
  ********************************************************************************/
  PROCEDURE monday_request_set(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
    --
    -- Package Variables
    --
    l_request_id NUMBER NULL;
  
    l_err_msg  VARCHAR2(3000);
    l_err_code NUMBER;
    l_sec      VARCHAR2(255);
  
    l_ok                   BOOLEAN := TRUE;
    l_success              NUMBER := 0;
    l_resp_application_id  NUMBER := 682; --OZF
    l_sysdate              DATE;
    l_end_date             VARCHAR2(30);
    l_period_name          VARCHAR2(15);
    l_gl_post              VARCHAR2(3);
    l_gl_post_cd           VARCHAR2(1);
    l_gl_trnsfr            VARCHAR2(3);
    l_gl_trnsfr_cd         VARCHAR2(1);
    l_report_level_meaning VARCHAR2(15);
    l_report_level         VARCHAR2(1);
    l_mode_meaning         VARCHAR2(15);
    l_mode                 VARCHAR2(1);
  
    --Exception
    srs_failed EXCEPTION;
  
    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.MONDAY_RERUEST_SET';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  
  BEGIN
  
    SELECT SYSDATE, to_char(trunc(SYSDATE), 'YYYY/MM/DD') || ' 00:00:00'
      INTO l_sysdate, l_end_date
      FROM dual;
  
    BEGIN
      SELECT period_name
        INTO l_period_name
        FROM gl.gl_period_statuses a
       WHERE application_id = 101
         AND set_of_books_id = 2021
         AND closing_status = 'O'
         AND (period_year || lpad(period_num, 2, 0)) IN
             (SELECT MIN(period_year || lpad(period_num, 2, 0))
                FROM gl.gl_period_statuses b
               WHERE application_id = 101
                 AND set_of_books_id = 2021
                 AND closing_status = 'O'
                 AND period_name NOT LIKE 'FYE%');
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find open period: ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
      
    END;
  
    -- Step 1 - call set_request_set    
    l_sec := 'Launching Request Set';
  
    fnd_file.put_line(fnd_file.log, l_sec);
    l_ok := fnd_submit.set_request_set(application => 'XXCUS'
                                      ,request_set => 'XXCUS_REBATES_MONDAY');
  
    -- --------------------------------------
    -- Stage FAE_ALL with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '1st job - FAE_ALL HDS funds Accrual Engine All Vendors';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'OZF'
                                       ,program     => 'HDS_SUBMIT_FAE_VENDORS'
                                       ,stage       => 'FAE_ALL'
                                       ,argument1   => NULL --Business Unit Name
                                        );
    ELSE
      l_success := -100;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
    --
    -- ESMS 269624: Begin
    -- 
    -- 1st job - SLA_US, Subledger accounting for US
    -- 
    IF l_ok AND l_success = 0
    THEN
      --
      l_sec := '1st job - Stage:SLA_US, Subledger Acct Program - US';
      --
      fnd_file.put_line(fnd_file.log, l_sec);
      --
      BEGIN
        SELECT meaning, submit_gl_post_flag
          INTO l_gl_post, l_gl_post_cd
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_YES_NO'
           AND xlk.lookup_code = xso.submit_gl_post_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find submit GL Post flag US: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT meaning, xso.submit_transfer_to_gl_flag
          INTO l_gl_trnsfr, l_gl_trnsfr_cd
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_YES_NO'
           AND xlk.lookup_code = xso.submit_transfer_to_gl_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find submit GL Transfer flag US: ' ||
                       SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT meaning, xso.accounting_mode_code
          INTO l_mode_meaning, l_mode
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_ACCOUNTING_ENTRY_STATUS'
           AND xlk.lookup_code = xso.accounting_mode_code
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find Mode US: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT meaning, xso.summary_report_flag
          INTO l_report_level_meaning, l_report_level
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_REPORT_LEVEL'
           AND xlk.lookup_code = xso.summary_report_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find Report Level US: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
      --
      l_ok := fnd_submit.submit_program(application => 'XLA'
                                       ,program     => 'OZFSLAAP'
                                       ,stage       => 'SLA_US'
                                       ,argument1   => 101 --Operating Unit     
                                       ,argument2   => l_resp_application_id --Source Application    
                                       ,argument3   => l_resp_application_id --Application ID    
                                       ,argument4   => 'Y' --Dummy Parameter 0     
                                       ,argument5   => 2021 --Ledger     
                                       ,argument6   => 'CHANNEL_REVENUE_MANAGEMENT' --Process Category       
                                       ,argument7   => l_end_date --End Date     
                                       ,argument8   => 'Y' --Create Accounting Flag    
                                       ,argument9   => 'Y' --Dummy Parameter 1     
                                       ,argument10  => 'F' --Accounting Mode     
                                       ,argument11  => 'Y' --Dummy Parameter 2     
                                       ,argument12  => 'N' --Errors Only     
                                       ,argument13  => l_report_level --Report  'S'    
                                       ,argument14  => l_gl_trnsfr_cd --Transfer to General Ledger     
                                       ,argument15  => 'Y' --Dummy Parameter 3     
                                       ,argument16  => l_gl_post_cd --Post in General Ledger     
                                       ,argument17  => l_sysdate --General Ledger Batch Name       
                                       ,argument18  => NULL --chr(0)              --Mixed Currency Precision     
                                       ,argument19  => 'N' --Include Zero Amount Entries and Lines     
                                       ,argument20  => NULL --Request ID     
                                       ,argument21  => NULL --Entity ID    
                                       ,argument22  => 'Trade Management' --Source Application Name    
                                       ,argument23  => 'Trade Management' --Application Name     
                                       ,argument24  => 'HDS Rebates USA' --Ledger Name     
                                       ,argument25  => 'Channel Revenue Management' --Process Category Name    
                                       ,argument26  => 'Yes' --Create Acct     
                                       ,argument27  => l_mode_meaning --Accounting Mode Name     
                                       ,argument28  => 'No' --Errors Only    
                                       ,argument29  => l_report_level_meaning --Report Level     
                                       ,argument30  => l_gl_trnsfr --Transfer To GL    
                                       ,argument31  => l_gl_post --Post in GL    
                                       ,argument32  => 'No' --Include Zero Amt Lines and Entries     
                                       ,argument33  => NULL --Valuation Method     
                                       ,argument34  => NULL --Security ID Integer 1    
                                       ,argument35  => NULL --Security ID Integer 2    
                                       ,argument36  => NULL --Security ID Integer 3    
                                       ,argument37  => NULL --Security ID Char 1     
                                       ,argument38  => NULL --Security ID Char 2     
                                       ,argument39  => NULL --Security ID Char 3     
                                       ,argument40  => NULL --Concurrent Request ID    
                                       ,argument41  => 'N' --Include User Transaction Identifiers    
                                       ,argument42  => 'No' --P_INCLUDE_USER_TRX_IDENTIFIERS     
                                       ,argument43  => 'N' --Enable Debug Log    
                                       ,argument44  => fnd_global.user_id());
    
    ELSE
      l_success := -100;
      dbms_output.put_line('Success = ' || l_success);
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF; 
    -- ESMS 269624: Begin
    -- 
    -- 1st job - SLA_CAD, Subledger accounting for Canada
    -- 
    IF l_ok AND l_success = 0
    THEN
      --
      l_sec := '1st job - Stage:SLA_CAD, Subledger Acct Program - CAD';
      --
      fnd_file.put_line(fnd_file.log, l_sec);
      --
      BEGIN
        SELECT meaning, submit_gl_post_flag
          INTO l_gl_post, l_gl_post_cd
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_YES_NO'
           AND xlk.lookup_code = xso.submit_gl_post_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find submit GL Post flag US: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT meaning, xso.submit_transfer_to_gl_flag
          INTO l_gl_trnsfr, l_gl_trnsfr_cd
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_YES_NO'
           AND xlk.lookup_code = xso.submit_transfer_to_gl_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find submit GL Transfer flag US: ' ||
                       SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT meaning, xso.accounting_mode_code
          INTO l_mode_meaning, l_mode
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_ACCOUNTING_ENTRY_STATUS'
           AND xlk.lookup_code = xso.accounting_mode_code
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find Mode US: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT meaning, xso.summary_report_flag
          INTO l_report_level_meaning, l_report_level
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_REPORT_LEVEL'
           AND xlk.lookup_code = xso.summary_report_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find Report Level US: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
      --
      l_ok := fnd_submit.submit_program(application => 'XLA'
                                       ,program     => 'OZFSLAAP'
                                       ,stage       => 'SLA_CAD'
                                       ,argument1   => 102 --Operating Unit     
                                       ,argument2   => l_resp_application_id --Source Application    
                                       ,argument3   => l_resp_application_id --Application ID    
                                       ,argument4   => 'Y' --Dummy Parameter 0     
                                       ,argument5   => 2023 --Ledger     
                                       ,argument6   => 'CHANNEL_REVENUE_MANAGEMENT' --Process Category       
                                       ,argument7   => l_end_date --End Date     
                                       ,argument8   => 'Y' --Create Accounting Flag    
                                       ,argument9   => 'Y' --Dummy Parameter 1     
                                       ,argument10  => 'F' --Accounting Mode     
                                       ,argument11  => 'Y' --Dummy Parameter 2     
                                       ,argument12  => 'N' --Errors Only     
                                       ,argument13  => l_report_level --Report  'S'    
                                       ,argument14  => l_gl_trnsfr_cd --Transfer to General Ledger     
                                       ,argument15  => 'Y' --Dummy Parameter 3     
                                       ,argument16  => l_gl_post_cd --Post in General Ledger     
                                       ,argument17  => l_sysdate --General Ledger Batch Name       
                                       ,argument18  => NULL --chr(0)              --Mixed Currency Precision     
                                       ,argument19  => 'N' --Include Zero Amount Entries and Lines     
                                       ,argument20  => NULL --Request ID     
                                       ,argument21  => NULL --Entity ID    
                                       ,argument22  => 'Trade Management' --Source Application Name    
                                       ,argument23  => 'Trade Management' --Application Name     
                                       ,argument24  => 'HDS Rebates USA' --Ledger Name     
                                       ,argument25  => 'Channel Revenue Management' --Process Category Name    
                                       ,argument26  => 'Yes' --Create Acct     
                                       ,argument27  => l_mode_meaning --Accounting Mode Name     
                                       ,argument28  => 'No' --Errors Only    
                                       ,argument29  => l_report_level_meaning --Report Level     
                                       ,argument30  => l_gl_trnsfr --Transfer To GL    
                                       ,argument31  => l_gl_post --Post in GL    
                                       ,argument32  => 'No' --Include Zero Amt Lines and Entries     
                                       ,argument33  => NULL --Valuation Method     
                                       ,argument34  => NULL --Security ID Integer 1    
                                       ,argument35  => NULL --Security ID Integer 2    
                                       ,argument36  => NULL --Security ID Integer 3    
                                       ,argument37  => NULL --Security ID Char 1     
                                       ,argument38  => NULL --Security ID Char 2     
                                       ,argument39  => NULL --Security ID Char 3     
                                       ,argument40  => NULL --Concurrent Request ID    
                                       ,argument41  => 'N' --Include User Transaction Identifiers    
                                       ,argument42  => 'No' --P_INCLUDE_USER_TRX_IDENTIFIERS     
                                       ,argument43  => 'N' --Enable Debug Log    
                                       ,argument44  => fnd_global.user_id());
    
    ELSE
      l_success := -100;
      dbms_output.put_line('Success = ' || l_success);
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
    --
    -- ESMS 269624: Begin
    -- 
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := 'Stage: XFER_TO_GL_US_CAD: Transfer to GL for US';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XLA'
                                       ,program     => 'XLAGLTRN'
                                       ,stage       => 'XFER_TO_GL_US_CAD'
                                       ,argument1   => 682
                                       ,argument2   => 682
                                       ,argument3   => 'Y'
                                       ,argument4   => 2021
                                       ,argument5   => 'CHANNEL_REVENUE_MANAGEMENT'
                                       ,argument6   => to_char(trunc(SYSDATE)
                                                              ,'YYYY/MM/DD HH24:MI:SS') --2014/10/16 00:00:00
                                       ,argument7   => 'N'
                                       ,argument8   => NULL
                                       ,argument9   => NULL
                                       ,argument10  => 'Y'
                                       ,argument11  => NULL
                                       ,argument12  => NULL
                                       ,argument13  => 'Y'
                                       ,argument14  => 'Y'
                                       ,argument15  => 'N'
                                       ,argument16  => NULL
                                       ,argument17  => NULL
                                       ,argument18  => NULL
                                       ,argument19  => NULL
                                       ,argument20  => 'Trade Management'
                                       ,argument21  => 'Trade Management'
                                       ,argument22  => 'HDS Rebates USA'
                                       ,argument23  => 'Channel Revenue Management'
                                       ,argument24  => 'No'
                                       ,argument25  => NULL
                                       ,argument26  => NULL
                                       ,argument27  => NULL
                                       ,argument28  => 'Yes'
                                       ,argument29  => 'No'
                                       ,argument30  => NULL
                                       ,argument31  => NULL
                                       ,argument32  => NULL
                                       ,argument33  => NULL
                                       ,argument34  => NULL
                                       ,argument35  => NULL
                                       ,argument36  => NULL
                                       ,argument37  => NULL
                                       ,argument38  => 'N');
    ELSE
      l_success := -104;
      print_log(l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
    --
    -- ESMS 269624: Begin Transfer to GL for Canada
    -- 
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := 'Stage: XFER_TO_GL_US_CAD: Transfer to GL for CAD';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XLA'
                                       ,program     => 'XLAGLTRN'
                                       ,stage       => 'XFER_TO_GL_US_CAD'
                                       ,argument1   => 682
                                       ,argument2   => 682
                                       ,argument3   => 'Y'
                                       ,argument4   => 2023
                                       ,argument5   => 'CHANNEL_REVENUE_MANAGEMENT'
                                       ,argument6   => to_char(trunc(SYSDATE)
                                                              ,'YYYY/MM/DD HH24:MI:SS') --2014/10/16 00:00:00
                                       ,argument7   => 'N'
                                       ,argument8   => NULL
                                       ,argument9   => NULL
                                       ,argument10  => 'Y'
                                       ,argument11  => NULL
                                       ,argument12  => NULL
                                       ,argument13  => 'Y'
                                       ,argument14  => 'Y'
                                       ,argument15  => 'N'
                                       ,argument16  => NULL
                                       ,argument17  => NULL
                                       ,argument18  => NULL
                                       ,argument19  => NULL
                                       ,argument20  => 'Trade Management'
                                       ,argument21  => 'Trade Management'
                                       ,argument22  => 'HDS Rebates USA'
                                       ,argument23  => 'Channel Revenue Management'
                                       ,argument24  => 'No'
                                       ,argument25  => NULL
                                       ,argument26  => NULL
                                       ,argument27  => NULL
                                       ,argument28  => 'Yes'
                                       ,argument29  => 'No'
                                       ,argument30  => NULL
                                       ,argument31  => NULL
                                       ,argument32  => NULL
                                       ,argument33  => NULL
                                       ,argument34  => NULL
                                       ,argument35  => NULL
                                       ,argument36  => NULL
                                       ,argument37  => NULL
                                       ,argument38  => 'N');
    ELSE
      l_success := -104;
      print_log(l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
    --           
    -- --------------------------------------
    -- New stage TM_SLA_ACCRUALS with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '4th job - TM_SLA_ACCRUALS HDS Rebates: Generate TM SLA Accruals Extract -By Period';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_TM_SLA_PERIOD'
                                       ,stage       => 'TM_SLA_ACCRUALS'
                                       ,argument1   => l_period_name --Period
                                       ,argument2   => '131100' --Rebates GL Account
                                        );
    ELSE
      l_success := -140;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
    -- --------------------------------------
    -- New stage EXTRACT_YTD_INCOME_B with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '@Seq: 45, HDS Rebates: Extract YTD Income -By Period';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_YTD_INCOME_PERIOD'
                                       ,stage       => 'EXTRACT_YTD_INCOME_B'
                                       ,argument1   => l_period_name --Fiscal Period
                                        );
    ELSE
      l_success := -145;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
    --
    -- --------------------------------------
    -- New stage PAM_REFRESH with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '5th job - PAM_REFRESH HDS  HDS TM PAM Refresh';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_PAM_REFRESH'
                                       ,stage       => 'PAM_REFRESH');
    ELSE
      l_success := -150;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
  
    -- --------------------------------------
    -- New stage GEN_TM_GL_JOURNALS_US with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '6th job - GEN_TM_GL_JOURNALS_US HDS Rebates: Generate TM GL Journals Extract';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_TM_RECON_JOURNALS'
                                       ,stage       => 'GEN_TM_GL_JOURNALS_US'
                                       ,argument1   => 'US' --Territory
                                       ,argument2   => l_period_name --Period
                                       ,argument3   => '131100' --Rebates GL Account
                                        );
    ELSE
      l_success := -160;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
  
    -- --------------------------------------
    -- New stage GEN_TM_GL_JOURNALS_CAD with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '7th job - GEN_TM_GL_JOURNALS_CAD HDS Rebates: Generate TM GL Journals Extract';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_TM_RECON_JOURNALS'
                                       ,stage       => 'GEN_TM_GL_JOURNALS_CAD'
                                       ,argument1   => 'CA' --Territory   --Version 2.0
                                       ,argument2   => l_period_name --Period
                                       ,argument3   => '131100' --Rebates GL Account
                                        );
    ELSE
      l_success := -170;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
    --    
    -- --------------------------------------
    -- New stage EXTRACT_GL_SUMMARY with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '8th job, HDS Rebates: Extract GL Summary Data -By Period';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_GL_SUMMARY'
                                       ,stage       => 'EXTRACT_GL_SUMMARY'
                                       ,argument1   => l_period_name --Fiscal Period
                                        );
    ELSE
      l_success := -180;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
    --     
  /*
    -- --------------------------------------
    -- New stage REFRESH_ACCRUALS_MV with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '8th job - REFRESH_ACCRUALS_MV HDS Refresh Rebates Materialized Views (Accruals MV)';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUSOZF_MV_REFRESH'
                                       ,stage       => 'REFRESH_ACCRUALS_MV'
                                       ,argument1   => 'XXCUSOZF_ACCRUALS_MV' --Materialized View  Name
                                        );
    ELSE
      l_success := -180;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
  */ -- commented on 01/13/2015 as we don't need to refresh the view anymore. we are using a xxcus_ozf_accruals_b for reporting
    -- -----------------------------------------------
    -- All requests in the set have been submitted now
    -- -----------------------------------------------
    IF l_ok AND l_success = 0
    THEN
      l_request_id := fnd_submit.submit_set(NULL, FALSE);
    
      fnd_file.put_line(fnd_file.log, 'Request_id = ' || l_request_id);
      COMMIT;
    
    ELSE
      l_success := -1000;
      fnd_file.put_line(fnd_file.log, 'Failed = ' || l_success);
      l_sec := 'Call to submit_set';
      RAISE srs_failed;
    END IF;
  
  EXCEPTION
    WHEN srs_failed THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_sec || ' Failed = ' || l_success || ' : ' ||
                    fnd_message.get;
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                         dbms_utility.format_error_stack() ||
                                                                         ' Error_Backtrace...' ||
                                                                         dbms_utility.format_error_backtrace()
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
    
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Request set submission failed - unknown error: ' ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
    
  END monday_request_set;

  /*******************************************************************************
  * Procedure:   SATURDAY_REQUEST_SET
  * Description: This is for the concurrent request set XXCUS_TM_REBATE_SATURDAY 
  *              Process will run the concurrent requests in request set 
  *              HDS Rebates Saturday (includes OZF Rebuild)
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     04/15/2014    Kathy Poling    Initial creation of the procedure
                                        SR 244910
  2.0     07/22/2014    Kathy Poling    Corrected Canada parm for TM Recon JE 
                                        RFC 41043 
  2.1     08/12/2014    Kathy Poling    SR 258560 Change to run Subledger Accounting 
                                        Program instead of Create Accounting 
                                        RFC 41267                                                                                                                                                                                 
  ********************************************************************************/
  PROCEDURE saturday_request_set(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
    --
    -- Package Variables
    --
    l_request_id NUMBER NULL;
  
    l_err_msg  VARCHAR2(3000);
    l_err_code NUMBER;
    l_sec      VARCHAR2(255);
  
    l_ok                   BOOLEAN := TRUE;
    l_success              NUMBER := 0;
    l_resp_application_id  NUMBER := 682; --OZF
    l_sysdate              DATE;
    l_end_date             VARCHAR2(30);
    l_period_name          VARCHAR2(15);
    l_gl_post              VARCHAR2(3);
    l_gl_post_cd           VARCHAR2(1);
    l_gl_trnsfr            VARCHAR2(3);
    l_gl_trnsfr_cd         VARCHAR2(1);
    l_report_level_meaning VARCHAR2(15);
    l_report_level         VARCHAR2(1);
    l_mode_meaning         VARCHAR2(15);
    l_mode                 VARCHAR2(1);
    l_list_hdr_low         NUMBER;
    l_list_hdr_high        NUMBER;
  
    --Exception
    srs_failed EXCEPTION;
  
    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.SATURDAY_RERUEST_SET';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  
  BEGIN
  
    SELECT SYSDATE, to_char(trunc(SYSDATE), 'YYYY/MM/DD') || ' 00:00:00'
      INTO l_sysdate, l_end_date
      FROM dual;
  
    BEGIN
      SELECT period_name
        INTO l_period_name
        FROM gl.gl_period_statuses a
       WHERE application_id = 101
         AND set_of_books_id = 2021
         AND closing_status = 'O'
         AND (period_year || lpad(period_num, 2, 0)) IN
             (SELECT MIN(period_year || lpad(period_num, 2, 0))
                FROM gl.gl_period_statuses b
               WHERE application_id = 101
                 AND set_of_books_id = 2021
                 AND closing_status = 'O'
                 AND period_name NOT LIKE 'FYE%');
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find open period: ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
      
    END;
  
    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    l_sec := 'Apps Initialize';
    fnd_global.apps_initialize(fnd_global.user_id()
                              ,fnd_global.resp_id()
                              ,l_resp_application_id);
  
    -- Step 1 - call set_request_set    
    l_sec := 'Launching Request Set';
    --dbms_output.put_line(l_sec);
    fnd_file.put_line(fnd_file.log, l_sec);
    l_ok := fnd_submit.set_request_set(application => 'XXCUS'
                                      ,request_set => 'XXCUS_REBATES_SATURDAY');
  
    -- --------------------------------------
    -- Stage XXCUS_OZF_REBUILD_INDX with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '1st job - XXCUS_OZF_REBUILD_INDX HDS Rebates: Rebuild OZF Stack -Master';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_REBUILD_OZF_STACK_MASTER'
                                       ,stage       => 'XXCUS_OZF_REBUILD_INDX');
    ELSE
      l_success := -100;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
  
    -- --------------------------------------
    -- Stage GATHER_OZF_STATS with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '2nd job - GATHER_OZF_STATS HDS Rebates: Gather Schema Statistics';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'FND'
                                       ,program     => 'FNDGSCST'
                                       ,stage       => 'GATHER_OZF_STATS'
                                       ,argument1   => 'OZF' --Schema Name
                                       ,argument2   => '10' --Estimate Percent
                                       ,argument3   => '4' --Degree
                                       ,argument4   => 'BACK' --Backup Flag
                                       ,argument5   => NULL --Restart Request ID
                                       ,argument6   => 'LASTRUN' --History Mode
                                       ,argument7   => 'GATHER' --Gather Options
                                       ,argument8   => '10' --Modifications Threshold
                                       ,argument9   => 'N' --Invalidate Dependent Cursors
                                        );
    ELSE
      l_success := -120;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
  
    -- --------------------------------------
    -- Stage XXCUS_OZF_EXT_STATS with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '3rd job - XXCUS_OZF_EXT_STATS HDS Rebates: Gather Extended Stats';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_GATHER_EXT_STATS'
                                       ,stage       => 'XXCUS_OZF_EXT_STATS');
    ELSE
      l_success := -130;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
  
    IF l_ok AND l_success = 0
    THEN
      -- ------------------------------------
      -- New stage CREATE_ACCTG_US with 3 request
      -- ----------------------------------- 
      l_sec := '4th job - CREATE_ACCTG_US Create Accounting - US request';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      BEGIN
        SELECT meaning, submit_gl_post_flag
          INTO l_gl_post, l_gl_post_cd
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_YES_NO'
           AND xlk.lookup_code = xso.submit_gl_post_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find submit GL Post flag US: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT meaning, xso.submit_transfer_to_gl_flag
          INTO l_gl_trnsfr, l_gl_trnsfr_cd
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_YES_NO'
           AND xlk.lookup_code = xso.submit_transfer_to_gl_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find submit GL Transfer flag US: ' ||
                       SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT meaning, xso.accounting_mode_code
          INTO l_mode_meaning, l_mode
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_ACCOUNTING_ENTRY_STATUS'
           AND xlk.lookup_code = xso.accounting_mode_code
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find Mode US: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT meaning, xso.summary_report_flag
          INTO l_report_level_meaning, l_report_level
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_REPORT_LEVEL'
           AND xlk.lookup_code = xso.summary_report_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find Report Level US: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
 /*   --Version 2.1
      l_ok := fnd_submit.submit_program(application => 'XLA'
                                       ,program     => 'XLAACCPB'
                                       ,stage       => 'CREATE_ACCTG_US'
                                       ,argument1   => l_resp_application_id --Application
                                       ,argument2   => l_resp_application_id --Source Application
                                       ,argument3   => 'Y' --Dummy Parameter 0
                                       ,argument4   => 2021 --Ledger
                                       ,argument5   => 'CHANNEL_REVENUE_MANAGEMENT' --Process Category
                                       ,argument6   => l_end_date --End Date
                                       ,argument7   => 'Y' --Create Accounting
                                       ,argument8   => 'Y' --Dummy Parameter
                                       ,argument9   => l_mode --Mode  'F'
                                       ,argument10  => 'Y' --Dummy Parameter
                                       ,argument11  => 'N' --Errors Only
                                       ,argument12  => l_report_level --Report 'S'
                                       ,argument13  => l_gl_trnsfr_cd --Transfer to General Ledger
                                       ,argument14  => 'Y' --Dummy Parameter
                                       ,argument15  => l_gl_post_cd --Post in General Ledger
                                       ,argument16  => l_sysdate --General Ledger Batch Name
                                       ,argument17  => NULL --chr(0)              --Mixed Currency Precision
                                       ,argument18  => 'N' --Include Zero Amount Entries and Lines
                                       ,argument19  => NULL --Request ID
                                       ,argument20  => NULL --Entity ID
                                       ,argument21  => 'Trade Management' --Source Application Name
                                       ,argument22  => 'Trade Management' --Application Name
                                       ,argument23  => 'HDS Rebates USA' --Ledger Name
                                       ,argument24  => 'Channel Revenue Management' --Process Category Name
                                       ,argument25  => 'Yes' --Create Acct
                                       ,argument26  => l_mode_meaning --Accounting Mode Name 'Final
                                       ,argument27  => 'No' --Errors Only
                                       ,argument28  => l_report_level_meaning --Report Level 'Summary'
                                       ,argument29  => l_gl_trnsfr --Transfer To GL
                                       ,argument30  => l_gl_post --Post in GL
                                       ,argument31  => 'No' --Include Zero Amt Lines and Entries
                                       ,argument32  => NULL --Valuation Method
                                       ,argument33  => NULL --Security ID Integer 1
                                       ,argument34  => NULL --Security ID Integer 2
                                       ,argument35  => NULL --Security ID Integer 3
                                       ,argument36  => NULL --Security ID Char 1
                                       ,argument37  => NULL --Security ID Char 2
                                       ,argument38  => NULL --Security ID Char 3
                                       ,argument39  => NULL --Concurrent Request ID
                                       ,argument40  => 'N' --Include User Transaction Identifiers
                                       ,argument41  => 'No' --P_INCLUDE_USER_TRX_IDENTIFIERS
                                       ,argument42  => 'N' --Enable Debug Log
                                       ,argument43  => fnd_global.user_id()
                                       ,argument44  => 'N' --P_SCALABLE_FLAG                                
                                        );
      */                                        
      --Version 2.1
        l_ok := fnd_submit.submit_program(application => 'XLA'
                                       ,program     => 'OZFSLAAP'
                                       ,stage       => 'CREATE_ACCTG_US'
                                       ,argument1   => 101 --Operating Unit     
                                       ,argument2   => l_resp_application_id --Source Application    
                                       ,argument3   => l_resp_application_id --Application ID    
                                       ,argument4   => 'Y' --Dummy Parameter 0     
                                       ,argument5   => 2021 --Ledger     
                                       ,argument6   => 'CHANNEL_REVENUE_MANAGEMENT' --Process Category       
                                       ,argument7   => l_end_date --End Date     
                                       ,argument8   => 'Y' --Create Accounting Flag    
                                       ,argument9   => 'Y' --Dummy Parameter 1     
                                       ,argument10  => 'F' --Accounting Mode     
                                       ,argument11  => 'Y' --Dummy Parameter 2     
                                       ,argument12  => 'N' --Errors Only     
                                       ,argument13  => l_report_level --Report  'S'    
                                       ,argument14  => l_gl_trnsfr_cd --Transfer to General Ledger     
                                       ,argument15  => 'Y' --Dummy Parameter 3     
                                       ,argument16  => l_gl_post_cd --Post in General Ledger     
                                       ,argument17  => l_sysdate --General Ledger Batch Name       
                                       ,argument18  => NULL --chr(0)              --Mixed Currency Precision     
                                       ,argument19  => 'N' --Include Zero Amount Entries and Lines     
                                       ,argument20  => NULL --Request ID     
                                       ,argument21  => NULL --Entity ID    
                                       ,argument22  => 'Trade Management' --Source Application Name    
                                       ,argument23  => 'Trade Management' --Application Name     
                                       ,argument24  => 'HDS Rebates USA' --Ledger Name     
                                       ,argument25  => 'Channel Revenue Management' --Process Category Name    
                                       ,argument26  => 'Yes' --Create Acct     
                                       ,argument27  => l_mode_meaning --Accounting Mode Name     
                                       ,argument28  => 'No' --Errors Only    
                                       ,argument29  => l_report_level_meaning --Report Level     
                                       ,argument30  => l_gl_trnsfr --Transfer To GL    
                                       ,argument31  => l_gl_post --Post in GL    
                                       ,argument32  => 'No' --Include Zero Amt Lines and Entries     
                                       ,argument33  => NULL --Valuation Method     
                                       ,argument34  => NULL --Security ID Integer 1    
                                       ,argument35  => NULL --Security ID Integer 2    
                                       ,argument36  => NULL --Security ID Integer 3    
                                       ,argument37  => NULL --Security ID Char 1     
                                       ,argument38  => NULL --Security ID Char 2     
                                       ,argument39  => NULL --Security ID Char 3     
                                       ,argument40  => NULL --Concurrent Request ID    
                                       ,argument41  => 'N' --Include User Transaction Identifiers    
                                       ,argument42  => 'No' --P_INCLUDE_USER_TRX_IDENTIFIERS     
                                       ,argument43  => 'N' --Enable Debug Log    
                                       ,argument44  => fnd_global.user_id()); 
    ELSE
      l_success := -140;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
  
    -- --------------------------------------
    -- 2 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      BEGIN
        SELECT MIN(list_header_id), MAX(list_header_id)
          INTO l_list_hdr_low, l_list_hdr_high
          FROM qp_list_headers_vl
         WHERE list_type_code IN
               ('PRL', 'AGR', 'SLT', 'DLT', 'PRO', 'DEL', 'PML', 'CHARGES')
           AND view_flag = 'Y'
           AND (orig_org_id IN (101, 102) OR
               (orig_org_id IS NULL AND global_flag = 'Y'));
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find list_header_id: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      l_sec := '4th job - CREATE_ACCTG_US (2nd request) QP: Maintains the denormalized data in QP Qualifiers)';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'QP'
                                       ,program     => 'QPXDENOB'
                                       ,stage       => 'CREATE_ACCTG_US'
                                       ,argument1   => l_list_hdr_low --LIST_HEADER_ID_LOW  
                                       ,argument2   => l_list_hdr_high --List Header ID (High)
                                       ,argument3   => 'ALL' --Update Type
                                       ,argument4   => l_list_hdr_low --Dummy
                                        );
    ELSE
      l_success := -142;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
  
    -- --------------------------------------
    -- 3 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '4th job - CREATE_ACCTG_US (3nd request) Build Attribute Mapping Rules)';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'QP'
                                       ,program     => 'QPXPSRCB'
                                       ,stage       => 'CREATE_ACCTG_US');
    ELSE
      l_success := -143;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
  
    -- --------------------------------------
    -- New stage CREATE_ACCTG_CAN with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '5th job - CREATE_ACCTG_CAN Create Accounting - CAD request';
      fnd_file.put_line(fnd_file.log, l_sec);
    
      BEGIN
        SELECT meaning, submit_gl_post_flag
          INTO l_gl_post, l_gl_post_cd
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_YES_NO'
           AND xlk.lookup_code = xso.submit_gl_post_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2023
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find submit GL Post flag CAD: ' ||
                       SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT meaning, xso.submit_transfer_to_gl_flag
          INTO l_gl_trnsfr, l_gl_trnsfr_cd
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_YES_NO'
           AND xlk.lookup_code = xso.submit_transfer_to_gl_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2023
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find submit GL Transfer flag CAD: ' ||
                       SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT meaning, xso.accounting_mode_code
          INTO l_mode_meaning, l_mode
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_ACCOUNTING_ENTRY_STATUS'
           AND xlk.lookup_code = xso.accounting_mode_code
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2023
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find Mode CAD: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT meaning, xso.summary_report_flag
          INTO l_report_level_meaning, l_report_level
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_REPORT_LEVEL'
           AND xlk.lookup_code = xso.summary_report_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2023
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find Report Level CAD: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
  /*  --Version 2.1
      l_ok := fnd_submit.submit_program(application => 'XLA'
                                       ,program     => 'XLAACCPB'
                                       ,stage       => 'CREATE_ACCTG_CAN'
                                       ,argument1   => l_resp_application_id --Application
                                       ,argument2   => l_resp_application_id --Source Application
                                       ,argument3   => 'Y' --Dummy Parameter 0
                                       ,argument4   => 2023 --Ledger
                                       ,argument5   => 'CHANNEL_REVENUE_MANAGEMENT' --Process Category
                                       ,argument6   => l_end_date --End Date
                                       ,argument7   => 'Y' --Create Accounting
                                       ,argument8   => 'Y' --Dummy Parameter
                                       ,argument9   => l_mode --Mode 'F'
                                       ,argument10  => 'Y' --Dummy Parameter
                                       ,argument11  => 'N' --Errors Only
                                       ,argument12  => l_report_level --Report 'S'
                                       ,argument13  => l_gl_trnsfr_cd --Transfer to General Ledger
                                       ,argument14  => 'Y' --Dummy Parameter
                                       ,argument15  => l_gl_post_cd --Post in General Ledger
                                       ,argument16  => l_sysdate --General Ledger Batch Name
                                       ,argument17  => NULL --chr(0)              --Mixed Currency Precision
                                       ,argument18  => 'N' --Include Zero Amount Entries and Lines
                                       ,argument19  => NULL --Request ID
                                       ,argument20  => NULL --Entity ID
                                       ,argument21  => 'Trade Management' --Source Application Name
                                       ,argument22  => 'Trade Management' --Application Name
                                       ,argument23  => 'HDS Rebates USA' --Ledger Name
                                       ,argument24  => 'Channel Revenue Management' --Process Category Name
                                       ,argument25  => 'Yes' --Create Acct
                                       ,argument26  => l_mode_meaning --Accounting Mode Name 'Final'
                                       ,argument27  => 'No' --Errors Only
                                       ,argument28  => l_report_level_meaning --Report Level 'Summary'
                                       ,argument29  => l_gl_trnsfr --Transfer To GL
                                       ,argument30  => l_gl_post --Post in GL
                                       ,argument31  => 'No' --Include Zero Amt Lines and Entries
                                       ,argument32  => NULL --Valuation Method
                                       ,argument33  => NULL --Security ID Integer 1
                                       ,argument34  => NULL --Security ID Integer 2
                                       ,argument35  => NULL --Security ID Integer 3
                                       ,argument36  => NULL --Security ID Char 1
                                       ,argument37  => NULL --Security ID Char 2
                                       ,argument38  => NULL --Security ID Char 3
                                       ,argument39  => NULL --Concurrent Request ID
                                       ,argument40  => 'N' --Include User Transaction Identifiers
                                       ,argument41  => 'No' --P_INCLUDE_USER_TRX_IDENTIFIERS
                                       ,argument42  => 'N' --Enable Debug Log
                                       ,argument43  => fnd_global.user_id()
                                       ,argument44  => 'N' --P_SCALABLE_FLAG); 
      */                                  
      --Version 2.1                                  
      l_ok := fnd_submit.submit_program(application => 'XLA'
                                       ,program     => 'OZFSLAAP'
                                       ,stage       => 'CREATE_ACCTG_CAN'
                                       ,argument1   => 102 --Operating Unit     
                                       ,argument2   => l_resp_application_id --Source Application    
                                       ,argument3   => l_resp_application_id --Application ID    
                                       ,argument4   => 'Y' --Dummy Parameter 0     
                                       ,argument5   => 2023 --Ledger     
                                       ,argument6   => 'CHANNEL_REVENUE_MANAGEMENT' --Process Category       
                                       ,argument7   => l_end_date --End Date     
                                       ,argument8   => 'Y' --Create Accounting Flag    
                                       ,argument9   => 'Y' --Dummy Parameter 1     
                                       ,argument10  => 'F' --Accounting Mode     
                                       ,argument11  => 'Y' --Dummy Parameter 2     
                                       ,argument12  => 'N' --Errors Only     
                                       ,argument13  => l_report_level --Report  'S'    
                                       ,argument14  => l_gl_trnsfr_cd --Transfer to General Ledger     
                                       ,argument15  => 'Y' --Dummy Parameter 3     
                                       ,argument16  => l_gl_post_cd --Post in General Ledger     
                                       ,argument17  => l_sysdate --General Ledger Batch Name       
                                       ,argument18  => NULL --chr(0)              --Mixed Currency Precision     
                                       ,argument19  => 'N' --Include Zero Amount Entries and Lines     
                                       ,argument20  => NULL --Request ID     
                                       ,argument21  => NULL --Entity ID    
                                       ,argument22  => 'Trade Management' --Source Application Name    
                                       ,argument23  => 'Trade Management' --Application Name     
                                       ,argument24  => 'HDS Rebates CAN' --Ledger Name     
                                       ,argument25  => 'Channel Revenue Management' --Process Category Name    
                                       ,argument26  => 'Yes' --Create Acct     
                                       ,argument27  => l_mode_meaning --Accounting Mode Name     
                                       ,argument28  => 'No' --Errors Only    
                                       ,argument29  => l_report_level_meaning --Report Level     
                                       ,argument30  => l_gl_trnsfr --Transfer To GL    
                                       ,argument31  => l_gl_post --Post in GL    
                                       ,argument32  => 'No' --Include Zero Amt Lines and Entries     
                                       ,argument33  => NULL --Valuation Method     
                                       ,argument34  => NULL --Security ID Integer 1    
                                       ,argument35  => NULL --Security ID Integer 2    
                                       ,argument36  => NULL --Security ID Integer 3    
                                       ,argument37  => NULL --Security ID Char 1     
                                       ,argument38  => NULL --Security ID Char 2     
                                       ,argument39  => NULL --Security ID Char 3     
                                       ,argument40  => NULL --Concurrent Request ID    
                                       ,argument41  => 'N' --Include User Transaction Identifiers    
                                       ,argument42  => 'No' --P_INCLUDE_USER_TRX_IDENTIFIERS     
                                       ,argument43  => 'N' --Enable Debug Log    
                                       ,argument44  => fnd_global.user_id());                                        
    
    ELSE
      l_success := -150;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
  
    -- --------------------------------------
    -- New stage SLA_ACCRUALS_EXTRACT with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '6th job - TM_SLA_ACCRUALS HDS Rebates: Generate TM SLA Accruals Extract -By Period';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_TM_SLA_PERIOD'
                                       ,stage       => 'SLA_ACCRUALS_EXTRACT'
                                       ,argument1   => l_period_name --Period
                                       ,argument2   => '131100' --Rebates GL Account
                                        );
    ELSE
      l_success := -160;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
    -- --------------------------------------
    -- New stage EXTRACT_YTD_INCOME_B with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '@Seq: 65, HDS Rebates: Extract YTD Income -By Period';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_YTD_INCOME_PERIOD'
                                       ,stage       => 'EXTRACT_YTD_INCOME_B'
                                       ,argument1   => l_period_name --Fiscal Period
                                        );
    ELSE
      l_success := -165;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
    --  
    -- --------------------------------------
    -- New stage REFRESH_PAM with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '7th job - REFRESH_PAM  HDS TM PAM Refresh';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_PAM_REFRESH'
                                       ,stage       => 'REFRESH_PAM');
    ELSE
      l_success := -170;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
  
    -- --------------------------------------
    -- New stage GL_JE_EXTRACT_US with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '8th job - GL_JE_EXTRACT_US HDS Rebates: Generate TM GL Journals Extract';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_TM_RECON_JOURNALS'
                                       ,stage       => 'GL_JE_EXTRACT_US'
                                       ,argument1   => 'US' --Territory
                                       ,argument2   => l_period_name --Period
                                       ,argument3   => '131100' --Rebates GL Account
                                        );
    ELSE
      l_success := -180;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
  
    -- --------------------------------------
    -- New stage GL_JE_EXTRACT_CAN with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '9th job - GL_JE_EXTRACT_CAN HDS Rebates: Generate TM GL Journals Extract';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_TM_RECON_JOURNALS'
                                       ,stage       => 'GL_JE_EXTRACT_CAN'
                                       ,argument1   => 'CA' --Territory --Version 2.0
                                       ,argument2   => l_period_name --Period
                                       ,argument3   => '131100' --Rebates GL Account
                                        );
    ELSE
      l_success := -190;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
    --    
    -- --------------------------------------
    -- New stage EXTRACT_GL_SUMMARY with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '10th job, HDS Rebates: Extract GL Summary Data -By Period';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_GL_SUMMARY'
                                       ,stage       => 'EXTRACT_GL_SUMMARY'
                                       ,argument1   => l_period_name --Fiscal Period
                                        );
    ELSE
      l_success := -200;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
    --    
    -- -----------------------------------------------
    -- All requests in the set have been submitted now
    -- -----------------------------------------------
    IF l_ok AND l_success = 0
    THEN
      l_request_id := fnd_submit.submit_set(NULL, FALSE);
    
      fnd_file.put_line(fnd_file.log, 'Request_id = ' || l_request_id);
      COMMIT;
    
    ELSE
      l_success := -2000;
      l_sec     := 'Call to submit_set failed';
      fnd_file.put_line(fnd_file.log, 'Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
  
  EXCEPTION
    WHEN srs_failed THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_sec || ' Failed = ' || l_success || ' : ' ||
                    fnd_message.get;
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                         dbms_utility.format_error_stack() ||
                                                                         ' Error_Backtrace...' ||
                                                                         dbms_utility.format_error_backtrace()
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
    
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Request set submission failed - unknown error: ' ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
    
  END saturday_request_set;
  --
  /*******************************************************************************
  * Procedure:   TUES_FRI_REQUEST_SET
  * Description: This is for the concurrent request set XXCUS_TM_REBATE_TUES_FRI 
  *              Process will run the concurrent requests in request set 
  *              HDS Rebates Tuesday thru Friday 
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     04/15/2014    Kathy Poling    Initial creation of the procedure
                                        SR 244910 
  2.0     07/22/2014    Kathy Poling    Corrected Canada parm for TM Recon JE 
                                        RFC 41043
  2.1     08/12/2014    Kathy Poling    SR 258560 Change to run Subledger Accounting 
                                        Program instead of Create Accounting 
                                        RFC 41267                                                                                                                                                                                                                
  ********************************************************************************/
  PROCEDURE tues_fri_request_set(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
    --
    -- Package Variables
    --
    l_request_id NUMBER NULL;
  
    l_err_msg  VARCHAR2(3000);
    l_err_code NUMBER;
    l_sec      VARCHAR2(255);
  
    l_ok                   BOOLEAN := TRUE;
    l_success              NUMBER := 0;
    l_resp_application_id  NUMBER := 682; --OZF
    l_sysdate              DATE;
    l_end_date             VARCHAR2(30);
    l_period_name          VARCHAR2(15);
    l_gl_post              VARCHAR2(3);
    l_gl_post_cd           VARCHAR2(1);
    l_gl_trnsfr            VARCHAR2(3);
    l_gl_trnsfr_cd         VARCHAR2(1);
    l_report_level_meaning VARCHAR2(15);
    l_report_level         VARCHAR2(1);
    l_mode_meaning         VARCHAR2(15);
    l_mode                 VARCHAR2(1);
    
    --Exception
    srs_failed EXCEPTION;
  
    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.TUES_FRI_RERUEST_SET';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  
  BEGIN
  
    SELECT SYSDATE, to_char(trunc(SYSDATE), 'YYYY/MM/DD') || ' 00:00:00'
      INTO l_sysdate, l_end_date
      FROM dual;
    
    BEGIN
      SELECT period_name
        INTO l_period_name
        FROM gl.gl_period_statuses a
       WHERE application_id = 101
         AND set_of_books_id = 2021
         AND closing_status = 'O'
         AND (period_year || lpad(period_num, 2, 0)) IN
             (SELECT MIN(period_year || lpad(period_num, 2, 0))
                FROM gl.gl_period_statuses b
               WHERE application_id = 101
                 AND set_of_books_id = 2021
                 AND closing_status = 'O'
                 AND period_name NOT LIKE 'FYE%');
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find open period: ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
      
    END;
  
    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    l_sec := 'Apps Initialize';
    fnd_global.apps_initialize(fnd_global.user_id()
                              ,fnd_global.resp_id()
                              ,l_resp_application_id);
  
    -- Step 1 - call set_request_set    
    l_sec := 'Launching Request Set';
    --dbms_output.put_line(l_sec);
    fnd_file.put_line(fnd_file.log, l_sec);
    l_ok := fnd_submit.set_request_set(application => 'XXCUS'
                                      ,request_set => 'XXCUS_REBATES_TUES_FRI');
  
    IF l_ok AND l_success = 0
    THEN
      -- ------------------------------------
      -- New stage CREATE_ACCTG_US with 1 request
      -- ----------------------------------- 
      l_sec := '1st job - CREATE_ACCTG_US Create Accounting - US request';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      BEGIN
        SELECT meaning, submit_gl_post_flag
          INTO l_gl_post, l_gl_post_cd
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_YES_NO'
           AND xlk.lookup_code = xso.submit_gl_post_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find submit GL Post flag US: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT meaning, xso.submit_transfer_to_gl_flag
          INTO l_gl_trnsfr, l_gl_trnsfr_cd
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_YES_NO'
           AND xlk.lookup_code = xso.submit_transfer_to_gl_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find submit GL Transfer flag US: ' ||
                       SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT meaning, xso.accounting_mode_code
          INTO l_mode_meaning, l_mode
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_ACCOUNTING_ENTRY_STATUS'
           AND xlk.lookup_code = xso.accounting_mode_code
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find Mode US: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT meaning, xso.summary_report_flag
          INTO l_report_level_meaning, l_report_level
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_REPORT_LEVEL'
           AND xlk.lookup_code = xso.summary_report_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find Report Level US: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
      /* --Version 2.1
         l_ok := fnd_submit.submit_program(application => 'XLA'
                                          ,program     => 'XLAACCPB'
                                          ,stage       => 'CREATE_ACCTG_US'
                                          ,argument1   => l_resp_application_id --Application
                                          ,argument2   => l_resp_application_id --Source Application
                                          ,argument3   => 'Y' --Dummy Parameter 0
                                          ,argument4   => 2021 --Ledger
                                          ,argument5   => 'CHANNEL_REVENUE_MANAGEMENT' --Process Category
                                          ,argument6   => l_end_date --End Date
                                          ,argument7   => 'Y' --Create Accounting
                                          ,argument8   => 'Y' --Dummy Parameter
                                          ,argument9   => l_mode --Mode 'F'
                                          ,argument10  => 'Y' --Dummy Parameter
                                          ,argument11  => 'N' --Errors Only
                                          ,argument12  => l_report_level --Report 'S'
                                          ,argument13  => l_gl_trnsfr_cd --Transfer to General Ledger
                                          ,argument14  => 'Y' --Dummy Parameter
                                          ,argument15  => l_gl_post_cd --Post in General Ledger
                                          ,argument16  => l_sysdate --General Ledger Batch Name
                                          ,argument17  => NULL --chr(0)              --Mixed Currency Precision
                                          ,argument18  => 'N' --Include Zero Amount Entries and Lines
                                          ,argument19  => NULL --Request ID
                                          ,argument20  => NULL --Entity ID
                                          ,argument21  => 'Trade Management' --Source Application Name
                                          ,argument22  => 'Trade Management' --Application Name
                                          ,argument23  => 'HDS Rebates USA' --Ledger Name
                                          ,argument24  => 'Channel Revenue Management' --Process Category Name
                                          ,argument25  => 'Yes' --Create Acct
                                          ,argument26  => l_mode_meaning --Accounting Mode Name 'Final'
                                          ,argument27  => 'No' --Errors Only
                                          ,argument28  => l_report_level_meaning --Report Level  'Summary'
                                          ,argument29  => l_gl_trnsfr --Transfer To GL
                                          ,argument30  => l_gl_post --Post in GL
                                          ,argument31  => 'No' --Include Zero Amt Lines and Entries
                                          ,argument32  => NULL --Valuation Method
                                          ,argument33  => NULL --Security ID Integer 1
                                          ,argument34  => NULL --Security ID Integer 2
                                          ,argument35  => NULL --Security ID Integer 3
                                          ,argument36  => NULL --Security ID Char 1
                                          ,argument37  => NULL --Security ID Char 2
                                          ,argument38  => NULL --Security ID Char 3
                                          ,argument39  => NULL --Concurrent Request ID
                                          ,argument40  => 'N' --Include User Transaction Identifiers
                                          ,argument41  => 'No' --P_INCLUDE_USER_TRX_IDENTIFIERS
                                          ,argument42  => 'N' --Enable Debug Log
                                          ,argument43  => fnd_global.user_id()
                                          ,argument44  => 'N' --P_SCALABLE_FLAG                                
                                           );
      */
      --Version 2.1
      l_ok := fnd_submit.submit_program(application => 'XLA'
                                       ,program     => 'OZFSLAAP'
                                       ,stage       => 'CREATE_ACCTG_US'
                                       ,argument1   => 101 --Operating Unit     
                                       ,argument2   => l_resp_application_id --Source Application    
                                       ,argument3   => l_resp_application_id --Application ID    
                                       ,argument4   => 'Y' --Dummy Parameter 0     
                                       ,argument5   => 2021 --Ledger     
                                       ,argument6   => 'CHANNEL_REVENUE_MANAGEMENT' --Process Category       
                                       ,argument7   => l_end_date --End Date     
                                       ,argument8   => 'Y' --Create Accounting Flag    
                                       ,argument9   => 'Y' --Dummy Parameter 1     
                                       ,argument10  => 'F' --Accounting Mode     
                                       ,argument11  => 'Y' --Dummy Parameter 2     
                                       ,argument12  => 'N' --Errors Only     
                                       ,argument13  => l_report_level --Report  'S'    
                                       ,argument14  => l_gl_trnsfr_cd --Transfer to General Ledger     
                                       ,argument15  => 'Y' --Dummy Parameter 3     
                                       ,argument16  => l_gl_post_cd --Post in General Ledger     
                                       ,argument17  => l_sysdate --General Ledger Batch Name       
                                       ,argument18  => NULL --chr(0)              --Mixed Currency Precision     
                                       ,argument19  => 'N' --Include Zero Amount Entries and Lines     
                                       ,argument20  => NULL --Request ID     
                                       ,argument21  => NULL --Entity ID    
                                       ,argument22  => 'Trade Management' --Source Application Name    
                                       ,argument23  => 'Trade Management' --Application Name     
                                       ,argument24  => 'HDS Rebates USA' --Ledger Name     
                                       ,argument25  => 'Channel Revenue Management' --Process Category Name    
                                       ,argument26  => 'Yes' --Create Acct     
                                       ,argument27  => l_mode_meaning --Accounting Mode Name     
                                       ,argument28  => 'No' --Errors Only    
                                       ,argument29  => l_report_level_meaning --Report Level     
                                       ,argument30  => l_gl_trnsfr --Transfer To GL    
                                       ,argument31  => l_gl_post --Post in GL    
                                       ,argument32  => 'No' --Include Zero Amt Lines and Entries     
                                       ,argument33  => NULL --Valuation Method     
                                       ,argument34  => NULL --Security ID Integer 1    
                                       ,argument35  => NULL --Security ID Integer 2    
                                       ,argument36  => NULL --Security ID Integer 3    
                                       ,argument37  => NULL --Security ID Char 1     
                                       ,argument38  => NULL --Security ID Char 2     
                                       ,argument39  => NULL --Security ID Char 3     
                                       ,argument40  => NULL --Concurrent Request ID    
                                       ,argument41  => 'N' --Include User Transaction Identifiers    
                                       ,argument42  => 'No' --P_INCLUDE_USER_TRX_IDENTIFIERS     
                                       ,argument43  => 'N' --Enable Debug Log    
                                       ,argument44  => fnd_global.user_id());
    
    ELSE
      l_success := -100;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
  
    -- --------------------------------------
    -- New stage CREATE_ACCTG_CAD with 1 request
    -- --------------------------------------
  
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '2nd job - CREATE_ACCTG_CAD Create Accounting - CAD request';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      BEGIN
        SELECT meaning, submit_gl_post_flag
          INTO l_gl_post, l_gl_post_cd
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_YES_NO'
           AND xlk.lookup_code = xso.submit_gl_post_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2023
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find submit GL Post flag CAD: ' ||
                       SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT meaning, xso.submit_transfer_to_gl_flag
          INTO l_gl_trnsfr, l_gl_trnsfr_cd
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_YES_NO'
           AND xlk.lookup_code = xso.submit_transfer_to_gl_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2023
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find submit GL Transfer flag CAD: ' ||
                       SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT meaning, xso.accounting_mode_code
          INTO l_mode_meaning, l_mode
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_ACCOUNTING_ENTRY_STATUS'
           AND xlk.lookup_code = xso.accounting_mode_code
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2023
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find Mode CAD: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      BEGIN
        SELECT meaning, xso.summary_report_flag
          INTO l_report_level_meaning, l_report_level
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_REPORT_LEVEL'
           AND xlk.lookup_code = xso.summary_report_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2023
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find Report Level CAD: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
    
      /* --Version 2.1
        l_ok := fnd_submit.submit_program(application => 'XLA'
                                         ,program     => 'XLAACCPB'
                                         ,stage       => 'CREATE_ACCTG_CAD'
                                         ,argument1   => l_resp_application_id --Application
                                         ,argument2   => l_resp_application_id --Source Application
                                         ,argument3   => 'Y' --Dummy Parameter 0
                                         ,argument4   => 2023 --Ledger
                                         ,argument5   => 'CHANNEL_REVENUE_MANAGEMENT' --Process Category
                                         ,argument6   => l_end_date --End Date
                                         ,argument7   => 'Y' --Create Accounting
                                         ,argument8   => 'Y' --Dummy Parameter
                                         ,argument9   => l_mode --Mode  'F'
                                         ,argument10  => 'Y' --Dummy Parameter
                                         ,argument11  => 'N' --Errors Only
                                         ,argument12  => l_report_level --Report  'S'
                                         ,argument13  => l_gl_trnsfr_cd --Transfer to General Ledger
                                         ,argument14  => 'Y' --Dummy Parameter
                                         ,argument15  => l_gl_post_cd --Post in General Ledger
                                         ,argument16  => l_sysdate --General Ledger Batch Name
                                         ,argument17  => NULL --chr(0)              --Mixed Currency Precision
                                         ,argument18  => 'N' --Include Zero Amount Entries and Lines
                                         ,argument19  => NULL --Request ID
                                         ,argument20  => NULL --Entity ID
                                         ,argument21  => 'Trade Management' --Source Application Name
                                         ,argument22  => 'Trade Management' --Application Name
                                         ,argument23  => 'HDS Rebates USA' --Ledger Name
                                         ,argument24  => 'Channel Revenue Management' --Process Category Name
                                         ,argument25  => 'Yes' --Create Acct
                                         ,argument26  => l_mode_meaning --Accounting Mode Name  'Final'
                                         ,argument27  => 'No' --Errors Only
                                         ,argument28  => l_report_level_meaning --Report Level  'Summary'
                                         ,argument29  => l_gl_trnsfr --Transfer To GL
                                         ,argument30  => l_gl_post --Post in GL
                                         ,argument31  => 'No' --Include Zero Amt Lines and Entries
                                         ,argument32  => NULL --Valuation Method
                                         ,argument33  => NULL --Security ID Integer 1
                                         ,argument34  => NULL --Security ID Integer 2
                                         ,argument35  => NULL --Security ID Integer 3
                                         ,argument36  => NULL --Security ID Char 1
                                         ,argument37  => NULL --Security ID Char 2
                                         ,argument38  => NULL --Security ID Char 3
                                         ,argument39  => NULL --Concurrent Request ID
                                         ,argument40  => 'N' --Include User Transaction Identifiers
                                         ,argument41  => 'No' --P_INCLUDE_USER_TRX_IDENTIFIERS
                                         ,argument42  => 'N' --Enable Debug Log
                                         ,argument43  => fnd_global.user_id()
                                         ,argument44  => 'N' --P_SCALABLE_FLAG  
                                          );
      */
      --Version 2.1
      l_ok := fnd_submit.submit_program(application => 'XLA'
                                       ,program     => 'OZFSLAAP'
                                       ,stage       => 'CREATE_ACCTG_CAD'
                                       ,argument1   => 102 --Operating Unit     
                                       ,argument2   => l_resp_application_id --Source Application    
                                       ,argument3   => l_resp_application_id --Application ID    
                                       ,argument4   => 'Y' --Dummy Parameter 0     
                                       ,argument5   => 2023 --Ledger     
                                       ,argument6   => 'CHANNEL_REVENUE_MANAGEMENT' --Process Category       
                                       ,argument7   => l_end_date --End Date     
                                       ,argument8   => 'Y' --Create Accounting Flag    
                                       ,argument9   => 'Y' --Dummy Parameter 1     
                                       ,argument10  => 'F' --Accounting Mode     
                                       ,argument11  => 'Y' --Dummy Parameter 2     
                                       ,argument12  => 'N' --Errors Only     
                                       ,argument13  => l_report_level --Report  'S'    
                                       ,argument14  => l_gl_trnsfr_cd --Transfer to General Ledger     
                                       ,argument15  => 'Y' --Dummy Parameter 3     
                                       ,argument16  => l_gl_post_cd --Post in General Ledger     
                                       ,argument17  => l_sysdate --General Ledger Batch Name       
                                       ,argument18  => NULL --chr(0)              --Mixed Currency Precision     
                                       ,argument19  => 'N' --Include Zero Amount Entries and Lines     
                                       ,argument20  => NULL --Request ID     
                                       ,argument21  => NULL --Entity ID    
                                       ,argument22  => 'Trade Management' --Source Application Name    
                                       ,argument23  => 'Trade Management' --Application Name     
                                       ,argument24  => 'HDS Rebates CAN' --Ledger Name     
                                       ,argument25  => 'Channel Revenue Management' --Process Category Name    
                                       ,argument26  => 'Yes' --Create Acct     
                                       ,argument27  => l_mode_meaning --Accounting Mode Name     
                                       ,argument28  => 'No' --Errors Only    
                                       ,argument29  => l_report_level_meaning --Report Level     
                                       ,argument30  => l_gl_trnsfr --Transfer To GL    
                                       ,argument31  => l_gl_post --Post in GL    
                                       ,argument32  => 'No' --Include Zero Amt Lines and Entries     
                                       ,argument33  => NULL --Valuation Method     
                                       ,argument34  => NULL --Security ID Integer 1    
                                       ,argument35  => NULL --Security ID Integer 2    
                                       ,argument36  => NULL --Security ID Integer 3    
                                       ,argument37  => NULL --Security ID Char 1     
                                       ,argument38  => NULL --Security ID Char 2     
                                       ,argument39  => NULL --Security ID Char 3     
                                       ,argument40  => NULL --Concurrent Request ID    
                                       ,argument41  => 'N' --Include User Transaction Identifiers    
                                       ,argument42  => 'No' --P_INCLUDE_USER_TRX_IDENTIFIERS     
                                       ,argument43  => 'N' --Enable Debug Log    
                                       ,argument44  => fnd_global.user_id());
    
    ELSE
      l_success := -120;
      fnd_file.put_line(fnd_file.log, 'Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
  
    -- --------------------------------------
    -- New stage SLA_ACCRUALS_EXTRACT with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '3rd job - TM_SLA_ACCRUALS HDS Rebates: Generate TM SLA Accruals Extract -By Period';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_TM_SLA_PERIOD'
                                       ,stage       => 'SLA_ACCRUALS_EXTRACT'
                                       ,argument1   => l_period_name --Period
                                       ,argument2   => '131100' --Rebates GL Account
                                        );
    ELSE
      l_success := -130;
      fnd_file.put_line(fnd_file.log, 'Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
    -- --------------------------------------
    -- New stage EXTRACT_YTD_INCOME_B with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := 'Seq: 32, HDS Rebates: Extract YTD Income -By Period';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_YTD_INCOME_PERIOD'
                                       ,stage       => 'EXTRACT_YTD_INCOME_B'
                                       ,argument1   => l_period_name --Fiscal Period
                                        );
    ELSE
      l_success := -132;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
    --  
    -- --------------------------------------
    -- New stage REFRESH_PAM with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '4th job - PAM_REFRESH HDS TM PAM Refresh';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_PAM_REFRESH'
                                       ,stage       => 'REFRESH_PAM');
    ELSE
      l_success := -135;
      fnd_file.put_line(fnd_file.log, 'Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
  
    -- --------------------------------------
    -- New stage GL_JE_EXTRACT_US with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '5th job - GL_JE_EXTRACT_US HDS Rebates: Generate TM GL Journals Extract';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_TM_RECON_JOURNALS'
                                       ,stage       => 'GL_JE_EXTRACT_US'
                                       ,argument1   => 'US' --Territory
                                       ,argument2   => l_period_name --Period
                                       ,argument3   => '131100' --Rebates GL Account
                                        );
    ELSE
      l_success := -140;
      fnd_file.put_line(fnd_file.log, 'Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
  
    -- --------------------------------------
    -- New stage GL_JE_EXTRACT_CAN with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '6th job - GL_JE_EXTRACT_CAN HDS Rebates: Generate TM GL Journals Extract';
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_TM_RECON_JOURNALS'
                                       ,stage       => 'GL_JE_EXTRACT_CAN'
                                       ,argument1   => 'CA' --Territory --Version 2.0
                                       ,argument2   => l_period_name --Period
                                       ,argument3   => '131100' --Rebates GL Account
                                        );
    ELSE
      l_success := -150;
      fnd_file.put_line(fnd_file.log, 'Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
    --    
    -- --------------------------------------
    -- New stage EXTRACT_GL_SUMMARY with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
    
      l_sec := '7th job, HDS Rebates: Extract GL Summary Data -By Period';
    
      fnd_file.put_line(fnd_file.log, l_sec);
    
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_GL_SUMMARY'
                                       ,stage       => 'EXTRACT_GL_SUMMARY'
                                       ,argument1   => l_period_name --Fiscal Period
                                        );
    ELSE
      l_success := -200;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
    --    
    -- -----------------------------------------------
    -- All requests in the set have been submitted now
    -- -----------------------------------------------
    IF l_ok AND l_success = 0
    THEN
      l_request_id := fnd_submit.submit_set(NULL, FALSE);
      fnd_file.put_line(fnd_file.log, 'Request_id = ' || l_request_id);
      COMMIT;
    
    ELSE
      l_success := -2000;
      l_sec     := 'Call to submit_set failed';
      fnd_file.put_line(fnd_file.log, 'Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
  
  EXCEPTION
    WHEN srs_failed THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_sec || ' Failed = ' || l_success || ' : ' ||
                    fnd_message.get;
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                         dbms_utility.format_error_stack() ||
                                                                         ' Error_Backtrace...' ||
                                                                         dbms_utility.format_error_backtrace()
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
    
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Request set failed: ' || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
    
  END tues_fri_request_set;
--
  /*******************************************************************************
  * Procedure:   reset_vol_offer_retro_flags
  * Description: Uncheck the retroactive flag for all offers where calendar year
  *              belongs to the previous fiscal year
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    -----------------------------------------
  1.0     11/18/2014    Balaguru Seshadri  Initial creation of the procedure                                                                                                                                                                                     
  ********************************************************************************/
  PROCEDURE reset_vol_offer_retro_flags (errbuf OUT VARCHAR2, retcode OUT NUMBER, p_offer_code in varchar2) IS
    --
    -- Package Variables
    --
    l_request_id NUMBER NULL;
  
    l_err_msg  VARCHAR2(3000);
    l_err_code NUMBER;
    l_sec      VARCHAR2(255);
  
    l_ok                   BOOLEAN := TRUE;
    l_success              NUMBER := 0;
    l_resp_application_id  NUMBER := 682; --OZF
    l_sysdate              DATE;
    l_end_date             VARCHAR2(30);
    l_period_name          VARCHAR2(15);
    l_gl_post              VARCHAR2(3);
    l_gl_post_cd           VARCHAR2(1);
    l_gl_trnsfr            VARCHAR2(3);
    l_gl_trnsfr_cd         VARCHAR2(1);
    l_report_level_meaning VARCHAR2(15);
    l_report_level         VARCHAR2(1);
    l_mode_meaning         VARCHAR2(15);
    l_mode                 VARCHAR2(1);
    n_prev_year            NUMBER :=Null;
    --Exception
    srs_failed EXCEPTION;
  
    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'xxcus_tm_interface_pkg.reset_vol_offer_retro_flags';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    --
    cursor my_offers (l_year in number, l_offer_code in varchar2) is
    select
        -- start of fields 
        qpvl.attribute7             offer_cal_year,
        ofr.status_code             offer_status,
        ofr.offer_id                offer_id, 
        ofr.offer_type              offer_type,
        qpvl.list_header_id         qp_list_header_id, 
        qpvl.name                   offer_code, 
        qpvl.description            offer_name,
        ams.media_name              media_name,
        opt.retroactive_flag        retroactive_flag,
        opt.offer_market_option_id  mkt_opt_id,
        opt.object_version_number   obj_ver_num
        -- end of fields
    from   apps.ozf_offr_market_options opt
          ,apps.qp_list_headers_vl      qpvl
          ,ozf.ozf_offers               ofr
          ,apps.ams_media_vl            ams    
    where  1 =1
      and  ofr.qp_list_header_id    =qpvl.list_header_id
      and  ofr.offer_id             =opt.offer_id
      and  opt.qp_list_header_id    =qpvl.list_header_id
      and  ofr.status_code in ('DRAFT','ONHOLD','ACTIVE')
      and  ams.media_id(+)          =ofr.activity_media_id 
      and  opt.retroactive_flag     ='Y' 
      and  ams.media_name in 
             (
                'Rebate Back to $1'
               ,'Tiered - Back to $1'
               ,'Tiered- Back to $ Amount'
               ,'Tiered - PS Back to $1'
               ,'Tiered- PS Back to $ Amount'
             )
      and  qpvl.attribute7          =l_year
      and  qpvl.name                =nvl(l_offer_code, qpvl.name);    
    --
    type my_offer_tbl is table of my_offers%rowtype index by binary_integer;
    my_offer_rec my_offer_tbl;
    --
    v_N_flag varchar2(1) :='N';
    --
    l_mo_rec ozf_offer_market_options_pvt.vo_mo_rec_type;
    l_return_status  varchar2(1) :=Null;
    --
    l_msg_data       varchar2(2000) :=Null;
    b_status_flag boolean;
    l_msg_count      number :=0;
    --
  BEGIN
   --
   Begin
    --
     select max(ent_year_id) -1
     into   n_prev_year 
     from   ozf_time_ent_period
     where  1 =1
     and  TRUNC(SYSDATE) between start_date and end_date;
    /*
    select max(period_year) -1
    into   n_prev_year
    from   gl_period_statuses
    where 1 =1
      and set_of_books_id IN (2021, 2023) --Rebates US and CAD ledgers
      and closing_status ='O' --Pick only open periods
      and application_id =101 --GL owned periods only  
      and adjustment_period_flag ='N';
    */
    --
   Exception
    When No_Data_Found then
     n_prev_year :=Null;
    When Others Then
     n_prev_year :=Null;
   End;
   --
   if (n_prev_year is null) then
    print_log('Failed to fetch previous fiscal year.');
    b_status_flag := fnd_concurrent.set_completion_status('ERROR','Failed to fetch previous fiscal year.');
   else
    print_log('Previous fiscal year ='||n_prev_year);
    --
    open  my_offers (l_year =>n_prev_year, l_offer_code =>p_offer_code);
    fetch my_offers bulk collect into my_offer_rec;
    close my_offers;
    --
    if my_offer_rec.count >0 then
     --
     for idx in 1 .. my_offer_rec.count loop
       --
       --
       print_log(
                 'offer_id ='||my_offer_rec(idx).offer_id||
                 ', offer_code ='||my_offer_rec(idx).offer_code||
                 ', offer_name ='||my_offer_rec(idx).offer_name||                 
                 ', offer_status ='||my_offer_rec(idx).offer_status
                );
       --
        l_mo_rec.offer_market_option_id :=my_offer_rec(idx).mkt_opt_id;
        l_mo_rec.offer_id               :=my_offer_rec(idx).offer_id;
        l_mo_rec.qp_list_header_id      :=my_offer_rec(idx).qp_list_header_id;
        l_mo_rec.retroactive_flag       :=v_N_flag;
        l_mo_rec.object_version_number  :=my_offer_rec(idx).obj_ver_num;
       --
       ozf_offer_market_options_pvt.update_market_options
        (
            p_api_version_number  =>1.0
            ,p_init_msg_list      =>FND_API.G_TRUE
            ,p_commit             =>FND_API.G_TRUE
            ,p_validation_level   =>FND_API.G_VALID_LEVEL_FULL
            ,x_return_status      =>l_return_status
            ,x_msg_count          =>l_msg_count
            ,x_msg_data           =>l_msg_data
            ,p_mo_rec             =>l_mo_rec
        );
       --
        if (l_return_status ='S') then
         --
         print_out('Successfully unchecked retroactive flag for offer: '||my_offer_rec(idx).offer_name||', offer code :'||my_offer_rec(idx).offer_code);
         --        
        else
         --
         print_out('Failed to uncheck retroactive flag for offer: '||my_offer_rec(idx).offer_name||', offer code :'||my_offer_rec(idx).offer_code);
         print_out('l_return_status ='||l_return_status);
         print_out('l_msg_count ='||l_msg_count);
         print_out('l_msg_data ='||l_msg_data);         
         --
        end if; 
     end loop;
     --
    else
     print_log('Failed to fetch offers for previous fiscal year, my_offer_rec.count ='||my_offer_rec.count);
    end if;
    --
   end if;
   --
  EXCEPTION
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Request set failed: ' || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');  
  END reset_vol_offer_retro_flags;
--
  /*******************************************************************************
  * Procedure:   fix_mass_copy_fund_issue
  * Description: The HDS Rebates Mass copy program copied the old funding id
  *              into the ozf_offers causing issues down the line. The below
  *             update will fix only offers that have a mismatch. It will 
  *             update the correct funding info.
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   ---------------    -----------------------------------------
  1.0     11/21/2014    Balaguru Seshadri  Initial creation of the procedure                                                                                                                                                                                     
  ********************************************************************************/
  PROCEDURE fix_mass_copy_fund_issue (errbuf OUT VARCHAR2, retcode OUT NUMBER, p_cal_year in varchar2) IS
    --
    -- Package Variables
    --
    l_request_id NUMBER NULL;
    --
    b_status_flag BOOLEAN;
    --
    l_err_msg  VARCHAR2(3000);
    l_err_code NUMBER;
    l_sec      VARCHAR2(255);
    --
    srs_failed EXCEPTION;
    --
    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'xxcus_tm_interface_pkg.fix_mass_copy_fund_issue';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    --
    cursor my_offers (l_year in varchar2) is
    select qlhv.attribute7          cal_year,
           oo.status_code           offer_status,
           oo.offer_code            offer_code,
           qlhv.description         offer_name,
           oo.budget_source_type    budget_source_type,
           ofat.short_name          offer_program,
           oo.budget_source_id      wrong_fund_id,
           oab.budget_source_id     correct_fund_id,
           ofat1.short_name         budget_program,
           oo.offer_id              offer_id
      from apps.qp_list_headers_vl qlhv,
           ozf.ozf_offers oo,
           ozf.ozf_funds_all_tl ofat,
           ozf.ozf_funds_all_tl ofat1,
           ozf.ozf_act_budgets oab
     where     1 = 1
           and qlhv.list_header_id = oo.qp_list_header_id
           and ofat.fund_id = oo.budget_source_id
           and ofat1.fund_id = oab.budget_source_id
           and qlhv.list_header_id = oab.act_budget_used_by_id
           and oab.budget_source_type = 'FUND'
           and oab.status_code = 'APPROVED'
           and oab.arc_act_budget_used_by = 'OFFR'
           and oo.budget_source_id <> oab.budget_source_id
           and qlhv.attribute7 =l_year
           --and oo.offer_code =nvl(l_offer_code, oo.offer_code)
           and oo.status_code in ('ACTIVE', 'ONHOLD');    
    --
    type my_offer_tbl is table of my_offers%rowtype index by binary_integer;
    my_offer_rec my_offer_tbl;
    --
  BEGIN --main processing 
   --
   if (p_cal_year is null) then
    print_log('Previous calendar year is required.');
    b_status_flag := fnd_concurrent.set_completion_status('ERROR','Previous calendar year is required.');
   else
    print_log('Checking offers for calendar year ='||p_cal_year||' with mismatch in fund information.');
    --
    open  my_offers (l_year =>p_cal_year);
    fetch my_offers bulk collect into my_offer_rec;
    close my_offers;
    --
    if my_offer_rec.count >0 then
     --
     for idx in 1 .. my_offer_rec.count loop
       --
       begin 
        --
        savepoint init_here;
        --
         update ozf_offers
         set budget_source_id =my_offer_rec(idx).correct_fund_id
         where 1 =1
           and offer_id =my_offer_rec(idx).offer_id;
        --
         if (SQL%rowcount >0) then
          --
           print_out('Successfully updated offer_code '||my_offer_rec(idx).offer_code||' with budget source id '||my_offer_rec(idx).correct_fund_id);
          --        
         else
          --
           print_out('Failed to update correct budget_source_id for offer code: '||my_offer_rec(idx).offer_code);
          --
         end if;
         --        
       exception
        --
        when no_data_found then
         --
         rollback to init_here;
        --
        when others then
        --
          print_log
                (
                 'Issue in offer_id ='||my_offer_rec(idx).offer_id||
                 ', offer_code ='||my_offer_rec(idx).offer_code||                
                 ', message ='||sqlerrm                 
                );        
         rollback to init_here;
        --
       end;
       -- 
     end loop;
     --
    else
     print_log('No offers found for previous calendar year with mismatch in budget source id''s, total_records ='||my_offer_rec.count);
    end if;
    --
   end if;
   --
  EXCEPTION
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Request set failed: ' || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');  
  END fix_mass_copy_fund_issue;  
  --
  -- Begin Ver 1.20
  /*******************************************************************************
  * Function:   get_hds_rbt_div_id
  * Description: Add function get_hds_rbt_div_id to be used as a rebatable qualifier for offers or otherwise by "Division"
  HISTORY
  ===============================================================================
  VERSION DATE                   AUTHOR(S)                DESCRIPTION
  -------         -----------          ---------------               -----------------------------------------
  1.20          11/12/2015    Balaguru Seshadri  Initial creation of the procedure    TMS:  20151217-00142                                                                                                                                                                             
  ********************************************************************************/  
    FUNCTION get_hds_rbt_div_id
    (
      p_resale_line_tbl ozf_order_price_pvt.resale_line_tbl_type -- OZF_ORDER_PRICE_PVT.G_RESALE_LINE_TBL
    )
    RETURN NUMBER
    IS
      l_hds_rbt_div_id NUMBER;
    BEGIN
      IF p_resale_line_tbl.COUNT > 0 THEN
        l_hds_rbt_div_id := p_resale_line_tbl(1).end_cust_party_id;
      ELSE
        l_hds_rbt_div_id := NULL;
      END IF;
      RETURN l_hds_rbt_div_id;
    EXCEPTION
     WHEN NO_DATA_FOUND THEN
      RETURN NULL;
     WHEN OTHERS THEN
      RETURN NULL;
    END get_hds_rbt_div_id;  
  -- End Ver 1.20  
  --
END xxcus_tm_misc_pkg;
/