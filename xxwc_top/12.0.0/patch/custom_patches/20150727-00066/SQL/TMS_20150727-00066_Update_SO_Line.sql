/* Formatted on 8/3/2015 11:44:59 AM (QP5 v5.256.13226.35538) */
/*
 TMS: 20150727-00066  
 Date: 08/03/2015
 Notes: Progress sales order line
*/
SET SERVEROUTPUT ON SIZE 1000000

DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20150727-00066 , Update 1 -Before Update');
   
update apps.oe_order_lines_all
set INVOICE_INTERFACE_STATUS_CODE='YES'
,open_flag='N'
,flow_status_code='CLOSED'
,INVOICED_QUANTITY=1
where line_id=48016963
and headeR_id=29163938;

-- 1 Record exepected to update

   DBMS_OUTPUT.put_line (
         'TMS: 20150721-00037, update 2 -After Update, rows modified: ');
		 
COMMIT;

END;
/