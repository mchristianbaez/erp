CREATE OR REPLACE 
PACKAGE  XXEIS.EIS_XXWC_INV_UTIL_PKG IS
 --//============================================================================
  --// Object Name          :: EIS_XXWC_INV_UTIL_PKG
  --//
  --// Object Type          :: Package Specification
  --//
  --// Object Description   :: This Package will trigger in view and populate the values into View.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva        06/06/2016         Initial Build  -- Added for TMS#20160601-00025.
  --// 1.1        Siva        10/17/2016         TMS#20160930-00078 
  --// 1.2        Siva        10-Jan-2017        TMS#20161025-00027 
  --//============================================================================
type T_VARCHAR_TYPE_VLDN_TBL is table of varchar2(200) index by varchar2(200);

G_ONHAND_QTY_VLDN_TBL   T_VARCHAR_TYPE_VLDN_TBL;

g_onhand_qty number;
--start for version 1.1
g_url varchar2(2000);
g_invoice_num varchar2(2000);
g_url_vldn_tbl   t_varchar_type_vldn_tbl;
G_invoice_num_VLDN_TBL   T_VARCHAR_TYPE_VLDN_TBL;
--End for version 1.1  

--start for version 1.2
g_vendor_num_vldn_tbl     t_varchar_type_vldn_tbl;
g_vendor_name_vldn_tbl    t_varchar_type_vldn_tbl;  
g_vendor_id_vldn_tbl      t_varchar_type_vldn_tbl; 
g_vendor_number varchar2(100);
g_vendor_name varchar2(240);
g_vendor_id number;
--End for version 1.2

FUNCTION GET_ONHAND_QTY(
    P_INVENTORY_ITEM_ID NUMBER,
    P_ORGANIZATION_ID   NUMBER,
    P_SUBINVENTORY_CODE VARCHAR2)
  RETURN number;
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Inventory - Onhand Quantity - WC"
  --//
  --// Object Name          :: GET_ONHAND_QTY
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva        06/06/2016      Initial Build  -- Added for TMS#20160601-00025.
  --//============================================================================

FUNCTION get_invoice_url(
    p_po_distribution_id NUMBER,
    p_period_name        VARCHAR2,
    p_request_type       VARCHAR2)
  RETURN VARCHAR2;
  --//============================================================================
  --// Object Usage         :: This Object Referred by "HDS Account Analysis Subledger Detail Report"
  --//
  --// Object Name          :: get_invoice_url
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva        10/07/2016      Initial Build  -- Added for TMS#20160930-00078
  --//============================================================================  
  
FUNCTION GET_VENDOR_OWNER_NUM(
    p_inventory_item_id NUMBER,
    p_request_type       varchar2)
  RETURN VARCHAR2;
  --//============================================================================
  --// Object Usage         :: This Object Referred by "PO Cost Change Analysis Report - WC"
  --//
  --// Object Name          :: GET_VENDOR_OWNER_NUM
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.2        Siva          10-Jan-2017        TMS#20161025-00027 
  --//============================================================================  
  
END;
/
 