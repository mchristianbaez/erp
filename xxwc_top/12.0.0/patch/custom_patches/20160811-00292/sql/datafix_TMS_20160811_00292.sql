/*************************************************************************
  $Header TMS_20160811-00292_AWAITING_INVOICE_TO_CLOSE.sql $
  Module Name: TMS_20160811-00292  Data Fix script for I675908

  PURPOSE: Data fix script for I675908--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        02-AUG-2016  Raghav Velichetti         TMS#20160811-00292

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
/* update apps.oe_order_lines_all
           SET 
                shipping_quantity = ordered_quantity,
                shipped_quantity = ordered_quantity,
                actual_shipment_date = TO_DATE('08/12/2016 11:18:48','MM/DD/YYYY HH24:MI:SS'),
                shipping_quantity_uom = order_quantity_uom,
                fulfilled_flag = 'Y',
                fulfillment_date = TO_DATE('08/12/2016 11:18:48','MM/DD/YYYY HH24:MI:SS'),
                fulfilled_quantity = ordered_quantity,
                last_updated_by = -1,
                last_update_date = SYSDATE
            -- ACCEPTED_BY = 4716,
             -- REVREC_SIGNATURE = 'TODD MYERS',
             -- REVREC_SIGNATURE_DATE = '07-FEB-2013' 
          WHERE line_id = 75972729
          and headeR_id= 46437356; */
   DBMS_OUTPUT.put_line ('TMS: 20160825-00139    , Before Update');

update apps.oe_order_lines_all
set FLOW_STATUS_CODE='CANCELLED',
CANCELLED_FLAG='Y'
where line_id = 75972729
and header_id= 46437356;
   DBMS_OUTPUT.put_line (
         'TMS: 20160811-00292  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS:20160811-00292    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160811-00292 , Errors : ' || SQLERRM);
END;
/