 /********************************************************************************
  FILE NAME: XXWC.XXWC_AP_GL_MAP_EXT_TBL.sql

  PROGRAM TYPE: External Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     05/16/2018    Pattabhi Avula  TMS#20180308-00301  -- Initial Version
  2.0     07/13/2018    Naveen K  		TMS#20180713-00072  -- Re-Load
  ********************************************************************************/
  CREATE TABLE XXWC.XXWC_AP_GL_MAP_EXT_TBL
   (AHH_GL_ACCOUNT		 VARCHAR2(50), 
	ORACLE_GL_ACCOUNT	 VARCHAR2(50)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_AR_AHH_CASH_RCPT_CONV_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_AP_GL_MAP_CONV_LOAD.bad'
    DISCARDFILE 'XXWC_AP_GL_MAP_CONV_LOAD.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                )
      LOCATION
       ( "XXWC_AR_AHH_CASH_RCPT_CONV_DIR":'XXWC_AP_GL_MAP_CONV_LOAD.csv'
       )
    )
   REJECT LIMIT UNLIMITED;
