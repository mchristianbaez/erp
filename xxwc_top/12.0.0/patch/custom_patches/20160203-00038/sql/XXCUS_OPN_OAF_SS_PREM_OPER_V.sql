  /*
    ========================================================================================================================================================
    Module: Oracle Property Manager
    Type: HDS GSC custom view
    PURPOSE: Used by HDS OPN Property Manager -Oracle Application Framework page SuperSearch,  TAB  "Premises and Operations"
    Additional comments: Explanation of the SQL filters used.
                                             We filter the PRP table on the values below (OPN_Type and LOCATION_TYPE_LOOKUP_CODE) to make it a flat file at the lowest level (section/office), 
                                             with current row (HDS_PROP_CURRENT_FLAG). Now exclude PeopleSoft Admin assignments, since we do not want People FRUs in the SuperSearch.
                                             The BL_LD_ID in the PRP table will display as the parent LOC ID (Bldg/Land level).
                                             The Location_Code in both the PRP and FRU-LOC table is the Section ID (office/section), and this is what you join the two tables by. .
                                             Filtering the FRU-LOC table on the most current row (for both LOC section, and FRU emp assignment).
    HISTORY
  ========================================================================================================================================================
      VERSION        DATE                  AUTHOR(S)                 DESCRIPTION                                                     TICKET
      -------        -----------           ---------------           ------------------------------                                  ---------------                          
      1.0            30-Dec-2015           Balaguru Seshadri         Created.                                                        TMS 20160209-00169           
	  1.1            11-Feb-2016           Balaguru Seshadri         Move tms ticket ref from  20160209-00169 to 20160203-00038      TMS 20160203-00038
  ========================================================================================================================================================	  
  */  
CREATE OR REPLACE FORCE VIEW APPS.XXCUS_OPN_OAF_SS_PREM_OPER_V AS
SELECT  A.BL_LD_ID LOC
           ,B.LOCATION_CODE SECTION           
           ,B.SPACE_TYPE OPS_TYPE
           ,A.BL_LD_AREA_GROSS  sf_acreage  --Modified from null to  BL_LD_AREA_GROSS on 02/02/2016
           ,C.ADDRESS_LINE1 ADDRESS
           ,C.ADDRESS_LINE3 ADDRESS_LINE3
           ,C.ADDRESS_LINE2 ADDRESS_LINE2           
           ,B.HDS_OPS_STATUS OPS_STATUS
           ,B.EMP_SPACE_COMMENTS COMMENTS
           ,B.LC_FRU FRU
           ,B.LC_FRU_DESCRIPTION FRU_DESCRIPTION
           ,B.LOB_ABB BU
           ,B.LC_LOB_BRANCH BR
           ,B.LC_ENTRP_LOC  ORACLE_ID --Added on 02/02/2016 
           ,B.EMP_ASSIGN_START_DATE OPS_START  --Added on 02/02/2016
           ,B.EMP_ASSIGN_END_DATE OPS_END  --Added on 02/02/2016
           ,B.AREA_ALLOCATED OPS_SF  --Added on 02/02/2016                                            
           ,CASE
            WHEN C.LOC_TYPE ='Building' THEN 'BLDG'
            WHEN C.LOC_TYPE ='Land' THEN 'LAND'
            ELSE  C.LOC_TYPE
            END LOC_TYPE
           ,C.OPN_TENANCY_SUITE SUITE  
           ,C.CITY CITY
           ,C.STATE_PROVINCE ST_PRV           
           ,C.LEASE_ID
           ,C.LEASE_CHANGE_ID
           ,C.RER_ID
FROM XXCUS.XXCUS_OPN_PROPERTIES_ALL     A
         ,XXCUS.XXCUS_OPN_FRU_LOC_ALL            B
        ,XXCUS.XXCUS_OPN_RER_LOCATIONS_ALL C
WHERE 1 =1
AND A.OPN_TYPE ='PROPERTY'
AND A.HDS_PROP_CURRENT_FLAG ='Y'
AND A.LOCATION_TYPE_LOOKUP_CODE IN ('OFFICE', 'SECTION')
AND A.SC_YD_SPACE_TYPE_CODE !='LAWSONADMIN'
AND C.LOC_CODE =A.BL_LD_ID
AND B.LOCATION_CODE =A.LOCATION_CODE
AND B.HDS_FRU_SECTION_CURRENT_FLAG ='Y';
--
COMMENT ON TABLE APPS.XXCUS_OPN_OAF_SS_PREM_OPER_V IS  'TMS 20160209-00169 OR ESMS: 195667';