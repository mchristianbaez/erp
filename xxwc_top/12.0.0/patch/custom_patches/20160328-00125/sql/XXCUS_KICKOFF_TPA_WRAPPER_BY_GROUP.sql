set serveroutput on size 1000000;
declare
/*
    TMS: 20160328-00125
    Incident: 665656
    Author: Balaguru Seshadri
    Date; 03/28/2016
    Scope: Submit Rebates TPA wrapper for specific vendors
*/
 --
    cursor tpa_group is 
    select a.group_no
    from xxcus.xxcus_ozf_tpa_runs_tmp a
             ,xxcus.xxcus_ozf_tpabatch_cntrl_tmp b
    where 1 =1
         and b.group_no =a.group_no
    group by  a.group_no
    order by a.group_no asc;
 --
    cursor tpa_partners (p_group_no in varchar2) is 
    select *
    from xxcus.xxcus_ozf_tpa_runs_tmp
    where 1 =1
          and group_no =p_group_no
    order by partner_account_id asc;
 -- 
    l_records_per_group number :=10;
    l_request_id number :=0;
    l_loc number;
    --
    l_user_id number :=1215; --User; 15837 -- BS006141 --1215 MC027824
    l_resp_id number :=51609; --Responsibility: HDS Trade Management Admin Super User - USD/CAD RBT
    l_resp_appl_id number :=682; --Responsibility Application: Trade Management Application
--     
--     
begin
     for rec_a in tpa_group loop 
      --
        dbms_output.put_line('----Current Group#  '||rec_a.group_no);
        l_loc :=1;
      --
       for rec_b in tpa_partners (p_group_no =>rec_a.group_no) loop 
        --
        --dbms_output.put_line('-----------Partner Account Id :'||rec_b.partner_account_id||', org_id ='||rec_b.org_id);
        --
        l_loc :=2;
        --
        fnd_global.apps_initialize
               (
                user_id =>l_user_id,
                resp_id =>l_resp_id,
                resp_appl_id =>l_resp_appl_id
               );
        --
        l_loc :=3;
        --
         fnd_request.set_org_id (org_id =>rec_b.org_id);
       --
         l_loc :=4;
       --         
        --
               l_request_id :=fnd_request.submit_request
                      (
                       application      =>'XXCUS',
                       program          =>'XXHDS_OZF_TP_ACCRUAL',
                       description      =>'',
                       start_time       =>'',
                       sub_request      =>FALSE,
                       argument1        =>to_char(rec_b.partner_account_id), --supplied by the Rebates team in a spreadsheet
                       argument2        =>'2016/01/01 00:00:00', --Constant supplied by the Rebates team
                       argument3        =>'2016/03/27 00:00:00' --Constant supplied by the Rebates team
                      );
        --
         if (l_request_id >0) then
           dbms_output.put_line('----------------------Partner Account Id :'||rec_b.partner_account_id||', org_id ='||rec_b.org_id||', request_id ='||l_request_id);
           commit;
        else
           dbms_output.put_line('----------------------Partner Account Id :'||rec_b.partner_account_id||', org_id ='||rec_b.org_id||', request_id =0');
        end if;
        --
        l_loc :=5;
        --
       end loop;
      --
     end loop;
exception
 when others then
  dbms_output.put_line('loc ='||l_loc||', message ='||sqlerrm);    
end;
/