--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: XXWC_DEL_FAV_REP_TBL
  Description: This table is used for EIS Delete Favorite Report List.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     04-Aug-2016        Siva            TMS#20160628-00049  
********************************************************************************/
DROP TABLE XXEIS.XXWC_DEL_FAV_REP_TBL CASCADE CONSTRAINTS;
 CREATE GLOBAL TEMPORARY TABLE XXEIS.XXWC_DEL_FAV_REP_TBL (
 DESCRIPTION VARCHAR2(4000)
 ) ON COMMIT PRESERVE ROWS
/
