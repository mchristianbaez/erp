/* Formatted on 11/20/2013 10:30:33 AM (QP5 v5.206) */
-- Start of DDL Script for Package Body APPS.XXWC_AR_GETPAID_ARCUST_V2
-- Generated 11/20/2013 10:30:29 AM from APPS@EBIZFQA

CREATE OR REPLACE PACKAGE BODY apps.xxwc_ar_getpaid_arcust_v2
IS
 /********************************************************************************
   FILE NAME: XXWC_AR_GETPAID_ARCUST_V2.pkg

   PROGRAM TYPE: PL/SQL Package Body

   PURPOSE: Getpaid outbound files.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)            DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     11/20/2013                           Initial creation
   1.1     09/29/2014   Maharajan Shunmugam     TMS# 20141001-00157 Canada Multi-Org changes
   1.2     04/19/2016   Neha Saini              TMS#20160407-00183  removing parallelism.
   ********************************************************************************/
    l_errbuf   CLOB;

    PROCEDURE populate_arcust_fields
    IS
    BEGIN
    apps.xxwc_mv_routines_add_pkg.enable_parallelism;--ver1.2
        BEGIN
            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##');

            EXECUTE IMMEDIATE 'CREATE TABLE xxwc.arcust##
(  customer_id number
   , custno           VARCHAR2 (30)
   ,parent            VARCHAR2 (30)
   ,company          VARCHAR2 (240)
   ,address1         VARCHAR2 (240)
   ,address2         VARCHAR2 (240)
   ,address3         VARCHAR2 (240)
   ,city             VARCHAR2 (60)
   ,state            VARCHAR2 (60)
   ,zip              VARCHAR2 (60)
   ,country          VARCHAR2 (60)
   ,phone            VARCHAR2 (60)
   ,faxno            VARCHAR2 (60)
   ,TYPE             VARCHAR2 (150)
   ,arcomment        VARCHAR2 (240)
   ,LIMIT            NUMBER
   ,salesmn          VARCHAR2 (30)
   ,contact          VARCHAR2 (360)
   ,lastpay          VARCHAR2 (8)
   ,ldate            VARCHAR2 (8)
   ,terr             VARCHAR2 (80)
   ,balance          NUMBER
   ,dateopend        DATE
   ,highbal          VARCHAR2 (14)
   ,lastamt          NUMBER
   ,lastysales       VARCHAR2 (14)
   ,ultimateduns     NUMBER
   ,fedid            VARCHAR2 (50)
   ,ytdsales         VARCHAR2 (14)
   ,credit_status    VARCHAR2 (1)
   ,sic_code         VARCHAR2 (30)
   ,sales_term       VARCHAR2 (240)
   ,email            VARCHAR2 (2000)
   ,prismterritory   VARCHAR2 (30)
   ,org_id           NUMBER (15, 0)
)';

            xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'arcust##');
        END;

        --**************************************
        BEGIN
            -- Create temp table structure.
            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##tmp_pmt_schdl_trx');

            EXECUTE IMMEDIATE 'CREATE TABLE xxwc.arcust##tmp_pmt_schdl_trx
AS
    SELECT apsa.customer_id
          ,apsa.org_id
          ,apsa.amount_due_remaining
      FROM apps.ar_payment_schedules apsa
     WHERE apsa.amount_due_remaining <> 0 AND apsa.customer_id IS NOT NULL and  1 = 2';

            -- Settings to
            xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'arcust##tmp_pmt_schdl_trx');

            -- Populate table, the APPEND hint uses parallelism
            EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
          INTO  xxwc.arcust##tmp_pmt_schdl_trx
        SELECT  apsa.customer_id
          ,apsa.org_id
          ,apsa.amount_due_remaining
          FROM apps.ar_payment_schedules apsa
         WHERE apsa.amount_due_remaining <> 0 AND apsa.customer_id IS NOT NULL';

            COMMIT;
        END;

        -----***********************************************************
        BEGIN
            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##tmp_pmt_schdl_sum');

            EXECUTE IMMEDIATE
                'create table XXWC.arcust##tmp_pmt_schdl_sum(balance number,customer_id number,org_id number)';

            xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'arcust##tmp_pmt_schdl_sum');

            EXECUTE IMMEDIATE 'insert /*+append*/ into XXWC.arcust##tmp_pmt_schdl_sum
  SELECT SUM (apsa.amount_due_remaining) balance, apsa.customer_id, apsa.org_id
    FROM xxwc.arcust##tmp_pmt_schdl_trx apsa
GROUP BY apsa.customer_id, apsa.org_id';

            COMMIT;
            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##tmp_pmt_schdl_trx');
        END;

        -----***********************************************************

        EXECUTE IMMEDIATE 'INSERT /*+append */
       INTO  xxwc.arcust## (balance, customer_id, org_id)
        SELECT balance, customer_id, org_id FROM xxwc.arcust##tmp_pmt_schdl_sum';

        COMMIT;
        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##tmp_pmt_schdl_sum');

        -----***********************************************************
        --populate lastysales
        -----***********************************************************
        BEGIN
            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##lastysales');

            EXECUTE IMMEDIATE
                'create table xxwc.arcust##lastysales as
SELECT s.customer_id, NVL (TO_CHAR (SUM (apsa.amount_due_original), ''9999999990.99''), ''0.00'') lastysales
  FROM apps.ar_payment_schedules apsa, xxwc.arcust## s
 WHERE apsa.customer_id = s.customer_id AND TO_CHAR (apsa.trx_date, ''YYYY'') = (TO_CHAR (SYSDATE, ''YYYY'') - 1) and 1=2
 group by s.customer_id';

            xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'xxwc.arcust##lastysales');

            EXECUTE IMMEDIATE
                'insert /*+append*/ into xxwc.arcust##lastysales
SELECT s.customer_id, NVL (TO_CHAR (SUM (apsa.amount_due_original), ''9999999990.99''), ''0.00'') lastysales
  FROM apps.ar_payment_schedules apsa, xxwc.arcust## s
 WHERE apsa.customer_id = s.customer_id AND TO_CHAR (apsa.trx_date, ''YYYY'') = (TO_CHAR (SYSDATE, ''YYYY'') - 1)
 group by s.customer_id';

            COMMIT;
        END;

        BEGIN
            xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC', 'arcust##', 'arcust##1');

            EXECUTE IMMEDIATE
                'insert/*+append*/ into xxwc.arcust##1(balance, customer_id, org_id,lastysales)
  select  s.balance, s.customer_id, s.org_id,nvl(l.lastysales,''0.00'') from xxwc.arcust## s,xxwc.arcust##lastysales l
  where l.customer_id(+) = s.customer_id';

            COMMIT;
            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##lastysales');
        END;

        -----***********************************************************
        --populate ytdsales
        -----***********************************************************
        BEGIN
            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##ytdsales');

            EXECUTE IMMEDIATE
                'create table xxwc.arcust##ytdsales as
SELECT s.customer_id, NVL (TO_CHAR (SUM (apsa.amount_due_original), ''9999999990.99''), ''0.00'') ytdsales
  FROM apps.ar_payment_schedules apsa, xxwc.arcust## s
 WHERE apsa.customer_id = s.customer_id AND TO_CHAR (apsa.trx_date, ''YYYY'') =  TO_CHAR (SYSDATE, ''YYYY'')  and 1=2
 group by s.customer_id';

            xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'arcust##ytdsales');

            EXECUTE IMMEDIATE
                'insert /*+append*/ into xxwc.arcust##ytdsales
SELECT s.customer_id, NVL (TO_CHAR (SUM (apsa.amount_due_original), ''9999999990.99''), ''0.00'') ytdsales
  FROM apps.ar_payment_schedules apsa, xxwc.arcust## s
 WHERE apsa.customer_id = s.customer_id AND TO_CHAR (apsa.trx_date, ''YYYY'') = TO_CHAR (SYSDATE, ''YYYY'')
 group by s.customer_id';

            COMMIT;
        END;

        BEGIN
            xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC', 'arcust##1', 'arcust##');

            EXECUTE IMMEDIATE
                'insert/*+append*/ into xxwc.arcust##(balance, customer_id, org_id,lastysales,ytdsales)
  select  s.balance, s.customer_id, s.org_id,s.lastysales,nvl(l.ytdsales,''0.00'') from xxwc.arcust##1 s,xxwc.arcust##ytdsales l
  where l.customer_id(+) = s.customer_id';

            COMMIT;
            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##ytdsales');
        END;

        -----***********************************************************
        -----***********************************************************
        --populate lastpay,lastamt
        -----***********************************************************

        BEGIN
            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##lastpay');

            EXECUTE IMMEDIATE
                'create table xxwc.arcust##lastpay as
 SELECT l.customer_id ,TO_CHAR (MAX (acra.receipt_date), ''MMDDYYYY'') lastpay, MAX (acra.cash_receipt_id) lastreceipt
    FROM apps.ar_cash_receipts acra, xxwc.arcust## l
   WHERE acra.pay_from_customer = l.customer_id  and 1=2
GROUP BY l.customer_id
 ';

            xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'xxwc.arcust##lastpay');

            EXECUTE IMMEDIATE
                'insert /*+append*/ into xxwc.arcust##lastpay
 SELECT l.customer_id,TO_CHAR (MAX (acra.receipt_date), ''MMDDYYYY'') lastpay, MAX (acra.cash_receipt_id) lastreceipt
    FROM apps.ar_cash_receipts acra, xxwc.arcust## l
   WHERE acra.pay_from_customer = l.customer_id
GROUP BY l.customer_id';

            COMMIT;
        END;

        BEGIN
            xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC', 'arcust##lastpay', 'arcust##lastpay1');

            EXECUTE IMMEDIATE ' alter table xxwc.arcust##lastpay1 add(lastamt number)';

            EXECUTE IMMEDIATE 'insert /*+append*/ into xxwc.arcust##lastpay1
 SELECT l.*,(SELECT NVL(acra.amount, 0)
               FROM apps.ar_cash_receipts acra
              WHERE acra.cash_receipt_id =l.lastreceipt) lastamt
    FROM xxwc.arcust##lastpay l
 ';

            COMMIT;
        END;

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##lastpay');

        BEGIN
            xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC', 'arcust##', 'arcust##1');

            EXECUTE IMMEDIATE
                'insert/*+append*/ into xxwc.arcust##1(balance, customer_id, org_id,lastysales,ytdsales,lastpay,lastamt)
  select  s.balance, s.customer_id, s.org_id,s.lastysales,s.ytdsales,l.lastpay,l.lastamt from xxwc.arcust## s,xxwc.arcust##lastpay1 l
  where l.customer_id(+) = s.customer_id';

            COMMIT;
            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##lastpay1');
        END;

        -----***********************************************************
        --populate ldate
        -----***********************************************************
        DECLARE
            v_tf   NUMBER;
        BEGIN
            xxwc_common_tunning_helpers.remove_tuning_factor_columns ('XXWC', 'arcust##1');
            v_tf := xxwc_common_tunning_helpers.add_tuning_parameter ('XXWC', 'xxwc.arcust##1', 100);

            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##ldate');

            EXECUTE IMMEDIATE 'create table xxwc.arcust##ldate (customer_id number, ldate varchar2(23))';

            xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'arcust##ldate');

            EXECUTE IMMEDIATE
                ' begin for r in 1..:v_tuning_loop loop
 INSERT /*+APPEND*/
      INTO  xxwc.arcust##ldate
        SELECT rcta.bill_to_customer_id customer_id, TO_CHAR (MAX (rcta.trx_date), ''MMDDYYYY'')
    FROM apps.ra_customer_trx rcta, xxwc.arcust##1 l
   WHERE rcta.bill_to_customer_id = l.customer_id AND l.group_number = r
GROUP BY rcta.bill_to_customer_id;
     COMMIT;
end loop; end;'
                USING v_tf;
        END;

        BEGIN
            xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC', 'arcust##1', 'arcust##');

            EXECUTE IMMEDIATE
                'insert/*+append*/ into xxwc.arcust##(balance, customer_id, org_id,lastysales,ytdsales,lastpay,lastamt,ldate)
  select  s.balance, s.customer_id, s.org_id,s.lastysales,s.ytdsales,s.lastpay,s.lastamt,l.ldate from xxwc.arcust##1 s,xxwc.arcust##ldate l
  where l.customer_id(+) = s.customer_id';

            COMMIT;
            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##ldate');
            xxwc_common_tunning_helpers.remove_tuning_factor_columns ('XXWC', 'arcust##');
        END;

        -----***********************************************************
        --CREATE CUSTOMER VIEW
        -----***********************************************************

        DECLARE
            v_view_exists   VARCHAR2 (1) := 'N';
        BEGIN
            FOR r IN (SELECT 'Y' ex
                        FROM all_objects
                       WHERE object_name = 'ARCUST##CUSTOMER' AND object_type = 'VIEW' AND owner = 'APPS')
            LOOP
                v_view_exists := r.ex;
            END LOOP;

            IF v_view_exists = 'N'
            THEN
                EXECUTE IMMEDIATE 'CREATE VIEW APPS.arcust##CUSTOMER AS
          SELECT cust_accounts.cust_account_id
          ,cust_accounts.party_id
          ,cust_accounts.account_number
          ,cust_accounts.account_established_date
          ,cust_accounts.comments
          ,cust_accounts.status cust_accounts_status
          ,cust_accounts.account_name
          ,party.party_name
          ,cust_acct_sites.cust_acct_site_id
          ,cust_acct_sites.status cust_acct_sites_status
          ,cust_acct_sites.org_id
          ,cust_acct_sites.bill_to_flag cust_acct_sites_bill_to_flag
          ,cust_acct_sites.ship_to_flag cust_acct_sites_ship_to_flag
          ,party_site.party_site_name
          ,party_site.party_site_number
          -- ,party_site.party_id
          ,party_site.location_id
          ,party_site.status party_site_status
          ,locations.address1
          ,locations.address2
          ,locations.address3
          ,locations.city
          ,locations.state
          ,locations.postal_code
          ,locations.country country
          ,cust_site_uses.location
          ,cust_site_uses.bill_to_site_use_id
          ,cust_site_uses.site_use_id
          ,cust_site_uses.site_use_code
          ,cust_site_uses.primary_flag
          ,cust_site_uses.status cust_site_uses_status
          ,cust_site_uses.org_id cust_site_uses_org_id
          ,cust_site_uses.primary_salesrep_id
          ,cust_accounts.attribute6
          ,cust_acct_sites.attribute17
          ,cust_acct_sites.attribute19
          ,party.duns_number
      FROM apps.hz_cust_accounts cust_accounts
          ,apps.hz_cust_acct_sites cust_acct_sites
          ,apps.hz_party_sites party_site
          ,apps.hz_locations locations
          ,apps.hz_cust_site_uses cust_site_uses
          ,apps.hz_parties party
     WHERE     cust_accounts.cust_account_id = cust_acct_sites.cust_account_id
           AND cust_acct_sites.party_site_id = party_site.party_site_id
           and party.party_id= cust_accounts.party_id
           AND locations.location_id = party_site.location_id
           AND cust_site_uses.cust_acct_site_id = cust_acct_sites.cust_acct_site_id
           AND cust_site_uses.site_use_code = ''BILL_TO''
           and NVL (cust_acct_sites.bill_to_flag, ''N'') = ''P''
           AND cust_site_uses.status = ''A''';
            END IF;
        END;

        -----***********************************************************

        BEGIN
            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##info');

            EXECUTE IMMEDIATE
                'create table xxwc.arcust##info as
SELECT c.account_number custno
       ,9999999 parent
      ,c.account_name company
      ,c.address1 address1
      ,c.address2 address2
      ,c.address3 address3
      ,c.city city
      ,c.state state
      ,c.postal_code zip
      ,c.country country
      , (SELECT raw_phone_number
           FROM apps.hz_contact_points
          WHERE     owner_table_id = c.party_id
                AND owner_table_name = ''HZ_PARTIES''
                AND contact_point_type = ''PHONE''
                AND phone_line_type = ''GEN''
                AND status = ''A''
                AND ROWNUM = 1)
           phone
      , (SELECT raw_phone_number
           FROM apps.hz_contact_points
          WHERE     owner_table_id = c.party_id
                AND owner_table_name = ''HZ_PARTIES''
                AND contact_point_type = ''PHONE''
                AND phone_line_type = ''FAX''
                AND status = ''A''
                AND ROWNUM = 1)
           faxno
      ,c.comments arcomment
      , (SELECT NVL (rsa.salesrep_number, ''9999999999'')
           FROM apps.ra_salesreps rsa
          WHERE c.primary_salesrep_id = rsa.salesrep_id AND c.org_id = rsa.org_id AND ROWNUM = 1)
           salesmn
      ,c.party_name contact
      ,c.account_established_date dateopend
      , (SELECT TO_CHAR (MAX (ats.op_bal_high_watermark), ''9999999990.99'')
           FROM apps.ar_trx_summary ats
          WHERE ats.cust_account_id = c.cust_account_id)
           highbal
      , (SELECT email_address
           FROM apps.hz_contact_points
          WHERE     owner_table_id = c.party_id
                AND owner_table_name = ''HZ_PARTIES''
                AND contact_point_type = ''EMAIL''
                AND primary_flag = ''Y''
                AND status = ''A''
                AND ROWNUM = 1)
           email
      ,c.org_id org_id
      ,c.duns_number ultimateduns
      ,l.customer_id
      ,c.party_id
  FROM apps.arcust##customer c, xxwc.arcust## l
 WHERE c.cust_account_id = l.customer_id and 1=2';

            xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'arcust##info');

            EXECUTE IMMEDIATE
                'insert /*+append*/ into xxwc.arcust##info
           SELECT c.account_number custno
           ,xxwc_ar_getpaid_arcust_v2.find_parent_party(c.party_id) parent
      ,c.account_name company
      ,c.address1 address1
      ,c.address2 address2
      ,c.address3 address3
      ,c.city city
      ,c.state state
      ,c.postal_code zip
      ,c.country country
      , (SELECT raw_phone_number
           FROM apps.hz_contact_points
          WHERE     owner_table_id = c.party_id
                AND owner_table_name = ''HZ_PARTIES''
                AND contact_point_type = ''PHONE''
                AND phone_line_type = ''GEN''
                AND status = ''A''
                AND ROWNUM = 1)
           phone
      , (SELECT raw_phone_number
           FROM apps.hz_contact_points
          WHERE     owner_table_id = c.party_id
                AND owner_table_name = ''HZ_PARTIES''
                AND contact_point_type = ''PHONE''
                AND phone_line_type = ''FAX''
                AND status = ''A''
                AND ROWNUM = 1)
           faxno
      ,c.comments arcomment
      , (SELECT NVL (rsa.salesrep_number, ''9999999999'')
           FROM apps.ra_salesreps rsa
          WHERE c.primary_salesrep_id = rsa.salesrep_id AND c.org_id = rsa.org_id AND ROWNUM = 1)
           salesmn
      ,c.party_name contact
      ,c.account_established_date dateopend
      , (SELECT TO_CHAR (MAX (ats.op_bal_high_watermark), ''9999999990.99'')
           FROM apps.ar_trx_summary ats
          WHERE ats.cust_account_id = c.cust_account_id)
           highbal
      , (SELECT email_address
           FROM apps.hz_contact_points
          WHERE     owner_table_id = c.party_id
                AND owner_table_name = ''HZ_PARTIES''
                AND contact_point_type = ''EMAIL''
                AND primary_flag = ''Y''
                AND status = ''A''
                AND ROWNUM = 1)
           email
      ,c.org_id org_id
       ,c.duns_number ultimateduns
      ,l.customer_id
       ,c.party_id
  FROM apps.arcust##customer c, xxwc.arcust## l
 WHERE c.cust_account_id = l.customer_id';

            COMMIT;
        END;

        -----***********************************************************
        -----***********************************************************

        BEGIN
            xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC', 'arcust##', 'arcust##1');

            EXECUTE IMMEDIATE 'alter table xxwc.arcust##1 add (party_id number)';

            EXECUTE IMMEDIATE '
INSERT /*+append*/
      INTO  xxwc.arcust##1 (balance
                          ,customer_id
                          ,org_id
                          ,lastysales
                          ,ytdsales
                          ,lastpay
                          ,lastamt
                          ,ldate
                          ,custno
                          ,parent
                          ,company
                          ,address1
                          ,address2
                          ,address3
                          ,city
                          ,zip
                          ,state
                          ,country
                          ,phone
                          ,faxno
                          ,arcomment
                          ,salesmn
                          ,contact
                          ,dateopend
                          ,email
                          ,ultimateduns
                          ,party_id)
    SELECT s.balance
          ,s.customer_id
          ,s.org_id
          ,s.lastysales
          ,s.ytdsales
          ,s.lastpay
          ,s.lastamt
          ,s.ldate
          ,l.custno
           ,l.parent
          ,l.company
          ,l.address1
          ,l.address2
          ,l.address3
          ,l.city
          ,l.zip
          ,l.state
          ,l.country
          ,l.phone
          ,l.faxno
          ,l.arcomment
          ,l.salesmn
          ,l.contact
          ,l.dateopend
          ,l.email
          ,l.ultimateduns
          ,l.party_id
      FROM xxwc.arcust## s, xxwc.arcust##info l
     WHERE l.customer_id(+) = s.customer_id';

            COMMIT;
            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##info');
        END;

        -----***********************************************************
        --populate profiles
        -----***********************************************************
        BEGIN
            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##profiles');

            EXECUTE IMMEDIATE
                'create table xxwc.arcust##profiles as
SELECT profiles_class.attribute1 TYPE
      ,profiless_amt.overall_credit_limit LIMIT
      , (SELECT al.meaning
           FROM apps.jtf_rs_defresources_v jrdv, apps.ar_lookups al
          WHERE     jrdv.source_id = ac.employee_id
                AND jrdv.user_name = al.lookup_code
                AND al.lookup_type = ''XXWC_GETPAID_COLLECTOR_XREF''
                AND SYSDATE BETWEEN al.start_date_active AND NVL (al.end_date_active, SYSDATE + 1)
                AND al.enabled_flag = ''Y''
                AND ROWNUM = 1)
           terr
      , (SELECT TO_CHAR (MAX (ats.op_bal_high_watermark), ''9999999990.99'')
           FROM apps.ar_trx_summary ats
          WHERE ats.cust_account_id = l.customer_id)
           highbal
      ,org_profiles.tax_reference fedid
      ,DECODE (profiles.credit_hold, ''Y'', ''H'', '' '') credit_status
      ,org_profiles.sic_code sic_code
      ,rt.description sales_term
      , (SELECT RPAD (jrdv.user_name, 10, '' '')
           FROM apps.jtf_rs_defresources_v jrdv
          WHERE jrdv.source_id = ac.employee_id AND ROWNUM = 1)
           prismterritory
      ,l.org_id org_id
      ,l.customer_id
  FROM apps.hz_customer_profiles profiles
      ,apps.hz_cust_profile_classes profiles_class
      ,apps.hz_cust_profile_amts profiless_amt
      ,apps.hz_organization_profiles org_profiles
      ,apps.ra_terms rt
      ,apps.ar_collectors ac
      ,xxwc.arcust##1 l
      ,apps.per_all_people_f ppf
 WHERE     l.party_id = org_profiles.party_id
       AND l.customer_id = profiles.cust_account_id
       AND profiles.site_use_id IS NULL
       AND profiles.cust_account_profile_id = profiless_amt.cust_account_profile_id(+)
       AND profiles.profile_class_id = profiles_class.profile_class_id
       AND profiles.collector_id = ac.collector_id
       AND profiles.standard_terms = rt.term_id(+)
       AND ac.employee_id =ppf.person_id(+)
       AND SYSDATE BETWEEN NVL (ppf.effective_start_date, SYSDATE - 1)
                           AND NVL (ppf.effective_end_date, SYSDATE + 1)
       AND SYSDATE BETWEEN NVL (org_profiles.effective_start_date, SYSDATE - 1)
                           AND NVL (org_profiles.effective_end_date, SYSDATE + 1)
       and 1=2';

            xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'arcust##profiles');

            EXECUTE IMMEDIATE
                'insert /*+append*/ into xxwc.arcust##profiles
               SELECT profiles_class.attribute1 TYPE
      ,profiless_amt.overall_credit_limit LIMIT
      , (SELECT al.meaning
           FROM apps.jtf_rs_defresources_v jrdv, apps.ar_lookups al
          WHERE     jrdv.source_id = ac.employee_id
                AND jrdv.user_name = al.lookup_code
                AND al.lookup_type = ''XXWC_GETPAID_COLLECTOR_XREF''
                AND SYSDATE BETWEEN al.start_date_active AND NVL (al.end_date_active, SYSDATE + 1)
                AND al.enabled_flag = ''Y''
                AND ROWNUM = 1)
           terr
      , (SELECT TO_CHAR (MAX (ats.op_bal_high_watermark), ''9999999990.99'')
           FROM apps.ar_trx_summary ats
          WHERE ats.cust_account_id = l.customer_id)
           highbal
      ,org_profiles.tax_reference fedid
      ,DECODE (profiles.credit_hold, ''Y'', ''H'', '' '') credit_status
      ,org_profiles.sic_code sic_code
      ,rt.description sales_term
      , (SELECT RPAD (jrdv.user_name, 10, '' '')
           FROM apps.jtf_rs_defresources_v jrdv
          WHERE jrdv.source_id = ac.employee_id AND ROWNUM = 1)
           prismterritory
      ,l.org_id org_id
      ,l.customer_id
  FROM apps.hz_customer_profiles profiles
      ,apps.hz_cust_profile_classes profiles_class
      ,apps.hz_cust_profile_amts profiless_amt
      ,apps.hz_organization_profiles org_profiles
      ,apps.ra_terms rt
      ,apps.ar_collectors ac
      ,xxwc.arcust##1 l
      ,apps.per_all_people_f ppf
 WHERE     l.party_id = org_profiles.party_id
       AND l.customer_id = profiles.cust_account_id
       AND profiles.site_use_id IS NULL
       AND profiles.cust_account_profile_id = profiless_amt.cust_account_profile_id(+)
       AND profiles.profile_class_id = profiles_class.profile_class_id
       AND profiles.collector_id = ac.collector_id
       AND profiles.standard_terms = rt.term_id(+)
       AND ac.employee_id =ppf.person_id(+)
       AND SYSDATE BETWEEN NVL (ppf.effective_start_date, SYSDATE - 1)
                           AND NVL (ppf.effective_end_date, SYSDATE + 1)
       AND SYSDATE BETWEEN NVL (org_profiles.effective_start_date, SYSDATE - 1)
                           AND NVL (org_profiles.effective_end_date, SYSDATE + 1)';

            COMMIT;
        END;

        BEGIN
            xxwc_common_tunning_helpers.create_table_from_other_table ('XXWC', 'arcust##1', 'arcust##');

            EXECUTE IMMEDIATE '
INSERT /*+append*/
      INTO  xxwc.arcust## (balance
                           ,customer_id
                           ,org_id
                           ,lastysales
                           ,ytdsales
                           ,lastpay
                           ,lastamt
                           ,ldate
                           ,custno
                           ,parent
                           ,company
                           ,address1
                           ,address2
                           ,address3
                           ,city
                           ,zip
                           ,state
                           ,country
                           ,phone
                           ,faxno
                           ,arcomment
                           ,salesmn
                           ,contact
                           ,dateopend
                           ,email
                           ,ultimateduns
                           ,party_id
                           ,TYPE
                           ,LIMIT
                           ,terr
                           ,highbal
                           ,fedid
                           ,credit_status
                           ,sic_code
                           ,sales_term
                           ,prismterritory)
    SELECT s.balance
          ,s.customer_id
          ,s.org_id
          ,s.lastysales
          ,s.ytdsales
          ,s.lastpay
          ,s.lastamt
          ,s.ldate
          ,s.custno
          ,s.parent
          ,s.company
          ,s.address1
          ,s.address2
          ,s.address3
          ,s.city
          ,s.zip
          ,s.state
          ,s.country
          ,s.phone
          ,s.faxno
          ,s.arcomment
          ,s.salesmn
          ,s.contact
          ,s.dateopend
          ,s.email
          ,s.ultimateduns
          ,s.party_id
          ,a.TYPE
          ,a.LIMIT
          ,a.terr
          ,a.highbal
          ,a.fedid
          ,a.credit_status
          ,a.sic_code
          ,a.sales_term
          ,a.prismterritory
      FROM xxwc.arcust##1 s, xxwc.arcust##profiles a
     WHERE a.customer_id(+) = s.customer_id';

            --

            COMMIT;
            --
            populate_parent_records;
            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##profiles');
            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##1');
        END;

        -----***********************************************************
        -----***********************************************************
        --need nonascii remove here
        --?>
        EXECUTE IMMEDIATE 'UPDATE xxwc.arcust##
   SET company = xxwc_common_tunning_helpers.remove_control_nonascii (company)
      ,address1 = xxwc_common_tunning_helpers.remove_control_nonascii (address1)
      ,address2 = xxwc_common_tunning_helpers.remove_control_nonascii (address2)
      ,address3 = xxwc_common_tunning_helpers.remove_control_nonascii (address3)
      ,city = xxwc_common_tunning_helpers.remove_control_nonascii (city)
      ,contact = xxwc_common_tunning_helpers.remove_control_nonascii (contact)
      ,terr = xxwc_common_tunning_helpers.remove_control_nonascii (terr)
      ,email = xxwc_common_tunning_helpers.remove_control_nonascii (email)
      ,zip = xxwc_common_tunning_helpers.remove_control_nonascii (zip)';

        COMMIT;

        -----***********************************************************
        --file
        -----***********************************************************
        BEGIN
            xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##file');

            EXECUTE IMMEDIATE
                'create table xxwc.arcust##file as
SELECT (   RPAD (NVL (custno, '' ''), 20, '' '')                                                                 --   CUSTNO
        || RPAD (NVL (parent, '' ''), 20, '' '')                                                                               --   PARENT
        || RPAD (NVL (company, '' ''), 50, '' '')                                                               --   COMPANY
        || RPAD (NVL (address1, '' ''), 50, '' '')                                                             --   ADDRESS1
        || RPAD (NVL (address2, '' ''), 50, '' '')                                                             --   ADDRESS2
        || RPAD (NVL (address3, '' ''), 50, '' '')                                                             --   ADDRESS3
        || RPAD (NVL (city, '' ''), 50, '' '')                                                                     --   CITY
        || RPAD (NVL (state, '' ''), 50, '' '')                                                                   --   STATE
        || RPAD (NVL (zip, '' ''), 20, '' '')                                                                       --   ZIP
        || RPAD (NVL (country, '' ''), 50, '' '')                                                               --   COUNTRY
        || RPAD (NVL ( (phone), '' ''), 35, '' '')                                                                --   PHONE
        || RPAD (NVL ( (faxno), '' ''), 35, '' '')                                                                --   FAXNO
        || RPAD (NVL (TYPE, '' ''), 10, '' '')                                                                     --   TYPE
        || RPAD (NVL (arcomment, '' ''), 65, '' '')                                                           --   ARCOMMENT
        || LPAD (NVL (TO_CHAR (LIMIT, ''9999999990.99''), ''0.00''), 20, '' '')                                     --   LIMIT
        || RPAD (NVL ( (salesmn), '' ''), 10, '' '')                                                            --   SALESMN
        || RPAD (contact, 20, '' '')                                                                          --   CONTACT
        || RPAD ('' '', 20, '' '')                                                                                --   TITLE
        || RPAD (NVL ( (lastpay), '' ''), 8, '' '')                                                             --   LASTPAY
        || RPAD (NVL (ldate, '' ''), 8, '' '')                                                                    --   LDATE
        || RPAD (NVL (terr, '' ''), 10, '' '')                                                                     --   TERR
        || LPAD (NVL (TO_CHAR (BALANCE, ''9999999990.99''), ''0.00''), 20, '' '')                                       --   TERR
        || RPAD ('' '', 10, '' '')                                                                              --   REFKEY1
        || RPAD ('' '', 10, '' '')                                                                              --   REFKEY2
        || RPAD ('' '', 8, '' '')                                                                              --   INVCHGDT
        || RPAD ('' '', 10, '' '')                                                                              --   LOCCURR
        || RPAD ('' '', 10, '' '')                                                                           --   CREDITSCOR
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER01
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER02
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER03
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER04
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER05
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER06
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER07
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER08
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER09
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER10
        || RPAD (TO_CHAR (NVL (dateopend, SYSDATE), ''MMDDYYYY''), 8, '' '')                                  --   DATEOPEND
        || LPAD (NVL (highbal, ''0.00''), 20, '' '')                                                            --   HIGHBAL
        || LPAD (NVL (TO_CHAR (lastamt, ''9999999990.99''), ''0.00''), 20, '' '')                                                         --   LASTAMT
        || LPAD (NVL ( (lastysales), '' ''), 20, '' '')                                                      --   LASTYSALES
        || LPAD (''0.00'', 20, '' '')                                                                        --   LASTQSALES
        || RPAD (NVL (TO_CHAR (ultimateduns), ''0''), 11, '' '')                                                   --   DUNS
        || RPAD ('' '', 11, '' '')                                                                         --   ULTIMATEDUNS
        || RPAD (NVL (fedid, '' ''), 11, '' '')                                                                   --   FEDID
        || RPAD ('' '', 9, '' '')                                                                              --   BNKRTNUM
        || LPAD (NVL ( (ytdsales), '' ''), 20, '' '')
        --   YTDSALES
        || LPAD (''0.00'', 20, '' '')                                                                          --   LTDSALES
        || RPAD ('' '', 8, '' '')                                                                                 --   FINYE
        || RPAD ('' '', 8, '' '')                                                                            --   CREDREVDTE
        || RPAD ('' '', 8, '' '')                                                                          --   EXPCRDLMTDTE
        || RPAD ('' '', 8, '' '')                                                                                --   LCVDTE
        || RPAD (''Accounts'', 50, '' '')                                                                     --   FIRSTNAME
        || RPAD (''Payable'', 50, '' '')                                                                       --   LASTNAME
        || RPAD ('' '', 20, '' '')                                                                         --   BUSINESSUNIT
        || RPAD ('' '', 10, '' '')                                                                          --   CREDIT_ACCT
        || RPAD ('' '', 10, '' '')                                                                               --   TICKER
        || RPAD (NVL (DECODE (credit_status, ''Y'', ''H'', '' ''), '' ''), 10, '' '')                           --   CREDIT_STATUS
        || LPAD (NVL (sic_code, ''0''), 10, '' '')                                                             --   SIC_CODE
        || RPAD ('' '', 4, '' '')                                                                         --   CUST_START_YR
        || RPAD ('' '', 4, '' '')                                                                          --   BUS_START_YR
        || RPAD ('' '', 8, '' '')                                                                                --   CRSCDT
        || RPAD ('' '', 8, '' '')                                                                                --   CRSTDT
        || RPAD ('' '', 8, '' '')                                                                                --   CRLIDT
        || RPAD (NVL (sales_term, '' ''), 10, '' '')                                                         --   SALES_TERM
        || RPAD (NVL (email, '' ''), 50, '' '')                                                                   --   EMAIL
        || RPAD ('' '', 10, '' '')                                                                                --   ARBDR
        || RPAD ('' '', 10, '' '')                                                                            --   CMACCT_ID
        || RPAD ('' '', 8, '' '')                                                                                --   CRRVDT
        || RPAD (NVL (prismterritory, '' ''), 10, '' '')                                                 --   PRISMTERRITORY
                                                    )
           rec_line
      ,org_id,BALANCE,custno
  FROM xxwc.arcust## where 1=2';

            xxwc_common_tunning_helpers.alter_table_temp ('XXWC', 'arcust##file');

            EXECUTE IMMEDIATE
                'insert/*+append*/ into  xxwc.arcust##file
SELECT (   RPAD (NVL (custno, '' ''), 20, '' '')                                                                 --   CUSTNO
        || RPAD (NVL (parent, '' ''), 20, '' '')                                                                 --   PARENT
        || RPAD (NVL (company, '' ''), 50, '' '')                                                               --   COMPANY
        || RPAD (NVL (address1, '' ''), 50, '' '')                                                             --   ADDRESS1
        || RPAD (NVL (address2, '' ''), 50, '' '')                                                             --   ADDRESS2
        || RPAD (NVL (address3, '' ''), 50, '' '')                                                             --   ADDRESS3
        || RPAD (NVL (city, '' ''), 50, '' '')                                                                     --   CITY
        || RPAD (NVL (state, '' ''), 50, '' '')                                                                   --   STATE
        || RPAD (NVL (zip, '' ''), 20, '' '')                                                                       --   ZIP
        || RPAD (NVL (country, '' ''), 50, '' '')                                                               --   COUNTRY
        || RPAD (NVL ( (phone), '' ''), 35, '' '')                                                                --   PHONE
        || RPAD (NVL ( (faxno), '' ''), 35, '' '')                                                                --   FAXNO
        || RPAD (NVL (TYPE, '' ''), 10, '' '')                                                                     --   TYPE
        || RPAD (NVL (arcomment, '' ''), 65, '' '')                                                           --   ARCOMMENT
        || LPAD (NVL (TO_CHAR (LIMIT, ''9999999990.99''), ''0.00''), 20, '' '')                                     --   LIMIT
        || RPAD (NVL ( (salesmn), '' ''), 10, '' '')                                                            --   SALESMN
        || RPAD (contact, 20, '' '')                                                                          --   CONTACT
        || RPAD ('' '', 20, '' '')                                                                                --   TITLE
        || RPAD (NVL ( (lastpay), '' ''), 8, '' '')                                                             --   LASTPAY
        || RPAD (NVL (ldate, '' ''), 8, '' '')                                                                    --   LDATE
        || RPAD (NVL (terr, '' ''), 10, '' '')                                                                     --   TERR
        || LPAD (NVL (TO_CHAR (BALANCE, ''9999999990.99''), ''0.00''), 20, '' '')                                       --   TERR
        || RPAD ('' '', 10, '' '')                                                                              --   REFKEY1
        || RPAD ('' '', 10, '' '')                                                                              --   REFKEY2
        || RPAD ('' '', 8, '' '')                                                                              --   INVCHGDT
        || RPAD ('' '', 10, '' '')                                                                              --   LOCCURR
        || RPAD ('' '', 10, '' '')                                                                           --   CREDITSCOR
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER01
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER02
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER03
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER04
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER05
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER06
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER07
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER08
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER09
        || RPAD ('' '', 10, '' '')                                                                           --   RESOLVER10
        || RPAD (TO_CHAR (NVL (dateopend, SYSDATE), ''MMDDYYYY''), 8, '' '')                                  --   DATEOPEND
        || LPAD (NVL (highbal, ''0.00''), 20, '' '')                                                            --   HIGHBAL
        || LPAD (NVL (TO_CHAR (lastamt, ''9999999990.99''), ''0.00''), 20, '' '')                                                         --   LASTAMT
        || LPAD (NVL ( (lastysales), '' ''), 20, '' '')                                                      --   LASTYSALES
        || LPAD (''0.00'', 20, '' '')                                                                        --   LASTQSALES
        || RPAD (NVL (TO_CHAR (ultimateduns), ''0''), 11, '' '')                                                   --   DUNS
        || RPAD ('' '', 11, '' '')                                                                         --   ULTIMATEDUNS
        || RPAD (NVL (fedid, '' ''), 11, '' '')                                                                   --   FEDID
        || RPAD ('' '', 9, '' '')                                                                              --   BNKRTNUM
        || LPAD (NVL ( (ytdsales), '' ''), 20, '' '')
        --   YTDSALES
        || LPAD (''0.00'', 20, '' '')                                                                          --   LTDSALES
        || RPAD ('' '', 8, '' '')                                                                                 --   FINYE
        || RPAD ('' '', 8, '' '')                                                                            --   CREDREVDTE
        || RPAD ('' '', 8, '' '')                                                                          --   EXPCRDLMTDTE
        || RPAD ('' '', 8, '' '')                                                                                --   LCVDTE
        || RPAD (''Accounts'', 50, '' '')                                                                     --   FIRSTNAME
        || RPAD (''Payable'', 50, '' '')                                                                       --   LASTNAME
        || RPAD ('' '', 20, '' '')                                                                         --   BUSINESSUNIT
        || RPAD ('' '', 10, '' '')                                                                          --   CREDIT_ACCT
        || RPAD ('' '', 10, '' '')                                                                               --   TICKER
        || RPAD (NVL (DECODE (credit_status, ''Y'', ''H'', '' ''), '' ''), 10, '' '')                           --   CREDIT_STATUS
        || LPAD (NVL (sic_code, ''0''), 10, '' '')                                                             --   SIC_CODE
        || RPAD ('' '', 4, '' '')                                                                         --   CUST_START_YR
        || RPAD ('' '', 4, '' '')                                                                          --   BUS_START_YR
        || RPAD ('' '', 8, '' '')                                                                                --   CRSCDT
        || RPAD ('' '', 8, '' '')                                                                                --   CRSTDT
        || RPAD ('' '', 8, '' '')                                                                                --   CRLIDT
        || RPAD (NVL (sales_term, '' ''), 10, '' '')                                                         --   SALES_TERM
        || RPAD (NVL (email, '' ''), 50, '' '')                                                                   --   EMAIL
        || RPAD ('' '', 10, '' '')                                                                                --   ARBDR
        || RPAD ('' '', 10, '' '')                                                                            --   CMACCT_ID
        || RPAD ('' '', 8, '' '')                                                                                --   CRRVDT
        || RPAD (NVL (prismterritory, '' ''), 10, '' '')                                                 --   PRISMTERRITORY
                                                    )
           rec_line
      ,org_id,BALANCE,custno
  FROM xxwc.arcust## where custno is not null';

            COMMIT;
        END;

        -----***********************************************************
        --CREATE LOG TABLE
        -----***********************************************************

        xxwc_common_tunning_helpers.drop_temp_table ('XXWC', 'arcust##file_LOG');

        EXECUTE IMMEDIATE 'CREATE TABLE xxwc.arcust##file_LOG AS
  SELECT COUNT (1) || ''_'' || SUM (balance) rec_line, org_id
    FROM xxwc.arcust##
  GROUP BY org_id';
  
     apps.xxwc_mv_routines_add_pkg. disable_parallelism;--ver 1.2
    -----***********************************************************
    -----***********************************************************
    -----***********************************************************
    -----***********************************************************
    -----***********************************************************
    -----***********************************************************
    -----***********************************************************
    -----***********************************************************

    EXCEPTION
        WHEN OTHERS
        THEN
            l_errbuf :=
                   ' Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            xxwc_common_tunning_helpers.write_log ('error executing procedure xxwc_ar_getpaid_arcust_v2 ' || l_errbuf);
            RAISE;
    END;

    /* This is called directly by UC4. Creates ARCUST file and the log */
    -----***********************************************************
    -----***********************************************************
    -----***********************************************************
    -----***********************************************************
    -----***********************************************************
    -----***********************************************************
    -----***********************************************************
    -----***********************************************************
    -----***********************************************************
    PROCEDURE create_arcust_file (p_errbuf              OUT VARCHAR2
                                 ,p_retcode             OUT NUMBER
                                 ,p_directory_name          VARCHAR2
                                 ,p_file_name               VARCHAR2
                                 ,p_log_file_name           VARCHAR2
                                 ,p_org_name         IN     VARCHAR2)
    IS
        l_file_name        VARCHAR2 (164);
        l_log_file_name    VARCHAR2 (164);
        p_org_id           NUMBER;
        v_query_str        VARCHAR2 (214);
        v_num_of_records   NUMBER;
    BEGIN
        p_retcode := 0;
        p_errbuf := NULL;

        

          -- Verifying Operating unit
      BEGIN
         SELECT organization_id
          INTO p_org_id
          FROM hr_operating_units
         WHERE name = p_org_name;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_org_id := 162;
         WHEN OTHERS
         THEN
           p_org_id := 162;
      END;

       mo_global.set_policy_context ('S', p_org_id);


        -- Creates temp tables with all data needed
        populate_arcust_fields;

        -- Create temp table with text formatted data

        -- default name
        IF p_file_name IS NULL
        THEN
            l_file_name := 'wcc_arcust_##' || TO_CHAR (SYSDATE, 'YYYYMMDD') || '.txt';
        ELSE
            l_file_name := p_file_name;
        END IF;

        -- Write the file!!
        xxwc_ar_getpaid_files_ob_pkg.create_file ('xxwc.arcust##file'                   --p_view_name        IN VARCHAR2
                                                 ,p_directory_name                      --p_directory_path   IN VARCHAR2
                                                 ,l_file_name                           --p_file_name        IN VARCHAR2
                                                 ,p_org_id                               --p_org_id           IN NUMBER)
                                                          );

        -- Work on the log file

        IF p_log_file_name IS NULL
        THEN
            l_log_file_name := 'wcc_arcust_##' || TO_CHAR (SYSDATE, 'YYYYMMDD') || '.log';
        ELSE
            l_log_file_name := p_log_file_name;
        END IF;

        DECLARE
            v_rtn_cd        INTEGER;

            l_fexists       BOOLEAN;
            l_file_length   NUMBER;
            l_block_size    INTEGER;
            l_file_handle   UTL_FILE.file_type;

            TYPE ref_cur IS REF CURSOR;

            view_cur        ref_cur;
            view_rec        xxwc_ob_common_pkg.xxcus_ob_file_rec;
        BEGIN
            UTL_FILE.fgetattr (p_directory_name
                              ,l_file_name
                              ,l_fexists
                              ,l_file_length
                              ,l_block_size);
            l_file_handle :=
                UTL_FILE.fopen (p_directory_name
                               ,l_log_file_name
                               ,'w'
                               ,32767);

            v_query_str := 'SELECT count(1)
                        FROM xxwc.arcust##file
                       WHERE org_id = :p_org_id';

            EXECUTE IMMEDIATE v_query_str INTO v_num_of_records USING p_org_id;

            UTL_FILE.put_line (l_file_handle, 'FILE: ' || l_file_name || CHR (13) || CHR (10));
            UTL_FILE.put_line (l_file_handle, 'RECORDS: ' || v_num_of_records || CHR (13) || CHR (10));
            UTL_FILE.put_line (l_file_handle, 'BYTES: ' || l_file_length || CHR (10) || CHR (13));

            UTL_FILE.fclose (l_file_handle);
        END;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_errbuf :=
                   ' Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            xxwc_common_tunning_helpers.write_log ('error executing procedure xxwc_ar_getpaid_arcust_v2 ' || l_errbuf);
            p_retcode := 2;
            p_errbuf := l_errbuf;
    END;

    --*****************************************************************
    --*****************************************************************
    FUNCTION find_parent_party (p_child_party_id NUMBER)
        RETURN VARCHAR2
    IS
        v_parent_number   VARCHAR2 (64) := NULL;
    BEGIN
        FOR r
            IN (SELECT objectparty.party_number
                  FROM apps.hz_relationships hzpuirelationshipseo
                      ,apps.hz_relationship_types reltype
                      ,apps.hz_parties subjectparty
                      ,apps.hz_parties objectparty
                      ,apps.fnd_lookup_values subjectpartytypelu
                      ,apps.fnd_lookup_values objectpartytypelu
                      ,apps.fnd_lookup_values relationshiprolelu
                 WHERE     hzpuirelationshipseo.subject_table_name = 'HZ_PARTIES'
                       AND hzpuirelationshipseo.object_table_name = 'HZ_PARTIES'
                       AND hzpuirelationshipseo.status IN ('A', 'I')
                       AND hzpuirelationshipseo.subject_id = subjectparty.party_id
                       AND hzpuirelationshipseo.object_id = objectparty.party_id
                       AND hzpuirelationshipseo.relationship_type = reltype.relationship_type
                       AND hzpuirelationshipseo.relationship_code = reltype.forward_rel_code
                       AND hzpuirelationshipseo.subject_type = reltype.subject_type
                       AND hzpuirelationshipseo.object_type = reltype.object_type
                       AND subjectpartytypelu.view_application_id = 222
                       AND subjectpartytypelu.lookup_type = 'PARTY_TYPE'
                       AND subjectpartytypelu.language = USERENV ('LANG')
                       AND subjectpartytypelu.lookup_code = hzpuirelationshipseo.subject_type
                       AND objectpartytypelu.view_application_id = 222
                       AND objectpartytypelu.lookup_type = 'PARTY_TYPE'
                       AND objectpartytypelu.language = USERENV ('LANG')
                       AND objectpartytypelu.lookup_code = hzpuirelationshipseo.object_type
                       AND relationshiprolelu.view_application_id = 222
                       AND relationshiprolelu.lookup_type = 'HZ_RELATIONSHIP_ROLE'
                       AND relationshiprolelu.language = USERENV ('LANG')
                       AND relationshiprolelu.lookup_code = reltype.role
                       AND subject_id = p_child_party_id
                       AND relationship_code <> 'CONTACT'
                       AND relationship_code = 'CHILD_OF'
                       AND subject_table_name = 'HZ_PARTIES'
                       AND hzpuirelationshipseo.object_type = 'ORGANIZATION'
                       AND hzpuirelationshipseo.relationship_type = 'National Accounts Grouping')
        LOOP
            v_parent_number := r.party_number;
        END LOOP;

        RETURN v_parent_number;
    END;

    ----****************************************************
    ----****************************************************
    ----****************************************************
    PROCEDURE populate_parent_records
    IS
    BEGIN
        EXECUTE IMMEDIATE '
BEGIN
    FOR r IN (SELECT distinct parent
                FROM xxwc.arcust##
               WHERE parent IS NOT NULL)
    LOOP
        INSERT INTO xxwc.arcust## (custno
                                  ,company
                                  ,address1
                                  ,address2
                                  ,address3
                                  ,city
                                  ,state
                                  ,zip
                                  ,country
                                  ,phone
                                  ,faxno
                                  ,contact
                                  ,dateopend
                                  ,email
                                  ,ultimateduns
                                  ,party_id
                                  ,org_id)
            SELECT party.party_number custno
                  ,party.party_name company
                  ,locations.address1
                  ,locations.address2
                  ,locations.address3
                  ,locations.city
                  ,locations.state
                  ,locations.postal_code
                  ,locations.country country
                  , (SELECT raw_phone_number
                       FROM apps.hz_contact_points
                      WHERE     owner_table_id = party.party_id
                            AND owner_table_name = ''HZ_PARTIES''
                            AND contact_point_type = ''PHONE''
                            AND phone_line_type = ''GEN''
                            AND status = ''A''
                            AND ROWNUM = 1)
                       phone
                  , (SELECT raw_phone_number
                       FROM apps.hz_contact_points
                      WHERE     owner_table_id = party.party_id
                            AND owner_table_name = ''HZ_PARTIES''
                            AND contact_point_type = ''PHONE''
                            AND phone_line_type = ''FAX''
                            AND status = ''A''
                            AND ROWNUM = 1)
                       faxno
                  ,party.party_name contact
                  ,party.creation_date dateopend
                  , (SELECT email_address
                       FROM apps.hz_contact_points
                      WHERE     owner_table_id = party.party_id
                            AND owner_table_name = ''HZ_PARTIES''
                            AND contact_point_type = ''EMAIL''
                            AND primary_flag = ''Y''
                            AND status = ''A''
                            AND ROWNUM = 1)
                       email
                  ,party.duns_number ultimateduns
                  ,party.party_id,162
              FROM apps.hz_party_sites party_site, apps.hz_locations locations, apps.hz_parties party
             WHERE     party.party_id = party_site.party_id
                   AND locations.location_id = party_site.location_id
                   AND party.party_number = r.parent;

        COMMIT;
    END LOOP;
END;';
    END;
END;
/

-- Grants for Package Body
GRANT EXECUTE ON apps.xxwc_ar_getpaid_arcust_v2 TO interface_xxcus
/

-- End of DDL Script for Package Body APPS.XXWC_AR_GETPAID_ARCUST_V2
