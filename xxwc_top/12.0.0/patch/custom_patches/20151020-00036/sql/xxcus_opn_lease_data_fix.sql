/*
 TMS: 20151020-00036
 Date: 10/20/2015
*/
set serveroutput on size 1000000;
declare
 --
 n_lease_id number :=0;
 n_parent_lease_id number :=0;
 v_db_name varchar2(20) :=Null;
 --
     procedure print_log(p_message in varchar2) is
     begin
      if apps.fnd_global.conc_request_id >0 then
       apps.fnd_file.put_line(fnd_file.log, p_message);
      else
       dbms_output.put_line(p_message);
      end if;
     end;
 --
begin
     --
     print_log('');     
     print_log('Begin update of OPN lease data');
     --   
     begin 
            select   name
            into      v_db_name
            from    v$database;
            --
     exception   
      when others then
        v_db_name :='NA'; 
        print_log('When-others-1, error ='||sqlerrm);       
     end;
    --           
     begin 
            select   lease_id
            into      n_lease_id
            from    pn_leases_all
            where 1 =1
            and lease_num ='1786NLUT15';
            --
     exception
      when no_data_found then
        n_lease_id :=0;        
      when others then
        n_lease_id :=0; 
        print_log('When-others-2, error ='||sqlerrm);       
     end;
    --
    print_log('n_lease_id ='||n_lease_id);
    --
     begin 
            select   lease_id
            into      n_parent_lease_id
            from    pn_leases_all
            where 1 =1
            and lease_num ='1786LEUT06';
            --
     exception
      when no_data_found then
        n_parent_lease_id :=0;        
      when others then
        n_parent_lease_id :=0; 
        print_log('When-others-3, error ='||sqlerrm);       
     end;
    --
    print_log('n_parent_lease_id ='||n_parent_lease_id);
    -- 
    if ( (n_lease_id >0) and (n_parent_lease_id >0) ) then
      begin 
       update pn_leases_all
       set lease_class_code  ='SUB_LEASE'
             ,parent_lease_id    =n_parent_lease_id
       where 1 =1
             and lease_id =n_lease_id;
        --
        print_log('Lease class and parent lease info updated for lease_id ='||n_lease_id||', msg ='||sql%rowcount);
        --
        if (sql%rowcount >0) then
         --
         commit;
         --
         print_log('Changes for lease_id '||n_lease_id||' saved in '||v_db_name);
         --                 
        end if;
        --
      exception
       when others then
        rollback to start_here;
        print_log('Error in updating lease_data for lease_id ='||n_lease_id||', msg ='||sqlerrm);
      end;
    else
     print_log('Either n_parent_lease_id or n_lease_id is zero, please check the value of both the variables.');
    end if;
    --
     print_log('End update of OPN lease data');
     print_log('');
     --    
exception
 when others then
  rollback to square1;
  print_log('Outer block, message ='||sqlerrm);
end;
/