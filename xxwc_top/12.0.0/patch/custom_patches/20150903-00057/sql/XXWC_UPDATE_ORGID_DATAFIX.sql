--TMS#20150903-00057 Maharajan Shunmugam
--Data fix to update org_id to 162 in staging table
UPDATE XXWC.XXWC_GETPAID_PRED_METRICS_TBL
SET ORG_ID = 162
/
COMMIT
/