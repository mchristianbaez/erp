/*************************************************************************
    $Header TMS_20161207-00249_Drop_Object.sql $
     Module Name: TMS_20161207-00249_Drop_Object
   
     PURPOSE:   This script to rename b2b table.

     REVISIONS:
     Ver        Date        Author                             Description
     ---------  ----------  -------------------------------    -------------------------
     1.0        12/08/2016  Rakesh Patel                       TMS#20161207-00249- B2B. Task to rename XXWC_B2B_CUST_INFO_TBL and XXWC_B2B_POD_INFO_TBL.
**************************************************************************/
DROP TABLE XXWC.XXWC_AR_BE_TEMP_TBL;
	
DROP PACKAGE APPS.xxwc_be_test_pkg;

CREATE TABLE XXWC.XXWC_B2B_CUST_INFO_TBL_BK1210 AS SELECT * FROM XXWC.XXWC_B2B_CUST_INFO_TBL;

DROP TABLE XXWC.XXWC_B2B_CUST_INFO_TBL;

CREATE TABLE XXWC.XXWC_B2B_POD_INFO_TBL_BK1210 AS SELECT * FROM XXWC.XXWC_B2B_POD_INFO_TBL;

DROP TABLE XXWC.XXWC_B2B_POD_INFO_TBL;