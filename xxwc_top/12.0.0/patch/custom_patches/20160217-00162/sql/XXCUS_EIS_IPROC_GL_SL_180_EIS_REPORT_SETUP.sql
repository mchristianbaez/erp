--Report Name            : HDS Account Analysis iPro Subledger Detail Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
/*prompt Creating View Data for HDS Account Analysis iPro Subledger Detail Report
set scan off define off*/
DECLARE
BEGIN 
--Inserting View XXCUS_EIS_IPROC_GL_SL_180_V
xxeis.eis_rs_ins.v( 'XXCUS_EIS_IPROC_GL_SL_180_V',101,'','','','','ID020048','XXEIS','Xxcus Eis Iproc Gl Sl 180 V','XEIGS1V','','');
--Delete View Columns for XXCUS_EIS_IPROC_GL_SL_180_V
xxeis.eis_rs_utility.delete_view_rows('XXCUS_EIS_IPROC_GL_SL_180_V',101,FALSE);
--Inserting View Columns for XXCUS_EIS_IPROC_GL_SL_180_V
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','PO_SHIP_TO_LOC',101,'Po Ship To Loc','PO_SHIP_TO_LOC','','','','ID020048','VARCHAR2','','','Po Ship To Loc','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GL_DATE',101,'Gl Date','GL_DATE','','','','ID020048','DATE','','','Gl Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','POL_CLOSED_CODE',101,'Pol Closed Code','POL_CLOSED_CODE','','','','ID020048','VARCHAR2','','','Pol Closed Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','PO_CLOSED_CODE',101,'Po Closed Code','PO_CLOSED_CODE','','','','ID020048','VARCHAR2','','','Po Closed Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','AP_INV_DATE',101,'Ap Inv Date','AP_INV_DATE','','','','ID020048','DATE','','','Ap Inv Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','FETCH_SEQ',101,'Fetch Seq','FETCH_SEQ','','','','ID020048','NUMBER','','','Fetch Seq','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','APPLIED_TO_SOURCE_ID_NUM_1',101,'Applied To Source Id Num 1','APPLIED_TO_SOURCE_ID_NUM_1','','','','ID020048','NUMBER','','','Applied To Source Id Num 1','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','SOURCE_DIST_ID_NUM_1',101,'Source Dist Id Num 1','SOURCE_DIST_ID_NUM_1','','','','ID020048','NUMBER','','','Source Dist Id Num 1','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','SOURCE_DIST_TYPE',101,'Source Dist Type','SOURCE_DIST_TYPE','','','','ID020048','VARCHAR2','','','Source Dist Type','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50368FUTURE_USEDESCR',101,'Gcc50368future Usedescr','GCC50368FUTURE_USEDESCR','','','','ID020048','VARCHAR2','','','Gcc50368future Usedescr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50368FUTURE_USE',101,'Gcc50368future Use','GCC50368FUTURE_USE','','','','ID020048','VARCHAR2','','','Gcc50368future Use','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50368SUBACCOUNTDESCR',101,'Gcc50368subaccountdescr','GCC50368SUBACCOUNTDESCR','','','','ID020048','VARCHAR2','','','Gcc50368subaccountdescr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50368SUBACCOUNT',101,'Gcc50368subaccount','GCC50368SUBACCOUNT','','','','ID020048','VARCHAR2','','','Gcc50368subaccount','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50368ACCOUNTDESCR',101,'Gcc50368accountdescr','GCC50368ACCOUNTDESCR','','','','ID020048','VARCHAR2','','','Gcc50368accountdescr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50368ACCOUNT',101,'Gcc50368account','GCC50368ACCOUNT','','','','ID020048','VARCHAR2','','','Gcc50368account','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50368DEPARTMENTDESCR',101,'Gcc50368departmentdescr','GCC50368DEPARTMENTDESCR','','','','ID020048','VARCHAR2','','','Gcc50368departmentdescr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50368DEPARTMENT',101,'Gcc50368department','GCC50368DEPARTMENT','','','','ID020048','VARCHAR2','','','Gcc50368department','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50368DIVISIONDESCR',101,'Gcc50368divisiondescr','GCC50368DIVISIONDESCR','','','','ID020048','VARCHAR2','','','Gcc50368divisiondescr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50368DIVISION',101,'Gcc50368division','GCC50368DIVISION','','','','ID020048','VARCHAR2','','','Gcc50368division','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50368PRODUCTDESCR',101,'Gcc50368productdescr','GCC50368PRODUCTDESCR','','','','ID020048','VARCHAR2','','','Gcc50368productdescr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50368PRODUCT',101,'Gcc50368product','GCC50368PRODUCT','','','','ID020048','VARCHAR2','','','Gcc50368product','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50328FUTURE_USE_2DESCR',101,'Gcc50328future Use 2descr','GCC50328FUTURE_USE_2DESCR','','','','ID020048','VARCHAR2','','','Gcc50328future Use 2descr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50328FUTURE_USE_2',101,'Gcc50328future Use 2','GCC50328FUTURE_USE_2','','','','ID020048','VARCHAR2','','','Gcc50328future Use 2','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50328FURTURE_USEDESCR',101,'Gcc50328furture Usedescr','GCC50328FURTURE_USEDESCR','','','','ID020048','VARCHAR2','','','Gcc50328furture Usedescr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50328FURTURE_USE',101,'Gcc50328furture Use','GCC50328FURTURE_USE','','','','ID020048','VARCHAR2','','','Gcc50328furture Use','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50328PROJECT_CODEDESCR',101,'Gcc50328project Codedescr','GCC50328PROJECT_CODEDESCR','','','','ID020048','VARCHAR2','','','Gcc50328project Codedescr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50328PROJECT_CODE',101,'Gcc50328project Code','GCC50328PROJECT_CODE','','','','ID020048','VARCHAR2','','','Gcc50328project Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50328ACCOUNTDESCR',101,'Gcc50328accountdescr','GCC50328ACCOUNTDESCR','','','','ID020048','VARCHAR2','','','Gcc50328accountdescr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50328ACCOUNT',101,'Gcc50328account','GCC50328ACCOUNT','','','','ID020048','VARCHAR2','','','Gcc50328account','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50328COST_CENTERDESCR',101,'Gcc50328cost Centerdescr','GCC50328COST_CENTERDESCR','','','','ID020048','VARCHAR2','','','Gcc50328cost Centerdescr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50328COST_CENTER',101,'Gcc50328cost Center','GCC50328COST_CENTER','','','','ID020048','VARCHAR2','','','Gcc50328cost Center','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50328LOCATIONDESCR',101,'Gcc50328locationdescr','GCC50328LOCATIONDESCR','','','','ID020048','VARCHAR2','','','Gcc50328locationdescr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50328LOCATION',101,'Gcc50328location','GCC50328LOCATION','','','','ID020048','VARCHAR2','','','Gcc50328location','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50328PRODUCTDESCR',101,'Gcc50328productdescr','GCC50328PRODUCTDESCR','','','','ID020048','VARCHAR2','','','Gcc50328productdescr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GCC50328PRODUCT',101,'Gcc50328product','GCC50328PRODUCT','','','','ID020048','VARCHAR2','','','Gcc50328product','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','CODE_COMBINATION_ID',101,'Code Combination Id','CODE_COMBINATION_ID','','','','ID020048','NUMBER','','','Code Combination Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','XXCUS_IMAGE_LINK',101,'Xxcus Image Link','XXCUS_IMAGE_LINK','','','','ID020048','VARCHAR2','','','Xxcus Image Link','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','XXCUS_PO_CREATED_BY',101,'Xxcus Po Created By','XXCUS_PO_CREATED_BY','','','','ID020048','VARCHAR2','','','Xxcus Po Created By','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','XXCUS_LINE_DESC',101,'Xxcus Line Desc','XXCUS_LINE_DESC','','','','ID020048','VARCHAR2','','','Xxcus Line Desc','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','ITEM_OVRHD_COST',101,'Item Ovrhd Cost','ITEM_OVRHD_COST','','','','ID020048','NUMBER','','','Item Ovrhd Cost','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','ITEM_MATERIAL_COST',101,'Item Material Cost','ITEM_MATERIAL_COST','','','','ID020048','NUMBER','','','Item Material Cost','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','ITEM_TXN_QTY',101,'Item Txn Qty','ITEM_TXN_QTY','','','','ID020048','NUMBER','','','Item Txn Qty','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','ITEM_UNIT_WT',101,'Item Unit Wt','ITEM_UNIT_WT','','','','ID020048','NUMBER','','','Item Unit Wt','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','PART_DESCRIPTION',101,'Part Description','PART_DESCRIPTION','','','','ID020048','VARCHAR2','','','Part Description','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','PART_NUMBER',101,'Part Number','PART_NUMBER','','','','ID020048','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','SLA_EVENT_TYPE',101,'Sla Event Type','SLA_EVENT_TYPE','','','','ID020048','VARCHAR2','','','Sla Event Type','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','AP_INV_SOURCE',101,'Ap Inv Source','AP_INV_SOURCE','','','','ID020048','VARCHAR2','','','Ap Inv Source','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','BOL_NUMBER',101,'Bol Number','BOL_NUMBER','','','','ID020048','VARCHAR2','','','Bol Number','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','PO_NUMBER',101,'Po Number','PO_NUMBER','','','','ID020048','VARCHAR2','','','Po Number','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','BATCH_NAME',101,'Batch Name','BATCH_NAME','','','','ID020048','VARCHAR2','','','Batch Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','NAME',101,'Name','NAME','','','','ID020048','VARCHAR2','','','Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','EFFECTIVE_PERIOD_NUM',101,'Effective Period Num','EFFECTIVE_PERIOD_NUM','','','','ID020048','NUMBER','','','Effective Period Num','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','TYPE',101,'Type','TYPE','','','','ID020048','VARCHAR2','','','Type','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','PERIOD_NAME',101,'Period Name','PERIOD_NAME','','','','ID020048','VARCHAR2','','','Period Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','CURRENCY_CODE',101,'Currency Code','CURRENCY_CODE','','','','ID020048','VARCHAR2','','','Currency Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GL_ACCOUNT_STRING',101,'Gl Account String','GL_ACCOUNT_STRING','','','','ID020048','VARCHAR2','','','Gl Account String','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','SALES_ORDER',101,'Sales Order','SALES_ORDER','','','','ID020048','VARCHAR2','','','Sales Order','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','TRANSACTION_NUM',101,'Transaction Num','TRANSACTION_NUM','','','','ID020048','VARCHAR2','','','Transaction Num','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','ASSOCIATE_NUM',101,'Associate Num','ASSOCIATE_NUM','','','','ID020048','VARCHAR2','','','Associate Num','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','SLA_DIST_ACCOUNTED_NET',101,'Sla Dist Accounted Net','SLA_DIST_ACCOUNTED_NET','','','','ID020048','NUMBER','','','Sla Dist Accounted Net','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','SLA_DIST_ACCOUNTED_DR',101,'Sla Dist Accounted Dr','SLA_DIST_ACCOUNTED_DR','','','','ID020048','NUMBER','','','Sla Dist Accounted Dr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','SLA_DIST_ACCOUNTED_CR',101,'Sla Dist Accounted Cr','SLA_DIST_ACCOUNTED_CR','','','','ID020048','NUMBER','','','Sla Dist Accounted Cr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','SLA_DIST_ENTERED_NET',101,'Sla Dist Entered Net','SLA_DIST_ENTERED_NET','','','','ID020048','NUMBER','','','Sla Dist Entered Net','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','SLA_DIST_ENTERED_DR',101,'Sla Dist Entered Dr','SLA_DIST_ENTERED_DR','','','','ID020048','NUMBER','','','Sla Dist Entered Dr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','SLA_DIST_ENTERED_CR',101,'Sla Dist Entered Cr','SLA_DIST_ENTERED_CR','','','','ID020048','NUMBER','','','Sla Dist Entered Cr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','SLA_LINE_ENTERED_NET',101,'Sla Line Entered Net','SLA_LINE_ENTERED_NET','','','','ID020048','NUMBER','','','Sla Line Entered Net','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','SLA_LINE_ENTERED_CR',101,'Sla Line Entered Cr','SLA_LINE_ENTERED_CR','','','','ID020048','NUMBER','','','Sla Line Entered Cr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','SLA_LINE_ENTERED_DR',101,'Sla Line Entered Dr','SLA_LINE_ENTERED_DR','','','','ID020048','NUMBER','','','Sla Line Entered Dr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','SLA_LINE_ACCOUNTED_NET',101,'Sla Line Accounted Net','SLA_LINE_ACCOUNTED_NET','','','','ID020048','NUMBER','','','Sla Line Accounted Net','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','SLA_LINE_ACCOUNTED_CR',101,'Sla Line Accounted Cr','SLA_LINE_ACCOUNTED_CR','','','','ID020048','NUMBER','','','Sla Line Accounted Cr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','SLA_LINE_ACCOUNTED_DR',101,'Sla Line Accounted Dr','SLA_LINE_ACCOUNTED_DR','','','','ID020048','NUMBER','','','Sla Line Accounted Dr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','ACCOUNTED_CR',101,'Accounted Cr','ACCOUNTED_CR','','','','ID020048','NUMBER','','','Accounted Cr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','ACCOUNTED_DR',101,'Accounted Dr','ACCOUNTED_DR','','','','ID020048','NUMBER','','','Accounted Dr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','ENTERED_CR',101,'Entered Cr','ENTERED_CR','','','','ID020048','NUMBER','','','Entered Cr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','ENTERED_DR',101,'Entered Dr','ENTERED_DR','','','','ID020048','NUMBER','','','Entered Dr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','CUSTOMER_OR_VENDOR_NUMBER',101,'Customer Or Vendor Number','CUSTOMER_OR_VENDOR_NUMBER','','','','ID020048','VARCHAR2','','','Customer Or Vendor Number','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','CUSTOMER_OR_VENDOR',101,'Customer Or Vendor','CUSTOMER_OR_VENDOR','','','','ID020048','VARCHAR2','','','Customer Or Vendor','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','GL_SL_LINK_ID',101,'Gl Sl Link Id','GL_SL_LINK_ID','','','','ID020048','NUMBER','','','Gl Sl Link Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','H_SEQ_ID',101,'H Seq Id','H_SEQ_ID','','','','ID020048','NUMBER','','','H Seq Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','SEQ_NUM',101,'Seq Num','SEQ_NUM','','','','ID020048','NUMBER','','','Seq Num','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','SEQ_ID',101,'Seq Id','SEQ_ID','','','','ID020048','NUMBER','','','Seq Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','LINE_ENT_CR',101,'Line Ent Cr','LINE_ENT_CR','','','','ID020048','NUMBER','','','Line Ent Cr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','LINE_ENT_DR',101,'Line Ent Dr','LINE_ENT_DR','','','','ID020048','NUMBER','','','Line Ent Dr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','LINE_ACCTD_CR',101,'Line Acctd Cr','LINE_ACCTD_CR','','','','ID020048','NUMBER','','','Line Acctd Cr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','LINE_ACCTD_DR',101,'Line Acctd Dr','LINE_ACCTD_DR','','','','ID020048','NUMBER','','','Line Acctd Dr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','LLINE',101,'Lline','LLINE','','','','ID020048','NUMBER','','','Lline','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','HNUMBER',101,'Hnumber','HNUMBER','','','','ID020048','NUMBER','','','Hnumber','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','LSEQUENCE',101,'Lsequence','LSEQUENCE','','','','ID020048','VARCHAR2','','','Lsequence','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','LINE_DESCR',101,'Line Descr','LINE_DESCR','','','','ID020048','VARCHAR2','','','Line Descr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','BATCH',101,'Batch','BATCH','','','','ID020048','NUMBER','','','Batch','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','ENTRY',101,'Entry','ENTRY','','','','ID020048','VARCHAR2','','','Entry','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','ACC_DATE',101,'Acc Date','ACC_DATE','','','','ID020048','DATE','','','Acc Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','JE_LINE_NUM',101,'Je Line Num','JE_LINE_NUM','','','','ID020048','NUMBER','','','Je Line Num','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','JE_HEADER_ID',101,'Je Header Id','JE_HEADER_ID','','','','ID020048','NUMBER','','','Je Header Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','JE_CATEGORY',101,'Je Category','JE_CATEGORY','','','','ID020048','VARCHAR2','','','Je Category','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','SOURCE',101,'Source','SOURCE','','','','ID020048','VARCHAR2','','','Source','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','PO_REQ_LINE_ITEM_DESC',101,'Po Req Line Item Desc','PO_REQ_LINE_ITEM_DESC','','','','ID020048','VARCHAR2','','','Po Req Line Item Desc','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_EIS_IPROC_GL_SL_180_V','PO_REQ_DESC',101,'Po Req Desc','PO_REQ_DESC','','','','ID020048','VARCHAR2','','','Po Req Desc','','','');
--Inserting View Components for XXCUS_EIS_IPROC_GL_SL_180_V
--Inserting View Component Joins for XXCUS_EIS_IPROC_GL_SL_180_V
END;
/
/*set scan on define on
prompt Creating Report LOV Data for HDS Account Analysis iPro Subledger Detail Report
set scan off define off*/
DECLARE
BEGIN 
--Inserting Report LOVs - HDS Account Analysis iPro Subledger Detail Report
xxeis.eis_rs_ins.lov( 101,'SELECT distinct user_je_source_name FROM gl_je_sources','','EIS_GL_JE_SOURCES_LOV','LOV of all Available Journal Sources','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( '','SELECT ffv.flex_value, ffvtl.description 
      FROM 
        fnd_flex_value_sets ffvs , 
        fnd_flex_values ffv, 
        fnd_flex_values_tl ffvtl 
      WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_PRODUCT'') 
       and ffv.flex_value_set_id = ffvs.flex_value_set_id 
       and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
      AND ffv.enabled_flag = upper(''Y'') 
      AND ffv.summary_flag <> upper(''Y'') 
      AND ffvtl.LANGUAGE = USERENV(''LANG'') 
      order by ffv.flex_value  ','','XXCUS_GL_PRODUCT','XXCUS_GL_PRODUCT','MM050208',NULL,'','','');
xxeis.eis_rs_ins.lov( '','SELECT ffv.flex_value, ffvtl.description 
      FROM 
        fnd_flex_value_sets ffvs , 
        fnd_flex_values ffv, 
        fnd_flex_values_tl ffvtl 
      WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_LOCATION'') 
       and ffv.flex_value_set_id = ffvs.flex_value_set_id 
       and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
      AND ffv.enabled_flag = upper(''Y'') 
      AND ffv.summary_flag <> upper(''Y'') 
      AND ffvtl.LANGUAGE = USERENV(''LANG'') 
      order by ffv.flex_value  ','','XXCUS_GL_LOCATION','XXCUS_GL_LOCATION','MM050208',NULL,'','','');
xxeis.eis_rs_ins.lov( '','SELECT ffv.flex_value, ffvtl.description 
      FROM 
        fnd_flex_value_sets ffvs , 
        fnd_flex_values ffv, 
        fnd_flex_values_tl ffvtl 
      WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_COSTCENTER'') 
       and ffv.flex_value_set_id = ffvs.flex_value_set_id 
       and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
      AND ffv.enabled_flag = upper(''Y'') 
      AND ffv.summary_flag <> upper(''Y'') 
      AND ffvtl.LANGUAGE = USERENV(''LANG'') 
      order by ffv.flex_value  ','','XXCUS_GL_COSTCENTER','XXCUS_GL_COSTCENTER','MM050208',NULL,'','','');
xxeis.eis_rs_ins.lov( '','SELECT ffv.flex_value, ffvtl.description 
      FROM 
        fnd_flex_value_sets ffvs , 
        fnd_flex_values ffv, 
        fnd_flex_values_tl ffvtl 
      WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_ACCOUNT'') 
       and ffv.flex_value_set_id = ffvs.flex_value_set_id 
       and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
      AND ffv.enabled_flag = upper(''Y'') 
      AND ffv.summary_flag <> upper(''Y'') 
      AND ffvtl.LANGUAGE = USERENV(''LANG'') 
      order by ffv.flex_value  ','','XXCUS_GL_ACCOUNT','XXCUS_GL_ACCOUNT','MM050208',NULL,'','','');
xxeis.eis_rs_ins.lov( '','SELECT ffv.flex_value, ffvtl.description 
      FROM 
        fnd_flex_value_sets ffvs , 
        fnd_flex_values ffv, 
        fnd_flex_values_tl ffvtl 
      WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_ACCOUNT'') 
       and ffv.flex_value_set_id = ffvs.flex_value_set_id 
       and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
      AND ffv.enabled_flag = upper(''Y'') 
      AND ffv.summary_flag <> upper(''Y'') 
      AND ffvtl.LANGUAGE = USERENV(''LANG'') 
      order by ffv.flex_value  ','','XXCUS_GL_ACCOUNT','XXCUS_GL_ACCOUNT','MM050208',NULL,'','','');
xxeis.eis_rs_ins.lov( '','SELECT ffv.flex_value, ffvtl.description 
      FROM 
        fnd_flex_value_sets ffvs , 
        fnd_flex_values ffv, 
        fnd_flex_values_tl ffvtl 
      WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_PROJECT'') 
       and ffv.flex_value_set_id = ffvs.flex_value_set_id 
       and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
      AND ffv.enabled_flag = upper(''Y'') 
      AND ffv.summary_flag <> upper(''Y'') 
      AND ffvtl.LANGUAGE = USERENV(''LANG'') 
      order by ffv.flex_value  ','','XXCUS_GL_PROJECT','XXCUS_GL_PROJECT','MM050208',NULL,'','','');
xxeis.eis_rs_ins.lov( 101,'select distinct period_name
from gl_je_headers','','HDS_GL_PERIOD_NAME','GL Period Name','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
/*set scan on define on
prompt Creating Report Data for HDS Account Analysis iPro Subledger Detail Report
set scan off define off*/
DECLARE
BEGIN 
--Deleting Report data - HDS Account Analysis iPro Subledger Detail Report
xxeis.eis_rs_utility.delete_report_rows( 'HDS Account Analysis iPro Subledger Detail Report' );
--Inserting Report - HDS Account Analysis iPro Subledger Detail Report
xxeis.eis_rs_ins.r( 101,'HDS Account Analysis iPro Subledger Detail Report','','This report will provide detail from the subledger for all entries posted for the account in general ledger. This was created for the accounting team to research the iProcurement PO invoices. Copied from the original report to include some additional columns.

ESMS# 306022
','','','','ID020048','XXCUS_EIS_IPROC_GL_SL_180_V','Y','','','ID020048','','N','HDS Standard Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - HDS Account Analysis iPro Subledger Detail Report
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'ACCOUNTED_CR','Accounted Cr','Accounted Cr','','','','','13','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'ACCOUNTED_DR','Accounted Dr','Accounted Dr','','','','','12','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'ACC_DATE','Acc Date','Acc Date','','','','','6','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'AP_INV_DATE','Ap Inv Date','Ap Inv Date','','','','','37','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'BATCH_NAME','Batch Name','Batch Name','','','','','3','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'BOL_NUMBER','Bol Number','Bol Number','','','','','45','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'CURRENCY_CODE','Currency Code','Currency Code','','','','','14','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'CUSTOMER_OR_VENDOR','Customer Or Vendor','Customer Or Vendor','','','','','11','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'CUSTOMER_OR_VENDOR_NUMBER','Customer Or Vendor Number','Customer Or Vendor Number','','','','','46','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'EFFECTIVE_PERIOD_NUM','Effective Period Num','Effective Period Num','','','','','15','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'ENTERED_CR','Entered Cr','Entered Cr','','','','','17','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'ENTERED_DR','Entered Dr','Entered Dr','','','','','16','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'ENTRY','Entry','Entry','','','','','18','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'GCC50328ACCOUNT','Gcc50328account','Gcc50328account','','','','','30','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'GCC50328ACCOUNTDESCR','Gcc50328accountdescr','Gcc50328accountdescr','','','','','31','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'GCC50328LOCATION','Gcc50328location','Gcc50328location','','','','','32','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'GCC50328LOCATIONDESCR','Gcc50328locationdescr','Gcc50328locationdescr','','','','','33','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'GCC50328PRODUCT','Gcc50328product','Gcc50328product','','','','','34','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'GCC50328PROJECT_CODE','Gcc50328project Code','Gcc50328project Code','','','','','35','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'GL_ACCOUNT_STRING','Gl Account String','Gl Account String','','','','','2','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'GL_SL_LINK_ID','Gl Sl Link Id','Gl Sl Link Id','','','','','36','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'HNUMBER','Hnumber','Hnumber','','','','','19','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'JE_CATEGORY','Je Category','Je Category','','','','','4','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'JE_LINE_NUM','Je Line Num','Je Line Num','','','','','20','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'LINE_ACCTD_CR','Line Acctd Cr','Line Acctd Cr','','','','','22','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'LINE_ACCTD_DR','Line Acctd Dr','Line Acctd Dr','','','','','21','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'LINE_DESCR','Line Descr','Line Descr','','','','','5','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'LINE_ENT_CR','Line Ent Cr','Line Ent Cr','','','','','24','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'LINE_ENT_DR','Line Ent Dr','Line Ent Dr','','','','','23','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'NAME','Name','Name','','','','','25','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'PERIOD_NAME','Period Name','Period Name','','','','','1','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'PO_NUMBER','Po Number','Po Number','','','','','8','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'SLA_DIST_ACCOUNTED_CR','Sla Dist Accounted Cr','Sla Dist Accounted Cr','','','','','27','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'SLA_DIST_ACCOUNTED_DR','Sla Dist Accounted Dr','Sla Dist Accounted Dr','','','','','26','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'SLA_DIST_ACCOUNTED_NET','Sla Dist Accounted Net','Sla Dist Accounted Net','','','','','28','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'SLA_EVENT_TYPE','Sla Event Type','Sla Event Type','','','','','44','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'SOURCE','Source','Source','','','','','7','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'TRANSACTION_NUM','Transaction Num','Transaction Num','','','','','9','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'TYPE','Type','Type','','','','','10','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'XXCUS_IMAGE_LINK','Xxcus Image Link','Xxcus Image Link','','','','','47','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'XXCUS_LINE_DESC','Xxcus Line Desc','Xxcus Line Desc','','','','','48','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'XXCUS_PO_CREATED_BY','Xxcus Po Created By','Xxcus Po Created By','','','','','49','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'GL_DATE','Gl Date','Gl Date','','','','','38','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'POL_CLOSED_CODE','Pol Closed Code','Pol Closed Code','','','','','40','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'PO_CLOSED_CODE','Po Closed Code','Po Closed Code','','','','','41','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'PO_REQ_DESC','Po Req Desc','Po Req Desc','','','','','42','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'PO_REQ_LINE_ITEM_DESC','Po Req Line Item Desc','Po Req Line Item Desc','','','','','43','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'PO_SHIP_TO_LOC','Po Ship To Loc','Po Ship To Loc','','','','','39','N','','','','','','','','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
xxeis.eis_rs_ins.rc( 'HDS Account Analysis iPro Subledger Detail Report',101,'SLA_DIST_ENT_NET','Sla Dist Ent Net','Po Ship To Loc','NUMBER','','','0','29','Y','','','','','','','XEIGS1V.SLA_DIST_ENTERED_NET*-1','ID020048','N','N','','XXCUS_EIS_IPROC_GL_SL_180_V','','');
--Inserting Report Parameters - HDS Account Analysis iPro Subledger Detail Report
xxeis.eis_rs_ins.rp( 'HDS Account Analysis iPro Subledger Detail Report',101,'Account Low','Gcc50328account','GCC50328ACCOUNT','>=','XXCUS_GL_ACCOUNT','','VARCHAR2','Y','Y','4','','Y','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS Account Analysis iPro Subledger Detail Report',101,'Cost Center','Gcc50328cost Center','GCC50328COST_CENTER','IN','XXCUS_GL_COSTCENTER','','VARCHAR2','N','Y','6','','Y','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS Account Analysis iPro Subledger Detail Report',101,'Location','Gcc50328location','GCC50328LOCATION','IN','XXCUS_GL_LOCATION','','VARCHAR2','N','Y','3','','Y','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS Account Analysis iPro Subledger Detail Report',101,'Product','Gcc50328product','GCC50328PRODUCT','IN','XXCUS_GL_PRODUCT','','VARCHAR2','Y','Y','2','','Y','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS Account Analysis iPro Subledger Detail Report',101,'Project Code','Gcc50328project Code','GCC50328PROJECT_CODE','IN','XXCUS_GL_PROJECT','','VARCHAR2','N','Y','7','','Y','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS Account Analysis iPro Subledger Detail Report',101,'Period Name','Period Name','PERIOD_NAME','IN','HDS_GL_PERIOD_NAME','','VARCHAR2','Y','Y','1','','Y','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS Account Analysis iPro Subledger Detail Report',101,'Source','Source','SOURCE','IN','EIS_GL_JE_SOURCES_LOV','','VARCHAR2','N','Y','8','','Y','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS Account Analysis iPro Subledger Detail Report',101,'Account High','Gcc50328account','GCC50328ACCOUNT','<=','XXCUS_GL_ACCOUNT','','VARCHAR2','Y','Y','5','','Y','CONSTANT','ID020048','Y','N','','','');
--Inserting Report Conditions - HDS Account Analysis iPro Subledger Detail Report
xxeis.eis_rs_ins.rcn( 'HDS Account Analysis iPro Subledger Detail Report',101,'GCC50328COST_CENTER','IN',':Gcc50328cost Center','','','Y','6','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS Account Analysis iPro Subledger Detail Report',101,'GCC50328LOCATION','IN',':Gcc50328location','','','Y','3','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS Account Analysis iPro Subledger Detail Report',101,'GCC50328PRODUCT','IN',':Gcc50328product','','','Y','2','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS Account Analysis iPro Subledger Detail Report',101,'GCC50328PROJECT_CODE','IN',':Gcc50328project Code','','','Y','7','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS Account Analysis iPro Subledger Detail Report',101,'PERIOD_NAME','IN',':Period Name','','','Y','1','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS Account Analysis iPro Subledger Detail Report',101,'SOURCE','IN',':Source','','','Y','8','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS Account Analysis iPro Subledger Detail Report',101,'GCC50328ACCOUNT','>=',':Account Low','','','Y','4','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS Account Analysis iPro Subledger Detail Report',101,'GCC50328ACCOUNT','<=',':Account High','','','Y','5','Y','ID020048');
--Inserting Report Sorts - HDS Account Analysis iPro Subledger Detail Report
--Inserting Report Triggers - HDS Account Analysis iPro Subledger Detail Report
--Inserting Report Templates - HDS Account Analysis iPro Subledger Detail Report
--Inserting Report Portals - HDS Account Analysis iPro Subledger Detail Report
--Inserting Report Dashboards - HDS Account Analysis iPro Subledger Detail Report
--Inserting Report Security - HDS Account Analysis iPro Subledger Detail Report
--Inserting Report Pivots - HDS Account Analysis iPro Subledger Detail Report
END;
/
--set scan on define on
