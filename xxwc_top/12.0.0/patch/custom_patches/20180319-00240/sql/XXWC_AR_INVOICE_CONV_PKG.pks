CREATE OR REPLACE PACKAGE APPS.XXWC_AR_INVOICE_CONV_PKG
AS
   /*************************************************************************************************************
      *    script name: xxwc_ar_invoice_conv_pkg.pks
      *
      *    script owners: lucidity consulting group.
      *
      *    client: white cap.
      *
      *    interface / conversion name: Open AR Invoice Conversion.
      *
      *    functional purpose: convert AR Invoices using open interface tables.
      *
      *    history:
      *
      *    Version    Date              Author          Description
      ***********************************************************************************************************
      *    1.0        04-Sep-2011       T.Rajaiah       Initial development.
      *    1.1        23-Mar-2018       Ashwin Sridhar  TMS#20180319-00240--AH HARRIS AR Transactions Conversion
      ************************************************************************************************************/
   l_verify_flag          VARCHAR2 (1);
   l_error_message        VARCHAR2 (240);
   g_validate_only        VARCHAR2 (1);
   g_import_only          VARCHAR2 (1);
   g_submit               VARCHAR2 (1);
   g_errorcode            NUMBER;
   g_errormessage         VARCHAR2 (240);
   v_errorcode            NUMBER;
   v_errormessage         VARCHAR2 (240);
   g_dev_phase            VARCHAR2 (100);
   g_dev_status           VARCHAR2 (100);
   g_request_id           NUMBER;
   ------
   g_set_of_books_id      NUMBER (15);
   g_cus_trx_type_id      NUMBER (15);
   g_site_id              NUMBER (15);
   g_account_id           NUMBER (15);
   g_salesrep_id          NUMBER (15);
   g_term_id              NUMBER (15);
   g_rec_ccid             NUMBER (15);
   g_revenue_ccid         NUMBER (15);
   g_terms_name           VARCHAR2 (240);
   g_sold_to              NUMBER;
   g_customer_id              NUMBER;
   g_bill_to_address_id   NUMBER;
   g_ship_to              NUMBER;
   g_ship_to_address_id   NUMBER;
   g_batch_source_id      NUMBER;
   g_batch_source_name    VARCHAR2 (100);
   -- GL Date value be defaulted from PRofile
   --G_GL_DATE              CONSTANT  DATE   := TO_DATE ('12/25/2011', 'MM/DD/YYYY') ;
   G_GL_DATE     CONSTANT DATE
      := TO_DATE (Fnd_Profile.VALUE ('XXWC_AR_CONVERSION_GL_DATE')
                 ,'DD-MON-YYYY') ;

   --*************************************************************************************************
   --Procedure  xxwc_ar_invoice_hdr_valid :  Call this API to validate AR Open Invoice record from stagign table. If
   -- the record is validated successfully then that records details are inserted into AR Open Invoice interface tables else
   -- the record is updated with status 'REJECTED'.


   PROCEDURE xxwc_ar_invoice_hdr_valid (p_amount                NUMBER
                                       ,p_cust_number           NUMBER
                                       ,p_bill_to               NUMBER
                                       ,p_sold_to               NUMBER
                                       ,p_salesrep_number       VARCHAR2
                                       ,p_terms_name            VARCHAR2
                                       ,p_attribute2            VARCHAR2
                                       ,p_cust_trx_type_name    VARCHAR2
                                       ,p_trx_number            VARCHAR2
                                       ,p_hds_site_flag         VARCHAR2
                                       ,p_ship_to               VARCHAR2);

   --*************************************************************************************************
   --Procedure  xxwc_ar_auto_invoice_prog :  Auto Invoice Master Program is kicked off using this API.

   PROCEDURE xxwc_ar_auto_invoice_prog;

   --*************************************************************************************************
   --Procedure  xxwc_stage_to_interface :  Using this API, successfully validated AR Open Invoices records from Staging tables are inserted into AR
   -- Open Invoice Interface Tables. .

   PROCEDURE xxwc_stage_to_interface(p_run_date IN VARCHAR2); --Added new a parameter by Ashwin.S for TMS#20180319-00240


   --*************************************************************************************************
   -- Procedure Main :  This procedure gets called from AR Open Invoice Conversion program.  Program can be called to just validate the records
   -- or validation and importing Open Invoices to AR schema can be done in one process too.
   -- How to call the API just to validate and not import.
   -- P_Validate_Only => 'Y'
   -- p_Submit        => 'N'
   -- to Import records from staging into AR Schema pass the program parameters as follows.
   -- P_Validate_Only => 'Y'
   -- p_Submit        => 'N'
   -- When an Invoice Transaction is processed successfully, Status column is updated as PROCESSED else it is updated as REJECTED
   -- IF a rejected record needs to be processed again then one should update Process_Status with NULL value.
   -- Only 20000 AR Open Invoice Transactions are processed every batch.

   PROCEDURE main (errbuf            OUT VARCHAR2
                  ,retcode           OUT NUMBER
                  ,p_validate_only   IN  VARCHAR2
                  ,p_submit          IN  VARCHAR2
                  ,p_run_date        IN  VARCHAR2); 
                  
  FUNCTION derive_ccid (p_segment1   IN     VARCHAR2,
                         p_segment2   IN     VARCHAR2,
                         p_segment3   IN     VARCHAR2,
                         p_segment4   IN     VARCHAR2,
                         p_segment5   IN     VARCHAR2,
                         p_segment6   IN     VARCHAR2,
                         p_segment7   IN     VARCHAR2,
                         x_ret_msg       OUT VARCHAR2)
      RETURN NUMBER;
                   
end XXWC_AR_INVOICE_CONV_PKG;
/