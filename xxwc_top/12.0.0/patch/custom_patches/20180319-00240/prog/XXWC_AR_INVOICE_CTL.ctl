--*********************************************************
--*File Name: XXWC_AR_INVOICE_CTL.ctl                     *
--*Author: Thiruvengadam Rajaiah.N                        *
--*Creation Date: 15-Sep-2011                             *
--*Last Updated Date: 22-May-2018 - Vamshi                *
--*Data Source: PMC_ORDER_IMPORT_STG                      *
--*Description: This is the control file. SQL Loader will *
--*           use to load the Ap Invoice information.     *
--* Satish U: 24-May-2012 : Added Options Parameter       *
--* Vamshi  : 22-May-2018 : Added SKIP=1                  *   
--*              TMS# 20180430-00041                      *
--*********************************************************

OPTIONS(SKIP=0,DIRECT=FALSE,PARALLEL=TRUE,ROWS=30000,BINDSIZE=20970000,ERRORS=3000)
LOAD DATA
REPLACE INTO TABLE XXWC_AR_INVOICES_CONV
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(INTERFACE_LINE_ATTRIBUTE1        " TRIM(:INTERFACE_LINE_ATTRIBUTE1)"
 , ATTRIBUTE2                       "TRIM(:ATTRIBUTE2)"
 , ATTRIBUTE3                       "TRIM(:ATTRIBUTE3)"
 , ACCTD_AMOUNT                     "TRIM(:ACCTD_AMOUNT)"
 , PAYING_CUSTOMER_ID               "TRIM(:PAYING_CUSTOMER_ID)"
 , AMOUNT                           "TRIM(:AMOUNT)"
 , TERM_NAME                        "TRIM(:TERM_NAME)"
 , ORIG_SYSTEM_BILL_CUSTOMER_REF    "TRIM(:ORIG_SYSTEM_BILL_CUSTOMER_REF)"
 , ORIG_SYSTEM_SOLD_CUSTOMER_REF    "TRIM(:ORIG_SYSTEM_SOLD_CUSTOMER_REF)"
 , TRX_DATE                         "to_date(:TRX_DATE,'MM/DD/RR')"
 , TRX_NUMBER                       "TRIM(:TRX_NUMBER)"
 , CUST_TRX_TYPE_NAME               "TRIM(:CUST_TRX_TYPE_NAME)" 
 , UNIT_SELLING_PRICE               "TRIM(:UNIT_SELLING_PRICE)"
 , PRIMARY_SALESREP_NUMBER          "TRIM(:PRIMARY_SALESREP_NUMBER)"
 , SALES_ORDER                      "TRIM(:SALES_ORDER)"
 , PURCHASE_ORDER                   "TRIM(:PURCHASE_ORDER)"
 , HDS_SITE_FLAG                    "TRIM(:HDS_SITE_FLAG)"
 , SHIP_TO_PARTY_SITE_NUMBER		"TRIM(:SHIP_TO_PARTY_SITE_NUMBER)"
 ,COMMENTS                          "REPLACE(REPLACE(TRIM(:COMMENTS),CHR(13),''),CHR(10),'')" 
)
