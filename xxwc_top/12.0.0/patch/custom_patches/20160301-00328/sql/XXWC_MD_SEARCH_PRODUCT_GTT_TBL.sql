  /*******************************************************************************
  Table: XXWC_MD_SEARCH_PRODUCT_GTT_TBL
  Description: This table is used to load items searched on AIS form
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     01-Mar-2016        Pahwa Nancy   TMS# 20160301-00328  Performance Tuning
  ********************************************************************************/

CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
(
  SCORE                   NUMBER,
  INVENTORY_ITEM_ID       NUMBER,
  ORGANIZATION_ID         NUMBER,
  PARTNUMBER              VARCHAR2(40 BYTE),
  TYPE                    CHAR(7 BYTE),
  MANUFACTURERPARTNUMBER  VARCHAR2(150 BYTE),
  MANUFACTURER            VARCHAR2(150 BYTE),
  SEQUENCE                NUMBER,
  SHORTDESCRIPTION        VARCHAR2(240 BYTE),
  QUANTITYONHAND          NUMBER,
  LIST_PRICE              NUMBER,
  BUYABLE                 CHAR(1 BYTE),
  KEYWORD                 VARCHAR2(1000 BYTE),
  ITEM_TYPE               VARCHAR2(30 BYTE),
  CROSS_REFERENCE         VARCHAR2(4000 BYTE),
  PRIMARY_UOM_CODE        VARCHAR2(3 BYTE)
)
ON COMMIT DELETE ROWS
RESULT_CACHE (MODE DEFAULT)
NOCACHE;
/