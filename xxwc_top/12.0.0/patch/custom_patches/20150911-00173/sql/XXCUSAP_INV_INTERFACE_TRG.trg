  CREATE OR REPLACE TRIGGER "APPS"."XXCUSAP_INV_INTERFACE_TRG" 
  AFTER UPDATE ON ap.ap_invoices_interface
  REFERENCING NEW AS NEW OLD AS OLD
  FOR EACH ROW


 WHEN (nvl (old.status, 'NOT PROCESSED') != 'PROCESSED' AND
        new.status = 'PROCESSED' --AND new.org_id IN (163, 162) AND NEW.source = 'DCTM'  --Version 1.1
 ) DECLARE
  /**************************************************************************
  File Name: XXCUSAP_INV_INTERFACE_TRG

  PROGRAM TYPE: SQL Script

  PURPOSE:      Attach Image url for invoice images from Documentum

  HISTORY
  =============================================================================
         Last Update Date : 07/27/2011
  =============================================================================
  =============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   -----------------  ------------------------------------
  1.0     28-Ju1-2011   Kathy Poling       Creation of trigger
  1.1     20-Aug-2014   Kathy Poling       ESMS 214639 enhance error handling
                                           and changes to following coding standards
                                           and added lookup for iProcurement RFC 42113 
  =============================================================================
  *****************************************************************************/
  l_err_msg       CLOB;
  l_sec           VARCHAR2(255);
  l_msg           VARCHAR2(150);
  l_invoice_id    ap_invoices_all.invoice_id%TYPE;
  l_category_id   NUMBER;
  l_doc_id        NUMBER(35);
  l_attach_doc_id NUMBER(35);
  l_media_id      NUMBER;
  l_rowid         ROWID;
  l_description   fnd_documents_tl.description%TYPE := 'Documentum Image URL';
  l_image_link    xxcus.xxcusap_dctm_inv_header_tbl.image_link%TYPE;
  l_data_type_id  fnd_document_datatypes.datatype_id%TYPE;
  l_user_id       NUMBER := fnd_global.user_id;
  l_login_id      NUMBER := fnd_profile.value('LOGIN_ID');
  l_org_exists    VARCHAR2(1) DEFAULT 'F';
  l_source_exists VARCHAR2(1) DEFAULT 'F';

  l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUSAP_INV_INTERFACE_TRG';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  no_attachment_insert EXCEPTION;
BEGIN
--Version 1.1
  BEGIN
    SELECT 'T'
      INTO l_org_exists
      FROM fnd_lookup_values
     WHERE 1 = 1
       AND lookup_type = 'XXCUS_AP_ORG_ATTACH_IMAGE'
       AND lookup_code = :new.org_id
       AND enabled_flag = 'Y'
       AND SYSDATE BETWEEN start_date_active AND
           nvl(end_date_active, SYSDATE + 1);
  EXCEPTION
    WHEN no_data_found THEN
      l_org_exists := 'F';
  END;

  IF l_org_exists = 'T'
  THEN

    BEGIN
      SELECT 'T'
        INTO l_source_exists
        FROM fnd_lookup_values
       WHERE 1 = 1
         AND lookup_type = 'XXCUS_AP_SOURCE_ATTACH_IMAGE'
         AND lookup_code = :new.source
         AND enabled_flag = 'Y'
         AND SYSDATE BETWEEN start_date_active AND
             nvl(end_date_active, SYSDATE + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_source_exists := 'F';
    END;

    IF l_source_exists = 'T'
    THEN
-- end lookup changes for version 1.1
      l_err_callpoint := 'Get INVOICE_ID in AP_INVOICES';

      BEGIN
        SELECT invoice_id
          INTO l_invoice_id
          FROM ap.ap_invoices_all
         WHERE invoice_num = :new.invoice_num
           AND vendor_id = :new.vendor_id
           AND vendor_site_id = :new.vendor_site_id;    --Version 1.1 

      EXCEPTION
        WHEN OTHERS THEN
          l_sec := 'No invoice exists for invoice_num: ' ||
                   :new.invoice_num || ' Vendor ID: ' || :new.vendor_id ||
                   ' in ap_invoices_all';
          RAISE program_error;
      END;

      l_err_callpoint := 'Creating URL Attachements';

      BEGIN
        l_image_link := NULL;

        IF :new.external_doc_ref IS NOT NULL AND
           :new.attribute11 IN ('FGHT', 'FRTEXP')
        THEN
          -- New BT to Interface logic
          l_image_link := :new.external_doc_ref;
        ELSE

          SELECT image_link
            INTO l_image_link
            FROM xxcus.xxcusap_dctm_inv_header_tbl
           WHERE invoice_id = :new.invoice_id;

        END IF;
      EXCEPTION
        WHEN no_data_found THEN
          l_sec := 'Image Link Not found for invoice num: ' ||
                   :new.invoice_num || ' Vendor ID: ' || :new.vendor_id ||
                   ' in xxcusap_dctm_inv_header_tbl';

          RAISE no_attachment_insert;
      END;

      BEGIN
        SELECT datatype_id
          INTO l_data_type_id
          FROM fnd_document_datatypes
         WHERE user_name = 'Web Page'
           AND datatype_id IS NOT NULL;
      EXCEPTION
        WHEN no_data_found THEN
          l_sec := 'Invoice_ID ' || l_invoice_id ||
                   ' data_type_id Not Found';

          RAISE no_attachment_insert;
      END;

      l_msg := 'l_data_type_id: ' || l_data_type_id;

      BEGIN
        SELECT category_id
          INTO l_category_id
          FROM fnd_document_categories_tl
         WHERE user_name = 'Invoice Internal'
           AND category_id IS NOT NULL;
      EXCEPTION
        WHEN no_data_found THEN
          l_sec := 'Invoice_ID ' || l_invoice_id ||
                   ' category_id Not Found';

          RAISE no_attachment_insert;
      END;

      l_sec := 'l_category_id: ' || l_category_id;

      l_doc_id        := NULL;
      l_attach_doc_id := NULL;

      SELECT fnd_documents_s.nextval INTO l_doc_id FROM dual;

      SELECT fnd_attached_documents_s.nextval
        INTO l_attach_doc_id
        FROM dual;

      l_sec := 'INSERT FND_DOCUMENTS for invoice_id: ' || l_invoice_id;
      --Version 1.1 removed inserts and changes to api's
      /*                         
        INSERT INTO fnd_documents
          (document_id
          ,last_update_date
          ,last_updated_by
          ,creation_date
          ,created_by
          ,last_update_login
          ,datatype_id
          ,category_id
          ,security_type
          ,security_id
          ,publish_flag
          ,usage_type
          ,url)
        VALUES
          (l_doc_id
          ,SYSDATE
          ,l_user_id
          ,SYSDATE
          ,l_user_id
          ,l_login_id
          ,l_data_type_id
          ,l_category_id
          ,'2'
          ,'1'
          ,'Y'
          ,'O'
          ,l_image_link);
      */
      fnd_documents_pkg.insert_row(x_rowid             => l_rowid
                                  ,x_document_id       => l_doc_id
                                  ,x_creation_date     => SYSDATE
                                  ,x_created_by        => l_user_id
                                  ,x_last_update_date  => SYSDATE
                                  ,x_last_updated_by   => l_user_id
                                  ,x_last_update_login => l_login_id
                                  ,x_datatype_id       => l_data_type_id
                                  ,x_category_id       => l_category_id
                                  ,x_security_type     => 2
                                  ,x_publish_flag      => 'Y'
                                  ,x_usage_type        => 'O'
                                  ,x_language          => 'US'
                                  ,x_description       => l_description
                                  ,x_media_id          => l_media_id
                                  ,x_url               => l_image_link);

      l_sec := 'INSERT FND_DOCUMENTS_TL for invoice_id: ' || l_invoice_id;

      /* INSERT INTO fnd_documents_tl
         (document_id
         ,last_update_date
         ,last_updated_by
         ,creation_date
         ,created_by
         ,last_update_login
         ,LANGUAGE
         ,file_name
         ,source_lang
         ,description)
       VALUES
         (l_doc_id
         ,SYSDATE
         ,l_user_id
         ,SYSDATE
         ,l_user_id
         ,l_login_id
         ,'US'
         ,l_image_link
         ,'US'
         ,'Image Link');
      */
      fnd_documents_pkg.insert_tl_row(x_document_id       => l_doc_id
                                     ,x_creation_date     => SYSDATE
                                     ,x_created_by        => l_user_id
                                     ,x_last_update_date  => SYSDATE
                                     ,x_last_updated_by   => l_user_id
                                     ,x_last_update_login => l_login_id
                                     ,x_language          => 'US');

      l_sec := 'INSERT FND_ATTACHED_DOCUMENTS for invoice_id: ' ||
               l_invoice_id;

      /* INSERT INTO fnd_attached_documents
         (attached_document_id
         ,document_id
         ,last_update_date
         ,last_updated_by
         ,creation_date
         ,created_by
         ,last_update_login
         ,seq_num
         ,entity_name
         ,automatically_added_flag
         ,pk1_value)
       VALUES
         (l_attach_doc_id
         ,l_doc_id
         ,SYSDATE
         ,l_user_id
         ,SYSDATE
         ,l_user_id
         ,l_login_id
         ,'10'
         ,'AP_INVOICES'
         ,'N'
         ,l_invoice_id);
       l_msg := 'Document Inserted for Oracle invoice_id: ' || l_invoice_id;
      */
      fnd_attached_documents_pkg.insert_row(x_rowid                    => l_rowid
                                           ,x_attached_document_id     => l_attach_doc_id
                                           ,x_document_id              => l_doc_id
                                           ,x_creation_date            => SYSDATE
                                           ,x_created_by               => l_user_id
                                           ,x_last_update_date         => SYSDATE
                                           ,x_last_updated_by          => l_user_id
                                           ,x_last_update_login        => l_login_id
                                           ,x_seq_num                  => '10'
                                           ,x_entity_name              => 'AP_INVOICES'
                                           ,x_column1                  => NULL
                                           ,x_pk1_value                => l_invoice_id
                                           ,x_pk2_value                => NULL
                                           ,x_pk3_value                => NULL
                                           ,x_pk4_value                => NULL
                                           ,x_pk5_value                => NULL
                                           ,x_automatically_added_flag => 'N'
                                           ,x_datatype_id              => 5
                                           ,x_category_id              => l_category_id
                                           ,x_security_type            => 2
                                           ,x_publish_flag             => 'Y'
                                           ,x_language                 => 'US'
                                           ,x_description              => l_description
                                           ,x_media_id                 => l_media_id
                                           ,x_url                      => l_image_link);

    END IF; -- source exists
  END IF; -- org exists

EXCEPTION
  WHEN no_attachment_insert THEN
    ROLLBACK;
    l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                 dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                 dbms_utility.format_error_backtrace();
    -- Calling ERROR API
    xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom
                                        ,p_calling     => l_err_callpoint
                                         --,p_request_id        => l_req_id
                                        ,p_ora_error_msg     => substr(l_err_msg
                                                                      ,1
                                                                      ,2000)
                                        ,p_error_desc        => substr(l_err_msg
                                                                      ,1
                                                                      ,240)
                                        ,p_argument1         => :new.invoice_id
                                        ,p_distribution_list => l_distro_list
                                        ,p_module            => 'AP');

  WHEN OTHERS THEN
    l_err_msg := l_err_msg || l_sec || ' ...Error_Stack...' ||
                 dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                 dbms_utility.format_error_backtrace();
    --l_msg := 'Failure in Trigger XXHSI_AP_INVOICES_INT_TRG: ' || SQLERRM;
    -- Calling ERROR API
    xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom
                                        ,p_calling     => l_err_callpoint
                                         --,p_request_id        => l_req_id
                                        ,p_ora_error_msg     => substr(l_err_msg
                                                                      ,1
                                                                      ,2000)
                                        ,p_error_desc        => substr(l_err_msg
                                                                      ,1
                                                                      ,240)
                                        ,p_argument1         => :new.invoice_id
                                        ,p_distribution_list => l_distro_list
                                        ,p_module            => 'AP');

END xxcusap_inv_interface_trg;
/
ALTER TRIGGER "APPS"."XXCUSAP_INV_INTERFACE_TRG" ENABLE;
/
