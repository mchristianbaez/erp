/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header APPS.XXWC_MD_SEARCH_MV_N4 $
  Module Name: APPS.XXWC_MD_SEARCH_MV_N4

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     1-SEP-2015   Pahwa, Nancy                Initially Created 
TMS# 20150825-00012
**************************************************************************/
drop index APPS.XXWC_MD_SEARCH_MV_N4;

create index APPS.XXWC_MD_SEARCH_MV_N4 on XXWC_MD_SEARCH_PRODUCTS_MV (name)
indextype is ctxsys.context parameters ('wordlist XXWC_MD_PRODUCT_SEARCH_PREF');