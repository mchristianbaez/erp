---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
$Header XXEIS.EIS_XXWC_NEW_ITEM_DATA_RPT_V $
Module Name : Inventory
PURPOSE   : New Item Report DataPull - WC
TMS Task Id : 20160503-00087,20160601-00138
REVISIONS   :
VERSION DATE               AUTHOR(S)       DESCRIPTION
------- -----------------  --------------- -----------------------------------------
1.0     16-May-2016        Siva      TMS#20160503-00087 , TMS#20160601-00138    Performance Tuning
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_NEW_ITEM_DATA_RPT_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_NEW_ITEM_DATA_RPT_V (ITEM_TYPE, ITEM_STATUS, ITEM_NUM, DESCRIPTION, ITEM_LEVEL, PRIMARY_UOM, VENDOR_PART_#, MASTER_VENDOR_#, VENDOR_NAME, VENDOR_TIER, PO_COST, CATEGORY_CLASS, CATMGT_CATEGORY_DESC, CATMGT_SUBCATEGORY_DESC, TIME_SENSITIVE, XPAR, SUGGESTED_RETAIL_PRICE, SELLING_UOM, HAZMAT_FLAG, COUNTRY_OF_ORIGIN, SHELF_LIFE_DAYS, WEIGHT, WEIGHT_UOM, CREATION_DATE, ITEM_CREATED_BY, LOCATION_NAME, APPROVAL_DATE)
AS
  SELECT mib.item_type ,
    mib.inventory_item_status_code item_status ,
    mib.segment1 item_num ,
    mib.description ,
    emib.c_ext_attr6 item_level, --Added by mahender for TMS#20150520-00159  on 06/05/2015
    MIB.PRIMARY_UNIT_OF_MEASURE PRIMARY_UOM,
    --  xref.cross_reference vendor_part_#, --commented for version 1.0
    XXEIS.EIS_XXWC_ITEM_DATA_PULL_PKG.get_vendor_part_num(mib.inventory_item_id) vendor_part_#, --added for version 1.0
    EMIB.C_EXT_ATTR1 MASTER_VENDOR_#,
    XXEIS.EIS_XXWC_ITEM_DATA_PULL_PKG.GET_VENDOR_DETAILS(EMIB.C_EXT_ATTR1,'VENDOR_NAME') VENDOR_NAME, --added for version 1.0
    XXEIS.EIS_XXWC_ITEM_DATA_PULL_PKG.GET_VENDOR_DETAILS(EMIB.C_EXT_ATTR1,'VENDOR_TIER') vendor_tier, --added for version 1.0
    --  APS.VENDOR_NAME, --commented for version 1.0
    --  aps.attribute3 vendor_tier, --commented for version 1.0
    MIB.LIST_PRICE_PER_UNIT PO_COST,
    -- mic.category_concat_segs category_class, --commented for version 1.0
    mc.concatenated_segments category_class , --added for version 1.0
    t_cat.description catmgt_category_desc ,
    T_SUB_CAT.DESCRIPTION CATMGT_SUBCATEGORY_DESC,
    /*(SELECT ts.category_concat_segs
    FROM apps.mtl_item_categories_v ts
    WHERE     MIB.INVENTORY_ITEM_ID = TS.INVENTORY_ITEM_ID
    AND ts.category_set_name = 'Time Sensitive'
    AND ROWNUM = 1                     --Added by mahender for TMS#20150520-00072 on 06/04/2015
    AND ts.organization_id = 222)
    TIME_SENSITIVE*/ --commented for version 1.0
    XXEIS.EIS_XXWC_ITEM_DATA_PULL_PKG.get_time_sensitive_cat(MIB.INVENTORY_ITEM_ID) time_sensitive, --added for version 1.0
    /*(SELECT micg.segment1
    FROM apps.mtl_item_catalog_groups_b micg
    WHERE mib.item_catalog_group_id = micg.item_catalog_group_id
    AND ROWNUM                      = 1 --Added by mahender for TMS#20150520-00072 on 06/04/2015
    ) xpar*/ --commented for version 1.0
    XXEIS.EIS_XXWC_ITEM_DATA_PULL_PKG.GET_ITEM_CATALOG_GROUP(mib.item_catalog_group_id) xpar, --added for version 1.0
    emib.c_ext_attr9 suggested_retail_price, --Added by mahender for TMS#20150520-00159  on 06/05/2015
    emib.c_ext_attr10 selling_uom ,          --Added by mahender for TMS#20150520-00159  on 06/05/2015
    mib.hazardous_material_flag hazmat_flag ,
    coo.meaning country_of_origin ,
    mib.shelf_life_days ,
    mib.unit_weight weight ,
    mib.weight_uom_code weight_uom ,
    mib.creation_date ,
    fuse.description item_created_by,
    --Added newly here:Location name
    /*   , (SELECT hrl.location_code
    FROM apps.per_people_f ppf, apps.per_assignments_v7 per, apps.hr_locations_all hrl
    WHERE     per.person_id = ppf.person_id
    AND ppf.person_id = fuse.employee_id
    AND ppf.effective_end_date > SYSDATE
    AND per.location_id = hrl.location_id
    AND ROWNUM = 1                     --Added by mahender for TMS#20150520-00072 on 06/04/2015
    )
    LOCATION_NAME
    */ --commented for version 1.0
    XXEIS.EIS_XXWC_ITEM_DATA_PULL_PKG.GET_LOCATION_NAME(fuse.employee_id) LOCATION_NAME, --added for version 1.0
    --,null --a.change_notice Change_Order_Number,
    --,null  -- a.status_type status_type,
    --,null  -- a.approval_status_type approval_status_type,
    --,null --   a.approval_date Approval_Date,
    XXEIS.EIS_XXWC_ITEM_DATA_PULL_PKG.GET_APPROVAL_DATE(mib.inventory_item_id,MIB.ORGANIZATION_ID) APPROVAL_DATE
    -- ,null --   a.change_id change_id,
    --,null --   b.status_name status_name,
    -- ,null --   d.status_code Approval_Status,
    --  f.segment1
    --Added newly here:Location name
  FROM apps.mtl_system_items_b mib,
    --,apps.mtl_parameters orgs
    --  ,APPS.MTL_ITEM_CATEGORIES_V MIC
    MTL_ITEM_CATEGORIES MIC ,
    MTL_CATEGORIES_B_KFV MC,
    /* ,(SELECT *
    FROM apps.mtl_cross_references_b
    where CROSS_REFERENCE_TYPE = 'VENDOR') XREF*/ --commented for version 1.0
    /*,(SELECT inventory_item_id
    ,organization_id
    ,c_ext_attr1
    ,c_ext_attr2
    ,c_ext_attr6                        --Added by mahender for TMS#20150520-00159  on 06/05/2015
    ,c_ext_attr9                        --Added by mahender for TMS#20150520-00159  on 06/05/2015
    ,c_ext_attr10                       --Added by mahender for TMS#20150520-00159  on 06/05/2015
    FROM apps.ego_mtl_sy_items_ext_b
    WHERE attr_group_id = 861 AND organization_id = 222) emib*/ --commented for version 1.0
    /*(SELECT lookup_code, meaning
    FROM apps.fnd_lookup_values
    WHERE lookup_type = 'XXWC_TERRITORIES') coo*/ --commented for version 1.0
    -- ,apps.ap_suppliers aps --commented for version 1.0
    apps.fnd_user fuse ,
    apps.mtl_categories_v mcv ,
    APPS.FND_FLEX_VALUES B_CAT ,
    APPS.FND_FLEX_VALUES_VL T_CAT,
    -- ,apps.fnd_flex_value_sets v_cat --commented for version 1.0
    APPS.FND_FLEX_VALUES B_SUB_CAT ,
    APPS.FND_FLEX_VALUES_VL T_SUB_CAT,
    -- ,APPS.FND_FLEX_VALUE_SETS V_SUB_CAT --commented for version 1.0
    APPS.EGO_MTL_SY_ITEMS_EXT_B EMIB ,
    apps.fnd_lookup_values coo
  WHERE 1 = 1
    --AND mib.organization_id = orgs.organization_id
  AND mib.inventory_item_id = mic.inventory_item_id
  AND MIB.ORGANIZATION_ID   = MIC.ORGANIZATION_ID
  AND MIC.CATEGORY_ID       = MC.CATEGORY_ID
    -- AND mib.inventory_item_id = xref.inventory_item_id(+) --commented for version 1.0
    --   and EMIB.C_EXT_ATTR1 = APS.SEGMENT1(+) --commented for version 1.0
  AND MIB.INVENTORY_ITEM_ID = EMIB.INVENTORY_ITEM_ID(+)
  AND MIB.organization_id   = EMIB.organization_id(+)
  AND EMIB.C_EXT_ATTR2      = COO.LOOKUP_CODE(+)
  AND COO.lookup_type(+)    = 'XXWC_TERRITORIES'
  AND MIB.CREATED_BY        = FUSE.USER_ID(+)
  AND mc.category_id        =mcv.category_id
    --AND mic.category_concat_segs = mcv.category_concat_segs --commented for version 1.0
    --  AND mcv.structure_name = 'Item Categories' --commented for version 1.0
  AND mcv.structure_id       = 101 --added for version 1.0
  AND B_CAT.FLEX_VALUE_ID    = T_CAT.FLEX_VALUE_ID 
  AND T_CAT.FLEX_VALUE_SET_ID=1016377 --added for version 1.0
    -- and B_CAT.FLEX_VALUE_SET_ID = V_CAT.FLEX_VALUE_SET_ID --commented for version 1.0
    -- AND v_cat.flex_value_set_name IN ('XXWC_CATMGT_CATEGORIES') --commented for version 1.0
  AND b_cat.flex_value           = mcv.attribute5
  AND B_SUB_CAT.FLEX_VALUE_ID    = T_SUB_CAT.FLEX_VALUE_ID
  AND T_SUB_CAT.FLEX_VALUE_SET_ID=1016378 --added for version 1.0
    --and B_SUB_CAT.FLEX_VALUE_SET_ID = V_SUB_CAT.FLEX_VALUE_SET_ID --commented for version 1.0
    --AND v_sub_cat.flex_value_set_name IN ('XXWC_CATMGT_SUBCATEGORIES') --commented for version 1.0
  AND b_sub_cat.flex_value = mcv.attribute6
    -- and MIC.CATEGORY_SET_NAME = 'Inventory Category' --commented for version 1.0
  AND MIC.CATEGORY_SET_id   =1100000062 --added for version 1.0
  AND emib.attr_group_id(+) = 861 --added for version 1.0
    --          AND mib.last_update_date = mib.creation_date  --Commented by mahender for TMS#20150520-00072 on 06/04/2015
    --          AND mib.last_update_date > (SYSDATE - 4)  --Commented by mahender for TMS#20150520-00072 on 06/04/2015
  AND mib.creation_date > (SYSDATE - 4) --Added by mahender for TMS#20150520-00072 on 06/04/2015
    --AND orgs.organization_code IN ('MST')
  AND mib.organization_id = 222
  UNION
  SELECT  /*+ INDEX(MC XXWC_MTL_CATEGORIES_B_N6)*/
    mib.item_type ,
    mib.inventory_item_status_code item_status ,
    mib.segment1 item_num ,
    mib.description ,
    emib.c_ext_attr6 item_level, --Added by mahender for TMS#20150520-00159  on 06/05/2015
    MIB.PRIMARY_UNIT_OF_MEASURE PRIMARY_UOM,
    --  xref.cross_reference vendor_part_#, --commented for version 1.0
    XXEIS.EIS_XXWC_ITEM_DATA_PULL_PKG.get_vendor_part_num(mib.inventory_item_id) vendor_part_#, --added for version 1.0
    EMIB.C_EXT_ATTR1 MASTER_VENDOR_#,
    XXEIS.EIS_XXWC_ITEM_DATA_PULL_PKG.GET_VENDOR_DETAILS(EMIB.C_EXT_ATTR1,'VENDOR_NAME') VENDOR_NAME, --added for version 1.0
    XXEIS.EIS_XXWC_ITEM_DATA_PULL_PKG.GET_VENDOR_DETAILS(EMIB.C_EXT_ATTR1,'VENDOR_TIER') vendor_tier, --added for version 1.0
    --  APS.VENDOR_NAME, --commented for version 1.0
    --  aps.attribute3 vendor_tier, --commented for version 1.0
    MIB.LIST_PRICE_PER_UNIT PO_COST,
    -- mic.category_concat_segs category_class, --commented for version 1.0
    mc.concatenated_segments category_class , --added for version 1.0
    t_cat.description catmgt_category_desc ,
    T_SUB_CAT.DESCRIPTION CATMGT_SUBCATEGORY_DESC,
    /*(SELECT ts.category_concat_segs
    FROM apps.mtl_item_categories_v ts
    WHERE     MIB.INVENTORY_ITEM_ID = TS.INVENTORY_ITEM_ID
    AND ts.category_set_name = 'Time Sensitive'
    AND ROWNUM = 1                     --Added by mahender for TMS#20150520-00072 on 06/04/2015
    AND ts.organization_id = 222)
    TIME_SENSITIVE*/ --commented for version 1.0
    XXEIS.EIS_XXWC_ITEM_DATA_PULL_PKG.get_time_sensitive_cat(MIB.INVENTORY_ITEM_ID) time_sensitive, --commented for version 1.0
    /*(SELECT micg.segment1
    FROM apps.mtl_item_catalog_groups_b micg
    WHERE mib.item_catalog_group_id = micg.item_catalog_group_id
    AND ROWNUM                      = 1 --Added by mahender for TMS#20150520-00072 on 06/04/2015
    ) xpar*/ --commented for version 1.0
    XXEIS.EIS_XXWC_ITEM_DATA_PULL_PKG.GET_ITEM_CATALOG_GROUP(mib.item_catalog_group_id) xpar, --commented for version 1.0
    emib.c_ext_attr9 suggested_retail_price, --Added by mahender for TMS#20150520-00159  on 06/05/2015
    emib.c_ext_attr10 selling_uom ,          --Added by mahender for TMS#20150520-00159  on 06/05/2015
    mib.hazardous_material_flag hazmat_flag ,
    coo.meaning country_of_origin ,
    mib.shelf_life_days ,
    mib.unit_weight weight ,
    mib.weight_uom_code weight_uom ,
    mib.creation_date ,
    fuse.description item_created_by,
    --Added newly here:Location name
    /*   , (SELECT hrl.location_code
    FROM apps.per_people_f ppf, apps.per_assignments_v7 per, apps.hr_locations_all hrl
    WHERE     per.person_id = ppf.person_id
    AND ppf.person_id = fuse.employee_id
    AND ppf.effective_end_date > SYSDATE
    AND per.location_id = hrl.location_id
    AND ROWNUM = 1                     --Added by mahender for TMS#20150520-00072 on 06/04/2015
    )
    LOCATION_NAME
    */ --commented for version 1.0
    XXEIS.EIS_XXWC_ITEM_DATA_PULL_PKG.GET_LOCATION_NAME(fuse.employee_id) LOCATION_NAME, --commented for version 1.0
    --eec.change_notice Change_Order_Number,
    -- eec.status_type,
    --  eec.approval_status_type,
    EEC.APPROVAL_DATE APPROVAL_DATE
    -- eec.change_id,
    -- ecs.status_name,
    -- ecr.status_code Approval_Status
    -- mib.segment1
  FROM apps.eng_engineering_changes eec,
    apps.eng_change_statuses_vl ecs,
    APPS.ENG_CHANGE_ORDER_TYPES_V ecot,
    APPS.ENG_CHANGE_ROUTES ecr,
    APPS.ENG_CHANGE_SUBJECTS ECS1,
    apps.mtl_system_items_b mib,
    --,apps.mtl_parameters orgs
    --  ,APPS.MTL_ITEM_CATEGORIES_V MIC
    MTL_ITEM_CATEGORIES MIC ,
    MTL_CATEGORIES_B_KFV MC,
    /* ,(SELECT *
    FROM apps.mtl_cross_references_b
    where CROSS_REFERENCE_TYPE = 'VENDOR') XREF*/ --commented for version 1.0
    /*,(SELECT inventory_item_id
    ,organization_id
    ,c_ext_attr1
    ,c_ext_attr2
    ,c_ext_attr6                        --Added by mahender for TMS#20150520-00159  on 06/05/2015
    ,c_ext_attr9                        --Added by mahender for TMS#20150520-00159  on 06/05/2015
    ,c_ext_attr10                       --Added by mahender for TMS#20150520-00159  on 06/05/2015
    FROM apps.ego_mtl_sy_items_ext_b
    WHERE attr_group_id = 861 AND organization_id = 222) emib*/ --commented for version 1.0
    /*(SELECT lookup_code, meaning
    FROM apps.fnd_lookup_values
    WHERE lookup_type = 'XXWC_TERRITORIES') coo*/ --commented for version 1.0
    -- ,apps.ap_suppliers aps --commented for version 1.0
    apps.fnd_user fuse ,
    apps.mtl_categories_v mcv ,
    APPS.FND_FLEX_VALUES B_CAT ,
    APPS.FND_FLEX_VALUES_VL T_CAT,
    -- ,apps.fnd_flex_value_sets v_cat --commented for version 1.0
    APPS.FND_FLEX_VALUES B_SUB_CAT ,
    APPS.FND_FLEX_VALUES_VL T_SUB_CAT,
    -- ,APPS.FND_FLEX_VALUE_SETS V_SUB_CAT --commented for version 1.0
    APPS.EGO_MTL_SY_ITEMS_EXT_B EMIB ,
    apps.fnd_lookup_values coo
  WHERE eec.change_order_type_id = ecot.change_order_type_id
  AND eec.CHANGE_ID              = ecr.OBJECT_ID1
  AND eec.change_id              = ecs1.change_id
  AND ecr.classification_code    = ecs.status_code
  AND ecs1.pk1_value             = mib.inventory_item_id
  AND ecs1.pk2_value             = mib.organization_id
  AND ecs.status_name            = 'Released'
  AND eec.change_mgmt_type_code  = 'CHANGE_REQUEST'
    --AND mib.organization_id = orgs.organization_id --commented for version 1.0
  AND mib.inventory_item_id = mic.inventory_item_id
  AND MIB.ORGANIZATION_ID   = MIC.ORGANIZATION_ID
  AND MIC.CATEGORY_ID       = MC.CATEGORY_ID
    -- AND mib.inventory_item_id = xref.inventory_item_id(+) --commented for version 1.0
    --   and EMIB.C_EXT_ATTR1 = APS.SEGMENT1(+) --commented for version 1.0
  AND MIB.INVENTORY_ITEM_ID = EMIB.INVENTORY_ITEM_ID(+)
  AND MIB.organization_id   = EMIB.organization_id(+)
  AND EMIB.C_EXT_ATTR2      = COO.LOOKUP_CODE(+)
  AND COO.lookup_type(+)    = 'XXWC_TERRITORIES'
  AND MIB.CREATED_BY        = FUSE.USER_ID(+)
  AND mc.category_id        = mcv.category_id
    --AND mic.category_concat_segs = mcv.category_concat_segs --commented for version 1.0
    --  AND mcv.structure_name = 'Item Categories' --commented for version 1.0
  AND mcv.structure_id       = 101 --added for version 1.0
  AND B_CAT.FLEX_VALUE_ID    = T_CAT.FLEX_VALUE_ID
  AND T_CAT.FLEX_VALUE_SET_ID= 1016377 --added for version 1.0
    -- and B_CAT.FLEX_VALUE_SET_ID = V_CAT.FLEX_VALUE_SET_ID --commented for version 1.0
    -- AND v_cat.flex_value_set_name IN ('XXWC_CATMGT_CATEGORIES') --commented for version 1.0
  AND b_cat.flex_value           = mcv.attribute5
  AND B_SUB_CAT.FLEX_VALUE_ID    = T_SUB_CAT.FLEX_VALUE_ID
  AND T_SUB_CAT.FLEX_VALUE_SET_ID= 1016378 --added for version 1.0
    --and B_SUB_CAT.FLEX_VALUE_SET_ID = V_SUB_CAT.FLEX_VALUE_SET_ID --commented for version 1.0
    --AND v_sub_cat.flex_value_set_name IN ('XXWC_CATMGT_SUBCATEGORIES') --commented for version 1.0
  AND b_sub_cat.flex_value = mcv.attribute6
    -- and MIC.CATEGORY_SET_NAME = 'Inventory Category' --commented for version 1.0
  AND MIC.CATEGORY_SET_id        = 1100000062 --added for version 1.0
  AND emib.attr_group_id(+)      = 861
  AND eec.organization_id        = 222
  AND TRUNC (EEC.APPROVAL_DATE) >= TRUNC (SYSDATE) - 4
/
