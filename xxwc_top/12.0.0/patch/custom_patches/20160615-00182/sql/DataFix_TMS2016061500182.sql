-- TMSTask ID: 20160615-00182  date-7/716 created by neha Saini
-- This is a datafix to fix rental Asset transfer report which is showing wrong cost in DFF.
SET SERVEROUTPUT ON
BEGIN
UPDATE apps.mtl_material_transactions
   SET attribute7 = 72379.20
 WHERE transaction_id = 401684643;
DBMS_OUTPUT.PUT_LINE(' no of lines updated ' ||SQL%ROWCOUNT );
COMMIT;
END;
/