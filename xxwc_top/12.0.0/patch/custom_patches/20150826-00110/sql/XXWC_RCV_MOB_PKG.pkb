CREATE OR REPLACE PACKAGE BODY APPS.XXWC_RCV_MOB_PKG
AS
   /*****************************************************************************************************************************************
   *   $Header XXWC_MWA_ROUTINES_PKG $                                                                                                      *
   *   Module Name: XXWC_MWA_ROUTINES_PKG                                                                                                   *
   *                                                                                                                                        *
   *   PURPOSE:   This package is used by the extensions in MSCA                                                                            *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *                                                                                                                                        *
   *   2.0        03-SEP-2015  P.Vamshidhar              TMS#20150826-00110  - RF - Receipt Traveller Generation                            *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *****************************************************************************************************************************************/

   PROCEDURE DEBUG_LOG (P_MESSAGE VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      IF g_debug = 'Y'
      THEN
         FND_LOG.STRING (
            G_LOG_LEVEL,
            UPPER (G_PACKAGE || '.' || G_CALL_FROM) || ' ' || G_CALL_POINT,
            P_MESSAGE);

         COMMIT;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message := 'Error in generating debug log ';

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END DEBUG_LOG;

   /*****************************************************************************************************************************************
   *   PROCEDURE GET_LOC_LOV_RCV                                                                                                            *
   *                                                                                                                                        *
   *   PURPOSE:   This function is used to get the default open quantity for receiving mobile forms                                         *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/

   PROCEDURE GET_PO_LOV (x_po                   OUT NOCOPY t_genref,
                         p_organization_id   IN            VARCHAR2,
                         p_po_number         IN            VARCHAR2)
   IS
      l_organization_id   NUMBER;

      l_instr_check       NUMBER;
   BEGIN
      g_call_from := 'GET_PO_LOV';
      g_call_point := 'Start';

      debug_log ('p_organization_id ' || p_organization_id);
      debug_log ('po_number ' || p_po_number);


      /*BEGIN
        SELECT instr(p_po_number,'%',1,1)
        INTO   l_instr_check
        FROM   dual;
      END;

      IF l_instr_check IN (1,2,3) THEN
        raise g_exception;
      END IF;
    */

      BEGIN
         SELECT TO_NUMBER (p_organization_id)
           INTO l_organization_id
           FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Could not conver organization id to number ';
            RAISE g_exception;
      END;

      g_call_point := 'Getting list of po numbers';


      --Added 5/12/2015 to prevent open cursors
      IF x_po%ISOPEN
      THEN
         debug_log ('closing x_po cursor');

         CLOSE x_po;
      END IF;

      OPEN x_po FOR
           SELECT pha.po_header_id,
                  pha.segment1 po_number,
                  pha.note_to_receiver,
                  TO_CHAR (TRUNC (FND_DATE.CANONICAL_TO_DATE (pha.attribute1)),
                           'MM/DD/YYYY')
                     need_by_date,
                  ass.segment1 vendor_number,
                  ass.vendor_name,
                  ass.vendor_id,
                  pha.vendor_site_id
             FROM po_headers pha, ap_suppliers ass
            WHERE     pha.TYPE_LOOKUP_CODE IN ('STANDARD',
                                               'PLANNED',
                                               'BLANKET',
                                               'CONTRACT')
                  AND NVL (pha.CANCEL_FLAG, 'N') IN ('N', 'I')
                  AND NVL (pha.CLOSED_CODE, 'OPEN') NOT IN ('FINALLY CLOSED',
                                                            'CLOSED FOR RECEIVING',
                                                            'CLOSED')
                  AND pha.vendor_id = ass.vendor_id
                  --AND    pha.segment1 like p_po_number --removed 4/20/2015
                  AND pha.segment1 = p_po_number             --added 4/20/2015
                  --Shipments
                  AND EXISTS
                         (SELECT *
                            FROM po_line_locations plla
                           WHERE     pha.po_header_id = plla.po_header_id
                                 AND NVL (plla.approved_flag, 'N') = 'Y'
                                 AND NVL (plla.cancel_flag, 'N') = 'N'
                                 AND NVL (plla.closed_code, 'OPEN') NOT IN ('FINALLY CLOSED',
                                                                            'CLOSED FOR RECEIVING',
                                                                            'CLOSED')
                                 AND plla.ship_to_organization_id =
                                        l_organization_id
                                 AND plla.shipment_type IN ('STANDARD',
                                                            'BLANKET',
                                                            'SCHEDULED'))
         ORDER BY DECODE (RTRIM (pha.segment1, '0123456789'),
                          NULL, NULL,
                          pha.segment1),
                  DECODE (RTRIM (pha.segment1, '0123456789'),
                          NULL, TO_NUMBER (pha.segment1),
                          NULL);
   EXCEPTION
      WHEN g_exception
      THEN
         debug_log (g_message || g_sqlcode || g_sqlerrm);

         --Added 5/12/2015 to prevent open cursors
         IF x_po%ISOPEN
         THEN
            debug_log ('closing x_po cursor');

            CLOSE x_po;
         END IF;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
      WHEN OTHERS
      THEN
         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message := 'when others error ';
         debug_log (g_message || g_sqlcode || g_sqlerrm);

         --Added 5/12/2015 to prevent open cursors
         IF x_po%ISOPEN
         THEN
            debug_log ('closing x_po cursor');

            CLOSE x_po;
         END IF;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END GET_PO_LOV;


   /*****************************************************************************************************************************************
   *   PROCEDURE GET_ITEM_LOV                                                                                                               *
   *                                                                                                                                        *
   *   PURPOSE:   This function is used to get the default open quantity for receiving mobile forms                                         *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/

   PROCEDURE GET_ITEM_LOV (x_item                       OUT NOCOPY t_genref --0
                                                                           ,
                           p_organization_id         IN            VARCHAR2 --1
                                                                           ,
                           p_item                    IN            VARCHAR2 --2
                                                                           ,
                           P_TRANSACTION_TYPE        IN            VARCHAR2 --3
                                                                           ,
                           P_PO_HEADER_ID            IN            VARCHAR2 --4
                                                                           ,
                           P_REQUISITION_HEADER_ID   IN            VARCHAR2 --5
                                                                           ,
                           P_SHIPMENT_HEADER_ID      IN            VARCHAR2 --6
                                                                           ,
                           P_ORDER_HEADER_ID         IN            VARCHAR2 --7
                                                                           ,
                           p_ship_from_org_id        IN            VARCHAR2 --8
                                                                           ,
                           p_group_id                IN            VARCHAR2 --9
                                                                           )
   IS
      l_organization_id         NUMBER;
      l_po_header_id            NUMBER;
      l_requisition_header_id   NUMBER;
      l_shipment_header_id      NUMBER;
      l_order_header_id         NUMBER;
      l_ship_from_org_id        NUMBER;
      l_group_id                NUMBER;
   BEGIN
      g_call_from := 'GET_ITEM_LOV';
      g_call_point := 'Start';

      debug_log ('p_organization_id ' || p_organization_id);
      debug_log ('p_item ' || p_item);
      debug_log ('p_transaction_type ' || p_transaction_type);
      debug_log ('p_po_header_id ' || p_po_header_id);
      debug_log ('p_requisition_header_id ' || p_requisition_header_id);
      debug_log ('p_shipment_header_id ' || p_shipment_header_id);
      debug_log ('p_order_header_id ' || p_order_header_id);
      debug_log ('p_ship_from_org_id ' || p_ship_from_org_id);
      debug_log ('p_group_id ' || p_group_id);


      g_call_point := 'Converting parameters from varchar2 to numbers';


      BEGIN
         g_call_point := 'Converting Organization Id numbers';

         SELECT TO_NUMBER (p_organization_id)
           INTO l_organization_id
           FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_po_header_id := NULL;
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message :=
                  'When others getting l_organization_id '
               || g_call_point
               || g_sqlcode
               || g_sqlerrm;
            debug_log (g_message);
            RAISE g_exception;
      END;


      BEGIN
         g_call_point := 'Converting PO Header to numbers';

         SELECT TO_NUMBER (p_po_header_id) INTO l_po_header_id FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_po_header_id := NULL;
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message :=
                  'When others getting l_po_header_id '
               || g_call_point
               || g_sqlcode
               || g_sqlerrm;
            debug_log (g_message);
            RAISE g_exception;
      END;


      BEGIN
         g_call_point := 'Converting Requisition Header to numbers';

         SELECT TO_NUMBER (p_requisition_header_id)
           INTO l_requisition_header_id
           FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_requisition_header_id := NULL;
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message :=
                  'When others getting l_requisition_header_id '
               || g_call_point
               || g_sqlcode
               || g_sqlerrm;
            debug_log (g_message);
            RAISE g_exception;
      END;


      BEGIN
         g_call_point := 'Converting Shipment to numbers';

         SELECT TO_NUMBER (p_shipment_header_id)
           INTO l_shipment_header_id
           FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_shipment_header_id := NULL;
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message :=
                  'When others getting l_shipment_header_id '
               || g_call_point
               || g_sqlcode
               || g_sqlerrm;
            debug_log (g_message);
            RAISE g_exception;
      END;

      BEGIN
         g_call_point := 'Converting Order Header to numbers';

         SELECT TO_NUMBER (p_order_header_id)
           INTO l_order_header_id
           FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_order_header_id := NULL;
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message :=
                  'When others getting l_order_header_id '
               || g_call_point
               || g_sqlcode
               || g_sqlerrm;
            debug_log (g_message);
            RAISE g_exception;
      END;


      BEGIN
         g_call_point := 'Converting Ship From Org Id to numbers';

         SELECT TO_NUMBER (p_ship_from_org_id)
           INTO l_ship_from_org_id
           FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_po_header_id := NULL;
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message :=
                  'When others getting l_ship_from_org_id '
               || g_call_point
               || g_sqlcode
               || g_sqlerrm;
            debug_log (g_message);
            RAISE g_exception;
      END;

      BEGIN
         g_call_point := 'Converting Ship From Org Id to numbers';

         SELECT TO_NUMBER (p_group_id) INTO l_group_id FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_po_header_id := NULL;
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message :=
                  'When others getting l_group_id '
               || g_call_point
               || g_sqlcode
               || g_sqlerrm;
            debug_log (g_message);
            RAISE g_exception;
      END;

      debug_log ('l_organization_id ' || l_organization_id);
      debug_log ('l_po_header_id ' || l_po_header_id);
      debug_log ('l_requisition_header_id ' || l_requisition_header_id);
      debug_log ('l_shipment_header_id ' || l_shipment_header_id);
      debug_log ('l_order_header_id ' || l_order_header_id);
      debug_log ('l_ship_from_org_id ' || l_ship_from_org_id);
      debug_log ('l_group_id ' || l_group_id);

      --Added 5/12/2015 to prevent open cursors
      IF x_item%ISOPEN
      THEN
         debug_log ('closing x_item cursor');

         CLOSE x_item;
      END IF;


      IF p_transaction_type = 'RECEIVE' OR p_transaction_type = 'RTV'
      THEN
         --PO Receive transaction
         IF l_po_header_id IS NOT NULL OR l_po_header_id != ''
         THEN
            OPEN x_item FOR
                 SELECT item.inventory_item_id,
                        item.concatenated_segments,
                        GREATEST (  SUM (ms.to_org_primary_quantity)
                                  - NVL (xxwc_rcv_mob_pkg.get_rcv_total (
                                            l_group_id,
                                            item.inventory_item_id,
                                            l_organization_id,
                                            p_transaction_type),
                                         0),
                                  0),
                        NVL (xxwc_rcv_mob_pkg.get_rcv_total (
                                l_group_id,
                                item.inventory_item_id,
                                l_organization_id,
                                p_transaction_type),
                             0),
                        item.primary_uom_code,
                        item.description,
                        item.lot_control_code,
                        item.serial_number_control_code,
                        item.shelf_life_days
                   FROM mtl_supply ms,
                        (SELECT msi.inventory_item_id,
                                msi.concatenated_segments,
                                msi.primary_uom_code,
                                msi.description,
                                msi.lot_control_code,
                                msi.serial_number_control_code,
                                msi.shelf_life_days
                           FROM mtl_system_items_kfv msi
                          WHERE     msi.organization_id = l_organization_id
                                AND msi.concatenated_segments LIKE
                                       NVL (UPPER (p_item),
                                            msi.concatenated_segments) --removed 4/20/2015
                         --AND    msi.concatenated_segments = upper(p_item) --added 4/20/2015
                         UNION
                         SELECT msi.inventory_item_id,
                                msi.concatenated_segments,
                                msi.primary_uom_code,
                                msi.description,
                                msi.lot_control_code,
                                msi.serial_number_control_code,
                                msi.shelf_life_days
                           FROM mtl_system_items_kfv msi
                          WHERE     msi.organization_id = l_organization_id
                                AND EXISTS
                                       (SELECT mcr.inventory_item_id
                                          FROM mtl_cross_references mcr
                                         WHERE     1 = 1 --msi.organization_id = fnd_profile.VALUE('XXWC_ITEM_MASTER_ORG')
                                               --AND mcr.cross_reference_type = fnd_profile.VALUE('INV:GTIN_CROSS_REFERENCE_TYPE') removed 5/12/2015
                                               AND mcr.organization_id IS NULL
                                               AND mcr.inventory_item_id =
                                                      msi.inventory_item_id
                                               AND mcr.cross_reference LIKE
                                                      NVL (p_item,
                                                           mcr.cross_reference) --removed 4/20/2015
                                               AND EXISTS
                                                      (SELECT mcrt.cross_reference_type --added exists cross reference type 5/12/2015 to include multiple cross reference types
                                                         FROM mtl_cross_reference_types mcrt
                                                        WHERE     NVL (
                                                                     mcrt.attribute1,
                                                                     'N') = 'Y'
                                                              AND mcrt.cross_reference_type =
                                                                     mcr.cross_reference_type))) --added 4/20/2015
                        item
                  --AND mcr.cross_reference = upper(p_item))) item
                  WHERE     ms.to_organization_id = l_organization_id
                        AND ms.po_header_id =
                               NVL (l_po_header_id, ms.po_header_id)
                        AND ms.supply_type_code = 'PO'
                        AND ms.item_id = item.inventory_item_id
               GROUP BY item.inventory_item_id,
                        item.concatenated_segments,
                        item.description,
                        item.lot_control_code,
                        item.serial_number_control_code,
                        item.shelf_life_days,
                        item.primary_uom_code
               ORDER BY item.concatenated_segments;
         END IF;

         /*
          --Internal Shipment Transaction
         IF l_shipment_header_id IS NOT NULL AND l_inventory_item_id IS NOT NULL THEN

           BEGIN
               SELECT ms.to_org_primary_uom, sum(ms.to_org_primary_quantity)
               INTO   l_order_uom, l_order_qty
               FROM   mtl_supply ms
               WHERE  ms.to_organization_id = l_organization_id
               AND    ms.item_id = nvl(l_inventory_item_id,ms.item_id)
               AND    ms.shipment_header_id = nvl(l_shipment_header_id,ms.shipment_header_id)
               AND    ms.supply_type_code = 'SHIPMENT'
             GROUP BY ms.to_org_primary_uom;
            EXCEPTION
               WHEN OTHERS THEN
                     l_order_qty := 0;
                     l_order_uom := l_item_uom;
           END;


         END IF;
         */

         --Requisition Transaction
         IF    l_requisition_header_id IS NOT NULL
            OR l_requisition_header_id != ''
         THEN
            OPEN x_item FOR
                 SELECT item.inventory_item_id,
                        item.concatenated_segments,
                          SUM (ms.to_org_primary_quantity)
                        - NVL (xxwc_rcv_mob_pkg.get_rcv_total (
                                  l_group_id,
                                  item.inventory_item_id,
                                  l_organization_id,
                                  p_transaction_type),
                               0),
                        NVL (xxwc_rcv_mob_pkg.get_rcv_total (
                                l_group_id,
                                item.inventory_item_id,
                                l_organization_id,
                                p_transaction_type),
                             0),
                        item.primary_uom_code,
                        item.description,
                        item.lot_control_code,
                        item.serial_number_control_code,
                        item.shelf_life_days
                   FROM mtl_supply ms,
                        (SELECT msi.inventory_item_id,
                                msi.concatenated_segments,
                                msi.primary_uom_code,
                                msi.description,
                                msi.lot_control_code,
                                msi.serial_number_control_code,
                                msi.shelf_life_days
                           FROM mtl_system_items_kfv msi
                          WHERE     msi.organization_id = l_organization_id
                                AND msi.concatenated_segments LIKE
                                       NVL (UPPER (p_item),
                                            msi.concatenated_segments) --removed 4/20/2015
                         --AND    msi.concatenated_segments = upper(p_item) --added 4/20/2015
                         UNION
                         SELECT msi.inventory_item_id,
                                msi.concatenated_segments,
                                msi.primary_uom_code,
                                msi.description,
                                msi.lot_control_code,
                                msi.serial_number_control_code,
                                msi.shelf_life_days
                           FROM mtl_system_items_kfv msi
                          WHERE     msi.organization_id = l_organization_id
                                AND EXISTS
                                       (SELECT mcr.inventory_item_id
                                          FROM mtl_cross_references mcr
                                         WHERE     1 = 1 --msi.organization_id = fnd_profile.VALUE('XXWC_ITEM_MASTER_ORG')
                                               --AND mcr.cross_reference_type = fnd_profile.VALUE('INV:GTIN_CROSS_REFERENCE_TYPE') removed 5/12/2015
                                               AND mcr.organization_id IS NULL
                                               AND mcr.inventory_item_id =
                                                      msi.inventory_item_id
                                               AND mcr.cross_reference LIKE
                                                      NVL (p_item,
                                                           mcr.cross_reference) --removed 4/20/2015
                                               AND EXISTS
                                                      (SELECT mcrt.cross_reference_type --added exists cross reference type 5/12/2015 to include multiple cross reference types
                                                         FROM mtl_cross_reference_types mcrt
                                                        WHERE     NVL (
                                                                     mcrt.attribute1,
                                                                     'N') = 'Y'
                                                              AND mcrt.cross_reference_type =
                                                                     mcr.cross_reference_type))) --added 4/20/2015
                        item
                  WHERE     ms.to_organization_id = l_organization_id
                        AND ms.req_header_id =
                               NVL (l_requisition_header_id, ms.req_header_id)
                        AND ms.supply_type_code = 'SHIPMENT'
                        AND ms.item_id = item.inventory_item_id
                        AND ms.from_organization_id =
                               NVL (l_ship_from_org_id,
                                    ms.from_organization_id)
               GROUP BY item.inventory_item_id,
                        item.concatenated_segments,
                        item.description,
                        item.lot_control_code,
                        item.serial_number_control_code,
                        item.shelf_life_days,
                        item.primary_uom_code
               ORDER BY item.concatenated_segments;
         END IF;



         --RMA Transaction
         IF l_order_header_id IS NOT NULL
         THEN
            OPEN x_item FOR
                 SELECT item.inventory_item_id,
                        item.concatenated_segments,
                          SUM (NVL (oola.ordered_quantity, 0))
                        - SUM (NVL (oola.cancelled_quantity, 0))
                        - SUM (NVL (oola.shipped_quantity, 0))
                        - NVL (xxwc_rcv_mob_pkg.get_rcv_total (
                                  l_group_id,
                                  item.inventory_item_id,
                                  l_organization_id,
                                  p_transaction_type),
                               0),
                        NVL (xxwc_rcv_mob_pkg.get_rcv_total (
                                l_group_id,
                                item.inventory_item_id,
                                l_organization_id,
                                p_transaction_type),
                             0),
                        item.primary_uom_code,
                        item.description,
                        item.lot_control_code,
                        item.serial_number_control_code,
                        item.shelf_life_days
                   FROM oe_order_lines oola,
                        (SELECT msi.inventory_item_id,
                                msi.concatenated_segments,
                                msi.primary_uom_code,
                                msi.description,
                                msi.lot_control_code,
                                msi.serial_number_control_code,
                                msi.shelf_life_days
                           FROM mtl_system_items_kfv msi
                          WHERE     msi.organization_id = l_organization_id
                                AND msi.concatenated_segments LIKE
                                       NVL (UPPER (p_item),
                                            msi.concatenated_segments) --removed 4/20/2015
                         --AND    msi.concatenated_segments = upper(p_item) --added 4/20/2015
                         UNION
                         SELECT msi.inventory_item_id,
                                msi.concatenated_segments,
                                msi.primary_uom_code,
                                msi.description,
                                msi.lot_control_code,
                                msi.serial_number_control_code,
                                msi.shelf_life_days
                           FROM mtl_system_items_kfv msi
                          WHERE     msi.organization_id = l_organization_id
                                AND EXISTS
                                       (SELECT mcr.inventory_item_id
                                          FROM mtl_cross_references mcr
                                         WHERE     1 = 1 --msi.organization_id = fnd_profile.VALUE('XXWC_ITEM_MASTER_ORG')
                                               --AND mcr.cross_reference_type = fnd_profile.VALUE('INV:GTIN_CROSS_REFERENCE_TYPE') removed 5/12/2015
                                               AND mcr.organization_id IS NULL
                                               AND mcr.inventory_item_id =
                                                      msi.inventory_item_id
                                               AND mcr.cross_reference LIKE
                                                      NVL (p_item,
                                                           mcr.cross_reference) --removed 4/20/2015
                                               AND EXISTS
                                                      (SELECT mcrt.cross_reference_type --added exists cross reference type 5/12/2015 to include multiple cross reference types
                                                         FROM mtl_cross_reference_types mcrt
                                                        WHERE     NVL (
                                                                     mcrt.attribute1,
                                                                     'N') = 'Y'
                                                              AND mcrt.cross_reference_type =
                                                                     mcr.cross_reference_type))) --added 4/20/2015
                        item
                  WHERE     oola.ship_from_org_id = l_organization_id
                        AND oola.header_id =
                               NVL (l_order_header_id, oola.header_id)
                        AND oola.inventory_item_id = item.inventory_item_id
                        AND oola.ship_from_org_id = l_organization_id
                        AND NVL (oola.cancelled_flag, 'N') = 'N'
                        AND NVL (oola.booked_flag, 'N') = 'Y'
                        AND NVL (oola.open_flag, 'N') = 'Y'
                        AND oola.flow_status_code NOT IN ('CLOSED', 'CANCELLED')
                        AND EXISTS
                               (SELECT *
                                  FROM oe_transaction_types_tl ottt2,
                                       oe_transaction_types otta2
                                 WHERE     ottt2.transaction_type_id =
                                              oola.line_type_id
                                       AND ottt2.NAME = 'RETURN WITH RECEIPT'
                                       AND ottt2.SOURCE_LANG = g_language
                                       AND otta2.transaction_type_id =
                                              ottt2.transaction_type_id
                                       AND otta2.transaction_type_code = 'LINE'
                                       AND otta2.order_category_code =
                                              oola.line_category_code)
               GROUP BY item.inventory_item_id,
                        item.concatenated_segments,
                        item.description,
                        item.lot_control_code,
                        item.serial_number_control_code,
                        item.shelf_life_days,
                        item.primary_uom_code
               ORDER BY item.concatenated_segments;
         END IF;
      END IF;
   EXCEPTION
      WHEN g_exception
      THEN
         debug_log (g_message || g_sqlcode || g_sqlerrm);


         --Added 5/12/2015 to prevent open cursors
         IF x_item%ISOPEN
         THEN
            debug_log ('closing x_item cursor');

            CLOSE x_item;
         END IF;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
      WHEN OTHERS
      THEN
         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message := 'when others error ';
         debug_log (g_message || g_sqlcode || g_sqlerrm);



         --Added 5/12/2015 to prevent open cursors
         IF x_item%ISOPEN
         THEN
            debug_log ('closing x_item cursor');

            CLOSE x_item;
         END IF;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END GET_ITEM_LOV;


   /*****************************************************************************************************************************************
   *   PROCEDURE validate_rcv_qty_val                                                                                                       *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to validate the receiving qty value                                                                *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/

   PROCEDURE validate_rcv_qty_val (p_qty           IN     VARCHAR2,
                                   p_running_qty   IN     VARCHAR2,
                                   x_return           OUT NUMBER,
                                   x_message          OUT VARCHAR2)
   IS
      l_qty           NUMBER;
      l_running_qty   NUMBER;
      l_return        NUMBER DEFAULT 0;
   BEGIN
      g_call_from := 'validate_rcv_qty_val';
      g_call_point := 'Start';

      debug_log ('p_qty ' || p_qty);
      debug_log ('p_running_qty ' || p_running_qty);


      g_call_point := 'Converting parameters from varchar2 to numbers';

      BEGIN
         SELECT NVL (TO_NUMBER (p_qty), 0) INTO l_qty FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Could not convert ' || p_qty || ' to number';
            RAISE g_exception;
      END;

      g_call_point := 'Converting parameters from varchar2 to numbers';

      BEGIN
         SELECT NVL (TO_NUMBER (p_running_qty), 0)
           INTO l_running_qty
           FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Could not convert ' || p_running_qty || ' to number';
            RAISE g_exception;
      END;


      IF NVL (l_qty, 0) + NVL (l_running_qty, 0) < 0
      THEN
         l_return := 1;
         g_message := 'Value must be greater than or equal to 0';
      ELSE
         l_return := 0;
         g_message := 'Success';
      END IF;

      debug_log ('x_return ' || l_return);
      debug_log ('x_message ' || g_message);


      x_return := l_return;
      x_message := g_message;
   EXCEPTION
      WHEN g_exception
      THEN
         x_return := 1;
         x_message := g_message;

         debug_log ('x_return ' || l_return);
         debug_log ('x_message ' || g_message);
      WHEN OTHERS
      THEN
         x_return := 1;

         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message := 'when others error ' || g_sqlcode || g_sqlerrm;
         debug_log (g_message || g_sqlcode || g_sqlerrm);

         x_return := 1;
         x_message := g_message;

         debug_log ('x_return ' || l_return);
         debug_log ('x_message ' || g_message);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END validate_rcv_qty_val;

   /*****************************************************************************************************************************************
   *   PROCEDURE INSERT_INTO_RCV_INTERFACE                                                                                                  *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to insert records into the receiving interface table                                               *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/

   PROCEDURE INSERT_INTO_RCV_INTERFACE (
      P_GROUP_ID            IN     NUMBER                                  --1
                                         ,
      P_ORGANIZATION_ID     IN     NUMBER                                  --2
                                         ,
      P_INVENTORY_ITEM_ID   IN     NUMBER                                  --3
                                         ,
      P_ITEM_DESCRIPTION    IN     VARCHAR2                                --4
                                           ,
      P_UOM_CODE            IN     VARCHAR2                                --5
                                           ,
      P_QUANTITY            IN     NUMBER                                  --6
                                         ,
      P_SOURCE_HEADER_ID    IN     NUMBER                                  --7
                                         ,
      P_SUBINVENTORY_CODE   IN     VARCHAR2                                --8
                                           ,
      P_LOCATOR_ID          IN     NUMBER                                  --9
                                         ,
      P_PROJECT_ID          IN     NUMBER                                 --10
                                         ,
      P_TASK_ID             IN     NUMBER                                 --11
                                         ,
      P_TRANSACTION_TYPE    IN     VARCHAR2                               --12
                                           ,
      P_DOCTYPE             IN     VARCHAR2                               --13
                                           ,
      P_SUPPRESS_TRAVELER   IN     VARCHAR2                               --14
                                           ,
      P_USER_ID             IN     NUMBER                                 --15
                                         ,
      P_VENDOR_ID           IN     NUMBER                                 --16
                                         ,
      P_VENDOR_SITE_ID      IN     NUMBER                                 --17
                                         ,
      X_RETURN                 OUT NUMBER                                 --18
                                         ,
      X_MESSAGE                OUT VARCHAR2)                              --19
   IS
      l_rcv_group_id                  NUMBER;
      l_header_interface_id           NUMBER;
      l_header_exists                 VARCHAR2 (1);
      l_employee_id                   NUMBER;
      l_shipment_num                  NUMBER;
      l_shipment_header_id            NUMBER;
      l_shipment_line_id              NUMBER;
      l_to_organization_id            NUMBER := p_organization_id;
      l_from_organization_id          NUMBER;
      l_expected_receipts_date        DATE;
      l_shipped_date                  DATE;
      l_category_id                   NUMBER;
      l_receipt_source_code           VARCHAR2 (25);
      l_unit_of_measure               VARCHAR2 (25);
      l_po_header_id                  NUMBER;
      l_po_line_id                    NUMBER;
      l_po_line_location_id           NUMBER;
      l_po_distribution_id            NUMBER;
      l_receiving_routing_id          NUMBER;
      l_unit_price                    NUMBER;
      l_currency_code                 VARCHAR2 (25);
      l_ship_to_location_id           NUMBER;
      l_deliver_to_person_id          NUMBER;
      l_project_id                    NUMBER;
      l_task_id                       NUMBER;
      l_requisition_header_id         NUMBER;
      l_requisition_line_id           NUMBER;
      l_requisition_distribution_id   NUMBER;
      l_oe_order_header_id            NUMBER;
      l_oe_order_line_id              NUMBER;
      l_customer_id                   NUMBER;
      l_customer_site_id              NUMBER;
      l_rcv_tp_interface              VARCHAR2 (50);
      l_routing_step_id               NUMBER;
      l_processing_request_id         NUMBER;
      l_next_receipt_num              NUMBER;
      l_reason_id                     NUMBER;
      l_transaction_interface_id      NUMBER;
      l_primary_quantity              NUMBER;
      l_quantity                      NUMBER;
   BEGIN
      g_call_from := 'INSERT_INTO_RCV_INTERFACE';
      g_call_point := 'Start';

      debug_log ('P_GROUP_ID ' || p_group_Id);
      debug_log ('P_ORGANIZATION_ID ' || P_ORGANIZATION_ID);
      debug_log ('P_INVENTORY_ITEM_ID ' || P_INVENTORY_ITEM_ID);
      debug_log ('P_UOM_CODE ' || P_UOM_CODE);
      debug_log ('P_QUANTITY ' || P_QUANTITY);
      debug_log ('P_SOURCE_HEADER_ID ' || P_SOURCE_HEADER_ID);
      debug_log ('P_SUBINVENTORY_CODE ' || P_SUBINVENTORY_CODE);
      debug_log ('P_LOCATOR_ID ' || P_LOCATOR_ID);
      debug_log ('P_PROJECT_ID ' || P_PROJECT_ID);
      debug_log ('P_TASK_ID ' || P_TASK_ID);
      debug_log ('P_TRANSACTION_TYPE ' || P_TRANSACTION_TYPE);
      debug_log ('P_DOCTYPE ' || P_DOCTYPE);
      debug_log ('P_SUPPRESS_TRAVELER ' || P_SUPPRESS_TRAVELER);
      debug_log ('P_USER_ID ' || P_USER_ID);


      IF p_inventory_item_id IS NOT NULL
      THEN
         g_call_point := 'RCV Interface transaction mode';

         BEGIN
            SELECT fnd_profile.VALUE ('RCV_TP_MODE')
              INTO l_rcv_tp_interface
              FROM DUAL;
         EXCEPTION
            WHEN OTHERS
            THEN
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message :=
                  'When others receiving interface transaction mode ';
               debug_log (g_message);
               RAISE g_exception;
         END;


         debug_log ('l_rcv_tp_interface ' || l_rcv_tp_interface);

         IF l_rcv_tp_interface = 'ONLINE'
         THEN
            l_processing_request_id := 0;
         ELSE
            l_processing_request_id := NULL;
         END IF;

         debug_log ('l_processing_request_id ' || l_processing_request_id);

         g_call_point := 'rcv header';

         --Find the receipt header on the headers interface table if group_id is tied to one already

         BEGIN
            SELECT header_interface_id
              INTO l_header_interface_id
              FROM rcv_headers_interface
             WHERE GROUP_ID = p_group_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_header_interface_id := NULL;
            WHEN OTHERS
            THEN
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message := 'When others getting header interface id ';
               debug_log (g_message);
               RAISE g_exception;
         END;

         IF l_header_interface_id IS NULL
         THEN
            g_call_point := 'getting interface header';

            BEGIN
               SELECT rcv_headers_interface_s.NEXTVAL
                 INTO l_header_interface_id
                 FROM DUAL;
            EXCEPTION
               WHEN OTHERS
               THEN
                  g_sqlcode := SQLCODE;
                  g_sqlerrm := SQLERRM;
                  g_message := 'When others getting header interface id ';
                  debug_log (g_message);
                  RAISE g_exception;
            END;

            l_header_exists := 'N';
         ELSE
            l_header_exists := 'Y';
         END IF;


         debug_log ('l_header_interface_id ' || l_header_interface_id);
         debug_log ('l_header_exists ' || l_header_exists);


         g_call_point := 'Get employee_id ';

         BEGIN
            SELECT employee_id
              INTO l_employee_id
              FROM fnd_user
             WHERE user_id = p_user_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_employee_id := NULL;
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message := 'When others getting employee_id ';
               debug_log (g_message);
               RAISE g_exception;
         END;


         g_call_point := 'Get categorey_id ';

         BEGIN
            SELECT mic.category_id
              INTO l_category_id
              FROM MTL_DEFAULT_CATEGORY_SETS mdcs, MTL_ITEM_CATEGORIES MIC
             WHERE     mdcs.functional_area_id = 2
                   AND mic.category_set_id = mdcs.category_set_id
                   AND mic.organization_Id = p_organization_id
                   AND mic.inventory_item_id = p_inventory_item_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_category_id := NULL;
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message := 'When others getting category_id ';
               debug_log (g_message);
               RAISE g_exception;
         END;

         debug_log ('category_id ' || l_category_id);


         g_call_point := 'Get receipt source code ';


         BEGIN
            SELECT DECODE (P_DOCTYPE,
                           'PO', 'VENDOR',
                           'REQ', 'INTERNAL ORDER',
                           'RMA', 'CUSTOMER')
              INTO l_receipt_source_code
              FROM DUAL;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_receipt_source_code := NULL;
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message := 'Error getting receipt source code';
               RAISE g_exception;
         END;


         g_call_point := 'Get receipt source code ';

         BEGIN
            SELECT unit_of_measure
              INTO l_unit_of_measure
              FROM mtl_units_of_measure
             WHERE uom_code = p_uom_code;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_unit_of_measure := NULL;
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message := 'Error getting unit of measure for ' || p_uom_code;
               RAISE g_exception;
         END;


         IF p_doctype = 'PO'
         THEN
            MO_GLOBAL.INIT ('PO');

            g_call_point := 'Getting po details';

            BEGIN
               SELECT po_header_id,
                      po_line_id,
                      po_line_location_id,
                      po_distribution_id
                 INTO l_po_header_id,
                      l_po_line_id,
                      l_po_line_location_id,
                      l_po_distribution_id
                 FROM mtl_supply
                WHERE     po_header_id = p_source_header_id
                      AND item_id = p_inventory_item_id
                      AND to_organization_id = p_organization_id
                      AND supply_type_code = 'PO'
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  g_sqlcode := SQLCODE;
                  g_sqlerrm := SQLERRM;
                  g_message := 'Error getting po details';
                  RAISE g_exception;
            END;

            g_call_point := 'Getting PO Header currency information';

            BEGIN
               SELECT currency_code
                 INTO l_currency_code
                 FROM po_headers
                WHERE po_header_id = l_po_header_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  g_sqlcode := SQLCODE;
                  g_sqlerrm := SQLERRM;
                  g_message :=
                        'Error getting currency information '
                     || g_sqlcode
                     || g_sqlerrm;
                  RAISE g_exception;
            END;

            g_call_point := 'Getting PO Line Price information';

            BEGIN
               SELECT unit_price
                 INTO l_unit_price
                 FROM po_lines
                WHERE po_line_id = l_po_line_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  g_sqlcode := SQLCODE;
                  g_sqlerrm := SQLERRM;
                  g_message :=
                        'Error getting po price information '
                     || g_sqlcode
                     || g_sqlerrm;
                  RAISE g_exception;
            END;

            g_call_point :=
               'Getting PO Line Location Date and Receiving Routing information';

            BEGIN
               SELECT need_by_date, receiving_routing_id, ship_to_location_id
                 INTO l_expected_receipts_date,
                      l_receiving_routing_id,
                      l_ship_to_location_id
                 FROM po_line_locations
                WHERE line_location_id = l_po_line_location_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  g_sqlcode := SQLCODE;
                  g_sqlerrm := SQLERRM;
                  g_message :=
                        'Error getting po date and receiving routing information '
                     || g_sqlcode
                     || g_sqlerrm;
                  RAISE g_exception;
            END;

            g_call_point :=
               'Getting PO Distributions Delivery to Informaiton information';

            BEGIN
               SELECT deliver_to_person_id, project_id, task_id
                 INTO l_deliver_to_person_id, l_project_id, l_task_id
                 FROM po_distributions
                WHERE po_distribution_id = l_po_distribution_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  g_sqlcode := SQLCODE;
                  g_sqlerrm := SQLERRM;
                  g_message :=
                        'Error getting po date and receiving routing information '
                     || g_sqlcode
                     || g_sqlerrm;
                  RAISE g_exception;
            END;
         END IF;


         IF p_doctype = 'REQ'
         THEN
            g_call_point := 'Getting Req Shipment Information information';

            BEGIN
               SELECT shipment_header_id,
                      shipment_line_id,
                      req_header_id,
                      req_line_id,
                      from_organization_id
                 INTO l_shipment_header_id,
                      l_shipment_line_id,
                      l_requisition_header_id,
                      l_requisition_line_id,
                      l_from_organization_id
                 FROM mtl_supply
                WHERE     shipment_header_id = p_source_header_id
                      AND item_id = p_inventory_item_id
                      AND to_organization_id = p_organization_id
                      AND supply_type_code = 'SHIPMENT'
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  g_sqlcode := SQLCODE;
                  g_sqlerrm := SQLERRM;
                  g_message := 'Error getting req details';
                  RAISE g_exception;
            END;


            g_call_point := 'Getting Req Shipment Details';

            BEGIN
               SELECT rsh.shipment_num,
                      rsh.expected_receipt_date,
                      rsl.req_distribution_id,
                      rsl.routing_header_id,
                      rsl.deliver_to_person_id,
                      rsl.ship_to_location_id
                 INTO l_shipment_num,
                      l_expected_receipts_date,
                      l_requisition_distribution_id,
                      l_receiving_routing_id,
                      l_deliver_to_person_id,
                      l_ship_to_location_id
                 FROM rcv_shipment_lines rsl, rcv_shipment_headers rsh
                WHERE     rsl.shipment_line_id = l_shipment_line_id
                      AND rsl.shipment_header_id = l_shipment_header_id
                      AND rsl.requisition_line_id = l_requisition_line_id
                      AND rsl.shipment_header_id = rsh.shipment_header_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  g_sqlcode := SQLCODE;
                  g_sqlerrm := SQLERRM;
                  g_message := 'Error getting shipment details';
                  RAISE g_exception;
            END;
         END IF;

         IF p_doctype = 'RMA'
         THEN
            MO_GLOBAL.INIT ('ONT');

            g_call_point := 'Getting Order Information';

            BEGIN
               SELECT ooha.header_id,
                      oola.line_id,
                      oola.sold_to_org_id,
                      oola.ship_to_org_id
                 INTO l_oe_order_header_id,
                      l_oe_order_line_id,
                      l_customer_id,
                      l_customer_site_id
                 FROM oe_order_headers ooha, oe_order_lines oola
                WHERE     ooha.header_id = p_source_header_id
                      AND oola.inventory_item_id = p_inventory_item_id
                      AND ooha.header_id = oola.header_id
                      AND oola.ship_from_org_id = p_organization_id
                      AND NVL (ooha.open_flag, 'N') = 'Y'
                      AND NVL (ooha.cancelled_flag, 'N') = 'N'
                      AND NVL (oola.open_flag, 'N') = 'Y'
                      AND NVL (oola.cancelled_flag, 'N') = 'N'
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  g_sqlcode := SQLCODE;
                  g_sqlerrm := SQLERRM;
                  g_message := 'Error getting rma details';
                  RAISE g_exception;
            END;
         END IF;


         g_call_point := 'Getting Routing Step';

         IF p_transaction_type = 'RECEIVE'
         THEN
            l_routing_step_id := 1;
         ELSIF p_transaction_type = 'INSPECT'
         THEN
            l_routing_step_id := 2;
         ELSIF p_transaction_type = 'DELIVER'
         THEN
            l_routing_step_id := 3;
         ELSE
            l_routing_step_id := NULL;
         END IF;

         debug_log ('l_routing_step_id ' || l_routing_step_id);

         g_call_point := 'Get Next Receipt Num';

         BEGIN
            SELECT next_receipt_num
              INTO l_next_receipt_num
              FROM rcv_parameters
             WHERE organization_Id = p_organization_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message := 'Error getting next receipt num';
               RAISE g_exception;
         END;

         BEGIN
            SELECT reason_id
              INTO l_reason_id
              FROM MTL_TRANSACTION_REASONS
             WHERE reason_name = '15 - RF';
         EXCEPTION
            WHEN OTHERS
            THEN
               l_reason_id := NULL;
         END;

         IF l_header_exists = 'N'
         THEN
            g_call_point := 'inserting into headers interface';

            BEGIN
               INSERT INTO rcv_headers_interface (header_interface_id,
                                                  org_id,
                                                  GROUP_ID,
                                                  processing_status_code,
                                                  receipt_source_code,
                                                  transaction_type,
                                                  auto_transact_code,
                                                  last_update_date,
                                                  last_update_login,
                                                  last_updated_by,
                                                  creation_date,
                                                  created_by,
                                                  validation_flag,
                                                  shipment_num,
                                                  from_organization_id,
                                                  ship_to_organization_id,
                                                  expected_receipt_date,
                                                  receipt_header_id,
                                                  employee_id,
                                                  attribute1,
                                                  vendor_id,
                                                  vendor_site_id,
                                                  customer_id,
                                                  customer_site_id,
                                                  --receipt_num, removed 5/13/2015
                                                  processing_request_id)
                    VALUES (l_header_interface_id,       --Header Interface ID
                            fnd_profile.VALUE ('ORG_ID'),            -- Org Id
                            p_group_id,                             --Group ID
                            'PENDING',                --Processing Status Code
                            l_receipt_source_code,       --Receipt source Code
                            'NEW',                          --Transaction Type
                            p_transaction_type,           --AUTO Transact Code
                            SYSDATE,                        --last update date
                            P_USER_ID,                       --last updated by
                            P_USER_ID,                     --Last Update Login
                            SYSDATE,                           --creation date
                            P_USER_ID,                            --created by
                            'Y',                             --Validation Flag
                            l_shipment_num,                  --Shipment Number
                            l_from_organization_id, --p_transaction_iface_rectype.organization_id,               --From Org
                            l_to_organization_id,                     --To org
                            l_expected_receipts_date,  --Expected Receipt Date
                            l_shipment_header_id,
                            l_employee_id,
                            p_suppress_traveler, --Indicate to suppress printing the receipt traveler if Y then do not print if N then Print
                            p_vendor_id,
                            p_vendor_site_id,
                            l_customer_id,                       --customer_id
                            l_customer_site_id,             --customer_site_id
                            --l_next_receipt_num,                                   --next_receipt_num --removed 5/13/2015
                            l_processing_request_id    --processing request id
                                                   );
            EXCEPTION
               WHEN OTHERS
               THEN
                  g_sqlcode := SQLCODE;
                  g_sqlerrm := SQLERRM;
                  g_message :=
                        'Could not insert into RCV_HEADERS_INTERFACE '
                     || SQLCODE
                     || SQLERRM;
                  RAISE g_exception;
            END;
         END IF;


         g_call_point := 'Getting rcv txn iface ';

         BEGIN
            SELECT XXWC_RCV_MOB_PKG.GET_RCV_TXN_IFACE_ID (
                      p_group_id            => p_group_id,
                      p_inventory_item_id   => p_inventory_item_id,
                      p_organization_id     => p_organization_id,
                      p_transaction_type    => p_transaction_type)
              INTO l_transaction_interface_id
              FROM DUAL;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_transaction_interface_id := -1;
         END;

         debug_log (
            'l_tranasction_interface_id ' || l_transaction_interface_id);

         --if l_transaction_interface_id is -1 then insert otherwise update
         IF l_transaction_interface_id = -1
         THEN
            IF p_quantity > 0
            THEN
               BEGIN
                  g_call_point := 'inserting into rcv_transactions_interface ';

                  INSERT
                    INTO rcv_transactions_interface (header_interface_id,
                                                     org_id,
                                                     GROUP_ID,
                                                     interface_transaction_id,
                                                     transaction_type,
                                                     transaction_date,
                                                     processing_status_code,
                                                     processing_mode_code,
                                                     transaction_status_code,
                                                     category_id,
                                                     quantity,
                                                     primary_quantity,
                                                     last_update_date,
                                                     last_updated_by,
                                                     creation_date,
                                                     created_by,
                                                     receipt_source_code,
                                                     destination_type_code,
                                                     auto_transact_code,
                                                     source_document_code,
                                                     interface_source_code,
                                                     item_id,
                                                     item_description,
                                                     uom_code,
                                                     unit_of_measure,
                                                     primary_unit_of_measure,
                                                     employee_id,
                                                     to_organization_id,
                                                     subinventory,
                                                     locator_id,
                                                     from_organization_id,
                                                     expected_receipt_date,
                                                     shipped_date,
                                                     shipment_num,
                                                     shipment_header_id,
                                                     shipment_line_id,
                                                     routing_header_id,
                                                     routing_step_id,
                                                     destination_context,
                                                     use_mtl_lot,
                                                     use_mtl_serial,
                                                     validation_flag,
                                                     vendor_id,
                                                     vendor_site_id,
                                                     po_header_id,
                                                     po_line_id,
                                                     po_line_location_id,
                                                     po_distribution_id,
                                                     po_unit_price,
                                                     currency_code,
                                                     project_id,
                                                     task_id,
                                                     location_id,
                                                     deliver_to_person_id,
                                                     deliver_to_location_id,
                                                     ship_to_location_id,
                                                     requisition_line_id,
                                                     req_distribution_id,
                                                     oe_order_header_id,
                                                     oe_order_line_id,
                                                     customer_id,
                                                     customer_site_id,
                                                     inspection_status_code,
                                                     mobile_txn,
                                                     currency_conversion_date,
                                                     processing_request_id,
                                                     reason_id)
                  VALUES (l_header_interface_id,         --Header Interface ID
                          fnd_profile.VALUE ('ORG_ID'),               --Org Id
                          p_group_id,                               --Group ID
                          rcv_transactions_interface_s.NEXTVAL, --Interface_transaction_id
                          p_transaction_type,               --Transaction Type
                          SYSDATE,                          --Transaction Date
                          'PENDING',                  --Processing Status Code
                          l_rcv_tp_interface, --'BATCH',             --Processing Mode Code
                          'PENDING',                 --Transaction Status Code
                          l_category_id,                         --Category ID
                          p_quantity,                               --Quantity
                          p_quantity,                       --Primary Quantity
                          SYSDATE,                          --last update date
                          p_user_id,                         --last updated by
                          SYSDATE,                             --creation date
                          p_user_id,                              --created by
                          l_receipt_source_code,         --Receipt source Code
                          'INVENTORY',                 --Destination Type Code
                          'DELIVER',                      --AUTO Transact Code
                          p_doctype,                    --Source Document Code
                          'RCV',                       --Interface Source Code
                          p_inventory_item_id,                       --Item ID
                          p_item_description,               --Item Description
                          p_uom_code,                               --UOM Code
                          l_unit_of_measure,                 --Unit Of Measure
                          l_unit_of_measure,        -- Primary Unit Of Measure
                          l_employee_id,                            --Employee
                          l_to_organization_id,           --To Organization ID
                          p_subinventory_code,              --Sub Inventory ID
                          p_locator_id,                           --To Locator
                          l_from_organization_id,          --From Organization
                          l_expected_receipts_date,    --Expected Receipt Date
                          l_shipped_date,                       --Shipped Date
                          l_shipment_num,                    --Shipment Number
                          l_shipment_header_id,           --Shipment Header Id
                          l_shipment_line_id,               --Shipment Line Id
                          l_receiving_routing_id,          --Routing Header Id
                          l_routing_step_id,                 --Routing Step Id
                          'INVENTORY',                   --Destination Context
                          1,                                     --use_mtl_lot
                          1,                                  --use_mtl_serial
                          'Y',                               --validation_flag
                          p_vendor_id,                             --vendor_id
                          p_vendor_site_id,                   --vendor_site_id
                          l_po_header_id,                       --po_header_id
                          l_po_line_id,                           --po_line_id
                          l_po_line_location_id,         --po_line_location_id
                          l_po_distribution_id,           --po_distribution_id
                          l_unit_price,                           --unit_price
                          l_currency_code,                     --currency_code
                          p_project_id,                           --project_id
                          p_task_id,                                 --task_id
                          l_ship_to_location_id,                 --location_id
                          l_deliver_to_person_id,       --deliver_to_person_id
                          l_ship_to_location_id,      --deliver_to_location_id
                          l_ship_to_location_id,         --ship_to_location_id
                          l_requisition_line_id,         --requisition_line_id
                          l_requisition_distribution_id, --requisition_distribution_id
                          l_oe_order_header_id,           --oe_order_header_id
                          l_oe_order_line_id,               --oe_order_line_id
                          l_customer_id,                         --customer_id
                          l_customer_site_id,               --customer_site_id
                          'NOT_INSPECTED',            --INSPECTION_STATUS_CODE
                          'Y',                                    --Mobile_Txn
                          SYSDATE,                  --Currency conversion date
                          l_processing_request_id,     --Processing Request Id
                          l_reason_id                              --Reason Id
                                     );
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     g_sqlcode := SQLCODE;
                     g_sqlerrm := SQLERRM;
                     g_message :=
                           'Could not insert into RCV_TRANSACTIONS_INTERFACE '
                        || SQLCODE
                        || SQLERRM;
                     RAISE g_exception;
               END;
            END IF;
         ELSE
            BEGIN
               SELECT NVL (primary_quantity, 0), NVL (quantity, 0)
                 INTO l_primary_quantity, l_quantity
                 FROM rcv_transactions_interface
                WHERE interface_transaction_id = l_transaction_interface_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_primary_quantity := 0;
                  l_quantity := 0;
            END;


            IF NVL (l_primary_quantity, 0) + NVL (p_quantity, 0) > 0
            THEN
               g_call_point := 'updating rcv_transactions_interface ';

               BEGIN
                  UPDATE rcv_transactions_interface
                     SET quantity = NVL (quantity, 0) + NVL (p_quantity, 0),
                         primary_quantity =
                            NVL (primary_quantity, 0) + NVL (p_quantity, 0),
                         last_update_date = SYSDATE,
                         last_updated_by = P_USER_ID
                   WHERE     GROUP_ID = p_group_id
                         AND interface_transaction_id =
                                l_transaction_interface_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     g_sqlcode := SQLCODE;
                     g_sqlerrm := SQLERRM;
                     g_message :=
                           'Could not update into RCV_TRANSACTIONS_INTERFACE '
                        || SQLCODE
                        || SQLERRM;
                     RAISE g_exception;
               END;
            ELSE
               g_call_point := 'deleting rcv_transactions_interface ';

               BEGIN
                  DELETE rcv_transactions_interface
                   WHERE     GROUP_ID = p_group_id
                         AND interface_transaction_id =
                                l_transaction_interface_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     g_sqlcode := SQLCODE;
                     g_sqlerrm := SQLERRM;
                     g_message :=
                           'Could not update into RCV_TRANSACTIONS_INTERFACE '
                        || SQLCODE
                        || SQLERRM;
                     RAISE g_exception;
               END;
            END IF;
         END IF;

         x_return := 0;
         x_message := 'Record Inserted';

         COMMIT;
      ELSE
         x_return := 1;
         x_message := 'Nothing inserted';
      END IF;
   EXCEPTION
      WHEN g_exception
      THEN
         debug_log (g_message);

         x_return := 2;
         x_message := g_message || g_sqlcode || g_sqlerrm;

         debug_log ('x_return ' || x_return);
         debug_log ('x_message ' || x_message);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
      WHEN OTHERS
      THEN
         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message := 'when others error ' || g_sqlcode || g_sqlerrm;
         debug_log (g_message);

         x_return := 2;
         x_message := g_message;

         debug_log ('x_return ' || x_return);
         debug_log ('x_message ' || x_message);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END INSERT_INTO_RCV_INTERFACE;


   /*****************************************************************************************************************************************
   *   PROCEDURE UPDATE_RCV_INTERFACE                                                                                                       *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to insert records into the receiving interface table                                               *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/


   PROCEDURE UPDATE_RCV_INTERFACE (P_GROUP_ID               IN     NUMBER  --1
                                                                         ,
                                   P_ORGANIZATION_ID        IN     NUMBER  --2
                                                                         ,
                                   P_TRANSACTION_TYPE       IN     VARCHAR2 --3
                                                                           ,
                                   P_DOCTYPE                IN     VARCHAR2 --4
                                                                           ,
                                   P_FREIGHT_CARRIER_CODE   IN     VARCHAR2 --5
                                                                           ,
                                   P_PACKING_SLIP           IN     VARCHAR2 --6
                                                                           ,
                                   P_BILL_OF_LADING         IN     VARCHAR2 --7
                                                                           ,
                                   P_USER_ID                IN     NUMBER  --8
                                                                         ,
                                   P_ATTRIBUTE2             IN     VARCHAR2 --9
                                                                           ,
                                   X_RETURN                    OUT NUMBER --10
                                                                         ,
                                   X_MESSAGE                   OUT VARCHAR2) --11
   IS
   BEGIN
      g_call_from := 'UPDATE_RCV_INTERFACE';
      g_call_point := 'Start';

      debug_log ('P_GROUP_ID ' || p_group_Id);
      debug_log ('P_ORGANIZATION_ID ' || P_ORGANIZATION_ID);
      debug_log ('P_TRANSACTION_TYPE ' || P_TRANSACTION_TYPE);
      debug_log ('P_DOCTYPE ' || P_DOCTYPE);
      debug_log ('P_FREIGHT_CARRIER_CODE ' || P_FREIGHT_CARRIER_CODE);
      debug_log ('P_PACKING_SLIP ' || P_PACKING_SLIP);
      debug_log ('P_BILL_OF_LADING ' || P_BILL_OF_LADING);
      debug_log ('P_USER_ID ' || P_USER_ID);
      debug_log ('P_ATTRIBUTE2 ' || p_attribute2);

      MO_GLOBAL.INIT ('PO');

      g_call_point := 'update rcv_headers_interface';

      BEGIN
         UPDATE rcv_headers_interface
            SET freight_carrier_code = p_freight_carrier_code,
                bill_of_lading = p_bill_of_lading,
                packing_slip = p_packing_slip,
                attribute2 = p_attribute2,
                last_update_date = SYSDATE,
                last_updated_by = p_user_id
          WHERE     GROUP_ID = p_group_id
                AND ship_to_organization_id = p_organization_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message :=
                  'Could not update RCV_HEADERS_INTERFACE '
               || SQLCODE
               || SQLERRM;
            RAISE g_exception;
      END;
   --COMMIT;

   EXCEPTION
      WHEN g_exception
      THEN
         debug_log (g_message);

         x_return := 1;
         x_message := g_message;

         debug_log ('x_return ' || x_return);
         debug_log ('x_message ' || x_message);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
      WHEN OTHERS
      THEN
         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message := 'when others error ' || g_sqlcode || g_sqlerrm;
         debug_log (g_message);

         x_return := 1;
         x_message := g_message;

         debug_log ('x_return ' || x_return);
         debug_log ('x_message ' || x_message);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END UPDATE_RCV_INTERFACE;



   /*****************************************************************************************************************************************
   *   PROCEDURE PROCESS_RCV_MGR                                                                                                            *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure is used to insert records into the receiving interface table                                               *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *                                                                                                                                        *
   *   2.0        03-SEP-2015  P.Vamshidhar             TMS#20150826-00110 - RF - Receipt Traveller Generation Issue                        *
   *                                                                                                                                        *
   *****************************************************************************************************************************************/

   PROCEDURE PROCESS_RCV_MGR (P_RCV_GROUP_ID   IN     NUMBER,
                              X_RETURN            OUT NUMBER,
                              X_MESSAGE           OUT VARCHAR2)
   IS
      retval                 NUMBER;
      l_trans_count          NUMBER;
      l_msg_cnt              NUMBER;
      l_req_id               NUMBER;
      l_complete_flag        BOOLEAN;
      l_phase                VARCHAR2 (100);
      l_status               VARCHAR2 (100);
      l_dev_phase            VARCHAR2 (100);
      l_dev_status           VARCHAR2 (100);
      l_message              VARCHAR2 (2000);
      l_msg_data             VARCHAR2 (2000);
      l_rcv_tp_interface     VARCHAR2 (50);

      rc                     NUMBER;
      l_timeout              NUMBER := 300;
      l_outcome              VARCHAR2 (200) := NULL;
      l_return_status        VARCHAR2 (5) := fnd_api.g_ret_sts_success;
      l_msg_count            NUMBER;


      -- Below variables added by Vamshi in TMS#20150826-00110  -- Start

      l_shipment_header_id   NUMBER;
      l_applicable           VARCHAR2 (1);
      l_printer              VARCHAR2 (30);
      l_copies               NUMBER;
      l_XML_Layout           BOOLEAN;
      l_set_mode             BOOLEAN;
      l_set_print_options    BOOLEAN;
   -- Above variables added by Vamshi in TMS#20150826-00110  -- End

   BEGIN
      g_call_from := 'PROCESS_RCV_MGR';
      g_call_point := 'Start';



      MO_GLOBAL.INIT ('PO');



      BEGIN
         SELECT fnd_profile.VALUE ('RCV_TP_MODE')
           INTO l_rcv_tp_interface
           FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'When others receiving interface transaction mode ';
            debug_log (g_message);
            RAISE g_exception;
      END;

      IF l_rcv_tp_interface != 'ONLINE'
      THEN
         l_req_id :=
            fnd_request.submit_request ('PO',
                                        'RVCTP',
                                        '',
                                        '',
                                        FALSE,
                                        l_rcv_tp_interface,
                                        p_rcv_group_id,
                                        fnd_profile.VALUE ('ORG_ID'));


         debug_log ('l_req_id ' || l_req_id);

         IF l_req_id <= 0
         THEN
            g_message :=
               '*** Receiving Transaction Processor runing exception ***';
            x_return := 1;
            x_message := g_message;
            debug_log (g_message);
            RAISE g_exception;
         ELSIF l_req_id > 0
         THEN
            debug_log (
               '*** Receiving Transaction Processor Request Submit successful ***');
            COMMIT;

            l_complete_flag :=
               fnd_concurrent.wait_for_request (l_req_id,
                                                1,
                                                3600,
                                                l_phase,
                                                l_status,
                                                l_dev_phase,
                                                l_dev_status,
                                                l_message);
         END IF;

         IF l_complete_flag
         THEN
            debug_log (' PLEASE VALIDATE RECEVING TRANSACTIONS');

            ---- Below code added by Vamshi in TMS#20150826-00110 V2.0 Start


            l_applicable := fnd_profile.VALUE ('XXWC_PRINT_TRAVELLER');

            IF l_applicable = 'Y'
            THEN
               BEGIN
                  SELECT MAX (shipment_header_id)
                    INTO l_shipment_header_id
                    FROM rcv_transactions
                   WHERE GROUP_ID = P_RCV_GROUP_ID AND REASON_ID = 54;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL;
               END;

               l_copies := 1;
               l_printer := fnd_profile.VALUE ('PRINTER');

               l_xml_layout :=
                  FND_REQUEST.ADD_LAYOUT ('XXWC',
                                          'XXWC_INV_REC_TRAVL',
                                          'en',
                                          'US',
                                          'PDF');

               l_set_mode := FND_Request.Set_Mode (TRUE);

               l_set_print_options :=
                  FND_Request.set_print_options (
                     printer            => l_printer,
                     style              => NULL,
                     copies             => l_copies,
                     save_output        => TRUE,
                     print_together     => 'N',
                     validate_printer   => 'RESOLVE');



               l_req_id :=
                  fnd_request.submit_request (
                     APPLICATION   => 'XXWC',
                     PROGRAM       => 'XXWC_INV_REC_TRAVL',
                     DESCRIPTION   => 'XXWC INV Receipt Traveller Report',
                     SUB_REQUEST   => FALSE,
                     ARGUMENT1     => l_shipment_header_id);



               BEGIN
                  --Update the RCV Shipment Header Attribute to flag that we have processed the record

                  UPDATE rcv_shipment_headers
                     SET attribute1 = 'Y'
                   WHERE shipment_header_id = l_shipment_header_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL;
               END;

               COMMIT;
            END IF;
         ---- Above code added by Vamshi in TMS#20150826-00110 V2.0 End

         END IF;
      ELSE
         /*BEGIN
               INSERT INTO rcv_transactions_interface
                                                  (
                                                   header_interface_id,
                                                   org_id,
                                                   group_id,
                                                   interface_transaction_id,
                                                   transaction_type,
                                                   transaction_date,
                                                   processing_status_code,
                                                   processing_mode_code,
                                                   transaction_status_code,
                                                   category_id,
                                                   quantity,
                                                   primary_quantity,
                                                   last_update_date,
                                                   last_updated_by,
                                                   creation_date,
                                                   created_by,
                                                   receipt_source_code,
                                                   destination_type_code,
                                                   auto_transact_code,
                                                   source_document_code,
                                                   interface_source_code,
                                                   item_id,
                                                   item_description,
                                                   uom_code,
                                                   unit_of_measure,
                                                   primary_unit_of_measure,
                                                   employee_id,
                                                   to_organization_id,
                                                   subinventory,
                                                   locator_id,
                                                   from_organization_id,
                                                   expected_receipt_date,
                                                   shipped_date,
                                                   shipment_num,
                                                   shipment_header_id,
                                                   shipment_line_id,
                                                   routing_header_id,
                                                   routing_step_id,
                                                   destination_context,
                                                   use_mtl_lot,
                                                   use_mtl_serial,
                                                   validation_flag,
                                                   vendor_id,
                                                   vendor_site_id,
                                                   po_header_id,
                                                   po_line_id,
                                                   po_line_location_id,
                                                   po_distribution_id,
                                                   po_unit_price,
                                                   currency_code,
                                                   project_id,
                                                   task_id,
                                                   location_id,
                                                   deliver_to_person_id,
                                                   deliver_to_location_id,
                                                   ship_to_location_id,
                                                   requisition_line_id,
                                                   req_distribution_id,
                                                   oe_order_header_id,
                                                   oe_order_line_id,
                                                   customer_id,
                                                   customer_site_id,
                                                   inspection_status_code,
                                                   mobile_txn,
                                                   currency_conversion_date,
                                                   processing_request_id
                                                   )
                                         select    header_interface_id,
                                                   org_id,
                                                   group_id,
                                                   interface_transaction_id,
                                                   transaction_type,
                                                   transaction_date,
                                                   'RUNNING', --processing_status_code,
                                                   processing_mode_code,
                                                   transaction_status_code,
                                                   category_id,
                                                   quantity,
                                                   primary_quantity,
                                                   last_update_date,
                                                   last_updated_by,
                                                   creation_date,
                                                   created_by,
                                                   receipt_source_code,
                                                   destination_type_code,
                                                   auto_transact_code,
                                                   source_document_code,
                                                   interface_source_code,
                                                   item_id,
                                                   item_description,
                                                   uom_code,
                                                   unit_of_measure,
                                                   primary_unit_of_measure,
                                                   employee_id,
                                                   to_organization_id,
                                                   subinventory,
                                                   locator_id,
                                                   from_organization_id,
                                                   expected_receipt_date,
                                                   shipped_date,
                                                   shipment_num,
                                                   shipment_header_id,
                                                   shipment_line_id,
                                                   routing_header_id,
                                                   routing_step_id,
                                                   destination_context,
                                                   use_mtl_lot,
                                                   use_mtl_serial,
                                                   validation_flag,
                                                   vendor_id,
                                                   vendor_site_id,
                                                   po_header_id,
                                                   po_line_id,
                                                   po_line_location_id,
                                                   po_distribution_id,
                                                   po_unit_price,
                                                   currency_code,
                                                   project_id,
                                                   task_id,
                                                   location_id,
                                                   deliver_to_person_id,
                                                   deliver_to_location_id,
                                                   ship_to_location_id,
                                                   requisition_line_id,
                                                   req_distribution_id,
                                                   oe_order_header_id,
                                                   oe_order_line_id,
                                                   customer_id,
                                                   customer_site_id,
                                                   inspection_status_code,
                                                   mobile_txn,
                                                   currency_conversion_date,
                                                   0 --processing_request_id
                                     FROM    rcv_transactions_interface
                                     where   group_id = p_rcv_group_id;
         EXCEPTION
           WHEN others THEN
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message := 'Error on-line inserting';
               raise g_exception;

         END;


   l_timeout := fnd_profile.value('INV_RPC_TIMEOUT');

  IF (l_timeout is NULL) THEN
     l_timeout := 300;
  END IF;
   */
         MO_GLOBAL.INIT ('PO');

         MO_GLOBAL.SET_POLICY_CONTEXT ('M', '162');

         rc :=
            fnd_transaction.synchronous (l_timeout,
                                         l_outcome,
                                         l_message,
                                         'PO',
                                         'RCVTPO',
                                         'ONLINE',
                                         p_rcv_group_id,
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL);
         debug_log ('rc ' || rc);
         debug_log ('l_outcome ' || l_outcome);
         debug_log ('l_message ' || l_message);



          /*
          l_req_id := fnd_request.submit_request('PO',
                                        'RCVTPO',
                                        '',
                                        '',
                                        FALSE
                                        );

          debug_log ('l_req_id ' || l_req_id);
        COMMIT;



          inv_receiving_transaction.txn_complete
            (p_group_id => p_rcv_group_id,
             p_txn_status => 'FALSE',
             p_txn_mode => 'ONLINE',
             x_return_status => l_return_status,
             x_msg_data => l_message,
             x_msg_count => l_msg_count);


        debug_log('l_return_status ' || l_return_status);
        debug_log('l_message ' || l_message);
        debug_log('l_msg_count ' || l_msg_count);

      COMMIT;

      INV_RCV_MOBILE_PROCESS_TXN.rcv_process_receive_txn(x_return_status => l_return_status
                                                        , x_msg_data => l_message);

        debug_log('l_return_status ' || l_return_status);
        debug_log('l_message ' || l_message);

*/

         COMMIT;
      END IF;



      COMMIT;

      x_return := 0;
      x_message := 'Success';
   EXCEPTION
      WHEN g_exception
      THEN
         g_message := g_message || g_sqlcode || g_sqlerrm;
         debug_log (g_message);
         x_return := -1;
         x_message := g_message;

         debug_log ('x_return ' || x_return);
         debug_log ('x_message ' || x_message);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
      WHEN OTHERS
      THEN
         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message := 'when others error ' || g_sqlcode || g_sqlerrm;
         debug_log (g_message);
         x_return := -1;
         x_message := g_message;

         debug_log ('x_return ' || x_return);
         debug_log ('x_message ' || x_message);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END PROCESS_RCV_MGR;

   /*****************************************************************************************************************************************
   *   FUNCTION  get_next_group_id                                                                                                          *
   *                                                                                                                                        *
   *   PURPOSE:   This function returns the next sequence number from rcv_interface_groups_s                                                *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/


   FUNCTION get_next_group_id
      RETURN NUMBER
   IS
      l_rcv_group_id   NUMBER;
   BEGIN
      g_call_from := 'get_next_group_id';
      g_call_point := 'Start';

      SELECT rcv_interface_groups_s.NEXTVAL INTO l_rcv_group_id FROM DUAL;


      debug_log ('l_rcv_group_id ' || l_rcv_group_id);

      RETURN l_rcv_group_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_rcv_group_id := -1;

         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message := 'when others error ' || g_sqlcode || g_sqlerrm;
         debug_log (g_message);


         debug_log ('l_rcv_group_id ' || l_rcv_group_id);


         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END get_next_group_id;


   /*****************************************************************************************************************************************
   *   FUNCTION  get_default_carrier                                                                                                        *
   *                                                                                                                                        *
   *   PURPOSE:   This function returns the next sequence number from rcv_interface_groups_s                                                *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/


   FUNCTION get_default_carrier (P_TRANSACTION_TYPE   IN VARCHAR2          --1
                                                                 ,
                                 P_DOCTYPE            IN VARCHAR2          --2
                                                                 ,
                                 P_SOURCE_HEADER_ID   IN NUMBER)           --3
      RETURN VARCHAR2
   IS
      l_ship_via_lookup_code   VARCHAR2 (25) DEFAULT '';
   BEGIN
      g_call_from := 'get_default_carrier';
      g_call_point := 'Start';

      debug_log ('P_TRANSACTION_TYPE ' || p_transaction_type);
      debug_log ('P_DOCTYPE ' || P_DOCTYPE);
      debug_log ('P_SOURCE_HEADER_ID ' || P_SOURCE_HEADER_ID);

      MO_GLOBAL.INIT ('PO');

      IF p_doctype = 'PO'
      THEN
         g_call_point := ' Getting PO ship via lookup code';

         BEGIN
            SELECT SHIP_VIA_LOOKUP_CODE
              INTO l_ship_via_lookup_code
              FROM po_headers
             WHERE po_header_id = p_source_header_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message := 'Error getting PO ship via lookup code ';
               RAISE g_exception;
         END;
      END IF;


      IF p_doctype = 'REQ'
      THEN
         g_call_point := ' Getting PO ship via lookup code';

         BEGIN
            SELECT FREIGHT_CARRIER_CODE
              INTO l_ship_via_lookup_code
              FROM rcv_shipment_headers
             WHERE shipment_header_id = p_source_header_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message := 'Error getting PO ship via lookup code ';
               RAISE g_exception;
         END;
      END IF;


      IF p_doctype = 'RMA'
      THEN
         g_call_point := ' Getting PO ship via lookup code';

         BEGIN
            SELECT FREIGHT_CARRIER_CODE
              INTO l_ship_via_lookup_code
              FROM oe_order_headers
             WHERE header_id = p_source_header_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message := 'Error getting PO ship via lookup code ';
               RAISE g_exception;
         END;
      END IF;



      debug_log ('l_ship_via_lookup_code ' || l_ship_via_lookup_code);

      RETURN l_ship_via_lookup_code;
   EXCEPTION
      WHEN g_exception
      THEN
         debug_log (g_message);
         debug_log ('l_ship_lookup_code ' || l_ship_via_lookup_code);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
      WHEN OTHERS
      THEN
         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message := 'when others error ' || g_sqlcode || g_sqlerrm;
         debug_log (g_message);
         debug_log ('l_ship_lookup_code ' || l_ship_via_lookup_code);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END get_default_carrier;


   /*****************************************************************************************************************************************
   *   PROCEDURE GET_REQ_LOV                                                                                                                *
   *                                                                                                                                        *
   *   PURPOSE:   This function is used to get the default open quantity for receiving mobile forms                                         *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/

   PROCEDURE GET_REQ_LOV (x_po                   OUT NOCOPY t_genref       --0
                                                                    ,
                          p_organization_id   IN            VARCHAR2       --1
                                                                    ,
                          p_req_number        IN            VARCHAR2)
   IS
      l_organization_id   NUMBER;
   BEGIN
      g_call_from := 'GET_REQ_LOV';
      g_call_point := 'Start';

      debug_log ('p_organization_id ' || p_organization_id);

      MO_GLOBAL.INIT ('PO');


      BEGIN
         SELECT TO_NUMBER (p_organization_id)
           INTO l_organization_id
           FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Could not conver organization id to number ';
            RAISE g_exception;
      END;

      g_call_point := 'Getting list of po numbers';



      --Added 5/12/2015 to prevent open cursors
      IF x_po%ISOPEN
      THEN
         debug_log ('closing x_po cursor');

         CLOSE x_po;
      END IF;


      OPEN x_po FOR
           SELECT DISTINCT prha.requisition_header_id,
                           prha.segment1 requisition_number,
                           TRUNC (mmt.transaction_date) shipped_date, -- to_char(trunc(FND_DATE.CANONICAL_TO_DATE(prha.attribute1)),'MM/DD/YYYY') need_by_date,
                           mp.organization_code ship_from_org,
                           ms.from_organization_id ship_from_org_id,
                           rsh.shipment_num,
                           rsh.shipment_header_id,
                           ooha.header_id,
                           ooha.order_number
             FROM mtl_supply ms,
                  po_requisition_headers prha,
                  mtl_parameters mp,
                  rcv_shipment_lines rsl,
                  rcv_shipment_headers rsh,
                  mtl_material_transactions mmt,
                  oe_order_headers ooha,
                  oe_order_lines oola
            WHERE     ms.to_organization_id = l_organization_id
                  AND ms.supply_type_code = 'SHIPMENT'
                  AND ms.req_header_id = prha.requisition_header_id
                  AND ms.from_organization_id = mp.organization_id
                  AND ms.req_line_id = rsl.requisition_line_id
                  AND rsl.shipment_header_id = rsh.shipment_header_id
                  --AND    prha.segment1 like nvl(p_req_number,prha.segment1) --Removed 4/20/2015
                  AND prha.segment1 = p_req_number           --Added 4/20/2015
                  AND rsl.mmt_transaction_id = mmt.transaction_id
                  AND mmt.source_line_id = oola.line_id
                  AND oola.header_id = ooha.header_id
                  AND ms.shipment_header_id = rsl.shipment_header_id
                  AND ms.shipment_line_Id = rsl.shipment_line_id
         ORDER BY DECODE (RTRIM (prha.segment1, '0123456789'),
                          NULL, NULL,
                          prha.segment1),
                  DECODE (RTRIM (prha.segment1, '0123456789'),
                          NULL, TO_NUMBER (prha.segment1),
                          NULL);
   EXCEPTION
      WHEN g_exception
      THEN
         debug_log (g_message || g_sqlcode || g_sqlerrm);


         --Added 5/12/2015 to prevent open cursors
         IF x_po%ISOPEN
         THEN
            debug_log ('closing x_po cursor');

            CLOSE x_po;
         END IF;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
      WHEN OTHERS
      THEN
         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message := 'when others error ';
         debug_log (g_message || g_sqlcode || g_sqlerrm);


         --Added 5/12/2015 to prevent open cursors
         IF x_po%ISOPEN
         THEN
            debug_log ('closing x_po cursor');

            CLOSE x_po;
         END IF;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END GET_REQ_LOV;


   /*****************************************************************************************************************************************
   *   PROCEDURE GET_RMA_LOV                                                                                                                *
   *                                                                                                                                        *
   *   PURPOSE:   This function is used to get the default open quantity for receiving mobile forms                                         *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/

   PROCEDURE GET_RMA_LOV (x_po                   OUT NOCOPY t_genref       --0
                                                                    ,
                          p_organization_id   IN            VARCHAR2       --1
                                                                    ,
                          p_rma_number        IN            VARCHAR2)
   IS
      l_organization_id   NUMBER;
   BEGIN
      g_call_from := 'GET_RMA_LOV';
      g_call_point := 'Start';

      debug_log ('p_organization_id ' || p_organization_id);
      debug_log ('p_rma_number ' || p_rma_number);

      MO_GLOBAL.INIT ('ONT');


      BEGIN
         SELECT TO_NUMBER (p_organization_id)
           INTO l_organization_id
           FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Could not convert organization id to number ';
            RAISE g_exception;
      END;

      g_call_point := 'Getting list of order numbers';



      --Added 5/12/2015 to prevent open cursors
      IF x_po%ISOPEN
      THEN
         debug_log ('closing x_po cursor');

         CLOSE x_po;
      END IF;


      OPEN x_po FOR
         SELECT DISTINCT ooha.header_id,
                         ooha.order_number,
                         TO_CHAR (TRUNC (ooha.creation_date), 'MM/DD/YYYY'),
                         hca.cust_account_id,
                         hp.party_id,
                         hca.account_number,
                         hp.party_name customer_name
           FROM oe_order_headers ooha, hz_cust_accounts hca, hz_parties hp
          WHERE     ooha.sold_to_org_id = hca.cust_account_id
                AND hca.party_id = hp.party_id
                --AND    ooha.order_number like nvl(p_rma_number, ooha.order_number) --Removed 4/20/2015
                AND ooha.order_number = p_rma_number         --Added 4/20/2015
                AND NVL (ooha.cancelled_flag, 'N') = 'N'
                AND NVL (ooha.booked_flag, 'N') = 'Y'
                AND NVL (ooha.open_flag, 'N') = 'Y'
                AND ooha.flow_status_code NOT IN ('CLOSED', 'CANCELLED')
                AND EXISTS
                       (SELECT *
                          FROM oe_transaction_types_tl ottt,
                               oe_transaction_types otta
                         WHERE     ottt.transaction_type_id =
                                      ooha.order_type_id
                               AND ottt.NAME = 'RETURN ORDER'
                               AND ottt.SOURCE_LANG = g_language
                               AND otta.transaction_type_id =
                                      ottt.transaction_type_id
                               AND otta.transaction_type_code = 'ORDER'
                               AND otta.order_category_code =
                                      ooha.order_category_code)
                AND EXISTS
                       (SELECT *
                          FROM oe_order_lines oola
                         WHERE     oola.header_id = ooha.header_id
                               AND oola.ship_from_org_id = l_organization_id
                               AND NVL (oola.cancelled_flag, 'N') = 'N'
                               AND NVL (oola.booked_flag, 'N') = 'Y'
                               AND NVL (oola.open_flag, 'N') = 'Y'
                               AND oola.flow_status_code NOT IN ('CLOSED',
                                                                 'CANCELLED')
                               AND EXISTS
                                      (SELECT *
                                         FROM oe_transaction_types_tl ottt2,
                                              oe_transaction_types otta2
                                        WHERE     ottt2.transaction_type_id =
                                                     oola.line_type_id
                                              AND ottt2.NAME =
                                                     'RETURN WITH RECEIPT'
                                              AND ottt2.SOURCE_LANG =
                                                     g_language
                                              AND otta2.transaction_type_id =
                                                     ottt2.transaction_type_id
                                              AND otta2.transaction_type_code =
                                                     'LINE'
                                              AND otta2.order_category_code =
                                                     oola.line_category_code));
   EXCEPTION
      WHEN g_exception
      THEN
         debug_log (g_message || g_sqlcode || g_sqlerrm);


         --Added 5/12/2015 to prevent open cursors
         IF x_po%ISOPEN
         THEN
            debug_log ('closing x_po cursor');

            CLOSE x_po;
         END IF;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
      WHEN OTHERS
      THEN
         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message := 'when others error ';
         debug_log (g_message || g_sqlcode || g_sqlerrm);



         --Added 5/12/2015 to prevent open cursors
         IF x_po%ISOPEN
         THEN
            debug_log ('closing x_po cursor');

            CLOSE x_po;
         END IF;


         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END GET_RMA_LOV;


   /*****************************************************************************************************************************************
   *   PROCEDURE EXECUTE_ROLLBACK                                                                                                           *
   *                                                                                                                                        *
   *   PURPOSE:   This function is used to get the default open quantity for receiving mobile forms                                         *
   *                                                                                                                                        *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/

   PROCEDURE EXECUTE_ROLLBACK
   IS
   BEGIN
      g_call_from := 'EXECUTE_ROLLBACK';
      g_call_point := 'Start';

      ROLLBACK;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message := 'when others error ';
         debug_log (g_message || g_sqlcode || g_sqlerrm);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END EXECUTE_ROLLBACK;

   /*****************************************************************************************************************************************
   *  PROCEDURE GET_RTV_LOV                                                                                                                 *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure used for list of values for internal rtv on the mobile device, C or H                                      *
   *                                                                                                                                        *
   *    Parameters: None                                                                                                                    *
   *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/

   PROCEDURE GET_RTV_LOV (x_flexvalues      OUT NOCOPY t_genref            --0
                                                               ,
                          p_flex_value   IN            VARCHAR2)
   IS
   BEGIN
      g_call_from := 'GET_RTV_LOV';
      g_call_point := 'Start';


      IF g_debug = 'Y'
      THEN
         debug_log ('p_flex_value ' || p_flex_value);
      END IF;



      --Added 5/12/2015 to prevent open cursors
      IF x_flexvalues%ISOPEN
      THEN
         debug_log ('closing x_flexvalues cursor');

         CLOSE x_flexvalues;
      END IF;

      OPEN x_flexvalues FOR
         SELECT ffvv.flex_value, ffvv.flex_value_meaning, ffvv.description
           FROM FND_FLEX_VALUE_SETS ffvs, FND_FLEX_VALUES_VL ffvv
          WHERE     ffvs.flex_value_set_name = 'XXWC_HOLD_OR_CLOSE'
                AND ffvs.flex_value_set_id = ffvv.flex_value_set_id
                AND ffvv.enabled_flag = 'Y'
                AND SYSDATE BETWEEN NVL (ffvv.start_date_active, SYSDATE - 1)
                                AND NVL (ffvv.end_date_active, SYSDATE + 1)
                AND ffvv.flex_value LIKE NVL (p_flex_value, ffvv.flex_value);
   EXCEPTION
      WHEN OTHERS
      THEN
         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message :=
               'When others list of values for XXWC_HOLD_OR_CLOSE valueset '
            || g_call_point
            || g_sqlcode
            || g_sqlerrm;
         debug_log (g_message);

         --Added 5/12/2015 to prevent open cursors
         IF x_flexvalues%ISOPEN
         THEN
            debug_log ('closing x_flexvalues cursor');

            CLOSE x_flexvalues;
         END IF;
   END GET_RTV_LOV;


   /*****************************************************************************************************************************************
   *  PROCEDURE PROCESS_UPDATE_RTV_DFF                                                                                                      *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure used to update the shipment_line_id list of values for internal rtv on the mobile device, C or H           *
   *                                                                                                                                        *
   *    Parameters: None                                                                                                                    *
   *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/

   PROCEDURE PROCESS_UPDATE_RTV_DFF (p_shipment_header_id   IN     VARCHAR2,
                                     p_inventory_item_id    IN     VARCHAR2,
                                     p_organization_id      IN     VARCHAR2,
                                     p_value                IN     VARCHAR2,
                                     p_user_id              IN     VARCHAR2,
                                     x_return                  OUT NUMBER,
                                     x_message                 OUT VARCHAR2)
   IS
      l_organization_id      NUMBER;
      l_shipment_header_id   NUMBER;
      l_inventory_item_id    NUMBER;
      l_user_id              NUMBER;
   BEGIN
      g_call_from := 'PROCESS_UPDATE_RTV_DFF';
      g_call_point := 'Start';

      IF g_debug = 'Y'
      THEN
         debug_log ('p_shipment_header_id ' || p_shipment_header_id);
         debug_log ('p_inventory_item_id ' || p_inventory_item_id);
         debug_log ('p_organization_id ' || p_organization_id);
         debug_log ('p_value ' || p_value);
         debug_log ('p_user_id ' || p_user_id);
      END IF;

      BEGIN
         g_call_point := 'Converting Organization Id numbers';

         SELECT TO_NUMBER (p_organization_id)
           INTO l_organization_id
           FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_organization_id := NULL;
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message :=
                  'When others getting l_organization_id '
               || g_call_point
               || g_sqlcode
               || g_sqlerrm;
            debug_log (g_message);
            RAISE g_exception;
      END;



      BEGIN
         g_call_point := 'Converting Item to numbers';

         SELECT TO_NUMBER (p_inventory_item_id)
           INTO l_inventory_item_id
           FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_inventory_item_id := NULL;
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message :=
                  'When others getting l_inventory_item_id '
               || g_call_point
               || g_sqlcode
               || g_sqlerrm;
            debug_log (g_message);
            RAISE g_exception;
      END;

      BEGIN
         g_call_point := 'Converting shipment_header id to number';

         SELECT TO_NUMBER (p_shipment_header_id)
           INTO l_shipment_header_id
           FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_inventory_item_id := NULL;
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message :=
                  'When others getting l_shipment_header_id '
               || g_call_point
               || g_sqlcode
               || g_sqlerrm;
            debug_log (g_message);
            RAISE g_exception;
      END;

      BEGIN
         g_call_point := 'Converting User Id numbers';

         SELECT TO_NUMBER (p_user_id) INTO l_user_id FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_user_id := NULL;
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message :=
                  'When others getting l_user_id '
               || g_call_point
               || g_sqlcode
               || g_sqlerrm;
            debug_log (g_message);
            RAISE g_exception;
      END;


      IF p_value = 'C' OR p_value = 'H' OR p_value IS NULL OR p_value = ''
      THEN
         BEGIN
            UPDATE rcv_shipment_lines
               SET attribute1 = p_value,
                   last_update_date = SYSDATE,
                   last_updated_by = l_user_id
             WHERE     shipment_header_id = l_shipment_header_id
                   AND item_id = l_inventory_item_id
                   AND to_organization_id = l_organization_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message :=
                     'Error updating rcv_shipment_lines '
                  || g_sqlcode
                  || g_sqlerrm;
               debug_log (g_message);
               ROLLBACK;
               RAISE g_exception;
         END;


         COMMIT;

         x_return := 0;
         x_message := 'Success ';
      ELSE
         x_return := 1;
         x_message := 'Value is not C, H, or blank.  Unable to update';
      END IF;
   EXCEPTION
      WHEN g_exception
      THEN
         x_return := 1;
         x_message := 'Error ' || g_message;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error in call '
                                     || g_call_from
                                     || g_call_point,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
      WHEN OTHERS
      THEN
         x_return := 1;
         x_message := 'Error ' || g_message;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error in call '
                                     || g_call_from
                                     || g_call_point,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END PROCESS_UPDATE_RTV_DFF;

   /*****************************************************************************************************************************************
   *  PROCEDURE get_po_acceptance_notes_cnt                                                                                                 *
   *                                                                                                                                        *
   *   PURPOSE:   This function calls the XXWC_PO_HELPERS.get_po_acceptance_notes_cnt                                                       *
   *                                                                                                                                        *
   *    Parameters: None                                                                                                                    *
   *    Return : 0 - No Accetpances, 1 Acceptance                                                                                           *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/


   FUNCTION get_po_acceptance_notes_cnt (p_po_header_id IN VARCHAR2)
      RETURN NUMBER
   IS
      l_po_header_id   NUMBER;
      l_return         NUMBER DEFAULT 0;
   BEGIN
      g_call_from := 'get_po_acceptance_notes_cnt';
      g_call_point := 'Start';

      IF g_debug = 'Y'
      THEN
         debug_log ('p_po_header_id ' || p_po_header_id);
      END IF;

      BEGIN
         g_call_point := 'Converting PO Header Id to numbers';

         SELECT TO_NUMBER (p_po_header_id) INTO l_po_header_id FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_po_header_id := NULL;
            l_return := 0;
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message :=
                  'When others getting l_po_header_id '
               || g_call_point
               || g_sqlcode
               || g_sqlerrm;
            debug_log (g_message);
            RAISE g_exception;
      END;


      IF g_debug = 'Y'
      THEN
         debug_log ('l_po_header_id ' || l_po_header_id);
      END IF;


      g_call_point := 'Calling XXWC_PO_HELPERS.GET_PO_ACCEPTANCE_NOTES_CNT';

      BEGIN
         l_return :=
            NVL (
               XXWC_PO_HELPERS.GET_PO_ACCEPTANCE_NOTES_CNT (l_po_header_id),
               0);
      EXCEPTION
         WHEN OTHERS
         THEN
            l_return := 0;
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message :=
                  'When others getting l_po_header_id '
               || g_call_point
               || g_sqlcode
               || g_sqlerrm;
            debug_log (g_message);
            RAISE g_exception;
      END;

      RETURN l_return;
   EXCEPTION
      WHEN g_exception
      THEN
         RETURN l_return;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error in call '
                                     || g_call_from
                                     || g_call_point,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
      WHEN OTHERS
      THEN
         RETURN l_return;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error in call '
                                     || g_call_from
                                     || g_call_point,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END get_po_acceptance_notes_cnt;


   /*****************************************************************************************************************************************
   *  PROCEDURE GET_PO_DEFER_ACCEPTANCE_LOV                                                                                                 *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure used for list of values for internal rtv on the mobile device, C or H                                      *
   *                                                                                                                                        *
   *    Parameters: None                                                                                                                    *
   *    Return : XXWC_LABEL_PRINTER profile                                                                                                 *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/


   PROCEDURE GET_PO_DEFER_ACCEPTANCE_LOV (
      x_flexvalues      OUT NOCOPY t_genref                                --0
                                           ,
      p_flex_value   IN            VARCHAR2)
   IS
   BEGIN
      g_call_from := 'GET_PO_DEFER_ACCEPTANCE_LOV';
      g_call_point := 'Start';


      IF g_debug = 'Y'
      THEN
         debug_log ('p_flex_value ' || p_flex_value);
      END IF;

      --Added 5/12/2015 to prevent open cursors
      IF x_flexvalues%ISOPEN
      THEN
         debug_log ('closing x_flexvalues cursor');

         CLOSE x_flexvalues;
      END IF;

      OPEN x_flexvalues FOR
         SELECT ffvv.flex_value, ffvv.flex_value_meaning, ffvv.description
           FROM FND_FLEX_VALUE_SETS ffvs, FND_FLEX_VALUES_VL ffvv
          WHERE     ffvs.flex_value_set_name =
                       'XXWC_PO_ACCEPTANCE_DEFER_REASONS'
                AND ffvs.flex_value_set_id = ffvv.flex_value_set_id
                AND ffvv.enabled_flag = 'Y'
                AND SYSDATE BETWEEN NVL (ffvv.start_date_active, SYSDATE - 1)
                                AND NVL (ffvv.end_date_active, SYSDATE + 1)
                AND ffvv.flex_value LIKE NVL (p_flex_value, ffvv.flex_value);
   EXCEPTION
      WHEN OTHERS
      THEN
         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message :=
               'When others list of values for XXWC_PO_ACCEPTANCE_DEFER_REASONS valueset '
            || g_call_point
            || g_sqlcode
            || g_sqlerrm;
         debug_log (g_message);

         --Added 5/12/2015 to prevent open cursors
         IF x_flexvalues%ISOPEN
         THEN
            debug_log ('closing x_flexvalues cursor');

            CLOSE x_flexvalues;
         END IF;
   END GET_PO_DEFER_ACCEPTANCE_LOV;

   /*****************************************************************************************************************************************
   *  PROCEDURE GET_RCV_TOTAL                                                                                                               *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure used for list of values for internal rtv on the mobile device, C or H                                      *
   *                                                                                                                                        *
   *    Parameters: None                                                                                                                    *
   *    Return : Return Current Receiving Total on Interface Table                                                                          *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/


   FUNCTION GET_RCV_TOTAL (p_group_id            IN NUMBER,
                           p_inventory_item_id   IN NUMBER,
                           p_organization_id     IN NUMBER,
                           p_transaction_type    IN VARCHAR2)
      RETURN NUMBER
   IS
      l_quantity   NUMBER;
   BEGIN
      g_call_from := 'GET_RCV_TOTAL';

      debug_log ('p_group_id ' || p_group_id);
      debug_log ('p_inventory_item_id ' || p_inventory_item_id);
      debug_log ('p_organization_id ' || p_organization_id);
      debug_log ('p_transaction_type ' || p_transaction_type);

      BEGIN
         SELECT NVL (SUM (primary_quantity), 0)
           INTO l_quantity
           FROM rcv_transactions_interface
          WHERE     GROUP_ID = p_group_id
                AND item_id = p_inventory_item_id
                AND to_organization_id = p_organization_id
                AND transaction_type = p_transaction_type
                AND transaction_status_code = 'PENDING'
                AND processing_status_code = 'PENDING';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_quantity := 0;
      END;

      RETURN l_quantity;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message :=
               'When others list of values for GET_RCV_TOTAL '
            || g_call_point
            || g_sqlcode
            || g_sqlerrm;
         debug_log (g_message);
         RETURN 0;
   END GET_RCV_TOTAL;

   /*****************************************************************************************************************************************
    *  PROCEDURE GET_RCV_TOLERANCE                                                                                                           *
    *                                                                                                                                        *
    *   PURPOSE:   This procedure used for to get the receipt tolerance                                                                      *
    *                                                                                                                                        *
    *    Parameters: None                                                                                                                    *
    *    Return : Return Current Receiving Total on Interface Table                                                                          *
    *                                                                                                                                        *
    *   REVISIONS:                                                                                                                           *
    *   Ver        Date        Author                     Description                                                                        *
    *   ---------  ----------  ---------------         -------------------------                                                             *
    *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
    *                                                     RF - Receiving                                                                     *
    *****************************************************************************************************************************************/


   PROCEDURE GET_RCV_TOLERANCE (P_ORGANIZATION_ID     IN     NUMBER        --1
                                                                   ,
                                P_INVENTORY_ITEM_ID   IN     NUMBER        --2
                                                                   ,
                                P_SOURCE_HEADER_ID    IN     NUMBER        --3
                                                                   ,
                                P_TRANSACTION_TYPE    IN     VARCHAR2      --4
                                                                     ,
                                P_DOCTYPE             IN     VARCHAR2      --5
                                                                     ,
                                P_QTY                 IN     NUMBER        --6
                                                                   ,
                                X_RETURN                 OUT NUMBER        --7
                                                                   ,
                                X_MESSAGE                OUT VARCHAR2)
   IS                                                                      --8
      l_qty_rcv_tolerance        NUMBER;
      l_qty_rcv_exception_code   VARCHAR2 (25);
      l_organization_id          NUMBER;
      l_ordered_qty              NUMBER;
      l_received_qty             NUMBER;
      l_max_received_qty         NUMBER;
      l_total_received_qty       NUMBER;
   BEGIN
      mo_global.init ('PO');

      g_call_from := 'GET_RCV_TOLERANCE';
      g_call_point := 'Start';

      debug_log ('p_organization_id ' || p_organization_id);
      debug_log ('p_inventory_item_id ' || p_inventory_item_id);
      debug_log ('p_source_header_id ' || p_source_header_id);
      debug_log ('p_transaction_type ' || p_transaction_type);
      debug_log ('p_doctype ' || p_doctype);
      debug_log ('p_qty ' || p_qty);

      IF p_transaction_type = 'RECEIVE'
      THEN
         IF P_DOCTYPE = 'PO'
         THEN
            g_call_point := 'Getting PO Receive over tolerance';

            BEGIN
                 SELECT NVL (SUM (plla.quantity), 0),
                        NVL (SUM (plla.QUANTITY_RECEIVED), 0),
                        plla.QTY_RCV_TOLERANCE,
                        plla.QTY_RCV_EXCEPTION_CODE
                   INTO l_ordered_qty,
                        l_received_qty,
                        l_qty_rcv_tolerance,
                        l_qty_rcv_exception_code
                   FROM po_headers pha, po_lines pla, po_line_locations plla
                  WHERE     pha.po_header_id = p_source_header_id
                        AND pla.item_id = p_inventory_item_id
                        AND pha.po_header_id = pla.po_header_id
                        AND pla.po_line_id = plla.po_line_Id
                        AND NVL (pha.cancel_flag, 'N') = 'N'
                        AND NVL (pha.approved_flag, 'N') = 'Y'
                        AND NVL (pla.cancel_flag, 'N') = 'N'
                        AND NVL (plla.APPROVED_FLAG, 'N') = 'Y'
               GROUP BY plla.QTY_RCV_TOLERANCE, plla.QTY_RCV_EXCEPTION_CODE;
            EXCEPTION
               WHEN OTHERS
               THEN
                  g_sqlcode := SQLCODE;
                  g_sqlerrm := SQLERRM;
                  g_message := 'Error getting PO Receive over tolerance';
                  RAISE g_exception;
            END;


            IF l_qty_rcv_tolerance IS NULL
            THEN
               BEGIN
                  SELECT qty_rcv_tolerance, qty_rcv_exception_code
                    INTO l_qty_rcv_tolerance, l_qty_rcv_exception_code
                    FROM rcv_parameters
                   WHERE organization_id = l_organization_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_qty_rcv_tolerance := 0;
                     l_qty_rcv_exception_code := 'REJECT';
               END;
            END IF;
         ELSIF P_DOCTYPE = 'REQ'
         THEN
            g_call_point := 'Getting Req over tolerance';

            BEGIN
               SELECT NVL (SUM (prla.quantity), 0),
                      NVL (SUM (prla.QUANTITY_RECEIVED), 0),
                      0,
                      'REJECT'
                 INTO l_ordered_qty,
                      l_received_qty,
                      l_qty_rcv_tolerance,
                      l_qty_rcv_exception_code
                 FROM po_requisition_headers prha,
                      po_requisition_lines prla,
                      mtl_supply ms
                WHERE     ms.shipment_header_id = p_source_header_id
                      AND prha.requisition_header_id = ms.req_header_id
                      AND prla.requisition_line_id = ms.req_line_id
                      AND prla.item_id = p_inventory_item_id
                      AND prha.requisition_header_id =
                             prla.requisition_header_id
                      AND NVL (prha.cancel_flag, 'N') = 'N'
                      AND NVL (prha.authorization_status, 'INCOMPLETE') =
                             'APPROVED'
                      AND NVL (prla.cancel_flag, 'N') = 'N' --GROUP BY  prla.QTY_RCV_TOLERANCE, prla.QTY_RCV_EXCEPTION_CODE
                                                           ;
            EXCEPTION
               WHEN OTHERS
               THEN
                  g_sqlcode := SQLCODE;
                  g_sqlerrm := SQLERRM;
                  g_message := 'Error getting PO Receive over tolerance';
                  RAISE g_exception;
            END;
         ELSIF P_DOCTYPE = 'RMA'
         THEN
            g_call_point := 'Getting Req over tolerance';

            BEGIN
               SELECT NVL (SUM (oola.ordered_quantity), 0),
                      NVL (SUM (oola.shipped_quantity), 0),
                      0,
                      'REJECT'
                 INTO l_ordered_qty,
                      l_received_qty,
                      l_qty_rcv_tolerance,
                      l_qty_rcv_exception_code
                 FROM oe_order_headers ooha, oe_order_lines oola
                WHERE     ooha.header_id = p_source_header_id
                      AND ooha.header_id = oola.header_id
                      AND oola.inventory_item_id = p_inventory_item_id
                      AND NVL (ooha.cancelled_flag, 'N') = 'N'
                      AND NVL (ooha.booked_flag, 'N') = 'Y'
                      AND NVL (oola.cancelled_flag, 'N') = 'N'
                      AND NVL (oola.booked_flag, 'N') = 'Y' --GROUP BY  prla.QTY_RCV_TOLERANCE, prla.QTY_RCV_EXCEPTION_CODE
                                                           ;
            EXCEPTION
               WHEN OTHERS
               THEN
                  g_sqlcode := SQLCODE;
                  g_sqlerrm := SQLERRM;
                  g_message := 'Error getting PO Receive over tolerance';
                  RAISE g_exception;
            END;
         ELSE
            g_message := 'Could not get the tranasction type';
            RAISE g_exception;
         END IF;

         debug_log ('l_ordered_qty ' || l_ordered_qty);
         debug_log ('l_received_qty ' || l_received_qty);
         debug_log ('l_qty_rcv_tolerance ' || l_qty_rcv_tolerance);
         debug_log ('l_qty_rcv_exception_code ' || l_qty_rcv_exception_code);


         BEGIN
            SELECT   (  NVL (l_ordered_qty, 0)
                      * NVL (l_qty_rcv_tolerance, 0)
                      * .01)
                   + NVL (l_ordered_qty, 0)
              INTO l_max_received_qty
              FROM DUAL;
         EXCEPTION
            WHEN OTHERS
            THEN
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message := 'Error getting PO Receive over tolerance';
               RAISE g_exception;
         END;

         debug_log ('l_max_received_qty ' || l_max_received_qty);
      END IF;


      l_total_received_qty := NVL (l_received_qty, 0) + NVL (p_qty, 0);

      debug_log ('l_total_received_qty ' || l_total_received_qty);

      IF NVL (l_max_received_qty, 0) < NVL (l_total_received_qty, 0)
      THEN
         x_message :=
               INITCAP (l_qty_rcv_exception_code)
            || ' message!  Over receipt tolerance exceeded.  Entered quantity '
            || NVL (p_qty, 0)
            || ' with existing received quantity of '
            || l_received_qty
            || ' totals '
            || NVL (l_total_received_qty, 0)
            || ' exceeds tolerance quantity of '
            || l_max_received_qty
            || '.';

         IF l_qty_rcv_exception_code != 'REJECT'
         THEN
            x_return := 1;
         ELSE
            x_return := 2;
         END IF;
      ELSE
         x_return := 0;                                 --l_qty_rcv_tolerance;
         x_message := l_qty_rcv_exception_code;
      END IF;


      debug_log ('x_return ' || x_return);
      debug_log ('x_message ' || x_message);
   EXCEPTION
      WHEN OTHERS
      THEN
         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message :=
               'When others list of values for GET_RCV_TOLERANCE '
            || g_call_point
            || g_sqlcode
            || g_sqlerrm;
         debug_log (g_message);
   END GET_RCV_TOLERANCE;



   /*****************************************************************************************************************************************
   *  PROCEDURE GET_RCV_TXN_IFACE_ID                                                                                                        *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure used for list of values for internal rtv on the mobile device, C or H                                      *
   *                                                                                                                                        *
   *    Parameters: None                                                                                                                    *
   *    Return : Return Current Receiving Total on Interface Table                                                                          *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/


   FUNCTION GET_RCV_TXN_IFACE_ID (p_group_id            IN NUMBER,
                                  p_inventory_item_id   IN NUMBER,
                                  p_organization_id     IN NUMBER,
                                  p_transaction_type    IN VARCHAR2)
      RETURN NUMBER
   IS
      l_interface_transaction_id   NUMBER;
   BEGIN
      g_call_from := 'GET_RCV_TXN_IFACE_ID';

      debug_log ('p_group_id ' || p_group_id);
      debug_log ('p_inventory_item_id ' || p_inventory_item_id);
      debug_log ('p_organization_id ' || p_organization_id);
      debug_log ('p_transaction_type ' || p_transaction_type);

      BEGIN
         SELECT DISTINCT interface_transaction_id
           INTO l_interface_transaction_id
           FROM rcv_transactions_interface
          WHERE     GROUP_ID = p_group_id
                AND item_id = p_inventory_item_id
                AND to_organization_id = p_organization_id
                AND transaction_type = p_transaction_type
                AND transaction_status_code = 'PENDING'
                AND processing_status_code = 'PENDING'
                AND ROWNUM = 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_interface_transaction_id := -1;
      END;

      RETURN l_interface_transaction_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_sqlcode := SQLCODE;
         g_sqlerrm := SQLERRM;
         g_message :=
               'When others list of values for GET_RCV_TXN_IFACE_ID '
            || g_call_point
            || g_sqlcode
            || g_sqlerrm;
         debug_log (g_message);
         RETURN -1;
   END GET_RCV_TXN_IFACE_ID;


   /*****************************************************************************************************************************************
   *  PROCEDURE DELETE_RCV_IFACE                                                                                                            *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure used for list of values for internal rtv on the mobile device, C or H                                      *
   *                                                                                                                                        *
   *    Parameters: None                                                                                                                    *
   *    Return : Return Current Receiving Total on Interface Table                                                                          *
   *                                                                                                                                        *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        09-FEB-2015  Lee Spitzer               TMS Ticket 20150302-00152                                                          *
   *                                                     RF - Receiving                                                                     *
   *****************************************************************************************************************************************/

   PROCEDURE DELETE_RCV_IFACE (p_group_id   IN     NUMBER,
                               x_return        OUT NUMBER,
                               x_message       OUT VARCHAR2)
   IS
      l_header_iface_exists   NUMBER;
      l_lines_iface_exists    NUMBER;
   BEGIN
      g_call_from := 'GET_RCV_TXN_IFACE_ID';
      g_call_point := 'Start';

      debug_log ('p_group_id ' || p_group_id);


      g_call_point :=
            'Getting count on rcv shipment header interface for group id '
         || p_group_id;


      BEGIN
         SELECT COUNT (GROUP_ID)
           INTO l_header_iface_exists
           FROM rcv_headers_interface
          WHERE GROUP_ID = p_group_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_header_iface_exists := 0;
      END;

      debug_log ('l_header_iface_exists ' || l_header_iface_exists);

      g_call_point :=
            'Getting count on rcv transactions interface for group id '
         || p_group_id;

      BEGIN
         SELECT COUNT (GROUP_ID)
           INTO l_lines_iface_exists
           FROM rcv_transactions_interface
          WHERE GROUP_ID = p_group_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_lines_iface_exists := 0;
      END;


      debug_log ('l_lines_iface_exists ' || l_lines_iface_exists);


      IF l_lines_iface_exists > 0
      THEN
         g_call_point := 'Deleting transactions interface';

         BEGIN
            DELETE rcv_transactions_interface
             WHERE GROUP_ID = p_group_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               G_SQLCODE := SQLCODE;
               G_SQLERRM := SQLERRM;
               RAISE g_exception;
         END;

         COMMIT;
      END IF;


      IF l_header_iface_exists > 0
      THEN
         g_call_point := 'Deleting shipment interface';

         BEGIN
            DELETE rcv_headers_interface
             WHERE GROUP_ID = p_group_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               G_SQLCODE := SQLCODE;
               G_SQLERRM := SQLERRM;
               RAISE g_exception;
         END;

         COMMIT;
      END IF;

      x_return := 0;
      x_message := 'Successful';
   EXCEPTION
      WHEN g_exception
      THEN
         x_return := 1;
         g_message :=
               'Error in call '
            || g_call_from
            || g_call_point
            || g_sqlcode
            || g_sqlerrm;
         x_message := g_message;

         ROLLBACK;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error in call '
                                     || g_call_from
                                     || g_call_point,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
      WHEN OTHERS
      THEN
         x_return := 1;
         g_message :=
               'When others in call '
            || g_call_from
            || g_call_point
            || g_sqlcode
            || g_sqlerrm;
         x_message := g_message;

         ROLLBACK;
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'When others in '
                                     || g_call_from
                                     || g_call_point,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END DELETE_RCV_IFACE;

   /*****************************************************************************************************************************************
   *  PROCEDURE CHECK_STUCK_GROUP_ID                                                                                                        *
   *                                                                                                                                        *
   *   PURPOSE:   This procedure used to check existing group id's on RCV Transactions Interface stuck                                      *
   *                                                                                                                                        *
   *    Parameters: None                                                                                                                    *
   *    Return : Return Current Receiving Total on Interface Table                                                                          *
   *                          > 0 means existing group_id                                                                                   *
   *                          = 0 no existing group id                                                                                      *
   *                          < 0 means multiple group_ids and reach out to IT                                                              *
   *   REVISIONS:                                                                                                                           *
   *   Ver        Date        Author                     Description                                                                        *
   *   ---------  ----------  ---------------         -------------------------                                                             *
   *   1.0        01-JUN-2015  Lee Spitzer               TMS Ticket 20150601-00248                                                          *
   *                                                     RF Break/fix - Timeout during receiving causes stuck transactions                  *
   *****************************************************************************************************************************************/

   PROCEDURE CHECK_STUCK_GROUP_ID (p_organization_id    IN     VARCHAR2    --1
                                                                       ,
                                   p_source_header_id   IN     VARCHAR2    --2
                                                                       ,
                                   p_doctype            IN     VARCHAR2    --3
                                                                       ,
                                   p_transaction_type   IN     VARCHAR2    --4
                                                                       ,
                                   p_user_id            IN     VARCHAR2    --5
                                                                       ,
                                   x_return                OUT NUMBER      --6
                                                                     ,
                                   x_message               OUT VARCHAR2)   --7
   IS
      l_receipt_source_code   VARCHAR2 (25);
      l_po_header_id          NUMBER;
      l_shipment_header_id    NUMBER;
      l_oe_order_header_id    NUMBER;
      l_routing_step_id       NUMBER;
      l_group_id              NUMBER DEFAULT -1;
      l_count                 NUMBER DEFAULT 0;
      l_groups                VARCHAR2 (2000);


      CURSOR c_group_id (
         p_routing_step_id      IN NUMBER,
         p_po_header_id         IN NUMBER,
         p_shipment_header_id   IN NUMBER,
         p_oe_order_header_id   IN NUMBER,
         p_organization_id      IN NUMBER)
      IS
         --select distinct 'Group id ' || group_id ||' ' || created_by || ' Date ' || creation_date group_id
         SELECT DISTINCT GROUP_ID
           FROM rcv_transactions_interface
          WHERE     processing_status_code = 'PENDING'
                AND NVL (routing_step_id, 1) = NVL (p_routing_step_id, 1)
                AND NVL (po_header_id, -1) = NVL (p_po_header_id, -1)
                AND NVL (shipment_header_id, -1) =
                       NVL (p_shipment_header_id, -1)
                AND NVL (oe_order_header_id, -1) =
                       NVL (p_oe_order_header_id, -1)
                AND NVL (to_organization_id, -1) =
                       NVL (p_organization_id, to_organization_id);
   BEGIN
      g_call_from := 'CHECK_STUCK_GROUP_ID';
      g_call_point := 'Start';

      debug_log ('p_organization_id ' || p_organization_id);
      debug_log ('p_source_header_id ' || p_source_header_id);
      debug_log ('p_doctype ' || p_doctype);
      debug_log ('p_transaction_type ' || p_transaction_type);
      debug_log ('p_user_id ' || p_user_id);


      g_call_point := 'Get receipt source code ';


      BEGIN
         SELECT DECODE (P_DOCTYPE,
                        'PO', 'VENDOR',
                        'REQ', 'INTERNAL ORDER',
                        'RMA', 'CUSTOMER')
           INTO l_receipt_source_code
           FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_receipt_source_code := NULL;
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message := 'Error getting receipt source code';
            RAISE g_exception;
      END;

      debug_log ('l_receipt_source_code ' || l_receipt_source_code);


      IF p_doctype = 'PO'
      THEN
         MO_GLOBAL.INIT ('PO');

         g_call_point := 'Getting po details';

         BEGIN
            SELECT po_header_id
              INTO l_po_header_id
              FROM po_headers
             WHERE po_header_id = p_source_header_id AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message := 'Error getting po details';
               RAISE g_exception;
         END;
      END IF;


      IF p_doctype = 'REQ'
      THEN
         MO_GLOBAL.INIT ('PO');

         g_call_point := 'Getting Req Shipment Information information';

         BEGIN
            SELECT DISTINCT shipment_header_id
              INTO l_shipment_header_id
              FROM mtl_supply
             WHERE     shipment_header_id = p_source_header_id
                   AND to_organization_id = p_organization_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message := 'Error getting req details';
               RAISE g_exception;
         END;
      END IF;

      IF p_doctype = 'RMA'
      THEN
         MO_GLOBAL.INIT ('ONT');

         g_call_point := 'Getting Order Information';

         BEGIN
            SELECT ooha.header_id
              INTO l_oe_order_header_id
              FROM oe_order_headers ooha
             WHERE     ooha.header_id = p_source_header_id
                   AND NVL (ooha.open_flag, 'N') = 'Y'
                   AND NVL (ooha.cancelled_flag, 'N') = 'N'
                   AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message := 'Error getting rma details';
               RAISE g_exception;
         END;
      END IF;


      g_call_point := 'Getting Routing Step';

      IF p_transaction_type = 'RECEIVE'
      THEN
         l_routing_step_id := 1;
      ELSIF p_transaction_type = 'INSPECT'
      THEN
         l_routing_step_id := 2;
      ELSIF p_transaction_type = 'DELIVER'
      THEN
         l_routing_step_id := 3;
      ELSE
         l_routing_step_id := NULL;
      END IF;

      debug_log ('l_routing_step_id ' || l_routing_step_id);


      g_call_point := 'Getting Existing Pending Group Ids from RTI';

      BEGIN
         SELECT DISTINCT NVL (GROUP_ID, -1)
           INTO l_group_id
           FROM rcv_transactions_interface rti
          WHERE     processing_status_code = 'PENDING'
                AND NVL (routing_step_id, 1) = NVL (l_routing_step_id, 1)
                AND NVL (po_header_id, -1) = NVL (l_po_header_id, -1)
                AND NVL (shipment_header_id, -1) =
                       NVL (l_shipment_header_id, -1)
                AND NVL (oe_order_header_id, -1) =
                       NVL (l_oe_order_header_id, -1)
                AND NVL (to_organization_id, -1) =
                       NVL (p_organization_id, to_organization_id);
      EXCEPTION
         --When No Data Found the Group ID is 0
         WHEN NO_DATA_FOUND
         THEN
            l_group_id := 0;
         --When too_many_rows we need to get all distinct group_id and put them in the message
         WHEN TOO_MANY_ROWS
         THEN
            l_group_id := -1;
            g_message :=
                  'Please contact the help desk. Doc Type is '
               || p_doctype
               || '  and source_header_id is '
               || p_source_header_id
               || ' Too many stuck group_ids on RTI';
            RAISE g_exception;
         /*BEGIN
           FOR r_group_id in c_group_id (l_routing_step_id, l_po_header_id, l_shipment_header_id, l_oe_order_header_id, p_organization_id)
             LOOP
               l_count := l_count + 1;

               if l_count = 1 then

                   g_message := r_group_id.group_id;

               else

                   g_message := g_message || r_group_id.group_id;

               end if;


             END LOOP;

           l_group_id := -1;

           raise g_exception;

         EXCEPTION
           WHEN OTHERS THEN
               g_sqlcode := SQLCODE;
               g_sqlerrm := SQLERRM;
               g_message := 'Error in too many rows loop ' || g_sqlcode || g_sqlerrm;
               raise g_exception;
         END;
     */

         --When others then raise the g_exception
         WHEN OTHERS
         THEN
            g_sqlcode := SQLCODE;
            g_sqlerrm := SQLERRM;
            g_message :=
                  'Please contact the help desk.  When others getting group_id '
               || g_sqlcode
               || g_sqlerrm;
            RAISE g_exception;
      END;

      x_return := l_group_id;
      x_message :=
            'Would you like to continue with an existing receipt of group_id '
         || l_group_id
         || '?';
   EXCEPTION
      WHEN g_exception
      THEN
         x_return := l_group_id;
         x_message := g_message;

         ROLLBACK;
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'When others in '
                                     || g_call_from
                                     || g_call_point,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
      WHEN OTHERS
      THEN
         x_return := l_group_id;
         g_message :=
               'When others in call '
            || g_call_from
            || g_call_point
            || g_sqlcode
            || g_sqlerrm;
         x_message := g_message;

         ROLLBACK;
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_package || '.' || g_call_from,
            p_calling             => g_call_point,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'When others in '
                                     || g_call_from
                                     || g_call_point,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END CHECK_STUCK_GROUP_ID;

END XXWC_RCV_MOB_PKG;
/