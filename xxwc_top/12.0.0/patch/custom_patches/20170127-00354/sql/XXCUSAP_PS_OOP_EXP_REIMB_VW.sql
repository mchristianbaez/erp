/*
TMS:  20170127-00354 
Incident# :  1128956
Date: 01/27/2016
Description: Fix employee number field to strip off alpha characters. 
*/
CREATE OR REPLACE FORCE VIEW APPS.XXCUSAP_PS_OOP_EXP_REIMB_VW
(
   PAYMENT_BATCH,
   CHECK_DATE,
   CHECK_NUMBER,
   EMPLOYEE_NUMBER,
   FIRST_NAME,
   LAST_NAME,
   TERMINATED_DATE,
   EMPLOYEE_STATUS,
   AMOUNT,
   PAYMENT_TYPE_FLAG
)
AS
     SELECT ac.checkrun_name payment_batch,
            ac.check_date check_date,
            ac.check_number check_number,
            LPAD(REGEXP_SUBSTR(papf.employee_number, '[0-9]+', 1, 1), 6, '0') employee_number,
            papf.first_name first_name,
            papf.last_name last_name,
            CASE
               WHEN papf.person_type_id = 9 THEN papf.effective_start_date - 1
               ELSE NULL
            END,
            DECODE (papf.person_type_id, 6, 'Active', 'Terminated')
               employee_status,
            SUM (aip.amount) amount,
            CASE
               WHEN ac.payment_type_flag = 'A' THEN 'Automatic'
               WHEN ac.payment_type_flag = 'M' THEN 'Manual'
               ELSE 'Quick'
            END
       FROM ap.ap_checks_all ac,
            ap.ap_invoices_all i,
            ap.ap_invoice_payments_all aip,
            ap.ap_suppliers v,
            hr.per_all_people_f papf
      WHERE     ac.check_id = aip.check_id
            AND aip.invoice_id = i.invoice_id
            AND i.vendor_id = v.vendor_id
            AND v.employee_id = papf.person_id
            AND v.vendor_type_lookup_code = 'EMPLOYEE'
            AND papf.effective_end_date =
                   TO_DATE ('31-DEC-4712', 'DD-MON-RRRR')
            AND UPPER (ac.bank_account_name) IN
                   (SELECT /*+ RESULT_CACHE */
                          UPPER (meaning)
                      FROM fnd_lookup_values
                     WHERE     1 = 1
                           AND lookup_type = 'XXCUS_IEXPENSE_OOP_PS_VIEW'
                           AND enabled_flag = 'Y'
                           AND SYSDATE BETWEEN start_date_active
                                           AND NVL (end_date_active,
                                                    SYSDATE + 1))
   GROUP BY ac.checkrun_name,
            ac.check_date,
            ac.check_number,
            LPAD(REGEXP_SUBSTR(papf.employee_number, '[0-9]+', 1, 1), 6, '0'),
            papf.first_name,
            papf.last_name,
            CASE
               WHEN papf.person_type_id = 9
               THEN
                  papf.effective_start_date - 1
               ELSE
                  NULL
            END,
            DECODE (papf.person_type_id, 6, 'Active', 'Terminated'),
            ac.payment_type_flag;
--
COMMENT ON TABLE APPS.XXCUSAP_PS_OOP_EXP_REIMB_VW IS 'TMS:  20170127-00354 OR Incident# :  1128956';
--
GRANT SELECT ON APPS.XXCUSAP_PS_OOP_EXP_REIMB_VW TO INTERFACE_HR;
--
GRANT SELECT ON APPS.XXCUSAP_PS_OOP_EXP_REIMB_VW TO INTERFACE_PRISM;
--
GRANT SELECT ON APPS.XXCUSAP_PS_OOP_EXP_REIMB_VW TO INTERFACE_XXCUS;
--