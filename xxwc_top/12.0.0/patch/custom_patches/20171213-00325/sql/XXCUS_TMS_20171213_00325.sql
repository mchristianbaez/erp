/*
   Ticket#                 Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS: 20171213-00325     12/13/2017   Balaguru Seshadri  Put 2017 WATERWORKS rebate agreements to on hold.
*/
declare
    cursor offers is
	select oo.qp_list_header_id,
		   qlhv.description rebate_name,
		   oo.offer_type rebate_type,
		   oo.offer_code rebate_code,
		   ams.description activity,
		   qlhv.attribute4 contract_id,
		   qlhv.attribute7 calendar_year,
		   oo.status_code,
		   oo.object_version_number obj_ver_num,
		   oo.offer_id, 
		   oo.org_id, 
		   oo.offer_code
	  from ozf.ozf_offers oo,
		   apps.qp_list_headers_vl qlhv,
		   apps.ams_media_vl ams
	 where 1 = 1
		   and oo.qp_list_header_id = qlhv.list_header_id
		   and oo.activity_media_id = ams.media_id
		   and oo.qp_list_header_id not in (3732957, 3776368)
		   and qlhv.attribute7 = '2017'
		   and oo.status_code ='ONHOLD'
           and oo.user_status_id =1607
		   and qlhv.attribute4 in 
		   (
			'14541',
			'14659',
			'14659',
			'7581',
			'7581',
			'13589',
			'13589',
			'13051',
			'13051',
			'2780',
			'13969',
			'13969',
			'13969',
			'13969',
			'8623',
			'8623',
			'15061',
			'15170',
			'8610',
			'8421',
			'8519',
			'14412',
			'15098',
			'15098',
			'6862',
			'6862',
			'15131',
			'15131',
			'14449',
			'15570',
			'15570',
			'13986',
			'13986',
			'8444',
			'8444',
			'14633',
			'14633',
			'15271',
			'15271',
			'13176',
			'2691',
			'14911',
			'5811',
			'8490',
			'8490',
			'2845',
			'15083',
			'15083',
			'12921',
			'14537',
			'14537',
			'6428',
			'6428',
			'12621',
			'12621',
			'12621',
			'9100',
			'9100',
			'15135',
			'12581',
			'12581',
			'3006',
			'13674',
			'15110',
			'8491',
			'8491',
			'15386',
			'15386',
			'14791',
			'14791',
			'14875',
			'14875',
			'14181',
			'13167',
			'13167',
			'15124',
			'15124',
			'15090',
			'15090',
			'6486',
			'15109',
			'15109',
			'15362',
			'15598',
			'15598',
			'15125',
			'15125',
			'8446',
			'8446',
			'8446',
			'8446',
			'14758',
			'14758',
			'2540',
			'2757',
			'2757',
			'15153',
			'15153',
			'15146',
			'15146',
			'15146',
			'15146',
			'15149',
			'8843',
			'14138',
			'14138',
			'14674',
			'14674',
			'15221',
			'15221',
			'7946',
			'7946',
			'14542',
			'14542',
			'12625',
			'2975',
			'15126',
			'15126',
			'12751',
			'12751',
			'15159',
			'15159',
			'15159',
			'8624',
			'8624',
			'15117',
			'15117',
			'14742',
			'14742',
			'12533',
			'14976',
			'5801',
			'15395',
			'15395',
			'4476',
			'14343',
			'14343',
			'14266',
			'14266',
			'6758',
			'15151',
			'15151',
			'5888',
			'5888',
			'7440',
			'7440',
			'8898',
			'15255',
			'15255',
			'13513',
			'13513',
			'7944',
			'7944',
			'8532',
			'8532',
			'8137',
			'8217',
			'2926',
			'15597',
			'15597',
			'5364',
			'15385',
			'7024',
			'7024',
			'15223',
			'15223',
			'15223',
			'15223',
			'7473',
			'15114',
			'2787',
			'4670',
			'13531',
			'13531',
			'15044',
			'15044',
			'15165',
			'15233',
			'15145',
			'15145',
			'5940',
			'13585',
			'13585',
			'15508',
			'14396',
			'15115',
			'14118',
			'14118',
			'14118',
			'14118',
			'15093',
			'15093',
			'15096',
			'2625',
			'13967',
			'7584',
			'14664',
			'12116',
			'12744',
			'8190',
			'15293',
			'14377',
			'12334',
			'12334',
			'8569',
			'15189',
			'14102',
			'14102',
			'14102',
			'CASH BASIS',
			'5898',
			'14658',
			'5749',
			'12397',
			'15407',
			'15122',
			'15122',
			'14728',
			'14728',
			'8900',
			'8900',
			'2551',
			'2551',
			'2658',
			'15142',
			'15142',
			'14400',
			'15579',
			'15579',
			'15139',
			'7913',
			'15268',
			'13630',
			'15222',
			'15150',
			'15150',
			'12865',
			'8323',
			'8323',
			'14729',
			'14729',
			'15162',
			'15190',
			'15190',
			'6447',
			'6447',
			'14852',
			'14852',
			'8572',
			'14396',
			'15200',
			'13874',
			'14930',
			'14930',
			'12992',
			'7579',
			'15064',
			'15120',
			'8944',
			'15154',
			'5273',
			'15291',
			'14628',
			'15099',
			'13226',
			'14574',
			'15130',
			'15130',
			'13819',
			'15179',
			'15179',
			'15637',
			'8514',
			'7344',
			'12629',
			'2689',
			'15104',
			'15042',
			'15091',
			'15091',
			'15246',
			'15246',
			'15089',
			'15089',
			'14037',
			'15156',
			'15105'
	       ) 
	;

    x_offer_adjustment_id   NUMBER;
    x_offer_adjst_tier_id   NUMBER;
    x_offer_adj_line_id     NUMBER;
    x_object_version_number NUMBER;
  
    x_qp_list_header_id NUMBER;
    x_err_location      NUMBER;
  
    l_offer_adj_rec        ozf_offer_adjustment_pvt.offer_adj_rec_type;
    l_offadj_tier_rec_type ozf_offer_adj_tier_pvt.offadj_tier_rec_type;
  
    l_offadj_line_rec_type ozf_offer_adj_line_pvt.offadj_line_rec_type;
  
    l_modifier_list_rec ozf_offer_pub.modifier_list_rec_type;
    l_modifier_line_tbl ozf_offer_pub.modifier_line_tbl_type;
    l_na_qualifier_tbl  ozf_offer_pub.na_qualifier_tbl_type;
    l_prod_rec_tbl      ozf_offer_pub.prod_rec_tbl_type;
    l_offer_tier_tbl    ozf_offer_pub.offer_tier_tbl_type;
    l_excl_rec_tbl      ozf_offer_pub.excl_rec_tbl_type;
    l_discount_line_tbl ozf_offer_pub.discount_line_tbl_type;
    l_act_product_tbl   ozf_offer_pub.act_product_tbl_type;
    l_vo_pbh_tbl        ozf_offer_pub.vo_disc_tbl_type;
    l_vo_dis_tbl        ozf_offer_pub.vo_disc_tbl_type;
    l_vo_prod_tbl       ozf_offer_pub.vo_prod_tbl_type;
    l_qualifier_tbl     ozf_offer_pub.qualifiers_tbl_type;
    l_vo_mo_tbl         ozf_offer_pub.vo_mo_tbl_type;
    l_budget_tbl        ozf_offer_pub.budget_tbl_type;
  
    l_object_version_number NUMBER;
    l_obj_version           NUMBER;
    l_start_date            DATE;
    l_offer_id              NUMBER;
    l_description           qp_list_headers_vl.description%TYPE;
    l_offer_type            VARCHAR2(50);
    l_user_status_id        NUMBER;
  
    x_errbuf  VARCHAR2(2000);
    x_retcode VARCHAR2(2000);
  
    x_return_status VARCHAR2(1);
    x_msg_count     NUMBER;
    x_msg_data      VARCHAR2(4000);
    
begin
 
 for rec in offers loop
  mo_global.set_policy_context('S', rec.org_id);
  savepoint here_we_start;
  
      l_modifier_list_rec.qp_list_header_id     :=rec.qp_list_header_id;
      l_modifier_list_rec.object_version_number :=rec.obj_ver_num;     
      l_modifier_list_rec.status_code           :='ACTIVE';
      l_modifier_list_rec.user_status_id        :=1604;
      l_modifier_list_rec.modifier_operation    :='UPDATE';
      l_modifier_list_rec.offer_operation       :='UPDATE';
      l_modifier_list_rec.offer_id              :=rec.offer_id;
	  	  
    
      ozf_offer_pub.process_modifiers(p_init_msg_list     => fnd_api.g_false
                                     ,p_api_version       => 1.0
                                     ,p_commit            => fnd_api.g_false
                                     ,x_return_status     => x_return_status
                                     ,x_msg_count         => x_msg_count
                                     ,x_msg_data          => x_msg_data
                                     ,p_offer_type        => l_offer_type
                                     ,p_modifier_list_rec => l_modifier_list_rec
                                     ,p_modifier_line_tbl => l_modifier_line_tbl
                                     ,p_qualifier_tbl     => l_qualifier_tbl
                                     ,p_budget_tbl        => l_budget_tbl
                                     ,p_act_product_tbl   => l_act_product_tbl
                                     ,p_discount_tbl      => l_discount_line_tbl
                                     ,p_excl_tbl          => l_excl_rec_tbl
                                     ,p_offer_tier_tbl    => l_offer_tier_tbl
                                     ,p_prod_tbl          => l_prod_rec_tbl
                                     ,p_na_qualifier_tbl  => l_na_qualifier_tbl
                                     ,x_qp_list_header_id => x_qp_list_header_id
                                     ,x_error_location    => x_err_location);
    
      IF ((x_return_status = fnd_api.g_ret_sts_error) OR (x_return_status = fnd_api.g_ret_sts_unexp_error)) THEN        
        dbms_output.put_line('offer id ='||rec.offer_id||', failed to update');
        rollback to here_we_start;      
      ELSE       
        --dbms_output.put_line('offer id ='||rec.offer_id||', updated successfully to ONHOLD'); 
        Null;		
      END IF;          
 end loop;
 
 commit;
 dbms_output.put_line('Commit Complete...');
exception
 when others then
 dbms_output.put_line('Outer block, message =>'||sqlerrm); 
  rollback to here_we_start;       
end;
/