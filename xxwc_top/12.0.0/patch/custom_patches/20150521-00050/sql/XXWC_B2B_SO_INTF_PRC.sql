CREATE OR REPLACE PROCEDURE APPS.xxwc_b2b_so_interface_prc(p_mode IN VARCHAR2, p_req_id     OUT NUMBER) IS
  /*******************************************************************************
  * Procedure:   xxwc_b2b_so_intf_prc
  * Description: This procedure is used by an APEX App (XXWC B2B Inbound SO Interface) 
                 to execute the interface program to commit changes.
                 Apex App cannot directly execute the interface program due to Oracle
                 limitation on executing Concurrent Program over dblink.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11-May-2012        Gopi Damuluri   Initial creation of the package
  1.1     16-Jun-2015        Gopi Damuluri   TMS# 20150521-00050
                                             Added PRAGMA AUTONOMOUS TRANSACTION to resolve Distributed Transaction Error
  ********************************************************************************/

  --Intialize Variables
  l_err_msg               VARCHAR2(2000);
  l_err_code              NUMBER;
  l_sec                   VARCHAR2(150);
  l_err_callfrom          VARCHAR2(75) := 'xxwc_b2b_so_intf_prc';
  l_distro_list           VARCHAR2(75) DEFAULT 'HDSORACLEDEVELOPERS@hdsupply.com';
  l_user_name             VARCHAR2(150) := 'XXWC_INT_SALESFULFILLMENT';
  l_responsibility_name   VARCHAR2(150) := 'HDS Order Mgmt Super User - WC';
  
  PRAGMA AUTONOMOUS_TRANSACTION; -- Version# 1.1
  
  --Start Main Program
BEGIN
  l_sec := 'Start B2B SO Interface from APEX';

  -- Call the procedure
  apps.xxwc_b2b_so_ib_pkg.submit_job(p_errbuf                 => l_err_msg,
                                     p_retcode                => l_err_code,
                                     p_user_name              => l_user_name,
                                     p_responsibility_name    => l_responsibility_name,
                                     p_process_type           => p_mode);

EXCEPTION
  WHEN OTHERS THEN
    l_err_code := 2;
    l_err_msg  := substr((l_err_msg || ' ERROR ' ||
                         substr(SQLERRM, 1, 2000)), 1, 3000);
    dbms_output.put_line(l_err_msg);
  
    xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom,
                                         p_calling           => l_sec,
                                         p_request_id        => -1 ,
                                         p_ora_error_msg     => substr(SQLERRM, 1, 300),
                                         p_error_desc        => substr(l_err_msg, 1, 300),
                                         p_distribution_list => l_distro_list,
                                         p_module            => 'OM');
END xxwc_b2b_so_interface_prc;
/
