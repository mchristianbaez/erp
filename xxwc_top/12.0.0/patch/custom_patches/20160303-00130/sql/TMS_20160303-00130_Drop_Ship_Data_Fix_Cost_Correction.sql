/*************************************************************************
  $Header TMS_20160303-00130_COST.sql $
  Module Name: TMS_20160303-00130 Drop Ship Cost correction SQL 

  PURPOSE: Data fix to correct the cost of Drop Ship PO

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        03-MAR-2016  Raghav Velichetti         TMS#20160303-00130

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160303-00130   , Before Update');

UPDATE oe_order_lines_all
SET project_id = 1, 
subinventory = 'Drop'
where line_id = 65786756;

   DBMS_OUTPUT.put_line (
         'TMS: 20160303-00130 Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160303-00130   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160303-00130, Errors : ' || SQLERRM);
END;
/