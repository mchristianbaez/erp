/*************************************************************************
  $Header TMS_20160928-00286_CANCEL_ORDER_LINE.sql $
  Module Name: TMS_20160928-00286  Data Fix script for 17517650

  PURPOSE: Data fix script for 17517650--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        10-OCT-2016  Niraj K Ranjan        TMS#20160928-00286

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160825-00139    , Before Update');

   update apps.oe_order_lines_all
   set FLOW_STATUS_CODE='CANCELLED',
   CANCELLED_FLAG='Y'
   where line_id = 75176411
   and header_id= 45941392;

   DBMS_OUTPUT.put_line (
         'TMS: 20160825-00139  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160825-00139    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160825-00139 , Errors : ' || SQLERRM);
END;
/



