  set serveroutput on;
 /*************************************************************************
      PURPOSE:  Close the Line Number 41 for Order 21079148

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        17-Aug-2017  Sundaramoorthy     Initial Version - TMS #20170817-00141
	************************************************************************/ 
 BEGIN
 dbms_output.put_line ('Start Update ');

 UPDATE oe_order_lines_all
  SET  flow_status_code ='CLOSED'
  , open_flag ='N'
  WHERE line_id = 76208929
  AND header_id =45055670;  
  COMMIT;
  
  dbms_output.put_line ('Update count '||SQL%ROWCOUNT);
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
	dbms_output.put_line ('Inside Exception '|| SUBSTR(SQLERRM,1,2000));	
END;
/
			   
  