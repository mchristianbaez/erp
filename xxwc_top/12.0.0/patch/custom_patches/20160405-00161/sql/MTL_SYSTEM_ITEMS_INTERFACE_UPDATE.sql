   /*****************************************************************************************************************************************
   -- File Name: MTL_SYSTEM_ITEMS_INTERFACE_UPDATE.sql
   --
   -- PROGRAM TYPE: SQL Script to purge MTL_SYSTEM_ITEMS_INTERFACE 

   -- TMS#20160405-00161 - Need to Purge error records from mtl_system_items_interface table
   ******************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

CREATE TABLE XXWC.XXWC_MTL_SYSTEM_ITEMS_INTF_BKP
AS
   SELECT *
     FROM apps.mtl_system_items_interface
    WHERE TRUNC (creation_date) <= TRUNC (SYSDATE) - 30;

CREATE TABLE XXWC.XXWC_MTL_INTERFACE_ERRORS_BKP
AS
   SELECT *
     FROM APPS.MTL_INTERFACE_ERRORS
    WHERE transaction_id IN (SELECT transaction_id
                               FROM XXWC.XXWC_MTL_SYSTEM_ITEMS_INTF_BKP
                              WHERE TRUNC (creation_date) <=
                                       TRUNC (SYSDATE) - 30);    

BEGIN
   DBMS_OUTPUT.put_line ('Before Delete');

   
DELETE FROM apps.mtl_system_items_interface
      WHERE TRUNC (creation_date) <= TRUNC (SYSDATE) - 30;

   DBMS_OUTPUT.put_line ('Number of Records Deleted (mtl_system_items_interface) '||SQL%ROWCOUNT);



DELETE FROM APPS.MTL_INTERFACE_ERRORS
      WHERE transaction_id IN (SELECT transaction_id
                                 FROM XXWC.XXWC_MTL_SYSTEM_ITEMS_INTF_BKP
                                WHERE TRUNC (creation_date) <=
                                         TRUNC (SYSDATE) - 30);

   DBMS_OUTPUT.put_line ('Number of Records Deleted (MTL_INTERFACE_ERRORS)'||SQL%ROWCOUNT);

DBMS_OUTPUT.put_line ('After Delete');

COMMIT;

EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('Delete failed '||SQLERRM);
END;
/