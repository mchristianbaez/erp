   **************************************************************************
     $Header XXWC_MD_SEARCH_PRODUCTS_MV_S $
     Module Name: XXWC_MD_SEARCH_PRODUCTS_MV_S.sql

     PURPOSE:   Create Synonym for 
          MV      : XXWC_MD_SEARCH_PRODUCTS_MV
          MV2     : XXWC_MD_SEARCH_PRODUCTS_MV2

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        06/28/2016  Gopi Damuluri           Initial Version TMS# 20160628-00001
   **************************************************************************/

CREATE OR REPLACE SYNONYM APPS.XXWC_MD_SEARCH_PRODUCTS_MV_S FOR APPS.XXWC_MD_SEARCH_PRODUCTS_MV;