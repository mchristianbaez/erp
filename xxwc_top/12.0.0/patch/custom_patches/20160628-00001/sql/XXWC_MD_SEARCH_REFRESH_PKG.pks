CREATE OR REPLACE PACKAGE APPS.XXWC_MD_SEARCH_REFRESH_PKG
AS        
   /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header XXWC_MD_SEARCH_REFRESH_PKG $
     Module Name: XXWC_MD_SEARCH_REFRESH_PKG.pks

     PURPOSE:   This package is used by AIS process to refresh MVs , Indexes and create Synonym
          MV      : XXWC_MD_SEARCH_PRODUCTS_MV
          MV2     : XXWC_MD_SEARCH_PRODUCTS_MV2
          SYNONYM : XXWC_MD_SEARCH_PRODUCTS_MV_S

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        06/28/2016  Gopi Damuluri           Initial Version TMS# 20160628-00001
     **************************************************************************/

   /*************************************************************************
     Procedure : refresh_ais_mv

     PURPOSE:   This procedure is to refresh AIS XXWC_MD_SEARCH_PRODUCTS_MV and its Indexes 

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        06/28/2016  Gopi Damuluri           Initial Version TMS# 20160628-00001
   ************************************************************************/
   PROCEDURE refresh_ais_mv (p_err_buf     OUT VARCHAR2,
                             p_return_code OUT NUMBER);

   /*************************************************************************
     Procedure : create_syn_for_ais_mv

     PURPOSE:   This procedure is create synonym XXWC_MD_SEARCH_PRODUCTS_MV_S pointing to XXWC_MD_SEARCH_PRODUCTS_MV

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        06/28/2016  Gopi Damuluri           Initial Version TMS# 20160628-00001
   ************************************************************************/
   PROCEDURE create_syn_for_ais_mv (p_err_buf     OUT VARCHAR2,
                             p_return_code OUT NUMBER);

   /*************************************************************************
     Procedure : backup_ais_mv

     PURPOSE:   This procedure is to refresh AIS XXWC_MD_SEARCH_PRODUCTS_MV2 and its Indexes 

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        06/28/2016  Gopi Damuluri           Initial Version TMS# 20160628-00001
   ************************************************************************/
   PROCEDURE backup_ais_mv (p_err_buf     OUT VARCHAR2,
                             p_return_code OUT NUMBER);

   /*************************************************************************
     Procedure : create_syn_for_ais_bkp

     PURPOSE:   This procedure is create synonym XXWC_MD_SEARCH_PRODUCTS_MV_S pointing to XXWC_MD_SEARCH_PRODUCTS_MV2

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        06/28/2016  Gopi Damuluri           Initial Version TMS# 20160628-00001
   ************************************************************************/
   PROCEDURE create_syn_for_ais_bkp (p_err_buf     OUT VARCHAR2,
                             p_return_code OUT NUMBER);

   /*************************************************************************
     Procedure : main

     PURPOSE:   This procedure is the main procedure for creating synonym XXWC_MD_SEARCH_PRODUCTS_MV_S 
                pointing to XXWC_MD_SEARCH_PRODUCTS_MV or XXWC_MD_SEARCH_PRODUCTS_MV2

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        06/28/2016  Gopi Damuluri           Initial Version TMS# 20160628-00001
   ************************************************************************/
   PROCEDURE MAIN (p_err_buf     OUT VARCHAR2,
                   p_return_code OUT NUMBER,
                   p_process_step IN VARCHAR2);

END XXWC_MD_SEARCH_REFRESH_PKG;
/