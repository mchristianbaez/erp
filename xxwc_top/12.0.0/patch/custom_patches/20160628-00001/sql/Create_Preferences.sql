BEGIN 
  ctx_ddl.create_preference('XXWC.XXWC_MD_PRODUCT_SEARCH_PREF', 'BASIC_WORDLIST'); 
  ctx_ddl.set_attribute('XXWC.XXWC_MD_PRODUCT_SEARCH_PREF','SUBSTRING_INDEX','TRUE');
  ctx_ddl.set_attribute('XXWC.XXWC_MD_PRODUCT_SEARCH_PREF','PREFIX_INDEX','TRUE');
  ctx_ddl.set_attribute('XXWC.XXWC_MD_PRODUCT_SEARCH_PREF','PREFIX_MIN_LENGTH',2);
  ctx_ddl.set_attribute('XXWC.XXWC_MD_PRODUCT_SEARCH_PREF', 'wildcard_maxterms', 20000) ;
  ctx_ddl.set_attribute('XXWC.XXWC_MD_PRODUCT_SEARCH_PREF','FUZZY_MATCH','ENGLISH');
  ctx_ddl.set_attribute('XXWC.XXWC_MD_PRODUCT_SEARCH_PREF','FUZZY_SCORE',70);
  ctx_ddl.set_attribute('XXWC.XXWC_MD_PRODUCT_SEARCH_PREF','STEMMER','ENGLISH');

   ctx_ddl.create_preference ('XXWC.XXWC_MD_PRODUCT_STORE_PREF', 'multi_column_datastore');  
   ctx_ddl.set_attribute   
 	 ('XXWC.XXWC_MD_PRODUCT_STORE_PREF',   
 	  'columns',   
 	  'partnumber,   
 	   shortdescription,
     cross_reference');  
   ctx_ddl.create_preference ('XXWC.XXWC_MD_PRODUCT_STORE_LEX1', 'basic_lexer');  
   ctx_ddl.set_attribute ('XXWC.XXWC_MD_PRODUCT_STORE_LEX1', 'whitespace', '/\|-_+,');  
   ctx_ddl.create_section_group ('XXWC.XXWC_MD_PRODUCT_STORE_SG', 'basic_section_group');  
   ctx_ddl.add_field_section ('XXWC.XXWC_MD_PRODUCT_STORE_SG', 'partnumber', 'partnumber', true);  
   ctx_ddl.add_field_section ('XXWC.XXWC_MD_PRODUCT_STORE_SG', 'shortdescription', 'shortdescription', true);  
   ctx_ddl.add_field_section ('XXWC.XXWC_MD_PRODUCT_STORE_SG', 'cross_reference', 'cross_reference', true);  

END;
/