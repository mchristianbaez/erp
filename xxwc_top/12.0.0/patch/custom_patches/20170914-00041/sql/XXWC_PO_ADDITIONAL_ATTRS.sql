/*
-- ***************************************************************************************************************************
-- $Header XXWC_PO_ADDITIONAL_ATTRS $
-- Module Name: XXWC_PO_ADDITIONAL_ATTRS

-- REVISIONS:
-- Ver        Date        Author           Description
-- ---------  ----------  ----------       ----------------
-- 1.0        12/29/2017  Niraj K Ranjan   TMS#20170914-00041   DMS Phase-2.0 Outbound Extract(PO), modify the PO for 1SEENOTES
-- ***************************************************************************************************************************
*/
CREATE TABLE XXWC.XXWC_PO_ADDITIONAL_ATTRS
(
  PO_HEADER_ID   NUMBER,
  SUPPLIER_ID    NUMBER,
  ADDRESS_LINE1  VARCHAR2(240 BYTE),
  ADDRESS_LINE2  VARCHAR2(240 BYTE),
  ADDRESS_LINE3  VARCHAR2(240 BYTE),
  CITY           VARCHAR2(60 BYTE),
  STATE          VARCHAR2(150 BYTE),
  ZIP            VARCHAR2(60 BYTE),
  PHONE_NUMBER   VARCHAR2(50 BYTE)
);

CREATE UNIQUE INDEX APPS.XXWC_PO_ADDITIONAL_ATTRS_N1 ON XXWC.XXWC_PO_ADDITIONAL_ATTRS
(PO_HEADER_ID);

CREATE OR REPLACE SYNONYM APPS.XXWC_PO_ADDITIONAL_ATTRS FOR XXWC.XXWC_PO_ADDITIONAL_ATTRS;