create or replace PACKAGE           xxwc_po_document_util is

/*******************************************************************************

Package Name: XXWC_PO_DOCUMENT_UTIL

Script Owners: Lucidity Consulting Group.

Client: 

Description: Package defined to house custom pricing funtions.


History:
Version   Date          Author          Description
----------------------------------------------------------
1.0       31-May-2012   Lee Spitzer         Initial development.
2.0       07-Jan-2014   Consuelo Gonzalez   TMS 20131121-00162: Added function
                                            to retrieve drop ship contact phone number
2.1       30-Jan-2014   Lee Spitzer         Vendor Cost Improvements 0130917-00676 
2.2       23-Jan-2015   Lee Spitzer         20140909-00029 - 1SEE NOTES
3.0       26-OCT-2017   Niraj K Ranjan      TMS#20170914-00041   DMS Phase-2.0 Outbound Extract(PO), modify the PO for 1SEENOTES
*******************************************************************************/

    g_called_function  VARCHAR2(40);
    g_package          VARCHAR2(40) := 'XXWC_PO_DOCUMENT_UTIL';
    g_source           VARCHAR2(40);
    
/*******************************************************************************

    xxwc_get_first_ds_pll_id
        (p_po_header_id IN NUMBER => pass the po_header_id to retrieve the fist drop ship line_loacation_id
        
        Return = po_line_location_id 
    
*******************************************************************************/

    function    xxwc_get_first_ds_pll_id (p_po_header_id    in NUMBER) return number;
    
/*******************************************************************************

    xxwc_get_first_ds_ship_loc_id
        (p_po_line_location_id IN NUMBER => pass the po_line_location_id to retrieve the po_ship_location_id) 
       Return = po_ship_location_id
    
*******************************************************************************/

    function    xxwc_get_first_ds_ship_loc_id (p_po_line_loc_id    in NUMBER) return number;
    

/*******************************************************************************

    xxwc_get_address_info
      xxwc_get_address_info (p_po_line_loc_id    in NUMBER, --line_location_id from po_line_locations_all  
                             p_po_ship_loc_id    in NUMBER, --ship_location_id from po_line_locations_all
                             p_return_info    in VARCHAR2)  --address information from po_line_locations_all, ie address1, address2, territory...
                             
                 return varchar2;  --return address based on return info parameter
*******************************************************************************/
    
    function    xxwc_get_address_info (p_po_line_loc_id    in NUMBER, p_po_ship_loc_id    in NUMBER, p_return_info    in VARCHAR2) return varchar2;

-- 01/07/2014 CG: TMS 20131121-00162: added function to retrieve Drop Ship Bill To Contact Phone Number
    function    xxwc_get_ds_bill_cont_phone (p_po_header_id in NUMBER) return varchar2;    


  /*************************************************************************************************   
   *   Function xxwc_get_vendor_contact_id                                                          *
   *   Purpose : get the vendor contact id for the correct address for the PO Docuemnt              *
   *                                                                                                *
   *                                                                                                *
   *       function   xxwc_get_vendor_contact_id (p_po_header_id in NUMBER)                         *
   *           RETURN NUMBER;                                                                       *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   2.1`       30-Jan-2014   Lee Spitzer             Vendor Cost Improvements 0130917-00676      *
   *                                                                                                *
   /************************************************************************************************/
    
  
    function   xxwc_get_vendor_contact_id (p_po_header_id in NUMBER)
               RETURN NUMBER;
               


  
  /*************************************************************************************************   
   *   Function xxwc_get_contact_address_info                                                       *
   *   Purpose : get the vendor contact address information for the correct address for the PO Document*
   *                                                                                                *
   *                                                                                                *
   *   function xxwc_get_contact_address_info (p_vendor_contact_id in NUMBER, p_return_info in VARCHAR2)*
   *           RETURN VARCHAR2;                                                                     *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   2.1        30-Jan-2014   Lee Spitzer             Vendor Cost Improvements 0130917-00676      *
   *   2.2        23-Jan-2015   Lee Spitzer             TMS Ticket 20140909-00029 - 1SEE NOTES      *
   *                                                                                                *
   /************************************************************************************************/
  
    function xxwc_get_contact_address_info (p_vendor_contact_id in NUMBER, p_return_info in VARCHAR2) 
              RETURN VARCHAR2;
    
	/* Start Change for Ver 3.0 */
   /******************************************************************************
    FUNCTION : xxwc_get_1seenotes_address
	PURPOSE  : To get 1SEE NOTES address from custom table
	REVISION :
	Ver      Date           Author            Description
	-----------------------------------------------------------------------------
	3.0      26-OCT-2017    Niraj K Ranjan    TMS#20170914-00041   DMS Phase-2.0 Outbound Extract(PO), modify the PO for 1SEENOTES
   ******************************************************************************/
    FUNCTION xxwc_get_1seenotes_address (p_po_header_id    IN NUMBER) 
	RETURN VARCHAR2;
   /* End Change for Ver 3.0 */
    
end xxwc_po_document_util;
/