BEGIN
   UPDATE apps.gl_period_statuses glp
      SET closing_status = 'N'
    WHERE     1 = 1
          --AND closing_status = 'N'
          AND glp.ledger_id = 2061
          AND glp.period_year = 2016
          AND glp.period_name in ( 'Nov-2016','Dec-2016','Jan-2016')
          AND glp.adjustment_period_flag = 'N'
          AND glp.application_id = 222;

   COMMIT;
END;
/