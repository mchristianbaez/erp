CREATE OR REPLACE PACKAGE BODY APPS.XXWC_MONITOR_TXW_OBJ_PKG
AS
   /* ************************************************************************
    HD Supply
    All rights reserved.
   **************************************************************************
     PURPOSE: Audit inserts or updates to specific taxware related tables.
     REVISIONS:
     Ticket           Ver         Date         Author                     Description
     ---------        ----------  ----------   ------------------------   -------------------------
     20150619-00149   1.0        6/19/2015    Bala Seshadri               1. Created
     20150921-00152   1.1        6/10/2016    Neha Saini                  2.modified flush_txw_prod_conv to not delete 90016
     20150921-00152   1.2        14/10/2016   Neha Saini                  3.modified flush_txw_prod_conv to not notify if no deletion
 CALLED BY:
    1) UC4 Daily AR AutoInvoice Kickoff job [TMS 20150624-00135 ]
   2) Request Set Daily OE to AR and SC Interface Batch Set [TMS 20150619-00149]
   ************************************************************************* */
   --
   PROCEDURE notify
   IS
      --Intialize Variables

      l_dflt_email      fnd_user.email_address%TYPE
                           := 'WC-ITFinanceSupport-U1@hdsupply.com';
      l_pkgproc         VARCHAR2 (100) := 'XXWC_MONITOR_TXW_OBJ_PKG.notify';
      l_sender          VARCHAR2 (100);
      l_sid             VARCHAR2 (8);
      l_body            VARCHAR2 (32767);
      l_body_header     VARCHAR2 (32767);
      l_body_detail     VARCHAR2 (32767);
      l_body_footer     VARCHAR2 (32767);
      l_sql_hint        VARCHAR2 (32767);
      l_error_hint      VARCHAR2 (1000);
      g_host            VARCHAR2 (256) := 'mailoutrelay.hdsupply.net';
      g_hostport        VARCHAR2 (20) := '25';
      g_ErrorMessage    VARCHAR2 (9000);
      l_err_callfrom    VARCHAR2 (175) := 'XXWC_MONITOR_TXW_OBJ_PKG.notify';
      l_err_callpoint   VARCHAR2 (175) := 'START';
      l_err_msg         CLOB;
   BEGIN
      --
      -- Local variables
      --
      SELECT LOWER (NAME) INTO l_sid FROM v$database;

      --
      l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
      --
      l_err_callpoint := '@101, assigned sender email id.';
      --
      l_err_msg := 'Get lookup for record count for looping';
      --
      l_err_callpoint := '@102, assigned receiver email id.';
      --
      l_body_header :=
            '<style type="text/css">
        .style1 {
            border-style: solid;
            border-width: 1px;
        }
        .style2 {
            border: 1px solid #000000;
        }
        .style3 {
            border: 1px solid #000000;
            background-color: #FFFF00;
        }
        .style4 {
            color: #FFFF00;
            border: 1px solid #000000;
            background-color: #000000;
        }
        .style5 {
            font-size: large;
        }
        .style6 {
            font-size: xx-large;
        }
        .style7 {
            color: #FFCC00;
        }
        </style>
        <p><span class="style6"><span class="style7"><strong>HDS WhiteCap</strong></span><br />
        <span class="style5">Monitor Taxware Tables:  Instance: '
         || UPPER (l_sid)
         || ' </span></p>
        <BR><table border="2" cellpadding="2" cellspacing="2" width="100%">'
         || '<td class="style2"><B>TABLE NAME</B></td>
                                <td class="style2"><B>TYPE</B></td>'
         || '
                                <td class="style2"><B>COMMENTS</B></td>
                                <td class="style2"><B>REQUEST ID</B></td><tr>';
      --
      l_err_callpoint := '@103, assigned email header.';
      --
      l_body_detail :=
            l_body_detail
         || '<td class="style3">'
         || 'TAXWARE.TAXPRODCONV'                  --c_errors_dist.called_from
         || '</td><td class="style3">'
         || 'Alert'
         || '</td><td class="style3">'
         || 'Auto removed a custom taxware prod code mapping. Please check table xxwc.xxwc_txware_taxprodconv for records archived.'
         || '</td><td class="style3">'
         || fnd_global.conc_request_id
         || '</td><TR>';
      --
      l_err_callpoint := '@104, assigned email body.';
      --
      l_body := l_body_header || l_body_detail || l_body_footer;
      --
      l_err_callpoint := '@105, email body with header and detail assigned.';
      --
      xxcus_misc_pkg.html_email (p_to              => l_dflt_email,
                                 p_from            => l_sender,
                                 p_text            => 'test',
                                 p_subject         => 'Taxware Table -Alert',
                                 p_html            => l_body,
                                 p_smtp_hostname   => g_host,
                                 p_smtp_portnum    => g_hostport);
      --
      l_err_callpoint := '@106, After calling xxcus_misc_pkg.html_email';
      --
      COMMIT;
   --
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_MONITOR_TXW_OBJ_PKG.notify',
            p_calling             => l_err_callpoint,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => l_dflt_email,
            p_module              => 'WC AR Taxware');
   END notify;

   --
   PROCEDURE flush_txw_prod_conv (retcode OUT VARCHAR2, errbuf OUT VARCHAR2)
   AS
      --
      l_err_msg           VARCHAR2 (2000);
      l_err_callfrom      VARCHAR2 (175) := 'XXWC_MONITOR_TXW_OBJ_PKG';
      l_err_callpoint     VARCHAR2 (175) := 'START';
      --
      l_distro_list       VARCHAR2 (80) := 'hdsoracledevelopers@hdsupply.com'; --Updated to use new Distribution List
      l_WC_FIN_IT_email   VARCHAR2 (80)
                             := 'WC-ITFinanceSupport-U1@hdsupply.com';
      l_module            VARCHAR2 (80) := 'WC AR Taxware';
      --
      l_message           VARCHAR2 (1000);
      l_sqlcode           VARCHAR2 (1000);
      l_sqlerrm           VARCHAR2 (1000);
      --
      l_count             NUMBER := 0;
      l_conc_status       BOOLEAN;
      l_exception         EXCEPTION;
   --
   BEGIN
      --
      l_err_callpoint := 'Begin: flush_txw_prod_conv';

      --
      BEGIN
         SELECT COUNT (1)
           INTO l_count
           FROM taxware.taxprodconv
          WHERE 1 = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            NULL;
         WHEN OTHERS
         THEN
            RAISE l_exception;
      END;

      --
      l_err_callpoint := '@flush_txw_prod_conv, End Taxware pre checks';

      --
      IF l_count > 0
      THEN
         --
         --
         BEGIN
            --
            l_err_callpoint :=
               SUBSTR (
                  '@Start copying taxprodconv table to xxwc.xxwc_txware_taxprodconv',
                  1,
                  175);

            --
            INSERT INTO xxwc.xxwc_txware_taxprodconv
               (SELECT * FROM taxware.taxprodconv);

            --
            COMMIT;
            --ver1.2 starts
            --
            --            BEGIN
            --               --
            --               l_err_callpoint := SUBSTR ('Before send email', 1, 175);
            --               --
            --               XXWC_MONITOR_TXW_OBJ_PKG.notify;
            --            --
            --            EXCEPTION
            --               WHEN OTHERS
            --               THEN
            --                  RAISE l_exception;
            --            END;
            --ver1.2 ends
            --
            COMMIT;
            --
            l_err_callpoint := SUBSTR ('After send email', 1, 175);
         --
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               NULL;
            WHEN OTHERS
            THEN
               RAISE l_exception;
         END;

         --
         BEGIN
            l_err_callpoint :=
               '@flush_txw_prod_conv, before delete taxware.taxprodconv';

            DELETE FROM taxware.taxprodconv
                  WHERE TWIPRODCODE NOT IN (SELECT meaning
                                              FROM apps.fnd_lookup_values flv
                                             WHERE flv.lookup_type =
                                                      'XXWC_NO_FLUSH_PCODE'); -- ver 1.1 TMS 20150921-00152 change by Neha

            --ver1.2 starts
            IF SQL%ROWCOUNT > 0
            THEN
               BEGIN
                  --
                  l_err_callpoint := SUBSTR ('Before send email', 1, 175);
                  --
                  XXWC_MONITOR_TXW_OBJ_PKG.notify;
               --
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     RAISE l_exception;
               END;
            END IF;

            --ver1.2 starts
            COMMIT;
            l_err_callpoint :=
               '@flush_txw_prod_conv, after delete taxware.taxprodconv';
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               NULL;
            WHEN OTHERS
            THEN
               RAISE l_exception;
         END;
      --
      END IF;

      --
      l_err_callpoint := 'End: flush_txw_prod_conv, exit normal.';
   --
   EXCEPTION
      WHEN l_exception
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => l_distro_list,
            p_module              => l_module);
         RAISE;
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => l_distro_list,
            p_module              => l_module);
   --
   END flush_txw_prod_conv;

   --
   /**************************************************************************
    *
    * PROCEDURE: uc4_wrapper
    * DESCRIPTION: HDS WC: Flush Taxware Prod Conversion Table
    * PARAMETERS
    * ==========
    * NAME                   TYPE     DESCRIPTION
   .* -----------------      -------- ---------------------------------------------
    * p_user_name            IN       EBS user who executes the current concurrent request
    * p_responsibility_name  IN      Current EBS Responsibility used to submit the job
    * RETURN VALUE: None
    *************************************************************************/
   --
   PROCEDURE uc4_wrapper (p_user_name             IN VARCHAR2,
                          p_responsibility_name   IN VARCHAR2)
   IS
      -- Variable definitions
      l_package               VARCHAR2 (50) := 'xxwc_monitor_txw_obj_pkg.uc4_wrapper';
      l_email                 VARCHAR2 (200) := 'HDSOracleDevelopers@hdsupply.com';
      --
      l_req_id                NUMBER NULL;
      v_phase                 VARCHAR2 (50);
      v_status                VARCHAR2 (50);
      v_dev_status            VARCHAR2 (50);
      v_dev_phase             VARCHAR2 (50);
      --
      v_message               VARCHAR2 (250);
      v_error_message         VARCHAR2 (3000);
      l_err_msg               VARCHAR2 (3000);
      l_err_code              NUMBER;
      --
      l_statement             VARCHAR2 (9000);
      l_user_id               NUMBER;
      l_responsibility_id     NUMBER;
      l_resp_application_id   NUMBER;
      --
      l_err_callfrom          VARCHAR2 (75) DEFAULT 'XXCUS_ERROR_PKG';
      l_err_callpoint         VARCHAR2 (75) DEFAULT 'START';
      l_distro_list           VARCHAR2 (75)
                                 DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   --
   BEGIN
      -- Deriving Ids from variables
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE     user_name = UPPER (p_user_name)
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
               'UserName ' || p_user_name || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving user_id for UserName - ' || p_user_name;
            RAISE PROGRAM_ERROR;
      END;

      --
      BEGIN
         SELECT responsibility_id, application_id
           INTO l_responsibility_id, l_resp_application_id
           FROM fnd_responsibility_vl
          WHERE     responsibility_name = p_responsibility_name
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
                  'Responsibility '
               || p_responsibility_name
               || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error deriving Responsibility_id for '
               || p_responsibility_name;
            RAISE PROGRAM_ERROR;
      END;

      -- Apps Initialize
      FND_GLOBAL.APPS_INITIALIZE (l_user_id,
                                  l_responsibility_id,
                                  l_resp_application_id);
      -- Submitting program HDS WC: Flush Taxware Prod Conversion Table
      l_req_id :=
         fnd_request.submit_request (
            application   => 'XXWC',
            program       => 'XXWC_FLUSH_TAXPRODCONV',
            description   => 'UC4 Submit',
            start_time    => SYSDATE,
            sub_request   => FALSE);
      COMMIT;
   --
   EXCEPTION
      --
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_code := 2;
         l_err_msg :=
            SUBSTR ( (l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000)),
                    1,
                    3000);
         --
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 2000),
            p_distribution_list   => l_distro_list,
            p_module              => 'WhiteCap AR');
      --
      WHEN OTHERS
      THEN
         l_err_code := 2;
         l_err_msg :=
            SUBSTR ( (l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000)),
                    1,
                    3000);
         --
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 2000),
            p_distribution_list   => l_distro_list,
            p_module              => 'WhiteCap AR');
   END uc4_wrapper;
--
END XXWC_MONITOR_TXW_OBJ_PKG;
/