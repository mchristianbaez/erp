   /***************************************************************************************************************************************
      $Header XXWC_PO_SEL_ONHAND_QTY_VW $
      Module Name: XXWC_PO_SEL_ONHAND_QTY_VW.sql
   
      PURPOSE:   View created to populate information in item info form.
   
      REVISIONS:
      Ver        Date        Author                  Description
      ---------  ----------  ---------------         -------------------------
      1.0        05/24/2016  P.Vamshidhar            TMS#20160129-00284- Show real time data in Purchasing version of Item Info form.
                                                     Initial Version
   ****************************************************************************************************************************************/

CREATE OR REPLACE FORCE VIEW APPS.XXWC_PO_SEL_ONHAND_QTY_VW
(
   INVENTORY_ITEM_ID,
   ORGANIZATION_ID,
   SEGMENT1,
   DESCRIPTION,
   AMU,
   ORGANIZATION_CODE,
   PRICING_REGION,
   ON_HAND_QTY,
   AVAILABLE_TO_RESERVE,
   AVAILABLE_TO_TRANSACT,
   OPEN_SALES_ORDERS,
   AVERAGE_COST,
   ON_HAND_GT_90,
   ON_HAND_GT_180,
   ON_HAND_GT_270,
   LAST_RECEIPT_DATE,
   MIN,
   MAX
)
AS
   SELECT stg.inventory_item_id,
          stg.organization_id,
          stg.segment1,
          stg.description,
          stg.amu,
          stg.organization_code,
          stg.pricing_region,
          xxwc_ascp_scwb_pkg.get_on_hand (stg.inventory_item_id,
                                          stg.organization_id,
                                          'RQ')
             on_hand_qty,
          xxwc_ascp_scwb_pkg.get_on_hand (stg.inventory_item_id,
                                          stg.organization_id,
                                          'R')
             available_to_reserve,
          xxwc_ascp_scwb_pkg.get_on_hand (stg.inventory_item_id,
                                          stg.organization_id,
                                          'T')
             available_to_transact,
          (SELECT NVL (
                     SUM (
                          l.ordered_quantity
                        * po_uom_s.po_uom_convert (
                             um.unit_of_measure,
                             stg.primary_unit_of_measure,
                             l.inventory_item_id)),
                     0)
             FROM apps.oe_order_lines l,
                  apps.oe_order_headers h,
                  MTL_UNITS_OF_MEASURE_VL um
            WHERE     l.header_id = h.header_id
                  AND h.flow_status_code NOT IN ('ENTERED',
                                                 'CLOSED',
                                                 'CANCELLED')
                  AND (   (l.flow_status_code = 'AWAITING_SHIPPING')
                       OR (    l.flow_status_code = 'BOOKED'
                           AND l.line_type_id = 1005))
                  AND l.ship_from_org_id = stg.organization_id
                  AND l.inventory_item_id = stg.inventory_item_id
                  AND l.order_quantity_uom = um.uom_code)
             open_sales_orders,
          cst_cost_api.get_item_cost (1,
                                      stg.inventory_item_id,
                                      stg.organization_id,
                                      NULL,
                                      NULL,
                                      5)
             average_cost,
          (SELECT SUM (NVL (moqd.primary_transaction_quantity, 0))
             FROM mtl_onhand_quantities_detail moqd
            WHERE     inventory_item_id = stg.inventory_item_id
                  AND organization_id = stg.organization_id
                  AND orig_date_received < SYSDATE - 90)
             on_hand_gt_90,
          (SELECT SUM (NVL (moqd.primary_transaction_quantity, 0))
             FROM mtl_onhand_quantities_detail moqd
            WHERE     inventory_item_id = stg.inventory_item_id
                  AND organization_id = stg.organization_id
                  AND orig_date_received < SYSDATE - 180)
             on_hand_gt_180,
          (SELECT SUM (NVL (moqd.primary_transaction_quantity, 0))
             FROM mtl_onhand_quantities_detail moqd
            WHERE     inventory_item_id = stg.inventory_item_id
                  AND organization_id = stg.organization_id
                  AND orig_date_received < SYSDATE - 270)
             on_hand_gt_270,
          (SELECT MAX (rtrx.transaction_date)
             FROM rcv_transactions rtrx, rcv_shipment_lines rsl
            WHERE     rtrx.transaction_Type = 'RECEIVE'
                  AND rtrx.shipment_line_id = rsl.shipment_line_id
                  AND stg.inventory_item_id = rsl.item_id
                  AND stg.organization_id = rsl.to_organization_id)
             last_receipt_date,
          stg.MIN,
          stg.MAX
     FROM xxwc.xxwc_po_sel_onhand_tbl stg
    WHERE 1 = 1
/