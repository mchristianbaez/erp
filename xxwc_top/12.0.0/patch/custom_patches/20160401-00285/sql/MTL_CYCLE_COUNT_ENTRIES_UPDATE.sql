  /****************************************************************************************************
  Description: resetting flag cycle count entries to process.

  HISTORY
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  1.0     01-Apr-2016        P.Vamshidhar    Initial version TMS#20160401-00285 
                                             Need to update mtl_cycle_count_entries table for Branch 046
  *****************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

   UPDATE mtl_cycle_count_entries
      SET export_flag = NULL
    WHERE     organization_id = 269
          AND entry_status_code IN (1, 3)
          AND export_flag IS NOT NULL;

   DBMS_OUTPUT.put_line ('Records updated: ' || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('After Update');
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
END;
/