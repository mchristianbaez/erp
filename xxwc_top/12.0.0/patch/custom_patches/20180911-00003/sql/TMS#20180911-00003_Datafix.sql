/***********************************************************************************************************************************************
   NAME:       TMS_20170309-00055_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        09/11/2018  Rakesh Patel     TMS#TMS#20180911-00003
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Update');
   
   
   UPDATE xxwc.xxwc_b2b_so_hdrs_stg_tbl
   SET process_flag ='S'
   WHERE customer_po_number in ('C00068810','C00067995','C00069229','52654' );

   DBMS_OUTPUT.put_line ('Records Updated in xxwc.xxwc_b2b_so_hdrs_stg_tbl  -' || SQL%ROWCOUNT);
   COMMIT;
   
   
   UPDATE xxwc.xxwc_b2b_so_lines_stg_tbl
      SET process_flag ='S'
    WHERE customer_po_number in ('C00068810','C00067995','C00069229','52654');

   DBMS_OUTPUT.put_line ('Records Update in xxwc.xxwc_b2b_so_lines_stg_tbl' || SQL%ROWCOUNT);
   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to Update record ' || SQLERRM);
	  ROLLBACK;
END;
/