CREATE OR REPLACE VIEW XXWC_MD_SEARCH_PRODUCTS_VW
/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_MD_SEARCH_PRODUCTS_VW $
  Module Name: XXWC_MD_SEARCH_PRODUCTS_VW

  PURPOSE:To pull in the data from MTL_SYSTEM_ITEMS_B and create a view for item search

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     1-SEP-2015   Pahwa, Nancy                Initially Created 
TMS# 20150825-00012
**************************************************************************/
AS
SELECT ' ' dummy, msib.inventory_item_id, msib.organization_id, msib.segment1 partNumber,
       'Product' TYPE,
       (SELECT XXWC_M_P_N_ATTR
          FROM XXWC_WHITE_C_M_AGV
         WHERE     ROWNUM = 1
               AND organization_id = 222
               AND inventory_item_id = msib.inventory_item_id)
          ManufacturerPartNumber,
       (SELECT XXWC_BRAND_ATTR
          FROM XXWC_WHITE_C_M_AGV
         WHERE     ROWNUM = 1
               AND organization_id = 222
               AND inventory_item_id = msib.inventory_item_id)
          Manufacturer,
      -- NULL ParentPartNumber,
       (SELECT XXWC_WEB_SEQUENCE_ATTR
          FROM XXWC_WHITE_C_M_AGV
         WHERE     inventory_item_id = msib.inventory_item_id
               AND organization_id = 222
               AND ROWNUM = 1)
          SEQUENCE,
      -- NULL ParentStoreIdentifier,
       'USD' CurrencyCode,
       (SELECT XXWC_W_S_D_ATTR
          FROM XXWC_WHITE_C_M_AGV
         WHERE     ROWNUM = 1
               AND organization_id = 222
               AND inventory_item_id = msib.INVENTORY_ITEM_ID)
          Name,
       msib.description ShortDescription,
       (SELECT XXWC_WEB_LONG_DESCRIPTION_ATTR
          FROM XXWC_WHITE_C_M_AGV
         WHERE     ROWNUM = 1
               AND organization_id = 222
               AND inventory_item_id = msib.INVENTORY_ITEM_ID)
          LongDescription,
          fnd_profile.VALUE ('XXWC_THUMBNAIL_PROD')
       || (SELECT XXWC_A_T_1_ATTR
             FROM XXWC_WHITE_C_M_AGV
            WHERE     ROWNUM = 1
                  AND organization_id = 222
                  AND inventory_item_id = msib.INVENTORY_ITEM_ID)
          Thumbnail,
          fnd_profile.VALUE ('XXWC_FULL_IMAGE_PROD')
       || (SELECT XXWC_A_F_1_ATTR
             FROM XXWC_WHITE_C_M_AGV
            WHERE     ROWNUM = 1
                  AND organization_id = 222
                  AND inventory_item_id = msib.INVENTORY_ITEM_ID)
          FullImage,
       'C62' QuantityMeasure,
       'LBS' WeightMeasure,
       unit_weight Weight,
       '1' Buyable,
       (SELECT XXWC_WEB_KEYWORDS_ATTR
          FROM XXWC_WHITE_C_M_AGV
         WHERE     inventory_item_id = msib.inventory_item_id
               AND organization_id = 222
               AND ROWNUM = 1)
          Keyword,
          msib.creation_date,
     msib.item_type,
          LISTAGG(mcr.cross_reference,',') within group (order by mcr.inventory_item_id) cross_reference,
          msib.primary_uom_code
   FROM MTL_SYSTEM_ITEMS_B MSIB
  left outer join mtl_cross_references MCR on
   MSIB.Inventory_Item_Id = MCR.inventory_item_id
  and mcr.cross_reference_type in ('VENDOR','MANUFACTURER','WC_XREF')
  where (trunc(msib.creation_date) > sysdate - 180 and msib.item_type = 'SPECIAL')

  /* where msib.inventory_item_id = 2929068*/
group by msib.inventory_item_id, msib.organization_id, msib.segment1 ,
       'Product', 'USD' ,    msib.description ,
       'C62' ,
       'LBS' ,
       unit_weight,
       '1' ,
       msib.creation_date,msib.item_type, msib.primary_uom_code
       union
       SELECT null dummy, msib.inventory_item_id, msib.organization_id, msib.segment1 partNumber,
       'Product' TYPE,
       (SELECT XXWC_M_P_N_ATTR
          FROM XXWC_WHITE_C_M_AGV
         WHERE     ROWNUM = 1
               AND organization_id = 222
               AND inventory_item_id = msib.inventory_item_id)
          ManufacturerPartNumber,
       (SELECT XXWC_BRAND_ATTR
          FROM XXWC_WHITE_C_M_AGV
         WHERE     ROWNUM = 1
               AND organization_id = 222
               AND inventory_item_id = msib.inventory_item_id)
          Manufacturer,
      -- NULL ParentPartNumber,
       (SELECT XXWC_WEB_SEQUENCE_ATTR
          FROM XXWC_WHITE_C_M_AGV
         WHERE     inventory_item_id = msib.inventory_item_id
               AND organization_id = 222
               AND ROWNUM = 1)
          SEQUENCE,
      -- NULL ParentStoreIdentifier,
       'USD' CurrencyCode,
       (SELECT XXWC_W_S_D_ATTR
          FROM XXWC_WHITE_C_M_AGV
         WHERE     ROWNUM = 1
               AND organization_id = 222
               AND inventory_item_id = msib.INVENTORY_ITEM_ID)
          Name,
       msib.description ShortDescription,
       (SELECT XXWC_WEB_LONG_DESCRIPTION_ATTR
          FROM XXWC_WHITE_C_M_AGV
         WHERE     ROWNUM = 1
               AND organization_id = 222
               AND inventory_item_id = msib.INVENTORY_ITEM_ID)
          LongDescription,
          fnd_profile.VALUE ('XXWC_THUMBNAIL_PROD')
       || (SELECT XXWC_A_T_1_ATTR
             FROM XXWC_WHITE_C_M_AGV
            WHERE     ROWNUM = 1
                  AND organization_id = 222
                  AND inventory_item_id = msib.INVENTORY_ITEM_ID)
          Thumbnail,
          fnd_profile.VALUE ('XXWC_FULL_IMAGE_PROD')
       || (SELECT XXWC_A_F_1_ATTR
             FROM XXWC_WHITE_C_M_AGV
            WHERE     ROWNUM = 1
                  AND organization_id = 222
                  AND inventory_item_id = msib.INVENTORY_ITEM_ID)
          FullImage,
       'C62' QuantityMeasure,
       'LBS' WeightMeasure,
       unit_weight Weight,
       '1' Buyable,
       (SELECT XXWC_WEB_KEYWORDS_ATTR
          FROM XXWC_WHITE_C_M_AGV
         WHERE     inventory_item_id = msib.inventory_item_id
               AND organization_id = 222
               AND ROWNUM = 1)
          Keyword,
          msib.creation_date,
     msib.item_type,
          LISTAGG(mcr.cross_reference,',') within group (order by mcr.inventory_item_id) cross_reference,
          msib.primary_uom_code
   FROM MTL_SYSTEM_ITEMS_B MSIB
  left outer join mtl_cross_references MCR on
   MSIB.Inventory_Item_Id = MCR.inventory_item_id
  and mcr.cross_reference_type in ('VENDOR','MANUFACTURER','WC_XREF')
  where  msib.item_type not in ('SPECIAL')
  /* where msib.inventory_item_id = 2929068*/
group by msib.inventory_item_id, msib.organization_id, msib.segment1 ,
       'Product', 'USD' ,    msib.description ,
       'C62' ,
       'LBS' ,
       unit_weight,
       '1' ,
       msib.creation_date,msib.item_type, msib.primary_uom_code;
/