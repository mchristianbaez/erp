---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_INV_PUBD_TRANSFER_V $
  Module Name : Inventory
  PURPOSE	  : INV PUBD TRANSFER-WC
  VERSION 	DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.1     14-Feb-2016       	Siva   		 TMS#20161221-00015 
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_INV_PUBD_TRANSFER_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_INV_PUBD_TRANSFER_V (ORG, ITEM_NUMBER, DESCRIPTION, QUANTITY, UOM, SHELF_LIFE_DAYS, DAYS_IN_PUBD, ITEM_COST, ITEM_EXTENDED_COST, VENDOR_NAME, MMT_TRANSACTION_ID, MSIB_INVENTORY_ITEM_ID, MSIB_ORGANIZATION_ID, BORN_ON_DATE)
AS
  SELECT
    mp.organization_code org,
    msib.concatenated_segments item_number,
    msib.description,
    --xxeis.eis_xxwc_inv_util_pkg.get_onhand_qty(mmt.inventory_item_id,mmt.organization_id,mmt.subinventory_code) quantity,--commented for version 1.1
    mmt.onhand_qty quantity, --added for version 1.1
    muom.unit_of_measure uom,
    msib.shelf_life_days,
    --ROUND((sysdate-mmt.transaction_date),2) days_in_pubd , --commented for version 1.1
    ROUND(sysdate-
    (SELECT MAX(mmt1.transaction_date)
    FROM apps.mtl_material_transactions mmt1
    WHERE mmt1.inventory_item_id = mmt.inventory_item_id
    AND mmt1.organization_id     = mmt.organization_id
    AND mmt1.subinventory_code   = mmt.subinventory_code
    ),2) days_in_pubd , --added for version 1.1
    NVL(apps.cst_cost_api.get_item_cost (1, msib.inventory_item_id, msib.organization_id), 0) item_cost ,
    --(xxeis.eis_xxwc_inv_util_pkg.get_onhand_qty(mmt.inventory_item_id,mmt.organization_id,mmt.subinventory_code))*(ROUND(NVL(apps.cst_cost_api.get_item_cost (1, msib.inventory_item_id, msib.organization_id), 0),2)) item_extended_cost, --commented for version 1.1
    (mmt.onhand_qty)*(ROUND(NVL(apps.cst_cost_api.get_item_cost (1, msib.inventory_item_id, msib.organization_id), 0),2)) item_extended_cost, --added for version 1.1
    (SELECT ap.vendor_name
    FROM apps.mtl_cross_references mic,
      apps.ap_suppliers ap
    WHERE mic.inventory_item_id  = msib.inventory_item_id
    AND mic.cross_reference_type = 'VENDOR'
    AND mic.attribute1           = ap.segment1
    AND rownum                   =1
    )vendor_name,
    --mmt.transaction_id mmt_transaction_id, --commented for version 1.1
    to_number(NULL) mmt_transaction_id, --added for version 1.1
    msib.inventory_item_id msib_inventory_item_id,
    msib.organization_id msib_organization_id
    ,mmt.orig_date_received born_on_date --added for version 1.1
  FROM
    (SELECT SUM(primary_transaction_quantity) onhand_qty,
      MAX(orig_date_received) orig_date_received,
      mmt.transaction_uom_code,
      mmt.inventory_item_id,
      mmt.organization_id,
      mmt.subinventory_code
    FROM apps.mtl_onhand_quantities_detail mmt
    WHERE subinventory_code ='PUBD'
    GROUP BY mmt.transaction_uom_code,
      mmt.inventory_item_id,
      mmt.organization_id,
      mmt.subinventory_code
    ) mmt, --added for version 1.1
    --apps.mtl_material_transactions mmt, --commented for version 1.1
    apps.mtl_system_items_kfv msib,
    apps.mtl_parameters mp ,
    apps.mtl_units_of_measure muom
  WHERE mmt.organization_id    = msib.organization_id
  AND mmt.inventory_item_id    = msib.inventory_item_id
  AND msib.organization_id     = mp.organization_id --added for version 1.1
  AND mmt.transaction_uom_code = muom.uom_code(+)
    --mmt.transaction_type_id IN (2,41) --commented for version 1.1
    --AND mmt.subinventory_code      ='PUBD' --commented for version 1.1
  AND EXISTS
    (SELECT 1
    FROM apps.mtl_material_transactions mmtr
    WHERE mmtr.inventory_item_id  = mmt.inventory_item_id
    AND mmtr.organization_id      = mmt.organization_id
    AND mmtr.subinventory_code    = mmt.subinventory_code
    AND mmtr.transaction_type_id IN (2,41,31)
    AND mmtr.subinventory_code    ='PUBD'
    ) --added for version 1.1
/
