/*
 TMS:   20170130-00247
 Scope: GSC, Rebates - Cleanup existing R&R LOB receipts
 Date: 01/30/2017
*/
set serveroutput on size 1000000;
declare
 --
 l_partition_sdw  varchar2(30) :='RR';
 l_count number :=0;
 l_db_name varchar2(30) :=Null;
 --
     procedure print_log(p_message in varchar2) is
     begin
      if apps.fnd_global.conc_request_id >0 then
       apps.fnd_file.put_line(fnd_file.log, p_message);
      else
       dbms_output.put_line(p_message);
      end if;
     end;
 --
begin
     --
     print_log('');     
     print_log('Begin flush of Rebates SDW Receipts.');
    -- 
    select upper(name) into l_db_name from v$database;
    --
    if l_db_name in ('EBSQA', 'EBSDEV') then
      --
      l_partition_sdw :='USABB';      
      --      
      begin
         --
         select count(1) into l_count from xxcus.xxcus_rebate_receipt_sdw_tbl partition (USABB);    
         print_log('Total rows available in partition '||l_partition_sdw||':'||l_count);       
       --      
       -- delete xxcus.xxcus_rebate_receipt_sdw_tbl partition (RR);  
       execute immediate '  Alter table xxcus.xxcus_rebate_receipt_sdw_tbl truncate partition ' ||l_partition_sdw;       
        -- 
        print_log('Partition data removed for LOB :'||l_partition_sdw);                  
        print_log('');   
        --     
        select count(1) into l_count from xxcus.xxcus_rebate_receipt_sdw_tbl partition (USABB);    
         print_log('Total rows available in partition '||l_partition_sdw||':'||l_count);    
        print_log('');                 
        --
         commit;
         --                
      exception
       when others then
        rollback;
        print_log('Error in deleting xxcus.xxcus_rebate_receipt_sdw_tbl partition (RR), message ='||sqlerrm);
      end;  
       --  
    elsif l_db_name ='EBSPRD' then
      --
      l_partition_sdw :='RR';      
      --   
      begin
         --
         select count(1) into l_count from xxcus.xxcus_rebate_receipt_sdw_tbl partition (RR);    
         print_log('Total rows available in partition '||l_partition_sdw||':'||l_count);       
       --      
       -- delete xxcus.xxcus_rebate_receipt_sdw_tbl partition (RR);  
       execute immediate '  Alter table xxcus.xxcus_rebate_receipt_sdw_tbl truncate partition ' ||l_partition_sdw;       
        -- 
        print_log('Partition data removed for LOB :'||l_partition_sdw);                  
        print_log('');   
        --     
        select count(1) into l_count from xxcus.xxcus_rebate_receipt_sdw_tbl partition (RR);    
         print_log('Total rows available in partition '||l_partition_sdw||':'||l_count);    
        print_log('');                 
        --
         commit;
         --                
      exception
       when others then
        rollback;
        print_log('Error in deleting xxcus.xxcus_rebate_receipt_sdw_tbl partition (RR), message ='||sqlerrm);
      end;  
       --      
    else
     Null; -- do nothing in other instances.
    end if; 
    --
     print_log('End flush of Rebates SDW Receipts.');
     print_log('');
     --    
exception
 when others then
  rollback;
  print_log('Outer block, message ='||sqlerrm);
end;
/