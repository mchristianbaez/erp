/***************************************************************************************************************
Table: APPS.XXWC_AP_EXPENSE_PO_WRT_OFF_TBL
Description: Creating this table to track expense PO closure process.

================================================================================================================
VERSION DATE               AUTHOR(S)       DESCRIPTION
------- -----------------  --------------- ---------------------------------------------------------------------
 1.0     18-Oct-2017        P.Vamshidhar    TMS#20171017-00058 - Intangible Write-Off Request
****************************************************************************************************************/
SET SERVEROUT ON  size 100000

DROP TABLE XXWC.XXWC_AP_EXPENSE_PO_WRT_OFF_TBL
/
ALTER SESSION SET CURRENT_SCHEMA=APPS
/
BEGIN MO_GLOBAL.SET_POLICY_CONTEXT('S','162'); END; 
/
CREATE TABLE XXWC.XXWC_AP_EXPENSE_PO_WRT_OFF_TBL
AS
SELECT vendor_name,
enabled_flag,
end_date_Active,
hold_flag,
vendor_site_code,
po_site_inactive_date,
org_id,
TERMS_ID,
vendor_id,
pay_vendor_site_id,
pay_vendor_site_code,
pay_site_inactive_date,
po_vendor_site_id,
po_header_id,
po_line_id,
line_location_id,
po_distribution_id,
INVENTORY_ITEM_ID,
currency_code,
SHIP_TO_LOCATION_CODE,
PO_NUMBER,
line_num,
item_number,
qty,
amt,
quantity_received,
quantity_billed,
quantity,
price_override,
unit_price,
shipment_num,
distribution_num,
Receipt_date,
INVOICE_ID,
INVOICE_LINE_ID,
SITE_ACTIVATED,
PROCESS_FLAG,
Error_Msg
from (SELECT pv.vendor_name,
          pv.enabled_flag,
          pv.end_date_Active,
          pv.hold_flag,
          pvs.vendor_site_code,
          pvs.inactive_date po_site_inactive_date,
          poh.org_id,
          poh.TERMS_ID,
          poh.vendor_id,
          (SELECT vendor_site_id
             FROM po_vendor_sites_all
            WHERE     vendor_id = poh.vendor_id
                  AND primary_pay_site_flag = 'Y'
                  AND org_id = 162)
             pay_vendor_site_id,
          (SELECT vendor_site_code
             FROM po_vendor_sites_all
            WHERE     vendor_id = poh.vendor_id
                  AND primary_pay_site_flag = 'Y'
                  AND org_id = 162)
             pay_vendor_site_code,
          (SELECT inactive_date
             FROM po_vendor_sites_all
            WHERE     vendor_id = poh.vendor_id
                  AND primary_pay_site_flag = 'Y'
                  AND org_id = 162)
             pay_site_inactive_date,
          poh.vendor_site_id po_vendor_site_id,
          poh.po_header_id,
          pol.po_line_id,
          poll.line_location_id,
          pod.po_distribution_id,
          pol.item_id INVENTORY_ITEM_ID,
          poh.currency_code currency_code,
          poll.SHIP_TO_LOCATION_CODE,
          poh.segment1 PO_NUMBER,
          pol.line_num,
          pol.item_number,
          (NVL (poll.quantity_received, 0) - NVL (poll.quantity_billed, 0))
             qty,
          (  (NVL (poll.quantity_received, 0) - NVL (poll.quantity_billed, 0))
           * pol.unit_price)
             amt,
          poll.quantity_received,
          poll.quantity_billed,
          poll.quantity,
          poll.price_override,
          pol.unit_price,
          poll.shipment_num,
          pod.distribution_num,
          (SELECT MAX (transaction_date) receipt_date
             FROM rcv_transactions
            WHERE     1 = 1
                  AND po_line_location_id = poll.line_location_id
                  AND transaction_type = 'RECEIVE')
             Receipt_date,
          0 INVOICE_ID,
          0 INVOICE_LINE_ID,
          ' ' SITE_ACTIVATED,
          ' ' PROCESS_FLAG,
          ' ' Error_Msg
     FROM po_distributions_all pod,
          po_line_locations_v poll,
          po_lines_v pol,
          po_headers_all poh,
          po_vendors pv,
          po_vendor_sites_all pvs
    WHERE     pol.po_header_id = poh.po_header_id
          AND poh.vendor_id = pv.vendor_id
          AND poh.vendor_site_id = pvs.vendor_site_id
          AND pv.vendor_id = pvs.vendor_id
          AND poll.po_line_id = pol.po_line_id
          AND pod.line_location_id = poll.line_location_id
          AND poh.type_lookup_code = 'STANDARD'
          AND poh.segment1 <> '289340'
          AND poh.org_id = 162
          AND poll.closed_date IS NULL
          AND NVL (poll.accrue_on_receipt_flag, 'N') = 'N'
          AND pod.destination_type_code = 'EXPENSE'
          AND NVL (pod.accrued_flag, 'N') = 'N'
          AND (   (  NVL (poll.quantity_received, 0)
                   - NVL (poll.quantity_billed, 0)) > 0
               OR (    NVL (poll.quantity_billed, 0) > 0
                   AND NOT EXISTS
                          (SELECT 1
                             FROM ap_invoice_lines_all
                            WHERE po_line_location_id = poll.line_location_id)))) TB
WHERE 				  TB.RECEIPT_DATE BETWEEN 
                               TO_DATE ('02/01/2017 00:00:00',
                                        'MM/DD/YYYY HH24:MI:SS')
							 and TO_DATE ('04/30/2017 00:00:00',
                                        'MM/DD/YYYY HH24:MI:SS')
/
alter table XXWC.XXWC_AP_EXPENSE_PO_WRT_OFF_TBL modify (SITE_ACTIVATED VARCHAR2(20), PROCESS_FLAG VARCHAR2(100), ERROR_MSG VARCHAR2(100))
/
DECLARE
   -- Local Variables Declaration
   ln_invoice_id         AP_INVOICES_INTERFACE.INVOICE_ID%TYPE;
   lvc_vendor_site_rec   AP_VENDOR_PUB_PKG.r_vendor_site_rec_type;
   lvc_return_status     VARCHAR2 (4000);
   lvc_msg_data          VARCHAR2 (32000);
   ln_msg_count          NUMBER;
   ln_vendor_site_id     APPS.AP_SUPPLIER_SITES_ALL.VENDOR_SITE_ID%TYPE;
   ln_user_id            FND_USER.USER_ID%TYPE := 16991;                -- Ram
   ln_resp_id            FND_RESPONSIBILITY_VL.RESPONSIBILITY_ID%TYPE
                            := 50983;         --HDS Purchasing Super User - WC
   ln_po_appl_id         FND_APPLICATION.APPLICATION_ID%TYPE := 201;     -- PO
   ln_line_number        AP_INVOICE_LINES_INTERFACE.LINE_NUMBER%TYPE;
   ln_site_count         NUMBER := 0;
   ln_inv_count          NUMBER := 0;
   ln_inv_line_count     NUMBER := 0;

   -- Cursor Declaration
   -- Primary Vendor Pay Sites list to activate
   CURSOR CUR_AP_SUPP_SITES
   IS
      SELECT DISTINCT XAET.VENDOR_ID, XAET.PAY_VENDOR_SITE_ID
        FROM XXWC.XXWC_AP_EXPENSE_PO_WRT_OFF_TBL XAET
       WHERE EXISTS
                (SELECT 1
                   FROM APPS.AP_SUPPLIER_SITES_ALL ASSA,
                        APPS.AP_SUPPLIERS APS
                  WHERE     ASSA.ORG_ID = 162
                        AND ASSA.VENDOR_ID = XAET.VENDOR_ID
                        AND ASSA.VENDOR_ID = APS.VENDOR_ID
                        AND APS.ENABLED_FLAG = 'Y'
                        AND ASSA.VENDOR_SITE_ID = XAET.pay_vendor_site_id
                        AND ASSA.PRIMARY_PAY_SITE_FLAG = 'Y'
                        AND NVL (XAET.RECEIPT_DATE, SYSDATE) BETWEEN 
                               TO_DATE ('02/01/2017 00:00:00',
                                        'MM/DD/YYYY HH24:MI:SS')
							 AND TO_DATE ('04/30/2017 00:00:00',
                                        'MM/DD/YYYY HH24:MI:SS')
                        AND NVL (ASSA.INACTIVE_DATE, SYSDATE + 1) < SYSDATE);

   --Cursor for Invoice Headers
   CURSOR CUR_INV_HDR_DATA
   IS
      SELECT DISTINCT PO_NUMBER,
                      TERMS_ID,
                      VENDOR_ID,
                      PAY_VENDOR_SITE_ID,
                      CURRENCY_CODE
        FROM XXWC.XXWC_AP_EXPENSE_PO_WRT_OFF_TBL XAET
       WHERE NVL (RECEIPT_DATE, SYSDATE) BETWEEN 
                               TO_DATE ('02/01/2017 00:00:00',
                                        'MM/DD/YYYY HH24:MI:SS')
							 AND TO_DATE ('04/30/2017 00:00:00',
                                        'MM/DD/YYYY HH24:MI:SS');

   --Cursor for Invoice lines
   CURSOR CUR_INV_LINE_DATA (
      p_po_number    VARCHAR2)
   IS
      SELECT ROWID VROWID,
             PO_HEADER_ID,
             PO_LINE_ID,
             LINE_LOCATION_ID,
             INVENTORY_ITEM_ID,
             QTY,
             AMT
        FROM XXWC.XXWC_AP_EXPENSE_PO_WRT_OFF_TBL XAET
       WHERE     XAET.PO_NUMBER = P_PO_NUMBER
             AND NVL (RECEIPT_DATE, SYSDATE) BETWEEN 
                               TO_DATE ('02/01/2017 00:00:00',
                                        'MM/DD/YYYY HH24:MI:SS')
							 AND TO_DATE ('04/30/2017 00:00:00',
                                        'MM/DD/YYYY HH24:MI:SS');
BEGIN
   DBMS_OUTPUT.PUT_LINE (
         'Vendor Pay Site Activation Begin '
      || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

   -- Vendor Primary Pay Site Activation - Begin
   BEGIN
      fnd_global.apps_initialize (user_id        => ln_user_id,
                                  resp_id        => ln_resp_id,
                                  resp_appl_id   => ln_po_appl_id);
      apps.mo_global.init ('PO');
      mo_global.set_policy_context ('S', '162');

      FOR REC_AP_SUPP_SITES IN CUR_AP_SUPP_SITES
      LOOP
         ln_vendor_site_id := rec_ap_supp_sites.pay_vendor_site_id;
         lvc_vendor_site_rec.inactive_date := TRUNC (SYSDATE) + 2;

         AP_VENDOR_PUB_PKG.Update_Vendor_Site (
            p_api_version        => 1.0,
            p_init_msg_list      => fnd_api.g_true,
            p_commit             => fnd_api.g_false,
            p_validation_level   => fnd_api.g_valid_level_full,
            x_return_status      => lvc_return_status,
            x_msg_count          => ln_msg_count,
            x_msg_data           => lvc_msg_data,
            p_vendor_site_rec    => lvc_vendor_site_rec,
            p_vendor_site_id     => ln_vendor_site_id);

         IF ln_msg_count > 0
         THEN
            DBMS_OUTPUT.put_line (
               SUBSTR (
                     'Failed to Update Activate the Site '
                  || rec_ap_supp_sites.pay_vendor_site_id
                  || ':'
                  || lvc_return_status
                  || '-'
                  || ln_msg_count
                  || '-'
                  || lvc_msg_data,
                  1,
                  255));
            ROLLBACK;
         ELSE
            DBMS_OUTPUT.put_line (
               SUBSTR (
                     'Activated Vendor Pay Site '
                  || rec_ap_supp_sites.pay_vendor_site_id
                  || ':'
                  || lvc_return_status
                  || '-'
                  || ln_msg_count
                  || '-'
                  || lvc_msg_data,
                  1,
                  255));
         END IF;

         UPDATE XXWC.XXWC_AP_EXPENSE_PO_WRT_OFF_TBL
            SET site_activated = 'Yes'
          WHERE     vendor_id = rec_ap_supp_sites.vendor_id
                AND pay_vendor_site_id = rec_ap_supp_sites.pay_vendor_site_id;

         ln_site_count := ln_site_count + 1;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         DBMS_OUTPUT.put_line (
               'Error Occured: Vendor Site Id: '
            || ln_vendor_site_id
            || ' Error Details:'
            || SUBSTR (SQLERRM, 1, 250));
         raise_application_error (
            '-20001',
            'Error Occured While activating Vendor Sites');
   END;

   DBMS_OUTPUT.PUT_LINE ('Number of Sites Activated ' || ln_site_count);
   DBMS_OUTPUT.PUT_LINE (
         'Vendor Pay Site Activation End '
      || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
   -- Vendor Primary Pay Site Activation - End.


   -- Inserting Invoice header and lines data into Interface tables.
   DBMS_OUTPUT.PUT_LINE (
         'Inserting Invoices data into Interface table - Begin '
      || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

   FOR REC_INV_HDR_DATA IN CUR_INV_HDR_DATA
   LOOP
      ln_invoice_id := NULL;

      SELECT AP_INVOICES_INTERFACE_S.NEXTVAL INTO ln_invoice_id FROM DUAL;

      -- Invoice Header inserting.
      BEGIN
         INSERT INTO ap_invoices_interface (invoice_id,
                                            invoice_num,
                                            invoice_type_lookup_code,
                                            TERMS_ID,
                                            vendor_id,
                                            vendor_site_id,
                                            invoice_amount,
                                            invoice_currency_code,
                                            PAY_GROUP_LOOKUP_CODE,
                                            source,
                                            GROUP_ID,
                                            creation_date,
                                            created_by,
                                            last_update_date,
                                            last_updated_by)
              VALUES (ln_invoice_id,
                      'WC_INV_PO_' || rec_inv_hdr_data.po_number,
                      'STANDARD',
                      rec_inv_hdr_data.terms_id,
                      rec_inv_hdr_data.vendor_id,
                      rec_inv_hdr_data.pay_vendor_site_id,
                      0,
                      rec_inv_hdr_data.currency_code,
                      'ZERO CHECK',
                      'MANUAL INVOICE ENTRY',
                      'WC_INTANGIBLE_PO_WRITE_OFF',
                      SYSDATE,
                      ln_user_id,
                      SYSDATE,
                      ln_user_id);

         ln_inv_count := ln_inv_count + 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            ROLLBACK;
            DBMS_OUTPUT.put_line (
                  'Error Occured: PO Number: '
               || rec_inv_hdr_data.po_number
               || ' Error Details:'
               || SUBSTR (SQLERRM, 1, 250));
            raise_application_error (
               '-20002',
               'Error Occured inserting data into Invoice headers interface table');
      END;

      BEGIN
         -- Invoice Lines inserting.
         ln_line_number := 1;

         FOR REC_INV_LINE_DATA
            IN CUR_INV_LINE_DATA (rec_inv_hdr_data.po_number)
         LOOP
            -- Inserting po line info. into invoice lines interface
            INSERT INTO ap_invoice_lines_interface (invoice_id,
                                                    invoice_line_id,
                                                    line_number,
                                                    line_type_lookup_code,
                                                    amount,
                                                    po_header_id,
                                                    po_line_id,
                                                    po_line_location_id,
                                                    inventory_item_id,
                                                    quantity_invoiced,
                                                    creation_date,
                                                    created_by,
                                                    last_update_date,
                                                    last_updated_by)
                 VALUES (ln_invoice_id,
                         AP_INVOICE_LINES_INTERFACE_S.NEXTVAL,
                         ln_line_number,
                         'ITEM',
                         ROUND (rec_inv_line_data.amt, 2),
                         rec_inv_line_data.po_header_id,
                         rec_inv_line_data.po_line_id,
                         rec_inv_line_data.line_location_id,
                         rec_inv_line_data.inventory_item_id,
                         rec_inv_line_data.qty,
                         SYSDATE,
                         ln_user_id,
                         SYSDATE,
                         ln_user_id);


            UPDATE xxwc.xxwc_ap_expense_po_wrt_off_tbl
               SET invoice_id = ln_invoice_id,
                   invoice_line_id = AP_INVOICE_LINES_INTERFACE_S.CURRVAL,
                   process_flag = 'Yes'
             WHERE ROWID = REC_INV_LINE_DATA.vrowid;

            ln_inv_line_count := ln_inv_line_count + 1;

            ln_line_number := ln_line_number + 1;

            -- Inserting offset line info. into invoice lines interface
            INSERT INTO ap_invoice_lines_interface (invoice_id,
                                                    invoice_line_id,
                                                    line_number,
                                                    line_type_lookup_code,
                                                    amount,
                                                    dist_code_combination_id,
                                                    creation_date,
                                                    created_by,
                                                    last_update_date,
                                                    last_updated_by)
                 VALUES (ln_invoice_id,
                         AP_INVOICE_LINES_INTERFACE_S.NEXTVAL,
                         ln_line_number,
                         'ITEM',
                         ROUND (-rec_inv_line_data.amt, 2),
                         108738,
                         SYSDATE,
                         ln_user_id,
                         SYSDATE,
                         ln_user_id);

            ln_inv_line_count := ln_inv_line_count + 1;
            ln_line_number := ln_line_number + 1;
         END LOOP;
      EXCEPTION
         WHEN OTHERS
         THEN
            ROLLBACK;
            DBMS_OUTPUT.put_line (
                  'Error Occured: PO Number: '
               || rec_inv_hdr_data.po_number
               || ' Error Details:'
               || SUBSTR (SQLERRM, 1, 250));

            raise_application_error (
               '-20003',
               'Error Occured inserting data into Invoice lines interface table');
      END;
   END LOOP;

   COMMIT;

   DBMS_OUTPUT.PUT_LINE (
      'Number of Invoice Headers Inserted ' || ln_inv_count);
   DBMS_OUTPUT.PUT_LINE (
      'Number of Invoice Lines Inserted ' || ln_inv_line_count);

   DBMS_OUTPUT.PUT_LINE (
         'Inserting Invoices data into Interface table - End '
      || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.PUT_LINE ('Error Occured: ' || SUBSTR (SQLERRM, 1, 250));
END;
/