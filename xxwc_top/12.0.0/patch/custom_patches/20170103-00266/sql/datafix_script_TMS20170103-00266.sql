 /*************************************************************************
   *   $Header datafix_20170103-00266.sql$
   *   Module Name: datafix_TMS20170103-00266.sql
   *
   *   PURPOSE:   This is a datafix to fix all invoices junk data in attribute12 so 
                that ARS extract can send all invoices .
   *
   *   REVISIONS:
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -------------------------
   *   1.0        03-Jan-2017  Neha Saini             Initial Version TMS# TMS20170103-00266 datafix script
   * ***************************************************************************/
SET SERVEROUTPUT ON
BEGIN
   UPDATE AR.RA_CUSTOMER_TRX_ALL
      SET attribute12 = NULL,
          last_update_date = SYSDATE,
          last_updated_by = 1290
    WHERE     1 = 1
          AND attribute12 IS NOT NULL
          AND trunc(CREATION_DATE) > '21-DEC-16'
          AND LENGTH (attribute12) > 10;

   DBMS_OUTPUT.PUT_LINE (
      'Total records updated in RA_CUSTOMER_TRX_ALL ' || SQL%ROWCOUNT);
   COMMIT;

   UPDATE APPS.RA_INTERFACE_LINES_ALL
      SET HEADER_attribute12 = NULL,
          last_update_date = SYSDATE,
          last_updated_by = 1290
    WHERE  interface_line_context = 'ORDER ENTRY';

   DBMS_OUTPUT.PUT_LINE (
      'Total records updated in RA_INTERFACE_LINES_ALL ' || SQL%ROWCOUNT);
   COMMIT;
END;
/