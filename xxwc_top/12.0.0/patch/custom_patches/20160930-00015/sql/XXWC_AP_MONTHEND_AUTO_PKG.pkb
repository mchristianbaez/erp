CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AP_MONTHEND_AUTO_PKG
AS
   /*************************************************************************
   *  Copyright (c) 2016 HD Supply Inc.
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_AP_MONTHEND_AUTO_PKG.pkb $
   *   Module Name: XXWC_AP_MONTHEND_AUTO_PKG.pkb
   *
   *   PURPOSE:    This package is used to Automate AP month end process.
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/15/2016  Neha Saini               Initial Version TMS# 20160930-00015
   * ***************************************************************************/
   /*****************************
 -- GLOBAL VARIABLES
 ******************************/

   g_error_message     VARCHAR2 (32000);
   g_location          VARCHAR2 (10000);
   g_ledger_id         NUMBER;
   g_application_id    NUMBER;
   G_EXCEPTION         EXCEPTION;
   g_user_id           NUMBER;
   --Global variable to store the org id and conc request id
   g_conc_request_id   NUMBER := FND_GLOBAL.CONC_REQUEST_ID;


   /*************************************************************************
   *  Procedure : Write_Error
   *
   * PURPOSE:   This procedure logs error message in Custom Error table
   * Parameter:
   *        IN
   *            p_debug_msg      -- Debug Message
   * REVISIONS:
   * Ver        Date        Author                     Description
   *  ---------  ----------  ---------------         -------------------------
   *  1.0        11/15/2016  Neha Saini               Initial Version TMS# 20160930-00015
  ************************************************************************/
   --add message to Error table
   PROCEDURE write_error (p_debug_msg IN VARCHAR2)
   IS
      l_err_callfrom    VARCHAR2 (100) DEFAULT 'XXWC_AP_MONTHEND_AUTO_PKG';
      l_err_callpoint   VARCHAR2 (100) DEFAULT 'START';
      l_distro_list     VARCHAR2 (100)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => g_conc_request_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running XXWC_AP_MONTHEND_AUTO_PKG with PROGRAM ERROR ',
         p_distribution_list   => l_distro_list,
         p_module              => 'AP');
   END write_error;

   /*************************************************************************
   *   Procedure : Write_output
   *
   *   PURPOSE:   This procedure is used for adding message to concurrent output file
   *   Parameter:
   *          IN
   *              p_debug_msg      -- Debug Message
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *  ---------  ----------  ---------------         -------------------------
   *   1.0        11/15/2016  Neha Saini               Initial Version TMS# 20160930-00015
   * ************************************************************************/

   PROCEDURE Write_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      IF NVL (g_conc_request_id, 0) <= 0
      THEN
         DBMS_OUTPUT.PUT_LINE (p_debug_msg);
      ELSE
         fnd_file.put_line (fnd_file.output, p_debug_msg);
         fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      END IF;
   END Write_output;

   /**************************************************************************
   *   procedure Name: open_next_period
   *  *   Parameter:
   *          IN   p_period_name
   *          OUT  None
   *   PURPOSE:  This procedure is used to open next period for AP
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/15/2016  Neha Saini             Initial Version TMS# 20160930-00015
   * ***************************************************************************/
   PROCEDURE open_next_period (p_period_name VARCHAR2)
   IS
   BEGIN
      g_error_message := NULL;
      g_location := 'opening next period now';
      write_output (g_location);

      UPDATE gl_period_statuses
         SET closing_status = 'O',
             last_updated_by = fnd_global.user_id,
             last_update_date = SYSDATE,
             last_update_login = fnd_global.login_id
       WHERE     set_of_books_id = g_ledger_id
             AND period_name = p_period_name
             AND application_id = g_application_id
             AND closing_status <> 'O';

     

      IF SQL%ROWCOUNT > 0
      THEN
         g_location := 'Next period ' || p_period_name || ' Open Successfully';
         write_output (g_location);
      ELSE
         g_location :=
            'Next period ' || p_period_name || ' is not Open Yet . ';
         write_output (g_location);
      END IF;
      
      COMMIT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         g_error_message :=
               'Error at open next period for '
            || p_period_name
            || '-'
            || 'Period is either already open or does not exist. please check gl_perid_statuses table.'
            || ' - '
            || SQLERRM;
      WHEN TOO_MANY_ROWS
      THEN
         g_error_message :=
               'Error at open next period for '
            || p_period_name
            || '-'
            || ' too many rowas returned for this period .please check gl_perid_statuses table.'
            || ' - '
            || SQLERRM;
      WHEN OTHERS
      THEN
         g_error_message :=
               'Error at open next period for '
            || p_period_name
            || ' - '
            || SQLERRM;
   END open_next_period;

   /**************************************************************************
 *   procedure Name: close_curr_period
 *  *   Parameter:
 *          IN    p_period_name,,p_next_period
 *          OUT   None
 *   PURPOSE:  This procedure is used to close current period for AP
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
 *   1.0        11/15/2016  Neha Saini             Initial Version TMS# 20160930-00015
 * ***************************************************************************/
   PROCEDURE close_curr_period (p_period_name    VARCHAR2,
                                p_next_period    VARCHAR2)
   IS
      v_app_id              NUMBER;
      v_resp_id             NUMBER;
      v_resp_name           VARCHAR2 (100);
      v_request_id          NUMBER;
      v_app_name            VARCHAR2 (20);
      v_req_phase           VARCHAR2 (100);
      v_req_status          VARCHAR2 (100);
      v_req_dev_phase       VARCHAR2 (100);
      v_req_dev_status      VARCHAR2 (100);
      v_req_message         VARCHAR2 (100);
      v_req_return_status   BOOLEAN;
      l_exception_flag      VARCHAR2 (50);
      l_process_flag        VARCHAR2 (500);
      l_start_date          DATE;
      l_end_date            DATE;
      l_message             VARCHAR2 (5000);
      l_xml_layout          BOOLEAN;
   BEGIN
      g_error_message := NULL;
      g_location := 'start " close current period "  now';
      write_output (g_location);

      --submit �Unaccounted Transactions Report (XML)

      g_user_id := FND_GLOBAL.USER_ID;


      g_location := 'get user id ' || g_user_id;
      write_output (g_location);

      BEGIN
         mo_global.init ('SQLAP');
         mo_global.set_policy_context ('S', 162);

         SELECT fa.application_id,
                fr.responsibility_id,
                fr.responsibility_name,
                fa.application_short_name
           INTO v_app_id,
                v_resp_id,
                v_resp_name,
                v_app_name
           FROM apps.fnd_responsibility_vl fr, apps.fnd_application fa
          WHERE     responsibility_name LIKE 'HDS Payables Close - Global'
                AND fr.application_id = fa.application_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_app_id := NULL;
            v_resp_id := NULL;
            v_resp_name := NULL;
            v_app_name := NULL;
      END;

      -- Initializing apps
      fnd_global.apps_initialize (user_id        => g_user_id,
                                  resp_id        => v_resp_id,
                                  resp_appl_id   => v_app_id);



      BEGIN
         SELECT start_date, end_date
           INTO l_start_date, l_end_date
           FROM apps.gl_period_statuses glp
          WHERE     1 = 1
                AND period_name = p_period_name
                AND glp.ledger_id = g_ledger_id
                AND glp.application_id = v_app_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_error_message :=
                  'Error while getting period start date and end date fpr period '
               || p_period_name;
            RAISE g_exception;
      END;

      g_location := 'period start date  ' || l_start_date;
      write_output (g_location);
      g_location := 'period end date ' || l_end_date;
      write_output (g_location);

      g_location :=
         'Now submitting request for Unaccounted Transactions Report (XML)';
      write_output (g_location);
      g_location :=
            'Parameters for  for Unaccounted Transactions Report (XML) g_ledger_id '
         || g_ledger_id;
      write_output (g_location);
      g_location :=
            'Parameters for Unaccounted Transactions Report (XML) p_period_name '
         || p_period_name;
      write_output (g_location);
      g_location :=
            'Parameters for Unaccounted Transactions Report (XML) p_next_period '
         || p_next_period;
      write_output (g_location);
      --Environment Set up
      FND_REQUEST.SET_ORG_ID (162);
      --adding Template to the report
      l_xml_layout :=
         fnd_request.add_layout (template_appl_name   => v_app_name,
                                 template_code        => 'APXUATR_RTF',
                                 template_language    => 'En',
                                 template_territory   => 'US',
                                 output_format        => 'PDF');

      IF l_xml_layout
      THEN
         g_location :=
            'l_xml_layout for template for Unaccounted Transactions Report (XML) done successfully ';
         write_output (g_location);

         --submitted �Unaccounted Transactions Report (XML)

         v_request_id :=
            fnd_request.submit_request (application   => v_app_name, /* Short name of the application under which the program is registered in this it will be ONT since the Reserve Order program is registered under order management*/
                                        program       => 'APXUATR_XMLP', -- Short name of the concurrent program needs to submitted
                                        description   => NULL,
                                        start_time    => NULL,
                                        sub_request   => NULL,
                                        argument1     => 1000,
                                        argument2     => g_ledger_id, /* Ledger ID */
                                        argument3     => g_ledger_id, /* Ledger ID */
                                        argument4     => '',
                                        argument5     => '',
                                        argument6     => p_period_name, --period name
                                        argument7     => 'Y',          --sweep
                                        argument8     => p_next_period, -- Sweep To Period
                                        argument9     => 'Y',  -- Debug switch
                                        argument10    => 'N',  -- Trace switch
                                        argument11    => CHR (0));



         COMMIT;

         g_location := 'Concurrent Program Req ID' || v_request_id;
         write_output (g_location);


         -- Concurrent Program Status
         IF v_request_id <> 0 AND v_request_id IS NOT NULL
         THEN
            LOOP
               v_req_return_status :=
                  fnd_concurrent.wait_for_request (v_request_id, -- request ID for which results need to be known
                                                   60, --interval parameter  time between checks default is 60 Seconds
                                                   0, -- Max wait default is 0 , this is the maximum amount of time a program should wait for it to get completed
                                                   v_req_phase,
                                                   v_req_status,
                                                   v_req_dev_phase,
                                                   v_req_dev_status,
                                                   v_req_message);
               /*Others are output parameters to get the phase and status of the submitted concurrent program*/
               EXIT WHEN    UPPER (v_req_phase) = 'COMPLETED'
                         OR UPPER (v_req_status) IN ('CANCELLED',
                                                     'ERROR',
                                                     'TERMINATED');
            END LOOP;
         END IF;

         g_location := 'Concurrent Program Status' || v_req_status;
         write_output (g_location);

         --check if Unaccounted Transactions Report (XML)  completed successfully if not end program with warning
         IF v_req_status IN ('CANCELLED', 'ERROR', 'TERMINATED')
         THEN
            g_error_message :=
               'Program Unaccounted Transactions Report (XML)  did not completed Successfully';
            RAISE g_exception;
         END IF;
      --submitted �Unaccounted Transactions Report (XML)
      END IF;

      --Now then submit �AP_PERIOD_CLOSE_PKG.process_period to check any unposted items�
      g_location :=
            'Now calling AP_PERIOD_CLOSE_PKG.process_period g_ledger_id  '
         || g_ledger_id;
      write_output (g_location);
      g_location :=
            'Parameters AP_PERIOD_CLOSE_PKG.process_period p_period_name '
         || p_period_name;
      write_output (g_location);
      g_location :=
            'Parameters  AP_PERIOD_CLOSE_PKG.process_period l_start_date '
         || l_start_date;
      write_output (g_location);
      g_location :=
            'Parameters  AP_PERIOD_CLOSE_PKG.process_period l_end_date '
         || l_end_date;
      write_output (g_location);
      g_location :=
            'Parameters  AP_PERIOD_CLOSE_PKG.process_period p_next_period '
         || p_next_period;
      write_output (g_location);

      APPS.AP_PERIOD_CLOSE_PKG.process_period (
         g_ledger_id,                                             -- ledger id
         NVL (FND_PROFILE.VALUE ('ORG_ID'), 162),                     --org id
         p_period_name,                                         -- period name
         l_start_date,                                    -- period start date
         l_end_date,                                         --period end sate
         p_next_period,                                      --sweep to period
         'G_ACTION_PERIOD_CLOSE',                                     --action
         'Y',                                                  -- debug switch
         l_process_flag,                                -- output process flag
         l_message);                                   -- output error message
      g_location :=
            'l_process_flag from  AP_PERIOD_CLOSE_PKG.process_period '
         || l_process_flag;
      write_output (g_location);

      IF (l_process_flag <> 'SS')
      THEN
         l_exception_flag := 'Y';
      ELSE
         l_exception_flag := 'N';
      END IF;

      g_location :=
            'l_exception_flag from  AP_PERIOD_CLOSE_PKG.process_period '
         || l_exception_flag;
      write_output (g_location);

      g_location :=
            'l_message returned from  AP_PERIOD_CLOSE_PKG.process_period '
         || l_message;
      write_output (g_location);

      IF l_exception_flag = 'N'
      THEN
         --Now closing Period
         g_location := 'Now closing Period ';
         write_output (g_location);

         BEGIN
            UPDATE gl_period_statuses
               SET closing_status = 'C',
                   last_updated_by = g_user_id,
                   last_update_date = SYSDATE,
                   last_update_login = g_user_id
             WHERE     set_of_books_id = g_ledger_id
                   AND period_name = p_period_name
                   AND application_id = v_app_id
                   AND closing_status = 'O';

            

            IF SQL%ROWCOUNT > 0
            THEN
               g_location :=
                     'Current period '
                  || p_period_name
                  || ' closed successfully ';
               write_output (g_location);
            ELSE
               g_location :=
                  'Current period ' || p_period_name || ' is not closed Yet.';
               write_output (g_location);
            END IF;
            
            COMMIT;
            
         EXCEPTION
            WHEN OTHERS
            THEN
               g_error_message :=
                     'Error While closing the period for .'
                  || p_period_name
                  || '-'
                  || SUBSTR (SQLERRM, 1, 1000);
               RAISE g_exception;
         END;
      ELSE
         g_error_message :=
            'Program AP_PERIOD_CLOSE_PKG.process_period  has some exceptions .';
         RAISE g_exception;
      END IF;


      --End of procedure
      g_location := 'Completed Successfully close_curr_period';
      write_output (g_location);
   EXCEPTION
      WHEN g_exception
      THEN
         g_error_message :=
               'Error at close current period '
            || g_error_message
            || '-'
            || SUBSTR (SQLERRM, 1, 1000);
      WHEN OTHERS
      THEN
         g_error_message :=
               'Error at close current period '
            || g_error_message
            || '-'
            || SQLERRM;
   END close_curr_period;

   /*************************************************************************
   *   Procedure : main
   *
   *   PURPOSE:   This procedure is for concurrent program -> XXWC AP Period Open/Close.
   *   Parameter:
   *          IN  p_operating_unit,p_action,p_next_period,p_curr_period
   *          OUT   ERRBUF,RETCODE
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/15/2016  Neha Saini               Initial Version TMS# 20160930-00015
   * ************************************************************************/

   PROCEDURE main (ERRBUF                OUT VARCHAR2,
                   RETCODE               OUT NUMBER,
                   p_operating_unit   IN     NUMBER,
                   p_action           IN     VARCHAR2,
                   p_next_period      IN     VARCHAR2,
                   p_curr_period      IN     VARCHAR2)
   IS
      l_status   APPS.gl_period_statuses.closing_status%TYPE;
      l_count    NUMBER := 0;
   BEGIN
      --initializing variables
      g_error_message := NULL;
      ERRBUF := NULL;
      RETCODE := 0;
      --printing IN parameters
      g_location := 'Start main for XXWC AP Period Open/Close Program';
      write_output (g_location);
      g_location := 'Now printing IN parameters ';
      write_output (g_location);
      g_location := 'p_operating_unit ' || p_operating_unit;
      write_output (g_location);
      g_location := 'p_action ' || p_action;
      write_output (g_location);
      g_location := 'p_next_period ' || p_next_period;
      write_output (g_location);
      g_location := 'p_curr_period ' || p_curr_period;
      write_output (g_location);

      -- get ledger id
      SELECT set_of_books_id
        INTO g_ledger_id
        FROM apps.hr_operating_units
       WHERE organization_id = p_operating_unit;

      g_location := 'g_ledger_id ' || g_ledger_id;
      write_output (g_location);

      -- get application id
      SELECT application_id
        INTO g_application_id
        FROM apps.fnd_application_vl
       WHERE application_name LIKE 'Payables';

      g_location := 'g_application_id ' || g_application_id;
      write_output (g_location);

      -- Actual logic started here
      --check if next AP period is open or not
      BEGIN
         SELECT closing_status
           INTO l_status
           FROM gl_period_statuses gps
          WHERE     gps.period_name = p_next_period
                AND gps.application_id = g_application_id
                AND gps.set_of_books_id = g_ledger_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_error_message :=
                  'Error fetching status of next period '
               || SUBSTR (SQLERRM, 1, 1000);
            RAISE G_EXCEPTION;
      END;

      g_location := 'l_status status for next period ' || l_status;
      write_output (g_location);

      IF p_action = 'Close'
      THEN
         g_location := ' check if next AP period is  open or not ? ';
         write_output (g_location);

         IF l_status <> 'O'
         THEN
            -- next AP period is not open ?
            g_location := ' Next period for AP is not open. ';
            write_output (g_location);
            g_error_message := g_location;
            RAISE G_EXCEPTION;
         ELSIF l_status = 'O'
         THEN
            g_location :=
               'Next period for AP is  open, trying to close period for AP. ';
            write_output (g_location);
            -- if next AP period is open then
            close_curr_period (p_curr_period, p_next_period);

            IF g_error_message IS NOT NULL
            THEN
               RAISE G_EXCEPTION;
            END IF;
         END IF;
      ELSIF p_action = 'Open'
      THEN
         g_location := ' Check if period is already Open ';
         write_output (g_location);

         BEGIN
            SELECT COUNT (closing_status)
              INTO l_count
              FROM gl_period_statuses
             WHERE     set_of_books_id = g_ledger_id
                   AND period_name = p_next_period
                   AND application_id = g_application_id
                   AND closing_status = 'O';

            IF l_count > 0
            THEN
               g_location :=
                  ' Period  ' || p_next_period || ' is already open for AP.';
               write_output (g_location);
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_count := 0;
         END;

         IF l_count = 0
         THEN
            g_location := ' trying to open next period ' || p_next_period;
            write_output (g_location);
            --open next period
            open_next_period (p_next_period);

            IF g_error_message IS NOT NULL
            THEN
               RAISE G_EXCEPTION;
            END IF;
         END IF;
      END IF;


      -- Logic ends here

      g_location :=
         'Successful End of main for XXWC AP Period Open/Close Program';
      write_output (g_location);
   EXCEPTION
      WHEN G_EXCEPTION
      THEN
         ERRBUF := NULL;
         RETCODE := 1;
         g_location :=
               'Error at g_exception in main  '
            || g_error_message
            || ' - '
            || SUBSTR (SQLERRM, 1, 1000);
         write_output (g_location);
      WHEN OTHERS
      THEN
         ERRBUF := NULL;
         RETCODE := 2;
         g_location :=
               'Error at when others in main  '
            || g_error_message
            || ' -  '
            || SUBSTR (SQLERRM, 1, 1000);
         write_output (g_location);
   END;
END XXWC_AP_MONTHEND_AUTO_PKG;
/