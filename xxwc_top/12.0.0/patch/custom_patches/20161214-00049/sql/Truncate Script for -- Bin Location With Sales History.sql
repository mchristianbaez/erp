---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  PURPOSE	  : Issue with Bin Location With Sales History
  REVISIONS   :
  VERSION 		DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     	   14-Dec-2016        	Siva   		 TMS#20161214-00049
**************************************************************************************************************/
TRUNCATE TABLE  XXEIS.EIS_XXWC_VALID_BIN_SALES_ORGS;
TRUNCATE TABLE  XXEIS.EIS_XXWC_BIN_SALES_ORGS_TAB;
TRUNCATE TABLE  XXEIS.EIS_XXWC_BIN_SALES_ONHAND_TAB;
TRUNCATE TABLE  XXEIS.EIS_XXWC_INV_BIN_LOC_SALES_TBL;