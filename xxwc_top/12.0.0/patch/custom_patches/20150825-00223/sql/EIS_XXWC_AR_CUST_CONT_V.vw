/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_AR_CUST_CONT_V.vw $
  Module Name: Receivables
  PURPOSE: View for EIS Report Customer Contact Report - WC
  TMS Task Id : 20150825-00223
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        09/18/2015  Mahender Reddy         Initial Version
**************************************************************************************************************/
CREATE OR REPLACE FORCE VIEW "XXEIS"."EIS_XXWC_AR_CUST_CONT_V" ("PARTY_ID", "ACCOUNT_NUMBER", "ACCOUNT_NAME", "FULL_NAME", "SITE_NUMBER", "SITE_NAME", "ADDRESS", "PHONE_NUMBER", "ROLE", "LOCATION_ID", "CONTACT_NUMBER", "FAX", "SALESPERSON", "STATUS", "EMAIL_ADDRESS") AS
  SELECT DISTINCT party.party_id party_id,
  hca.account_number account_number,
  hca.account_name account_name,
  party.party_name FULL_NAME ,
  hps.party_site_number site_number,
  hps.party_site_name site_name,
  (SELECT (hl.address1
    ||' '
    ||hl.address2
    ||' '
    ||hl.address3
    ||' '
    ||hl.address4
    ||' '
    ||hl.city
    ||' '
    ||hl.state
    ||' '
    ||hl.postal_code
    ||' '
    ||hl.country)
  FROM HZ_ORG_CONTACTS hoc,
    hz_relationships hr ,
    HZ_PARTIES hp1,
    hz_party_sites hps,
    hz_locations hl,
    HZ_PARTIES hp,
    hz_cust_accounts hca1
  WHERE hr.relationship_id =hoc.party_relationship_id
  AND hp1.party_id         =hr.subject_id
  AND hr.party_id          =hps.party_id
  AND hps.location_id      =hl.location_id
  AND hp.party_id          =hr.object_id
  AND hp.party_id          =hca1.party_id
  AND hr.relationship_code = 'CONTACT_OF'
  AND hca1.CUST_ACCOUNT_ID = hca.CUST_ACCOUNT_ID
  AND rownum               = 1
  ) address,
  (SELECT DECODE(Phone_number,NULL,NULL, '('
    ||Phone_area_code
    ||') '
    ||Phone_number )
  FROM hz_contact_points
  WHERE owner_table_id   = hcp.owner_table_id
  AND contact_point_type = 'PHONE'
  AND rownum               = 1
  ) phone_number,
  NVL(flv.meaning,'Contact Only') Role,
  hps.location_id LOCATION_ID,
  org_cont.contact_number,
  (SELECT DECODE(phone_line_type,NULL,NULL, phone_area_code
    ||phone_number )
  FROM hz_contact_points
  WHERE owner_table_id = hcp.owner_table_id
  AND phone_line_type  = 'FAX'
  AND rownum               = 1
  ) FAX,
  res.resource_name SALESPERSON,
  CASE
    WHEN hcar.current_role_state = 'A'
    THEN 'ACTIVE'
    WHEN hcar.current_role_state = 'I'
    THEN 'INACTIVE'
    ELSE 'ALL'
  END STATUS,
  (SELECT EMAIL_ADDRESS
  FROM hz_contact_points
  WHERE owner_table_id   = hcp.owner_table_id
  AND contact_point_type = 'EMAIL'
  AND rownum               = 1
  ) EMAIL
FROM HZ_CUST_ACCOUNTS hca,
  HZ_CUST_ACCOUNT_ROLES hcar,
  HZ_ROLE_RESPONSIBILITY hrr,
  FND_LOOKUP_VALUES flv,
  apps.hz_parties party,
  apps.hz_relationships rel,
  apps.hz_org_contacts org_cont,
  hz_party_sites hps,
  APPS.hz_contact_points hcp,
  hz_cust_acct_sites_all hcas,
hz_cust_site_uses_all hcsu,
JTF_RS_SALESREPS SR,
JTF_RS_RESOURCE_EXTNS_VL RES
WHERE 1                            = 1
AND hcar.cust_account_id           =hca.cust_account_id
AND hcar.cust_account_role_id      =hrr.cust_account_role_id(+)
AND FLV.LOOKUP_CODE (+)            =HRR.RESPONSIBILITY_TYPE
AND org_cont.party_relationship_id = rel.relationship_id
AND hcar.party_id                  = rel.party_id
AND FLV.LOOKUP_TYPE (+)            ='SITE_USE_CODE'
AND rel.subject_table_name         = 'HZ_PARTIES'
AND rel.object_table_name          = 'HZ_PARTIES'
AND rel.directional_flag           = 'F'
AND hcar.role_type                 = 'CONTACT'
AND rel.subject_id                 = party.party_id
AND party.party_id                 =hps.party_id(+)
AND hcp.owner_table_id(+)          = hcar.party_id
AND hcp.owner_table_name(+)        = 'HZ_PARTIES'
AND hca.cust_account_id = hcas.cust_account_id
and hcas.cust_acct_site_id = hcsu.cust_acct_site_id
--AND SUBSTR(hcsu.location,-9) = hca.account_number
--AND hcsu.location like '%'||hca.account_number||'%'
AND hcSU.PRIMARY_SALESREP_ID = SR.SALESREP_ID (+)
  AND SR.RESOURCE_ID         = RES.RESOURCE_ID (+)
/*AND hcp.contact_point_type(+)        IN ('PHONE','EMAIL')
AND (hcp.phone_line_type    = 'GEN'
OR hcp.phone_line_type     IS NULL)*/
AND HCAR.CUST_ACCT_SITE_ID IS NULL
AND hcsu.site_use_code = 'BILL_TO'
AND hcsu.primary_flag = 'Y'
/