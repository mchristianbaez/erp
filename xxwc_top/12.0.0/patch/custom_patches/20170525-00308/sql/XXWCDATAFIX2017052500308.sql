   /**************************************************************************
      $Header XXWC_DATAFIX_TMS20170525-00308 $
      FILE XXWCDATAFIX2017052500308.sql
      PURPOSE:   Datafix script to cleaer shipping insttructions 
      REVISIONS:
      Ver        Date        Author             Description
      ---------  ----------  ---------------   -------------------------
       1.0       05/12/2016  Neha Saini         Initial Build - Task ID20170525-00308: 
    /*************************************************************************/
SET SERVEROUTPUT ON;
BEGIN
UPDATE apps.oe_order_headers_all  set  shipping_instructions = NULL  where order_number='24495050';
DBMS_OUTPUT.PUT_LINE('Total rows updated '||sql%ROWCOUNT);
COMMIT;
END;
/