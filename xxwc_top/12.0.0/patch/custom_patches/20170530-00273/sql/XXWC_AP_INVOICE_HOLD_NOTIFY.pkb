/* Formatted on 3/5/2014 10:12:28 PM (QP5 v5.206) */
-- Start of DDL Script for Package Body APPS.XXWC_AP_INVOICE_HOLD_NOTIFY
-- Generated 3/5/2014 10:12:26 PM from APPS@EBIZPRD

CREATE OR REPLACE PACKAGE BODY apps.xxwc_ap_invoice_hold_notify
IS
    --
    -- Purpose: This supports the AP Invoice Hold Notification
    -- workflow customizations and the WC DIRT (Discrepant Invoice Resolution
    -- and tracking) project. Some of the included procedures are brand new and
    -- some are copies of seeded procedures for use by the Workflow.
    --
    -- MODIFICATION HISTORY
    -- Person      Date    Comments
    -- ---------   ------  ------------------------------------------
    -- Raskiha G            Author
    -- Andre R     9/17/12  Added these comments
    -- Pattabhi    09/16/2014 TMS#20141001-00029
    -- Maha        11/24/2014 TMS#20141001-00029 MO changes
    --
    g_current_runtime_level   CONSTANT NUMBER := fnd_log.g_current_runtime_level;
    g_level_statement         CONSTANT NUMBER := fnd_log.level_statement;
    g_module_name             CONSTANT VARCHAR2 (100) := 'XXWC_AP_INVOICE_HOLD_NOTIFY';
    g_level_procedure         CONSTANT NUMBER := fnd_log.level_procedure;

    FUNCTION get_branch_manager (p_location_id NUMBER)
        RETURN NUMBER
    IS
        v_person_id   NUMBER;
    BEGIN
        FOR r IN (SELECT attribute13 person_id_branch_manager
                    FROM mtl_parameters_view
                   WHERE organization_id IN (SELECT organization_id
                                               FROM hr_all_organization_units
                                              WHERE location_id = p_location_id))
        LOOP
            v_person_id := r.person_id_branch_manager;
        END LOOP;

        RETURN v_person_id;
    END get_branch_manager;

    --***********************************
    FUNCTION get_user_id (p_person_id NUMBER)
        RETURN NUMBER
    IS
        v_user_id   NUMBER;
    BEGIN
        FOR r IN (SELECT user_id
                    FROM fnd_user
                   WHERE employee_id = p_person_id AND NVL (end_date, SYSDATE + 1) > SYSDATE)
        LOOP
            v_user_id := r.user_id;
        END LOOP;

        RETURN v_user_id;
    END get_user_id;

    --***********************************
    FUNCTION get_user_name (p_person_id NUMBER)
        RETURN VARCHAR2
    IS
        v_user_name   VARCHAR2 (65);
    BEGIN
        FOR r IN (SELECT user_name
                    FROM fnd_user
                   WHERE employee_id = p_person_id AND NVL (end_date, SYSDATE + 1) > SYSDATE)
        LOOP
            v_user_name := r.user_name;
        END LOOP;

        RETURN v_user_name;
    END get_user_name;

    --******************************************
    FUNCTION get_approver (p_transaction_id NUMBER)
        RETURN VARCHAR2
    IS
        v_approver   VARCHAR2 (124) := NULL;
        l_org_id NUMBER;                                --Added org id initialization by Maha for TMS#20141001-00163 
        l_org_name VARCHAR2(30);
    BEGIN

--Added by Maha for TMS#20141001-00163 <<sTART

      l_org_id := MO_GLOBAL.GET_CURRENT_ORG_ID; 
    
      mo_global.set_policy_context('S',l_org_id);

       BEGIN
         SELECT REPLACE (SUBSTR (name, 5, 9), ' ', '')
           INTO l_org_name
 	  FROM hr_operating_units
 	 WHERE organization_id = l_org_id;
       EXCEPTION
        WHEN OTHERS THEN
           l_org_name := 'WhiteCap';
           l_org_id   := 162 ;
      END;                                    -->> END


        FOR r IN (SELECT *
                    FROM apps.ap_holds
                   WHERE hold_id = p_transaction_id)
        LOOP
            --user defined first approver
          --  IF r.attribute1 IS NOT NULL AND (r.attribute_category = 'WhiteCap' OR r.attribute_category = '162') --commented and added below by Maha for TMS#20141001-00163 
	      IF r.attribute1 IS NOT NULL AND (r.attribute_category = l_org_name OR r.attribute_category = l_org_id)
            THEN
                v_approver := 'person_id:' || r.attribute1;
            --  'QTY REC' will send to branch manager, if user will not populate attribute1
            ELSIF     r.hold_lookup_code = 'QTY REC'
                  AND r.attribute1 IS NULL
                  AND r.line_location_id IS NOT NULL
                  AND v_approver IS NULL
            THEN
                BEGIN
                    SELECT    'person_id:'
                           || NVL (apps.xxwc_ap_invoice_hold_notify.get_branch_manager (ph.ship_to_location_id)
                                  ,ph.agent_id)
                      INTO v_approver
                      FROM apps.ap_holds aph, apps.po_distributions pd, apps.po_headers ph
                     WHERE     pd.line_location_id = aph.line_location_id
                           AND pd.po_header_id = ph.po_header_id
                           AND aph.hold_id = p_transaction_id;
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        NULL;
                END;
            ELSIF r.line_location_id IS NOT NULL AND r.attribute1 IS NULL AND v_approver IS NULL
            THEN
                BEGIN
                    SELECT 'person_id:' || ph.agent_id
                      INTO v_approver
                      FROM apps.ap_holds aph, apps.po_distributions pd, apps.po_headers ph
                     WHERE     pd.line_location_id = aph.line_location_id
                           AND pd.po_header_id = ph.po_header_id
                           AND aph.hold_id = p_transaction_id;
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        NULL;
                END;
            END IF;
        END LOOP;

        --  V_APPROVER := NVL (V_APPROVER, 'not found');
        RETURN v_approver;
    END;

    -- APPS.XXWC_AP_INVOICE_HOLD_NOTIFY.GET_APPROVER (P_TRANSACTION_ID NUMBER)
    --**************************************************************************
    -- Can this be completely dropped? -AR 9/17/12
    FUNCTION get_ap_person
        RETURN VARCHAR2
    IS
        v_ap_random_approver   VARCHAR2 (124);
    BEGIN
        v_ap_random_approver := 'person_id:' || 62243;
        /* SELECT 'person_id:' || EMPLOYEE_ID
           INTO V_AP_RANDOM_APPROVER
           FROM (  SELECT U.EMPLOYEE_ID
                         ,U.USER_NAME
                         ,WA.USER_NAME
                         ,USER_ORIG_SYSTEM_ID
                         ,DBMS_RANDOM.VALUE ()
                     FROM APPS.WF_ALL_USER_ROLES WA, FND_USER U
                    WHERE     WA.ROLE_NAME = 'UMX|XXWC_ROLE_ACCOUNTS_PAYABLE_ASSOCIATE'
                          AND WA.START_DATE <= SYSDATE
                          AND NVL (WA.EXPIRATION_DATE, SYSDATE + 1) > SYSDATE
                          AND U.USER_NAME = WA.USER_NAME
                          AND USER_ORIG_SYSTEM <> 'FND_USR'
                 ORDER BY DBMS_RANDOM.VALUE ())
          WHERE ROWNUM = 1;*/

        RETURN v_ap_random_approver;
    EXCEPTION
        WHEN OTHERS
        THEN
            NULL;
            RETURN NULL;
    END;

    /* --*************************************************************
     -- here is modification of seeded function -

     --************************************************************* */
    PROCEDURE get_next_approver (itemtype    IN            VARCHAR2
                                ,itemkey     IN            VARCHAR2
                                ,actid       IN            NUMBER
                                ,funcmode    IN            VARCHAR2
                                ,resultout      OUT NOCOPY VARCHAR2)
    IS
        l_hold_id             NUMBER;
        v_current_order       NUMBER;
        l_approver_id         NUMBER;
        v_current_user_name   VARCHAR2 (124);
        v_last_user_name      VARCHAR2 (124);
    BEGIN
        l_hold_id := wf_engine.getitemattrnumber ('APINVHDN', itemkey, 'HOLD_ID');
        -- get current approver, returns person_id
        l_approver_id := wf_engine.getitemattrnumber ('APINVHDN', itemkey, 'INTERNAL_REP_PERSON_ID');

        --***********************************************
        IF l_approver_id IS NOT NULL
        THEN
            FOR r IN (SELECT name
                        FROM wf_roles
                       WHERE orig_system_id = l_approver_id AND status = 'ACTIVE' AND orig_system = 'PER')
            LOOP
                v_current_user_name := r.name;
            END LOOP;
        END IF;

        FOR r IN (SELECT name
                    FROM ame_temp_old_approver_lists
                   WHERE     order_number = (SELECT MAX (order_number)
                                               FROM ame_temp_old_approver_lists
                                              WHERE transaction_id = l_hold_id)
                         AND transaction_id = l_hold_id
                         AND ROWNUM = 1)
        LOOP
            v_last_user_name := r.name;
        END LOOP;

        IF v_last_user_name IS NOT NULL AND v_current_user_name IS NOT NULL AND v_current_user_name = v_last_user_name -- last approver alredy pushed "release" button
        THEN                                                                                 /*no approver on the list*/
            resultout := wf_engine.eng_completed || ':' || 'Y';
        ELSE
            resultout := wf_engine.eng_completed || ':' || 'N';
        END IF;
    --*********************************************

    EXCEPTION
        WHEN OTHERS
        THEN
            wf_core.context ('APINVHDN'
                            ,'GET_APPROVER'
                            ,itemtype
                            ,itemkey
                            ,TO_CHAR (actid)
                            ,funcmode);
            RAISE;
    END get_next_approver;

    --****************************************************
    PROCEDURE xxwc_send_email_to_ap (itemtype    IN            VARCHAR2
                                    ,itemkey     IN            VARCHAR2
                                    ,actid       IN            NUMBER
                                    ,funcmode    IN            VARCHAR2
                                    ,resultout      OUT NOCOPY VARCHAR2)
    IS
        l_org_id                 NUMBER;
        l_invoice_id             NUMBER;
        l_hold_id                NUMBER;
        l_num                    NUMBER;
        l_hold_release_code      apps.ap_holds.release_lookup_code%TYPE;
        l_wait_time              NUMBER;
        l_debug_info             VARCHAR2 (2000);
        n                        NUMBER;
        --v_distro_list            VARCHAR2 (256) := 'wcaporaclenotifications@hdsupply.com';
		v_distro_list            VARCHAR2 (256) := 'noreply@hdsupply.com';

        v_body_header            CLOB;
        v_body                   CLOB;
        v_sid                    VARCHAR2 (164);

        --v_email                  VARCHAR2 (164) := 'wcaporaclenotifications@hdsupply.com';
		v_email                  VARCHAR2 (164) := 'noreply@hdsupply.com';

        v_errbuf                 CLOB;
        v_err_code               VARCHAR2 (16) := '0';
        l_approver_id            NUMBER;
        v_add_string             VARCHAR2 (512);
        lxxwc_resolution_code    VARCHAR2 (240);
        lxxwc_resolution_value   VARCHAR2 (240);
        v_wf_notes               VARCHAR2 (214);
        v_vendor_item_desc       VARCHAR2 (240);
        v_inv_line_number        VARCHAR2 (16);
        v_inv_line_amount        NUMBER;
        v_po_line_amount         NUMBER;
        v_database               VARCHAR2 (36);
    BEGIN
        l_org_id := wf_engine.getitemattrnumber (itemtype, itemkey, 'ORG_ID');

        l_invoice_id := wf_engine.getitemattrnumber (itemtype, itemkey, 'INVOICE_ID');

        l_hold_id := wf_engine.getitemattrnumber (itemtype, itemkey, 'HOLD_ID');
        l_approver_id := wf_engine.getitemattrnumber (itemtype, itemkey, 'INTERNAL_REP_PERSON_ID');
        lxxwc_resolution_code := wf_engine.getitemattrtext (itemtype, itemkey, 'XXWC_RESOLUTION_CODE');

        FOR r IN (SELECT meaning
                    FROM apps.wf_lookups
                   WHERE lookup_type = 'XXWC_APINVHDN_RESOLUTION_CODE' AND lookup_code = lxxwc_resolution_code)
        LOOP
            lxxwc_resolution_value := r.meaning;
        END LOOP;

        BEGIN
            FOR tr IN (  SELECT notification_id, wf_notification.getattrtext (notification_id, 'WF_NOTE') wf_notes
                           FROM wf_notifications
                          WHERE item_key = TO_CHAR (l_hold_id) AND MESSAGE_TYPE = 'APINVHDN' AND status = 'CLOSED'
                       ORDER BY notification_id)
            LOOP
                v_wf_notes := tr.wf_notes;
            END LOOP;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        IF lxxwc_resolution_value IS NOT NULL
        THEN
            FOR r
                IN (  SELECT i.invoice_num
                            ,i.invoice_amount
                            ,hl.hold_reason
                            ,approver_comments
                            ,i.source
                            ,i.invoice_date
                            ,h.approver_name
                            ,h.amount_approved
                            ,xxwc_ap_invoice_hold_notify.get_ship_to_location (hl.hold_id) ship_to_location
                            ,h.response
                            ,xxwc_ap_invoice_hold_notify.get_po_number (hl.hold_id) po_number
                            ,approval_history_id
                        FROM apps.ap_inv_aprvl_hist h
                            ,apps.ap_invoices i
                            ,apps.ap_holds hl
                            ,ap_lookup_codes s
                       WHERE     h.response IN ('ACKNOWLEDGE')
                             AND h.hold_id = l_hold_id                                                           --32280
                             AND h.invoice_id = l_invoice_id                                                   --6405033
                             AND h.history_type = 'HOLDAPPROVAL'
                             --AND H.APPROVER_ID = L_APPROVER_ID
                             AND h.invoice_id = i.invoice_id                                              --I.INVOICE_ID
                             AND hl.hold_id = h.hold_id
                             AND s.lookup_type = 'HOLD_WORKFLOW_STATUS'
                             AND s.lookup_code = hl.wf_status
                             AND hl.wf_status IS NOT NULL
                             AND hl.wf_status NOT IN ('MANUALLYRELEASED', 'RELEASED')
                    ORDER BY approval_history_id)
            LOOP
                IF r.ship_to_location IS NOT NULL
                THEN
                    v_add_string := '; Ship To Location : ' || r.ship_to_location || '; Po Number: ' || r.po_number;
                END IF;

                v_vendor_item_desc := NULL;
                v_inv_line_number := NULL;
                v_inv_line_amount := NULL;
                v_po_line_amount := NULL;

                FOR tr IN (SELECT DISTINCT vendor_item_desc
                                          ,inv_line_number
                                          ,inv_line_amount
                                          ,po_line_amount
                             FROM apps.xxwc_apinvoicelinesall_hold
                            WHERE hold_id = l_hold_id AND ROWNUM = 1)
                LOOP
                    v_vendor_item_desc := tr.vendor_item_desc;
                    v_inv_line_number := tr.inv_line_number;
                    v_inv_line_amount := tr.inv_line_amount;
                    v_po_line_amount := tr.po_line_amount;
                END LOOP;

                SELECT name INTO v_database FROM v$database;

                v_body_header :=
                       v_database
                    || ' Please release hold for invoice:'
                    || r.invoice_num
                    || '; Invoice Hold Reason: '
                    || r.hold_reason
                    || '; Invoice Amount: '
                    || r.invoice_amount
                    || v_add_string;
                v_body :=
                       '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>Resolution code: '
                    || lxxwc_resolution_value
                    || '
</HEAD>
<BODY>
<!--StartFragment-->
<TABLE BORDER=2>

<TR>
<TD>Invoice Number</TD><TD>'
                    || r.invoice_num
                    || '</TD>
</TR>
<TR>
<TD> Item Description</TD><TD>'
                    || v_vendor_item_desc
                    || '</TD>
</TR>
<TR>
<TD>Invoice Line Number</TD><TD>'
                    || v_inv_line_number
                    || '</TD>
</TR>
<TR>
<TD>Invoice Line Amount</TD><TD>'
                    || v_inv_line_amount
                    || '</TD>
</TR>
<TR>
<TD>PO Line Amount</TD><TD>'
                    || v_po_line_amount
                    || '</TD>
</TR>
<TR>
<TD>Invoice Amount</TD><TD>'
                    || r.invoice_amount
                    || '</TD>
</TR>
<TR>
<TD>Hold Reason</TD><TD>'
                    || r.hold_reason
                    || '</TD>
</TR>
<TR>
<TD>Resolution Code</TD><TD>'
                    || lxxwc_resolution_value
                    || '</TD>
</TR>
<TR>
<TD>Approver Comments</TD><TD>'
                    || NVL (v_wf_notes, r.approver_comments)
                    || '</TD>
</TR>
<TR>
<TD>Source</TD><TD>'
                    || r.source
                    || '</TD>
</TR>
<TR>
<TD>Invoice Date</TD><TD>'
                    || r.invoice_date
                    || '</TD>
</TR>
<TR>
<TD>Approver Name</TD><TD>'
                    || r.approver_name
                    || '</TD>
</TR>
<TR>
<TD>Amount Approved</TD><TD>'
                    || r.amount_approved
                    || '</TD>
</TR>
<TR>
<TD>Ship To Location</TD><TD>'
                    || r.ship_to_location
                    || '</TD>
</TR>
<TR>
<TD>Po Number</TD><TD>'
                    || r.po_number
                    || '</TD>
</TR>
</TABLE>
<!--EndFragment-->
</BODY>
</HTML>';
            END LOOP;

            INSERT INTO xxwc.ap_hold_sent_to_ap_log (hold_id, sent_date)
                 VALUES (l_hold_id, SYSDATE);

            DELETE FROM xxwc.ap_hold_sent_to_ap_log
                  WHERE sent_date < SYSDATE - 10;

            COMMIT;
            xxcus_misc_pkg.html_email (p_to              => v_email
                                      ,p_from            => 'donotreply@hdsupply.com'             -- Edited.AR 9/17/2012
                                      ,p_text            => 'test'
                                      ,p_subject         => v_body_header
                                      ,                                                 -- p_subject       => l_subject,
                                       p_html            => v_body
                                      ,p_smtp_hostname   => 'mailoutrelay.hdsupply.net'
                                      ,p_smtp_portnum    => '25');
        ELSE
            INSERT INTO xxwc.ap_hold_sent_to_ap_log (hold_id, sent_date, error_message)
                 VALUES (l_hold_id, SYSDATE, 'attemp to send without resolution_code');

            COMMIT;
        END IF;

        resultout := wf_engine.eng_completed || ':' || 'Y';
    EXCEPTION
        WHEN OTHERS
        THEN
            v_errbuf :=
                   v_errbuf
                || 'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            COMMIT;

            wf_core.context ('APINVHDN'
                            ,'xxwc_send_email_to_ap'
                            ,itemtype
                            ,itemkey
                            ,TO_CHAR (actid)
                            ,funcmode);
            RAISE;
    END;

    --****************************************************************
    --****************************************************************
    -- This is a copy of the seeded process_ack_pomatched procedure. -AR
    PROCEDURE xxwc_process_ack_pomatched (itemtype    IN            VARCHAR2
                                         ,itemkey     IN            VARCHAR2
                                         ,actid       IN            NUMBER
                                         ,funcmode    IN            VARCHAR2
                                         ,resultout      OUT NOCOPY VARCHAR2)
    IS
        l_org_id                 NUMBER;
        l_invoice_id             NUMBER;
        l_hold_id                NUMBER;
        l_approver_id            NUMBER;
        l_display_name           VARCHAR2 (150);
        l_hist_rec               ap_inv_aprvl_hist%ROWTYPE;
        l_api_name      CONSTANT VARCHAR2 (200) := 'process_ack_pomatched';
        l_debug_info             VARCHAR2 (2000);
        l_iteration              NUMBER;
        l_notf_iteration         NUMBER;
        -- bug 8940578
        l_comments               VARCHAR2 (240);
        lxxwc_resolution_code    VARCHAR2 (240);
        lxxwc_resolution_value   VARCHAR2 (240);
    BEGIN
        l_org_id := wf_engine.getitemattrnumber (itemtype, itemkey, 'ORG_ID');

        l_invoice_id := wf_engine.getitemattrnumber (itemtype, itemkey, 'INVOICE_ID');

        l_hold_id := wf_engine.getitemattrnumber (itemtype, itemkey, 'HOLD_ID');

        l_approver_id := wf_engine.getitemattrnumber (itemtype, itemkey, 'INTERNAL_REP_PERSON_ID');

        l_iteration := wf_engine.getitemattrnumber (itemtype, itemkey, 'ITERATION');

        l_notf_iteration := wf_engine.getitemattrnumber (itemtype, itemkey, 'NOTF_ITERATION');
        l_display_name := wf_engine.getitemattrtext (itemtype, itemkey, 'INTERNAL_REP_DISPLAY_NAME');
        -- bug 8940578
        l_comments := wf_engine.getitemattrtext (itemtype, itemkey, 'WF_NOTE');
        lxxwc_resolution_code := wf_engine.getitemattrtext (itemtype, itemkey, 'XXWC_RESOLUTION_CODE');

        FOR r IN (SELECT meaning
                    FROM apps.wf_lookups
                   WHERE lookup_type = 'XXWC_APINVHDN_RESOLUTION_CODE' AND lookup_code = lxxwc_resolution_code)
        LOOP
            lxxwc_resolution_value := r.meaning;
        END LOOP;

        -- INSERT INTO XXWC.XXWC_ERROR
        --    VALUES (SYSDATE, ' LXXWC_RESOLUTION_CODE' || LXXWC_RESOLUTION_CODE);

        --Now set the environment
        mo_global.init ('SQLAP');
        mo_global.set_policy_context ('S', l_org_id);

        l_hist_rec.history_type := 'HOLDAPPROVAL';
        l_hist_rec.invoice_id := l_invoice_id;
        l_hist_rec.iteration := l_iteration;
        l_hist_rec.notification_order := l_notf_iteration;
        l_hist_rec.response := 'ACKNOWLEDGE';
        l_hist_rec.approver_id := l_approver_id;
        l_hist_rec.approver_name := l_display_name;
        l_hist_rec.created_by := NVL (TO_NUMBER (fnd_profile.VALUE ('USER_ID')), -1);
        l_hist_rec.creation_date := SYSDATE;
        l_hist_rec.last_update_date := SYSDATE;
        l_hist_rec.last_updated_by := NVL (TO_NUMBER (fnd_profile.VALUE ('USER_ID')), -1);
        l_hist_rec.last_update_login := NVL (TO_NUMBER (fnd_profile.VALUE ('LOGIN_ID')), -1);
        l_hist_rec.org_id := l_org_id;
        l_hist_rec.amount_approved := NULL;
        l_hist_rec.hold_id := l_hold_id;

        -- bug 8940578\
        -- l_hist_rec.APPROVER_COMMENTS := l_comments;
        IF lxxwc_resolution_code IS NOT NULL
        THEN
            l_comments := lxxwc_resolution_value || '; ' || l_comments;
            l_comments := SUBSTR (l_comments, 1, 240);
        END IF;

        l_hist_rec.approver_comments := l_comments;
        l_debug_info := 'Before insert_history_table';

        IF (g_level_statement >= g_current_runtime_level)
        THEN
            fnd_log.string (g_level_statement, g_module_name || l_api_name, l_debug_info);
        END IF;

        ap_workflow_pkg.insert_history_table (p_hist_rec => l_hist_rec);

        l_debug_info := 'Before AME_API.updateApprovalStatus2';

        IF (g_level_statement >= g_current_runtime_level)
        THEN
            fnd_log.string (g_level_statement, g_module_name || l_api_name, l_debug_info);
        END IF;

        ame_api.updateapprovalstatus2 (applicationidin      => 200
                                      ,transactionidin      => TO_CHAR (l_hold_id)
                                      ,approvalstatusin     => ame_util.approvedstatus
                                      ,approverpersonidin   => l_approver_id
                                      ,approveruseridin     => NULL
                                      ,transactiontypein    => 'APHLD');

        resultout := wf_engine.eng_completed || ':' || 'Y';
    EXCEPTION
        WHEN OTHERS
        THEN
            wf_core.context ('APINVHDN'
                            ,'process_ack_pomatched'
                            ,itemtype
                            ,itemkey
                            ,TO_CHAR (actid)
                            ,funcmode);
            RAISE;
    END xxwc_process_ack_pomatched;

    --*********************************************************************
    --*********************************************************************
    -- This is a copy of the seeded process_ack_pounmatched procedure. -AR
    PROCEDURE xxwc_process_ack_pounmatched (itemtype    IN            VARCHAR2
                                           ,itemkey     IN            VARCHAR2
                                           ,actid       IN            NUMBER
                                           ,funcmode    IN            VARCHAR2
                                           ,resultout      OUT NOCOPY VARCHAR2)
    IS
        l_org_id                 NUMBER;
        l_invoice_id             NUMBER;
        l_hold_id                NUMBER;
        l_approver_id            NUMBER;
        l_display_name           VARCHAR2 (150);
        l_hist_rec               ap_inv_aprvl_hist%ROWTYPE;
        l_api_name      CONSTANT VARCHAR2 (200) := 'process_ack_pounmatched';
        l_debug_info             VARCHAR2 (2000);
        l_iteration              NUMBER;
        l_notf_iteration         NUMBER;
        -- bug 8940578
        l_comments               VARCHAR2 (240);
        lxxwc_resolution_code    VARCHAR2 (240);
        lxxwc_resolution_value   VARCHAR2 (240);
    BEGIN
        l_org_id := wf_engine.getitemattrnumber (itemtype, itemkey, 'ORG_ID');

        l_invoice_id := wf_engine.getitemattrnumber (itemtype, itemkey, 'INVOICE_ID');

        l_hold_id := wf_engine.getitemattrnumber (itemtype, itemkey, 'HOLD_ID');

        l_approver_id := wf_engine.getitemattrnumber (itemtype, itemkey, 'INTERNAL_REP_PERSON_ID');

        l_iteration := wf_engine.getitemattrnumber (itemtype, itemkey, 'ITERATION');

        l_notf_iteration := wf_engine.getitemattrnumber (itemtype, itemkey, 'NOTF_ITERATION');

        l_display_name := wf_engine.getitemattrtext (itemtype, itemkey, 'INTERNAL_REP_DISPLAY_NAME');
        -- bug 8940578
        l_comments := wf_engine.getitemattrtext (itemtype, itemkey, 'WF_NOTE');

        -- bug 8940578
        lxxwc_resolution_code := wf_engine.getitemattrtext (itemtype, itemkey, 'XXWC_RESOLUTION_CODE');

        FOR r IN (SELECT meaning
                    FROM apps.wf_lookups
                   WHERE lookup_type = 'XXWC_APINVHDN_RESOLUTION_CODE' AND lookup_code = lxxwc_resolution_code)
        LOOP
            lxxwc_resolution_value := r.meaning;
        END LOOP;

        --Now set the environment
        mo_global.init ('SQLAP');
        mo_global.set_policy_context ('S', l_org_id);

        l_hist_rec.history_type := 'HOLDAPPROVAL';
        l_hist_rec.invoice_id := l_invoice_id;
        l_hist_rec.iteration := l_iteration;
        l_hist_rec.notification_order := l_notf_iteration;
        l_hist_rec.response := 'ACKNOWLEDGE';
        l_hist_rec.approver_id := l_approver_id;
        l_hist_rec.approver_name := l_display_name;
        l_hist_rec.created_by := NVL (TO_NUMBER (fnd_profile.VALUE ('USER_ID')), -1);
        l_hist_rec.creation_date := SYSDATE;
        l_hist_rec.last_update_date := SYSDATE;
        l_hist_rec.last_updated_by := NVL (TO_NUMBER (fnd_profile.VALUE ('USER_ID')), -1);
        l_hist_rec.last_update_login := NVL (TO_NUMBER (fnd_profile.VALUE ('LOGIN_ID')), -1);
        l_hist_rec.org_id := l_org_id;
        l_hist_rec.amount_approved := NULL;
        l_hist_rec.hold_id := l_hold_id;

        -- l_hist_rec.APPROVER_COMMENTS := l_comments;
        -- bug 8940578
        --add here user resolution code to the notes:
        IF lxxwc_resolution_code IS NOT NULL
        THEN
            l_comments := lxxwc_resolution_value || '; ' || l_comments;
            l_comments := SUBSTR (l_comments, 1, 240);
        END IF;

        l_hist_rec.approver_comments := l_comments;
        l_debug_info := 'Before insert_history_table';

        IF (g_level_statement >= g_current_runtime_level)
        THEN
            fnd_log.string (g_level_statement, g_module_name || l_api_name, l_debug_info);
        END IF;

        ap_workflow_pkg.insert_history_table (p_hist_rec => l_hist_rec);

        l_debug_info := 'Before AME_API.updateApprovalStatus2';

        IF (g_level_statement >= g_current_runtime_level)
        THEN
            fnd_log.string (g_level_statement, g_module_name || l_api_name, l_debug_info);
        END IF;

        ame_api.updateapprovalstatus2 (applicationidin      => 200
                                      ,transactionidin      => TO_CHAR (l_hold_id)
                                      ,approvalstatusin     => ame_util.approvedstatus
                                      ,approverpersonidin   => l_approver_id
                                      ,approveruseridin     => NULL
                                      ,transactiontypein    => 'APHLD');

        resultout := wf_engine.eng_completed || ':' || 'Y';
    EXCEPTION
        WHEN OTHERS
        THEN
            wf_core.context ('APINVHDN'
                            ,'xxwc_process_ack_pounmatched'
                            ,itemtype
                            ,itemkey
                            ,TO_CHAR (actid)
                            ,funcmode);
            RAISE;
    END xxwc_process_ack_pounmatched;

    --**********************************************
    --**********************************************
    PROCEDURE populate_add_attributes (itemtype    IN            VARCHAR2
                                      ,itemkey     IN            VARCHAR2
                                      ,actid       IN            NUMBER
                                      ,funcmode    IN            VARCHAR2
                                      ,resultout      OUT NOCOPY VARCHAR2)
    IS
        v_line_location_id   NUMBER;
        l_org_id             NUMBER;
        l_invoice_id         NUMBER;
        l_hold_id            NUMBER;
        l_ship_to_location   VARCHAR2 (128);
        l_user_po_number     VARCHAR2 (128);
    BEGIN
        l_org_id := wf_engine.getitemattrnumber (itemtype, itemkey, 'ORG_ID');

        l_invoice_id := wf_engine.getitemattrnumber (itemtype, itemkey, 'INVOICE_ID');

        l_hold_id := wf_engine.getitemattrnumber (itemtype, itemkey, 'HOLD_ID');

        --******************************

        -- Raskiha G 1/9/2013 added user PoNumber (attribute2) to display in notification send part
        FOR r IN (SELECT line_location_id, attribute2
                    FROM apps.ap_holds
                   WHERE hold_id = l_hold_id)
        LOOP
            v_line_location_id := r.line_location_id;

            IF r.attribute2 IS NOT NULL
            THEN
                l_user_po_number := 'User PO Number: ' || r.attribute2;
                --populate workflow(invoice hold) attribute
                wf_engine.setitemattrtext (itemtype
                                          ,itemkey
                                          ,'XXWC_ENTER_PO_NUMBER'
                                          ,l_user_po_number);
            END IF;
        END LOOP;

        IF v_line_location_id IS NOT NULL AND v_line_location_id > 0
        THEN
            NULL;
        END IF;

        l_ship_to_location := xxwc_ap_invoice_hold_notify.get_ship_to_location (l_hold_id);
        --******************************

        wf_engine.setitemattrtext (itemtype
                                  ,itemkey
                                  ,'XXWC_SHIP_TO_LOCATION'
                                  ,l_ship_to_location);
        -- XXWC_SHIP_TO_LOCATION
        NULL;
        resultout := wf_engine.eng_completed || ':' || 'Y';
    EXCEPTION
        WHEN OTHERS
        THEN
            wf_core.context ('APINVHDN'
                            ,'POPULATE_ADD_ATTRIBUTES'
                            ,itemtype
                            ,itemkey
                            ,TO_CHAR (actid)
                            ,funcmode);
            RAISE;
    END populate_add_attributes;

    --***************************************************
    FUNCTION get_ship_to_location (p_hold_id NUMBER)
        RETURN VARCHAR2
    IS
        v_ship_to_location   VARCHAR2 (128);
    BEGIN
        FOR r
            IN (SELECT location_code
                  FROM po_ship_to_loc_org_v
                 WHERE location_id IN
                           (SELECT ship_to_location_id
                              FROM apps.po_headers
                             WHERE po_header_id IN (SELECT po_header_id
                                                      FROM apps.po_line_locations
                                                     WHERE line_location_id IN (SELECT line_location_id
                                                                                  FROM apps.ap_holds
                                                                                 WHERE hold_id = p_hold_id))))
        LOOP
            v_ship_to_location := r.location_code;
        END LOOP;

        RETURN v_ship_to_location;
    END get_ship_to_location;

    --**********************************
    --***************************************************
    FUNCTION get_po_number (p_hold_id NUMBER)
        RETURN VARCHAR2
    IS
        v_po_number   VARCHAR2 (128);
    BEGIN
        FOR r IN (SELECT segment1
                    FROM apps.po_headers
                   WHERE po_header_id IN (SELECT po_header_id
                                            FROM apps.po_line_locations
                                           WHERE line_location_id IN (SELECT line_location_id
                                                                        FROM apps.ap_holds
                                                                       WHERE hold_id = p_hold_id)))
        LOOP
            v_po_number := r.segment1;
        END LOOP;

        RETURN v_po_number;
    END get_po_number;
END;
/

-- End of DDL Script for Package Body APPS.XXWC_AP_INVOICE_HOLD_NOTIFY
