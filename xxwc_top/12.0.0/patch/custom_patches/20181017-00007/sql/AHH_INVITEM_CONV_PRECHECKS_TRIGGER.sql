-- 20180716-00106 (AH Harris Items Conversions - PRE CHECKS)
set serveroutput ON
BEGIN
  dbms_output.put_line('... Started');
  EXECUTE IMMEDIATE 'ALTER TRIGGER XXCUS_MTL_ITEMS_AUDIT_TRG DISABLE';
  dbms_output.put_line('... Alter XXCUS_MTL_ITEMS_AUDIT_TRG Trigger - Done');
  
  dbms_output.put_line('... <Ended');
EXCEPTION
  WHEN OTHERS THEN
      dbms_output.put_line('... Error Occurred: '||sqlerrm);

END;
/