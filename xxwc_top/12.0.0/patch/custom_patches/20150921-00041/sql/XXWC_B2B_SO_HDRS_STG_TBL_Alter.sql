/*************************************************************************************************
$Header XXWC_B2B_SO_HDRS_STG_TBL.sql $

Module Name: XXWC_B2B_SO_HDRS_STG_TBL

PURPOSE: Table to maintain Customer B2B PO Information.

REVISIONS:
Ver        Date         Author                Description
---------  -----------  ------------------    ----------------------------------------------
      1.0  09/21/2015   Gopi Damuluri         TMS# 20150615-00088 , TMS# 20150921-00041
                                              Additional columns for B2B Maintenance Form.
**************************************************************************************************/
UPDATE XXWC.XXWC_B2B_SO_HDRS_STG_TBL
   SET DELIVER_POA = NULL
 WHERE DELIVER_POA = 'N';
/

ALTER TABLE XXWC.XXWC_B2B_SO_HDRS_STG_TBL MODIFY (DELIVER_POA   NUMBER DEFAULT 0);
/

UPDATE XXWC.XXWC_B2B_SO_HDRS_STG_TBL
   SET DELIVER_POA = 0
 WHERE DELIVER_POA IS NULL;
/