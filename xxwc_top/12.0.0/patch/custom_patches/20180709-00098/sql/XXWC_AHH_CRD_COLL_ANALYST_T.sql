  /********************************************************************************
  FILE NAME: XXWC_AHH_CRD_COLL_ANALYST_T.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: Archive Table
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)         DESCRIPTION
  ------- -----------   ----------------  ----------------------------------------------------
  1.0     07/09/2018    Vamshi Palavarapu Initial Version.
  *******************************************************************************************/
WHENEVER SQLERROR CONTINUE
-- Drop Table
-- DROP TABLE XXWC.XXWC_AHH_CRD_COLL_ANALYST_T;

-- Create Table
CREATE TABLE XXWC.XXWC_AHH_CRD_COLL_ANALYST_T
(
  AHH_COLL_CODE    VARCHAR2(10),
  ORA_COLL_ID      NUMBER,
  COLLECTOR_NAME   VARCHAR2(150),
  ORA_CRD_ANLST_ID NUMBER,
  CRD_ANALYST_NAME VARCHAR2(150)
);

GRANT ALL ON XXWC.XXWC_AHH_CRD_COLL_ANALYST_T TO EA_APEX;
/