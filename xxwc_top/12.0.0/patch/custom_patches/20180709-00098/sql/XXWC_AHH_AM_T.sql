  /********************************************************************************
  FILE NAME: XXWC_AHH_AM_T.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: Archive Table
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)         DESCRIPTION
  ------- -----------   ----------------  ----------------------------------------------------
  1.0     07/09/2018    Vamshi Palavarapu Initial Version.
  *******************************************************************************************/
WHENEVER SQLERROR CONTINUE
-- Drop Table
-- DROP TABLE XXWC.XXWC_AHH_AM_T;

-- Create table
CREATE TABLE XXWC.XXWC_AHH_AM_T
(
  SLSREPOUT   VARCHAR2(20),
  SALESREP_ID NUMBER
);
/
GRANT ALL ON XXWC.XXWC_AHH_AM_T TO EA_APEX;
/