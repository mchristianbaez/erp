  /********************************************************************************
  FILE NAME: XXWC_AHH_AR_CUST_CROSS_OVER_T.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: Archive Table
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)         DESCRIPTION
  ------- -----------   ----------------  ----------------------------------------------------
  1.0     07/09/2018    Vamshi Palavarapu Initial Version.
  *******************************************************************************************/
WHENEVER SQLERROR CONTINUE
-- DROP TABLE
-- DROP TABLE XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T;

-- Create table
CREATE TABLE XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T
(
  CUST_NUM        VARCHAR2(20),
  CUST_SITE       VARCHAR2(50),
  CUST_NAME       VARCHAR2(240),
  CUST_ADD_LINE1  VARCHAR2(240),
  ORACLE_CUST_NUM VARCHAR2(20)
);

GRANT ALL ON XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T TO EA_APEX;
/