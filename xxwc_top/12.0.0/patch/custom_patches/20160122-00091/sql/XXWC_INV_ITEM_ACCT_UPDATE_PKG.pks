CREATE OR REPLACE PACKAGE APPS.XXWC_INV_ITEM_ACCT_UPDATE_PKG
/******************************************************************************************************
-- File Name: XXWC_INV_ITEM_ACCT_UPDATE_PKG.pks
--
-- PROGRAM TYPE: PL/SQL Script   <API>
--
-- PURPOSE: Developed to update item accounts (COGS, SALES and Expenses)
-- HISTORY
-- ==========================================================================================================
-- ==========================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- --------------------------------------------------------------------
-- 1.0     21-OCT-2016   P.Vamshidhar    TMS#20160122-00091  GL Code Enhancements to leverage GL API
--                                       Initial version

************************************************************************************************************/
IS
   PROCEDURE printlog (p_message IN VARCHAR2);

   FUNCTION create_ccid (p_concat_segs IN VARCHAR2)
      RETURN NUMBER;

   FUNCTION process_event (p_subscription_guid   IN            RAW,
                           p_event               IN OUT NOCOPY wf_event_t)
      RETURN VARCHAR2;

   FUNCTION accts_update (p_org_id IN NUMBER, p_item_id NUMBER)
      RETURN BOOLEAN;

   PROCEDURE addlogtable (p_item_id         IN NUMBER,
                          p_org_id          IN NUMBER,
                          p_acct_type       IN VARCHAR2,
                          p_old_acct_ccid   IN NUMBER,
                          p_new_acct_ccid   IN NUMBER,
                          p_status          IN VARCHAR2,
                          p_comments        IN VARCHAR2);

   PROCEDURE CATE_ITEM_ACCT_UPDATE (X_ERRBUF    OUT VARCHAR2,
                                    X_RETCODE   OUT VARCHAR2);

   FUNCTION INV_ITEM_ACCTS_UPD (p_item_id IN NUMBER)
      RETURN BOOLEAN;
END XXWC_INV_ITEM_ACCT_UPDATE_PKG;
/