/******************************************************************************************************
-- File Name: XXWC_GL_CONCATSEGS_U1.sql
--
-- PROGRAM TYPE: Index script
--
-- PURPOSE: Index to improve performance to retrive code_combination_id.
-- HISTORY
-- ==========================================================================================================
-- ==========================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- --------------------------------------------------------------------
-- 1.0     21-OCT-2016   P.Vamshidhar    TMS#20160122-00091  GL Code Enhancements to leverage GL API
--                                       Initial version

************************************************************************************************************/
CREATE UNIQUE INDEX XXWC_GL_CONCAT_SEGS_U1
   ON gl_code_combinations (
         segment1
      || '.'
      || segment2
      || '.'
      || segment3
      || '.'
      || segment4
      || '.'
      || segment5
      || '.'
      || segment6
      || '.'
      || segment7)      
/