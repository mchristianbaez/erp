/******************************************************************************************************
-- File Name: XXWC_MTL_CAT_UPDATES_LOG_TRG.sql
--
-- PROGRAM TYPE: Trigger
--
-- PURPOSE: Trigger created to track changes on MTL_CATEGORIES_B table for only attribute1, 
--          attribute2 and attribute 8

-- HISTORY
-- ==========================================================================================================
-- ==========================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- --------------------------------------------------------------------
-- 1.0     21-OCT-2016   P.Vamshidhar    TMS#20160122-00091  GL Code Enhancements to leverage GL API
--                                       Initial version

************************************************************************************************************/
CREATE OR REPLACE TRIGGER APPS.XXWC_MTL_CAT_UPDATES_LOG_TRG
   AFTER UPDATE
   ON INV.MTL_CATEGORIES_B
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
DECLARE
   lvc_cogs_acct    VARCHAR2 (150);
   lvc_sales_acct   VARCHAR2 (150);
   lvc_expn_acct    VARCHAR2 (150);
   l_sec            VARCHAR2 (250);
BEGIN
   IF    (NVL (:OLD.ATTRIBUTE1, '0') <> NVL (:NEW.ATTRIBUTE1, '0'))
      OR (NVL (:OLD.ATTRIBUTE2, '0') <> NVL (:NEW.ATTRIBUTE2, '0'))
      OR (NVL (:OLD.ATTRIBUTE8, '0') <> NVL (:NEW.ATTRIBUTE8, '0'))
   THEN
      IF NVL (:OLD.ATTRIBUTE1, '0') <> NVL (:NEW.ATTRIBUTE1, '0')
      THEN
         lvc_cogs_acct := :NEW.ATTRIBUTE1;
      END IF;

      IF NVL (:OLD.ATTRIBUTE2, '0') <> NVL (:NEW.ATTRIBUTE2, '0')
      THEN
         lvc_sales_acct := :NEW.ATTRIBUTE2;
      END IF;

      IF NVL (:OLD.ATTRIBUTE8, '0') <> NVL (:NEW.ATTRIBUTE8, '0')
      THEN
         lvc_expn_acct := :NEW.ATTRIBUTE8;
      END IF;

      l_sec := ' Inserting into Log table';

      INSERT INTO XXWC.XXWC_MTL_INV_CATE_UPDATE_LOG (CATEGORY_ID,
                                                     COGS_ACCT,
                                                     SALES_ACCT,
                                                     EXPN_ACCT,
                                                     PROCESS_STATUS,
                                                     CREATION_DATE)
           VALUES (:NEW.CATEGORY_ID,
                   lvc_cogs_acct,
                   lvc_sales_acct,
                   lvc_expn_acct,
                   'NEW',
                   SYSDATE);
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      XXCUS_error_pkg.XXCUS_error_main_api (
         p_called_from         => 'XXWC_MTL_CAT_UPDATES_LOG_TRG',
         p_calling             => l_sec,
         p_request_id          => NULL,
         p_ora_error_msg       => SUBSTR (SQLERRM, 1, 250),
         p_error_desc          => 'Error occured ' || :NEW.CATEGORY_ID,
         p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
         p_module              => 'XXWC');
END;
/